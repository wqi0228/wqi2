/*jslint browser: true */
/*global $, jQuery, alert, console, PF, Map, Set */
/*jshint esversion: 6 */

//tree 元件屬性
var PF_CLASS_UI_TREE_CONTAINER = "ui-tree-container";
var PF_CLASS_UI_TREE_COMPONENT = "ui-tree";
var PF_CLASS_UI_TREE_NODE = "ui-treenode";
var PF_CLASS_UI_TREE_NODE_TOGGLER = "ui-tree-toggler";
// tree node 的 toggler 已經展開了
var PF_CLASS_UI_TREE_TOGGLER_EXPEND = "ui-icon-triangle-1-s";
var PF_CLASS_UI_TREE_NODE_CONTENT = "ui-treenode-content";
//tree node 屬性: data-rowkey
var PF_CLASS_UI_TREE_NODE_ATTR_ROW_KEY = "data-rowkey";
//tree node 屬性: user sid
var MUTIUSERQKPKER_NODE_ATTR_USER_SID = "user-sid";

//data list 元件屬性
var PF_CLASS_UI_DATALIST_CONTENT = "ui-datalist-content";
var PF_CLASS_UI_DATALIST_DATA = "ui-datalist-data";
var PF_CLASS_UI_DATALIST_ITEM = "ui-datalist-item";


//標註 tree node 為 user node
var MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_IS_USER_NODE = "mutiuserqkpker-treenodeattr-is-user-node";
//標註 tree node 需隱藏 (by 不是本樹的節點 ：和另一個樹互斥)
var MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_HIDE_BY_NOT_MY_NODE = "mutiuserqkpker-treenodeattr-hide-by-selected";
//標註 tree node 需隱藏 (by 過濾)
var MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_HIDE_BY_FILTER = "mutiuserqkpker-treenodeattr-hide-by-filter";
//標註 tree node 需隱藏 (by 群組清單)
var MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_HIDE_BY_GROUP = "mutiuserqkpker-treenodeattr-hide-by-group";

//待選清單項目顯示樣式
var MUTIUSERQKPKER_CLASS_DATALIET_ITEM_STYLE = "mutiuserqkpker-datalist-item";
var MUTIUSERQKPKER_CLASS_DATALIET_ATTR_USER_SID = "userSid";

//為空文字訊息顯示訊息區 class
var MUTIUSERQKPKER_CLASS_AREA_EMPTY_MESSAGE = "mutiuserqkpker-area-emptyMessage";
//node name 的顯示區
var MUTIUSERQKPKER_CLASS_AREA_NODE_NAME = "mutiuserqkpker-area-nodename";
//標住名稱有被 highlight , 復原顯示用
var MUTIUSERQKPKER_CLASS_ATTR_IS_NAME_HIGHLIGHT = "mutiuserqkpker-attr-is-name-highlight";
//屬性 原始顯示名稱 (還原 highlight 用)
var MUTIUSERQKPKER_NODE_ATTR_ORIGINAL_NAME = "originalName";

//雙擊判斷 - 間隔時間 (毫秒)
var MUTIUSERQKPKER_SETTING_DOUBLE_CLICK_TIME = 400;
//過濾功能觸發等待時間 (毫秒)
var MUTIUSERQKPKER_SETTING_FILTER_WAIT_TIME = 1200;
//切換功能動畫時間 (毫秒)
var MUTIUSERQKPKER_TARGET_SHOW_MODE_SWITCH_DELAY = 0;
// highlight 的 calss
var MUTIUSERQKPKER_CLASS_HIGHLIGHT = "WS1-1-3";


function MultiUserQuickPicker(componentID, syncSelectedUserToServerFunc, defaultTargetAreaShowMode) {
	'use strict';
	'use esversion:6';

	var self = this;
	this.componentID = componentID;
	this.syncSelectedUserToServerFunc = syncSelectedUserToServerFunc;
	if (syncSelectedUserToServerFunc === undefined || !$.isFunction(syncSelectedUserToServerFunc)) {
		console.log("未傳入 update function !");
		throw "人員選單初始化失敗，請恰系統人員!";
	}
	this.defaultTargetAreaShowMode = defaultTargetAreaShowMode;

	//全部可選節點的 data
	this.allNodeDataClass = componentID + "_data_allNodeData";
	//已選節點的 data
	this.selectedNodeDataClass = componentID + "_data_selectedNodeData";
	//已選 usersid
	this.selectedUserSidsClass = componentID + "_data_selectedUserSids";

	//待選樹
	this.srcTreeClass = componentID + "_src_tree";
	//待選樹 Widget 名稱
	this.srcTreeWidgetName = componentID + "_src_tree_wv";
	//待選樹搜尋輸入框
	this.srcTreeFilterClass = componentID + "_src_tree_filter";
	//待選樹收合.展開按鈕區
	this.srcTreeECIconContainerClass = componentID + "_src_tree_ECIconContainer";
	//群組選單 WidgetVar name
	this.srcTreeGroupMenuWidgetVarName = componentID + "_src_tree_group_menu_widgetVar";

	//已選樹的區塊 (包含 top 區 、外框)
	this.targetTreeBlockClass = componentID + "_target_tree_block";
	//已選樹
	this.targetTreeClass = componentID + "_target_tree";
	//已選樹 Widget 名稱
	this.targetTreeWidgetName = componentID + "_target_tree_wv";
	//已選樹 搜尋輸入框
	this.targetTreeFilterClass = componentID + "_target_tree_filter";
	//已選樹 收合.展開按鈕區
	this.targetTreeECIconContainerClass = componentID + "_target_tree_ECIconContainer";
	//已選樹 顯示模式切換
	this.targetTreeDisplaySwitchClass = componentID + "_target_tree_display_switch";
	//已選樹 顯示模式切換
	this.targetTreeDisplaySwitchClass = componentID + "_target_tree_display_switch";

	//已選清單的區塊 (包含 top 區 、外框)
	this.targetListBlockClass = componentID + "_target_list_block";
	//已選清單
	this.targetListClass = componentID + "_target_list";
	//已選清單搜尋輸入框
	this.targetListFilterClass = componentID + "_target_list_filter";
	//已選樹 顯示模式切換
	this.targetListDisplaySwitchClass = componentID + "_target_list_display_switch";

	// =================================
	// 共用
	// =================================
	//已經選擇的 user Sid
	this.selectedUserSidsElm = $("." + this.selectedUserSidsClass);
	if (this.selectedUserSidsElm.length === 0) {
		console.log("取不到已選擇人員資料!  selectedUserSidsClass = [" + this.selectedUserSidsClass + "]");
		throw "人員選單初始化失敗，請恰系統人員!";
	}

	// =================================
	// 待選樹
	// =================================
	// 待選樹區域
	this.srcTree = $("." + this.srcTreeClass).find("." + PF_CLASS_UI_TREE_COMPONENT);
	if (this.srcTree.length === 0) {
		console.log("錯誤,元件找不到(待選樹區域):[" + this.srcTreeClass + "]");
		throw "人員選單初始化失敗，請恰系統人員!";
	}
	// 待選樹過濾文字區
	this.srcTreeFilter = $("." + this.srcTreeFilterClass);
	if (this.srcTreeFilter.length === 0) {
		console.log("錯誤,元件找不到(待選樹過濾文字區):[" + this.srcTreeFilterClass + "]");
		throw "人員選單初始化失敗，請恰系統人員!";
	}
	// 待選樹 Widget
	this.srcTreeWidget = PF(this.srcTreeWidgetName);
	if (this.srcTreeWidget === null) {
		console.log("錯誤,傳入待選樹 Widget 為空![" + this.srcTreeWidgetName + "]");
		throw "人員選單初始化失敗，請恰系統人員!";
	}

	// 待選樹開收合 ICON Container
	this.srcTreeECIconContainer = $("." + this.srcTreeECIconContainerClass);
	if (this.srcTreeECIconContainer.length === 0) {
		console.log("錯誤,元件找不到(待選樹開收合 ICON Container):[" + this.srcTreeECIconContainerClass + "]");
		throw "人員選單初始化失敗，請恰系統人員!";
	}

	this.srcTreeLoading = this.srcTree.parent().find(".cp-spinner");
	this.srcTreeLoading.hide();

	// =================================
	// 已選樹
	// =================================
	// 已選樹區
	this.targetTreeBlock = $("." + this.targetTreeBlockClass);
	if (this.targetTreeBlock.length == 0) {
		console.log("錯誤,元件找不到(已選樹區 targetTreeBlock):[" + targetTreeBlockClass + "]");
		throw "人員選單初始化失敗，請恰系統人員!";
	}
	// 已選樹區域
	this.targetTree = $("." + this.targetTreeClass).find("." + PF_CLASS_UI_TREE_COMPONENT);
	if (this.targetTree.length === 0) {
		console.log("錯誤,元件找不到(已選樹區域):[" + this.targetTreeClass + "]");
		throw "人員選單初始化失敗，請恰系統人員!";
	}
	// 已選樹過濾文字區
	this.targetTreeFilter = $("." + this.targetTreeFilterClass);
	if (this.targetTreeFilter.length === 0) {
		console.log("錯誤,元件找不到(已選樹過濾文字區):[" + this.targetTreeFilterClass + "]");
		throw "人員選單初始化失敗，請恰系統人員!";
	}
	// 已選樹 Widget
	this.targetTreeWidget = PF(this.targetTreeWidgetName);
	if (this.targetTreeWidget === null) {
		console.log("錯誤,傳入已選樹 Widget 為空! targetTreeWidgetName:[" + this.targetTreeWidgetName + "]");
		throw "人員選單初始化失敗，請恰系統人員!";
	}

	// 已選樹開收合 ICON Container
	this.targetTreeECIconContainer = $("." + this.targetTreeECIconContainerClass);
	if (this.targetTreeECIconContainer.length === 0) {
		console.log("錯誤,元件找不到(已選樹開收合 ICON Container):[" + this.targetTreeECIconContainerClass + "]");
		throw "人員選單初始化失敗，請恰系統人員!";
	}

	// 已選清單顯示模式切換開關
	this.targetTreeInputSwitch = $("." + this.targetTreeDisplaySwitchClass);
	if (this.targetTreeInputSwitch.length === 0) {
		console.log("錯誤,元件找不到(已選樹切換開關):[" + this.targetTreeDisplaySwitchClass + "]");
		throw "人員選單初始化失敗，請恰系統人員!";
	}
	
	this.targetTreeLoading = this.targetTree.parent().find(".cp-spinner");
	this.targetTreeLoading.hide();

	// =================================
	// 已選列表
	// =================================
	// 已選列表區
	this.targetListBlock = $("." + this.targetListBlockClass);
	if (this.targetListBlock.length == 0) {
		console.log("錯誤,元件找不到(已選列表區 targetListBlock):[" + targetListBlockClass + "]");
		throw "人員選單初始化失敗，請恰系統人員!";
	}

	// 已選列表區
	this.targetList = $("." + this.targetListClass).find("." + PF_CLASS_UI_DATALIST_CONTENT);
	if (this.targetList.length == 0) {
		console.log("錯誤,元件找不到(已選擇物件區 targetList):[" + targetListClass + "]");
		throw "人員選單初始化失敗，請恰系統人員!";
	}
	// 已選列表過濾文字區
	this.targetListFilter = $("." + this.targetListFilterClass);
	if (this.targetListFilter.length === 0) {
		console.log("錯誤,元件找不到(已選樹過濾文字區):[" + this.targetTreeFilterClass + "]");
		throw "人員選單初始化失敗，請恰系統人員!";
	}

	// 已選清單顯示模式切換開關
	this.targetListInputSwitch = $("." + this.targetListDisplaySwitchClass);
	if (this.targetListInputSwitch.length === 0) {
		console.log("錯誤,元件找不到(已選樹切換開關):[" + this.targetListDisplaySwitchClass + "]");
		throw "人員選單初始化失敗，請恰系統人員!";
	}

	// =================================
	// 依據傳入傳入類別, 取得對應 dom 物件
	// =================================
	this.func_tree_getTreeElm = function(treeType) {
		return (treeType === "src") ? self.srcTree : self.targetTree;
	};

	this.func_tree_getTreeWidget = function(treeType) {
		return (treeType === "src") ? self.srcTreeWidget : self.targetTreeWidget;
	};

	this.func_getFilter = function(filterType) {
		if (filterType === "src") {
			return self.srcTreeFilter;
		} else if (filterType === "target") {
			return self.targetTreeFilter;
		} else if (filterType === "list") {
			return self.targetListFilter;
		}
		throw "未定義的 filter 類別! [" + treeType + "]";
	};

	// ========================================================================
	// 初始化
	// ========================================================================
	// 所有節點資料 (Map<rowKey, nodeData>) - 後端帶入
	this.allNodeDataMapByRowKey = [];
	// 所有userNode資料 以 userSid 為 key (Map<userSid, nodeData>)
	this.userNodeDataMapByUserSid = new Map();
	// 所有user node rowkey 索引 (Map[sid, Set<rowkey>]) 因兼職主管, 故同一個 sid 可能有多個node
	this.allUserNodeRowkeysMapByNodeSid = new Map();
	// 已選擇節點資料 (Set) - 後端帶入
	this.selectedUserSids = new Set();

	// ====================================
	// funciton init
	// ====================================
	this.init = function() {
		// ---------------
		// 資料初始化
		// ---------------
		self.srcTreeFilter.val("");
		self.targetTreeFilter.val("");
		self.targetListFilter.val("");
		//若有顯示群組選單, 使其回到第一個選項
		if (PrimeFaces.widgets[self.srcTreeGroupMenuWidgetVarName]) {
			PrimeFaces.widgets[self.srcTreeGroupMenuWidgetVarName].selectValue("all");
		}

		//已選清單預設顯示模式
		let isListMode = (self.defaultTargetAreaShowMode === "LIST");
		this.targetTreeInputSwitch.prop("checked", isListMode);
		this.targetListInputSwitch.prop("checked", isListMode);
		//if (isListMode) {
		//    this.targetListBlock.show();
		//    this.targetTreeBlock.hide();
		//} else {
		//    this.targetTreeBlock.show();
		//    this.targetListBlock.hide();
		//}

		// ---------------
		// 解析 client 端資料
		// ---------------
		self.init_parseClientData();

		// ---------------
		// 標註 user 節點
		// ---------------
		//待選樹
		self.init_funcMarkUserNode(self.srcTree);
		//已選樹
		self.init_funcMarkUserNode(self.targetTree);

		// ---------------
		// 標註已選擇狀態
		// ---------------
		//待選樹
		self.prepareNodeHideMarkBySelectedUsers(self.srcTree, true);
		//已選樹
		self.prepareNodeHideMarkBySelectedUsers(self.targetTree, false);

		// ---------------
		// 重置顯示狀態
		// ---------------
		//兩個樹選單
		self.func_tree_resetAllTreeDisplayStatus();
		//已選擇列表
		self.func_target_list_resetTargetDataList();
	};
	// ====================================
	// function 解析 client 端資料
	// ====================================
	this.init_parseClientData = function() {

		self.allNodeDataMapByRowKey = [];
		self.userNodeDataMapByUserSid = new Map();
		self.allUserNodeRowkeysMapByNodeSid = new Map();
		self.selectedUserSidSet = new Set();

		// ---------------
		// 取得所有 node data
		// ---------------
		// 取得資料儲存區 （JSON String）
		var allNodeDataElm = $("." + self.allNodeDataClass);
		if (allNodeDataElm.length === 0) {
			console.log("錯誤,元件找不到(所有節點資料儲存區 allNodeData Class):[" + self.allNodeDataClass + "]");
			throw "人員選單初始化失敗，請恰系統人員!";
		}
		if (allNodeDataElm.length === 0 || allNodeDataElm.text().isEmpty()) {
			console.log("取不到節點資料! allNodeDataElm = [" + self.allNodeDataElm.text() + "]");
			throw "人員選單初始化失敗，請恰系統人員!";
		}


		//反解 base 64
		//let decodeBase64 = Base64.decode(allNodeDataElm.text());
		//console.log(decodeBase64);
		// 解析資料字串 JSON to object
		//self.allNodeDataMapByRowKey = JSON.parse(decodeBase64);
		self.allNodeDataMapByRowKey = JSON.parse(allNodeDataElm.text());

		// ---------------
		// 取得已選擇人員資料
		// ---------------
		self.selectedUserSidSet = new Set(JSON.parse(self.selectedUserSidsElm.val()));

		// ---------------
		// 整理資料
		// ---------------
		$.each(self.allNodeDataMapByRowKey, function(index, node) {
			// 以 sid 建立rowkey索引資料
			var rowKeySet = new Set();
			if (self.allUserNodeRowkeysMapByNodeSid.has(node.sid)) {
				rowKeySet = self.allUserNodeRowkeysMapByNodeSid.get(node.sid);
			}
			rowKeySet.add(node.rk);
			self.allUserNodeRowkeysMapByNodeSid.set(node.sid, rowKeySet);

			// 以 sid 建立user node data索引資料
			// 因兼職關係，同一個 user 會出現多筆, 在此只記錄第一筆
			// 給 dataList 用, 同一個人僅出現一次, 不用出現兼職資訊
			if (!self.userNodeDataMapByUserSid.has(node.sid)) {
				self.userNodeDataMapByUserSid.set(node.sid, node);
			}
		});
	};

	// ====================================
	// function 標註樹節點中的 user 節點
	// ====================================
	this.init_funcMarkUserNode = function(treeElm) {
		treeElm.find("." + PF_CLASS_UI_TREE_NODE).each(function() {
			var nodeData = self.allNodeDataMapByRowKey[$(this).attr("data-rowkey")];
			if (nodeData !== null && nodeData.isUr) {
				//標注此 node 為 user node
				$(this).addClass(MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_IS_USER_NODE);
				//標注此 node 的 user sid
				$(this).attr(MUTIUSERQKPKER_NODE_ATTR_USER_SID, nodeData.sid);
			}
		});
	};

	// ========================================================================
	// 標記樹選單下所有節點的隱藏狀態 (依據已選擇 user)
	// ========================================================================
	this.prepareNodeHideMarkBySelectedUsers = function(treeElm, isSrcTree) {
		// ---------------
		// 取得傳入樹下的所有user節點
		// ---------------
		var userNodes = treeElm.find("." + MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_IS_USER_NODE);
		if (userNodes.length === 0) {
			return;
		}

		// ---------------
		// 標記
		// ---------------
		for (var i = 0; i < userNodes.length; i++) {
			var currUserNode = $(userNodes[i]);

			//取得 tree node rowKey
			var currUserNodeRowkey = $(currUserNode).attr(PF_CLASS_UI_TREE_NODE_ATTR_ROW_KEY);
			if (currUserNodeRowkey.isEmpty()) {
				continue;
			}
			// 取得 tree node 資料
			var currUserNodeData = self.allNodeDataMapByRowKey[currUserNodeRowkey];

			//比對node是否在已選擇清單
			var isSelectedUser = self.selectedUserSidSet.has(currUserNodeData.sid);

			//待選樹
			if (isSrcTree) {
				if (isSelectedUser) {
					//隱藏已選擇
					self.tools_addClass(currUserNode, MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_HIDE_BY_NOT_MY_NODE);
				} else {
					//顯示未選擇
					currUserNode.removeClass(MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_HIDE_BY_NOT_MY_NODE);
				}
				//已選樹
			} else {
				if (isSelectedUser) {
					//顯示已選擇
					currUserNode.removeClass(MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_HIDE_BY_NOT_MY_NODE);
				} else {
					//隱藏未選擇
					self.tools_addClass(currUserNode, MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_HIDE_BY_NOT_MY_NODE);
				}
			}
		}
	};
	// ========================================================================
	// 處理樹的顯示狀態
	// ========================================================================
	// ====================================
	// function-tree-重置樹選單的顯示狀態
	// ====================================
	//重置待選樹、已選樹選單顯示狀態
	this.func_tree_resetAllTreeDisplayStatus = function() {
		//待選樹
		self.func_tree_resetTreeDisplayStatus(true, self.srcTree, self.srcTreeECIconContainer);
		//已選樹
		self.func_tree_resetTreeDisplayStatus(false, self.targetTree, self.targetTreeECIconContainer);
		//若已選樹不為空, 則全部展開
		if (self.selectedUserSidSet.size > 0) {
			self.event_tree_expandOrCollapseAllNode("target", true);
		}
	};
	//重置單一樹選單顯示狀態
	this.func_tree_resetTreeDisplayStatus = function(isSrcTree) {
		//取得 tree
		var treeElm = (isSrcTree) ? self.srcTree : self.targetTree;
		//取得 tree 收合、展開功能區域
		var treeECIconContainer = isSrcTree ? self.srcTreeECIconContainer : self.targetTreeECIconContainer;

		//重置樹選單顯示狀態, 並回傳是否有可以顯示的 node
		var hasDisplayNode = self.func_tree_resetTreeNodeDisplayStatus(treeElm);
		//畫面控制
		var treeContainer = treeElm.find("." + PF_CLASS_UI_TREE_CONTAINER);
		//樹選單區塊
		self.tools_elmDisplay(treeContainer, hasDisplayNode);
		//樹選單全部展開/收合按鈕
		self.tools_elmDisplay(treeECIconContainer, hasDisplayNode);
		//重置訊息欄顯示狀態
		self.func_tree_resetEmptyMessageArea(isSrcTree, treeElm);
	};

	// ====================================
	// function-tree-重置樹以下節點的顯示狀態 (show/hide)
	// ====================================
	this.func_tree_resetTreeNodeDisplayStatus = function(treeElm) {
		// 若父節點下無可顯示的節點, 則隱藏父節點
		var hasDisplayNode = false;

		// 取得所有的 treeNode
		var treeNodes = treeElm.find("." + PF_CLASS_UI_TREE_NODE);
		if (treeNodes.length == 0) {
			return hasDisplayNode;
		}

		//逐筆檢查
		for (var i = 0; i < treeNodes.length; i++) {
			var currTreeNode = $(treeNodes[i]);

			//判斷傳入 tree node 是否為 user node (反之為部門)
			var isUserNode = currTreeNode.is("." + MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_IS_USER_NODE);

			//逐筆判斷 node 顯示狀態
			var isShowNode = false;
			if (isUserNode) {
				//為 user node 時, 判斷是否沒有被標註隱藏
				if (currTreeNode.is(self.treeNodeSelector_isShowNode)) {
					isShowNode = true;
				}
			} else if (currTreeNode.find(self.treeNodeSelector_isShowNode).length !== 0) {
				//為 部門 node 時, 判斷下層是否有任一節點需要顯示
				isShowNode = true;
			}

			if (isShowNode) {
				currTreeNode.show();
				hasDisplayNode = true;
			} else {
				currTreeNode.hide();
			}
		}
		return hasDisplayNode;
	};
	// ====================================
	// function-tree-重置訊息欄顯示狀態
	// ====================================
	this.func_tree_resetEmptyMessageArea = function(isSrcTree, treeElm) {
		// ---------------
		// 建立或取得訊息區
		// ---------------
		var emptyMessageArea = treeElm.find("." + MUTIUSERQKPKER_CLASS_AREA_EMPTY_MESSAGE);
		if (emptyMessageArea.length == 0) {
			emptyMessageArea = $("<div></div>");
			emptyMessageArea.css("color", "gray");
			emptyMessageArea.css("margin", "20px 15px 0px 10px");
			emptyMessageArea.addClass(MUTIUSERQKPKER_CLASS_AREA_EMPTY_MESSAGE);
			treeElm.append(emptyMessageArea);
		}

		// ---------------
		// 判斷無須顯示
		// ---------------
		//查詢是否有可顯示 node
		var isHasShowNode = (treeElm.find(self.treeNodeSelector_isShowNode).length !== 0);
		//有可顯示節點時, 隱藏顯示區
		if (isHasShowNode) {
			emptyMessageArea.hide();
			return;
		}

		// ---------------
		// 判斷節點狀態
		// ---------------
		//取得 tree 自己的 node (排除已經移到 another tree)
		var treeNodes = treeElm.find(self.treeNodeSelector_isMyNode);
		//檢查是否有因為過濾或群組功能而隱藏的 node
		var hasHideNodeByFilter = false;
		for (var i = 0; i < treeNodes.length; i++) {
			var currTreeNode = $(treeNodes[i]);
			if (currTreeNode.hasClass(MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_HIDE_BY_FILTER) ||
				currTreeNode.hasClass(MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_HIDE_BY_GROUP)) {
				hasHideNodeByFilter = true;
				break;
			}
		}
		// ---------------
		// 顯示訊息
		// ---------------
		if (hasHideNodeByFilter) {
			//有因為過濾或群組功能而隱藏的 node
			emptyMessageArea.text("無符合結果，請清除『過濾條件』或『群組條件』！");
			//紅字顯示
			self.tools_addClass(emptyMessageArea, "WS1-1-2");
		} else {
			if (isSrcTree) {
				//待選樹訊息
				emptyMessageArea.text("已無可選擇人員");
			} else {
				//已選樹訊息
				emptyMessageArea.text("尚未選擇人員");
			}
			//一般顯示
			emptyMessageArea.removeClass("WS1-1-2");
		}
		// ---------------
		// 區塊控制
		// ---------------
		// 區塊顯示
		emptyMessageArea.show();
	};

	// ====================================
	// tree Node 展開、收合
	// ====================================
	// 節點展開
	this.func_tree_node_EC_expand = function(treeWidget, treeNodeElm) {
		if (self.func_tree_node_EC_isMustDoEC(treeNodeElm, true)) {
			treeWidget.expandNode(treeNodeElm);
		}
	};
	// 節點收合
	this.func_tree_node_EC_collapse = function(treeWidget, treeNodeElm) {
		if (self.func_tree_node_EC_isMustDoEC(treeNodeElm, false)) {
			treeWidget.collapseNode(treeNodeElm);
		}
	};
	// 依據節點狀態判斷是否需要做收合
	this.func_tree_node_EC_isMustDoEC = function(treeNodeElm, isDoExpand) {
		if (treeNodeElm === undefined || treeNodeElm === null) {
			console.log("傳入 treeNodeElm 為空!");
			//console.trace();
			return false;
		}
		//為 user 節點, 略過
		if (treeNodeElm.hasClass(MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_IS_USER_NODE)) {
			return false;
		}
		// 取得收合elm (toggler)
		var treeToggler = treeNodeElm.children("." + PF_CLASS_UI_TREE_NODE_CONTENT).children("." + PF_CLASS_UI_TREE_NODE_TOGGLER);
		if (treeToggler.length === 0) {
			return false;
		}
		//判斷是否已經展開了
		var isInExpand = treeToggler.hasClass(PF_CLASS_UI_TREE_TOGGLER_EXPEND);
		// 要做展開, 但已經展開了
		if (isDoExpand && isInExpand) {
			return false;
		}
		// 要做收合, 但已經收合了
		if (!isDoExpand && !isInExpand) {
			return false;
		}
		return true;
	};

	// 展開樹節點的上層, 直到頂端
	this.func_tree_node_EC_expandUntilRoot = function(treeWidget, treeElm) {

		// 無則終止
		if (treeElm === null || treeElm === undefined) {
			console.log("傳入為空");
			return;
		}

		// 取得父節點
		var parentElm = self.getParentNode(treeElm);
		// 無則終止
		if (parentElm == null) {
			return;
		}
		// 遞迴呼叫展開上層
		self.func_tree_node_EC_expandUntilRoot(treeWidget, parentElm);
		// 展開父節點
		self.func_tree_node_EC_expand(treeWidget, parentElm);
	};
	//取得上層 node
	this.getParentNode = function(targetElm) {
		var parentElm = $(targetElm).parent();
		//已達 doc 最頂端
		if (parentElm == null) {
			return null;
		}

		// 已達最上層- 終止
		if (parentElm.is("." + PF_CLASS_UI_TREE_CONTAINER)) {
			return null;
		}

		// 判斷父元素是否為 tree node
		if (parentElm.is("." + PF_CLASS_UI_TREE_NODE)) {
			return parentElm;
		}
		//遞迴往上爬
		return self.getParentNode(parentElm);
	};

	// ========================================================================
	// 搜尋
	// ========================================================================
	// ====================================
	// 輸入查詢關鍵字
	// ====================================
	//Filter 是否正在執行鎖
	this.treeNodeFilterProcessingLock = false;
	//Filter執行中，使用者在還未完成時，又觸發先的 Filter 事件
	this.treeNodeFilterGotNewEvent = false;
	//事件: filter onchange
	this.event_node_filter_onChange = function(filterType, filterElm, event) {
		//--------------------
		// 判斷
		//--------------------
		// 避免快速輸入時, 執行很多次 (鎖定每次執行完成之後,才可以再進去）
		// 1.確認 Filter 是否正在執行，若正在執行則忽略此次 onchange event
		// 2.紀錄有新的 onchange event ,等執行完後，自動再觸發一次執行
		if (self.treeNodeFilterProcessingLock) {
			self.treeNodeFilterGotNewEvent = true;
			return;
		}

		if (event !== undefined && event !== null && event.keyCode == 13) {
			//輸入 enter 時, 馬上觸發
			self.treeNodeFilter_process(filterType, filterElm);
		} else {
			// 延遲觸發, 等使用者輸入一定時間後才觸發 (壓到時間軸後面)
			setTimeout(function() {
				self.treeNodeFilter_process(filterType, filterElm);
			}, MUTIUSERQKPKER_SETTING_FILTER_WAIT_TIME);
		}
	};

	this.treeNodeFilter_process = function(filterType, filterElm) {
		//執行前先洗掉, 避免密集循環觸發
		self.treeNodeFilterGotNewEvent = false;

		//開始執行鎖
		self.treeNodeFilterProcessingLock = true;
		// 執行
		var nowTime = Date.now();
		if (filterType === "list") {
			self.target_list_doFilter();
		} else {
			//樹選單搜尋
			self.tree_node_filter_doFilter(filterType, filterElm);
		}
		//self.debug("Filter(" + filterType + ") [" + $(filterElm).val() + "] cost time:" + (Date.now() - nowTime));

		// 執行完成解鎖
		self.treeNodeFilterProcessingLock = false;

		// 執行過程中，使用者又異動 filter, 重新執行
		if (self.treeNodeFilterGotNewEvent) {
			self.treeNodeFilterGotNewEvent = false;
			self.treeNodeFilter(treeType, treeFilterElm);
		}
	};

	this.treeNodeFilterLastTimeKeyword = "";
	this.tree_node_filter_doFilter = function(treeType, treeFilterElm) {

		//self.debug("filter do tigger:[" + $(treeFilterElm).val() + "]");

		//1.含有關鍵字的 node name Highlight效果
		//2.含有關鍵字的 node 以上所有 node 展開
		//3.過濾:
		//  3-1.不含關鍵字的 user node 隱藏
		//  3-2.dep node 下層若不包含任何查詢結果時(沒有需要顯示的 node), dep node 隱藏
		//  3-3.包含關鍵字的 dep node
		//      3-3-1.本層『展開』
		//      3-3-2.若本曾以下還有未命中的子部門, 子部門步展開
		//      3-3-2.關鍵字部門以下所有 user 不被過濾(還可以選的到),

		// ---------------
		// 過濾關鍵字
		// ---------------
		// 取得本次輸入文字
		var filterKeyword = ($(treeFilterElm).val() + "").toUpperCase().trim();
		// 若與上次處理過的關鍵字一樣時，不處理 (srcTree、targetTree分開計算)
		var repeatKeyword = treeType + ":" + filterKeyword;
		if (self.treeNodeFilterLastTimeKeyword === repeatKeyword) {
			return;
		} else {
			self.treeNodeFilterLastTimeKeyword = repeatKeyword;
		}

		// ---------------
		// var
		// ---------------
		//取得選單樹
		var treeElm = self.func_tree_getTreeElm(treeType);
		//取得選單樹 Widget
		var treeWidget = self.func_tree_getTreeWidget(treeType);

		// ---------------
		// 清除所有的 higlight 效果
		// ---------------
		self.tools_highlight_removeStyle(treeElm);

		// ---------------
		// 關鍵字為空時，顯示全部
		// ---------------
		if (filterKeyword.isEmpty()) {
			//移除『因過濾器而隱藏』的標籤
			treeElm.find("." + MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_IS_USER_NODE).removeClass(MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_HIDE_BY_FILTER);
			//重置選單樹顯示
			self.func_tree_resetTreeDisplayStatus(("src" === treeType));
			//重置訊息欄顯示狀態
			self.func_tree_resetEmptyMessageArea(("src" === treeType), treeElm);
			//結束
			return;
		}

		// ---------------
		// 以遍歷的方式處理整個 tree 的所有 node
		// ---------------
		//取得 root 下的 node (第一層)
		var childTreeNodes = treeElm.children().children("." + PF_CLASS_UI_TREE_NODE);
		//逐筆處理第一層 node
		treeWidget.cfg.animate = false;
		childTreeNodes.each(function() {
			self.tree_node_filter_traversalTree($(this), treeWidget, filterKeyword, false);
		});
		treeWidget.cfg.animate = true;

		// ---------------
		// 重置訊息欄顯示狀態
		// ---------------
		self.func_tree_resetEmptyMessageArea(("src" === treeType), treeElm);
	};
	//以遍歷方式處理選單樹
	this.tree_node_filter_traversalTree = function(treeNodeElm, treeWidget, filterKeyword, isParentHit) {
		// ---------------
		// 準備node資料
		// ---------------
		//取得 data-rowKey
		var treeNodeRowkey = treeNodeElm.attr(PF_CLASS_UI_TREE_NODE_ATTR_ROW_KEY);
		if (treeNodeRowkey.isEmpty()) {
			console.log("treeNode 找不到屬性 data-rowKey!");
			return; //continue
		}

		//取得 node data
		if (!(treeNodeRowkey in self.allNodeDataMapByRowKey)) {
			console.log("以 data-rowKey 找不到 node data!");
			return; //continue
		}
		var treeNodeData = self.allNodeDataMapByRowKey[treeNodeRowkey];

		// ---------------
		// 處理名稱比對
		// ---------------
		//判斷名稱是否命中
		//比對時忽略大小寫
		var treeNodeName = (treeNodeData.na + "").toUpperCase().trim();
		//比對是否包含關鍵字
		var isNameCompareHit = treeNodeName.includes(filterKeyword);

		//關鍵字 highlight
		if (isNameCompareHit) {
			self.tools_highlight_doHighlightNodeName(
				treeNodeElm.children("." + PF_CLASS_UI_TREE_NODE_CONTENT),
				treeNodeData.na,
				filterKeyword);
		}

		// ---------------
		// user node 處理
		// ---------------
		if (treeNodeData.isUr) {
			if (isNameCompareHit || isParentHit) {
				//名稱命中,或直系上層部門名稱命中時, 顯示
				treeNodeElm.removeClass(MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_HIDE_BY_FILTER);
			} else {
				self.tools_addClass(treeNodeElm, MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_HIDE_BY_FILTER);
			}
			//是否顯示, 須考慮notmynode . filter , group
			var isShow = treeNodeElm.is(self.treeNodeSelector_isShowNode);
			if (isShow) {
				treeNodeElm.show();
			} else {
				treeNodeElm.hide();
			}
			// user node 無下層 直接 return
			// 判斷所有隱藏條件, 決定上層是否顯示
			return isShow;
		}

		// ---------------
		// dep node 處理
		// ---------------
		//是否因為子 node , 而需要展開
		var isExpendByChild = false;
		//處理下『一層』的  child node
		var childTreeNodes = treeWidget.getNodeChildrenContainer(treeNodeElm).children("." + PF_CLASS_UI_TREE_NODE);

		childTreeNodes.each(function() {
			if (self.tree_node_filter_traversalTree($(this), treeWidget, filterKeyword, isNameCompareHit)) {
				isExpendByChild = true;
			}
		});

		//case.1 此 node 名未稱命中, 且下層無命中 node
		if (!isNameCompareHit && !isExpendByChild) {
			//console.log("case1[" + treeNodeName + "]:" + isParentHit);
			//本節點隱藏
			treeNodeElm.hide();
			return false;
		}

		//case.2 部門名稱命中, 但以下 user 無命中
		//1.有 user 時, 保留所有 user , 但本層不展開
		//2.沒有user時 (已經被移走, group 過濾), 連部門也不顯示
		if (isNameCompareHit && !isExpendByChild) {
			//console.log("case2[" + treeNodeName + "]:" + isParentHit);
			//檢查以下是否還有可以顯示的 node
			if (treeNodeElm.find(self.treeNodeSelector_isShowNode).length > 0) {
				//本節點顯示, 上層展開
				treeNodeElm.show();
				return true;
			} else {
				//完全沒有可以顯示的 node, 不管怎麼樣都隱藏此節點
				treeNodeElm.hide();
				//上層不一定要展開
				return false;
			}
		}

		//case 3 下層有命中, 本層不管有沒有命中, 都顯示並展開本層, 且告知上層展開
		//顯示此 node
		treeNodeElm.show();
		//展開
		self.func_tree_node_EC_expand(treeWidget, treeNodeElm);
		return true;
	};

	// ========================================================================
	// 事件
	// ========================================================================
	// ====================================
	// 擊點 tree node (onClick)
	// ====================================
	this.event_tree_node_onClick = function(treeType, node, event) {

		// 取得(擊點)傳入 node 的 row key
		var rowkey = $(node).attr(PF_CLASS_UI_TREE_NODE_ATTR_ROW_KEY);
		if (rowkey.isEmpty()) {
			return;
		}

		// 判斷是否為雙擊
		if (!self.tools_isDoubleClick(treeType + "_" + rowkey)) {
			return;
		}

		// 取得選擇節點的 資料
		var nodeData = self.allNodeDataMapByRowKey[rowkey];
		if (nodeData == null) {
			console.error("傳入的 node 找不到對應資料! rowkey:[" + rowkey + "]");
			return;
		}

		self.func_tree_node_move(treeType, node, rowkey, nodeData);

	};
	this.func_tree_node_move = function(treeType, node, rowkey, nodeData) {
		//判斷為新增或移除
		var isAdd = (treeType === "src") ? true : false;
		// 為資料節點
		if (nodeData.isUr) {
			self.func_tree_nodeMove_forTreeNodeOnclick(isAdd, nodeData);
		}

		// 若選擇為 部門 節點, 加入下面所有未選擇的資料節點資料
		if (!nodeData.isUr) {
			// 取得下層所有 『可選擇 (排除 filter, group , 已挑選)』的user node
			var childUserNodes = $(node).find(self.treeNodeSelector_isShowNode);

			for (var i = 0; i < childUserNodes.length; i++) {
				var childUserNode = $(childUserNodes[i]);
				// 取得row key
				var childRowkey = $(childUserNode).attr(PF_CLASS_UI_TREE_NODE_ATTR_ROW_KEY);
				if (childRowkey.isEmpty()) {
					console.log("childRowkey is empty");
					continue;
				}
				var childUserNodeData = self.allNodeDataMapByRowKey[childRowkey];
				if (childUserNodeData == null) {
					console.error("傳入的 node 找不到對應資料! rowkey:[" + rowkey + "]");
					continue;
				}
				self.func_tree_nodeMove_forTreeNodeOnclick(isAdd, childUserNodeData);
			}
		}

		//新增 node 自動清除已選擇清單的過濾條件
		//體感上會比較不順 mark
		//if (treeType === "src") {
		//    this.event_filter_clearFilter("target");
		///   this.event_filter_clearFilter("list");
		//}

		//重置樹選單顯示狀態
		self.func_tree_resetAllTreeDisplayStatus();

		//重置已選清單顯示狀態
		self.func_target_list_resetTargetDataList();

		//更新已選擇清單到後端
		self.updateSelectedUsers();

		//轉移的 node forcs
		self.func_tree_node_focusTrnsNode(isAdd, rowkey);
	}


	//在移轉樹節點後, 將選單焦點, 移到被移轉後的 node 上
	this.func_tree_node_focusTrnsNode = function(isAdd, rowKey) {
		var treeElm = isAdd ? self.targetTree : self.srcTree;
		var treeWidget = isAdd ? self.targetTreeWidget : self.srcTreeWidget;
		var treeNode = treeElm.find("." + PF_CLASS_UI_TREE_NODE + "[" + PF_CLASS_UI_TREE_NODE_ATTR_ROW_KEY + "='" + rowKey + "']");
		if (treeNode.length != 0) {
			//展開
			treeWidget.cfg.animate = false;
			self.func_tree_node_EC_expandUntilRoot(treeWidget, treeNode);
			treeWidget.cfg.animate = true;

			//移除全部已選擇
			self.tools_unselectAllNode(treeElm, treeWidget);
			//選擇-本次移轉的 node
			treeWidget.selectNode(treeNode);

			//移動捲軸
			treeNode.scrollintoview({
				duration: "horizontal"
			});
		}
	};

	// ====================================
	// 事件樹選單全部展開、收合
	// ====================================
	this.event_tree_expandOrCollapseAllNode = function(treeType, isExpand) {
		// 依據類型取得要處理的樹
		var treeElm = self.func_tree_getTreeElm(treeType);
		var treeWidget = self.func_tree_getTreeWidget(treeType);

		// 取得所有節點
		var treeNodes = treeElm.find("." + PF_CLASS_UI_TREE_NODE + ":not(" + MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_IS_USER_NODE + ")");
		// disable 節點展開動畫 (避免處理過程過慢)
		treeWidget.cfg.animate = false;
		for (var i = 0; i < treeNodes.length; i++) {
			var treeNodeElm = $(treeNodes[i]);
			if (isExpand) {
				//展開
				self.func_tree_node_EC_expand(treeWidget, treeNodeElm);
			} else {
				//收合
				self.func_tree_node_EC_collapse(treeWidget, treeNodeElm);
			}
		}
		// enable 節點展開動畫
		treeWidget.cfg.animate = true;
	};

	// ====================================
	// 事件: clear filter
	// ====================================
	this.event_filter_clearFilter = function(filterType) {
		var filterElm = self.func_getFilter(filterType);
		//清空過濾框
		filterElm.val("");

		//呼叫 filter
		if (filterType === 'list') {
			self.target_list_doFilter();
		} else {
			self.treeNodeFilter_process(filterType, filterElm);
		}

		//為待選樹時,收合所有 node
		if (filterType === 'src') {
			self.event_tree_expandOrCollapseAllNode(filterType, false);
		}
	};

	// ====================================
	// 事件: 群組選單 on change
	// ====================================
	this.event_changeUserGroup = function(groupUserSidsJsonStr) {

		// ---------------
		// 解析傳入的群組名單 str
		// ---------------
		if (groupUserSidsJsonStr === null || groupUserSidsJsonStr.isEmpty() || groupUserSidsJsonStr === "all") {
			groupUserSidsJsonStr = "[]";
		}
		// JSON 解析後, 轉成 Set<String>
		let groupUserSids = new Set();
		JSON.parse(groupUserSidsJsonStr).forEach(function(userSid) {
			groupUserSids.add(userSid + "");
		});

		// ---------------
		// 逐筆標記非群組的 user
		// ---------------
		//取得所有的 user node
		let treeUserNodes = self.srcTree.find("." + MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_IS_USER_NODE);
		//收集
		let groupUserNodes = [];

		if (groupUserSids.size === 0) {
			treeUserNodes.removeClass(MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_HIDE_BY_GROUP);
		} else {
			//逐筆處理
			treeUserNodes.each(function() {
				let currUserNode = $(this);
				//取得該 node 的 user sid
				let userSid = currUserNode.attr(MUTIUSERQKPKER_NODE_ATTR_USER_SID);

				//比對是否在此傳入群組中
				if (groupUserSids.has(userSid + "")) {
					//移除隱藏標籤
					currUserNode.removeClass(MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_HIDE_BY_GROUP);
					//收集
					groupUserNodes.push(currUserNode);
				} else {
					//不是群組名單的 node 隱藏
					self.tools_addClass(currUserNode, MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_HIDE_BY_GROUP);
				}
			});
		}

		// ---------------
		// 重新整理顯示狀態
		// ---------------
		//待選樹顯示
		self.func_tree_resetTreeDisplayStatus(true, self.srcTree, self.srcTreeECIconContainer);

		//群組非空時, 展開 node (不使用全部展開方法, 以加速執行)
		if (groupUserSids.size > 0) {
			groupUserNodes.forEach(function(value) {
				let currTreeNode = $(value);
				if ($(currTreeNode).is(self.treeNodeSelector_isShowNode)) {
					self.func_tree_node_EC_expandUntilRoot(self.srcTreeWidget, currTreeNode);
				}
			});
		}
		//移除全部已選擇
		self.tools_unselectAllNode(self.srcTree, self.srcTreeWidget);
	};

	// ====================================
	// 事件: 已選選單模式切換(樹/列表)
	// ====================================
	this.event_switchTargetShowMode = function(eventSwitchElm) {
		eventSwitchElm = $(eventSwitchElm);
		let isChecked = eventSwitchElm.is(":checked");
		// ---------------
		// 將另外一個選單上的開關也同步切換
		// ---------------
		if (eventSwitchElm.is("." + self.targetTreeDisplaySwitchClass)) {
			//self.targetListInputSwitch.click();
			self.targetListInputSwitch.prop("checked", isChecked);
		} else {
			//self.targetTreeInputSwitch.click();
			self.targetTreeInputSwitch.prop("checked", isChecked);
		}

		// ---------------
		// 切換動畫
		// ---------------
		self.targetTreeInputSwitch.prop('disabled', true);
		if (isChecked) {
			//樹選單漸隱
			self.targetTreeBlock.animate({
				opacity: '0'
			}, MUTIUSERQKPKER_TARGET_SHOW_MODE_SWITCH_DELAY, function() {
				self.targetTreeBlock.hide();
				self.targetTreeInputSwitch.prop('disabled', false);
			});
			//清單顯示
			self.targetListBlock.css("opacity", 1);
			self.targetListBlock.show();
		} else {
			//清單漸隱
			self.targetListBlock.animate({
				opacity: '0'
			}, MUTIUSERQKPKER_TARGET_SHOW_MODE_SWITCH_DELAY, function() {
				self.targetListBlock.hide();
				self.targetTreeInputSwitch.prop('disabled', false);
			});
			//樹選單顯示
			self.targetTreeBlock.css("opacity", 1);
			self.targetTreeBlock.show();
		}
	};

	//重置群組選單, 使其回到第一項  (外部設定, callback 更新畫面用)
	this.event_resetGroupMenu = function() {
		//若有顯示群組選單, 使其回到第一個選項
		if (PrimeFaces.widgets[self.srcTreeGroupMenuWidgetVarName]) {
			PrimeFaces.widgets[self.srcTreeGroupMenuWidgetVarName].selectValue("all");
		}
		//移除被群組選項過濾的 user node
		self.event_changeUserGroup("all");
	};

	// ========================================================================
	// tree資料移動
	// ========================================================================
	// ====================================
	// 處理樹 node 移動 for tree mode on click
	// ====================================
	this.func_tree_nodeMove_forTreeNodeOnclick = function(isAdd, nodeData) {
		// ---------------
		// 更新已選擇列表
		// ---------------
		if (isAdd) {
			//新增:待選 -> 已選
			self.selectedUserSidSet.add(nodeData.sid);
		} else {
			//移除:已選 -> 待選
			self.selectedUserSidSet.delete(nodeData.sid);
		}
		// ---------------
		// 更新樹節點
		// ---------------
		self.func_tree_nodeMove(isAdd, nodeData);
	};
	// ====================================
	// tree資料移動 core
	// ====================================
	this.func_tree_nodeMove = function(isAdd, nodeData) {
		// ---------------
		// 處理選單
		// ---------------
		// 待選樹
		//var srcTreeNodes = self.srcTree.find(self.treeNodeSelector_findByRowKeySet(nodeRowKeySet));
		var srcTreeNodes = self.tools_findTreeNodeByUserSid(self.srcTree, nodeData.sid);
		if (srcTreeNodes.length != 0) {
			// 註記已經移到已選擇樹
			srcTreeNodes.each(function() {
				if (isAdd) {
					self.tools_addClass($(this), MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_HIDE_BY_NOT_MY_NODE);
				} else {
					$(this).removeClass(MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_HIDE_BY_NOT_MY_NODE);
				}
			});
		} else {
			console.error("src tree 找不到 userSid:[" + nodeData.sid + "]的 node");
		}

		// 已選樹
		//var targetTreeNodes = self.targetTree.find(self.treeNodeSelector_findByRowKeySet(nodeRowKeySet));
		var targetTreeNodes = self.tools_findTreeNodeByUserSid(self.targetTree, nodeData.sid);
		if (targetTreeNodes.length != 0) {
			// 註記 node 已經被移過來, 需要顯示 (remove hide tag)
			targetTreeNodes.each(function() {
				if (isAdd) {
					$(this).removeClass(MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_HIDE_BY_NOT_MY_NODE);
				} else {
					self.tools_addClass($(this), MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_HIDE_BY_NOT_MY_NODE);
				}
			});
		} else {
			console.error("target tree 找不到 userSid:[" + nodeData.sid + "]的 node");
		}
	};

	//以 userSid  取得tree node
	this.tools_findTreeNodeByUserSid = function(treeElm, userSid) {
		//兜組 selector
		//1.為 userNode
		var selector = "." + MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_IS_USER_NODE;
		//2.符合傳入 usersid
		selector += "[" + MUTIUSERQKPKER_NODE_ATTR_USER_SID + "='" + userSid + "']";
		//find
		return treeElm.find(selector);
	};

	// ====================================
	// 更新已選擇清單
	// ====================================
	this.updateSelectedUsers = function() {
		//放入已選擇 usersid 區
		//Set物件 不能直接 stringify, 須轉為 array
		self.selectedUserSidsElm.val(JSON.stringify(Array.from(self.selectedUserSidSet.values())));
		//將資料同步到 server
		self.syncSelectedUserToServerFunc();
	};
	// ========================================================================
	// 待選清單
	// ========================================================================
	this.func_target_list_resetTargetDataList = function() {

		// ---------------
		// 顯示區初始化清空顯示區
		// ---------------
		self.targetList.html("");

		// ---------------
		// 沒有資料
		// ---------------
		if (self.selectedUserSidSet.size === 0) {
			// 顯示提示訊息
			self.target_list_resetEmptyMessage();
			return;
		}

		// ---------------
		// 取得對應資料
		// ---------------
		var userNodeDatas = [];
		// 以 userSid 收集 user node data
		self.selectedUserSidSet.forEach(function(userSid) {
			//取得user node data
			if (!self.userNodeDataMapByUserSid.has(userSid)) {
				console.error(" 以 sid 找不到對應 node 資料 ! sid:[" + nodeData.sid + "]");
				return;
			}
			userNodeDatas.push(self.userNodeDataMapByUserSid.get(userSid));
		});

		// 排序 - 以樹狀清單順序(rowkey)
		userNodeDatas.sort(function(a, b) {
			if (a.rk > b.rk) {
				return 1;
			} else if (a.rk < b.rk) {
				return -1;
			}
			return 0;
		});

		// ---------------
		// 組顯示區域
		// ---------------
		//資料列表區域
		var dataListAreaElm = $("<ol></ol>");
		dataListAreaElm.addClass(PF_CLASS_UI_DATALIST_DATA);
		//列表項目(已選擇人員)
		for (var i = 0; i < userNodeDatas.length; i++) {
			var userNodeData = userNodeDatas[i];

			// create dataListItem (data item 容器)
			var dataListItemElm = $("<li></li>");
			dataListItemElm.addClass(PF_CLASS_UI_DATALIST_ITEM);

			// 停用時加上標記
			if (!userNodeData.act) {
				dataListItemElm.append("<span class='WS1-1-2'>(停用) - </span>");
			}

			// 顯示名稱
			var nodeNameElm = $("<label>" + userNodeData.lna + "</label>");
			//名稱區域標示
			nodeNameElm.addClass(MUTIUSERQKPKER_CLASS_AREA_NODE_NAME);
			//顯示樣是標示
			nodeNameElm.addClass(MUTIUSERQKPKER_CLASS_DATALIET_ITEM_STYLE);
			//屬性加上 此項目的 usersid
			nodeNameElm.attr(MUTIUSERQKPKER_CLASS_DATALIET_ATTR_USER_SID, userNodeData.sid);
			// 綁定 click 事件
			nodeNameElm.on("click", function() {
				var userSid = $(this).attr(MUTIUSERQKPKER_CLASS_DATALIET_ATTR_USER_SID);
				self.event_list_node_onClick(userSid);
			});
			//名稱加入 item 容器
			dataListItemElm.append(nodeNameElm);
			//劃底線
			dataListItemElm.append("<hr style='border-style: ridge'/>");
			//item 加入 列表容器
			dataListAreaElm.append(dataListItemElm);
		}
		//列表加入顯示區
		self.targetList.html(dataListAreaElm);
	};

	this.targetListFilterLastTimeKeyword = "";
	this.target_list_doFilter = function() {
		// ---------------
		// 過濾關鍵字
		// ---------------
		// 取得本次輸入文字
		var filterKeyword = (self.targetListFilter.val() + "").toUpperCase().trim();
		// 若與上次處理過的關鍵字一樣時，不處理 (srcTree、targetTree分開計算)
		if (self.targetListFilterLastTimeKeyword === filterKeyword) {
			return;
		} else {
			self.targetListFilterLastTimeKeyword = filterKeyword;
		}

		// ---------------
		// 清除所有的 higlight 效果
		// ---------------
		self.tools_highlight_removeStyle(self.targetList);

		// ---------------
		// 取得所有項目
		// ---------------
		var itemElms = self.targetList.find("." + PF_CLASS_UI_DATALIST_ITEM);
		if (itemElms.length === 0) {
			//處理空訊息顯示區
			self.target_list_resetEmptyMessage();
			return;
		}

		// ---------------
		// 關鍵字為空時，顯示全部
		// ---------------
		if (filterKeyword.isEmpty()) {
			//顯示所有存在項目
			itemElms.show();
			//處理空訊息顯示區
			self.target_list_resetEmptyMessage();
			return;
		}

		// ---------------
		// 逐筆處理
		// ---------------
		itemElms.each(function() {
			var pfItemElm = $(this);
			//找到名稱顯示容器
			var itemElm = pfItemElm.find("." + MUTIUSERQKPKER_CLASS_AREA_NODE_NAME);
			if (itemElm.length === 0) {
				console.error("[" + PF_CLASS_UI_DATALIST_ITEM + "]元素下找不到[" + MUTIUSERQKPKER_CLASS_AREA_NODE_NAME + "]元素");
				return; //等於 continue
			}
			//取得 user sid
			var userSid = itemElm.attr(MUTIUSERQKPKER_CLASS_DATALIET_ATTR_USER_SID);

			//取得 user node data
			if (!self.userNodeDataMapByUserSid.has(userSid)) {
				console.error(" 以 sid 找不到對應 node 資料 ! sid:[" + nodeData.sid + "]");
				return;
			}
			var nodeData = self.userNodeDataMapByUserSid.get(userSid);

			//判斷名稱是否命中
			//比對時忽略大小寫
			var nodeName = (nodeData.lna + "").toUpperCase().trim();
			//比對是否包含關鍵字
			var isNameCompareHit = nodeName.includes(filterKeyword);

			if (isNameCompareHit) {
				//關鍵字 highlight
				self.tools_highlight_doHighlightNodeName(pfItemElm, nodeData.lna, filterKeyword);
				//顯示項目
				pfItemElm.show();
			} else {
				//隱藏項目
				pfItemElm.hide();
			}
		});

		//處理空訊息顯示區
		self.target_list_resetEmptyMessage();
	};

	this.target_list_resetEmptyMessage = function() {
		//取得顯示區
		var emptyMessageArea = self.targetList.find("." + MUTIUSERQKPKER_CLASS_AREA_EMPTY_MESSAGE);

		if (emptyMessageArea.length === 0) {
			emptyMessageArea = $("<div style='text-align: center;'></div>");
			emptyMessageArea.css("color", "gray");
			emptyMessageArea.addClass(MUTIUSERQKPKER_CLASS_AREA_EMPTY_MESSAGE);
			self.targetList.prepend(emptyMessageArea);
		}

		//查詢所有列表項目
		var items = self.targetList.find("." + PF_CLASS_UI_DATALIST_ITEM);

		if (items.length === 0) {
			//無可選項目
			emptyMessageArea.removeClass("WS1-1-2");
			emptyMessageArea.text("尚未選擇人員");
			emptyMessageArea.show();

		} else if (items.find(":visible").length == 0) {
			//因過濾條件，而無可以顯示的項目 (全部被隱藏了)
			//無可選項目
			self.tools_addClass(emptyMessageArea, "WS1-1-2");
			emptyMessageArea.text("無符合結果，請清除過濾條件！");
			emptyMessageArea.show();
		} else {
			if (emptyMessageArea.is(":visible")) {
				emptyMessageArea.hide();
			}
		}
	};


	this.event_list_node_onClick = function(userSid) {
		// ---------------
		// 判斷是否為雙擊
		// ---------------
		if (!self.tools_isDoubleClick("list_" + userSid)) {
			return;
		}

		// ---------------
		//移除:已選 -> 待選
		// ---------------
		self.selectedUserSidSet.delete(userSid);

		// ---------------
		//移除擊點的節點
		// ---------------
		//找到擊點的 item
		var selectItem = self.targetList.find(
			"." + MUTIUSERQKPKER_CLASS_AREA_NODE_NAME +
			"[" + MUTIUSERQKPKER_CLASS_DATALIET_ATTR_USER_SID + "=" + userSid + "]");
		//往上一層後 - 移除 (PF_CLASS_UI_DATALIST_ITEM)
		selectItem.parent().remove();

		// ---------------
		// 處理空訊息區塊
		// ---------------
		self.target_list_resetEmptyMessage();

		// ---------------
		// 更新已選擇清單到後端
		// ---------------
		self.updateSelectedUsers();

		// ---------------
		// 重整樹選單
		// ---------------
		//取得 user node data
		if (!self.userNodeDataMapByUserSid.has(userSid)) {
			console.error(" 以 sid 找不到對應 node 資料 ! sid:[" + nodeData.sid + "]");
			return;
		}
		var nodeData = self.userNodeDataMapByUserSid.get(userSid);

		//tree node move
		self.func_tree_nodeMove(false, nodeData);

		//重置樹選單顯示狀態
		self.func_tree_resetAllTreeDisplayStatus();
	};

	// ========================================================================
	// 工具
	// ========================================================================
	// 前次擊點節點
	var prevClickRowkey = "";
	// 前次擊點時間
	var prevClickTime = 0;
	// 模擬判斷是否 double click
	this.tools_isDoubleClick = function(key) {
		// 取得目前時間
		var nowTime = Date.now();
		// 比較上次擊點時間
		var intervalTime = nowTime - prevClickTime;
		// 判斷須為上次擊點的 node , 且間隔時間不大於定值
		var result = (key == prevClickRowkey && intervalTime <= MUTIUSERQKPKER_SETTING_DOUBLE_CLICK_TIME);

		// 更新本次擊點狀態
		prevClickRowkey = key;
		prevClickTime = nowTime;

		return result;
	};

	//比較
	this.tools_compareInt = function(val1, val2) {
		return (val1 + "") === (val2 + "");
	};

	//改變元素顯示狀態
	this.tools_elmDisplay = function(elm, isShow) {
		if (elm == undefined || elm == null) {
			console.log("傳入元素為空");
			console.trace();
			return;
		}
		var currElm = $(elm);
		if (currElm.length == 0) {
			console.log("傳入元素個數為0");
			console.trace();
			return;
		}

		if (isShow) {
			currElm.show();
		} else {
			currElm.hide();
		}
	};

	//對元素新增class
	this.tools_addClass = function(elm, className) {
		if (elm == null || elm == undefined) {
			console.log("傳入elm 為空!");
			return;
		}
		if ($(elm).hasClass(className)) {
			return;
		}
		$(elm).addClass(className);
	};


	// ====================================
	// 移除所有已選擇的 node
	// ====================================
	this.tools_unselectAllNode = function(treeElm, treeWidget) {
		//移除已選擇
		let selectedNodes = treeElm.find("." + PF_CLASS_UI_TREE_NODE + "[aria-selected='true']");
		selectedNodes.each(function() {
			treeWidget.unselectNode($(this));
		});
	};

	// ====================================
	// highlight 相關
	// ====================================
	//對名稱進行關鍵字  highlight
	this.tools_highlight_doHighlightNodeName = function(targetElm, originalName, filterKeyword) {
		//以下處理
		//取得名稱顯示區
		var nameElm = targetElm.find("." + MUTIUSERQKPKER_CLASS_AREA_NODE_NAME);
		//關鍵字標亮
		var highlightContext = self.tools_highlight_prepareHighlightContext(originalName, filterKeyword);
		//取代
		nameElm.html(highlightContext);
		//標注被 highlight (快速還原顯示用)
		self.tools_addClass(nameElm, MUTIUSERQKPKER_CLASS_ATTR_IS_NAME_HIGHLIGHT);
		//紀錄原始名稱
		nameElm.attr(MUTIUSERQKPKER_NODE_ATTR_ORIGINAL_NAME, originalName);
	};

	//準備關鍵字 highlight 顯示內容
	this.tools_highlight_prepareHighlightContext = function(nodeName, keyword) {

		nodeName = (nodeName + "").trim();
		keyword = (keyword + "").trim();
		if (keyword.isEmpty()) {
			return nodeName;
		}

		var nodeNameForCompare = nodeName.toUpperCase();
		var keywordForCompare = keyword.toUpperCase();
		// 相等時直接標亮全部
		if (nodeNameForCompare === keywordForCompare) {
			return self.tools_highlightStr(nodeName);
		}

		var highlightContext = "";
		var queryKeywordLength = keyword.length;

		// 逐字比對
		for (var i = 0; i < nodeName.length; i++) {
			var currContext = nodeNameForCompare.substr(i);
			if (currContext.startsWith(keywordForCompare)) {
				highlightContext += self.tools_highlightStr(nodeName.substr(i, queryKeywordLength));
				i += queryKeywordLength - 1;
			} else {
				highlightContext += nodeName.substr(i, 1);
			}
		}
		return highlightContext;
	};

	//highlight
	this.tools_highlightStr = function(str) {
		return "<span class='" + MUTIUSERQKPKER_CLASS_HIGHLIGHT + "'>" + str + "</span>";
	};

	//移除所有的 Highlight 效果
	this.tools_highlight_removeStyle = function(treeElm) {
		//取得所有有被 highlight的 tree node name 顯示區
		treeElm.find("." + MUTIUSERQKPKER_CLASS_ATTR_IS_NAME_HIGHLIGHT).each(function() {
			var teeNodeNameArea = $(this);
			//顯示文字還原
			teeNodeNameArea.text(teeNodeNameArea.attr(MUTIUSERQKPKER_NODE_ATTR_ORIGINAL_NAME));
			//移除 highlight 標記
			teeNodeNameArea.removeClass(MUTIUSERQKPKER_CLASS_ATTR_IS_NAME_HIGHLIGHT);
		});
	};

	// ========================================================================
	// tree node selector
	// ========================================================================
	// ====================================
	// 為可顯示的 node
	// ====================================
	//為 userNode, 但排除有 hide1 或 hide2 或 hide3
	//#('.usernode:not(.hide1,.hide2,.hide3)')
	//為 user node
	this.treeNodeSelector_isShowNode = "." + MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_IS_USER_NODE;
	this.treeNodeSelector_isShowNode += ":not(";
	//不為被隱藏節點(因選擇狀態)
	this.treeNodeSelector_isShowNode += "." + MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_HIDE_BY_NOT_MY_NODE;
	//不為被隱藏節點(因過濾)
	this.treeNodeSelector_isShowNode += ",." + MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_HIDE_BY_FILTER;
	//不為被隱藏節點(因群組過濾)
	this.treeNodeSelector_isShowNode += ",." + MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_HIDE_BY_GROUP;
	this.treeNodeSelector_isShowNode += ")";
	// ====================================
	// curr tree node (排除因被選擇而移到 another tree 的 user node)
	// ====================================
	this.treeNodeSelector_isMyNode = "." + MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_IS_USER_NODE;
	this.treeNodeSelector_isMyNode += ":not(." + MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_HIDE_BY_NOT_MY_NODE + ")";
	// ====================================
	// find tree node By rowKey
	// ====================================
	this.treeNodeSelector_findByRowKeySet = function(rowkeySet) {
		var selector = "";
		var isFirst = true;
		rowkeySet.forEach(function(value) {
			if (isFirst) {
				isFirst = false;
			} else {
				selector += ",";
			}
			selector += "." + MUTIUSERQKPKER_CLASS_TREE_NODE_ATTR_IS_USER_NODE;
			selector += "[" + PF_CLASS_UI_TREE_NODE_ATTR_ROW_KEY + "='" + value + "']";
		});
		return selector;
	};

	// ========================================================================
	// debug 訊息控制
	// ========================================================================
	this.debug = function(message) {
		if (true) {
			console.log(message);
		}
	};

	// ========================================================================
	// 初始化元件
	// ========================================================================
	try {
		//$(".quick-select-tree-selected-list").hide();
		self.init();
		console.log("init :[" + componentID + "]");
	} catch (e) {
		console.log("初始化失敗:" + e);
		console.log(e);
	}
}

MultiUserQuickPicker.prototype = {
	constructor: MultiUserQuickPicker
};

// String 擴充方法 ,判斷是否為空
if (typeof String.prototype.isEmpty != 'function') {
	String.prototype.isEmpty = function() {
		return (this == null || this.length === 0 || !this.trim());
	};
}

if (!String.prototype.trim) {
	String.prototype.trim = function() {
		return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
	};
}
/**
 * startsWith (for safari)
 */
if (!String.prototype.startsWith) {
	Object.defineProperty(String.prototype, 'startsWith', {
		value: function(search, pos) {
			pos = !pos || pos < 0 ? 0 : +pos;
			return this.substring(pos, pos + search.length) === search;
		}
	});
}

if (!String.prototype.includes) {
	String.prototype.includes = function(search, start) {
		'use strict';
		if (search instanceof RegExp) {
			throw TypeError('first argument must not be a RegExp');
		}
		if (start === undefined) {
			start = 0;
		}
		return this.indexOf(search, start) !== -1;
	};
}