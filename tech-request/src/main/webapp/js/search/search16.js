/* 
 * ON程式 廳主全選功能
 * 
 */

var idCusManyMenu = "#idCusManyMenu";
var idCusSelAll = "#idCusSelAll";
function toggleCustomerSelPanelSmm() {
    var isSelAll = $(idCusSelAll + "_input").attr("aria-checked") === 'false';
    if ($(idCusManyMenu).length > 0) {
        var pushValue = [];
        $(idCusManyMenu + " .ui-selectlistbox-list").find("tr").each(function () {
            eventByChangeClz(isSelAll, $(this), "ui-state-highlight");
            $(this).find(".ui-chkbox-box").each(function () {
                eventByChangeClz(isSelAll, $(this), "ui-state-active");
                $(this).find(".ui-chkbox-icon").each(function () {
                    eventByChangeClz(isSelAll, $(this), "ui-icon-check", "ui-icon-blank");
                });
            });
        });
        if (isSelAll) {
            $(idCusManyMenu + "_input").children().each(function () {
                pushValue.push($(this).val());
            });
        }
        $(idCusManyMenu + "_input").val(pushValue);
    }
}

function eventByChangeClz(selAll, obj, clzOne, ClzTwo) {
    if (selAll) {
        $(obj).addClass(clzOne);
        if (ClzTwo !== null) {
            $(obj).removeClass(ClzTwo);
        }
    } else {
        $(obj).removeClass(clzOne);
        if (ClzTwo !== null) {
            $(obj).addClass(ClzTwo);
        }
    }
}

function toggleCustomerSelPanelSmmItem() {
    var allSize = $(idCusManyMenu + " .ui-selectlistbox-item").size();
    var activeSize = $(idCusManyMenu + " .ui-state-active").size();
    if (allSize === activeSize) {
        $(idCusSelAll).children(" .ui-chkbox-box").addClass("ui-state-active");
        $(idCusSelAll).children(" .ui-chkbox-box").children(".ui-chkbox-icon").removeClass("ui-icon-blank");
        $(idCusSelAll).children(" .ui-chkbox-box").children(".ui-chkbox-icon").addClass("ui-icon-check");
    } else {
        $(idCusSelAll).children(" .ui-chkbox-box").removeClass("ui-state-active");
        $(idCusSelAll).children(" .ui-chkbox-box").children(".ui-chkbox-icon").removeClass("ui-icon-check");
        $(idCusSelAll).children(" .ui-chkbox-box").children(".ui-chkbox-icon").addClass("ui-icon-blank");
    }
}
