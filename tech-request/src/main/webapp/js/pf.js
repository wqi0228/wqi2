/**
 * 
 */

// 進dialog 跳過calendar.foucs
PrimeFaces.widget.Dialog.prototype.applyFocus = function() {
	var firstInput = this.jq.find(':not(:submit):not(:button):input:visible:enabled:first');
	if (!firstInput.hasClass('hasDatepicker')) {
		firstInput.focus();
	}
}
// 進OverlayPanel 跳過calendar.foucs
PrimeFaces.widget.OverlayPanel.prototype.applyFocus = function() {
	var firstInput = this.jq.find(':not(:submit):not(:button):input:visible:enabled:first');
	if (!firstInput.hasClass('hasDatepicker')) {
		firstInput.focus();
	}
}

PrimeFaces.locales['zh_TW'] = {
	closeText: '關閉',
	prevText: '上個月',
	nextText: '下個月',
	currentText: '今天',
	monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
	monthNamesShort: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
	dayNames: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
	dayNamesShort: ['日', '一', '二', '三', '四', '五', '六'],
	dayNamesMin: ['日', '一', '二', '三', '四', '五', '六'],
	weekHeader: '周',
	firstDay: 1,
	isRTL: false,
	showMonthAfterYear: true,
	yearSuffix: '', // 年
	timeOnlyTitle: '僅時間',
	timeText: '時間',
	hourText: '時',
	minuteText: '分',
	secondText: '秒',
	ampm: false,
	month: '月',
	week: '周',
	day: '日',
	allDayText: '全天'
};

//防止所有未另行加入enter觸發事件呼叫ActionListener
$(function() {
	//除了input text外，停用Backsapce，防止不慎回前一頁
	$(document).keydown(function(e) {
		if (e.which === 8 && !(e.target.type === "text" || e.target.type === "textarea" || e.target.type === "password")) {
			e.preventDefault();
		}
		//除了textarea外，停用ENTER，以防觸發button
		if (e.which === 13 && e.target.type !== "textarea") {
			e.preventDefault();
		}
	});
});

var win;

function openInNewTab(url) {
	win = window.open(url, '_blank');
}

function openTabByUrl(url) {
	win = window.open(url);
}

function replaceUrl(url) {
	win.location = url;
}

jQuery(window).load(function() {
	reEditor();
});

function reEditor() {
	$("form").find(".ui-editor").each(function() {
		var widthLenght = $(this).width() - 30;
		var heightLenght = $(this).height() - 35;
		$(this).find('iframe').contents().find('body').css({
			'word-wrap': 'break-word',
			'display': 'block',
			'white-space': 'pre',
			'height': heightLenght,
			'width': widthLenght,
			'word-break': 'break-word',
		});
	});
}
//判斷為iframe
function checkIframe() {
	if (self != top) {
		return true;
	}
	return false;
}
//判斷是否按下Enter按鈕
function checkEnter(event) {
	if (event.keyCode == 13) {
		return true;
	}
	return false;
}
//取得元件
function getElementId(obj) {
	return $(document.getElementById(obj));
}
//取得元件
function getElementIdAndCssName(obj, cssName) {
	return $(document.getElementById(obj).getElementsByClassName(cssName));
}
//判斷元件是否存在
function checkElementId(obj) {
	if (getElementId(obj).length > 0) {
		return true;
	}
	return false;
}
//顯示剩餘時間
function countDown(timeStr) {
	$('#countDownDisplay').countdown(timeStr, function(event) {
		$(this).html(event.strftime('可編輯剩餘時間： %M:%S'));
	});
}

function getUrlParameter(sParam) {
	var sPageURL = decodeURIComponent(window.location.search.substring(1)),
		sURLVariables = sPageURL.split('&'),
		sParameterName, i;

	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');

		if (sParameterName[0] === sParam) {
			return sParameterName[1] === undefined ? true : sParameterName[1];
		}
	}
}

//重新搜尋的空方法，主要讓各報表覆寫
var reSearch = function() {
	console.log('no reSearch');
}

// 檢查widgets是否存在
function checkWidgets(obj) {
	if (PrimeFaces.widgets[obj]) {
		return true;
	}
	console.error("查無 widgetVarl：" + obj);
	return false;
}

// 開啟primefaces元件
function showPF(obj) {
	if (!checkWidgets(obj)) {
		return;
	}
	try {
		PF(obj).show();
	} catch (err) {
		console.log(err);
	}
}
// 隱藏primefaces元件
function hidePF(obj) {
	if (!checkWidgets(obj)) {
		return;
	}
	try {
		PF(obj).hide();
	} catch (err) {
		console.log(err);
	}
}
// editor元件存入內容
function saveHTML(obj) {
	if (!checkWidgets(obj)) {
		return;
	}
	PF(obj).saveHTML();
}
// focus primefaces元件
function focusPF(obj) {
	if (!checkWidgets(obj)) {
		return;
	}
	PF(obj).focus();
}

function moveScrollBottom() {
	$("html, body").animate({
		scrollTop: $(document).height()
	}, 500);
}

function moveScrollTop() {
	if ($("#viewPanel .ui-scrollpanel").length > 0) {
		$("#viewPanel").scrollTop("0");
	}
	if ($("#req03scrid .ui-scrollpanel").length > 0) {
		$("#req03scrid").scrollTop("0");
	}
}

/**
 * 開啟所有頁籤(以 p:tabView id="viewPanelBottomInfoTabId" 為基準)
 * 
 * @param widgetVar
 *            p:accordionPanel 的 widgetVar
 * @param widgetId
 *            p:accordionPanel 的 id
 */
function openAllTab(tabViewId, widgetVar, widgetId) {
	if (!checkWidgets(widgetVar)) {
		return;
	}
	$('#' + tabViewId + '\\:' + widgetId).children(".ui-accordion-header").each(function(index) {
		PF(widgetVar).select(index);
	});
}
/**
 * 關閉所有頁籤(以 p:tabView id="viewPanelBottomInfoTabId" 為基準)
 * 
 * @param widgetVar
 *            p:accordionPanel 的 widgetVar
 * @param widgetId
 *            p:accordionPanel 的 id
 */
function closeAllTab(tabViewId, widgetVar, widgetId) {
	if (!checkWidgets(widgetVar)) {
		return;
	}
	$('#' + tabViewId + '\\:' + widgetId).children(".ui-accordion-header").each(function(index) {
		PF(widgetVar).unselect(index);
	});
}

// 展開
function expendWidget(widgetVar, size, hideClzName, showClzName) {
	if (hideClzName != null && showClzName != null) {
		document.getElementsByClassName(hideClzName)[0].style.display = 'none';
		document.getElementsByClassName(showClzName)[0].style.display = 'inline-block';
	}
	if (!checkWidgets(widgetVar)) {
		return;
	}
	for (var i = 0; i < size; i++) {
		PF(widgetVar).select(i);
	}
}

function foldWidget(widgetVar, size, hideClzName, showClzName) {
	if (hideClzName != null && showClzName != null) {
		document.getElementsByClassName(hideClzName)[0].style.display = 'none';
		document.getElementsByClassName(showClzName)[0].style.display = 'inline-block';
	}
	if (!checkWidgets(widgetVar)) {
		return;
	}
	for (var i = 0; i < size; i++) {
		PF(widgetVar).unselect(i);
	}
}
// p:accordionPanel use
function accPanelSelectTab(widgetVar, idx) {
	if (!checkWidgets(widgetVar)) {
		return;
	}
	PF(widgetVar).select(idx);
	if (idx !== 0) {
		for (var i = 0; i < idx; i++) {
			PF(widgetVar).unselect(i);
		}
	}
}

function accPanelSelectTabNoCloseOther(widgetVar, idx) {
	if (!checkWidgets(widgetVar)) {
		return;
	}
	PF(widgetVar).select(idx);
}

function bottomTabCloseAndSel(widgetVar, idx, size) {
	if (!checkWidgets(widgetVar)) {
		return;
	}
	PF(widgetVar).select(idx);
	for (var i = 0; i < size; i++) {
		if (i !== idx)
			PF(widgetVar).unselect(i);
	}
}

/**
 * @param widgetVarStr
 * @param onclickCallback
 * @returns
 */
function PF_tabMenu_SetClientClickEffect(widgetVarStr, onclickCallback) {
	if (!checkWidgets(widgetVarStr)) {
		return;
	}
	var tabMenu = PF(widgetVarStr);
	if (!tabMenu.items || tabMenu.items.length == 0) {
		console.log(widgetVarStr + "找不到 menuitem 項目 ");
		return;
	}

	for (var i = 0; i < tabMenu.items.length; i++) {
		var menuItem = $(tabMenu.items[i]);
		menuItem.attr("PF_tabMenuSelect_Index", i);
		$(tabMenu.items[i]).click(function() {
			var currMenuItem = $(this);
			var index = currMenuItem.attr("PF_tabMenuSelect_Index");
			PF_tabMenuSelect(widgetVarStr, index);
			if (common_isFunc(onclickCallback)) {
				onclickCallback(index);
			}
		});
	}
}

function PF_tabMenuSelect(widgetVar, idx) {
	if (!checkWidgets(widgetVar)) {
		return;
	}
	var tabMenu = PF(widgetVar);
	if (!tabMenu.items || tabMenu.items.length == 0) {
		console.error(widgetVar + "找不到 menuitem 項目 ");
		return;
	}

	for (var i = 0; i < tabMenu.items.length; i++) {
		var menuitem = $(tabMenu.items[i]);
		menuitem.removeClass("ui-state-active");
		if (/^[-]?\d+$/.test(idx) && i === Number(idx)) {
			menuitem.addClass("ui-state-active");
		}
	}
}

function PF_clearFilters(widgetVar) {
	if (!checkWidgets(widgetVar)) {
		return;
	}
	//console.log("PF_clearFilters:" + widgetVar);
	PF(widgetVar).clearFilters();
}

function showLoad() {
	showPF('dlgLoading');
	onShowTime();
}
// 關閉遲延dlg
function hideLoad() {
	hidePF('dlgLoading');
	onHidTime();
}

//初始化視窗位置
function PF_initPosition(widgetVar) {
	if (!checkWidgets(widgetVar)) {
		return;
	}
	PF(widgetVar).initPosition();
}

function onShowTime() {
	try {
		Example1.resetStopwatch();
		Example1.Timer.toggle();
	} catch (err) {
		console.log(err);
	}
}

function onHidTime() {
	try {
		Example1.resetStopwatch();
	} catch (err) {
		console.log(err);
	}
}

//移除datatable row drag 時, 游標特效會卡住的問題
function deleteDraggableRowsErrorContent() {
	$(".ui-sortable-helper").each(function() {
		var self = $(this);

		//二次判斷, 需為 absolute 才處理()
		if ("absolute" == self.css("position")) {
			self.remove();

			//處理游標變成『十字』無法還原問題
			//移除 body style 標註
			var currBody = $("body");
			if ("move" == currBody.css("cursor")) {
				currBody.css("cursor", "auto");
			}
			//移除 style
			currBody.find("style").each(function() {
				if ($(this).html().indexOf("cursor: move")) {
					$(this).remove();
				}
			});
		}
	});
}

var _selectMenuItemMakeupInProcessMap = {};
//處理 selectitem label 非純文字時, 無法顯示 css 內容問題
function selectMenuItemMakeup(elmClass) {
	$("." + elmClass).find("label ,.ui-corner-right").click(function() {
		$(".ui-selectonemenu-list-item").each(function() {
			$(this).html(unescape($(this).attr("data-label")));
		});
	});

	$("." + elmClass).find("select").change(function() {
		$("." + elmClass).find("label").each(function() {
			$(this).html(unescape($(this).text()));
		});
	});

	$("." + elmClass).find("label").each(function() {
		$(this).html(unescape($(this).text()));
	});

	//避免非 onchange 事件
	$('body').on('DOMSubtreeModified', '.' + elmClass, function() {
		//避免無窮迴圈
		if(_selectMenuItemMakeupInProcessMap[elmClass]){
			return;
		}
		_selectMenuItemMakeupInProcessMap[elmClass] = true;
		$("." + elmClass).find("label").each(function() {
			$(this).html(unescape($(this).text()));
		});
		_selectMenuItemMakeupInProcessMap[elmClass] = false;
	});
}