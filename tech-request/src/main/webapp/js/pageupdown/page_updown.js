/**
 * 分頁上下筆按鍵
 * @returns {undefined}
 */

//無上層視窗時關閉上下筆(優先處理)
function checkDisableUpDownBtn(upid, downid) {
    var addDisable = hasOpener();
    if (!addDisable) {
        $("#" + upid).addClass("ui-state-disabled");
        $("#" + downid).addClass("ui-state-disabled");
    }
}

function hasOpener() {
    var opener = window.opener;
    if (opener === null) {
        return false;
    }
    if (opener === "undefined") {
        return false;
    }
    return true;
}

function callByUpBtn() {
    try {
        showLoad();
        window.opener.openerByBtnUp();
        //還有問題勿開放20160811 by shaun
        //checkUrlToRedir();
    } catch (err) {
        console.log(err);
    }
}

function callByDownBtn() {
    try {
        showLoad();
        window.opener.openerByBtnDown();
        //還有問題勿開放20160811 by shaun
        //checkUrlToRedir();
    } catch (err) {
        console.log(err);
    }
}

function checkUrlToRedir() {
    var isHasOpener = hasOpener();
    if (isHasOpener) {
        var openUrl = window.opener.location.href;
        var currUrl = window.location.href;
        //當上層查詢頁面不為草稿查詢時，分頁url應為 02
        if (openUrl.indexOf('search/search23') === -1) {
            if (currUrl.indexOf('require/require06') !== -1) {
                window.close();
                //無法正常跳轉至02會另外導致01，可能currRow已被洗掉
                //window.location = '../require/require02.xhtml';
            }
        }
        //當上層查詢頁面為草稿查詢時，分頁url應為 06
        if (openUrl.indexOf('search/search23') !== -1) {
            if (currUrl.indexOf('require/require02') !== -1) {
                window.location = '../require/require06.xhtml';
            }
        }
    }
}