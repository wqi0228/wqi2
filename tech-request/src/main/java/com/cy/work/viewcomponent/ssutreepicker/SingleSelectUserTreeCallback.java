package com.cy.work.viewcomponent.ssutreepicker;

import java.io.Serializable;

public class SingleSelectUserTreeCallback implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 872092792705733481L;

    /**
     * 雙擊項目時的動作
     * 
     * @return
     */
    public void doubleClickItem(){
        throw new UnsupportedOperationException("SingleSelectUserTreeCallback: 未實做 doubleClickItem");
    }
}
