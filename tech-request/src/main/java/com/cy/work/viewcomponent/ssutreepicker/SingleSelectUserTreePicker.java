package com.cy.work.viewcomponent.ssutreepicker;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.common.vo.WkItemName;
import com.cy.work.viewcomponent.helper.OrgUserTreePickerCallBack;
import com.cy.work.viewcomponent.helper.OrgUserTreePickerHelper;
import com.cy.work.viewcomponent.helper.PickerComponentHelper;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Slf4j
public class SingleSelectUserTreePicker implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3668598683504650680L;
    // ========================================================================
    // 服務元件區
    // ========================================================================
    private PickerComponentHelper pickerHelper = PickerComponentHelper.getInstance();
    private OrgUserTreePickerHelper orgUserTreePickerHelper = OrgUserTreePickerHelper.getInstance();

    // ========================================================================
    // 常數
    // ========================================================================
    private final String sidStyle = ""
            + "color: cadetblue;"
            + "font-family: monospace;"
            + "font-weight: bold;";

    // ========================================================================
    // 變數區
    // ========================================================================
    /**
     * 所有項目
     */
    @Getter
    private List<WkItem> allItems;

    /**
     * 樹節點結構資料
     */
    @Getter
    @Setter
    private TreeNode rootNode = new DefaultTreeNode();

    /**
     * 搜尋關鍵字
     */
    @Getter
    @Setter
    private String serachItemNameKeyword;

    /**
     * 已選擇項目
     */
    @Getter
    @Setter
    private TreeNode selectedItem;

    /**
     * 
     */
    @Getter
    private SingleSelectUserTreeCallback singleSelectUserTreeCallback;

    private boolean isShowSid = false;
    private boolean isShowUserId = false;
    private boolean isShowOrgID = false;
    /**
     * 顯示部門人數
     */
    private boolean isShowDepCount = false;
    private String managerMark = "";
    private String deputyManagerMark = "";

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * 建構子
     * 
     * @param config 建構參數容器
     */
    public SingleSelectUserTreePicker(SingleSelectUserTreePickerConfig config) {

        // ====================================
        // 取得傳入組態
        // ====================================
        this.singleSelectUserTreeCallback = config.getCallback();
        this.isShowSid = config.isShowSid();
        this.isShowUserId = config.isShowUserID();
        this.isShowOrgID = config.isShowOrgID();
        this.isShowDepCount = config.isShowDepCount();
        this.managerMark = config.getManagerMark();
        this.deputyManagerMark = config.getDeputyManagerMark();

        // ====================================
        // 建立 所有的 WkItem
        // ====================================
        this.allItems = orgUserTreePickerHelper.prepareWkItems(
                config.getCanUseDepSids(),
                this.callback);

        // ====================================
        // 建立選單樹
        // ====================================
        // 預設選擇項目
        String defaultSelectedItemSid = config.getSelectedDepSid() + ":" + config.getSelectedUserSid();
        // 建立選單樹
        this.orgUserTreePickerHelper.buildTreeNode(
                this.rootNode,
                allItems,
                false, // 未實做顯示停用單位
                defaultSelectedItemSid,
                Lists.newArrayList(),
                "", // 不輸入過濾字串-全不顯示
                callback);
    }

    /**
     * 回傳已選擇項目的 depSid
     * 
     * @return depSid
     */
    public Integer getSelectedDepSid() { return this.prepareSelectedData()[0]; }

    /**
     * 回傳已選擇項目的 userSid
     * 
     * @return userSid
     */
    public Integer getSelectedUserSid() { return this.prepareSelectedData()[1]; }

    /**
     * 解析已選擇項目的資料 (拆分選擇項目字串)
     * 
     * @return Integer[depSid, userSid]
     */
    private Integer[] prepareSelectedData() {
        Integer[] results = new Integer[] { null, null };

        if (this.selectedItem == null || this.selectedItem.getData() == null) {
            return results;
        }
        WkItem wkitem = (WkItem) this.selectedItem.getData();
        if (wkitem == null || WkStringUtils.isEmpty(wkitem.getSid())) {
            return results;
        }

        String[] dataAry = WkStringUtils.safeTrim(wkitem.getSid()).split(":");

        if (dataAry.length > 0) {
            if (!WkStringUtils.isNumber(dataAry[0])) {
                log.error("項目部門sid非數字!item sid[{}]", wkitem.getSid());
            }
            results[0] = Integer.parseInt(dataAry[0]);
        }

        if (dataAry.length > 1) {
            if (!WkStringUtils.isNumber(dataAry[1])) {
                log.error("項目user sid非數字!item sid[{}]", wkitem.getSid());
            }
            results[1] = Integer.parseInt(dataAry[1]);
        }

        return results;
    }

    /**
     * 事件：搜尋
     */
    public void event_searchItemNameByKeyword() {
        // 重新轉換查詢關鍵字, 忽略大小寫差異
        String keyword = WkStringUtils.safeTrim(serachItemNameKeyword).toUpperCase();

        // 建立選單樹
        this.orgUserTreePickerHelper.buildTreeNode(
                this.rootNode,
                allItems,
                false, // 未實做顯示停用單位
                this.selectedItem != null ? ((WkItem) this.selectedItem.getData()).getSid() : "",
                Lists.newArrayList(),
                keyword,
                callback);

        // 遞迴展開名稱符合或已選的節點
        this.pickerHelper.tree_searchItemNameByKeywordForWkItemName(
                this.rootNode,
                keyword);
    }

    /**
     * 事件：雙擊可選項目
     */
    public void event_doubleClickItem() {
        if (this.singleSelectUserTreeCallback != null) {
            this.singleSelectUserTreeCallback.doubleClickItem();
        }
    }

    /**
     * 清除已選擇
     */
    public void clearSelectedItem() {
        // 清除選擇項目
        this.selectedItem = null;
        // 清除搜尋資訊
        this.serachItemNameKeyword = "";
        // 收合所有節點
        this.setExpandedToAllNode(this.rootNode, false);
    }

    /**
     * 展開化收合所有節點
     * 
     * @param treeNode
     * @param isExpanded
     */
    private void setExpandedToAllNode(TreeNode treeNode, boolean isExpanded) {
        if (treeNode == null) {
            return;
        }
        treeNode.setExpanded(isExpanded);
        if (WkStringUtils.notEmpty(treeNode.getChildren())) {
            for (TreeNode childrenNode : treeNode.getChildren()) {
                setExpandedToAllNode(childrenNode, isExpanded);
            }
        }
    }

    /**
     * 實做『建立項目』方法
     */
    protected final OrgUserTreePickerCallBack callback = new OrgUserTreePickerCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 2638288146080291571L;

        /**
         * @param dep 單位資料檔
         * @return WkItem
         */
        @Override
        public WkItem createOrgItem(Org dep) {
            // ====================================
            // 建立項目物件
            // ====================================
            WkItem item = new WkItem(
                    dep.getSid() + "",
                    dep.getName(),
                    Activation.ACTIVE.equals(dep.getStatus()));

            // 為部門節點, 不可選擇
            item.setSelectable(false);

            // ====================================
            // sid
            // ====================================
            if (isShowSid) {
                item.getWkItemNames().add(
                        new WkItemName(dep.getSid() + "", "<span style='" + sidStyle + "'>[%s]</span>&nbsp;"));
            }

            // ====================================
            // org ID
            // ====================================
            if (isShowOrgID) {
                item.getWkItemNames().add(
                        new WkItemName(dep.getId(), "<span style='color:teal;opacity:0.8;font-family: monospace;'>[%s]</span>&nbsp;"));
            }

            // ====================================
            // name
            // ====================================
            item.getWkItemNames().add(
                    new WkItemName(dep.getName() + "", "%s"));

            // ====================================
            // 部門人數
            // ====================================
            if (isShowDepCount) {
                Set<Integer> depSids = WkOrgCache.getInstance().findAllChildSids(dep.getSid());
                depSids.add(dep.getSid());
                long userCount = WkUserCache.getInstance().findByPrimaryOrgSidsIn(depSids).stream()
                        .filter(user -> WkUserUtils.isActive(user))
                        .distinct()
                        .count();

                // 人數不入查詢關鍵字
                item.getWkItemNames().add(
                        new WkItemName("", "<span style='color:brown;opacity:0.6;font-family: monospace;'>（%s" + userCount + "人）</span>&nbsp;"));
            }

            // ====================================
            // 設定樹狀列表顯示名稱
            // ====================================
            // 設定顯示
            item.setShowTreeName(item.prepareShowWkItemNames());

            return item;
        }

        /**
         * @param targetDep 附掛部門
         * @param user      使用者資料檔
         * @return WkItem
         */
        @Override
        public WkItem createUserItem(Org targetDep, User user) {

            if (user == null || user.getPrimaryOrg() == null) {
                return null;
            }

            if (targetDep == null) {
                return null;
            }

            // ====================================
            // 建立項目物件
            // ====================================
            WkItem item = new WkItem(
                    targetDep.getSid() + ":" + user.getSid(),
                    user.getName(),
                    Activation.ACTIVE.equals(user.getStatus()));

            // ====================================
            // 組顯示字串
            // ====================================

            // 人員ICON + 滑鼠游標highlight
            String prefix = "<i class=\"fa fa-user\" aria-hidden=\"true\"></i>";
            prefix += "<span class='ssTree-dataNode-highlight'>";
            item.getWkItemNames().add(new WkItemName("", prefix));

            // sid
            if (isShowSid) {
                item.getWkItemNames().add(
                        new WkItemName(user.getSid() + "", "<span style='" + sidStyle + "'>[%s]</span>&nbsp;"));
            }

            // USER NAME
            item.getWkItemNames().add(
                    new WkItemName(user.getName(), isShowUserId ? "【%s】" : "%s"));

            // USER ID
            if (isShowUserId) {
                item.getWkItemNames().add(
                        new WkItemName(user.getId(), "<span style='color:gray;opacity:0.5;'>%s</span>"));
            }

            // 後方
            String subfix = "</span>";
            item.getWkItemNames().add(new WkItemName("", subfix));

            // 為部門主管時，後方加 managerMark
            if (WkOrgUtils.isMainDepManager(targetDep.getSid(), user.getSid())) {
                item.getWkItemNames().add(new WkItemName(managerMark, "&nbsp;" + managerMark));
            }

            // 為部門副主管時，後方加 deputyManagerMark
            if (WkOrgUtils.isDeputyDepManager(targetDep.getSid(), user.getSid())) {
                item.getWkItemNames().add(new WkItemName(deputyManagerMark, "&nbsp;" + deputyManagerMark));
            }

            // 主要單位不在傳入部門時，代表為兼(代)職主管, 加上兼職標示
            if (!WkCommonUtils.compareByStr(targetDep.getSid(), user.getPrimaryOrg().getSid())) {
                item.getWkItemNames().add(new WkItemName("兼", "&nbsp;" + "(兼)"));
            }

            // ====================================
            // 顯示名稱
            // ====================================
            item.setShowTreeName(item.prepareShowWkItemNames());

            return item;
        }

        /**
         * 以關鍵字比對項目名稱
         * 
         * @param wkItem  WkItem
         * @param keyword 關鍵字
         * @return true:符合 , false:不符合
         */
        public boolean compareItemNameByKeyword(WkItem wkItem, String keyword) {
            if (WkStringUtils.isEmpty(wkItem.getWkItemNames())) {
                return false;
            }

            for (WkItemName wkItemName : wkItem.getWkItemNames()) {
                String currItemName = WkStringUtils.safeTrim(wkItemName.getContent()).toUpperCase();
                if (currItemName.contains(keyword)) {
                    return true;
                }
            }
            return false;
        }
    };
}
