/**
 * 
 */
package com.cy.work.viewcomponent.usrdtpker;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkUserUtils;

import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 *
 */
@Getter
@Setter
public class UserDatatablePickerVO {

    /**
     * @param user
     */
    public UserDatatablePickerVO(Integer userSid) {
        User user = WkUserCache.getInstance().findBySid(userSid);
        this.prepareProps(user);
    }

    /**
     * @param user
     */
    public UserDatatablePickerVO(User user) {
        this.prepareProps(user);
    }

    /**
     * 準備prop value
     * 
     * @param user
     */
    private void prepareProps(User user) {
        if (user == null) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.ERROR, "傳入 user 為空!");
            return;
        }

        // user sid
        this.userSid = user.getSid();

        // 單位資訊
        if (user.getPrimaryOrg() != null) {
            this.depSid = user.getPrimaryOrg().getSid();
            this.depInfo = WkOrgUtils.prepareBreadcrumbsByDepNameAndMakeup(
                    this.depSid,
                    OrgLevel.DIVISION_LEVEL,
                    true,
                    "－");
        }

        // user 顯示資訊
        this.initUserInfo();
    }

    /**
     * 初始化 user 顯示資訊 欄位
     */
    public void initUserInfo() {
        // user 顯示資訊
        this.userInfo = WkUserUtils.findNameBySid(this.userSid);
        // 停用者加上停用前綴並劃上刪除線
        if (!WkUserUtils.isActive(this.userSid)) {
            this.userInfo = WkHtmlUtils.addStrikethroughStyle(this.userInfo, "停用");
        }
    }

    /**
     * user sid
     */
    private Integer userSid;

    /**
     * 主要部門 SID
     */
    private Integer depSid;

    /**
     * 部門顯示資訊
     */
    private String depInfo;

    /**
     * 名稱顯示資訊
     */
    private String userInfo;

    /**
     * 名稱顯示資訊
     */
    private boolean unmoveable;

}
