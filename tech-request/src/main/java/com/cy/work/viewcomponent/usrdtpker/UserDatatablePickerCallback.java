/**
 * 
 */
package com.cy.work.viewcomponent.usrdtpker;

import java.util.List;

import com.google.common.collect.Lists;

/**
 * @author allen1214_wu
 *
 */
public class UserDatatablePickerCallback {

    /**
     * 預設顯示單位，不實做時，會顯示所部門人員
     * 
     * @return 預設顯示單位sid
     */
    public List<Integer> prepareDefaultOrgInSrcDatatable() {
        //回傳空 list 代表顯示所有部門人員
        return Lists.newArrayList();
    }
}
