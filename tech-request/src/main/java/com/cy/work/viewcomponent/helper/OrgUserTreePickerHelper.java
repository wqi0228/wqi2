package com.cy.work.viewcomponent.helper;

import java.io.Serializable;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgType;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * @author allen1214_wu
 */
@Component
public class OrgUserTreePickerHelper implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4223006095114491068L;
    // ========================================================================
    // implement InitializingBean
    // ========================================================================
    private static OrgUserTreePickerHelper instance;

    public static OrgUserTreePickerHelper getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        OrgUserTreePickerHelper.instance = this;
    }

    // ========================================================================
    // 服務區
    // ========================================================================

    // ========================================================================
    // 對外方法區
    // ========================================================================
    /**
     * @return
     * @throws Exception
     */
    public List<WkItem> prepareWkItems(
            Collection<Integer> canUseDepSids,
            OrgUserTreePickerCallBack createWkItemCallBack) {

        // ====================================
        // 整理傳入的可使用單位
        // ====================================
        Set<Integer> canUseDepSidSet = Sets.newHashSet();
        if (WkStringUtils.notEmpty(canUseDepSids)) {
            canUseDepSidSet.addAll(canUseDepSids);
        }

        // ====================================
        // 準備所有要在選單上顯示的部門 (包含路徑部門[非可使用, 但需建立出來的上層部門])
        // ====================================
        List<Integer> allDepSidsWithPath = this.prepareDepSidsWithPath(canUseDepSidSet);

        // ====================================
        // 轉為 WkItem, 並兜組父子關係
        // ====================================
        return this.prepareWkItemByDepSids(canUseDepSidSet, allDepSidsWithPath, createWkItemCallBack);
    }

    /**
     * 建立樹資料結構
     * 
     * @param targetRootNode         目標根節點 (TreeNode)
     * @param allItems               所有項目
     * @param isShowInactive         是否顯示停用項目
     * @param defaultSelectedItemSid 預設選擇項目 SID
     * @param defaultExpendItemSids  預設展開的項目 SID
     * @param filterKeyword          過濾關鍵字 (若不符合此關鍵字的, 不會產生)
     * @param callback               實做方法
     * @return 預設選擇項目 (TreeNode)
     */
    public TreeNode buildTreeNode(
            TreeNode targetRootNode,
            List<WkItem> allItems,
            boolean isShowInactive,
            String defaultSelectedItemSid,
            List<String> defaultExpendItemSids,
            String filterKeyword,
            OrgUserTreePickerCallBack callback) {

        TreeNode selectedNode = null;

        // ====================================
        // 防呆
        // ====================================
        if (targetRootNode == null) {
            throw new SystemDevelopException("傳入 root node 為空!");
        }

        // ====================================
        // 初始化子節點
        // ====================================
        targetRootNode.getChildren().clear();

        // ====================================
        // 判斷是否需要產生資料
        // ====================================
        if (WkStringUtils.isEmpty(allItems)) {
            return null;
        }

        // ====================================
        // 建立<父,所有子項> map
        // ====================================
        Map<WkItem, List<WkItem>> childItemsMapByParent = Maps.newHashMap();
        for (WkItem wkItem : allItems) {
            List<WkItem> childItems = childItemsMapByParent.get(wkItem.getParent());
            if (childItems == null) {
                childItems = Lists.newArrayList();
                childItemsMapByParent.put(wkItem.getParent(), childItems);
            }
            childItems.add(wkItem);
        }

        // ====================================
        // 過濾關鍵字
        // ====================================
        // 重新轉換查詢關鍵字, 忽略大小寫差異
        String keyword = WkStringUtils.safeTrim(filterKeyword).toUpperCase();

        // 過濾後的項目
        Set<WkItem> currProcItems = Sets.newHashSet();

        for (WkItem wkItem : allItems) {

            // 有傳入關鍵字時, 進行過濾
            if (!"".equals(keyword)) {
                // 已選擇項目不用比對
                if (!WkCommonUtils.compareByStr(wkItem.getSid(), keyword)) {
                    // 呼叫實做方法進行比對,比對失敗時略過此節點
                    if (!callback.compareItemNameByKeyword(wkItem, keyword)) {
                        continue;
                    }
                }
            }

            // 1.加入自己
            currProcItems.add(wkItem);

            // 2.加入直系上層所有節點 （path）
            WkItem parentItem = wkItem.getParent();
            while (true) {
                if (parentItem == null) {
                    break;
                }
                currProcItems.add(parentItem);
                parentItem = parentItem.getParent();
            }

            // 3.加入下層所有節點
            this.addAllChilds(wkItem, childItemsMapByParent, currProcItems);

        }

        // ====================================
        // 建立父子關係資料結構
        // ====================================
        Map<WkItem, TreeNode> treeNodeMapByItem = Maps.newLinkedHashMap();

        // 以序item序號排序
        List<WkItem> sortedItems = currProcItems.stream()
                .sorted(Comparator.comparing(WkItem::getItemSeq))
                .collect(Collectors.toList());

        // 收集要展開的 treeNode
        List<TreeNode> expendTreeNodes = Lists.newArrayList();

        for (WkItem item : sortedItems) {

            // 不顯示停用, 且項目為停用時, 跳過
            if (!isShowInactive && !item.isActive()) {
                continue;
            }

            // 建立項目樹節點物件
            DefaultTreeNode treeNode = new DefaultTreeNode(item);
            treeNode.setSelectable(!item.isDisable());
            treeNode.setSelectable(item.isSelectable());

            // 比對已選擇 (SID)
            if (WkCommonUtils.compareByStr(item.getSid(), defaultSelectedItemSid)) {
                selectedNode = treeNode;
                selectedNode.setSelected(true);

                // 為預設選擇node 時, 也預設展開
                PickerComponentHelper.getInstance().changeNodeExpandToTop(treeNode, true);
            }
            // 比對預設展開
            if (defaultExpendItemSids.contains(item.getSid())) {
                PickerComponentHelper.getInstance().changeNodeExpandToTop(treeNode, true);
            }

            treeNodeMapByItem.put(item, treeNode);
        }

        for (WkItem wkitem : sortedItems) {

            // 取得項目節點物件
            TreeNode treeNode = treeNodeMapByItem.get(wkitem);

            // 取得父項目
            WkItem parentWkItem = wkitem.getParent();

            TreeNode parentTreeNode = treeNodeMapByItem.get(parentWkItem);

            if (parentWkItem == null) {
                // 無父項目, 指向 root
                if (!targetRootNode.getChildren().contains(treeNode)) {
                    targetRootNode.getChildren().add(treeNode);
                }
            } else if (parentTreeNode == null) {
                // 找不到父項目節點物件, 指向 root (應該不會發生, 除非傳入資料有問題)
                if (!targetRootNode.getChildren().contains(treeNode)) {
                    targetRootNode.getChildren().add(treeNode);
                }
            } else {
                // 將節點指向父節點
                if (!parentTreeNode.getChildren().contains(treeNode)) {
                    parentTreeNode.getChildren().add(treeNode);
                }
            }
        }

        // ====================================
        // 預設處理展開
        // ====================================
        for (TreeNode treeNode : expendTreeNodes) {
            PickerComponentHelper.getInstance().changeNodeExpandToTop(treeNode, true);
        }

        // ====================================
        // 回傳選擇節點 (如果有找到的話)
        // ====================================
        return selectedNode;
    }

    /**
     * 是否為該單位主管
     * 
     * @param dep
     * @param depUser
     * @return
     */
    public boolean isOrgManager(Org dep, User depUser) {
        if (dep == null || dep.getManager() == null || depUser == null) {
            return false;
        }
        return WkCommonUtils.compareByStr(dep.getManager().getSid(), depUser.getSid());
    }

    // ========================================================================
    // 內部方法區
    // ========================================================================
    /**
     * 準備所有要再選單上顯示的部門 (包含路徑部門[非可使用, 但需建立出來的上層部門])
     * 
     * @param canUseDepSidSet
     * @return
     */
    private List<Integer> prepareDepSidsWithPath(Set<Integer> canUseDepSidSet) {

        Set<Integer> depSids = Sets.newHashSet();
        for (Integer depSid : canUseDepSidSet) {
            // 取的直系往上所有單位
            List<Org> parentOrgs = WkOrgCache.getInstance().findAllParent(depSid);

            // 往上有任何一個部門停用, 即取消顯示該單位
            for (Org parentDep : parentOrgs) {
                if (!WkOrgUtils.isActive(parentDep)) {
                    continue;
                }
            }

            // 加入所有直系向上部門 (路徑節點)
            for (Org parentOrg : parentOrgs) {
                depSids.add(parentOrg.getSid());
            }

            // 加入自己
            depSids.add(depSid);
        }

        // 排序所有單位
        return WkOrgUtils.sortDepSidByOrgTree(Lists.newArrayList(depSids));
    }

    /**
     * 依據傳入部門, 轉為 WkItem, 並兜組父子關係
     * 
     * @param depSidSet
     * @return
     */
    private List<WkItem> prepareWkItemByDepSids(
            Set<Integer> canUseDepSids,
            List<Integer> allDepSidsWithPath,
            OrgUserTreePickerCallBack createWkItemCallBack) {

        // 防呆
        if (WkStringUtils.isEmpty(canUseDepSids)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 轉換格式
        // ====================================
        List<WkItem> currAllItems = Lists.newArrayList();
        Map<Integer, WkItem> itemMapBySid = Maps.newHashMap();
        Map<Integer, Org> depMapBySid = Maps.newHashMap();
        Long depIndex = Long.valueOf(99999); // 讓 user 排在部門前面, 故部門起始數提高
        Long userIndex = Long.valueOf(0);

        for (Integer depSid : allDepSidsWithPath) {

            // 取得公司資料
            Org dep = WkOrgCache.getInstance().findBySid(depSid);

            // ====================================
            // 排除條件
            // ====================================
            if (dep == null) {
                continue;
            }
            // 檢核項目需為部門 (非公司)
            if (!OrgType.DEPARTMENT.equals(dep.getType())) {
                continue;
            }
            // 非啟用部門不顯示
            if (!Activation.ACTIVE.equals(dep.getStatus())) {
                continue;
            }

            // ====================================
            // 建立部門 wkItem
            // ====================================
            // 建立項目物件
            WkItem depItem = createWkItemCallBack.createOrgItem(dep);
            if (depItem == null) {
                continue;
            }
            // 排序序號
            depItem.setItemSeq(depIndex++);
            // 加入所有項目列表
            currAllItems.add(depItem);

            // 資料索引
            itemMapBySid.put(dep.getSid(), depItem);
            depMapBySid.put(dep.getSid(), dep);

            // ====================================
            // 建立部門下所有使用者的 wkItem
            // ====================================
            // 不是可選部門, 不建立user 節點
            if (!canUseDepSids.contains(depSid)) {
                continue;
            }

            // 排序
            // 為主管
            Comparator<User> comparator = Comparator.comparing(depUser -> WkOrgUtils.isMainDepManager(depSid, depUser.getSid()), Comparator.reverseOrder());
            // 為副主管
            comparator = comparator.thenComparing(depUser -> WkOrgUtils.isDeputyDepManager(depSid, depUser.getSid()), Comparator.reverseOrder());
            // 依名稱排序
            comparator = comparator.thenComparing(Comparator.comparing(User::getName));

            // 取得單位下所有部門使用者, 並排序
            List<User> depUsers = WkUserCache.getInstance().findUserWithManagerByOrgSids(Lists.newArrayList(dep.getSid()), Activation.ACTIVE)
                    .stream()
                    .sorted(comparator)
                    .collect(Collectors.toList());

            // 建立部門成員節點 , 並掛在部門節點下面
            for (User depUser : depUsers) {
                // 排除停用 user
                if (!Activation.ACTIVE.equals(depUser.getStatus())) {
                    continue;
                }
                // 建立 user 節點 (呼叫實做)
                WkItem userItem = createWkItemCallBack.createUserItem(dep, depUser);
                if (userItem == null) {
                    continue;
                }
                // 將 user 掛到部門底下
                userItem.setParent(depItem);
                // 排序序號
                userItem.setItemSeq(userIndex++);
                //
                currAllItems.add(userItem);
            }
        }

        // 建立部門父子關係
        for (WkItem item : currAllItems) {
            // 略過 user 節點
            if (item.getSid().indexOf(":") >= 0) {
                continue;
            }
            Org dep = depMapBySid.get(Integer.parseInt(item.getSid()));
            if (dep.getParent() == null) {
                continue;
            }
            WkItem parentItem = itemMapBySid.get(dep.getParent().getSid());
            if (parentItem == null) {
                continue;
            }
            item.setParent(parentItem);
        }

        return currAllItems;
    }

    /**
     * 加入所有子節點
     * 
     * @param wkItem
     * @param childItemsMapByParent
     * @param currProcItems
     */
    private void addAllChilds(
            WkItem wkItem,
            Map<WkItem, List<WkItem>> childItemsMapByParent,
            Set<WkItem> currProcItems) {

        if (wkItem == null) {
            return;
        }
        List<WkItem> childItems = childItemsMapByParent.get(wkItem);

        if (WkStringUtils.isEmpty(childItems)) {
            return;
        }
        currProcItems.addAll(childItems);

        for (WkItem childItem : childItems) {
            this.addAllChilds(childItem, childItemsMapByParent, currProcItems);
        }
    }
}
