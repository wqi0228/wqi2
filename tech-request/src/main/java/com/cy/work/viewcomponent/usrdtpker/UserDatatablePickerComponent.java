/**
 * 
 */
package com.cy.work.viewcomponent.usrdtpker;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.viewcomponent.treepker.TreePickerCallback;
import com.cy.work.viewcomponent.treepker.TreePickerComponent;
import com.cy.work.viewcomponent.treepker.helper.TreePickerDepHelper;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Slf4j
public class UserDatatablePickerComponent {

    // ========================================================================
    // 服務區 (非spring 託管物件 @Autowired 無效)
    // ========================================================================
    private transient WkUserCache wkUserCache = WkUserCache.getInstance();
    private transient WkOrgCache wkOrgCache = WkOrgCache.getInstance();

    // ========================================================================
    // 內部變數
    // ========================================================================
    // 元件ID
    private String componentID;
    // 登入者公司
    private String compID;
    // 不可移動的 userSid
    private List<Integer> unmoveableUserSids;
    // 例外不出現在待選區的 user Sid
    private List<Integer> excludeUserSids;
    // 取得單位排序
    private Map<Integer, Integer> orgOrderSeqMapByOrgSid = this.wkOrgCache.findAllOrgOrderSeqMap();
    // 標亮關鍵字的 class
    private String HIGTLIGHT_KEYWORFD_STYLE = "WS1-1-3b";
    // ========================================================================
    //
    // ========================================================================
    /**
     * 待選清單 user
     */
    @Getter
    private List<Integer> selectedOrgSids;

    /**
     * 待選清單 user
     */
    @Getter
    private List<UserDatatablePickerVO> srcUserVOs;

    /**
     * 待選清單已選擇人員
     */
    @Getter
    @Setter
    private UserDatatablePickerVO srcSelectedUserVO;

    /**
     * 已選清單 user
     */
    @Getter
    private List<UserDatatablePickerVO> targetUserVOs;

    /**
     * 已選清單已選擇人員
     */
    @Getter
    @Setter
    private UserDatatablePickerVO targetSelectedUserVO;

    /**
     * 名稱搜尋輸入框
     */
    @Getter
    @Setter
    private String searchUserNameKeyWord;

    @Getter
    private TreePickerComponent depTreePickerComponent;

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * @param rootOrgID          初始的可選擇 root 單位 (顯示人員為 root 單位含以下)
     * @param componentID
     * @param selectedUserSids
     * @param unmoveableUserSids
     * @param excludeUserSids
     * @param callback
     * @throws SystemOperationException
     */
    public UserDatatablePickerComponent(
            String componentID,
            String compID,
            List<Integer> selectedUserSids,
            List<Integer> unmoveableUserSids,
            List<Integer> excludeUserSids,
            List<Integer> defaultSrcUserOrgSids) throws SystemOperationException {

        // ====================================
        // 防呆
        // ====================================
        if (WkStringUtils.isEmpty(componentID) || componentID.contains(" ")) {
            throw new SystemDevelopException(
                    "開發時期錯誤！傳入的元件ID不可為空，或有空白 componentID:[" + componentID + "]");
        }
        this.componentID = componentID;

        Org comp = this.wkOrgCache.findById(compID);
        if (comp == null) {
            throw new SystemOperationException(
                    "傳入的公司別找不到，compID:[" + comp + "]", InfomationLevel.ERROR);
        }

        // 記錄公司ID
        this.compID = compID;

        // 複製一份不可移動名單
        this.unmoveableUserSids = Lists.newArrayList();
        if (WkStringUtils.notEmpty(unmoveableUserSids)) {
            this.unmoveableUserSids = unmoveableUserSids.stream()
                    .collect(Collectors.toList());
        }

        // 複製一份不可選擇名單
        this.excludeUserSids = Lists.newArrayList();
        if (WkStringUtils.notEmpty(excludeUserSids)) {
            this.excludeUserSids = excludeUserSids.stream()
                    .collect(Collectors.toList());
        }

        // ====================================
        // 待選區的單位
        // ====================================
        if (WkStringUtils.notEmpty(defaultSrcUserOrgSids)) {
            this.selectedOrgSids = defaultSrcUserOrgSids.stream().collect(Collectors.toList());
        } else {

            // 取得公司下全部單位
            this.selectedOrgSids = this.wkOrgCache.findAllDepSidByCompSid(comp.getSid());
            // 過濾停用
            this.selectedOrgSids = this.selectedOrgSids.stream()
                    .filter(orgSid -> WkOrgUtils.isActive(orgSid))
                    .collect(Collectors.toList());
        }

        // ====================================
        // 準備選項
        // ====================================
        this.prepareUserItems(
                this.selectedOrgSids,
                selectedUserSids,
                this.unmoveableUserSids,
                this.excludeUserSids);
    }

    /**
     * @param srcOrgSids
     * @param selectedUserSids
     * @param unmoveableUserSids
     * @param excludeUserSids
     */
    private void prepareUserItems(
            List<Integer> srcOrgSids,
            List<Integer> selectedUserSids,
            List<Integer> unmoveableUserSids,
            List<Integer> excludeUserSids) {

        // ====================================
        // 防呆
        // ====================================
        if (srcOrgSids == null) {
            srcOrgSids = Lists.newArrayList();
        }
        if (selectedUserSids == null) {
            selectedUserSids = Lists.newArrayList();
        }
        if (unmoveableUserSids == null) {
            unmoveableUserSids = Lists.newArrayList();
        }
        if (excludeUserSids == null) {
            excludeUserSids = Lists.newArrayList();
        }

        // ====================================
        // 初始化
        // ====================================
        this.srcUserVOs = Lists.newArrayList();
        this.targetUserVOs = Lists.newArrayList();

        // ====================================
        // 依據傳入清單，取得所有待選單位人員
        // ====================================
        // 查詢所有傳入單位下的 user
        // 1.包含代理主管 (user的主要單位, 不是傳入單位)
        // 2.不顯示停用
        List<User> allUsers = this.wkUserCache.findUserWithManagerByOrgSids(srcOrgSids, Activation.ACTIVE);

        // ====================================
        // 待選、已選清單
        // ====================================
        for (User user : allUsers) {
            // -------------------
            // 建立VO
            // -------------------
            UserDatatablePickerVO userVO = this.createUserVO(user);

            // -------------------
            // 判斷待選/已選擇狀態
            // -------------------
            // 已選擇
            if (selectedUserSids.contains(user.getSid())) {
                this.targetUserVOs.add(userVO);
                continue;
            }

            // 排除
            if (excludeUserSids.contains(user.getSid())) {
                continue;
            }

            // 待選擇
            this.srcUserVOs.add(userVO);
        }

        // ====================================
        // 處理不是『待選部門』，但在已選擇清單的人員
        // ====================================
        // 取出目前已在已選擇(target)清單的人員
        List<Integer> targetUserSids = this.targetUserVOs.stream()
                .map(UserDatatablePickerVO::getUserSid)
                .collect(Collectors.toList());

        // 把還不存在者，加入已選清單
        for (Integer selectedUserSid : selectedUserSids) {
            if (!targetUserSids.contains(selectedUserSid)) {
                this.targetUserVOs.add(this.createUserVO(selectedUserSid));
            }
        }

        // ====================================
        // 排序
        // ====================================
        this.srcUserVOs = this.formatUserVOLists(this.srcUserVOs, true);
        this.targetUserVOs = this.formatUserVOLists(this.targetUserVOs, false);

    }

    /**
     * 排序
     * 
     * @param userVOs
     * @return
     */
    private List<UserDatatablePickerVO> formatUserVOLists(
            List<UserDatatablePickerVO> userVOs,
            boolean isSrc) {

        if (WkStringUtils.isEmpty(userVOs)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 排序
        // ====================================
        // 組排序條件 1.org order 2.user name
        Comparator<UserDatatablePickerVO> comparator = Comparator.comparing(
                user -> orgOrderSeqMapByOrgSid.get(user.getDepSid()));
        comparator = comparator.thenComparing(Comparator.comparing(UserDatatablePickerVO::getUserInfo));

        List<UserDatatablePickerVO> currUserVOs = userVOs.stream()
                .sorted(comparator)
                .collect(Collectors.toList());

        // ====================================
        // 來源(src)選單關鍵字 highlight
        // ====================================
        // 來源選單 且 有關鍵字時，加上關鍵字 highlight 效果
        if (isSrc && WkStringUtils.notEmpty(this.searchUserNameKeyWord)) {
            for (UserDatatablePickerVO userVO : currUserVOs) {
                // 重建user顯示資訊 （移除加工資訊）
                userVO.initUserInfo();
                // 加上 highlight
                userVO.setUserInfo(
                        WkJsoupUtils.getInstance().highlightKeyWordByClass(
                                userVO.getUserInfo(),
                                this.searchUserNameKeyWord,
                                this.HIGTLIGHT_KEYWORFD_STYLE));

            }
        }
        // 清掉『已選擇(target)清單』關鍵字
        // 若為『來源選單(src)』但無關鍵字過濾時，也清掉關鍵字
        else {
            for (UserDatatablePickerVO userVO : currUserVOs) {
                // 重建user顯示資訊 （移除加工資訊）
                userVO.initUserInfo();
            }
        }

        // ====================================
        // 鎖定
        // ====================================
        return currUserVOs;
    }

    /**
     * @param userSid
     * @return
     */
    private UserDatatablePickerVO createUserVO(Integer userSid) {
        User user = WkUserCache.getInstance().findBySid(userSid);
        return this.createUserVO(user);
    }

    /**
     * @param user
     * @return
     */
    private UserDatatablePickerVO createUserVO(User user) {
        UserDatatablePickerVO userVO = new UserDatatablePickerVO(user);
        // 是否在不可移動清單
        userVO.setUnmoveable(unmoveableUserSids.contains(user.getSid()));
        return userVO;
    }

    /**
     * 外部方法：取得元件中『被選取的』USER SID
     * 
     * @return
     */
    public List<Integer> getSelectUserSids() {
        if (WkStringUtils.isEmpty(this.targetUserVOs)) {
            return Lists.newArrayList();
        }

        return this.targetUserVOs.stream()
                .map(UserDatatablePickerVO::getUserSid)
                .collect(Collectors.toList());
    }

    // ========================================================================
    // 畫面動作
    // ========================================================================
    /**
     * 搬移選擇項目
     * 
     * @param isMoveToRight true/false:左邊搬到右邊/右邊搬到左邊
     */
    public void event_btn_moveSelected(
            boolean isMoveToRight) {

        // ====================================
        // 依據方向，指定來源、目標選單
        // ====================================
        // 來源選單
        List<UserDatatablePickerVO> eventSrcUserVOs;
        // 目標選單
        List<UserDatatablePickerVO> eventTargetUserVOs;

        UserDatatablePickerVO eventSelectedUserVO;

        // 依據方向指令
        if (isMoveToRight) {
            eventSrcUserVOs = this.srcUserVOs;
            eventTargetUserVOs = this.targetUserVOs;
            eventSelectedUserVO = this.srcSelectedUserVO;
        } else {
            eventSrcUserVOs = this.targetUserVOs;
            eventTargetUserVOs = this.srcUserVOs;
            eventSelectedUserVO = this.targetSelectedUserVO;
        }

        // ====================================
        // pass
        // ====================================
        // 未選擇時跳過
        if (eventSelectedUserVO == null) {
            return;
        }

        // 為不可移動項目時跳過
        if (this.unmoveableUserSids.contains(eventSelectedUserVO.getUserSid())) {
            return;
        }

        // ====================================
        // item move
        // ====================================
        // 從來源清單移除
        eventSrcUserVOs.remove(eventSelectedUserVO);
        // 加入目標清單
        eventTargetUserVOs.add(eventSelectedUserVO);
        // 排序已選清單
        eventTargetUserVOs = this.formatUserVOLists(eventTargetUserVOs, isMoveToRight);

        // 更新目標選單
        if (isMoveToRight) {
            this.targetUserVOs = eventTargetUserVOs;
            this.targetSelectedUserVO = eventSelectedUserVO;
            this.srcSelectedUserVO = null;
        } else {
            this.srcUserVOs = eventTargetUserVOs;
            this.srcSelectedUserVO = eventSelectedUserVO;
            this.targetSelectedUserVO = null;
        }
    }

    /**
     * 全部搬移
     */
    public void event_btn_moveAll(boolean isMoveToRight) {
        // ====================================
        // 依據方向，指定來源、目標選單
        // ====================================
        // 來源選單
        List<UserDatatablePickerVO> eventSrcUserVOs;
        // 目標選單
        List<UserDatatablePickerVO> eventTargetUserVOs;
        // 依據方向指令
        if (isMoveToRight) {
            eventSrcUserVOs = this.srcUserVOs;
            eventTargetUserVOs = this.targetUserVOs;
        } else {
            eventSrcUserVOs = this.targetUserVOs;
            eventTargetUserVOs = this.srcUserVOs;
        }

        // ====================================
        // pass
        // ====================================
        // 選單為空時， pass
        if (WkStringUtils.isEmpty(eventSrcUserVOs)) {
            return;
        }

        // ====================================
        // 逐項處理
        // ====================================
        // 複製一份用來loop
        List<UserDatatablePickerVO> currSrcUserVOs = eventSrcUserVOs.stream()
                .collect(Collectors.toList());

        for (UserDatatablePickerVO userVO : currSrcUserVOs) {
            // 不可移動者, 留在來源清單
            if (this.unmoveableUserSids.contains(userVO.getUserSid())) {
                continue;
            } else {
                // 從來源選單移除
                eventSrcUserVOs.remove(userVO);
                // 加入目標選單
                eventTargetUserVOs.add(userVO);
            }
        }

        // ====================================
        // 整裡選單
        // ====================================
        eventSrcUserVOs = this.formatUserVOLists(eventSrcUserVOs, false);
        eventTargetUserVOs = this.formatUserVOLists(eventTargetUserVOs, false);
    }

    /**
     * 名稱搜尋
     */
    public void searchUserNameByKeyword() {

        // 初始化待選清單
        this.srcUserVOs = Lists.newArrayList();

        // 關鍵字值去前後空白
        this.searchUserNameKeyWord = WkStringUtils.safeTrim(this.searchUserNameKeyWord);

        // 取出目前已在已選擇(target)清單的人員
        List<Integer> targetUserSids = this.targetUserVOs.stream()
                .map(UserDatatablePickerVO::getUserSid)
                .collect(Collectors.toList());

        // ====================================
        // 準備待選區的 user
        // ====================================

        // -------------------
        // 當關鍵字清空時，依據『選擇的部門(部門選單內容)』取得所有user
        // -------------------
        if (WkStringUtils.isEmpty(this.searchUserNameKeyWord)) {
            // 查詢所有傳入單位下的 user
            // 1.包含代理主管 (user的主要單位, 不是傳入單位)
            // 2.不顯示停用
            List<User> selectedOrgUsers = this.wkUserCache.findUserWithManagerByOrgSids(
                    this.selectedOrgSids,
                    Activation.ACTIVE);

            // 轉為UserVO
            if (WkStringUtils.notEmpty(selectedOrgUsers)) {
                this.srcUserVOs = selectedOrgUsers.stream()
                        .map(user -> this.createUserVO(user))
                        .collect(Collectors.toList());
            }
        }

        // -------------------
        // 關鍵字不為空時，查詢『公司下所有部門』的user
        // -------------------
        else {
            // 查詢命中的 user sid
            // 1.暱稱 like 命中 (不區分大小寫)
            // 2.排除停用
            // 3.已經在『已選擇』清單中的不需要出現
            List<Integer> nameLikeUserSids = WkUserUtils.findByNameLike(this.searchUserNameKeyWord, this.compID) // 依據暱稱過濾
                    .stream()
                    .filter(userSid -> WkUserUtils.isActive(userSid)) // 過濾停用
                    .filter(userSid -> !targetUserSids.contains(userSid)) // 過濾已經在已選清單
                    .collect(Collectors.toList());

            // 轉UserVO
            // 並標亮關鍵字
            if (WkStringUtils.notEmpty(nameLikeUserSids)) {
                for (Integer nameLikeUserSid : nameLikeUserSids) {
                    UserDatatablePickerVO userVO = new UserDatatablePickerVO(nameLikeUserSid);
                    userVO.setUserInfo(
                            WkJsoupUtils.getInstance().highlightKeyWordByClass(
                                    userVO.getUserInfo(),
                                    this.searchUserNameKeyWord,
                                    this.HIGTLIGHT_KEYWORFD_STYLE));

                    this.srcUserVOs.add(userVO);
                }

            }
        }
    }

    public boolean isSelectedInList(boolean isSrc) {
        // ====================================
        // 待選擇選單判斷
        // ====================================
        if (isSrc) {
            if (this.srcSelectedUserVO == null || WkStringUtils.isEmpty(this.srcUserVOs)) {
                return false;
            }
            return this.srcUserVOs.contains(this.srcSelectedUserVO);
        }
        // ====================================
        // 已選擇選單判斷
        // ====================================
        else {

            if (this.targetSelectedUserVO == null || WkStringUtils.isEmpty(this.targetUserVOs)) {
                return false;
            }
            return this.targetUserVOs.contains(this.targetSelectedUserVO);
        }
    }

    /**
     * 判斷清單是否為空
     * 
     * @param isSrc
     * @return
     */
    public boolean isEmptyList(List<UserDatatablePickerVO> userVOs) {
        return WkStringUtils.isEmpty(userVOs);
    }

    // ========================================================================
    // 部門選單
    // ========================================================================
    @Getter
    private String DEP_DIALOG_NAME = "DEP_DIALOG_NAME";
    @Getter
    private String DEP_DIALOG_CONTENT = "DEP_DIALOG_NAME";

    /**
     * 開啟部門選單
     */
    public void openDepDialog() {

        // ====================================
        // 初始化部門選單
        // ====================================
        try {
            if (this.depTreePickerComponent == null) {
                this.depTreePickerComponent = new TreePickerComponent(depTreePickerCallback, "使用者單位選單");
                this.depTreePickerComponent.setAlwaysOnlyAvtive(true);
            }
            this.depTreePickerComponent.rebuild();
        } catch (Exception e) {
            String message = WkMessage.EXECTION + "(" + e.getMessage() + ")";
            log.error(message);
            MessagesUtils.showError(message);
            return;
        }

        // ====================================
        // 開啟視窗
        // ====================================
        DisplayController.getInstance().showPfWidgetVar(this.componentID + this.DEP_DIALOG_NAME);
    }

    /**
     * 部門選單 - 確定
     */
    public void confirmDepDialog() {

        try {
            // 清空關鍵字
            this.searchUserNameKeyWord = "";
            // 更新已選擇部門清單
            this.selectedOrgSids = this.depTreePickerComponent.getSelectedItemIntegerSids()
                    .stream()
                    .collect(Collectors.toList());

            // 重建清單
            this.prepareUserItems(
                    this.selectedOrgSids,
                    this.getSelectUserSids(),
                    this.unmoveableUserSids,
                    this.excludeUserSids);
        } catch (Exception e) {
            String message = WkMessage.EXECTION + "(" + e.getMessage() + ")";
            log.error(message);
            MessagesUtils.showError(message);
            return;
        }

        // ====================================
        // 關閉視窗
        // ====================================
        DisplayController.getInstance().hidePfWidgetVar(this.componentID + this.DEP_DIALOG_NAME);

    }

    /**
     * 分派通知單位選單 - callback 事件
     */
    private final TreePickerCallback depTreePickerCallback = new TreePickerCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = 1239972134466448657L;

        @Override
        public List<WkItem> prepareAllItems() throws Exception {
            TreePickerDepHelper treePickerDepHelper = WorkSpringContextHolder.getBean(TreePickerDepHelper.class);
            return treePickerDepHelper.prepareDepItems(
                    SecurityFacade.getCompanyId(),
                    WkOrgCache.getInstance().findAllDepSidByCompID(SecurityFacade.getCompanyId()));
        }

        /**
         * 準備已選擇項目
         * 
         * @return
         */
        @Override
        public List<String> prepareSelectedItemSids() throws Exception {

            // 回傳
            if (WkStringUtils.isEmpty(selectedOrgSids)) {
                return Lists.newArrayList();
            }

            return selectedOrgSids.stream().map(orgSid -> orgSid + "").collect(Collectors.toList());
        }

        @Override
        public List<String> prepareDisableItemSids() throws Exception {
            return Lists.newArrayList();
        }
    };
}
