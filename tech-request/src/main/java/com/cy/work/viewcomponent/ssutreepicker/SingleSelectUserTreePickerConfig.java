/**
 * 
 */
package com.cy.work.viewcomponent.ssutreepicker;

import java.io.Serializable;
import java.util.Collection;
import lombok.Getter;
import lombok.Setter;

/**
 * SingleSelectTreePicker 建構參數設定物件
 * 
 * @author allen1214_wu
 */
@Getter
@Setter
public class SingleSelectUserTreePickerConfig implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1579064669428646060L;

    /**
     * 建構子
     * 
     * @param canUseDepSids 可使用的部門 sid
     */
    public SingleSelectUserTreePickerConfig(Collection<Integer> canUseDepSids) {
        this.canUseDepSids = canUseDepSids;
    }

    /**
     * 可選擇的單位
     */
    public Collection<Integer> canUseDepSids;
    /**
     * 已選擇的人員所在的單位 orgSid
     */
    public Integer selectedDepSid;
    /**
     * 已選擇的人員 userSid
     */
    public Integer selectedUserSid;
    /**
     * call back 事件實做
     */
    public SingleSelectUserTreeCallback callback;
    /**
     * 是否在單位、使用者前方顯示 sid (預設不顯示)
     */
    public boolean showSid = false;

    /**
     * 是否同時顯示使用者登入ID
     */
    public boolean showUserID = false;
    
    /**
     * 是否顯示 ORG ID
     */
    private boolean isShowOrgID = false;
    
    /**
     * 顯示部門人數
     */
    private boolean isShowDepCount = false;
    
    /**
     * 主管標記
     */
    public String managerMark = "(主管)";
    
    /**
     * 副主管標記
     */
    public String deputyManagerMark = "(副)";
}
