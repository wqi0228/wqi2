package com.cy.work.viewcomponent.treepker.helper;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkTreeUtils;
import com.cy.work.common.vo.WkItem;
import com.google.common.collect.Lists;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@NoArgsConstructor
@Component
@Slf4j
public class TreePickerDepHelper implements InitializingBean, Serializable {

    // ========================================================================
    // InitializingBean
    // ========================================================================

    /**
     * 
     */
    private static final long serialVersionUID = -6648813682569964830L;
    private static TreePickerDepHelper instance;

    public static TreePickerDepHelper getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        TreePickerDepHelper.instance = this;
    }

    // ========================================================================
    // 工具區
    // ========================================================================
    /**
     * 類別資料服務
     */
    @Autowired
    private WkOrgCache wkOrgCache;

    // ========================================================================
    // 外部方法
    // ========================================================================
    /**
     * @return
     */
    public List<WkItem> prepareDepItems(
            String compID, List<Integer> canUseDepSids) {

        // ====================================
        // 取得所有單位
        // ====================================
        List<Org> allDeps = wkOrgCache.findAllDepByCompID(compID);
        if (WkStringUtils.isEmpty(allDeps)) {
            log.warn("取不到任何一筆 org 資料!");
            return Lists.newArrayList();
        }
        // ====================================
        // 取得所有單位排序
        // ====================================
        Map<Integer, Integer> orgOrderSeqMapBySid =  WkOrgCache.getInstance().findAllOrgOrderSeqMap();

        // ====================================
        // 轉換資料型態
        // ====================================
        List<WkItem> allItems = allDeps.stream()
                .map(dep -> {
                    WkItem item = new WkItem(
                            dep.getSid() + "",
                            WkOrgUtils.getOrgName(dep),
                            Activation.ACTIVE.equals(dep.getStatus()));
                    item.getOtherInfo().put("PARENT_SID", (dep.getParent() == null) ? "" : dep.getParent().getSid() + "");
                    item.setItemSeq(Long.parseLong(orgOrderSeqMapBySid.get(dep.getSid())+""));
                    return item;
                })
                .collect(Collectors.toList());

        Map<String, WkItem> allItemsMapBySid = allItems.stream()
                .collect(Collectors.toMap(
                        WkItem::getSid,
                        WkItem::getThis));

        // ====================================
        // 建立父子關係
        // ====================================
        for (WkItem wkItem : allItems) {
            String parentSid = wkItem.getOtherInfo().get("PARENT_SID");
            wkItem.setParent(allItemsMapBySid.get(parentSid));
        }

        // ====================================
        // 準備可選單位
        // ====================================
        // 可選單位 sid, 轉字串
        List<String> trueItemSids = Lists.newArrayList();
        if (WkStringUtils.notEmpty(canUseDepSids)) {
            trueItemSids = canUseDepSids.stream().map(sid -> sid + "").collect(Collectors.toList());
        }

        // ====================================
        // 清除不可選單位
        // ====================================
        List<WkItem> results = WkTreeUtils.cleanItemsByUnnecessary(allItems, trueItemSids);

        return results;
    }
}
