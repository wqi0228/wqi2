package com.cy.log;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.utils.WkStringUtils;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;

public class UserConverter extends ClassicConverter {
    @Override
    public String convert(ILoggingEvent event) {
        String userID = SecurityFacade.getUserId();
        if (WkStringUtils.notEmpty(userID)) {
            return userID;
        }
        return "";
    }
}
