/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.vo;

import com.cy.commons.enums.Activation;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 第二分類 操作自訂物件
 *
 * @author kasim
 */
@Data
@ToString
@NoArgsConstructor
public class TROtherCategoryTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 239344074690272386L;

    private String sid;

    private Activation status;

    /** 分類名稱 */
    private String name = "";

    /** 類別Sid */
    private List<String> cateSids = Lists.newArrayList();

    /** 備註 */
    private String memo = "";

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TROtherCategoryTo other = (TROtherCategoryTo) obj;
        if (!Objects.equals(this.sid, other.sid)) {
            return false;
        }
        return true;
    }

}
