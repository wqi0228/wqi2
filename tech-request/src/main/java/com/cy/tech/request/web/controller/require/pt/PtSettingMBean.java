/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. 
 */
package com.cy.tech.request.web.controller.require.pt;

import com.cy.tech.request.web.controller.require.*;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.config.exception.PtEditExceptions;
import com.cy.tech.request.logic.service.pt.PtService;
import com.cy.tech.request.logic.vo.PtCheckTo;
import com.cy.tech.request.vo.enums.SubNoticeType;
import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerCallback;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerCallbackDefaultImplType;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerComponent;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerConfig;
import com.cy.tech.request.web.controller.component.mipker.helper.MultItemPickerByOrgHelper;
import com.cy.tech.request.web.controller.component.mutiuserqkpker.MultiUserQuickPickerCallback;
import com.cy.tech.request.web.controller.component.mutiuserqkpker.MultiUserQuickPickerComponent;
import com.cy.tech.request.web.controller.component.mutiuserqkpker.MultiUserQuickPickerConfig;
import com.cy.tech.request.web.controller.component.reqconfirm.ReqConfirmClientHelper;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.helper.AssignDepHelper;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.ConfirmCallbackDialogController;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonContentUtils;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 原型確認設定 控制<BR/>
 * 對應 pt_setting_dialog.xhtml
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope("view")
public class PtSettingMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6643614636767877778L;
    // ========================================================================
    //
    // ========================================================================
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private PtService ptService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private AssignDepHelper assignDepHelper;
    @Autowired
    transient private DisplayController displayController;
    @Autowired
    transient private MultItemPickerByOrgHelper multItemPickerByOrgHelper;
    @Autowired
    transient private ConfirmCallbackDialogController confirmCallbackDialogController;

    /** 編輯用主檔 */
    @Getter
    @Setter
    private PtCheck editPt;
    /** 備份資料 - 用來檢核 */
    private PtCheckTo tempTo;

    /** 編輯狀態控制 */
    @Getter
    private PtEditType editType;

    /**
     * 歸屬單位選項
     */
    @Getter
    private List<SelectItem> ownerDepSelectItems;
    /**
     * 選擇的歸屬單位
     */
    @Getter
    @Setter
    private WkItem selectOwnerDepItem;

    private enum PtEditType {
        /** 檢視 */
        IS_VIEW,
        /** 新增 */
        IS_NEW,
        /** 編輯 */
        IS_EDIT,
        /** 編輯通知 */
        IS_EDIT_NOTIFY,;
    }

    // ========================================================================
    //
    // ========================================================================
    /**
     * 記錄開啟時傳入的 Require01MBean
     */
    private transient Require01MBean require01MBean;

    // ========================================================================
    //
    // ========================================================================
    /**
     * 物件初始化
     */
    @PostConstruct
    public void init() {
        editPt = new PtCheck();
        this.editType = PtEditType.IS_VIEW;
    }

    /**
     * 開啟新增對話框
     * 
     * @param r01MBean
     */
    public void openDialogByNewAdd(Require01MBean r01MBean) {
        // 取得主單 bean
        if (r01MBean == null) {
            MessagesUtils.showWarn("資訊已逾期，請重新整理畫面");
            return;
        }
        Require require = r01MBean.getRequire();

        try {
            // 元件變數初始化
            this.init();
            // 建立編輯主檔
            this.editPt = ptService.createEmptyPtCheck(loginBean.getUser(), require);

            // 準備單據歸屬單位選單資料
            List<WkItem> ownerDepItems = assignDepHelper.prepareLoginUserCaseOwnerDepsByAssignDep(require.getSid());
            if (ownerDepItems.size() == 1) {
                // 僅有一筆時, 不做下拉選單
                this.ownerDepSelectItems = null;
                this.selectOwnerDepItem = ownerDepItems.get(0);
            } else {
                // 多筆時，不預設選擇項目
                this.selectOwnerDepItem = null;
                // 將部門資料轉 SelectItem
                this.ownerDepSelectItems = ownerDepItems.stream()
                        .map(wkitem -> new SelectItem(wkitem, wkitem.getName()))
                        .collect(Collectors.toList());
            }

            // 定義為新增模式
            this.editType = PtEditType.IS_NEW;

        } catch (SystemDevelopException e) {
            MessagesUtils.showError("開發時期錯誤->" + e.getMessage());
            return;
        } catch (SystemOperationException e) {
            MessagesUtils.showError("系統處理錯誤，請嘗試重整頁面！" + e.getMessage());
            return;
        } catch (Exception e) {
            String message = WkMessage.EXECTION + "[" + e.getMessage() + "]";
            log.error(message, e);
            MessagesUtils.showError(message);
            return;
        }

        // pt_setting_dlg_wv
        displayController.showPfWidgetVar("pt_setting_dlg_wv");
    }

    public void initByEdit(PtCheck ptCheck) {

        try {

            this.editPt = ptService.findByPtNo(ptCheck.getPtNo());
            this.tempTo = ptService.findToByPtNo(ptCheck.getPtNo());
            if (editPt.getNoticeDeps() == null) {
                editPt.setNoticeDeps(new JsonStringListTo());
            }

            // 準備單據歸屬單位選單資料
            this.selectOwnerDepItem = this.assignDepHelper.trnsOrgSidToWkItem(editPt.getCreateDep().getSid());
            this.ownerDepSelectItems = null; // 不顯示下拉選單

            this.editType = PtEditType.IS_EDIT;

        } catch (SystemOperationException e) {
            MessagesUtils.showError("系統處理錯誤，請嘗試重整頁面！" + e.getMessage());
            return;
        } catch (Exception e) {
            String message = WkMessage.EXECTION + "[" + e.getMessage() + "]";
            log.error(message, e);
            MessagesUtils.showError(message);
            return;
        }

        // pt_setting_dlg_wv
        displayController.showPfWidgetVar("pt_setting_dlg_wv");
    }

    public void cancelSetting() {
        if (this.editType.equals(PtEditType.IS_EDIT)) {
            editPt = ptService.findByPtNo(editPt.getPtNo());
        }
        this.editType = PtEditType.IS_VIEW;
    }

    /**
     * 頁面儲存
     * 
     * @param r01MBean
     */
    public void save(Require01MBean r01MBean) {
        this.doSave(r01MBean, false);
    }

    public boolean saveAndPassCheck(Require01MBean r01MBean) {
        return this.doSave(r01MBean, true);
    }

    /**
     * @param r01MBean
     */
    private boolean doSave(Require01MBean r01MBean, boolean isPassCheck) {

        // ====================================
        // 輸入檢核
        // ====================================
        if (!isPassCheck) {

            if (!PtEditType.IS_EDIT_NOTIFY.equals(this.editType)) {
                if (this.selectOwnerDepItem == null) {
                    MessagesUtils.showWarn("請選擇『單據歸屬單位』!");
                    return false;
                }

                if (editPt.getEstablishDate() == null) {
                    MessagesUtils.showWarn("請輸入『預計完成日』!");
                    return false;
                }
                if (WkStringUtils.isEmpty(editPt.getTheme())) {
                    MessagesUtils.showWarn("請輸入『主題』!");
                    return false;
                }
                if (WkStringUtils.isEmpty(
                        WkJsoupUtils.getInstance().clearCssTag(editPt.getContentCss()))) {
                    MessagesUtils.showWarn("『內容』不可為空白!");
                    return false;
                }
            }

            if (WkStringUtils.isEmpty(editPt.getNoticeDeps().getValue())) {
                MessagesUtils.showWarn("請選擇『通知單位』!");
                return false;
            }

        }

        // ====================================
        // update
        // ====================================
        if (this.editType.equals(PtEditType.IS_EDIT) || this.editType.equals(PtEditType.IS_EDIT_NOTIFY)) {

            try {
                if (PtEditType.IS_EDIT.equals(this.editType)) {
                    ptService.saveByEditPt(r01MBean.getRequire(),
                            editPt,
                            loginBean.getUser(),
                            tempTo);

                } else if (PtEditType.IS_EDIT_NOTIFY.equals(this.editType)) {
                    ptService.saveByEditPtNotify(r01MBean.getRequire(),
                            editPt,
                            loginBean.getUser());
                }
            } catch (IllegalAccessException e) {
                log.debug("檢核失敗", e);
                r01MBean.getPtBottomMBean().recoveryView(r01MBean, editPt);
                MessagesUtils.showError(e.getMessage());
                return false;
            } catch (PtEditExceptions e) {
                log.error("存檔失敗", e.getMessage());
                r01MBean.getPtBottomMBean().recoveryView(r01MBean, editPt);
                MessagesUtils.showError(e.getMessage());
                return false;
            } catch (Exception e) {
                log.error(r01MBean.getRequire().getRequireNo() + ":" + editPt.getPtNo() + ":" + loginBean.getUser().getName() + ":編輯原型確認失敗..." + e.getMessage(),
                        e);
                r01MBean.getPtBottomMBean().recoveryView(r01MBean, editPt);
                MessagesUtils.showError(e.getMessage());
                return false;
            }

            // 更新畫面
            this.updateScreen(r01MBean, this.editPt);
        }

        // ====================================
        // insert
        // ====================================
        if (PtEditType.IS_NEW.equals(this.editType))

        {
            try {

                // 新增時, 取得歸屬單位
                Org createDep = WkOrgCache.getInstance().findBySid(
                        Integer.valueOf(this.selectOwnerDepItem.getSid()));
                editPt.setCreateDep(createDep);

                // 新增原型確認單
                ptService.saveByNewPt(
                        r01MBean.getRequire(),
                        editPt,
                        loginBean.getUser());

                // 更新畫面
                this.updateScreen(r01MBean, this.editPt);
                //
                this.editType = PtEditType.IS_VIEW;

            } catch (UserMessageException e) {
                MessagesUtils.show(e);
                return false;
            } catch (Exception e) {
                r01MBean.reBuildeByUpdateFaild();
                String msg = String.format("新增原型確認單失敗!{}", r01MBean.getRequire().getRequireNo());
                log.error(msg, e);
                MessagesUtils.showError(msg);
                return false;
            }

        }
        return true;
    }

    private void updateScreen(Require01MBean r01MBean, PtCheck editPt) {
        // 更新全頁面內容
        r01MBean.reBuildeByUpdateFaild();
        r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.PT_CHECK_INFO);

        display.hidePfWidgetVar("pt_setting_dlg_wv");// 主設定視窗
        int idx = 0;
        try {
            idx = r01MBean.getPtBottomMBean().getPtChecks().indexOf(editPt);
            if (idx == -1) {
                idx = 0;
            }
        } catch (Exception e) {
        }
        display.execute("accPanelSelectTab('pt_acc_panel_layer_zero'," + idx + ");" + ReqConfirmClientHelper.CLIENT_SRIPT_CLOSE_PANEL);
    }

    public void initByEditNotifyPerson(Require01MBean r01MBean, PtCheck ptCheck) {
        this.editPt = ptCheck;
        this.tempTo = ptService.findToByPtNo(ptCheck.getPtNo());
        this.editType = PtEditType.IS_EDIT_NOTIFY;
        this.noticeUsersPickerOpenDialog(r01MBean);
    }

    /**
     * 讀取 子程序 通知異動紀錄
     * 
     * @param r01MBean
     * @param ptCheck
     */
    public void btnLoadSubNoticeInfo(Require01MBean r01MBean, PtCheck ptCheck) {
        r01MBean.loadSubNoticeInfo(ptCheck.getSourceSid(), ptCheck.getSid(),
                Lists.newArrayList(SubNoticeType.PT_NOTICE_MEMBER, SubNoticeType.PT_NOTICE_DEP));
    }

    // ========================================================================
    // 通知單位元件
    // ========================================================================
    /**
     * 通知單位-編輯視窗
     */
    @Getter
    private final String ASSIGN_DEP_DLG_NAME = "ASSIGN_DEP_DLG_NAME_" + this.getClass().getSimpleName();

    /**
     * 通知單位-編輯視窗內容區
     */
    @Getter
    private final String ASSIGN_DEP_DLG_CONTENT = "ASSIGN_DEP_DLG_CONTENT_" + this.getClass().getSimpleName();
    /**
     * 通知單位-編輯視窗-部門選擇器ID
     */
    @Getter
    private final String ASSIGN_DEP_PICKER_ID = "ASSIGN_DEP_PICKER_ID_" + this.getClass().getSimpleName();

    /**
     * 通知單位選擇器
     */
    @Getter
    private transient MultItemPickerComponent assignDepPicker;

    /**
     * 記錄開啟編輯的來源
     */
    private transient String openDialogEventFrom = "";

    /**
     * 開啟通知單位編輯視窗
     * 
     * @param require01MBean Require01MBean
     * @param openDialogFrom 動作觸發來源來原
     */
    public void assignDepOpenDialog(Require01MBean require01MBean, String openDialogEventFrom) {

        this.require01MBean = require01MBean;
        this.openDialogEventFrom = openDialogEventFrom;

        try {
            // 選擇器設定資料
            MultItemPickerConfig config = new MultItemPickerConfig();
            config.setTreeModePrefixName("單位");
            config.setContainFollowing(true);
            config.setEnableGroupMode(true); // 開啟群組功能
            config.setItemComparator(this.multItemPickerByOrgHelper.parpareComparator());
            // 傳入 ID , 自動清除 filter
            config.setComponentID(ASSIGN_DEP_PICKER_ID);

            // 部分實作方法用 Default
            this.assignDepPickerCallback.setDefaultImplType(MultItemPickerCallbackDefaultImplType.DEP);

            // 選擇器初始化
            this.assignDepPicker = new MultItemPickerComponent(config, this.assignDepPickerCallback);

        } catch (Exception e) {
            String message = "通知單位選單初始化失敗!" + e.getMessage();
            MessagesUtils.showError(message);
            log.error(message, e);
            return;
        }

        // 更新畫面
        this.displayController.update(this.ASSIGN_DEP_DLG_CONTENT);
        // 開啟 dialog
        this.displayController.showPfWidgetVar(this.ASSIGN_DEP_DLG_NAME);
    }

    public void openDialogByEdit(Require01MBean r01MBean, PtCheck ptCheck) {
        this.editPt = ptService.findByPtNo(ptCheck.getPtNo());
        this.tempTo = ptService.findToByPtNo(ptCheck.getPtNo());
        this.assignDepOpenDialog(r01MBean, "EDIT_ONLY");
    }

    /**
     * 確認按鈕
     */
    public void assignDepSave(boolean isPreConfirm) {

        if (WkStringUtils.isEmpty(this.assignDepPicker.getSelectedSid())) {
            MessagesUtils.showError("未挑選通知部門！！");
            return;
        }

        // ====================================
        // 取得異動前後單位清單
        // ====================================
        // 取得選單上已選擇的單位
        List<Integer> afterDepSids = this.assignDepPicker.getSelectedSid().stream()
                .filter(userSid -> WkStringUtils.isNumber(userSid))
                .map(userSid -> Integer.parseInt(userSid))
                .collect(Collectors.toList());

        // 取得異動前單位
        List<Integer> beforeDepSids = Lists.newArrayList();
        if (this.editPt.getNoticeDeps() != null && this.editPt.getNoticeDeps().getValue() != null) {
            // 轉數字
            beforeDepSids = this.editPt.getNoticeDeps().getValue().stream()
                    .filter(userSid -> WkStringUtils.isNumber(userSid))
                    .map(userSid -> Integer.parseInt(userSid))
                    .collect(Collectors.toList());
        }

        // 比對是否未異動
        if (WkCommonUtils.compare(beforeDepSids, afterDepSids)) {
            MessagesUtils.showInfo("未異動通知單位!");
            return;
        }

        // ====================================
        // 確認訊息
        // ====================================
        // 不為新增時, 跳出異動的確認訊息
        if (isPreConfirm && WkStringUtils.notEmpty(this.editPt.getSid())) {
            String drffMessage = WkCommonContentUtils.prepareDiffMessageForDepartment(
                    beforeDepSids,
                    afterDepSids,
                    "新增通知單位",
                    "移除通知單位");

            drffMessage = "<span class='WS1-1-3b'>異動【通知單位】結果如下：</span>"
                    + "<br/><br/>"
                    + drffMessage
                    + "<br/><br/>";

            this.confirmCallbackDialogController.showConfimDialog(
                    drffMessage,
                    Lists.newArrayList(),
                    () -> this.assignDepSave(false));
            return;
        }

        // ====================================
        // 單獨編輯通知單位 (非新增、非ON程式編輯模式)
        // ====================================
        if (this.editPt.getSid() != null && !"EDIT_ALL".equals(openDialogEventFrom)) {
            try {

                // 更新通知單位 + 建立異動記錄 + 首頁推撥
                this.ptService.modifyNoticeDeps(
                        require01MBean.getRequire().getSid(),
                        require01MBean.getRequire().getRequireNo(),
                        this.editPt.getPtNo(),
                        this.assignDepPicker.getSelectedSid(),
                        WkUserCache.getInstance().findBySid(SecurityFacade.getUserSid()));

                // 取得最新資料
                this.editPt = this.ptService.findByPtNo(this.editPt.getPtNo());

                // 更新畫面
                this.updateScreen(this.require01MBean, this.editPt);

            } catch (Exception e) {
                String message = "通知單位設定時發生錯誤! [" + e.getMessage() + "]";
                log.error(message, e);
                MessagesUtils.showError(message);
                return;
            }
        }

        // ====================================
        // 更新主檔容器資料內的資料
        // ====================================
        else {
            this.editPt.getNoticeDeps().setValue(this.assignDepPicker.getSelectedSid());
        }

        // ====================================
        // 關閉對話框
        // ====================================
        this.displayController.hidePfWidgetVar(this.ASSIGN_DEP_DLG_NAME);
    }

    /**
     * 準備通知單位 tool tip 內容
     * 
     * @param ptCheck
     * @return
     */
    public String prepareNoticeDepsTooltip(PtCheck ptCheck) {
        if (ptCheck != null &&
                ptCheck.getNoticeDeps() != null &&
                WkStringUtils.notEmpty(ptCheck.getNoticeDeps().getValue())) {

            Set<Integer> depSids = Sets.newHashSet();
            for (String depSid : ptCheck.getNoticeDeps().getValue()) {
                if (!WkStringUtils.isNumber(depSid)) {
                    continue;
                }
                depSids.add(Integer.parseInt(depSid));
            }

            return WkOrgUtils.prepareDepsNameByTreeStyle(Lists.newArrayList(depSids), 50);
        }
        return "";
    }

    /**
     * 分派單位
     */
    private final MultItemPickerCallback assignDepPickerCallback = new MultItemPickerCallback() {

        /**
         * 
         */
        private static final long serialVersionUID = 8035591822752249336L;

        /** * 取得已選擇部門 */
        @Override
        public List<WkItem> prepareSelectedItems() throws SystemDevelopException {
            // 防呆
            if (editPt == null
                    || editPt.getNoticeDeps() == null
                    || editPt.getNoticeDeps().getValue() == null) {
                return Lists.newArrayList();

            }

            // 由外部取回已選擇部門, 並轉換為 WkItem 物件
            return editPt.getNoticeDeps().getValue().stream()
                    .distinct() // 去重複
                    .filter(depSid -> WkStringUtils.isNumber(depSid)) // 轉數字
                    .map(depSid -> multItemPickerByOrgHelper.createDepItemByDepSid(Integer.valueOf(depSid))) // 轉 wkitem
                    .collect(Collectors.toList());
        }

        /** * 準備 disable 的單位 */
        @Override
        public List<String> prepareDisableItemSids() throws SystemDevelopException {

            // 防呆
            if (require01MBean == null
                    || require01MBean.getRequire() == null
                    || require01MBean.getRequire().getCreateDep() == null) {
                return Lists.newArrayList();
            }

            // 鎖定需求單位 (需求單位不可移除)
            return Lists.newArrayList(require01MBean.getRequire().getCreateDep().getSid() + "");
        }
    };

    // ========================================================================
    // 通知人員選單
    // ========================================================================
    /** 通知人員選單 dialog **/
    @Getter
    private final String DLG_NOTICE_MEMBER = "DLG_NOTICE_MEMBER";
    /** 通知人員選單 dialog 內容 **/
    @Getter
    private final String DLG_NOTICE_MEMBER_CONTENT = "DLG_NOTICE_MEMBER_CONTENT";
    /** 通知人員設定 - 人員選擇器 */
    @Getter
    private transient MultiUserQuickPickerComponent noticeUsersPicker;

    /**
     * 通知人員
     * 
     * @param require01MBean
     */
    public void noticeUsersPickerOpenDialog(Require01MBean require01MBean) {

        // 紀錄綁定的 require01MBean
        this.require01MBean = require01MBean;

        // ====================================
        // 防呆
        // ====================================
        if (this.editPt == null) {
            MessagesUtils.showWarn(WkMessage.NEED_RELOAD);
            return;
        }
        // ====================================
        // 取得已設定的通知人員
        // ====================================
        List<Integer> selectedUserSids = Lists.newArrayList();

        if (this.editPt.getNoticeMember() != null && this.editPt.getNoticeMember().getValue() != null) {
            // 轉Integer
            selectedUserSids = this.editPt.getNoticeMember().getValue().stream()
                    .map(userSid -> Integer.parseInt(userSid))
                    .collect(Collectors.toList());
        }

        // ====================================
        // 初始化人員選單
        // ====================================
        try {
            // 選擇器設定資料
            MultiUserQuickPickerConfig config = new MultiUserQuickPickerConfig();
            // 預設展開登入者
            config.setDefaultExpandUserSids(Sets.newHashSet(SecurityFacade.getUserSid()));
            // 開啟群組功能
            config.setEnableGroupMode(true);
            // 選擇器初始化
            this.noticeUsersPicker = new MultiUserQuickPickerComponent(
                    SecurityFacade.getCompanyId(),
                    DLG_NOTICE_MEMBER,
                    selectedUserSids,
                    config,
                    new MultiUserQuickPickerCallback()); // 直接傳入 MultiUserQuickPickerCallback :實作取得群組的方式用預設

        } catch (Exception e) {
            String message = "初始化人員選單失敗!" + "[" + e.getMessage() + "]";
            log.error(message, e);
            MessagesUtils.showError(message);
            return;
        }

        // ====================================
        // 畫面控制
        // ====================================
        // 更新內容
        this.displayController.update(this.DLG_NOTICE_MEMBER_CONTENT);
        // 顯示 dialog
        this.displayController.showPfWidgetVar(this.DLG_NOTICE_MEMBER);
    }

    /**
     * 通知人員設定 - 確認
     */
    public void noticeUsersPickerConfirm(boolean isPreConfirm) {

        // ====================================
        // 防呆
        // ====================================
        if (this.editPt == null) {
            MessagesUtils.showWarn(WkMessage.NEED_RELOAD);
            return;
        }

        // ====================================
        // 取得異動前後人員名單
        // ====================================
        // 取得選單上已選擇的人員
        List<Integer> afterUserSids = this.noticeUsersPicker.getSelecedtUserSids();
        // 取得異動前人員
        List<Integer> beforeUserSids = Lists.newArrayList();
        if (this.editPt.getNoticeMember() != null && this.editPt.getNoticeMember().getValue() != null) {
            // 轉數字
            beforeUserSids = this.editPt.getNoticeMember().getValue().stream()
                    .filter(userSid -> WkStringUtils.isNumber(userSid))
                    .map(userSid -> Integer.parseInt(userSid))
                    .collect(Collectors.toList());
        }

        // 比對是否未異動
        if (WkCommonUtils.compare(beforeUserSids, afterUserSids)) {
            MessagesUtils.showInfo("未異動人員名單!");
            return;
        }

        // ====================================
        // 確認訊息
        // ====================================
        // 不為新增時, 跳出異動的確認訊息
        if (isPreConfirm && WkStringUtils.notEmpty(this.editPt.getSid())) {
            String drffMessage = WkCommonContentUtils.prepareDiffMessageForUser(
                    beforeUserSids,
                    afterUserSids,
                    "新增通知人員",
                    "移除通知人員");

            String actionName = PtEditType.IS_EDIT_NOTIFY.equals(this.editType) ? "異動" : "編輯";

            drffMessage = "<span class='WS1-1-3b'>" + actionName + "【通知人員】結果如下：</span>"
                    + "<br/><br/>"
                    + drffMessage
                    + "<br/><br/>";

            this.confirmCallbackDialogController.showConfimDialog(
                    drffMessage,
                    Lists.newArrayList(),
                    () -> this.noticeUsersPickerConfirm(false));

            return;
        }

        // ====================================
        // 將值放回VO
        // ====================================
        // set 回 entity
        this.editPt.getNoticeMember().setValue(
                afterUserSids.stream()
                        .map(userSid -> userSid + "")
                        .collect(Collectors.toList()));

        // ====================================
        // 為單獨編輯人員模式
        // ====================================
        if (PtEditType.IS_EDIT_NOTIFY.equals(this.editType)) {
            // 存檔
            boolean isSuccess = this.saveAndPassCheck(this.require01MBean);
            // 不成功時, 留在選單頁
            if (!isSuccess) {
                return;
            }
        }

        // ====================================
        // 畫面控制
        // ====================================
        // 關閉選單視窗
        this.displayController.hidePfWidgetVar(this.DLG_NOTICE_MEMBER);
    }

    /**
     * 準備通知人員 tool tip 內容
     * 
     * @param ptCheck
     * @return
     */
    public String prepareNoticeUsersTooltip(PtCheck ptCheck) {
        if (ptCheck != null &&
                ptCheck.getNoticeMember() != null &&
                WkStringUtils.notEmpty(ptCheck.getNoticeMember().getValue())) {

            Set<Integer> userSids = Sets.newHashSet();
            for (String userSid : ptCheck.getNoticeMember().getValue()) {
                if (!WkStringUtils.isNumber(userSid)) {
                    continue;
                }
                userSids.add(Integer.parseInt(userSid));
            }

            return WkCommonContentUtils.prepareUserNameWithDepByDataListStyle(
                    userSids,
                    OrgLevel.MINISTERIAL,
                    true,
                    ">&nbsp;",
                    "尚未設定通知人員",
                    false);
        }
        return "";
    }
}
