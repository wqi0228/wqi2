/**
 * 
 */
package com.cy.tech.request.web.controller.setting.setting10;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.utils.RequireSkypeAlertHelper;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.client.skypebot.SkypeBotClient;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkStringUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Slf4j
@Controller
@Scope("view")
public class Set10SkypeBotTestMBean {

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private SkypeBotClient skypeBotClient;

    @Autowired
    private URLService urlService;

    // ========================================================================
    // view
    // ========================================================================
    @Getter
    @Setter
    private String skypeUserId = "";
    @Getter
    @Setter
    private String sendMessage = "";

    // ========================================================================
    // 方法
    // ========================================================================

    /**
     * 傳送訊息到個人
     */
    public void sendMessageToUser() {

        if (WkStringUtils.isEmpty(this.skypeUserId)) {
            MessagesUtils.showError("未輸入使用者的 skype ID!");
            return;
        }

        try {
            this.skypeBotClient.sendMessageToUser(this.skypeUserId, prepareSendMessage());
        } catch (SystemOperationException e) {
            MessagesUtils.show(e);
            log.error(e.getMessage());
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, e.getMessage(), 2);
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + e.getMessage();
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);;
        }
    }

    /**
     * 傳送訊息到聊天室
     */
    public void sendMessageToChat() {
        try {
            //this.skypeBotClient.sendMessageToChat(prepareSendMessage());
            this.skypeBotClient.sendMessageToChat(this.sendMessage);
        } catch (SystemOperationException e) {
            MessagesUtils.show(e);
            log.error(e.getMessage());
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, e.getMessage(), 2);
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + e.getMessage();
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);;
        }
    }

    /**
     * 傳送系統警告到聊天室
     */
    public void sendSystemAlertToChat() {
        try {
            RequireSkypeAlertHelper.getInstance().sendSkypeAlert("測試系統警告");            
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + e.getMessage();
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);;
        }
    }

    private String prepareSendMessage() throws SystemOperationException {

        String message = "%s\r\n\r\n\r\n\r\nskype bot 測試訊息\r\nfrom：[%s]\r\n %s";
        return String.format(
                message,
                this.urlService.buildRequireAPURL(SecurityFacade.getCompanyId()),
                WkDateUtils.formatDate(new Date(), WkDateUtils.YYYY_MM_DD_HH24_MI));

    }

}
