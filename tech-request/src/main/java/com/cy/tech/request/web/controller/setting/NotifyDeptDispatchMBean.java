package com.cy.tech.request.web.controller.setting;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.tech.request.logic.service.setting.BatchDeptDispatchService;
import com.cy.tech.request.repository.result.TransRequireVO;
import com.cy.tech.request.vo.enums.RequireTransProgramType;
import com.cy.tech.request.web.controller.view.component.UserAutoComplete;
import com.cy.tech.request.web.logic.helper.TransNotifyDepLogicHelper;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.google.common.base.Preconditions;

import lombok.Getter;
import lombok.Setter;

/**
 * @author aken_kao
 */
@Controller
@Scope("view")
public class NotifyDeptDispatchMBean extends AbstractBatchTransDepts {
    /**
     * 
     */
    private static final long serialVersionUID = -7268650762066756661L;
    @Autowired
    private BatchDeptDispatchService batchDeptDispatchService;
    @Autowired
    private TransNotifyDepLogicHelper transNotifyDepLogicHelper;

    @Getter
    @Setter
    private RequireTransProgramType requireTransProgramType;
    @Getter
    @Setter
    private String action;
    @Getter
    @Setter
    private UserAutoComplete userAutoComplete = new UserAutoComplete();

    @Override
    protected void validate() {
        Preconditions.checkArgument(CollectionUtils.isNotEmpty(originTreeVO.getSelectedDeptSids()), "請選擇通知單位!");
    }

    public void search() {
        try {
            validate();
            resultList = batchDeptDispatchService.searchByNotifiedDepts(requireTransProgramType, originTreeVO.getSelectedDeptSids());
            assignSuccessIcon();
        } catch (Exception e) {
            MessagesUtils.showError(e.getMessage());
        } finally {
            disabledExportBtn = CollectionUtils.isEmpty(resultList);
            selectedAll = false;
            selectedResultList.clear();
        }
    }

    public void update() {
        try {
            Preconditions.checkArgument(StringUtils.isNotBlank(action), "請選擇動作!");

            List<String> updatedList = null;
            List<String> orderNoList = selectedResultList.stream().map(TransRequireVO::getOrderNo).collect(Collectors.toList());
            if ("PLUS".equals(action)) {
                Preconditions.checkArgument(CollectionUtils.isNotEmpty(transOrgTreeVO.getSelectedDeptSids()), "請選擇轉換單位!");
                updatedList = transNotifyDepLogicHelper.updateBatchTransDep(orderNoList, transOrgTreeVO.getSelectedDepts().get(0).getSid());

            } else if ("MINUS".equals(action)) {
                updatedList = transNotifyDepLogicHelper.updateBatchTransRecoveryDep(orderNoList, originTreeVO.getSelectedDeptSids());

            }

            updatedSet.addAll(updatedList);
            search();
            MessagesUtils.showInfo("轉單成功!");
        } catch (Exception e) {
            MessagesUtils.showError(e.getMessage());
        }

    }

}
