
package com.cy.tech.request.web.controller.search;

import com.cy.tech.request.logic.vo.query.search.SearchQuery;

public interface ISearchMBean{
	/**
	 * 實做取得本頁查詢物件的方法
	 * @return SearchQuery
	 */
	public SearchQuery getQueryCondition();
}
