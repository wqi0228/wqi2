package com.cy.tech.request.web.controller.component.singleselecttree;

import java.io.Serializable;
import java.util.List;

import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.vo.WkItem;

public class SingleSelectTreeCallback implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -7891864965534470445L;

    /**
	 * 準備所有的項目
	 * 
	 * @return
	 */
	public List<WkItem> prepareAllItems() throws SystemDevelopException {
		throw new SystemDevelopException("SingleSelectTreeCallback: 未實做 prepareAllItem");
	}

	/**
	 * 擊點項目時的動作
	 * 
	 * @return
	 */
	public void clickItem() {
		throw new SystemDevelopException("SingleSelectTreeCallback: 未實做 clickItem");
	}
}
