package com.cy.tech.request.web.view.orgtrns.components;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.orgtrns.OrgTrnsAlertInboxService;
import com.cy.tech.request.logic.service.orgtrns.OrgTrnsService;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsBaseComponentPageVO;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsWorkVerifyVO;
import com.cy.tech.request.web.pf.utils.ConfirmCallbackDialogController;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkStringUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 回覆相關資料轉檔 MBean
 */
@Component
@Scope("view")
@Slf4j
public class OrgTrnsAlertInboxMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2597581682578739420L;
    // ========================================================================
    // 服務元件區
    // ========================================================================
    @Autowired
    private transient DisplayController displayController;
    @Autowired
    private transient ConfirmCallbackDialogController confirmCallbackDialogController;
    @Autowired
    private transient OrgTrnsAlertInboxService orgTrnsAlertInboxService;
    @Autowired
    private transient OrgTrnsService orgTrnsService;
    @Autowired
    private transient EntityManager entityManager;
    // ========================================================================
    // 元件ID
    // ========================================================================
    @Getter
    private final String DIALOG_CONTENT_ID = this.getClass().getSimpleName() + "_DIALOG_CONTENT_ID";
    @Getter
    private final String DIALOG_DATATABLE_ID = this.getClass().getSimpleName() + "_DIALOG_AREA_ID";
    @Getter
    private final String DIALOG_ID = this.getClass().getSimpleName() + "_DIALOG_ID";

    // ========================================================================
    // 變數區
    // ========================================================================
    @Getter
    @Setter
    private OrgTrnsBaseComponentPageVO cpontVO = new OrgTrnsBaseComponentPageVO();
    /**
     * 待轉筆數資料
     */
    Map<String, Integer> waitTransCountMapByDataKey;

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * 全轉
     * 
     * @param selVerifyDtVO 選擇的轉換資料
     * @param trnsTypeStr   無作用
     */
    public void beforeAllTrans(OrgTrnsWorkVerifyVO selVerifyDtVO, String trnsTypeStr) {
        // ====================================
        // 檢核
        // ====================================
        if (selVerifyDtVO == null) {
            MessagesUtils.showError("找不到選擇的資料!");
            return;
        }

        // ====================================
        // 初始化VO
        // ====================================
        this.cpontVO = new OrgTrnsBaseComponentPageVO();

        // 紀錄傳入參數
        this.cpontVO.setSelVerifyDtVO(selVerifyDtVO);

        // ====================================
        // 查詢
        // ====================================
        try {
            // 將全部查到的資料放進放進『已選擇』
            this.cpontVO.setSelectedDtVOList(
                    this.orgTrnsAlertInboxService.queryByReciveDep(
                            this.getCpontVO().getSelVerifyDtVO().getBeforeOrgSid()));
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            MessagesUtils.showError("系統錯誤!" + e.getMessage());
            log.error(e.getMessage(), e);
            return;
        }

        if (WkStringUtils.isEmpty(this.cpontVO.getSelectedDtVOList())) {
            MessagesUtils.showInfo("沒有需要轉檔的資料!");
            return;
        }

        // ====================================
        // 確認視窗
        // ====================================
        String confimInfo = "待轉筆數：【%s】，確定要轉檔嗎？";
        confimInfo = String.format(confimInfo, WkHtmlUtils.addBlueBlodClass(this.cpontVO.getSelectedDtVOList().size() + ""));

        this.confirmCallbackDialogController.showConfimDialog(
                confimInfo,
                "",
                () -> this.btnTrnsSelected());

    }

    /**
     * 畫面功能:開啟操作視窗
     * 
     * @param selVerifyDtVO 選擇的轉換資料
     * @param trnsTypeStr   無作用
     */
    public void openTrnsWindow(OrgTrnsWorkVerifyVO selVerifyDtVO, String trnsTypeStr) {

        // ====================================
        // 檢核
        // ====================================
        if (selVerifyDtVO == null) {
            MessagesUtils.showError("找不到選擇的資料!");
            return;
        }

        // ====================================
        // 初始化VO
        // ====================================
        this.cpontVO = new OrgTrnsBaseComponentPageVO();

        // 紀錄傳入參數
        this.cpontVO.setSelVerifyDtVO(selVerifyDtVO);

        // ====================================
        // 查詢
        // ====================================
        boolean isSuccess = this.btnQuery();
        // 打開視窗
        if (isSuccess) {
            this.displayController.showPfWidgetVar(this.DIALOG_ID);
        }
    }

    /**
     * 畫面功能:查詢
     */
    public boolean btnQuery() {
        // ====================================
        // 頁面初始化
        // ====================================
        // 清空
        this.cpontVO.setSelectedDtVOList(null);

        if (this.getCpontVO() == null || this.getCpontVO().getSelVerifyDtVO() == null) {
            MessagesUtils.showError("資料已遺失, 請重新刷新頁面");
            return false;
        }

        // ====================================
        // 查詢
        // ====================================
        try {
            this.cpontVO.setShowDtVOList(
                    this.orgTrnsAlertInboxService.queryByReciveDep(
                            this.getCpontVO().getSelVerifyDtVO().getBeforeOrgSid()));
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return false;
        } catch (Exception e) {
            MessagesUtils.showError("系統錯誤!" + e.getMessage());
            log.error(e.getMessage(), e);
            return false;
        }

        // ====================================
        // 更新頁面
        // ====================================
        this.displayController.clearDtatableFilters(this.DIALOG_DATATABLE_ID);
        this.displayController.update(this.DIALOG_CONTENT_ID);

        return true;
    }

    /**
     * 畫面功能:轉換選擇資料
     */
    public void btnTrnsSelected() {

        // ====================================
        // 檢核
        // ====================================
        if (WkStringUtils.isEmpty(this.cpontVO.getSelectedDtVOList())) {
            MessagesUtils.showError("未選擇單據");
            return;
        }

        // ====================================
        // 轉檔
        // ====================================
        int updateRow = 0;
        try {

            updateRow = this.orgTrnsAlertInboxService.process(
                    this.cpontVO.getSelVerifyDtVO(),
                    this.cpontVO.getSelectedDtVOList(),
                    SecurityFacade.getUserSid());

            entityManager.clear();
            MessagesUtils.showInfo("執行成功，共" + updateRow + "筆");

        } catch (SystemDevelopException e) {
            String msg = "開發時期錯誤!" + e.getMessage();
            MessagesUtils.showError(msg);
            log.error(msg, e);
            return;
        } catch (Exception e) {
            String msg = "執行失敗!" + e.getMessage();
            MessagesUtils.showError(msg);
            log.error(msg, e);
            return;
        }

        // ====================================
        // 重新查詢
        // ====================================
        this.btnQuery();
    }

    /**
     * 計算待轉筆數
     * 
     * @param orgTransMappingTos
     * @throws UserMessageException
     */
    public void executeCountAllWaitTrans(List<OrgTrnsWorkVerifyVO> orgTransMappingTos) throws UserMessageException {
        this.waitTransCountMapByDataKey = this.orgTrnsAlertInboxService.countAllWaitTrans(orgTransMappingTos);
    }

    /**
     * 取得待轉筆數字串
     * 
     * @param verifyVO
     * @param programTypeStr
     * @return
     */
    public String getWaitTransCount(OrgTrnsWorkVerifyVO verifyVO, String programTypeStr) {
        return this.orgTrnsService.prepareWaitTransCountInfo(
                this.waitTransCountMapByDataKey,
                verifyVO,
                programTypeStr);
    }

}
