package com.cy.tech.request.web.controller.values;

import com.cy.tech.request.web.controller.to.OnlineUserInfoTo;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * 線上成員登入資訊倉庫
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Component
public class StorageLoginHelper implements InitializingBean, Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 458026300774827257L;
    private static StorageLoginHelper instance;

	public static StorageLoginHelper getInstance() {
		return instance;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		StorageLoginHelper.instance = this;
	}

	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private Map<Integer, OnlineUserInfoTo> onlineUserInfos;

	@Value("${config.session.timeout}")
	@Getter
	private int sessionTimeoutMinutes;

	@PostConstruct
	public void init() {
		onlineUserInfos = Maps.newConcurrentMap();
	}

	public void add(LoginBean loginBean) {
		String startActiveTimeStr = sdf.format(new Date());
		if (!onlineUserInfos.containsKey(loginBean.getUser().getSid())) {
			onlineUserInfos.put(loginBean.getUser().getSid(), new OnlineUserInfoTo(loginBean.getUser(), loginBean.getDep(), startActiveTimeStr, loginBean));
		} else {
			onlineUserInfos.get(loginBean.getUser().getSid()).setStartActivityTime(startActiveTimeStr);
		}
		log.info(loginBean.getUser().getName() + " 登入需求系統...");
	}

	boolean isFirst_remove = true;
	/**
	 * 每30分鐘觸發一次
	 */
	private final long removeFixedDelay = (30 * 60 *1000);

	@Scheduled(fixedDelay = removeFixedDelay) // 每30分鐘觸發一次
	public void remove() {

		DateTime currTime = DateTime.now();

		for (OnlineUserInfoTo onlineUserInfoTo : onlineUserInfos.values()) {
			try {
				String parseDateStr = Strings.isNullOrEmpty(onlineUserInfoTo.getLastActiveTime())
				        ? onlineUserInfoTo.getStartActivityTime()
				        : onlineUserInfoTo.getLastActiveTime();
				DateTime lastActiveTime = new DateTime(sdf.parse(parseDateStr));
				int minutes = Minutes.minutesBetween(lastActiveTime, currTime).getMinutes();
				if (minutes > sessionTimeoutMinutes) {
					log.info("執行清除離線成員資訊：" + onlineUserInfoTo.getUserName());
					onlineUserInfoTo.setLoginBeanLink(null);
					onlineUserInfos.remove(onlineUserInfoTo.getUserSid());
				}
			} catch (ParseException ex) {
				log.error("清除離線成員活動時間資訊失敗...", ex);
			}
		}

		// 為避免遺忘此排程，故第一次運作時，印出執行訊息
		if (isFirst_remove) {
			log.info(String.format(
			        "【排程】執行清除離線成員資訊, 執行間隔時間=%s 分, ",
			        (this.removeFixedDelay / 1000 / 60)));
			isFirst_remove = false;
		}
	}

	public void updateActiveInfo(String activeViewId, LoginBean loginBean) {
		if (onlineUserInfos.containsKey(loginBean.getUser().getSid())) {
			onlineUserInfos.get(loginBean.getUser().getSid()).setLastActiveViewId(activeViewId);
			onlineUserInfos.get(loginBean.getUser().getSid()).setLastActiveTime(sdf.format(new Date()));
		}
	}

	/** 操作線上成員進行資訊更新 */
	// loginBean 內容都改為即時計算, 故不需重置
	// public void callInit() {
	// onlineUserInfos.values().stream().map(each -> (LoginBean)
	// each.getLoginBeanLink()).forEach(LoginBean::initUserInfo);
	// }

	public List<OnlineUserInfoTo> findOnLineUser() {
		return Lists.newArrayList(onlineUserInfos.values());
	}

}
