package com.cy.tech.request.web.view.orgtrns.components;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.orgtrns.OrgTrnsInChargeDepService;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsBaseComponentPageVO;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsDtVO;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsWorkVerifyVO;
import com.cy.tech.request.web.pf.utils.ConfirmCallbackDialogController;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 回覆相關資料轉檔 MBean
 */
@Component
@Scope("view")
@Slf4j
public class OrgTrnsInChangeDepMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7990159105712228133L;
    // ========================================================================
    // 服務元件區
    // ========================================================================
    @Autowired
    private transient DisplayController displayController;
    @Autowired
    private transient OrgTrnsInChargeDepService orgTrnsInChargeDepService;
    @Autowired
    private transient ConfirmCallbackDialogController confirmCallbackDialogController;
    @Autowired
    private transient EntityManager entityManager;

    // ========================================================================
    // 變數區
    // ========================================================================
    @Getter
    @Setter
    private OrgTrnsBaseComponentPageVO cpontVO = new OrgTrnsBaseComponentPageVO();

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * 畫面功能:開啟操作視窗
     */
    public void openTrnsWindow(List<OrgTrnsWorkVerifyVO> allTrnsWorks) {

        // ====================================
        // 檢核
        // ====================================
        if (WkStringUtils.isEmpty(allTrnsWorks)) {
            MessagesUtils.showWarn("無組織異動設定資料！");
            return;
        }

        // ====================================
        // 初始化VO
        // ====================================
        this.cpontVO = new OrgTrnsBaseComponentPageVO();
        this.cpontVO.setAllTrnsWorks(allTrnsWorks);

        // ====================================
        // 查詢
        // ====================================
        if (this.btnQuery()) {
            // 打開視窗
            this.displayController.showPfWidgetVar("dlgInChargeDepsTransform");
        }
    }

    /**
     * 畫面功能:查詢
     */
    public boolean btnQuery() {
        // ====================================
        // 頁面初始化
        // ====================================
        // 更新頁面
        this.displayController.update("dlgInChargeDepsTransform_id");

        // ====================================
        // 查詢
        // ====================================
        try {
            this.cpontVO.setShowDtVOList(
                    this.orgTrnsInChargeDepService.queryTrnsData(
                            this.getCpontVO().getAllTrnsWorks()));
        } catch (Exception e) {
            MessagesUtils.showError("系統錯誤!" + e.getMessage());
            log.error(e.getMessage(), e);
            return false;
        }

        return true;
    }

    /**
     * 畫面功能:轉換選擇資料
     */
    public void btnTrnsSelected(boolean isReflash) {
        // ====================================
        // 檢核
        // ====================================
        if (WkStringUtils.isEmpty(this.cpontVO.getSelectedDtVOList())) {
            MessagesUtils.showError("未選擇單據");
            return;
        }

        // ====================================
        // 建立單位轉換前後對應資料
        // ====================================
        Map<Integer, Integer> depTrnsMapping = Maps.newHashMap();
        for (OrgTrnsWorkVerifyVO orgTrnsWorkVerifyVO : this.cpontVO.getAllTrnsWorks()) {
            depTrnsMapping.put(orgTrnsWorkVerifyVO.getBeforeOrgSid(), orgTrnsWorkVerifyVO.getAfterOrgSid());
        }

        // ====================================
        // 轉檔
        // ====================================
        Date execDate = new Date();

        log.info("\r\n開始進行 主責單位 轉檔 \r\n"
                + "共選擇" + this.cpontVO.getSelectedDtVOList().size() + "筆");

        List<OrgTrnsDtVO> sucessVOs = Lists.newArrayList();
        List<String> errorMessages = Lists.newArrayList();

        for (OrgTrnsDtVO orgTrnsDtVO : this.cpontVO.getSelectedDtVOList()) {
            try {
                this.orgTrnsInChargeDepService.processTrns(
                        depTrnsMapping,
                        orgTrnsDtVO,
                        execDate);

                sucessVOs.add(orgTrnsDtVO);
            } catch (Exception e) {
                String errorMessage = String.format(
                        "[%s]執行失敗！:[%s]",
                        orgTrnsDtVO.getCaseNo(),
                        e.getMessage());

                log.error(errorMessage, e);
                errorMessages.add(errorMessage);
            }
        }

        if (WkStringUtils.notEmpty(sucessVOs)) {
            entityManager.clear();
        }

        // ====================================
        // 轉檔記錄
        // ====================================
        // 有成功筆數時才寫
        String backupErrorMessage = "";
        if (WkStringUtils.notEmpty(sucessVOs)) {
            try {
                this.orgTrnsInChargeDepService.processBackupInfo(
                        sucessVOs,
                        this.getCpontVO().getAllTrnsWorks().get(0).getEffectiveDate(),
                        SecurityFacade.getUserSid(),
                        execDate);
            } catch (Exception e) {
                backupErrorMessage = "寫備份資料失敗!" + e.getMessage();
                log.error(backupErrorMessage, e);
            }

        }

        // ====================================
        // show process message
        // ====================================
        List<String> resultMessages = Lists.newArrayList();
        resultMessages.add("執行完成!");
        resultMessages.add("");
        if (WkStringUtils.notEmpty(sucessVOs)) {
            resultMessages.add("成功:" + sucessVOs.size() + "筆");
            resultMessages.add("");
        }
        if (WkStringUtils.notEmpty(errorMessages)) {
            resultMessages.add("失敗:" + errorMessages.size() + "筆");
            for (String errorMessage : errorMessages) {
                resultMessages.add("&nbsp;&nbsp;" + errorMessage);
            }
            resultMessages.add("");
        }

        if (WkStringUtils.notEmpty(backupErrorMessage)) {
            resultMessages.add(backupErrorMessage);
        }

        MessagesUtils.showInfo(String.join("<br/>", resultMessages));

        // ====================================
        // 重新查詢
        // ====================================
        this.btnQuery();
    }
    
    
    public void btnAllTrans(List<OrgTrnsWorkVerifyVO> allTrnsWorks) {

        // ====================================
        // 查詢
        // ====================================
        try {
            List<OrgTrnsDtVO> results = this.orgTrnsInChargeDepService.queryTrnsData(allTrnsWorks);

            this.cpontVO.setAllTrnsWorks(allTrnsWorks);
            this.cpontVO.setShowDtVOList(results);
            this.cpontVO.setSelectedDtVOList(results);

        } catch (Exception e) {
            MessagesUtils.showError("系統錯誤!" + e.getMessage());
            log.error(e.getMessage(), e);
            return;
        }

        if (WkStringUtils.isEmpty(this.cpontVO.getSelectedDtVOList())) {
            MessagesUtils.showInfo("沒有需要轉檔的資料!");
            return;
        }

        // ====================================
        // 確認視窗
        // ====================================
        String confimInfo = "待轉筆數：【%s】，確定要轉檔嗎？";
        confimInfo = String.format(
                confimInfo,
                WkHtmlUtils.addBlueBlodClass(this.cpontVO.getSelectedDtVOList().size() + ""));

        this.confirmCallbackDialogController.showConfimDialog(
                confimInfo,
                "",
                () -> this.btnTrnsSelected(false));

    }
    
 
}
