package com.cy.tech.request.web.controller.setting;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.tech.request.logic.service.CategoryCreateService;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.logic.enumerate.BasicDataCategoryType;
import com.cy.tech.request.logic.service.TemplateService;
import com.cy.tech.request.logic.vo.BasicDataCategoryTo;
import com.cy.tech.request.vo.anew.enums.SignLevelType;
import com.cy.tech.request.web.controller.searchheader.CommonHeaderMBean;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 類別基本資料建立作業
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope("view")
public class Setting01MBean implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 332624797721020211L;
    @Autowired
	transient private DisplayController displayController;
	@Autowired
	transient private CategoryCreateService categoryCreateService;
	@Autowired
	transient private TemplateService templateService;
	@Autowired
	transient private LoginBean loginBean;
	@Autowired
	transient private CommonHeaderMBean commHeaderMBean;

	@Getter
	@Setter
	private Activation filterStatus;
	@Getter
	@Setter
	private String searchText;
	@Getter
	@Setter
	private BasicDataCategoryTo maintenBasicData;
	@Getter
	@Setter
	transient private TreeNode root;
	@Getter
	@Setter
	transient private TreeNode selectNode;
	@Getter
	@Setter
	private Boolean maintenance;
	@Getter
	@Setter
	private String maintenanceHeader;
	@Getter
	@Setter
	private Boolean mainteanceDisableBtn;
	@Getter
	@Setter
	private Boolean mainteanceDisableStatusBtn;

	@PostConstruct
	public void init() {
		this.setMaintenance(Boolean.FALSE);
		this.setMaintenanceHeader("檢視");
		this.setMainteanceDisableBtn(Boolean.FALSE);
		this.setMainteanceDisableStatusBtn(Boolean.FALSE);
		this.setSearchText("");
		this.searchByStatusAndTextData();
	}

	public void searchByStatusAndTextData() {
		root = new DefaultTreeNode("Root", null);
		BasicDataCategoryTo rootTo = categoryCreateService.findByStatusAndFazzyTextTree(this.getFilterStatus(), this.getSearchText());
		if (rootTo.getChilds() != null) {
			this.createNodeTree(rootTo.getChilds(), root);
		}
		this.setMaintenance(Boolean.FALSE);
	}

	private void createNodeTree(List<BasicDataCategoryTo> childs, TreeNode parentNode) {
		childs.forEach(each -> {
			TreeNode eachNode = new DefaultTreeNode(each, parentNode);
			eachNode.setExpanded(true);
			if (each.getChilds() != null) {
				this.createNodeTree(each.getChilds(), eachNode);
			}
		});
	}

	/**
	 * 進入維護畫面 新增 | 編輯
	 *
	 * @param maintenaceHeader
	 * @param id
	 */
	public void startMaintenace(String maintenaceHeader, String id) {
		this.setMaintenance(Boolean.TRUE);
		this.setMaintenanceHeader(maintenaceHeader);
		if ("nvl".equals(id)) {
			this.setMaintenBasicData(new BasicDataCategoryTo());
			this.getMaintenBasicData().setCheckAttachment(Boolean.FALSE);
			this.getMaintenBasicData().setStatus(Activation.ACTIVE);
			this.setMainteanceDisableBtn(Boolean.FALSE);
		} else {
			categoryCreateService.resetTo((BasicDataCategoryTo) selectNode.getData());
			this.setMaintenBasicData((BasicDataCategoryTo) selectNode.getData());
			this.setMainteanceDisableBtn(categoryCreateService.isAnyDocUse(maintenBasicData));
			this.setMainteanceDisableStatusBtn(categoryCreateService.isAnyChildIsActive(maintenBasicData));
			commHeaderMBean.setRequireDepts(this.maintenBasicData.getCanUseDepts().getValue());
		}
		this.updateViewPanel();
	}

	private void updateViewPanel() {
		displayController.update("viewPanel");
	}

	public void onNodeExpand(NodeExpandEvent event) {
		event.getTreeNode().setExpanded(true);
	}

	public void onNodeCollapse(NodeCollapseEvent event) {
		event.getTreeNode().setExpanded(false);
	}

	public void save() {
		// 當有類別已被使用時..不可以停用
		if (Activation.INACTIVE.equals(this.getMaintenBasicData().getStatus())
		        && categoryCreateService.isAnyDocUse(this.getMaintenBasicData())) {
			this.init();
			this.updateViewPanel();
			MessagesUtils.showWarn("已有單據使用相關類別！！");
			return;
		}
		try {
			this.checkInputInfo();
			this.checkMappingKey(this.maintenBasicData);
			this.categoryCreateService.save(this.loginBean.getUser(), this.getMaintenBasicData());
			this.templateService.saveMapping(maintenBasicData, "新增".equals(this.getMaintenanceHeader()));
			log.debug("執行存檔..." + toString());
			this.init();
			this.updateViewPanel();
		} catch (IllegalArgumentException e) {
			log.debug(e.getMessage());
			MessagesUtils.showWarn(e.getMessage());
		} catch (Exception e) {
			log.error("存檔失敗..." + e.getMessage(), e);
			MessagesUtils.showError("存檔失敗! " + e.getMessage());
		}
	}

	/**
	 * 離開維護畫面
	 */
	public void exitMaintenace() {
		log.debug("exitMaintenace..." + toString());
		this.searchByStatusAndTextData();
		this.setMaintenance(Boolean.FALSE);
		this.setMaintenanceHeader("檢視");
		this.updateViewPanel();
	}

	private void checkInputInfo() {
		Preconditions.checkArgument(!Strings.isNullOrEmpty(maintenBasicData.getId()), "代碼不可為空白，請重新輸入！！");
		Preconditions.checkArgument(!Strings.isNullOrEmpty(maintenBasicData.getName()), "名稱不可為空白，請重新輸入！！");
		Preconditions.checkArgument(maintenBasicData.getType() != null, "請選擇屬性！！");
		if (maintenBasicData.getType().equals(BasicDataCategoryType.SMALL)) {
			Preconditions.checkArgument(maintenBasicData.getDefaultDueDays() != null, "期望完成日預設天數不可為空白，請重新輸入！！");
		}

		if (!maintenBasicData.getType().equals(BasicDataCategoryType.BIG)) {
			Preconditions.checkArgument(maintenBasicData.getParent() != null, "請設定對應關係！！");
		}
		if (BasicDataCategoryType.SMALL.equals(maintenBasicData.getType())) {
			Preconditions.checkArgument(maintenBasicData.getSignLevel() != null, "請設定簽核層級！！");
		}
		if ("新增".equals(this.getMaintenanceHeader())) {
			Preconditions.checkArgument(!categoryCreateService.isExistId(maintenBasicData), "您所輸入的代碼已存在，請重新輸入！！");
			Preconditions.checkArgument(!categoryCreateService.isExistName(maintenBasicData), "類型名稱已存在，請重新輸入！！");
		}
		if ("編輯".equals(this.getMaintenanceHeader())) {
			Preconditions.checkArgument(!categoryCreateService.isExistIdByEdit(maintenBasicData), "您所輸入的代碼已存在，請重新輸入！！");
			Preconditions.checkArgument(!categoryCreateService.isExistNameByEdit(maintenBasicData), "類型名稱已存在，請重新輸入！！");
		}
	}

	private void checkMappingKey(BasicDataCategoryTo to) {
		if (to.getType().equals(BasicDataCategoryType.SMALL) && "新增".equals(this.getMaintenanceHeader())) {
			Preconditions.checkArgument(!templateService.isExistMappingId(maintenBasicData), "模版組合已重覆，請檢查組合ID值！！");
		}
	}

	/**
	 * 畫面初始化可使用單位
	 */
	public void initOrgTree() {
		commHeaderMBean.initOrgBySetting(this.maintenBasicData.getCanUseDepts().getValue());
	}

	/**
	 * 組合可使用單位字串
	 *
	 * @param deptSids
	 * @return
	 */
	public String canUserDeptsStr() {
		if (maintenBasicData == null || maintenBasicData.getCanUseDepts() == null) {
			return "";
		}
		return WkOrgUtils.findNameBySidStrs(maintenBasicData.getCanUseDepts().getValue(), "、");
	}

	/**
	 * 若自動產生on程式頁籤,需求單位主管簽核一定要為否
	 */
	public void checkNeedReqMgrSignMustBeNoSign() {
		if (maintenBasicData.getAutoCreateOnpg()) {
		    if(!SignLevelType.NO_SIGN.equals(maintenBasicData.getSignLevel())) {
		        maintenBasicData.setSignLevel(SignLevelType.NO_SIGN);
		        MessagesUtils.showInfo("選擇『自動產生ON程式』時強制無需需求單位簽核!");
		    }
			
		}
	}

}
