package com.cy.tech.request.web.controller.view.component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.primefaces.model.TreeNode;

import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.listener.ReportOrgTreeCallBack;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserWithRolesCache;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.logic.WkUserAndOrgLogic;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author aken_kao 組織樹元件
 */
@Slf4j
public class OrgTreeVO {
    private LoginBean loginBean = WorkSpringContextHolder.getBean(LoginBean.class);
    private OrganizationService orgService = WorkSpringContextHolder.getBean(OrganizationService.class);
    transient private WorkBackendParamHelper workBackendParamHelper = WorkSpringContextHolder.getBean(WorkBackendParamHelper.class);

    @Getter
    @Setter
    private List<String> requireDepts = Lists.newArrayList();
    @Getter
    @Setter
    private Set<String> canViewDepSids = Sets.newHashSet();
    @Getter
    @Setter
    private boolean hasGMPermission;
    @Getter
    @Setter
    private String selectedDeptNames = "尚未選擇";

    @Getter
    /** 類別樹 Component */
    private ReportOrgTreeComponent orgTreeComponent;

    public OrgTreeVO() {

        // 查詢登入者所有角色
        List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
                SecurityFacade.getUserSid(),
                SecurityFacade.getCompanyId());

        orgTreeComponent = new ReportOrgTreeComponent(loginBean.getCompanyId(), loginBean.getDep(),
                roleSids, true, reportOrgTreeCallBack);
    }

    /**
     * 初始化-報表單位挑選元件
     */
    public void btnOpenOrgTree() {
        try {
            if (hasGMPermission) {
                orgTreeComponent.initOrgTree(loginBean.getComp(), requireDepts, false, true);
            } else {
                orgTreeComponent.initOrgTree(loginBean.getDep(), requireDepts, false, false);
            }
        } catch (Exception e) {
            log.error("btnOpenCategoryTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 初始化-透過user primary company
     */
    public void initOrgTree(boolean isJustActiveDep, boolean isSeleAll) {
        try {
            orgTreeComponent.setSelectAllNode(false);
            orgTreeComponent.initOrgTree(loginBean.getComp(), requireDepts, isJustActiveDep, isSeleAll);
        } catch (Exception e) {
            log.error("btnOpenCategoryTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 組織樹初始化 by 登入者部門
     */
    public void initOrgTreeByDept() {
        orgTreeComponent.initOrgTreeByDept(loginBean.getUserSId());
    }

    /**
     * 初始化 by dept
     */
    public void initOrgTreeByDept(Org dept, boolean isJustActiveDep, boolean isSeleAll) {
        try {
            orgTreeComponent.setSelectAllNode(false);
            orgTreeComponent.initOrgTree(dept, requireDepts, isJustActiveDep, isSeleAll);
        } catch (Exception e) {
            log.error("btnOpenCategoryTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 取得已選取部門物件 (單選)
     * 
     * @return
     */
    public Org getSingleSelectedDept() {
        if (orgTreeComponent.getSingleSelectedNode() != null) {
            return (Org) orgTreeComponent.getSingleSelectedNode().getData();
        }
        return null;
    }

    /**
     * 取得已選取部門物件
     * 
     * @return
     */
    public List<Org> getSelectedDepts() { return orgService.findBySids(getSelectedDeptSids()); }

    /**
     * 取得已選取部門Sid
     * 
     * @return
     */
    public List<Integer> getSelectedDeptSids() { return convert(orgTreeComponent.getSelOrgSids()); }

    /**
     * 初始化已選取部門 for requireDepts(向下展開選取)
     * 
     * @param org
     */
    public void initDefaultDepts(Org org) {
        orgTreeComponent.setSelectAllNode(false);
        requireDepts.clear();
        requireDepts.add(String.valueOf(loginBean.getDep().getSid()));
        this.initDefaultDepts(org, requireDepts);
    }

    /**
     * 單位挑選-預設勾選單位
     */
    public void init() {
        hasGMPermission = workBackendParamHelper.isGM(loginBean.getUserSId());

        if (hasGMPermission) {
            this.orgTreeComponent.setSelectAllNode(true);
            this.requireDepts = WkOrgCache.getInstance().findAllDepSidByCompID(SecurityFacade.getCompanyId()).stream()
                    .map(sid -> sid + "")
                    .collect(Collectors.toList());
        } else {
            WkUserAndOrgLogic wkUserAndOrgLogic = WorkSpringContextHolder.getBean(WkUserAndOrgLogic.class);
            this.requireDepts = wkUserAndOrgLogic.prepareCanViewDepSidsBaseOnMinsterial(SecurityFacade.getUserSid(), false)
                    .stream()
                    .map(depSid -> depSid + "")
                    .collect(Collectors.toList());
        }

        if (WkStringUtils.notEmpty(this.requireDepts)) {
            this.canViewDepSids = Sets.newHashSet(this.requireDepts);
        }

    }

    /**
     * 選擇單位
     * 
     * @param org
     */
    public void selectOrg(Org org) {
        List<TreeNode> children = orgTreeComponent.getOrgTreeRoot().getChildren();
        orgTreeComponent.selectOrg(children, org);
        orgTreeComponent.setHighlightOrgs(Lists.newArrayList(org));
    }

    /**
     * 選擇登入者單位
     * 
     * @param org
     */
    public void selectSelfOrg() {
        selectOrg(loginBean.getDep());
    }

    private void initDefaultDepts(Org parent, List<String> defaultDepts) {
        List<Org> children = orgService.findByParentOrg(parent);
        if (children.isEmpty()) {
            return;
        }
        defaultDepts.addAll(this.getOrgSids(children));
        children.forEach(each -> this.initDefaultDepts(each, defaultDepts));
    }

    private List<String> getOrgSids(List<Org> children) {
        return children.stream()
                .map(Org::getSid)
                .map(Object::toString)
                .collect(Collectors.toList());
    }

    /** 報表 組織樹 Component CallBack */
    private final ReportOrgTreeCallBack reportOrgTreeCallBack = new ReportOrgTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 841648812660348684L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelOrg() {
            requireDepts = orgTreeComponent.getSelOrgSids();
            selectedDeptNames = WkOrgUtils.findNameBySidStrs(requireDepts, "、");
        }
    };

    /** 訊息呼叫 */
    private final MessageCallBack messageCallBack = new MessageCallBack() {

        /**
         * 
         */
        private static final long serialVersionUID = -7766210431940178574L;

        @Override
        public void showMessage(String m) {
            MessagesUtils.showError(m);
        }
    };

    private List<Integer> convert(List<String> deptSids) {
        if (CollectionUtils.isNotEmpty(deptSids)) {
            return deptSids.stream().map(sid -> Integer.valueOf(sid)).collect(Collectors.toList());
        }

        return Lists.newArrayList();
    }
}
