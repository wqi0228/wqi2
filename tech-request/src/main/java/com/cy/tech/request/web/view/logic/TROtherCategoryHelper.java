/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.view.logic;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.logic.service.CommonService;
import com.cy.tech.request.logic.service.TROtherCategoryService;
import com.cy.tech.request.repository.category.BasicDataSmallCategoryRepository;
import com.cy.tech.request.vo.category.TROtherCategory;
import com.cy.tech.request.web.controller.view.vo.TROtherCategoryTo;
import com.cy.tech.request.web.controller.view.vo.TROtherCategoryView;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.faces.model.SelectItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 第二分類 web logic
 *
 * @author kasim
 */
@Component
@Slf4j
public class TROtherCategoryHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 345998043453252113L;
    @Autowired
    private TROtherCategoryService otherCategoryService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private BasicDataSmallCategoryRepository smallCategoryDao;

    /**
     * 過濾資料
     *
     * @param filterStatus
     * @param filterName LIKE %分類名稱%
     * @return
     */
    public List<TROtherCategoryView> findByStatusAndName(Activation filterStatus, String filterName) {
        Map<String, String> smallCategorys = this.getSmallCategoryName();
        return otherCategoryService.findByStatusAndName(filterStatus, filterName).stream()
                .map(each -> this.transView(each, smallCategorys))
                .sorted((v1, v2) -> v2.getUpdatedDate().compareTo(v1.getUpdatedDate()))
                .collect(Collectors.toList());
    }

    /**
     * 取得小類名稱對照資料
     *
     * @return
     */
    private Map<String, String> getSmallCategoryName() {
        return smallCategoryDao.findAll().stream()
                .collect(Collectors.toMap(key -> key.getSid(), value -> value.getName()));
    }

    /**
     * 轉為自訂物件
     *
     * @param obj
     * @param smallCategorys
     * @return
     */
    private TROtherCategoryView transView(TROtherCategory obj, Map<String, String> smallCategorys) {
        TROtherCategoryView result = new TROtherCategoryView();
        result.setSid(obj.getSid());
        result.setCreatedDate(obj.getCreatedDate());
        result.setCreatedUserName(WkUserCache.getInstance().findBySid(obj.getCreatedUser()).getName());
        result.setName(obj.getName());
        result.setMappingData(this.bulidSmallCategoryName(obj.getCategorySidsTo().getValue(), smallCategorys));
        result.setStatus(commonService.get(obj.getStatus()));
        result.setUpdatedDate(obj.getUpdatedDate());
        return result;
    }

    /**
     * 建立小類名稱
     *
     * @param values
     * @param smallCategorys
     * @return
     */
    private String bulidSmallCategoryName(List<String> values, Map<String, String> smallCategorys) {
        if (values == null || values.isEmpty()) {
            return "";
        }
        StringBuilder result = new StringBuilder();
        values.forEach(each -> {
            if (smallCategorys.containsKey(each)) {
                if (result.length() > 0) {
                    result.append("、");
                }
                result.append(smallCategorys.get(each));
            } else {
                log.error("找不到小類對應資訊請確認!!SID：" + each);
            }
        });
        return result.toString();
    }

    /**
     * 進行更新
     *
     * @param obj
     * @param userSid
     */
    public void save(TROtherCategoryTo to, Integer userSid) {
        Preconditions.checkState(!Strings.isNullOrEmpty(to.getName()), "[分類名稱]　為必要輸入欄位，請確認！！");
        Preconditions.checkState(to.getName().length() <= 255, "[分類名稱]　最大輸入長度為[資料庫中記錄的長度]，請確認！！");
        Preconditions.checkState(to.getCateSids() != null && !to.getCateSids().isEmpty(), "[分類名稱]　為必要輸入欄位，請確認！！");
        Preconditions.checkState(to.getMemo().length() <= 255, "[備註]　最大輸入長度為[資料庫中記錄的長度]，請確認！！");
        if (to.getSid() != null) {
            Preconditions.checkState(!otherCategoryService.isAnyUseByNameAndExcludeSid(to.getName(), to.getSid()), "[" + to.getName() + "]　已經被使用，請確認！！");
        } else {
            Preconditions.checkState(!otherCategoryService.isAnyUseByName(to.getName()), "[" + to.getName() + "]　已經被使用，請確認！！");
        }
        otherCategoryService.save(this.transEntity(to), userSid);
    }

    /**
     * 轉為自訂物件
     *
     * @param to
     * @return
     */
    private TROtherCategory transEntity(TROtherCategoryTo to) {
        TROtherCategory result;
        if (to.getSid() != null) {
            result = otherCategoryService.findBySid(to.getSid());
        } else {
            result = new TROtherCategory();
        }
        result.setStatus(to.getStatus());
        result.setName(to.getName());
        JsonStringListTo categorySidsTo = new JsonStringListTo();
        categorySidsTo.setValue(to.getCateSids());
        result.setCategorySidsTo(categorySidsTo);
        result.setMemo(to.getMemo());
        return result;
    }

    /**
     * 取得操作用自訂物件
     *
     * @param sid
     * @return
     */
    public TROtherCategoryTo findToBySid(String sid) {
        TROtherCategory obj = otherCategoryService.findBySid(sid);
        Preconditions.checkState(obj != null, "找不到第二分類，請確認!!SID：" + sid);
        return this.transTo(obj);
    }

    /**
     * 轉為自訂物件
     *
     * @param obj
     * @return
     */
    private TROtherCategoryTo transTo(TROtherCategory obj) {
        TROtherCategoryTo result = new TROtherCategoryTo();
        result.setSid(obj.getSid());
        result.setStatus(obj.getStatus());
        result.setName(obj.getName());
        result.setCateSids(obj.getCategorySidsTo().getValue());
        result.setMemo(obj.getMemo());
        return result;
    }

    /**
     * 建立有效的第2分類選項
     *
     * @return
     */
    public List<SelectItem> bulidItemsByActive() {
        return otherCategoryService.findByStatus(Activation.ACTIVE).stream()
                .map(each -> new SelectItem(each.getSid(), each.getName()))
                .collect(Collectors.toList());
    }

}
