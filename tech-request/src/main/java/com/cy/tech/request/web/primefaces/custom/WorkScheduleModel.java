package com.cy.tech.request.web.primefaces.custom;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

/**
 * @author aken_kao
 */
public class WorkScheduleModel implements ScheduleModel, Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = 1220796986288292422L;
    private List<ScheduleEvent> events;
    private boolean eventLimit = false;

    public WorkScheduleModel() {
        events = new ArrayList<ScheduleEvent>(); 
    }
    
    public WorkScheduleModel(List<ScheduleEvent> events) {
        this.events = events;
    }
    
    public void addEvent(ScheduleEvent event) {
        events.add(event);
    }
    
    public boolean deleteEvent(ScheduleEvent event) {
        return events.remove(event);
    }
    
    public List<ScheduleEvent> getEvents() {
        return events;
    }
    
    public ScheduleEvent getEvent(String id) {
        for(ScheduleEvent event : events) {
            if(event.getId().equals(id))
                return event;
        }
        
        return null;
    }
    
    public void updateEvent(ScheduleEvent event) {
        int index = -1;
        
        for(int i = 0 ; i < events.size(); i++) {
            if(events.get(i).getId().equals(event.getId())) {
                index = i;
                
                break;
            }
        }
        
        if(index >= 0) {
            events.set(index, event);
        }
    }
    
    public void addOrUpdateEvent(ScheduleEvent event){
        if(event.getId() == null){
            event.setId(UUID.randomUUID().toString());
            addEvent(event);
        } else {
            updateEvent(event);
        }
    }
    
    public int getEventCount() {
        return events.size();
    }

    public void clear() {
        events = new ArrayList<ScheduleEvent>();
    }

    public boolean isEventLimit() {
        return eventLimit;
}

    public void setEventLimit(boolean eventLimit) {
        this.eventLimit = eventLimit;
    } 
}
