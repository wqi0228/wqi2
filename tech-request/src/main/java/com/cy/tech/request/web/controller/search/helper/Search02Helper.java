/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search.helper;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search02QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.Search02View;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.web.view.to.search.query.SearchQuery02;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * 輔助 Search02MBean
 *
 * @author kasim
 */
@Component
public class Search02Helper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6677258713760138550L;
    @Autowired
    transient private OrganizationService orgService;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private SearchHelper searchHelper;
    @Autowired
    transient private Search02QueryService queryService;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchResultHelper searchResultHelper;

    /**
     * 取得預設查詢部門
     *
     * @param loginDepSid
     * @return
     */
    public List<String> getDefaultDepSids(Integer loginDepSid) {
        Set<String> results = Sets.newHashSet(String.valueOf(loginDepSid));
        return Lists.newArrayList(this.getDefaultDepSids(loginDepSid, results));
    }

    /**
     * 取得預設查詢部門
     *
     * @param depSid
     * @param results
     * @return
     */
    private Set<String> getDefaultDepSids(Integer depSid, Set<String> results) {
        orgService.findByParentSid(depSid).forEach(each -> {
            results.add(String.valueOf(each.getSid()));
            this.getDefaultDepSids(each.getSid(), results);
        });
        return results;
    }

    /**
     * 查詢
     *
     * @param loginUser
     * @param query
     * @return
     */
    public List<Search02View> search(String compId, User loginUser, SearchQuery02 query) {
        String requireNo = reqularUtils.getRequireNo(compId, query.getFuzzyText());
        StringBuilder builder = new StringBuilder();
        Map<String, Object> parameters = Maps.newHashMap();

        builder.append("SELECT tr.require_sid, ");
        builder.append("       tr.require_no, ");
        builder.append("       tid.field_content, ");
        builder.append("       tr.urgency, ");
        builder.append("       tr.create_dt, ");
        builder.append("       ckm.big_category_name, ");
        builder.append("       ckm.middle_category_name, ");
        builder.append("       ckm.small_category_name, ");
        builder.append("       tr.dep_sid, ");
        builder.append("       tr.create_usr, ");
        builder.append("       rmsi.bpm_default_signed_name, ");
        builder.append("       rmsi.paper_code, ");
        builder.append("       tr.require_establish_dt, ");
        // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
        builder.append(this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire());

        builder.append("FROM ( ");
        this.buildRequireCondition(query, requireNo, builder, parameters);

        builder.append(this.searchHelper.buildRequireIndexDictionaryCondition(
                query.getTheme(),
                query.getFuzzyText(),
                requireNo));

        this.buildCategoryKeyMappingCondition(query, requireNo, builder);
        this.bulidReqUnitSignInfoCondition(query, requireNo, builder, parameters);

        // 檢查項目 (系統別)
        // 後方需對 tr.require_sid 做 group by
        builder.append(this.searchConditionSqlHelper.prepareCommonJoin(SecurityFacade.getUserSid()));

        builder.append("WHERE tr.require_sid IS NOT NULL ");

        builder.append(" GROUP BY tr.require_sid ");
        builder.append("ORDER BY tr.update_dt DESC ");

        // ====================================
        // 查詢
        // ====================================
        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.SIGN, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.SIGN, SecurityFacade.getUserSid());

        List<Search02View> resultList = queryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // ====================================
        // 後續處理
        // ====================================
        if (WkStringUtils.notEmpty(resultList)) {
            // 後續處理-開始
            usageRecord.afterProcessStart();
            // 系統別查詢條件過濾
            resultList = resultList.stream()
                    .filter(each -> this.searchResultHelper.filterCheckItems(
                            each.getCheckItemTypes(),
                            query.getCheckItemTypes(),
                            false,
                            null))
                    .collect(Collectors.toList());
            // 後續處理-結束
            usageRecord.afterProcessEnd();
        }
        
        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return resultList;

    }

    /**
     * 組合需求單部分
     *
     * @param searchQuery
     * @param requireNo
     * @param builder
     * @param parameters
     */
    private void buildRequireCondition(
            SearchQuery02 searchQuery,
            String requireNo,
            StringBuilder builder,
            Map<String, Object> parameters) {

        builder.append("    SELECT * FROM tr_require tr ");
        builder.append("    WHERE 1=1 ");
        // 需求單號
        if (!Strings.isNullOrEmpty(requireNo)) {
            builder.append("    AND tr.require_no = '").append(requireNo).append("' ");
        }
        // 需求單位審核狀態(需求單位簽核進度查詢條件)
        builder.append("    AND tr.has_require_manager_sign = '1' ");
        // 需求單位
        String depSidsStr = "'isEmpty'";
        if (searchQuery.getRequireDepts() != null && !searchQuery.getRequireDepts().isEmpty()) {
            depSidsStr = searchQuery.getRequireDepts().stream()
                    .map(depSid -> depSid + "")
                    .collect(Collectors.joining(","));
        }
        builder.append("    AND tr.dep_sid IN (").append(depSidsStr).append(") ");
        if (Strings.isNullOrEmpty(requireNo)) {
            // 立單區間
            if (searchQuery.getStartDate() != null && searchQuery.getEndDate() != null) {
                builder.append("    AND tr.create_dt BETWEEN '")
                        .append(searchHelper.transStrByStartDate(searchQuery.getStartDate())).append("' AND '")
                        .append(searchHelper.transStrByEndDate(searchQuery.getEndDate())).append("' ");
            } else if (searchQuery.getStartDate() != null) {
                builder.append("    AND tr.create_dt >= '")
                        .append(searchHelper.transStrByStartDate(searchQuery.getStartDate())).append("' ");
            } else if (searchQuery.getEndDate() != null) {
                builder.append("    AND tr.create_dt <= '")
                        .append(searchHelper.transStrByEndDate(searchQuery.getEndDate())).append("' ");
            }
            // 需求人員
            if (!Strings.isNullOrEmpty(searchQuery.getTrCreatedUserName())) {
                String userSidsStr = "'isEmpty'";

                List<Integer> userSids = WkUserUtils.findByNameLike(searchQuery.getTrCreatedUserName(), SecurityFacade.getCompanyId());
                if (WkStringUtils.isEmpty(userSids)) {
                    userSids.add(-999);
                }

                if (userSids != null && !userSids.isEmpty()) {
                    userSidsStr = userSids.stream()
                            .map(each -> String.valueOf(each))
                            .collect(Collectors.joining(","));
                }
                builder.append("    AND tr.create_usr IN (").append(userSidsStr).append(") ");
            }

        }
        //////////////////// 以下為進階搜尋條件//////////////////////////////
        if (Strings.isNullOrEmpty(requireNo)) {
            // 異動區間
            if (searchQuery.getStartUpdatedDate() != null && searchQuery.getEndUpdatedDate() != null) {
                builder.append("    AND tr.update_dt BETWEEN '")
                        .append(searchHelper.transStrByStartDate(searchQuery.getStartUpdatedDate())).append("' AND '")
                        .append(searchHelper.transStrByEndDate(searchQuery.getEndUpdatedDate())).append("' ");
            } else if (searchQuery.getStartUpdatedDate() != null) {
                builder.append("    AND tr.update_dt >= '")
                        .append(searchHelper.transStrByStartDate(searchQuery.getStartUpdatedDate())).append("' ");
            } else if (searchQuery.getEndUpdatedDate() != null) {
                builder.append("    AND tr.update_dt <= '")
                        .append(searchHelper.transStrByEndDate(searchQuery.getEndUpdatedDate())).append("' ");
            }
            // 緊急度
            if (searchQuery.getUrgencyList() != null && !searchQuery.getUrgencyList().isEmpty()) {
                String urgencyStr = searchQuery.getUrgencyList().stream()
                        .collect(Collectors.joining(","));
                builder.append("    AND tr.urgency IN (").append(urgencyStr).append(") ");
            }
            // 需求單號
            if (!Strings.isNullOrEmpty(searchQuery.getRequireNo())) {
                String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(searchQuery.getRequireNo()) + "%";
                builder.append("    AND tr.require_no LIKE :textNo ");
                parameters.put("textNo", textNo);
            }
            // 轉發部門
            if (searchQuery.getForwardDepts() != null && !searchQuery.getForwardDepts().isEmpty()) {
                String forwardDepts = searchQuery.getForwardDepts().stream()
                        .collect(Collectors.joining(","));
                builder.append("    AND tr.require_sid IN ( ");
                builder.append("        SELECT tai.require_sid FROM tr_alert_inbox tai  ");
                builder.append("            WHERE tai.forward_type = 'FORWARD_DEPT' ");
                builder.append("            AND tai.receive_dep IN (").append(forwardDepts).append(") ");
                builder.append("    ) ");
            }
        }
        builder.append(") AS tr ");
    }

    /**
     * 組合模板查詢語句
     *
     * @param query
     * @param requireNo
     * @param builder
     */
    private void buildCategoryKeyMappingCondition(SearchQuery02 query, String requireNo, StringBuilder builder) {
        builder.append("INNER JOIN ( ");
        builder.append("    SELECT * FROM tr_category_key_mapping ckm ");
        builder.append("        WHERE 1=1 ");
        if (Strings.isNullOrEmpty(requireNo)) {
            // 需求類別
            if (!Strings.isNullOrEmpty(query.getSelectBigCategorySid())
                    && query.getBigDataCateSids().isEmpty()) {
                builder.append("        AND ckm.big_category_sid = '")
                        .append(query.getSelectBigCategorySid()).append("' ");
            }
            // 類別組合
            if (!query.getBigDataCateSids().isEmpty()
                    || !query.getMiddleDataCateSids().isEmpty()
                    || !query.getSmallDataCateSids().isEmpty()) {
                String bigs = "'isEmpty'";
                String middles = "'isEmpty'";
                String smalls = "'isEmpty'";
                if (!query.getBigDataCateSids().isEmpty()) {
                    bigs = query.getBigDataCateSids().stream()
                            .collect(Collectors.joining("','", "'", "'"));
                }
                if (!query.getMiddleDataCateSids().isEmpty()) {
                    middles = query.getMiddleDataCateSids().stream()
                            .collect(Collectors.joining("','", "'", "'"));
                }
                if (!query.getSmallDataCateSids().isEmpty()) {
                    smalls = query.getSmallDataCateSids().stream()
                            .collect(Collectors.joining("','", "'", "'"));
                }
                if (!Strings.isNullOrEmpty(query.getSelectBigCategorySid())) {
                    if ("'isEmpty'".equals(bigs)) {
                        bigs = "'" + query.getSelectBigCategorySid() + "'";
                    } else {
                        bigs = bigs + ",'" + query.getSelectBigCategorySid() + "'";
                    }
                }
                builder.append("        AND( ");
                builder.append("            ckm.big_category_sid IN (").append(bigs).append(") ");
                builder.append("            OR ");
                builder.append("            ckm.middle_category_sid IN (").append(middles).append(") ");
                builder.append("            OR ");
                builder.append("            ckm.small_category_sid IN (").append(smalls).append(")");
                builder.append("        ) ");
            }
        }
        builder.append(") AS ckm ON tr.mapping_sid=ckm.key_sid ");
    }

    /**
     * 組合簽核查詢語句
     *
     * @param query
     * @param requireNo
     * @param builder
     * @param parameters
     */
    private void bulidReqUnitSignInfoCondition(SearchQuery02 query, String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        /////////// 需求單位簽核進度查詢////////////
        builder.append("INNER JOIN ( ");
        builder.append("    SELECT * FROM tr_require_manager_sign_info rmsi ");
        builder.append("        WHERE 1=1 ");
        if (Strings.isNullOrEmpty(requireNo)) {
            // 需求單位審核狀態
            if (query.getSelectStatuses() != null && !query.getSelectStatuses().isEmpty()) {
                String instanceStatus = query.getSelectStatuses().stream()
                        .collect(Collectors.joining("','", "'", "'"));
                builder.append("        AND rmsi.paper_code IN (").append(instanceStatus).append(") ");
            }
            // 待審核人員
            if (!Strings.isNullOrEmpty(query.getPendingSignOnUserName())) {
                String signOnUsertext = "%" + reqularUtils.replaceIllegalSqlLikeStr(query.getPendingSignOnUserName()) + "%";
                builder.append("        AND rmsi.bpm_default_signed_name LIKE :signOnUsertext ");
                parameters.put("signOnUsertext", signOnUsertext);
            }
        }
        builder.append(") AS rmsi ON tr.require_sid=rmsi.require_sid ");
    }

}
