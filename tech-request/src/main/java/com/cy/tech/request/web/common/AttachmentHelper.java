/**
 * 
 */
package com.cy.tech.request.web.common;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkCommonUtils;

/**
 * @author allen1214_wu
 *
 */
@Component
public class AttachmentHelper {

    @Autowired
    private transient WkUserCache wkUserCache;
    @Autowired
    private transient WkOrgCache wkOrgCache;

    /**
     * 是否可刪除
     * @param atthOwnerDepSid 檔案歸屬單位
     * @param atthOwnerUserSid 檔案上傳者
     * @param loginUserSid 登入者
     * @return
     */
    public boolean isCanDelete(Integer atthOwnerDepSid, Integer atthOwnerUserSid, Integer loginUserSid) {

        // ====================================
        // 條件一：檔案為本人上傳
        // ====================================
        if (WkCommonUtils.compareByStr(atthOwnerUserSid, loginUserSid)) {
            return true;
        }

        // ====================================
        // 條件2：與上傳者同部門
        // ====================================
        // 取得使用者資料
        User loginUser = this.wkUserCache.findBySid(loginUserSid);
        if (loginUser == null || loginUser.getPrimaryOrg() == null) {
            return false;
        }
        // 比對
        if (WkCommonUtils.compareByStr(atthOwnerDepSid, loginUser.getPrimaryOrg().getSid())) {
            return true;
        }

        // ====================================
        // 條件3：上層單位主管
        // ====================================
        // 查詢分派單位所有上層部門 （過濾停用）
        List<Integer> parentOrgSids = this.wkOrgCache.findAllParent(atthOwnerDepSid).stream()
                .filter(org -> Activation.ACTIVE.equals(org.getStatus()))
                .filter(org -> org.getManager() != null)
                .map(org -> org.getManager().getSid())
                .collect(Collectors.toList());

        if (parentOrgSids.contains(loginUser.getPrimaryOrg().getSid())) {
            return true;
        }

        // ====================================
        // 條件4：管理單位 (代理)
        // ====================================
        // 取得登入者所管理的單位 （過濾停用）
        List<Integer> managerOrgSids = this.wkOrgCache.findManagerOrgs(loginUserSid).stream()
                .filter(org -> Activation.ACTIVE.equals(org.getStatus()))
                .map(org -> org.getSid())
                .collect(Collectors.toList());

        if (managerOrgSids.contains(loginUser.getPrimaryOrg().getSid())) {
            return true;
        }

        return false;
    }
}
