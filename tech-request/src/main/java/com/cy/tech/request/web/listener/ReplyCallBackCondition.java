/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.listener;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author brain0925_liao
 */
public class ReplyCallBackCondition {

    @Setter
    @Getter
    private String os_sid;
    @Setter
    @Getter
    private String os_no;
    @Setter
    @Getter
    private String require_sid;
    @Setter
    @Getter
    private String require_no;
    @Setter
    @Getter
    private String os_history_sid;
    @Setter
    @Getter
    private String reply_and_reply_history_sid;
    @Setter
    @Getter
    private String os_reply_sid;
    @Setter
    @Getter
    private String os_reply_and_reply_sid;

}
