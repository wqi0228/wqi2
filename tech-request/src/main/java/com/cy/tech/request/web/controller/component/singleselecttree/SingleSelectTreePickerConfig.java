/**
 * 
 */
package com.cy.tech.request.web.controller.component.singleselecttree;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * SingleSelectTreePicker 建構參數設定物件
 * 
 * @author allen1214_wu
 */
@Getter
@Setter
public class SingleSelectTreePickerConfig implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 1633322843788882315L;
    public SingleSelectTreePickerConfig(SingleSelectTreeCallback singleSelectTreeCallback) {
		this.singleSelectTreeCallback = singleSelectTreeCallback;
	}

	/**
	 * 可選擇項目 sid
	 */
	private SingleSelectTreeCallback singleSelectTreeCallback;
	/**
	 * 預設選擇項目Sid
	 */
	private String defaultSelectedItemSid;
}
