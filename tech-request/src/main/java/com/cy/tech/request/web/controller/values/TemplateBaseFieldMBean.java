/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.values;

import com.cy.tech.request.logic.service.TemplateBaseFieldService;
import com.cy.tech.request.vo.template.TemplateBaseDataField;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 基礎欄位欄版
 *
 * @author shaun
 */
@Controller
@Scope(WebApplicationContext.SCOPE_APPLICATION)
public class TemplateBaseFieldMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2177509876504715557L;
    @Autowired
    transient private TemplateBaseFieldService service;

    public List<TemplateBaseDataField> getValues() {
        return service.findByFazzyText("");
    }
}
