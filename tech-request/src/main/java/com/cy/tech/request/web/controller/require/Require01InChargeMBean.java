package com.cy.tech.request.web.controller.require;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.RequireTraceService;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.component.reqconfirm.ReqConfirmClientHelper;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.pf.utils.ConfirmCallbackDialogController;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.viewcomponent.ssutreepicker.SingleSelectUserTreeCallback;
import com.cy.work.viewcomponent.ssutreepicker.SingleSelectUserTreePicker;
import com.cy.work.viewcomponent.ssutreepicker.SingleSelectUserTreePickerConfig;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@NoArgsConstructor
@Controller
@Scope("view")
public class Require01InChargeMBean implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 8495728749692752961L;
    // ========================================================================
	// 服務組件區
	// ========================================================================
	@Autowired
	private transient Require01MBean require01MBean;
	@Autowired
	private transient RequireService requireService;
	@Autowired
	private transient RequireTraceService requireTraceService;
	@Autowired
	private transient DisplayController displayController;
	@Autowired
	private transient ConfirmCallbackDialogController confirmCallbackDialogController;

	// ========================================================================
	// 變數區
	// ========================================================================

	/**
	 * 組件:主責單位負責人選單
	 */
	@Getter
	private SingleSelectUserTreePicker userInChargeTreePicker;

	@Getter
	private final String dialogName = "require_dlg_in_charge";
	@Getter
	private final String dialogContentName = "require_dlg_in_charge_content";

	// ========================================================================
	// 方法區
	// ========================================================================
	/**
	 * 開啟 dialog
	 */
	public void openDialog() {

		// ====================================
		// 防呆
		// ====================================
		if (require01MBean == null || require01MBean.getRequire() == null) {
			MessagesUtils.showWarn(WkMessage.SESSION_TIMEOUT);
			return;
		}

		// ====================================
		// 建立選單組件
		// ====================================
		try {
			// 可選擇範圍為登入者公司下所有單位
			Org comp = WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId());
			Set<Integer> allDepSids = WkOrgCache.getInstance().findAllChildSids(comp.getSid());

			// 建立組件 config
			SingleSelectUserTreePickerConfig config = new SingleSelectUserTreePickerConfig(
			        Sets.newHashSet(allDepSids));

			// 已選擇的人員所在的單位 orgSid
			config.setSelectedDepSid(require01MBean.getRequire().getInChargeDep());
			// 已選擇的人員 userSid
			config.setSelectedUserSid(require01MBean.getRequire().getInChargeUsr());
			// call back 事件實做 (雙擊選項事件)
			config.setCallback(userInChargeTreePickerCallback);

			// 初始化選單組件
			this.userInChargeTreePicker = new SingleSelectUserTreePicker(config);

		} catch (Exception e) {
			String message = WkMessage.EXECTION + "[" + e.getMessage() + "]";
			log.error(message, e);
			MessagesUtils.showWarn(message);
			return;
		}

		// ====================================
		// 開啟選單
		// ====================================
		this.displayController.showPfWidgetVar(dialogName);
	}

	/**
	 * 確認
	 */
	public void confirm() {

		// ====================================
		// 防呆
		// ====================================
		if (require01MBean == null || require01MBean.getRequire() == null) {
			MessagesUtils.showWarn(WkMessage.SESSION_TIMEOUT);
			return;
		}

		// ====================================
		// 新增、編輯頁 , 僅更新 require ,並更新顯示資訊
		// ====================================
		if (require01MBean.isEditMode()) {
			require01MBean.getRequire().setInChargeDep(this.userInChargeTreePicker.getSelectedDepSid());
			require01MBean.getRequire().setInChargeUsr(this.userInChargeTreePicker.getSelectedUserSid());
			this.displayController.update("require01_title_info_id");
			this.displayController.hidePfWidgetVar(dialogName);
			return;
		}

		// ====================================
		// 組確認訊息
		// ====================================
		String traceContent = this.requireTraceService.createChangeInChargeTraceMessage(
		        require01MBean.getRequire().getInChargeDep(),
		        this.userInChargeTreePicker.getSelectedDepSid(),
		        require01MBean.getRequire().getInChargeUsr(),
		        this.userInChargeTreePicker.getSelectedUserSid());

		if (WkStringUtils.isEmpty(traceContent)) {
			MessagesUtils.showInfo("未異動主責單位資訊!");
			this.displayController.hidePfWidgetVar(dialogName);
			return;
		}

		traceContent = "是否確認異動主責單位資訊？<br/><br/>" + traceContent;

		confirmCallbackDialogController.showConfimDialog(
		        traceContent,
		        "",
		        () -> this.update());

	}

	private void update() {

		// ====================================
		// 防呆
		// ====================================
		if (require01MBean == null || require01MBean.getRequire() == null) {
			MessagesUtils.showWarn(WkMessage.SESSION_TIMEOUT);
			return;
		}

		// ====================================
		// 異動訊息
		// ====================================
		try {
			this.requireService.updateInCharge(
			        require01MBean.getRequire().getSid(),
			        this.userInChargeTreePicker.getSelectedDepSid(),
			        this.userInChargeTreePicker.getSelectedUserSid(),
			        SecurityFacade.getUserSid(),
			        new Date(),
			        false);

		} catch (UserMessageException e) {
			MessagesUtils.show(e);
			return;
		} catch (Exception e) {
			String message = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
			MessagesUtils.showError(message);
			return;
		}

		// ====================================
		// 更新畫面
		// ====================================
		this.require01MBean.reBuildeByUpdateFaild();
		this.require01MBean.getTraceMBean().clear();
		this.require01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.TRACE);

		this.displayController.execute(""
		        + "accPanelSelectTab('req_trace_tab_wv', 0);"
		        + ReqConfirmClientHelper.CLIENT_SRIPT_CLOSE_PANEL);

		this.displayController.hidePfWidgetVar(dialogName);
	}

	/**
	 * 
	 */
	protected final SingleSelectUserTreeCallback userInChargeTreePickerCallback = new SingleSelectUserTreeCallback() {
		/**
         * 
         */
        private static final long serialVersionUID = -7611412601379692768L;

        /**
		 * 雙擊項目時的動作
		 */
		@Override
		public void doubleClickItem() {
			confirm();
		}
	};

	/**
	 * 兜組主責單位資訊
	 * 
	 * @param require 需求單主檔物件
	 * @return
	 */
	public String prepareInChargeDepInfo(Require require) {
		// 防呆
		if (require == null) {
			return "";
		}

		if (require.getInChargeDep() == null) {
			return "<span class='WS1-1-2b'>未設定</span>";
		}

		// 主責單位
		String depName = WkOrgUtils.prepareBreadcrumbsByDepName(
		        require.getInChargeDep(),
		        OrgLevel.MINISTERIAL,
		        true,
		        "-");

		// 已停用部門加上標示
		if (!WkOrgUtils.isActive(require.getInChargeDep())) {
			depName = WkHtmlUtils.addStrikethroughStyle(depName, "停用");
		}

		// 負責人
		String userName = "";
		if (WkUserCache.getInstance().isExsit(require.getInChargeUsr())) {
			userName = WkUserUtils.findNameBySid(require.getInChargeUsr());
			if (!WkUserUtils.isActive(require.getInChargeUsr())) {
				userName = WkHtmlUtils.addStrikethroughStyle(userName, "停用");
			}
			userName = "&nbsp;(" + userName + ")";
		}

		return depName + userName;
	}

	/**
	 * @return
	 */
	public boolean showSettingBtn() {

		try {
			// 防呆
			if (this.require01MBean == null || this.require01MBean.getRequire() == null || this.require01MBean.getRequire().getRequireStatus() == null) {
				return false;
			}

			// 全單編輯模式時，顯示
			if (RequireStep.LOAD_IN_PAGE_EDIT.equals(this.require01MBean.getSetp())) {
				return true;
			}

			// 流程在『檢查確認』節點後，顯示單獨編輯模式 (檢查前都走全單編輯模式)
			return !this.require01MBean.getRequire().getRequireStatus().isFlowBeforeWaitCheck();

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return false;
	}

}
