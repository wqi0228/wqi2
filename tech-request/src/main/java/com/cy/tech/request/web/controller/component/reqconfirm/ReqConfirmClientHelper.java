package com.cy.tech.request.web.controller.component.reqconfirm;

import java.io.Serializable;

import com.cy.tech.request.vo.require.Require;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.utils.WkCommonUtils;

public class ReqConfirmClientHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -13376546157336942L;
    /**
     * 關閉面板的 script
     */
    public static final String CLIENT_SRIPT_CLOSE_PANEL = ";if(window._reqConfirmDepPanel){window._reqConfirmDepPanel.close();};";

    /**
     * 準備 client 讓進度面板初始化的 script
     * 
     * @param require
     * @return
     */
    public static String prepareClientPanelInitScript(Require require) {

        ReqConfirmDepPanelComponent reqConfirmDepPanelComponent = WorkSpringContextHolder.getBean(ReqConfirmDepPanelComponent.class);
        boolean isShowPanel = false;
        if (require == null) {
            isShowPanel = false;
        } else if (reqConfirmDepPanelComponent != null) {
            isShowPanel = reqConfirmDepPanelComponent.showPanel(require);
        } else {
            WkCommonUtils.logWithStackTrace(InfomationLevel.ERROR, "取不到 ReqConfirmDepPanelComponent");
        }

        return prepareScript(isShowPanel);
    }

    /**
     * 準備 client 讓進度面板移除的 script
     * 
     * @return
     */
    public static String prepareClientPanelRemoveScript() {
        return prepareScript(false);
    }

    private static String prepareScript(boolean isShowPanel) {

        String script = ";if(window._reqConfirmDepPanel){";
        script += "window._reqConfirmDepPanel.isShow=" + isShowPanel;
        script += ";window._reqConfirmDepPanel.init();";
        script += CLIENT_SRIPT_CLOSE_PANEL;
        script += "};";

        return script;
    }
}
