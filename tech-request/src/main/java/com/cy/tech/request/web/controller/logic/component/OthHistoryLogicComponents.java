/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.logic.component;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.tros.TrOsHistoryService;
import com.cy.tech.request.logic.utils.ToolsDate;
import com.cy.tech.request.logic.vo.AttachmentVO;
import com.cy.tech.request.logic.vo.SimpleDateFormatEnum;
import com.cy.tech.request.logic.vo.TrOsHistoryVO;
import com.cy.tech.request.vo.enums.OthSetHistoryBehavior;
import com.cy.tech.request.web.controller.view.vo.HistoryVO;
import com.cy.tech.request.web.controller.view.vo.SubReplyVO;
import com.cy.tech.request.web.listener.ReplyCallBack;
import com.cy.tech.request.web.listener.TabLoadCallBack;
import com.cy.tech.request.web.listener.UploadAttCallBack;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

/**
 * History 邏輯元件
 *
 * @author brain0925_liao
 */
@Component
public class OthHistoryLogicComponents implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3086553907890552205L;
    private static OthHistoryLogicComponents instance;
    /** TrOsHistoryService */
    @Autowired
    private TrOsHistoryService trOsHistoryService;
    /** WkJsoupUtils */
    @Autowired
    private WkJsoupUtils jsoupUtils;

    public static OthHistoryLogicComponents getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        OthHistoryLogicComponents.instance = this;
    }

    /**
     * 建立完成Hitory
     *
     * @param os_sid           其他設定資訊Sid
     * @param os_no            其他設定資訊單號
     * @param require_sid      需求單Sid
     * @param require_no       需求單單號
     * @param reply_person_dep 執行完成者部門Sid
     * @param reply_person_sid 執行完成者Sid
     * @param reply_content    回覆內容
     * @param attachmentVOs    附件
     * @return
     */
    public void createFinishHistory(String os_sid, String os_no, String require_sid,
            String require_no, Integer reply_person_dep,
            Integer reply_person_sid, String reply_content,
            List<AttachmentVO> attachmentVOs) {
        trOsHistoryService.createFinishHistory(os_sid, os_no, require_sid, require_no, reply_person_dep, reply_person_sid, reply_content, attachmentVOs);

    }

    /**
     * 建立取消History
     *
     * @param os_sid           其他設定資訊Sid
     * @param os_no            其他設定資訊單號
     * @param require_sid      需求單Sid
     * @param require_no       需求單單號
     * @param reply_person_dep 執行取消者部門Sid
     * @param reply_person_sid 執行取消者Sid
     * @param reply_content    回覆內容
     * @param attachmentVOs    附件
     * @return
     */
    public void createCancelHistory(String os_sid, String os_no, String require_sid,
            String require_no, Integer reply_person_dep,
            Integer reply_person_sid, String reply_content,
            List<AttachmentVO> attachmentVOs) {
        trOsHistoryService.createCancelHistory(os_sid, os_no, require_sid, require_no, reply_person_dep, reply_person_sid, reply_content, attachmentVOs);
    }

    /**
     * 建立回覆的回覆Hitory
     *
     * @param os_sid           其他設定資訊Sid
     * @param os_reply_sid     回覆Sid
     * @param reply_person_dep 回覆的回覆的使用者部門Sid
     * @param reply_person_sid 回覆的回覆的使用者Sid
     * @param reply_content    回覆的回覆的內容
     * @param attachmentVOs    附件
     * @return
     */
    public String createReplyAndReplyHistory(String os_sid, String os_reply_sid, Integer reply_person_dep,
            Integer reply_person_sid, String reply_content,
            List<AttachmentVO> attachmentVOs) {
        return trOsHistoryService.createReplyAndReplyHistory(os_sid, os_reply_sid, reply_person_dep, reply_person_sid, reply_content, attachmentVOs).getOs_reply_and_reply_sid();
    }

    /**
     * 建立回覆的History
     *
     * @param os_sid                其他設定資訊Sid
     * @param os_no                 其他設定資訊單號
     * @param require_sid           需求單Sid
     * @param require_no            需求單單號
     * @param reply_person_dep      回覆者部門Sid
     * @param reply_person_sid      回覆者Sid
     * @param reply_content         回覆內容
     * @param attachmentVOs         附件
     * @param othSetHistoryBehavior 行為
     * @return
     */
    public String createReplyHistory(String os_sid, String os_no, String require_sid,
            String require_no, Integer reply_person_dep,
            Integer reply_person_sid, String reply_content,
            List<AttachmentVO> attachmentVOs, OthSetHistoryBehavior othSetHistoryBehavior) {
        Preconditions.checkState(!Strings.isNullOrEmpty(jsoupUtils.clearCssTag(reply_content)), "回覆不可為空白，請重新輸入！！");
        return trOsHistoryService.createReplyHistory(os_sid, os_no, require_sid, require_no, reply_person_dep, reply_person_sid, reply_content, attachmentVOs, othSetHistoryBehavior).getSid();
    }

    /**
     * 修改回覆
     *
     * @param updateUserSid 更新者Sid
     * @param historySid    HistorySid
     * @param replySid      回覆Sid
     * @param content       內容
     */
    public void modifyReplyHistory(Integer updateUserSid, String historySid, String replySid, String content) {
        Preconditions.checkState(!Strings.isNullOrEmpty(jsoupUtils.clearCssTag(content)), "回覆不可為空白，請重新輸入！！");
        trOsHistoryService.modifyReplyHistory(updateUserSid, historySid, replySid, content);
    }

    /**
     * 修改回覆的回覆
     *
     * @param updateUserSid    更新者Sid
     * @param historySid       HistorySid
     * @param replyAndReplySid 回覆的回覆Sid
     * @param content          內容
     */
    public void modifyReplyAndReplyHistory(Integer updateUserSid, String historySid, String replyAndReplySid, String content) {
        Preconditions.checkState(!Strings.isNullOrEmpty(jsoupUtils.clearCssTag(content)), "回覆不可為空白，請重新輸入！！");
        trOsHistoryService.modifyReplyAndReplyHistory(updateUserSid, historySid, replyAndReplySid, content);
    }

    /**
     * 取得History物件List By request_Sid And s_os_sid
     * 
     * @param request_Sid       需求單Sid
     * @param s_os_sid          其他設定資訊Sid
     * @param loginUserSid      登入者Sid
     * @param loginUserDepSid   登入者部門Sid
     * @param uploadAttCallBack UploadAttCallBack
     * @param tabLoadCallBack   TabLoadCallBack
     * @param replyCallBack     ReplyCallBack
     * @return
     */
    public List<HistoryVO> getHistoryVOByRequestSidAndOs_Sid(String request_Sid, String s_os_sid, Integer loginUserSid,
            Integer loginUserDepSid, UploadAttCallBack uploadAttCallBack, TabLoadCallBack tabLoadCallBack, ReplyCallBack replyCallBack) {
        // Boolean requireCloseCode = requireService.getCloseCodeBySid(request_Sid);
        List<TrOsHistoryVO> trHistoryVOs = trOsHistoryService.getTrOsByRequestSid(request_Sid, s_os_sid);
        List<HistoryVO> historyVOs = Lists.newArrayList();
        trHistoryVOs.forEach(historyItem -> {
            
            User createUser = WkUserCache.getInstance().findBySid(historyItem.getCreate_usr());
            Org createDep = WkOrgCache.getInstance().findBySid(createUser.getPrimaryOrg().getSid());
            String content = "";
            boolean showAttachmentBtn = false;
            boolean showModifyBtn = false;
            boolean showExpandAndCompress = false;
            boolean showReplyAndReplyBtn = false;
            List<SubReplyVO> subReplyVOs = Lists.newArrayList();
            if (historyItem.getBehavior().equals(OthSetHistoryBehavior.REPLY)) {
                content = historyItem.getTrOsReplyVO().getReply_content_css();
                showAttachmentBtn = (historyItem.getTrOsReplyVO().getTrOsAttachmentVOs() != null
                        && !historyItem.getTrOsReplyVO().getTrOsAttachmentVOs().isEmpty());
                historyItem.getTrOsReplyVO().getTrOsReplyAndReplyVO().forEach(replyAndReplyItem -> {
                    boolean showReplyAndReplyAttachmentBtn = (replyAndReplyItem.getTrOsAttachmentVOs() != null
                            && !replyAndReplyItem.getTrOsAttachmentVOs().isEmpty());
                    boolean showReplyAndReplyModifyBtn = true;
                    boolean disableModifyBtn = !(replyAndReplyItem.getReply_person_sid().equals(loginUserSid));
                    User replyAndReplyCreateUser = WkUserCache.getInstance().findBySid(replyAndReplyItem.getReply_person_sid());
                    Org replyAndReplyCreateDep = WkOrgCache.getInstance().findBySid(replyAndReplyCreateUser.getPrimaryOrg().getSid());

                    SubReplyVO subReplyVO = new SubReplyVO(
                            replyAndReplyItem.getOs_reply_and_reply_sid(),
                            replyAndReplyItem.getOs_sid(),
                            historyItem.getOs_history_sid(),
                            replyAndReplyItem.getOs_history_sid(),
                            showReplyAndReplyModifyBtn,
                            showReplyAndReplyAttachmentBtn,
                            disableModifyBtn,
                            replyAndReplyCreateUser.getSid(),
                            replyAndReplyCreateDep.getSid(),
                            replyAndReplyCreateUser.getName(),
                            WkOrgUtils.getOrgName(replyAndReplyCreateDep),
                            ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS_slide.getValue(), replyAndReplyItem.getReply_udt()),
                            replyAndReplyItem.getReply_content_css(),
                            replyCallBack,
                            subReplyVOs.size());
                    subReplyVOs.add(subReplyVO);
                });

            } else {
                content = historyItem.getContent_css();
            }
            if (historyItem.getBehavior().equals(OthSetHistoryBehavior.REPLY)
                    && (subReplyVOs == null || subReplyVOs.isEmpty())
                    && historyItem.getCreate_usr().equals(loginUserSid)) {
                showModifyBtn = true;
            }
            if (historyItem.getBehavior().equals(OthSetHistoryBehavior.REPLY)
                    && (subReplyVOs != null && !subReplyVOs.isEmpty())) {
                showExpandAndCompress = true;
            }
            if (historyItem.getBehavior().equals(OthSetHistoryBehavior.REPLY)) {
                showReplyAndReplyBtn = true;
            }
            HistoryVO hv = new HistoryVO(
                    historyItem.getOs_sid(),
                    historyItem.getOs_history_sid(),
                    historyItem.getOs_reply_sid(),
                    showModifyBtn,
                    showAttachmentBtn,
                    showExpandAndCompress,
                    showReplyAndReplyBtn,
                    historyItem.getBehavior().getVal(),
                    historyItem.getCreate_usr(),
                    createUser.getPrimaryOrg().getSid(),
                    createUser.getName(),
                    WkOrgUtils.getOrgName(createDep),
                    ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS_slide.getValue(), historyItem.getUpdate_dt()),
                    content,
                    subReplyVOs,
                    replyCallBack,
                    historyVOs.size());
            historyVOs.add(hv);
        });

        return historyVOs;
    }

}
