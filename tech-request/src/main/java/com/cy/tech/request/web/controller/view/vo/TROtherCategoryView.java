/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 第二分類 列表自訂物件
 *
 * @author kasim
 */
@Data
@ToString
@NoArgsConstructor
public class TROtherCategoryView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4975540925565767040L;

    private String sid;

    /** 建立日期 */
    private Date createdDate;

    /** 建立人員 */
    private String createdUserName;

    /** 分類名稱 */
    private String name;

    /** 對應資訊 */
    private String mappingData;

    /** 狀態 */
    private String status;

    /** 異動日期 */
    private Date updatedDate;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TROtherCategoryView other = (TROtherCategoryView) obj;
        if (!Objects.equals(this.sid, other.sid)) {
            return false;
        }
        return true;
    }

}
