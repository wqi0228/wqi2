/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.component.searchquery;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;

import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.DateType;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.service.CategorySettingService;
import com.cy.tech.request.logic.service.customer.ReqWorkCustomerHelper;
import com.cy.tech.request.logic.vo.CustomerTo;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.Search16QueryColumn;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.tech.request.web.controller.component.qkstree.QuickSelectTreeCallback;
import com.cy.tech.request.web.controller.component.qkstree.impl.OrgUserTreeMBean;
import com.cy.tech.request.web.controller.enums.ReqStatusMBean;
import com.cy.tech.request.web.controller.enums.WaitReadReasonTypeBean;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterArrayStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterDetailVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterVO;
import com.cy.tech.request.web.controller.view.vo.Search16QueryVo;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.pf.utils.CommonBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.customer.vo.enums.EnableType;
import com.cy.work.logic.WkUserAndOrgLogic;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author shaun
 */
@Slf4j
public class SearchQuery16 {

    private WkUserAndOrgLogic wkUserAndOrgLogic = WorkSpringContextHolder.getBean(WkUserAndOrgLogic.class);

    /** 畫面用查詢物件 */
    @Getter
    private Search16QueryVo queryVo;
    /** 自定義查詢物件 */
    @Getter
    private Search16QueryVo customerQueryVo;
    /** 人員選擇器 (回覆人員) */
    @Getter
    private OrgUserTreeMBean usersPicker;
    /** 登入公司ID */
    private String compId;

    /** 畫面顯示控制元件 */
    private DisplayController display;
    /** 訊息呼叫控制 */
    private MessageCallBack messageCallBack;
    /** 需求單狀態處理工具 */
    private ReqStatusMBean reqStatusUtils;
    /** 客戶資料服務 */
    private ReqWorkCustomerHelper customerService;
    //////////////////////////////////////////////////////////////////////////// 以下為廳主
    /* 廳主選項 **/
    @Getter
    private List<CustomerTo> customerItems;
    /** 廳主 */
    @Getter
    @Setter
    private List<CustomerTo> customers = Lists.newArrayList();
    /** 是否廳主分頁 */
    @Getter
    @Setter
    private Boolean isCustomerPage = Boolean.FALSE;
    /** 廳主(分頁條件) */
    @Getter
    @Setter
    private CustomerTo customerPage;
    @Getter
    @Setter
    /* 廳主分頁選項 **/
    private List<CustomerTo> customerPageItems;
    /** 區間選擇 */
    @Getter
    @Setter
    private Search16QueryColumn dateTimeType = Search16QueryColumn.CREATE_DATE;

    //////////////////////////////////////////////////////////////////////////// 以下為進階搜尋條件
    /** 該登入者在需求單,自訂查詢條件List */
    private List<ReportCustomFilterVO> reportCustomFilterVOs;
    /** 該登入者在需求單,挑選自訂查詢條件物件 */
    private ReportCustomFilterVO selReportCustomFilterVO;
    /** ReportCustomFilterLogicComponent */
    private ReportCustomFilterLogicComponent reportCustomFilterLogicComponent;

    private Integer loginUserSid;

    public SearchQuery16(
            String compId,
            MessageCallBack messageCallBack,
            DisplayController display,
            ReqStatusMBean reqStatusUtils,
            ReqWorkCustomerHelper customerService,
            ReportCustomFilterLogicComponent reportCustomFilterLogicComponent,
            CategorySettingService categorySettingService,
            OrgUserTreeMBean usersPicker,
            Integer loginUserSid) {
        this.compId = compId;
        this.usersPicker = usersPicker;
        this.reportCustomFilterLogicComponent = reportCustomFilterLogicComponent;
        this.loginUserSid = loginUserSid;
        this.messageCallBack = messageCallBack;
        this.queryVo = new Search16QueryVo();
        this.customerQueryVo = new Search16QueryVo();
        this.display = display;
        this.reqStatusUtils = reqStatusUtils;
        this.customerService = customerService;
    }

    public void saveDefaultSetting() {
        try {
            ReportCustomFilterDetailVO demandType = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search16QueryColumn.DemandType,
                    (customerQueryVo.getSelectBigCategory() != null) ? customerQueryVo.getSelectBigCategory() : "");

            ReportCustomFilterDetailVO department = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search16QueryColumn.Department,
                    customerQueryVo.getCreateOnpgDepts());
            ReportCustomFilterDetailVO noticeDepartment = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(
                    Search16QueryColumn.NotifyDepartment,
                    customerQueryVo.getOnpgNoticeDepts());

            // 為全部時, 特別處理
            ReportCustomFilterDetailVO demandProcess = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(
                    Search16QueryColumn.DemandProcess,
                    (customerQueryVo.getSelectRequireStatusType() != null) ? customerQueryVo.getSelectRequireStatusType().name() : "ALL");

            ReportCustomFilterDetailVO readStatus = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search16QueryColumn.ReadStatus,
                    (customerQueryVo.getSelectReadRecordType() != null) ? customerQueryVo.getSelectReadRecordType().name() : "");
            ReportCustomFilterDetailVO demandPerson = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search16QueryColumn.DemandPerson,
                    customerQueryVo.getOpCreatedUsrName());
            List<String> waitReadResonStr = Lists.newArrayList();
            customerQueryVo.getReadReason().forEach(item -> {
                waitReadResonStr.add(item.name());
            });
            ReportCustomFilterDetailVO waitReadResons = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search16QueryColumn.WaitReadReson,
                    waitReadResonStr);
            ReportCustomFilterDetailVO onpgStatuss = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search16QueryColumn.OnpgStatus,
                    customerQueryVo.getOnPgStatus());
            ReportCustomFilterDetailVO categoryCombo = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search16QueryColumn.CategoryCombo,
                    customerQueryVo.getSmallDataCateSids());
            ReportCustomFilterDetailVO dateIndex = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search16QueryColumn.DateIndex,
                    customerQueryVo.getDateTypeIndexStr());
            ReportCustomFilterDetailVO searchText = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search16QueryColumn.SearchText,
                    customerQueryVo.getFuzzyText());

            List<ReportCustomFilterDetailVO> saveDetails = Lists.newArrayList();
            saveDetails.add(demandType);
            saveDetails.add(department);
            saveDetails.add(noticeDepartment);
            saveDetails.add(demandProcess);
            saveDetails.add(readStatus);
            saveDetails.add(demandPerson);
            saveDetails.add(waitReadResons);
            saveDetails.add(onpgStatuss);
            saveDetails.add(categoryCombo);
            saveDetails.add(dateIndex);
            saveDetails.add(searchText);

            reportCustomFilterLogicComponent.saveReportCustomFilter(
                    Search16QueryColumn.Search16Query,
                    loginUserSid,
                    (selReportCustomFilterVO != null) ? selReportCustomFilterVO.getIndex() : null,
                    true,
                    saveDetails);

            loadSetting();
            display.update("headerTitle");
            display.execute("doSearchData();");
            display.hidePfWidgetVar("dlgReportCustomFilter");
        } catch (Exception e) {
            this.messageCallBack.showMessage(e.getMessage());
            log.error("saveDefaultSetting ERROR", e);
        }
    }

    public void loadSetting() {
        initSearchQueryVODefautSetting(queryVo);
        loadSettingData(queryVo);
    }

    public void loadSettingWithoutCust() {
        initSearchQueryVODefautSetting(queryVo);
    }

    public void loadSettingData(Search16QueryVo searchQueryVO) {
        reportCustomFilterVOs = reportCustomFilterLogicComponent.getReportCustomFilter(Search16QueryColumn.Search16Query, loginUserSid);
        if (reportCustomFilterVOs == null || reportCustomFilterVOs.isEmpty()) {
            return;
        }
        selReportCustomFilterVO = reportCustomFilterVOs.get(0);
        selReportCustomFilterVO.getReportCustomFilterDetailVOs().forEach(item -> {
            if (Search16QueryColumn.DemandType.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDemandType(item, searchQueryVO);
            } else if (Search16QueryColumn.Department.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingDepartment(item, searchQueryVO);
            } else if (Search16QueryColumn.NotifyDepartment.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingNotifyDepartment(item, searchQueryVO);
            } else if (Search16QueryColumn.DemandProcess.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDemandProcess(item, searchQueryVO);
            } else if (Search16QueryColumn.ReadStatus.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingReadStatus(item, searchQueryVO);
            } else if (Search16QueryColumn.DemandPerson.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDemandPerson(item, searchQueryVO);
            } else if (Search16QueryColumn.WaitReadReson.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingWaitReadReson(item, searchQueryVO);
            } else if (Search16QueryColumn.OnpgStatus.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingOnpgStatus(item, searchQueryVO);
            } else if (Search16QueryColumn.CategoryCombo.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingCategoryCombo(item, searchQueryVO);
            } else if (Search16QueryColumn.DateIndex.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDateIndex(item, searchQueryVO);
            } else if (Search16QueryColumn.SearchText.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingFuzzyText(item, searchQueryVO);
            }
        });
    }

    /**
     * 塞入模糊搜尋
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingFuzzyText(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search16QueryVo searchQueryVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQueryVO.setFuzzyText(reportCustomFilterStringVO.getValue());
        } catch (Exception e) {
            log.error("settingFuzzyText", e);
        }
    }

    /**
     * 塞入立單區間Type
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDateIndex(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search16QueryVo searchQueryVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQueryVO.setDateTypeIndexStr(reportCustomFilterStringVO.getValue());
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                searchQueryVO.setDateTypeIndex(Integer.valueOf(reportCustomFilterStringVO.getValue()));
            }

            if (searchQueryVO.equals(queryVo)) {
                if (DateType.PREVIOUS_MONTH.ordinal() == queryVo.getDateTypeIndex()) {
                    getDateInterval(DateType.PREVIOUS_MONTH.name());
                } else if (DateType.THIS_MONTH.ordinal() == queryVo.getDateTypeIndex()) {
                    getDateInterval(DateType.THIS_MONTH.name());
                } else if (DateType.NEXT_MONTH.ordinal() == queryVo.getDateTypeIndex()) {
                    getDateInterval(DateType.NEXT_MONTH.name());
                } else if (DateType.TODAY.ordinal() == queryVo.getDateTypeIndex()) {
                    getDateInterval(DateType.TODAY.name());
                }
            }
        } catch (Exception e) {
            log.error("settingFuzzyText", e);
        }
    }

    /**
     * 塞入需求類別
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingCategoryCombo(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search16QueryVo searchQueryVO) {
        try {
            searchQueryVO.getSmallDataCateSids().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQueryVO.getSmallDataCateSids().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingCategoryCombo", e);
        }
    }

    /**
     * 塞入Onpg狀態
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingOnpgStatus(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search16QueryVo searchQueryVO) {
        try {
            searchQueryVO.getOnPgStatus().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQueryVO.getOnPgStatus().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingDepartment", e);
        }
    }

    /**
     * 塞入待閱原因
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingWaitReadReson(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search16QueryVo searchQueryVO) {
        try {
            searchQueryVO.getReadReason().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                reportCustomFilterArrayStringVO.getArrayStrings().forEach(item -> {
                    searchQueryVO.getReadReason().add(WaitReadReasonType.valueOf(item));
                });
            }
        } catch (Exception e) {
            log.error("settingWaitReadReson", e);
        }
    }

    /**
     * 塞入需求人員
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandPerson(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search16QueryVo searchQueryVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQueryVO.setOpCreatedUsrName(reportCustomFilterStringVO.getValue());
        } catch (Exception e) {
            log.error("trCreatedUserName", e);
        }
    }

    /**
     * 塞入是否閱讀：
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingReadStatus(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search16QueryVo searchQueryVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                searchQueryVO.setSelectReadRecordType(ReadRecordType.valueOf(reportCustomFilterStringVO.getValue()));
            }
        } catch (Exception e) {
            log.error("settingDemandSource", e);
        }
    }

    /**
     * 塞入需求類別
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandProcess(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search16QueryVo searchQueryVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            String value = reportCustomFilterStringVO.getValue();
            if (WkStringUtils.notEmpty(value)) {
                if ("ALL".equals(value)) {
                    searchQueryVO.setSelectRequireStatusType(null);
                } else {
                    searchQueryVO.setSelectRequireStatusType(RequireStatusType.valueOf(value));
                }
            }
        } catch (Exception e) {
            log.error("settingDemandType", e);
        }
    }

    /**
     * 塞入被分派單位
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingNotifyDepartment(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search16QueryVo searchQueryVO) {
        try {
            searchQueryVO.getOnpgNoticeDepts().clear();

            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQueryVO.getOnpgNoticeDepts().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingDepartment", e);
        }

    }

    /**
     * 塞入被分派單位
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDepartment(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search16QueryVo searchQueryVO) {
        try {
            searchQueryVO.getCreateOnpgDepts().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQueryVO.getCreateOnpgDepts().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingDepartment", e);
        }
    }

    /**
     * 塞入需求類別
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandType(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search16QueryVo searchQueryVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                searchQueryVO.setSelectBigCategory(reportCustomFilterStringVO.getValue());
            }
        } catch (Exception e) {
            log.error("settingDemandType", e);
        }
    }

    public void initSetting() {
        this.initCustomerItems();
        this.initSearchQueryVODefautSetting(queryVo);
        this.customers = Lists.newArrayList();
        this.isCustomerPage = false;
        this.customerPage = null;
        this.getDateInterval(ReportType.WORK_ON_PG.getDateType().name());
    }

    private void initSearchQueryVODefautSetting(Search16QueryVo searchQueryVO) {
        // 共用查詢條件初始化
        searchQueryVO.publicConditionInit();
        // 第一排條件設定
        searchQueryVO.setSelectBigCategory(null);
        searchQueryVO.setSmallDataCateSids(Lists.newArrayList());
        searchQueryVO.setSelectRequireStatusType(RequireStatusType.PROCESS);
        searchQueryVO.setSelectReadRecordType(null);

        // 預設自己單位 
        List<String> canViewDepSids = wkUserAndOrgLogic.prepareCanViewDepSidsBaseOnMinsterial(
                SecurityFacade.getUserSid(), false).stream()
                .map(depsid -> depsid + "")
                .collect(Collectors.toList());

        // 填單單位
        searchQueryVO.setCreateOnpgDepts(Lists.newArrayList(canViewDepSids));
        // 通知部門
        searchQueryVO.setOnpgNoticeDepts(Lists.newArrayList(canViewDepSids));

        // 第二排條件設定
        searchQueryVO.setReadReason(WaitReadReasonTypeBean.getOnpgValues());
        searchQueryVO.setOnPgStatus(this.getOnPgStatus());
        searchQueryVO.setDateTypeIndex(ReportType.WORK_ON_PG.getDateType().ordinal());
        searchQueryVO.setOnpgStartDt(new Date());
        searchQueryVO.setOnpgEndDt(new Date());
        searchQueryVO.setReplyUsers(Lists.newArrayList());

        // 第三排
        // 填單人員
        searchQueryVO.setOpCreatedUsrName("");
        // ON程式主題
        searchQueryVO.setOnpgTheme("");
        // 模糊查詢
        searchQueryVO.setFuzzyText("");

    }

    private List<String> getOnPgStatus() {
        List<String> defOnPgStatus = Lists.newArrayList();
        for (WorkOnpgStatus s : WorkOnpgStatus.values()) {
            defOnPgStatus.add(s.name());
        }
        return defOnPgStatus;
    }

    /**
     * 取得日期區間
     *
     * @param dateType
     */
    public void getDateInterval(String dateType) {
        // 改為由前端處理
        // if (DateType.PREVIOUS_MONTH.name().equals(dateType)) {
        // LocalDate lastDate = new LocalDate(new Date()).minusMonths(1);
        // System.out.println("lastDate:[" + lastDate + "]");
        // queryVo.setOnpgStartDt(lastDate.dayOfMonth().withMinimumValue().toDate());
        // queryVo.setOnpgEndDt(lastDate.dayOfMonth().withMaximumValue().toDate());
        // queryVo.setDateTypeIndex(DateType.PREVIOUS_MONTH.ordinal());
        // } else if (DateType.THIS_MONTH.name().equals(dateType)) {
        // queryVo.setOnpgStartDt(new LocalDate().dayOfMonth().withMinimumValue().toDate());
        // queryVo.setOnpgEndDt(new LocalDate().dayOfMonth().withMaximumValue().toDate());
        // queryVo.setDateTypeIndex(DateType.THIS_MONTH.ordinal());
        // } else if (DateType.NEXT_MONTH.name().equals(dateType)) {
        // if (queryVo.getOnpgStartDt() == null) {
        // queryVo.setOnpgStartDt(new Date());
        // }
        // LocalDate nextDate = new LocalDate(new Date()).plusMonths(1);
        // queryVo.setOnpgStartDt(nextDate.dayOfMonth().withMinimumValue().toDate());
        // queryVo.setOnpgEndDt(nextDate.dayOfMonth().withMaximumValue().toDate());
        // queryVo.setDateTypeIndex(DateType.NEXT_MONTH.ordinal());
        // } else if (DateType.TODAY.name().equals(dateType)) {
        // queryVo.setOnpgStartDt(new Date());
        // queryVo.setOnpgEndDt(new Date());
        // queryVo.setDateTypeIndex(DateType.TODAY.ordinal());
        // } else if (DateType.NULL.name().equals(dateType)) {
        // queryVo.setOnpgStartDt(null);
        // queryVo.setOnpgEndDt(null);
        // queryVo.setDateTypeIndex(DateType.NULL.ordinal());
        // }
    }

    public void loadDefaultSettingDialog() {
        initSearchQueryVODefautSetting(customerQueryVo);
        loadSettingData(customerQueryVo);
    }

    public void clearDateType() {
        if ("4".equals(customerQueryVo.getDateTypeIndexStr())) {
            customerQueryVo.setDateTypeIndexStr("");
        }
    }

    /**
     * 取得 廳主選項
     */
    private void initCustomerItems() {
        customerItems = Lists.newArrayList();
        customerService.findByEnable(EnableType.ENABLE).forEach(each -> {
            customerItems.add(new CustomerTo(each.getSid(), each.getAlias(), each.getName(), each.getLoginCode(),
                    CommonBean.getInstance().get(each.getCustomerType())));
        });
    }

    /**
     * 取得customers中sid
     *
     * @return
     */
    public List<Long> getCustomerSids() { return this.customers.stream().map(CustomerTo::getSid).collect(Collectors.toList()); }

    public String createNoticeRegexpParam() {
        return queryVo.getOnpgNoticeDepts().stream().map(each -> "\"" + each + "\"").collect(Collectors.joining("|"));
    }

    /**
     * 取得待閱原因字串 List
     *
     * @param types             待閱原因 List
     * @param toEmptyReasonType
     * @return
     */
    public List<String> getWaitReadReasonQueryStr(List<WaitReadReasonType> types, WaitReadReasonType toEmptyReasonType) {
        if (types == null) {
            return Lists.newArrayList();
        }
        List<String> names = Lists.newArrayList();
        types.stream().forEach(type -> {
            if (type.equals(toEmptyReasonType)) {
                names.add("");
            } else {
                names.add(type.name());
            }
        });
        return names;
    }

    /**
     * 建立需求狀態查詢 List
     *
     * @return
     */
    public List<String> createQueryReqStatus() {
        return reqStatusUtils.createQueryReqStatus(queryVo.getSelectRequireStatusType(), this.getAllReqStatus());
    }

    /**
     * 建立需求狀態查詢 List
     *
     * @return
     */
    public List<String> createQueryAllReqStatus() {
        return this.getAllReqStatus().stream()
                .map(each -> each.name())
                .collect(Collectors.toList());
    }

    /**
     * 取得此報表全部製作進度查詢
     *
     * @return
     */
    private List<RequireStatusType> getAllReqStatus() { return reqStatusUtils.createExcludeStatus(
            Lists.newArrayList(RequireStatusType.DRAFT)); }

    /**
     * 畫面用全部製作進度查詢
     *
     * @return
     */
    public SelectItem[] getReqStatusItems() { return reqStatusUtils.buildItem(this.getAllReqStatus()); }

    /**
     * 回覆人員設定選單 callback
     */
    private final QuickSelectTreeCallback usersPickerCallback = new QuickSelectTreeCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = 1235745896443158354L;

        @SuppressWarnings("unchecked")
        @Override
        public List<Integer> getSelectedDataList() {

            // 取得已設定的人員
            List<Integer> userSids = queryVo.getReplyUsers();

            return Lists.newArrayList(userSids);
        }
    };

    /**
     * 開啟回覆人員選單
     */
    public void openUsersPicker() {

        // ====================================
        // 初始化單位選擇元件
        // ====================================
        try {
            // this.usersPicker = new OrgUserTreeMBean();

            Org comp = WkOrgCache.getInstance().findById(this.compId);
            if (comp == null) {
                messageCallBack.showMessage("公司ID:[" + this.compId + "]找不到對應資料!");
                return;

            }

            this.usersPicker.init(
                    comp.getSid(),
                    this.loginUserSid,
                    usersPickerCallback);

            this.usersPicker.preOpenDlg();

        } catch (Exception e) {
            String message = "人員選單初始化失敗!" + e.getMessage();
            messageCallBack.showMessage(message);
            log.error(message, e);
            return;
        }

        // 開啟 dialog
        display.showPfWidgetVar("search16_usersPicker_dlg");
    }

    /**
     * 回覆人員選擇確認
     */
    public void confirmPicker() {

        try {

            // ====================================
            // 取得選擇內容
            // ====================================
            List<Integer> sids = this.usersPicker.getSelectedDataList();
            this.queryVo.setReplyUsers(sids);

            // ====================================
            // 關閉視窗
            // ====================================
            this.display.hidePfWidgetVar("search16_usersPicker_dlg");

        } catch (Exception e) {
            String message = "回覆人員選擇確認失敗!" + e.getMessage();
            messageCallBack.showMessage(message);
            log.error(message, e);
            return;
        }

    }
}
