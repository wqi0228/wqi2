/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.ReqUnitBpmService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireUnitSignInfo;
import com.cy.tech.request.web.controller.component.reqconfirm.ReqConfirmClientHelper;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.exception.UserMessageException;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 需求單位流程顯示控制
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class RequireBpmMBean implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -3451444741209536536L;
    @Autowired
	transient private RequireService reqService;
	@Autowired
	transient private ReqUnitBpmService reqBpmService;
	@Autowired
	transient private LoginBean loginBean;
	@Autowired
	transient private DisplayController display;

	/** 需求單位主管簽核物件 */
	@Getter
	private RequireUnitSignInfo signInfo;
	/** 流程圖 */
	private List<ProcessTaskBase> tasks;
	@Getter
	private SelectItem[] rollbackItems;
	/** 選擇的退回物件 */
	@Getter
	@Setter
	private Integer rollbackSelectIndex;
	/** 退回理由 */
	@Getter
	@Setter
	private String rollbackComment;

	@PostConstruct
	public void init() {
		signInfo = null;
		tasks = null;
	}

	/**
	 * 取得簽核流程圖
	 *
	 * @param require
	 * @return
	 */
	public List<ProcessTaskBase> findFlow(Require require) {
		if (require == null || !require.getHasReqUnitSign() || require.getReqUnitSign() == null) {
			return tasks;
		}
		if (tasks == null) {
			this.updateFlow(require);
		}
		return tasks;
	}

	/**
	 * 取得技術事業群主管簽核流程當前簽核人員名稱
	 *
	 * @return
	 */
	public String findDefaultSignName() {
		if (signInfo == null) {
			return "";
		}
		return signInfo.getDefaultSignedName();
	}

	public void clickSign(Require01MBean r01MBean, String comment) {
		try {
			// ====================================
			// 需求單位簽名(BPM + 後續流程)
			// ====================================
			reqBpmService.doSign(signInfo.getRequire(), loginBean.getUser(), comment);

			// ====================================
			// 為內部需求時, 跳出是否自動 onpg 確認視窗
			// ====================================
			if (reqService.isPopupConfirmOnpgNotify(signInfo.getRequire()) &&
			        signInfo.getInstanceStatus().equals(InstanceStatus.APPROVED)) {

				// 取得水管圖
				tasks = reqBpmService.findFlowChartByInstanceId(signInfo.getBpmInstanceId(), SecurityFacade.getUserId());
				this.clickByCheckOnpgCompleteNotifyConfirm(r01MBean);
				// 更新簽核資訊
				display.update("req_bpm_onpg_notify_complete_id");
				return;
			}

			r01MBean.reBuildeRequire();

			r01MBean.getBottomTabMBean().resetTabIdx(r01MBean);
			if (signInfo.getInstanceStatus().equals(InstanceStatus.APPROVED)) {
				r01MBean.getTraceMBean().clear();
			}
			r01MBean.getBottomTabMBean().resetTabIdx(r01MBean);
			r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.REQUIT_UNIT_SIGN);
			r01MBean.getTitleBtnMBean().clear();
			this.init();

			display.execute(ReqConfirmClientHelper.prepareClientPanelInitScript(r01MBean.getRequire()));

		} catch (Exception e) {
			log.error(r01MBean.getRequire().getRequireNo() + ":" + loginBean.getUser().getName() + ":簽核失敗..." + e.getMessage(), e);
			this.recoveryView(r01MBean);
			MessagesUtils.showError(e.getMessage());
		}
	}

	public void clickByCheckOnpgCompleteNotifyConfirm(Require01MBean r01MBean) {
		try {
			reqBpmService.updateFlowApproveByReqUnitForInternal(signInfo.getRequire(), loginBean.getUser());

			r01MBean.getBottomTabMBean().resetTabIdx(r01MBean);
			if (signInfo.getInstanceStatus().equals(InstanceStatus.APPROVED)) {
				r01MBean.getTraceMBean().clear();
			}
			r01MBean.getBottomTabMBean().resetTabIdx(r01MBean);
			r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.REQUIT_UNIT_SIGN);
			if (signInfo.getRequire().getMapping().getSmall().getAutoCreateOnpg()) {
				r01MBean.reBuildeByUpdateFaild();
				r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.ONPG);
				display.execute("accPanelSelectTab('op_acc_panel_layer_zero',0)");
			}
			r01MBean.getTitleBtnMBean().clear();
			this.init();
		} catch (Exception e) {
			log.error(r01MBean.getRequire().getRequireNo() + ":" + loginBean.getUser().getName() + ":建立ON程式..." + e.getMessage(), e);
			this.recoveryView(r01MBean);
			MessagesUtils.showError(e.getMessage());
		}
	}

	private void recoveryView(Require01MBean r01MBean) {
		r01MBean.reBuildeRequire();
		r01MBean.getTemplateMBean().initByTable(r01MBean.getRequire());
		r01MBean.getTraceMBean().clear();
		r01MBean.getTitleBtnMBean().clear();
		this.init();
		this.findFlow(r01MBean.getRequire());
	}

	public void clickRecovery(Require01MBean r01MBean) {
		try {
			reqBpmService.doRecovery(signInfo.getRequire(), loginBean.getUser());
			tasks = reqBpmService.findFlowChartByInstanceId(signInfo.getBpmInstanceId(), SecurityFacade.getUserId());
			r01MBean.reBuildeRequire();
			r01MBean.getBottomTabMBean().resetTabIdx(r01MBean);
			r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.REQUIT_UNIT_SIGN);
			r01MBean.getTitleBtnMBean().clear();
			this.init();
		} catch (UserMessageException e) {
		    this.recoveryView(r01MBean);
		    MessagesUtils.show(e);
		    return;
		} catch (Exception e) {
			log.error(r01MBean.getRequire().getRequireNo() + ":" + loginBean.getUser().getName() + ":復原失敗..." + e.getMessage(), e);
			this.recoveryView(r01MBean);
			MessagesUtils.showError(e.getMessage());
			return;
		}
	}

	/**
	 * 執行退回前判斷
	 *
	 * @param require
	 */
	public void doRollBackBefor(Require require) {
		rollbackComment = "";
		List<ProcessTaskBase> copy = Lists.newArrayList(tasks);
		copy.remove(copy.size() - 1);
		rollbackItems = new SelectItem[copy.size()];
		int index = 0;
		for (ProcessTaskBase each : copy) {
			String label = each.getTaskName().contains("申請人") ? "申請人" : each.getRoleName();
			SelectItem item = new SelectItem(tasks.indexOf(each), label);
			rollbackItems[index++] = item;
		}
		display.update("requireRollBackDlgId");
		display.showPfWidgetVar("requireRollBackDlgWv");
	}

	public void clickRollBack(Require01MBean r01MBean) {
		try {
			tasks = reqBpmService.findFlowChartByInstanceId(signInfo.getBpmInstanceId(), SecurityFacade.getUserId());
			reqBpmService.doRollBack(signInfo.getRequire(),
			        loginBean.getUser(),
			        (ProcessTaskHistory) tasks.get(rollbackSelectIndex), rollbackComment);
			r01MBean.reBuildeRequire();
			r01MBean.getBottomTabMBean().resetTabIdx(r01MBean);
			r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.REQUIT_UNIT_SIGN);
			r01MBean.getTitleBtnMBean().clear();
			this.init();
		} catch (Exception e) {
			log.error(r01MBean.getRequire().getRequireNo() + ":" + loginBean.getUser().getName() + ":退回失敗..." + e.getMessage(), e);
			this.recoveryView(r01MBean);
			MessagesUtils.showError(e.getMessage());
		}
	}

	public void clickInvaild(Require01MBean r01MBean) {
		try {
			reqBpmService.doInvaild(r01MBean.getRequire(), loginBean.getUser());
			r01MBean.getBottomTabMBean().resetTabIdx(r01MBean);
			r01MBean.getTraceMBean().clear();
			if (r01MBean.getRequire().getHasReqUnitSign()) {
				tasks = reqBpmService.findFlowChartByInstanceId(signInfo.getBpmInstanceId(), SecurityFacade.getUserId());
				r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.REQUIT_UNIT_SIGN);
			} else {
				r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.TRACE);
			}
			r01MBean.getTitleBtnMBean().clear();
			this.init();
		} catch (Exception e) {
			log.error(r01MBean.getRequire().getRequireNo() + ":" + loginBean.getUser().getName() + ":作廢失敗..." + e.getMessage(), e);
			this.recoveryView(r01MBean);
			MessagesUtils.showError(e.getMessage());
		}
	}

	public void clearTaskValue() {
		this.tasks.clear();
	}

	public void updateFlow(Require require) {
		if (require == null || !require.getHasReqUnitSign() || require.getReqUnitSign() == null) {
			return;
		}
		signInfo = require.getReqUnitSign();
		// signInfo.setRequire(require);
		tasks = reqBpmService.findFlowChartByInstanceId(signInfo.getBpmInstanceId(), SecurityFacade.getUserId());
	}

	public void updateBpmStatus(Require require, InstanceStatus status) {
		try {
			reqBpmService.updateSignInfo(require, status);
		} catch (ProcessRestException e) {
			log.error(e.getMessage(), e);

		}

	}

}
