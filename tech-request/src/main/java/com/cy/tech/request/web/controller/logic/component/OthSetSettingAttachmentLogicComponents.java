/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.logic.component;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.tros.TrOsAttachmentService;
import com.cy.tech.request.logic.utils.ToolsDate;
import com.cy.tech.request.logic.vo.AttachmentVO;
import com.cy.tech.request.logic.vo.SimpleDateFormatEnum;
import com.cy.tech.request.vo.enums.OthSetAttachmentBehavior;
import com.cy.tech.request.vo.require.tros.TrOsAttachment;
import com.cy.tech.request.vo.require.tros.TrOsHistory;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * 其他設定資訊邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class OthSetSettingAttachmentLogicComponents implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4552445299943189995L;
    private static OthSetSettingAttachmentLogicComponents instance;
    @Autowired
    private TrOsAttachmentService trOsAttachmentService;

    public static OthSetSettingAttachmentLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        OthSetSettingAttachmentLogicComponents.instance = this;
    }

    /**
     * 刪除其他設定資訊附件
     *
     * @param attSid 附件Sid
     * @param loginUserSid 刪除者Sid
     */
    public void deleteAtt(String att_sid, Integer update_userSid) {
        trOsAttachmentService.deleteTrOsAttachment(att_sid, update_userSid);
    }

    /**
     * 刪除其他設定資訊相關附件(回覆,回覆的回覆,取消,完成)
     *
     * @param attSid 附件Sid
     * @param loginUserSid 刪除者Sid
     * @return
     */
    public TrOsHistory deleteHistoryAtt(String att_sid, Integer update_userSid) {
        return trOsAttachmentService.deleteTrOSHistoryAttachment(att_sid, update_userSid);
    }

    /**
     * 取得AttachmentVO By Sid
     *
     * @param sid TrOsAttachmentSid
     * @return
     */
    public AttachmentVO getAttachmentVOByAttachment(String att_sid) {
        TrOsAttachment ta = trOsAttachmentService.findBySid(att_sid);
        AttachmentVO av = new AttachmentVO(ta.getSid(), ta.getFile_name(), ta.getDescription(), ta.getFile_name(), true, String.valueOf(ta.getCreate_usr()),
                ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashSlide.getValue(), ta.getCreate_dt())
        );
        try {
            User user = WkUserCache.getInstance().findBySid(ta.getCreate_usr());
            av.setCreateUserSId(String.valueOf(user.getSid()));
            Org dep = WkOrgCache.getInstance().findBySid(user.getPrimaryOrg().getSid());
            
            //String userInfo = orgLogicComponents.showParentDep(dep) + "-" + user.getName();
            av.setUserInfo(WkOrgUtils.getOrgName(dep) + "-" + user.getName());
            av.setShowEditBtn(true);
            av.setCreateTime(ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS_slide.getValue(), ta.getCreate_dt()));
            av.setParamCreateTime(ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS_slide.getValue(), ta.getCreate_dt()));
        } catch (Exception e) {
            log.error("transWRAttachmentToAttachmentVO", e);
        }
        return av;
    }

    /**
     * 更新附件描述
     *
     * @param att_sid 附件Sid
     * @param desc 描述
     * @param update_userSid 更新者Sid
     * @return
     */
    public AttachmentVO updateAttDesc(String att_sid, String desc, Integer update_userSid) {
        TrOsAttachment ta = trOsAttachmentService.findBySid(att_sid);
        if (ta == null) {
            return null;
        }
        ta.setDescription(desc);
        TrOsAttachment tros = trOsAttachmentService.updateTrOsAttachment(ta, update_userSid);
        if (tros == null) {
            return null;
        }
        return new AttachmentVO(tros.getSid(), tros.getFile_name(), tros.getDescription(), tros.getFile_name(), true, String.valueOf(tros.getCreate_usr()),
                ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashSlide.getValue(), tros.getCreate_dt())
        );
    }

    public AttachmentVO createAttachment(String attName, String os_sid, String os_no, String require_sid,
            String require_no, String os_history_sid, Integer lgoinUserSid, Integer loginUserDepSid,
            OthSetAttachmentBehavior othSetAttachmentBehavior) {
        TrOsAttachment trOsAttachment = new TrOsAttachment();
        trOsAttachment.setOs_sid(os_sid);
        trOsAttachment.setOs_no(os_no);
        trOsAttachment.setRequire_sid(require_sid);
        trOsAttachment.setRequire_no(require_no);
        trOsAttachment.setFile_name(attName);
        if (Strings.isNullOrEmpty(os_history_sid)) {
            trOsAttachment.setOs_history_sid(null);
        } else {
            trOsAttachment.setOs_history_sid(os_history_sid);
        }
        trOsAttachment.setDepartment(loginUserDepSid);
        trOsAttachment.setBehavior(othSetAttachmentBehavior.name());
        trOsAttachment = trOsAttachmentService.createTrOsAttachment(trOsAttachment, lgoinUserSid);
        AttachmentVO av = new AttachmentVO(trOsAttachment.getSid(), trOsAttachment.getFile_name(),
                trOsAttachment.getDescription(),
                trOsAttachment.getFile_name(),
                false, String.valueOf(trOsAttachment.getCreate_usr()),
                ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashSlide.getValue(), trOsAttachment.getCreate_dt()));
        try {
            User user = WkUserCache.getInstance().findBySid(trOsAttachment.getCreate_usr());
            av.setCreateUserSId(String.valueOf(user.getSid()));
            Org dep = WkOrgCache.getInstance().findBySid(user.getPrimaryOrg().getSid());
            //String userInfo = orgLogicComponents.showParentDep(dep) + "-" + user.getName();
            av.setUserInfo(WkOrgUtils.getOrgName(dep) + "-" + user.getName());
            av.setCreateTime(ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS_slide.getValue(), trOsAttachment.getCreate_dt()));
            av.setParamCreateTime(ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS_slide.getValue(), trOsAttachment.getCreate_dt()));
        } catch (Exception e) {
            log.error("transWRAttachmentToAttachmentVO", e);
        }
        return av;
    }

    public List<AttachmentVO> getAttachmentVOByOSSidAndHistorySId(String os_sid, String os_history_sid) {
        List<AttachmentVO> attachments = Lists.newArrayList();
        if (Strings.isNullOrEmpty(os_sid) || Strings.isNullOrEmpty(os_history_sid)) {
            return attachments;
        }
        try {
            trOsAttachmentService.getTrOsAttachmentByOSSidAndOsHistorySid(os_sid, os_history_sid).forEach(item -> {
                AttachmentVO av = new AttachmentVO(item.getSid(), item.getFile_name(), item.getDescription(), item.getFile_name(), false,
                        String.valueOf(item.getCreate_usr()), ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashSlide.getValue(), item.getCreate_dt()));
                try {
                    av.setUserInfo(WkUserUtils.prepareUserNameWithDep(item.getCreate_usr(), "-"));
                    av.setCreateTime(ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS_slide.getValue(), item.getCreate_dt()));
                    av.setParamCreateTime(ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS_slide.getValue(), item.getCreate_dt()));
                } catch (Exception e) {
                    log.error("transWRAttachmentToAttachmentVO", e);
                }
                attachments.add(av);
            });
        } catch (Exception e) {
            log.error("getAttachmentVOByOSSid", e);
        }
        return attachments;

    }

    public List<AttachmentVO> getAttachmentVOByOSSid(String os_sid) {
        List<AttachmentVO> attachments = Lists.newArrayList();
        try {
            trOsAttachmentService.getTrOsAttachmentByOSSid(os_sid).forEach(item -> {
                AttachmentVO av = new AttachmentVO(item.getSid(), item.getFile_name(),
                        item.getDescription(), item.getFile_name(), false, String.valueOf(item.getCreate_usr()),
                        ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashSlide.getValue(), item.getCreate_dt()));
                try {
                    User user = WkUserCache.getInstance().findBySid(item.getCreate_usr());
                    av.setCreateUserSId(String.valueOf(user.getSid()));
                    Org dep = WkOrgCache.getInstance().findBySid(user.getPrimaryOrg().getSid());
                    av.setUserInfo(WkOrgUtils.getOrgName(dep) + "-" + user.getName());
                    av.setCreateTime(ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS_slide.getValue(), item.getCreate_dt()));
                    av.setParamCreateTime(ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS_slide.getValue(), item.getCreate_dt()));
                } catch (Exception e) {
                    log.error("transWRAttachmentToAttachmentVO", e);
                }
                attachments.add(av);
            });
        } catch (Exception e) {
            log.error("getAttachmentVOByOSSid", e);
        }
        return attachments;
    }

}
