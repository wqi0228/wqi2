/**
 * 
 */
package com.cy.tech.request.web.controller.setting.setting10;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;

import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.value.to.ReadRecordGroupTo;
import com.cy.work.common.vo.value.to.ReadRecordHistoryTo;
import com.cy.work.common.vo.value.to.ReadRecordTo;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.HikariPoolMXBean;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Slf4j
@Controller
public class Set10TrnsOldReadRecordMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1290200576081258798L;

    // ========================================================================
    // 服務區
    // ========================================================================
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private JdbcTemplate jdbcTemplate;

    // ========================================================================
    // 方法區
    // ========================================================================
    public void processRequire() {
        this.process(FormType.REQUIRE);
        WkCommonUtils.memonyMonitor();
    }

    public void processPt() {
        this.process(FormType.PTCHECK);
        WkCommonUtils.memonyMonitor();
    }

    public void processTest() {
        this.process(FormType.WORKTESTSIGNINFO);
        WkCommonUtils.memonyMonitor();
    }

    public void processOnpg() {
        this.process(FormType.WORKONPG);
        WkCommonUtils.memonyMonitor();
    }

    private void process(FormType formType) {

        HikariDataSource hikariDs = (HikariDataSource) WorkSpringContextHolder.getBean("dataSource");
        HikariPoolMXBean poolBean = hikariDs.getHikariPoolMXBean();
        poolBean.softEvictConnections();

        Long startTime = System.currentTimeMillis();
        // ====================================
        // 查詢待轉檔
        // ====================================
        StringBuffer varname1 = new StringBuffer();
        varname1.append("SELECT " + formType.getReadRecordFormSidColumnName() + " AS sid, ");
        varname1.append("       read_record AS readRecord ");
        varname1.append("FROM   " + formType.getMainTableName() + "  ");
        varname1.append("WHERE  trns_flag_for_read_record = 0 ");
        varname1.append("ORDER  BY create_dt DESC ");
        varname1.append("LIMIT  20000;");

        List<Map<String, Object>> dataMaps = this.jdbcTemplate.queryForList(varname1.toString());
        if (WkStringUtils.isEmpty(dataMaps)) {
            MessagesUtils.showInfo("已無待轉檔資料");
            return;
        }

        log.debug("查詢:[{}]筆", dataMaps.size());

        // ====================================
        // 組SQL
        // ====================================
        List<List<Map<String, Object>>> dataParts = WkCommonUtils.averageAssign(dataMaps, 2000);

        for (List<Map<String, Object>> currDataMaps : dataParts) {
            this.process(formType, currDataMaps);
        }
        String message = WkCommonUtils.prepareCostMessage(startTime, "已轉檔" + dataMaps.size() + "筆");
        log.info(message);
        MessagesUtils.showInfo(message);

    }

    private void process(
            FormType formType,
            List<Map<String, Object>> dataMaps) {

        // 預計 update 已轉的 sid
        Set<String> transedSids = Sets.newHashSet();

        // 預計 delete 新閱讀記錄的 sid
        Set<String> deleteSids = Sets.newHashSet();

        List<String> insertValues = Lists.newArrayList();

        List<ReadRecordType> waitReadTypes = Lists.newArrayList(
                ReadRecordType.UN_READ,
                ReadRecordType.WAIT_READ);

        for (Map<String, Object> dataMap : dataMaps) {

            // -----------------
            // SID
            // -----------------
            if (dataMap.get("sid") == null) {
                continue;
            }
            String sid = dataMap.get("sid") + "";
            transedSids.add(sid);

            // -----------------
            // 取得閱讀記錄
            // -----------------
            String readRecordJsonStr = dataMap.get("readRecord") + "";
            ReadRecordGroupTo readRecordGroupTo = null;
            try {
                readRecordGroupTo = WkJsonUtils.getInstance()
                        .fromJson(readRecordJsonStr, ReadRecordGroupTo.class);
            } catch (IOException ex) {
                log.error("parser json to ReadRecordGroupTo fail!!" + ex.getMessage());
            }
            // 為空時，
            // 1.無需轉為新閱讀記錄
            // 2.無需先行清空新閱讀記錄檔
            if (readRecordGroupTo == null
                    || WkStringUtils.isEmpty(readRecordGroupTo.getRecords())) {
                continue;
            }

            for (ReadRecordTo readRecordTo : readRecordGroupTo.getRecords().values()) {

                Date readDay = readRecordTo.getReadDay();
                if (readDay == null && WkStringUtils.notEmpty(readRecordTo.getHistory())) {
                    for (ReadRecordHistoryTo readRecordHistoryTo : readRecordTo.getHistory()) {
                        if (readRecordHistoryTo.getReadDay() != null) {
                            readDay = readRecordHistoryTo.getReadDay();
                            break;
                        }
                    }
                }

                String insertValue = String.format("('%s',%s,%s,%s)",
                        sid,
                        readRecordTo.getReader(),
                        waitReadTypes.contains(readRecordTo.getType()) ? "'Y'" : "'N'",
                        readDay == null ? "NULL"
                                : "'" + WkDateUtils.formatDate(readDay, WkDateUtils.YYYY_MM_DD_HH24_mm_ss2) + "'");

                insertValues.add(insertValue);
            }

            deleteSids.add(sid);
        }

        // ====================================
        // 兜組執行SQL
        // ====================================
        String deleteSql = "";
        if (WkStringUtils.notEmpty(deleteSids)) {
            String prefix = ""
                    + "DELETE FROM " + formType.getReadRecordTableName() + " "
                    + "WHERE " + formType.getReadRecordFormSidColumnName() + " IN ('";
            deleteSql = deleteSids.stream()
                    .collect(Collectors.joining("','", prefix, "');"));

            this.jdbcTemplate.execute(deleteSql);
        }

        String insertSql = "";
        if (WkStringUtils.notEmpty(insertValues)) {
            String prefix = ""
                    + "INSERT INTO " + formType.getReadRecordTableName() + " "
                    + " (" + formType.getReadRecordFormSidColumnName() + ", user_sid, wait_read, read_dt) VALUES ";
            insertSql = insertValues.stream()
                    .collect(Collectors.joining(",", prefix, ";"));           

            this.jdbcTemplate.execute(insertSql);
        }

        String updateSql = "";
        if (WkStringUtils.notEmpty(transedSids)) {
            String prefix = ""
                    + "UPDATE " + formType.getMainTableName() + " "
                    + "   SET trns_flag_for_read_record = 1 "
                    + " WHERE  " + formType.getReadRecordFormSidColumnName() + " IN ('";
            updateSql = transedSids.stream()
                    .collect(Collectors.joining("','", prefix, "');"));

            this.jdbcTemplate.execute(updateSql);
        }

        // ====================================
        // 執行
        // ====================================

        log.info("閱讀記錄已轉檔[{}]筆 , 主檔:[{}]",
                dataMaps.size() + "",
                formType.getReadRecordTableName());
    }

}
