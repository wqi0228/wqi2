/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require;

import java.io.Serializable;

/**
 * 需求單頁籤載入
 *
 * @author shaun
 */
public interface IRequireBottom extends Serializable {

    /** 初始化頁籤 */
    public void initTabInfo(Require01MBean r01MBean);
}
