package com.cy.tech.request.web.controller.component.assignnotice;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerCallback;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerComponent;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerConfig;
import com.cy.tech.request.web.controller.component.mipker.helper.MultItemPickerByOrgHelper;
import com.cy.tech.request.web.controller.component.mipker.vo.MultItemPickerShowMode;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 設定分派/通知單位視窗
 * 
 * @author allen1214_wu
 */
@Slf4j
public class AssignNoticeComponent implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7365194009015283746L;

    /**
     * 
     */
    private transient AssignNoticeComponentCallback callback;

    /**
     * 分派單位選擇器
     */
    @Getter
    private transient MultItemPickerComponent assignDepPicker;

    /**
     * 通知單位選擇器
     */
    @Getter
    private transient MultItemPickerComponent noticeDepPicker;

    /**
     * 選擇的檢查項目
     */
    @Getter
    @Setter
    private List<RequireCheckItemType> selectedCheckItemTypes = Lists.newArrayList();

    /**
     * 選擇的檢查項目
     */
    @Getter
    private List<SelectItem> checkItemTypes = Lists.newArrayList();

    /**
     * 建構子
     * 
     * @param callback
     */
    public AssignNoticeComponent(AssignNoticeComponentCallback callback) {
        this.callback = callback;
    }

    /**
     * @param requireSid        需求單 sid
     * @param loadSettingByAuto 取得自動分派/通知單位
     * @throws Exception
     */
    public void openDialog() throws Exception {

        // 選擇器設定資料
        MultItemPickerConfig config = new MultItemPickerConfig();
        config.setTreeModePrefixName("單位");
        
        config.setItemComparator(MultItemPickerByOrgHelper.getInstance().parpareComparator());
        config.setDefaultShowMode(MultItemPickerShowMode.TREE);

        // 初始化檢查項目選項
        this.checkItemTypes = Lists.newArrayList();
        if (this.callback.isCheckConfirm()) {
            this.checkItemTypes = this.callback.prepareCheckItems();
        }

        // 檢查項目初始化完之後，才處理分派/通知單位
        // 分派單位選擇器初始化
        config.setContainFollowing(false);
        this.assignDepPicker = new MultItemPickerComponent(config, assignDepPickerCallback);

        // 通知單位選擇器初始化
        config.setContainFollowing(true);
        this.noticeDepPicker = new MultItemPickerComponent(config, noticeDepPickerCallback);
    }

    /**
     * 取得已選取的分派單位
     * 
     * @return
     */
    public List<Integer> getAssignDepSids() { return this.trnsToInt(this.getAssignDepPicker().getSelectedItems()); }

    /**
     * 設定分派單位
     * 
     * @param selectedItemSids 選擇項目 sid
     */
    public void setAssignDepSids(List<Integer> depSids) {
        if (depSids == null) {
            this.assignDepPicker.setSelectedItemsBySid(Lists.newArrayList());
            return;
        }

        this.assignDepPicker.setSelectedItemsBySid(
                depSids.stream()
                        .map(each -> each + "")
                        .collect(Collectors.toList()));
    }

    /**
     * 設定通知單位
     * 
     * @param selectedItemSids 選擇項目 sid
     */
    public void setNoticeDepSids(List<Integer> depSids) {

        if (depSids == null) {
            this.noticeDepPicker.setSelectedItemsBySid(Lists.newArrayList());
        }

        this.noticeDepPicker.setSelectedItemsBySid(
                depSids.stream()
                        .map(each -> each + "")
                        .collect(Collectors.toList()));

    }

    /**
     * 取得已選取的通知單位
     * 
     * @return
     */
    public List<Integer> getNoticeDepSids() { return this.trnsToInt(this.getNoticeDepPicker().getSelectedItems()); }

    /**
     * @param items
     * @return
     */
    private List<Integer> trnsToInt(List<WkItem> items) {
        if (WkStringUtils.isEmpty(items)) {
            return Lists.newArrayList();
        }
        return items.stream().map(item -> Integer.parseInt(item.getSid())).collect(Collectors.toList());
    }

    /**
     * 確認按鈕
     */
    public void btnConfirm(ActionEvent event) {
        try {
            this.callback.btnConfirm(
                    event,
                    MultItemPickerByOrgHelper.getInstance().itemsToSids(this.assignDepPicker.getTargetVO().getAllItems()),
                    MultItemPickerByOrgHelper.getInstance().itemsToSids(this.noticeDepPicker.getTargetVO().getAllItems()));
        } catch (Exception e) {
            String message = "分派/通知設定視窗處理錯誤! [" + e.getMessage() + "]";
            log.error(message, e);
            MessagesUtils.showError(message);
            return;
        }
    }

    /**
     * 是否為『檢查確認』功能使用
     */
    public boolean isCheckConfirm() { return this.callback.isCheckConfirm(); }

    /**
     * 分派單位
     */
    private final MultItemPickerCallback assignDepPickerCallback = new MultItemPickerCallback() {

        /**
         * 
         */
        private static final long serialVersionUID = 4625775194258778299L;

        @Override
        public List<WkItem> prepareAllItems() throws SystemDevelopException {
            // 取得所有單位
            return MultItemPickerByOrgHelper.getInstance().prepareAllOrgItems();
        }

        @Override
        public List<WkItem> prepareSelectedItems() throws SystemDevelopException {
            // 由外部取回已選擇部門, 並轉換為 WkItem 物件
            List<Integer> deps = callback.prepareAssignSelectedDeps();
            List<WkItem> items = MultItemPickerByOrgHelper.getInstance().createDepItemByDepSids(
                    deps);
            return items;
        }

        /**
         * 
         */
        @Override
        public List<String> prepareDisableItemSids() throws SystemDevelopException {
            // 僅分派單位會有鎖定單位
            // 取得 disable 資料
            List<Integer> deps = callback.prepareAssignDisableDepSids();
            if (WkStringUtils.isEmpty(deps)) {
                return Lists.newArrayList();
            }
            // 轉 string
            return deps.stream()
                    .map(each -> (each + ""))
                    .collect(Collectors.toList());
        }

        @Override
        public List<WkItem> beforTargetShow(List<WkItem> targetItems) {
            return sortTargetItems(targetItems);
        }
    };

    /**
     * 通知單位
     */
    private final MultItemPickerCallback noticeDepPickerCallback = new MultItemPickerCallback() {

        /**
         * 
         */
        private static final long serialVersionUID = 3917585062343328238L;

        @Override
        public List<WkItem> prepareAllItems() throws SystemDevelopException {
            // 取得所有單位
            return MultItemPickerByOrgHelper.getInstance().prepareAllOrgItems();
        }

        @Override
        public List<WkItem> prepareSelectedItems() throws SystemDevelopException {
            // 由外部取回已選擇部門, 並轉換為 Item 物件
            return MultItemPickerByOrgHelper.getInstance().createDepItemByDepSids(
                    callback.prepareNoticeSelectedDeps());
        }

        @Override
        public List<String> prepareDisableItemSids() throws SystemDevelopException {
            // 僅分派單位會有鎖定單位
            return Lists.newArrayList();
        }

        @Override
        public List<WkItem> beforTargetShow(List<WkItem> targetItems) {
            return sortTargetItems(targetItems);
        }
    };

    /**
     * @param targetItems
     * @return
     */
    public List<WkItem> sortTargetItems(List<WkItem> targetItems) {

        // 規則：若被選擇的【組】,沒有上層單位被選擇, 則排在最前面 by jekki

        if (WkStringUtils.isEmpty(targetItems)) {
            return Lists.newArrayList();
        }

        Map<String, WkItem> targetItemsMapBySid = targetItems.stream()
                .distinct()
                .collect(Collectors.toMap(
                        WkItem::getSid,
                        item -> item));

        List<WkItem> newResiltItems = Lists.newArrayList();

        // 將組級部門, 但上層部門未選取者, 抽出放在前面
        for (WkItem item : targetItems) {
            // 取得部門資訊
            Org dep = findOrgBySid(item.getSid());
            if (dep == null) {
                continue;
            }
            // 為組時才處理
            if (!OrgLevel.THE_PANEL.equals(dep.getLevel())) {
                continue;
            }
            // 父部門已在選擇列表時跳過
            if (dep.getParent() != null
                    && targetItemsMapBySid.containsKey(dep.getParent().getSid() + "")) {
                continue;
            }
            newResiltItems.add(item);
        }

        // 未符合規則者, 依序加入
        for (WkItem item : targetItems) {
            if (!newResiltItems.contains(item)) {
                newResiltItems.add(item);
            }
        }

        return newResiltItems;
    }

    private Org findOrgBySid(String orgSid) {
        orgSid = WkStringUtils.safeTrim(orgSid);
        try {
            return WkOrgCache.getInstance().findBySid(Integer.parseInt(orgSid));
        } catch (Exception e) {
            log.warn("解析 orgsid 錯誤 (找不到對應 org):[" + orgSid + "]");
        }
        return null;

    }

    @SuppressWarnings("unchecked")
    public void onCheckItemChange(ValueChangeEvent event) {
        // 異動前
        List<RequireCheckItemType> beforeCheckItemTypes = Lists.newArrayList();
        if (event.getOldValue() != null) {
            beforeCheckItemTypes = (List<RequireCheckItemType>) event.getOldValue();
        }

        // 異動後
        List<RequireCheckItemType> afterCheckItemTypes = Lists.newArrayList();
        if (event.getNewValue() != null) {
            afterCheckItemTypes = (List<RequireCheckItemType>) event.getNewValue();
        }

        // 差異 = 異動後 - 異動前
        List<RequireCheckItemType> addCheckItemTypes = afterCheckItemTypes.stream().collect(Collectors.toList());
        addCheckItemTypes.removeAll(beforeCheckItemTypes);

        RequireCheckItemType checkItemType = null;
        if (WkStringUtils.notEmpty(addCheckItemTypes)) {
            checkItemType = addCheckItemTypes.get(0);
        }

        this.callback.onCheckItemSelected(checkItemType);
    }
}
