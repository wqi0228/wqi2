/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.vo;

import com.cy.tech.request.logic.vo.AttachmentVO;
import com.cy.tech.request.web.attachment.TrOsAttachMaintainCompant;
import com.cy.tech.request.web.listener.AttachMaintainCallBack;
import com.cy.tech.request.web.listener.ReplyCallBack;
import com.cy.tech.request.web.listener.ReplyCallBackCondition;
import com.cy.tech.request.web.listener.TabLoadCallBack;
import com.cy.tech.request.web.listener.UploadAttCallBack;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * 其他設定資訊介面物件
 *
 * @author brain0925_liao
 */
public class OthSetSettingVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1903155620154275377L;

    public OthSetSettingVO(
            String sid,
            String info,
            Integer createUserDepSid,
            String createUserDepName,
            Integer createUserSid,
            String createUserName,
            String createTime,
            String processStatus,
            boolean showCancelTime,
            String cancelTime,
            boolean showFinishTime,
            String finishTime,
            String title,
            String content,
            String remark,
            List<String> notifyDepNames,
            String showNotifiyDepsTitle,
            List<AttachmentVO> attachmentVOs,
            List<HistoryVO> historyVOs,
            ReplyCallBack replyCallBack
    ) {
        this.sid = sid;
        this.info = info;
        this.createUserDepSid = createUserDepSid;
        this.createUserDepName = createUserDepName;
        this.createUserSid = createUserSid;
        this.createUserName = createUserName;
        this.createTime = createTime;
        this.processStatus = processStatus;
        this.showCancelTime = showCancelTime;
        this.cancelTime = cancelTime;
        this.showFinishTime = showFinishTime;
        this.finishTime = finishTime;
        this.title = title;
        this.content = content;
        this.remark = remark;
        this.notifyDepNames = notifyDepNames;
        this.showNotifiyDepsTitle = showNotifiyDepsTitle;
        this.attachmentVOs = attachmentVOs;
        this.historyVOs = historyVOs;
        this.replyCallBack = replyCallBack;
    }

    public void addReply() {
        ReplyCallBackCondition rc = new ReplyCallBackCondition();
        rc.setOs_sid(os_sid);
        rc.setOs_no(os_no);
        rc.setRequire_no(require_no);
        rc.setRequire_sid(require_sid);
        replyCallBack.addReply(rc);
    }

    public void addCancel() {
        ReplyCallBackCondition rc = new ReplyCallBackCondition();
        rc.setOs_sid(os_sid);
        rc.setOs_no(os_no);
        rc.setRequire_no(require_no);
        rc.setRequire_sid(require_sid);
        replyCallBack.addCancel(rc);
    }

    public void addFinish() {
        ReplyCallBackCondition rc = new ReplyCallBackCondition();
        rc.setOs_sid(os_sid);
        rc.setOs_no(os_no);
        rc.setRequire_no(require_no);
        rc.setRequire_sid(require_sid);
        replyCallBack.addFinish(rc);
    }

    public void loadAttachmentCompant(String os_sid, String os_no, String require_sid,
            String require_no, String os_history_sid, Integer lgoinUserSid, Integer loginUserDepSid, UploadAttCallBack uploadAttCallBack, TabLoadCallBack tabLoadCallBack, AttachMaintainCallBack attachMaintainCallBack) {
        this.trOsAttachMaintainCompant = new TrOsAttachMaintainCompant(lgoinUserSid, os_sid, attachMaintainCallBack);
        this.trOsAttachMaintainCompant.loadData(attachmentVOs, uploadAttCallBack, tabLoadCallBack);
        this.os_sid = os_sid;
        this.os_no = os_no;
        this.require_sid = require_sid;
        this.require_no = require_no;
        this.os_history_sid = os_history_sid;
    }

    public void loadButton(boolean disableEdit, boolean disableFinish,
            boolean disableCancel, boolean disableReply, boolean showExpandAndCompressBtn) {
        this.disableEdit = disableEdit;
        this.disableFinish = disableFinish;
        this.disableCancel = disableCancel;
        this.disableReply = disableReply;
        this.showExpandAndCompressBtn = showExpandAndCompressBtn;
    }
    @Getter
    private boolean disableEdit = true;
    @Getter
    private boolean disableFinish = true;
    @Getter
    private boolean disableCancel = true;
    @Getter
    private boolean disableReply = true;
    @Getter
    private boolean showExpandAndCompressBtn = false;
    @Getter
    private String os_sid;
    @Getter
    private String os_no;
    @Getter
    private String require_sid;
    @Getter
    private String require_no;
    @Getter
    private String os_history_sid;

    @Getter
    private TrOsAttachMaintainCompant trOsAttachMaintainCompant;
    @Getter
    private String sid;
    /** 其它設定資訊 */
    @Getter
    @Setter
    private String info;
    /** 建立者部門Sid */
    @Getter
    @Setter
    private Integer createUserDepSid;
    /** 建立者部門名稱 */
    @Getter
    @Setter
    private String createUserDepName;
    /** 建立者Sid */
    @Getter
    @Setter
    private Integer createUserSid;
    /** 建立者名稱 */
    @Getter
    @Setter
    private String createUserName;
    /** 建立時間 */
    @Getter
    @Setter
    private String createTime;
    /** 其它設定資訊狀態 */
    @Getter
    private String processStatus;
    @Getter
    private boolean showCancelTime;
    @Getter
    private String cancelTime;
    @Getter
    private boolean showFinishTime;
    @Getter
    private String finishTime;
    /** 主題 */
    @Getter
    @Setter
    private String title;
    /** 內容 */
    @Getter
    @Setter
    private String content;
    /** 備註 */
    @Getter
    @Setter
    private String remark;
    /** 通知單位 */
    @Getter
    private List<String> notifyDepNames;
    @Getter
    private String showNotifiyDepsTitle;
    /** 附件 */
    @Getter
    private List<AttachmentVO> attachmentVOs;
    /** 回覆 */
    @Getter
    private List<HistoryVO> historyVOs;

    private ReplyCallBack replyCallBack;

    public void updateHistory(List<HistoryVO> historyVOs) {
        this.historyVOs = historyVOs;
    }

}
