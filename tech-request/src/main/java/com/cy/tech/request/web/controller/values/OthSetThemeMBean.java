/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.values;

import com.cy.tech.request.logic.service.othset.OthSetThemeService;
import com.cy.tech.request.vo.require.othset.OthSetTheme;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author brain0925_liao
 */
@Controller
@Scope(WebApplicationContext.SCOPE_APPLICATION)
public class OthSetThemeMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -358103819804317142L;
    @Autowired
    transient private OthSetThemeService othSetThemeService;

    public List<OthSetTheme> getOthSetThemes() {
        return othSetThemeService.findAll();
    }

}
