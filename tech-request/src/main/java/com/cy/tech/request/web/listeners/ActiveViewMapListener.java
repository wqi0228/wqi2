package com.cy.tech.request.web.listeners;

import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.PostConstructViewMapEvent;
import javax.faces.event.SystemEvent;
import javax.faces.event.ViewMapListener;

/**
 * 有效的ViewScope 判斷
 *
 * @author kasim
 */
@Slf4j
public class ActiveViewMapListener implements ViewMapListener {

    /** 有效的ViewScope */
    public static final String ACTIVE_VIEW_MAPS = "com.sun.faces.application.view.activeViewMaps";
    /** 有效的ViewScope ID */
    public static final String VIEW_MAP_ID = "com.sun.faces.application.view.viewMapId";

    @Override
    public void processEvent(SystemEvent se) throws AbortProcessingException {
        if (se instanceof PostConstructViewMapEvent) {
            this.checkViewScopedMap();
        }
    }

    /**
     * 檢查 activeViewMaps 是否產生雙份key
     */
    public void checkViewScopedMap() {
        String viewMapId = this.getViewMapId();
        if (viewMapId == null) {
            log.info("無效的 viewMapId");
            return;
        }
        Map<String, Object> viewMaps = this.getViewMaps();
        log.info("ViewScoped 數量 => " + viewMaps.size());
        if (!viewMaps.containsKey(viewMapId)) {
            log.info("viewMapId 不存在 不進行判斷");
            return;
        }
        log.info("當前 viewMapId：" + viewMapId);
        Map<String, Object> tempMap1 = Maps.newLinkedHashMap(viewMaps);
        Object viewMap = viewMaps.get(viewMapId);
        tempMap1.keySet().forEach(each -> {
            if (viewMap.equals(viewMaps.get(each)) && !each.equals(viewMapId)) {
                log.info("刪除重複 viewMap：" + each);
                viewMaps.remove(each);
            }
        });
    }

    /**
     * 取得 當前頁面 viewMap ID
     *
     * @return
     */
    private String getViewMapId() {
        UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
        Object viewMapId = viewRoot.getTransientStateHelper().getTransient(VIEW_MAP_ID);
        if (viewMapId == null) {
            return null;
        }
        return (String) viewMapId;
    }

    /**
     * 取得viewMap
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    private Map<String, Object> getViewMaps() {
        Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        return (Map<String, Object>) sessionMap.get(ACTIVE_VIEW_MAPS);
    }

    @Override
    public boolean isListenerForSource(Object o) {
        return o instanceof UIViewRoot;
    }

}
