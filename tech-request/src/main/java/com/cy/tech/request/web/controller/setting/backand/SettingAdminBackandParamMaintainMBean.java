package com.cy.tech.request.web.controller.setting.backand;

import java.util.List;
import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.setting.SettingAdminBackandParamMaintainService;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerCallback;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerComponent;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerConfig;
import com.cy.tech.request.web.controller.component.mipker.helper.MultItemPickerByOrgHelper;
import com.cy.tech.request.web.controller.component.mipker.vo.MultItemPickerShowMode;
import com.cy.tech.request.web.controller.component.qkstree.QuickSelectTreeCallback;
import com.cy.tech.request.web.controller.component.qkstree.impl.OrgUserTreeMBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.backend.vo.WorkBackendParamVO;
import com.cy.work.backend.vo.enums.WkBackendParamValueType;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.vo.WkItem;
import com.cy.work.common.vo.converter.SplitListIntConverter;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 參數後台維護 MBean
 * 
 * @author allen1214_wu
 */
@Slf4j
@Controller
@Scope("view")
public class SettingAdminBackandParamMaintainMBean {

	// ========================================================================
	// 服務
	// ========================================================================
	@Autowired
	private transient SettingAdminBackandParamMaintainService settingAdminBackandParamMaintainService;

	@Autowired
	private transient MultItemPickerByOrgHelper multItemPickerByOrgHelper;

	@Autowired
	private transient DisplayController displayController;

	// ========================================================================
	// view 變數
	// ========================================================================
	@Getter
	public List<WorkBackendParamVO> params;

	@Getter
	@Setter
	public WorkBackendParamVO voForTextEdit;

	/**
	 * 可檢查部門選擇器
	 */
	@Getter
	private transient MultItemPickerComponent depsPicker;
	/**
	 * 人員選擇器
	 */
	@Autowired
	private transient OrgUserTreeMBean userQSTreeMBean;

	// ========================================================================
	// 方法區
	// ========================================================================
	/**
	 * 初始化
	 */
	@PostConstruct
	public void init() {
		this.reloadData();
	}

	/**
	 * 文字編輯：開啟對話框
	 * 
	 * @param workBackendParamVO
	 */
	public void openDialog(WorkBackendParamVO workBackendParamVO) {
		// ====================================
		// 記錄編輯的資料
		// ====================================
		if (workBackendParamVO == null) {
			MessagesUtils.showError("找不到編輯的資料");
			log.error("找不到編輯的資料:selectedParam 為空");
			return;
		}

		this.voForTextEdit = new WorkBackendParamVO(
		        workBackendParamVO.getSid(),
		        workBackendParamVO.getKeyword(),
		        workBackendParamVO.getContent());

		WkBackendParamValueType valueType = workBackendParamVO.getKeyword().getWkBackendParamValueType();

		// ====================================
		// 初始化對話框元件
		// ====================================
		// -----------
		// 部門選單
		// -----------
		if (WkBackendParamValueType.DEP_SID.equals(valueType) 
				|| WkBackendParamValueType.DEP_SID_INCLUDE.equals(valueType)) {
			try {
				// 選擇器設定資料
				MultItemPickerConfig config = new MultItemPickerConfig();
				config.setDefaultShowMode(MultItemPickerShowMode.TREE);
				config.setTreeModePrefixName("單位");
				config.setContainFollowing(true);
				config.setItemComparator(this.multItemPickerByOrgHelper.parpareComparator());

				// 選擇器初始化
				this.depsPicker = new MultItemPickerComponent(config, depsMultItemPickerCallback);

			} catch (Exception e) {
				String message = "可檢查單位選單初始化失敗!" + e.getMessage();
				MessagesUtils.showError(message);
				log.error(message, e);
				return;
			}

		}
		// -----------
		// 人員選單
		// -----------
		else if (WkBackendParamValueType.USER_SID.equals(valueType)) {
			try {

				Org comp = WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId());
				if (comp == null) {
					MessagesUtils.showError("公司ID:[" + SecurityFacade.getCompanyId() + "]找不到對應資料!");
					return;
				}
				this.userQSTreeMBean.init(
				        comp.getSid(),
				        SecurityFacade.getUserSid(),
				        userQSTreeCallback);

				this.userQSTreeMBean.preOpenDlg();

			} catch (Exception e) {
				String message = "可檢查人員選單初始化失敗!" + e.getMessage();
				MessagesUtils.showError(message);
				log.error(message, e);
				return;
			}
		}

		// ====================================
		// 開啟對應視窗
		// ====================================
		this.displayController.showPfWidgetVar(this.getDlgNameByType(valueType));
	}

	/**
	 * 文字編輯：確認
	 */
	public void editText_confirm() {
		save();
	}

	/**
	 * 部門編輯：確認
	 */
	public void editDeps_confirm() {

		// ====================================
		// 取得選擇單位
		// ====================================
		List<Integer> depSids = this.multItemPickerByOrgHelper.itemsToSids(
		        this.depsPicker.getSelectedItems());

		// ====================================
		// 轉型後放入 vo
		// ====================================
		this.voForTextEdit.setContent(new SplitListIntConverter().convertToDatabaseColumn(depSids));

		// ====================================
		// save
		// ====================================
		this.save();
	}

	/**
	 * 人員編輯：確認
	 */
	public void editusers_confirm() {

		// ====================================
		// 取得選擇人員
		// ====================================
		List<Integer> userSids = this.userQSTreeMBean.getSelectedDataList();

		// ====================================
		// 轉型後放入 vo
		// ====================================
		this.voForTextEdit.setContent(new SplitListIntConverter().convertToDatabaseColumn(userSids));

		// ====================================
		// save
		// ====================================
		this.save();
	}

	/**
	 * 
	 */
	private void save() {

		// ====================================
		// save
		// ====================================
		try {
			String message = this.settingAdminBackandParamMaintainService.saveByVO(
					this.voForTextEdit, 
					SecurityFacade.getUserSid());
			
			MessagesUtils.showInfo(message);
		} catch (UserMessageException e) {
			MessagesUtils.show(e);
			log.error(e.getMessage(), e);
			return;
		} catch (Exception e) {
			MessagesUtils.showError(WkMessage.PROCESS_FAILED);
			log.error("儲存系統參數處理失敗！" + e.getMessage(), e);
			return;
		}

		// ====================================
		// 重整畫面
		// ====================================
		// 更新畫面資料
		this.reloadData();
		this.displayController.update("main_panel");
		// 關閉對話框
		this.displayController.hidePfWidgetVar(
				this.getDlgNameByType(
						this.voForTextEdit.getKeyword().getWkBackendParamValueType()));
	}

	private void reloadData() {
		this.params = this.settingAdminBackandParamMaintainService.findAllCanSettingParam();
	}

	/**
	 * 取得對應的對話框名稱
	 * 
	 * @param valueType
	 * @return
	 */
	private String getDlgNameByType(WkBackendParamValueType valueType) {
		if (WkBackendParamValueType.TEXT.equals(valueType)) {
			return "settingAdminBackandParamMaintain_editText_dlg";

		} else if (WkBackendParamValueType.DEP_SID.equals(valueType)) {
			return "settingAdminBackandParamMaintain_editDeps_dlg";

		} else if (WkBackendParamValueType.DEP_SID_INCLUDE.equals(valueType)) {
			return "settingAdminBackandParamMaintain_editDeps_dlg";
			
		} else if (WkBackendParamValueType.USER_SID.equals(valueType)) {
			return "settingAdminBackandParamMaintain_editUsers_dlg";
		}

		throw new SystemDevelopException("沒有實做的類別(WkBackendParamValueType):" + valueType);
	}

	/**
	 * 可執行單位設定選單 callback
	 */
	private final MultItemPickerCallback depsMultItemPickerCallback = new MultItemPickerCallback() {

		/**
         * 
         */
        private static final long serialVersionUID = -6986111961588292900L;

        /**
		 * 取得所有可選擇單位
		 */
		@Override
		public List<WkItem> prepareAllItems() throws SystemDevelopException {
			// 取得所有單位
			return multItemPickerByOrgHelper.prepareAllOrgItems();
		}

		/**
		 * 取得已選擇部門
		 */
		@Override
		public List<WkItem> prepareSelectedItems() throws SystemDevelopException {
			// 取得已設定的部門
			List<Integer> depSids = new SplitListIntConverter()
			        .convertToEntityAttribute(voForTextEdit.getContent());

			// 轉為 WkItem 物件
			return multItemPickerByOrgHelper.prepareWkItemByDepSids(Sets.newHashSet(depSids));
		}

		/**
		 * 準備 disable 的單位
		 */
		@Override
		public List<String> prepareDisableItemSids() throws SystemDevelopException {
			// 不需要 Disable
			return Lists.newArrayList();
		}
	};

	/**
	 * 人員設定選單 callback
	 */
	private final QuickSelectTreeCallback userQSTreeCallback = new QuickSelectTreeCallback() {
		/**
         * 
         */
        private static final long serialVersionUID = 5165882179940552436L;

        @SuppressWarnings("unchecked")
		@Override
		public List<Integer> getSelectedDataList() {
			// 取得已設定的人員
			List<Integer> depSids = new SplitListIntConverter()
			        .convertToEntityAttribute(voForTextEdit.getContent());

			return Lists.newArrayList(Sets.newHashSet(depSids));
		}
	};

}
