/**
 * 
 */
package com.cy.tech.request.web.controller.require;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.AssignSendInfoService;
import com.cy.tech.request.logic.service.RequireCheckItemService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.setting.SettingCheckConfirmRightService;
import com.cy.tech.request.logic.service.setting.SettingDefaultAssignSendDepService;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.require.AssignSendInfo;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireCheckItem;
import com.cy.tech.request.web.pf.utils.ConfirmCallbackDialogController;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.value.to.ItemsCollectionDiffTo;
import com.cy.work.common.vo.value.to.ShowInfo;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@NoArgsConstructor
@Controller
@Scope("view")
@Slf4j
public class RequireCheckItemMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6145910900515210276L;
    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private transient RequireCheckItemService requireCheckItemService;
    @Autowired
    private transient AssignSendInfoService assignSendInfoService;
    @Autowired
    private transient WorkBackendParamHelper workBackendParamHelper;
    @Autowired
    private transient DisplayController displayController;
    @Autowired
    private transient SettingCheckConfirmRightService settingCheckConfirmRightService;
    @Autowired
    private transient ConfirmCallbackDialogController confirmCallbackDialogController;
    @Autowired
    private transient RequireService requireService;
    @Autowired
    private transient SettingDefaultAssignSendDepService settingDefaultAssignSendDepService;
    

    // ========================================================================
    // 變數
    // ========================================================================
    /**
     * 選擇項目
     */
    @Getter
    @Setter
    private List<RequireCheckItemType> selectedCheckItemTypes = Lists.newArrayList();
    /**
     * 前一次的選擇項目
     */
    private List<RequireCheckItemType> preSelectedCheckItemTypes = Lists.newArrayList();

    @Getter
    private final String dialogName = "require_dlg_check_Items";
    @Getter
    private final String dialogContentName = "require_dlg_check_Items_content";

    /**
     * 
     */
    private Require01MBean editRequire01MBean;

    // ========================================================================
    // 方法
    // ========================================================================
    @PostConstruct
    public void init() {
        this.selectedCheckItemTypes = Lists.newArrayList();
        this.preSelectedCheckItemTypes = Lists.newArrayList();
    }

    /**
     * @param requireSid
     */
    public void reload(String requireSid) {
        this.selectedCheckItemTypes = this.requireCheckItemService.findCheckItemTypesByRequireSid(requireSid);
        // 複製一份
        this.preSelectedCheckItemTypes = Lists.newArrayList(this.selectedCheckItemTypes);
    }

    /**
     * 未選擇『無』，也未選擇任一項目時，回傳 false
     * 
     * @return true:通過檢查
     */
    public boolean checkMustInput() {

        // 有選擇任一『檢查項目』
        if (WkStringUtils.notEmpty(selectedCheckItemTypes)) {
            return true;
        }

        return false;
    }

    /**
     * 準備畫面顯示資訊
     * 
     * @param require 需求單資料物件
     * @return ShowInfo[text.tooltip]
     */
    public ShowInfo prepareShowInfo(Require require) {
        return requireCheckItemService.prepareSelectedItemTypesInfoByRequireSid(
                require,
                SecurityFacade.getUserSid());
    }

    /**
     * 顯示系統別編輯相關按鈕
     * 
     * @param require01MBean
     * @return
     */
    public boolean isCanEdit(Require01MBean require01MBean) {

        // 可使用調整系統別權限
        // 說明:依據單據製作進度、和使用者身份判斷
        //
        // 使用者身份分為下面三類
        // 1.需求單位: 申請者+立案部門直系主管
        // 2.分派單位: 分派單位 (不含向上主管、向下部門成員， 不含通知單位)
        // 3.GM:GM後台維護 -> GM設定(一般同仁)
        //
        // 可編輯權限如下
        // 1.需求單位:全製作進度可編輯，但檢查時期(待檢查確認、檢查退件)不可編輯，避免被抽掉檢查單位
        // 2.分派單位:製作進度通過『進行中』後可編輯 (作廢單據不可編輯)
        // 3.GM:進入『待檢查確認』之後的流程都可以編輯

        // ====================================
        // 共通判斷邏輯
        // ====================================
        if (require01MBean == null) {
            return false;
        }

        // 單據編輯中不顯單獨編輯鈕
        if (require01MBean.isEditMode()) {
            return false;
        }

        Require require = require01MBean.getRequire();

        if (require == null) {
            return false;
        }

        RequireStatusType requireStatus = require.getRequireStatus();

        if (requireStatus == null) {
            return false;
        }

        // ====================================
        // 依據單據製作進度、和使用者身份判斷
        // ====================================
        // 需求單位
        if (requireStatus.isCanEditCheckItemsByReqDep() // 製作進度判斷
                && this.isEditCheckItemRightByReqDep(require)) { // 登入者身份判斷
            return true;
        }

        // 分派單位
        if (requireStatus.isCanEditCheckItemsByReqAssignDep() // 製作進度判斷
                && this.isEditCheckItemRightByAssignDep(require)) {// 登入者身份判斷
            return true;
        }

        // GM
        if (requireStatus.isCanEditCheckItemsByGM() // 製作進度判斷
                && this.workBackendParamHelper.isGM(SecurityFacade.getUserSid())) {// 登入者身份判斷
            return true;
        }

        // 未符合可編輯狀態
        return false;
    }

    /**
     * 編輯系統別身份判斷:是否為申請人或立案部門直系主管
     * 
     * @param require 需求單主檔
     * @return
     */
    private boolean isEditCheckItemRightByReqDep(Require require) {
        // 判斷為需求者
        User createdUser = require.getCreatedUser();
        if (createdUser == null) {
            return false;
        }
        // 為申請者
        if (WkCommonUtils.compareByStr(createdUser.getSid(), SecurityFacade.getUserSid())) {
            return true;
        }

        // 判斷為立案部門直系主管
        Org createDep = require.getCreateDep();
        if (createDep == null) {
            return false;
        }
        Set<Integer> managerUsers = WkOrgCache.getInstance().findDirectManagerUserSids(createDep.getSid());
        if (managerUsers.contains(SecurityFacade.getUserSid())) {
            return true;
        }

        return false;
    }

    /**
     * 編輯系統別身份判斷:是否為分派單位
     * 
     * @param require
     * @return
     */
    private boolean isEditCheckItemRightByAssignDep(Require require) {
        // 取得分派、通知單位
        Map<AssignSendType, AssignSendInfo> settingMap = this.assignSendInfoService.findByRequireSidAndStatus(require.getSid());

        // 取得分派單位資料
        AssignSendInfo assignSendInfo = settingMap.get(AssignSendType.ASSIGN);
        if (assignSendInfo == null) {
            return false;
        }

        // 取得分派單位
        List<Integer> depSids = this.assignSendInfoService.prepareDeps(assignSendInfo);
        // 取得所有分派單位下的成員 (包含兼職主管)
        Set<Integer> depUserSids = WkUserCache.getInstance().findUserSidByOrgsWithManager(depSids, Activation.ACTIVE);

        // 比對是否
        return depUserSids.contains(SecurityFacade.getUserSid());
    }

    // ========================================================================
    // 事件
    // ========================================================================
    /**
     * 事件：點選檢查項目<br/>
     * 功能說明：<br/>
     * 點選『無』時，反勾選其他項目<br/>
     * 點選『一般項目』時，取消勾選『無』
     */
    public void event_changeSelectedCheckItemTypes() {

        // 防呆,應該不會發生
        if (WkStringUtils.isEmpty(this.selectedCheckItemTypes)) {
            return;
        }

        // 差集 : 本次 - 上次 = 新增的項目
        List<RequireCheckItemType> differenceItems = Lists.newArrayList(this.selectedCheckItemTypes);
        if (WkStringUtils.notEmpty(differenceItems)) {
            differenceItems.removeAll(this.preSelectedCheckItemTypes);
        }

        // 未新增選項 (未改變或反勾選) -> pass
        if (WkStringUtils.isEmpty(differenceItems)) {
            return;
        }

        // 新增了『無』的選項時，把其他不是『無』的項目取消勾選
        if (differenceItems.contains(RequireCheckItemType.NONE)) {
            // 僅留下無
            this.selectedCheckItemTypes = Lists.newArrayList(RequireCheckItemType.NONE);
        }

        // 新增了『無』以外的選項時，把『無』取消勾選
        else {
            this.selectedCheckItemTypes.remove(RequireCheckItemType.NONE);
        }

        // 複製一份記錄
        this.preSelectedCheckItemTypes = Lists.newArrayList(this.selectedCheckItemTypes);
    }

    /**
     * 開啟編輯視窗
     */
    public void event_open_editDialog(Require01MBean require01MBean) {
        // ====================================
        // 防呆
        // ====================================
        if (require01MBean == null || require01MBean.getRequire() == null) {
            MessagesUtils.showWarn(WkMessage.SESSION_TIMEOUT);
            return;
        }

        this.editRequire01MBean = require01MBean;

        // ====================================
        // 讀取資料
        // ====================================
        try {
            this.reload(this.editRequire01MBean.getRequire().getSid());
        } catch (Exception e) {
            String message = WkMessage.EXECTION + "[" + e.getMessage() + "]";
            log.error(message, e);
            MessagesUtils.showWarn(message);
            return;
        }

        // ====================================
        // 開啟選單
        // ====================================
        displayController.showPfWidgetVar(dialogName);
    }

    /**
     * 
     */
    public void event_confirm_editDialog() {

        // ====================================
        // 防呆
        // ====================================
        if (this.editRequire01MBean == null || this.editRequire01MBean.getRequire() == null) {
            MessagesUtils.showWarn(WkMessage.NEED_RELOAD);
            return;
        }

        Require require = this.editRequire01MBean.getRequire();

        if (WkStringUtils.isEmpty(this.selectedCheckItemTypes)) {
            MessagesUtils.showWarn("請至少選擇一個項目!");
            return;
        }

        // 異動後的選擇項目 (面選擇項目)
        List<RequireCheckItemType> afterCheckItemTypes = Lists.newArrayList(this.selectedCheckItemTypes);

        // ====================================
        // 取得異動前的檢查項目
        // ====================================
        // 查詢
        List<RequireCheckItem> beforeCheckItems = this.requireCheckItemService.findCheckItemsByRequireSid(require.getSid());

        // 比對是否有異動
        // 轉型
        List<RequireCheckItemType> beforeCheckItemTypes = beforeCheckItems.stream()
                .map(RequireCheckItem::getCheckItemType)
                .collect(Collectors.toList());
        // 比對
        if (WkCommonUtils.compare(beforeCheckItemTypes, afterCheckItemTypes)) {
            MessagesUtils.showWarn("選擇項目未異動!");
            this.displayController.hidePfWidgetVar(dialogName);
            return;
        }

        // ====================================
        // 待檢查確認時特別處理
        // ====================================
        if (RequireStatusType.WAIT_CHECK.equals(require.getRequireStatus())) {
            // 檢查確認提示訊息
            String confirmMessage = this.prepareConfirmMessageWhenWaitConfirm(
                    require,
                    beforeCheckItems,
                    afterCheckItemTypes,
                    SecurityFacade.getUserSid());

            // 有確認訊息時，顯示提示視窗
            if (WkStringUtils.notEmpty(confirmMessage)) {
                // callback 提示訊息確認窗
                confirmCallbackDialogController.showConfimDialog(
                        confirmMessage,
                        Lists.newArrayList(),
                        () -> this.saveWhenWaitCheckConfirm(require.getSid(), afterCheckItemTypes, SecurityFacade.getUserSid(), new Date()));
                // callback 後面不可再放其他流程
                return;
            }

            // 無確認訊息時，直接執行異動檢查項目
            this.saveWhenWaitCheckConfirm(
                    require.getSid(),
                    afterCheckItemTypes,
                    SecurityFacade.getUserSid(),
                    new Date());
        }

        // ====================================
        // 非檢查確認流程，僅編輯系統別項目
        // ====================================
        else {
            this.saveWhenNormal(beforeCheckItems);
        }

    }

    /**
     * @param require
     * @param beforeRequireCheckItems
     * @param afterRequireCheckItemTypes
     * @param execUserSid
     * @return
     */
    private String prepareConfirmMessageWhenWaitConfirm(
            Require require,
            List<RequireCheckItem> beforeRequireCheckItems,
            List<RequireCheckItemType> afterRequireCheckItemTypes,
            Integer execUserSid) {

        // ====================================
        // 防呆：為待檢查確認時才需要處理
        // ====================================
        if (!RequireStatusType.WAIT_CHECK.equals(require.getRequireStatus())) {
            log.error("單據狀態不是『待檢查確認』,跳過異動確認訊息");
            return "";
        }

        // ====================================
        // 資料準備
        // ====================================
        // 已檢查項目
        List<RequireCheckItemType> checkedItemTypes = beforeRequireCheckItems.stream()
                .filter(checkItemType -> checkItemType.getCheckDate() != null)
                .map(RequireCheckItem::getCheckItemType)
                .collect(Collectors.toList());

        // 異動前待檢查項目 (異動前項目 - 已檢查項目)
        List<RequireCheckItemType> beforeWaitCheckItemTypes = Lists.newArrayList(
                beforeRequireCheckItems.stream()
                        .map(RequireCheckItem::getCheckItemType)
                        .collect(Collectors.toList()));
        beforeWaitCheckItemTypes.removeAll(checkedItemTypes);

        // 異動後待檢查項目 (異動後項目 - 已檢查項目)
        List<RequireCheckItemType> afterWaitCheckItemTypes = Lists.newArrayList(afterRequireCheckItemTypes);
        afterWaitCheckItemTypes.removeAll(checkedItemTypes);

        // 執行者可檢查的項目
        List<RequireCheckItemType> execUserCheckRights = this.settingCheckConfirmRightService.findUserCheckRights(execUserSid);

        // 確認訊息
        List<String> confirmMessages = Lists.newArrayList();

        // ====================================
        // 提示：已檢查的項目要被移除
        // ====================================
        // 已檢查項目，不存在於異動後項目
        List<RequireCheckItemType> removeCheckedItemTypes = Lists.newArrayList();
        for (RequireCheckItemType checkedItemType : checkedItemTypes) {
            if (!afterRequireCheckItemTypes.contains(checkedItemType)) {
                removeCheckedItemTypes.add(checkedItemType);
            }
        }

        // 兜組提視訊息
        if (WkStringUtils.notEmpty(removeCheckedItemTypes)) {
            String msg = "已檢查項目【"
                    + removeCheckedItemTypes.stream().map(each -> each.getDescr()).collect(Collectors.joining("】,【"))
                    + "】將被移除!";

            // 上色
            msg = WkHtmlUtils.addRedBlodClass(msg);

            confirmMessages.add(msg);
        }

        // ====================================
        // 提示：獲得 或 喪失檢查權限
        // ====================================
        // 登入者在異動前有待檢查項目
        boolean isBeforChecker = beforeWaitCheckItemTypes.stream()
                .anyMatch(waitCheckItemType -> execUserCheckRights.contains(waitCheckItemType));

        // 登入者在異動後有待檢查項目
        boolean isAfterChecker = afterWaitCheckItemTypes.stream()
                .anyMatch(waitCheckItemType -> execUserCheckRights.contains(waitCheckItemType));

        // 因項目改變後，喪失檢查權限 (本來有『待』檢查項目)
        if (isBeforChecker && !isAfterChecker) {
            confirmMessages.add("您將不再是此單據檢查人員 (無法使用『檢查確認』功能)");
        }

        // 因項目改變後，獲得檢查權限 (本來沒有『待』檢查項目)
        if (!isBeforChecker && isAfterChecker) {
            confirmMessages.add("您將成為單據檢查人員 (獲得『檢查確認』權限)");
        }

        // ====================================
        // 提示：單據狀態將改變
        // ====================================
        if (WkStringUtils.isEmpty(afterWaitCheckItemTypes)) {
            String message = "調整後，此單據所有項目都已檢查完成，需求製作進度【待檢查確認->進行中】";
            // 上色
            message = WkHtmlUtils.addRedBlodClass(message);
            confirmMessages.add(message);
        }

        // ====================================
        // 回傳提示訊息
        // ====================================
        if (WkStringUtils.notEmpty(confirmMessages)) {
            return "<br/>"
                    + String.join("<br/>", confirmMessages)
                    + "<br/><br/>"
                    + "是否確定？";
        }

        return "";
    }

    /**
     * 異動檢查項目 for 檢查確認
     * 
     * @param requireSid          需求單 sid
     * @param afterCheckItemTypes 異動後檢查項目
     * @param execUserSid         執行者 sid
     * @param execDate            執行時間
     */
    private void saveWhenWaitCheckConfirm(
            String requireSid,
            List<RequireCheckItemType> afterCheckItemTypes,
            Integer execUserSid,
            Date execDate) {
        // ====================================
        // 異動系統別 並自動推進進度(如果需要)
        // ====================================
        try {
            this.requireService.executeChangeCheckitemForWaitConform(
                    requireSid,
                    afterCheckItemTypes,
                    execUserSid,
                    execDate);

        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {

            if (e instanceof UserMessageException) {
                MessagesUtils.show((UserMessageException) e);
                return;
            }
            if (e.getCause() instanceof UserMessageException) {
                MessagesUtils.show((UserMessageException) e.getCause());
                return;
            }

            String message = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            MessagesUtils.showError(message);
            return;
        }

        // ====================================
        // 更新畫面
        // ====================================
        // 單據
        if (this.editRequire01MBean.getRequire().getRequireStatus().isShowRequireConfirmPanel()) {
            // 更新單據，並轉向追蹤第一筆
            this.editRequire01MBean.reBuildeAndGotoTraceTab();
        } else {
            // 僅更新畫面
            this.editRequire01MBean.reBuildeByUpdateFaild();
        }

        // ====================================
        // 關閉選單
        // ====================================
        this.displayController.hidePfWidgetVar(dialogName);
    }

    /**
     * 
     */
    private void saveWhenNormal(
            List<RequireCheckItem> beforeCheckItems) {
        // ====================================
        // 異動系統別
        // ====================================
        try {
            this.requireCheckItemService.updateCheckItemTypes(
                    this.editRequire01MBean.getRequire().getSid(),
                    this.selectedCheckItemTypes,
                    this.editRequire01MBean.getRequire().getRequireStatus().isShowRequireConfirmPanel(),
                    SecurityFacade.getUserSid(),
                    new Date());

        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            String message = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            MessagesUtils.showError(message);
            return;
        }

        // ====================================
        // 更新畫面
        // ====================================
        // 單據
        if (this.editRequire01MBean.getRequire().getRequireStatus().isShowRequireConfirmPanel()) {
            // 更新單據，並轉向追蹤第一筆
            this.editRequire01MBean.reBuildeAndGotoTraceTab();
        } else {
            // 僅更新畫面
            this.editRequire01MBean.reBuildeByUpdateFaild();
        }
        
        // ====================================
        // 關閉選單
        // ====================================
        this.displayController.hidePfWidgetVar(dialogName);
    }

    /**
     * 使用者若在GM檢查後異動系統別，需提示新系統別應分派/通知的單位
     * 
     * @param requireSid        主單 sid
     * @param smallCategorySid  主單小類 sid
     * @param beforeCheckItems  異動前系統別
     * @param newCheckItemTypes 異動後系統別
     * @return 提示訊息 (無需提示時，回傳空白)
     */
    @SuppressWarnings("unused")
    private String prepareAssignNoticeConfirmMessage(
            Require require,
            List<RequireCheckItem> beforeCheckItems,
            List<RequireCheckItemType> newCheckItemTypes) {

        // ====================================
        // 防呆
        // ====================================
        // 為空 (應該不可能，前面會擋)
        if (require == null || WkStringUtils.isEmpty(newCheckItemTypes)) {
            return "";
        }

        // ====================================
        // 狀態
        // ====================================
        // 為進行中才提示
        if (RequireStatusType.PROCESS.equals(require.getRequireStatus())) {
            return "";
        }

        // ====================================
        //
        // ====================================
        String requireSid = require.getSid();
        String smallCategorySid = require.getMapping().getSmall().getSid();

        // 比對已分派/通知單位，和目前預設單位的差異
        ItemsCollectionDiffTo<RequireCheckItemType> diffTo = WkCommonUtils.itemCollectionDiff(
                beforeCheckItems.stream().map(RequireCheckItem::getCheckItemType).collect(Collectors.toSet()),
                newCheckItemTypes);

        // 取得『還沒』被分派/通知的預設單位 (預設單位 - 以設定單位)
        Collection<RequireCheckItemType> plusCheckItemTypes = diffTo.getPlusItems();

        if (WkStringUtils.isEmpty(plusCheckItemTypes)) {
            return "";
        }

        // ====================================
        // 取得還未分派的單位
        // ====================================
        String assignDepsMessage = this.prepareNeedAssignNoticeMessage(
                AssignSendType.ASSIGN, requireSid, smallCategorySid, newCheckItemTypes);

        // ====================================
        // 取得還未通知的單位
        // ====================================
        String noticeDepsMessage = this.prepareNeedAssignNoticeMessage(
                AssignSendType.SEND, requireSid, smallCategorySid, newCheckItemTypes);

        // ====================================
        // 組訊息
        // ====================================
        // 沒有需要新增的單位
        if (WkStringUtils.isEmpty(assignDepsMessage)
                && WkStringUtils.isEmpty(noticeDepsMessage)) {
            return "";
        }

        // 組異動系統別訊息
        String message = "";
        message += "異動後系統別：";
        message += plusCheckItemTypes
                .stream()
                .map(RequireCheckItemType::getDescr)
                .collect(Collectors.joining("、", "【", "】"));

        message += "<br/>";
        message += "以下相關的單位還未被設定，請確認是否需手動新增！";
        message = WkHtmlUtils.addBlueBlodClass(message);

        // 通知單位
        message += assignDepsMessage;
        message += noticeDepsMessage;

        return message;

    }

    /**
     * @param assignSendType
     * @param requireSid
     * @param smallCategorySid
     * @param newCheckItemTypes
     * @return
     */
    private String prepareNeedAssignNoticeMessage(
            AssignSendType assignSendType,
            String requireSid,
            String smallCategorySid,
            List<RequireCheckItemType> newCheckItemTypes) {

        // ====================================
        // 取得所有項目預設單位
        // ====================================
        // 收集各系統別設定
        Set<Integer> allSettingDepSids = Sets.newHashSet();
        for (RequireCheckItemType requireCheckItemType : newCheckItemTypes) {
            List<Integer> settingDepSids = settingDefaultAssignSendDepService.findDepSidsByUnqKey(
                    smallCategorySid,
                    requireCheckItemType,
                    assignSendType);

            if (WkStringUtils.notEmpty(settingDepSids)) {
                allSettingDepSids.addAll(settingDepSids);
            }
        }
        // 沒有預設單位
        if (WkStringUtils.isEmpty(allSettingDepSids)) {
            return "";
        }

        // ====================================
        // 取得目前已分派或通知的單位
        // ====================================
        Set<Integer> nowAssignOrNoticeDepSids = assignSendInfoService.findLastActiveDepSidByRequireAndType(requireSid, assignSendType);

        // ====================================
        // 比對
        // ====================================
        // 比對已分派/通知單位，和目前預設單位的差異
        ItemsCollectionDiffTo<Integer> diffTo = WkCommonUtils.itemCollectionDiff(
                nowAssignOrNoticeDepSids,
                allSettingDepSids);

        // 取得『還沒』被分派/通知的預設單位 (預設單位 - 以設定單位)
        Collection<Integer> plusDepSids = diffTo.getPlusItems();

        if (WkStringUtils.isEmpty(plusDepSids)) {
            return "";
        }

        // ====================================
        // 組訊息
        // ====================================
        String message = "<br/><br/>";
        message += assignSendType.getLabel() + "：";
        message += "<br/>";
        message += WkOrgUtils.prepareDepsNameByTreeStyle(Lists.newArrayList(plusDepSids), 10);

        return message;
    }

    /**
     * 異動系統別操作：儲存
     */
    public void event_checkItems_save(Require01MBean require01MBean) {

    }

}
