/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.component;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.utils.ToolsDate;
import com.cy.tech.request.logic.vo.SimpleDateFormatEnum;
import com.cy.tech.request.vo.enums.OthSetAttachmentBehavior;
import com.cy.tech.request.vo.enums.OthSetHistoryBehavior;
import com.cy.tech.request.web.attachment.TrOsAttachMaintainCompant;
import com.cy.tech.request.web.attachment.TrOsReplyAttachMaintainCompant;
import com.cy.tech.request.web.attachment.TrOsReplyUploadAttachCompant;
import com.cy.tech.request.web.attachment.TrOsUploadAttachCompant;
import com.cy.tech.request.web.attachment.util.AttachMaintainCompant;
import com.cy.tech.request.web.controller.component.reqconfirm.ReqConfirmClientHelper;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.logic.component.OthHistoryLogicComponents;
import com.cy.tech.request.web.controller.logic.component.OthSetReplyLogicComponents;
import com.cy.tech.request.web.controller.logic.component.OthSetSettingLogicComponents;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.view.vo.HistoryVO;
import com.cy.tech.request.web.controller.view.vo.OthSetSettingVO;
import com.cy.tech.request.web.controller.view.vo.ReplyEditVO;
import com.cy.tech.request.web.controller.view.vo.SubReplyVO;
import com.cy.tech.request.web.listener.AttachMaintainCallBack;
import com.cy.tech.request.web.listener.DeleteAttCallBack;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.listener.ReloadAttCallBack;
import com.cy.tech.request.web.listener.ReplyCallBack;
import com.cy.tech.request.web.listener.ReplyCallBackCondition;
import com.cy.tech.request.web.listener.TabLoadCallBack;
import com.cy.tech.request.web.listener.UploadAttCallBack;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
public class OthSetSettingViewComponents implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -11108401076529405L;
    @Getter
    private OthSetSettingVO selOthSetSettingVO;
    /** 其他設定資訊List */
    @Getter
    private List<OthSetSettingVO> othSetSettingVOs;
    /** 登入者Sid */
    private Integer loginUserSid;
    /** 登入者部門Sid */
    private Integer loginUserDepSid;
    /** 登入者公司Sid */
    @SuppressWarnings("unused")
    private Integer loginUserCompSid;
    /** 需求單Sid */
    private String request_Sid;
    /** 其他設定資訊主檔上傳View元件 */
    @Getter
    private TrOsUploadAttachCompant trOsUploadAttachCompant;
    /** 其他設定資訊(回覆,回覆的回覆,完成,取消)上傳View元件 */
    @Getter
    private TrOsReplyUploadAttachCompant trOsReplyUploadAttachCompant;
    /** 其他設定資訊(回覆,回覆的回覆,完成,取消)附件維護View元件 */
    @Getter
    private TrOsReplyAttachMaintainCompant trOsReplyAttachMaintainCompant;
    /** 刪除附件時,需進行追蹤刷新,藉由此CallBack,進行刷新追蹤資料 */
    private TabLoadCallBack tabLoadCallBack;
    /** (回覆,回覆的回覆,完成,取消)新增修改物件 */
    @Getter
    private ReplyEditVO replyEditVO;
    /** 訊息CallBack */
    private MessageCallBack messageCallBack;
    /** 其他設定資訊刪除附件時,使用共用的Dialog,故藉此取得當下欲刪除的附件 */
    @Getter
    private TrOsAttachMaintainCompant selAttachMaintainCompant;

    public OthSetSettingViewComponents(Integer loginUserSid, Integer loginUserDepSid,
            Integer loginUserCompSid, TabLoadCallBack tabLoadCallBack, MessageCallBack messageCallBack) {
        this.loginUserSid = loginUserSid;
        this.loginUserDepSid = loginUserDepSid;
        this.loginUserCompSid = loginUserCompSid;
        this.trOsUploadAttachCompant = new TrOsUploadAttachCompant(reloadTrOsAttCallBack);
        this.trOsReplyUploadAttachCompant = new TrOsReplyUploadAttachCompant(reloadTrOsReplyAttCallBack);
        this.trOsReplyAttachMaintainCompant = new TrOsReplyAttachMaintainCompant(loginUserSid, deleteAttCallBack, tabLoadCallBack);
        this.tabLoadCallBack = tabLoadCallBack;
        this.messageCallBack = messageCallBack;
        this.replyEditVO = new ReplyEditVO("", "", "", "", "", "", "", "", "", "", "");
        this.selAttachMaintainCompant = new TrOsAttachMaintainCompant(loginUserSid, "", null);
    }

    /**
     * 刷新資料
     *
     * @param request_Sid 需求單Sid
     */
    public void loadData(String request_Sid) {
        this.request_Sid = request_Sid;
        this.loadData();
    }

    /** 刷新資料 */
    public void loadData() {
        try {
            othSetSettingVOs = OthSetSettingLogicComponents.getInstance().getOthSetSettingVOByRequestSid(request_Sid, loginUserSid, loginUserDepSid,
                    uploadAttCallBack, tabLoadCallBack, replyCallBack, attachMaintainCallBack);
        } catch (Exception e) {
            log.error("loadData", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 其他設定資訊主檔,上傳完附件需即時刷新介面
     *
     * @param tros_Sid 其他設定資訊Sid
     */
    private void loadTrOsAtt(String tros_Sid) {
        try {
            List<OthSetSettingVO> othSetSettingVO = othSetSettingVOs.stream()
                    .filter(each -> tros_Sid.equals(each.getSid()))
                    .collect(Collectors.toList());
            OthSetSettingVO temp = othSetSettingVO.get(0);
            trOsUploadAttachCompant.loadAttData(temp.getOs_sid(), temp.getOs_no(), temp.getRequire_sid(), temp.getRequire_no(), temp.getOs_history_sid(),
                    loginUserSid, loginUserDepSid);
            DisplayController.getInstance().update("viewPanelBottomInfoTabId:tros_upload_ID");
            DisplayController.getInstance().showPfWidgetVar("tros_upload_Wv");
        } catch (Exception e) {
            log.error("loadTrOsAtt", e);
        }
    }

    /**
     * 其他設定資訊(回覆,回覆的回覆,完成,取消)初始化上傳附件元件
     *
     * @param autoMappingEntity        新增附件時,是否自動綁訂主檔(回覆,回覆的回覆,完成,取消) -
     *                                 新增傳入false,編輯傳入true
     * @param othSetAttachmentBehavior 行為
     * @param uploadFinishView         上傳完,欲更新View ID
     */
    public void loadTrOsReplyAtt(boolean autoMappingEntity, OthSetAttachmentBehavior othSetAttachmentBehavior,
            String uploadFinishView) {
        this.trOsReplyUploadAttachCompant.loadAttData(replyEditVO.getOs_sid(),
                replyEditVO.getOs_no(), replyEditVO.getRequire_sid(),
                replyEditVO.getRequire_no(), replyEditVO.getOs_history_sid(),
                loginUserSid, loginUserDepSid, autoMappingEntity,
                othSetAttachmentBehavior, uploadFinishView);
    }

    /** 其他設定資訊刪除附件時,使用共用的Dialog,執行刪除 */
    public void deleteTrOsAtt() {
        selAttachMaintainCompant.deleteAtt();
        selAttachMaintainCompant = new TrOsAttachMaintainCompant(loginUserSid, "", null);
    }

    /**
     * 重新刷新其他設定資訊附件
     *
     * @param tros_Sid 其他設定資訊Sid
     */
    private void reloadTrOsAtt(String tros_Sid) {
        List<OthSetSettingVO> othSetSettingVO = othSetSettingVOs.stream()
                .filter(each -> tros_Sid.equals(each.getSid()))
                .collect(Collectors.toList());
        OthSetSettingVO temp = othSetSettingVO.get(0);
        temp.getTrOsAttachMaintainCompant().reloadData();
    }

    /** 上傳其他設定資訊主檔附件CallBack */
    private final UploadAttCallBack uploadAttCallBack = new UploadAttCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -8874224851644084583L;

        @Override
        public void doUploadAtt(String sid) {
            loadTrOsAtt(sid);
        }
    };

    /** 重新刷新其他設定資訊主檔附件CallBack */
    private final ReloadAttCallBack reloadTrOsAttCallBack = new ReloadAttCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -5223939078231582589L;

        @Override
        public void reloadUploadAtt(String sid) {
            reloadTrOsAtt(sid);
        }
    };

    /** 其他設定資訊主檔附件維護,進行刪除CallBack */
    private final AttachMaintainCallBack attachMaintainCallBack = new AttachMaintainCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -3374266692605118693L;

        @Override
        public void loadDelete(AttachMaintainCompant attachMaintainCompant) {
            selAttachMaintainCompant = (TrOsAttachMaintainCompant) attachMaintainCompant;
        }
    };

    /** 重新刷新其他設定資訊(回覆,回覆的回覆,完成,取消)附件CallBack */
    private final ReloadAttCallBack reloadTrOsReplyAttCallBack = new ReloadAttCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -5612189789050004277L;

        @Override
        public void reloadUploadAtt(String sid) {
            if (!Strings.isNullOrEmpty(sid)) {
                int index = 0;
                for (OthSetSettingVO item : othSetSettingVOs) {
                    if (!item.getOs_sid().equals(replyEditVO.getOs_sid())) {
                        index++;
                        continue;
                    }
                    int historyIndex = 0;
                    int replyAndReplyIndex = 0;
                    String historySid = "";
                    for (HistoryVO historyItem : item.getHistoryVOs()) {
                        if (!Strings.isNullOrEmpty(replyEditVO.getOs_reply_and_reply_sid())
                                && !Strings.isNullOrEmpty(historyItem.getOs_reply_sid())
                                && historyItem.getOs_reply_sid().equals(replyEditVO.getOs_reply_sid())) {
                            historySid = historyItem.getHistorySid();
                            SubReplyVO sv = new SubReplyVO(replyEditVO.getOs_reply_and_reply_sid());
                            replyAndReplyIndex = historyItem.getSubReplyVO().indexOf(sv);
                            if (replyAndReplyIndex >= 0) {
                                historyItem.getSubReplyVO().get(replyAndReplyIndex).setShowAttachmentBtn(true);
                            }
                            break;
                        } else if (replyEditVO.getOs_history_sid().equals(historyItem.getHistorySid())) {
                            historyItem.setShowAttachmentBtn(true);
                            break;
                        } else {
                            historyIndex++;
                        }
                    }
                    DisplayController.getInstance().update("viewPanelBottomInfoTabId:os_acc_panel_layer_zero");
                    DisplayController.getInstance().execute("accPanelSelectTab('os_acc_panel_layer_zero','" + index + "');");
                    DisplayController.getInstance().execute("accPanelSelectTab('os_acc_panel_layer_one_" + item.getOs_sid() + "','" + historyIndex + "');");
                    if (!Strings.isNullOrEmpty(replyEditVO.getOs_reply_and_reply_sid())) {
                        DisplayController.getInstance().execute("accPanelSelectTab('os_acc_panel_layer_two_" + historySid + "','" + replyAndReplyIndex + "');");
                    }
                }
            }
        }
    };

    /** 執行完成動作 */
    public void saveFinish(Require01MBean r01MBean) {
        try {
            OthHistoryLogicComponents.getInstance().createFinishHistory(
                    replyEditVO.getOs_sid(),
                    replyEditVO.getOs_no(),
                    replyEditVO.getRequire_sid(),
                    replyEditVO.getRequire_no(),
                    loginUserDepSid,
                    loginUserSid,
                    replyEditVO.getContent(),
                    trOsReplyUploadAttachCompant.getSelectedAttachmentVOs());

            //更新主檔資料
            r01MBean.reBuildeByUpdateFaild();
            
            //更新頁簽資料
            r01MBean.getOthSetSettingViewComponents().loadData();

            r01MBean.getBottomTabMBean().resetTabIdx(r01MBean);
            r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.OTH_SET);



            DisplayController.getInstance().hidePfWidgetVar("tros_finish_dlg_wv");

            loadData();

            int index = 0;
            String execScript = "";
            for (OthSetSettingVO item : othSetSettingVOs) {
                if (!item.getOs_sid().equals(replyEditVO.getOs_sid())) {
                    index++;
                    continue;
                }
                DisplayController.getInstance().update("viewPanelBottomInfoTabId:os_acc_panel_layer_zero");
                execScript += "accPanelSelectTab('os_acc_panel_layer_zero','" + index + "');";
            }

            DisplayController.getInstance().execute(execScript + ReqConfirmClientHelper.CLIENT_SRIPT_CLOSE_PANEL);
        } catch (IllegalStateException e) {
            log.warn(e.getMessage());
            MessagesUtils.showWarn(e.getMessage());
            return;
        } catch (Exception e) {
            log.error("saveFinish error!" + e.getMessage(), e);
            MessagesUtils.showError("執行失敗!" + e.getMessage());
        }
    }

    /** 執行取消動作 */
    public void saveCancel() {
        try {

            OthHistoryLogicComponents.getInstance().createCancelHistory(replyEditVO.getOs_sid(), replyEditVO.getOs_no(),
                    replyEditVO.getRequire_sid(), replyEditVO.getRequire_no(), loginUserDepSid,
                    loginUserSid, replyEditVO.getContent(),
                    trOsReplyUploadAttachCompant.getSelectedAttachmentVOs());
            loadData();
            int index = 0;
            for (OthSetSettingVO item : othSetSettingVOs) {
                if (!item.getOs_sid().equals(replyEditVO.getOs_sid())) {
                    index++;
                    continue;
                }
                // List<HistoryVO> historys =
                // OthHistoryLogicComponents.getInstance().getHistoryVOByRequestSidAndOs_Sid(item.getRequire_sid(),
                // item.getOs_sid(), loginUserSid, loginUserDepSid, uploadAttCallBack,
                // tabLoadCallBack, replyCallBack);
                // item.updateHistory(historys);
                DisplayController.getInstance().update("viewPanelBottomInfoTabId:os_acc_panel_layer_zero");
                DisplayController.getInstance().execute("accPanelSelectTab('os_acc_panel_layer_zero','" + index + "');");
                DisplayController.getInstance().hidePfWidgetVar("tros_cancel_dlg_wv");
            }
        } catch (Exception e) {
            log.error("saveCancel", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /** 執行回覆動作 */
    public void saveReply() {
        try {
            if (Strings.isNullOrEmpty(replyEditVO.getOs_history_sid()) && Strings.isNullOrEmpty(replyEditVO.getOs_reply_sid())) {
                OthHistoryLogicComponents.getInstance().createReplyHistory(replyEditVO.getOs_sid(), replyEditVO.getOs_no(),
                        replyEditVO.getRequire_sid(), replyEditVO.getRequire_no(), loginUserDepSid,
                        loginUserSid, replyEditVO.getContent(),
                        trOsReplyUploadAttachCompant.getSelectedAttachmentVOs(),
                        OthSetHistoryBehavior.REPLY);
            } else {
                OthHistoryLogicComponents.getInstance().modifyReplyHistory(loginUserSid, replyEditVO.getOs_history_sid(), replyEditVO.getOs_reply_sid(),
                        replyEditVO.getContent());
            }
            int index = 0;
            for (OthSetSettingVO item : othSetSettingVOs) {
                if (!item.getOs_sid().equals(replyEditVO.getOs_sid())) {
                    index++;
                    continue;
                }
                List<HistoryVO> historys = OthHistoryLogicComponents.getInstance().getHistoryVOByRequestSidAndOs_Sid(item.getRequire_sid(),
                        item.getOs_sid(), loginUserSid, loginUserDepSid, uploadAttCallBack, tabLoadCallBack, replyCallBack);
                item.updateHistory(historys);
                DisplayController.getInstance().update("viewPanelBottomInfoTabId:os_acc_panel_layer_zero");
                DisplayController.getInstance().execute("accPanelSelectTab('os_acc_panel_layer_zero','" + index + "');");
                DisplayController.getInstance().hidePfWidgetVar("tros_reply_dlg_wv");
                break;
            }
        } catch (Exception e) {
            log.error("saveReply", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /** 執行回覆的回覆動作 */
    public void saveReplyAndReply() {
        try {
            String os_reply_and_reply_sid = "";
            if (Strings.isNullOrEmpty(replyEditVO.getOs_reply_and_reply_sid())) {
                os_reply_and_reply_sid = OthHistoryLogicComponents.getInstance().createReplyAndReplyHistory(replyEditVO.getOs_sid(),
                        replyEditVO.getOs_reply_sid(), loginUserDepSid, loginUserSid, replyEditVO.getContent(),
                        trOsReplyUploadAttachCompant.getSelectedAttachmentVOs());
            } else {
                os_reply_and_reply_sid = replyEditVO.getOs_reply_and_reply_sid();
                OthHistoryLogicComponents.getInstance().modifyReplyAndReplyHistory(loginUserSid, replyEditVO.getOs_history_sid(),
                        replyEditVO.getOs_reply_and_reply_sid(), replyEditVO.getContent());
            }
            int index = 0;
            for (OthSetSettingVO item : othSetSettingVOs) {
                if (!item.getOs_sid().equals(replyEditVO.getOs_sid())) {
                    index++;
                    continue;
                }
                List<HistoryVO> historys = OthHistoryLogicComponents.getInstance().getHistoryVOByRequestSidAndOs_Sid(item.getRequire_sid(),
                        item.getOs_sid(), loginUserSid, loginUserDepSid, uploadAttCallBack, tabLoadCallBack, replyCallBack);
                item.updateHistory(historys);
                int replyIndex = 0;
                for (HistoryVO hv : historys) {
                    if (!hv.getOs_reply_sid().equals(replyEditVO.getOs_reply_sid())) {
                        replyIndex++;
                        continue;
                    }
                    SubReplyVO sv = new SubReplyVO(os_reply_and_reply_sid);
                    int replyAndReplyIndex = hv.getSubReplyVO().indexOf(sv);
                    DisplayController.getInstance().update("viewPanelBottomInfoTabId:os_acc_panel_layer_zero");
                    DisplayController.getInstance().execute("accPanelSelectTab('os_acc_panel_layer_zero','" + index + "');");
                    DisplayController.getInstance().execute("accPanelSelectTab('os_acc_panel_layer_one_" + item.getOs_sid() + "','" + replyIndex + "');");
                    DisplayController.getInstance()
                            .execute("accPanelSelectTab('os_acc_panel_layer_two_" + hv.getHistorySid() + "','" + replyAndReplyIndex + "');");
                    DisplayController.getInstance().hidePfWidgetVar("tros_reply_and_reply_dlg_wv");
                    break;
                }

            }
        } catch (Exception e) {
            log.error("saveReplyAndReply", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /** 其他設定資訊(回覆,回覆的回覆,完成,取消)附件維護,進行刪除CallBack */
    private final DeleteAttCallBack deleteAttCallBack = new DeleteAttCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -999216357631424766L;

        @Override
        public void reloadAttBtn(ReplyCallBackCondition replyCallBackCondition) {

            int index = 0;
            for (OthSetSettingVO item : othSetSettingVOs) {
                if (!item.getOs_sid().equals(replyCallBackCondition.getOs_sid())) {
                    index++;
                    continue;
                }
                String historySid = "";
                int replyAndReplyIndex = 0;
                for (HistoryVO historyItem : item.getHistoryVOs()) {
                    if (Strings.isNullOrEmpty(replyCallBackCondition.getOs_reply_and_reply_sid())) {
                        if (replyCallBackCondition.getOs_history_sid().equals(historyItem.getHistorySid())) {
                            historyItem.setShowAttachmentBtn(false);
                        }
                    } else {
                        int tempReplyAndReplyIndex = historyItem.getSubReplyVO().indexOf(new SubReplyVO(replyCallBackCondition.getOs_reply_and_reply_sid()));
                        if (tempReplyAndReplyIndex >= 0) {
                            historySid = historyItem.getHistorySid();
                            replyAndReplyIndex = tempReplyAndReplyIndex;
                            historyItem.getSubReplyVO().get(replyAndReplyIndex).setShowAttachmentBtn(false);
                        }
                    }
                }
                int replyIndex = 0;
                if (!Strings.isNullOrEmpty(replyCallBackCondition.getOs_reply_and_reply_sid())) {
                    HistoryVO selHistoryVO = new HistoryVO(historySid);
                    replyIndex = item.getHistoryVOs().indexOf(selHistoryVO);
                } else {
                    HistoryVO selHistoryVO = new HistoryVO(replyCallBackCondition.getOs_history_sid());
                    replyIndex = item.getHistoryVOs().indexOf(selHistoryVO);
                }
                DisplayController.getInstance().update("viewPanelBottomInfoTabId:os_acc_panel_layer_zero");
                DisplayController.getInstance().execute("accPanelSelectTab('os_acc_panel_layer_zero','" + index + "');");
                DisplayController.getInstance().execute("accPanelSelectTab('os_acc_panel_layer_one_" + item.getOs_sid() + "','" + replyIndex + "');");
                if (!Strings.isNullOrEmpty(replyCallBackCondition.getOs_reply_and_reply_sid())) {
                    if (!Strings.isNullOrEmpty(historySid) && replyAndReplyIndex >= 0) {
                        DisplayController.getInstance().execute("accPanelSelectTab('os_acc_panel_layer_two_" + historySid + "','" + replyAndReplyIndex + "');");
                    }
                }
            }
            DisplayController.getInstance().hidePfWidgetVar("tros_history_attach_dlg_wv");
        }
    };

    /** (回覆,回覆的回覆,完成,取消)新增,修改載入CallBack */
    private final ReplyCallBack replyCallBack = new ReplyCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 5933534564303726841L;

        /** 新增回覆 */
        @Override
        public void addReply(ReplyCallBackCondition rc) {
            Org dep = WkOrgCache.getInstance().findBySid(loginUserDepSid);
            User user = WkUserCache.getInstance().findBySid(loginUserSid);
            replyEditVO = new ReplyEditVO(
            		WkOrgUtils.getOrgName(dep),
                    user.getName(),
                    ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS_slide.getValue(), new Date()),
                    "",
                    rc.getOs_sid(),
                    rc.getOs_no(),
                    rc.getRequire_sid(),
                    rc.getRequire_no(),
                    rc.getOs_history_sid(),
                    rc.getOs_reply_sid(),
                    rc.getOs_reply_and_reply_sid());
            loadTrOsReplyAtt(false, OthSetAttachmentBehavior.REPLY_FILE, "tros_reply_dlg_attach_view_id");
        }

        /** 新增回覆的回覆 */
        @Override
        public void addReplyAndReply(ReplyCallBackCondition rc) {
            Org dep = WkOrgCache.getInstance().findBySid(loginUserDepSid);
            User user = WkUserCache.getInstance().findBySid(loginUserSid);
            replyEditVO = new ReplyEditVO(
            		WkOrgUtils.getOrgName(dep),
                    user.getName(),
                    ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS_slide.getValue(), new Date()),
                    "",
                    rc.getOs_sid(),
                    rc.getOs_no(),
                    rc.getRequire_sid(),
                    rc.getRequire_no(),
                    rc.getOs_history_sid(),
                    rc.getOs_reply_sid(),
                    rc.getOs_reply_and_reply_sid());
            loadTrOsReplyAtt(false, OthSetAttachmentBehavior.REPLY_AND_REPLY_FILE, "tros_reply_and_reply_dlg_attach_view_id");

            for (OthSetSettingVO item : othSetSettingVOs) {
                if (!item.getOs_sid().equals(rc.getOs_sid())) {
                    continue;
                }
                HistoryVO selHistoryVO = new HistoryVO(rc.getOs_history_sid());
                int replyIndex = item.getHistoryVOs().indexOf(selHistoryVO);
                DisplayController.getInstance()
                        .execute("accPanelSelectTabNoCloseOther('os_acc_panel_layer_one_" + item.getOs_sid() + "','" + replyIndex + "');");
                break;
            }

        }

        /** 修改回覆 */
        @Override
        public void modifyReply(ReplyCallBackCondition rc) {
            try {
                replyEditVO = OthSetReplyLogicComponents.getInstance().getReplyEditByReplySid(rc.getOs_reply_sid(), rc.getOs_history_sid());
                loadTrOsReplyAtt(true, OthSetAttachmentBehavior.REPLY_FILE, "");
                for (OthSetSettingVO item : othSetSettingVOs) {
                    if (!item.getOs_sid().equals(replyEditVO.getOs_sid())) {
                        continue;
                    }
                    HistoryVO selHistoryVO = new HistoryVO(replyEditVO.getOs_history_sid());
                    int replyIndex = item.getHistoryVOs().indexOf(selHistoryVO);
                    DisplayController.getInstance()
                            .execute("accPanelSelectTabNoCloseOther('os_acc_panel_layer_one_" + item.getOs_sid() + "','" + replyIndex + "');");
                }
            } catch (Exception e) {
                log.error("modifyReply", e);
                messageCallBack.showMessage(e.getMessage());
            }
        }

        /** 修改回覆的回覆 */
        @Override
        public void modifyReplyAndReply(ReplyCallBackCondition rc) {
            try {
                replyEditVO = OthSetReplyLogicComponents.getInstance().getReplyEditByReplyReplySid(rc.getOs_reply_and_reply_sid(),
                        rc.getReply_and_reply_history_sid());
                loadTrOsReplyAtt(true, OthSetAttachmentBehavior.REPLY_AND_REPLY_FILE, "");
                for (OthSetSettingVO item : othSetSettingVOs) {
                    if (!item.getOs_sid().equals(replyEditVO.getOs_sid())) {
                        continue;
                    }
                    HistoryVO selHistoryVO = new HistoryVO(rc.getOs_history_sid());
                    int replyIndex = item.getHistoryVOs().indexOf(selHistoryVO);

                    DisplayController.getInstance()
                            .execute("accPanelSelectTabNoCloseOther('os_acc_panel_layer_one_" + item.getOs_sid() + "','" + replyIndex + "');");
                    selHistoryVO = item.getHistoryVOs().get(replyIndex);
                    SubReplyVO sv = new SubReplyVO(rc.getOs_reply_and_reply_sid());
                    int replyAndReplyIndex = selHistoryVO.getSubReplyVO().indexOf(sv);
                    DisplayController.getInstance().execute(
                            "accPanelSelectTabNoCloseOther('os_acc_panel_layer_two_" + selHistoryVO.getHistorySid() + "','" + replyAndReplyIndex + "');");
                }
            } catch (Exception e) {
                log.error("modifyReplyAndReply", e);
                messageCallBack.showMessage(e.getMessage());
            }
        }

        /** 新增完成 */
        @Override
        public void addFinish(ReplyCallBackCondition rc) {
            Org dep = WkOrgCache.getInstance().findBySid(loginUserDepSid);
            User user = WkUserCache.getInstance().findBySid(loginUserSid);
            replyEditVO = new ReplyEditVO(
            		WkOrgUtils.getOrgName(dep),
                    user.getName(),
                    ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS_slide.getValue(), new Date()),
                    "",
                    rc.getOs_sid(),
                    rc.getOs_no(),
                    rc.getRequire_sid(),
                    rc.getRequire_no(),
                    rc.getOs_history_sid(),
                    rc.getOs_reply_sid(),
                    rc.getOs_reply_and_reply_sid());
            loadTrOsReplyAtt(false, OthSetAttachmentBehavior.FINISH_OS_FILE, "tros_finish_dlg_attach_view_id");
        }

        /** 新增取消 */
        @Override
        public void addCancel(ReplyCallBackCondition rc) {
            Org dep = WkOrgCache.getInstance().findBySid(loginUserDepSid);
            User user = WkUserCache.getInstance().findBySid(loginUserSid);
            replyEditVO = new ReplyEditVO(
            		WkOrgUtils.getOrgName(dep),
                    user.getName(),
                    ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS_slide.getValue(), new Date()),
                    "",
                    rc.getOs_sid(),
                    rc.getOs_no(),
                    rc.getRequire_sid(),
                    rc.getRequire_no(),
                    rc.getOs_history_sid(),
                    rc.getOs_reply_sid(),
                    rc.getOs_reply_and_reply_sid());
            loadTrOsReplyAtt(false, OthSetAttachmentBehavior.CANCEL_OS_FILE, "tros_cancel_dlg_attach_view_id");
        }

        /** 點選附件 */
        @Override
        public void maintainAtt(ReplyCallBackCondition replyCallBackCondition) {

            trOsReplyAttachMaintainCompant.loadData(Lists.newArrayList(), replyCallBackCondition.getOs_sid(), replyCallBackCondition.getOs_history_sid());
        }

    };

}
