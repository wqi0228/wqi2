package com.cy.tech.request.web.controller.setting;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.MenuService;
import com.cy.tech.request.logic.service.report.favorite.HomePageFavoriteService;
import com.cy.tech.request.vo.enums.MenuBaseType;
import com.cy.tech.request.vo.menu.TrMenuGroupBase;
import com.cy.tech.request.vo.menu.TrMenuItem;
import com.cy.tech.request.vo.report.favorite.HomePageFavorite;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.work.common.cache.WkUserWithRolesCache;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 使用者自訂義報表快選區
 *
 * @author kasim
 */
@NoArgsConstructor
@Controller
@Scope("view")
public class Setting09MBean implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -8878874586774569090L;
    @Autowired
	transient private LoginBean loginBean;
	@Autowired
	transient private MenuService menuService;
	@Autowired
	transient private HomePageFavoriteService homePageService;

	/** 操作的物件 */
	private HomePageFavorite homePage;
	@Getter
	@Setter
	/** (不)選取所有報表 */
	private Boolean selectAll;
	@Getter
	@Setter
	/** 選取的報表快選 */
	private List<String> selectReportTypes = Lists.newArrayList();
	@Getter
	/** 報表選項 */
	private SelectItem[] reportItems;

	@PostConstruct
	public void init() {
		this.initReportItems();
		homePage = homePageService.findByCreatedUser(loginBean.getUser());
		if (homePage != null) {
			if (homePage.getHomePages().getValue() != null
			        && !homePage.getHomePages().getValue().isEmpty()) {
				selectReportTypes = homePage.getHomePages().getValue();
			}
		}
		this.checkSelectAll();
	}

	/**
	 * 初始化報表選項
	 */
	private void initReportItems() {
		LinkedHashMap<String, String> itemMap = Maps.newLinkedHashMap();
		TrMenuGroupBase mnuGroupBase = menuService.findMenuBaseGroup(MenuBaseType.REPORT);
		
		//查詢登入者所有角色
		List<String> roleNames = WkUserWithRolesCache.getInstance().findRoleNamesByUserAndLoginCompID(
			SecurityFacade.getUserSid(),
			SecurityFacade.getCompanyId());	
		
		mnuGroupBase.getMenuItemGroup().forEach(menuGroup -> {
			menuGroup.getItems().forEach(item -> {
				if (menuService.hasPermission(
				        item.getPermissionRole(),
				        roleNames,
				        SecurityFacade.getUserSid())) {
					itemMap.put(item.getUrl(), item.getTitle());
				}
			});
		});
		
		// 加入送測排程行事曆
		TrMenuItem menuItem = menuService.findyByComponentId("fun-sendTestSchedule");
		
		if (menuService.hasPermission(
		        menuItem.getPermissionRole(),
		        roleNames,
		        SecurityFacade.getUserSid())) {
			itemMap.put(menuItem.getUrl(), menuItem.getTitle());
		}

		reportItems = new SelectItem[itemMap.size()];
		int i = 0;
		for (String each : itemMap.keySet()) {
			reportItems[i++] = new SelectItem(each, itemMap.get(each));
		}
	}

	/**
	 * 判斷是否選取全部
	 */
	private void checkSelectAll() {
		if (reportItems == null || reportItems.length == 0
		        || selectReportTypes == null || selectReportTypes.isEmpty()) {
			return;
		}
		selectAll = Boolean.TRUE;
		List<String> temp = Lists.newArrayList(selectReportTypes);
		for (SelectItem each : reportItems) {
			if (!temp.contains((String) each.getValue())) {
				selectAll = Boolean.FALSE;
				break;
			}
		}
	}

	/**
	 * 進行全(反)選
	 */
	public void doSelectAll() {
		if (selectAll) {
			for (SelectItem each : reportItems) {
				selectReportTypes.add((String) each.getValue());
			}
		} else {
			selectReportTypes = Lists.newArrayList();
		}
	}

	/**
	 * 存入報表快選區設定
	 */
	public void btnSaveFavorite() {
		homePage = homePageService.save(homePage, loginBean.getUser(), selectReportTypes);
		this.checkSelectAll();
	}
}
