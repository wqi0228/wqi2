package com.cy.tech.request.web.controller.logic.component;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author aken_kao
 *
 */
public class UsersMultiSelectComponent<T> {

    /** 來源列表 */
    @Getter
    @Setter
    private List<T> sourceUsers = Lists.newArrayList();
    /** 來源列表(Filter) */
    @Getter
    @Setter
    private List<T> filterSourceUsers = Lists.newArrayList();

    /** dataTable選取的資料 */
    @Getter
    @Setter
    private List<T> selSourceUser;
    /** 選取列表 */
    @Getter
    @Setter
    private List<T> targetUsers = Lists.newArrayList();
    /** dataTable選取的資料 */
    @Getter
    @Setter
    private List<T> selTargetUser;

    /**
     * 增加資料
     */
    public void btnAddRowByMember() {
        if (selSourceUser == null) {
            filterSourceUsers.addAll(sourceUsers);
            return;
        }
        if (CollectionUtils.isEmpty(filterSourceUsers)) {
            sourceUsers.removeAll(selSourceUser);
            filterSourceUsers.addAll(sourceUsers);
        } else {
            sourceUsers.removeAll(selSourceUser);
            filterSourceUsers.removeAll(selSourceUser);
        }

        targetUsers.addAll(selSourceUser);
        selSourceUser = null;
        selTargetUser = null;
    }

    /**
     * 增加資料(All)
     */
    public void btnAddAllRowByMember() {
        if (sourceUsers == null || sourceUsers.isEmpty()) {
            return;
        }

        if (CollectionUtils.isEmpty(filterSourceUsers)) {
            targetUsers.addAll(sourceUsers);
            sourceUsers = Lists.newArrayList();
        } else {

            if (WkStringUtils.notEmpty(filterSourceUsers)) {
                targetUsers.addAll(filterSourceUsers);
                sourceUsers.removeAll(filterSourceUsers);
                filterSourceUsers.removeAll(filterSourceUsers);
            }

        }
        selSourceUser = null;
        selTargetUser = null;
    }

    /**
     * 移除資料
     */
    public void btnRemoveRowByMember() {
        if (selTargetUser == null) {
            filterSourceUsers.addAll(sourceUsers);
            return;
        }
        if (CollectionUtils.isEmpty(filterSourceUsers)) {
            filterSourceUsers.addAll(sourceUsers);
        }
        if (WkStringUtils.notEmpty(selTargetUser)) {
            targetUsers.removeAll(selTargetUser);
            sourceUsers.addAll(selTargetUser);
            filterSourceUsers.addAll(selTargetUser);
        }

        selSourceUser = null;
        selTargetUser = null;
    }

    /**
     * 移除資料
     */
    public void btnRemoveAllRowByMember() {
        if (targetUsers == null || targetUsers.isEmpty()) {
            filterSourceUsers.addAll(sourceUsers);
            return;
        }
        sourceUsers.addAll(targetUsers);
        filterSourceUsers.addAll(sourceUsers);
        targetUsers = Lists.newArrayList();
        selSourceUser = null;
        selTargetUser = null;
    }
}
