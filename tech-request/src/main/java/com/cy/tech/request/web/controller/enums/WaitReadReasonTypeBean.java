/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.enums;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.SelectItem;

import org.springframework.stereotype.Controller;

import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.google.common.collect.Lists;

/**
 *
 * @author kasim
 */
@Controller
public class WaitReadReasonTypeBean implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -4940325912572974609L;

    public SelectItem[] getPrototypeItems() {
		List<WaitReadReasonType> types = this.getPrototypeValues();
		SelectItem[] items = new SelectItem[types.size()];
		int i = 0;
		for (WaitReadReasonType each : types) {
			items[i++] = new SelectItem(each, each.getValue());
		}
		return items;
	}

	public List<WaitReadReasonType> getPrototypeValues() {
		return Lists.newArrayList(
		        WaitReadReasonType.PROTOTYPE_NOT_WAITREASON,
		        WaitReadReasonType.PROTOTYPE_SIGN_PROCESS,
		        WaitReadReasonType.PROTOTYPE_VERIFY_INVAILD,
		        WaitReadReasonType.PROTOTYPE_APPROVE,
		        WaitReadReasonType.PROTOTYPE_HAS_REPLY,
		        WaitReadReasonType.PROTOTYPE_REDO,
		        WaitReadReasonType.PROTOTYPE_FUNCTION_CONFORM,
		        WaitReadReasonType.FORCE_CLOSE);
	}

	public SelectItem[] getTestItems() {
		List<WaitReadReasonType> types = this.getTestValues();
		SelectItem[] items = new SelectItem[types.size()];
		int i = 0;
		for (WaitReadReasonType each : types) {
			items[i++] = new SelectItem(each, each.getValue());
		}
		return items;
	}

	public List<WaitReadReasonType> getTestValues() {
		return Lists.newArrayList(
		        WaitReadReasonType.TEST_NOT_WAITREASON,
		        WaitReadReasonType.TEST_SIGN_PROCESS,
		        WaitReadReasonType.TEST_APPROVE,
		        WaitReadReasonType.TEST_HAS_REPLY,
		        WaitReadReasonType.TEST_RETEST,
		        WaitReadReasonType.TEST_QA_TEST,
		        WaitReadReasonType.TEST_COMPLETE,
		        WaitReadReasonType.TEST_ROLL_BACK,
		        WaitReadReasonType.TEST_CANCEL,
		        WaitReadReasonType.TEST_UPDATE_ESTABLISHDATE,
		        WaitReadReasonType.TEST_HAS_PERSONAL_REPLY,
		        WaitReadReasonType.TEST_PAPER_READY,
		        WaitReadReasonType.QA_JOIN_SCHEDULE,
		        WaitReadReasonType.QA_UNJOIN_SCHEDULE,
		        WaitReadReasonType.ADJUST_TEST_DATE,
		        WaitReadReasonType.ADJUST_ONLINE_DATE,
		        WaitReadReasonType.ADJUST_MASTER_TESTER,
		        WaitReadReasonType.ADJUST_SLAVE_TESTER,
		        WaitReadReasonType.ADJUST_SCHEDULE_FINISH_DATE,
		        WaitReadReasonType.ADJUST_DATE);

	}

	public SelectItem[] getOnpgItems() {
		List<WaitReadReasonType> types = getOnpgValues();
		SelectItem[] items = new SelectItem[types.size()];
		int i = 0;
		for (WaitReadReasonType each : types) {
			items[i++] = new SelectItem(each, each.getValue());
		}
		return items;
	}

	public static List<WaitReadReasonType> getOnpgValues() {
		return Lists.newArrayList(
		        WaitReadReasonType.ONPG_NOT_WAITREASON,
		        WaitReadReasonType.ONPG_CHECK_RECORD,
		        WaitReadReasonType.ONPG_CHECK_REPLY,
		        WaitReadReasonType.ONPG_CANCEL,
		        WaitReadReasonType.ONPG_CHECK_COMPLETE,
		        WaitReadReasonType.ONPG_ANNOTATION,
		        WaitReadReasonType.ONPG_UPDATE_ESTABLISHDATE,
		        WaitReadReasonType.ONPG_UPDATE_NOTE,
		        WaitReadReasonType.FORCE_CLOSE);
	}
	
	

}
