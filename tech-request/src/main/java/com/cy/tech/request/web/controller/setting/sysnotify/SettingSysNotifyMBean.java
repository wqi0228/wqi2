package com.cy.tech.request.web.controller.setting.sysnotify;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.helper.component.ComponentHelper;
import com.cy.tech.request.logic.helper.component.mutipker.MultItemPickerByCustomerHelper;
import com.cy.tech.request.logic.helper.component.treepker.TreePickerCategoryHelper;
import com.cy.tech.request.logic.service.setting.sysnotify.SettingSysNotifyService;
import com.cy.tech.request.vo.setting.sysnotify.to.SettingSysNotifyMainInfo;
import com.cy.tech.request.vo.setting.sysnotify.to.SettingSysNotifyTypeInfo;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerCallback;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerComponent;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerConfig;
import com.cy.tech.request.web.controller.component.mipker.vo.MultItemPickerShowMode;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.notify.vo.enums.NotifyFilterType;
import com.cy.work.notify.vo.enums.NotifyMode;
import com.cy.work.notify.vo.enums.NotifyModule;
import com.cy.work.notify.vo.enums.NotifyType;
import com.cy.work.viewcomponent.treepker.TreePickerCallback;
import com.cy.work.viewcomponent.treepker.TreePickerComponent;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 系統通知設定
 * 
 * @author allen1214_wu
 */
@Slf4j
@Controller
@Scope("view")
public class SettingSysNotifyMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2768044733553600732L;
    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private transient SettingSysNotifyService settingSysNotifyService;
    @Autowired
    private transient DisplayController displayController;
    @Autowired
    private transient TreePickerCategoryHelper treePickerCategoryHelper;
    @Autowired
    private transient MultItemPickerByCustomerHelper multItemPickerByCustomerHelper;
    @Autowired
    private transient ComponentHelper componentHelper;

    // ========================================================================
    // 常數
    // ========================================================================
    @Getter
    public final String CUSTOMER_PICKER_ID = "CUSTOMER_PICKER_ID";
    @Getter
    public final String MAIL_SETTING_DLG_ID = "MAIL_SETTING_DLG_ID";
    /**
     * 所有模組別
     */
    @Getter
    private final List<NotifyModule> notifyModules = Lists.newArrayList(NotifyModule.values());
    /**
     * 所有模組別
     */
    @Getter
    private final List<NotifyType> notifyTypes = Lists.newArrayList(NotifyType.values());

    // ========================================================================
    // 變數
    // ========================================================================

    /**
     * 通知設定資料
     */
    @Getter
    private SettingSysNotifyMainInfo settingInfo;

    /**
     * 自訂通知 mail
     */
    @Getter
    @Setter
    private String editMailAddrs;
    /**
     * 設定視窗的標的設定項目
     */
    public NotifyType targetNotifyType;

    /**
     * 項目樹
     */
    @Getter
    public TreePickerComponent categoryTreePicker;

    /**
     * 廳主選擇器
     */
    @Getter
    private transient MultItemPickerComponent customerPicker;

    @Getter
    @Setter
    private Integer tabIndex = 0;

    // ========================================================================
    // 方法
    // ========================================================================

    /**
     * 初始化
     */
    @PostConstruct
    public void init() {
        try {
            this.findLoginUserSetting();
            this.displayController.update("notify_setting_info");
        } catch (Exception e) {
            String message = WkMessage.PROCESS_FAILED + ":" + e.getMessage();
            MessagesUtils.showError(message);
            log.error(message, e);
            return;
        }
    }

    /**
     * 查詢登入者設定
     * 
     * @throws Exception
     */
    private void findLoginUserSetting() throws Exception {
        // ====================================
        // 查詢
        // ====================================
        // 取得設定主檔
        this.settingInfo = this.settingSysNotifyService.findSettingInfoByUserSid(SecurityFacade.getUserSid());
        // 複製通知mail到view變數
        this.editMailAddrs = this.settingInfo.getNotifyMailAddrs();

        // ====================================
        // 取得所有需求類別
        // ====================================
        List<WkItem> allCategoryItems = this.categoryPickerCallback.prepareAllItems();

        // ====================================
        // 取得所有廳主
        // ====================================
        List<WkItem> allCustomerItems = this.customerPickerCallback.prepareAllItems();

        // ====================================
        // 準備顯示文字
        // ====================================
        this.settingSysNotifyService.prepareTiggerShowInfo(
                this.settingInfo,
                allCategoryItems,
                allCustomerItems);
    }

    /**
     * 更新項目通知開關
     * 
     * @param notifyType
     * @param isMail
     */
    public void updateNotifyTypeSwitch(NotifyType notifyType, String notifyModeStr) {

        NotifyMode notifyMode = NotifyMode.safeValueOf(notifyModeStr);
        if (notifyMode == null) {
            String message = "無法解析通知模式:[" + notifyModeStr + "], 請洽系統人員";
            log.error(message);
            MessagesUtils.showError(message);
            return;
        }

        try {

            // ====================================
            // 取得觸發通知類別的開關設定值
            // ====================================
            SettingSysNotifyTypeInfo settingSysNotifyTypeInfo = this.settingInfo.getTypeInfo().get(notifyType);
            Map<NotifyMode, Boolean> openSwitch = settingSysNotifyTypeInfo.getOpenSwitch();

            Boolean isOpen = Boolean.valueOf(openSwitch.get(notifyMode) + "");

            // ====================================
            // update
            // ====================================
            this.settingSysNotifyService.updateNotifySwitch(
                    SecurityFacade.getUserSid(),
                    SecurityFacade.getUserSid(),
                    notifyType,
                    notifyMode,
                    isOpen);

        } catch (Exception e) {
            String message = WkMessage.PROCESS_FAILED + ":" + e.getMessage();
            MessagesUtils.showError(message);
            log.error(message, e);
            this.init();
        }
    }

    /**
     * 更新項目通知開關
     * 
     * @param notifyType
     * @param isMail
     */
    public void updateMainSwitch() {
        // ====================================
        // update
        // ====================================

        try {

            this.settingSysNotifyService.updateMainSwitch(
                    SecurityFacade.getUserSid(),
                    SecurityFacade.getUserSid(),
                    this.settingInfo.isOpenMainSwich());

        } catch (Exception e) {
            String message = WkMessage.PROCESS_FAILED + ":" + e.getMessage();
            MessagesUtils.showError(message);
            log.error(message, e);
            this.init();
        }
    }

    /**
     * 開啟mail 設定視窗
     */
    public void event_open_setting_mailAddr_dlg() {
        try {
            this.findLoginUserSetting();
            this.displayController.update("notify_setting_info");
        } catch (Exception e) {
            String message = WkMessage.PROCESS_FAILED + ":" + e.getMessage();
            MessagesUtils.showError(message);
            log.error(message, e);
            return;
        }

        this.displayController.showPfWidgetVar(this.MAIL_SETTING_DLG_ID);

    }

    /**
     * 更新通知 mail 設定
     */
    public void event_setting_mailAddr_confirm() {

        // ====================================
        // 更新設定資料
        // ====================================
        try {

            this.settingSysNotifyService.updateNotifyMailAddrs(
                    SecurityFacade.getUserSid(),
                    SecurityFacade.getUserSid(),
                    this.editMailAddrs);

        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            String message = WkMessage.PROCESS_FAILED + ":" + e.getMessage();
            MessagesUtils.showError(message);
            log.error(message, e);
            return;
        }

        // ====================================
        // 更新畫面資訊
        // ====================================
        this.init();

        // ====================================
        // 關閉設定視窗
        // ====================================
        this.displayController.hidePfWidgetVar(this.MAIL_SETTING_DLG_ID);
    }

    // ========================================================================
    // 需求項目選單
    // ========================================================================

    /**
     * 需求項目選單 - 開啟
     */
    public void event_dialog_categoryTree_open(NotifyType targetNotifyType) {

        // 記錄編輯目標項目別
        this.targetNotifyType = targetNotifyType;

        try {
            // 初始化需求項目樹
            this.categoryTreePicker = new TreePickerComponent(categoryPickerCallback, "需求類別");
            this.categoryTreePicker.setAlwaysOnlyAvtive(true);
            this.categoryTreePicker.setAlwaysContainFollowing(true);
            this.categoryTreePicker.setPropagateSelectionUp(true);
            this.categoryTreePicker.rebuild();
        } catch (Exception e) {
            String message = "開啟【需求類別】設定視窗失敗！" + e.getMessage();
            log.error(message, e);
            MessagesUtils.showError(message);
            return;
        }

        displayController.showPfWidgetVar("wv_dlg_categoryCollection");
    }

    /**
     * 需求項目選單 - 確認
     */
    public void event_dialog_categoryTree_confirm() {

        // ====================================
        // 由元件中取回被選擇資料, 並轉 SID
        // ====================================
        List<String> selectedCategorySids = Lists.newArrayList();
        // 由元件中取回被選擇資料, 並轉 SID
        if (WkStringUtils.notEmpty(this.categoryTreePicker.getSelectedItems())) {
            for (WkItem item : this.categoryTreePicker.getSelectedItems()) {
                selectedCategorySids.add(item.getSid());
            }
        }

        // ====================================
        // 取得全部項目
        // ====================================
        // 取得全部 (大中小類) (有過濾斷頭項目)
        List<WkItem> allItems = Lists.newArrayList();
        try {
            allItems = this.categoryPickerCallback.prepareAllItems();
        } catch (Exception e) {
            String message = WkMessage.PROCESS_FAILED + e.getMessage();
            log.error(message, e);
            MessagesUtils.showError(message);
            this.init();
            return;
        }

        // 僅留下小類
        List<WkItem> allSmallCategoryItems = this.treePickerCategoryHelper.filterOnlySmallCategory(allItems);

        // ====================================
        // 收集沒有選的 （黑名單）
        // ====================================
        // 過濾掉已選擇的 (白名單轉黑名單)
        List<String> blackList = allSmallCategoryItems.stream()
                .filter(wkItem -> !selectedCategorySids.contains(wkItem.getSid()))
                .map(WkItem::getSid)
                .collect(Collectors.toList());

        // ====================================
        // update
        // ====================================
        try {
            this.settingSysNotifyService.updateBlackList(
                    SecurityFacade.getUserSid(),
                    SecurityFacade.getUserSid(),
                    this.targetNotifyType,
                    NotifyFilterType.SMALL_CATEGORY,
                    blackList);

        } catch (Exception e) {
            String message = WkMessage.PROCESS_FAILED + e.getMessage();
            log.error(message, e);
            MessagesUtils.showError(message);
            this.init();
            return;
        }

        // ====================================
        // 畫面重整
        // ====================================
        this.init();

        displayController.hidePfWidgetVar("wv_dlg_categoryCollection");
    }

    /**
     * 需求項目選單 - callback 事件
     */
    protected final TreePickerCallback categoryPickerCallback = new TreePickerCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = 386475515405954611L;

        /**
         * 準備所有的項目
         * 
         * @return
         * @throws Exception
         */
        @Override
        public List<WkItem> prepareAllItems() throws Exception {
            // 取得類別項目
            List<WkItem> allItems = componentHelper.prepareAllCategoryItems();
            // 過濾掉停用項目
            return allItems.stream().filter(item -> item.isActive()).collect(Collectors.toList());
        }

        /**
         * 準備已選擇項目
         * 
         * @return
         */
        @Override
        public List<String> prepareSelectedItemSids() throws Exception {

            // ====================================
            // 查詢黑名單項目 (未勾選項目)
            // ====================================
            List<String> blockList = settingSysNotifyService.findBlackListByUserSidAndNotifyType(
                    SecurityFacade.getUserSid(),
                    targetNotifyType,
                    NotifyFilterType.SMALL_CATEGORY);

            // ====================================
            // 取得全部小類項目
            // ====================================
            List<WkItem> allSmallCategoryItems = treePickerCategoryHelper.filterOnlySmallCategory(
                    this.prepareAllItems());

            // ====================================
            // 收集
            // ====================================
            List<String> selectedItemSids = Lists.newArrayList();
            for (WkItem wkItem : allSmallCategoryItems) {
                // 為黑名單時略過
                if (blockList.contains(wkItem.getSid())) {
                    continue;
                }

                selectedItemSids.add(wkItem.getSid());
            }
            return selectedItemSids;
        }

        @Override
        public List<String> prepareDisableItemSids() throws Exception {
            return Lists.newArrayList();
        }
    };

    // ========================================================================
    // 廳主選單
    // ========================================================================
    /**
     * 廳主選單 - 開啟
     */
    public void event_dialog_customer_open(NotifyType targetNotifyType) {
        // 記錄編輯目標項目別
        this.targetNotifyType = targetNotifyType;

        try {

            // 選擇器設定資料
            MultItemPickerConfig config = new MultItemPickerConfig();
            config.setDefaultShowMode(MultItemPickerShowMode.LIST);
            config.setTreeModePrefixName("廳主");
            config.setHideTopArea(true);

            // 選擇器初始化
            this.customerPicker = new MultItemPickerComponent(config, customerPickerCallback);

        } catch (Exception e) {
            String message = "開啟【廳主】設定視窗失敗！" + e.getMessage();
            log.error(message, e);
            MessagesUtils.showError(message);
            this.init();
            return;
        }

        displayController.showPfWidgetVar(this.CUSTOMER_PICKER_ID);
    }

    /**
     * 廳主選單 - 確認
     */
    public void event_dialog_customer_confirm() {
        // ====================================
        // 取得已選擇的廳主
        // ====================================
        List<String> selectedCustSids = this.customerPicker.getSelectedItems().stream()
                .map(WkItem::getSid)
                .collect(Collectors.toList());

        // ====================================
        // 取得全部項目
        // ====================================
        List<WkItem> allCustomers = customerPickerCallback.prepareAllItems();

        // ====================================
        // 收集沒有選的 （黑名單）
        // ====================================
        // 過濾掉已選擇的 (白名單轉黑名單)
        List<String> blackList = allCustomers.stream()
                .filter(wkItem -> !selectedCustSids.contains(wkItem.getSid()))
                .map(WkItem::getSid)
                .collect(Collectors.toList());

        // ====================================
        // update
        // ====================================
        try {
            this.settingSysNotifyService.updateBlackList(
                    SecurityFacade.getUserSid(),
                    SecurityFacade.getUserSid(),
                    this.targetNotifyType,
                    NotifyFilterType.COSTOMER,
                    blackList);

        } catch (Exception e) {
            String message = WkMessage.PROCESS_FAILED + e.getMessage();
            log.error(message, e);
            MessagesUtils.showError(message);
            this.init();
            return;
        }

        // ====================================
        // 畫面重整
        // ====================================
        this.init();

        displayController.hidePfWidgetVar(this.CUSTOMER_PICKER_ID);
    }

    /**
     * 可執行單位設定選單 callback
     */
    private final MultItemPickerCallback customerPickerCallback = new MultItemPickerCallback() {

        /**
         * 
         */
        private static final long serialVersionUID = 6654815709368619111L;

        /**
         * 取得所有可選擇單位
         */
        @Override
        public List<WkItem> prepareAllItems() throws SystemDevelopException {
            // 取得所有廳主
            return multItemPickerByCustomerHelper.prepareAllItems();
        }

        /**
         * 取得已選擇部門
         */
        @Override
        public List<WkItem> prepareSelectedItems() throws SystemDevelopException {
            // ====================================
            // 查詢黑名單項目 (未勾選項目)
            // ====================================
            List<String> blockList = settingSysNotifyService.findBlackListByUserSidAndNotifyType(
                    SecurityFacade.getUserSid(),
                    targetNotifyType,
                    NotifyFilterType.COSTOMER);

            // ====================================
            // 取得所有廳主
            // ====================================
            List<WkItem> alItems = multItemPickerByCustomerHelper.prepareAllItems();

            // ====================================
            // 扣除黑名單項目
            // ====================================
            return alItems.stream()
                    .filter(wkItem -> !blockList.contains(wkItem.getSid()))
                    .collect(Collectors.toList());
        }

        /**
         * 準備 disable 的單位
         */
        @Override
        public List<String> prepareDisableItemSids() throws SystemDevelopException {
            // 不需要 Disable
            return Lists.newArrayList();
        }
    };

}
