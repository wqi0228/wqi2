/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.view.to.search.query;

import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.vo.query.search.SearchQuery;
import com.cy.work.common.enums.InstanceStatus;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.LocalDate;

/**
 * Search02MBean 查詢欄位
 *
 * @author kasim
 */
public class SearchQuery02 extends SearchQuery implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7271367827420150880L;
    @Getter
    @Setter
    /** 選擇的大類 */
    private String selectBigCategorySid;
    @Getter
    @Setter
    /** 類別組合(大類 sid) */
    private List<String> bigDataCateSids = Lists.newArrayList();
    @Getter
    @Setter
    /** 類別組合(中類 sid) */
    private List<String> middleDataCateSids = Lists.newArrayList();
    @Getter
    @Setter
    /** 類別組合(小類 sid) */
    private List<String> smallDataCateSids = Lists.newArrayList();
    @Getter
    @Setter
    /** 待審核人員 */
    private String pendingSignOnUserName;
    @Getter
    @Setter
    /** 需求單位 */
    private Set<Integer> requireDepts = Sets.newHashSet();
    @Getter
    @Setter
    /** 需求人員 */
    private String trCreatedUserName;
    @Getter
    @Setter
    /** 模糊搜尋 */
    private String fuzzyText;
    @Getter
    @Setter
    /** 時間切換的index */
    private int dateTypeIndex;
    @Getter
    @Setter
    /** 立單區間(起) */
    private Date startDate;
    @Getter
    @Setter
    /** 立單區間(訖) */
    private Date endDate;
    @Getter
    @Setter
    /** 異動區間(起) */
    private Date startUpdatedDate;
    @Getter
    @Setter
    /** 異動區間(訖) */
    private Date endUpdatedDate;
    @Getter
    @Setter
    /** 轉發單位 */
    private List<String> forwardDepts = Lists.newArrayList();
    @Getter
    @Setter
    /** 緊急度 */
    private List<String> urgencyList;
    @Getter
    @Setter
    /** 需求單號 */
    private String requireNo;
    @Getter
    @Setter
    /** 需求單位審核狀態 */
    private List<String> selectStatuses;

    public SearchQuery02(ReportType reportType) {
        this.reportType = reportType;
    }

    /**
     * 清除
     *
     * @param requireDepts
     */
    public void clear(Set<Integer> requireDepts) {
        this.init();
        this.initDefault(requireDepts);
    }

    /**
     * 初始化
     */
    private void init() {
        // 共用查詢條件初始化
        this.publicConditionInit();
        this.selectBigCategorySid = null;
        this.bigDataCateSids = Lists.newArrayList();
        this.middleDataCateSids = Lists.newArrayList();
        this.smallDataCateSids = Lists.newArrayList();
        this.pendingSignOnUserName = null;
        this.trCreatedUserName = null;
        this.fuzzyText = null;
        this.dateTypeIndex = 0;
        this.startDate = null;
        this.endDate = null;
        this.selectStatuses = Lists.newArrayList();
        this.clearAdvance();
    }

    /**
     * 清除進階選項
     */
    public void clearAdvance() {
        startUpdatedDate = null;
        endUpdatedDate = null;
        urgencyList = null;
        requireNo = null;
        forwardDepts.clear();
    }

    /**
     * 初始化報表預設值
     *
     * @param requireDepts
     */
    private void initDefault(Set<Integer> requireDepts) {
        this.changeDateIntervalThisMonth();
        this.requireDepts = requireDepts;
        this.selectStatuses = Lists.newArrayList(InstanceStatus.NEW_INSTANCE,
                InstanceStatus.WAITING_FOR_APPROVE, InstanceStatus.RECONSIDERATION,
                InstanceStatus.APPROVING).stream()
                .map(each -> each.getValue())
                .collect(Collectors.toList());
    }

    /**
     * 上個月
     */
    public void changeDateIntervalPreMonth() {
        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate lastDate = new LocalDate(date).minusMonths(1);
        this.dateTypeIndex = 0;
        this.startDate = lastDate.dayOfMonth().withMinimumValue().toDate();
        this.endDate = lastDate.dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 本月份
     */
    public void changeDateIntervalThisMonth() {
        this.dateTypeIndex = 1;
        this.startDate = new LocalDate().dayOfMonth().withMinimumValue().toDate();
        this.endDate = new LocalDate().dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 下個月
     */
    public void changeDateIntervalNextMonth() {
        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate nextDate = new LocalDate(date).plusMonths(1);
        this.dateTypeIndex = 2;
        this.startDate = nextDate.dayOfMonth().withMinimumValue().toDate();
        this.endDate = nextDate.dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 今日
     */
    public void changeDateIntervalToDay() {
        this.dateTypeIndex = 3;
        this.startDate = new Date();
        this.endDate = new Date();
    }

}
