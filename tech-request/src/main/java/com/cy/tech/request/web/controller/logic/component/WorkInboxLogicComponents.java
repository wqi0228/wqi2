/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.logic.component;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.TrAlertInboxSendGroupService;
import com.cy.tech.request.logic.service.TrAlertInboxService;
import com.cy.tech.request.logic.utils.ToolsDate;
import com.cy.tech.request.logic.vo.SimpleDateFormatEnum;
import com.cy.tech.request.vo.enums.ForwardType;
import com.cy.tech.request.vo.enums.ReadRecordStatus;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.TrAlertInbox;
import com.cy.tech.request.vo.require.TrAlertInboxSendGroup;
import com.cy.tech.request.web.controller.view.vo.InBoxGroupItemVO;
import com.cy.tech.request.web.controller.view.vo.SentBackReadStatusVO;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.vo.value.to.ReadRecordTo;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WorkInboxLogicComponents implements InitializingBean, Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 2135364710526917303L;
    private static WorkInboxLogicComponents instance;
	@Autowired
	private TrAlertInboxService trAlertInboxService;
	@Autowired
	private TrAlertInboxSendGroupService trAlertInboxSendGroupService;
	@Autowired
	private RequireService requireService;

	public static WorkInboxLogicComponents getInstance() {
		return instance;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		WorkInboxLogicComponents.instance = this;
	}

	public void syncInBoxReadStatus() {
		List<TrAlertInbox> activeAll = trAlertInboxService.findActiveAll();
		activeAll.forEach(item -> {
			try {
				TrAlertInbox nowObj = trAlertInboxService.getTrAlertInboxBySid(item.getSid());
				if (nowObj.getStatus() == null || nowObj.getStatus().equals(Activation.INACTIVE)) {
					log.info("該轉寄資訊已回收 alert_sid [" + nowObj.getSid() + "]");
					return;
				}
				if (nowObj.getRead_record() != null) {
					log.info("該轉寄資訊已存在已未讀資訊 alert_sid [" + nowObj.getSid() + "]");
					return;
				}
				Require re = requireService.findByReqSid(nowObj.getRequireSid());
				if (re.getReadRecordGroup() == null || re.getReadRecordGroup().getRecords() == null) {
					nowObj.setRead_record(ReadRecordStatus.UNREAD);
					trAlertInboxService.updateWorkInboxOnly(nowObj);
					log.info("該需求單[" + re.getRequireNo() + "]無已讀資訊,Falert_sid [" + nowObj.getSid() + "],更新成未讀");
					return;
				}
				if (nowObj.getForwardType().equals(ForwardType.FORWARD_MEMBER)) {
					User user = WkUserCache.getInstance().findBySid(nowObj.getReceive());
					ReadRecordTo readRecordTo = re.getReadRecordGroup().getRecords().get(String.valueOf(nowObj.getReceive()));
					if (readRecordTo == null) {
						nowObj.setRead_record(ReadRecordStatus.UNREAD);
						trAlertInboxService.updateWorkInboxOnly(nowObj);
						log.info("該需求單[" + re.getRequireNo() + "]被轉寄者[" + user.getName() + "]無已讀資訊,Falert_sid [" + nowObj.getSid() + "],更新成未讀");
					} else if (ReadRecordType.HAS_READ.equals(readRecordTo.getType())) {
						nowObj.setRead_record(ReadRecordStatus.READ);
						nowObj.setRead_dt(readRecordTo.getReadDay());
						trAlertInboxService.updateWorkInboxOnly(nowObj);
						log.info("該需求單[" + re.getRequireNo() + "]被轉寄者[" + user.getName() + "]有已讀資訊,Falert_sid [" + nowObj.getSid() + "],更新成已讀");
					} else {
						nowObj.setRead_record(ReadRecordStatus.UNREAD);
						trAlertInboxService.updateWorkInboxOnly(nowObj);
						log.info("該需求單[" + re.getRequireNo() + "]被轉寄者[" + user.getName() + "]無已讀資訊,Falert_sid [" + nowObj.getSid() + "],更新成未讀");
					}
				} else if (nowObj.getForwardType().equals(ForwardType.FORWARD_DEPT)) {
					Org dep = WkOrgCache.getInstance().findBySid(nowObj.getReceiveDep());
					Set<String> keys = re.getReadRecordGroup().getRecords().keySet();
					boolean isReaded = false;
					Date readDate = null;
					for (String key : keys) {
						if (Strings.isNullOrEmpty(key)) {
							continue;
						}
						User user = WkUserCache.getInstance().findBySid(Integer.valueOf(key));
						ReadRecordTo readRecordTo = re.getReadRecordGroup().getRecords().get(String.valueOf(user.getSid()));
						if (readRecordTo != null && ReadRecordType.HAS_READ.equals(readRecordTo.getType())) {
							isReaded = true;
							readDate = readRecordTo.getReadDay();
							break;
						}
					}
					if (isReaded) {
						nowObj.setRead_record(ReadRecordStatus.READ);
						nowObj.setRead_dt(readDate);
						trAlertInboxService.updateWorkInboxOnly(nowObj);
						log.info("該需求單[" + re.getRequireNo() + "]被轉寄部門已有人閱讀[" + WkOrgUtils.getOrgName(dep) + "]有已讀資訊,Falert_sid [" + nowObj.getSid() + "],更新成已讀");
					} else {
						nowObj.setRead_record(ReadRecordStatus.UNREAD);
						trAlertInboxService.updateWorkInboxOnly(nowObj);
						log.info("該需求單[" + re.getRequireNo() + "]被轉寄部門[" + WkOrgUtils.getOrgName(dep) + "]無已讀資訊,Falert_sid [" + nowObj.getSid() + "],更新成未讀");
					}
				}

			} catch (Exception e) {
				log.error("syncInBoxReadStatus ERROR", e);
			}
		});

	}

	public List<InBoxGroupItemVO> getInBoxGroupItemBySendUserSid(String requireSid, Integer loginUserSid) {
		User sendUser = WkUserCache.getInstance().findBySid(loginUserSid);
		List<TrAlertInboxSendGroup> wRInboxSendGroups = trAlertInboxSendGroupService.getInboxSendGroupByRequireSidAndLoginUserSid(requireSid,
		        loginUserSid);
		List<InBoxGroupItemVO> inBoxGroupItemVOs = Lists.newArrayList();
		wRInboxSendGroups.forEach(item -> {
			InBoxGroupItemVO inboxSendGroup = new InBoxGroupItemVO(item.getSid(), item.getMessageCss(),
			        (ForwardType.FORWARD_DEPT.equals(item.getForwardType())), item.getSendTo(), sendUser.getName(),
			        ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(),
			                item.getCreatedDate()));
			inBoxGroupItemVOs.add(inboxSendGroup);
		});
		return inBoxGroupItemVOs;
	}

	public List<SentBackReadStatusVO> getSentBackReadStatusVOByInboxGroupSid(String inbox_group_sid) {
		List<SentBackReadStatusVO> sentBackReadStatusVOs = Lists.newArrayList();
		try {
			List<TrAlertInbox> workInboxs = trAlertInboxService.findByInboxByInboxGroupSid(inbox_group_sid);
			workInboxs.forEach(item -> {
				boolean canCancel = false;
				if ((item.getRead_record() == null || item.getRead_record().equals(ReadRecordStatus.UNREAD))
				        && item.getStatus().equals(Activation.ACTIVE)) {
					canCancel = true;
				}
				SentBackReadStatusVO sv = new SentBackReadStatusVO(
				        item.getSid(),
				        WkOrgCache.getInstance().findNameBySid(item.getReceiveDep()),
				        ((item.getReceive() != null) ? WkUserCache.getInstance().findBySid(item.getReceive()).getName() : ""),
				        ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getRead_dt()),
				        // 若狀態為失效,代表最後更新時間變為回收時間
				        (item.getStatus().equals(Activation.INACTIVE))
				                ? ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getUpdatedDate())
				                : "",
				        (item.getForwardType().equals(ForwardType.FORWARD_DEPT)),
				        canCancel,
				        item.getReceive(),
				        item.getReceiveDep());
				sentBackReadStatusVOs.add(sv);
			});
		} catch (Exception e) {
			log.error("getSentBackReadStatusVOByInboxGroupSid", e);
		}
		return sentBackReadStatusVOs;
	}

	public void doCancelWorkInBox(List<String> sids, Integer loginUserSid) {
		if (sids == null || sids.isEmpty()) {
			Preconditions.checkState(false, "尚未選擇回收項目，請確認！！");
		}
		List<TrAlertInbox> workInboxs = trAlertInboxService.getWorkInboxBySids(sids);
		List<TrAlertInbox> readedWorkInBoxs = workInboxs.stream().filter(each -> ReadRecordStatus.READ.equals(each.getRead_record()))
		        .collect(Collectors.toList());
		if (readedWorkInBoxs != null && !readedWorkInBoxs.isEmpty()) {
			StringBuilder moveErrorMessage = new StringBuilder();
			readedWorkInBoxs.forEach(item -> {
				if (!Strings.isNullOrEmpty(moveErrorMessage.toString())) {
					moveErrorMessage.append("<br>");
				}
				if (item.getForwardType().equals(ForwardType.FORWARD_DEPT)) {
					moveErrorMessage.append("[" + WkOrgCache.getInstance().findNameBySid(item.getReceiveDep()) + "]" + "已有人閱讀,不可回收");
				} else {
					User canRemoveUser = WkUserCache.getInstance().findBySid(item.getReceive());
					moveErrorMessage.append("[" + canRemoveUser.getName() + "]" + "已閱讀,不可回收");
				}
			});
			Preconditions.checkState(false, moveErrorMessage.toString());
		}

		List<TrAlertInbox> canceledWorkInBox = workInboxs.stream().filter(each -> each.getStatus().equals(Activation.INACTIVE))
		        .collect(Collectors.toList());
		if (canceledWorkInBox != null && !canceledWorkInBox.isEmpty()) {
			StringBuilder cancelErrorMessage = new StringBuilder();
			canceledWorkInBox.forEach(item -> {
				if (!Strings.isNullOrEmpty(cancelErrorMessage.toString())) {
					cancelErrorMessage.append("<br>");
				}
				if (item.getForwardType().equals(ForwardType.FORWARD_DEPT)) {
					cancelErrorMessage.append(
					        "[" + WkOrgCache.getInstance().findNameBySid(item.getReceiveDep()) + "]"
					                + "已回收,不可再次回收");
				} else {
					User canRemoveUser = WkUserCache.getInstance().findBySid(item.getReceive());
					cancelErrorMessage.append("[" + canRemoveUser.getName() + "]" + "已回收,不可再次回收");
				}
			});
			Preconditions.checkState(false, cancelErrorMessage.toString());
		}

		workInboxs.forEach(item -> {
			item.setStatus(Activation.INACTIVE);
			trAlertInboxService.updateWorkInbox(item, loginUserSid);
		});

		String groupID = workInboxs.get(0).getAlert_group_sid();
		TrAlertInboxSendGroup wRInboxSendGroup = trAlertInboxSendGroupService.getTrAlertInboxSendGroupBySid(groupID);
		List<TrAlertInbox> workInboxsUpdateOver = trAlertInboxService.findByInboxByInboxGroupSid(groupID);
		List<TrAlertInbox> selWorkInboxUpdateOver = workInboxsUpdateOver.stream().filter(each -> each.getStatus().equals(Activation.ACTIVE))
		        .collect(Collectors.toList());

		StringBuilder sendTo = new StringBuilder("");
		selWorkInboxUpdateOver.forEach(item -> {
			if (!Strings.isNullOrEmpty(sendTo.toString())) {
				sendTo.append(",");
			}
			if (item.getForwardType().equals(ForwardType.FORWARD_DEPT)) {
				sendTo.append(WkOrgCache.getInstance().findNameBySid(item.getReceiveDep()));
			} else {
				User user = WkUserCache.getInstance().findBySid(item.getReceive());
				sendTo.append(user.getName());
			}
		});
		wRInboxSendGroup.setSendTo(sendTo.toString());
		trAlertInboxSendGroupService.updateInboxSendGroup(wRInboxSendGroup, loginUserSid);
	}

}
