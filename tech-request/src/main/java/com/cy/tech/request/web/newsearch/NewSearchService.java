package com.cy.tech.request.web.newsearch;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.ReqConfirmDepSearchHelper;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.helper.AssignSendSearchHelper;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepService;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.logic.vo.RequireConfirmDepVO;
import com.cy.tech.request.vo.converter.SetupInfoToConverter;
import com.cy.tech.request.vo.enums.ReqConfirmDepProgStatus;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.value.to.SetupInfoTo;
import com.cy.tech.request.web.newsearch.newsearch02.NewSearch02ConditionVO;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.logic.lib.manager.WorkCommonReadRecordManager;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Service
@Slf4j
public class NewSearchService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7096519605789153664L;
    @Autowired
    private transient NativeSqlRepository nativeSqlRepository;
    @Autowired
    private transient ReqularPattenUtils reqularPattenUtils;
    @Autowired
    private transient WkOrgCache wkOrgCache;
    @Autowired
    private transient URLService urlService;
    @Autowired
    private transient SearchService searchService;
    @Autowired
    private transient RequireConfirmDepService requireConfirmDepService;
    @Autowired
    private transient ReqConfirmDepSearchHelper reqConfirmDepSearchHelper;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchResultHelper searchResultHelper;
    @Autowired
    transient private AssignSendSearchHelper assignSendSearchHelper;
    @Autowired
    transient private WorkCommonReadRecordManager workCommonReadRecordManager;

    /**
     * 主查詢
     * 
     * @param conditionVO
     * @return
     */
    public List<NewSearchResultVO> queryByCondition(
            ReportType reportType,
            NewSearchConditionVO conditionVO) {

        Map<String, Object> parameters = Maps.newHashMap();

        // ====================================
        // 需求單號
        // ====================================
        // 由模糊查詢中解析是否為需求單號
        if (WkStringUtils.notEmpty(conditionVO.getFuzzyText())) {
            conditionVO.setRequireNo(
                    reqularPattenUtils.getRequireNo(
                            SecurityFacade.getCompanyId(),
                            conditionVO.getFuzzyText()));
        }

        // ====================================
        // SELECT
        // ====================================
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT tr.require_sid            as requireSid, ");
        sql.append("       tr.require_no             as requireNo, ");
        sql.append("       tid.field_content         as requireTheme_src, ");
        sql.append("       tr.urgency                as urgency, ");
        sql.append("       tr.create_dt              as createdDate, ");
        sql.append("       tr.update_dt              as updateDate, ");
        sql.append("       ckm.big_category_name     as bigName, ");
        sql.append("       ckm.middle_category_name  as middleName, ");
        sql.append("       ckm.small_category_name   as smallName, ");
        sql.append("       tr.dep_sid                as createDepSid, ");
        sql.append("       tr.create_usr             as createdUserSid, ");
        sql.append("       tr.has_tech_manager_sign  as hasTechManagerSign, ");
        sql.append("       tr.require_suspended_code as requireSuspendedCode, ");
        sql.append("       tr.require_status         as requireStatus, ");
        sql.append("       tr.hope_dt                as hopeDate, ");
        sql.append("       assignInfo.info           as assignInfo_src, ");
        sql.append("       assignInfo.create_dt      as assignDate, ");
        sql.append("       sendInfo.info             as sendInfo_src, ");
        sql.append("       sendInfo.create_dt        as sendDate, ");
        // 組共通 select 欄位
        sql.append(this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire());

        // ====================================
        // FROM (JOIN) TABLE
        // ====================================
        sql.append("FROM   tr_require tr ");

        // ------------------
        // JOIN：要過濾的分派單位 (應為限制查詢條件 + 畫面選擇條件)
        // ------------------
        if (WkStringUtils.notEmpty(conditionVO.getFilterAssignDepSids())) {
            String assignSchDepsStr = conditionVO.getFilterAssignDepSids().stream()
                    .map(sid -> sid + "").collect(Collectors.joining(","));
            sql.append("       INNER JOIN tr_assign_send_search_info assignSch ");
            sql.append("               ON assignSch.type = 0 ");
            sql.append("               AND tr.require_sid = assignSch.require_sid ");
            sql.append("               AND assignSch.dep_sid IN (" + assignSchDepsStr + ") ");
        }

        // ------------------
        // JOIN：過濾通知單位
        // ------------------
        if (WkStringUtils.notEmpty(conditionVO.getFilterSendDepSids())) {
            String sendSchDepsStr = conditionVO.getFilterSendDepSids().stream()
                    .map(sid -> sid + "").collect(Collectors.joining(","));

            sql.append("       INNER JOIN tr_assign_send_search_info sendSch ");
            sql.append("               ON sendSch.type = 1 ");
            sql.append("               AND tr.require_sid = sendSch.require_sid ");
            sql.append("               AND sendSch.dep_sid IN (" + sendSchDepsStr + ")");
        }

        // ------------------
        // join 主題 資料
        // ------------------
        sql.append("       INNER JOIN (SELECT tid.require_sid, ");
        sql.append("                          tid.field_content ");
        sql.append("                   FROM   tr_index_dictionary tid ");
        sql.append("                   WHERE  1 = 1 ");
        sql.append("                          AND tid.field_name = '主題') AS tid ");
        sql.append("               ON tr.require_sid = tid.require_sid ");

        // ------------------
        // join 需求分類資料
        // ------------------
        sql.append("       INNER JOIN tr_category_key_mapping ckm ");
        sql.append("               ON tr.mapping_sid = ckm.key_sid ");

        // 取得需求分類選項 (不分大中小類)
        Set<String> categoryCollectionSids = Sets.newHashSet();;
        if (WkStringUtils.notEmpty(conditionVO.getCategoryCollectionSids())) {
            categoryCollectionSids.addAll(conditionVO.getCategoryCollectionSids());
        }
        // 將單獨選擇的大項, 加入
        if (WkStringUtils.notEmpty(conditionVO.getBigCategorySid())) {
            categoryCollectionSids.add(conditionVO.getBigCategorySid());
        }
        // 不為空時，加入過濾條件
        if (WkStringUtils.notEmpty(categoryCollectionSids)) {
            String categoryCollectionSidsStr = categoryCollectionSids.stream()
                    .collect(Collectors.joining("',' ", "'", "'"));

            sql.append("          AND ( ckm.big_category_sid IN (" + categoryCollectionSidsStr + ") ");
            sql.append("                OR ckm.middle_category_sid IN (" + categoryCollectionSidsStr + ") ");
            sql.append("                OR ckm.small_category_sid IN (" + categoryCollectionSidsStr + ") ");
            sql.append("              ) ");
        }

        // ------------------
        // 分派單位資料 (V7.0 之後僅會有一筆)
        // ------------------
        sql.append("       LEFT JOIN tr_assign_send_info assignInfo ");
        sql.append("         ON assignInfo.require_sid = tr.require_sid ");
        sql.append("        AND assignInfo.type = 0 ");

        // ------------------
        // 通知單位資料 (V7.0 之後僅會有一筆)
        // ------------------
        sql.append("       LEFT JOIN tr_assign_send_info sendInfo ");
        sql.append("         ON sendInfo.require_sid = tr.require_sid ");
        sql.append("        AND sendInfo.type = 1 ");

        // ------------------
        // 系統別、閱讀記錄
        // ------------------
        sql.append(this.searchConditionSqlHelper.prepareCommonJoin(
                SecurityFacade.getUserSid()));

        // ------------------
        // 模糊搜尋
        // ------------------
        if (WkStringUtils.notEmpty(conditionVO.getFuzzyText())
                && WkStringUtils.isEmpty(conditionVO.getRequireNo())) {
            // 索引資料表
            sql.append("       LEFT JOIN tr_index_dictionary indexDic ");
            sql.append("         ON tr.require_sid = indexDic.require_sid ");
            sql.append("        AND indexDic.field_name is not null ");
            sql.append("        AND indexDic.field_name != '' ");

            // 需求資訊補充
            sql.append("       LEFT JOIN tr_require_trace trace ");
            sql.append("         ON trace.require_sid = tr.require_sid ");
        }

        // ====================================
        // WHERE
        // ====================================
        sql.append("WHERE  tr.require_sid IS NOT NULL ");

        // 依據報表類型，自動兜組報表的限定查詢條件
        this.prepareWhereConditionByReport(reportType, conditionVO, sql, parameters);
        // 兜組使用者查詢條件
        this.prepareWhereCondition(conditionVO, sql, parameters);

        // ====================================
        //
        // ====================================
        sql.append(" GROUP  BY tr.require_sid ");
        sql.append(" ORDER  BY tr.update_dt DESC");

        // ====================================
        // 查詢
        // ====================================
        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                reportType, sql.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                reportType, SecurityFacade.getUserSid());

        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();

        List<NewSearchResultVO> results = nativeSqlRepository.getResultList(
                sql.toString(), parameters, NewSearchResultVO.class);

        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((results == null) ? 0 : results.size());

        if (WkStringUtils.notEmpty(results)) {

            // ====================================
            // 欄位轉置
            // ====================================
            // 解析資料-開始
            usageRecord.parserDataStart();
            for (NewSearchResultVO newSearchResultVO : results) {
                this.prepareRowColumn(newSearchResultVO);
            }
            // 解析資料-結束
            usageRecord.parserDataEnd();

            // ====================================
            // 查詢條件過濾
            // ====================================
            if (WkStringUtils.notEmpty(results)) {
                // 後續處理-開始
                usageRecord.afterProcessStart();

                // 系統別
                results = results.stream()
                        .filter(each -> this.searchResultHelper.filterCheckItems(
                                each.getCheckItemTypes(),
                                conditionVO.getCheckItemTypes(),
                                false,
                                null))
                        .collect(Collectors.toList());

                // 後續處理-結束
                usageRecord.afterProcessEnd();
            }
        }
        // ====================================
        // 使用記錄
        // ====================================
        usageRecord.saveUsageRecord();

        return results;
    }

    /**
     * 依據報表類型，自動兜組報表的限定查詢條件
     * 
     * @param reportType
     * @param conditionVO
     * @param sql
     * @param parameters
     */
    private void prepareWhereConditionByReport(
            ReportType reportType,
            NewSearchConditionVO conditionVO,
            StringBuffer sql,
            Map<String, Object> parameters) {

        // ====================================
        // 限定需求單 SID
        // ====================================
        switch (reportType) {
        case NEW_SEARCH02:
            if (WkStringUtils.notEmpty(conditionVO.getMustFilter_requireSids())) {
                sql.append(" AND tr.require_sid in ('" + String.join("','", conditionVO.getMustFilter_requireSids()) + "') ");
                // 避免參數過多時, 造成 driver 錯誤, 改為直接組進字串
                // parameters.put("requireSids", conditionVO.getMustFilter_requireSids());
            }
            break;

        case NEW_SEARCH03:
            // 被通知單據查詢，僅被通知單位權限可顯示，已經過濾在被通知單位選單
            // 為了避免使用者因為全部選通知單位避過過濾器，看到無權限的單據
            // 所以當沒有選任何一筆通知單位時，下一個不可能的條件，讓查詢結果為空
            if (WkStringUtils.isEmpty(conditionVO.getFilterSendDepSids())) {
                sql.append(" AND tr.require_sid = 'NEVER_SHOW_RESULT' ");
            }
            break;
        default:
            break;
        }
        // ====================================
        // 製作進度
        // ====================================
        switch (reportType) {
        case NEW_SEARCH02:
            // 需為進行中
            sql.append(" AND tr.require_status = 'PROCESS' ");
            break;
        default:
            break;
        }
    }

    /**
     * @param conditionVO
     * @param sql
     * @param parameters
     */
    private void prepareWhereCondition(
            NewSearchConditionVO conditionVO,
            StringBuffer sql,
            Map<String, Object> parameters) {

        // ====================================
        // 以需求單號查詢 (有輸入時，忽略其他條件)
        // ====================================
        if (WkStringUtils.notEmpty(conditionVO.getRequireNo())) {
            // 需求單號
            sql.append(" AND tr.require_no = :requireNo ");
            parameters.put("requireNo", conditionVO.getRequireNo());
            // 輸入需求單號時，忽略其他查詢條件
            return;
        }

        // ====================================
        // 閱讀狀態
        // ====================================
        if (WkStringUtils.isEmpty(conditionVO.getRequireNo())
                && conditionVO.getSelectReadRecordType() != null) {
            sql.append(this.workCommonReadRecordManager.prepareWhereConditionSQL(
                    conditionVO.getSelectReadRecordType(), "readRecord"));
        }

        // ====================================
        // 結案狀態
        // ====================================
        List<String> selectedColseCode = conditionVO.getSelectedColseCode();
        if (WkStringUtils.isEmpty(conditionVO.getRequireNo())
                && selectedColseCode.size() == 1) {
            sql.append(" AND tr.close_code = :closedCode");
            parameters.put("closedCode", selectedColseCode.get(0));
        }

        // ====================================
        // 模糊查詢 (查詢欄位資料索引 + 需求資訊補充)
        // ====================================
        if (WkStringUtils.isEmpty(conditionVO.getRequireNo())
                && WkStringUtils.notEmpty(conditionVO.getFuzzyText())) {
            // 去前後空白
            String fuzzyText = WkStringUtils.safeTrim(conditionVO.getFuzzyText());

            sql.append(" AND ( ");
            // 欄位索引
            sql.append(
                    WkStringUtils.prepareLikeWhereStamentWithIllegalChar(
                            "indexDic.field_content",
                            fuzzyText,
                            parameters));
            sql.append(" OR ");
            // 追蹤
            sql.append(
                    WkStringUtils.prepareLikeWhereStamentWithIllegalChar(
                            "trace.require_trace_content",
                            fuzzyText,
                            parameters));
            sql.append(") ");
        }

        // ====================================
        // 製作進度
        // ====================================
        if (WkStringUtils.isEmpty(conditionVO.getRequireNo())
                && conditionVO.getRequireStatus() != null) {
            sql.append(" AND tr.require_status = :requireStatus ");
            parameters.put("requireStatus", conditionVO.getRequireStatus().name());
        }
    }

    /**
     * 準備其他欄位資料
     * 
     * @param resultVO
     */
    private void prepareRowColumn(NewSearchResultVO resultVO) {
        if (resultVO == null) {
            return;
        }

        // ====================================
        // 本地端連結網址
        // ====================================
        resultVO.setLocalUrlLink(urlService.createLoacalURLLink(
                URLService.URLServiceAttr.URL_ATTR_M,
                urlService.createSimpleUrlTo(SecurityFacade.getUserSid(), resultVO.getRequireNo(), 1)));

        // ====================================
        //
        // ====================================
        // 需求單位
        resultVO.setCreateDepName(this.wkOrgCache.findNameBySid(resultVO.getCreateDepSid()));
        // 需求人員
        resultVO.setCreatedUserName(WkUserUtils.findNameBySid(resultVO.getCreatedUserSid()));

        // ====================================
        // 主題
        // ====================================
        if (WkStringUtils.notEmpty(resultVO.getRequireTheme_src())) {
            resultVO.setRequireTheme(searchService.combineFromJsonStr(resultVO.getRequireTheme_src()));
        }

        // ====================================
        // 閱讀記錄
        // ====================================
        // 待閱讀
        ReadRecordType readRecordType = workCommonReadRecordManager.transToReadRecordType(
                resultVO.getWaitRead(),
                resultVO.getReadDate());

        resultVO.setReadRecordType(readRecordType);

        // ====================================
        // 分派單位
        // ====================================
        if (WkStringUtils.notEmpty(resultVO.getAssignInfo_src())) {
            // json 轉 物件 SetupInfoTo
            SetupInfoTo assignInfo = new SetupInfoToConverter().convertToEntityAttribute(resultVO.getAssignInfo_src());
            // 取部門設定
            if (assignInfo != null && assignInfo.getDepartment() != null) {
                List<Integer> deps = Lists.newArrayList();
                for (String depsid : assignInfo.getDepartment()) {
                    // catch 資料錯誤 (非字串)
                    try {
                        deps.add(Integer.valueOf(depsid));
                    } catch (Exception e) {
                        WkCommonUtils.logWithStackTrace(
                                InfomationLevel.WARN,
                                "從 tr_assign_send_info 取回的 info 欄位中的部門資料, 有非數字者! RequireSid:[" + resultVO.getRequireSid() + "]");
                    }
                }
                resultVO.setAssignDeps(deps);
            }
        }

        // ====================================
        // 檢查項目 (系統別)
        // ====================================
        // 取得回傳資料字串
        String checkItmesStr = resultVO.getCheckItemNames();
        if (WkStringUtils.notEmpty(checkItmesStr)) {
            // 用 Set 過濾重複
            Set<RequireCheckItemType> currCheckItemTypes = Sets.newHashSet();
            for (String checkItmeStr : checkItmesStr.split(",")) {

                if (WkStringUtils.isEmpty(checkItmeStr)) {
                    continue;
                }

                RequireCheckItemType checkItemType = RequireCheckItemType.safeValueOf(checkItmeStr);
                if (checkItemType == null) {
                    log.warn("無法解析的 RequireCheckItemType : [{}]", checkItmeStr);
                    continue;
                }
                currCheckItemTypes.add(checkItemType);

            }

            // 排序
            List<RequireCheckItemType> sortCheckItemTypes = currCheckItemTypes.stream()
                    .sorted(Comparator.comparing(each -> each.ordinal()))
                    .collect(Collectors.toList());

            resultVO.setCheckItemTypes(sortCheckItemTypes);
            resultVO.setCheckItemsDescr(sortCheckItemTypes.stream().map(RequireCheckItemType::getDescr).collect(Collectors.joining("、")));
        }

        // ====================================
        // 通知單位
        // ====================================
        if (WkStringUtils.notEmpty(resultVO.getSendInfo_src())) {
            // json 轉 物件 SetupInfoTo
            SetupInfoTo assignInfo = new SetupInfoToConverter().convertToEntityAttribute(resultVO.getSendInfo_src());
            // 取部門設定
            if (assignInfo != null && assignInfo.getDepartment() != null) {
                List<Integer> deps = Lists.newArrayList();
                for (String depsid : assignInfo.getDepartment()) {
                    // catch 資料錯誤 (非字串)
                    try {
                        deps.add(Integer.valueOf(depsid));
                    } catch (Exception e) {
                        WkCommonUtils.logWithStackTrace(
                                InfomationLevel.WARN,
                                "從 tr_assign_send_info 取回的 info 欄位中的部門資料, 有非數字者! RequireSid:[" + resultVO.getRequireSid() + "]");
                    }
                }
                resultVO.setSendDeps(deps);
            }
        }
    }

    /**
     * NewSearch02 用的查詢
     * 
     * @param condition
     * @return
     * @throws UserMessageException
     */
    public List<NewSearchResultVO> searchForNewSearch02(NewSearch02ConditionVO conditionVO) throws UserMessageException {

        // ====================================
        // 查詢與登入者相關的需求完成確認單位檔 (RequireConfirmDep)
        // ====================================
        // 查詢

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.NEW_SEARCH02, SecurityFacade.getUserSid());

        usageRecord.dbQueryStart();

        List<RequireConfirmDepVO> requireConfirmDepVOs = this.reqConfirmDepSearchHelper.prepareRequireConfirmDepByLoginUserRelation(
                ReqConfirmDepProgStatus.WAIT_CONFIRM,
                conditionVO.getFilterAssignDepSids(),
                SecurityFacade.getUserSid(),
                SecurityFacade.getCompanyId(),
                conditionVO.getReqConcirmDepOwnerName(),
                true, // 需要取得分派單位
                false); // 未領單不需含特殊可閱

        if (WkStringUtils.isEmpty(requireConfirmDepVOs)) {
            // 第一段就沒資料時，還是要記錄查詢
            usageRecord.dbQueryEnd(0);
            usageRecord.saveUsageRecord();
            return Lists.newArrayList();
        }

        // ====================================
        // 查詢需求單資料
        // ====================================
        // 準備查詢條件
        // 收集 require sid
        List<String> requireSids = requireConfirmDepVOs.stream()
                .map(RequireConfirmDepVO::getRequireSid)
                .distinct()
                .collect(Collectors.toList());

        conditionVO.setMustFilter_requireSids(requireSids);

        // 查詢
        List<NewSearchResultVO> resultVOs = this.queryByCondition(
                ReportType.NEW_SEARCH02,
                (NewSearchConditionVO) conditionVO);

        if (WkStringUtils.isEmpty(resultVOs)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 組裝顯示欄位
        // ====================================
        this.prepareShowContent(resultVOs, requireConfirmDepVOs);

        // ====================================
        // 排序
        // ====================================
        // 為被分派部門排在最上面
        Comparator<NewSearchResultVO> comparator = Comparator.comparing(NewSearchResultVO::isTrueAssignDep).reversed();
        // 分派日期
        comparator = comparator.thenComparing(Comparator.comparing(NewSearchResultVO::getAssignDate).reversed());

        // 排序
        resultVOs = resultVOs.stream()
                .sorted(comparator)
                .collect(Collectors.toList());

        return resultVOs;
    }

    /**
     * 組裝顯示欄位
     * 
     * @param resultVOs
     * @param requireConfirmDepVOs
     */
    private void prepareShowContent(
            List<NewSearchResultVO> resultVOs,
            List<RequireConfirmDepVO> requireConfirmDepVOs) {

        if (WkStringUtils.isEmpty(resultVOs)) {
            return;
        }

        // ====================================
        // 以 RequireSid 對 requireConfirmDep 做分組
        // ====================================
        Map<String, List<RequireConfirmDepVO>> requireConfirmDepVOMapByRequireSid = requireConfirmDepVOs.stream()
                .collect(Collectors.groupingBy(
                        RequireConfirmDepVO::getRequireSid,
                        Collectors.mapping(
                                vo -> vo,
                                Collectors.toList())));

        // ====================================
        // 真實分派部門
        // ====================================
        // 計算使用者相關部門
        List<Integer> spcRuleAssignDepSids = this.requireConfirmDepService.prepareUserMatchDepSids(SecurityFacade.getUserSid());

        // 收集需求單 sid
        Set<String> requireSids = requireConfirmDepVOs.stream()
                .map(RequireConfirmDepVO::getRequireSid)
                .collect(Collectors.toSet());

        // 以需求單 sid 查詢對應分派部門
        Map<String, Set<Integer>> assignDepSidsMapByRequireSid = this.assignSendSearchHelper.findAssignDepSidsMapByRequireSids(
                requireSids);

        // ====================================
        // 逐筆處理
        // ====================================
        for (NewSearchResultVO resultVO : resultVOs) {
            // 取得與本筆需求單相關的需求單位確認檔
            List<RequireConfirmDepVO> rcdVOs = requireConfirmDepVOMapByRequireSid.get(resultVO.getRequireSid());
            if (WkStringUtils.isEmpty(rcdVOs)) {
                continue;
            }

            // ==================
            // 收集需求完成確認單位 + 負責人資訊
            // ==================
            List<String> ownerInfos = Lists.newArrayList();
            for (RequireConfirmDepVO requireConfirmDepVO : rcdVOs) {
                String depName = wkOrgCache.findNameBySid(requireConfirmDepVO.getDepSid());
                String ownerName = this.requireConfirmDepService.prepareOwnerName(
                        requireConfirmDepVO.getOwnerSid(),
                        requireConfirmDepVO.getDepSid());
                ownerInfos.add(depName + "：<span class='WS1-1-3b'>" + ownerName + "</span>");
            }

            resultVO.setShow_reqConfirmDepAndOwner(String.join("<br/>", ownerInfos));

            // ==================
            // 真實分派單位
            // ==================
            // 取得該需求單被分派的單位
            Set<Integer> assignDepSids = assignDepSidsMapByRequireSid.get(resultVO.getRequireSid());
            if (WkStringUtils.notEmpty(assignDepSids)) {

                // 比對是否為使用者相關單位
                for (Integer assignDepSid : assignDepSids) {
                    if (spcRuleAssignDepSids.contains(assignDepSid)) {
                        resultVO.setTrueAssignDep(true);
                        break;
                    }
                }
            }
        }
    }
}
