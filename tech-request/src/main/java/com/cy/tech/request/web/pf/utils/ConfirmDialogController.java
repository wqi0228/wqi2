package com.cy.tech.request.web.pf.utils;

import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 供前端 ConfirmDialog 使用
 *
 * @author cosmo
 */
@Component
public class ConfirmDialogController implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6840836416437373242L;

    public enum Severity {

        ALERT, INFO
    }

    @Autowired
    transient private DisplayController controller;

    private String title;
    private String message;
    private Severity severity = Severity.ALERT;

    /**
     * 設定要顯示的標題及訊息，並進行 Update
     *
     * @param title
     * @param message
     * @param widgetVar
     * @param updateTargetId
     */
    public void showInfoWithUpdate(String title, String message, String widgetVar,
              String updateTargetId) {
        showInfo(title, message, widgetVar);
        controller.update(updateTargetId);
    }

    /**
     * 設定要顯示的標題及訊息
     *
     * @param title
     * @param message
     * @param widgetVar
     */
    public void showInfo(String title, String message, String widgetVar) {
        setInfo(title, message);
        controller.showPfWidgetVar(widgetVar);
    }

    /**
     * 設定要顯示的標題及訊息（必須自行在 PF 頁面操作）
     *
     * @param title
     * @param message
     */
    public void setInfo(String title, String message) {
        this.title = title;
        this.message = message;
        this.severity = Severity.INFO;
    }

    /**
     * 設定要顯示的標題及警告訊息，並進行 Update
     *
     * @param title
     * @param message
     * @param widgetVar
     * @param updateTargetId
     */
    public void showAlertWithUpdate(String title, String message, String widgetVar,
              String updateTargetId) {
        showAlert(title, message, widgetVar);
        controller.update(updateTargetId);
    }

    /**
     * 設定要顯示的標題及警告訊息
     *
     * @param title
     * @param message
     * @param widgetVar
     */
    public void showAlert(String title, String message, String widgetVar) {
        setAlert(title, message);
        controller.showPfWidgetVar(widgetVar);
    }

    /**
     * 設定要顯示的標題及警告訊息（必須自行在 PF 頁面操作）
     *
     * @param title
     * @param message
     */
    public void setAlert(String title, String message) {
        this.title = title;
        this.message = message;
        this.severity = Severity.ALERT;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Severity getSeverity() {
        return severity;
    }

    public void setSeverity(Severity severity) {
        this.severity = severity;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
