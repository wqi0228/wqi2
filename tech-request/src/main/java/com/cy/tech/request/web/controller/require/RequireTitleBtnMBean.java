/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.ReqUnitBpmService;
import com.cy.tech.request.logic.service.RequireShowService;
import com.cy.tech.request.vo.constants.ReqPermission;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.collect.Lists;

import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 需求單抬頭顯示(上方資訊幕)
 *
 * @author shaun
 */
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class RequireTitleBtnMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -553511313273086447L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private RequireShowService rsService;
    @Autowired
    transient private ReqUnitBpmService reqBpmService;
    @Autowired
    transient private RequireCheckItemMBean requireCheckItemMBean;

    private Boolean showEditBtn;
    private Boolean showAdminEditBtn;
    private Boolean showSaveBtn;
    private Boolean showFavoriteBtn;
    private Boolean showAttachmentBtn;
    private Boolean showForwardBtn;
    private Boolean showTraceBtn;
    private Boolean showReplyBtn;
    private Boolean showForwardReplyBtn;
    private Boolean showCheckConfirmBtn;
    private Boolean showRollBackNotifyBtn;
    private Boolean showUrgencyBtn;
    private Boolean showRelationshipBtn;
    private Boolean showCloseBtn;
    private Boolean showForceCloseBtn;
    private Boolean showRollbackCloseBtn;
    private Boolean showForceRollbackCloseBtn;
    private Boolean showTransFogbugz;
    private Boolean showRollbackBtn;
    private Boolean showSignBtn;
    private Boolean showRecoveryBtn;
    private Boolean showInvaildBtn;
    private Boolean showPrototypeBtn;
    private Boolean showSendTestBtn;
    private Boolean showOnpgBtn;
    private Boolean showOthSetBtn;
    private Boolean showRequireCompleteBtn;
    private Boolean showAssignSendBtn;
    private Boolean showChangeHopeDateBtn;
    private Boolean showCustmoerBtn;
    private Boolean showAuthorBtn;
    private Boolean showDraftSubmit;
    private Boolean showDraftDelete;
    private Boolean showCopy;
    private Boolean showNcsBtn;
    @Setter
    private Boolean showPmisBtn;
    @Setter
    private Boolean showCheckItemsEditBtn;

    /** 因為同步問題，所以需另行開放設定 */
    @Setter
    private Boolean isContainAssign;

    public void clear() {
        showEditBtn = null;
        showAdminEditBtn = null;
        showSaveBtn = null;
        showFavoriteBtn = null;
        showAttachmentBtn = null;
        showForwardBtn = null;
        showTraceBtn = null;
        showReplyBtn = null;
        showForwardReplyBtn = null;
        showCheckConfirmBtn = null;
        showRollBackNotifyBtn = null;
        showUrgencyBtn = null;
        showRelationshipBtn = null;
        showCloseBtn = null;
        showForceCloseBtn = null;
        showTransFogbugz = null;
        showRollbackBtn = null;
        showSignBtn = null;
        showRecoveryBtn = null;
        showInvaildBtn = null;
        showPrototypeBtn = null;
        showSendTestBtn = null;
        showOnpgBtn = null;
        showOthSetBtn = null;
        showRequireCompleteBtn = null;
        showAssignSendBtn = null;
        showChangeHopeDateBtn = null;
        showCustmoerBtn = null;
        showAuthorBtn = null;
        isContainAssign = null;
        showDraftSubmit = null;
        showDraftDelete = null;
        showCopy = null;
        showPmisBtn = null;

        // 反結案
        showRollbackCloseBtn = null;
        showForceRollbackCloseBtn = null;

        // 清空以查詢的預設值,使其重算
        this.isContainAssign = null;

        this.showCheckItemsEditBtn = null;
    }

    /**
     * 1.需處理載入狀態<BR/>
     * 2.非刪除狀態
     *
     * @param r01MBean
     * @return
     */
    private Boolean isLoadPageStep(Require01MBean r01MBean) {
        return r01MBean.getSetp().equals(RequireStep.LOAD_IN_PAGE);
    }

    private Boolean isLoadEditPageStep(Require01MBean r01MBean) {
        return r01MBean.getSetp().equals(RequireStep.LOAD_IN_PAGE_EDIT);
    }

    /**
     * 是否顯示編輯按鈕
     * 
     * @param r01MBean
     * @return
     */
    public Boolean showEditBtn(Require01MBean r01MBean) {
        if (showEditBtn == null) {
            showEditBtn = this.isLoadPageStep(r01MBean)
                    && rsService.showEditBtn(
                            r01MBean.getRequire(),
                            loginBean.getUser());
        }
        return showEditBtn;
    }

    /**
     * 是否顯示管理員編輯按鈕
     * 
     * @param r01MBean
     * @return
     */
    public Boolean showAdminEditBtn(Require01MBean r01MBean) {
        if (showAdminEditBtn == null) {
            // 為需求單系統管理員
            boolean isSysAdmin = WkUserUtils.isUserHasRole(
                    SecurityFacade.getUserSid(),
                    SecurityFacade.getCompanyId(),
                    ReqPermission.ROLE_SYS_ADMIN);

            showAdminEditBtn = this.isLoadPageStep(r01MBean) && isSysAdmin;
        }
        return showAdminEditBtn;
    }

    /**
     * 是否顯示存檔按鈕
     * 
     * @param r01MBean
     * @return
     */
    public Boolean showSaveBtn(Require01MBean r01MBean) {
        if (showSaveBtn == null) {
            showSaveBtn = this.isLoadEditPageStep(r01MBean)
                    && (r01MBean.isEditByAdmin()
                            || rsService.showSaveBtn(r01MBean.getRequire()));
        }
        return showSaveBtn;
    }

    /**
     * @param r01MBean
     * @return
     */
    public Boolean showFavoriteBtn(Require01MBean r01MBean) {
        if (showFavoriteBtn == null) {
            showFavoriteBtn = this.isLoadPageStep(r01MBean);
        }
        return showFavoriteBtn;
    }

    /**
     * 附加檔案
     * 
     * @param r01MBean
     * @return
     */
    public Boolean showAttachmentBtn(Require01MBean r01MBean) {
        if (showAttachmentBtn == null) {
            showAttachmentBtn = this.isLoadPageStep(r01MBean)
                    && rsService.showAttachmentBtn(r01MBean.getRequire());
        }
        return showAttachmentBtn;
    }

    /**
     * 轉寄
     * 
     * @param r01MBean
     * @return
     */
    public Boolean showForwardBtn(Require01MBean r01MBean) {
        if (showForwardBtn == null) {
            showForwardBtn = this.isLoadPageStep(r01MBean)
                    && rsService.showForwardBtn(r01MBean.getRequire());
        }
        return showForwardBtn;
    }

    /**
     * 追蹤按鈕
     *
     * @param r01MBean
     * @return
     */
    public Boolean showTraceBtn(Require01MBean r01MBean) {
        if (showTraceBtn == null) {
            showTraceBtn = this.isLoadPageStep(r01MBean);
        }
        return showTraceBtn;
    }

    /**
     * @param r01MBean
     * @return
     */
    public Boolean showRequireAddInfoBtn(Require01MBean r01MBean) {
        if (showReplyBtn == null) {
            showReplyBtn = this.isLoadPageStep(r01MBean)
                    && rsService.showRequireAddInfoBtn(r01MBean.getRequire());
        }
        return showReplyBtn;
    }

    /**
     * 顯示轉寄回覆鍵 !@# 未完成
     *
     * @param r01MBean
     * @return
     */
    public Boolean showForwardReplyBtn(Require01MBean r01MBean) {
        if (showForwardReplyBtn == null) {
            showForwardReplyBtn = Boolean.FALSE;
        }
        return showForwardReplyBtn;
    }

    /**
     * 顯示【檢查確認】
     * 
     * @param r01MBean
     * @return
     */
    public Boolean showCheckConfirmBtn(Require01MBean r01MBean) {
        if (showCheckConfirmBtn == null) {
            showCheckConfirmBtn = this.isLoadPageStep(r01MBean)
                    && rsService.showCheckConfirmBtn(
                            r01MBean.getRequire(),
                            SecurityFacade.getUserSid());
        }
        return showCheckConfirmBtn;
    }

    /**
     * 顯示【退件通知】
     * 
     * @param r01MBean
     * @return
     */
    public Boolean showRollBackNotifyBtn(Require01MBean r01MBean) {
        if (showRollBackNotifyBtn == null) {
            showRollBackNotifyBtn = this.isLoadPageStep(r01MBean)
                    && rsService.showRollBackNotifyBtn(
                            r01MBean.getRequire(),
                            SecurityFacade.getUserSid());
        }
        return showRollBackNotifyBtn;
    }

    /**
     * 顯示【緊急度】
     * 
     * @param r01MBean
     * @return
     */
    public Boolean showUrgencyBtn(Require01MBean r01MBean) {
        if (showUrgencyBtn == null) {
            showUrgencyBtn = this.isLoadPageStep(r01MBean)
                    && rsService.showUrgencyBtn(
                            r01MBean.getRequire(),
                            SecurityFacade.getUserSid());
        }
        return showUrgencyBtn;
    }

    /**
     * 顯示【關連】
     * 
     * @param r01MBean
     * @return
     */
    public Boolean showRelationshipBtn(Require01MBean r01MBean) {
        if (showRelationshipBtn == null) {
            showRelationshipBtn = this.isLoadPageStep(r01MBean)
                    && rsService.showRelationshipBtn(r01MBean.getRequire());
        }
        return showRelationshipBtn;
    }

    /**
     * 顯示『結案』按鈕
     * 
     * @param r01MBean
     * @return
     */
    public Boolean showCloseBtn(Require01MBean r01MBean) {
        if (showCloseBtn == null) {
            showCloseBtn = this.isLoadPageStep(r01MBean)
                    && rsService.showCloseBtn(r01MBean.getRequire(), SecurityFacade.getUserSid());
        }
        return showCloseBtn;
    }

    /**
     * 顯示『強制結案』按鈕
     * 
     * @param r01MBean
     * @return
     */
    public Boolean showForceCloseBtn(Require01MBean r01MBean) {
        if (showForceCloseBtn == null) {
            showForceCloseBtn = this.isLoadPageStep(r01MBean)
                    && rsService.showForceCloseBtn(r01MBean.getRequire(), SecurityFacade.getUserSid());
        }
        return showForceCloseBtn;
    }

    /**
     * 顯示『反結案』按鈕
     * 
     * @param r01MBean
     * @return
     */
    public Boolean showRollbackCloseBtn(Require01MBean r01MBean) {
        if (showRollbackCloseBtn == null) {
            showRollbackCloseBtn = this.isLoadPageStep(r01MBean)
                    && rsService.showRollbackCloseBtn(r01MBean.getRequire(), SecurityFacade.getUserSid());
        }
        return showRollbackCloseBtn;
    }

    /**
     * 顯示『強制反結案』按鈕
     * 
     * @param r01MBean
     * @return
     */
    public Boolean showForceRollbackCloseBtn(Require01MBean r01MBean) {
        if (showForceRollbackCloseBtn == null) {
            showForceRollbackCloseBtn = this.isLoadPageStep(r01MBean)
                    && rsService.showForceRollbackCloseBtn(r01MBean.getRequire(), SecurityFacade.getUserSid());
        }
        return showForceRollbackCloseBtn;
    }

    /**
     * 顯示轉FB
     *
     * @param r01MBean
     * @return
     */
    public Boolean showTransFogbugz(Require01MBean r01MBean) {
        if (showTransFogbugz == null) {
            // 具有 FB分派執行操作者 角色
            boolean hasPermissionRole = WkUserUtils.isUserHasRole(
                    SecurityFacade.getUserSid(),
                    SecurityFacade.getCompanyId(),
                    ReqPermission.FB_ASSIGN_EXEC);

            showTransFogbugz = this.isLoadPageStep(r01MBean)
                    && rsService.showTransFogbugz(
                            r01MBean.getRequire(),
                            loginBean.getUser(),
                            hasPermissionRole);
        }
        return showTransFogbugz;
    }

    /**
     * 顯示退回
     *
     * @param r01MBean
     * @return
     */
    public Boolean showRollbackBtn(Require01MBean r01MBean) {
        if (showRollbackBtn == null) {
            showRollbackBtn = this.isLoadPageStep(r01MBean)
                    && r01MBean.getRequire().getHasReqUnitSign()
                    && reqBpmService.showRollBackBtn(r01MBean.getRequire(), loginBean.getUser());
        }
        return showRollbackBtn;
    }

    /**
     * 顯示需求主管簽核
     *
     * @param r01MBean
     * @return
     */
    public Boolean showSignBtn(Require01MBean r01MBean) {
        if (showSignBtn == null) {
            showSignBtn = this.isLoadPageStep(r01MBean)
                    && r01MBean.getRequire().getHasReqUnitSign()
                    && reqBpmService.showSignBtn(r01MBean.getRequire(), loginBean.getUser());
        }
        return showSignBtn;
    }

    /**
     * 顯示復原
     *
     * @param r01MBean
     * @return
     */
    public Boolean showRecoveryBtn(Require01MBean r01MBean) {
        if (showRecoveryBtn == null) {
            showRecoveryBtn = this.isLoadPageStep(r01MBean)
                    && r01MBean.getRequire().getHasReqUnitSign()
                    && reqBpmService.showRecoveryBtn(r01MBean.getRequire(), loginBean.getUser());
        }
        return showRecoveryBtn;
    }

    /**
     * 顯示作廢..介於bpm之間複和結果得到是否顯示
     *
     * @param r01MBean
     * @return
     */
    public Boolean showInvaildBtn(Require01MBean r01MBean) {
        if (showInvaildBtn == null) {
            showInvaildBtn = this.isLoadPageStep(r01MBean)
                    && r01MBean.getRequire().getHasReqUnitSign()
                    && reqBpmService.showInvaildBtn(r01MBean.getRequire(),
                            loginBean.getUser());
        }
        return showInvaildBtn;
    }

    /**
     * 顯示原型確認按鈕
     *
     * @param r01MBean
     * @return
     */
    public Boolean showPrototypeBtn(Require01MBean r01MBean) {
        if (showPrototypeBtn == null) {

            showPrototypeBtn = this.isLoadPageStep(r01MBean)
                    && rsService.showPrototypeBtn(
                            r01MBean.getRequire(),
                            this.isContainAssign(r01MBean));// 登入者是否為分派單位成員
        }
        return showPrototypeBtn;
    }

    /**
     * 顯示送測確認按鈕
     *
     * @param r01MBean
     * @return
     */
    public Boolean showSendTestBtn(Require01MBean r01MBean) {
        if (showSendTestBtn == null) {

            showSendTestBtn = this.isLoadPageStep(r01MBean)
                    && rsService.showSendTestBtn(
                            r01MBean.getRequire(),
                            this.isContainAssign(r01MBean));
        }
        return showSendTestBtn;
    }

    /**
     * 顯示ON程式確認按鈕
     *
     * @param r01MBean
     * @return
     */
    public Boolean showOnpgBtn(Require01MBean r01MBean) {
        if (showOnpgBtn == null) {

            showOnpgBtn = this.isLoadPageStep(r01MBean)
                    && rsService.showOnpgBtn(r01MBean.getRequire(),
                            this.isContainAssign(r01MBean));
        }
        return showOnpgBtn;
    }

    /**
     * 顯示其它設定資訊按鈕
     *
     * @param r01MBean
     * @return
     */
    public Boolean showOthSetBtn(Require01MBean r01MBean) {
        if (showOthSetBtn == null) {
            showOthSetBtn = this.isLoadPageStep(r01MBean)
                    && rsService.showOthSetBtn(r01MBean.getRequire(), loginBean.getUser(),
                            this.isContainAssign(r01MBean));
        }
        return showOthSetBtn;
    }

    /**
     * 顯示強制需求完成按鍵
     *
     * @param r01MBean
     * @return
     */
    public Boolean showRequireCompleteBtn(Require01MBean r01MBean) {
        if (showRequireCompleteBtn == null) {
            showRequireCompleteBtn = this.isLoadPageStep(r01MBean)
                    && rsService.showRequireCompleteBtn(
                            r01MBean.getRequire(),
                            loginBean.getUser());
        }
        return showRequireCompleteBtn;
    }

    /**
     * 顯示分派通知按鈕
     *
     * @param r01MBean
     * @return
     */
    public Boolean showAssignSendBtn(Require01MBean r01MBean) {
        if (showAssignSendBtn == null) {

            showAssignSendBtn = this.isLoadPageStep(r01MBean)
                    && rsService.showAssignSendBtn(
                            r01MBean.getRequire(),
                            SecurityFacade.getUserSid());
        }
        return showAssignSendBtn;
    }

    /**
     * 顯示修改期望完成日按鍵
     *
     * @param r01MBean
     * @return
     */
    public Boolean showChangeHopeDateBtn(Require01MBean r01MBean) {
        if (showChangeHopeDateBtn == null) {
            showChangeHopeDateBtn = this.isLoadPageStep(r01MBean)
                    && rsService.showChangeHopeDateBtn(r01MBean.getRequire(), loginBean.getUser());
        }
        return showChangeHopeDateBtn;
    }

    /**
     * 判斷登入者是否在分派單位或成員
     *
     * @param r01MBean
     * @return
     */
    private Boolean isContainAssign(Require01MBean r01MBean) {
        if (isContainAssign == null) {
            isContainAssign = rsService.isContainAssign(r01MBean.getRequire(), loginBean.getUser());
        }
        return isContainAssign;
    }

    /**
     * 是否顯示修改廳主按鈕
     *
     * @param r01MBean
     * @return
     */
    public Boolean showCustmoerBtn(Require01MBean r01MBean) {
        if (showCustmoerBtn == null) {

            // 具有 業務單位主管、客戶資料管理者 角色
            boolean hasPermissionRole = WkUserUtils.isUserHasRole(
                    SecurityFacade.getUserSid(),
                    SecurityFacade.getCompanyId(),
                    Lists.newArrayList(ReqPermission.BUSINESS_UNIT_MGR, ReqPermission.CUSTOMER_DATA_MGR));

            showCustmoerBtn = this.isLoadPageStep(r01MBean)
                    && rsService.showCustmoerBtn(
                            r01MBean.getRequire(),
                            loginBean.getUser(),
                            hasPermissionRole);
        }
        return showCustmoerBtn;
    }

    /**
     * 是否顯示修客戶改按鈕
     *
     * @param r01MBean
     * @return
     */
    public Boolean showAuthorBtn(Require01MBean r01MBean) {
        if (showAuthorBtn == null) {

            // 具有 業務單位主管、客戶資料管理者 角色
            boolean hasPermissionRole = WkUserUtils.isUserHasRole(
                    SecurityFacade.getUserSid(),
                    SecurityFacade.getCompanyId(),
                    Lists.newArrayList(ReqPermission.BUSINESS_UNIT_MGR, ReqPermission.CUSTOMER_DATA_MGR));

            showAuthorBtn = this.isLoadPageStep(r01MBean)
                    && rsService.showAuthorBtn(r01MBean.getRequire(), loginBean.getUser(),
                            hasPermissionRole);

        }
        return showAuthorBtn;
    }

    /**
     * 顯示草稿提交
     *
     * @param r01MBean
     * @return
     */
    public Boolean showDraftSubmit(Require01MBean r01MBean) {
        if (showDraftSubmit == null) {
            showDraftSubmit = this.isLoadPageStep(r01MBean)
                    && rsService.showDraftSubmit(r01MBean.getRequire(), loginBean.getUser());
        }
        return showDraftSubmit;
    }

    /**
     * 顯示草稿刪除
     *
     * @param r01MBean
     * @return
     */
    public Boolean showDraftDelete(Require01MBean r01MBean) {
        if (showDraftDelete == null) {
            showDraftDelete = this.isLoadPageStep(r01MBean)
                    && rsService.showDraftDelete(r01MBean.getRequire(), loginBean.getUser());
        }
        return showDraftDelete;
    }

    /**
     * 顯示複製
     *
     * @param r01MBean
     * @return
     */
    public Boolean showCopy(Require01MBean r01MBean) {
        if (showCopy == null) {
            showCopy = this.isLoadPageStep(r01MBean);
        }
        return showCopy;
    }

    /**
     * 顯示NCS
     * 
     * @param r01MBean
     * @return
     */
    public Boolean showNcsBtn() {
        if (showNcsBtn == null) {
            showNcsBtn = rsService.showNcsBtn(loginBean.getUser());
        }
        return showNcsBtn;
    }

    /**
     * 顯示轉PMIS
     * 
     * @param r01MBean
     * @return
     */
    public Boolean showPmisBtn(Require01MBean r01MBean) {
        if (showPmisBtn == null) {
            showPmisBtn = rsService.showPmisBtn(r01MBean.getRequire(), SecurityFacade.hasPermission(ReqPermission.ROLE_PMIS_MGR));
        }
        return showPmisBtn;
    }

    /**
     * 顯示檢查項目(系統別)編輯鈕
     * 
     * @param require01MBean
     * @return
     */
    public boolean showCheckItemsEditBtn(Require01MBean require01MBean) {
        if (showCheckItemsEditBtn == null) {
            // 1.可使用全單據編輯時，不顯示單獨編輯模式
            // ps. 待檢查時，也可使用單獨編輯
            if (!RequireStatusType.WAIT_CHECK.equals(require01MBean.getRequire().getRequireStatus())
                    && this.showEditBtn(require01MBean)) {
                this.showCheckItemsEditBtn = false;
            }
            // 2.不可使用全單據編輯磨事實，依據權限判斷
            else {
                this.showCheckItemsEditBtn = this.requireCheckItemMBean.isCanEdit(require01MBean);
            }
        }
        return showCheckItemsEditBtn;
    }
}
