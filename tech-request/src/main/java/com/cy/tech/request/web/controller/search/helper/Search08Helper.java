/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search.helper;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search08QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.Search08View;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.SpecificPermissionService;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.web.view.to.search.query.SearchQuery08;
import com.cy.tech.request.web.view.to.search.query.SearchQuery08.SortType;
import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.logic.lib.manager.WorkCommonReadRecordManager;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.logic.WkUserAndOrgLogic;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * 輔助 Search08MBean
 *
 * @author kasim
 */
@Component
public class Search08Helper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5929646412737773846L;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private SearchHelper searchHelper;
    @Autowired
    transient private UserService userService;
    @Autowired
    transient private OrganizationService orgService;
    @Autowired
    transient private SpecificPermissionService spService;
    @Autowired
    transient private Search08QueryService search08QueryService;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchResultHelper searchResultHelper;
    @Autowired
    transient private WkUserAndOrgLogic userAndOrgLogic;
    @Autowired
    transient private WorkCommonReadRecordManager workCommonReadRecordManager;

    /**
     * 取得預設查詢部門
     *
     * @param loginDepSid
     * @return
     */
    public List<String> getDefaultDepSids(Integer loginDepSid, List<Long> roleSids) {
        Set<String> results = Sets.newHashSet(String.valueOf(loginDepSid));
        return Lists.newArrayList(this.addSpDepts(roleSids, this.getDefaultDepSids(loginDepSid, results)));
    }

    /**
     * 取得預設查詢部門
     *
     * @param depSid
     * @param results
     * @return
     */
    private Set<String> getDefaultDepSids(Integer depSid, Set<String> results) {
        orgService.findByParentSid(depSid, Activation.ACTIVE).forEach(each -> {
            results.add(String.valueOf(each.getSid()));
            this.getDefaultDepSids(each.getSid(), results);
        });
        return results;
    }

    /**
     * 初始化特殊權限部門
     *
     * @param roles
     * @param results
     * @return
     */
    private Set<String> addSpDepts(List<Long> roleSids, Set<String> results) {
        List<Org> spDepts = spService.findSpecificDeptsByRoleSidIn(roleSids);
        spDepts.forEach(each -> {
            if (Activation.ACTIVE.equals(each.getStatus())) {
                results.add(String.valueOf(each.getSid()));
            }
        });
        return results;
    }

    /**
     * 查詢
     *
     * @param loginDep
     * @param loginUser
     * @param query
     * @return
     */
    public List<Search08View> search(String compId, Org loginDep, User loginUser, SearchQuery08 query) {

        String requireNo = reqularUtils.getRequireNo(compId, query.getFuzzyText());
        StringBuilder builder = new StringBuilder();
        Map<String, Object> parameters = Maps.newHashMap();

        builder.append("SELECT tpc.pt_check_sid, ");
        builder.append("       tr.require_no, ");
        builder.append("       tid.field_content, ");
        builder.append("       tr.urgency, ");
        builder.append("       tpc.create_dt, ");
        builder.append("       tpc.dep_sid    AS pDep, ");
        builder.append("       tpc.create_usr AS pUser, ");
        builder.append("       ckm.big_category_name, ");
        builder.append("       ckm.middle_category_name, ");
        builder.append("       ckm.small_category_name, ");
        builder.append("       tr.dep_sid, ");
        builder.append("       tr.create_usr, ");
        builder.append("       tpc.pt_check_status, ");
        builder.append("       rpsi.paper_code, ");
        builder.append("       tpc.pt_check_estimate_dt, ");
        builder.append("       rpr.reply_udt, ");
        builder.append("       tpc.currently_ver, ");
        builder.append("       tpc.pt_notice_member, ");
        builder.append("       tpc.pt_notice_deps, ");
        builder.append("       tpc.read_reason, ");
        builder.append("       tr.require_status, ");
        builder.append("       tpc.pt_check_theme, ");
        builder.append("       tpc.pt_check_no, ");
        // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
        builder.append(this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire());

        builder.append("FROM ( ");
        this.buildRequirePrototypeCheckCondition(loginDep, loginUser, query, requireNo, builder, parameters);
        this.buildRequireCondition(query, requireNo, builder, parameters);
        this.buildRequireIndexDictionaryCondition(builder);
        this.buildCategoryKeyMappingCondition(query, requireNo, builder);
        this.buildRequirePrototypeSignInfoCondition(builder);
        this.buildRequirePrototypeReplyCondition(builder);
        // 檢查項目 (系統別)
        // 後方需做 group by
        builder.append(this.searchConditionSqlHelper.prepareSubFormCommonJoin(
                SecurityFacade.getUserSid(),
                FormType.PTCHECK,
                "tpc"));

        builder.append("WHERE tpc.pt_check_sid IS NOT NULL ");

        // 查詢條件：是否閱讀
        if (WkStringUtils.isEmpty(requireNo)) {
            builder.append(
                    this.workCommonReadRecordManager.prepareWhereConditionSQL(
                            ReadRecordType.safeValueOf(query.getSelectReadRecordType()), "readRecord"));
        }

        builder.append(" GROUP BY tpc.pt_check_sid ");
        builder.append("ORDER BY tpc.");
        builder.append(SortType.valueOf(query.getSortType()).getColName()).append(" DESC");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.PROTOTYPE_CHECK, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.PROTOTYPE_CHECK, SecurityFacade.getUserSid());

        // 查詢
        List<Search08View> resultList = search08QueryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // ====================================
        // 後續處理
        // ====================================
        if (WkStringUtils.notEmpty(resultList)) {
            // 後續處理-開始
            usageRecord.afterProcessStart();
            // 查詢條件過濾：系統別
            resultList = resultList.stream()
                    .filter(each -> this.searchResultHelper.filterCheckItems(
                            each.getCheckItemTypes(),
                            query.getCheckItemTypes(),
                            false,
                            null))
                    .collect(Collectors.toList());

            // 若待閱原因無勾選"無待閱原因", 只留待閱讀
            if (!query.getReadReason().contains(WaitReadReasonType.PROTOTYPE_NOT_WAITREASON.name())) {
                resultList = resultList.stream()
                        .filter(vo -> ReadRecordType.WAIT_READ.equals(vo.getReadRecordType()))
                        .collect(Collectors.toList());
            }
            // 後續處理-結束
            usageRecord.afterProcessEnd();
        }

        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return resultList;
    }

    /**
     * 組合語句
     *
     * @param loginDep
     * @param loginUser
     * @param query
     * @param requireNo
     * @param builder
     * @param parameters
     */
    private void buildRequirePrototypeCheckCondition(Org loginDep, User loginUser, SearchQuery08 query, String requireNo, StringBuilder builder,
            Map<String, Object> parameters) {

        // 條件1：可閱單位 (部門向下、管理部門向下、特殊可閱)
        String canViewDepsSql = " = -9999 ";
        Set<Integer> canViewDepSids = this.userAndOrgLogic.prepareCanViewDepSids(loginUser.getSid(), true);
        if (WkStringUtils.notEmpty(canViewDepSids)) {
            canViewDepsSql = canViewDepSids.stream()
                    .map(canViewDepSid -> canViewDepSid + "")
                    .collect(Collectors.joining(",", " IN(", ")"));
        }

        // 條件2 : 單據狀態已簽核完成(核准)後, 通知人員、通知單位可閱
        String paperCode = Lists.newArrayList(InstanceStatus.CLOSED, InstanceStatus.APPROVED).stream()
                .map(each -> each.getValue())
                .collect(Collectors.joining("','", "'", "'"));
        String notifyMember = "'\"" + loginUser.getSid() + "\"'";
        String notifyDeps = "'\"" + loginDep.getSid() + "\"'";

        builder.append("SELECT * ");
        builder.append("FROM   work_pt_check tpc ");
        builder.append("WHERE  1 = 1 ");
        builder.append("  AND ( ");
        builder.append("         tpc.dep_sid " + canViewDepsSql);
        builder.append("         OR ( tpc.pt_check_sid IN (SELECT rpsi_temp.pt_check_sid ");
        builder.append("                                  FROM   work_pt_sign_info rpsi_temp ");
        builder.append("                                  WHERE  rpsi_temp.paper_code IN ( " + paperCode + " )) ");
        builder.append("             AND ( tpc.pt_notice_member REGEXP " + notifyMember + " ");
        builder.append("                    OR tpc.pt_notice_deps REGEXP " + notifyDeps + " ) ) )");

        if (Strings.isNullOrEmpty(requireNo)) {
            // 填單區間
            String colName = "tpc.create_dt";
            if (SortType.UPDATE_DESC.name().equals(query.getSortType())) {
                // 異動區間
                colName = "tpc.read_update_dt";
            }
            if (query.getStartDate() != null && query.getEndDate() != null) {
                builder.append("        AND ").append(colName).append(" BETWEEN '")
                        .append(searchHelper.transStrByStartDate(query.getStartDate())).append("' AND '")
                        .append(searchHelper.transStrByEndDate(query.getEndDate())).append("' ");
            } else if (query.getStartDate() != null) {
                builder.append("        AND ").append(colName).append(" >= '")
                        .append(searchHelper.transStrByStartDate(query.getStartDate())).append("' ");
            } else if (query.getEndDate() != null) {
                builder.append("        AND ").append(colName).append(" <= '")
                        .append(searchHelper.transStrByEndDate(query.getEndDate())).append("' ");
            }
            // 待閱原因
            builder.append(
                    this.searchConditionSqlHelper.prepareWhereConditionForWaitReadReason(
                            requireNo,
                            WaitReadReasonType.valueNames(FormType.PTCHECK),
                            query.getReadReason(),
                            WaitReadReasonType.PROTOTYPE_NOT_WAITREASON.name(),
                            "tpc.read_reason"));

            // 填單人員
            if (!Strings.isNullOrEmpty(query.getTrCreatedUserName())) {
                String userSidsStr = "'isEmpty'";
                List<Integer> userSids = userService.findAllByLike(query.getTrCreatedUserName()).stream().map(User::getSid).collect(Collectors.toList());

                if (userSids != null && !userSids.isEmpty()) {
                    userSidsStr = userSids.stream()
                            .map(each -> String.valueOf(each))
                            .collect(Collectors.joining(","));
                }
                builder.append("        AND tpc.create_usr IN (").append(userSidsStr).append(") ");
            }
            // 原型確認狀態
            if (query.getPrototypeStatus() != null && !query.getPrototypeStatus().isEmpty()) {
                String proStatus = query.getPrototypeStatus().stream()
                        .collect(Collectors.joining("','", "'", "'"));
                builder.append("        AND tpc.pt_check_status IN (").append(proStatus).append(") ");
            }
            // 是否為最新版次
            if (query.getIsLatestEdition() != null && query.getIsLatestEdition()) {
                builder.append("        AND (tpc.pt_check_source_no,tpc.currently_ver) IN ( ");
                builder.append("            SELECT tpc2.pt_check_source_no,MAX(tpc2.currently_ver) FROM work_pt_check tpc2 ");
                builder.append("            GROUP BY tpc2.pt_check_source_no ");
                builder.append("        ) ");
            }
            // 模糊搜尋
            if (!Strings.isNullOrEmpty(query.getFuzzyText())) {
                String text = "%" + reqularUtils.replaceIllegalSqlLikeStr(query.getFuzzyText()) + "%";
                builder.append("        AND ( ");
                builder.append("            tpc.pt_check_content LIKE :text ");
                builder.append("            OR ");
                builder.append("            tpc.pt_check_theme LIKE :text ");
                builder.append("            OR ");
                builder.append("            tpc.pt_check_note LIKE :text ");
                builder.append("            OR ");
                builder.append("            tpc.pt_check_sid IN ( ");
                builder.append("                SELECT wpr.pt_check_sid FROM work_pt_reply wpr ");
                builder.append("                    WHERE wpr.reply_content LIKE :text ");
                builder.append("            ) ");
                builder.append("            OR ");
                builder.append("            tpc.pt_check_sid IN ( ");
                builder.append("                SELECT wpar.pt_check_sid FROM work_pt_already_reply wpar ");
                builder.append("                    WHERE wpar.reply_content LIKE :text ");
                builder.append("            ) ");
                builder.append("        ) ");

                parameters.put("text", text);
            }
        }
        builder.append(") AS tpc ");
    }

    /**
     * 組合語句
     *
     * @param searchQuery
     * @param requireNo
     * @param builder
     * @param parameters
     */
    private void buildRequireCondition(SearchQuery08 searchQuery, String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN ( ");
        builder.append("    SELECT * FROM tr_require tr ");
        builder.append("        WHERE 1=1 ");

        // 需求類別製作進度
        builder.append(
                this.searchConditionSqlHelper.prepareWhereConditionForRequireStatus(
                        requireNo,
                        searchQuery.getSelectRequireStatusType()));

        if (Strings.isNullOrEmpty(requireNo)) {
            // 緊急度
            if (searchQuery.getUrgencyList() != null && !searchQuery.getUrgencyList().isEmpty()) {
                String urgencyStr = searchQuery.getUrgencyList().stream()
                        .collect(Collectors.joining(","));
                builder.append("        AND tr.urgency IN (").append(urgencyStr).append(") ");
            }
            // 需求單號
            if (!Strings.isNullOrEmpty(searchQuery.getRequireNo())) {
                String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(searchQuery.getRequireNo()) + "%";
                builder.append("        AND tr.require_no LIKE :textNo ");
                parameters.put("textNo", textNo);
            }
            // 轉發部門
            if (searchQuery.getForwardDepts() != null && !searchQuery.getForwardDepts().isEmpty()) {
                String forwardDepts = searchQuery.getForwardDepts().stream()
                        .collect(Collectors.joining(","));
                builder.append("        AND tr.require_sid IN ( ");
                builder.append("            SELECT tai.require_sid FROM tr_alert_inbox tai");
                builder.append("                WHERE tai.forward_type='FORWARD_DEPT' ");
                builder.append("                AND tai.receive_dep IN (").append(forwardDepts).append(") ");
                builder.append("        ) ");
            }
        }
        builder.append(") AS tr ON tr.require_sid=tpc.pt_check_source_sid ");
    }

    /**
     * 組合語句
     *
     * @param builder
     */
    private void buildRequireIndexDictionaryCondition(StringBuilder builder) {
        builder.append("INNER JOIN ( ");
        builder.append("    SELECT tid.require_sid,tid.field_content FROM tr_index_dictionary tid ");
        builder.append("        WHERE tid.field_name='主題' ");
        builder.append(") AS tid ON tr.require_sid = tid.require_sid ");
    }

    /**
     * 組合語句
     *
     * @param query
     * @param requireNo
     * @param builder
     */
    private void buildCategoryKeyMappingCondition(SearchQuery08 query, String requireNo, StringBuilder builder) {
        builder.append("INNER JOIN ( ");
        builder.append("    SELECT * FROM tr_category_key_mapping ckm ");
        builder.append("        WHERE 1=1 ");
        if (Strings.isNullOrEmpty(requireNo)) {
            // 需求類別
            if (!Strings.isNullOrEmpty(query.getSelectBigCategorySid())
                    && query.getBigDataCateSids().isEmpty()) {
                builder.append("        AND ckm.big_category_sid = '")
                        .append(query.getSelectBigCategorySid()).append("' ");
            }
            // 類別組合
            if (!query.getBigDataCateSids().isEmpty()
                    || !query.getMiddleDataCateSids().isEmpty()
                    || !query.getSmallDataCateSids().isEmpty()) {
                String bigs = "'isEmpty'";
                String middles = "'isEmpty'";
                String smalls = "'isEmpty'";
                if (!query.getBigDataCateSids().isEmpty()) {
                    bigs = query.getBigDataCateSids().stream()
                            .collect(Collectors.joining("','", "'", "'"));
                }
                if (!query.getMiddleDataCateSids().isEmpty()) {
                    middles = query.getMiddleDataCateSids().stream()
                            .collect(Collectors.joining("','", "'", "'"));
                }
                if (!query.getSmallDataCateSids().isEmpty()) {
                    smalls = query.getSmallDataCateSids().stream()
                            .collect(Collectors.joining("','", "'", "'"));
                }
                if (!Strings.isNullOrEmpty(query.getSelectBigCategorySid())) {
                    if ("'isEmpty'".equals(bigs)) {
                        bigs = "'" + query.getSelectBigCategorySid() + "'";
                    } else {
                        bigs = bigs + ",'" + query.getSelectBigCategorySid() + "'";
                    }
                }
                builder.append("        AND( ");
                builder.append("            ckm.big_category_sid IN (").append(bigs).append(") ");
                builder.append("            OR ");
                builder.append("            ckm.middle_category_sid IN (").append(middles).append(") ");
                builder.append("            OR ");
                builder.append("            ckm.small_category_sid IN (").append(smalls).append(")");
                builder.append("        ) ");
            }
        }
        builder.append(") AS ckm ON tr.mapping_sid=ckm.key_sid ");
    }

    /**
     * 組合語句
     *
     * @param builder
     */
    private void buildRequirePrototypeSignInfoCondition(StringBuilder builder) {
        builder.append("INNER JOIN ( ");
        builder.append("    SELECT rpsi.pt_check_sid,rpsi.paper_code FROM work_pt_sign_info rpsi ");
        builder.append(") AS rpsi ON rpsi.pt_check_sid=tpc.pt_check_sid ");
    }

    /**
     * 組合語句
     *
     * @param builder
     */
    private void buildRequirePrototypeReplyCondition(StringBuilder builder) {
        builder.append("LEFT JOIN ( ");
        builder.append("    SELECT rpr.pt_check_sid,rpr.reply_udt,MAX(rpr.reply_udt) FROM work_pt_reply rpr ");
        builder.append("    GROUP BY rpr.pt_check_sid ");
        builder.append(") AS rpr ON rpr.pt_check_sid=tpc.pt_check_sid ");
    }
}
