package com.cy.tech.request.web.controller.require;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.tech.request.logic.vo.AssignTabHistoryTo;
import com.cy.tech.request.logic.vo.AssignTabInfoTo;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.require.tab.helper.AssignTabTableHelper;
import com.cy.tech.request.web.controller.require.tab.helper.AssignTabTreeHelper;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author allen1214_wu
 *
 */
@Controller
@NoArgsConstructor
@Scope("view")
public class AssignNoticeTabMBean implements IRequireBottom, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7341455632380479462L;
    /** 分派資訊 */
    private List<AssignTabInfoTo> tabInfo;
    /** 分派結果樹 */
    private TreeNode resultRoot;
    /** 分派歷程樹 */
    private TreeNode historyRoot;
    @Getter
    @Setter
    private TreeNode selNode;

    @Autowired
    transient private AssignTabTableHelper tableHelper;
    @Autowired
    transient private AssignTabTreeHelper tabTreeHelper;

    @PostConstruct
    public void init() {
        tabInfo = null;
        resultRoot = null;
        historyRoot = null;
    }

    public TreeNode showResultTree() {
        if (this.tabInfo == null || tabInfo.isEmpty()) {
            return null;
        }
        if (resultRoot == null) {
            AssignTabInfoTo resultTab = this.tabInfo.get(0);
            List<AssignTabHistoryTo> resultRows = tableHelper.createResultRowInfo(resultTab);
            resultRoot = tabTreeHelper.createRoot(resultRows);
        }
        return resultRoot;
    }

    public TreeNode showHistoryTree() {
        if (this.tabInfo == null || tabInfo.isEmpty()) {
            return null;
        }
        if (historyRoot == null) {
            List<AssignTabHistoryTo> historyRows = tableHelper.createHistoryRowInfo(this.tabInfo);
            historyRoot = tabTreeHelper.createRoot(historyRows);
        }
        return historyRoot;
    }

    public void onNodeSelect(NodeSelectEvent event) {
        if (!event.getTreeNode().isLeaf()) {
            event.getTreeNode().setExpanded(!event.getTreeNode().isExpanded());
        }
    }

    @Override
    public void initTabInfo(Require01MBean r01MBean) {

        Require require = r01MBean.getRequire();
        if (require == null || !require.getHasAssign()) {
            return;
        }
        this.resultRoot = null;
        historyRoot = null;
        if (!r01MBean.getBottomTabMBean().currentTabByName(RequireBottomTabType.ASSIGN_SEND_INFO)) {
            return;
        }

        tabInfo = tableHelper.createTabInfo(require);

    }
    
    /**
     * 尋找分派通知頁籤資訊
     *
     * @param require
     * @return
     */
    public List<AssignTabInfoTo> findTabInfo(Require require) {
        if (require == null || !require.getHasAssign()) {
            return null;
        }
        if (tabInfo == null) {
            tabInfo = tableHelper.createTabInfo(require);
        }
        return tabInfo;
    }
}
