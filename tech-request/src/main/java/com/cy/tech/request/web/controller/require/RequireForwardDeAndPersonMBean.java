package com.cy.tech.request.web.controller.require;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.ForwardService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.TrAlertInboxService;
import com.cy.tech.request.logic.service.forward.RequireForwardDeAndPersonService;
import com.cy.tech.request.vo.enums.ForwardType;
import com.cy.tech.request.vo.require.TrAlertInboxHisVO;
import com.cy.tech.request.vo.require.TrAlertInboxVO;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerCallback;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerComponent;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerConfig;
import com.cy.tech.request.web.controller.component.mipker.helper.MultItemPickerByOrgHelper;
import com.cy.tech.request.web.controller.component.mutiuserqkpker.MultiUserQuickPickerCallback;
import com.cy.tech.request.web.controller.component.mutiuserqkpker.MultiUserQuickPickerComponent;
import com.cy.tech.request.web.controller.component.mutiuserqkpker.MultiUserQuickPickerConfig;
import com.cy.tech.request.web.pf.utils.ConfirmCallbackDialogController;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonContentUtils;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkMathUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.logic.SettingCustomGroupService;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@NoArgsConstructor
@Controller
@Scope("view")
public class RequireForwardDeAndPersonMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3776024035688549543L;
    // ========================================================================
    // service
    // ========================================================================
    @Autowired
    transient private MultItemPickerByOrgHelper multItemPickerByOrgHelper;
    @Autowired
    transient private SettingCustomGroupService settingCustomGroupService;
    @Autowired
    transient private RequireForwardDeAndPersonService requireForwardDeAndPersonService;
    @Autowired
    transient private ForwardService forwardService;
    @Autowired
    transient private TrAlertInboxService trAlertInboxService;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private DisplayController displayController;
    @Autowired
    transient private ConfirmCallbackDialogController confirmCallbackDialogController;

    // ========================================================================
    // ID
    // ========================================================================

    /** dialog ID **/
    @Getter
    private final String MAIN_DIALOG_NAME = "RequireForwardDeAndPersonMBean_MAIN_DIALOG_NAME";
    /** dialog 所有內容區塊 **/
    @Getter
    private final String MAIN_DIALOG_CONTENT = "RequireForwardDeAndPersonMBean_MAIN_DIALOG_CONTENT";

    /** 轉寄部門 dialog ID **/
    @Getter
    private final String FORWARD_DEP_DIALOG_NAME = "RequireForwardDeAndPersonMBean_FORWARD_DEP_DIALOG_NAME";
    /** 轉寄部門 dialog ID **/
    @Getter
    private final String FORWARD_DEP_DIALOG_CONTENT = "RequireForwardDeAndPersonMBean_FORWARD_DEP_DIALOG_CONTENT";
    /** 轉寄部門選單 ID **/
    @Getter
    private final String FORWARD_DEP_PICKER_ID = "RequireForwardDeAndPersonMBean_FORWARD_DEP_PICKER_ID";

    /** 轉寄個人 dialog ID **/
    @Getter
    private final String FORWARD_USER_DIALOG_NAME = "RequireForwardDeAndPersonMBean_FORWARD_USER_DIALOG_NAME";
    /** 轉寄個人 dialog ID **/
    @Getter
    private final String FORWARD_USER_DIALOG_CONTENT = "RequireForwardDeAndPersonMBean_FORWARD_USER_DIALOG_CONTENT";
    /** 轉寄個人選單 ID **/
    @Getter
    private final String FORWARD_USER_PICKER_ID = "RequireForwardDeAndPersonMBean_FORWARD_USER_PICKER_ID";

    // ========================================================================
    // variable
    // ========================================================================
    /** 本次開啟視窗,所使用的需求單號 **/
    private String requireNo;
    /** 轉寄部門資訊 **/
    @Getter
    private List<TrAlertInboxHisVO> forwardDepInfos;
    /** 轉寄個人資訊 **/
    @Getter
    private List<TrAlertInboxHisVO> forwardPersonInfos;
    /** Require01MBean 更新聯動畫面用 **/
    private Require01MBean require01MBean;

    // ========================================================================
    // 主畫面
    // ========================================================================
    /**
     * 開啟視窗
     * 
     * @param requireNo
     * @param require01MBean
     */
    public void openDialog(String requireNo, Require01MBean require01MBean) {

        // ================================
        //
        // ================================
        if (!this.requireService.isExistNo(requireNo)) {
            log.warn("開啟轉寄功能失敗, 找不到需求單資料! requireNo:[{}]", requireNo);
            MessagesUtils.showWarn(WkMessage.NEED_RELOAD);
            return;
        }
        this.requireNo = requireNo;
        this.require01MBean = require01MBean;

        // ================================
        // 查詢轉寄資料
        // ================================
        try {
            this.prepareForwardInfo();
        } catch (Exception e) {
            log.error("開啟轉寄功能失敗!" + e.getMessage(), e);
            MessagesUtils.showError(WkMessage.EXECTION);
            return;
        }

        // ================================
        // 畫面處理
        // ================================
        // 更新dialog 內容
        this.displayController.update(this.MAIN_DIALOG_CONTENT);
        // 開啟 dialog
        this.displayController.showPfWidgetVar(this.MAIN_DIALOG_NAME);
    }

    /**
     * 依據需求單, 查詢所有轉寄資料
     */
    private void prepareForwardInfo() {
        // 查詢
        Map<ForwardType, List<TrAlertInboxHisVO>> resultMap = this.forwardService.prepareForwardInfoByRequire(requireNo);
        // 轉寄部門
        this.forwardDepInfos = resultMap.get(ForwardType.FORWARD_DEPT);
        // 轉寄個人
        this.forwardPersonInfos = resultMap.get(ForwardType.FORWARD_MEMBER);
    }

    /**
     * 重新讀取
     *
     * @param r01MBean
     */
    private void reLoadForward() {
        if (this.require01MBean != null) {
            this.require01MBean.reBuildeRequire();
            this.require01MBean.getIxReportMBean().clear();
            this.require01MBean.getIxPersonalMBean().clear();
            this.require01MBean.getIxInstructionMBean().clear();
            this.require01MBean.getWorkInboxComponent().loadData();
            this.displayController.update(
                    Lists.newArrayList(
                            "forwardDeptDivId",
                            "viewPanelBottomInfoTabId"));
            this.displayController.execute("reSearch()");
        }
    }

    // ========================================================================
    // 轉寄部門選單
    // ========================================================================
    /** 轉寄部門選擇器 */
    @Getter
    private MultItemPickerComponent forwardDepPicker;
    /** 已轉寄部門 */
    private List<Org> forwardDeps;
    /** 已轉寄部門中, 需被鎖定不可移除的單位 */
    private List<Integer> lockforwardDepSids;

    /**
     * 開啟轉寄部門視窗
     */
    public void forwardDepPickerOpenDialog() {

        // ================================
        // 準備已轉寄部門資訊
        // ================================
        try {
            this.forwardDepPickerPrepareForwardInfo();
        } catch (Exception e) {
            log.error("開啟轉寄部門功能失敗!" + e.getMessage(), e);
            MessagesUtils.showError(WkMessage.EXECTION);
            return;
        }

        // ================================
        // 準備選單元件
        // ================================
        try {
            this.forwardDepPickerInitializer();
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            log.error("元件初始化失敗!" + e.getMessage(), e);
            MessagesUtils.showError(WkMessage.EXECTION);
            return;
        }

        // ================================
        // 畫面處理
        // ================================
        // 更新dialog 內容
        this.displayController.update(this.FORWARD_DEP_DIALOG_CONTENT);
        // 開啟 dialog
        this.displayController.showPfWidgetVar(this.FORWARD_DEP_DIALOG_NAME);

    }

    /**
     * 準備已轉寄部門資訊
     * 
     * @param requireNo
     */
    private void forwardDepPickerPrepareForwardInfo() {
        // ================================
        // 查詢所有部門轉寄資料
        // ================================
        List<TrAlertInboxVO> trAlertInboxVOs = this.trAlertInboxService.findByRequireNoAndForwardType(
                this.requireNo,
                ForwardType.FORWARD_DEPT);

        if (trAlertInboxVOs == null) {
            trAlertInboxVOs = Lists.newArrayList();
        }

        // ================================
        // 收集已轉寄的部門
        // ================================
        this.forwardDeps = trAlertInboxVOs.stream()
                .filter(trAlertInboxVO -> trAlertInboxVO.getReceiveDep() != null) // 過濾轉寄部門為空 (防呆)
                .map(trAlertInboxVO -> WkOrgCache.getInstance().findBySid(trAlertInboxVO.getReceiveDep()))// 取得轉寄部門
                .distinct() // 去除重複
                .collect(Collectors.toList());

        // ================================
        // 收集不可移除轉寄的部門
        // 1.該部門已有成員讀取
        // 2.不是自己單位進行轉寄時, 替別人不可收回
        // ================================
        // 初始化鎖定單位
        this.lockforwardDepSids = Lists.newArrayList();
        // 判斷鎖定單位
        if (!WkStringUtils.isEmpty(this.forwardDeps)) {
            // 取得鎖定單位
            this.lockforwardDepSids = this.requireForwardDeAndPersonService.prepareCannotRemoveForwordDepSids(
                    requireNo,
                    trAlertInboxVOs,
                    SecurityFacade.getUserSid());
        }
    }

    /**
     * 轉寄部門選單初始化
     * 
     * @throws UserMessageException
     */
    private void forwardDepPickerInitializer() throws UserMessageException {
        try {
            // 選擇器設定資料
            MultItemPickerConfig config = new MultItemPickerConfig();
            config.setTreeModePrefixName("單位");
            config.setContainFollowing(true);
            config.setEnableGroupMode(true);
            config.setItemComparator(MultItemPickerByOrgHelper.getInstance().parpareComparator());
            // 開啟 filter 自動清除機制
            config.setComponentID(FORWARD_DEP_PICKER_ID);

            // 選擇器初始化
            this.forwardDepPicker = new MultItemPickerComponent(config, forwardDepPickerCallback);

        } catch (Exception e) {
            log.error("MultItemPickerComponent build error!", e);
            throw new UserMessageException("建立轉寄部門選單失敗, 請恰系統人員!", InfomationLevel.ERROR);
        }
    }

    /**
     * 轉寄部門選單 callback 事件
     */
    private final MultItemPickerCallback forwardDepPickerCallback = new MultItemPickerCallback() {

        /**
         * 
         */
        private static final long serialVersionUID = 6953218670533802194L;

        /**
         * 準備所有的項目
         * 
         * @return
         * @throws Exception
         */
        @Override
        public List<WkItem> prepareAllItems() throws SystemDevelopException {
            // 取得所有單位
            return multItemPickerByOrgHelper.prepareAllOrgItems();
        }

        /**
         * 準備已選擇項目
         * 
         * @return
         */
        @Override
        public List<WkItem> prepareSelectedItems() throws SystemDevelopException {
            if (WkStringUtils.isEmpty(forwardDeps)) {
                return Lists.newArrayList();
            }
            // 轉 wkiItem
            List<WkItem> selectedItems = Lists.newArrayList();
            for (Org dep : forwardDeps) {
                selectedItems.add(multItemPickerByOrgHelper.createDepItem(dep));
            }

            return selectedItems;
        }

        /**
         * 已選項目中, 被鎖定的項目
         */
        @Override
        public List<String> prepareDisableItemSids() throws SystemDevelopException {
            return lockforwardDepSids.stream()
                    .map(sid -> sid + "")
                    .collect(Collectors.toList());
        }

        /**
         * @return
         * @throws SystemDevelopException
         */
        @Override
        public List<SelectItem> prepareGroupItems() throws SystemDevelopException {
            return settingCustomGroupService.findDepGroupForMultItemPicker(SecurityFacade.getUserSid());
        }

        /**
         * 依據傳入的群組值, 準備該群組的項目
         * 
         * @return 回傳群組項目
         * @throws SystemDevelopException 未實做時拋出
         */
        public List<String> prepareItemSidByGroupSid(String groupSid) throws SystemDevelopException {
            return settingCustomGroupService.findGroupDepsBySid(Long.parseLong(groupSid));
        }
    };

    /**
     * 確認-儲存
     * 
     * @param isPreConfirm true:頁面呼叫 false: confirm callback
     */
    public void forwardDepPickerSave(boolean isPreConfirm) {

        // ================================
        // 取得選單上異動後的轉寄部門
        // ================================
        List<Integer> afterForwardDepSids = Lists.newArrayList();
        // 由元件中取得選擇項目
        if (this.forwardDepPicker.getSelectedSid() != null) {
            afterForwardDepSids = this.forwardDepPicker.getSelectedSid().stream()
                    .map(depSid -> Integer.parseInt(depSid))
                    .collect(Collectors.toList());
        }

        // ================================
        // 查詢所有部門轉寄資料
        // ================================
        // 查詢
        List<TrAlertInboxVO> trAlertInboxVOs = trAlertInboxService.findByRequireNoAndForwardType(
                requireNo,
                ForwardType.FORWARD_DEPT);
        // 防呆
        if (WkStringUtils.isEmpty(trAlertInboxVOs)) {
            trAlertInboxVOs = Lists.newArrayList();
        }

        // ================================
        // 判斷未異動資料
        // ================================
        // 取得未異動前轉寄部門資料
        List<Integer> beforeForwardDepSids = trAlertInboxVOs.stream()
                .map(TrAlertInboxVO::getReceiveDep)
                .collect(Collectors.toList());

        // 比對異動前後部門
        if (WkCommonUtils.compare(afterForwardDepSids, beforeForwardDepSids)) {
            MessagesUtils.showWarn("未異動轉寄單位");
            return;
        }

        // ================================
        // 檢查此次要移除的單位
        // ================================
        // 此次移除轉寄的部門 (前-後)
        List<Integer> removeForwardDepSids = Lists.newArrayList(beforeForwardDepSids);
        if (WkStringUtils.notEmpty(afterForwardDepSids)) {
            removeForwardDepSids.removeAll(afterForwardDepSids);
        }

        if (!WkStringUtils.isEmpty(removeForwardDepSids)) {

            List<TrAlertInboxVO> removeForwardDep = trAlertInboxVOs.stream()
                    .filter(vo -> removeForwardDepSids.contains(vo.getReceiveDep()))
                    .collect(Collectors.toList());

            // 判斷是否有無法移除的單位,並取得說明
            String cannotRemoveReason = this.requireForwardDeAndPersonService.prepareCannotRemoveForwordReasons(
                    requireNo,
                    removeForwardDep,
                    SecurityFacade.getUserSid());

            // 警示訊息
            if (WkStringUtils.notEmpty(cannotRemoveReason)) {
                MessagesUtils.showWarn(cannotRemoveReason);
                return;
            }
        }

        // ================================
        // 確認視窗 -> 確認轉寄異動結果
        // ================================
        if (isPreConfirm) {
            String confirmMessage = WkCommonContentUtils.prepareDiffMessageForDepartment(
                    beforeForwardDepSids,
                    afterForwardDepSids,
                    "新增轉寄單位",
                    "收回轉寄單位");

            confirmMessage = "<span class='WS1-1-3'>異動【轉寄單位】結果如下：</span>"
                    + "<br/><br/>"
                    + confirmMessage
                    + "<br/><br/>";

            this.confirmCallbackDialogController.showConfimDialog(
                    confirmMessage,
                    Lists.newArrayList(),
                    () -> forwardDepPickerSave(false));

            return;
        }

        // ================================
        // 異動轉寄資料
        // ================================
        try {
            this.requireForwardDeAndPersonService.saveForwardDep(
                    requireNo,
                    afterForwardDepSids,
                    SecurityFacade.getUserSid());
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            String message = WkMessage.PROCESS_FAILED + "!";
            MessagesUtils.showError(message);
            log.error(message + e.getMessage(), e);
            return;
        }

        // ================================
        // 重新查詢轉寄資料
        // ================================
        try {
            this.prepareForwardInfo();
        } catch (Exception e) {
            log.error("查詢最新資料失敗!" + e.getMessage(), e);
            MessagesUtils.showError(WkMessage.EXECTION);
            return;
        }

        // ================================
        // 畫面控制
        // ================================
        // 更新需求單主單畫面
        this.reLoadForward();
        // 更新dialog 內容
        this.displayController.update(this.MAIN_DIALOG_CONTENT);
        // 關閉 dialog
        this.displayController.hidePfWidgetVar(this.FORWARD_DEP_DIALOG_NAME);
        // 訊息
        MessagesUtils.showInfo("轉寄部門成功!");

    }

    // ========================================================================
    // 轉寄人員選單
    // ========================================================================
    /** 轉寄人員選擇器 */
    @Getter
    private MultiUserQuickPickerComponent forwardUserPicker;
    /** 轉寄留言 */
    @Getter
    @Setter
    private String messageCss;

    /**
     * 開啟轉寄人員選單
     */
    public void forwardUserPickerOpenDialog() {

        // ================================
        // 準備選單元件
        // ================================
        try {
            this.forwardUserPickerInitializer();
        } catch (Exception e) {
            String errorMessage = "建立使用者選單失敗!";
            log.error(errorMessage, e);
            MessagesUtils.showError("建立單位選單失敗, 請恰系統人員!");
            return;
        }

        // ================================
        // 初始化留言訊息
        // ================================
        this.messageCss = "";

        // ================================
        // 畫面處理
        // ================================
        // 更新dialog 內容
        this.displayController.update(this.FORWARD_USER_DIALOG_CONTENT);
        // 開啟 dialog
        this.displayController.showPfWidgetVar(this.FORWARD_USER_DIALOG_NAME);
    }

    /**
     * 初始化人員選單元件
     * 
     * @throws UserMessageException
     */
    private void forwardUserPickerInitializer() throws UserMessageException {

        // 選擇器設定資料
        MultiUserQuickPickerConfig config = new MultiUserQuickPickerConfig();
        // 預設展開登入者
        config.setDefaultExpandUserSids(Sets.newHashSet(SecurityFacade.getUserSid()));
        // 開啟群組模式
        config.setEnableGroupMode(true);
        
        long startTime = System.currentTimeMillis();
        
        // 選擇器初始化
        this.forwardUserPicker = new MultiUserQuickPickerComponent(
                SecurityFacade.getCompanyId(),
                this.FORWARD_USER_PICKER_ID,
                Lists.newArrayList(), // 不預設選擇人員
                config,
                new MultiUserQuickPickerCallback());// 直接傳入 MultiUserQuickPickerCallback :實作取得群組的方式用預設
        
        log.info(WkCommonUtils.prepareCostMessage(startTime, "open user 使用時間"));
    }

    /**
     * 人員轉寄 - 確認
     * @param isPreConfirm
     * @param isPassForwardedCheck
     */
    public void forwardUserPickerSave(boolean isPreConfirm, boolean isPassForwardedCheck) {

        // ================================
        // 取得選擇的使用者
        // ================================
        // 從元件中取得名單
        List<Integer> selectedForwardUserSids = this.forwardUserPicker.getSelecedtUserSids();
        if (WkStringUtils.isEmpty(selectedForwardUserSids)) {
            MessagesUtils.showWarn("您尚未選擇轉寄人員!");
            return;
        }

        // ================================
        // 檢查是否已轉寄過
        // ================================
        if (isPassForwardedCheck) {
            // 查詢轉寄過的資料
            List<Integer> recivers = trAlertInboxService.findReceiverByRequireNoAndSender(
                    requireNo,
                    SecurityFacade.getUserSid());

            // 交集得出重複轉寄的 user
            List<Integer> alreadyforwardedUserSids = WkMathUtils.getIntersection(selectedForwardUserSids, recivers);

            // 有重複轉寄 user 時, 跳確認框
            if (WkStringUtils.notEmpty(alreadyforwardedUserSids)) {

                String confirmMessage = "<span class='WS1-1-2'>您已經轉寄過下列人員：</span>"
                        + "<br/><br/>"
                        + WkUserUtils.findNameBySid(alreadyforwardedUserSids, "<br/>")
                        + "<br/><br/>"
                        + "是否要再轉寄一次呢?"
                        + "<br/><br/>";

                this.confirmCallbackDialogController.showConfimDialog(
                        confirmMessage,
                        Lists.newArrayList(),
                        () -> forwardUserPickerSave(true, false));

                return;
            }
        }

        // ================================
        // 檢查是否已轉寄過
        // ================================
        if (isPreConfirm) {
            String confirmMessage = "<span class='WS1-1-3'>將轉寄給下列人員：</span>"
                    + "<br/><br/>"
                    + WkCommonContentUtils.prepareUserNameWithDepByDataListStyle(
                            selectedForwardUserSids,
                            OrgLevel.MINISTERIAL,
                            true,
                            ">",
                            "",
                            false)
                    + "<br/><br/>"
                    + "是否確定?"
                    + "<br/><br/>";

            this.confirmCallbackDialogController.showConfimDialog(
                    confirmMessage,
                    Lists.newArrayList(),
                    () -> this.forwardUserPickerSave(false, false));
            return;
        }

        // ================================
        // update
        // ================================
        try {
            this.requireForwardDeAndPersonService.saveForwardUser(
                    requireNo,
                    selectedForwardUserSids,
                    messageCss,
                    SecurityFacade.getUserSid());

        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            String message = WkMessage.PROCESS_FAILED + "!";
            MessagesUtils.showError(message);
            log.error(message + e.getMessage(), e);
            return;
        }

        // ================================
        // 重新查詢轉寄資料
        // ================================
        try {
            this.prepareForwardInfo();
        } catch (Exception e) {
            log.error("查詢最新資料失敗!" + e.getMessage(), e);
            MessagesUtils.showError(WkMessage.EXECTION);
            return;
        }

        // ================================
        // 畫面控制
        // ================================
        // 更新需求單主單畫面
        this.reLoadForward();
        // 更新dialog 內容
        this.displayController.update(this.MAIN_DIALOG_CONTENT);
        // 關閉 dialog
        this.displayController.hidePfWidgetVar(this.FORWARD_USER_DIALOG_NAME);
        // 訊息
        MessagesUtils.showInfo("轉寄成功!");
    }
}
