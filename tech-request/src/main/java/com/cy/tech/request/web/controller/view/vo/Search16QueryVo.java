/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.vo;

import com.cy.tech.request.logic.vo.query.search.SearchQuery;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.work.common.enums.ReadRecordType;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @author shaun
 */
@EqualsAndHashCode(callSuper = true, of = {})
public class Search16QueryVo extends SearchQuery implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4328448608948389555L;
    //////////////////////////////////////////////////////////////////////////// 第一排搜尋條件
    /** 選擇的大類 */
    @Getter
    @Setter
    private String selectBigCategory;
    /** 選擇的需求製作進度 */
    @Getter
    @Setter
    private RequireStatusType selectRequireStatusType;
    /** 選擇的閱讀類型 */
    @Getter
    @Setter
    private ReadRecordType selectReadRecordType;
    /** ON程式填單單位 */
    @Getter
    @Setter
    private List<String> createOnpgDepts = Lists.newArrayList();
    /** ON程式通知單位 */
    @Getter
    @Setter
    private List<String> onpgNoticeDepts = Lists.newArrayList();

    //////////////////////////////////////////////////////////////////////////// 第二排搜尋條件
    /** 類別組合 大類 */
    @Getter
    @Setter
    private List<String> bigDataCateSids = Lists.newArrayList();
    /** 類別組合 中類 */
    @Getter
    @Setter
    private List<String> middleDataCateSids = Lists.newArrayList();
    /** 類別組合 小類 */
    @Getter
    @Setter
    private List<String> smallDataCateSids = Lists.newArrayList();
    /** 待閱原因 */
    @Getter
    @Setter
    private List<WaitReadReasonType> readReason = Lists.newArrayList();
    /** ON程式狀態 */
    @Getter
    @Setter
    private List<String> onPgStatus = Lists.newArrayList();
    
    /** 回覆人員 */
    @Getter
    @Setter
    private List<Integer> replyUsers = Lists.newArrayList();
    //////////////////////////////////////////////////////////////////////////// 第三排搜尋條件
    /** ON程式填單人員 */
    @Getter
    @Setter
    private String opCreatedUsrName;
    /** 主題 */
    @Getter
    @Setter
    private String onpgTheme;
    /** 模糊搜尋 */
    @Getter
    @Setter
    private String fuzzyText;
    
    //////////////////////////////////////////////////////////////////////////// 第四排搜尋條件
    /** 時間切換的index */
    @Getter
    @Setter
    private int dateTypeIndex;
    /** ON程式填單區間 啟始日 */
    @Getter
    private Date onpgStartDt;
    

    
    /** ON程式填單區間 結束日 */
    @Getter
    @Setter
    private Date onpgEndDt;

    @Getter
    @Setter
    private String dateTypeIndexStr;

    /**
     * @param onpgStartDt the onpgStartDt to set
     */
    public void setOnpgStartDt(Date onpgStartDt) { this.onpgStartDt = onpgStartDt; }

}
