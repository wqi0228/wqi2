/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.setting;

import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.tech.request.logic.service.TemplateService;
import com.cy.tech.request.logic.service.setting.SettingDefaultAssignSendDepService;
import com.cy.tech.request.vo.category.BasicDataSmallCategory;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.setting.asdep.SettingDefaultAssignSendDepVO;
import com.cy.tech.request.vo.template.CategoryKeyMapping;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 類別設定查詢
 *
 * @author jason_h
 */
@Controller
@Scope("view")
@Slf4j
public class Setting06MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2583905556251162381L;
    /** 模版服務 */
    @Autowired
    transient private TemplateService tmpService;
    @Autowired
    transient private SettingDefaultAssignSendDepService settingDefaultAssignSendDepService;

    /** 查詢文字 */
    @Getter
    @Setter
    private String searchText = "";
    /** 所有模版類別 */
    @Getter
    @Setter
    private List<CategoryKeyMapping> allTmpCategories;
    /**
     * 分派/通知部門設定資訊
     */
    @Getter
    Map<String, SettingDefaultAssignSendDepVO> defaultDepInfoMapByDataKey;
    /**
     * 檢查項目
     */
    @Getter
    private List<RequireCheckItemType> checkItemTypes = Lists.newArrayList(RequireCheckItemType.values());

    @PostConstruct
    private void init() {
        this.searchByText();
    }

    public void searchByText() {
        this.allTmpCategories = tmpService.findAllByFuzzyTextAndMaxVersion(searchText);
        this.prepareDefaultDepInfo();
    }

    /**
     * 取得小類的失效日
     *
     * @param small
     * @return
     */
    public Date getSmallCateDisableDate(BasicDataSmallCategory small) {
        if (Activation.INACTIVE.equals(small.getStatus())) {
            return small.getUpdatedDate();
        }
        return null;
    }

    /**
     * 準備分派/通知單位設定資訊
     */
    private void prepareDefaultDepInfo() {
        // ====================================
        // 查詢全部設定
        // ====================================
        List<SettingDefaultAssignSendDepVO> settingInfos = this.settingDefaultAssignSendDepService.findAll();

        // ====================================
        // 兜組資料集
        // ====================================
        this.defaultDepInfoMapByDataKey = Maps.newHashMap();

        for (SettingDefaultAssignSendDepVO vo : settingInfos) {

            // ====================================
            // 處理顯示資料
            // ====================================
            if (WkStringUtils.isEmpty(vo.getDepsInfo())) {
                vo.setDepsInfoTag(WkHtmlUtils.addRedBlodClass("[未設定]"));
                vo.setDepsInfoForExcel("無");
            } else {
                // 去除重複
                Set<Integer> depSids = Sets.newHashSet(vo.getDepsInfo());

                // 計算設定部門 -> 啟用、停用個數
                int activeCnt = 0;
                int inactiveCnt = 0;
                for (Integer depSid : depSids) {
                    if (WkOrgUtils.isActive(depSid)) {
                        activeCnt++;
                    } else {
                        inactiveCnt++;
                    }
                }
                // 組顯示tag
                String depsInfoTag = "[單位:%s個%s]";
                String inactiveInfo = inactiveCnt > 0 ? WkHtmlUtils.addRedBlodClass(",停用:" + inactiveCnt + "個") : "";
                depsInfoTag = String.format(depsInfoTag, activeCnt + "", inactiveInfo);
                vo.setDepsInfoTag(WkHtmlUtils.addBlueBlodClass(depsInfoTag));

                // tool tip
                vo.setDepsInfoForTooltip(WkOrgUtils.prepareDepsNameByTreeStyle(Lists.newArrayList(depSids), 40));

                List<Integer> sortDepSids = WkOrgUtils.sortByOrgTree(depSids);

                // Excel (排除停用單位)
                String depsInfoForExcel = sortDepSids.stream()
                        .filter(sid -> WkOrgUtils.isActive(sid))
                        .map(sid -> WkOrgUtils.prepareBreadcrumbsByDepNameAndMakeup(sid, OrgLevel.DIVISION_LEVEL, false, "－"))
                        .collect(Collectors.joining("\r\n"));

                if (WkStringUtils.isEmpty(depsInfoForExcel)) {
                    depsInfoForExcel = "無";
                }

                vo.setDepsInfoForExcel(depsInfoForExcel);

            }

            // ====================================
            // 放入 map
            // ====================================
            // 組 key
            String dataKey = this.settingDefaultAssignSendDepService.prepareDataKey(
                    vo.getSmallCategorySid(), vo.getCheckItemType(), vo.getAssignSendType());

            defaultDepInfoMapByDataKey.put(dataKey, vo);
        }
    }

    /**
     * 取得分派/通知單位設定資料
     * 
     * @param smallCategory  小類 SID
     * @param checkItemType  檢查項目
     * @param assignSendType 分派/通知類別
     * @param valueType      需要的資料型態
     * @return 顯示內容
     */
    public String getDefaultDepInfoForView(
            BasicDataSmallCategory smallCategory,
            RequireCheckItemType checkItemType,
            String assignSendTypeStr,
            String valueType) {

        AssignSendType assignSendType = AssignSendType.safeValueOf(assignSendTypeStr);

        // ====================================
        // 防呆
        // ====================================
        if (this.defaultDepInfoMapByDataKey == null) {
            log.warn("defaultDepInfoMapByDataKey 尚未初始化 (開發階段錯誤, 不應該發生)");
            return "";
        }

        if (smallCategory == null
                || checkItemType == null
                || assignSendType == null) {
            log.warn("傳入資料有誤（null?）smallCategorySid:[{}],checkItemType:[{}],assignSendType[{}]",
                    (smallCategory == null ? "null" : smallCategory.getSid()),
                    checkItemType,
                    assignSendTypeStr);
            return "";
        }

        // ====================================
        // 取vo
        // ====================================
        // 組 key
        String dataKey = this.settingDefaultAssignSendDepService.prepareDataKey(
                smallCategory.getSid(), checkItemType, assignSendType);

        // 取值
        SettingDefaultAssignSendDepVO vo = this.defaultDepInfoMapByDataKey.get(dataKey);
        if (vo == null) {
            switch (valueType) {
            case "TAG":
                return WkHtmlUtils.addRedBlodClass("[未設定]");
            case "TOOLTIP":
                return "";
            case "EXCEL":
                return "無";
            }
            return "";
        }

        // ====================================
        // 取值
        // ====================================
        switch (valueType) {
        case "TAG":
            return vo.getDepsInfoTag();

        case "TOOLTIP":
            return vo.getDepsInfoForTooltip();

        case "EXCEL":
            String value = vo.getDepsInfoForExcel();
            value = value.replaceAll("<span style='color:gray'>", "");
            value = value.replaceAll("</span>", "");
            value = value.replaceAll("<span style='font-weight:bold;'>", "");
            // value = value.replaceAll("&nbsp;&nbsp;", " ");
            // value = WkJsoupUtils.getInstance().htmlToText(value);
            // value = value.replaceAll("<br/>", "\r\n");
            // value = value.replaceAll("<BR>", "\r\n");
            // value = value.replaceAll("<BR/>", "\r\n");
            // value = value.replaceAll("<br>", "\r\n");

            return value;
        }

        log.warn("傳入資料有誤, 未定義的 valueType:[{}]", valueType);

        return "";
    }

    // ========================================================================
    // 匯出
    // ========================================================================

    /**
     * EXCEL匯出名稱
     * 
     * @return
     */
    public String prepareExcelName() {
        return "Req(" + WkDateUtils.formatDate(new Date(), WkDateUtils.YYYY_MM_DD2);
    }

    public void postProcessXLS(Object document) {

        HSSFWorkbook wb = (HSSFWorkbook) document;
        wb.setSheetName(0, "設定資訊");
        HSSFSheet sheet = wb.getSheetAt(0);

        if (sheet == null || sheet.getRow(0) == null) {
            return;
        }

        // ====================================
        // 調整 Header
        // ====================================

        HSSFRow headerRow = sheet.getRow(0);

        // headerRow.setHeight((short) 400);

        //int columnIndex = 0;
        for (Cell cell : headerRow) {

            HSSFCellStyle style = wb.createCellStyle();
            // 底色:藍底
            style.setFillForegroundColor(HSSFColor.HSSFColorPredefined.BLUE.getIndex());

            HSSFFont font = wb.createFont();
            // 白字
            font.setColor(HSSFColor.HSSFColorPredefined.WHITE.getIndex());
            // 粗體
            font.setBold(true);
            // 字體大小 12
            font.setFontHeight((short) (12 * 20));
            style.setFont(font);
            style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            // 置中對齊
            style.setVerticalAlignment(VerticalAlignment.CENTER);
            style.setAlignment(HorizontalAlignment.CENTER);

            cell.setCellStyle(style);

            // 欄位寬度
            // sheet.setColumnWidth(index, (int) (200 * 0.2 * 256));
            // sheet.autoSizeColumn(columnIndex);

            //columnIndex++;
        }

        // ====================================
        // CELL
        // ====================================
        for (Row row : sheet) {
            int maxLine = 1;
            int cellIndex = 0;
            for (Cell cell : row) {
                RichTextString richTextString = cell.getRichStringCellValue();
                int breakline = WkStringUtils.safeTrim(richTextString.toString())
                        .split("\r\n").length;
                if (breakline > maxLine) {
                    maxLine = breakline;
                }

                HSSFCellStyle style = wb.createCellStyle();

                // 置中對齊
                style.setVerticalAlignment(VerticalAlignment.CENTER);

                if (cellIndex > 2) {
                    style.setAlignment(HorizontalAlignment.CENTER);
                }

                cell.setCellStyle(style);

                cellIndex++;
            }

            int rowHeight = 400;
            if (maxLine > 1) {
                rowHeight = (290 * maxLine);
            }

            row.setHeight((short) rowHeight);
        }

        // ====================================
        //
        // ====================================
        this.autoSizeColumns(wb);

    }

    public void autoSizeColumns(HSSFWorkbook workbook) {
        int numberOfSheets = workbook.getNumberOfSheets();
        for (int i = 0; i < numberOfSheets; i++) {
            HSSFSheet sheet = workbook.getSheetAt(i);
            if (sheet.getPhysicalNumberOfRows() > 0) {
                Row row = sheet.getRow(sheet.getFirstRowNum());
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    int columnIndex = cell.getColumnIndex();
                    sheet.autoSizeColumn(columnIndex);
                }
            }
        }
    }

}
