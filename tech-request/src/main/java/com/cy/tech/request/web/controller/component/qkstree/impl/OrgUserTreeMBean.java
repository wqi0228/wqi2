package com.cy.tech.request.web.controller.component.qkstree.impl;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.web.controller.component.qkstree.AbstractQucikSelectTreeComponentMBean;
import com.cy.tech.request.web.controller.component.qkstree.QuickSelectTreeCallback;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

/**
 * 組織人員挑選樹
 * 
 * @author allen
 */
@Slf4j
@Component
@Scope("view")
public class OrgUserTreeMBean extends AbstractQucikSelectTreeComponentMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4024594393264104828L;
    // ========================================================================
    // 服務區
    // ========================================================================
    @Autowired
    private transient WkUserCache wkUserCache;
    @Autowired
    private transient WkOrgCache wkOrgCache;

    // ========================================================================
    // 參數區
    // ========================================================================
    /**
     * 登入者公司別 SID (建構時傳入)
     */
    private Integer loginUserCompSid;

    /**
     * 登入者單位 SID (建構時傳入)
     */
    private Integer loginUserOrgSid;

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * 初始化方法
     * 
     * @param loginUserCompSid 登入者公司別 SID
     * @param loginUserOrgSid  登入者單位 SID
     * @param callback
     */
    public void init(
            Integer loginUserCompSid,
            Integer loginUserOrgSid,
            QuickSelectTreeCallback callback) {

        try {
            // ====================================
            // 初始化前執行
            // ====================================
            this.brforeInitialize(callback);

            // ====================================
            // 記錄傳入參數
            // ====================================
            this.loginUserCompSid = loginUserCompSid;
            this.loginUserOrgSid = loginUserOrgSid;

            // ====================================
            // 建立單位樹
            // ====================================
            this.buildOrgTree();

            // ====================================
            // 展開登入者單位
            // ====================================
            this.expandedUserOrgNode();

            // ====================================
            // 取得已選清單人員
            // ====================================
            // 由外部方法取得已選取的 user sid list
            List<Integer> userSids = this.callback.getSelectedDataList();
            // 整理已選擇人員
            this.prepareSelectedUsers((List<Integer>) userSids);

            // ====================================
            // 初始後前執行
            // ====================================
            this.afterInitialize();

        } catch (Exception e) {
            this.message = "初始化失敗![" + e.getMessage() + "]";
            log.error(this.message + "\r\n" + WkStringUtils.getExceptionStackTrace(e));
        }
    }

    /**
     * 事件：切換【顯示停用】
     */
    public void switchShowActiveOrinactiveMode() {
        try {

            // ====================================
            // 建立單位樹
            // ====================================
            this.buildOrgTree();

            // ====================================
            // 展開登入者單位
            // ====================================
            this.expandedUserOrgNode();

            // ====================================
            // 整理已選擇人員
            // ====================================
            this.prepareSelectedUsers(getSelectedDataList());

        } catch (Exception e) {
            log.error("重建組織樹處理失敗! " + e.getMessage() + "\r\n" + WkStringUtils.getExceptionStackTrace(e));
        }
    }

    /**
     * 回傳面選擇的人員 SID 清單
     * 
     * @return
     */
    public List<Integer> getSelectedDataList() {

        // 取得目前畫面選取的 user sid list
        if (WkStringUtils.isEmpty(this.selectedNodeDataJsonString)) {
            this.selectedNodeDataJsonString = "[]";
        }
        List<OrgUserTreeNodeData> userList = new Gson().fromJson(
                this.selectedNodeDataJsonString,
                new TypeToken<List<OrgUserTreeNodeData>>() {
                }.getType());

        List<Integer> userSids = Lists.newArrayList();
        for (OrgUserTreeNodeData orgUserTreeNodeData : userList) {
            userSids.add(Integer.parseInt(orgUserTreeNodeData.getSid()));
        }

        return userSids;
    }

    /**
     * 展開登入者單位
     */
    private void expandedUserOrgNode() {
        // ====================================
        // 將TreeNode 轉為 list
        // ====================================
        List<TreeNode> allNodelist = Lists.newArrayList();
        this.treeNodeToList(allNodelist, this.rootTreeNode);

        // ====================================
        // 取得使用者單位的 tree node
        // ====================================
        this.setSelectedNode(null);
        for (TreeNode eachTreeNode : allNodelist) {
            if (eachTreeNode.getData() == null) {
                continue;
            }
            // 比對為單位節點, 且 SID 相同
            OrgUserTreeNodeData nodeData = (OrgUserTreeNodeData) eachTreeNode.getData();
            if (!nodeData.isDataNode() && (nodeData.getSid() + "").equals(this.loginUserOrgSid + "")) {
                this.setSelectedNode(eachTreeNode);
            }
        }
        if (this.getSelectedNode() == null) {
            return;
        }

        // ====================================
        // 展開預設單位上層節點
        // ====================================
        this.expandForChildren(this.getSelectedNode());

        // ====================================
        // 處理使用者單位節點
        // ====================================
        // 展開本層節點
        this.getSelectedNode().setExpanded(true);
        // 標註選擇節點
        this.getSelectedNode().setSelected(true);

    }

    /**
     * 展開該子節點上層的父節點，若該父節點還有父節點也展開，直到根節點。
     *
     * @param children
     */
    private void expandForChildren(TreeNode children) {
        if (children.getParent() != null) {
            children.getParent().setExpanded(true);
            this.expandForChildren(children.getParent());
        }
    }

    /**
     * 整理已選擇人員
     */
    private void prepareSelectedUsers(List<Integer> userSids) {

        // ====================================
        // 整理成要使用的資料結構
        // ====================================
        // 初始化 list
        List<AbstractTreeComponentNodeData> currSelectedList = Lists.newArrayList();
        // 防呆
        if (WkStringUtils.isEmpty(userSids)) {
            return;
        }

        // 逐筆處理
        for (Integer userSid : userSids) {
            // 取得 user 資料
            User user = wkUserCache.findBySid(userSid);
            if (user == null) {
                continue;
            }
            // 由所有 node 資料中, 取出該筆 user 的 node data
            String key = true + "" + user.getSid();

            AbstractTreeComponentNodeData nodeData = null;
            if (this.allNodeDataMapBySid.containsKey(key)) {
                nodeData = this.allNodeDataMapBySid.get(key);
            } else {
                // 找不到時, 可能為 user 已經停用,所以 tree 的資料沒有
                nodeData = new OrgUserTreeNodeData(user);
                nodeData.setRowKey("__" + nodeData.getOuName() + "(" + nodeData.getSid() + ")");
            }
            currSelectedList.add(nodeData);
        }

        // 排序 - 依照樹狀清單順序
        currSelectedList = currSelectedList
                .stream()
                .sorted(Comparator.comparing(AbstractTreeComponentNodeData::getRowKey))
                .collect(Collectors.toList());

        this.selectedNodeDataJsonString = new Gson().toJson(currSelectedList);

        this.selectedList = currSelectedList;
    }

    /**
     * 建立單位樹
     * 
     * @param orgNameFilterStr 不為空時, 過濾單位名稱
     * @throws Exception
     */
    private void buildOrgTree() throws Exception {

        try {

            // ====================================
            // 建立 Tree 結構
            // ====================================
            Org loginUserComp = wkOrgCache.findBySid(loginUserCompSid);
            // 根節點為公司別
            this.rootTreeNode = new DefaultTreeNode(new OrgUserTreeNodeData(loginUserComp));

            List<Org> allDeps = WkOrgUtils.sortOrgByOrgTree(wkOrgCache.findAll());

            // 遞迴建立子單位樹
            this.RecursiveBuildNodeTree(
                    this.rootTreeNode,
                    allDeps);

            // ====================================
            // 呼叫公用方法
            // ====================================
            this.afterbuildTreeData();

        } catch (Exception e) {
            log.error("建置組織樹失敗!!", e);
            throw e;
        }
    }

    /**
     * 加入部門 User 節點
     * 
     * @param orgNode
     */
    private void appendUserNode(TreeNode orgNode) {

        if (orgNode == null || orgNode.getData() == null) {
            return;
        }

        OrgUserTreeNodeData orgNodeData = (OrgUserTreeNodeData) orgNode.getData();

        // user 節點不做處理 + 防呆
        if (orgNodeData.isDataNode() || WkStringUtils.isEmpty(orgNodeData.getSid())) {
            return;
        }

        // 取得單位內所有的 User
        List<User> orgUsers = wkUserCache.getUsersByPrimaryOrgSid(Integer.parseInt(orgNodeData.getSid()));

        if (WkStringUtils.isEmpty(orgUsers)) {
            return;
        }

        // 依據人名排序
        orgUsers.sort(Comparator.comparing(User::getName));

        for (User user : orgUsers) {
            // 不存在或停用
            if (user == null) {
                continue;
            }

            // 不顯示停用時過濾停用
            if (!this.queryShowInactiveNode && Activation.INACTIVE.equals(user.getStatus())) {
                continue;
            }

            // 依附此 user 於單位節點
            new DefaultTreeNode(new OrgUserTreeNodeData(user), orgNode);
        }
    }

    /**
     * 遞迴建立子單位樹
     * 
     * @param parentNode 父節點
     * @param allOrgList 所有單位SID
     * @throws Exception
     */
    private void RecursiveBuildNodeTree(
            TreeNode parentNode,
            List<Org> allOrgList) throws Exception {

        if (parentNode == null || parentNode.getData() == null) {
            log.warn("RecursiveBuildNodeTree:傳入資料為空");
            return;
        }

        // ====================================
        // 取得父節點資料
        // ====================================
        if (!(parentNode.getData() instanceof OrgUserTreeNodeData)) {
            throw new Exception("RecursiveBuildNodeTree: 傳入資料型別錯誤 [" + parentNode.getData().getClass() + "]");
        }

        OrgUserTreeNodeData parentNodeData = (OrgUserTreeNodeData) parentNode.getData();

        // user 節點不做處理
        if (parentNodeData.isDataNode()) {
            return;
        }

        // ====================================
        // 加入此節點 User
        // ====================================
        // 不取根節點 user
        if (parentNode.getParent() != null) {
            this.appendUserNode(parentNode);
        }

        // ====================================
        // 取得傳入單位 的子部門 （僅下一層）
        // ====================================
        // 防呆
        if (WkStringUtils.isEmpty(parentNodeData.getSid())) {
            return;
        }
        // 從所有單位List中, 取出父單位的下一層子單位
        List<Org> childOrgList = allOrgList.stream()
                .filter(eachChildOrg -> this.isTargetOrgParent(eachChildOrg, parentNodeData.getSid()))
                .collect(Collectors.toList());

        if (childOrgList == null || childOrgList.isEmpty()) {
            return;
        }

        // ====================================
        // 建立子部門 tree
        // ====================================
        // 將子部門, 依附於父節點
        for (Org eachChildOrg : childOrgList) {
            // 未勾選【顯示停用】時, 排除停用單位
            if (!this.queryShowInactiveNode &&
                    Activation.INACTIVE.equals(eachChildOrg.getStatus())) {
                continue;
            }

            // 建立此單位節點
            TreeNode currOrgNode = new DefaultTreeNode(new OrgUserTreeNodeData(eachChildOrg), parentNode);

            // 遞迴繼續處理此子部門的下一層部門
            this.RecursiveBuildNodeTree(currOrgNode, allOrgList);
        }
    }

    /**
     * 比對是否為目標單位的父單位
     * 
     * @param targetOrg
     * @param parentOrg
     * @return
     */
    private boolean isTargetOrgParent(Org targetOrg, String parentOrgSid) {
        if (targetOrg == null || targetOrg.getParent() == null) {
            return false;
        }
        return (targetOrg.getParent().getSid() + "").equals(parentOrgSid);
    }

    // ==========================================================================
    //
    // ==========================================================================
    /**
     * @author allen
     *
     */
    public class OrgUserTreeNodeData extends AbstractTreeComponentNodeData {

        /**
         * 
         */
        private static final long serialVersionUID = 5605117936203835372L;

        public OrgUserTreeNodeData(Org org) {
            this.dataNode = false;
            if (org != null) {
                this.name = org.getName();
                this.sid = org.getSid() + "";
                this.active = Activation.ACTIVE.equals(org.getStatus());
            }
        }

        public OrgUserTreeNodeData(User user) {
            this.dataNode = true;
            if (user != null) {
                this.name = user.getName();
                this.sid = user.getSid() + "";
                this.active = Activation.ACTIVE.equals(user.getStatus());
                this.ouName = user.getPrimaryOrg().getName() + " - " + user.getName();
            }
        }

        public OrgUserTreeNodeData(String name, String sid, boolean isUserNode, String nodeDataRowKey) {
            this.name = name;
            this.sid = sid;
            this.rowKey = nodeDataRowKey;
            this.dataNode = isUserNode;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null)
                return false;
            if (!(obj instanceof OrgUserTreeNodeData)) {
                return false;
            }
            OrgUserTreeNodeData o = (OrgUserTreeNodeData) obj;

            // 比較節點類型
            if (this.isDataNode() != o.isDataNode()) {
                return false;
            }

            // 比較SID
            return this.getSid().equals(o.getSid());

        }

        @Override
        public int hashCode() {
            String aa = this.isDataNode() ? "10000" : "99999";
            aa += this.sid;
            return Integer.parseInt(aa);
        }
    }

    // ==========================================================================
    // 顯示
    // ==========================================================================
    /**
     * @param userSids
     * @return
     */
    public String prepareUserName(List<Integer> userSids, String emptyDefaultContent) {
        if (WkStringUtils.isEmpty(userSids)) {
            return emptyDefaultContent;
        }
        return WkUserUtils.findNameBySid(userSids, "、");
    }

    /**
     * @param userSids
     * @return
     */
    public String prepareUserNameForToolTip(List<Integer> userSids, String emptyDefaultContent) {
        if (WkStringUtils.isEmpty(userSids)) {
            return emptyDefaultContent;
        }
        return WkUserUtils.prepareUserNameWithDep(
                userSids,
                OrgLevel.MINISTERIAL,
                true,
                "-",
                "<br/>",
                false);
    }

}
