package com.cy.tech.request.web.controller.enums;

import com.cy.commons.enums.Activation;
import java.io.Serializable;
import javax.faces.model.SelectItem;
import org.springframework.stereotype.Controller;

/**
 *
 * @author shaun
 */
@Controller
public class ActivationBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7647133272267219202L;

    public SelectItem[] getValues() {
        SelectItem[] items = new SelectItem[Activation.values().length];
        int i = 0;
        for (Activation each : Activation.values()) {
            //String label = common.get(each);
            items[i++] = new SelectItem(each, Activation.ACTIVE.equals(each)?"正常":"停用");
        }
        return items;
    }
}
