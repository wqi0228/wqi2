/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.component;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.enums.OrgType;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.SpecificPermissionService;
import com.cy.tech.request.web.listener.ReportOrgTreeCallBack;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.collections.CollectionUtils;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.NodeUnselectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 * 報表 組織樹 Component
 *
 * @author kasim
 */
@Slf4j
public class ReportOrgTreeComponent implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 498909024040036693L;
    private OrganizationService orgService;
	private SpecificPermissionService spService;

	/** 訊息呼叫 */
	private ReportOrgTreeCallBack reportOrgTreeCallBack;

	/** 登入公司 */
	private String compId;
	/** 登入單位 */
	@SuppressWarnings("unused")
	private Org loginDep;
	/** 登入角色 */
	private List<Long> loginRoleSids;

	/** 呼叫不包含停用部門的方法 */
	/** true >>invoke rebuildRootByDeptLevel() */
	/** false >> invoke rebuildRoot() */
	private boolean buildByDeptLevel;
	@Getter
	@Setter
	/** 組織樹搜尋字串 */
	private String deptText;
	@Getter
	@Setter
	/** 組織樹 - 不包含停用部門 */
	private boolean selectJustActive;
	@Getter
	@Setter
	/** 組織樹 - 全選 */
	private boolean selectAllNode;
	@Getter
	@Setter
	/** 組織樹 - 樹狀容器 */
	private TreeNode orgTreeRoot;
	@Getter
	@Setter
	/** 組織樹 - 選擇的樹 */
	private TreeNode[] orgTreeSelect;
	/** 需要註記部門 */
	@Getter
	@Setter
	private List<Org> highlightOrgs;

	/** */
	private Org root;
	/** */
	private List<Org> allOrgs;
	/** 需disable 單位 */
	private List<Org> disableOrgList;

	/** */
	private List<TreeNode> disableTreeNode;

	@Getter
	@Setter
	private TreeNode singleSelectedNode;

	private WkOrgCache wkOrgCache = WkOrgCache.getInstance();

	/**
	 * 建構式
	 *
	 * @param buildByDeptLevel
	 * @param reportOrgTreeCallBack
	 */
	public ReportOrgTreeComponent(String compId, Org loginDep, List<Long> roleSids, boolean buildByDeptLevel, ReportOrgTreeCallBack reportOrgTreeCallBack) {
		this.orgService = WorkSpringContextHolder.getBean(OrganizationService.class);
		this.spService = WorkSpringContextHolder.getBean(SpecificPermissionService.class);
		this.reportOrgTreeCallBack = reportOrgTreeCallBack;

		this.compId = compId;
		this.loginDep = loginDep;
		this.loginRoleSids = roleSids;
		this.buildByDeptLevel = buildByDeptLevel;
		this.highlightOrgs = Lists.newArrayList();
		this.selectAllNode = false;
		this.selectJustActive = true;
	}

	/**
	 * 初始化
	 *
	 * @param org
	 * @param selDepts
	 */
	public void initOrgTree(Org org, List<String> selDepts) {
		this.deptText = "";
		this.initRoot(org);
		this.markSelectedNode(selDepts, orgTreeRoot);
	}

	/**
	 * 初始化
	 *
	 * @param org
	 * @param selDepts
	 * @param isJustActiveDep
	 * @param isSeleAll
	 */
	public void initOrgTree(Org org, List<String> selDepts, boolean isJustActiveDep, boolean isSeleAll) {
		this.selectJustActive = isJustActiveDep;
		this.selectAllNode = isSeleAll;
		this.deptText = "";
		this.resetSelectAll();
		this.initRootByDeptLevel(compId, org);
		this.markSelectedNode(selDepts, orgTreeRoot);
		this.againResetSelectAll();
	}

	private void againResetSelectAll() {
		if (orgTreeRoot == null) {
			return;
		}
		List<TreeNode> list = Lists.newArrayList();
		this.allNode(list, orgTreeRoot);
		boolean ckSelAll = false;
		for (TreeNode each : list) {
			if (each.isSelectable()) {
				if (!ckSelAll && each.isSelected()) {
					ckSelAll = true;
				} else if (!each.isSelected()) {
					ckSelAll = false;
					break;
				}
			}
		}
		this.selectAllNode = ckSelAll;
	}

	/**
	 * 將所有樹狀節點加入list中
	 *
	 * @param list
	 * @param root
	 */
	private void allNode(List<TreeNode> list, TreeNode root) {
		if (root == null || root.getChildren() == null) {
			return;
		}
		root.getChildren().forEach(each -> {
			list.add(each);
			this.allNode(list, each);
		});
	}

	/**
	 *
	 */
	private void resetSelectAll() {
		if (orgTreeRoot == null) {
			return;
		}
		boolean hasAnyUnSelect = false;
		if (orgTreeRoot.getChildren() != null && !orgTreeRoot.getChildren().isEmpty()) {
			for (TreeNode node : orgTreeRoot.getChildren()) {
				if (this.checkNodeUnSelect(node)) {
					hasAnyUnSelect = true;
					break;
				}
			}
		}
		this.selectAllNode = !hasAnyUnSelect;
	}

	private boolean checkNodeUnSelect(TreeNode source) {
		if (!source.isSelected()) {
			return true;
		}
		if (source.getChildren() != null && !source.getChildren().isEmpty()) {
			for (TreeNode node : source.getChildren()) {
				return this.checkNodeUnSelect(node);
			}
		}
		return false;
	}

	/**
	 * 初始根節點
	 *
	 * @param org
	 */
	private void initRoot(Org org) {
		this.initRoot(org, null);
	}

	/**
	 * 部門名稱稱查詢
	 */
	public void searchDep() {
		try {
			if (Strings.isNullOrEmpty(deptText)) {
				orgTreeRoot = null;
				// 輸入空白 = 全查
				initOrgTree(root, Lists.newArrayList(), true, false);
				return;
			}
			this.searchNodeByText(deptText, orgTreeRoot);
		} catch (Exception e) {
			log.error("searchDep Error", e);
			reportOrgTreeCallBack.showMessage(e.getMessage());
		}
	}

	/**
	 * 部門名稱查詢, focus 第一筆
	 */
	public void searchSingleDept() {
		searchDep();
		if (orgTreeSelect != null && orgTreeSelect.length != 0) {
			Org org = (Org) orgTreeSelect[0].getData();
			highlightOrgs.clear();
			highlightOrgs.add(org);
			for (TreeNode node : orgTreeSelect) {
				node.setSelected(false);
			}
			orgTreeSelect = new TreeNode[] { orgTreeSelect[0] };
			singleSelectedNode = orgTreeSelect[0];
		}
	}

	/**
	 * 組織樹初始化-全部展開by部門(通知人員設定使用
	 * 
	 * @param compId
	 * @param dept
	 */
	public void initOrgTreeByDept(Integer userSid) {

		User loginUser = WkUserCache.getInstance().findBySid(userSid);
		if (loginUser == null || loginUser.getPrimaryOrg() == null) {
			return;
		}

		root = this.wkOrgCache.findBySid(loginUser.getPrimaryOrg().getSid());

		// 取得使用者所在的單位
		Set<Integer> userDeps = Sets.newHashSet();
		userDeps.add(loginUser.getPrimaryOrg().getSid());

		// 取得使用者管理單位
		List<Org> managerOrgs = this.wkOrgCache.findManagerOrgs(userSid);
		if (WkStringUtils.notEmpty(managerOrgs)) {
			for (Org managerOrg : managerOrgs) {
				userDeps.add(managerOrg.getSid());
			}
		}

		// enable的組織單位
		Set<Org> enableOrgs = Sets.newHashSet();
		for (Integer depSid : userDeps) {
			Org dep = this.wkOrgCache.findBySid(depSid);

			if (!OrgType.DEPARTMENT.equals(dep.getType())) {
				continue;
			}

			if (dep != null) {
				enableOrgs.add(dep);
			}

			// 取得所有子部門
			List<Org> childDeps = this.wkOrgCache.findAllChild(dep.getSid());
			if (WkStringUtils.notEmpty(childDeps)) {
				enableOrgs.addAll(childDeps);
			}
		}

		// enable 單位的組織需要disable
		Set<Org> disableOrgs = Sets.newHashSet();
		for (Org enableDep : enableOrgs) {
			// 父單位不存在可使用單位時，加入清單
			List<Org> allParentDeps = this.wkOrgCache.findAllParent(enableDep.getSid());
			for (Org parentDep : allParentDeps) {
				if (!enableOrgs.contains(parentDep)) {
					disableOrgs.add(parentDep);
				}
			}
		}

		// 如果是部級以上 加入下層單位
		// if (dept.getLevel() != null && !OrgLevel.THE_PANEL.equals(dept.getLevel())) {
		// enableOrgs.addAll(orgService.findChildOrgs(dept));
		// }
		// 要disable的上層組織
		// List<Org> disableOrgs = Lists.newArrayList();
		// enableOrgs.forEach(o -> this.getDisableParentOrg(o, enableOrgs,
		// disableOrgs));

		// 取得公司別下完整的樹
		this.orgTreeRoot = this.getOtherRoot(this.wkOrgCache.findById(compId));

		this.filterOrg(
		        orgTreeRoot,
		        Lists.newArrayList(enableOrgs),
		        Lists.newArrayList(disableOrgs));

		expandedAllByRootNode(Lists.newArrayList(orgTreeRoot));
	}

	/**
	 * 依組織的層級來初始根節點
	 *
	 * @param compId
	 * @param dept
	 */
	private void initRootByDeptLevel(String compId, Org dept) {
		root = dept;
		Org parent;
		if (dept.getLevel() != null && OrgLevel.THE_PANEL.equals(dept.getLevel())) {
			if (dept.getParent() != null) {
				parent = orgService.findBySid(dept.getParent().getSid());
			} else {
				parent = dept;
			}
		} else {
			parent = dept;
		}
		this.initAllOrgs();
		List<Org> collectionOrgs = Lists.newArrayList();
		collectionOrgs.add(parent);
		// 取得向下所有部門
		this.getAllChildren(parent, allOrgs, collectionOrgs);

		// 特殊權限的部門
		List<Org> spDepts = spService.findSpecificDeptsByRoleSidIn(loginRoleSids);

		if (spDepts != null && !spDepts.isEmpty()) {
			// 移除相同的部門
			collectionOrgs.removeAll(spDepts);
			collectionOrgs.addAll(spDepts);
		}

		// 要disable的上層組織
		List<Org> disableOrgs = Lists.newArrayList();
		collectionOrgs.forEach(o -> this.getDisableParentOrg(o, collectionOrgs, disableOrgs));

		this.orgTreeRoot = this.getOtherRoot(orgService.findByOrgId(compId));
		this.filterOrg(orgTreeRoot, collectionOrgs, disableOrgs);
	}

	/**
	 * 初始所有組織
	 */
	private void initAllOrgs() {
		if (allOrgs == null) {
			allOrgs = orgService.findAll();
		}
	}

	/**
	 * 取得登入者部門下的所有子部門
	 *
	 * @param parent
	 * @param allOrgs
	 * @param collectionOrgs
	 */
	private void getAllChildren(Org parent, List<Org> allOrgs, List<Org> collectionOrgs) {
		allOrgs.stream()
		        .filter(o -> o.getParent() != null && o.getParent().getSid().equals(parent.getSid()))
		        .forEach(o -> {
			        collectionOrgs.add(o);
			        getAllChildren(o, allOrgs, collectionOrgs);
		        });
	}

	/**
	 * 取得要disable的上層組織
	 *
	 * @param parent
	 * @param collectionOrgs
	 * @param disableOrgs
	 */
	private void getDisableParentOrg(Org parent, List<Org> collectionOrgs, List<Org> disableOrgs) {
		if (parent == null) {
			return;
		}
		if (parent.getParent() != null && OrgType.DEPARTMENT.equals(parent.getParent().getType())
		        && !collectionOrgs.contains(orgService.findBySid(parent.getParent().getSid()))) {
			disableOrgs.add(orgService.findBySid(parent.getParent().getSid()));
		}
		if (parent.getParent() == null) {
			return;
		}
		this.getDisableParentOrg(orgService.findBySid(parent.getParent().getSid()), collectionOrgs, disableOrgs);
	}

	/**
	 * 取得另一個組織樹
	 *
	 * @param org
	 */
	private TreeNode getOtherRoot(Org org) {
		Activation status = selectJustActive ? Activation.ACTIVE : null;
		this.initAllOrgs();
		TreeNode otherRoot = new DefaultTreeNode("root", null);
		for (Org o : allOrgs) {
			Org parent = null;
			if (o.getParent() != null) {
				parent = orgService.findBySid(o.getParent().getSid());
			}
			if (status == null) {
				if (parent != null && parent.equals(org)) {
					TreeNode node = new DefaultTreeNode(o, otherRoot);
					this.buildTree(node, allOrgs, status);
				}
			} else if (status.equals(o.getStatus()) && parent != null && parent.equals(org)) {
				TreeNode node = new DefaultTreeNode(o, otherRoot);
				this.buildTree(node, allOrgs, status);
			}
		}
		return otherRoot;
	}

	/**
	 * 篩選組織-根據有權限的部門及要disable的上層組織來過濾完整的組織樹
	 *
	 * @param root
	 * @param collectionOrgs
	 * @param disableOrgs
	 */
	private void filterOrg(TreeNode root, List<Org> collectionOrgs, List<Org> disableOrgs) {
		if (root.getChildren() == null || root.getChildren().isEmpty()) {
			return;
		}
		List<TreeNode> nodes = Lists.newArrayList();
		for (TreeNode node : root.getChildren()) {
			Org o = (Org) node.getData();
			if (disableOrgs.contains(o)) {
				node.setSelectable(false);
			}
			if (collectionOrgs.contains(o) || disableOrgs.contains(o)) {
				nodes.add(node);
			}
		}

		List<TreeNode> children = root.getChildren();
		children.clear();
		children.addAll(nodes);
		for (TreeNode node : root.getChildren()) {
			this.filterOrg(node, collectionOrgs, disableOrgs);
		}
	}

	/**
	 * 建置樹
	 *
	 * @param parentNode
	 * @param allOrgs
	 * @param status
	 */
	private void buildTree(TreeNode parentNode, List<Org> allOrgs, Activation status) {
		Org parent = (Org) parentNode.getData();
		for (Org org : allOrgs) {
			if (status == null) {
				if (org.getParent() != null && org.getParent().getSid().equals(parent.getSid())) {
					TreeNode node = new DefaultTreeNode(org, parentNode);
					this.buildTree(node, allOrgs, status);
				}
			} else if (status.equals(org.getStatus()) && org.getParent() != null && org.getParent().getSid().equals(parent.getSid())) {
				TreeNode node = new DefaultTreeNode(org, parentNode);
				this.buildTree(node, allOrgs, status);
			}
		}
	}

	/**
	 * 依查詢字串來查詢節點
	 *
	 * @param orgName
	 * @param parent
	 */
	private void searchNodeByText(String orgName, TreeNode parent) {
		this.highlightOrgs = Lists.newArrayList();
		this.unselAllNode(parent);
		List<TreeNode> selNodes = Lists.newArrayList();
		this.selectedNodeByText(orgName, parent, selNodes, true);
		TreeNode[] nodes = new TreeNode[selNodes.size()];
		orgTreeSelect = selNodes.toArray(nodes);
	}

	/**
	 * 不選取所有節點
	 *
	 * @param parent
	 */
	private void unselAllNode(TreeNode parent) {
		for (TreeNode node : parent.getChildren()) {
			if (node.getParent() != null) {
				node.getParent().setExpanded(false);
			}
			node.setSelected(false);
			this.unselAllNode(node);
		}
	}

	/**
	 * 往上展開父節點
	 *
	 * @param orgName
	 * @param parent
	 * @param selNodes
	 */
	private void selectedNodeByText(String orgName, TreeNode parent, List<TreeNode> selNodes, boolean select) {
		for (TreeNode node : parent.getChildren()) {
			Org dep = (Org) node.getData();
			if (WkOrgUtils.getOrgName(dep).toLowerCase().contains(orgName.toLowerCase())) {
				if (!Strings.isNullOrEmpty(orgName)) {
					this.expandedAboveNode(node.getParent());
				}
				node.setSelected(select);
				if (select) {
					selNodes.add(node);
				}
				this.highlightOrgs.add(dep);
			}
			this.disabledDep(node, dep);
			if (!selNodes.contains(node) && node.isSelected()) {
				selNodes.add(node);
			}
			this.selectedNodeByText(orgName, node, selNodes, select);
		}
	}

	/**
	 *
	 * @param node
	 */
	private void expandedAboveNode(TreeNode node) {
		if (node == null) {
			return;
		}
		node.setExpanded(true);
		if (node.getParent() != null) {
			this.expandedAboveNode(node.getParent());
		}
	}

	/**
	 * 全部展開
	 * 
	 * @param children
	 */
	private void expandedAllByRootNode(List<TreeNode> children) {
		if (CollectionUtils.isEmpty(children)) {
			return;
		}
		for (TreeNode node : children) {
			node.setExpanded(true);
			if (CollectionUtils.isNotEmpty(node.getChildren())) {
				this.expandedAllByRootNode(node.getChildren());
			}
		}
	}

	/**
	 * 選擇單位
	 * 
	 * @param children
	 */
	public void selectOrg(List<TreeNode> children, Org org) {
		if (CollectionUtils.isEmpty(children)) {
			return;
		}
		for (TreeNode node : children) {
			Org treeOrg = (Org) node.getData();
			if (treeOrg.getSid().equals(org.getSid())) {
				this.singleSelectedNode = node;
				node.setSelected(true);
			}
			if (CollectionUtils.isNotEmpty(node.getChildren())) {
				this.selectOrg(node.getChildren(), org);
			}
		}
	}

	/**
	 * Disabled部門
	 */
	private void disabledDep(TreeNode node, Org o) {
		if (this.checkContainsDisabledDep(o)) {
			node.setSelectable(false);
			node.setSelected(true);
		}
	}

	/**
	 * 判斷是否有在預設Disabled部門中
	 *
	 * @param o
	 */
	private boolean checkContainsDisabledDep(Org o) {
		if (disableOrgList == null) {
			disableOrgList = Lists.newArrayList();
		}
		return disableOrgList.contains(o);
	}

	/**
	 * 是否顯示停用部門
	 */
	public void rebuildRootByDeptLevel() {
		try {
			if (buildByDeptLevel) {
				this.initRootByDeptLevel(compId, root);
			} else {
				this.rebuildRoot();
			}
		} catch (Exception e) {
			log.error("rebuildRootByDeptLevel Error", e);
			reportOrgTreeCallBack.showMessage(e.getMessage());
		}
	}

	/**
	 * 重建根節點
	 */
	private void rebuildRoot() {
		this.rebuildRoot(root, disableOrgList);
		this.markSelectedNode(Lists.newArrayList(), orgTreeRoot);
	}

	/**
	 * 重建根節點
	 *
	 * @param root
	 * @param disableOrgList
	 */
	private void rebuildRoot(Org root, List<Org> disableOrgList) {
		orgTreeRoot = null;
		this.initRoot(root, disableOrgList);
	}

	/**
	 * 初始根節點
	 *
	 * @param org
	 * @param disableOrgList
	 */
	private void initRoot(Org org, List<Org> disableOrgList) {
		this.disableOrgList = disableOrgList;
		this.root = org;
		Activation status = selectJustActive ? Activation.ACTIVE : null;
		this.initAllOrgs();
		orgTreeRoot = new DefaultTreeNode("root", null);
		for (Org o : allOrgs) {
			Org parent = null;
			if (o.getParent() != null) {
				parent = orgService.findBySid(o.getParent().getSid());
			}
			if (status == null) {
				if (parent != null && parent.equals(org)) {
					TreeNode node = new DefaultTreeNode(o, orgTreeRoot);
					this.buildTree(node, allOrgs, status);
				}
			} else if (status.equals(o.getStatus()) && parent != null && parent.equals(org)) {
				TreeNode node = new DefaultTreeNode(o, orgTreeRoot);
				this.buildTree(node, allOrgs, status);
			}
		}
	}

	/**
	 * 標註已選取的節點(依已選擇的組織sid)
	 *
	 * @param selOrgSids
	 * @param parent
	 */
	private void markSelectedNode(List<String> selOrgSids, TreeNode parent) {
		this.highlightOrgs = Lists.newArrayList();
		this.unselAllNode(parent);
		this.selectedNodeBySelOrgSids(selOrgSids, parent);
	}

	/**
	 *
	 * @param selOrgSids
	 * @param parent
	 */
	private void selectedNodeBySelOrgSids(List<String> selOrgSids, TreeNode parent) {
		for (TreeNode node : parent.getChildren()) {
			String orgSid = String.valueOf(((Org) node.getData()).getSid());
			if (selOrgSids.contains(orgSid)) {
				node.setSelected(true);
			}
			this.disabledDep(node, (Org) node.getData());
			this.markSelectedNode(selOrgSids, node);
		}
	}

	/**
	 * 全選 - AJAX
	 */
	public void actionSelectALL() {
		try {
			List<TreeNode> selNodes = Lists.newArrayList();
			this.disableTreeNode = Lists.newArrayList();
			if (selectAllNode) {
				this.selectAllNodeStatus(orgTreeRoot.getChildren(), selNodes, true);
				TreeNode[] nodes = new TreeNode[selNodes.size()];
				this.orgTreeSelect = selNodes.toArray(nodes);
				return;
			}
			// 全關
			this.selectAllNodeStatus(orgTreeRoot.getChildren(), selNodes, false);
			if (disableTreeNode.isEmpty()) {
				this.orgTreeSelect = null;
			} else {
				TreeNode[] nodes = new TreeNode[disableTreeNode.size()];
				this.orgTreeSelect = disableTreeNode.toArray(nodes);
			}
		} catch (Exception e) {
			log.error("actionSelectALL Error", e);
			reportOrgTreeCallBack.showMessage(e.getMessage());
		}
	}

	/**
	 * 擇選全部節點
	 *
	 * @param childsNode
	 */
	private void selectAllNodeStatus(List<TreeNode> childsNode, List<TreeNode> selNodes, boolean status) {
		if (childsNode == null) {
			return;
		}
		for (TreeNode node : childsNode) {
			node.setSelected(status);
			selNodes.add(node);
			this.disabledDep(node, (Org) node.getData());
			if (!disableTreeNode.contains(node) && node.isSelected()) {
				disableTreeNode.add(node);
			}
			this.selectAllNodeStatus(node.getChildren(), selNodes, status);
		}
	}

	/**
	 *
	 * @param event
	 */
	public void onNodeExpand(NodeExpandEvent event) {
		try {
			event.getTreeNode().setExpanded(true);
		} catch (Exception e) {
			log.error("actionSelectALL Error", e);
			reportOrgTreeCallBack.showMessage(e.getMessage());
		}
	}

	/**
	 *
	 * @param event
	 */
	public void onNodeCollapse(NodeCollapseEvent event) {
		try {
			event.getTreeNode().setExpanded(false);
		} catch (Exception e) {
			log.error("actionSelectALL Error", e);
			reportOrgTreeCallBack.showMessage(e.getMessage());
		}
	}

	/**
	 *
	 * @param event
	 */
	public void onNodeSelect(NodeSelectEvent event) {
		try {
			if (event.getTreeNode().getData() != null) {
				Org selectOrg = (Org) event.getTreeNode().getData();
				if (highlightOrgs != null && !highlightOrgs.contains(selectOrg)) {
					highlightOrgs.add(selectOrg);
				}
			}
		} catch (Exception e) {
			log.error("actionSelectALL Error", e);
			reportOrgTreeCallBack.showMessage(e.getMessage());
		}
	}

	/**
	 * 選擇單一部門
	 * 
	 * @param event
	 */
	public void onSingleNodeSelect(NodeSelectEvent event) {
		try {
			orgTreeSelect = null; // 只 focus singleSelectedNode
			if (event.getTreeNode().getData() != null) {
				singleSelectedNode = event.getTreeNode();
				Org selectOrg = (Org) event.getTreeNode().getData();
				if (highlightOrgs != null) {
					highlightOrgs.clear();
					highlightOrgs.add(selectOrg);
				}
			}
		} catch (Exception e) {
			log.warn(e.getMessage(), e);
			reportOrgTreeCallBack.showMessage(e.getMessage());
		}
	}

	/**
	 *
	 * @param event
	 */
	public void onNodeUnselect(NodeUnselectEvent event) {
		try {
			if (event.getTreeNode().getData() != null) {
				Org unselectOrg = (Org) event.getTreeNode().getData();
				if (highlightOrgs != null && highlightOrgs.contains(unselectOrg)) {
					highlightOrgs.remove(unselectOrg);
				}
			}
		} catch (Exception e) {
			log.error("actionSelectALL Error", e);
			reportOrgTreeCallBack.showMessage(e.getMessage());
		}
	}

	/**
	 * 選取組織樹
	 */
	public void btnSubmit() {
		reportOrgTreeCallBack.confirmSelOrg();
		againResetSelectAll();
	}

	/**
	 * 取得已選取組織樹sid
	 *
	 * @return
	 */
	public List<String> getSelOrgSids() {
		List<String> orgSis = Lists.newArrayList();
		if (orgTreeSelect != null && orgTreeSelect.length > 0) {
			for (TreeNode node : orgTreeSelect) {
				Org dept = (Org) node.getData();
				orgSis.add(String.valueOf(dept.getSid()));
			}
		} else if (singleSelectedNode != null) {
			Org dept = (Org) singleSelectedNode.getData();
			orgSis.add(String.valueOf(dept.getSid()));
		}

		return orgSis;
	}

	/**
	 * 取得已選取組織樹sid
	 *
	 * @return
	 */
	public List<String> getAllTreeOrgs() {
		List<String> orgSids = Lists.newArrayList();
		List<TreeNode> list = Lists.newArrayList();
		allNode(list, orgTreeRoot);
		for (TreeNode node : list) {
			Org org = (Org) node.getData();
			orgSids.add(String.valueOf(org.getSid()));
		}

		return orgSids;
	}

	/**
	 * 依照部門名稱進行搜尋後，將查詢到資料highlight
	 *
	 * @param o
	 * @return
	 */
	public String treeSyle(Org o) {
		if (o != null && highlightOrgs != null && !highlightOrgs.isEmpty()
		        && highlightOrgs.contains(o)) {
			return "ui-state-highlight";
		}
		return "";
	}

}
