/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.searchheader;

import com.cy.work.common.enums.ReadRecordType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.web.controller.enums.ReqStatusMBean;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 未結案單據 的搜尋表頭
 *
 * @author jason_h
 */
@Controller
@Scope("view")
public class Search10HeaderMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6923745527920571685L;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private ReqStatusMBean reqStatusUtils;

    /** 選擇的需求製作進度(未結案單據查詢條件) */
    @Getter
    @Setter
    private RequireStatusType selectRequireStatusType;

    /** 選擇的閱讀類型 */
    @Getter
    @Setter
    private ReadRecordType selectReadRecordType;

    @PostConstruct
    private void init() {
        this.clear();
    }

    /**
     * 清除/還原選項
     */
    public void clear() {
        this.initCommHeader();
        this.initHeader();
    }

    /**
     * 初始化commHeader
     */
    private void initCommHeader() {
        commHeaderMBean.clear();
        commHeaderMBean.clearAdvance();
        commHeaderMBean.initDefaultRequireDepts(true, false);
        commHeaderMBean.setStartDate(null);
        commHeaderMBean.setEndDate(new Date());
    }

    /**
     * 初始化Header
     */
    private void initHeader() {
        selectRequireStatusType = null;
        selectReadRecordType = null;
    }

    public SelectItem[] getReadRecordTypeItems() {
        ReadRecordType[] types = ReadRecordType.values();
        SelectItem[] items = new SelectItem[types.length];
        for (int i = 0; i < types.length; i++) {
            items[i] = new SelectItem(types[i], types[i].getValue());
        }
        return items;
    }

    /**
     * 建立需求狀態查詢 List
     *
     * @return
     */
    public List<String> createQueryReqStatus() {
        return reqStatusUtils.createQueryReqStatus(this.selectRequireStatusType, this.getAllReqStatus());
    }

    /**
     * 取得此報表全部製作進度查詢
     *
     * @return
     */
    private List<RequireStatusType> getAllReqStatus() {
        return reqStatusUtils.createExcludeStatus(
                Lists.newArrayList(
                        RequireStatusType.INVALID,
                        RequireStatusType.CLOSE,
                        RequireStatusType.AUTO_CLOSED,
                        RequireStatusType.DRAFT)
        );
    }

    /**
     * 畫面用全部製作進度查詢
     *
     * @return
     */
    public SelectItem[] getReqStatusItems() {
        return reqStatusUtils.buildItem(this.getAllReqStatus());
    }

    /**
     * 建立需求狀態查詢 List
     *
     * @return
     */
    public List<String> createQueryAllReqStatus() {
        return this.getAllReqStatus().stream()
                .map(each -> each.name())
                .collect(Collectors.toList());
    }
}
