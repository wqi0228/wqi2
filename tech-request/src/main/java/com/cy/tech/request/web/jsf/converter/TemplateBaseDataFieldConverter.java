/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.jsf.converter;

import com.cy.tech.request.vo.template.TemplateBaseDataField;
import com.google.common.base.Strings;
import java.util.List;
import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.springframework.stereotype.Component;

/**
 * 基礎欄位模版轉換器
 *
 * @author shaun
 */
@Component("templateBaseDataFieldConverter")
public class TemplateBaseDataFieldConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (Strings.isNullOrEmpty(value)) {
            return null;
        }
        if (component instanceof SelectOneMenu) {
            SelectOneMenu som = (SelectOneMenu) component;
            for (UIComponent each : som.getChildren()) {
                if (each instanceof UISelectItems) {
                    UISelectItems uItems = (UISelectItems) each;
                    @SuppressWarnings("rawtypes")
                    List values = (List) uItems.getValue();
                    for (Object each2 : values) {
                        if (each2 instanceof TemplateBaseDataField) {
                            TemplateBaseDataField obj = (TemplateBaseDataField) each2;
                            if (obj.getSid().equals(value)) {
                                return obj;
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (component instanceof SelectOneMenu && value instanceof TemplateBaseDataField) {
            return ((TemplateBaseDataField) value).getSid();
        }
        return null;
    }
}
