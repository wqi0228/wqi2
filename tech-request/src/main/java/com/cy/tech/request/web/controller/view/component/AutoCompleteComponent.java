package com.cy.tech.request.web.controller.view.component;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

import com.cy.commons.vo.User;

public abstract class AutoCompleteComponent {
    
    
    public abstract List<User> fuzzySearch(String text);
    /**
     * add relation tags of itemSelect
     * @param event
     */
    public abstract void itemSelect(SelectEvent event);
    /**
     * remove tags of itemUnselect
     * @param event
     */
    public abstract void itemUnselect(UnselectEvent event);
    
    public <T> void filterSelected(List<T> originList, List<T> ownsList){
        if(CollectionUtils.isEmpty(ownsList)){
            return;
        }
        Iterator<T> ownsIterator = ownsList.iterator();
        outerLoop:
        while(ownsIterator.hasNext()){
            T ownsElem= ownsIterator.next();
            Iterator<T> originIterator = originList.iterator();
            while(originIterator.hasNext()){
                T elem= originIterator.next();
                if(ownsElem.equals(elem)){
                    originIterator.remove();
                    continue outerLoop;
                }
            }
        }
    }
}
