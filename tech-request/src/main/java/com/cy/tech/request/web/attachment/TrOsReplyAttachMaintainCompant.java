/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.attachment;

import com.cy.tech.request.logic.vo.AttachmentVO;
import com.cy.tech.request.vo.require.tros.TrOsHistory;
import com.cy.tech.request.web.attachment.util.AttachMaintainCompant;
import com.cy.tech.request.web.controller.logic.component.OthSetSettingAttachmentLogicComponents;
import com.cy.tech.request.web.listener.DeleteAttCallBack;
import com.cy.tech.request.web.listener.ReplyCallBackCondition;
import com.cy.tech.request.web.listener.TabLoadCallBack;
import com.google.common.collect.Lists;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import lombok.extern.slf4j.Slf4j;

/**
 * 其他設定資訊( 回覆,回覆的回覆,完成,取消)附件維護元件
 *
 * @author brain0925_liao
 */
@Slf4j
public class TrOsReplyAttachMaintainCompant extends AttachMaintainCompant {

    /**
     * 
     */
    private static final long serialVersionUID = -4285460198573066031L;
    /** HistorySId */
    private String history_Sid;
    /** 其他設定資訊Sid */
    private String os_Sid;
    /** TabLoadCallBack */
    private TabLoadCallBack tabLoadCallBack;
    /** DeleteAttCallBack */
    private DeleteAttCallBack deleteAttCallBack;

    public TrOsReplyAttachMaintainCompant(Integer userSid, DeleteAttCallBack deleteAttCallBack, TabLoadCallBack tabLoadCallBack) {
        super(userSid, "tech-request",null);
        this.deleteAttCallBack = deleteAttCallBack;
        this.tabLoadCallBack = tabLoadCallBack;
    }

    public void loadData(List<AttachmentVO> attachmentVOs, String os_Sid, String history_Sid) {
        super.attachmentVOs = attachmentVOs;
        this.os_Sid = os_Sid;
        this.history_Sid = history_Sid;
        reloadData();
    }

    public void reloadData() {
        try {
            List<AttachmentVO> tempAttachmentVOs = OthSetSettingAttachmentLogicComponents.getInstance().getAttachmentVOByOSSidAndHistorySId(os_Sid, history_Sid);
            tempAttachmentVOs.forEach(item -> {
                if (!attachmentVOs.contains(item)) {
                    item.setChecked(false);
                    if (String.valueOf(userSid).equals(item.getCreateUserSId())) {
                        item.setShowEditBtn(true);
                    } else {
                        item.setShowEditBtn(false);
                    }
                    item.setEditMode(false);
                    attachmentVOs.add(item);
                }
            });
            List<AttachmentVO> tempReal = Lists.newArrayList();
            attachmentVOs.forEach(item -> {
                if (tempAttachmentVOs.contains(item)) {
                    tempReal.add(item);
                }
            });
            Collections.sort(tempReal, attachmentVOComparator);
            attachmentVOs.clear();
            attachmentVOs.addAll(tempReal);
            //DisplayController.getInstance().update("attachmentMaintain_othSet_" + os_Sid);

        } catch (Exception e) {
            log.error("loadData", e);
        }

    }

    public void toUpload() {
    }

    @Override
    protected void doDeleteAtt(AttachmentVO selAttachmentVO) {
        TrOsHistory trOsHistory = OthSetSettingAttachmentLogicComponents.getInstance().deleteHistoryAtt(selAttachmentVO.getAttSid(), userSid);
        attachmentVOs.remove(selAttachmentVO);
        if (attachmentVOs == null || attachmentVOs.isEmpty()) {
            ReplyCallBackCondition rc = new ReplyCallBackCondition();
            rc.setOs_sid(trOsHistory.getOs_sid());
            rc.setOs_history_sid(trOsHistory.getSid());
            rc.setOs_reply_and_reply_sid(trOsHistory.getOs_reply_and_reply_sid());
            deleteAttCallBack.reloadAttBtn(rc);
        }
        tabLoadCallBack.reloadTraceTab();
    }

    @Override
    protected void doSaveAttDesc(AttachmentVO selAttachmentVO) {
        OthSetSettingAttachmentLogicComponents.getInstance().updateAttDesc(selAttachmentVO.getAttSid(), selAttachmentVO.getAttDesc(), userSid);
    }

    transient private Comparator<AttachmentVO> attachmentVOComparator = new Comparator<AttachmentVO>() {
        @Override
        public int compare(AttachmentVO obj1, AttachmentVO obj2) {
            final String seq1 = obj1.getCreateTime();
            final String seq2 = obj2.getCreateTime();
            return seq2.compareTo(seq1);
        }
    };

}
