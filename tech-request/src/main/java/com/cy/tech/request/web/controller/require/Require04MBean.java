package com.cy.tech.request.web.controller.require;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.URLService.URLServiceAttr;
import com.cy.tech.request.web.controller.require.help.Req04ByIssueHelper;
import com.cy.tech.request.web.controller.require.help.Req04ByReqHelper;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.group.logic.RelevanceManager;
import com.cy.work.group.vo.WorkLinkGroupDetail;
import com.cy.work.group.vo.to.GroupDetailListTo;
import java.io.IOException;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 關聯檢視
 *
 * @author jason_h
 */
@Slf4j
@Controller
@Scope("view")
public class Require04MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1070230618812122618L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private URLService urlService;
    @Autowired
    transient private RelevanceManager relevanceManager;
    @Autowired
    transient private Req04ByIssueHelper issueMBean;
    @Autowired
    transient private Req04ByReqHelper reqMBean;
    @Autowired
    transient private DisplayController display;

    private String groupSid;

    private WorkLinkGroupDetail selWorkLinkGroupDetail;

    private String urlProtocol;

    /** 列表資料 */
    @Getter
    private GroupDetailListTo listTo = new GroupDetailListTo();

    public void updateUrlProtocol() {
        try {
            String value = Faces.getRequestParameterMap().get("urlProtocol");
            log.info("updateUrlProtocol [" + value + "]");
            this.urlProtocol = value;
        } catch (Exception e) {
            log.error("updateUrlProtocol ERROR", e);
        }
    }

    @PostConstruct
    private void init() {
        String param_l = Faces.getRequestParameterMap().get(URLServiceAttr.URL_ATTR_L.getAttr());
        if (StringUtils.isBlank(param_l)) {
            return;
        }
        
        String linkGroupInfoSid = urlService.parseIllegalUrlParam(
        		param_l,
        		WkUserCache.getInstance().findBySid(SecurityFacade.getUserSid()),
        		SecurityFacade.getCompanyId());	
        
        if (linkGroupInfoSid.contains(URLService.ILLEAL_ACCESS)) {
            this.reloadToIllegalPage(linkGroupInfoSid.replace(URLService.ILLEAL_ACCESS, ""));
            return;
        }
        boolean noLink = this.buildList(linkGroupInfoSid);
        if (noLink) {
            display.execute("window.close();");
            return;
        }
        listTo.init("screen_view_content_dataTable_req", "screen_view_content_widgetVar_req",
                "screen_view_content_dataTable_issue", "screen_view_content_widgetVar_issue");
        this.onRowSelectByReq();
    }

    private void settingMainLinkSid(String groupSid) {
        try {
            String mainLinkSid = relevanceManager.getWorkLinkGroupBySid(groupSid).getMainLinkSid();
            listTo.setMainLinkSid(mainLinkSid);
        } catch (Exception e) {
            log.error("settingMainLinkSid ERROR", e);
        }
    }

    private void reloadToIllegalPage(String errorCode) {
        try {
            Faces.getExternalContext().redirect("../error/illegal_read.xhtml?" + URLService.ERROR_CODE_ATTR + "=" + errorCode);
            Faces.getContext().responseComplete();
        } catch (IOException ex) {
            log.error("導向讀取失敗頁面失敗...", ex);
        }
    }

    /**
     *
     * @param obj
     */
    private boolean buildList(String groupSid) {
        this.groupSid = groupSid;
        settingMainLinkSid(groupSid);
        listTo.setGroupDetailsByReq(relevanceManager.findByGroupSidAndType(
                groupSid, WorkSourceType.TECH_REQUEST));
        listTo.setGroupDetailsByIssue(relevanceManager.findByGroupSidAndType(
                groupSid, WorkSourceType.TECH_ISSUE));
        if (listTo.getGroupDetailsByReq() == null || listTo.getGroupDetailsByReq().isEmpty()) {

            return true;
        }
        listTo.setSelectedByReq(listTo.getGroupDetailsByReq().get(0));
        return false;
    }

    public void clickMainLinkBtn(WorkLinkGroupDetail to) {
        try {
        	this.selWorkLinkGroupDetail = to;
            if (to.getSourceSid().equals(listTo.getMainLinkSid())) {
                this.display.showPfWidgetVar("remove_main_confirm_wv");
            } else {
                this.display.showPfWidgetVar("add_main_confirm_wv");
            }
        } catch (Exception e) {
            log.error("clickMainLinkBtn ERROR", e);
        }
    }

    public void doAddMainLink() {
        try {
            relevanceManager.addMainLink(groupSid, selWorkLinkGroupDetail.getSourceSid(), selWorkLinkGroupDetail.getSourceNo(), loginBean.getUser().getSid());
            this.settingMainLinkSid(groupSid);
            this.display.update("screen_view_content_dataTable_req");
            this.display.update("screen_view_content_dataTable_issue");
            this.display.hidePfWidgetVar("add_main_confirm_wv");
        } catch (Exception e) {
            log.error("doAddMainLink ERROR", e);
        }
    }

    public void doRemoveMainLink() {
        try {
            relevanceManager.removeMainLink(groupSid, selWorkLinkGroupDetail.getSourceSid(), loginBean.getUser().getSid());
            this.settingMainLinkSid(groupSid);
            this.display.update("screen_view_content_dataTable_req");
            this.display.update("screen_view_content_dataTable_issue");
            this.display.hidePfWidgetVar("remove_main_confirm_wv");
        } catch (Exception e) {
            log.error("doRemoveMainLink ERROR", e);
        }
    }

    /**
     * dataTable選擇事件
     */
    public void onRowSelectByIssue() {
        issueMBean.onRowSelect(listTo, urlProtocol);
    }

    /**
     * 切換版面到明細
     *
     * @param to
     * @param dtId
     */
    public void onRowSelectThemeByIssue(WorkLinkGroupDetail to) {
        listTo.setSelectedByIssue(to);
        this.onRowSelectByIssue();
    }

    /**
     * 開啟分頁
     *
     * @param to
     */
    public void btnOpenUrlByIssue(WorkLinkGroupDetail to) {
        issueMBean.btnOpenUrl(to, listTo);
    }

    /**
     * 重建資料
     *
     * @param to
     */
    public void buildColByIssue(WorkLinkGroupDetail to) {
        issueMBean.buildCol(loginBean.getUser(), to);
    }

    /**
     * dataTable選擇事件
     *
     * @param dtId
     */
    public void onRowSelectByReq() {
        String url = "../require/require04_half.xhtml" + urlService.createUrlLinkParam(URLService.URLServiceAttr.URL_ATTR_M,
                urlService.createSimpleUrlTo(loginBean.getUser().getSid(), listTo.getSelectedByReq().getSourceNo(), 1))
                + "&" + URLService.URLServiceAttr.URL_ATTR_SHOW_TITLE_BTN.getAttr() + "=false";
        reqMBean.onRowSelect(listTo, url);
    }

    /**
     * 切換版面到明細
     *
     * @param to
     * @param dtId
     */
    public void onRowSelectThemeByReq(WorkLinkGroupDetail to) {
        listTo.setSelectedByReq(to);
        this.onRowSelectByReq();
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param to
     */
    public void btnOpenUrlByReq(WorkLinkGroupDetail to) {
        reqMBean.btnOpenUrl(to, listTo);
    }

    /**
     * 取得案件單另開分頁網址
     *
     * @param sourceNo
     * @return
     */
    public String getIssueURL(String sourceNo) {
        return issueMBean.getIssueURL(sourceNo);
    }

    /**
     * 取得REQ URL
     *
     * @param requireNo
     * @return
     */
    public String getReqUrl(String sourceNo) {
        return urlService.createLoacalURLLink(URLService.URLServiceAttr.URL_ATTR_M,
                urlService.createSimpleUrlTo(loginBean.getUser().getSid(), sourceNo, 1));
    }
}
