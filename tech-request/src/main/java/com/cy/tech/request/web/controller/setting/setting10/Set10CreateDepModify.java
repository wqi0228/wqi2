/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.setting.setting10;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.pmis.PmisHelper;
import com.cy.tech.request.logic.utils.SimpleDateFormatEnum;
import com.cy.tech.request.logic.utils.ToolsDate;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.RequireTransProgramType;
import com.cy.tech.request.web.condition.Setting10CreateDepModifyCondition;
import com.cy.tech.request.web.controller.setting.Setting10MBean;
import com.cy.tech.request.web.logic.helper.TransCreateDepLogicHelper;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.tech.request.web.setting.RequireTransSetting;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkOrgUtils;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 異動建單單位
 *
 * @author brain0925_liao
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope("view")
public class Set10CreateDepModify implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -8336242463618837974L;
    @Autowired
    private PmisHelper pmisHelper;
    /** 程序挑選 */
    @Getter
    @Setter
    private String selTransProgramType;

    /** 需求製作進度 */
    @Getter
    @Setter
    private List<String> selReqTypes;

    /** 子程序狀態 */
    @Getter
    @Setter
    private List<String> selSubProgramTypes;

    /** 來源對應部門 */
    @Getter
    @Setter
    private Org sourceMappOrg;
    /** 目標新增部門 */
    @Getter
    @Setter
    private Org targetMappOrg;

    /** 轉換單號 */
    @Getter
    @Setter
    private String transNO;

    /** 執行結果 */
    @Getter
    @Setter
    private String output;

    /** ErrorMessage */
    @Getter
    private String errorMessage;

    private StringBuilder result = new StringBuilder();

    @Autowired
    transient private Setting10MBean set10Bean;
    @Autowired
    private TransCreateDepLogicHelper transCreateDepLogicHelper;
    /** 下載用記錄檔 */
    @Getter
    private StreamedContent file;

    /** 計算並核對正確的轉換數量 */
    public List<Setting10CreateDepModifyCondition> doPreViewTransCreateDep() {
        List<Setting10CreateDepModifyCondition> results = Lists.newArrayList();
        try {
            results = doViewTransCreateDep();
        } catch (Exception e) {
            showMessage(e.getMessage());
            log.error("doPreViewTransCreateDep ERROR", e);
        }
        return results;
    }

    /**
     * * 執行計算並核對正確的轉換數量
     *
     * @return 結果物件List
     */
    private List<Setting10CreateDepModifyCondition> doViewTransCreateDep() {
        List<Setting10CreateDepModifyCondition> results = Lists.newArrayList();
        checkTransCreateDep();
        clearOutput();
        RequireTransProgramType transProgramType = RequireTransProgramType.valueOf(selTransProgramType);
        results.addAll(doViewTransCreateDep(transProgramType));
        List<Setting10CreateDepModifyCondition> faults = results.stream()
                .filter(each -> !each.isResult())
                .collect(Collectors.toList());
        addOutput("開始預覽執行[" + RequireTransSetting.getTransProgramTypeName(transProgramType) + "]建立單位轉換...<BR/>");
        addOutput("原單單位 : " + WkOrgUtils.getOrgName(sourceMappOrg) + "...<BR/>");
        addOutput("轉換單位 : " + WkOrgUtils.getOrgName(targetMappOrg) + "...<BR/>");
        addOutput("處理筆數 : " + results.size() + "...<BR/>");
        addOutput("失敗筆數 : " + faults.size() + "...<BR/>");
        addOutput("記錄日期 : " + ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), new Date()) + "...<BR/>");
        addOutput("=========================================================================================...<BR/>");

        results.forEach(item -> {
            addOutput(item.getText() + "...<BR/>");
            addOutput("------------------------------------------------------------------------------------...<BR/>");
        });
        return results;
    }

    /** 執行轉換建立部門 */
    public void doTransCreateDep() {
        try {

            // 先行確認執行結果,若有不符合條件,將不進行轉單
            List<Setting10CreateDepModifyCondition> results = doViewTransCreateDep();
            List<Setting10CreateDepModifyCondition> faults = results.stream()
                    .filter(each -> !each.isResult())
                    .collect(Collectors.toList());
            if (faults != null && !faults.isEmpty()) {
                addOutput("是否可進行轉單 : 不可執行...<BR/>");
                return;
            } else {
                addOutput("是否可進行轉單 : 可執行...<BR/>");
            }
            RequireTransProgramType transProgramType = RequireTransProgramType.valueOf(selTransProgramType);
            List<Setting10CreateDepModifyCondition> doResults = Lists.newArrayList();
            doResults.addAll(doTransCreateDep(transProgramType));
            List<Setting10CreateDepModifyCondition> doFaults = doResults.stream()
                    .filter(each -> !each.isResult())
                    .collect(Collectors.toList());
            addOutput("開始執行[" + RequireTransSetting.getTransProgramTypeName(transProgramType) + "]建立單位轉換...<BR/>");
            addOutput("原單單位 : " + WkOrgUtils.getOrgName(sourceMappOrg) + "...<BR/>");
            addOutput("轉換單位 : " + WkOrgUtils.getOrgName(targetMappOrg) + "...<BR/>");
            addOutput("處理筆數 : " + doResults.size() + "...<BR/>");
            addOutput("失敗筆數 : " + doFaults.size() + "...<BR/>");
            addOutput("記錄日期 : " + ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), new Date()) + "...<BR/>");
            addOutput("=========================================================================================...<BR/>");

            doResults.forEach(item -> {
                addOutput(item.getText() + "...<BR/>");
                addOutput("------------------------------------------------------------------------------------...<BR/>");
            });
        } catch (Exception e) {
            showMessage(e.getMessage());
            log.error("doPreViewTransCreateDep ERROR", e);
        }
    }

    /**
     * 根據程序挑選進行檢視各自動作
     *
     * @param transProgramType 轉單程式 - 程序挑選類型列舉
     * @return
     */
    private List<Setting10CreateDepModifyCondition> doViewTransCreateDep(RequireTransProgramType transProgramType) {
        boolean justView = true;
        if (transProgramType.equals(RequireTransProgramType.REQUIRE)) {
            return transCreateDepLogicHelper.viewRequieTransCreateDep(selReqTypes, sourceMappOrg, targetMappOrg, transNO, justView);
        } else if (transProgramType.equals(RequireTransProgramType.OTHSET)) {
            return transCreateDepLogicHelper.viewOthSetTransCreateDep(selReqTypes, sourceMappOrg, targetMappOrg, transNO, selSubProgramTypes, justView);
        } else if (transProgramType.equals(RequireTransProgramType.PTCHECK)) {
            return transCreateDepLogicHelper.viewPtCheckTransCreateDep(selReqTypes, sourceMappOrg, targetMappOrg, transNO, selSubProgramTypes, justView);
        } else if (transProgramType.equals(RequireTransProgramType.WORKTESTSIGNINFO)) {
            return transCreateDepLogicHelper.viewWorkTestSignInfoTransCreateDep(selReqTypes, sourceMappOrg, targetMappOrg, transNO, selSubProgramTypes,
                    justView);
        } else if (transProgramType.equals(RequireTransProgramType.WORKONPG)) {
            return transCreateDepLogicHelper.viewWorkOnpgTransCreateDep(selReqTypes, sourceMappOrg, targetMappOrg, transNO, selSubProgramTypes, justView);
        }
        return Lists.newArrayList();
    }

    /**
     * 根據程序挑選進行執行各自動作
     *
     * @param transProgramType 轉單程式 - 程序挑選類型列舉
     * @return
     * @throws UserMessageException
     */
    private List<Setting10CreateDepModifyCondition> doTransCreateDep(RequireTransProgramType transProgramType) throws UserMessageException {
        boolean justView = false;
        if (transProgramType.equals(RequireTransProgramType.REQUIRE)) {
            List<Setting10CreateDepModifyCondition> list = transCreateDepLogicHelper
                    .viewRequieTransCreateDep(selReqTypes, sourceMappOrg, targetMappOrg, transNO, justView);
            List<RequireStatusType> status = Lists.newArrayList(RequireStatusType.PROCESS, RequireStatusType.COMPLETED,
                    RequireStatusType.SUSPENDED, RequireStatusType.CLOSE, RequireStatusType.AUTO_CLOSED);
            for (Setting10CreateDepModifyCondition condition : list) {
                if (condition.isResult()) {
                    if (status.contains(condition.getTrRequire().getRequireStatus())) {
                        pmisHelper.transferToPMISWhenOrgChange(condition.getTrRequire().getSid(), SecurityFacade.getUserSid());
                    } else {
                        log.info("此需求單不轉PMIS, 單號:{} 進度:{}", condition.getTrRequire().getRequireNo(),
                                condition.getTrRequire().getRequireStatus());
                    }
                }
            }
            return list;
        } else if (transProgramType.equals(RequireTransProgramType.OTHSET)) {
            return transCreateDepLogicHelper.viewOthSetTransCreateDep(selReqTypes, sourceMappOrg, targetMappOrg, transNO, selSubProgramTypes, justView);
        } else if (transProgramType.equals(RequireTransProgramType.PTCHECK)) {
            return transCreateDepLogicHelper.viewPtCheckTransCreateDep(selReqTypes, sourceMappOrg, targetMappOrg, transNO, selSubProgramTypes, justView);
        } else if (transProgramType.equals(RequireTransProgramType.WORKTESTSIGNINFO)) {
            return transCreateDepLogicHelper.viewWorkTestSignInfoTransCreateDep(selReqTypes, sourceMappOrg, targetMappOrg, transNO, selSubProgramTypes,
                    justView);
        } else if (transProgramType.equals(RequireTransProgramType.WORKONPG)) {
            return transCreateDepLogicHelper.viewWorkOnpgTransCreateDep(selReqTypes, sourceMappOrg, targetMappOrg, transNO, selSubProgramTypes, justView);
        }
        return Lists.newArrayList();
    }

    /** 檢測必填資訊 */
    private void checkTransCreateDep() {
        Preconditions.checkState(!Strings.isNullOrEmpty(selTransProgramType), "請挑選[程序挑選]");
        Preconditions.checkState(selReqTypes != null && !selReqTypes.isEmpty(), "請挑選[需求製作進度]");
        RequireTransProgramType requireTransProgramType = RequireTransProgramType.valueOf(selTransProgramType);
        if (!requireTransProgramType.equals(RequireTransProgramType.REQUIRE)) {
            Preconditions.checkState(selSubProgramTypes != null && !selSubProgramTypes.isEmpty(), "請挑選[子程序狀態]");
        }
        Preconditions.checkState(!Strings.isNullOrEmpty(transNO), "請輸入[轉換單號]");
        Preconditions.checkState(sourceMappOrg != null, "請挑選[來源對應部門]");
        Preconditions.checkState(targetMappOrg != null, "請挑選[目標新增部門]");
    }

    /** 將Log寫入紀錄內 */
    private void addOutput(String message) {
        this.output = output + message;
        this.result.append(message);
        log.info(message);
        set10Bean.addInfoMsg(message);
    }

    /** 清除Log記錄 */
    private void clearOutput() {
        this.output = "";
        this.result = new StringBuilder();
        this.set10Bean.clearInfoScreenMsg();
    }

    public void showMessage(String detail) {
        MessagesUtils.showInfo(detail);
    }

    public void downloadHandlerInfo() {
        try {
            String downLoadMsg = result.toString().replaceAll("<BR/>", "\r\n");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            String fileName = "BackstageTransCreateDepLog" + sdf.format(new Date()) + ".txt";
            InputStream is = new ByteArrayInputStream(downLoadMsg.getBytes(Charset.forName("UTF-8")));
            is.close();
            file = new DefaultStreamedContent(is, "text/plain", fileName);
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }
    }
}
