package com.cy.tech.request.web.controller.searchheader;

import com.cy.tech.request.logic.service.customer.ReqWorkCustomerHelper;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.web.controller.enums.ReqStatusMBean;
import com.cy.work.customer.vo.WorkCustomer;
import com.cy.work.customer.vo.enums.EnableType;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kasim
 */
@Controller
@Scope("view")
public class Search24HeaderMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -619642623083271746L;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private ReqWorkCustomerHelper customerService;
    @Autowired
    transient private ReqStatusMBean reqStatusUtils;

    /** 選擇的需求製作進度 */
    @Getter
    @Setter
    private List<RequireStatusType> selectRequireStatusTypes;
    /** 選擇的閱讀類型 */
    @Getter
    @Setter
    private ReadRecordType selectReadRecordType;
    /** 選擇的案件單進度 */
    @Getter
    @Setter
    private String issueStatus;
    /** 廳主條件 */
    @Getter
    @Setter
    private List<WorkCustomer> customers;
    /* 廳主選項 **/
    @Getter
    private List<WorkCustomer> customerItems;
    /** 是否廳主分頁 */
    @Getter
    @Setter
    private Boolean isCustomerPage = Boolean.FALSE;
    /** 分頁條件 */
    @Getter
    @Setter
    private WorkCustomer customerPage;
    /* 廳主分頁選項 **/
    @Getter
    @Setter
    private List<WorkCustomer> customerPageItems;

    @PostConstruct
    private void init() {
        this.clear();
    }

    /**
     * 清除/還原選項
     */
    public void clear() {
        this.initCommHeader();
        this.initHeader();
    }

    /**
     * 初始化commHeader
     */
    private void initCommHeader() {
        commHeaderMBean.clear();
        commHeaderMBean.setEndDate(new Date());
    }

    /**
     * 初始化Header
     */
    private void initHeader() {
        initCustomerItems();
        customers = Lists.newArrayList();
        isCustomerPage = Boolean.FALSE;
        customerPage = null;
    }

    /**
     * 取得 廳主選項
     */
    private void initCustomerItems() {
        customerItems = customerService.findByEnable(EnableType.ENABLE);
    }

    /**
     * 畫面用全部製作進度查詢
     *
     * @return
     */
    public SelectItem[] getReqStatusItems() {
        return reqStatusUtils.buildItem(this.getAllReqStatus());
    }

    /**
     * 取得此報表全部製作進度查詢
     *
     * @return
     */
    private List<RequireStatusType> getAllReqStatus() {
        return reqStatusUtils.createExcludeStatus(
                Lists.newArrayList(
                        RequireStatusType.DRAFT,
                        RequireStatusType.NEW_INSTANCE,
                        RequireStatusType.WAIT_CHECK,
                        RequireStatusType.ROLL_BACK_NOTIFY,
                        RequireStatusType.INVALID));
    }

    /**
     * 案件單進度 選項
     *
     * @return
     */
    public SelectItem[] getIssueStatusItems() {
        SelectItem[] items = new SelectItem[5];
        items[0] = new SelectItem("", "全部");
        items[1] = new SelectItem("PAPER000", "新建檔");
        items[2] = new SelectItem("PAPER002", "進行中");
        items[3] = new SelectItem("PAPER999", "結案");
        items[4] = new SelectItem("PAPER013", "自動結案");
        return items;
    }

    /**
     * 取得customers中sid
     *
     * @return
     */
    public List<Long> getCustomerSids() {
        return customers.stream().map(WorkCustomer::getSid).collect(Collectors.toList());
    }

    /**
     * 建立需求狀態查詢 List
     *
     * @return
     */
    public List<String> createQueryAllReqStatus() {
        return this.getAllReqStatus().stream()
                .map(each -> each.name())
                .collect(Collectors.toList());
    }
}
