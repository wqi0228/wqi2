package com.cy.tech.request.web.controller.report;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.commons.io.IOUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author aken_kao
 */
@Slf4j
public abstract class AbstractReporter implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -7931358326604576175L;
    @Getter
    @Setter
    protected boolean disabledExportBtn = true;
    
    /**
     * 匯出報表
     */
    public void exportReport() {
        ExcelWriter writer = buildExcelWriter();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext ec = facesContext.getExternalContext();
        ec.setResponseContentType("application/vnd.ms-excel");
        ec.responseReset();
        ec.setResponseHeader("Content-Disposition", "attachment;filename=report.xls");
        
        OutputStream output = null;
        try {
            output = ec.getResponseOutputStream();
            writer.output(output);
            output.flush();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            IOUtils.closeQuietly(output);
            facesContext.responseComplete();
        }
    }
    
    
    /**
     * 建立excel 報表
     * @return
     */
    public abstract ExcelWriter buildExcelWriter();
}
