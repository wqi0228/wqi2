/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.view.to.search.query;

import java.io.Serializable;
import java.util.List;

import com.cy.commons.vo.Org;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.vo.enums.Search17QueryColumn;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.view.component.CategoryTreeComponent;
import com.cy.tech.request.web.controller.view.component.ReportOrgTreeComponent;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterArrayStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterDetailVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterVO;
import com.cy.tech.request.web.listener.CategoryTreeCallBack;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.listener.ReportCustomFilterCallback;
import com.cy.tech.request.web.listener.ReportOrgTreeCallBack;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.cache.WkOrgCache;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
public class SearchQuery17CustomFilter implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4289150998781239980L;
    /** 登入者Sid */
    private Integer loginUserSid;
    /** 登入者部門UserSid */
    private Integer loginDepSid;
    @Getter
    private SearchQuery17 tempSearchQuery17;
    /** 該登入者在需求單,自訂查詢條件List */
    private List<ReportCustomFilterVO> reportCustomFilterVOs;
    /** 該登入者在需求單,挑選自訂查詢條件物件 */
    private ReportCustomFilterVO selReportCustomFilterVO;
    /** ReportCustomFilterLogicComponent */
    private ReportCustomFilterLogicComponent reportCustomFilterLogicComponent;
    /** 類別樹 Component */
    @Getter
    private CategoryTreeComponent categoryTreeComponent;
    /** MessageCallBack */
    private MessageCallBack messageCallBack;
    /** DisplayController */
    private DisplayController display;
    /** ReportCustomFilterCallback */
    private ReportCustomFilterCallback reportCustomFilterCallback;
    @Getter
    /** 報表 組織樹 Component */
    private ReportOrgTreeComponent orgTreeComponent;

    @Getter
    /** 報表 組織樹 Component */
    private ReportOrgTreeComponent notifyorgTreeComponent;

    public SearchQuery17CustomFilter(String compId, Integer loginUserSid, Integer loginDepSid, Integer loginCompSid,
            ReportCustomFilterLogicComponent reportCustomFilterLogicComponent, MessageCallBack messageCallBack,
            DisplayController display,
            ReportCustomFilterCallback reportCustomFilterCallback,
            List<Long> roleSids, Org dep) {
        this.loginUserSid = loginUserSid;
        this.loginDepSid = loginDepSid;
        this.reportCustomFilterLogicComponent = reportCustomFilterLogicComponent;
        this.messageCallBack = messageCallBack;
        this.display = display;
        this.categoryTreeComponent = new CategoryTreeComponent(categoryTreeCallBack);
        this.reportCustomFilterCallback = reportCustomFilterCallback;
        this.tempSearchQuery17 = new SearchQuery17(ReportType.WORK_ON_HISTORY);
        
        this.orgTreeComponent = new ReportOrgTreeComponent(
                compId, dep, roleSids, true, reportOrgTreeCallBack);
        
        this.notifyorgTreeComponent = new ReportOrgTreeComponent(
                compId, dep, roleSids, false, reportOrgTreeNoticeCallBack);
    }

    public void loadDefaultSetting(List<String> noticeDepts, SearchQuery17 searchQuery) {
        this.initDefault(noticeDepts, searchQuery);
        reportCustomFilterVOs = reportCustomFilterLogicComponent.getReportCustomFilter(Search17QueryColumn.Search17Query, loginUserSid);
        if (reportCustomFilterVOs != null && !reportCustomFilterVOs.isEmpty()) {
            selReportCustomFilterVO = reportCustomFilterVOs.get(0);
            loadSettingData(searchQuery);
        }
    }

    /** 載入挑選的自訂搜尋條件 */
    public void loadSettingData(SearchQuery17 searchQuery) {
        selReportCustomFilterVO.getReportCustomFilterDetailVOs().forEach(item -> {
            if (Search17QueryColumn.DemandType.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDemandType(item, searchQuery);
            } else if (Search17QueryColumn.DemandDep.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingDemandDep(item, searchQuery);
            } else if (Search17QueryColumn.NotifyDep.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingNotifyDep(item, searchQuery);
            } else if (Search17QueryColumn.SearchText.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingFuzzyText(item, searchQuery);
            } else if (Search17QueryColumn.OnpgNo.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingOnpgNo(item, searchQuery);
            } else if (Search17QueryColumn.CategoryCombo.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingCategoryCombo(item, searchQuery);
            } else if (Search17QueryColumn.OnpgStatus.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingOnpgStatus(item, searchQuery);
            } else if (Search17QueryColumn.DateIndex.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDateIndex(item, searchQuery);
            }
        });
    }

    /**
     * 塞入送測狀態
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingOnpgStatus(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery17 searchQuery) {
        try {
            searchQuery.getOnpgStatus().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQuery.getOnpgStatus().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingOnpgStatus", e);
        }
    }

    /**
     * 塞入立單區間Type
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDateIndex(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery17 searchQuery) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQuery.setDateTypeIndex(Integer.valueOf(reportCustomFilterStringVO.getValue()));

            switch (searchQuery.getDateTypeIndex()) {
                case 0:
                    searchQuery.setStartDate(null);
                    searchQuery.setEndDate(null);
                    searchQuery.changeDateIntervalPreMonth();
                    break;
                case 1:
                    searchQuery.setStartDate(null);
                    searchQuery.setEndDate(null);
                    searchQuery.changeDateIntervalThisMonth();
                    break;
                case 2:
                    searchQuery.setStartDate(null);
                    searchQuery.setEndDate(null);
                    searchQuery.changeDateIntervalNextMonth();
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            log.error("settingFuzzyText", e);
        }
    }

    /**
     * 塞入需求類別
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingCategoryCombo(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery17 searchQuery) {
        try {
            searchQuery.getSmallDataCateSids().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQuery.getSmallDataCateSids().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingCategoryCombo", e);
        }
    }

    /**
     * 塞入送測單號
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingOnpgNo(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery17 searchQuery) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQuery.setOnpgNo(reportCustomFilterStringVO.getValue());
        } catch (Exception e) {
            log.error("settingFuzzyText", e);
        }
    }

    /**
     * 塞入模糊搜尋
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingFuzzyText(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery17 searchQuery) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQuery.setFuzzyText(reportCustomFilterStringVO.getValue());
        } catch (Exception e) {
            log.error("settingFuzzyText", e);
        }
    }

    /**
     * 塞入通知單位
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingNotifyDep(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery17 searchQuery) {
        try {
            searchQuery.getNoticeDepts().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQuery.getNoticeDepts().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingSendTestDep", e);
        }
    }

    /**
     * 塞入填單單位
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandDep(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery17 searchQuery) {
        try {
            searchQuery.getRequireDepts().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQuery.getRequireDepts().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingDemandType", e);
        }
    }

    /**
     * 塞入需求類別
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandType(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery17 searchQuery) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQuery.setSelectBigCategorySid(reportCustomFilterStringVO.getValue());
        } catch (Exception e) {
            log.error("settingDemandType", e);
        }
    }

    /** 儲存自訂搜尋條件 */
    public void saveReportCustomFilter() {
        try {
            ReportCustomFilterDetailVO demandType = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search17QueryColumn.DemandType, (tempSearchQuery17.getSelectBigCategorySid() != null) ? tempSearchQuery17.getSelectBigCategorySid() : "");
            ReportCustomFilterDetailVO demandDep = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search17QueryColumn.DemandDep, tempSearchQuery17.getRequireDepts());
            ReportCustomFilterDetailVO notifyDep = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search17QueryColumn.NotifyDep, tempSearchQuery17.getNoticeDepts());
            ReportCustomFilterDetailVO searchText = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search17QueryColumn.SearchText, (tempSearchQuery17.getFuzzyText() != null) ? tempSearchQuery17.getFuzzyText() : "");
            ReportCustomFilterDetailVO onpgNo = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search17QueryColumn.OnpgNo, (tempSearchQuery17.getOnpgNo() != null) ? tempSearchQuery17.getOnpgNo() : "");
            ReportCustomFilterDetailVO categoryCombo = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search17QueryColumn.CategoryCombo, tempSearchQuery17.getSmallDataCateSids());
            ReportCustomFilterDetailVO onpgStatus = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search17QueryColumn.OnpgStatus, tempSearchQuery17.getOnpgStatus());
            ReportCustomFilterDetailVO dateIndex = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search17QueryColumn.DateIndex, (tempSearchQuery17.getDateTypeIndex() != null) ? String.valueOf(tempSearchQuery17.getDateTypeIndex()) : "5");
            List<ReportCustomFilterDetailVO> saveDetails = Lists.newArrayList();
            saveDetails.add(demandType);
            saveDetails.add(demandDep);
            saveDetails.add(notifyDep);
            saveDetails.add(searchText);
            saveDetails.add(onpgNo);
            saveDetails.add(categoryCombo);
            saveDetails.add(onpgStatus);
            saveDetails.add(dateIndex);
            reportCustomFilterLogicComponent.saveReportCustomFilter(Search17QueryColumn.Search17Query, loginUserSid, (selReportCustomFilterVO != null) ? selReportCustomFilterVO.getIndex() : null, true, saveDetails);
            reportCustomFilterCallback.reloadDefault("");
            display.update("headerTitle");
            display.execute("doSearchData();");
            display.hidePfWidgetVar("dlgReportCustomFilter");
        } catch (Exception e) {
            this.messageCallBack.showMessage(e.getMessage());
            log.error("saveReportCustomFilter ERROR", e);
        }
    }

    public void initDefault(List<String> noticeDepts, SearchQuery17 searchQuery) {
        searchQuery.clear(noticeDepts);
    }

    /** 類別樹 Component CallBack */
    private final CategoryTreeCallBack categoryTreeCallBack = new CategoryTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -1131954780074001191L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelCate() {
            categoryTreeComponent.selCate();
            tempSearchQuery17.getBigDataCateSids().clear();
            tempSearchQuery17.getMiddleDataCateSids().clear();
            tempSearchQuery17.getSmallDataCateSids().clear();
            tempSearchQuery17.getBigDataCateSids().addAll(Lists.newArrayList(categoryTreeComponent.getBigDataCateSids()));
            tempSearchQuery17.getMiddleDataCateSids().addAll(Lists.newArrayList(categoryTreeComponent.getMiddleDataCateSids()));
            tempSearchQuery17.getSmallDataCateSids().addAll(Lists.newArrayList(categoryTreeComponent.getSmallDataCateSids()));
        }
    };

    /**
     * 開啟 類別樹
     */
    public void btnOpenCategoryTree() {
        try {
            categoryTreeComponent.init();
            categoryTreeComponent.selectedItem(tempSearchQuery17.getSmallDataCateSids());
            display.showPfWidgetVar("defaultDlgCate");
        } catch (Exception e) {
            log.error("btnOpenCategoryTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /** 清除立單區間Type */
    public void clearDateType() {
        if (3 == tempSearchQuery17.getDateTypeIndex()) {
            tempSearchQuery17.setDateTypeIndex(4);
        }
    }

    /**
     * 開啟 單位挑選 組織樹
     */
    public void btnOpenOrgTree() {
        try {
            Org dep = WkOrgCache.getInstance().findBySid(loginDepSid);
            orgTreeComponent.initOrgTree(dep, tempSearchQuery17.getRequireDepts(), false, false);
            display.showPfWidgetVar("defaultdlgOrgTree");
        } catch (Exception e) {
            log.error("btnOpenOrgTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void btnOpenNoticeOrgTree() {
        try {
            Org dep = WkOrgCache.getInstance().findBySid(loginDepSid);
            notifyorgTreeComponent.initOrgTree(dep, tempSearchQuery17.getNoticeDepts(), false, false);
            display.showPfWidgetVar("defaultdlgOrgTreeNotice");
        } catch (Exception e) {
            log.error("btnOpenNoticeOrgTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /** 報表 組織樹 Component CallBack */
    private final ReportOrgTreeCallBack reportOrgTreeCallBack = new ReportOrgTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 7759084163365246435L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelOrg() {
            tempSearchQuery17.getRequireDepts().clear();
            tempSearchQuery17.getRequireDepts().addAll(orgTreeComponent.getSelOrgSids());
        }
    };

    /** 報表 組織樹 Component CallBack */
    private final ReportOrgTreeCallBack reportOrgTreeNoticeCallBack = new ReportOrgTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -988869561453482032L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelOrg() {
            tempSearchQuery17.setNoticeDepts(notifyorgTreeComponent.getSelOrgSids());
        }
    };

}
