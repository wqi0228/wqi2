/**
 * 
 */
package com.cy.tech.request.web.controller.setting;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.omnifaces.util.Faces;
import org.primefaces.event.ReorderEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerCallback;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerComponent;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerConfig;
import com.cy.tech.request.web.controller.component.mipker.helper.MultItemPickerByOrgHelper;
import com.cy.tech.request.web.controller.component.mipker.vo.MultItemPickerShowMode;
import com.cy.tech.request.web.controller.component.mutiuserqkpker.MultiUserQuickPickerComponent;
import com.cy.tech.request.web.controller.component.mutiuserqkpker.MultiUserQuickPickerConfig;
import com.cy.tech.request.web.pf.utils.ConfirmCallbackDialogController;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.logic.SettingCustomGroupService;
import com.cy.work.vo.SettingCustomGroupVO;
import com.cy.work.vo.enums.CustomGroupType;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 *
 */
@Slf4j
@Controller
@Scope("view")
public class SettingCustomGroupMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4531677068894584340L;
    // ========================================================================
    // 服務區
    // ========================================================================
    @Autowired
    private SettingCustomGroupService settingCustomGroupService;
    @Autowired
    private transient MultItemPickerByOrgHelper multItemPickerByOrgHelper;
    @Autowired
    private DisplayController displayController;
    @Autowired
    private ConfirmCallbackDialogController confirmCallbackDialogController;

    // ========================================================================
    // ID
    // ========================================================================
    @Getter
    private final String SETTING_TYPE_DEP = CustomGroupType.DEP.name();
    @Getter
    private final String SETTING_TYPE_USER = CustomGroupType.USER.name();
    /** data table */
    @Getter
    private final String DATA_TABLE_ID = "DATA_TABLE_ID";
    /** 群組編輯-視窗ID、widgetVar */
    @Getter
    private final String EDIT_DIALOG_NAME = "EDIT_DIALOG_NAME";
    /** 群組編輯-視窗內容區 */
    @Getter
    private final String EDIT_DIALOG_CONTENT = "EDIT_DIALOG_CONTENT";
    /** 群組編輯 - 部門選單元件ID */
    @Getter
    private final String DEP_DLG_GROUP_DEP_PICKER_ID = "DEP_DLG_GROUP_DEP_PICKER_ID";
    /** 群組編輯 - 人員選單元件ID */
    @Getter
    private final String EDIT_DLG_USER_GROUP_PICKER_ID = "EDIT_DLG_USER_GROUP_PICKER_ID";

    /** 分享部門-編輯視窗 */
    @Getter
    private final String SHARE_DEP_DLG_NAME = "SHARE_DEP_DLG_NAME";
    /** 分享部門-編輯視窗內容區 */
    @Getter
    private final String SHARE_DEP_DLG_CONTENT = "SHARE_DEP_DLG_CONTENT";
    /** 分享部門-編輯視窗-部門選擇器ID */
    @Getter
    private final String SHARE_DEP_PICKER_ID = "SHARE_DEP_PICKER_ID";
    /** 分享人員-編輯視窗 */
    @Getter
    private final String SHARE_USER_DLG_NAME = "SHARE_USER_DLG_NAME";
    /** 分享人員-編輯視窗內容區 */
    @Getter
    private final String SHARE_USER_DLG_CONTENT = "SHARE_USER_DLG_CONTENT";
    /** 分享人員-編輯視窗-部門選擇器ID */
    @Getter
    private final String SHARE_USER_PICKER_ID = "SHARE_USER_PICKER_ID";

    // ========================================================================
    // var
    // ========================================================================
    /** 列表顯示區顯示資料-部門群組 */
    @Getter
    private List<SettingCustomGroupVO> depSettings;
    /** 列表顯示區顯示資料-人員群組 */
    @Getter
    private List<SettingCustomGroupVO> userSettings;
    @Getter
    private SettingCustomGroupVO editVO;
    /** 編輯模式是否為新增 */
    @Getter
    private boolean editModeIsAdd = false;
    /** 編輯類型是否為群組部門 */
    @Getter
    private boolean editTypeIsDep = false;

    @Getter
    @Setter
    private SettingCustomGroupVO selectedVO;

    /**
     * 部門群組設定 - 部門選擇器
     */
    @Getter
    private transient MultItemPickerComponent groupDepsPicker;
    /**
     * 人員群組設定 - 人員選擇器
     */
    @Getter
    private transient MultiUserQuickPickerComponent groupUsersPicker;
    /**
     * 部門選擇器 - 分享部門
     */
    @Getter
    private transient MultItemPickerComponent shareDepsPicker;

    /**
     * 人員選擇器 - 分享人員
     */
    @Getter
    private transient MultiUserQuickPickerComponent shareUsersPicker;

    /**
     * 焦點 tab
     */
    @Getter
    private String focusTab = SETTING_TYPE_DEP;

    /**
     * 呼叫者的 ComponentID
     */
    @Getter
    private String openerComponentID;

    // ========================================================================
    // 方法區
    // ========================================================================
    @PostConstruct
    public void init() {
        //log.info("SettingCustomGroupMBean init");

        // ====================================
        // 取得傳入參數
        // ====================================
        // 為 multItemPicker 群組選單呼叫
        if (Faces.getRequestParameterMap().containsKey("componentID")) {
            openerComponentID = Faces.getRequestParameterMap().get("componentID");
        }

        // 網址參數中指定顯示部門或人員
        if (Faces.getRequestParameterMap().containsKey("ctype")) {
            focusTab = Faces.getRequestParameterMap().get("ctype");
        }

        // ====================================
        // 查詢全部資料
        // ====================================
        try {
            this.prepareAllData();
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        }
    }

    /**
     * 
     */
    private void reloadOpener() {
        if (WkStringUtils.notEmpty(this.openerComponentID)) {
            this.displayController.execute("reloadOpener('" + openerComponentID + "');");
        }
    }

    /**
     * 查詢所有資料
     * 
     * @throws UserMessageException
     */
    private void prepareAllData() throws UserMessageException {
        try {
            // ====================================
            // 查詢全部資料
            // ====================================
            List<SettingCustomGroupVO> allData = this.settingCustomGroupService.findByCreateUser(SecurityFacade.getUserSid());

            // ====================================
            // 分類
            // ====================================
            // 部門資料
            this.depSettings = allData.stream()
                    .filter(vo -> CustomGroupType.DEP.equals(vo.getGroupType()))
                    .collect(Collectors.toList());

            // 人員資料
            this.userSettings = allData.stream()
                    .filter(vo -> CustomGroupType.USER.equals(vo.getGroupType()))
                    .collect(Collectors.toList());

            // ====================================
            // 更新畫面
            // ====================================
            this.displayController.update(
                    Lists.newArrayList(
                            this.DATA_TABLE_ID + this.SETTING_TYPE_DEP,
                            this.DATA_TABLE_ID + this.SETTING_TYPE_USER));

        } catch (Exception e) {
            String errorMessage = "查詢設定資料失敗 (" + e.getMessage() + ")";
            log.error(errorMessage, e);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }
    }

    /**
     * @param isDep
     * @param isAdd
     * @param editSid
     */
    public void openEditGroupDialog(boolean isDep, boolean isAdd, long editSid) {

        // 編輯模式:新增/編輯
        this.editModeIsAdd = isAdd;
        // 編輯類型:部門/人員
        this.editTypeIsDep = isDep;

        // ====================================
        // 準備編輯內容 VO
        // ====================================
        if (isAdd) {
            // 初始化資料容器
            this.editVO = new SettingCustomGroupVO();
        } else {
            // 依據傳入 sid , 查詢群組資料
            try {
                // 查詢
                this.editVO = this.settingCustomGroupService.findBySid(editSid);
                // 檢查編輯資料已不存在
                if (this.editVO == null) {
                    MessagesUtils.showWarn(WkMessage.NEED_RELOAD);
                    return;
                }
            } catch (Exception e) {
                String errorMessage = WkMessage.EXECTION + "(" + e.getMessage() + ")";
                log.error(errorMessage, e);
                MessagesUtils.showError(errorMessage);
                return;
            }
        }

        // ====================================
        // 初始化部門/人員選單元件
        // ====================================
        if (isDep) {
            // 初始化單位群組選擇器
            this.initGroupDepsPicker();
        } else {
            // 初始化人員群組選擇器
            this.initGroupUsersPicker(this.editVO.getGroupUserSids());
        }

        // ====================================
        // 畫面處理
        // ====================================
        // 更新編輯內容
        this.displayController.update(this.EDIT_DIALOG_CONTENT);
        // 開啟 dialog
        this.displayController.showPfWidgetVar(this.EDIT_DIALOG_NAME);
    }

    /**
     * 編輯儲存
     */
    public void openEditGroupDialog_save() {

        // ====================================
        // 防呆
        // ====================================
        if (this.editVO == null) {
            MessagesUtils.showWarn(WkMessage.SESSION_TIMEOUT);
            return;
        }
        // ====================================
        // 判斷編輯類別
        // ====================================
        CustomGroupType customGroupType = this.editTypeIsDep ? CustomGroupType.DEP : CustomGroupType.USER;

        // ====================================
        // 由元件中, 取得群組的設定項目
        // ====================================

        List<Integer> groupItemSids = null;
        if (CustomGroupType.DEP.equals(customGroupType)) {
            // 取得設定的部門 SID
            groupItemSids = MultItemPickerByOrgHelper.getInstance().itemsToSids(this.groupDepsPicker.getSelectedItems());
        } else {
            // 取得設定的人員 SID
            groupItemSids = this.groupUsersPicker.getSelecedtUserSids();
        }

        // ====================================
        // 新增
        // ====================================
        try {
            if (this.editModeIsAdd) {
                // 新增
                this.settingCustomGroupService.SaveForMaintainAdd(
                        this.editVO.getGroupName(),
                        this.editVO.getGroupDescr(),
                        customGroupType,
                        groupItemSids,
                        SecurityFacade.getUserSid());
            } else {
                // 編輯
                this.settingCustomGroupService.SaveForMaintainUpdate(
                        this.editVO.getSid(),
                        this.editVO.getGroupName(),
                        this.editVO.getGroupDescr(),
                        customGroupType,
                        groupItemSids,
                        SecurityFacade.getUserSid());
            }

            // 關閉視窗
            this.displayController.hidePfWidgetVar(EDIT_DIALOG_NAME);

        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + e.getMessage();
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);
            return;
        }

        // ====================================
        // 更新呼叫者資料 (若為由 MultItemPicker 開啟 )
        // ====================================
        this.reloadOpener();

        // ====================================
        // 更新畫面資料
        // ====================================
        try {
            this.prepareAllData();
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        }
    }

    /**
     * 更新部門群組排序
     * 
     * @param event
     */
    public void sortDepGroups(ReorderEvent event) {
        log.info("更新部門群組排序 Row MovedFrom: " + event.getFromIndex() + ", To:" + event.getToIndex());
        this.sort(this.depSettings);
    }

    /**
     * 更新人員群組排序
     * 
     * @param event
     */
    public void sortUserGroups(ReorderEvent event) {
        log.info("更新人員群組排序 Row MovedFrom: " + event.getFromIndex() + ", To:" + event.getToIndex());
        this.sort(this.userSettings);
    }

    /**
     * 異動排序
     * 
     * @param event
     */
    private void sort(List<SettingCustomGroupVO> settings) {

        // ====================================
        // 更新排序序號
        // ====================================
        // 防呆
        if (WkStringUtils.isEmpty(settings)) {
            return;
        }

        try {
            // 收集排序後的 SID
            List<Long> soredSids = settings.stream()
                    .map(SettingCustomGroupVO::getSid)
                    .collect(Collectors.toList());

            // 更新排序序號
            this.settingCustomGroupService.updateSortBySid(soredSids, SecurityFacade.getUserSid());

        } catch (Exception e) {
            String msg = "排序資料時發生錯誤!";
            log.error(msg, e);
            MessagesUtils.showError(msg);
            return;
        }

        // ====================================
        // 更新呼叫者資料 (若為由 MultItemPicker 開啟 )
        // ====================================
        this.reloadOpener();

        // ====================================
        // 重撈列表資料
        // ====================================
        try {
            this.prepareAllData();
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        }
    }

    /**
     * @param sid
     */
    public void preDelete(Long sid) {
        // ====================================
        // 查詢編輯的資料
        // ====================================
        try {
            this.editVO = this.settingCustomGroupService.findBySid(sid);
        } catch (Exception e) {
            String errorMessage = WkMessage.EXECTION + "(" + e.getMessage() + ")";
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);
            return;
        }

        // ====================================
        // 確認視窗
        // ====================================
        confirmCallbackDialogController.showConfimDialog(
                "確認要將『" + this.editVO.getGroupName() + "』刪除嗎？",
                "",
                () -> this.delete());
    }

    /**
     * 
     */
    public void delete() {
        // ====================================
        // 防呆
        // ====================================
        if (this.editVO == null || this.editVO.getSid() == null) {
            MessagesUtils.showWarn(WkMessage.NEED_RELOAD);
        }

        // ====================================
        // 刪除資料
        // ====================================
        try {
            this.settingCustomGroupService.delete(this.editVO.getSid());
        } catch (Exception e) {
            String msg = "排序資料時發生錯誤!";
            log.error(msg, e);
            MessagesUtils.showError(msg);
            return;
        }

        // ====================================
        // 更新呼叫者資料 (若為由 MultItemPicker 開啟 )
        // ====================================
        this.reloadOpener();

        // ====================================
        // 重撈列表資料
        // ====================================
        try {
            this.prepareAllData();
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        }
    }

    /**
     * 編輯儲存
     */
    public void saveShareSetting(boolean isDep) {

        // ====================================
        // 防呆
        // ====================================
        if (this.editVO == null) {
            MessagesUtils.showWarn(WkMessage.SESSION_TIMEOUT);
            return;
        }

        // ====================================
        // 新增
        // ====================================
        try {
            List<Integer> shereSids = null;
            if (isDep) {
                // 取得選擇部門
                shereSids = this.multItemPickerByOrgHelper.itemsToSids(this.shareDepsPicker.getSelectedItems());
            } else {
                // 取得選擇人員
                shereSids = this.shareUsersPicker.getSelecedtUserSids();
            }

            // 編輯
            this.settingCustomGroupService.SaveShareSetting(
                    isDep,
                    this.editVO.getSid(),
                    shereSids,
                    SecurityFacade.getUserSid());

            // 關閉視窗
            this.displayController.hidePfWidgetVar(isDep ? this.SHARE_DEP_DLG_NAME : this.SHARE_USER_DLG_NAME);

        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + e.getMessage();
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);
            return;
        }

        // ====================================
        // 更新呼叫者資料 (若為由 MultItemPicker 開啟 )
        // ====================================
        this.reloadOpener();

        // ====================================
        // 更新畫面資料
        // ====================================
        try {
            this.prepareAllData();
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        }
    }

    // ========================================================================
    // GroupDepsPicker 群組單位選擇器
    // ========================================================================
    /**
     * 初始化群組單位選擇器
     */
    private void initGroupDepsPicker() {
        try {
            // 選擇器設定資料
            MultItemPickerConfig config = new MultItemPickerConfig();
            config.setDefaultShowMode(MultItemPickerShowMode.TREE);
            config.setTreeModePrefixName("單位");
            config.setContainFollowing(true);
            config.setItemComparator(this.multItemPickerByOrgHelper.parpareComparator());
            // 設定ID 以自動清過濾器
            config.setComponentID(this.DEP_DLG_GROUP_DEP_PICKER_ID);

            // 選擇器初始化
            this.groupDepsPicker = new MultItemPickerComponent(config, groupDepsPickerPickerCallback);

        } catch (Exception e) {
            String message = "單位選單初始化失敗!" + e.getMessage();
            MessagesUtils.showError(message);
            log.error(message, e);
            return;
        }
    }

    /**
     * 可執行單位設定選單 callback
     */
    private final MultItemPickerCallback groupDepsPickerPickerCallback = new MultItemPickerCallback() {

        /**
         * 
         */
        private static final long serialVersionUID = 8214133787909252799L;

        /**
         * 取得所有可選擇單位
         */
        @Override
        public List<WkItem> prepareAllItems() throws SystemDevelopException {
            // 取得所有單位
            return multItemPickerByOrgHelper.prepareAllOrgItems();
        }

        /**
         * 取得已選擇部門
         */
        @Override
        public List<WkItem> prepareSelectedItems() throws SystemDevelopException {

            // 檢查資料為空
            if (editVO == null || WkStringUtils.isEmpty(editVO.getGroupDepSids())) {
                return Lists.newArrayList();
            }

            // 轉為 WkItem 物件
            return multItemPickerByOrgHelper.prepareWkItemByDepSids(Sets.newHashSet(editVO.getGroupDepSids()));
        }

        /**
         * 準備 disable 的單位
         */
        @Override
        public List<String> prepareDisableItemSids() throws SystemDevelopException {
            // 不需要 Disable
            return Lists.newArrayList();
        }
    };

    // ========================================================================
    // GroupUsersPicker 人員群組選擇器
    // ========================================================================
    /**
     * 初始化群組單位選擇器
     * 
     * @param selectedUserSids 已選擇人員清單
     */
    private void initGroupUsersPicker(List<Integer> selectedUserSids) {
        try {
            // 選擇器設定資料
            MultiUserQuickPickerConfig config = new MultiUserQuickPickerConfig();
            // 預設展開登入者
            config.setDefaultExpandUserSids(Sets.newHashSet(SecurityFacade.getUserSid()));

            // 選擇器初始化
            this.groupUsersPicker = new MultiUserQuickPickerComponent(
                    SecurityFacade.getCompanyId(),
                    this.EDIT_DLG_USER_GROUP_PICKER_ID,
                    selectedUserSids,
                    config,
                    null);

        } catch (Exception e) {
            String errorMessage = "初始化人員選單失敗!";
            log.error(errorMessage, e);
            MessagesUtils.showError("初始化人員選單失敗, 請恰系統人員!");
            return;
        }
    }

    // ========================================================================
    // ShareDepsPicker 分享單位選擇器
    // ========================================================================
    /**
     * 開啟單位分享設定視窗
     * 
     * @param sid
     */
    public void openShareDepDialog_Edit(Long sid) {

        // ====================================
        // 查詢編輯的資料
        // ====================================
        try {
            this.editVO = this.settingCustomGroupService.findBySid(sid);
        } catch (Exception e) {
            String errorMessage = WkMessage.EXECTION + "(" + e.getMessage() + ")";
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);
            return;
        }

        // ====================================
        // 查詢編輯資料
        // ====================================
        try {
            // 選擇器設定資料
            MultItemPickerConfig config = new MultItemPickerConfig();
            config.setDefaultShowMode(MultItemPickerShowMode.TREE);
            config.setTreeModePrefixName("單位");
            config.setContainFollowing(true);
            config.setItemComparator(this.multItemPickerByOrgHelper.parpareComparator());

            // 選擇器初始化
            this.shareDepsPicker = new MultItemPickerComponent(config, shareDepsPickerPickerCallback);

        } catch (Exception e) {
            String message = "單位選單初始化失敗!" + e.getMessage();
            MessagesUtils.showError(message);
            log.error(message, e);
            return;
        }

        // ====================================
        // 畫面控制
        // ====================================
        // 更新dialog 資料
        this.displayController.update(this.SHARE_DEP_DLG_CONTENT);
        // 開啟視窗
        this.displayController.showPfWidgetVar(this.SHARE_DEP_DLG_NAME);
    }

    /**
     * 可執行單位設定選單 callback
     */
    private final MultItemPickerCallback shareDepsPickerPickerCallback = new MultItemPickerCallback() {

        /**
         * 
         */
        private static final long serialVersionUID = -5488786193070947625L;

        /**
         * 取得所有可選擇單位
         */
        @Override
        public List<WkItem> prepareAllItems() throws SystemDevelopException {
            // 取得所有單位
            return multItemPickerByOrgHelper.prepareAllOrgItems();
        }

        /**
         * 取得已選擇部門
         */
        @Override
        public List<WkItem> prepareSelectedItems() throws SystemDevelopException {

            // 檢查資料為空
            if (editVO == null || WkStringUtils.isEmpty(editVO.getShareDepSids())) {
                return Lists.newArrayList();
            }

            // 轉為 WkItem 物件
            return multItemPickerByOrgHelper.prepareWkItemByDepSids(Sets.newHashSet(editVO.getShareDepSids()));
        }

        /**
         * 準備 disable 的單位
         */
        @Override
        public List<String> prepareDisableItemSids() throws SystemDevelopException {
            // 不需要 Disable
            return Lists.newArrayList();
        }
    };

    // ========================================================================
    // ShareUsersPicker 分享人員選擇器
    // ========================================================================
    /**
     * 開啟人員選單
     * 
     * @param maintainTypeStr
     */
    public void openShareUserDialog_Edit(Long sid) {

        // ====================================
        // 查詢編輯的資料
        // ====================================
        try {
            this.editVO = this.settingCustomGroupService.findBySid(sid);
        } catch (Exception e) {
            String errorMessage = WkMessage.EXECTION + "(" + e.getMessage() + ")";
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);
            return;
        }

        // 取得已設定的分享人員
        List<Integer> selectedUserSids = this.editVO.getShareUserSids();
        if (selectedUserSids == null) {
            selectedUserSids = Lists.newArrayList();
        }

        // ====================================
        // 初始化單位選擇元件
        // ====================================
        try {
            // 選擇器設定資料
            MultiUserQuickPickerConfig config = new MultiUserQuickPickerConfig();
            // 預設展開登入者
            config.setDefaultExpandUserSids(Sets.newHashSet(SecurityFacade.getUserSid()));

            // 選擇器初始化
            this.shareUsersPicker = new MultiUserQuickPickerComponent(
                    SecurityFacade.getCompanyId(),
                    this.SHARE_USER_PICKER_ID,
                    selectedUserSids,
                    config,
                    null);

        } catch (Exception e) {
            String errorMessage = "初始化人員選單失敗!";
            log.error(errorMessage, e);
            MessagesUtils.showError("初始化人員選單失敗, 請恰系統人員!");
            return;
        }

        // ====================================
        // 畫面控制
        // ====================================
        // 更新dialog 資料
        this.displayController.update(this.SHARE_USER_DLG_CONTENT);
        // 開啟視窗
        this.displayController.showPfWidgetVar(this.SHARE_USER_DLG_NAME);
    }
}
