/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require.pt;

import com.cy.tech.request.vo.pt.PtAttachment;
import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.web.attach.IAttachDlg;
import com.cy.tech.request.web.controller.require.*;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.PFAttachmentUtils;
import com.cy.work.common.vo.AttachmentService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.omnifaces.util.Faces;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
//@Scope(WebApplicationContext.SCOPE_REQUEST)
@Scope("view")
public class PtAttachMBean implements IAttachDlg<Require01MBean, PtCheck, PtAttachment>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7032290516883562471L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    @Qualifier("pt_attach")
    transient private AttachmentService<PtAttachment, PtCheck, String> attachService;
    @Autowired
    transient private PFAttachmentUtils pfAttachUtils;
    @Autowired
    transient private DisplayController display;

    /** 刪除物件 */
    @Getter
    private PtAttachment editAttach;
    /** 選取的附加檔案 */
    @Getter
    @Setter
    private List<PtAttachment> selectedAttachments;
    /** 當下的附加檔案名稱 */
    private List<String> tempFileNames;
    /** 上傳完成後資訊 */
    @Getter
    @Setter
    private StringBuilder uploadMessages;
    /** 上傳失敗訊息 */
    @Getter
    private StringBuilder failMsg;
    /** 檔案上傳限制大小 */
    @Getter
    @Setter
    private Integer fileLimitSize;
    /** 預設Dialog寬度 */
    private final Integer defaultWidth = 1000;
    /** 預設Dialog高度 */
    private final Integer defaultHeight = 500;

    @PostConstruct
    public void init() {
        selectedAttachments = Lists.newArrayList();
        tempFileNames = Lists.newCopyOnWriteArrayList();
        uploadMessages = new StringBuilder();
        failMsg = new StringBuilder();
    }

    public List<PtAttachment> findAttachsByLazy(PtCheck ptCheck) {
        if (ptCheck == null) {
            return null;
        }
        return attachService.findAttachsByLazy(ptCheck);
    }

    /**
     * 由頁面呼叫
     *
     * @param fileSLimitize
     */
    @Override
    public void onUploadDialogOpen(Integer fileSLimitize) {
        this.fileLimitSize = fileSLimitize;
        tempFileNames.clear();
        this.clearMessages();
    }

    /**
     * 清除上傳過程所產生的訊息.
     */
    private void clearMessages() {
        uploadMessages.delete(0, uploadMessages.length());
        this.clearFailMessages();
    }

    /**
     * 清除上傳過程所產生的訊息(dialog)
     */
    @Override
    public void clearFailMessages() {
        failMsg.delete(0, failMsg.length());
    }

    /**
     * 由頁面呼叫
     *
     * @param event
     */
    @Override
    public void onUploadDialogClose(CloseEvent event) {
        selectedAttachments.clear();
    }

    /**
     * 由頁面呼叫，處理一筆檔案上傳的細節
     *
     * @param event
     */
    @Override
    public synchronized void handleFileUpload(FileUploadEvent event) {
        PtCheck ptCheck = (PtCheck) event.getComponent().getAttributes().get("attach_depend_entity");
        UploadedFile uploadedFile = event.getFile();
        // 在此步驟先把檔名正規化， 避免某些瀏覽器 (MSIE) 在檔名欄位填寫檔案完整路徑，
        // 造成檔名必定包含特殊字元 (back slash)
        String fileName = FilenameUtils.getName(uploadedFile.getFileName());
        String fileNameExtension = FilenameUtils.getExtension(fileName);
        File tempFile = null;
        try {
            if (attachService.validateByFileNameAndResponse(this.findAttachsByLazy(ptCheck), tempFileNames, fileName, uploadMessages, failMsg)) {
                attachService.createDirectoryIfNotExist();
                tempFile = attachService.saveToTemporaryFile(uploadedFile.getInputstream(), fileNameExtension);
                PtAttachment attachment = attachService.createEmptyAttachment(fileName, loginBean.getUser(), loginBean.getDep());
                File serverSideFile = attachService.getServerSideFile(attachment);
                tempFile.renameTo(serverSideFile);
                this.findAttachsByLazy(ptCheck).add(attachment);
                if (!Strings.isNullOrEmpty(ptCheck.getSid())) {
                    attachService.linkRelation(attachment, ptCheck, loginBean.getUser());
                } else {
                    this.selectedAttachments.add(attachment);
                }
                attachService.addMessage(fileName, uploadMessages, "OK");
            }
        } catch (IOException ex) {
            log.error("handleFileUpload faild", ex);
            attachService.addFailMessage(fileName, "發生錯誤，" + ex.getLocalizedMessage(), uploadMessages, failMsg);
        } catch (Exception ex) {
            log.error("上傳附件並進行對應發生錯誤", ex);
        } finally {
            FileUtils.deleteQuietly(tempFile);
        }
    }

    /**
     * 由頁面呼叫（「編輯」按鈕），開啟欄位
     *
     * @param attachment
     */
    public void initEdit(PtAttachment attachment) {
        attachment.setKeyCheckEdit(Boolean.TRUE);
    }

    /**
     * 由頁面呼叫（「存檔」按鈕），直接將資料狀態改成停用
     *
     * @param attachment
     */
    public void saveByEdit(PtAttachment attachment) {
        attachment.setKeyCheckEdit(Boolean.FALSE);
        try {
            attachService.updateAttach(attachment);
        } catch (Exception e) {
            log.error("更新附加檔案失敗。", e);
        }
    }

    @Override
    public void initRecycle(PtAttachment attachment) {
        editAttach = attachment;
    }

    @Override
    public void whenDeleteAttachment(Require01MBean r01MBean, PtCheck ptCheck) {
        this.deleteAttachment(ptCheck);
        r01MBean.getTraceMBean().clear();
        display.update("viewPanelBottomInfoTabId:req_trace_tab_id");
    }

    private void deleteAttachment(PtCheck ptCheck) {
        try {
            this.findAttachsByLazy(editAttach.getPtCheck()).remove(editAttach);
            selectedAttachments.remove(editAttach);
            attachService.changeStatusToInActive(this.findAttachsByLazy(ptCheck), editAttach, ptCheck, loginBean.getUser());
        } catch (Exception e) {
            log.error("停用附加檔案失敗!需求單單號:" + editAttach.getSourceNo() + " 原型確認版號:" + editAttach.getPtCheck().getVersion(), e);
        }
    }

    /**
     * 由頁面呼叫（「下載」按鈕），跟 PrimeFaces 的 fileDownload 標籤一起使用.
     *
     * @return
     * @throws IOException
     */
    public StreamedContent downloadFile() throws IOException {
        if (selectedAttachments.isEmpty()) {
            return null;
        }
        // 下載單一檔案，預設檔名使用該檔案的原始檔名（但是要經過編碼）
        if (selectedAttachments.size() == 1) {
            return pfAttachUtils.createStreamedContent(attachService, selectedAttachments.get(0));
        }
        // 將多檔案壓縮成單一 zip 檔案再輸出
        return pfAttachUtils.createWrappedStreamedContent(attachService, selectedAttachments);
    }

    public String downloadFileContentDisposition() {
        if (selectedAttachments.isEmpty()) {
            return null;
        }
        String encodedFileName = (selectedAttachments.size() == 1)
                ? attachService.encodeFileName(selectedAttachments.get(0).getFileName())
                : AttachmentService.MULTIFILES_ZIP_FILENAME;
        String contentDispositionPattern
                = attachService.isMSIE(Faces.getRequest())
                ? "{0}; filename=\"{1}\""
                : "{0}; filename=\"{1}\"; filename*=UTF-8''{1}";
        return MessageFormat.format(contentDispositionPattern, "attachment", encodedFileName);
    }

    /**
     * 由頁面呼叫，全選
     *
     * @param ptCheck
     */
    public void selectedAll(PtCheck ptCheck) {
        selectedAttachments.clear();
        this.findAttachsByLazy(ptCheck).forEach(each -> {
            each.setKeyChecked(Boolean.TRUE);
            selectedAttachments.add(each);
        });
    }

    /**
     * 異動Checkbox更改selectedAttachments
     *
     * @param ptCheck
     */
    public void changeCheckbox(PtCheck ptCheck) {
        selectedAttachments.clear();
        this.findAttachsByLazy(ptCheck).stream()
                .filter(each -> each.getKeyChecked())
                .forEach(each -> selectedAttachments.add(each));
    }

    /**
     * 讀取附件上傳者及部門
     *
     * @param attachment
     * @return
     */
    public String depAndUserName(PtAttachment attachment) {
        return attachService.depAndUserName(attachment);
    }

    @Override
    public Integer getDialogWidth() {
        return isRenderedByMsie() ? null : defaultWidth;
    }

    @Override
    public Integer getDialogHeight() {
        return isRenderedByMsie() ? null : defaultHeight;
    }

    /**
     * 判斷瀏覽器的引擎是否為 Microsoft Internet Explorer
     *
     * @return true: MSIE; false: otherwise.
     */
    private boolean isRenderedByMsie() {
        return attachService.isMSIE(Faces.getRequest()) && !attachService.isChromeFrame(Faces.getRequest());
    }

    public boolean showFunByDept(PtAttachment attachment) {
        return attachment.getUploadDept().getSid().equals(loginBean.getDep().getSid());
    }

    @Override
    public int findAttachCnt(PtCheck dependEntity) {
        List<PtAttachment> attachs = attachService.findAttachs(dependEntity);
        if (attachs == null) {
            return 0;
        }
        return attachs.size();
    }

    @Override
    public void endUploadAction(PtCheck dependEntity) {
    }

}
