package com.cy.tech.request.web.newsearch;

import lombok.Getter;

/**
 * @author allen1214_wu
 * 查詢區間類型
 */
public enum NewSearchConditionDateRangeType {

    ASSIGN_DATE("分派區間"),
    NOTICE_DATE("通知區間"),
    UPDATE_DATE("異動區間"),
    ;

    @Getter
    private final String lable;

    /**
     * @param lable
     */
    private NewSearchConditionDateRangeType(String lable) {
        this.lable = lable;
    }

}
