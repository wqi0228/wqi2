/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.vo;

import com.cy.tech.request.vo.enums.SearchReportCustomEnum;
import com.cy.tech.request.vo.enums.SearchReportCustomType;
import com.cy.work.common.utils.WkJsonUtils;
import java.util.List;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
public class ReportCustomFilterArrayStringVO implements ReportCustomFilterDetailVO {

    @Getter
    private SearchReportCustomEnum searchReportCustomEnum;
    @Getter
    private List<String> arrayStrings;

    public ReportCustomFilterArrayStringVO(SearchReportCustomEnum searchReportCustomEnum, List<String> arrayStrings) {
        this.searchReportCustomEnum = searchReportCustomEnum;
        this.arrayStrings = arrayStrings;
    }

    public ReportCustomFilterArrayStringVO(SearchReportCustomEnum searchReportCustomEnum, String value) {
        this.searchReportCustomEnum = searchReportCustomEnum;
        try {
            arrayStrings = WkJsonUtils.getInstance().fromJsonToList(value, String.class);
        } catch (Exception e) {
            log.error("WkJsonUtils.getInstance().fromJsonToList(value, String.class); ERROR", e);
        }
    }

    public String getJsonValue() {
        try {
            return WkJsonUtils.getInstance().toJson(arrayStrings);
        } catch (Exception e) {
            log.error("WkJsonUtils.getInstance().toJson(arrayStrings); ERROR", e);
        }
        return "";
    }

    public SearchReportCustomType getSearchReportCustomType() {
        return SearchReportCustomType.ARRAYSTRING;
    }

}
