/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.common;

import com.cy.tech.request.vo.enums.RequireTransProgramType;
import com.cy.tech.request.web.setting.RequireTransSetting;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.SelectItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 取得轉單程式-子程序狀態選項
 *
 * @author brain0925_liao
 */
@Controller
@Scope("request")
@Slf4j
public class SubProgramTypeBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -9103767120572298864L;

    public List<SelectItem> getTransJobRequireStatusTypeItems() {
        return RequireTransSetting.getTransJobRequireStatusTypeItems();
    }

    public List<SelectItem> getSubProgramTypeItems(String requireTransProgramTypeKey) {
        if (Strings.isNullOrEmpty(requireTransProgramTypeKey)) {
            return Lists.newArrayList();
        }
        try {
            RequireTransProgramType requireTransProgramType = RequireTransProgramType.valueOf(requireTransProgramTypeKey);
            if (requireTransProgramType.equals(RequireTransProgramType.PTCHECK)) {
                return RequireTransSetting.getTransJobPtStatusItems();
            } else if (requireTransProgramType.equals(RequireTransProgramType.WORKTESTSIGNINFO)) {
                return RequireTransSetting.getTransJobWorkTestStatusItems();
            } else if (requireTransProgramType.equals(RequireTransProgramType.OTHSET)) {
                return RequireTransSetting.getTransJobOthSetStatusItems();
            } else if (requireTransProgramType.equals(RequireTransProgramType.WORKONPG)) {
                return RequireTransSetting.getTransJobWorkOnpgStatusItems();
            }
        } catch (Exception e) {
            log.error("getSubProgramTypeItems ERROR", e);
        }
        return Lists.newArrayList();
    }

}
