package com.cy.tech.request.web.controller.view.vo;

import java.io.Serializable;
import java.util.Set;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.vo.query.search.SearchQuery;
import com.cy.tech.request.web.controller.view.component.DateIntervalVO;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;

public class Search33QueryVO extends SearchQuery implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6426645882648538794L;

    public enum Search33Column {
        TEST_DATE("送測區間", "t1.test_date"),
        ESTIMATE_DATE("預計完成日", "t1.testinfo_estimate_dt"),
        QA_AUDIT_DATE("審核區間", "t1.qa_audit_dt");

        @Getter
        private String name;
        @Getter
        private String value;

        private Search33Column(String name, String value) {
            this.name = name;
            this.value = value;
        }
    }

    /** 日期種類 */
    @Getter
    @Setter
    private String dateType;
    /** 審核狀態 */
    @Getter
    @Setter
    private String qaAuditStatus = "WAIT_APPROVE";
    @Getter
    @Setter
    /** 日期區間 */
    private DateIntervalVO dateIntervalVO = new DateIntervalVO();

    /**
     * 填單單位
     */
    @Getter
    @Setter
    Set<Integer> sendTestDepSids = Sets.newHashSet();

    public Search33QueryVO() {
    }

    public Search33QueryVO(Org org, User user, ReportType reportType) {
        this.dep = org;
        this.user = user;
        this.reportType = reportType;
    }

    public void init() {
        dateType = Search33Column.TEST_DATE.getValue();
        qaAuditStatus = "WAIT_APPROVE";
        dateIntervalVO.clear();
    }

    public void triggerEndDateMin() {
        dateIntervalVO.setEndDate(dateIntervalVO.getStartDate());
    }

}
