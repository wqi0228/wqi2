/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require.onpg;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.onpg.OnpgShowService;
import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.onpg.WorkOnpgHistory;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 功能按鍵顯示處理
 *
 * @author shaun
 */
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class OnpgShowMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6012417948191128182L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private OnpgShowService opShowService;

    public String findNoticeDepTitle(WorkOnpg onpg) {
        return opShowService.findNoticeDepTitle(onpg);
    }

    public List<String> findNoticeDepTitleByOp(WorkOnpg onpg) {
        return opShowService.findNoticeDepTitleByOp(onpg);
    }

    public boolean disableEdit(Require req, WorkOnpg onpg) {
        return opShowService.disableEdit(req, onpg, loginBean.getUser());
    }

    /**
     * disabled 通知單位
     *
     * @param req
     * @param onpg
     * @return
     */
    public boolean disableNotifyDep(Require req, WorkOnpg onpg) {
        return opShowService.disableNotifyDep(req, onpg, loginBean.getUser());
    }

    public boolean disableCheckRecord(Require req, WorkOnpg onpg) {
        return opShowService.disableCheckRecord(req, onpg, loginBean.getUser());
    }

    public boolean disableGMReply(Require req, WorkOnpg onpg) {
        return opShowService.disableGMReply(req, onpg, loginBean.getUser());
    }

    public boolean disableQAReply(Require req, WorkOnpg onpg) {
        return opShowService.disableQAReply(req, onpg, loginBean.getUser());
    }

    public boolean disableGeneralReply(Require req, WorkOnpg onpg) {
        return opShowService.disableGeneralReply(req, onpg, loginBean.getUser());
    }

    public boolean showCheckRecordEdit(Require req, WorkOnpg onpg, WorkOnpgHistory history) {
        return opShowService.showCheckRecordEdit(req, onpg, history, loginBean.getUser());
    }

    public boolean disableCancel(Require req, WorkOnpg onpg) {
        return opShowService.disableCancel(req, onpg, loginBean.getUser());
    }

    public boolean disableCheckMemo(Require req, WorkOnpg onpg) {
        return opShowService.disableCheckMemo(req, onpg, loginBean.getUser());
    }

    public boolean disableCheckComplate(Require req, WorkOnpg onpg) {
        return opShowService.disableCheckComplate(
                req, 
                onpg, 
                loginBean.getUser(),
                loginBean.getDep().getSid(), 
                this.checkComplateBySmallCategoryRole(req.getMapping().getSmall().getExecOnpgOkRole()));
    }

    /**
     * 判斷登入者是否有符合小類 可On pg Ok 角色
     *
     * @param execOnpgOkRole
     * @return
     */
    private boolean checkComplateBySmallCategoryRole(String execOnpgOkRole) {
        if (Strings.isNullOrEmpty(execOnpgOkRole)) {
            return false;
        }
        List<String> permissions = Lists.newArrayList(execOnpgOkRole.split(","));
        
        return WkUserUtils.isUserHasRole(
				SecurityFacade.getUserSid(), 
				SecurityFacade.getCompanyId(),
				permissions);
    }

    public boolean showCheckMemoEdit(Require req, WorkOnpgHistory history) {
        return opShowService.showCheckMemoEdit(req, history, loginBean.getUser());
    }

    public boolean disableUploadFun(Require req, WorkOnpg onpg) {
        return opShowService.disableUploadFun(req, onpg);
    }

    public boolean isCheckRecordBehavior(WorkOnpgHistory history) {
        return opShowService.isCheckRecordBehavior(history);
    }
}
