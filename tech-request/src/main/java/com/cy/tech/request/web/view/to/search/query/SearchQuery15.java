/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.view.to.search.query;

import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.vo.query.search.SearchQuery;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.LocalDate;

/**
 * Search15MBean 查詢欄位
 *
 * @author kasim
 */
public class SearchQuery15 extends SearchQuery implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 5126442780956036100L;
    /** 預設 送測單號 */
	private String defaultTestinfoNo;
	@Getter
	@Setter
	/** 選擇的大類 */
	private String selectBigCategorySid;
	@Getter
	@Setter
	/** 類別組合(大類 sid) */
	private List<String> bigDataCateSids = Lists.newArrayList();
	@Getter
	@Setter
	/** 類別組合(中類 sid) */
	private List<String> middleDataCateSids = Lists.newArrayList();
	@Getter
	@Setter
	/** 類別組合(小類 sid) */
	private List<String> smallDataCateSids = Lists.newArrayList();
	@Getter
	@Setter
	/** 模糊搜尋 */
	private String fuzzyText;
	@Getter
	@Setter
	/** 送測單號 */
	private String testinfoNo;
	@Getter
	@Setter
	/** 送測狀態 */
	private List<String> testStatus = Lists.newArrayList();
	@Getter
	@Setter
	/** 時間切換的index */
	private Integer dateTypeIndex;
	@Getter
	@Setter
	/** 填單區間(起) */
	private Date startDate;
	@Getter
	@Setter
	/** 填單區間(訖) */
	private Date endDate;
	@Getter
	@Setter
	/** 立單區間(起) */
	private Date startUpdatedDate;
	@Getter
	@Setter
	/** 立單區間(訖) */
	private Date endUpdatedDate;
	@Getter
	@Setter
	/** 需求單號 */
	private String requireNo;
	@Getter
	@Setter
	/** 填單單位 */
	private List<String> requireDepts = Lists.newArrayList();
	@Getter
	@Setter
	/** 送測單位 */
	private List<String> noticeDepts = Lists.newArrayList();


	public SearchQuery15(ReportType reportType) {
		this.reportType = reportType;
	}

	public void initDefaultTestinfoNo(String defaultTestinfoNo) {
		this.defaultTestinfoNo = defaultTestinfoNo;
	}

	/**
	 * 清除
	 *
	 * @param noticeDepts
	 */
	public void clear(List<String> noticeDepts) {
		this.init();
		this.initDefault(noticeDepts);
	}

	/**
	 * 初始化
	 */
	private void init() {
		
		// 共用查詢條件初始化
		this.publicConditionInit();
		
		this.selectBigCategorySid = null;
		this.bigDataCateSids = Lists.newArrayList();
		this.middleDataCateSids = Lists.newArrayList();
		this.smallDataCateSids = Lists.newArrayList();
		this.fuzzyText = null;
		this.testinfoNo = null;
		this.testStatus = Lists.newArrayList();
		this.dateTypeIndex = 4;
		this.startDate = null;
		this.endDate = null;
		this.requireDepts = Lists.newArrayList();
		this.clearAdvance();
	}

	/**
	 * 清除進階選項
	 */
	public void clearAdvance() {
		this.startUpdatedDate = null;
		this.endUpdatedDate = null;
		this.requireNo = null;
	}

	/**
	 * 初始化報表預設值
	 *
	 * @param noticeDepts
	 */
	private void initDefault(List<String> noticeDepts) {
		this.noticeDepts = noticeDepts;
		if (!Strings.isNullOrEmpty(defaultTestinfoNo)) {
			this.testinfoNo = defaultTestinfoNo;
		}
	}

	/**
	 * 上個月
	 */
	public void changeDateIntervalPreMonth() {
		Date date = new Date();
		if (this.startDate != null) {
			date = this.startDate;
		}
		LocalDate lastDate = new LocalDate(date).minusMonths(1);
		this.dateTypeIndex = 0;
		this.startDate = lastDate.dayOfMonth().withMinimumValue().toDate();
		this.endDate = lastDate.dayOfMonth().withMaximumValue().toDate();
	}

	/**
	 * 本月份
	 */
	public void changeDateIntervalThisMonth() {
		this.dateTypeIndex = 1;
		this.startDate = new LocalDate().dayOfMonth().withMinimumValue().toDate();
		this.endDate = new LocalDate().dayOfMonth().withMaximumValue().toDate();
	}

	/**
	 * 下個月
	 */
	public void changeDateIntervalNextMonth() {
		Date date = new Date();
		if (this.startDate != null) {
			date = this.startDate;
		}
		LocalDate nextDate = new LocalDate(date).plusMonths(1);
		this.dateTypeIndex = 2;
		this.startDate = nextDate.dayOfMonth().withMinimumValue().toDate();
		this.endDate = nextDate.dayOfMonth().withMaximumValue().toDate();
	}
}
