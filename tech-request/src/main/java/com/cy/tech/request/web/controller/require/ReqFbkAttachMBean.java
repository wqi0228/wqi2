/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | TemplatesonUploadDialogOpen
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require;

import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.web.pf.utils.PFAttachmentUtils;
import com.cy.tech.request.vo.require.RequireFbkAttachment;
import com.cy.tech.request.vo.require.RequireTrace;
import com.cy.tech.request.web.attach.IAttachDlg;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.vo.AttachmentService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.omnifaces.util.Faces;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Slf4j
@NoArgsConstructor
@Controller
//@Scope(WebApplicationContext.SCOPE_REQUEST)
@Scope("view")
public class ReqFbkAttachMBean implements IAttachDlg<Require01MBean, RequireTrace, RequireFbkAttachment>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 458776062434771603L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    @Qualifier("require_fbk_attach")
    transient private AttachmentService<RequireFbkAttachment, RequireTrace, String> attachService;
    @Autowired
    transient private PFAttachmentUtils pfAttachUtils;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private RequireService requireService;

    /** 刪除物件 */
    @Getter
    private RequireFbkAttachment editAttach;
    /** 選取的附加檔案 */
    @Getter
    @Setter
    private List<RequireFbkAttachment> selectedAttachments;
    /** 當下的附加檔案名稱 */
    private List<String> tempFileNames;
    /** 上傳完成後資訊 */
    @Getter
    @Setter
    private StringBuilder uploadMessages;
    /** 上傳失敗訊息 */
    @Getter
    private StringBuilder failMsg;
    /** 檔案上傳限制大小 */
    @Getter
    @Setter
    private Integer fileLimitSize;
    /** 預設Dialog寬度 */
    private final Integer defaultWidth = 1000;
    /** 預設Dialog高度 */
    private final Integer defaultHeight = 500;

    @PostConstruct
    public void init() {
        selectedAttachments = Lists.newArrayList();
        tempFileNames = Lists.newCopyOnWriteArrayList();
        uploadMessages = new StringBuilder();
        failMsg = new StringBuilder();
    }

    public void init(RequireTrace trace) {
        attachService.findAttachsByLazy(trace).forEach(each -> each.setKeyChecked(Boolean.FALSE));
        this.init();
    }

    public List<RequireFbkAttachment> findAttachsByLazy(RequireTrace trace) {
        if (trace == null) {
            return null;
        }
        return attachService.findAttachsByLazy(trace);
    }

    /**
     * 由頁面呼叫
     *
     * @param fileSLimitize
     */
    @Override
    public void onUploadDialogOpen(Integer fileSLimitize) {
        this.fileLimitSize = fileSLimitize;
        tempFileNames.clear();
        this.clearMessages();
    }

    /**
     * 清除上傳過程所產生的訊息.
     */
    private void clearMessages() {
        uploadMessages.delete(0, uploadMessages.length());
        this.clearFailMessages();
    }

    /**
     * 清除上傳過程所產生的訊息(dialog)
     */
    @Override
    public void clearFailMessages() {
        failMsg.delete(0, failMsg.length());
    }

    /**
     * 由頁面呼叫
     *
     * @param event
     */
    @Override
    public void onUploadDialogClose(CloseEvent event) {
    }

    /**
     * 由頁面呼叫，處理一筆檔案上傳的細節
     *
     * @param event
     */
    @Override
    public synchronized void handleFileUpload(FileUploadEvent event) {
        RequireTrace trace = (RequireTrace) event.getComponent().getAttributes().get("attach_depend_entity");
        UploadedFile uploadedFile = event.getFile();
        // 在此步驟先把檔名正規化， 避免某些瀏覽器 (MSIE) 在檔名欄位填寫檔案完整路徑，
        // 造成檔名必定包含特殊字元 (back slash)
        String fileName = FilenameUtils.getName(uploadedFile.getFileName());
        String fileNameExtension = FilenameUtils.getExtension(fileName);
        File tempFile = null;
        try {
            if (attachService.validateByFileNameAndResponse(this.findAttachsByLazy(trace), tempFileNames, fileName, uploadMessages, failMsg)) {
                attachService.createDirectoryIfNotExist();
                tempFile = attachService.saveToTemporaryFile(uploadedFile.getInputstream(), fileNameExtension);
                RequireFbkAttachment attachment = attachService.createEmptyAttachment(fileName, loginBean.getUser(), loginBean.getDep());
                File serverSideFile = attachService.getServerSideFile(attachment);
                tempFile.renameTo(serverSideFile);
                this.findAttachsByLazy(trace).add(attachment);
                if (!Strings.isNullOrEmpty(trace.getSid())) {
                    attachService.linkRelation(attachment, trace, loginBean.getUser());
                } else {
                    this.selectedAttachments.add(attachment);
                }
                attachService.addMessage(fileName, uploadMessages, "OK");
            }
        } catch (IOException ex) {
            log.error("handleFileUpload faild", ex);
            attachService.addFailMessage(fileName, "發生錯誤，" + ex.getLocalizedMessage(), uploadMessages, failMsg);
        } catch (Exception ex) {
            log.error("上傳附件並進行對應發生錯誤", ex);
        } finally {
            FileUtils.deleteQuietly(tempFile);
        }
    }

    /**
     * 由頁面呼叫（「編輯」按鈕），開啟欄位
     *
     * @param attachment
     */
    public void initEdit(RequireFbkAttachment attachment) {
        attachment.setKeyCheckEdit(Boolean.TRUE);
    }

    /**
     * 由頁面呼叫（「存檔」按鈕），直接將資料狀態改成停用
     *
     * @param attachment
     */
    public void saveByEdit(RequireFbkAttachment attachment) {
        attachment.setKeyCheckEdit(Boolean.FALSE);
        try {
            attachService.updateAttach(attachment);
        } catch (Exception e) {
            log.error("更新附加檔案失敗。", e);
        }
    }

    @Override
    public void initRecycle(RequireFbkAttachment attachment) {
        editAttach = attachment;
    }

    @Override
    public void whenDeleteAttachment(Require01MBean r01MBean, RequireTrace trace) {
        this.deleteAttachment(trace);
        r01MBean.getTraceMBean().clear();
        display.update("viewPanelBottomInfoTabId:req_trace_tab_id");
    }

    /**
     * 不直接用editAttach內的require進行參數使用，是因為要考量致畫面更新
     *
     * @param require
     */
    private void deleteAttachment(RequireTrace trace) {
        try {
            this.findAttachsByLazy(trace).remove(editAttach);
            selectedAttachments.remove(editAttach);
            attachService.changeStatusToInActive(this.findAttachsByLazy(trace), editAttach, trace, loginBean.getUser());
        } catch (Exception e) {
            log.error("停用需求資訊回覆附加檔案失敗!!: 追蹤 sid = " + editAttach.getTrace().getSid(), e);
        }
    }

    /**
     * 由頁面呼叫（「下載」按鈕），跟 PrimeFaces 的 fileDownload 標籤一起使用.
     *
     * @return
     * @throws IOException
     */
    public StreamedContent downloadFile() throws IOException {
        if (selectedAttachments.isEmpty()) {
            return null;
        }
        // 下載單一檔案，預設檔名使用該檔案的原始檔名（但是要經過編碼）
        if (selectedAttachments.size() == 1) {
            return pfAttachUtils.createStreamedContent(attachService, selectedAttachments.get(0));
        }
        // 將多檔案壓縮成單一 zip 檔案再輸出
        return pfAttachUtils.createWrappedStreamedContent(attachService, selectedAttachments);
    }

    public String downloadFileContentDisposition() {
        if (selectedAttachments.isEmpty()) {
            return null;
        }
        String encodedFileName = (selectedAttachments.size() == 1)
                ? attachService.encodeFileName(selectedAttachments.get(0).getFileName())
                : AttachmentService.MULTIFILES_ZIP_FILENAME;
        String contentDispositionPattern
                = attachService.isMSIE(Faces.getRequest())
                ? "{0}; filename=\"{1}\""
                : "{0}; filename=\"{1}\"; filename*=UTF-8''{1}";
        return MessageFormat.format(contentDispositionPattern, "attachment", encodedFileName);
    }

    /**
     * 由頁面呼叫，全選
     *
     * @param require
     */
    public void selectedAll(RequireTrace trace) {
        selectedAttachments.clear();
        this.findAttachsByLazy(trace).forEach(each -> {
            each.setKeyChecked(Boolean.TRUE);
            selectedAttachments.add(each);
        });
    }

    /**
     * 異動Checkbox更改selectedAttachments
     *
     * @param require
     */
    public void changeCheckbox(RequireTrace trace) {
        selectedAttachments.clear();
        this.findAttachsByLazy(trace).stream()
                .filter(each -> each.getKeyChecked())
                .forEach(each -> selectedAttachments.add(each));
    }

    /**
     * 讀取附件上傳者及部門
     *
     * @param attachment
     * @return
     */
    public String depAndUserName(RequireFbkAttachment attachment) {
        return attachService.depAndUserName(attachment);
    }

    @Override
    public Integer getDialogWidth() {
        return isRenderedByMsie() ? null : defaultWidth;
    }

    @Override
    public Integer getDialogHeight() {
        return isRenderedByMsie() ? null : defaultHeight;
    }

    /**
     * 判斷瀏覽器的引擎是否為 Microsoft Internet Explorer.
     *
     * @return true: MSIE; false: otherwise.
     */
    private boolean isRenderedByMsie() {
        return attachService.isMSIE(Faces.getRequest()) && !attachService.isChromeFrame(Faces.getRequest());
    }

    public boolean showFunByDept(RequireFbkAttachment attachment) {
        return attachment.getUploadDept().getSid().equals(loginBean.getDep().getSid());
    }

    @Override
    public int findAttachCnt(RequireTrace dependEntity) {
        List<RequireFbkAttachment> attachs = attachService.findAttachs(dependEntity);
        if (attachs == null) {
            return 0;
        }
        return attachs.size();
    }

    @Override
    public void endUploadAction(RequireTrace dependEntity) {
        try {
            if (!Strings.isNullOrEmpty(dependEntity.getSid())) {
                requireService.updateRecordByUpdateReqInfoMemo(dependEntity.getRequire(), loginBean.getUser());
            }
        } catch (Exception e) {
            log.error("上傳附加檔案結束後異動需求單紀錄失敗!!", e);
        }
    }

}
