/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.values;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 前端頁面取得組織相關資訊用
 *
 * @author shaun
 */
@Slf4j
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class OrgBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7234172139802942542L;
    @Autowired
    private LoginBean loginBean;
    @Autowired
    transient private UserService userService;
    @Autowired
    transient private OrganizationService orgService;

    /**
     * AutoComplete 用：使用者名稱
     *
     * @param query
     * @return
     */
    public List<Org> autoCompleteForOrgName(String query) {
        try {
            return orgService.findByStatusAndCompanyAndOrgNameLike(Activation.ACTIVE, loginBean.getComp(), query);
        } catch (Exception ex) {
            log.error("autoCompleteForOrgName >> " + ex.getMessage(), ex);
            MessagesUtils.showError("發生錯誤");
            return Lists.newArrayList();
        }
    }

    /**
     * AutoComplete 用：使用者名稱
     *
     * @param query
     * @return
     */
    public List<Org> autoCompleteForOrgNameByAll(String query) {
        try {
            return orgService.findByStatusAndCompanyAndOrgNameLike(null, loginBean.getComp(), query);
        } catch (Exception ex) {
            log.error("autoCompleteForOrgName >> " + ex.getMessage(), ex);
            MessagesUtils.showError("發生錯誤");
            return Lists.newArrayList();
        }
    }

    /**
     * 取得公司名稱 <BR/>
     * accept class <BR/>
     * Org、String、Integer
     *
     * @param org
     * @return
     */
    public String name(Object obj) {
        if (obj == null) {
            return "";
        }
        String result = orgService.getOrgName(obj);
        if (Strings.isNullOrEmpty(result)) {
            log.error(Faces.getViewId() + " find org name by error class : " + obj.getClass().getSimpleName());
            return "";
        }
        return result;
    }

    /**
     * 從成員取得主要公司名稱 <BR>
     * accept class <BR/>
     * User、String、Integer
     *
     * @param obj
     * @return
     */
    public String nameByUsr(Object obj) {
        if (obj == null) {
            return "";
        }
        String result = userService.getPrimaryOrgNameByUsr(obj);
        if (Strings.isNullOrEmpty(result)) {
            log.error(Faces.getViewId() + " find org name from user by error user class : " + obj.getClass().getSimpleName());
            return "";
        }
        return result;
    }

    public String nameByUserSid(Object userSid) {
        if (userSid == null) {
            return "";
        }
        Integer temp = null;
        try {
            temp = Integer.parseInt(userSid + "");
        } catch (Exception e) {
            log.error("nameByUserSid :[" + userSid + "]");
            return "";
        }

        User user = WkUserCache.getInstance().findBySid(temp);
        if (user == null || user.getPrimaryOrg() == null) {
            return "";
        }

        return WkUserUtils.findNameBySid(temp);
    }

    /**
     * 取得部門名稱
     *
     * @param org
     * @return
     */
    public String findFullOrgNameByUser(User user) {
        if (user == null || user.getPrimaryOrg() == null) {
            return "";
        }
        return WkOrgUtils.prepareBreadcrumbsByDepNameAndMakeup(user.getPrimaryOrg().getSid(), OrgLevel.MINISTERIAL, true, "►");
    }
}
