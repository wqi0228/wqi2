/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require.pt;

import com.cy.tech.request.logic.service.pt.PtShowService;
import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.values.LoginBean;

import java.io.Serializable;
import java.util.List;
import lombok.NoArgsConstructor;

import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 功能按鍵顯示處理
 *
 * @author shaun
 */
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class PtShowMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4494623646526379562L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private PtShowService ptShowService;

    public String findNoticeDepTitle(PtCheck ptCheck) {
        return ptShowService.findNoticeDepTitle(ptCheck);
    }

    public List<String> findNoticeDepTitleByPt(PtCheck ptCheck) {
        return ptShowService.findNoticeDepTitleByOp(ptCheck);
    }

    /**
     * 組合通知人員抬頭訊息
     *
     * @param ptCheck
     * @return
     */
    public String findNoticeMemberTitle(PtCheck ptCheck) {
        return ptShowService.findNoticeMemberTitle(ptCheck);
    }

    /**
     * 組合通知人員抬頭訊息 for overlay
     *
     * @param ptCheck
     * @return
     */
    public List<String> findNoticeMemberTitleByOp(PtCheck ptCheck) {
        return ptShowService.findNoticeMemberTitleByOp(ptCheck);
    }

    /**
     * 關閉編輯
     *
     * @param req
     * @param ptCheck
     * @return
     */
    public boolean disableEdit(Require req, PtCheck ptCheck) {
        return ptShowService.disableEdit(req, ptCheck, loginBean.getUser());
    }

    /**
     * 關閉回覆
     *
     * @param req
     * @param ptCheck
     * @return
     */
    public boolean disableReply(Require req, PtCheck ptCheck) {
        return ptShowService.disableReply(req, ptCheck, loginBean.getUser());
    }

    /**
     * 關閉重做
     *
     * @param req
     * @param ptCheck
     * @return
     */
    public boolean disableRedo(Require req, PtCheck ptCheck) {
        return ptShowService.disableRedo(req, ptCheck, loginBean.getUser());
    }

    /**
     * 關閉測試完成
     *
     * @param req
     * @param ptCheck
     * @return
     */
    public boolean disableFunctionConform(Require req, PtCheck ptCheck) {
        return ptShowService.disableFunctionConform(req, ptCheck, loginBean.getUser());
    }

    /**
     * 顯示簽核資訊查詢(渲染)
     *
     * @param req
     * @param ptCheck
     * @return
     */
    public boolean showSignInfo(Require req, PtCheck ptCheck) {
        return ptShowService.showSignInfo(req, ptCheck);
    }

    public boolean disableUploadFun(Require req, PtCheck ptCheck) {
        return ptShowService.disableUploadFun(req, ptCheck);
    }

    public boolean disableEditNotify(Require req, PtCheck ptCheck) {
        return ptShowService.disableEditNotify(req, ptCheck, loginBean.getUser());
    }
    
    /**
     * disable BPM button when use form sign web
     * @return
     */
    public boolean disableBpmBtn() {
        return Faces.getRequestParameter("mode") != null;
    }
}
