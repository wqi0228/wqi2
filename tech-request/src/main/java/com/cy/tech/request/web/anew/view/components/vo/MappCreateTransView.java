/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.anew.view.components.vo;

import java.io.Serializable;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * 轉ON程式顯示物件
 *
 * @author kasim
 */
@EqualsAndHashCode(of = {"sid"})
public class MappCreateTransView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 473464447052992848L;

    @Getter
    private String sid;

    @Getter
    /** 轉入時間 */
    private String createDate;

    @Getter
    /** 執行單位 */
    private String createDepName;

    @Getter
    /** 執行人員 */
    private String createUserName;

    @Getter
    /** ON程式主題 */
    private String onpgTheme;

    @Getter
    /** ON程式單號 */
    private String onpgNo;

    public MappCreateTransView(String sid, String createDate, String createDepName, String createUserName,
            String onpgTheme, String onpgNo) {
        this.sid = sid;
        this.createDate = createDate;
        this.createDepName = createDepName;
        this.createUserName = createUserName;
        this.onpgTheme = onpgTheme;
        this.onpgNo = onpgNo;
    }

}
