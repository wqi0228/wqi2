/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.vo;

import com.cy.tech.request.logic.vo.query.search.SearchQuery;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.work.common.enums.ReadRecordType;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author kasim
 */
public class Search13QueryVO extends SearchQuery implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 129759631212561079L;

    @Getter
    @Setter
    /** 需求類別 */
    private String selectBigCategory;

    @Getter
    @Setter
    /** 需求製作進度 */
    private RequireStatusType selectRequireStatusType;

    @Getter
    @Setter
    /** 填單單位 */
    private List<String> requireDepts = Lists.newArrayList();

    @Getter
    @Setter
    /** 送測單位 */
    private List<String> noticeDepts = Lists.newArrayList();

    @Getter
    @Setter
    /** 填單人員 */
    private String trCreatedUserName;

    @Getter
    @Setter
    /** 模糊搜尋 */
    private String fuzzyText;

    @Getter
    @Setter
    /** 類別組合(大類) */
    private List<String> bigDataCateSids = Lists.newArrayList();

    @Getter
    @Setter
    /** 類別組合(小類) */
    private List<String> middleDataCateSids = Lists.newArrayList();

    @Getter
    @Setter
    /** 類別組合(小類) */
    private List<String> smallDataCateSids = Lists.newArrayList();

    @Getter
    @Setter
    /** 待閱原因 */
    private List<WaitReadReasonType> readReason = Lists.newArrayList();

    @Getter
    @Setter
    /** 送測狀態 */
    private List<String> testStatus = Lists.newArrayList();

    @Getter
    @Setter
    /** 時間切換的index */
    private int dateTypeIndex;

    @Getter
    @Setter
    private String dateTypeIndexStr;

    @Getter
    @Setter
    /** 填單區間(起) */
    private Date startDate;

    @Getter
    @Setter
    /** 填單區間(訖) */
    private Date endDate;

    @Getter
    @Setter
    /** 排序方式 */
    private String sortType = SortType.CREATE_DESC.name();

    @Getter
    @Setter
    /** 是否閱讀 */
    private ReadRecordType selectReadRecordType;

    @Getter
    @Setter
    /** 立單區間(起) */
    private Date startUpdatedDate;

    @Getter
    @Setter
    /** 立單區間(訖) */
    private Date endUpdatedDate;

    @Getter
    @Setter
    /** 需求單號 */
    private String requireNo;

    

    /** 排序類型 */
    public enum SortType {
        CREATE_DESC("create_dt"),
        UPDATE_DESC("read_update_dt");
        @Getter
        private final String colName;

        SortType(String colName) {
            this.colName = colName;
        }
    }
}
