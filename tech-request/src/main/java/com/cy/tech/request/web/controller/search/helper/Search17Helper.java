/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search.helper;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search17QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.Search17View;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.SpecificPermissionService;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.web.view.to.search.query.SearchQuery17;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * 輔助 Search17MBean
 *
 * @author kasim
 */
@Component
public class Search17Helper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5677983789553816081L;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private OrganizationService orgService;
    @Autowired
    transient private SpecificPermissionService spService;
    @Autowired
    transient private Search17QueryService search00QueryService;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchResultHelper searchResultHelper;

    /**
     * 取得預設查詢部門
     * 
     * @param loginDepSid
     * @param roleSids
     * @return
     */
    public List<String> getDefaultDepSids(Integer loginDepSid, List<Long> roleSids) {
        Set<String> results = Sets.newHashSet(String.valueOf(loginDepSid));
        return Lists.newArrayList(this.addSpDepts(roleSids, this.getDefaultDepSids(loginDepSid, results)));
    }

    /**
     * 取得預設查詢部門
     *
     * @param depSid
     * @param results
     * @return
     */
    private Set<String> getDefaultDepSids(Integer depSid, Set<String> results) {
        orgService.findByParentSid(depSid).forEach(each -> {
            results.add(String.valueOf(each.getSid()));
            this.getDefaultDepSids(each.getSid(), results);
        });
        return results;
    }

    /**
     * 初始化特殊權限部門
     *
     * @param roleSids
     * @param results
     * @return
     */
    private Set<String> addSpDepts(List<Long> roleSids, Set<String> results) {
        List<Org> spDepts = spService.findSpecificDeptsByRoleSidIn(roleSids);
        spDepts.forEach(each -> {
            results.add(String.valueOf(each.getSid()));
        });
        return results;
    }

    /**
     * 查詢
     *
     * @param loginUser
     * @param query
     * @return
     */
    public List<Search17View> search(String compId, User loginUser, SearchQuery17 query) {
        String requireNo = reqularUtils.getRequireNo(compId, query.getFuzzyText());
        StringBuilder builder = new StringBuilder();
        Map<String, Object> parameters = Maps.newHashMap();
        builder.append("SELECT woph.onpg_history_sid, ");
        builder.append("       tr.require_no, ");
        builder.append("       tid.field_content, ");
        builder.append("       tr.urgency, ");
        builder.append("       tr.dep_sid      AS requireDep, ");
        builder.append("       tr.create_dt    AS requireDate, ");
        builder.append("       tr.create_usr   AS requireUser, ");
        builder.append("       ckm.big_category_name, ");
        builder.append("       ckm.middle_category_name, ");
        builder.append("       ckm.small_category_name, ");
        builder.append("       tr.require_status, ");
        builder.append("       wop.create_dt   AS workOnpgCreatedDate, ");
        builder.append("       wop.dep_sid     AS workOnpgCreatedDep, ");
        builder.append("       wop.create_usr  AS workOnpgCreatedUser, ");
        builder.append("       wop.onpg_theme, ");
        builder.append("       wop.onpg_deps, ");
        builder.append("       woph.onpg_no, ");
        builder.append("       woph.behavior_status, ");
        builder.append("       woph.create_dt  AS workOnpgHistoryCreatedDate, ");
        builder.append("       woph.create_usr AS workOnpgHistoryCreatedUser, ");
        builder.append("       woph.reason, ");
        builder.append("       wop.onpg_sid, ");
        // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
        builder.append(this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire());

        builder.append("FROM ( ");

        this.buildWorkOnpgHistoryCondition(query, requireNo, builder, parameters);
        this.buildWorkOnpgCondition(query, requireNo, builder, parameters);
        this.buildRequireCondition(query, requireNo, builder, parameters);
        this.buildRequireIndexDictionaryCondition(builder);
        this.buildCategoryKeyMappingCondition(query, requireNo, builder);
        // 檢查項目 (系統別)
        // 後方需對 主單 sid 做 group by
        builder.append(this.searchConditionSqlHelper.prepareSubFormCommonJoin(
                SecurityFacade.getUserSid(),
                FormType.WORKONPG,
                "wop"));

        builder.append("WHERE woph.onpg_history_sid IS NOT NULL ").append("\n");
        builder.append(" GROUP BY woph.onpg_history_sid ");
        builder.append("ORDER BY tr.require_no,wop.onpg_no,woph.create_dt DESC").append("\n");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.WORK_ON_HISTORY, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.WORK_ON_HISTORY, SecurityFacade.getUserSid());

        // 查詢
        List<Search17View> resultList = search00QueryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // ====================================
        // 後續處理
        // ====================================
        if (WkStringUtils.notEmpty(resultList)) {
            // 後續處理-開始
            usageRecord.afterProcessStart();
            // 查詢條件過濾：系統別
            resultList = resultList.stream()
                    .filter(each -> this.searchResultHelper.filterCheckItems(
                            each.getCheckItemTypes(),
                            query.getCheckItemTypes(),
                            false,
                            null))
                    .collect(Collectors.toList());

            // 後續處理-結束
            usageRecord.afterProcessEnd();
        }

        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return resultList;
    }

    /**
     * 組合sql 語法
     *
     * @param query
     * @param requireNo
     * @param builder
     * @param parameters
     */
    private void buildWorkOnpgHistoryCondition(SearchQuery17 query, String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("SELECT * FROM work_onpg_history woph ").append("\n");
        builder.append("    WHERE woph.visiable = 1 ").append("\n");
        // 技術需求單類型
        builder.append("    AND woph.onpg_source_type='TECH_REQUEST' ").append("\n");
        if (Strings.isNullOrEmpty(requireNo)) {
            // 填單區間
            if (query.getStartDate() != null && query.getEndDate() != null) {
                builder.append("    AND woph.create_dt BETWEEN '")
                        .append(helper.transStrByStartDate(query.getStartDate())).append("' AND '")
                        .append(helper.transStrByEndDate(query.getEndDate())).append("' ").append("\n");
            } else if (query.getStartDate() != null) {
                builder.append("    AND woph.create_dt >= '")
                        .append(helper.transStrByStartDate(query.getStartDate())).append("' ").append("\n");
            } else if (query.getEndDate() != null) {
                builder.append("    AND woph.create_dt <= '")
                        .append(helper.transStrByEndDate(query.getEndDate())).append("' ").append("\n");
            }
            // ON程式單號
            if (!Strings.isNullOrEmpty(query.getOnpgNo())) {
                String onpgNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(query.getOnpgNo()) + "%";
                builder.append("    AND woph.onpg_no LIKE :onpgNo ").append("\n");
                parameters.put("onpgNo", onpgNo);
            }
            // ON程式狀態
            if (query.getOnpgStatus() != null && !query.getOnpgStatus().isEmpty()) {
                String onpgStatus = query.getOnpgStatus().stream()
                        .collect(Collectors.joining("','", "'", "'"));
                builder.append("    AND woph.behavior_status IN (").append(onpgStatus).append(") ").append("\n");
            }
        }
        builder.append(") AS woph ").append("\n");
    }

    /**
     * 組合sql 語法
     *
     * @param query
     * @param requireNo
     * @param builder
     * @param parameters
     */
    private void buildWorkOnpgCondition(SearchQuery17 query, String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN ( ").append("\n");
        builder.append("    SELECT * FROM work_onpg wop ").append("\n");
        // 技術需求單類型
        builder.append("        WHERE wop.onpg_source_type='TECH_REQUEST' ").append("\n");
        if (Strings.isNullOrEmpty(requireNo)) {
            // 填單單位
            if (query.getRequireDepts() != null && !query.getRequireDepts().isEmpty()) {
                String depSids = query.getRequireDepts().stream()
                        .collect(Collectors.joining(","));
                builder.append("        AND wop.dep_sid IN (").append(depSids).append(") ").append("\n");
            }
            // 填送測單的單位 或 收到送測單的單位
            if (query.getRequireDepts() != null && !query.getRequireDepts().isEmpty()
                    && query.getNoticeDepts() != null && !query.getNoticeDepts().isEmpty()) {
                String depSids = query.getRequireDepts().stream()
                        .collect(Collectors.joining(","));
                String depSid = "'" + query.getNoticeDepts().stream()
                        .map(each -> "\"" + each + "\"")
                        .collect(Collectors.joining("|")) + "'";
                builder.append("        AND ( ").append("\n");
                builder.append("            wop.dep_sid IN (").append(depSids).append(")").append("\n");
                builder.append("            OR ").append("\n");
                builder.append("            wop.onpg_deps REGEXP ").append(depSid).append(" ").append("\n");
                builder.append("        ) ").append("\n");
            } else if (query.getRequireDepts() != null && !query.getRequireDepts().isEmpty()) {
                String depSids = query.getRequireDepts().stream()
                        .collect(Collectors.joining(","));
                builder.append("        AND wop.dep_sid IN (").append(depSids).append(") ").append("\n");
            } else if (query.getNoticeDepts() != null && !query.getNoticeDepts().isEmpty()) {
                String depSid = "'" + query.getNoticeDepts().stream()
                        .map(each -> "\"" + each + "\"")
                        .collect(Collectors.joining("|")) + "'";
                builder.append("        AND wop.onpg_deps REGEXP ").append(depSid).append(" ").append("\n");
            }
            // 模糊搜尋
            if (!Strings.isNullOrEmpty(query.getFuzzyText())) {
                String text = "%" + reqularUtils.replaceIllegalSqlLikeStr(query.getFuzzyText()) + "%";
                builder.append("        AND ( ").append("\n");
                builder.append("            wop.onpg_content LIKE :text ").append("\n");
                builder.append("            OR ").append("\n");
                builder.append("            wop.onpg_theme LIKE :text ").append("\n");
                builder.append("            OR ").append("\n");
                builder.append("            wop.onpg_note LIKE :text ").append("\n");
                builder.append("            OR").append("\n");
                builder.append("            wop.onpg_sid IN ( ").append("\n");
                builder.append("                SELECT ocr.onpg_sid FROM work_onpg_check_record ocr ").append("\n");
                builder.append("                    WHERE ocr.record_content LIKE :text ").append("\n");
                builder.append("            ) ").append("\n");
                builder.append("            OR ").append("\n");
                builder.append("            wop.onpg_sid IN ( ").append("\n");
                builder.append("                SELECT ocrr.onpg_sid FROM work_onpg_check_record_reply ocrr ").append("\n");
                builder.append("                    WHERE ocrr.reply_content LIKE :text ").append("\n");
                builder.append("            ) ").append("\n");
                builder.append("            OR ").append("\n");
                builder.append("            wop.onpg_sid IN ( ").append("\n");
                builder.append("                SELECT ocm.onpg_sid FROM work_onpg_check_memo ocm").append("\n");
                builder.append("                    WHERE ocm.memo_content LIKE :text ").append("\n");
                builder.append("            ) ").append("\n");
                builder.append("        ) ").append("\n");

                parameters.put("text", text);
            }
        }
        builder.append(") AS wop ON woph.onpg_sid=wop.onpg_sid ").append("\n");
    }

    /**
     * 組合sql 語法
     *
     * @param query
     * @param requireNo
     * @param builder
     * @param parameters
     */
    private void buildRequireCondition(SearchQuery17 query, String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (").append("\n");
        builder.append("    SELECT * FROM tr_require tr ").append("\n");
        builder.append("        WHERE 1=1 ").append("\n");
        // 需求單號
        if (!Strings.isNullOrEmpty(requireNo)) {
            builder.append("        AND tr.require_no = '").append(requireNo).append("' ").append("\n");
        } else {
            // 需求單-立單日期區間
            if (query.getStartUpdatedDate() != null && query.getEndUpdatedDate() != null) {
                builder.append("        AND tr.create_dt BETWEEN '")
                        .append(helper.transStrByStartDate(query.getStartUpdatedDate())).append("' AND '")
                        .append(helper.transStrByEndDate(query.getEndUpdatedDate())).append("' ").append("\n");
            } else if (query.getStartUpdatedDate() != null) {
                builder.append("        AND tr.create_dt >= '")
                        .append(helper.transStrByStartDate(query.getStartUpdatedDate())).append("' ").append("\n");
            } else if (query.getEndUpdatedDate() != null) {
                builder.append("        AND tr.create_dt <= '")
                        .append(helper.transStrByEndDate(query.getEndUpdatedDate())).append("' ").append("\n");
            }
            // 需求單號
            if (!Strings.isNullOrEmpty(query.getRequireNo())) {
                String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(query.getRequireNo()) + "%";
                builder.append("        AND tr.require_no LIKE :textNo ").append("\n");
                parameters.put("textNo", textNo);
            }
        }

        builder.append(") AS tr ON tr.require_no=woph.onpg_source_no ").append("\n");
    }

    /**
     * 組合sql 語法
     *
     * @param builder
     */
    private void buildRequireIndexDictionaryCondition(StringBuilder builder) {
        builder.append("INNER JOIN ( ").append("\n");
        builder.append("    SELECT * FROM tr_index_dictionary tid ").append("\n");
        builder.append("        WHERE tid.field_name='主題' ").append("\n");
        builder.append(") AS tid ON tr.require_sid=tid.require_sid ").append("\n");
    }

    /**
     * 組合sql 語法
     *
     * @param query
     * @param requireNo
     * @param builder
     */
    private void buildCategoryKeyMappingCondition(SearchQuery17 query, String requireNo, StringBuilder builder) {
        builder.append("INNER JOIN ( ").append("\n");
        builder.append("    SELECT * FROM tr_category_key_mapping ckm ").append("\n");
        builder.append("        WHERE 1=1 ").append("\n");
        if (Strings.isNullOrEmpty(requireNo)) {
            // 需求類別
            if (!Strings.isNullOrEmpty(query.getSelectBigCategorySid())
                    && query.getBigDataCateSids().isEmpty()) {
                builder.append("        AND ckm.big_category_sid = '")
                        .append(query.getSelectBigCategorySid()).append("' ").append("\n");
            }
            // 類別組合
            if (!query.getBigDataCateSids().isEmpty()
                    || !query.getMiddleDataCateSids().isEmpty()
                    || !query.getSmallDataCateSids().isEmpty()) {
                String bigs = "'isEmpty'";
                String middles = "'isEmpty'";
                String smalls = "'isEmpty'";
                if (!query.getBigDataCateSids().isEmpty()) {
                    bigs = query.getBigDataCateSids().stream()
                            .collect(Collectors.joining("','", "'", "'"));
                }
                if (!query.getMiddleDataCateSids().isEmpty()) {
                    middles = query.getMiddleDataCateSids().stream()
                            .collect(Collectors.joining("','", "'", "'"));
                }
                if (!query.getSmallDataCateSids().isEmpty()) {
                    smalls = query.getSmallDataCateSids().stream()
                            .collect(Collectors.joining("','", "'", "'"));
                }
                if (!Strings.isNullOrEmpty(query.getSelectBigCategorySid())) {
                    if ("'isEmpty'".equals(bigs)) {
                        bigs = "'" + query.getSelectBigCategorySid() + "'";
                    } else {
                        bigs = bigs + ",'" + query.getSelectBigCategorySid() + "'";
                    }
                }
                builder.append("        AND( ").append("\n");
                builder.append("            ckm.big_category_sid IN (").append(bigs).append(") ").append("\n");
                builder.append("            OR ").append("\n");
                builder.append("            ckm.middle_category_sid IN (").append(middles).append(") ").append("\n");
                builder.append("            OR ").append("\n");
                builder.append("            ckm.small_category_sid IN (").append(smalls).append(")").append("\n");
                builder.append("        ) ").append("\n");
            }
        }
        builder.append(") AS ckm ON tr.mapping_sid=ckm.key_sid ").append("\n");
    }

}
