/**
 * 
 */
package com.cy.tech.request.web.controller.setting.setting10;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;

import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.setting.onetimetrns.OneTimeTrnsForRequireDepCommonService;
import com.cy.tech.request.logic.service.setting.onetimetrns.OneTimeTrnsForRequireDepOwnerService;
import com.cy.tech.request.logic.service.setting.onetimetrns.OneTimeTrnsForRequireDepRebuildService;
import com.cy.tech.request.logic.service.setting.onetimetrns.OneTimeTrnsForRequireDepRestToDefaultService;
import com.cy.tech.request.logic.service.setting.onetimetrns.to.RequireDepRebuildTo;
import com.cy.tech.request.logic.service.setting.onetimetrns.to.RequireReAssignTo;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.HikariPoolMXBean;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Slf4j
@Controller
public class Set10OneTimeTransMBean implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 5450261924447933829L;

    // ========================================================================
    // 服務區
    // ========================================================================
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private OneTimeTrnsForRequireDepRebuildService requireDepRebuildService;
    @Autowired
    private OneTimeTrnsForRequireDepOwnerService requireDepOwnerService;
    @Autowired
    private OneTimeTrnsForRequireDepCommonService requireDepCommonService;
    @Autowired
    private OneTimeTrnsForRequireDepRestToDefaultService requireDepRestToDefaultService;
    @Autowired
    private RequireService requireService;

    @Getter
    @Setter
    private String requireDepRebuildServiceTargetRequireNo = "";

    @Getter
    @Setter
    private String requireDepRebuildRunCount = "1";

    @Getter
    @Setter
    private String requireDepOwnerServiceTargetRequireNo = "";

    @Getter
    @Setter
    private String requireDepRestToDefaultTargetRequireNo = "";

    // ========================================================================
    // 方法區
    // ========================================================================

    /**
     * 需求確認單位重建
     * 
     * @param isOnlyClose 是否僅轉結案單據
     */
    public void processRequireDepRebuild(boolean isOnlyClose) {

        this.requireDepRebuildServiceTargetRequireNo = WkStringUtils.safeTrim(
                this.requireDepRebuildServiceTargetRequireNo);
        this.requireDepRebuildRunCount = WkStringUtils.safeTrim(
                this.requireDepRebuildRunCount);

        if (WkStringUtils.notEmpty(this.requireDepRebuildServiceTargetRequireNo)) {
            if (this.requireService.findByReqNo(this.requireDepRebuildServiceTargetRequireNo) == null) {
                MessagesUtils.showWarn("指定單號：[" + this.requireDepRebuildServiceTargetRequireNo + "] 不存在");
                return;
            }
        }

        int runCount = 200;
        if (WkStringUtils.notEmpty(this.requireDepRebuildRunCount)) {
            if (!WkStringUtils.isNumber(requireDepRebuildRunCount)) {
                MessagesUtils.showWarn("執行次數：[" + this.requireDepRebuildServiceTargetRequireNo + "]輸入錯誤");
                return;
            }
            runCount = Integer.parseInt(this.requireDepRebuildRunCount);
        }

        Map<String, Integer> countMap = Maps.newHashMap();
        List<String> errorMessages = Lists.newArrayList();

        if (WkStringUtils.notEmpty(this.requireDepRebuildServiceTargetRequireNo)) {
            this.processRequireDepRebuild(
                    this.requireDepRebuildServiceTargetRequireNo,
                    isOnlyClose,
                    countMap,
                    errorMessages);
        } else {
            for (int i = 0; i < runCount; i++) {

                boolean isContinue = this.processRequireDepRebuild(
                        this.requireDepRebuildServiceTargetRequireNo,
                        isOnlyClose,
                        countMap,
                        errorMessages);

                WkCommonUtils.memonyMonitor();

                if (!isContinue) {
                    break;
                }
            }
        }

        if (countMap.get("TOTAL") == 0) {
            MessagesUtils.showInfo("已無待轉檔資料");
        }

        // ====================================
        // 統計訊息
        // ====================================
        String message = "<br/>" + String.format("本次處理:[%s]筆，成功[%s]筆，失敗[%s]",
                countMap.get("TOTAL"),
                countMap.get("SUCCESS"),
                countMap.get("TOTAL") - countMap.get("SUCCESS"));

        if (WkStringUtils.notEmpty(errorMessages)) {
            message += "<br/>" + errorMessages.stream().collect(Collectors.joining("<br/>"));
        }

        MessagesUtils.showInfo(message);
        log.info(WkJsoupUtils.getInstance().htmlToText(message));

    }

    public boolean processRequireDepRebuild(
            String targetRequireNo,
            boolean isOnlyClose,
            Map<String, Integer> countMap,
            List<String> errorMessages) {

        if (countMap.get("TOTAL") == null) {
            countMap.put("TOTAL", 0);
        }
        if (countMap.get("SUCCESS") == null) {
            countMap.put("SUCCESS", 0);
        }

        long startTime = System.currentTimeMillis();

        HikariDataSource hikariDs = (HikariDataSource) WorkSpringContextHolder.getBean("dataSource");
        HikariPoolMXBean poolBean = hikariDs.getHikariPoolMXBean();
        poolBean.softEvictConnections();

        // ====================================
        // 查詢未轉檔
        // ====================================
        Map<String, String> requireNoBySid;

        requireNoBySid = this.requireDepRebuildService.findWaitTransRequireSids(
                targetRequireNo,
                isOnlyClose);

        if (WkStringUtils.isEmpty(requireNoBySid)) {
            return false;
        }

        countMap.put("TOTAL", countMap.get("TOTAL") + requireNoBySid.size());

        Set<String> requireSids = requireNoBySid.keySet();

        // ====================================
        // 查詢分派、通知單位
        // ====================================
        Map<String, Set<Integer>> assignDepSidMapByRequireSid = this.requireDepCommonService.findAssignNoticeDepSids(
                requireSids, AssignSendType.ASSIGN);

        Map<String, Set<Integer>> noticeDepSidMapByRequireSid = this.requireDepCommonService.findAssignNoticeDepSids(
                requireSids, AssignSendType.SEND);

        // ====================================
        // 查詢確認單位檔
        // ====================================
        Map<String, List<RequireDepRebuildTo>> requireDepRebuildTosMapByRequireSid = this.requireDepCommonService.findRequireConfirmDeps(requireSids);

        // ====================================
        // 逐筆處理
        // ====================================
        int index = 0;
        for (String requireSid : requireSids) {

            index++;

            if (!requireDepRebuildTosMapByRequireSid.containsKey(requireSid)) {
                String errorMessage = String.format(
                        "單據無確認單位檔 requireSid:[%s], requireNo:[%s]",
                        requireSid,
                        requireNoBySid.get(requireSid));
                log.debug(errorMessage);
                errorMessages.add(errorMessage);
                continue;
            }

            if (!assignDepSidMapByRequireSid.containsKey(requireSid)) {
                String errorMessage = String.format(
                        "單據無分派單位資料 requireSid:[%s], requireNo:[%s]",
                        requireSid,
                        requireNoBySid.get(requireSid));
                log.debug(errorMessage);
                errorMessages.add(errorMessage);
                continue;
            }

            try {
                this.requireDepRebuildService.process(
                        requireSid,
                        requireNoBySid.get(requireSid),
                        assignDepSidMapByRequireSid.get(requireSid),
                        noticeDepSidMapByRequireSid.get(requireSid),
                        requireDepRebuildTosMapByRequireSid.get(requireSid));

                countMap.put("SUCCESS", countMap.get("SUCCESS") + 1);
            } catch (UserMessageException e) {
                String errorMessage = String.format(
                        e.getMessage() + "，requireSid:[%s], requireNo:[%s]",
                        requireSid,
                        requireNoBySid.get(requireSid));
                log.info(errorMessage);
                errorMessages.add(errorMessage);
            } catch (Exception e) {
                String errorMessage = String.format(
                        e.getMessage() + "，requireSid:[%s], requireNo:[%s]",
                        requireSid,
                        requireNoBySid.get(requireSid));
                log.error(errorMessage, e);
                errorMessages.add(errorMessage);
            }

            if (index % 50 == 0) {
                log.debug("[{}]", index);
            }

        }
        log.info(WkCommonUtils.prepareCostMessage(startTime, "部分轉檔結束![" + requireNoBySid.size() + "]筆"));
        return true;
    }

    // ========================================================================
    // 需求確認單位轉檔 (負責人單位異動轉檔)
    // ========================================================================

    /**
     * 需求確認單位轉檔 (負責人單位異動轉檔)
     */
    public void processTrnsRequireDepByOwner() {
        requireDepOwnerServiceTargetRequireNo = WkStringUtils.safeTrim(requireDepOwnerServiceTargetRequireNo);

        if (WkStringUtils.notEmpty(requireDepOwnerServiceTargetRequireNo)) {
            if (this.requireService.findByReqNo(requireDepOwnerServiceTargetRequireNo) == null) {
                MessagesUtils.showWarn("指定單號：[" + requireDepOwnerServiceTargetRequireNo + "] 不存在");
                return;
            }
        }

        long startTime = System.currentTimeMillis();

        HikariDataSource hikariDs = (HikariDataSource) WorkSpringContextHolder.getBean("dataSource");
        HikariPoolMXBean poolBean = hikariDs.getHikariPoolMXBean();
        poolBean.softEvictConnections();

        // ====================================
        // 查詢未轉檔
        // ====================================
        Map<String, String> requireNoBySid = this.requireDepOwnerService.findWaitTransRequireSids(
                this.requireDepOwnerServiceTargetRequireNo);

        if (WkStringUtils.isEmpty(requireNoBySid)) {
            MessagesUtils.showInfo("找不到狀態為進行中的單據");
            return;
        }

        Set<String> requireSids = requireNoBySid.keySet();

        // ====================================
        // 查詢分派、通知單位
        // ====================================
        Map<String, Set<Integer>> assignDepSidMapByRequireSid = this.requireDepCommonService.findAssignNoticeDepSids(
                requireSids, AssignSendType.ASSIGN);

        Map<String, Set<Integer>> noticeDepSidMapByRequireSid = this.requireDepCommonService.findAssignNoticeDepSids(
                requireSids, AssignSendType.SEND);

        // ====================================
        // 查詢確認單位檔
        // ====================================
        Map<String, List<RequireDepRebuildTo>> requireDepRebuildTosMapByRequireSid = this.requireDepCommonService.findRequireConfirmDeps(requireSids);

        // ====================================
        // 執行
        // ====================================

        List<String> errorMessages = Lists.newArrayList();
        Integer success = 0;
        for (String requireSid : requireSids) {
            try {
                this.requireDepOwnerService.process(
                        requireSid,
                        requireNoBySid.get(requireSid),
                        assignDepSidMapByRequireSid.get(requireSid),
                        noticeDepSidMapByRequireSid.get(requireSid),
                        requireDepRebuildTosMapByRequireSid.get(requireSid));

                success++;
            } catch (UserMessageException e) {
                String errorMessage = String.format(
                        e.getMessage() + "，requireSid:[%s], requireNo:[%s]",
                        requireSid,
                        requireNoBySid.get(requireSid));
                log.info(errorMessage);
                errorMessages.add(requireSid);
            } catch (Exception e) {
                String errorMessage = String.format(
                        e.getMessage() + "，requireSid:[%s], requireNo:[%s]",
                        requireSid,
                        requireNoBySid.get(requireSid));
                log.error(errorMessage, e);
                errorMessages.add(requireSid);
            }
        }

        // ====================================
        // 統計訊息
        // ====================================
        String message = "<br/>" + String.format("本次處理:[%s]筆，成功[%s]筆，失敗[%s]",
                requireSids.size(),
                success,
                requireSids.size() - success);

        if (WkStringUtils.notEmpty(errorMessages)) {
            message += "<br/>" + errorMessages.stream().collect(Collectors.joining("<br/>"));
        }

        MessagesUtils.showInfo(message);

        log.info(WkCommonUtils.prepareCostMessage(startTime, WkJsoupUtils.getInstance().htmlToText(message)));
    }

    // ========================================================================
    // 加預設單位
    // ========================================================================
    public void processRequireDepRestToDefault() {

        // ====================================
        // 檢查指定需求單
        // ===================================
        this.requireDepRestToDefaultTargetRequireNo = WkStringUtils.safeTrim(this.requireDepRestToDefaultTargetRequireNo);

        if (WkStringUtils.notEmpty(this.requireDepRestToDefaultTargetRequireNo)) {
            if (this.requireService.findByReqNo(this.requireDepRestToDefaultTargetRequireNo) == null) {
                MessagesUtils.showWarn("指定單號：[" + this.requireDepRestToDefaultTargetRequireNo + "] 不存在");
                return;
            }
        }
        // ====================================
        // 查詢
        // ====================================
        long startTime = System.currentTimeMillis();

        List<RequireReAssignTo> requireReAssignTos = this.requireDepRestToDefaultService.findWaitTransRequireSids(
                this.requireDepRestToDefaultTargetRequireNo);

        if (WkStringUtils.isEmpty(requireReAssignTos)) {
            MessagesUtils.showInfo("找不到需要轉檔的單據");
            return;
        }

        // ====================================
        // 執行
        // ====================================
        List<String> errorMessages = Lists.newArrayList();
        Integer success = 0;
        for (RequireReAssignTo requireReAssignTo : requireReAssignTos) {
            try {
                this.requireDepRestToDefaultService.process(requireReAssignTo);
                success++;

            } catch (Exception e) {
                String errorMessage = String.format(
                        e.getMessage() + "，requireSid:[%s], requireNo:[%s]",
                        requireReAssignTo.getRequireSid(),
                        requireReAssignTo.getRequireNo());
                log.error(errorMessage, e);
                errorMessages.add(errorMessage);
            }
        }

        // ====================================
        // 統計訊息
        // ====================================
        String message = "<br/>" + String.format("本次處理:[%s]筆，成功[%s]筆，失敗[%s]",
                requireReAssignTos.size(),
                success,
                requireReAssignTos.size() - success);

        if (WkStringUtils.notEmpty(errorMessages)) {
            message += "<br/>" + errorMessages.stream().collect(Collectors.joining("<br/>"));
        }

        MessagesUtils.showInfo(message);

        log.info(WkCommonUtils.prepareCostMessage(startTime, WkJsoupUtils.getInstance().htmlToText(message)));

    }

}
