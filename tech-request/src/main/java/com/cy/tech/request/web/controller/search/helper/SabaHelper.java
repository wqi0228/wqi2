package com.cy.tech.request.web.controller.search.helper;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.RequireTraceService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.repository.result.SabaQueryResult;
import com.cy.tech.request.vo.enums.ExtraSettingType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireTrace;
import com.cy.tech.request.vo.require.setting.RequireExtraSetting;
import com.cy.tech.request.web.controller.view.vo.SabaQueryVO;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkJsonUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class SabaHelper {

    @Autowired
    private RequireService requireService;
    @Autowired
    private RequireTraceService traceService;
    @Autowired
    private WkJsonUtils jsonUtils;
    @Autowired
    private URLService urlService;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private UserService userService;

    public List<SabaQueryResult> findSabaByCondition(SabaQueryVO queryVO) {
        Date startDate = queryVO.getDateIntervalVO().getStartDate();
        Date endDate = null;
        if (queryVO.getDateIntervalVO().getEndDate() != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(queryVO.getDateIntervalVO().getEndDate().getTime());
            cal.add(Calendar.DATE, 1);
            endDate = cal.getTime();
        }

        String searchText = queryVO.getSearchText() == null ? null : "%" + reqularUtils.replaceIllegalSqlLikeStr(queryVO.getSearchText()) + "%";
        String categorySid = queryVO.getCatetory() == null ? null : queryVO.getCatetory().getSid();
        String ncsDone = queryVO.getIsNcsDone() == null ? null : queryVO.getIsNcsDone() ? "Y" : "N";
        //類別組合參數
        Boolean hasL1Sids = null, hasL2Sids = null, hasL3Sids = null;
        List<String> bCategorySids = null, mCategorySids = null, sCategorySids = null;
        if (!queryVO.getCategoryCombineVO().getBigDataCateSids().isEmpty()) {
            hasL1Sids = false;
            bCategorySids = queryVO.getCategoryCombineVO().getBigDataCateSids();
        }
        if (!queryVO.getCategoryCombineVO().getMiddleDataCateSids().isEmpty()) {
            hasL2Sids = false;
            mCategorySids = queryVO.getCategoryCombineVO().getMiddleDataCateSids();
        }
        if (!queryVO.getCategoryCombineVO().getSmallDataCateSids().isEmpty()) {
            hasL3Sids = false;
            sCategorySids = queryVO.getCategoryCombineVO().getSmallDataCateSids();
        }

        //進階查詢參數
        Date startUpdatedDate = queryVO.getAdvancedVO().getStartUpdatedDate();
        Date endUpdatedDate = null;
        if (queryVO.getAdvancedVO().getEndUpdatedDate() != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(queryVO.getAdvancedVO().getEndUpdatedDate().getTime());
            cal.add(Calendar.DATE, 1);
            endUpdatedDate = cal.getTime();
        }
        String requireNo = queryVO.getAdvancedVO().getRequireNo();
        requireNo = Strings.isNullOrEmpty(requireNo) ? null : requireNo;
        Boolean hasUrgency = null, hasDepts = null;
        List<Integer> urgencyList = null;
        List<String> forwardDepts = null;
        if (!queryVO.getAdvancedVO().getUrgencyList().isEmpty()) {
            urgencyList = Lists.newArrayList();
            hasUrgency = false;
            for (String str : queryVO.getAdvancedVO().getUrgencyList()) {
                switch (UrgencyType.valueOf(str)) {
                    case GENERAL:
                        urgencyList.add(0);
                        break;
                    case URGENT:
                        urgencyList.add(1);
                        break;
                    default:
                        break;
                }
            }
        }
        if (!queryVO.getAdvancedVO().getForwardDepts().isEmpty()) {
            hasDepts = false;
            forwardDepts = queryVO.getAdvancedVO().getForwardDepts();
        }

        List<SabaQueryResult> resultList = requireService.findSabaByCondition(startDate, endDate, searchText, categorySid, ncsDone, hasL1Sids,
                  hasL2Sids, hasL3Sids, bCategorySids, mCategorySids, sCategorySids, startUpdatedDate, endUpdatedDate,
                  hasUrgency, urgencyList, requireNo, hasDepts, forwardDepts);

        log.debug("resultList records:{}", resultList.size());
        return convert(resultList, queryVO.getUser());
    }

    public void saveRequieTrace(String sid, User loginUser) {
        Require require = requireService.findByReqSid(sid);
        for (RequireExtraSetting setting : require.getExtraSettings()) {
            if (setting.getCode().equals(ExtraSettingType.NCS_CONFIRM.getCode())) {
                setting.setValue("Y");
                break;
            }
        };

        RequireTrace entity = new RequireTrace();
        entity.setRequire(require);
        entity.setRequireNo(require.getRequireNo());
        entity.setRequireTraceType(RequireTraceType.NCS_CHECK_OK);
        entity.setRequireTraceContent(loginUser.getName() + "進行NCS執行完成");
        entity.setRequireTraceContentCss(loginUser.getName() + "進行NCS執行完成");
        entity.setStatus(Activation.ACTIVE);
        entity.setCreatedDate(new Date());
        entity.setCreatedUser(loginUser);

        requireService.save(require);
        traceService.save(entity);
    }

    private List<SabaQueryResult> convert(List<SabaQueryResult> resultList, User loginUser) {
        resultList.forEach(model -> {
            try {
                model.setUserName(userService.getUserName(model.getUserSid()));
                model.setDeptName(WkOrgCache.getInstance().findNameBySid(model.getDepSid()));
                //theme
                if (!Strings.isNullOrEmpty(model.getTheme())) {
                    model.setTheme(String.join(" ", jsonUtils.fromJsonToList(model.getTheme(), String.class)));
                }
                //open windows link
                model.setLocalUrlLink(urlService.createLoacalURLLink(
                          URLService.URLServiceAttr.URL_ATTR_M,
                          urlService.createSimpleUrlTo(loginUser.getSid(), model.getRequireNo(), 1)));
                //status code
                model.setStatus(RequireStatusType.valueOf(model.getStatus()).getValue());
                //finish code
                String code = model.getFinishCode();
                if ("Y".equals(code) || "y".equals(code)) {
                    model.setFinishCode("已完成");
                } else if ("n".equals(code)) {
                    model.setFinishCode("終止");
                } else if ("N".equals(code)) {
                    model.setFinishCode("未完成");
                }

            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        });

        return resultList;
    }
}
