/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.attachment;

import com.cy.tech.request.logic.vo.AttachmentVO;
import com.cy.tech.request.vo.enums.OthSetAttachmentBehavior;
import com.cy.tech.request.web.attachment.util.AttachmentCondition;
import com.cy.tech.request.web.attachment.util.AttachmentUploadCompant;
import com.cy.tech.request.web.controller.logic.component.OthSetSettingAttachmentLogicComponents;
import com.cy.tech.request.web.listener.ReloadAttCallBack;
import java.util.List;

/**
 *
 * @author brain0925_liao
 */
public class TrOsUploadAttachCompant extends AttachmentUploadCompant {

    /**
     * 
     */
    private static final long serialVersionUID = 7650984199942871308L;

    private String os_sid;

    private String os_no;

    private String require_sid;

    private String require_no;

    private String os_history_sid;

    private Integer lgoinUserSid;

    private Integer loginUserDepSid;

    private final ReloadAttCallBack reloadAttCallBack;

    public TrOsUploadAttachCompant(ReloadAttCallBack reloadAttCallBack) {
        this.reloadAttCallBack = reloadAttCallBack;
    }

    public void loadAttData(String os_sid, String os_no, String require_sid,
            String require_no, String os_history_sid, Integer lgoinUserSid, Integer loginUserDepSid) {
        this.os_sid = os_sid;
        this.os_no = os_no;
        this.require_sid = require_sid;
        this.require_no = require_no;
        this.os_history_sid = os_history_sid;
        this.lgoinUserSid = lgoinUserSid;
        this.loginUserDepSid = loginUserDepSid;
        AttachmentCondition attachmentCondition = new AttachmentCondition(os_sid, "", true);
        super.loadAttachment(attachmentCondition);
    }

    @Override
    protected List<AttachmentVO> getPersistedAttachments(String os_sid) {
        return OthSetSettingAttachmentLogicComponents.getInstance().getAttachmentVOByOSSid(os_sid);

    }
    
    @Override
    public void finishUpload() {

    }

    @Override
    public Integer getFileSizeLimit() {
        return 50;
    }

    @Override
    public String getDirectoryName() {
        return "tech-request";
    }

    @Override
    protected AttachmentVO createAndPersistAttachment(String fileName, AttachmentCondition attachmentCondition) {

        if (attachmentCondition.isAutoMappingEntity()) {
            AttachmentVO result = OthSetSettingAttachmentLogicComponents.getInstance().createAttachment(fileName,
                    os_sid, os_no, require_sid, require_no, os_history_sid, lgoinUserSid, loginUserDepSid, OthSetAttachmentBehavior.FIRST_FILE);
            reloadAttCallBack.reloadUploadAtt(os_sid);
            return result;
        } else {
            AttachmentVO result = OthSetSettingAttachmentLogicComponents.getInstance().createAttachment(fileName,
                    "", "", "", "", "", lgoinUserSid, loginUserDepSid, OthSetAttachmentBehavior.FIRST_FILE);
            reloadAttCallBack.reloadUploadAtt(os_sid);
            return result;
        }

    }

}
