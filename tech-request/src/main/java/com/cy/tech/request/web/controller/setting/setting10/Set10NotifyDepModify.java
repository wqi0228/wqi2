package com.cy.tech.request.web.controller.setting.setting10;

import com.cy.tech.request.web.controller.setting.Setting10MBean;
import com.cy.tech.request.web.logic.helper.TransNotifyDepLogicHelper;
import com.cy.tech.request.web.logic.helper.TransNotifyTo;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 通知單位轉換
 *
 * @author kasim
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope("view")
public class Set10NotifyDepModify implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1975035435644965793L;
    @Autowired
    private TransNotifyDepLogicHelper transNotifyDepHelper;
    @Autowired
    private Setting10MBean setting10MBean;

    /** 轉單 自訂物件 */
    @Getter
    @Setter
    private TransNotifyTo notifyTo = new TransNotifyTo();
    @Getter
    @Setter
    private Boolean showDownloadLog;
    /** 下載用記錄檔 */
    @Getter
    private StreamedContent file;
    @Getter
    private TransNotifyTo recoveryTransNotifyTo = new TransNotifyTo();

    @PostConstruct
    void init() {
        showDownloadLog = false;
    }

    /**
     * 計算並核對正確的轉換數量
     */
    public void btnCheck() {
        this.trans(Boolean.FALSE);
    }

    /**
     * 開始轉換
     */
    public void btnTrans() {
        this.trans(Boolean.TRUE);
    }

    /**
     * 開始轉換
     */
    private void trans(Boolean isTrans) {
        this.clearMsg();
        try {
            transNotifyDepHelper.checkTrans(notifyTo);
            if (isTrans) {
                boolean reuslt = transNotifyDepHelper.transNotifyDep(Boolean.FALSE, notifyTo);
                if (!reuslt) {
                    notifyTo.addLog("是否可進行轉單 : 不可執行...");
                } else {
                    notifyTo.setCount(0);
                    notifyTo.setErrCount(0);
                    notifyTo.addLog("是否可進行轉單 : 可執行...");
                    transNotifyDepHelper.transNotifyDep(isTrans, notifyTo);
                    showDownloadLog = true;
                }
            } else {
                transNotifyDepHelper.transNotifyDep(Boolean.FALSE, notifyTo);
            }

        } catch (Exception e) {
            notifyTo.addLog(e.getMessage());
            log.error("trans ERROR", e);
        }
        log.info(notifyTo.getMsg());
        setting10MBean.addInfoMsg(notifyTo.getMsg());
    }

    public void recoveryTrans() {
        this.clearMsg();
        try {
            transNotifyDepHelper.checkRecoveryTrans(recoveryTransNotifyTo);
            boolean reuslt = transNotifyDepHelper.recoveryTransNotifyDep(Boolean.FALSE, recoveryTransNotifyTo);
            if (!reuslt) {
                recoveryTransNotifyTo.addLog("是否可進行轉單 : 不可執行...");
            } else {
                recoveryTransNotifyTo.addLog("是否可進行轉單 : 可執行...");
                recoveryTransNotifyTo.setCount(0);
                recoveryTransNotifyTo.setErrCount(0);
                transNotifyDepHelper.recoveryTransNotifyDep(Boolean.TRUE, recoveryTransNotifyTo);
                showDownloadLog = true;
            }

        } catch (Exception e) {
            recoveryTransNotifyTo.addLog(e.getMessage());
            log.error("trans ERROR", e);
        }
        log.info(recoveryTransNotifyTo.getRecoveryMsg());
        setting10MBean.addInfoMsg(recoveryTransNotifyTo.getRecoveryMsg());
    }

    /**
     * 清除訊息
     */
    private void clearMsg() {
        showDownloadLog = false;
        notifyTo.clearMsg();
        recoveryTransNotifyTo.clearMsg();
        setting10MBean.clearInfoScreenMsg();
    }

    public void downloadHandlerInfo() {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            String fileName = "BackstageNotifyLog" + sdf.format(new Date()) + ".txt";
            InputStream is = new ByteArrayInputStream(notifyTo.getMsg().getBytes(Charset.forName("UTF-8")));
            is.close();
            file = new DefaultStreamedContent(is, "text/plain", fileName);
            showDownloadLog = false;
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    public void downloadRecoveryHandlerInfo() {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            String fileName = "BackstageRecoveryNotifyLog" + sdf.format(new Date()) + ".txt";
            InputStream is = new ByteArrayInputStream(recoveryTransNotifyTo.getMsg().getBytes(Charset.forName("UTF-8")));
            is.close();
            file = new DefaultStreamedContent(is, "text/plain", fileName);
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }
    }

}
