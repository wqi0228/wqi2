/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.vo;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * 反需求完成物件
 *
 * @author brain0925_liao
 */
public class RollBackFinishVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 792557252301561964L;
    /** 使用者資訊 */
    @Getter
    @Setter
    private String userName = "";
    /** 新增時間 */
    @Getter
    @Setter
    private String addTime = "";
    /** 反需求完成內容 */
    @Getter
    @Setter
    private String addInfo = "";

}
