/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.searchheader;

import com.cy.tech.request.logic.service.customer.ReqWorkCustomerHelper;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.web.controller.enums.ReqStatusMBean;
import com.cy.work.customer.vo.WorkCustomer;
import com.cy.work.customer.vo.enums.EnableType;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.google.common.collect.Lists;
import java.util.stream.Collectors;
import javax.faces.model.SelectItem;

/**
 * 收費金額一覽表 的搜尋表頭
 *
 * @author jason_h
 */
@Controller
@Scope("view")
public class Search12HeaderMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -216542797438614740L;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private ReqWorkCustomerHelper reqWorkCustomerHelper;
    @Autowired
    transient private ReqStatusMBean reqStatusUtils;

    public static final String INDEX_KEY = "是否收費";

    /** 選擇的需求製作進度 */
    @Getter
    @Setter
    private List<String> selectRequireStatusTypes = Lists.newArrayList();

    /** 廳主條件 */
    @Getter
    @Setter
    private List<WorkCustomer> customers;
    /** 是否廳主分頁 */
    @Getter
    @Setter
    private Boolean isCustomerPage = Boolean.FALSE;
    /** 分頁條件 */
    @Getter
    @Setter
    private WorkCustomer customerPage;

    /*廳主選項**/
    @Getter
    private List<WorkCustomer> customerItems;
    /*廳主分頁選項**/
    @Getter
    @Setter
    private List<WorkCustomer> customerPageItems;

    @PostConstruct
    private void init() {
        this.clear();
    }

    /**
     * 清除/還原選項
     */
    public void clear() {
        this.initCommHeader();
        this.initHeader();
    }

    /**
     * 初始化commHeader
     */
    private void initCommHeader() {
        commHeaderMBean.clear();
        commHeaderMBean.initDefaultRequireDepts(false);
    }

    /**
     * 初始化Header
     */
    private void initHeader() {
        this.initRequireStatus();
        this.initCustomerItems();
        customers = Lists.newArrayList();
        isCustomerPage = Boolean.FALSE;
        customerPage = null;
    }

    private void initRequireStatus() {
        selectRequireStatusTypes.clear();
        selectRequireStatusTypes.add(RequireStatusType.NEW_INSTANCE.name());
        selectRequireStatusTypes.add(RequireStatusType.WAIT_CHECK.name());
        selectRequireStatusTypes.add(RequireStatusType.ROLL_BACK_NOTIFY.name());
        selectRequireStatusTypes.add(RequireStatusType.PROCESS.name());
        selectRequireStatusTypes.add(RequireStatusType.CLOSE.name());
        selectRequireStatusTypes.add(RequireStatusType.AUTO_CLOSED.name());
        selectRequireStatusTypes.add(RequireStatusType.COMPLETED.name());
    }

    /**
     * 取得 廳主選項
     */
    private void initCustomerItems() {
        customerItems = reqWorkCustomerHelper.findByEnable(EnableType.ENABLE);
    }

    /**
     * 取得customers中sid
     *
     * @return
     */
    public List<Long> getCustomerSids() {
        List<Long> sids = Lists.newArrayList();
        customers.forEach(each -> {
            sids.add(each.getSid());
        });
        return sids;
    }

    /**
     * 建立需求狀態查詢 List
     *
     * @return
     */
    public List<String> createQueryReqStatus() {
        return reqStatusUtils.createQueryReqStatus(this.selectRequireStatusTypes, this.getAllReqStatus());
    }

    /**
     * 取得此報表全部製作進度查詢
     *
     * @return
     */
    private List<RequireStatusType> getAllReqStatus() {
        return reqStatusUtils.createExcludeStatus(
                Lists.newArrayList(RequireStatusType.DRAFT)
        );
    }

    /**
     * 畫面用全部製作進度查詢
     *
     * @return
     */
    public SelectItem[] getReqStatusItems() {
        return reqStatusUtils.buildItem(this.getAllReqStatus());
    }

    /**
     * 建立需求狀態查詢 List
     *
     * @return
     */
    public List<String> createQueryAllReqStatus() {
        return this.getAllReqStatus().stream()
                .map(each -> each.name())
                .collect(Collectors.toList());
    }
}
