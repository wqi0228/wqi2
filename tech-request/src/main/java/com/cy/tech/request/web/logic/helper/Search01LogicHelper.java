/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.logic.helper;

import com.cy.commons.vo.Org;
import com.cy.tech.request.logic.service.OrganizationService;
import com.google.common.collect.Lists;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 需求單查詢 - 邏輯Helper
 *
 * @author brain0925_liao
 */
@Component
public class Search01LogicHelper {

    @Autowired
    private OrganizationService orgService;

    public List<String> initAllDepts() {
        List<String> requireDepts = orgService.findAll().stream()
                .map(each -> each.getSid().toString())
                .collect(Collectors.toList());
        return requireDepts;
    }

    /**
     * 初始化 登入者的預設單位
     * @param loginDepSid
     * @return
     */
    public List<String> initDefaultDepts(Integer loginDepSid) {
        List<String> requireDepts = Lists.newArrayList(String.valueOf(loginDepSid));
        this.initDefaultDepts(orgService.findBySid(loginDepSid), requireDepts);
        return requireDepts;
    }

    private void initDefaultDepts(Org parent, List<String> defaultDepts) {
        List<Org> children = orgService.findByParentOrg(parent);
        if (children.isEmpty()) {
            return;
        }
        defaultDepts.addAll(this.getOrgSids(children));
        children.forEach(each -> this.initDefaultDepts(each, defaultDepts));
    }

    private List<String> getOrgSids(List<Org> children) {
        return children.stream()
                .map(Org::getSid)
                .map(Object::toString)
                .collect(Collectors.toList());
    }

}
