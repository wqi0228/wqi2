/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.TraceService;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.to.DataTablePickList;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.values.OrganizationTreeMBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.logic.WkUserAndOrgLogic;
import com.cy.work.trace.vo.WorkTrace;
import com.cy.work.trace.vo.enums.TraceStatus;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 追蹤 控制
 *
 * @author kasim
 */
@NoArgsConstructor
@Controller
@Scope("view")
public class TraceMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5096304989377889165L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private TraceService traceService;
    @Autowired
    transient private OrganizationService orgService;
    @Autowired
    transient private DisplayController displayController;

    @Autowired
    transient private WkUserAndOrgLogic wkUserAndOrgLogic;
    @Getter
    @Autowired
    private OrganizationTreeMBean orgTreeMBean;

    /** 是否有追蹤紀錄 */
    private Boolean isTraceCount;
    /** 操作 需求單 */
    private Require require;
    @Getter
    /** 操作 追蹤 */
    private WorkTrace traceObj = new WorkTrace();
    @Getter
    @Setter
    /** 追蹤事項CSS */
    private String memoCss;
    @Getter
    @Setter
    /** 追蹤方式(0：自行追蹤、1：邀請其他人追蹤、2：所屬部門追蹤) */
    private String traceRadio;
    @Getter
    /** 追蹤人員 */
    private List<User> traceUsers = Lists.newArrayList();
    @Getter
    /** 追蹤部門 */
    private List<Org> traceDeps = Lists.newArrayList();
    @Getter
    @Setter
    /** 查詢欄位 - 使用者名稱 */
    private String traceUserName;
    @Getter
    /** DataTablePickList 物件 */
    private DataTablePickList<User> userDataTablePickList = new DataTablePickList<>(User.class);
    /** 查詢欄位 - 部門名稱 */
    @Getter
    @Setter
    private String depName;
    /** 是否重新讀取報表 */
    private Boolean isReload = Boolean.FALSE;

    @PostConstruct
    public void init() {
        isTraceCount = null;
        this.initTrace();
    }

    /**
     * 初始化
     */
    public void initTraceByMemo(Require require, WorkTrace traceObj) {
        this.require = require;
        this.traceObj = traceObj;
        memoCss = traceObj.getMemoCss();
        isReload = Boolean.TRUE;
    }

    /**
     * 初始化
     */
    public void initTrace(Require01MBean r01MBean) {
        this.btnTrace(r01MBean);
        isReload = Boolean.TRUE;
    }

    /**
     * 開啟追蹤視窗
     *
     * @param r01MBean
     */
    public void btnTrace(Require01MBean r01MBean) {
        isReload = Boolean.FALSE;
        require = r01MBean.getRequire();
        this.initTrace();
        this.initTraceUsersAndTraceDeps(require);
    }

    /**
     * 初始化
     */
    private void initTrace() {
        traceObj = new WorkTrace();
        userDataTablePickList = new DataTablePickList<>(User.class);
        memoCss = "";
        traceRadio = "0";
        orgTreeMBean.init();
    }

    /**
     * 初始化已經追蹤人員和部門
     *
     * @param require
     */
    private void initTraceUsersAndTraceDeps(Require require) {
        this.initTraceUsers(require);
        this.initTraceTraceDeps(require);
    }

    /**
     * 初始化已經追蹤人員
     *
     * @param require
     */
    private void initTraceUsers(Require require) {
        traceUsers = Lists.newArrayList();
        List<User> users = traceService.findTraceUserByRequireAndStatusAndTraceStatus(
                require, Activation.ACTIVE, TraceStatus.UN_FINISHED);
        for (User user : users) {
            if (Strings.isNullOrEmpty(user.getName())) {
                user = WkUserCache.getInstance().findBySid(user.getSid());
            }
            traceUsers.add(user);
        }
    }

    /**
     * 初始化已經追蹤部門
     *
     * @param require
     */
    private void initTraceTraceDeps(Require require) {
        traceDeps = Lists.newArrayList();
        List<Org> orgs = traceService.findTraceDepByRequireAndStatusAndTraceStatus(
                require, Activation.ACTIVE, TraceStatus.UN_FINISHED);
        for (Org org : orgs) {
            if (Strings.isNullOrEmpty(WkOrgUtils.getOrgName(org))) {
                org = WkOrgCache.getInstance().findBySid(org.getSid());
            }
            traceDeps.add(org);
        }
    }

    /**
     * 變更追蹤方式
     */
    public void onChangeTraceRadio() {
        if ("1".equals(traceRadio)) {
            traceUserName = "";
            this.initSearchUserDataTable();
            displayController.update("dlgTraceActionUsers_view");
            displayController.showPfWidgetVar("dlgTraceActionUsers");
        }
    }

    /**
     * 初始查詢
     */
    private void initSearchUserDataTable() {
        // 查詢關係部門
        Set<Integer> relationOrgSids = this.wkUserAndOrgLogic.prepareCanViewDepSidsBaseOnPanel(SecurityFacade.getUserSid(), false);
        this.prepareSrcUsr(relationOrgSids);
    }

    private void prepareSrcUsr(Set<Integer> targetDepSids) {

        // 防呆
        if (this.traceUsers == null) {
            this.traceUsers = Lists.newArrayList();
        }

        // ====================================
        // 準備排序器
        // ====================================
        // 取得部門排序
        Map<Integer, Integer> orgSortNumberMapByDepSid = WkOrgCache.getInstance().findOrgSortNumberMapByDepSid();

        Comparator<User> comparator = Comparator.comparing(user -> this.prepareSortSeq(user, orgSortNumberMapByDepSid), Comparator.nullsLast(Comparator.naturalOrder()));
        comparator = comparator.thenComparing(Comparator.comparing(User::getName, Comparator.nullsLast(Comparator.naturalOrder())));

        // ====================================
        // 取得標的部門所有的 user
        // ====================================
        // 查詢 user
        List<User> users = WkUserCache.getInstance().findUserWithManagerByOrgSids(targetDepSids, Activation.ACTIVE)
                .stream()
                // 排除自己
                .filter(user -> !WkCommonUtils.compareByStr(SecurityFacade.getUserSid(), user.getSid()))
                // 排除已選擇
                .filter(user -> !this.traceUsers.contains(user))
                // 排序
                .sorted(comparator)
                .collect(Collectors.toList());

        userDataTablePickList.getTargetData().clear();

        this.updateUserListSourceData(users);

    }

    /**
     * @param user
     * @param orgSortNumberMapByDepSid
     * @return
     */
    private Integer prepareSortSeq(User user, Map<Integer, Integer> orgSortNumberMapByDepSid) {
        if (user == null || user.getPrimaryOrg() == null) {
            return Integer.MAX_VALUE;
        }
        return orgSortNumberMapByDepSid.get(user.getPrimaryOrg().getSid());
    }

    /**
     * 更新 userDataTablePickList
     */
    private void updateUserListSourceData(List<User> sourceUsers) {
        if (WkStringUtils.notEmpty(userDataTablePickList.getTargetData())) {
            sourceUsers.removeAll(userDataTablePickList.getTargetData());
        }

        userDataTablePickList.setSourceData(sourceUsers);
    }

    /**
     * 名稱查詢
     */
    public void btnTraceUserName() {

        // 防呆
        if (this.traceUsers == null) {
            this.traceUsers = Lists.newArrayList();
        }

        // ====================================
        // 準備排序器
        // ====================================
        // 取得部門排序
        Map<Integer, Integer> orgSortNumberMapByDepSid = WkOrgCache.getInstance().findOrgSortNumberMapByDepSid();

        Comparator<User> comparator = Comparator.comparing(user -> this.prepareSortSeq(user, orgSortNumberMapByDepSid), Comparator.nullsLast(Comparator.naturalOrder()));
        comparator = comparator.thenComparing(Comparator.comparing(User::getName, Comparator.nullsLast(Comparator.naturalOrder())));

        // ====================================
        // 取得標的部門所有的 user
        // ====================================
        // 查詢 user
        List<User> users = WkUserUtils.findByNameLike(this.traceUserName, SecurityFacade.getCompanyId())
                .stream()
                // 排除自己
                .filter(userSid -> !WkCommonUtils.compareByStr(SecurityFacade.getUserSid(), userSid))
                // userSid -> user
                .map(userSid -> WkUserCache.getInstance().findBySid(userSid))
                // 排除已停用
                .filter(user -> WkUserUtils.isActive(user))
                // 排除已選擇
                .filter(user -> !this.traceUsers.contains(user))
                // 排序
                .sorted(comparator)
                .collect(Collectors.toList());

        userDataTablePickList.getTargetData().clear();

        this.updateUserListSourceData(users);

    }

    /**
     * 開啟組織樹視窗
     *
     * @param r01MBean
     */
    public void clickCheckUsersOrgTreeDlg(Require01MBean r01MBean) {
        depName = "";
        orgTreeMBean.init();
        orgTreeMBean.rebuildRoot(WkOrgCache.getInstance().findBySid(r01MBean.getRequire().getCreateCompany().getSid()));
    }

    /**
     * 查詢部門組織樹名稱
     *
     * @param r01MBean
     */
    public void searchDepByTree(Require01MBean r01MBean) {
        orgTreeMBean.initRoot(WkOrgCache.getInstance().findBySid(r01MBean.getRequire().getCreateCompany().getSid()));
        if (Strings.isNullOrEmpty(depName)) {
            orgTreeMBean.rebuildRoot(orgService.findBySid(r01MBean.getRequire().getCreateCompany().getSid()));
            return;
        }
        orgTreeMBean.searchNodeByTextNoSelect(depName, orgTreeMBean.getOrgTreeRoot());
    }

    /**
     * 選取部門
     */
    public void clickByUsersOrgTreeDlg(Require01MBean r01MBean) {
        if (orgTreeMBean.getOrgTreeSelect() == null) {
            return;
        }

        Set<Integer> selectedOrgSids = Lists.newArrayList(orgTreeMBean.getOrgTreeSelect()).stream()
                .map(treeNode -> {
                    if (treeNode.getData() == null || !treeNode.getData().getClass().equals(Org.class)) {
                        return -1;
                    }
                    return ((Org) treeNode.getData()).getSid();
                })
                .collect(Collectors.toSet());

        this.prepareSrcUsr(selectedOrgSids);

    }

    /**
     * 加入追蹤
     *
     * @param r01MBean
     */
    public void btnAddTrack(Require01MBean r01MBean) {
        this.initTraceUsersAndTraceDeps(require);
        if (!this.checkSave()) {
            return;
        }
        this.save(r01MBean);
    }

    /**
     * 確認選取其他使用者
     *
     * @param r01MBean
     */
    public void btnSelTraceUsers(Require01MBean r01MBean) {
        this.initTraceUsersAndTraceDeps(require);
        if (!this.checkSave()) {
            return;
        }
        this.save(r01MBean);
        displayController.hidePfWidgetVar("dlgTraceActionUsers");
    }

    /**
     * 檢核
     *
     * @return
     */
    private boolean checkSave() {
        if ("0".equals(traceRadio)) {
            if (traceUsers.contains(loginBean.getUser())) {
                MessagesUtils.showInfo("該案件你已有進行追蹤動作！！");
                return false;
            }
        } else if ("1".equals(traceRadio)) {
            return this.checkTargetUsers();
        } else if (traceDeps.contains(loginBean.getDep())) {
            MessagesUtils.showInfo("該案件部門已有進行追蹤動作！！");
            return false;
        }
        return true;
    }

    /**
     * 檢核其他追蹤
     *
     * @return
     */
    private boolean checkTargetUsers() {
        if (userDataTablePickList.getTargetData() == null || userDataTablePickList.getTargetData().isEmpty()) {
            MessagesUtils.showError("你尚未選擇資料！！");
            return false;
        }
        String errMsg = "";
        for (User u : userDataTablePickList.getTargetData()) {
            if (traceUsers.contains(u)) {
                if (!Strings.isNullOrEmpty(errMsg)) {
                    errMsg += "、";
                }
                errMsg += u.getName();
            }
        }
        if (!Strings.isNullOrEmpty(errMsg)) {
            MessagesUtils.showError("已有追蹤人員如下：" + errMsg + "，請確認！！");
            return false;
        }
        return true;
    }

    /**
     * 存檔
     *
     * @param r01MBean
     */
    private void save(Require01MBean r01MBean) {
        if ("0".equals(traceRadio)) {
            traceService.saveByPersonal(traceObj, require, memoCss, loginBean.getUser());
        } else if ("1".equals(traceRadio)) {
            traceService.saveByOtherPerson(traceObj, require,
                    userDataTablePickList.getTargetData(), memoCss, loginBean.getUser());
        } else {
            traceService.saveByDepartment(traceObj, require, loginBean.getDep(),
                    memoCss, loginBean.getUser());
        }
        r01MBean.getTitleBtnMBean().clear();
        isTraceCount = null;
        displayController.hidePfWidgetVar("dlgTraceAction");
        displayController.update("title_info_click_btn_id");
        if (isReload) {
            displayController.execute("searchByKeepPage()");
        }
    }

    /**
     * 判斷是否有追蹤資料
     *
     * @param r01MBean
     * @return
     */
    public Boolean hasTrace(Require01MBean r01MBean) {
        if (isTraceCount == null) {
            isTraceCount = traceService.hasTrace(r01MBean.getRequire(),
                    loginBean.getDep(), loginBean.getUser());
        }
        return isTraceCount;
    }

    /**
     * 異動追蹤事項
     */
    public void btnSaveMome() {
        traceService.updateByMemo(traceObj, memoCss, loginBean.getUser());
        this.reLoadReport();
    }

    /**
     * 取得使用者名稱
     *
     * @param user
     * @return
     */
    public String findUserName(User user) {
        if (user != null) {
            if (Strings.isNullOrEmpty(user.getName())) {
                user = WkUserCache.getInstance().findBySid(user.getSid());
            }
            return user.getName();
        }
        return "";
    }

    /**
     * 取得部門名稱
     *
     * @param org
     * @return
     */
    public String findOrgName(Org org) {
        return WkOrgUtils.getOrgName(org);
    }

    /**
     * 重新讀取
     */
    private void reLoadReport() {
        if (isReload) {
            displayController.execute("reSearch()");
        }
    }

}
