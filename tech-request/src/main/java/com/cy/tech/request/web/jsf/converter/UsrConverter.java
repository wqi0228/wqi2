/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.jsf.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.stereotype.Component;

import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;

/**
 * 前端JSF、PF元件轉換成員
 *
 * @author shaun
 */
@Component("usrConverter")
public class UsrConverter implements Converter {


    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if ((value == null) || "".equals(value.trim()) || "null".equals(value.trim())) {
            return null;
        }
        return WkUserCache.getInstance().findBySid(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value != null && value instanceof User) {
            return String.valueOf(((User) value).getSid());
        }
        if (value != null && value instanceof String) {
            return (String) value;
        }
        if (value != null && value instanceof Integer) {
            return ((Integer) value).toString();
        }
        return "";
    }

}
