/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require;

import com.cy.commons.vo.User;
import com.cy.fb.rest.client.vo.Project;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.FogbugzService;
import com.cy.tech.request.logic.service.pmis.PmisService;
import com.cy.tech.request.vo.require.FogbugzAssign;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import com.cy.tech.request.vo.template.component.ComBase;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.pf.utils.ConfirmCallbackDialogController;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class FogbugzMBean implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 4320927716168715011L;
    @Autowired
	transient private FogbugzService fbService;
	@Autowired
	transient private DisplayController disply;
	@Autowired
	private PmisService pmisService;
	@Autowired
	private ConfirmCallbackDialogController confirmCallbackDialogController;

	private List<FogbugzAssign> fbItems;

	@PostConstruct
	public void init() {
		fbItems = null;
	}

	public List<FogbugzAssign> findItems(Require01MBean r01MBean) {
		if (r01MBean.getRequire() == null || !r01MBean.getRequire().getHasFbRecord()) {
			return Lists.newArrayList();
		}
		if (fbItems == null) {
			this.updateItems(r01MBean.getRequire());
		}
		return fbItems;
	}

	public void updateItems(Require require) {
		fbItems = fbService.findByRequire(require);
	}

	public void initTabInfo(Require require) {
		if (require == null || !require.getHasFbRecord()) {
			return;
		}
		this.updateItems(require);
	}

	/**
	 * @param r01MBean
	 */
	public void preTransFogbugz(Require01MBean r01MBean) {
		// ====================================
		// 檢查資料異常
		// ====================================
		if (r01MBean == null ||
		        r01MBean.getRequire() == null ||
		        WkStringUtils.isEmpty(r01MBean.getRequire().getRequireNo())) {
			MessagesUtils.showWarn(WkMessage.SESSION_TIMEOUT);
			return;
		}

		// ====================================
		// 取得登入者資料
		// ====================================
		User user = WkUserCache.getInstance().findBySid(SecurityFacade.getUserSid());
		if (user == null) {
			MessagesUtils.showWarn(WkMessage.SESSION_TIMEOUT);
			return;
		}

		// ====================================
		// 取得FB專案資料並做權限檢核
		// ====================================
		Project fbProject = null;
		try {
			fbProject = this.fbService.verifyAuthAndFindFBProject(r01MBean.getRequire(), user);
		} catch (UserMessageException e) {
			MessagesUtils.show(e);
			return;
		} catch (Exception e) {
			String errorMessage = WkMessage.EXECTION + ":[" + e.getMessage() + "]";
			MessagesUtils.showError(errorMessage);
			log.error(errorMessage, e);
			return;
		}

		// ====================================
		// 確認訊息
		// ====================================
		// 取得已存在
		FogbugzAssign fogbugzAssign = this.fbService.findByRequireAndUser(r01MBean.getRequire(), user);

		// 兜組確認訊息
		String confirmMessage = "";
		if (fogbugzAssign == null) {
			confirmMessage = "將轉入 FogBugz Project:[" + fbProject.getsProject() + "]";
		} else {
			confirmMessage = "將更新 FogBugz Case:[" + fogbugzAssign.getFbId() + "]";
		}
		confirmMessage += "<br/>是否確定？";

		// 顯示確認訊息, 並設定 callback
		confirmCallbackDialogController.showConfimDialog(
		        confirmMessage,
		        Lists.newArrayList(
		                "title_info_click_btn_id",
		                "viewPanelBottomInfoTabId",
		                "require01_title_info_id"),
		        () -> this.transFogbugz(r01MBean));
	}

	/**
	 * 轉FB
	 *
	 * @param r01MBean
	 */
	private void transFogbugz(Require01MBean r01MBean) {
		// ====================================
		// 檢查資料異常
		// ====================================
		if (r01MBean == null ||
		        r01MBean.getRequire() == null ||
		        WkStringUtils.isEmpty(r01MBean.getRequire().getRequireNo())) {
			MessagesUtils.showWarn(WkMessage.SESSION_TIMEOUT);
			return;
		}

		// ====================================
		// 取得登入者資料
		// ====================================
		User user = WkUserCache.getInstance().findBySid(SecurityFacade.getUserSid());
		if (user == null) {
			MessagesUtils.showWarn(WkMessage.SESSION_TIMEOUT);
			return;
		}

		// ====================================
		// 取得FB專案資料並做權限檢核
		// ====================================
		Project fbProject = null;
		try {
			fbProject = this.fbService.verifyAuthAndFindFBProject(r01MBean.getRequire(), user);
		} catch (UserMessageException e) {
			MessagesUtils.show(e);
			return;
		} catch (Exception e) {
			String errorMessage = WkMessage.EXECTION + ":[" + e.getMessage() + "]";
			MessagesUtils.showError(errorMessage);
			log.error(errorMessage, e);
			return;
		}

		// ====================================
		// 確認訊息
		// ====================================
		// 取得已存在
		FogbugzAssign fogbugzAssign = this.fbService.findByRequireAndUser(r01MBean.getRequire(), user);

		// ====================================
		// 執行新增或更新 FB
		// ====================================
		try {
			List<FieldKeyMapping> selectTemplateItem = r01MBean.getTemplateMBean().getTemplateItem();
			Map<String, ComBase> comValueMap = r01MBean.getTemplateMBean().getComValueMap();

			// 執行新增或更新 FB
			this.fbService.save(
			        r01MBean.getRequire(),
			        fbProject,
			        fogbugzAssign,
			        selectTemplateItem,
			        comValueMap,
			        user,
			        new Date());

			// 自動更新轉pmis的需求單
			if (pmisService.hasPmisHistory(r01MBean.getRequire().getRequireNo())) {
				r01MBean.transferToPMIS();
			}

			// 更新主檔物件
			this.updateItems(r01MBean.getRequire());
			
		} catch (IllegalArgumentException | UnsupportedEncodingException e) {
			log.error("[" + r01MBean.getRequire().getRequireNo() + "]:轉FB ... " + e.getMessage(),e);
			r01MBean.reBuildeByUpdateFaild();
			MessagesUtils.showError(e.getMessage());
		} catch (UserMessageException e) {
			MessagesUtils.show(e);
			return;
		} catch (Exception e) {
			MessagesUtils.showError("執行轉FB失敗!" + e.getMessage());
			log.error("執行轉FB失敗!" + e.getMessage(), e);
			return;
		}

		try {
			r01MBean.getBottomTabMBean().resetTabIdx(r01MBean);
			r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.DELIVER_FB_REC);
			r01MBean.getTitleBtnMBean().clear();
			disply.execute("moveScrollBottom()");
		} catch (Exception e) {
			MessagesUtils.showError("更新畫面失敗, 請重新讀取本單據!");
			log.error("更新畫面失敗!" + e.getMessage(), e);
			return;
		}

	}

}
