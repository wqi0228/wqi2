package com.cy.tech.request.web.newsearch;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.helper.component.ComponentHelper;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.component.reqconfirm.ReqConfirmClientHelper;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.search.CustomColumnMBean;
import com.cy.tech.request.web.controller.search.TableUpDownBean;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.logic.WkUserAndOrgLogic;
import com.cy.work.viewcomponent.treepker.TreePickerCallback;
import com.cy.work.viewcomponent.treepker.TreePickerComponent;
import com.cy.work.viewcomponent.treepker.helper.TreePickerDepHelper;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public abstract class BaseNewSearchController {

    // ========================================================================
    // 工具區
    // ========================================================================
    @Autowired
    private transient ReqLoadBean loadManager;
    @Autowired
    protected transient RequireService requireService;
    @Autowired
    protected transient TreePickerDepHelper treePickerDepHelper;
    @Autowired
    protected transient DisplayController displayController;
    @Autowired
    protected transient WkOrgCache wkOrgCache;
    @Autowired
    protected transient WkUserCache wkUserCache;
    @Autowired
    protected transient WkUserAndOrgLogic wkUserAndOrgLogic;

    // ========================================================================
    // Bean
    // ========================================================================
    @Autowired
    transient private TableUpDownBean tableUpDownBean;
    @Autowired
    transient private CustomColumnMBean customColumnMBean;
    @Autowired
    transient private NewSearchDisplayComponent newSearchDisplayComponent;
    @Autowired
    transient private ComponentHelper componentHelper;

    // ========================================================================
    // 畫面控制
    // ========================================================================
    /**
     * 
     */
    @Getter
    public final String dataTableId = "dtRequire";
    @Getter
    public final String dataTableWv = "QueryResultDataTable_WV";

    /**
     * 選擇的資料
     */
    @Getter
    @Setter
    private NewSearchResultVO querySelection;

    /**
     * 上下筆移動keeper
     */
    @Getter
    private NewSearchResultVO queryKeeper;

    // ========================================================================
    // 變數區
    // ========================================================================
    /**
     * 查詢條件容器
     */
    @Getter
    public NewSearchConditionVO conditionVO = new NewSearchConditionVO();

    /**
     * 項目樹
     */
    @Getter
    public transient TreePickerComponent categoryTreePicker;

    /**
     * 查詢結果
     */
    @Getter
    @Setter
    protected List<NewSearchResultVO> queryItems;

    // ========================================================================
    // 方法區
    // ========================================================================
    public void init() {
    }

    /**
     * @param pageCount
     * @param rowVO
     */
    public void readRow(NewSearchResultVO rowVO) {
        this.querySelection = rowVO;
        this.queryKeeper = rowVO;

        // highlight列表位置
        this.displayController.execute("selectRow('" + this.dataTableWv + "'," + this.getRowIndex(this.customColumnMBean.getPageCount()) + ");");

        // 去除粗體Class
        this.displayController.execute("removeClassByTextBold('" + this.dataTableId + "'," + this.getRowIndex(this.customColumnMBean.getPageCount()) + ");");
        this.displayController.execute("changeAlreadyRead('" + this.dataTableId + "'," + this.getRowIndex(this.customColumnMBean.getPageCount()) + ");");

        // 變更已閱讀
        rowVO.setReadRecordType(ReadRecordType.HAS_READ);

        // 重設定上下筆資訊
        this.tableUpDownBean.setCurrRow(this.queryKeeper.getRequireNo());
        this.tableUpDownBean.resetUpDown(this.queryItems.indexOf(this.queryKeeper), this.queryItems.size());
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(NewSearchResultVO view) {
        this.queryKeeper = this.querySelection = view;
        this.newSearchDisplayComponent.switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        this.newSearchDisplayComponent.switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();

        // 製作進度面板控制 (移除)
        displayController.execute(ReqConfirmClientHelper.prepareClientPanelRemoveScript());
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (this.newSearchDisplayComponent.switchType.equals(SwitchType.CONTENT)) {
            this.newSearchDisplayComponent.switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            return;
        }
        if (this.newSearchDisplayComponent.switchType.equals(SwitchType.DETAIL)) {
            this.newSearchDisplayComponent.switchFullType = SwitchType.DETAIL;
            this.newSearchDisplayComponent.switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
    }

    /**
     * 上下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtn(int step) {

        if (WkStringUtils.isEmpty(this.queryItems)) {
            return;
        }

        NewSearchResultVO rowVO = this.querySelection;
        // 取得目前的資料列
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index < 0) {
            index = 0;
        }

        // ++(下一筆) or --(上一筆)
        index += step;
        // 取得新的資料列
        if (index >= 0 && queryItems.size() >= index + 1) {
            rowVO = this.queryItems.get(index);
        }

        this.readRow(rowVO);
        // this.changeRequireContent(this.querySelection);

        // if (switchFullType.equals(SwitchType.DETAIL) &&
        // !switchType.equals(SwitchType.CONTENT)
        // || switchFullType.equals(SwitchType.FULLCONTENT) &&
        // !switchType.equals(SwitchType.CONTENT)) {
        // this.normalScreenReport();
        // display.update("headerTitle");
        // display.update("searchBody");
        // }
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(NewSearchResultVO view) {
        // 查詢需求單
        Require r = requireService.findByReqNo(view.getRequireNo());
        User user = this.wkUserCache.findBySid(SecurityFacade.getUserSid());
        // 閱讀記錄
        view.setReadRecordType(ReadRecordType.HAS_READ);
        // load 需求單內容
        loadManager.reloadReqForm(r, user);
    }

    // ========================================================================
    // 類別組合選單
    // ========================================================================

    /**
     * 類別組合選單 - 開啟
     */
    public void event_dialog_categoryTree_open() {

        try {
            this.categoryTreePicker.rebuild();
        } catch (Exception e) {
            String message = "開啟【類別組合】設定視窗失敗！" + e.getMessage();
            log.error(message, e);
            MessagesUtils.showError(message);
            return;
        }

        displayController.showPfWidgetVar("wv_dlg_categoryCollection");
    }

    /**
     * 類別組合選單 - 確認
     */
    public void event_dialog_categoryTree_confirm() {
        // 由元件中取回被選擇資料
        this.conditionVO.setCategoryCollectionSids(this.categoryTreePicker.getSelectedItemSids());
        displayController.hidePfWidgetVar("wv_dlg_categoryCollection");
    }

    /**
     * 類別組合選單 - callback 事件
     */
    protected final TreePickerCallback categoryPickerCallback = new TreePickerCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = -7895051794308536192L;

        /**
         * 準備所有的項目
         * 
         * @return
         * @throws Exception
         */
        @Override
        public List<WkItem> prepareAllItems() throws Exception {
            // 取得類別項目
            return componentHelper.prepareAllCategoryItems();
        }

        /**
         * 準備已選擇項目
         * 
         * @return
         */
        @Override
        public List<String> prepareSelectedItemSids() throws Exception {
            // 初始化
            if (WkStringUtils.isEmpty(conditionVO.getCategoryCollectionSids())) {
                conditionVO.setCategoryCollectionSids(Sets.newHashSet());
            }
            // 回傳
            return Lists.newArrayList(conditionVO.getCategoryCollectionSids());
        }

        @Override
        public List<String> prepareDisableItemSids() throws Exception {
            return Lists.newArrayList();
        }
    };

    // ========================================================================
    // 轉寄部門
    // ========================================================================
    /**
     * 轉寄部門樹
     */
    @Getter
    public transient TreePickerComponent forwardDepTreePicker;

    /**
     * 轉寄部門選單 - callback 事件
     */
    protected final TreePickerCallback forwardDepTreeCallback = new TreePickerCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = -8703817931413180286L;

        @Override
        public List<WkItem> prepareAllItems() throws Exception {

            // 取得所有登入公司部門
            List<Org> allDeps = wkOrgCache.findAllDepByCompID(SecurityFacade.getCompanyId());

            if (WkStringUtils.isEmpty(allDeps)) {
                return Lists.newArrayList();
            }

            // 轉 sid
            List<Integer> allDepSids = allDeps.stream().map(Org::getSid).collect(Collectors.toList());

            // 轉 WkItem 物件
            return treePickerDepHelper.prepareDepItems(
                    SecurityFacade.getCompanyId(),
                    Lists.newArrayList(allDepSids));
        }

        /**
         * 準備已選擇項目
         * 
         * @return
         */
        @Override
        public List<String> prepareSelectedItemSids() throws Exception {

            // 初始化
            if (WkStringUtils.isEmpty(conditionVO.getForwardDepSids())) {
                conditionVO.setForwardDepSids(Sets.newHashSet());
            }

            // 回傳
            return conditionVO.getForwardDepSids().stream().map(sid -> sid + "").collect(Collectors.toList());
        }

        @Override
        public List<String> prepareDisableItemSids() throws Exception {
            // 無不可選擇的資料
            return Lists.newArrayList();
        }
    };

    /**
     * 轉寄部門選單 - 開啟選單
     */
    public void event_dialog_forwardDepTree_open() {

        try {
            this.forwardDepTreePicker.rebuild();
        } catch (Exception e) {
            log.error("開啟【轉寄部門】設定視窗失敗", e);
            MessagesUtils.showError("開啟【轉寄部門】設定視窗失敗");
            return;
        }

        displayController.showPfWidgetVar("wv_dlg_forwardDep");
    }

    /**
     * 轉寄部門選單 - 確認
     */
    public void event_dialog_forwardDepTree_confirm() {

        // ====================================
        // 收集資料
        // ====================================
        // 取得元件中被選擇的項目
        this.conditionVO.setForwardDepSids(this.forwardDepTreePicker.getSelectedItemIntegerSids());

        // ====================================
        // 畫面控制
        // ====================================
        displayController.hidePfWidgetVar("wv_dlg_forwardDep");
    }
}
