/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.attachment.util;

import com.cy.tech.request.logic.vo.AttachmentVO;
import com.cy.tech.request.web.listener.AttachMaintainCallBack;
import com.cy.work.common.vo.AttachmentService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.List;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
public abstract class AttachMaintainCompant implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7878530994080012676L;
    @Getter
    protected List<AttachmentVO> attachmentVOs;
    @Getter
    private List<AttachmentVO> selectAttachmentVOs;

    private String directoryName;

    protected String sid;

    protected Integer userSid;
    @Getter
    protected AttachmentVO selDelAtt;

    private AttachMaintainCallBack attachMaintainCallBack;

    public AttachMaintainCompant(Integer userSid, String directoryName, AttachMaintainCallBack attachMaintainCallBack) {
        this.userSid = userSid;
        this.directoryName = directoryName;
        this.attachMaintainCallBack = attachMaintainCallBack;
        this.selectAttachmentVOs = Lists.newArrayList();
        this.selDelAtt = new AttachmentVO("", "", "", "", false, "", "");
    }

    public void changeToEditMode(String sid) {
        attachmentVOs.forEach(item -> {
            if (item.getAttSid().equals(sid)) {
                item.setEditMode(true);
            }
        });
    }

    public void loadDeleteAtt(String sid) {
        attachmentVOs.forEach(item -> {
            if (item.getAttSid().equals(sid)) {
                this.selDelAtt = item;
            }
        });
        if (attachMaintainCallBack != null) {
            attachMaintainCallBack.loadDelete(this);
        }
    }

    protected abstract void doDeleteAtt(AttachmentVO selAttachmentVO);

    public void deleteAtt() {
        if (selDelAtt == null || Strings.isNullOrEmpty(selDelAtt.getAttSid())) {
            return;
        }
        List<AttachmentVO> tempAv = Lists.newArrayList(attachmentVOs);
        tempAv.forEach(item -> {
            if (item.getAttSid().equals(selDelAtt.getAttSid())) {
                try {
                    this.doDeleteAtt(item);
                } catch (Exception e) {
                    log.error("deleteAtt Error :", e);
                }
            }
        });
    }

    protected abstract void doSaveAttDesc(AttachmentVO selAttachmentVO);

    public void saveAttDesc(String sid) {
        attachmentVOs.forEach(item -> {
            if (item.getAttSid().equals(sid)) {
                try {
                    this.doSaveAttDesc(item);
                    item.setEditMode(false);
                } catch (Exception e) {
                    log.error("saveAttDesc Error :", e);
                }
            }
        });
    }

    public void selectAll() {
        selectAttachmentVOs.clear();
        attachmentVOs.forEach(item -> {
            selectAttachmentVOs.add(item);
            item.setChecked(true);
        });
    }

    public void changeCheckBox() {
        selectAttachmentVOs.clear();
        attachmentVOs.forEach(item -> {
            if (item.isChecked()) {
                selectAttachmentVOs.add(item);
            }
        });
    }

    public StreamedContent downloadFile() throws IOException {
        if (selectAttachmentVOs.isEmpty()) {
            return null;
        }
        // 下載單一檔案，預設檔名使用該檔案的原始檔名（但是要經過編碼）
        if (selectAttachmentVOs.size() == 1) {
            return AttachmentUtils.createStreamedContent(selectAttachmentVOs.get(0), directoryName);
        }
        // 將多檔案壓縮成單一 zip 檔案再輸出
        return AttachmentUtils.createWrappedStreamedContent(selectAttachmentVOs, directoryName);
    }

    public String downloadFileContentDisposition() {
        if (selectAttachmentVOs.isEmpty()) {
            return null;
        }
        String encodedFileName = (selectAttachmentVOs.size() == 1)
                ? AttachmentUtils.encodeFileName(selectAttachmentVOs.get(0).getAttName())
                : AttachmentService.MULTIFILES_ZIP_FILENAME;
        String contentDispositionPattern
                = AttachmentUtils.isMSIE(Faces.getRequest())
                ? "{0}; filename=\"{1}\""
                : "{0}; filename=\"{1}\"; filename*=UTF-8''{1}";
        return MessageFormat.format(contentDispositionPattern, "attachment", encodedFileName);
    }

}
