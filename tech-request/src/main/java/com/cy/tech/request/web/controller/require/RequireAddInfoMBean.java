/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require;

import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.feedback.ReqFbkReplyService;
import com.cy.tech.request.vo.require.RequireTrace;
import com.cy.tech.request.vo.require.feedback.ReqFbkReply;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.require.feedback.ReqFbkReplyAttachMBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 需求單資訊補充控制
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class RequireAddInfoMBean implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -3426874673147240192L;
    @Autowired
	transient private RequireService requireService;
	@Autowired
	transient private ReqFbkReplyService fbkReplyService;

	@Autowired
	transient private WkJsoupUtils jsoupUtils;
	@Autowired
	transient private DisplayController display;
	@Autowired
	transient private LoginBean loginBean;


	@Getter
	private RequireTrace addInfoTrace;
	@Getter
	private ReqFbkReply editReply;

	transient private boolean isEditReply;

	@PostConstruct
	public void init() {
		addInfoTrace = new RequireTrace();
		editReply = new ReqFbkReply();
		isEditReply = false;
	}

	public void initRequireAddInfo(Require01MBean r01MBean) {
		addInfoTrace = requireService.initRequireReply(r01MBean.getRequire().getSid(), loginBean.getUser());
	}

	public void initAddInfoEdit(RequireTrace trace) {
		this.addInfoTrace = trace;
	}

	/**
	 * 儲存
	 *
	 * @param r01MBean
	 */
	public void save(Require01MBean r01MBean) {
		try {
			String content = jsoupUtils.clearCssTag(addInfoTrace.getRequireTraceContentCss());

			if (WkStringUtils.isEmpty(content)) {
				MessagesUtils.showWarn("請輸入需求資訊補充內容。");
				return;
			}

			String oldContent = jsoupUtils.clearCssTag(
			        requireService.initRequireReply(
			                r01MBean.getRequire().getSid(),
			                loginBean.getUser())
			                .getRequireTraceContentCss());

			// 新舊內容不同時，才發送系統通知
			boolean sendNotify = !content.equals(oldContent);

			requireService.saveRequireReply(r01MBean.getRequire(), loginBean.getUser(), addInfoTrace, sendNotify);

			// 以下進行畫面整理
			this.updateTabTrace(r01MBean);
			r01MBean.getBottomTabMBean().resetTabIdx(r01MBean);
			r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.TRACE);
			display.hidePfWidgetVar("req_add_info_dlg_wv");
			r01MBean.getTraceMBean().openTab(r01MBean, addInfoTrace);

		} catch (UserMessageException e) {
			MessagesUtils.show(e);
			r01MBean.reBuildeByUpdateFaild();
			return;
		} catch (Exception e) {
			log.error(r01MBean.getRequire().getRequireNo() + ":" + loginBean.getUser().getName() + ":建立需求資訊補充失敗..." + e.getMessage(), e);
			r01MBean.reBuildeByUpdateFaild();
			MessagesUtils.showError(e.getMessage());
		}
	}

	public void cancelAddInfo(Require01MBean r01MBean) {
		this.updateTabTrace(r01MBean);
		display.update("viewPanelBottomInfoTabId");
		r01MBean.getTraceMBean().openTab(r01MBean, addInfoTrace);
	}

	/**
	 * 取消時，也要呼叫(可能只有上傳附加檔案)
	 *
	 * @param r01MBean
	 */
	public void updateTabTrace(Require01MBean r01MBean) {
		r01MBean.getTraceMBean().clear();
	}

	public void openAttachByTrace(RequireTrace trace, ReqFbkAttachMBean fbkAttachMBean) {
		fbkAttachMBean.init(trace);
		this.addInfoTrace = trace;
	}

	public void initReply(RequireTrace trace) {
		isEditReply = false;
		editReply = fbkReplyService.createEmptyReply(trace, loginBean.getUser());
	}

	public void initReplyEdit(ReqFbkReply reply) {
		isEditReply = true;
		editReply = reply;
	}

	public void cancelReplyEdit(Require01MBean r01MBean) {
		if (isEditReply) {
			editReply = fbkReplyService.findReply(editReply.getSid());
		}
		isEditReply = false;
		display.update("viewPanelBottomInfoTabId");
		r01MBean.getTraceMBean().openTab(r01MBean, editReply.getTrace());
	}

	/**
	 * 新增回覆的回覆
	 * 
	 * @param r01MBean
	 */
	public void saveReply(Require01MBean r01MBean) {
		try {
			String content = jsoupUtils.clearCssTag(editReply.getContentCss());
			Preconditions.checkArgument(!Strings.isNullOrEmpty(content), "請輸入補充回覆內容。");
			boolean sendMail = false;
			if (!isEditReply) {
				fbkReplyService.saveByNewReply(editReply, loginBean.getUser());
				sendMail = true;
			} else {
				String oldContent = jsoupUtils.clearCssTag(fbkReplyService.findReply(editReply.getSid()).getContentCss());
				sendMail = !content.equals(oldContent);
				fbkReplyService.saveByEditReply(editReply, loginBean.getUser(), sendMail);
			}

			isEditReply = false;
			this.updateTabTrace(r01MBean);
			display.update("viewPanelBottomInfoTabId");
			r01MBean.getBottomTabMBean().resetTabIdx(r01MBean);
			r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.TRACE);
			r01MBean.getTraceMBean().openTab(r01MBean, editReply.getTrace());
			display.hidePfWidgetVar("req_fbk_reply_dlg_wv");
		} catch (IllegalArgumentException e) {
			log.debug("檢核不通過", e);
			MessagesUtils.showError(e.getMessage());
		} catch (Exception e) {
			log.error(r01MBean.getRequire().getRequireNo()
			        + ":" + loginBean.getUser().getName()
			        + ":" + (isEditReply ? "編輯" : "建立")
			        + "需求資訊補充失敗..." + e.getMessage(), e);
			r01MBean.reBuildeByUpdateFaild();
			MessagesUtils.showError(e.getMessage());
		}
	}

	public List<ReqFbkReply> findReplys(RequireTrace trace) {
		return fbkReplyService.findReplys(trace);
	}

	public void openAttachByReply(ReqFbkReply reply, ReqFbkReplyAttachMBean attachMBean) {
		attachMBean.init(reply);
		this.editReply = reply;
	}

}
