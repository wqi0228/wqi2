/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.view.to.search.query;

import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.vo.query.search.SearchQuery;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

/**
 * Search08MBean 查詢欄位
 *
 * @author kasim
 */
public class SearchQuery08 extends SearchQuery implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1213070785185656754L;
    @Getter
    @Setter
    /** 選擇的大類 */
    private String selectBigCategorySid;
    @Getter
    @Setter
    /** 類別組合(大類 sid) */
    private List<String> bigDataCateSids = Lists.newArrayList();
    @Getter
    @Setter
    /** 類別組合(中類 sid) */
    private List<String> middleDataCateSids = Lists.newArrayList();
    @Getter
    @Setter
    /** 類別組合(小類 sid) */
    private List<String> smallDataCateSids = Lists.newArrayList();
    @Getter
    @Setter
    /** 選擇的閱讀類型 */
    private String selectReadRecordType;
    @Getter
    @Setter
    /** 填單人員 */
    private String trCreatedUserName;
    @Getter
    @Setter
    /** 模糊搜尋 */
    private String fuzzyText;
    @Getter
    @Setter
    /** 待閱原因 */
    private List<String> readReason;
    @Getter
    @Setter
    /** 原型確認狀態 */
    private List<String> prototypeStatus;
    @Getter
    @Setter
    /** 時間切換的index */
    private Integer dateTypeIndex;
    @Getter
    @Setter
    /** 填單區間(起) */
    private Date startDate;
    @Getter
    @Setter
    /** 填單區間(訖) */
    private Date endDate;
    @Getter
    @Setter
    /** 是否為最新版次 */
    private Boolean isLatestEdition = Boolean.TRUE;
    @Getter
    @Setter
    /** 排序類型 */
    private String sortType;
    @Getter
    @Setter
    /** 轉發單位 */
    private List<String> forwardDepts = Lists.newArrayList();
    @Getter
    @Setter
    /** 緊急度 */
    private List<String> urgencyList;
    @Getter
    @Setter
    /** 需求單號 */
    private String requireNo;

    public SearchQuery08(ReportType reportType) {
        this.reportType = reportType;
    }

    /**
     * 清除
     *
     * @param requireDepts
     */
    public void clear() {
        this.init();
        this.initDefault();
    }

    /**
     * 初始化
     */
    private void init() {
    	// 共用查詢條件初始化
    	this.publicConditionInit();
    	
        this.selectBigCategorySid = null;
        this.bigDataCateSids = Lists.newArrayList();
        this.middleDataCateSids = Lists.newArrayList();
        this.smallDataCateSids = Lists.newArrayList();
        this.selectRequireStatusType = null;
        this.selectReadRecordType = null;
        this.trCreatedUserName = null;
        this.fuzzyText = null;
        this.readReason = Lists.newArrayList();
        this.clearAdvance();
    }

    /**
     * 清除進階選項
     */
    public void clearAdvance() {
        this.urgencyList = null;
        this.requireNo = null;
        this.forwardDepts.clear();
    }

    /**
     * 初始化報表預設值
     *
     * @param requireDepts
     */
    private void initDefault() {
        this.dateTypeIndex = 1;
        this.startDate = new DateTime(new Date()).minusMonths(1).toDate();
        this.endDate = new Date();
        this.isLatestEdition = Boolean.TRUE;
        this.sortType = SortType.CREATE_DESC.name();
        this.prototypeStatus = Lists.newArrayList(PtStatus.values()).stream()
                .map(each -> each.name())
                .collect(Collectors.toList());
        this.readReason = this.getDefaultByWaitReadReasonType().stream()
                .map(each -> each.name())
                .collect(Collectors.toList());
    }

    public List<WaitReadReasonType> getDefaultByWaitReadReasonType() {
        return Lists.newArrayList(WaitReadReasonType.PROTOTYPE_NOT_WAITREASON,
                WaitReadReasonType.PROTOTYPE_SIGN_PROCESS,
                WaitReadReasonType.PROTOTYPE_VERIFY_INVAILD,
                WaitReadReasonType.PROTOTYPE_APPROVE,
                WaitReadReasonType.PROTOTYPE_HAS_REPLY,
                WaitReadReasonType.PROTOTYPE_REDO,
                WaitReadReasonType.PROTOTYPE_FUNCTION_CONFORM);
    }

    /**
     * 上個月
     */
    public void changeDateIntervalPreMonth() {
        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate lastDate = new LocalDate(date).minusMonths(1);
        this.dateTypeIndex = 0;
        this.startDate = lastDate.dayOfMonth().withMinimumValue().toDate();
        this.endDate = lastDate.dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 本月份
     */
    public void changeDateIntervalThisMonth() {
        this.dateTypeIndex = 1;
        this.startDate = new LocalDate().dayOfMonth().withMinimumValue().toDate();
        this.endDate = new LocalDate().dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 下個月
     */
    public void changeDateIntervalNextMonth() {
        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate nextDate = new LocalDate(date).plusMonths(1);
        this.dateTypeIndex = 2;
        this.startDate = nextDate.dayOfMonth().withMinimumValue().toDate();
        this.endDate = nextDate.dayOfMonth().withMaximumValue().toDate();
    }

    /** 排序類型 */
    public enum SortType {
        CREATE_DESC("create_dt"),
        UPDATE_DESC("read_update_dt");

        @Getter
        private final String colName;

        SortType(String colName) {
            this.colName = colName;
        }
    }
}
