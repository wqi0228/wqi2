/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.setting.setting10;

import com.cy.tech.request.logic.service.sysbackend.SysMemInfoService;
import com.cy.tech.request.logic.vo.SysMemInfoTo;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 系統資源觀測
 *
 * @author shaun
 */
@NoArgsConstructor
@Controller
@Scope("view")
public class MemMonitorMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2329598129325176889L;

    @Autowired
    transient private SysMemInfoService sysinfoService;

    private final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    private final int MB = 1024 * 1024;
    /** 最大記錄數 */
    private final int MAX_REC = 180;//3H

    /** 畫面自動更新控制 */
    @Getter
    @Setter
    private Boolean autoUpdateSwtich;
    /** 節點顯示數量 */
    @Getter
    @Setter
    private Integer canShowMaxNode;
    /** 顯示節點模式 */
    @Getter
    @Setter
    private Integer showRecMode;
    /** 顯示模式資訊模式 */
    @Getter
    @Setter
    private String showModeSwitch;

    /** 即時資源圖 */
    @Getter
    private LineChartModel sysinfoImmediateModel;
    /** 記憶體記錄 */
    private List<SysMemInfoTo> userdMemory;
    private List<SysMemInfoTo> freeMemory;
    private List<SysMemInfoTo> totalMemory;

    @PostConstruct
    public void init() {
        this.initEnvVar();
        this.initFirstVar();
        this.showModeSwitchAction();
        this.recSysInfoByView();
    }

    /**
     * 初始化環境配置
     */
    private void initEnvVar() {
        autoUpdateSwtich = false;
        showModeSwitch = "history";
        showRecMode = 1;
        canShowMaxNode = 10;
    }

    /**
     * 初始化顯示配置
     */
    private void initFirstVar() {
        showModeSwitch = sysinfoService.getFreeMemory().size() > 0 ? "history" : "current";
        userdMemory = Lists.newArrayList();
        freeMemory = Lists.newArrayList();
        totalMemory = Lists.newArrayList();
        sysinfoImmediateModel = new LineChartModel();
    }

    public void showModeSwitchAction() {
        showRecMode = 1;
        canShowMaxNode = 10;
        this.initSysInfoModel(showModeSwitch.equals("history") ? "3小時內系統記憶體使用情況" : "當前系統記憶體使用情況");
        if (showModeSwitch.equals("current")) {
            autoUpdateSwtich = true;
        }
    }

    private void initSysInfoModel(String title) {
        Runtime runtime = Runtime.getRuntime();
        sysinfoImmediateModel.setTitle(title);
        sysinfoImmediateModel.setLegendPosition("e");
        sysinfoImmediateModel.setShowPointLabels(true);
        Axis yAxis = sysinfoImmediateModel.getAxis(AxisType.Y);
        yAxis.setLabel("Max Memory");
        yAxis.setMin(100);
        yAxis.setMax(runtime.maxMemory() / MB);

        sysinfoImmediateModel.getAxes().put(AxisType.X, new CategoryAxis("Time"));
    }

    public void recSysInfoByView() {
        this.recSysinfo();
        this.showSysInfoMem();
    }

    private void recSysinfo() {
        Runtime runtime = Runtime.getRuntime();
        Date time = new Date();
        userdMemory.add(new SysMemInfoTo(sdf.format(time), (runtime.totalMemory() - runtime.freeMemory()) / MB));
        freeMemory.add(new SysMemInfoTo(sdf.format(time), runtime.freeMemory() / MB));
        totalMemory.add(new SysMemInfoTo(sdf.format(time), runtime.totalMemory() / MB));
        if (userdMemory.size() > MAX_REC) {
            userdMemory.remove(0);
            freeMemory.remove(0);
            totalMemory.remove(0);
        }
    }

    public void showSysInfoMem() {
        sysinfoImmediateModel.clear();
        List<SysMemInfoTo> usedMem = showModeSwitch.equals("history") ? sysinfoService.getUserdMemory() : userdMemory;
        List<SysMemInfoTo> freeMem = showModeSwitch.equals("history") ? sysinfoService.getFreeMemory() : freeMemory;
        List<SysMemInfoTo> totalMem = showModeSwitch.equals("history") ? sysinfoService.getTotalMemory() : totalMemory;

        sysinfoImmediateModel.addSeries(createChartSeries("Used Memory", usedMem));
        sysinfoImmediateModel.addSeries(createChartSeries("Free Memory", freeMem));
        sysinfoImmediateModel.addSeries(createChartSeries("Total Memory", totalMem));
    }

    private LineChartSeries createChartSeries(String label, List<SysMemInfoTo> sysMemInfos) {
        LineChartSeries series = new LineChartSeries();
        series.setLabel(label);
        //依照模式移除多餘元素，且需從最後幾個元素開始取值
        if (showRecMode == null || canShowMaxNode == null) {
            this.initEnvVar();
        }
        if (showRecMode == 1) {
            int startShowIdx = sysMemInfos.size() > canShowMaxNode ? sysMemInfos.size() - canShowMaxNode : 0;
            sysMemInfos.stream().filter(each -> sysMemInfos.indexOf(each) >= startShowIdx)
                      .forEach(each -> series.set(each.getDate() + "", each.getMemory()));
        } else {
            List<SysMemInfoTo> show = Lists.newArrayList();
            for (int i = 0; i < sysMemInfos.size() - 1; i++) {
                if (i % showRecMode == 0) {
                    show.add(sysMemInfos.get(i));
                }
            }
            int startShowIdx = show.size() > canShowMaxNode ? show.size() - canShowMaxNode : 0;
            show.stream().filter(each -> show.indexOf(each) >= startShowIdx)
                      .forEach(each -> series.set(each.getDate() + "", each.getMemory()));
        }
        return series;
    }

    public SelectItem[] getViewIntervalItem() {
        if (showModeSwitch.equals("history")) {
            SelectItem[] items = new SelectItem[3];
            items[0] = new SelectItem(1, "1分鐘");
            items[1] = new SelectItem(5, "5分鐘");
            items[2] = new SelectItem(10, "10分鐘");
            return items;
        } else {
            SelectItem[] items = new SelectItem[4];
            items[0] = new SelectItem(1, "1秒");
            items[1] = new SelectItem(5, "5秒");
            items[2] = new SelectItem(10, "10秒");
            items[3] = new SelectItem(30, "30秒");
            return items;
        }
    }

}
