/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search33QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.view.Search33View;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.send.test.SendTestService;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.require.send.test.SendTestBottomMBean;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.view.vo.Search33QueryVO;
import com.cy.tech.request.web.controller.view.vo.Search33QueryVO.Search33Column;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.viewcomponent.treepker.TreePickerCallback;
import com.cy.work.viewcomponent.treepker.TreePickerComponent;
import com.cy.work.viewcomponent.treepker.helper.TreePickerDepHelper;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * QA待審核單據查詢
 *
 * @author marlow_chen
 */
@Controller
@Scope("view")
@Slf4j
public class Search33MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1162283279351516282L;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private WorkTestUpDownBean workTestUpDownBean;
    @Autowired
    transient private Search33QueryService search33QueryService;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private SendTestService sendTestService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private Require01MBean r01MBean;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private DisplayController displayController;

    @Getter
    @Autowired
    transient private SendTestBottomMBean sendTestBottomMBean;

    /** 所有的送測單 */
    @Getter
    @Setter
    private List<Search33View> queryItems;
    /** 所有的送測單 */
    @Getter
    @Setter
    private List<Search33View> filterList;

    /** 選擇的送測單 */
    @Getter
    @Setter
    private Search33View querySelection;

    /** 上下筆移動keeper */
    @Getter
    private Search33View queryKeeper;

    /** 切換模式 */
    @Getter
    private SwitchType switchType = SwitchType.CONTENT;

    /** 切換模式 - 全畫面狀態 */
    @Getter
    private SwitchType switchFullType = SwitchType.DETAIL;

    @Getter
    private final String dataTableId = "dtRequire";

    /** 所有填單單位(Filter下拉選單) */
    @Getter
    private List<String> createDeps;

    /** 是否閱讀(Filter下拉選單) */
    @Getter
    private List<String> isRead;

    /** 納入排程狀態(Filter下拉選單) */
    @Getter
    private List<String> qaScheduleStatus;

    @Getter
    @Setter
    private List<SelectItem> dateTypeItem;
    @Getter
    @Setter
    private Search33QueryVO queryVO = new Search33QueryVO();
    @Getter
    @Setter
    private String reasonCss;

    @PostConstruct
    public void init() {
        dateTypeItem = Lists.newArrayList();
        for (Search33Column column : Search33QueryVO.Search33Column.values()) {
            SelectItem selectItem = new SelectItem();
            selectItem.setLabel(column.getName());
            selectItem.setValue(column.getValue());
            dateTypeItem.add(selectItem);
        }

        this.initCreateDepSids();

        display.execute("doSearchData();");
    }

    public void search() {
        queryItems = this.findWithQuery();
        refreshFilterSelect();

        this.filterList = Lists.newArrayList(queryItems);
        display.resetDataTable(dataTableId);
    }

    /**
     * 建立dataTable Filter 下拉選單
     */
    private void refreshFilterSelect() {
        Set<String> allCreateDeps = new HashSet<String>();
        Set<String> allIsReads = new HashSet<String>();
        Set<String> allQaSchedule = new HashSet<String>();

        queryItems.forEach(each -> {
            allCreateDeps.add(each.getCreateDept());
            allIsReads.add(each.getReadRecordTypeDescr());
            allQaSchedule.add(each.getQaScheduleStatus());
        });

        this.createDeps = Lists.newArrayList(allCreateDeps);
        this.isRead = Lists.newArrayList(allIsReads);
        this.qaScheduleStatus = Lists.newArrayList(allQaSchedule);
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        queryVO.init();
        this.initCreateDepSids();
        this.search();
    }

    /**
     * 初始化填單單位
     */
    private void initCreateDepSids() {
        this.allSendTestDepSids = this.sendTestService.findAllCreateDepSids(SecurityFacade.getCompanySid());
        this.queryVO.setSendTestDepSids(allSendTestDepSids.stream()
                .filter(sid -> WkOrgUtils.isActive(sid))
                .collect(Collectors.toSet()));
    }

    private List<Search33View> findWithQuery() {
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT "
                + "t1.testinfo_sid,"
                + "t1.test_date,"
                + "t1.dep_sid,"
                + "t1.create_usr,"
                + "t1.testinfo_theme,"
                + "t1.qa_audit_status,"
                + "t1.testinfo_estimate_dt,"
                + "t1.qa_schedule_status,"
                + "t1.qa_audit_dt,"
                + "t1.create_dt,"
                + "t1.testinfo_source_no,"
                + "t1.expect_online_date,"
                + "t1.testinfo_no,"
                + "t1.testinfo_status,"
                + "t2.create_usr as approveUser, "
                + this.searchConditionSqlHelper.prepareReadRecordSelectColumn()

                + " FROM work_test_info t1  ");
        builder.append(""
                + "LEFT JOIN work_test_info_history t2 "
                + "       ON t1.testinfo_sid = t2.testinfo_sid "
                + "      AND t2.behavior IN ('QA_JOIN_SCHEDULE', 'QA_UNJOIN_SCHEDULE') ");

        builder.append(this.searchConditionSqlHelper.prepareSubFormReadRecordJoin(
                SecurityFacade.getUserSid(),
                FormType.WORKTESTSIGNINFO,
                "t1"));

        buildWorkTestInfoCondition(builder, parameters);

        builder.append(" ORDER BY t1.test_date ASC, t1.create_dt ASC");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.WORK_QA_REVIEW, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.WORK_QA_REVIEW, SecurityFacade.getUserSid());

        // 查詢
        List<Search33View> resultList = search33QueryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return resultList;
    }

    private void buildWorkTestInfoCondition(StringBuilder builder, Map<String, Object> parameters) {

        builder.append("WHERE t1.testinfo_source_type='TECH_REQUEST'");
        builder.append("  AND t1.comp_sid = " + SecurityFacade.getCompanySid() + "  ");

        // 送測區間或預計完成日
        if (queryVO.getDateIntervalVO().getStartDate() != null && queryVO.getDateIntervalVO().getEndDate() != null) {
            builder.append(" AND " + queryVO.getDateType() + " BETWEEN :startDate AND :endDate");
            parameters.put("startDate", helper.transStartDate(queryVO.getDateIntervalVO().getStartDate()));
            parameters.put("endDate", helper.transEndDate(queryVO.getDateIntervalVO().getEndDate()));
        }
        // 提交狀態
        if (StringUtils.isNotBlank(queryVO.getQaAuditStatus())) {
            if ("WAIT_APPROVE".equals(queryVO.getQaAuditStatus())) {
                builder.append(" AND t1.qa_audit_status=:qaAuditStatus AND t1.testinfo_status='SONGCE'");
            } else if ("APPROVED".equals(queryVO.getQaAuditStatus())) {
                builder.append(" AND t1.qa_audit_status=:qaAuditStatus");
            }
            parameters.put("qaAuditStatus", queryVO.getQaAuditStatus());
        } else {
            builder.append(" AND((t1.qa_audit_status = 'WAIT_APPROVE' AND t1.testinfo_status = 'SONGCE') OR t1.qa_audit_status = 'APPROVED' OR t1.qa_audit_status = 'UNNEEDED_APPROVE' )");
        }

        // ====================================
        // 填單單位
        // ====================================
        if (WkStringUtils.notEmpty(this.queryVO.getSendTestDepSids())) {
            // 非全選時才過濾
            if (!WkCommonUtils.compare(this.allSendTestDepSids, this.queryVO.getSendTestDepSids())) {
                String depSidsStr = this.queryVO.getSendTestDepSids().stream()
                        .map(sid -> sid + "")
                        .collect(Collectors.joining(", "));
                builder.append("  AND t1.dep_sid IN (" + depSidsStr + ")  ");
            }
        }
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            this.moveScreenTab();
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (Search33View) event.getObject();
        this.changeRequireContent(this.queryKeeper);
        this.moveScreenTab();
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Search33View view) {
        view.setReadRecordType(ReadRecordType.HAS_READ);
        Require require = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(require, loginBean.getUser(), RequireBottomTabType.SEND_TEST_INFO);
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Search33View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
        this.moveScreenTab();
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
        this.moveScreenTab();
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        helper.exportExcel(document, queryVO.getDateIntervalVO().getStartDate(), queryVO.getDateIntervalVO().getEndDate(), ReportType.WORK_QA_REVIEW);
    }

    public void moveScreenTab() {
        this.r01MBean.getBottomTabMBean().resetTabIdx(this.r01MBean);
        this.r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.SEND_TEST_INFO);

        List<String> sids = sendTestService.findSidsBySourceSid(this.r01MBean.getRequire().getSid());
        String indicateSid = queryKeeper.getSid();
        if (sids.indexOf(indicateSid) != -1) {
            display.execute("PF('st_acc_panel_layer_zero').select(" + sids.indexOf(indicateSid) + ");");
            for (int i = 0; i < sids.size(); i++) {
                if (i != sids.indexOf(indicateSid)) {
                    display.execute("PF('st_acc_panel_layer_zero').unselect(" + i + ");");
                }
            }
        }
    }

    /**
     * 送測主題開啟分頁
     * 
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void openUrlByLink(String widgetVar, String pageCount, Search33View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.reloadWorkTestUpDownBean();
    }

    /**
     * 重新載入送測單上下筆資訊
     */
    public void reloadWorkTestUpDownBean() {
        workTestUpDownBean.setCurrRow(queryKeeper.getTestInfoNo());
        workTestUpDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
        workTestUpDownBean.resetTabInfo(RequireBottomTabType.SEND_TEST_INFO, queryKeeper.getSid());
    }

    /**
     * 開啟分頁
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String widgetVar, String pageCount, Search33View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            display.update("headerTitle");
            display.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Search33View to) {
        querySelection = to;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
        upDownBean.resetTabInfo(RequireBottomTabType.SEND_TEST_INFO, queryKeeper.getSid());
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    public void workTestInfoRecordUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.highlightReportTo(widgetVar, pageCount, querySelection);
        this.reloadWorkTestUpDownBean();
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    public void workTestInfoRecordDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.highlightReportTo(widgetVar, pageCount, querySelection);
        this.reloadWorkTestUpDownBean();
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    /**
     * 納入排程
     * 
     * @param testInfo
     */
    public void joinSchedule(String testInfoNo, boolean updateTab) {
        sendTestBottomMBean.joinSchedule(testInfoNo, updateTab);

        updateDataTable(testInfoNo);
    }

    /**
     * 不納入排程
     * 
     * @param testInfo
     */
    public void unjoinSchedule(boolean updateTab) {
        sendTestBottomMBean.getEditInfoHistory().setReasonCss(reasonCss);
        sendTestBottomMBean.unjoinSchedule(updateTab);
        String testInfoNo = sendTestBottomMBean.getEditInfoHistory().getTestinfoNo();

        updateDataTable(testInfoNo);
        display.hidePfWidgetVar("search33_dlg");
    }

    /**
     * 如果查詢狀態為待審核, 將該筆record移除 (表示已審核
     * 如果查詢狀態為全部, 將更新該筆record
     * 
     * @param testInfoNo
     */
    private void updateDataTable(String testInfoNo) {
        try {
            if ("WAIT_APPROVE".equals(queryVO.getQaAuditStatus())) {
                queryItems.removeIf(view -> {
                    return view.getTestInfoNo().equals(testInfoNo);
                });
                filterList.removeIf(view -> {
                    return view.getTestInfoNo().equals(testInfoNo);
                });
                refreshFilterSelect();
            } else if (StringUtils.isBlank(queryVO.getQaAuditStatus())) {
                WorkTestInfo entity = sendTestService.findByTestinfoNo(testInfoNo);
                Search33View view = search33QueryService.convertToView(entity, loginBean.getUser());
                Integer index = queryItems.indexOf(view);
                queryItems.set(index, view);
                // filterList 需與 queryItems 同步操作
                filterList.set(index, view);
            }

        } catch (Exception e) {
            log.error("更新dataTable失敗:{}", e.getMessage());
        }
    }

    public void createEmptyHistoryByReason(String testInfoNo, String reason) {
        this.reasonCss = reason;
        sendTestBottomMBean.createEmptyHistoryByReason(testInfoNo, reason);
    }

    // ========================================================================
    // 填單單位樹
    // ========================================================================

    /** dialog name */
    @Getter
    protected final String TREE_PICKER_DLG_NAME = "TREE_PICKER_DLG_NAME" + this.getClass().getSimpleName();
    /** dialog 內容區 */
    @Getter
    protected final String TREE_PICKER_DLG_CONTENT = "TREE_PICKER_DLG_CONTENT" + this.getClass().getSimpleName();
    /** tree Picker commpoent ID */
    @Getter
    protected final String TREE_PICKER_COMPONENT_ID = "TREE_PICKER_COMPONENT_ID" + this.getClass().getSimpleName();

    /** TreePickerComponent */
    @Getter
    private transient TreePickerComponent treePicker;
    @Autowired
    private transient TreePickerDepHelper treePickerDepHelper;
    @Getter
    protected String treePickerConfirmUpdate;

    /**
     * 曾經開過送測單的單位
     */
    private Set<Integer> allSendTestDepSids;

    /**
     * @param typeStr
     */
    public void treepicker_openDialog() {

        // ====================================
        // 初始化選單
        // ====================================
        try {
            this.treePicker = new TreePickerComponent(
                    treePickerCallback,
                    "填單單位");
            this.treePicker.setShowInActiveItem(false);

            this.treePicker.rebuild();
        } catch (Exception e) {
            String errorMessage = "開啟【" + "填單單位" + "】設定視窗失敗";
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);
            return;
        }

        // ====================================
        // 畫面處理
        // ====================================
        // 更新 dialog 內容
        this.displayController.update(this.TREE_PICKER_DLG_CONTENT);
        // 顯示 dialog
        this.displayController.showPfWidgetVar(this.TREE_PICKER_DLG_NAME);
    }

    public void treePicker_confirm() {
        this.queryVO.setSendTestDepSids(this.treePicker.getSelectedItemIntegerSids());

        // ====================================
        // 畫面處理
        // ====================================
        // 顯示 dialog
        this.displayController.hidePfWidgetVar(this.TREE_PICKER_DLG_NAME);
    }

    /**
     * 樹選單 - callback 事件
     */
    private final TreePickerCallback treePickerCallback = new TreePickerCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = -1230591973323428848L;

        @Override
        public List<WkItem> prepareAllItems() throws Exception {

            return treePickerDepHelper.prepareDepItems(
                    SecurityFacade.getCompanyId(),
                    Lists.newArrayList(allSendTestDepSids));
        }

        /**
         * 準備已選擇項目
         * 
         * @return
         */
        @Override
        public List<String> prepareSelectedItemSids() throws Exception {
            if (WkStringUtils.isEmpty(queryVO.getSendTestDepSids())) {
                return Lists.newArrayList();
            }
            return queryVO.getSendTestDepSids().stream()
                    .map(sid -> String.valueOf(sid))
                    .collect(Collectors.toList());
        }

        /**
         * 準備鎖定項目
         * 
         * @return
         */
        @Override
        public List<String> prepareDisableItemSids() throws Exception {
            return Lists.newArrayList();
        }
    };
}
