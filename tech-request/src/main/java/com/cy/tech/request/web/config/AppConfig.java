package com.cy.tech.request.web.config;

import com.cy.commons.config.CommonsUtilConfig;
import com.cy.commons.web.scopes.ViewScope;
import com.google.common.collect.Maps;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import javax.naming.NamingException;
import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration("com.cy.tech.request.web.config.AppConfig")
@ComponentScan({ "com.cy.work.viewcomponent", "com.cy.tech.request.web" })
@PropertySources({
        @PropertySource("classpath:config.properties"),
        @PropertySource("classpath:common.properties")
})
@Import({
        SecurityConfig.class,
        com.cy.tech.request.logic.config.LogicConfig.class,
        com.cy.commons.web.config.AppConfig.class,
        CommonsUtilConfig.class })
@EnableScheduling
public class AppConfig {

    @Bean
    public static PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() {
        PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = new PropertyPlaceholderConfigurer();
        propertyPlaceholderConfigurer.setLocations(
                new Resource[] {
                        new ClassPathResource("config.properties"),
                        new ClassPathResource("common.properties")
                });
        propertyPlaceholderConfigurer.setIgnoreUnresolvablePlaceholders(true);
        return propertyPlaceholderConfigurer;
    }

    @Bean
    public static CustomScopeConfigurer customScopeConfigurer() {
        CustomScopeConfigurer customScopeConfigurer = new CustomScopeConfigurer();
        Map<String, Object> map = Maps.newHashMap();
        map.put("view", new ViewScope());
        customScopeConfigurer.setScopes(map);
        return customScopeConfigurer;
    }

    @Bean
    public static PropertyPlaceholderConfigurer initProp() throws NamingException, IOException {
        String configHome = System.getenv("FUSION_CONFIG_HOME");
        Path commonUtilPath = Paths.get(configHome, "commons-util.properties");
        Path techPath = Paths.get(configHome, "tech-request.properties");
        Path fbservicePath = Paths.get(configHome, "fbservice.properties");
        Path urlPath = Paths.get(configHome, "url.properties");
        Path customer = Paths.get(configHome, "tech-customer.properties");
        Path workCommonPath = Paths.get(configHome, "work-common.properties");
        Path bpmRest = Paths.get(configHome, "bpm-rest.properties");
        PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = new PropertyPlaceholderConfigurer();
        propertyPlaceholderConfigurer.setLocations(
                new PathResource[] {
                        new PathResource(commonUtilPath),
                        new PathResource(techPath),
                        new PathResource(fbservicePath),
                        new PathResource(urlPath),
                        new PathResource(workCommonPath),
                        new PathResource(customer),
                        new PathResource(bpmRest)
                });
        // 開啟後@Value如果有預設值則會永遠使用預設值需注意...
        propertyPlaceholderConfigurer.setIgnoreUnresolvablePlaceholders(true);
        return propertyPlaceholderConfigurer;
    }

}
