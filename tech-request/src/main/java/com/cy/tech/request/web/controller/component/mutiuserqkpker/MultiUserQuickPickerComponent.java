package com.cy.tech.request.web.controller.component.mutiuserqkpker;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgType;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.web.controller.component.mutiuserqkpker.model.MultiUserQuickPickerClientNodeData;
import com.cy.tech.request.web.controller.component.mutiuserqkpker.model.MultiUserQuickPickerNodeData;
import com.cy.tech.request.web.controller.component.mutiuserqkpker.model.MultiUserQuickPickerUserGroupData;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.ShowMode;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.viewcomponent.helper.OrgUserTreePickerCallBack;
import com.cy.work.viewcomponent.helper.OrgUserTreePickerHelper;
import com.cy.work.viewcomponent.helper.PickerComponentHelper;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Slf4j
public class MultiUserQuickPickerComponent implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7725983815175300951L;
    // ========================================================================
    // 服務區 (非spring 託管物件 @Autowired 無效)
    // ========================================================================
    private transient WkUserCache wkUserCache = WkUserCache.getInstance();
    private transient WkOrgCache wkOrgCache = WkOrgCache.getInstance();
    private transient OrgUserTreePickerHelper orgUserTreePickerHelper = WorkSpringContextHolder.getBean(OrgUserTreePickerHelper.class);
    private transient PickerComponentHelper pickerComponentHelper = WorkSpringContextHolder.getBean(PickerComponentHelper.class);
    private transient DisplayController displayController = DisplayController.getInstance();

    // ========================================================================
    // 常數
    // ========================================================================
    private static final String WKITEM_DATAKEY_NODE_DATE = "WKITEM_DATAKEY_NODE_DATE";

    // ========================================================================
    // for view
    // ========================================================================
    /** 元件ID */
    @Getter
    private String componentID;
    /** 是否開啟群組功能 */
    @Getter
    private boolean enableGroupMode = false;
    /** 已選區預設顯示模式 */
    @Getter
    private ShowMode targetAreaDefaultShowMode = ShowMode.LIST;

    /** 選擇樹根節點 */
    @Getter
    private TreeNode rootTreeNode;
    /** 要傳到client 的 node 對應資料 */
    @Getter
    private String allNodeDataJsonString = "";
    /** 由client端回傳的已選擇 userSid 資料 */
    @Getter
    @Setter
    private String selectedUserSidsJsonString;
    /** 群組項目 */
    @Getter
    @Setter
    private List<SelectItem> groupItems = Lists.newArrayList();
    /** 給 client 物件一個空個資料,用於渲染畫面, 實際顯示在 client 處理 */
    @Getter
    private final List<MultiUserQuickPickerClientNodeData> selectedUserNodeDatas = Lists.newArrayList();

    // ========================================================================
    // 內部變數
    // ========================================================================
    /** 所有項目 */
    private List<WkItem> allItems;
    /** 已選擇的 user */
    private List<String> selectedUserSids;
    /** user 節點標註 */
    @Getter
    private String userNodeIcon = "";
    /** 主管標記 */
    @Getter
    private String managerMark = "";
    /** 代理主管標記 */
    @Getter
    private String agentMark = "";
    /** call back function */
    private MultiUserQuickPickerCallback callback;

    // ========================================================================
    // 方法區
    // ========================================================================

    /**
     * 建構子
     * 
     * @param rootOrgID        頂層單位ID (公司 ex:TG)
     * @param componentID      元件 ID, 避免畫面中有多個實例時，ID 重複
     * @param selectedUserSids 已選擇的使用者
     * @param config           config
     * @param callback         目前僅開啟群組功能時會用到,未開啟時, 可傳 null 進來
     */
    public MultiUserQuickPickerComponent(
            String rootOrgID,
            String componentID,
            List<Integer> selectedUserSids,
            MultiUserQuickPickerConfig config,
            MultiUserQuickPickerCallback callback) {

        // ====================================
        // 防呆
        // ====================================
        if (WkStringUtils.isEmpty(componentID) || componentID.contains(" ")) {
            throw new SystemDevelopException(
                    "開發時期錯誤！傳入的元件ID不可為空，或有空白 componentID:[" + componentID + "]");
        }

        if (config == null) {
            throw new SystemDevelopException(
                    "開發時期錯誤！傳入的 MultiUserQuickPickerConfig 為空!");
        }

        if (config.isEnableGroupMode() && callback == null) {
            throw new SystemDevelopException(
                    "開發時期錯誤！開啟群組功能時, 需傳入 callback function 物件 (MultiUserQuickPickerCallback)!");
        }

        // ====================================
        // 取得預設值
        // ====================================
        this.componentID = componentID;
        this.targetAreaDefaultShowMode = config.getSelectedAreaDefaultShowMode();
        this.userNodeIcon = config.getUserNodeIcon();
        this.managerMark = config.getManagerMark();
        this.agentMark = config.getAgentMark();
        this.enableGroupMode = config.isEnableGroupMode();
        this.callback = callback;

        // 整理已選擇的 user
        // 1.去重複
        // 2.過濾錯誤資料
        // 3.轉string (client 處理用)
        if (selectedUserSids == null) {
            selectedUserSids = Lists.newArrayList();
        }
        this.selectedUserSids = selectedUserSids.stream()
                .distinct()
                .filter(userSid -> {
                    if (!WkUserCache.getInstance().isExsit(userSid)) {
                        log.warn("傳入的已選擇 user 查不到資料,已忽略! userSid:[" + userSid + "]");
                        return false;
                    }
                    return true;
                })
                .map(userSid -> userSid + "")
                .collect(Collectors.toList());

        // 複製一份
        // this.preSelectedUserSids =
        // this.selectedUserSids.stream().collect(Collectors.toList());

        // 預設展開
        List<String> defaultExpendItemSids = Lists.newArrayList();
        if (WkStringUtils.notEmpty(config.getDefaultExpandUserSids())) {
            defaultExpendItemSids = config.getDefaultExpandUserSids().stream()
                    .map(sid -> sid + "")
                    .collect(Collectors.toList());
        }
        // ====================================
        // 取得所有單位資料
        // ====================================
        // 頂層單位
        Org rootOrg = this.wkOrgCache.findById(rootOrgID);
        if (rootOrg == null) {
            throw new SystemDevelopException(
                    "傳入的頂層單位找不到 rootOrgID:[" + rootOrgID + "]");
        }
        // 取得所有單位
        Set<Integer> allDepSids = this.wkOrgCache.findAllChildSids(rootOrg.getSid());
        // 沒有下層部門，或者傳入頂層部門不為公司時，把頂層部門加入
        if (WkStringUtils.isEmpty(allDepSids) || OrgType.DEPARTMENT.equals(rootOrg.getType())) {
            allDepSids.add(rootOrg.getSid());
        }

        // ====================================
        // 建立 所有的 WkItem
        // ====================================
        // 取得頂層單位下所有的
        this.allItems = orgUserTreePickerHelper.prepareWkItems(
                allDepSids,
                this.orgUserTreePickerCallBack);

        // ====================================
        // 建立選單樹
        // ====================================
        this.rootTreeNode = new DefaultTreeNode();
        this.orgUserTreePickerHelper.buildTreeNode(
                this.rootTreeNode,
                this.allItems,
                false, // 未實做顯示停用單位
                "", // 不預設選擇
                defaultExpendItemSids,
                "", // 不輸入過濾字串-全不顯示
                orgUserTreePickerCallBack);

        // ====================================
        // 建立client端資料
        // ====================================
        // 建立待選節點資料
        this.allNodeDataJsonString = this.prepareNodeDataMapByRowKey(this.rootTreeNode, selectedUserSids);
        // log.info("before length:" + this.allNodeDataJsonString.length() );
        // base64 壓縮
        // this.allNodeDataJsonString = WkStringUtils.toBase64(this.allNodeDataJsonString);
        // log.info("after length:" + this.allNodeDataJsonString.length() );

        // 建立已選User資料
        this.selectedUserSidsJsonString = new Gson().toJson(this.selectedUserSids);
        if (WkStringUtils.isEmpty(this.selectedUserSidsJsonString)) {
            this.selectedUserSidsJsonString = "[]";
        }

        // ====================================
        // 建立群組選項
        // ====================================
        // 有開啟功能時才實作
        if (this.enableGroupMode) {
            // 呼叫傳入的實作方法 (未實作時會吐 SystemDevelopException)
            List<MultiUserQuickPickerUserGroupData> groupDatas = this.callback.prepareGroupItems();
            // 轉 selectitems
            this.groupItems = this.prepareGroupItems(groupDatas);
        }

        // ====================================
        // client 初始化
        // ====================================
        this.displayController.execute(componentID + "_pageLoad();");
        // this.displayController.execute("console.log('MultiUserQuickPickerComponent exec');");
    }

    /**
     * 建立待選節點資料 (包含部門+user)
     * 
     * @param rootNode
     * @return
     */
    private String prepareNodeDataMapByRowKey(TreeNode rootNode, List<Integer> selectedUserSids) {

        // 由 treeNode 取得而不由 allItems 取得的原因
        // 1. 需要由 treeNode取得最終的 data-rowkey , data-rowkey 需要在 tree node 樹狀結構長完後,
        // 才會取得最終結果值
        // 2. treeNode 為最終有使用到的 node ,會剃除掉沒使用的, 減少無用資料投放到 client

        // ====================================
        // 防呆
        // ====================================
        if (selectedUserSids == null) {
            selectedUserSids = Lists.newArrayList();
        }

        if (rootNode == null) {
            throw new SystemDevelopException("傳入rootNode為 null!");
        }

        // ====================================
        // 取得所有 tree node
        // ====================================
        List<TreeNode> treeNodelist = Lists.newArrayList();
        this.pickerComponentHelper.tree_NodeToList(treeNodelist, rootNode);

        // ====================================
        // 轉map
        // ====================================
        Map<String, MultiUserQuickPickerClientNodeData> nodeDataMapByRowkey = treeNodelist.stream()
                .collect(Collectors.toMap(
                        TreeNode::getRowKey,
                        treeNode -> {
                            WkItem wkItem = (WkItem) treeNode.getData();
                            MultiUserQuickPickerNodeData nodeData = (MultiUserQuickPickerNodeData) wkItem.getOtherObject().get(WKITEM_DATAKEY_NODE_DATE);
                            nodeData.setRowKey(treeNode.getRowKey());
                            return new MultiUserQuickPickerClientNodeData(nodeData);
                        }));

        // ====================================
        // 補因為停用而不在tree node 的 user
        // ====================================
        // 收集已經在資料集的 userSid
        Set<Integer> exsitUserSids = nodeDataMapByRowkey.values().stream()
                .filter(MultiUserQuickPickerClientNodeData::isUr)
                .map(nodedata -> Integer.parseInt(nodedata.getSid()))
                .collect(Collectors.toSet());

        // 去重複
        selectedUserSids = selectedUserSids.stream().distinct().collect(Collectors.toList());

        // 檢查目前不存在於資料集中的 user
        // 產生一份 user data 放入資料集
        for (Integer selectedUserSid : selectedUserSids) {
            if (!exsitUserSids.contains(selectedUserSid)) {

                // 查詢user 資料
                User user = this.wkUserCache.findBySid(selectedUserSid);
                if (user == null) {
                    log.warn("傳入的已選擇 user 查不到資料,已忽略! userSid:[" + selectedUserSid + "]");
                    continue;
                }

                // 因為不再樹節點, 所以沒有 rowkey , 產生一個不重複的虛擬 key
                // 無意義, 僅為資料傳到 client
                String virtualRowkey = "notTreeNode@" + selectedUserSid;

                // 產生一份資料
                nodeDataMapByRowkey.put(virtualRowkey, null);
                new MultiUserQuickPickerClientNodeData(
                        new MultiUserQuickPickerNodeData(user));
            }
        }

        // ====================================
        // 轉 JSON
        // ====================================
        String jsonString = new Gson().toJson(nodeDataMapByRowkey);
        if (WkStringUtils.isEmpty(jsonString)) {
            jsonString = "{}";
        }
        return jsonString;
    }

    /**
     * @param groupDatas
     * @return
     */
    private List<SelectItem> prepareGroupItems(List<MultiUserQuickPickerUserGroupData> groupDatas) {

        // ====================================
        // first item
        // ====================================
        List<SelectItem> selectItems = Lists.newArrayList();
        if (WkStringUtils.isEmpty(groupDatas)) {
            selectItems.add(new SelectItem("all", "-尚未建立群組-"));
            return selectItems;
        } else {
            selectItems.add(new SelectItem("all", "-請選擇-"));
        }

        // ====================================
        // 依據傳入群組資料, 建立選項
        // ====================================
        for (MultiUserQuickPickerUserGroupData groupData : groupDatas) {
            // 群組 user 資料, 轉成 JSON
            String value = "[]";
            if (WkStringUtils.notEmpty(groupData.getGroupUserSids())) {
                value = new Gson().toJson(groupData.getGroupUserSids());
            }
            // 加入項目
            selectItems.add(new SelectItem(value, groupData.getName()));
        }

        return selectItems;
    }

    /**
     * 實做『建立項目』方法
     */
    private final OrgUserTreePickerCallBack orgUserTreePickerCallBack = new OrgUserTreePickerCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -7491997442005089209L;

        /**
         * @param dep 單位資料檔
         * @return WkItem
         */
        @Override
        public WkItem createOrgItem(Org dep) {

            // 防呆
            if (dep == null) {
                log.warn("傳入 Org 為 null !");
                return null;
            }

            // 建立項目物件
            WkItem item = new WkItem(
                    dep.getSid() + "",
                    dep.getName(),
                    Activation.ACTIVE.equals(dep.getStatus()));

            // 建立client端使用資料
            item.getOtherObject().put(WKITEM_DATAKEY_NODE_DATE, new MultiUserQuickPickerNodeData(dep));

            return item;
        }

        /**
         * @param targetDep 附掛部門
         * @param user      使用者資料檔
         * @return WkItem
         */
        @Override
        public WkItem createUserItem(Org targetDep, User user) {

            // ====================================
            // 防呆
            // ====================================
            if (targetDep == null) {
                log.warn("傳入 targetDep 為 null !");
                return null;
            }
            if (user == null) {
                log.warn("傳入 user 為 null !");
                return null;
            }
            if (user.getPrimaryOrg() == null) {
                log.warn("傳入 user 為 無部門 userSid:[{}] !", user.getSid());
                return null;
            }

            // ====================================
            // 建立項目物件
            // ====================================
            WkItem item = new WkItem(
                    targetDep.getSid() + ":" + user.getSid(),
                    user.getName(),
                    Activation.ACTIVE.equals(user.getStatus()));

            // ====================================
            // 建立client端使用資料
            // ====================================
            MultiUserQuickPickerNodeData nodeData = new MultiUserQuickPickerNodeData(user);

            // 為部門主管
            if (orgUserTreePickerHelper.isOrgManager(targetDep, user)) {
                nodeData.setIsManager("1");

                // 主要單位不在傳入部門時，代表為兼(代)職主管, 加上兼職標示
                if (!WkCommonUtils.compareByStr(targetDep.getSid(), user.getPrimaryOrg().getSid())) {
                    nodeData.setIsAgent("1");
                }
            }

            // 加入
            item.getOtherObject().put(WKITEM_DATAKEY_NODE_DATE, nodeData);

            return item;
        }

        /**
         * 以關鍵字比對項目名稱
         * 
         * @param wkItem  WkItem
         * @param keyword 關鍵字
         * @return true:符合 , false:不符合
         */
        public boolean compareItemNameByKeyword(WkItem wkItem, String keyword) {

            for (String currItemName : wkItem.getItemNamesForSearch()) {
                // 去空白、轉大寫
                currItemName = WkStringUtils.safeTrim(currItemName).toUpperCase();
                if (currItemName.contains(keyword)) {
                    return true;
                }
            }
            return false;
        }
    };

    // ========================================================================
    // view 判斷
    // ========================================================================
    /**
     * 判斷
     * 
     * @param wkItem
     * @return
     */
    public boolean isNodeAttrByType(WkItem wkItem, String type) {
        // 防呆
        if (wkItem == null || wkItem.getOtherObject().get(WKITEM_DATAKEY_NODE_DATE) == null) {
            return false;
        }

        MultiUserQuickPickerNodeData nodeData = (MultiUserQuickPickerNodeData) wkItem.getOtherObject().get(WKITEM_DATAKEY_NODE_DATE);

        switch (type) {
        case "USER_NODE":
            return nodeData.isUserNode();
        case "MANAGER":
            return "1".equals(nodeData.getIsManager());
        case "AGENT":
            return "1".equals(nodeData.getIsAgent());
        default:

            break;
        }

        log.warn("傳入未定義的類型 type:[{}]", type);
        return false;
    }

    public void event_reloadItemGroup() {
        if (this.enableGroupMode) {
            // 呼叫傳入的實作方法 (未實作時會吐 SystemDevelopException)
            List<MultiUserQuickPickerUserGroupData> groupDatas = this.callback.prepareGroupItems();
            // 轉 selectitems
            this.groupItems = this.prepareGroupItems(groupDatas);
        }
    }

    // ========================================================================
    // client 即時更新已選清單
    // ========================================================================
    // private List<String> preSelectedUserSids = Lists.newArrayList();
    public void updateSelectedSids(ActionEvent event) {

        // 為空
        if (WkStringUtils.isEmpty(this.selectedUserSidsJsonString)
                || "[]".equals(this.selectedUserSidsJsonString)) {
            this.selectedUserSids = Lists.newArrayList();
            return;
        }
        // JSON to list
        this.selectedUserSids = new Gson().fromJson(
                this.selectedUserSidsJsonString,
                new TypeToken<List<String>>() {
                }.getType());
    }

    // ========================================================================
    // 取值
    // ========================================================================
    /**
     * 供外部取值, 取得目前元件中被選擇的 user
     * 
     * @return
     */
    public List<Integer> getSelecedtUserSids() {
        // 轉數字, 並去除重複(應該不會)
        return this.selectedUserSids.stream()
                .distinct()
                .map(userSid -> Integer.parseInt(userSid))
                .collect(Collectors.toList());
    }
}
