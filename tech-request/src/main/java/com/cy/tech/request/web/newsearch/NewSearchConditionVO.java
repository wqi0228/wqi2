package com.cy.tech.request.web.newsearch;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;

import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.enums.RequireFinishCodeType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 *
 */
@Slf4j
@NoArgsConstructor
public class NewSearchConditionVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7996902045884273292L;

    // ========================================================================
    // 必要限定過濾欄位 (內部產生的限制條件)
    // ========================================================================
    /**
     * 限定查詢的需求 sid
     */
    @Getter
    @Setter
    private List<String> mustFilter_requireSids;

    // ========================================================================
    // 頁面查詢條件
    // ========================================================================
    /**
     * 需求單號
     */
    @Getter
    @Setter
    private String requireNo;

    /**
     * 需求大類
     */
    @Getter
    @Setter
    private String bigCategorySid;

    /**
     * 類別組合
     */
    @Getter
    @Setter
    private Set<String> categoryCollectionSids;

    /**
     * 頁面用 分派/通知類型
     */
    @Getter
    @Setter
    private AssignSendType assignSendType;

    /**
     * 頁面用 分派/通知單位
     */
    @Getter
    @Setter
    private Set<Integer> assignSendDepSidsFromView = Sets.newHashSet();

    /**
     * 要過濾的分派單位 (應為限制查詢條件 + 畫面選擇條件)
     */
    @Getter
    @Setter
    private Set<Integer> filterAssignDepSids = Sets.newHashSet();

    /**
     * 要過濾的通知單位
     */
    @Getter
    @Setter
    private Set<Integer> filterSendDepSids = Sets.newHashSet();

    /**
     * 轉寄部門
     */
    @Getter
    @Setter
    private Set<Integer> forwardDepSids = Sets.newHashSet();;

    /**
     * 結案狀態
     */
    @Getter
    @Setter
    private List<String> selectedColseCode = Lists.newArrayList();

    /**
     * 完成碼
     */
    @Getter
    @Setter
    private List<String> selectFinishCode = Lists.newArrayList();

    /**
     * 模糊搜尋
     */
    @Getter
    @Setter
    private String fuzzyText;

    /**
     * 待閱原因 (預設全選)
     */
    @Getter
    @Setter
    private List<String> selectReqToBeReadTypes = Lists.newArrayList(
            Lists.newArrayList(ReqToBeReadType.values()).stream()
                    .map(item -> item.name())
                    .collect(Collectors.toList()));

    /**
     * 是否閱讀
     */
    @Getter
    @Setter
    private ReadRecordType selectReadRecordType;

    /**
     * 緊急度
     */
    @Getter
    @Setter
    private List<String> urgencyTypes;

    /**
     * 製作進度
     */
    @Getter
    @Setter
    private RequireStatusType requireStatus;

    /**
     * 檢查項目 (預設全選) REQ-1456
     */
    @Getter
    @Setter
    protected List<RequireCheckItemType> checkItemTypes = Lists.newArrayList(
            Lists.newArrayList(RequireCheckItemType.values()).stream()
            .collect(Collectors.toList()));

    // ========================================================================
    // 需求完成碼
    // ========================================================================
    /**
     * 查詢選項:完成碼類型
     */
    @Getter
    private final List<SelectItem> selectItems_requireFinishCodeTypes = Lists.newArrayList(
            new SelectItem(RequireFinishCodeType.INCOMPLETE.getCode(), RequireFinishCodeType.INCOMPLETE.getCodeName()),
            new SelectItem(RequireFinishCodeType.TERMINATED.getCode(), RequireFinishCodeType.TERMINATED.getCodeName()),
            new SelectItem(RequireFinishCodeType.COMPLETE.getCode() + "," + RequireFinishCodeType.HALF_COMPLETE.getCode(),
                    RequireFinishCodeType.COMPLETE.getCodeName()));

    /**
     * 需求完成碼 (預設全選)
     */
    @Getter
    @Setter
    private List<String> reqFinishCodeForView = Lists.newArrayList(
            this.selectItems_requireFinishCodeTypes.stream()
                    .map(item -> item.getValue() + "")
                    .collect(Collectors.toList()));

    /**
     * 是否無須過濾 完成碼類型
     * 
     * @return
     */
    public boolean isSkipFilterReqFinishCode() {
        if (WkStringUtils.isEmpty(this.reqFinishCodeForView)) {
            return false;
        }
        return reqFinishCodeForView.size() == selectItems_requireFinishCodeTypes.size();
    }

    /**
     * 所選擇的完成碼類型
     * 
     * @return
     */
    public List<String> getReqFinishCode() {
        Set<String> results = Sets.newHashSet();
        if (WkStringUtils.notEmpty(this.reqFinishCodeForView)) {
            for (String codeStr : this.reqFinishCodeForView) {
                // 已完成項目有兩種 Y,n, 故需分割
                results.addAll(Lists.newArrayList(WkStringUtils.safeTrim(codeStr).split(",")));
            }
        }
        return Lists.newArrayList(results);
    }

    // ========================================================================
    // 日期相關
    // ========================================================================
    /**
     * 選擇的查詢日期類別
     */
    @Getter
    @Setter
    private NewSearchConditionDateRangeType dateRangeType = NewSearchConditionDateRangeType.UPDATE_DATE;

    /**
     * 查詢區間 - 起始日 - 分派
     */
    @Getter
    @Setter
    private Date assignStartDate;

    /**
     * 查詢區間 - 結束日 - 分派
     */
    @Getter
    @Setter
    private Date assignEndDate;

    /**
     * 查詢區間 - 起始日 - 通知
     */
    @Getter
    @Setter
    private Date noticeStartDate;

    /**
     * 查詢區間 - 結束日 - 通知
     */
    @Getter
    @Setter
    private Date noticeEndDate;

    /**
     * 查詢區間 - 起始日 - 異動
     */
    @Getter
    @Setter
    private Date updateStartDate;

    /**
     * 查詢區間 - 結束日 - 異動
     */
    @Getter
    @Setter
    private Date updateEndDate;

    /**
     * @param date
     */
    public void setStartDate(Date date) {
        this.assignStartDate = null;
        this.noticeStartDate = null;
        this.updateStartDate = null;

        switch (dateRangeType) {
        case ASSIGN_DATE:
            this.assignStartDate = date;
            break;
        case NOTICE_DATE:
            this.noticeStartDate = date;
            break;
        case UPDATE_DATE:
            this.updateStartDate = date;
            break;
        default:
            log.error("未實做的日期區間類型:" + dateRangeType);
            break;
        }
    }

    /**
     * @return
     */
    public Date getStartDate() {
        this.assignStartDate = null;
        this.noticeStartDate = null;
        this.updateStartDate = null;

        switch (dateRangeType) {
        case ASSIGN_DATE:
            return this.assignStartDate;
        case NOTICE_DATE:
            return this.noticeStartDate;
        case UPDATE_DATE:
            return updateStartDate;
        default:
            log.error("未實做的日期區間類型:" + dateRangeType);
            break;
        }
        return null;
    }

    /**
     * @param date
     */
    public void setEndDate(Date date) {
        this.assignEndDate = null;
        this.noticeEndDate = null;
        this.updateEndDate = null;

        switch (dateRangeType) {
        case ASSIGN_DATE:
            this.assignEndDate = date;
            break;
        case NOTICE_DATE:
            this.noticeEndDate = date;
            break;
        case UPDATE_DATE:
            this.updateEndDate = date;
            break;
        default:
            log.error("未實做的日期區間類型:" + dateRangeType);
            break;
        }
    }

    /**
     * @return
     */
    public Date getEndDate() {
        this.assignEndDate = null;
        this.noticeEndDate = null;
        this.updateEndDate = null;

        switch (dateRangeType) {
        case ASSIGN_DATE:
            return this.assignEndDate;
        case NOTICE_DATE:
            return this.noticeEndDate;
        case UPDATE_DATE:
            return updateEndDate;
        default:
            log.error("未實做的日期區間類型:" + dateRangeType);
            break;
        }
        return null;
    }

}
