/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;

import org.apache.commons.lang.StringUtils;
import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.util.FusionUrlServiceUtils;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.PropKeyType;
import com.cy.tech.request.logic.service.AlertInboxService;
import com.cy.tech.request.logic.service.FormViewUpdateService;
import com.cy.tech.request.logic.service.LockService;
import com.cy.tech.request.logic.service.RequireProcessCloseRollbackService;
import com.cy.tech.request.logic.service.RequireProcessCloseService;
import com.cy.tech.request.logic.service.RequireProcessCompleteService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.RequireTraceService;
import com.cy.tech.request.logic.service.TrSubNoticeInfoService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.customer.ReqWorkCustomerHelper;
import com.cy.tech.request.logic.service.onpg.OnpgService;
import com.cy.tech.request.logic.service.othset.OthSetService;
import com.cy.tech.request.logic.service.pmis.PmisHelper;
import com.cy.tech.request.logic.service.pt.PtService;
import com.cy.tech.request.logic.service.send.test.SendTestService;
import com.cy.tech.request.logic.vo.TrSubNoticeInfoTo;
import com.cy.tech.request.vo.enums.ExtraSettingType;
import com.cy.tech.request.vo.enums.InboxType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.enums.SubNoticeType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireTrace;
import com.cy.tech.request.vo.require.setting.RequireExtraSetting;
import com.cy.tech.request.vo.template.CategoryKeyMapping;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.web.anew.config.ReqWebConstants;
import com.cy.tech.request.web.callback.UpdateCallBack;
import com.cy.tech.request.web.controller.component.reqconfirm.ReqConfirmClientHelper;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.logic.component.ReadLogicComponent;
import com.cy.tech.request.web.controller.require.feedback.ReqFbkReplyAttachMBean;
import com.cy.tech.request.web.controller.require.inbox.InboxInstructionBottomMBean;
import com.cy.tech.request.web.controller.require.inbox.InboxPersonalBottomMBean;
import com.cy.tech.request.web.controller.require.inbox.InboxReportBottomMBean;
import com.cy.tech.request.web.controller.require.onpg.OnpgBottomMBean;
import com.cy.tech.request.web.controller.require.othset.OthSetAttachMBean;
import com.cy.tech.request.web.controller.require.othset.OthSetBottomMBean;
import com.cy.tech.request.web.controller.require.othset.OthSetHistoryAttachMBean;
import com.cy.tech.request.web.controller.require.pt.PtAttachMBean;
import com.cy.tech.request.web.controller.require.pt.PtBottomMBean;
import com.cy.tech.request.web.controller.require.pt.PtBpmMBean;
import com.cy.tech.request.web.controller.require.pt.PtHistoryAttachMBean;
import com.cy.tech.request.web.controller.require.send.test.SendTestAttachMBean;
import com.cy.tech.request.web.controller.require.send.test.SendTestBottomMBean;
import com.cy.tech.request.web.controller.require.send.test.SendTestBpmMBean;
import com.cy.tech.request.web.controller.require.send.test.SendTestHistoryAttachMBean;
import com.cy.tech.request.web.controller.search.TableUpDownBean;
import com.cy.tech.request.web.controller.search.WorkTestUpDownBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.view.component.OthSetSettingViewComponents;
import com.cy.tech.request.web.controller.view.component.RollBackFinishComponent;
import com.cy.tech.request.web.controller.view.component.WorkInboxComponent;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.listener.TabLoadCallBack;
import com.cy.tech.request.web.pf.utils.ConfirmCallbackDialogController;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.customer.vo.WorkCustomer;
import com.cy.work.customer.vo.enums.CustomerType;
import com.cy.work.customer.vo.enums.EnableType;
import com.cy.work.mapp.create.trans.vo.MappCreateTrans;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 需求單控制畫面切換口
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope("view")
public class Require01MBean implements IRequireLoad, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7117044952738499793L;
    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private ReqWebConstants reqWebConstants;
    @Autowired
    transient private TableUpDownBean tableUpDownBean;
    @Autowired
    transient private WorkTestUpDownBean workTestUpDownBean;
    @Autowired
    transient private FormViewUpdateService formViewUpdateService;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private RequireProcessCompleteService requireProcessCompleteService;
    @Autowired
    transient private RequireTraceService traceService;
    @Autowired
    transient private LockService lockService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private PtService ptService;
    @Autowired
    transient private SendTestService stService;
    @Autowired
    transient private OnpgService opService;
    @Autowired
    transient private OthSetService osService;
    @Autowired
    transient private AlertInboxService inboxService;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private URLService urlService;
    /** 初始化控制 */
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    private TrSubNoticeInfoService subNoticeInfoService;
    @Autowired
    private RequireCategoryTreeMBean requireCategoryTreeMBean;
    @Autowired
    transient private ReqWorkCustomerHelper reqWorkCustomerHelper;
    @Autowired
    transient private PmisHelper pmisHelper;
    @Autowired
    transient private RequireProcessCloseService requireProcessCloseService;
    @Autowired
    transient private RequireProcessCloseRollbackService requireProcessCloseRollbackService;
    @Autowired
    @Getter
    private ConfirmCallbackDialogController confirmCallbackDialogController;
    @Autowired
    @Getter
    private DisplayController displayController;

    // ========================================================================
    // 其他 MBean
    // ========================================================================
    @Autowired
    @Getter
    transient private RequireCheckItemMBean requireCheckItemMBean;

    /** 按鍵控制 */
    @Value("#{requireTitleBtnMBean}")
    @Getter
    private RequireTitleBtnMBean titleBtnMBean;
    /** 需求單下方頁籤控制 */
    @Value("#{requireBottomTabMBean}")
    @Getter
    private RequireBottomTabMBean bottomTabMBean;
    /** 需求單位流程控制 */
    @Value("#{requireBpmMBean}")
    @Getter
    private RequireBpmMBean reqBpmMBean;
    /**
     * 頁簽：分派/通知資訊
     */
    @Setter
    @Getter
    @Autowired
    private AssignSendInfoComponent assignInfo;

    /** 追蹤控制 */
    @Value("#{requireTraceMBean}")
    @Getter
    private RequireTraceMBean traceMBean;
    /** 需求單需求資訊回覆補充控制 */
    @Value("#{requireAddInfoMBean}")
    @Getter
    private RequireAddInfoMBean addInfoMBean;
    /** 需求單需求資訊回覆補充附加檔案控制 */
    @Value("#{reqFbkAttachMBean}")
    @Getter
    private ReqFbkAttachMBean reqFbkAttachMBean;
    /** 需求單需求資訊補充回覆附加檔案控制 */
    @Value("#{reqFbkReplyAttachMBean}")
    @Getter
    private ReqFbkReplyAttachMBean reqFbkReplyAttachMBean;
    /** 附加檔案控制 */
    @Value("#{reqAttachMBean}")
    @Getter
    private ReqAttachMBean reqAttachMBean;
    /** 模版控制 */
    @Value("#{requireTemplateFieldMBean}")
    @Getter
    private RequireTemplateFieldMBean templateMBean;
    /** 關聯頁籤 */
    @Value("#{requireGroupLinkMBean}")
    @Getter
    private RequireGroupLinkMBean groupLinkMBean;
    /** 關聯控制 */
    @Value("#{relevanceMBean}")
    @Getter
    private RelevanceMBean relevanceMBean;
    /** Fogbugz控制 */
    @Value("#{fogbugzMBean}")
    @Getter
    private FogbugzMBean fogbugzMBean;
    /** 收藏夾 控制 */
    @Value("#{favoriteMBean}")
    @Getter
    private FavoriteMBean favoriteMBean;
    /** 原型確認頁籤控制 */
    @Value("#{ptBottomMBean}")
    @Getter
    private PtBottomMBean ptBottomMBean;
    /** 原型確認流程控制 */
    @Value("#{ptBpmMBean}")
    @Getter
    private PtBpmMBean ptBpmMBean;
    /** 原型確認附加檔案控制 */
    @Value("#{ptAttachMBean}")
    @Getter
    private PtAttachMBean ptAttachMBean;
    /** 原型確認附加檔案控制 - 歷程 */
    @Value("#{ptHistoryAttachMBean}")
    @Getter
    private PtHistoryAttachMBean ptHistoryAttachMBean;
    /** 送測設定控制 */
    // @Value("#{sendTestSettingMBean}")
    // @Getter
    // private SendTestSettingMBean sendTestSettingMBean;
    /** 送測頁籤控制 */
    @Value("#{sendTestBottomMBean}")
    @Getter
    private SendTestBottomMBean sendTestBottomMBean;
    /** 送測流程控制 */
    @Value("#{sendTestBpmMBean}")
    @Getter
    private SendTestBpmMBean sendTestBpmMBean;
    /** 送測附加檔案控制 */
    @Value("#{sendTestAttachMBean}")
    @Getter
    private SendTestAttachMBean sendTestAttachMBean;
    /** 送測附加檔案控制 - 歷程 */
    @Value("#{sendTestHistoryAttachMBean}")
    @Getter
    private SendTestHistoryAttachMBean sendTestHistoryAttachMBean;
    /** ON程式頁籤控制 */
    @Value("#{onpgBottomMBean}")
    @Getter
    private OnpgBottomMBean onpgBottomMBean;
    /** 其它設定資訊設定控制 */
    // @Value("#{othSetSettingMBean}")
    // @Getter
    // private OthSetSettingMBean othSetSettingMBean;
    /** 其它設定資訊附加檔案控制 */
    @Value("#{othSetAttachMBean}")
    @Getter
    private OthSetAttachMBean othSetAttachMBean;
    /** 其它設定資訊附加檔案控制 - 歷程 */
    @Value("#{othSetHistoryAttachMBean}")
    @Getter
    private OthSetHistoryAttachMBean othSetHistoryAttachMBean;
    /** 其它設定資訊頁籤控制 */
    @Value("#{othSetBottomMBean}")
    @Getter
    private OthSetBottomMBean othSetBottomMBean;
    /** 追蹤 控制 */
    @Value("#{traceMBean}")
    @Getter
    private TraceMBean traceActionMBean;
    /** 收件夾 - 收呈報 */
    @Value("#{inboxReportBottomMBean}")
    @Getter
    private InboxReportBottomMBean ixReportMBean;
    /** 收件夾 - 收個人 */
    @Value("#{inboxPersonalBottomMBean}")
    @Getter
    private InboxPersonalBottomMBean ixPersonalMBean;
    /** 收件夾 - 收指示 */
    @Value("#{inboxInstructionBottomMBean}")
    @Getter
    private InboxInstructionBottomMBean ixInstructionMBean;
    @Getter
    private WorkInboxComponent workInboxComponent;

    // ========================================================================
    // 變數
    // ========================================================================
    /** 頁面處理狀態 */
    @Getter
    @Setter
    private RequireStep setp = RequireStep.NEW_IN_TREE;
    /** 是否為需求單管理員編輯 */
    @Getter
    private boolean isEditByAdmin = false;
    /** 是否為剛建立草稿時狀態(判斷是否在require01.xhtml用) */
    @Getter
    private Boolean isDraftCreateAfter = false;
    /** 是否為剛提交狀態 (判斷上下筆用) */
    @Getter
    private Boolean isDraftSubmitAfter = false;
    /** 是否為案件單轉需求單ON程式 */
    @Getter
    private Boolean isIssueCreateMode = false;
    @Getter
    private String issueCreateMappKeySid;
    @Getter
    /** 案件單轉入onpg 轉入sid */
    private String issueCreateOnpgKeySid;
    /** 需求單本體 */
    @Getter
    @Setter
    private Require require;
    /** 需求單備份 */
    @Getter
    @Setter
    private Require backupReq;
    /** 警告訊息 */
    @Getter
    private String warningMsg;
    /** 提醒訊息 */
    @Getter
    private String noticeMsg;
    /** 提示訊息 */
    @Getter
    private String promptMsg;

    /** 結案 | 需求完成 trace */
    @Getter
    @Setter
    private RequireTrace requireTrace;
    /** 外部連結觸發顯示功能鍵參數 (單據簽核作業) */
    @Getter
    private Boolean showTitleBtn = Boolean.TRUE;
    /** 外部連結觸發參數 - 上下筆切換 */
    transient private Map<String, Object> urlPara;
    /** 廳主&客戶控制渲染下拉選單(dialog部份) */
    @Getter
    @Setter
    private Boolean renderDlgList;

    @Getter
    @Setter
    private Boolean forceCloce = false;

    @Getter
    private OthSetSettingViewComponents othSetSettingViewComponents;
    @Autowired
    private ReadLogicComponent readLogicComponent;
    @Getter
    private RollBackFinishComponent rollBackFinishComponent;

    @Getter
    /** 子程序 通知異動紀錄 */
    private List<TrSubNoticeInfoTo> subNoticeInfoTos;

    // ========================================================================
    // 變數
    // ========================================================================
    @Override
    public void initByNewRequire() {
        require = requireService.createEmptyReq(requireCategoryTreeMBean.getSmallCategoryDefaultDueDay());
        setp = RequireStep.NEW_IN_TREE;
    }

    /**
     * 複製
     *
     * @param reqSid
     */
    @Override
    public void initByCopy(Require source) {
        this.require = requireService.createEmptyReq(requireCategoryTreeMBean.getSmallCategoryDefaultDueDay());
        setp = RequireStep.NEW_IN_PAGE;
        display.execute("copyMode();");
    }

    @Override
    public void initByIssueCreate(MappCreateTrans trans) {
        require = requireService.createEmptyReq(requireCategoryTreeMBean.getSmallCategoryDefaultDueDay());
        WorkCustomer customer = reqWorkCustomerHelper.findByEnableAndCustomerTypeAndAlias(EnableType.ENABLE, CustomerType.INNER_VIRTUAL_CUSTOMER, "內部需求流程");
        require.setCustomer(customer);
        require.setAuthor(customer);

        // REQ-1440 為『內部需求』時，主責單位+負責人員預設為【開單者】
        require.setInChargeDep(SecurityFacade.getPrimaryOrgSid());
        require.setInChargeUsr(SecurityFacade.getUserSid());

        setp = RequireStep.NEW_IN_PAGE;
        isIssueCreateMode = true;
        issueCreateMappKeySid = Faces.getRequestParameterMap().get(reqWebConstants.getFromIssueCreateKey());
        display.execute(reqWebConstants.getFunctionNameByIssueCreate());
    }

    @Override
    public void initByIssueCreateOnpg(MappCreateTrans trans) {
        Require r = requireService.findByReqSid(trans.getTargetSid());
        if (r == null) {
            return;
        }

        this.initByUrl(r.getRequireNo());
        this.issueCreateOnpgKeySid = trans.getSid();
        display.execute(reqWebConstants.getFunctionNameByTransOnpg());
    }

    @Override
    public void initByUrl(String sourceNo) {
        if (!requireService.isExistNo(sourceNo)) {
            if (sourceNo.contains(URLService.ILLEAL_ACCESS)) {
                this.reloadToIllegalPage(sourceNo.replace(URLService.ILLEAL_ACCESS, ""));
                return;
            }
            this.reloadNewPage(sourceNo);
            return;
        }
        this.setp = RequireStep.LOAD_IN_PAGE;

        this.require = requireService.findByReqNo(sourceNo);
        this.initByTable(require);
        display.execute("replaceUrlNotNavigation('" + rebindUrl(sourceNo) + "');");
        if (requireService.checkReqTempNotify(loginBean.getUserSId(), sourceNo)) {
            this.messageCallBack.showMessage("此需求單已經被反需求完成");
        }

    }

    @Override
    public void initByTable(Require source) {
        this.require = source;
        popupRollbackPrompt();
        if (require.getExtraSettings() != null && require.getExtraSettings().size() != 0) {
            Map<String, String> map = require.getExtraSettings().stream()
                    .collect(Collectors.toMap(RequireExtraSetting::getCode, RequireExtraSetting::getValue));
            if (map.get(ExtraSettingType.SABA_ENABLED.getCode()) != null) {
                require.setIsNcsDone("Y".equals(map.get(ExtraSettingType.NCS_CONFIRM.getCode())));
            }
        }
        // 載入(其他設定資訊)資料

        setp = RequireStep.LOAD_IN_PAGE;
        try {
            readLogicComponent.readWorkInBox(this.require.getSid(), loginBean.getUser().getSid(), loginBean.getDep().getSid());
            formViewUpdateService.updateReqReadRecord(this.require, loginBean.getUser());
        } catch (Exception e) {
            log.error("更新需求單閱讀記錄失敗。" + e.getMessage(), e);
        }
        this.othSetSettingViewComponents.loadData(require.getSid());
        this.workInboxComponent.loadData(require.getSid());
        this.checkTabUrlAttr();
        tableUpDownBean.clearCurrRow();

        if (requireService.checkReqTempNotify(loginBean.getUserSId(), source.getRequireNo())) {
            this.messageCallBack.showMessage("此需求單已經被反需求完成");
        }
    }

    @PostConstruct
    public void init() {

        try {
            // 新建草稿後將轉向require06.xhtml，如果在require01.xhtml底下的iframe開啟require06.xhtml進行刪除時，需靠此變數辨識後續動作
            if (Faces.getRequestParameterMap().containsKey("isDraftCreateAfter")) {
                isDraftCreateAfter = true;
            }
            // 草稿提交辨識用
            if (Faces.getRequestParameterMap().containsKey("isDraftSubmitAfter")) {
                isDraftSubmitAfter = true;
            }

            requireTrace = new RequireTrace();
            initComponents();

            if (Faces.getRequest() == null) {
                return;
            }

            this.checkTitleModeByUrlParam();

            if (loadManager.checkLoad(this)) {
                popupRollbackPrompt();
                return;
            }

            if (!Faces.getRequestURI().contains("search")
                    && !Faces.getRequestURI().contains("home")
                    && !Faces.getRequestURI().contains("require04")) {
                this.reloadNewPage("");
            }
        } catch (Exception e) {
            log.error("Require01MBean 初始化失敗!" + e.getMessage(), e);
            MessagesUtils.showError("初始化失敗!");
        }

        // this.reqConfirmDepPanelComponent.init(this);
    }

    private void initComponents() {
        String loginUserId = SecurityFacade.getUserId();
        String companyId = SecurityFacade.getCompanyId();
        User user = WkUserCache.getInstance().findById(loginUserId);
        Org companyOrg = WkOrgCache.getInstance().findById(companyId);
        othSetSettingViewComponents = new OthSetSettingViewComponents(user.getSid(), user.getPrimaryOrg().getSid(),
                companyOrg.getSid(), tabLoadCallBack, messageCallBack);
        workInboxComponent = new WorkInboxComponent(user.getSid(), updateCallBack, messageCallBack);
        rollBackFinishComponent = new RollBackFinishComponent(tabLoadCallBack, messageCallBack);

        this.requireCheckItemMBean.init();
    }

    @Getter
    private final UpdateCallBack updateCallBack = new UpdateCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 1226370620693367902L;

        @Override
        public void doUpdateData() {
            workInboxComponent.loadData();
            DisplayController.getInstance().update("viewPanelBottomInfoTabId");
        }
    };

    /**
     * 檢查是否為portal站台最新消息連結 <BR/>
     * form portal Adm30MBean onRowSelect call by jason_h
     */
    private void checkTitleModeByUrlParam() {
        // form-sign old url from tr-rest
        if (Faces.getRequestParameterMap().containsKey(URLService.URLServiceAttr.URL_ATTR_SHOW_TITLE_BTN.getAttr())) {
            showTitleBtn = Boolean.valueOf(Faces.getRequestParameterMap().get(URLService.URLServiceAttr.URL_ATTR_SHOW_TITLE_BTN.getAttr()));
        }
        // form-sign url from form-sign-web
        if (Faces.getRequestParameterMap().containsKey(URLService.URLServiceAttr.URL_ATTR_SHOW_TITLE_BTN_FROM_FORMSIGN.getAttr())) {
            String formSigningMode = Faces.getRequestParameterMap().get(URLService.URLServiceAttr.URL_ATTR_SHOW_TITLE_BTN_FROM_FORMSIGN.getAttr());
            showTitleBtn = formSigningMode.toLowerCase().contains("edit");
        }
    }

    /**
     * 重新選擇類別組合
     *
     * @param templateMBean
     * @param reqAttachMBean
     */
    public void selectCategory(RequireTemplateFieldMBean templateMBean, ReqAttachMBean reqAttachMBean) {
        this.setp = RequireStep.NEW_IN_TREE;
        templateMBean.clearAllValue();
        if (reqAttachMBean != null) {
            reqAttachMBean.init();
        }
    }

    /**
     * 點擊編輯
     *
     * @param setpStr
     */
    public void startMaintenace(String setpStr, boolean isEditByAdmin) {

        this.setp = RequireStep.valueOf(setpStr);
        this.isEditByAdmin = isEditByAdmin;

        if (RequireStep.LOAD_IN_PAGE_EDIT.equals(this.setp)) {
            templateMBean.toggleDisabled(Boolean.FALSE);
        }
        titleBtnMBean.clear();
        backupReq = requireService.findByReqObj(require);
        this.requireCheckItemMBean.reload(require.getSid());
    }

    /**
     * 取消編輯
     *
     * @param templateMBean
     */
    public void recovery(RequireTemplateFieldMBean templateMBean) {
        try {
            this.reBuildeRequire();
            templateMBean.toggleDisabled(Boolean.TRUE);
            this.setp = RequireStep.LOAD_IN_PAGE;
            templateMBean.recoveryTemplate(require);
            this.titleBtnMBean.clear();
            this.traceMBean.clear();
        } catch (Exception ex) {
            log.error("取消編輯..." + ex.getMessage(), ex);
            MessagesUtils.showWarn("取消編輯，" + ex.getMessage());
        }
    }

    @Override
    public void reloadToIllegalPage(String errorCode) {
        try {
            String reqBasePath = FusionUrlServiceUtils.getUrlByPropKey(PropKeyType.TECH_REQUEST_AP_URL.getValue());
            Faces.getExternalContext().redirect(reqBasePath + "/error/illegal_read.xhtml?" + URLService.ERROR_CODE_ATTR + "=" + errorCode);
            Faces.getContext().responseComplete();
        } catch (Exception ex) {
            log.info("導向讀取失敗頁面失敗..." + ex.getMessage());
        }
    }

    /**
     * 單據導向01page
     *
     * @param requireNo
     */
    public void reloadNewPage(String requireNo) {
        // 避免已經做過 responseComplete 這邊又做一次
        if (Faces.getContext().getResponseComplete()) {
            return;
        }

        String fullUrl = "";
        try {
            // WkCommonUtils.logWithStackTrace(InfomationLevel.DEBUG, "導向新建需求單");
            fullUrl = urlService.buildFullAddFormUrl(SecurityFacade.getCompanyId());
            Faces.getExternalContext().redirect(fullUrl);
            Faces.getContext().responseComplete();

        } catch (Exception e) {
            log.error("導向新建需求單失敗...redirect [" + fullUrl + "] fail.. [" + e.getMessage() + "]");
        }
    }

    /**
     * 新建檔後，導向02page
     *
     * @param require
     */
    public void reloadPage(Require require) {
        tableUpDownBean.clearAllRec();
        tableUpDownBean.setCurrRow(require.getRequireNo());
    }

    /**
     * 取消鎖定
     */
    public void cancelLock() {
        if (require == null) {
            return;
        }
        try {
            lockService.unlock(require);
        } catch (Exception e) {
            log.error("取消鎖定失敗..." + e.getMessage(), e);
        }
    }

    /**
     * 檢查切換TAB的seesion
     */
    private void checkTabUrlAttr() {
        urlPara = loadManager.maybeCreateUrlAttrMap();
        this.setTabByAttr();
        this.doSelectTabByUrlAttr();
    }

    private void setTabByAttr() {
        if (urlPara != null) {
            this.initTabInfo((RequireBottomTabType) urlPara.get(ReqLoadBean.TAB_TYPE));
            this.bottomTabMBean.resetTabIdx(this);
            this.bottomTabMBean.changeTabByTabType((RequireBottomTabType) urlPara.get(ReqLoadBean.TAB_TYPE));
        }
    }

    /**
     * require02.xhtml 畫面ready後執行。(畫面未渲染完畢前執行無效)
     */
    private void doSelectTabByUrlAttr() {
        if (urlPara == null) {
            return;
        }
        Boolean showRequire = false;
        showRequire = Boolean.valueOf(Faces.getRequestParameter("showRequire"));
        String indicateSid = (String) urlPara.get(ReqLoadBean.TAB_SID);
        RequireBottomTabType tabType = (RequireBottomTabType) urlPara.get(ReqLoadBean.TAB_TYPE);
        if (tabType != null && RequireBottomTabType.ASSIGN_SEND_INFO.equals(tabType)) {
            display.execute("moveScrollTop();");
            return;
        }
        if (tabType.equals(RequireBottomTabType.PT_CHECK_INFO)) {
            this.doScrollBottomTabToSid(indicateSid, ptService.findSidsBySourceSid(this.require.getSid()), "pt_acc_panel_layer_zero");
        }
        if (tabType.equals(RequireBottomTabType.SEND_TEST_INFO)) {
            this.doScrollBottomTabToSid(indicateSid, stService.findSidsBySourceSid(this.require.getSid()), "st_acc_panel_layer_zero");
        }
        if (tabType.equals(RequireBottomTabType.ONPG)) {
            this.doScrollBottomTabToSid(indicateSid, opService.findSidsBySourceSid(this.require.getSid()), "op_acc_panel_layer_zero");
        }
        if (tabType.equals(RequireBottomTabType.OTH_SET)) {
            this.doScrollBottomTabToSid(indicateSid, osService.findSidsByRequire(this.require), "os_acc_panel_layer_zero");
        }
        // 收件夾
        if (tabType.equals(RequireBottomTabType.INBOX_REPORT)) {
            this.doScrollBottomTabToSid(indicateSid,
                    inboxService.findSidsByRequire(this.require, loginBean.getUser(), InboxType.INCOME_REPORT),
                    "ix_report_acc_panel_layer_zero");
        }
        if (tabType.equals(RequireBottomTabType.INBOX_PERSONAL)) {
            this.doScrollBottomTabToSid(indicateSid,
                    inboxService.findSidsByRequire(this.require, loginBean.getUser(), InboxType.INCOME_MEMBER),
                    "ix_personal_acc_panel_layer_zero");
        }
        if (tabType.equals(RequireBottomTabType.INBOX_INSTRUCTION)) {
            this.doScrollBottomTabToSid(indicateSid,
                    inboxService.findSidsByRequire(this.require, loginBean.getUser(), InboxType.INCOME_INSTRUCTION),
                    "ix_instruction_acc_panel_layer_zero");
        }
        if (showRequire) {
            display.execute("moveScrollTop();");
        } else {
            display.execute("moveScrollBottom();");
        }

    }

    private void doScrollBottomTabToSid(String indicateSid, List<String> sids, String tabName) {
        if (indicateSid.equals("toOne")) {
            display.execute("bottomTabCloseAndSel('" + tabName + "', " + 0 + "," + sids.size() + ")");
            return;
        }
        Integer index = sids.indexOf(indicateSid);
        if (index != -1) {
            display.execute("bottomTabCloseAndSel('" + tabName + "', " + index + "," + sids.size() + ")");
        }
    }

    /**
     * 執行者名稱
     *
     * @param user
     * @return
     */
    public String findConfirmExecName(User user) {
        if (user == null) {
            return "";
        }
        return WkUserUtils.prepareUserNameWithDep(user.getSid(), "-");
    }

    /**
     * 結案前初始化準備
     */
    public void initByClose(boolean isForceClose) {

        this.forceCloce = isForceClose;
        // ====================================
        // 防呆
        // ====================================
        if (require == null || loginBean.getUser() == null) {
            log.warn("require==null || loginBean.getUser() ==null ");
            MessagesUtils.showWarn(WkMessage.SESSION_TIMEOUT);
            return;
        }

        requireTrace = traceService.createNewTrace(require.getSid(), loginBean.getUser());
        requireTrace.setRequireTraceType(RequireTraceType.CLOSE_MEMO);

        if (this.forceCloce) {
            requireTrace.setRequireTraceContent("強制結案原因：\r\n");
        }

        display.update("req_close_note_dlg_id");
        display.showPfWidgetVar("req_close_note_dlg_wv");
    }

    /**
     * 執行需求單結案
     */
    public void executeRequireClose() {
        try {
            if (RequireStatusType.COMPLETED.equals(require.getRequireStatus()) || RequireStatusType.SUSPENDED.equals(require.getRequireStatus())) {
                if (require.getReqUnitSign() != null) {
                    // for require unit
                    reqBpmMBean.updateBpmStatus(require, InstanceStatus.CLOSED);
                }
            }
            // , boolean isForceClose
            requireProcessCloseService.executeByUser(require, loginBean.getUser(), requireTrace, this.forceCloce);

            // ====================================
            // 畫面控制
            // ====================================
            this.reBuildeAndGotoTraceTab();

        } catch (Exception e) {
            log.error(this.require.getRequireNo() + ":" + loginBean.getUser().getName() + ":執行需求單結案失敗..." + e.getMessage(), e);
            this.reBuildeByUpdateFaild();
            MessagesUtils.showError(e.getMessage());
        }
    }

    /**
     * 
     */
    public void preRequrieComplete() {

        // ====================================
        // 檢核子單還在簽核中
        // ====================================
        // 建立需求完成警告 - 流程未簽核部份 (原型確認、送測)
        String needSignmessage = requireService.createCompleteWaringMsg(require);
        // 不可需求完成
        if (WkStringUtils.notEmpty(needSignmessage)) {
            MessagesUtils.showWarn(needSignmessage);
            return;
        }

        // ====================================
        // 組確認訊息
        // ====================================
        this.noticeMsg = this.requireService.prepareRequireCompleteConfirmMesssag(require);

        // ====================================
        // 更新追蹤物件
        // ====================================
        this.requireTrace = traceService.createNewTrace(require.getSid(), loginBean.getUser());

        // ====================================
        // 呼叫確認視窗
        // ====================================
        this.confirmCallbackDialogController.showConfimDialog(
                noticeMsg,
                "",
                () -> openCompleteMemoDialog());
    }

    /**
     * 需求完成補充資訊
     */
    public void openCompleteMemoDialog() {
        // 開啟確認窗
        display.update("req_complete_memo_dlg_id");
        display.showPfWidgetVar("req_complete_memo_dlg_wv");
    }

    /**
     * 初始化需求完成資訊補充 和需求人員資訊
     */
    public void initByCompleteTrace() {
        // 建立 trace, 供需求完成說明 dialog 顯示
        requireTrace = traceService.createNewTrace(require.getSid(), loginBean.getUser());
        requireService.updateReqFinishHelper(require, loginBean.getUser());
    }

    /**
     * 執行強制需求完成
     */
    public void executeRequireComplete() {
        // ====================================
        // 執行強制需求完成
        // ====================================
        try {
            this.requireProcessCompleteService.executeByForce(require, loginBean.getUser(), requireTrace);
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
        } catch (Exception e) {
            log.error(require.getRequireNo() + ":" + loginBean.getUser().getName() + ":執行需求完成失敗..." + e.getMessage(), e);
            MessagesUtils.showError(e.getMessage());
        }

        // ====================================
        // 更新畫面組件
        // ====================================
        try {
            this.reBuildeAndGotoTraceTab();
        } catch (Exception e) {
            String msg = "執行成功，但更新畫面資料失敗，請重新讀取!";
            log.error(msg, e);
            MessagesUtils.showError(e.getMessage());
        }
    }

    /**
     * 重組需求單內容
     */
    public void reBuildeRequire() {
        if (require == null || Strings.isNullOrEmpty(require.getSid())) {
            return;
        }
        entityManager.clear();
        this.require = requireService.findByReqSid(require.getSid());
    }

    /**
     * 重組建需求單內容資訊(全頁面)
     */
    public void reBuildeByUpdateFaild() {
        this.reBuildeRequire();
        this.titleBtnMBean.clear();
        templateMBean.initByTable(require);
        bottomTabMBean.resetTabIdx(this);
        loadManager.clearPageMem(this);

        display.update(Lists.newArrayList(
                "title_info_click_btn_id",
                "require01_title_info_id",
                "require_template_id",
                "viewPanelBottomInfoTabId"));

        // 初始化製作進度面板
        this.displayController.execute(ReqConfirmClientHelper.prepareClientPanelInitScript(this.require));
    }

    /**
     * 
     */
    public void reBuildeAndGotoTraceTab() {

        this.reBuildeByUpdateFaild();

        // 顯示追蹤頁簽
        this.bottomTabMBean.changeTabByTabType(RequireBottomTabType.TRACE);
        // 更新追蹤頁簽
        this.initTabInfo(RequireBottomTabType.TRACE);

        // 追蹤第一筆
        this.displayController.execute("accPanelSelectTab('req_trace_tab_wv', 0);");

    }

    /**
     * 載入需求單
     */
    public void loadRequireByOpener() {
        tableUpDownBean.doWaitUpDown();
        if (!tableUpDownBean.isEmptyCurrRow()) {
            if (this.checkToReq06()) {
                log.debug("checkToReq06 return");
                return;
            }
            String requireNo = tableUpDownBean.getCurrRow();
            log.debug("loadRequireByOpener " + requireNo);
            loadManager.reloadReqForm(requireService.findByReqNo(requireNo), loginBean.getUser());
            urlPara = tableUpDownBean.maybeCreateTabParam();
            this.setTabByAttr();
            this.doSelectTabByUrlAttr();
            tableUpDownBean.clearCurrRow();
            display.execute("replaceUrlNotNavigation('" + this.rebindUrl(requireNo) + "');");
            display.update(Lists.newArrayList(
                    "title_info_click_btn_id",
                    "require01_title_info_id",
                    "require_template_id",
                    "viewPanelBottomInfoId"));

            // 處理製作進度面板
            // 未檢查前, 畫面不顯示面板, 故更新狀態後, 需重新初始化 （讓 slider 頁簽建立起來）
            String clientScript = ReqConfirmClientHelper.prepareClientPanelInitScript(this.require);
            clientScript += "hideLoad();";
            displayController.execute(clientScript);

        }
    }

    /**
     * 當require02.xhtml進行上下筆動作時，需判讀是否為草稿提交導向而來<BR/>
     * 如果為草稿提交導向而來，需再度導向回require06.xhtml
     */
    public boolean checkToReq06() {
        if (this.isDraftSubmitAfter) {
            log.debug("checkToReq06 start");
            try {
                Faces.getExternalContext().redirect("../require/require06.xhtml");
                Faces.getContext().responseComplete();
            } catch (Exception e) {
                log.info("導向草稿失敗..." + e.getMessage(), e);
            }
            return true;
        }
        return false;
    }

    private String rebindUrl(String requireNo) {
        if (Faces.getViewId().contains("require06.xhtml")) {// 草稿
            return urlService.createDraftURLLink(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(loginBean.getUser().getSid(), requireNo, 1));
        } else if (Faces.getViewId().contains("require02.xhtml")) {// 已提交需求單
            // 子程序及收件夾
            if (urlPara != null) {
                String subSid = (String) urlPara.get(ReqLoadBean.TAB_SID);
                switch ((RequireBottomTabType) urlPara.get(ReqLoadBean.TAB_TYPE)) {
                case PT_CHECK_INFO:
                    return this.createSubUrl(requireNo, subSid, URLService.URLServiceAttr.URL_ATTR_TAB_PT);
                case SEND_TEST_INFO:
                    return this.createSubUrl(requireNo, subSid, URLService.URLServiceAttr.URL_ATTR_TAB_ST);
                case ONPG:
                    return this.createSubUrl(requireNo, subSid, URLService.URLServiceAttr.URL_ATTR_TAB_OP);
                case OTH_SET:
                    return this.createSubUrl(requireNo, subSid, URLService.URLServiceAttr.URL_ATTR_TAB_OS);
                case INBOX_REPORT:
                    return this.createSubUrl(requireNo, subSid, URLService.URLServiceAttr.URL_ATTR_TAB_INBOX_REPORT);
                case INBOX_PERSONAL:
                    return this.createSubUrl(requireNo, subSid, URLService.URLServiceAttr.URL_ATTR_TAB_INBOX_PERSONAL);
                case INBOX_INSTRUCTION:
                    return this.createSubUrl(requireNo, subSid, URLService.URLServiceAttr.URL_ATTR_TAB_INBOX_INSTRUCTION);
                case INBOX_SEND:
                    return this.createSubUrl(requireNo, subSid, URLService.URLServiceAttr.URL_ATTR_TAB_INBOX_SEND);
                default:
                    break;
                }
            }
            // 非子程序
            return urlService.createLoacalURLLink(URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(loginBean.getUser().getSid(), requireNo, 1));
        }
        return "";
    }

    private String createSubUrl(String reqNo, String subSid, URLService.URLServiceAttr attr) {
        String url = urlService.createLocalUrlLinkParamForTab(
                URLService.URLServiceAttr.URL_ATTR_M,
                urlService.createSimpleUrlTo(loginBean.getUser().getSid(), reqNo, 1),
                attr, subSid);
        // add param for formsign-web to use
        if (Faces.getRequestParameter("mode") != null) {
            url += "&mode=" + Faces.getRequestParameter("mode");
        }

        return url;
    }

    /**
     * 提前初始化TAB內容
     */
    public void initTabInfo(RequireBottomTabType type) {
        if (type == null) {
            return;
        }
        switch (type) {
        case TRACE:
            this.initTabInfo(this.traceMBean);
            break;
        case PT_CHECK_INFO:
            this.initTabInfo(this.ptBottomMBean);
            break;
        case SEND_TEST_INFO:
            this.initTabInfo(this.sendTestBottomMBean);
            break;
        case ONPG:
            this.initTabInfo(this.onpgBottomMBean);
            break;
        case OTH_SET:
            // this.initTabInfo(this.othSetBottomMBean);
            othSetSettingViewComponents.loadData(this.require.getSid());
            break;
        case ASSIGN_SEND_INFO:
            // this.initTabInfo(this.assignSendMBean);
            this.initTabInfo(this.assignInfo);
            break;

        // case ASSIGN_INFO:
        // this.initTabInfo(this.assignNoticeTabMBean);

        case INBOX_REPORT:
            this.initTabInfo(this.ixReportMBean);
            break;
        case INBOX_PERSONAL:
            this.initTabInfo(this.ixPersonalMBean);
            break;
        case INBOX_INSTRUCTION:
            this.initTabInfo(this.ixInstructionMBean);
            break;
        case INBOX_SEND:
            this.workInboxComponent.setCanShowSendBackUp(true);
            this.workInboxComponent.loadData();
            break;
        default:
            break;
        }
    }

    public void initTabInfo(IRequireBottom tabBottom) {
        tabBottom.initTabInfo(this);
    }

    private final TabLoadCallBack tabLoadCallBack = new TabLoadCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -6271879332394734720L;

        @Override
        public void reloadTraceTab() {
            traceMBean.clear();
            DisplayController.getInstance().update("viewPanelBottomInfoTabId:req_trace_tab_id");
        }
    };

    private final MessageCallBack messageCallBack = new MessageCallBack() {

        /**
         * 
         */
        private static final long serialVersionUID = 1444460219911701119L;

        @Override
        public void showMessage(String str) {
            MessagesUtils.showError(str);
        }

    };

    /**
     * 讀取子程序紀錄
     *
     * @param requireSid
     * @param subProcessSid
     * @param types
     */
    public void loadSubNoticeInfo(String requireSid, String subProcessSid, List<SubNoticeType> types) {
        this.subNoticeInfoTos = subNoticeInfoService.findActiveByRequireSidAndSubProcessSidAndTypeIn(
                requireSid, subProcessSid, types);
        display.update("dlg_sub_notice_info_view_id");
        display.showPfWidgetVar("dlg_sub_notice_info_wv");
    }

    public void clearIssueCreateOnpgKeySid() {
        this.issueCreateOnpgKeySid = "";
    }

    /**
     * popup Rollback Prompt
     * 
     * @param require
     */
    public void popupRollbackPrompt() {

        if (require == null) {
            return;
        }

        String confirmMsg = traceService.evenRollbackAlertMsg(
                require.getSid(),
                SecurityFacade.getUserId());

        if (StringUtils.isNotBlank(confirmMsg)) {
            MessagesUtils.showInfo(confirmMsg);
        }
    }

    /**
     * 轉PMIS
     */
    public void transferToPMIS() {
        try {
            this.pmisHelper.transferToPMIS(require.getSid(), SecurityFacade.getUserSid());

        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            String errorMessage = WkMessage.EXECTION + "(" + e.getMessage() + ")";
            MessagesUtils.showError(errorMessage);
            return;
        }

        titleBtnMBean.setShowPmisBtn(null);
        this.reBuildeAndGotoTraceTab();
    }

    /**
     * 透過URL讀取送測單號，開啟送測單
     */
    public boolean loadSuccessWorkTestInfoByUrlParam(WorkTestInfo workTestInfo) {
        // ====================================
        // 權限檢核
        // ====================================
        if (!loadManager.hasReadPermission(workTestInfo.getSourceNo())) {
            log.info("透過URL讀取送測單號，開啟送測單:無權限");
            // WkCommonUtils.logWithStackTrace(InfomationLevel.ERROR, "", 50);
            this.reloadToIllegalPage(URLService.ERROR_CODE_NO_PERMISSION);
            return false;
            // throw new Exception("透過URL讀取送測單號，開啟送測單:無權限");
        }

        // ====================================
        // 初始化資料
        // ====================================
        sendTestBottomMBean.setLoadWorkTestInfoByUrlParam(true);
        sendTestBottomMBean.reloadWorkTestInfo(workTestInfo.getTestinfoNo());
        this.require = requireService.findByReqNo(workTestInfo.getSourceNo());
        // clear CurrRow 與ReqLoadBean.checkWorkTestUpdownBeanPara()相關
        workTestUpDownBean.clearCurrRow();
        return true;
    }

    /**
     * 判斷是否為編輯模式
     * 
     * @return
     */
    public boolean isEditMode() {
        if (RequireStep.NEW_IN_PAGE.equals(this.getSetp())) {
            return true;
        }

        if (RequireStep.LOAD_IN_PAGE_EDIT.equals(this.getSetp())) {
            return true;
        }

        return false;
    }

    /**
     * 為編輯模式, 但非 admin 編輯
     * 
     * @return
     */
    public boolean isNormalEditMode() { return this.isEditMode() && !this.isEditByAdmin; }

    /**
     * 異動系統別編輯狀態
     */
    @Getter
    private boolean webCodeInEdit = false;

    /**
     * 異動系統別操作：編輯
     */
    public void webcode_process_edit() {
        webCodeInEdit = true;
    }

    /**
     * 異動系統別操作：取消
     */
    public void webcode_process_cancel() {
        webCodeInEdit = false;

        this.reBuildeRequire();
        this.titleBtnMBean.clear();

        display.update(Lists.newArrayList(
                "work_customer_webCode_group",
                "require01_title_info_id"));

    }

    /**
     * 異動系統別操作：儲存
     */
    public void webcode_process_save() {

        // ====================================
        // 異動系統別
        // ====================================
        // TODO
        // try {
        // this.requireService.updateWebCode(
        // this.require.getSid(),
        // this.require.getWebCode(),
        // loginBean.getUser(),
        // new Date());
        // } catch (UserMessageException e) {
        // MessagesUtils.show(e);
        // return;
        // } catch (Exception e) {
        // String message = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
        // MessagesUtils.showError(message);
        // return;
        // }

        // ====================================
        // 更新畫面
        // ====================================
        this.reBuildeByUpdateFaild();
        this.getTraceMBean().clear();
        this.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.TRACE);

        display.execute(""
                + "accPanelSelectTab('req_trace_tab_wv', 0);"
                + ReqConfirmClientHelper.CLIENT_SRIPT_CLOSE_PANEL);

        webCodeInEdit = false;
    }

    /**
     * 兜組需求單類別資訊
     * 
     * @param require 需求單主檔物件
     * @return info str
     */
    public String prepareReqItemInfo(CategoryKeyMapping mapping) {
        // 防呆
        if (mapping == null) {
            return "";
        }
        String reqItemInfo = "";
        // 大類
        reqItemInfo += "[<span class='cy-hover-highlight'>" + mapping.getBigName() + "</span>]";
        // 分隔
        reqItemInfo += "<span class='WS1-1-4' style='font-weight:900;'>&nbsp;■&nbsp;</span>";
        // 中類
        reqItemInfo += "[<span class='cy-hover-highlight'>" + mapping.getMiddleName() + "</span>]";
        // 分隔
        reqItemInfo += "<span class='WS1-1-4' style='font-weight:900;'>&nbsp;■&nbsp;</span>";
        // 小類
        reqItemInfo += "[<span class='WS1-1-3' style='font-weight:600;'>" + mapping.getSmallName() + "</span>]";
        return reqItemInfo;
    }

    /**
     * 反結案 - 前置
     * 
     * @param isForce
     */
    public void preRollbackClose(boolean isForce) {

        // ====================================
        // 防呆
        // ====================================
        if (require == null || loginBean.getUser() == null) {
            MessagesUtils.showWarn(WkMessage.SESSION_TIMEOUT);
            return;
        }

        String confirmMessage = "是否進行『%s反結案』(製作進度 -> 已完成)？";
        confirmMessage = String.format(
                confirmMessage,
                isForce ? "強制" : "");

        confirmCallbackDialogController.showConfimDialog(
                confirmMessage,
                Lists.newArrayList(),
                () -> rollbackClose(isForce));

    }

    /**
     * 反結案
     */
    public void rollbackClose(boolean isForce) {

        try {

            // ====================================
            // 執行反結案
            // ====================================
            this.requireProcessCloseRollbackService.executeByUser(
                    require,
                    WkUserCache.getInstance().findBySid(SecurityFacade.getUserSid()),
                    new Date(),
                    isForce);

            // ====================================
            // 畫面控制
            // ====================================
            // 全頁面重置
            this.reBuildeByUpdateFaild();
            // 更新追蹤頁簽
            initTabInfo(RequireBottomTabType.TRACE);
            // 顯示追蹤頁簽
            this.bottomTabMBean.changeTabByTabType(RequireBottomTabType.TRACE);

            display.execute("accPanelSelectTab('req_trace_tab_wv', 0);");

        } catch (Exception e) {
            log.error("rollbackClose ERROR：" + e.getMessage(), e);
            MessagesUtils.showError("執行失敗, 請恰相關人員！ " + e.getMessage());
            return;
        }
    }
}
