/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.common;

import com.cy.tech.request.web.setting.RequireTransSetting;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.SelectItem;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 取得轉單程式-程序挑選選項
 *
 * @author brain0925_liao
 */
@Controller
@Scope("request")
public class ProgramTypeBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8311627948752777515L;

    /** 轉單程式-建立單位轉換,程序挑選選項 */
    public List<SelectItem> getTransCreateDepProgramTypeItems() {
        return RequireTransSetting.getTransCreateDepProgramTypeItems();
    }

    /** 轉單程式-通知單位轉換,程序挑選選項 */
    public List<SelectItem> getTransNotifyDepProgramTypeItems() {
        return RequireTransSetting.getTransNotifyDepProgramTypeItems();
    }
}
