package com.cy.tech.request.web.controller.values;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.omnifaces.util.Faces;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.utils.WkStringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 成員登入資訊
 *
 * @author shaun
 */
@Controller
@Scope(WebApplicationContext.SCOPE_SESSION)
@Slf4j
public class LoginBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 959150848255183743L;

    @PostConstruct
    void init() {
        // 未登入時不做初始化
        if (!SecurityFacade.isAuthenticated() || WkStringUtils.isEmpty(SecurityFacade.getCompanyId())) {
            return;
        }
        StorageLoginHelper.getInstance().add(this);
    }

    @PreDestroy
    void destory() {
        if (user != null) {
            log.info("銷毀成員【" + user.getName() + "】session。");
        }
    }

    /**
     * 
     */
    public void reStartIdle() {
        try {
            String activePage = "";
            for (ReportType type : ReportType.values()) {
                if (Faces.getViewId().contains(type.getViewId())) {
                    activePage = " (" + type.getReportName() + ")";
                    break;
                }
            }
            StorageLoginHelper.getInstance().updateActiveInfo(Faces.getViewId() + activePage, this);
            WorkSpringContextHolder.getBean(DisplayController.class).execute("reStartIdleMonitor();");
        } catch (Exception e) {
            log.error("session time out!!\n" + e.getMessage());
        }
    }
    // ========================================================================
    // 特殊角色檢查
    // ========================================================================

    // ========================================================================
    // 以下為常用
    // ========================================================================
    /**
     * 取得登入者 ID
     * 
     * @return
     */
    public String getUserId() { return SecurityFacade.getUserId(); }

    /**
     * 取得登入者 SID
     * 
     * @return
     */
    public Integer getUserSId() { return SecurityFacade.getUserSid(); }

    /**
     * 取得登入者資料
     * 
     * @return
     */
    private User user;

    public User getUser() {
        // 曾經有查到過就記錄起來
        user = WkUserCache.getInstance().findBySid(this.getUserSId());
        return user;
    }

    /**
     * 取得登入者部門
     * 
     * @return
     */
    public Org getDep() {
        User user = this.getUser();
        if (user == null || user.getPrimaryOrg() == null) {
            return null;
        }
        return WkOrgCache.getInstance().findBySid(user.getPrimaryOrg().getSid());
    }

    /**
     * 取得登入公司 ID
     * 
     * @return
     */
    public String getCompanyId() { return SecurityFacade.getCompanyId(); }

    /**
     * 取得登入公司 SID
     * 
     * @return
     */
    public Integer getCompanySid() {
        Org comp = this.getComp();
        if (comp != null) {
            return comp.getSid();
        }
        return null;
    }

    /**
     * 取得登入公司資料
     * 
     * @return
     */
    public Org getComp() { return WkOrgCache.getInstance().findById(this.getCompanyId()); }

}
