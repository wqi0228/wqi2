/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search28QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.view.Search28View;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.onpg.OnpgService;
import com.cy.tech.request.logic.utils.ToolsDate;
import com.cy.tech.request.logic.vo.SimpleDateFormatEnum;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.ReqCateType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.logic.component.OrgLogicComponents;
import com.cy.tech.request.web.controller.logic.component.WorkCalendarLogicComponent;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.searchheader.CommonHeaderMBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.cache.WkCommonCache;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.utils.WkOrgUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * ON程式檢查完成提醒清單
 *
 * @author jason_h
 */
@Controller
@Scope("view")
@Slf4j
public class Search28MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6448617542737430745L;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private WkCommonCache wkCommonCache;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private OnpgService opService;
    @Autowired
    transient private Require01MBean r01MBean;

    @Autowired
    transient private Search28QueryService search28QueryService;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;

    // @Autowired
    // transient private Search16LogicHelper search16LogicHelper;

    // /** 所有的ON程式單 */
    // private List<Search16View> allQueryItems;
    @Getter
    @Setter
    private List<Search28View> queryItems;
    /** 選擇的ON程式單 */
    @Getter
    @Setter
    private Search28View querySelection;
    /** 上下筆移動keeper */
    @Getter
    private Search28View queryKeeper;
    /** 切換模式 */
    @Getter
    private SwitchType switchType = SwitchType.CONTENT;
    /** 切換模式 - 全畫面狀態 */
    @Getter
    private SwitchType switchFullType = SwitchType.DETAIL;
    /** 在匯出的時候，某些內容需要隱藏 */
    @Getter
    private boolean hasDisplay = true;
    @Getter
    private final String dataTableId = "dtRequire";
    /** 查詢控制物件 */
    // @Getter
    // private SearchQuery16 searchQuery;
    @Autowired
    private transient WorkBackendParamHelper workBackendParamHelper;
    @Autowired
    private OrgLogicComponents orgLogicComponents;
    @Autowired
    private WorkCalendarLogicComponent workCalendarLogicComponent;
    @Getter
    /** 訊息呼叫 */
    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 6194594063759425228L;

        @Override
        public void showMessage(String m) {
            MessagesUtils.showError(m);
        }
    };

    @PostConstruct
    public void init() {
        this.initComponent();
        display.execute("doSearchData();");
    }

    private void initComponent() {
        // searchQuery = new SearchQuery16(
        // helper,
        // search16LogicHelper,
        // messageCallBack,
        // display,
        // reqStatusUtils,
        // customerService,
        // common,
        // Lists.newArrayList(loginBean.getRoles()),
        // loginBean.getDep(),
        // reportCustomFilterLogicComponent,
        // categorySettingService,
        // loginBean.getUserSId());
        // searchQuery.initSetting();
        commHeaderMBean.clearAdvance();
        // searchQuery.loadSetting();
    }

    public void search() {
        queryItems = this.findWithQuery();
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        // searchQuery.initSetting();
        commHeaderMBean.clearAdvance();
        // searchQuery.loadSetting();
        this.search();
    }

    private List<Search28View> findWithQuery() {
        try {
            List<Integer> depSids = Lists.newArrayList();
            orgLogicComponents.getAllManagerOrgs(loginBean.getUserSId()).forEach(item -> {
                if (item != null && !depSids.contains(item.getSid())) {
                    depSids.add(item.getSid());
                }
            });
            WkOrgCache.getInstance().findAllParent(loginBean.getDep().getSid()).forEach(item -> {
                if (item != null && !depSids.contains(item.getSid())) {
                    depSids.add(item.getSid());
                }
            });
            depSids.add(loginBean.getDep().getSid());
            if (depSids.isEmpty()) {
                return Lists.newArrayList();
            }

            Map<String, Object> parameters = Maps.newHashMap();
            StringBuilder builder = new StringBuilder();
            builder.append("SELECT "
                    + "wop.onpg_sid,"
                    + "tr.require_no,"
                    + "tid.field_content,"
                    + "tr.urgency,"
                    + "wop.create_dt,"
                    + "wop.dep_sid,"
                    + "wop.create_usr,"
                    + "wop.onpg_theme,"
                    + "ckm.big_category_name,"
                    + "ckm.middle_category_name,"
                    + "ckm.small_category_name,"
                    + "tr.dep_sid AS requireDep,"
                    + "tr.create_usr AS requireUser,"
                    + "wop.onpg_status,"
                    + "wop.read_reason,"
                    + "woh.reason,"
                    + "wop.onpg_estimate_dt,"
                    + "wop.onpg_finish_dt,"
                    + "wop.read_update_dt,"
                    + "wop.onpg_no,"
                    + "wop.onpg_deps,"
                    + "tr.create_dt AS requireCreatedDate,"
                    + "tr.customer,"
                    + "tr.author,"
                    + "tr.require_status, "
                    // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
                    + this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire()

                    + " FROM ");

            builder.append(" work_onpg wop ");
            buildRequireCondition(builder, parameters);
            buildRequireIndexDictionaryCondition(builder, parameters);
            buildCategoryKeyMappingCondition(builder, parameters);
            buildWorkOnpgHistoryCondition(builder, parameters);
            // 檢查項目 (系統別)
            // 後方需對 主單 sid 做 group by
            builder.append(this.searchConditionSqlHelper.prepareSubFormCommonJoin(
                    SecurityFacade.getUserSid(),
                    FormType.WORKONPG,
                    "wop"));

            buildWorkOnpgCondition(builder, parameters, new Date(), depSids);

            builder.append(" GROUP BY wop.onpg_sid ");
            builder.append("  ORDER BY wop.create_dt ASC ");

            // show SQL in debug log
            SearchCommonHelper.getInstance().showSQLDebugLog(
                    ReportType.WORK_ON_PG_CHECK, builder.toString(), parameters);

            // 建立報表使用記錄物件
            RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                    ReportType.WORK_ON_PG_CHECK, SecurityFacade.getUserSid());

            // 查詢
            List<Search28View> resultList = search28QueryService.findWithQuery(
                    new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                    parameters,
                    SecurityFacade.getUserSid(),
                    usageRecord);

            // 儲存使用記錄
            usageRecord.saveUsageRecord();

            return resultList;
        } catch (Exception e) {
            log.error("findWithQuery ERROR", e);
        }
        return Lists.newArrayList();
    }

    private void buildWorkOnpgCondition(StringBuilder builder, Map<String, Object> parameters, Date end, List<Integer> depSids) {
        StringBuilder deSb = new StringBuilder();
        depSids.forEach(item -> {
            if (!Strings.isNullOrEmpty(deSb.toString())) {
                deSb.append(",");
            }
            deSb.append(item);
        });
        StringBuilder managerDeSb = new StringBuilder();
        List<Integer> managerDepSids = Lists.newArrayList();
        orgLogicComponents.getAllManagerOrgs(loginBean.getUserSId()).forEach(item -> {
            if (item != null && !managerDepSids.contains(item.getSid())) {
                if (!Strings.isNullOrEmpty(managerDeSb.toString())) {
                    managerDeSb.append(",");
                }
                managerDeSb.append(item.getSid());
                managerDepSids.add(item.getSid());
            }
        });
        builder.append("WHERE wop.onpg_sid IS NOT NULL ");
        // 技術需求單類型
        builder.append(" AND wop.onpg_source_type='TECH_REQUEST'");
        // [work_onpg].[onpg_status] in (CHECK_REPLY , ALREADY_ON)
        builder.append(" AND wop.onpg_status in ('" + WorkOnpgStatus.CHECK_REPLY.name() + "','" + WorkOnpgStatus.ALREADY_ON.name() + "') ");
        // [work_onpg].[create_dt]< systemdate - 三個工作天的筆數
        String dateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), workCalendarLogicComponent.getSpecialWorkDay(end, 3));
        dateStr = dateStr + " 23:59:59";
        builder.append(" AND wop.create_dt <= '" + dateStr + "' ");

        // AND 有GM回覆的日期（第一則）< system day - 三個工作天的筆數
        // GM回覆的撈取方式: work_onpg_check_record資料表中
        // REPLY_TYPE=GM_REPLY 時間抓取: work_onpg_check_record.RECORD_DT
        // builder.append(" select wocr.onpg_check_record_sid from work_onpg_check_record wocr ");
        // builder.append(" where wocr.reply_type ='" + WorkOnpgCheckRecordReplyType.GM_REPLY.name() + "' ");
        //
        // builder.append(" AND wocr.record_dt < '" + dateStr + "' )");
        builder.append(" AND (");
        // 外部需求- 當LoginUser=[tr_require].[dep] 的同仁(含底下單位的同仁們)or
        // [tr_require].[dep]的主管(包含部級單位主管與處級單位主管時）
        builder.append(" ( tbckm.req_category = '" + ReqCateType.EXTERNAL + "' AND  tr.dep_sid in (" + deSb.toString() + ") )");
        // 內部需求單的需求單的create_usr或需求單create_usr的主管們時
        builder.append(" OR ( tbckm.req_category = '" + ReqCateType.INTERNAL + "' AND (tr.create_usr = '" + loginBean.getUserSId() + "' ");
        if (!Strings.isNullOrEmpty(managerDeSb.toString())) {
            builder.append(" OR  tr.dep_sid in (" + managerDeSb.toString() + ")");
        }
        builder.append(") )");
        // 當LoginUser=企業服務處（含底下單位的同仁 或 含底下單位主管） 且 當ON程式的通知單位有企業服務處（含底下單位）
        // OR (當ON程式的通知單位 同時包含企業服務處（含底下單位）與
        // 會員服務處（含底下單位）)
        Org marketDep = workBackendParamHelper.getMarketDep();
        if (marketDep != null) {
            List<Org> marketDeps = WkOrgCache.getInstance().findAllChild(marketDep.getSid());
            marketDeps.add(marketDep);

            List<Org> flitermarketDeps = marketDeps.stream()
                    .filter(each -> each != null && each.getSid() != null && depSids.contains(each.getSid()))
                    .collect(Collectors.toList());
            if (flitermarketDeps != null && !flitermarketDeps.isEmpty()) {
                StringBuilder deMarketSb = new StringBuilder();
                marketDeps.forEach(item -> {
                    if (!Strings.isNullOrEmpty(deMarketSb.toString())) {
                        deMarketSb.append(" OR ");
                    }
                    deMarketSb.append(" wop.onpg_deps like '%\"" + item.getSid() + "\"%'");
                });
                builder.append(" OR ( tbckm.req_category = '" + ReqCateType.INTERNAL + "' AND  (" + deMarketSb.toString() + ") ) ");
            }
        }
        builder.append(" ) ");
    }

    private void buildRequireCondition(StringBuilder builder, Map<String, Object> parameters) {
        builder.append(" INNER JOIN  tr_require tr ON tr.require_no = wop.onpg_source_no ");
        // 需求類別製作進度(先決條件：[tr_require].[ require_status]= PROCESS)
        builder.append(" AND tr.require_status = '" + RequireStatusType.PROCESS.name() + "'");
    }

    private void buildRequireIndexDictionaryCondition(StringBuilder builder, Map<String, Object> parameters) {
        builder.append(" INNER JOIN  tr_index_dictionary tid ON "
                + "tr.require_sid = tid.require_sid AND tid.field_name='主題' ");
    }

    private void buildCategoryKeyMappingCondition(StringBuilder builder, Map<String, Object> parameters) {
        builder.append(" INNER JOIN tr_category_key_mapping ckm ON tr.mapping_sid=ckm.key_sid ");
        builder.append(" INNER JOIN tr_basic_data_big_category tbckm ON tbckm.basic_data_big_category_sid=ckm.big_category_sid ");
    }

    private void buildWorkOnpgHistoryCondition(StringBuilder builder, Map<String, Object> parameters) {
        builder.append(" LEFT JOIN (SELECT woh.onpg_history_sid,woh.reason,woh.onpg_sid,MAX(woh.create_dt) "
                + "FROM work_onpg_history woh "
                + "WHERE woh.onpg_source_type ='TECH_REQUEST' GROUP BY woh.onpg_sid) "
                + "AS woh ON woh.onpg_sid = wop.onpg_sid ");
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            this.moveScreenTab();
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (Search28View) event.getObject();
        this.changeRequireContent(this.queryKeeper);
        this.moveScreenTab();
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Search28View view) {
        view.setReadRecordType(ReadRecordType.HAS_READ);
        Require require = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(require, loginBean.getUser(), RequireBottomTabType.ONPG);
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Search28View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
        this.moveScreenTab();
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
        this.moveScreenTab();
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        try {
            helper.exportExcel(document, loginBean.getUser().getName(), "需求單待辦提醒：" + commHeaderMBean.getReportType().getReportName());
        } catch (Exception e) {
            log.error("exportExcel ERROR", e);
        }
        hasDisplay = true;
    }

    /**
     * 隱藏部分column裡的內容
     */
    public void hideColumnContent() {
        hasDisplay = false;
    }

    public String getNoticeDepsStr(Search28View view) {

        // cache 中取回
        String cacheName = "Search28View-getNoticeDepsStr";
        List<String> keys = Lists.newArrayList(view.getOnpgNo());
        String orgNames = wkCommonCache.getCache(cacheName, keys, 5);
        if (orgNames != null) {
            return orgNames;
        }

        // 組單位名稱
        List<String> orgSids = Optional.ofNullable(view.getNoticeDeps())
                .map(send -> send.getValue())
                .orElse(Lists.newArrayList());

        orgNames = WkOrgUtils.findNameBySidStrs(orgSids, "、");

        // put 快取
        wkCommonCache.putCache(cacheName, keys, orgNames);

        return orgNames;

    }

    public void moveScreenTab() {
        this.r01MBean.getBottomTabMBean().resetTabIdx(this.r01MBean);
        this.r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.ONPG);

        List<String> sids = opService.findSidsBySourceSid(this.r01MBean.getRequire().getSid());
        String indicateSid = queryKeeper.getSid();
        if (sids.indexOf(indicateSid) != -1) {
            display.execute("PF('op_acc_panel_layer_zero').select(" + sids.indexOf(indicateSid) + ");");
            for (int i = 0; i < sids.size(); i++) {
                if (i != sids.indexOf(indicateSid)) {
                    display.execute("PF('op_acc_panel_layer_zero').unselect(" + i + ");");
                }
            }
        }
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search28View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.removeClassByTextBold(dtId, pageCount);
        this.transformHasRead();
        this.checkHelfScreen();
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            display.update("headerTitle");
            display.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Search28View to) {
        querySelection = to;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 去除粗體Class
     *
     * @param dtId
     * @param pageCount
     */
    private void removeClassByTextBold(String dtId, String pageCount) {
        display.execute("removeClassByTextBold('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
        display.execute("changeAlreadyRead('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 變更已閱讀
     */
    private void transformHasRead() {
        querySelection.setReadRecordType(ReadRecordType.HAS_READ);
        queryKeeper = querySelection;
        queryItems.set(queryItems.indexOf(querySelection), querySelection);
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
        upDownBean.resetTabInfo(RequireBottomTabType.ONPG, queryKeeper.getSid());
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.transformHasRead();
        this.removeClassByTextBold(dtId, pageCount);
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }
}
