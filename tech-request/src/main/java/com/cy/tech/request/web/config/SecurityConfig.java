/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.config;

import com.cy.security.BasedSecurityConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Configuration("com.cy.tech.request.web.config.SecurityConfig")
@EnableWebSecurity
public class SecurityConfig extends BasedSecurityConfig {

    /**
     * monitor use
     *
     * @param webSecurity
     * @throws Exception
     */
    @Override
    public void configure(WebSecurity webSecurity) throws Exception {
        super.configure(webSecurity);
        webSecurity.ignoring().antMatchers("/monitor/**");
        webSecurity.ignoring().antMatchers("/clear/**");
        webSecurity.ignoring().antMatchers("/service/cache/**");
        //webSecurity.ignoring().antMatchers("/test/**");
        //webSecurity.ignoring().antMatchers("/pps6/**");
    }
}
