package com.cy.tech.request.web.controller.require;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.group.logic.RelevanceManager;
import com.cy.work.group.vo.WorkLinkGroup;
import com.cy.work.group.vo.WorkLinkGroupDetail;
import com.cy.work.group.vo.to.GroupTo;
import com.cy.work.tech.issue.client.exception.TransReqClientClientException;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 關聯設定控制
 *
 * @author kasim
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class RelevanceMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6675210890026755348L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private DisplayController displayController;
    @Autowired
    transient private RelevanceManager relevanceManager;

    /** 是否顯示關聯的dataTable */
    @Getter
    private boolean showRelevance;
    /** 關聯類型 */
    @Getter
    @Setter
    private WorkSourceType sourceType;
    /** 關聯日期(起) */
    @Getter
    @Setter
    private Date startRelevanceDate;
    /** 關聯日期(迄) */
    @Getter
    @Setter
    private Date endRelevanceDate;
    /** 關聯包含文字 */
    @Getter
    @Setter
    private String relevanceText;
    /** 可關聯的需求單 */
    @Getter
    @Setter
    private List<WorkLinkGroupDetail> allRelevance;
    /** 選取將進行關聯的需求單 */
    @Getter
    @Setter
    private List<WorkLinkGroupDetail> selRelevance;
    /** 同關聯的需求單 */
    @Getter
    private List<WorkLinkGroupDetail> allLinkDetails;
    /** 日期(起) */
    @Getter
    @Setter
    private Date startLinkGroupDate;
    /** 日期(迄) */
    @Getter
    @Setter
    private Date endLinkGroupDate;
    /** 包含文字 */
    @Getter
    @Setter
    private String linkGroupText;
    /** 同關聯的需求單 */
    @Getter
    private List<WorkLinkGroup> allLinkGroups;
    /** DataTable選取的資料 */
    @Getter
    @Setter
    private WorkLinkGroup selLinkGroup;
    @Getter
    /** 關聯群組明細 */
    private List<WorkLinkGroupDetail> allLinkGroupDetails;

    @Getter
    @Setter
    private WorkLinkGroupDetail selectedDetail;
    
    /**
     * 初始化關聯相關資訊
     */
    public void initRelevance() {
        showRelevance = false;
        sourceType = WorkSourceType.TECH_REQUEST;
        startRelevanceDate = new Date();
        endRelevanceDate = new Date();
        relevanceText = "";
        selRelevance = Lists.newArrayList();
        endLinkGroupDate = new Date();
        startLinkGroupDate = new DateTime(endLinkGroupDate).plusDays(-30).toDate();
        linkGroupText = "";
        selLinkGroup = null;
    }

    /**
     * 執行關聯查詢
     *
     * @param r01MBean
     */
    public void searchRelevance(Require01MBean r01MBean) {
        showRelevance = true;
        this.searchWaitRelevance(r01MBean);
        this.searchHasRelevance(r01MBean);
        this.reloadWorkLinkGroup(r01MBean);
    }

    /**
     * 查詢尚可關聯的需求單
     *
     * @return List<Require>
     */
    private void searchWaitRelevance(Require01MBean r01MBean) {
        GroupTo to = new GroupTo(sourceType, loginBean.getComp().getSid(), startRelevanceDate,
                endRelevanceDate, relevanceText, r01MBean.getRequire().getRequireNo());
        allRelevance = relevanceManager.searchWaitRelevance(to);
    }

    /**
     * 查詢同關聯的需求單
     *
     * @param r01MBean
     */
    private void searchHasRelevance(Require01MBean r01MBean) {
        GroupTo to = new GroupTo(WorkSourceType.TECH_REQUEST, r01MBean.getRequire().getRequireNo());
        allLinkDetails = relevanceManager.searchHasRelevance(to);
    }

    /**
     * 建立關聯
     *
     * @param r01MBean
     */
    public void createRelevance(Require01MBean r01MBean) {
        try {
            Preconditions.checkArgument(selRelevance != null, "尚未選取需求單，請確認！！");
            Preconditions.checkArgument(!selRelevance.isEmpty(), "尚未選取需求單，請確認！！");
            List<WorkLinkGroupDetail> relevances = Lists.newArrayList(selRelevance);
            WorkLinkGroupDetail groupDetail = new WorkLinkGroupDetail(
                    r01MBean.getRequire().getSid(), r01MBean.getRequire().getRequireNo(), WorkSourceType.TECH_REQUEST);
            relevances.add(groupDetail);
            boolean isDoubleMainLink = relevanceManager.isGroupMainLinkDouble(relevances);
            if (isDoubleMainLink) {
                displayController.showPfWidgetVar("doubleMainLink_AddDetail_confirm_wv");
            } else {
                this.doCreateRelevance(r01MBean, selRelevance);
            }
        } catch (IllegalArgumentException e) {
            log.error(r01MBean.getRequire().getRequireNo() + ":" + loginBean.getUser().getName() + ":建立關聯失敗..." + e.getMessage(), e);
            r01MBean.reBuildeByUpdateFaild();
            MessagesUtils.showError(e.getMessage());
        } catch (Exception e) {
            log.error(r01MBean.getRequire().getRequireNo() + ":" + loginBean.getUser().getName() + ":建立關聯失敗..." + e.getMessage(), e);
            r01MBean.reBuildeByUpdateFaild();
            MessagesUtils.showError(e.getMessage());
        }
    }

    /**
     * 建立關聯
     *
     * @param r01MBean
     * @param groupDetails
     * @throws TransReqClientClientException 
     */
    private void updateRelevance(Require01MBean r01MBean, List<WorkLinkGroupDetail> groupDetails) throws TransReqClientClientException {
        List<WorkLinkGroupDetail> relevances = Lists.newArrayList(groupDetails);
        WorkLinkGroupDetail groupDetail = new WorkLinkGroupDetail(
                r01MBean.getRequire().getSid(), r01MBean.getRequire().getRequireNo(), WorkSourceType.TECH_REQUEST);
        relevances.add(groupDetail);
        WorkLinkGroup group = relevanceManager.createRelevance(relevances, loginBean.getUser().getSid());
        if (group != null) {
            r01MBean.getRequire().setLinkGroup(group);
            r01MBean.getRequire().setHasLink(Boolean.TRUE);
        }
    }

    /**
     * 刪除關聯並進行查詢(Dialog)
     *
     * @param r01MBean
     * @param detail
     */
    public void removeRelevanceAndSearch(Require01MBean r01MBean, WorkLinkGroupDetail detail) {
        try {
            relevanceManager.removeRelevance(detail, loginBean.getUser().getSid());
            r01MBean.reBuildeRequire();
            this.searchRelevance(r01MBean);
            this.reLoadRelevanceByDel(r01MBean);
            // 移除主要關聯
            if(r01MBean.getRequire().getLinkGroup() != null && detail.getSourceNo().equals(r01MBean.getRequire().getLinkGroup().getMainLinkNo())){
            	relevanceManager.removeMainLink(detail.getLinkGroup().getSid(), r01MBean.getRequire().getLinkGroup().getMainLinkSid(), SecurityFacade.getUserSid());
            	displayController.showPfWidgetVar("dlg_msg_wv");
            }
        } catch (Exception e) {
            log.error(r01MBean.getRequire().getRequireNo() + ":" + loginBean.getUser().getName() + ":刪除關聯失敗..." + e.getMessage(), e);
            r01MBean.reBuildeByUpdateFaild();
            MessagesUtils.showError(e.getMessage());
        }
    }

    /**
     * 刪除關聯(by bottom tab)
     *
     * @param r01MBean
     * @param detail
     */
    public void removeRelevance(Require01MBean r01MBean, WorkLinkGroupDetail detail) {
        try {
            relevanceManager.removeRelevance(detail, loginBean.getUser().getSid());
            this.reLoadRelevanceByDel(r01MBean);
        } catch (Exception e) {
            log.error(r01MBean.getRequire().getRequireNo() + ":" + loginBean.getUser().getName() + ":刪除關聯失敗..." + e.getMessage(), e);
            r01MBean.reBuildeByUpdateFaild();
            MessagesUtils.showError(e.getMessage());
        }
    }

    /**
     * 執行群組查詢
     *
     * @param r01MBean
     */
    public void searchLinkGroup(Require01MBean r01MBean) {
        showRelevance = true;
        allLinkGroupDetails = Lists.newArrayList();
        GroupTo to = new GroupTo(loginBean.getComp().getSid(), startLinkGroupDate, endLinkGroupDate, linkGroupText);
        allLinkGroups = relevanceManager.searchGroup(to);
        if (r01MBean.getRequire().getLinkGroup() != null) {
            allLinkGroups.remove(r01MBean.getRequire().getLinkGroup());
        }
    }

    /**
     * 點擊群組關聯按鈕(加入現有群組)
     *
     * @param r01MBean
     */
    public void btnLinkGroup(Require01MBean r01MBean) {
        try {
            Preconditions.checkArgument(selLinkGroup != null, "尚未選取群組，請確認！！");
        } catch (IllegalArgumentException e) {
            log.debug(e.getMessage(), e);
            MessagesUtils.showError(e.getMessage());
            return;
        }
        List<WorkLinkGroupDetail> groupDetails
                = relevanceManager.findByLinkGroupAndExcludeNo(selLinkGroup, null);
        if (groupDetails == null || groupDetails.isEmpty()) {
            MessagesUtils.showWarn("選取的資料已被異動，請重新查詢");
            return;
        }
        List<WorkLinkGroupDetail> relevances = Lists.newArrayList(groupDetails);
        WorkLinkGroupDetail groupDetail = new WorkLinkGroupDetail(
                r01MBean.getRequire().getSid(), r01MBean.getRequire().getRequireNo(), WorkSourceType.TECH_REQUEST);
        relevances.add(groupDetail);
        boolean isDoubleMainLink = relevanceManager.isGroupMainLinkDouble(relevances);
        if (isDoubleMainLink) {
            displayController.showPfWidgetVar("doubleMainLink_LinkGroup_confirm_wv");
        } else {
            this.doCreateRelevance(r01MBean, groupDetails);
        }
    }

    public void doCreateRelevanceLinkGroup(Require01MBean r01MBean) {
        try {
            Preconditions.checkArgument(selLinkGroup != null, "尚未選取群組，請確認！！");
        } catch (IllegalArgumentException e) {
            log.debug(e.getMessage(), e);
            MessagesUtils.showError(e.getMessage());
            return;
        }
        List<WorkLinkGroupDetail> groupDetails
                = relevanceManager.findByLinkGroupAndExcludeNo(selLinkGroup, null);
        if (groupDetails == null || groupDetails.isEmpty()) {
            MessagesUtils.showWarn("選取的資料已被異動，請重新查詢");
            return;
        }
        this.doCreateRelevance(r01MBean, groupDetails);
    }

    private void doCreateRelevance(Require01MBean r01MBean, List<WorkLinkGroupDetail> groupDetails) {
        try {
            this.updateRelevance(r01MBean, groupDetails);
            this.reLoadRelevance(r01MBean);
        } catch (Exception e) {
            log.error(r01MBean.getRequire().getRequireNo() + ":" + loginBean.getUser().getName() + ":建立關聯失敗..." + e.getMessage(), e);
            r01MBean.reBuildeByUpdateFaild();
            MessagesUtils.showError(e.getMessage());
        }
        displayController.hidePfWidgetVar("dlgRelevance");
        displayController.hidePfWidgetVar("doubleMainLink_AddDetail_confirm_wv");
        displayController.hidePfWidgetVar("doubleMainLink_LinkGroup_confirm_wv");
    }

    public void doCreateRelevanceAddGroup(Require01MBean r01MBean) {
        try {
            Preconditions.checkArgument(selRelevance != null, "尚未選取需求單，請確認！！");
            Preconditions.checkArgument(!selRelevance.isEmpty(), "尚未選取需求單，請確認！！");
            this.doCreateRelevance(r01MBean, selRelevance);
        } catch (IllegalArgumentException e) {
            log.error(r01MBean.getRequire().getRequireNo() + ":" + loginBean.getUser().getName() + ":建立關聯失敗..." + e.getMessage(), e);
            r01MBean.reBuildeByUpdateFaild();
            MessagesUtils.showWarn(e.getMessage());
        } catch (Exception e) {
            log.error(r01MBean.getRequire().getRequireNo() + ":" + loginBean.getUser().getName() + ":建立關聯失敗..." + e.getMessage(), e);
            r01MBean.reBuildeByUpdateFaild();
            MessagesUtils.showWarn(e.getMessage());
        }
    }

    /**
     * 取得關聯明細資料
     *
     * @return
     */
    public void findLinkGroupInfoDetail() {
        if (selLinkGroup == null || selLinkGroup.getSid() == null) {
            allLinkGroupDetails = Lists.newArrayList();
            return;
        }
        allLinkGroupDetails = relevanceManager.findByLinkGroupAndExcludeNo(
                selLinkGroup, null);
    }

    /**
     * 重新讀取
     *
     * @param r01MBean
     * @return List<LinkGroupInfo>
     */
    private void reLoadRelevance(Require01MBean r01MBean) {
        r01MBean.getGroupLinkMBean().clear();
        r01MBean.getBottomTabMBean().resetTabIdx(r01MBean);
        r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.LINK_GROUP);
        displayController.execute("reSearch()");
    }

    private void reLoadRelevanceByDel(Require01MBean r01MBean) {
        r01MBean.getGroupLinkMBean().clear();
        r01MBean.getRequire().setHasLink(!r01MBean.getGroupLinkMBean().findDetails(r01MBean.getRequire()).isEmpty());
        r01MBean.getBottomTabMBean().resetTabIdx(r01MBean);
        if (r01MBean.getRequire().getHasLink()) {
            r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.LINK_GROUP);
        } else {
            r01MBean.getBottomTabMBean().changeTabByTabAuto();
        }
        displayController.execute("reSearch()");
    }
    
    /**
     * popup confirm dialog
     */
    public void confirmDeleteDetail(WorkLinkGroupDetail selectedDetail){
        this.selectedDetail = selectedDetail;
        displayController.showPfWidgetVar("confirmation");
    }
    
    /**
     * update main relevance
     */
    public void updateMainRelevance(){
		relevanceManager.addMainLink(selectedDetail.getLinkGroup().getSid(),
				selectedDetail.getSourceSid(), selectedDetail.getSourceNo(),
				SecurityFacade.getUserSid());
		//change main relevance 
		selLinkGroup.setMainLinkNo(selectedDetail.getSourceNo());
		selLinkGroup.setMainLinkSid(selectedDetail.getSourceSid());
		
    }
    
    private void reloadWorkLinkGroup(Require01MBean r01MBean){
    	selLinkGroup = r01MBean.getRequire().getLinkGroup();
    }
}
