package com.cy.tech.request.web.controller.component.reqconfirm;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.helper.RequireFinishHelper;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepPanelActionService;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepService;
import com.cy.tech.request.logic.vo.RequireConfirmDepVO;
import com.cy.work.common.constant.WkConstants;
import com.cy.work.common.constant.WkMessage;
import com.cy.tech.request.vo.enums.ReqConfirmDepCompleteType;
import com.cy.tech.request.vo.enums.ReqConfirmDepProcType;
import com.cy.tech.request.vo.enums.ReqConfirmDepProgStatus;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.ConfirmCallbackDialogController;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 需求完成確認面板
 * 
 * @author allen1214_wu
 */
@Slf4j
@Controller
@Scope("view")
@NoArgsConstructor
public class ReqConfirmDepPanelComponent implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -793012674646680326L;
    // ========================================================================
    // 服務元件區
    // ========================================================================
    @Autowired
    private transient ConfirmCallbackDialogController confirmCallbackDialogController;
    @Autowired
    private transient RequireConfirmDepService requireConfirmDepService;
    @Autowired
    private transient RequireConfirmDepPanelActionService requireConfirmDepPanelActionService;
    @Autowired
    private transient ReqConfirmDepPanelHelper reqConfirmDepPanelHelper;
    @Autowired
    private transient RequireFinishHelper requireFinishHelper;
    @Autowired
    private transient DisplayController displayController;

    // ========================================================================
    // 畫面控制變數
    // ========================================================================
    @Setter
    @Getter
    private Integer tabIndex = 0;

    /**
     * 【確認單位】下拉選單選項
     */
    @Getter
    private List<SelectItem> confirmDepItems = Lists.newArrayList();

    /**
     * 確認單位【負責人】下拉選單選項
     */
    @Getter
    private List<SelectItem> confirmDepUserItems = Lists.newArrayList();

    /**
     * 是否為負責人編輯狀態
     */
    @Getter
    private boolean ownerInEdit = false;

    /**
     * 是否 disable 無須執行
     */
    @Getter
    private boolean disableWontdo = false;

    // ========================================================================
    // 資料存放區
    // ========================================================================
    /**
     * 主頁面 Manager Bean
     */
    @Autowired
    private LoginBean loginBean;
    /**
     * 主頁面 Manager Bean
     */
    @Autowired
    private Require01MBean require01MBean;
    /**
     * 此單據【所有】需求完成確認單位資料
     */
    @Getter
    private List<RequireConfirmDepVO> allRequireConfirmDepVOs;
    /**
     * 【登入者單位】所屬的 需求完成確認單位資料
     */
    @Getter
    private RequireConfirmDepVO loginUserRequireConfirmDepVO;

    // ========================================================================
    // view 變數區
    // ========================================================================
    @Setter
    @Getter
    private Integer loginUserRequireConfirmDepOwnerSid;

    @Setter
    @Getter
    private Long selectedRequireConfirmDepSid;

    /**
     * 已執行/確認/無需處理 說明
     */
    @Setter
    @Getter
    private String complateMemo;

    /**
     * 完成類別
     */
    private ReqConfirmDepCompleteType completeType;

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * 判斷是否顯示面板
     * 
     * @return
     */
    public boolean showPanel() {
        return this.showPanel(this.require01MBean);
    }

    public boolean showPanel(Require01MBean req01MBean) {
        // 防呆
        if (req01MBean == null) {
            return false;
        }
        return showPanel(req01MBean.getRequire());
    }

    public boolean showPanel(Require require) {
        // 防呆
        if (require == null || require.getRequireStatus() == null) {
            return false;
        }

        // 排除無需顯示面板的製作進度狀態
        if (!require.getRequireStatus().isShowRequireConfirmPanel()) {
            return false;
        }

        // 排除未產生需求製作進度的單據 REQ-1398 (7.0轉檔前資料相容)
        if (this.requireConfirmDepService == null) {
            this.requireConfirmDepService = WorkSpringContextHolder.getBean(RequireConfirmDepService.class);
        }
        if (this.requireConfirmDepService == null) {
            return true;
        }
        return this.requireConfirmDepService.isExsitRequireConfirmDep(require.getSid());
    }

    public void load() {
        if (!this.showPanel()) {
            return;
        }

        // 主檔 Sid
        String requireSid = require01MBean.getRequire().getSid();
        // ====================================
        // 變數初始化
        // ====================================
        this.completeType = null;
        this.complateMemo = null;
        this.ownerInEdit = false;
        this.loginUserRequireConfirmDepOwnerSid = WkConstants.MANAGER_VIRTAUL_USER_SID;

        this.loginUserRequireConfirmDepVO = null;
        this.confirmDepUserItems = Lists.newArrayList();
        this.confirmDepItems = Lists.newArrayList();

        // ====================================
        // 查詢需進行『需求完成確認』的單位
        // ====================================
        this.allRequireConfirmDepVOs = this.reqConfirmDepPanelHelper.prepareRequireConfirmDepInfo(
                requireSid,
                SecurityFacade.getUserSid());

        if (WkStringUtils.isEmpty(this.allRequireConfirmDepVOs)) {
            return;
        }

        // ====================================
        // 取得 登入者 所有可操作的單位
        // ====================================
        // 取得 登入者 所有可操作的單位
        List<Integer> userMatchDepSids = this.requireConfirmDepService.prepareUserMatchDepSids(
                SecurityFacade.getUserSid());

        if (this.selectedRequireConfirmDepSid == null) {
            // 以 dep sid 整理資料
            Map<Integer, RequireConfirmDepVO> requireConfirmDepVOMapByDepSid = allRequireConfirmDepVOs.stream()
                    .collect(Collectors.toMap(
                            RequireConfirmDepVO::getDepSid,
                            eachVo -> eachVo));

            for (Integer userMatchDepSid : userMatchDepSids) {
                if (requireConfirmDepVOMapByDepSid.keySet().contains(userMatchDepSid)) {
                    // 優先命中
                    this.selectedRequireConfirmDepSid = requireConfirmDepVOMapByDepSid.get(userMatchDepSid).getSid();
                    this.loginUserRequireConfirmDepVO = requireConfirmDepVOMapByDepSid.get(userMatchDepSid);
                    break;
                }
            }
        } else {

            // 以 ConfirmDepSid 整理資料
            Map<Long, RequireConfirmDepVO> requireConfirmDepVOMapByConfirmDepSid = allRequireConfirmDepVOs.stream()
                    .collect(Collectors.toMap(
                            RequireConfirmDepVO::getSid,
                            eachVo -> eachVo));

            if (requireConfirmDepVOMapByConfirmDepSid.containsKey(this.selectedRequireConfirmDepSid)) {
                this.loginUserRequireConfirmDepVO = requireConfirmDepVOMapByConfirmDepSid.get(this.selectedRequireConfirmDepSid);
            } else {
                // 找不到選擇的單位時，取第一筆
                this.selectedRequireConfirmDepSid = this.allRequireConfirmDepVOs.get(0).getSid();
                this.loginUserRequireConfirmDepVO = this.allRequireConfirmDepVOs.get(0);
            }
        }

        // ====================================
        // 準備確認單位選單
        // ====================================
        if (this.loginUserRequireConfirmDepVO != null) {
            for (RequireConfirmDepVO requireConfirmDepVO : allRequireConfirmDepVOs) {
                // 過濾和登入者無關的資料
                if (!userMatchDepSids.contains(requireConfirmDepVO.getDepSid())) {
                    continue;
                }
                // 逐筆建立選項
                this.confirmDepItems.add(new SelectItem(requireConfirmDepVO.getSid(), requireConfirmDepVO.getDepName()));
            }
        }

        // ====================================
        // 準備【需求單位確認部門-負責人】下拉選單項目
        // ====================================
        if (this.loginUserRequireConfirmDepVO != null) {
            this.confirmDepUserItems = this.reqConfirmDepPanelHelper.prepareLoginDepOwnerSelectItems(
                    this.loginUserRequireConfirmDepVO);
        }
    }

    /**
     * 
     */
    public void loadx() {

        if (!this.showPanel()) {
            return;
        }

        // 主檔 Sid
        String requireSid = require01MBean.getRequire().getSid();
        // ====================================
        // 變數初始化
        // ====================================
        this.completeType = null;
        this.complateMemo = null;
        this.ownerInEdit = false;
        this.loginUserRequireConfirmDepOwnerSid = WkConstants.MANAGER_VIRTAUL_USER_SID;

        // ====================================
        // 查詢需進行『需求完成確認』的單位
        // ====================================
        this.allRequireConfirmDepVOs = this.reqConfirmDepPanelHelper.prepareRequireConfirmDepInfo(
                requireSid,
                SecurityFacade.getUserSid());

        // ====================================
        // 比對登入部門對應的確認檔
        // ====================================
        // 取得比對結果
        this.selectedRequireConfirmDepSid = this.reqConfirmDepPanelHelper.findLoginUserConfirmDepSid(
                this.allRequireConfirmDepVOs,
                SecurityFacade.getUserSid());

        // ====================================
        // 準備顯示資料
        // ====================================
        this.prepareSelectedConfirmDepData(
                SecurityFacade.getUserSid(),
                this.selectedRequireConfirmDepSid,
                this.allRequireConfirmDepVOs);
    }

    public void loadTarget() {

        if (!this.showPanel()) {
            return;
        }

        // 主檔 Sid
        String requireSid = require01MBean.getRequire().getSid();
        // ====================================
        // 變數初始化
        // ====================================
        this.completeType = null;
        this.complateMemo = null;
        this.ownerInEdit = false;
        this.loginUserRequireConfirmDepOwnerSid = WkConstants.MANAGER_VIRTAUL_USER_SID;

        // ====================================
        // 查詢需進行『需求完成確認』的單位
        // ====================================
        this.allRequireConfirmDepVOs = this.reqConfirmDepPanelHelper.prepareRequireConfirmDepInfo(
                requireSid,
                SecurityFacade.getUserSid());

        // ====================================
        // 準備顯示資料
        // ====================================
        this.prepareSelectedConfirmDepData(
                SecurityFacade.getUserSid(),
                this.selectedRequireConfirmDepSid,
                this.allRequireConfirmDepVOs);

    }

    private void prepareSelectedConfirmDepData(
            Integer loginUserSid,
            Long selectedRequireConfirmDepSid,
            List<RequireConfirmDepVO> allRequireConfirmDepVOs) {

        // ====================================
        // 初始化
        // ====================================
        this.loginUserRequireConfirmDepVO = null;
        this.loginUserRequireConfirmDepOwnerSid = -1;
        this.confirmDepUserItems = Lists.newArrayList();
        this.confirmDepItems = Lists.newArrayList();

        if (selectedRequireConfirmDepSid == null) {
            return;
        }

        // ====================================
        // 查詢和此登入者相關的『確認單位』（依據重要性排序）
        // ====================================
        List<Integer> userMatchDepSids = this.requireConfirmDepService.prepareUserMatchDepSids(loginUserSid);

        // 為空時, 無須繼續往下處理
        if (WkStringUtils.isEmpty(userMatchDepSids)) {
            log.info("無 和此登入者相關的『確認單位』 資料 ");
            return;
        }

        // ====================================
        // 準備確認單位選單
        // ====================================
        for (RequireConfirmDepVO requireConfirmDepVO : allRequireConfirmDepVOs) {
            // 過濾和登入者無關的資料
            if (!userMatchDepSids.contains(requireConfirmDepVO.getDepSid())) {
                continue;
            }

            // 逐筆建立選項
            this.confirmDepItems.add(new SelectItem(requireConfirmDepVO.getSid(), requireConfirmDepVO.getDepName()));
            // 比對所選擇的確認單位
            if (WkCommonUtils.compareByStr(requireConfirmDepVO.getSid(), selectedRequireConfirmDepSid)) {
                this.loginUserRequireConfirmDepVO = requireConfirmDepVO;
                log.debug("target requireConfirmDep is 【" + this.loginUserRequireConfirmDepVO.getDepName() + "】");
            }
        }

        // ====================================
        // 準備【需求單位確認部門-負責人】下拉選單項目
        // ====================================
        this.confirmDepUserItems = this.reqConfirmDepPanelHelper.prepareLoginDepOwnerSelectItems(
                this.loginUserRequireConfirmDepVO);

    }

    /**
     * @param event
     */
    public void onTabChange(org.primefaces.event.TabChangeEvent event) {
        String tabID = event.getTab().getId();
        if ("reqConfirmDepPanel_tabView_progress".equals(tabID)) {
            this.tabIndex = this.loginUserRequireConfirmDepVO == null ? 0 : 1;
        }
    }

    // ====================================================================================================
    // 權限控制
    // ====================================================================================================
    /**
     * 取得 需求完成確認單位 - 處理結果類型 說明
     * 
     * @param item
     * @return
     */
    public String getCompleteTypeName(String item) {
        ReqConfirmDepCompleteType type = ReqConfirmDepCompleteType.valueOf(item);

        if (type == null) {
            return "";
        }

        return type.getLabel();
    }

    /**
     * 依據需求製作進度, 決定是否可使用
     * 
     * @return
     */
    public boolean isReqStatusInCanUse() {
        // 防呆
        if (require01MBean == null
                || require01MBean.getRequire() == null
                || require01MBean.getRequire().getRequireStatus() == null) {
            return false;
        }

        // 依據製作進度決定是否顯示
        return require01MBean.getRequire().getRequireStatus().isCanUseRequireConfirm();
    }

    /**
     * 顯示【領單】按鈕
     * 
     * @return
     */
    public boolean isShowBtnRecive() { return isCanReceive(this.loginUserRequireConfirmDepVO); }

    /**
     * 可領單
     * 
     * @param loginDepVO
     * @return
     */
    private boolean isCanReceive(RequireConfirmDepVO loginDepVO) {
        // 登入者非需求確認部門使用者
        if (loginDepVO == null) {
            return false;
        }

        // 領單不影響流程, 故不卡控需求單狀態
        // 為不可用的需求製作進度狀態
        // if (!isReqStatusInCanUse()) {
        // return false;
        // }

        // 確認狀態需為【待確認】
        if (!ReqConfirmDepProgStatus.WAIT_CONFIRM.equals(loginDepVO.getProgStatus())) {
            return false;
        }
        return true;
    }

    /**
     * 顯示【完成/確認】按鈕
     * 
     * @return
     */
    public boolean isShowBtnFinish() { return this.isCanFinish(this.loginUserRequireConfirmDepVO); }

    /**
     * @param loginDepVO
     * @return
     */
    private boolean isCanFinish(RequireConfirmDepVO loginDepVO) {

        // 登入者非需求確認部門使用者
        if (loginDepVO == null) {
            return false;
        }
        // 為不可用的需求製作進度狀態
        if (!isReqStatusInCanUse()) {
            return false;
        }
        // 進度需為【進行中】
        if (!ReqConfirmDepProgStatus.IN_PROCESS.equals(loginDepVO.getProgStatus())) {
            return false;
        }

        return true;
    }

    /**
     * 顯示【復原】按鈕
     * 
     * @return
     */
    public boolean isShowBtnRecovery() { return isCanRecovery(this.loginUserRequireConfirmDepVO); }

    /**
     * @param loginDepVO
     * @return
     */
    private boolean isCanRecovery(RequireConfirmDepVO loginDepVO) {

        // 登入者非需求確認部門使用者
        if (loginDepVO == null) {
            return false;
        }
        // 為不可用的需求製作進度狀態
        if (!isReqStatusInCanUse()) {
            return false;
        }
        // 進度為『流程終點狀態』時可使用參考 REQ-1434
        ReqConfirmDepProgStatus reqConfirmDepProgStatus = this.loginUserRequireConfirmDepVO.getProgStatus();
        if (reqConfirmDepProgStatus == null || !reqConfirmDepProgStatus.isInFinish()) {
            return false;
        }

        return true;
    }

    /**
     * 顯示【異動負責人】按鈕
     * 
     * @return
     */
    public boolean isShowBtnOwnerEdit() {

        // 已經為編輯模式時不顯示
        if (this.ownerInEdit) {
            return false;
        }

        return isCanEditOwner();
    }

    /**
     * 是否可編輯負責人
     * 
     * @return
     */
    public boolean isCanEditOwner() { return isCanEditOwner(this.loginUserRequireConfirmDepVO); }

    /**
     * 是否可編輯負責人
     * 
     * @param loginDepVO
     * @return
     */
    private boolean isCanEditOwner(RequireConfirmDepVO loginDepVO) {

        // 登入者非需求確認部門使用者
        if (loginDepVO == null) {
            return false;
        }
        // 進度為【已完成】時鎖定
        if (ReqConfirmDepProgStatus.COMPLETE.equals(loginDepVO.getProgStatus())) {
            return false;
        }

        return true;
    }

    // ====================================================================================================
    // 【異動負責人】操作
    // ====================================================================================================
    /**
     * 異動負責人 - 編輯
     */
    public void changeOwner_edit() {
        // ====================================
        // 取得最新資料檔狀態
        // ====================================
        RequireConfirmDepVO newLoginrequireConfirmDepVO = null;
        if (this.loginUserRequireConfirmDepVO != null) {
            newLoginrequireConfirmDepVO = this.requireConfirmDepService.findVoBySid(this.loginUserRequireConfirmDepVO.getSid());
        }

        // ====================================
        // 檢查編輯權限
        // ====================================
        if (!isCanEditOwner(newLoginrequireConfirmDepVO)) {
            this.actionForDataChanged();
            return;
        }

        this.ownerInEdit = true;
    }

    /**
     * 異動負責人 - 取消
     */
    public void changeOwner_cancel() {
        this.ownerInEdit = false;
        // 復原選擇前
        this.loginUserRequireConfirmDepOwnerSid = this.loginUserRequireConfirmDepVO.getOwnerSid();
    }

    /**
     * @param event
     */
    public void changeOwner_save_preConfirm() {

        // ====================================
        // 檢核未異動
        // ====================================
        if (WkCommonUtils.compareByStr(
                this.loginUserRequireConfirmDepOwnerSid,
                this.loginUserRequireConfirmDepVO.getOwnerSid())) {
            this.changeOwner_cancel();
            return;
        }

        // ====================================
        // 組確認訊息
        // ====================================
        String message = "將異動負責人為：";
        message += this.requireConfirmDepService.prepareOwnerName(
                this.loginUserRequireConfirmDepOwnerSid,
                this.loginUserRequireConfirmDepVO.getDepSid());

        // ====================================
        // 呼叫確認視窗
        // ====================================
        this.require01MBean.getConfirmCallbackDialogController().showConfimDialog(
                message,
                Lists.newArrayList(
                        "@(.reqConfirmDepPanel_datatable)",
                        "@(.reqConfirmDepPanel_top)"),
                () -> changeOwner_save());
    }

    /**
     * 異動負責人 - 儲存
     */
    public void changeOwner_save() {

        try {
            // ====================================
            // 取得最新資料檔狀態
            // ====================================
            RequireConfirmDepVO newLoginrequireConfirmDepVO = null;
            if (this.loginUserRequireConfirmDepVO != null) {
                newLoginrequireConfirmDepVO = this.requireConfirmDepService.findVoBySid(this.loginUserRequireConfirmDepVO.getSid());
            }

            // ====================================
            // 檢查編輯權限
            // ====================================
            if (!isCanEditOwner(newLoginrequireConfirmDepVO)) {
                this.actionForDataChanged();
                return;
            }

            // ====================================
            // log 計時開始
            // ====================================
            Long startTime = System.currentTimeMillis();
            String procTitle = "需求完成確認單位 - 異動負責人";
            log.info(WkCommonUtils.prepareCostMessageStart(procTitle));

            // ====================================
            // 異動資料
            // ====================================
            // 並更新資料
            this.requireConfirmDepPanelActionService.changeOwner(
                    this.require01MBean.getRequire().getRequireNo(),
                    this.loginUserRequireConfirmDepVO,
                    this.loginUserRequireConfirmDepOwnerSid,
                    SecurityFacade.getUserSid(),
                    loginBean.getCompanySid(),
                    new Date());

            // ====================================
            // 更新資料
            // ====================================
            this.load();

            // ====================================
            // 畫面控制
            // ====================================
            this.ownerInEdit = false;

            // ====================================
            // log ：執行計時結束
            // ====================================
            log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
        } catch (Exception e) {
            String message = "異動負責人失敗! [" + e.getMessage() + "]";
            log.error(message, e);
            MessagesUtils.showError(message);
            return;
        }
    }

    // ====================================================================================================
    // 【異動正在操作的確認單位】
    // ====================================================================================================
    public void changeRequireDep() {
        this.load();
    }

    // ====================================================================================================
    // 【領單】
    // ====================================================================================================
    /**
     * 領單
     */
    public void btn_receive_preConfirm(ActionEvent event) {
        // ====================================
        // 取得最新資料檔狀態
        // ====================================
        RequireConfirmDepVO newLoginrequireConfirmDepVO = null;
        if (this.loginUserRequireConfirmDepVO != null) {
            newLoginrequireConfirmDepVO = this.requireConfirmDepService.findVoBySid(this.loginUserRequireConfirmDepVO.getSid());
        }

        // ====================================
        // 檢查權限
        // ====================================
        if (!this.isCanReceive(newLoginrequireConfirmDepVO)) {
            this.actionForDataChanged();
            return;
        }

        // ====================================
        // 組確認訊息
        // ====================================
        String message = "請確認本案件的 單位負責人/聯絡窗口 為:【" + this.loginUserRequireConfirmDepVO.getOwnerName() + "】";

        // ====================================
        // 呼叫確認視窗
        // ====================================
        this.require01MBean.getConfirmCallbackDialogController().showConfimDialog(
                message,
                "@(.reqConfirmDepPanel_tabView)",
                () -> btn_receive());
    }

    /**
     * 領單
     */
    public void btn_receive() {

        try {
            // ====================================
            // 取得最新資料檔狀態
            // ====================================
            RequireConfirmDepVO newLoginrequireConfirmDepVO = null;
            if (this.loginUserRequireConfirmDepVO != null) {
                newLoginrequireConfirmDepVO = this.requireConfirmDepService.findVoBySid(this.loginUserRequireConfirmDepVO.getSid());
            }

            // ====================================
            // 檢查權限
            // ====================================
            if (!this.isCanReceive(newLoginrequireConfirmDepVO)) {
                this.actionForDataChanged();
                return;
            }

            // ====================================
            // log 計時開始
            // ====================================
            Long startTime = System.currentTimeMillis();
            String procTitle = "需求完成確認單位 - 領單" + ReqConfirmDepProcType.RECEIVE.getLabel();
            log.info(WkCommonUtils.prepareCostMessageStart(procTitle));

            // ====================================
            // 執行領單
            // ====================================
            this.requireConfirmDepPanelActionService.actionReceive(
                    this.require01MBean.getRequire().getRequireNo(),
                    this.loginUserRequireConfirmDepVO.getSid(),
                    "領單",
                    SecurityFacade.getUserSid());

            // ====================================
            // log ：執行計時結束
            // ====================================
            log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));

        } catch (Exception e) {
            String message = "執行領單失敗! [" + e.getMessage() + "]";
            log.error(message, e);
            MessagesUtils.showError(message);
            return;
        }

        this.load();
        this.require01MBean.getDisplayController().update("reqConfirmDepPanel_tabView");
    }

    // ====================================================================================================
    // 【已完成/確認】【無需處理】
    // ====================================================================================================
    /**
     * @param completeTypeStr
     */
    public void btn_pre_finish(String completeTypeStr) {

        this.complateMemo = "";

        // ====================================
        // 取得完成類型
        // ====================================
        this.completeType = ReqConfirmDepCompleteType.valueOf(completeTypeStr);
        if (this.completeType == null) {
            log.error("傳入類別找不到!:[" + completeTypeStr + "]");
            MessagesUtils.showError("傳入類別找不到!:[" + completeTypeStr + "]");
            return;
        }

        // ====================================
        // 檢查
        // ====================================
        // 檢查單據狀態、可執行權限
        if (!this.finishValidate()) {
            return;
        }

        // 檢查單據未簽核完成 (未簽核完成不可)
        String warnMessage = this.requireFinishHelper.validateSignNotCompleteByConfirmDep(
                this.require01MBean.getRequire(),
                this.loginUserRequireConfirmDepVO.getDepSid(),
                this.completeType);

        if (WkStringUtils.notEmpty(warnMessage)) {
            MessagesUtils.showWarn(warnMessage);
            return;
        }

        // ====================================
        // 子單狀態檢查
        // ====================================
        // 檢查子單是否已全部完成
        String confirmMessage = this.reqConfirmDepPanelHelper.validateNotFinish(
                this.loginUserRequireConfirmDepVO.getDepSid(),
                this.require01MBean.getRequire().getRequireNo());

        if (WkStringUtils.notEmpty(confirmMessage)) {
            confirmCallbackDialogController.showConfimDialog(
                    confirmMessage,
                    "",
                    () -> this.btn_finish_Finish_Memo_dialog());
            return;
        }

        this.btn_finish_Finish_Memo_dialog();
    }

    /**
     * 開啟完成說明 dialog
     */
    public void btn_finish_Finish_Memo_dialog() {
        log.debug("開啟【" + this.completeType.getLabel() + "】前置對話框");
        // 顯示說明輸入框
        this.displayController.update("reqConfirmDepPanel_Finish_Memo_dialog_content");
        String execScript = "PF('reqConfirmDepPanel_Finish_Memo_dialog').show();";
        this.require01MBean.getDisplayController().execute(execScript);
    }

    /**
     * 完成
     */
    public void btn_finish() {

        // 是否執行主單需求完成
        boolean isExecRequireComplete = false;

        try {
            // ====================================
            // 無需處理時須輸入說明
            // ====================================
            // 防呆
            if (this.completeType == null) {
                log.error("完成類別已被清空");
                MessagesUtils.showError("完成類別已被清空");
                this.require01MBean.getDisplayController().hidePfWidgetVar("reqConfirmDepPanel_Finish_Memo_dialog");
                return;
            }

            if (ReqConfirmDepCompleteType.WONT_DO.equals(this.completeType)) {
                // 取得無 html 標籤的文字
                String complateMemoText = WkJsoupUtils.getInstance().clearCssTag(this.complateMemo);
                // 去斷行
                complateMemoText = complateMemoText.replaceAll("\r\n", "");
                complateMemoText = complateMemoText.replaceAll("\n", "");
                // 檢查為空
                if (WkStringUtils.isEmpty(complateMemoText)) {
                    MessagesUtils.showError(ReqConfirmDepCompleteType.WONT_DO.getLabel() + "時，請填寫說明！");
                    return;
                }
            }

            // ====================================
            // 執行權限檢查
            // ====================================
            if (!this.finishValidate()) {
                return;
            }

            // ====================================
            // log 計時開始
            // ====================================
            Long startTime = System.currentTimeMillis();
            String procTitle = "需求完成確認單位 - " + this.completeType.getLabel();
            log.info(WkCommonUtils.prepareCostMessageStart(procTitle));

            // ====================================
            // 執行
            // ====================================
            isExecRequireComplete = this.requireConfirmDepPanelActionService.actionComplete(
                    this.require01MBean.getRequire().getRequireNo(),
                    this.loginUserRequireConfirmDepVO.getSid(),
                    SecurityFacade.getUserSid(),
                    this.completeType,
                    this.complateMemo);

            // ====================================
            // log ：執行計時結束
            // ====================================
            log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));

        } catch (UserMessageException e) {
            MessagesUtils.showInfo(e.getMessage());
            // 繼續執行關閉視窗和 reload
        } catch (Exception e) {
            String message = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            log.error(message, e);
            MessagesUtils.showError(message);
            return;
        }

        // ====================================
        // 畫面控制
        // ====================================
        // 初始化畫面元件
        this.initClinetComponent();
        // 進行畫面更新
        this.require01MBean.reBuildeAndGotoTraceTab();

        String message = "";
        if (ReqConfirmDepCompleteType.WONT_DO.equals(this.completeType)) {
            message = "已確認此需求無需處理！";
        } else {
            message = "單位已執行/確認需求完成！";
        }

        if (isExecRequireComplete) {
            message += "<br/><br/>所有分派單位皆已確認完成，製作進度變更為『已完成』";
        }
        MessagesUtils.showInfo(message);
    }

    /**
     * 執行：完成 前檢查
     * 
     * @return
     */
    private boolean finishValidate() {

        // ====================================
        // 取得最新資料檔狀態
        // ====================================
        RequireConfirmDepVO newLoginrequireConfirmDepVO = null;
        if (this.loginUserRequireConfirmDepVO != null) {
            newLoginrequireConfirmDepVO = this.requireConfirmDepService.findVoBySid(this.loginUserRequireConfirmDepVO.getSid());
        }

        // ====================================
        // 檢查可執行權限
        // ====================================
        if (!this.isCanFinish(newLoginrequireConfirmDepVO)) {
            this.actionForDataChanged();
            return false;
        }

        return true;
    }

    // ====================================================================================================
    // 【復原】
    // ====================================================================================================
    /**
     * @param completeTypeStr
     */
    public void btn_pre_recovery() {

        this.completeType = null;
        this.complateMemo = "";

        // ====================================
        // 取得最新資料檔狀態
        // ====================================
        RequireConfirmDepVO newLoginrequireConfirmDepVO = null;
        if (this.loginUserRequireConfirmDepVO != null) {
            newLoginrequireConfirmDepVO = this.requireConfirmDepService.findVoBySid(this.loginUserRequireConfirmDepVO.getSid());
        }

        // ====================================
        // 執行前檢查
        // ====================================
        // 檢查可執行權限
        if (!this.isCanRecovery(newLoginrequireConfirmDepVO)) {
            this.actionForDataChanged();
            return;
        }

        log.debug("開啟復原前置對話框");
        // 顯示說明輸入框
        String execScript = "PF('reqConfirmDepPanel_Finish_Memo_dialog').show();";
        this.require01MBean.getDisplayController().execute(execScript);
    }

    /**
     * 復原
     */
    public void btn_recovery() {

        // 是否執行反需求完成
        boolean isRoolbackReqStatus = false;

        try {

            // 取得無 html 標籤的文字
            String complateMemoText = WkJsoupUtils.getInstance().clearCssTag(this.complateMemo);
            // 去斷行
            complateMemoText = complateMemoText.replaceAll("\r\n", "");
            complateMemoText = complateMemoText.replaceAll("\n", "");
            // 檢查為空
            if (WkStringUtils.isEmpty(complateMemoText)) {
                MessagesUtils.showError("請填寫復原說明！");
                return;
            }

            // ====================================
            // 取得最新資料檔狀態
            // ====================================
            RequireConfirmDepVO newLoginrequireConfirmDepVO = null;
            if (this.loginUserRequireConfirmDepVO != null) {
                newLoginrequireConfirmDepVO = this.requireConfirmDepService.findVoBySid(this.loginUserRequireConfirmDepVO.getSid());
            }

            // ====================================
            // 執行前檢查
            // ====================================
            // 檢查可執行權限
            if (!this.isCanRecovery(newLoginrequireConfirmDepVO)) {
                this.actionForDataChanged();
                return;
            }

            // ====================================
            // log 計時開始
            // ====================================
            Long startTime = System.currentTimeMillis();
            String procTitle = "需求完成確認單位 - " + ReqConfirmDepProcType.RECOVERY.getLabel();
            log.info(WkCommonUtils.prepareCostMessageStart(procTitle));

            // ====================================
            // 執行
            // ====================================
            isRoolbackReqStatus = this.requireConfirmDepPanelActionService.actionRecovary(
                    this.require01MBean.getRequire().getRequireNo(),
                    this.loginUserRequireConfirmDepVO.getSid(),
                    SecurityFacade.getUserSid(),
                    this.complateMemo);

            // ====================================
            // log ：執行計時結束
            // ====================================
            log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));

        } catch (SystemOperationException e) {
            MessagesUtils.showError(e.getMessage());
            log.error(e.getMessage(), e);
            // 繼續執行關閉視窗和 reload
        } catch (Exception e) {
            String message = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            log.error(message, e);
            MessagesUtils.showError(message);
            return;
        }

        // ====================================
        // 畫面控制
        // ====================================
        // 初始化畫面元件
        this.initClinetComponent();

        // 復原需求狀態, 進行畫面更新
        if (isRoolbackReqStatus) {
            this.require01MBean.reBuildeByUpdateFaild();
        }

        String message = "已復原單位確認狀態!";

        if (isRoolbackReqStatus) {
            message += "<br/><br/>需求進度變更『已完成』->『進行中』";
        }
        MessagesUtils.showInfo(message);
    }

    // ====================================================================================================
    // 其他
    // ====================================================================================================
    public void btn_memo_dialog_confirm() {
        if (this.completeType != null) {
            // log.debug("執行【" + this.completeType.getLabel() + "】");
            this.btn_finish();
        } else {
            // log.debug("執行【復原】");
            this.btn_recovery();
        }
    }

    /**
     * 
     */
    private void actionForDataChanged() {
        MessagesUtils.showInfo(WkMessage.NEED_RELOAD);
        this.initClinetComponent();
    }

    /**
     * 初始化 client 端元件
     */
    private void initClinetComponent() {
        // 關閉輸入視窗
        String execScript = "PF('reqConfirmDepPanel_Finish_Memo_dialog').hide();";
        // 關閉製作進度面板 (避免資料未更新, 強迫使用者重新點開, 觸發資料重新讀取)
        execScript += ReqConfirmClientHelper.prepareClientPanelInitScript(this.require01MBean.getRequire());
        this.require01MBean.getDisplayController().execute(execScript);
    }
}
