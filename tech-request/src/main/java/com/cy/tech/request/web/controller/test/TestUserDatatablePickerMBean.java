package com.cy.tech.request.web.controller.test;

import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.enums.OrgLevel;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.viewcomponent.usrdtpker.UserDatatablePickerComponent;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Controller
@Scope("view")
@Slf4j
public class TestUserDatatablePickerMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6910877623938782458L;
    /**
     * 項目樹
     */
    @Getter
    private UserDatatablePickerComponent userDatatablePickerComponent;
    @Getter
    private String componentID = "UserDatatablePickerComponentID";
    
    @Getter
    @Setter
    private Integer menuWidth = 1000;
    @Getter
    @Setter
    private Integer menuHeight = 400;

    @PostConstruct
    public void init() {

        // ====================================
        // 預設顯示部門
        // ====================================
        Set<Integer> defaultSrcOrgSids = Sets.newHashSet();

        // 取得基礎部門 - 到處
        Integer baseOrgSid = WkOrgUtils.prepareBaseDepSid(SecurityFacade.getPrimaryOrgSid(), OrgLevel.DIVISION_LEVEL);
        defaultSrcOrgSids.add(baseOrgSid);
        // 處以下部門
        Set<Integer> baseOrgChildSids = WkOrgCache.getInstance().findAllChildSids(baseOrgSid);
        if (WkStringUtils.notEmpty(baseOrgChildSids)) {
            defaultSrcOrgSids.addAll(baseOrgChildSids);
        }

        // 取得管理部門
        Set<Integer> managerDepSids = WkOrgCache.getInstance().findManagerOrgSids(SecurityFacade.getUserSid());
        if (WkStringUtils.notEmpty(managerDepSids)) {
            for (Integer managerDepSid : managerDepSids) {
                // 加入管理部門
                defaultSrcOrgSids.add(managerDepSid);
                // 加入管理部門含以下
                Set<Integer> managerDepChildSids = WkOrgCache.getInstance().findAllChildSids(baseOrgSid);
                if (WkStringUtils.notEmpty(managerDepChildSids)) {
                    defaultSrcOrgSids.addAll(managerDepChildSids);
                }
            }

        }

        // ====================================
        // 預設顯示部門
        // ====================================
        try {
            this.userDatatablePickerComponent = new UserDatatablePickerComponent(
                    componentID,
                    SecurityFacade.getCompanyId(),
                    Lists.newArrayList(3383, 1342),
                    Lists.newArrayList(1342),
                    Lists.newArrayList(),
                    defaultSrcOrgSids.stream().collect(Collectors.toList()));
        } catch (Exception e) {
            MessagesUtils.showError("初始化錯誤!");
            log.error("初始化錯誤", e);
        }
    }

}
