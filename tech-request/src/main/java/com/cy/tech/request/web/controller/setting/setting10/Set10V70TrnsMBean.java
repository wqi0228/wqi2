
/**
 * 
 */
package com.cy.tech.request.web.controller.setting.setting10;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.Set10V70TrnsService;
import com.cy.tech.request.logic.service.onetimetrns.AddDefaultNoticeDepToCaseService;
import com.cy.tech.request.logic.service.setting.onetimetrns.Setting10OneTimeTrnsForCheckItemsService;
import com.cy.tech.request.logic.service.setting.onetimetrns.Setting10OneTimeTrnsForDefaultAsDepService;
import com.cy.tech.request.logic.service.setting.onetimetrns.Setting10OneTimeTrnsforPrivateGroupService;
import com.cy.tech.request.logic.service.Set10V70TrneBatchHelper;
import com.cy.tech.request.vo.require.AssignSendInfoForTrnsVO;
import com.cy.tech.request.web.controller.setting.Setting10MBean;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 *
 */
@Slf4j
@Controller
@Scope("view")
public class Set10V70TrnsMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -212897214193524459L;
    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private transient Set10V70TrnsService set10V70TrnsService;
    @Autowired
    transient private Setting10MBean set10Bean;
    @Autowired
    transient private Set10V70TrneBatchHelper set10V70TrneBatchHelper;
    @Autowired
    transient private AddDefaultNoticeDepToCaseService addDefaultNoticeDepToCaseService;
    @Autowired
    transient private Setting10OneTimeTrnsForCheckItemsService setting10OneTimeTrnsForCheckItemsService;
    @Autowired
    transient private Setting10OneTimeTrnsForDefaultAsDepService setting10OneTimeTrnsForDefaultAsDepService;
    @Autowired
    transient private Setting10OneTimeTrnsforPrivateGroupService setting10OneTimeTrnsforPrivateGroupService;

    // ========================================================================
    // view 變數
    // ========================================================================
    /**
     * 起日
     */
    @Getter
    @Setter
    public Date condition_startDate;
    /**
     * 迄日
     */
    @Getter
    @Setter
    public Date condition_endDate;
    /**
     * 
     */
    @Getter
    @Setter
    public String condition_requireNos;

    // ========================================================================
    // 方法區
    // ========================================================================
    @PostConstruct
    public void init() {
        // log.info("set10V70TrnsMBean init");
        // condition_startDate = new GregorianCalendar(2016, 0, 1).getTime();
        // condition_endDate = new Date();
    }

    /**
     * 轉檔
     */
    public void trnsData() {

        this.set10Bean.clearInfoScreenMsg();

        try {

            String logMessage = "開始處理轉檔!";
            this.set10Bean.addInfoMsg("\r\n" + logMessage);
            log.info(logMessage);

            Long startTime = System.currentTimeMillis();

            // ====================================
            // 以需求單號, 分類資料
            // ====================================
            Map<String, List<AssignSendInfoForTrnsVO>> assignSendInfoMapByReqNo = null;

            try {
                assignSendInfoMapByReqNo = this.prepareRequireNoAssignSendInfoForTrnsVOMap();
            } catch (UserMessageException e) {
                MessagesUtils.show(e);
                return;
            }

            if (assignSendInfoMapByReqNo.size() == 0) {
                logMessage = "找不到需要轉檔的需求單!";
                this.set10Bean.addInfoMsg("\r\n" + logMessage);
                log.info(logMessage);
                return;
            } else {
                logMessage = "開始處理需要轉檔的需求單共[" + assignSendInfoMapByReqNo.size() + "]筆!";
                this.set10Bean.addInfoMsg("\r\n" + logMessage);
                log.info(logMessage);
            }

            // ====================================
            // 查詢已經有確認檔的需求單
            // ====================================
            List<String> exsitConfirmDepRequireSids = this.set10V70TrneBatchHelper.queryExsitConfirmDep();

            // ====================================
            //
            // ====================================
            Iterator<String> reqNosIt = assignSendInfoMapByReqNo.keySet().stream().sorted().collect(Collectors.toList()).iterator();
            List<String> currRequireNos = Lists.newArrayList();
            // 每 500 筆 commit 一次
            while (reqNosIt.hasNext()) {
                currRequireNos.add(reqNosIt.next());

                if (currRequireNos.size() >= 500) {
                    this.set10V70TrnsService.process(
                            currRequireNos,
                            assignSendInfoMapByReqNo,
                            exsitConfirmDepRequireSids,
                            SecurityFacade.getUserSid());
                    currRequireNos.clear();
                }

            }
            // 執行 while loop 最後一次剩下的
            if (currRequireNos.size() > 0) {
                this.set10V70TrnsService.process(
                        currRequireNos,
                        assignSendInfoMapByReqNo,
                        exsitConfirmDepRequireSids,
                        SecurityFacade.getUserSid());
            }

            logMessage = WkCommonUtils.prepareCostMessage(startTime, "總耗時");
            log.info(logMessage);
            this.set10Bean.addInfoMsg("\r\n" + logMessage);
        } catch (Exception e) {
            this.set10Bean.addInfoMsg("\r\n執行錯誤!");
            MessagesUtils.showError("執行錯誤!");
            log.error("執行錯誤", e);
            return;
        }

        this.set10Bean.addInfoMsg("\r\n轉檔完成");
        MessagesUtils.showInfo("轉檔完成");
    }

    /**
     * @return
     * @throws UserMessageException
     */
    private Map<String, List<AssignSendInfoForTrnsVO>> prepareRequireNoAssignSendInfoForTrnsVOMap() throws UserMessageException {
        // ====================================
        // 解析輸入的條件
        // ====================================
        // 最小日期
        String greaterThanRequireNo = "";
        if (condition_startDate != null) {
            Calendar cal = Calendar.getInstance(TimeZone.getDefault());
            cal.setTime(condition_startDate);
            greaterThanRequireNo = "TGTR"
                    + WkStringUtils.padding(cal.get(Calendar.YEAR) + "", '0', 4, true)
                    + WkStringUtils.padding((cal.get(Calendar.MONTH) + 1) + "", '0', 2, true)
                    + WkStringUtils.padding(cal.get(Calendar.DAY_OF_MONTH) + "", '0', 2, true)
                    + "000";
        }
        // 最大日期
        String lessThanRequireNo = "";
        if (condition_endDate != null) {
            Calendar cal = Calendar.getInstance(TimeZone.getDefault());
            cal.setTime(condition_endDate);
            lessThanRequireNo = "TGTR"
                    + WkStringUtils.padding(cal.get(Calendar.YEAR) + "", '0', 4, true)
                    + WkStringUtils.padding((cal.get(Calendar.MONTH) + 1) + "", '0', 2, true)
                    + WkStringUtils.padding(cal.get(Calendar.DAY_OF_MONTH) + "", '0', 2, true)
                    + "999";
        }
        // 需求單號字串
        List<String> requireNos = null;
        String currCondition_requireNos = WkStringUtils.safeTrim(this.condition_requireNos);
        // 去斷行
        currCondition_requireNos = currCondition_requireNos.replaceAll("\r\n", currCondition_requireNos);
        currCondition_requireNos = currCondition_requireNos.replaceAll("\n", currCondition_requireNos);
        // 去空白
        currCondition_requireNos = currCondition_requireNos.replaceAll(" ", currCondition_requireNos);
        if (WkStringUtils.notEmpty(currCondition_requireNos)) {
            requireNos = Lists.newArrayList(currCondition_requireNos.split(","));
        }
        // 去除空白資料
        if (WkStringUtils.notEmpty(requireNos)) {
            requireNos = requireNos.stream().filter(requireNo -> WkStringUtils.notEmpty(requireNo)).collect(Collectors.toList());
        }

        // ====================================
        // 查詢
        // ====================================
        List<AssignSendInfoForTrnsVO> assignSendInfos = this.set10V70TrnsService.queryForV70Trns(
                requireNos,
                greaterThanRequireNo,
                lessThanRequireNo);

        // ====================================
        // 以需求單號, 分類資料
        // ====================================
        return assignSendInfos.stream()
                .collect(Collectors.groupingBy(
                        AssignSendInfoForTrnsVO::getRequireNo,
                        Collectors.mapping(
                                each -> each,
                                Collectors.toList())));

    }

    public void proceeAddAssignDepForInternal() {

        try {
            this.set10V70TrnsService.proceeAddAssignDepForInternal();
            MessagesUtils.showInfo("處理結束!");
        } catch (Throwable e) {
            log.error("轉檔失敗", e);
            MessagesUtils.showError("轉檔失敗!");
        }
    }

    /**
     * 減派已停用部門
     */
    public void clearAssignNoticeDeps(String isTrueInsert) {
        try {
            this.set10V70TrnsService.clearAssignNoticeDeps("TRUE".equals(isTrueInsert));
            MessagesUtils.showInfo("處理結束!");
        } catch (Throwable e) {
            log.error("轉檔失敗", e);
            MessagesUtils.showError("轉檔失敗!");
        }
    }

    /**
     * 補預設通知單位
     */
    public void addDefaultNoticeDepToCaseService() {

        try {
            if (WkStringUtils.isEmpty(this.condition_requireNos)) {
                this.addDefaultNoticeDepToCaseService.processByDate(this.condition_startDate, this.condition_endDate);
            } else {
                this.addDefaultNoticeDepToCaseService.processByRequireNos(this.condition_requireNos);
            }

            MessagesUtils.showInfo("處理結束!");

        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Throwable e) {
            log.error("轉檔失敗", e);
            MessagesUtils.showError("轉檔失敗!" + e.getMessage());
            return;
        }
    }

    /**
     * 一次性轉檔 - 檢查項目 (系統別)
     */
    public void processTrnsForCheckItems() {
        try {
            this.setting10OneTimeTrnsForCheckItemsService.process();
            MessagesUtils.showInfo("處理結束!");

        } catch (Throwable e) {
            log.error("轉檔失敗", e);
            MessagesUtils.showError("轉檔失敗!" + e.getMessage());
            return;
        }
    }

    /**
     * 一次性轉檔 - 預設分派/通知單位
     */
    public void processTrnsForDefailtAsDep() {
        try {
            this.setting10OneTimeTrnsForDefaultAsDepService.process();
            MessagesUtils.showInfo("處理結束!");

        } catch (Throwable e) {
            log.error("轉檔失敗", e);
            MessagesUtils.showError("轉檔失敗!" + e.getMessage());
            return;
        }
    }

    /**
     * 一次性轉檔 - 使用者自定義群組
     */
    public void processTrnsForPrivateGroup() {
        try {
            this.setting10OneTimeTrnsforPrivateGroupService.process();
            MessagesUtils.showInfo("處理結束!");

        } catch (Throwable e) {
            log.error("轉檔失敗", e);
            MessagesUtils.showError("轉檔失敗!" + e.getMessage());
            return;
        }
    }

}
