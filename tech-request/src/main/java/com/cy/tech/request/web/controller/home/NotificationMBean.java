package com.cy.tech.request.web.controller.home;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.NotificationService;
import com.cy.tech.request.vo.value.to.NotificationEventVO;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 最新通知
 * @author aken_kao
 *
 */
@Slf4j
@Controller
@Scope("view")
public class NotificationMBean implements Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = 6509907159037301444L;

    @Autowired
    private NotificationService notificationService;
    
    @Getter
    private List<NotificationEventVO> resultList; 
    @Getter
    private List<NotificationEventVO> selectedResultList = Lists.newArrayList();
    @Getter
    @Setter
    private boolean selectedAll = false;
    
    @PostConstruct
    void init() {
        search();
    }
    
    public void search() {
        this.resultList = notificationService.findAllByUserSid(SecurityFacade.getUserSid());
        this.selectedAll = false;
    }
    
    /**
     * 標記已閱讀
     */
    public void markRead() {
        notificationService.markRead(selectedResultList);
        selectedResultList.clear();
        search();
    }
    
    /**
     * 變更dataTable 全選
     */
    public void checkBySelectedAll() {
        try {
            selectedResultList.clear();
            for(NotificationEventVO vo : resultList){
                vo.setChecked(selectedAll);
                if(selectedAll) {
                    selectedResultList.add(vo);
                }
            }
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
        }
    }
    
    /**
     * 點擊check box
     * @param vo
     */
    public void checkBySingle(NotificationEventVO vo) {
        try {
            if(vo==null) {
                return;
            }
            
            if (vo.isChecked()) {
                selectedResultList.add(vo);
                selectedAll = resultList.stream().allMatch(each -> each.isChecked());
            } else {
                selectedResultList.remove(vo);
                selectedAll = false;
            }
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
        }
    }
}
