/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search.helper;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.enums.ConditionInChargeMode;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search27QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.BaseSearchView;
import com.cy.tech.request.logic.search.view.Search27View;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.logic.vo.CustomerTo;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.web.controller.enums.ReqStatusMBean;
import com.cy.tech.request.web.controller.view.component.searchquery.SearchQuery27;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.logic.lib.manager.WorkCommonReadRecordManager;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 輔助 Search27MBean
 *
 * @author kasim
 */
@Component
public class Search27Helper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7570978279636138288L;
    @Autowired
    transient private Search27QueryService search27QueryService;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private OrganizationService orgSerivce;
    @Autowired
    transient private UserService userService;
    @Autowired
    transient private ReqStatusMBean reqStatusUtils;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchResultHelper searchResultHelper;
    @Autowired
    transient private WorkCommonReadRecordManager workCommonReadRecordManager;

    /**
     * 查詢列表資料
     *
     * @param searchQuery
     * @param customerItems
     * @return
     * @throws UserMessageException
     */
    @SuppressWarnings("unchecked")
    public List<Search27View> findWithQuery(
            String compId,
            SearchQuery27 searchQuery,
            List<CustomerTo> customerItems,
            Integer loginUserSid) throws UserMessageException {
        String requireNo = reqularUtils.getRequireNo(compId, searchQuery.getFuzzyText());
        StringBuilder builder = new StringBuilder();
        Map<String, Object> parameters = Maps.newHashMap();
        builder.append("SELECT "
                + "tr.require_sid,"
                + "tr.require_no,"
                + "tid.field_content,"
                + "tr.has_forward_dep,"
                + "tr.has_forward_member,"
                + "tr.has_link,"
                + "tr.create_dt,"
                + "ckm.big_category_name,"
                + "ckm.middle_category_name,"
                + "ckm.small_category_name,"
                + "tr.dep_sid,"
                + "tr.create_usr,"
                + "tr.urgency,"
                + "tr.require_status,"
                + "tr.read_reason,"
                + "sc.check_attachment,"
                + "tr.has_attachment,"
                + "tr.hope_dt,"
                + "tr.customer,"
                + "tr.update_dt, "
                // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
                + this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire()

                + " FROM ");
        this.buildRequireCondition(requireNo, searchQuery, customerItems, builder);
        this.buildRequireIndexDictionaryCondition(requireNo, searchQuery, builder, parameters);
        this.buildCategoryKeyMappingCondition(requireNo, searchQuery, builder);
        this.buildBasicDataSmallCategoryCondition(builder);
        // 檢查項目 (系統別)
        // 後方需對 主單 sid 做 group by
        builder.append(this.searchConditionSqlHelper.prepareCommonJoin(SecurityFacade.getUserSid()));

        builder.append("WHERE tr.require_sid IS NOT NULL ");

        // 查詢條件：是否閱讀
        if (WkStringUtils.isEmpty(requireNo)) {
            builder.append(
                    this.workCommonReadRecordManager.prepareWhereConditionSQL(
                            searchQuery.getSelectReadRecordType(), "readRecord"));
        }

        builder.append(" GROUP BY tr.require_sid ");
        builder.append(" ORDER BY tr.update_dt DESC");

        List<Search27View> resultList = null;

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.REQUIRE_FOR_GM, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.REQUIRE_FOR_GM, SecurityFacade.getUserSid());

        resultList = search27QueryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // ====================================
        // 後續處理
        // ====================================
        if (WkStringUtils.notEmpty(resultList)) {
            // 後續處理-開始
            usageRecord.afterProcessStart();

            // 查詢條件過濾：系統別
            resultList = resultList.stream()
                    .filter(each -> this.searchResultHelper.filterCheckItems(
                            each.getCheckItemTypes(),
                            searchQuery.getCheckItemTypes(),
                            false,
                            null))
                    .collect(Collectors.toList());

            // 準備檢查項目資訊欄位
            this.searchResultHelper.prepareCheckConfirmInfo(
                    (List<BaseSearchView>) (List<?>) resultList,
                    loginUserSid,
                    false);

            // 過濾檢查人員欄位
            if (WkStringUtils.notEmpty(searchQuery.getSearchCheckUserName())) {
                // 取得模糊查詢清單
                final List<Integer> userSids = WkUserUtils.findByNameLike(searchQuery.getSearchCheckUserName(), compId);
                // 過濾檢查者
                resultList = resultList.stream()
                        .filter(vo -> {
                            // 判斷單據檢查人員, 和模糊查詢清單有交集
                            if (WkStringUtils.isEmpty(vo.getCheckConfirmUserSids())) {
                                return false;
                            }
                            // 做交集
                            List<Integer> intersection = Lists.newArrayList(vo.getCheckConfirmUserSids());
                            intersection.retainAll(userSids);

                            // 交集筆數大於 0 時命中
                            return intersection.size() > 0;
                        })
                        .collect(Collectors.toList());
            }

            // REQ-1440 主責單位-已停用異常檢查
            if (ConditionInChargeMode.EXCEPTION_INACTIVE.equals(searchQuery.getConditionInChargeMode())) {
                resultList = resultList.stream()
                        .filter(each -> this.searchResultHelper.filterInactiveByInCharge(
                                each.getInChargeDepSid(),
                                each.getInChargeUsrSid()))
                        .collect(Collectors.toList());
            }

            // 若待閱原因無勾選"無待閱原因", 只留待閱讀
            if (!searchQuery.getSelectReqToBeReadType().contains(ReqToBeReadType.NO_TO_BE_READ.name())) {
                resultList = resultList.stream()
                        .filter(vo -> ReadRecordType.WAIT_READ.equals(vo.getReadRecordType()))
                        .collect(Collectors.toList());
            } // 後續處理-結束
            usageRecord.afterProcessEnd();
        }

        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return resultList;
    }

    /**
     * 組合需求單語法
     *
     * @param requireNo
     * @param searchQuery
     * @param customerItems
     * @param builder
     */
    private void buildRequireCondition(String requireNo, SearchQuery27 searchQuery, List<CustomerTo> customerItems, StringBuilder builder) {
        builder.append("    (SELECT * FROM tr_require tr");
        builder.append("        WHERE 1=1");

        // 日期區間
        builder.append(
                this.searchConditionSqlHelper.prepareWhereConditionForDateRange(
                        requireNo,
                        searchQuery.getDateTimeType().getVal(),
                        searchQuery.getStartDate(),
                        searchQuery.getEndDate()));

        // 需求類別製作進度
        builder.append(
                this.searchConditionSqlHelper.prepareWhereConditionForRequireStatus(
                        requireNo,
                        searchQuery.getSelectRequireStatusType()));

        // 單位查詢
        this.createReqDefQuery(searchQuery, builder);

        // 廳主
        this.createCustomers(requireNo, searchQuery, customerItems, builder);

        // 待閱原因
        builder.append(
                this.searchConditionSqlHelper.prepareWhereConditionForWaitReadReason(
                        requireNo,
                        ReqToBeReadType.valueNames(),
                        searchQuery.getSelectReqToBeReadType(),
                        ReqToBeReadType.NO_TO_BE_READ.name(),
                        "tr.read_reason"));

        // 需求人員
        this.createCereatedUserName(requireNo, searchQuery, builder);

        // 主責單位
        builder.append(
                this.searchConditionSqlHelper.prepareWhereConditionForInCharge(
                        requireNo,
                        searchQuery.getConditionInChargeMode(),
                        searchQuery.getInChargeDepSids(),
                        searchQuery.getInChargeUserSids()));

        //////////////////// 以下為進階搜尋條件//////////////////////////////
        // 異動區間
        if (Strings.isNullOrEmpty(requireNo) && searchQuery.getStartUpdatedDate() != null
                && searchQuery.getEndUpdatedDate() != null) {
            builder.append("        AND tr.update_dt BETWEEN '").append(helper.transStrByStartDate(searchQuery.getStartUpdatedDate()))
                    .append("' AND '").append(helper.transStrByEndDate(searchQuery.getEndUpdatedDate())).append("'");
        }
        // 緊急度
        this.createUrgency(requireNo, searchQuery, builder);
        // 需求單號
        if (!Strings.isNullOrEmpty(requireNo)) {
            builder.append("        AND tr.require_no = '").append(requireNo).append("'");
        }
        // 需求單號
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(searchQuery.getRequireNo())) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(searchQuery.getRequireNo()) + "%";
            builder.append("        AND tr.require_no LIKE '").append(textNo).append("'");
        }

        builder.append("    ) AS tr ");
    }

    /**
     * 單位查詢
     *
     * @param searchQuery
     * @param builder
     */
    private void createReqDefQuery(SearchQuery27 searchQuery, StringBuilder builder) {
        this.createByReqOrQuery(searchQuery, builder);
        this.createByReqCreateQuery(searchQuery, builder);
        this.createByReqAssignQuery(searchQuery, builder);
        this.createByReqForwardQuery(searchQuery, builder);
    }

    /**
     * 單位查詢
     *
     * @param searchQuery
     * @param builder
     */
    private void createByReqOrQuery(SearchQuery27 searchQuery, StringBuilder builder) {
        if ("All".equals(searchQuery.getReqSource())) {
            List<Integer> onlyGmOrgs = searchQuery.getOnlyGmOrgs();
            List<Integer> onlyNotGmOrgs = searchQuery.getOnlyNotGmOrgs();
            if (!onlyGmOrgs.isEmpty() && !onlyNotGmOrgs.isEmpty()) {
                this.createGMSqlByParttGm(onlyGmOrgs, onlyNotGmOrgs, builder);
            } else if (!onlyNotGmOrgs.isEmpty()) {
                this.createGMSqlByAllNotGm(searchQuery.getGmOrgs(), onlyNotGmOrgs, builder);
            } else {
                this.createGMSqlByAllGm(onlyGmOrgs, builder);
            }
        } else if (WkStringUtils.isEmpty(searchQuery.getReqSource())) {
            // 當不選擇條件時, 僅過濾單據公司別
            builder.append(" AND comp_sid = " + searchQuery.getComp().getSid() + " ");
            // this.createByOriginalDep(searchQuery, builder);
        }
    }

    /**
     * 選擇單位有部分GM單位
     *
     * @param onlyGmOrgs
     * @param onlyNotGmOrgs
     * @param builder
     */
    private void createGMSqlByParttGm(List<Integer> onlyGmOrgs, List<Integer> onlyNotGmOrgs, StringBuilder builder) {
        String gmOrgs = onlyGmOrgs.stream()
                .map(each -> String.valueOf(each))
                .collect(Collectors.joining(","));
        String gmUsers = this.getUsersStr(onlyGmOrgs);
        String notGmOrgs = onlyNotGmOrgs.stream()
                .map(each -> String.valueOf(each))
                .collect(Collectors.joining(","));
        String notGmUsers = this.getUsersStr(onlyNotGmOrgs);
        builder.append("        AND (");
        // 1. GM單位 所建立的需求單
        builder.append("            tr.dep_sid IN (").append(gmOrgs).append(")");
        // 2. 分派到 GM單位 的需求單
        builder.append("            OR");
        this.bulidGmSqlByAssign(gmOrgs, builder);
        // 3.轉發到 GM單位或成員 的需求單
        builder.append("            OR");
        this.bulidGmSqlByForward(gmOrgs, gmUsers, builder);
        // 4. 外部需求
        // 不顯示的條件如下：
        // (1)需求單位<>GM所歸屬的單位 && 製作進度＝新建檔
        // OR
        // (2)(製作進度＝作廢 && 作廢碼=Y)
        // 5. 內部需求單
        // (1)內部需求不需要流程的 -> 全部顯示
        // OR
        // (2)內部需求需要流程的 -> (製作進度＝進行中 or 已完成 or 結案)
        builder.append("            OR");
        this.bulidGmSqlByExternalAndInternal(gmOrgs, builder);
        // AND (立單位 OR 分單位 OR 轉單位)
        builder.append("            AND(");
        this.bulidGmSqlByDep(notGmOrgs, notGmUsers, builder);
        builder.append("            )");
        builder.append("        )");
    }

    /**
     * 選擇單位無GM單位
     *
     * @param onlyGmOrgs
     * @param onlyNotGmOrgs
     * @param builder
     */
    private void createGMSqlByAllNotGm(List<Integer> onlyGmOrgs, List<Integer> onlyNotGmOrgs, StringBuilder builder) {
        String gmOrgs = onlyGmOrgs.stream()
                .map(each -> String.valueOf(each))
                .collect(Collectors.joining(","));
        String notGmOrgs = onlyNotGmOrgs.stream()
                .map(each -> String.valueOf(each))
                .collect(Collectors.joining(","));
        String notGmUsers = this.getUsersStr(onlyNotGmOrgs);
        // 4. 外部需求
        // 不顯示的條件如下：
        // (1)需求單位<>GM所歸屬的單位 && 製作進度＝新建檔
        // OR
        // (2)(製作進度＝作廢 && 作廢碼=Y)
        // 5. 內部需求單
        // (1)內部需求不需要流程的 -> 全部顯示
        // OR
        // (2)內部需求需要流程的 -> (製作進度＝進行中 or 已完成 or 結案)
        // AND (立單位 OR 分單位 OR 轉單位)
        builder.append("        AND (");
        this.bulidGmSqlByExternalAndInternal(gmOrgs, builder);
        builder.append("            AND(");
        this.bulidGmSqlByDep(notGmOrgs, notGmUsers, builder);
        builder.append("            )");
        builder.append("        )");
    }

    /**
     * 選擇單位皆是GM單位
     *
     * @param onlyGmOrgs
     * @param builder
     */
    private void createGMSqlByAllGm(List<Integer> onlyGmOrgs, StringBuilder builder) {
        String gmOrgs = onlyGmOrgs.stream()
                .map(each -> String.valueOf(each))
                .collect(Collectors.joining(","));
        String gmUsers = this.getUsersStr(onlyGmOrgs);
        builder.append("        AND (");
        // 1. GM單位 所建立的需求單
        builder.append("            tr.dep_sid IN (").append(gmOrgs).append(")");
        // 2. 分派到 GM單位 的需求單
        builder.append("             OR");
        this.bulidGmSqlByAssign(gmOrgs, builder);
        // 3.轉發到 GM單位或成員 的需求單
        builder.append("            OR");
        this.bulidGmSqlByForward(gmOrgs, gmUsers, builder);
        builder.append("        )");
    }

    /**
     * 分派到 GM單位 的需求單
     *
     * @param gmOrgs
     * @param builder
     */
    private void bulidGmSqlByAssign(String gmOrgs, StringBuilder builder) {
        builder.append("            tr.require_sid IN (");
        builder.append("                SELECT assi.require_sid FROM tr_assign_send_search_info assi");
        builder.append("                    WHERE assi.type = 0");
        builder.append("                    AND assi.dep_sid IN (").append(gmOrgs).append(") ");
        builder.append("            )");
    }

    /**
     * 轉發到 GM單位或成員 的需求單
     *
     * @param gmOrgs
     * @param gmUsers
     * @param builder
     */
    private void bulidGmSqlByForward(String gmOrgs, String gmUsers, StringBuilder builder) {
        builder.append("            tr.require_sid IN(");
        builder.append("                SELECT tai.require_sid FROM tr_alert_inbox tai");
        builder.append("                    WHERE (");
        builder.append("                        (tai.forward_type = 'FORWARD_DEPT' AND tai.receive_dep in (").append(gmOrgs).append("))");
        builder.append("                        OR");
        builder.append("                        (tai.forward_type = 'FORWARD_MEMBER' AND tai.receive in (").append(gmUsers).append("))");
        builder.append("                    )");
        builder.append("                GROUP BY tai.require_sid ");
        builder.append("            )");
    }

    /**
     * GM條件 - 外部需求
     * 不顯示的條件如下： (1)需求單位<>GM所歸屬的單位 && 製作進度＝新建檔 OR (2)(製作進度＝作廢 && 作廢碼=Y)
     * GM條件 - 外部需求
     * (1)內部需求不需要流程的 -> 全部顯示 OR (2)內部需求需要流程的 -> (製作進度＝進行中 or 已完成 or 結案)
     *
     * @param gmOrgs
     * @param builder
     */
    private void bulidGmSqlByExternalAndInternal(String gmOrgs, StringBuilder builder) {
        builder.append("            tr.require_sid IN (");
        builder.append("                SELECT tr_temp.require_sid FROM tr_require tr_temp");
        builder.append("                    LEFT JOIN (");
        builder.append("                        SELECT ckm_temp.key_sid,ckm_temp.big_category_sid FROM tr_category_key_mapping ckm_temp ");
        builder.append("                    ) AS ckm_temp ON tr_temp.mapping_sid=ckm_temp.key_sid");
        builder.append("                    LEFT JOIN (");
        builder.append("                        SELECT bc_temp.basic_data_big_category_sid,bc_temp.req_category FROM tr_basic_data_big_category bc_temp");
        builder.append("                    )AS bc_temp ON bc_temp.basic_data_big_category_sid=ckm_temp.big_category_sid");
        builder.append("                    WHERE (");
        builder.append("                        (");
        builder.append("                            bc_temp.req_category = 'EXTERNAL'");
        builder.append("                            AND (tr_temp.require_status <> 'INVALID' OR tr_temp.back_code=1)");
        builder.append("                            AND (");
        builder.append("                                tr_temp.dep_sid IN (").append(gmOrgs).append(")");
        builder.append("                                OR");
        builder.append("                                tr_temp.require_status <> 'NEW_INSTANCE' ");
        builder.append("                            )");
        builder.append("                        )");
        builder.append("                        OR");
        builder.append("                        (");
        builder.append("                            bc_temp.req_category = 'INTERNAL'");
        builder.append("                            AND (");
        builder.append("                                tr_temp.has_require_manager_sign = 0");
        builder.append("                                OR");
        builder.append("                                (tr_temp.has_require_manager_sign = 1 AND tr_temp.require_status in('PROCESS','COMPLETED','CLOSE') )");
        builder.append("                            )");
        builder.append("                        )");
        builder.append("                    )");
        builder.append("            )");
    }

    /**
     * GM條件 - 立單位 OR 分單位 OR 轉單位
     *
     * @param notGmOrgs
     * @param notGmUsers
     * @param builder
     */
    private void bulidGmSqlByDep(String notGmOrgs, String notGmUsers, StringBuilder builder) {
        // 立單位
        builder.append("                tr.dep_sid IN (").append(notGmOrgs).append(")");
        // 分單位
        builder.append("                OR");
        builder.append("                tr.require_sid IN(");
        builder.append("                    SELECT assi_temp.require_sid FROM tr_assign_send_search_info assi_temp");
        builder.append("                        WHERE assi_temp.type = 0");
        builder.append("                        AND assi_temp.dep_sid IN (").append(notGmOrgs).append(")");
        builder.append("                )");
        // 轉單位
        builder.append("                OR");
        builder.append("                tr.require_sid IN(");
        builder.append("                    SELECT tai_temp.require_sid FROM tr_alert_inbox tai_temp");
        builder.append("                        WHERE (");
        builder.append("                            (tai_temp.forward_type = 'FORWARD_MEMBER' AND tai_temp.receive in (").append(notGmUsers).append("))");
        builder.append("                            OR");
        builder.append("                            (tai_temp.forward_type = 'FORWARD_DEPT' AND tai_temp.receive_dep in (").append(notGmOrgs).append("))");
        builder.append("                        )");
        builder.append("                        GROUP BY tai_temp.require_sid");
        builder.append("                )");
    }

    /**
     * 立案單位 SQL 組法
     *
     * @param searchQuery
     * @param builder
     */
    private void createByReqCreateQuery(SearchQuery27 searchQuery, StringBuilder builder) {
        if ("立".equals(searchQuery.getReqSource())) {
            String requireDepts = searchQuery.getRequireDepts() == null || searchQuery.getRequireDepts().isEmpty()
                    ? "nerverFind"
                    : searchQuery.getRequireDepts().stream()
                            .collect(Collectors.joining(","));
            // 立單單位需求單位及其特殊權限可閱部門
            builder.append("        AND tr.dep_sid IN (").append(requireDepts).append(")");
        }
    }

    /**
     * 分派單位 SQL 組法
     *
     * @param searchQuery
     * @param builder
     */
    private void createByReqAssignQuery(SearchQuery27 searchQuery, StringBuilder builder) {
        if ("分".equals(searchQuery.getReqSource())) {
            // 所勾選的立案單位 分派到自己單位的需求單
            String requireDepts = searchQuery.getRequireDepts() == null || searchQuery.getRequireDepts().isEmpty()
                    ? "nerverFind"
                    : searchQuery.getRequireDepts().stream()
                            .collect(Collectors.joining(","));
            builder.append("        AND tr.dep_sid IN (").append(requireDepts).append(")");
            // 被分派單位的同仁們要能夠看的到（要能看到自己含底下單位所有的被分派的單據）
            String assignDep = this.createOrgsInParam(orgSerivce.findChildOrgs(searchQuery.getDep())).stream()
                    .map(each -> String.valueOf(each))
                    .collect(Collectors.joining(","));
            builder.append("        AND tr.require_sid IN(");
            builder.append("            SELECT assi.require_sid FROM tr_assign_send_search_info assi");
            builder.append("                WHERE assi.type = 0");
            builder.append("                AND assi.dep_sid IN (").append(assignDep).append(")");
            builder.append("        )");
        }
    }

    /**
     * 轉發單位 SQL 組法
     *
     * @param searchQuery
     * @param builder
     */
    private void createByReqForwardQuery(SearchQuery27 searchQuery, StringBuilder builder) {
        if ("轉".equals(searchQuery.getReqSource())) {
            // 被轉發部門與被轉發個人 要能夠看的到
            String requireDepts = searchQuery.getRequireDepts() == null || searchQuery.getRequireDepts().isEmpty()
                    ? "nerverFind"
                    : searchQuery.getRequireDepts().stream()
                            .collect(Collectors.joining(","));
            builder.append("        AND tr.dep_sid IN (").append(requireDepts).append(")");
            // 被轉發部門與被轉發個人 要能夠看的到
            builder.append("        AND tr.require_sid IN(");
            builder.append("            SELECT tai.require_sid FROM tr_alert_inbox tai");
            builder.append("                WHERE (");
            builder.append("                    (tai.forward_type = 'FORWARD_MEMBER' AND tai.receive = ").append(searchQuery.getUser().getSid()).append(")");
            builder.append("                    OR");
            builder.append("                    (tai.forward_type = 'FORWARD_DEPT' AND tai.receive_dep = ").append(searchQuery.getDep().getSid()).append(") ");
            builder.append("                )");
            builder.append("           GROUP BY tai.require_sid ");
            builder.append("        )");
        }
    }

    /**
     * 組合 廳主 SQL
     *
     * @param requireNo
     * @param searchQuery
     * @param customerItems
     * @param builder
     */
    private void createCustomers(String requireNo, SearchQuery27 searchQuery, List<CustomerTo> customerItems, StringBuilder builder) {
        if (Strings.isNullOrEmpty(requireNo) && searchQuery.getCustomers() != null
                && !searchQuery.getCustomers().isEmpty()
                && searchQuery.getCustomers().size() != customerItems.size()) {
            String customers = searchQuery.getCustomers().stream()
                    .map(each -> String.valueOf(each.getSid()))
                    .collect(Collectors.joining(","));
            if (Strings.isNullOrEmpty(customers)) {
                customers = "''";
            }
            builder.append("        AND tr.customer IN (").append(customers).append(")");
        }
    }

    /**
     * 組合 需求人員 SQL
     *
     * @param requireNo
     * @param searchQuery
     * @param builder
     */
    private void createCereatedUserName(String requireNo, SearchQuery27 searchQuery, StringBuilder builder) {
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(searchQuery.getTrCreatedUserName())) {

            List<Integer> userSids = WkUserUtils.findByNameLike(searchQuery.getTrCreatedUserName(), SecurityFacade.getCompanyId());
            if (WkStringUtils.isEmpty(userSids)) {
                userSids.add(-999);
            }

            String userSidStr = userSids.stream()
                    .map(each -> String.valueOf(each))
                    .collect(Collectors.joining(","));

            builder.append("        AND tr.create_usr IN (").append(userSidStr).append(")");
        }
    }

    /**
     * 組合 緊急度 SQL
     *
     * @param requireNo
     * @param searchQuery
     * @param builder
     */
    private void createUrgency(String requireNo, SearchQuery27 searchQuery, StringBuilder builder) {
        if (Strings.isNullOrEmpty(requireNo)) {
            List<Integer> urgencyList = this.getUrgencyTypeList(searchQuery);
            if (urgencyList != null && !urgencyList.isEmpty()) {
                builder.append("        AND tr.urgency IN (").append(urgencyList.stream().map(each -> String.valueOf(each)).collect(Collectors.joining(",")))
                        .append(")");
            }
        }
    }

    /**
     * @param requireNo
     * @param searchQuery
     * @param builder
     */
    private void buildRequireIndexDictionaryCondition(String requireNo, SearchQuery27 searchQuery, StringBuilder builder, Map<String, Object> parameters) {
        // 模糊搜尋
        builder.append("    INNER JOIN (");
        builder.append("        SELECT tid.require_sid,tid.field_content FROM tr_index_dictionary tid");
        builder.append("            WHERE tid.field_name='主題'");
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(searchQuery.getFuzzyText())) {
            String text = "%" + reqularUtils.replaceIllegalSqlLikeStr(searchQuery.getFuzzyText()) + "%";
            builder.append("            AND (");
            builder.append("                tid.require_sid IN(");
            builder.append("                    SELECT DISTINCT tid1.require_sid FROM tr_index_dictionary tid1");
            builder.append("                        WHERE tid1.field_content LIKE :text ");
            builder.append("                )");
            builder.append("                OR");
            builder.append("                tid.require_sid IN (");
            builder.append("                    SELECT DISTINCT trace.require_sid FROM tr_require_trace trace");
            builder.append("                        WHERE trace.require_trace_type = 'REQUIRE_INFO_MEMO'");
            builder.append("                        AND trace.require_trace_content LIKE :text ");
            builder.append("                )");
            builder.append("            )");

            parameters.put("text", text);
        }
        builder.append("    ) AS tid ON tr.require_sid=tid.require_sid ");
        // only 主題
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(searchQuery.getTheme())) {
            String themeText = "'%" + reqularUtils.replaceIllegalSqlLikeStr(searchQuery.getTheme()) + "%'";
            builder.append("    INNER JOIN (");
            builder.append("        SELECT tidByTheme.require_sid,tidByTheme.field_content FROM tr_index_dictionary tidByTheme ");
            builder.append("            WHERE tidByTheme.field_name='主題'");
            builder.append("            AND tidByTheme.field_content LIKE ").append(themeText);
            builder.append("    ) AS tidByTheme ON tr.require_sid=tidByTheme.require_sid ");
        }
        // only 內容
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(searchQuery.getContent())) {
            String contentText = "'%" + reqularUtils.replaceIllegalSqlLikeStr(searchQuery.getContent()) + "%'";
            builder.append("    INNER JOIN (");
            builder.append("        SELECT tidByContent.require_sid,tidByContent.field_content FROM tr_index_dictionary tidByContent ");
            builder.append("            WHERE tidByContent.field_name='內容'");
            builder.append("            AND tidByContent.field_content LIKE ").append(contentText);
            builder.append("     ) AS tidByContent ON tr.require_sid=tidByContent.require_sid ");
        }
    }

    /**
     * @param requireNo
     * @param searchQuery
     * @param builder
     */
    private void buildCategoryKeyMappingCondition(String requireNo, SearchQuery27 searchQuery, StringBuilder builder) {
        builder.append("     INNER JOIN (");
        builder.append("         SELECT * FROM tr_category_key_mapping ckm ");
        builder.append("             WHERE 1=1");
        // 類別組合
        if (Strings.isNullOrEmpty(requireNo) && searchQuery.isSelCateSids()) {
            builder.append("             AND (");
            if (Strings.isNullOrEmpty(requireNo) && CollectionUtils.isNotEmpty(searchQuery.getBigDataCateSids())) {
                builder.append("ckm.big_category_sid IN (");
                builder.append(searchQuery.getBigDataCateSids().stream().collect(Collectors.joining("','", "'", "'"))).append(")");
                if (CollectionUtils.isNotEmpty(searchQuery.getMiddleDataCateSids()) || CollectionUtils.isNotEmpty(searchQuery.getSmallDataCateSids())) {
                    builder.append("or ");
                }
            }
            if (Strings.isNullOrEmpty(requireNo) && CollectionUtils.isNotEmpty(searchQuery.getMiddleDataCateSids())) {
                builder.append("ckm.middle_category_sid IN (");
                builder.append(searchQuery.getMiddleDataCateSids().stream().collect(Collectors.joining("','", "'", "'"))).append(")");
                if (CollectionUtils.isNotEmpty(searchQuery.getSmallDataCateSids())) {
                    builder.append("or ");
                }
            }
            if (Strings.isNullOrEmpty(requireNo) && CollectionUtils.isNotEmpty(searchQuery.getSmallDataCateSids())) {
                builder.append("ckm.small_category_sid IN (");
                builder.append(searchQuery.getSmallDataCateSids().stream().collect(Collectors.joining("','", "'", "'"))).append(")");
            }
            builder.append("             )");
        }
        // 需求類別
        if (Strings.isNullOrEmpty(requireNo) && searchQuery.getSelectBigCategory() != null) {
            builder.append("             AND ckm.big_category_sid = '")
                    .append(searchQuery.getSelectBigCategory().getSid()).append("'");
        }
        builder.append("     ) AS ckm ON tr.mapping_sid=ckm.key_sid ");
    }

    /**
     * @param builder
     * @param parameters
     */
    private void buildBasicDataSmallCategoryCondition(StringBuilder builder) {
        builder.append("     LEFT JOIN (");
        builder.append("         SELECT sc.basic_data_small_category_sid,sc.check_attachment FROM tr_basic_data_small_category sc");
        builder.append("     ) AS sc ON sc.basic_data_small_category_sid = ckm.small_category_sid ");
    }

    /**
     * 得到部門使用者
     *
     * @param orgs
     * @return
     */
    private String getUsersStr(List<Integer> orgs) {
        return userService.findUserByPrimaryIn(orgs, Activation.ACTIVE).stream()
                .map(each -> each.getSid())
                .map(each -> String.valueOf(each))
                .collect(Collectors.joining(","));
    }

    /**
     * 建立是否有分派快取
     *
     * @param searchQuery
     * @param reqSids
     * @return
     */
    public List<String> createAssignCache(SearchQuery27 searchQuery, List<String> reqSids) {
        if (CollectionUtils.isEmpty(reqSids)) {
            return Lists.newArrayList();
        }
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT DISTINCT assi.require_sid FROM tr_assign_send_search_info assi ");
        builder.append("  WHERE assi.type = 0 ");

        Map<String, Object> param = Maps.newHashMap();
        if (WkStringUtils.isEmpty(searchQuery.getReqSource())) {
            builder.append("    AND assi.dep_sid IN (:hasAssignDep) ");
            param.put("hasAssignDep", searchQuery.getRequireDepts() == null || searchQuery.getRequireDepts().isEmpty()
                    ? "nerverFind"
                    : searchQuery.getRequireDepts());
        }

        // 避免參數過多造成 java.nio.BufferOverflowException
        // 直接用字串串接
        builder.append("    AND assi.require_sid IN ('" + String.join("','", reqSids) + "') ");

        return search27QueryService.findWithQueryForCache(builder.toString(), param, searchQuery.getUser());
    }

    /**
     * 建立是否有分派給自己快取
     *
     * @param searchQuery
     * @param reqSids
     * @return
     */
    public List<String> createForwardSelfCache(SearchQuery27 searchQuery, List<String> reqSids) {
        if (CollectionUtils.isEmpty(reqSids)) {
            return Lists.newArrayList();
        }
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT DISTINCT tai.require_sid FROM tr_alert_inbox  tai ");
        builder.append("  WHERE ((tai.forward_type = 'FORWARD_MEMBER' AND tai.receive     = :hasForwardInboxReceive) OR ");
        builder.append("        ( tai.forward_type = 'FORWARD_DEPT'   AND tai.receive_dep = :hasForwardInboxReceiveDep)) ");

        // 避免參數過多造成 java.nio.BufferOverflowException
        // 直接用字串串接
        builder.append("    AND tai.require_sid IN ('" + String.join("','", reqSids) + "') ");

        Map<String, Object> param = Maps.newHashMap();
        param.put("hasForwardInboxReceive", searchQuery.getUser().getSid());
        param.put("hasForwardInboxReceiveDep", searchQuery.getDep().getSid());

        return search27QueryService.findWithQueryForCache(builder.toString(), param, searchQuery.getUser());
    }

    /**
     * 建立是否有追蹤資料快取
     *
     * @param searchQuery
     * @param reqSids
     * @return
     */
    public List<String> createTraceCache(SearchQuery27 searchQuery, List<String> reqSids) {
        if (CollectionUtils.isEmpty(reqSids)) {
            return Lists.newArrayList();
        }
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT DISTINCT trace.trace_source_sid FROM work_trace_info trace ");
        builder.append(" WHERE trace.trace_source_type = 'TECH_REQUEST'");
        builder.append("   AND trace.trace_source_sid IN ('" + String.join("','", reqSids) + "')");
        builder.append("   AND (trace.trace_dep = ").append(searchQuery.getDep().getSid());
        builder.append("         OR trace.trace_usr = ").append(searchQuery.getUser().getSid());
        builder.append("       ) ");
        builder.append("   AND trace.trace_status = 'UN_FINISHED' ");
        builder.append("   AND trace.Status = '0' ");

        // 改為 直接組進字串, 以免參數過多時發生錯誤
        // Map<String, Object> param = Maps.newHashMap();
        // param.put("reqSids", reqSids);
        return search27QueryService.findWithQueryForCache(builder.toString(), Maps.newHashMap(), searchQuery.getUser());
    }

    /**
     * 轉換List<Org>為報表查詢用參數
     *
     * @param orgs
     * @return
     */
    private List<Integer> createOrgsInParam(List<Org> orgs) {
        return orgs.stream().map(Org::getSid).collect(Collectors.toList());
    }

    /**
     * 取得此報表全部製作進度查詢
     *
     * @return
     */
    private List<RequireStatusType> getAllReqStatus() {
        return Lists.newArrayList(RequireStatusType.NEW_INSTANCE,
                RequireStatusType.WAIT_CHECK,
                RequireStatusType.INVALID,
                RequireStatusType.ROLL_BACK_NOTIFY,
                RequireStatusType.PROCESS,
                RequireStatusType.SUSPENDED,
                RequireStatusType.CLOSE,
                RequireStatusType.COMPLETED,
                RequireStatusType.AUTO_CLOSED);
    }

    /**
     * @param searchQuery
     * @return
     */
    private List<Integer> getUrgencyTypeList(SearchQuery27 searchQuery) {
        if (searchQuery.getUrgencyList() == null) {
            return Lists.newArrayList();
        }
        return searchQuery.getUrgencyList().stream()
                .map(UrgencyType::valueOf)
                .map(UrgencyType::ordinal)
                .collect(Collectors.toList());
    }

    /**
     * 畫面用全部製作進度查詢
     *
     * @return
     */
    public SelectItem[] getReqStatusItems() { return reqStatusUtils.buildItem(this.getAllReqStatus()); }

    /**
     * 建立是否有分派快取
     *
     * @param orgSid
     * @param reqSids
     * @return
     */
    public List<String> createCreateCache(List<String> orgSids, List<String> reqSids) {
        if (CollectionUtils.isEmpty(reqSids)) {
            return Lists.newArrayList();
        }
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT DISTINCT tr.require_sid FROM tr_require tr ");
        builder.append("    WHERE tr.require_sid IN (")
                .append(reqSids.stream().collect(Collectors.joining("','", "'", "'"))).append(") ");
        builder.append("    AND tr.dep_sid IN (")
                .append(orgSids.stream().collect(Collectors.joining(","))).append(") ");
        return search27QueryService.findWithQueryForCache(builder.toString());
    }

    /**
     * 建立是否有分派快取
     *
     * @param orgSids
     * @param reqSids
     * @return
     */
    public List<String> createAssignCache(List<String> orgSids, List<String> reqSids) {
        if (CollectionUtils.isEmpty(reqSids)) {
            return Lists.newArrayList();
        }
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT DISTINCT assi.require_sid FROM tr_assign_send_search_info assi ");
        builder.append("  WHERE assi.type = 0 ");
        builder.append("    AND assi.dep_sid IN (")
                .append(orgSids.stream().collect(Collectors.joining(","))).append(") ");
        builder.append("    AND assi.require_sid IN (")
                .append(reqSids.stream().collect(Collectors.joining("','", "'", "'"))).append(") ");
        return search27QueryService.findWithQueryForCache(builder.toString());
    }

    /**
     * 建立是否有分派給自己快取
     *
     * @param searchQuery
     * @param reqSids
     * @return
     */
    public List<String> createForwardSelfCache(List<String> orgSids, List<String> reqSids) {
        if (CollectionUtils.isEmpty(reqSids)) {
            return Lists.newArrayList();
        }
        String userSids = this.getUsersStr(orgSids.stream()
                .map(each -> Integer.valueOf(each))
                .collect(Collectors.toList()));
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT DISTINCT tai.require_sid FROM tr_alert_inbox  tai ");
        builder.append("    WHERE tai.require_sid IN (")
                .append(reqSids.stream().collect(Collectors.joining("','", "'", "'"))).append(") ");
        builder.append("    AND(");
        builder.append("        (tai.forward_type = 'FORWARD_MEMBER' AND tai.receive IN (").append(userSids).append(")) ");
        builder.append("        OR");
        builder.append("        (tai.forward_type = 'FORWARD_DEPT' AND tai.receive_dep IN (")
                .append(orgSids.stream().collect(Collectors.joining(","))).append(")) ");
        builder.append("    ) ");
        return search27QueryService.findWithQueryForCache(builder.toString());
    }
}
