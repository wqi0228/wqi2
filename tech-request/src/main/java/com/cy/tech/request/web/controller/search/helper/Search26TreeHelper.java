/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search.helper;

import com.cy.tech.request.logic.search.enums.ReqSubType;
import com.cy.tech.request.logic.search.view.Search26BaseView;
import com.cy.tech.request.logic.search.view.Search26TreeView;
import com.cy.tech.request.logic.search.view.Search26View;
import com.cy.tech.request.logic.search.view.Search26ViewIndex;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.stereotype.Component;

/**
 *
 * @author shaun
 */
@Component
public class Search26TreeHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1282067065344241919L;

    /**
     * 建立需求製作進度樹
     *
     * @param items
     * @return
     */
    public TreeNode createRoot(List<Search26BaseView> items) {
        TreeNode root = new DefaultTreeNode(new Search26TreeView(), null);
        Map<String, List<Search26BaseView>> groupMap = this.filterReqGroupMap(items);
        Search26ViewIndex index = new Search26ViewIndex();
        groupMap.values().forEach(each -> this.createRootNode(root, each, index));
        return root;
    }

    /**
     * 分類各次需求單進度內容<BR/>
     * {req00 <BR/>
     * &nbsp;&nbsp;&nbsp;sub1 <BR/>
     * &nbsp;&nbsp;&nbsp;sub2 <BR/>
     * &nbsp;&nbsp;&nbsp;sub3 <BR/>
     * &nbsp;&nbsp;&nbsp;sub4 <BR/>
     * }, <BR/>
     * {req01 <BR/>
     * &nbsp;&nbsp;&nbsp;sub1 <BR/>
     * &nbsp;&nbsp;&nbsp;sub2 <BR/>
     * &nbsp;&nbsp;&nbsp;sub3 <BR/>
     * &nbsp;&nbsp;&nbsp;sub4 <BR/>
     * },.. <BR/>
     * 維持原本排序需使用LinkedHashMap
     *
     * @param rowInfo
     * @return
     */
    private Map<String, List<Search26BaseView>> filterReqGroupMap(List<Search26BaseView> items) {
        return items.stream().collect(Collectors.groupingBy(each -> each.getReqNo(), LinkedHashMap::new, toList()));
    }

    /**
     * 建立子節點
     *
     * @param rowInfo
     * @return
     */
    private void createRootNode(TreeNode root, List<Search26BaseView> itmes, Search26ViewIndex index) {
        Search26BaseView reqRootView = this.findReqRoot(itmes);
        Map<ReqSubType, List<Search26BaseView>> subGroup = this.filterSubMap(itmes);
        Search26TreeView reqRootTree = new Search26TreeView((Search26View) reqRootView, index.getIndex());
        reqRootTree.resetReqMemo(itmes);// 需求單節點備註處理
        TreeNode reqRootNode = new DefaultTreeNode(reqRootTree, root);
        reqRootNode.setExpanded(false);
        subGroup.keySet().forEach(each -> this.createSubNode(reqRootNode, each, subGroup.get(each), index));
    }

    private Search26BaseView findReqRoot(List<Search26BaseView> itmes) {

        Optional<Search26BaseView> optional = itmes.stream()
                .filter(each -> each.getSubType().equals(ReqSubType.ROOT))
                .findAny();

        if (optional.isPresent()) {
            return optional.get();
        }
        return null;

    }

    private Map<ReqSubType, List<Search26BaseView>> filterSubMap(List<Search26BaseView> items) {
        return items.stream()
                .filter(each -> !each.getSubType().equals(ReqSubType.ROOT))
                .collect(Collectors.groupingBy(each -> each.getSubType(), LinkedHashMap::new, toList()));
    }

    private void createSubNode(TreeNode reqRootNode, ReqSubType subType, List<Search26BaseView> itmes, Search26ViewIndex index) {
        TreeNode subRoot = new DefaultTreeNode(
                new Search26TreeView((Search26TreeView) reqRootNode.getData(), subType),
                reqRootNode);
        subRoot.setExpanded(true);
        itmes.forEach(each -> new DefaultTreeNode(new Search26TreeView((Search26View) each, index.getIndex()), subRoot));
    }

    public Map<Integer, TreeNode> createTreeCache(TreeNode root) {
        Map<Integer, TreeNode> cache = Maps.newHashMap();
        this.createCacheByNode(root, cache);
        return cache;
    }

    private void createCacheByNode(TreeNode node, Map<Integer, TreeNode> cache) {
        if (node.getChildren() == null || node.getChildren().isEmpty()) {
            return;
        }
        node.getChildren().forEach(each -> {
            Search26TreeView view = (Search26TreeView) each.getData();
            if (!cache.containsKey(view.getIndex())) {
                // 子程序群組不建立
                if (!view.getIsSubGroup()) {
                    cache.put(view.getIndex(), each);
                }
                this.createCacheByNode(each, cache);
            }
        });
    }

    /**
     * 重置樹節點<BR/>
     * 1.清除本身以外的select<BR/>
     * 2.先全部展開(如果不先全展，往上走會有問題)<BR/>
     * 3.關閉非本身需求單root<BR/>
     * 4.select自己<BR/>
     * 5.展開自己
     *
     * @param selectNode
     * @param cache
     */
    public void resetExpanded(TreeNode selectNode, Map<Integer, TreeNode> cache) {
        cache.values().stream()
                .map(each -> {
                    each.setSelected(false);
                    each.setExpanded(true);
                    return each;
                })
                .filter(each -> ((Search26TreeView) each.getData()).getIsRoot())
                .filter(each -> !((Search26TreeView) each.getData()).getReqSid().equals(((Search26TreeView) selectNode.getData()).getReqSid()))
                .filter(each -> !each.equals(selectNode))
                .forEach(each -> each.setExpanded(false));
        selectNode.setSelected(true);
        selectNode.setExpanded(true);
    }

    public void expandedAll(Map<Integer, TreeNode> cache) {
        cache.values().forEach(each -> each.setExpanded(true));
    }

    public void foldAll(Map<Integer, TreeNode> cache) {
        cache.values().stream()
                .filter(each -> ((Search26TreeView) each.getData()).getIsRoot())
                .forEach(each -> each.setExpanded(false));
    }

}
