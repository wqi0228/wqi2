package com.cy.tech.request.web.newsearch;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Convert;

import com.cy.tech.request.vo.converter.ReqToBeReadTypeConverter;
import com.cy.tech.request.vo.converter.RequireFinishCodeTypeConverter;
import com.cy.tech.request.vo.converter.RequireStatusTypeConverter;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.enums.RequireFinishCodeType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.vo.converter.UrgencyTypeConverter;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author allen1214_wu
 */
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = "requireSid")
public class NewSearchResultVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4952136821365693110L;
    /** 需求單主檔SID **/
    private String requireSid;
    /** 需求單號 **/
    private String requireNo;
    /** 主題 **/
    private String requireTheme;
    /** 主題 **/
    private String requireTheme_src;
    /** 緊急度 **/
    @Convert(converter = UrgencyTypeConverter.class)
    private UrgencyType urgency;
    /** 立案時間 **/
    private Date createdDate;
    /** 異動時間 **/
    private Date updateDate;
    /** 大類名稱 **/
    private String bigName;
    /** 中類名稱 **/
    private String middleName;
    /** 小類名稱 **/
    private String smallName;
    /** 需求單位 sid */
    private Integer createDepSid;
    /** 需求單位 name */
    private String createDepName;
    /** 需求人員 sid */
    private Integer createdUserSid;
    /** 需求人員 name */
    private String createdUserName;
    /** 技術事業群主管是否審核 */
    private Boolean hasTechManagerSign;
    /** 需求暫緩碼 */
    private Boolean requireSuspendedCode;
    /** 需求製作進度 */
    @Convert(converter = RequireStatusTypeConverter.class)
    private RequireStatusType requireStatus;
    /** 待閱原因 */
    @Convert(converter = ReqToBeReadTypeConverter.class)
    private ReqToBeReadType readReason;
    /** 完成碼 */
    @Convert(converter = RequireFinishCodeTypeConverter.class)
    private RequireFinishCodeType finishCode;
    /** 期望完成日 */
    private Date hopeDate;
    /** 分派日期 */
    private Date assignDate;
    /** 分派單位 */
    private List<Integer> assignDeps;
    /** 分派資訊(TEXT) */
    private String assignInfo_src;
    /** 通知日期 */
    private Date sendDate;
    /** 通知單位 */
    private List<Integer> sendDeps;
    /** 通知資訊(TEXT) */
    private String sendInfo_src;
    /** 本地端連結網址 */
    private String localUrlLink;
   

    /**
     * 檢查項目 (系統別)
     */
    private String checkItemNames;
    private String checkItemsDescr;
    private List<RequireCheckItemType> checkItemTypes;

    /**
     * 主責單位
     */
    private Integer inChargeDepSid;
    /**
     * 主責單位
     */
    private String inChargeDepDescr;
    /**
     * 負責人
     */
    private Integer inChargeUsrSid;
    /**
     * 負責人
     */
    private String inChargeUsrDescr;



    // ====================================
    //
    // ====================================
    /** 需求完成確認單位 + 負責人 */
    private String show_reqConfirmDepAndOwner;
    
    // ====================================
    // 閱讀記錄
    // ====================================
    /** 是否待閱讀 */
    private String waitRead;
    /** 讀取時間 */
    private Date readDate;
    /**
     * 閱讀狀態
     */
    private ReadRecordType readRecordType = ReadRecordType.UN_READ;
    /**
     * 閱讀狀態說明
     * 
     * @return
     */
    public String getReadRecordTypeDescr() {
        if (this.readRecordType == null) {
            return "";
        }
        return this.readRecordType.getValue();
    }
    

    // ====================================
    // for newsearch02
    // ====================================
    /**
     * 為真實被分派單位
     */
    private boolean trueAssignDep;

}
