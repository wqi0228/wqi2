/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.enums;

import com.cy.work.common.enums.UrgencyType;
import java.io.Serializable;
import javax.faces.model.SelectItem;
import org.springframework.stereotype.Controller;

/**
 *
 * @author shaun
 */
@Controller
public class UrgencyTypeMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3734962031079578001L;

    public SelectItem[] getTypeValues() {
        SelectItem[] items = new SelectItem[UrgencyType.values().length];
        int i = 0;
        for (UrgencyType each : UrgencyType.values()) {
            items[i++] = new SelectItem(each, each.getValue());
        }
        return items;
    }

}
