/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.anew.config;

import java.io.Serializable;
import lombok.Getter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

/**
 * 變數 管理
 *
 * @author kasim
 */
@Component
public class ReqWebConstants implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3590867489643797064L;

    @Getter
    /** url 參數 (轉需求單) */
    private final String fromIssueCreateKey = "fromIssueCreate";

    @Getter
    /** url 參數 (轉Onpg) */
    private final String fromIssueCreateOnpgKey = "fromIssueCreateOnpg";

    @Getter
    /** ajax function name */
    private final String functionNameByIssueCreate = "issueCreateMode();";

    @Getter
    /** ajax function name */
    private final String functionNameByTransOnpg = "transOnpg();";

    @Getter
    /** 特定網頁url */
    private final String require01XhtmlKey = "require01.xhtml";

    @Getter
    /** 特定網頁url */
    private final String require02XhtmlKey = "require02.xhtml";

    @Getter
    /** url 參數 */
    private final String copyKey = "copy";

    private static ReqWebConstants instance;

    public static ReqWebConstants getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        ReqWebConstants.instance = this;
    }
}
