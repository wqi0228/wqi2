/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search29QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.view.Search29View;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.URLService.URLServiceAttr;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.require.RequireForwardDeAndPersonMBean;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.searchheader.CommonHeaderMBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.group.vo.WorkLinkGroup;
import com.cy.work.logic.WkUserAndOrgLogic;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 未結案單據
 *
 * @author jason_h
 */
@Controller
@Scope("view")
@Slf4j
public class Search29MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8915275707791451669L;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private URLService urlService;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private Require01MBean r01MBean;
    @Autowired
    transient private Search29QueryService search29QueryService;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private WkUserAndOrgLogic wkUserAndOrgLogic;

    /** 所有的需求單 */
    @Getter
    @Setter
    private List<Search29View> queryItems;

    /** 選擇的需求單 */
    @Getter
    @Setter
    private Search29View querySelection;

    /** 上下筆移動keeper */
    @Getter
    private Search29View queryKeeper;

    /** 切換模式 */
    @Getter
    private SwitchType switchType = SwitchType.CONTENT;

    /** 切換模式 - 全畫面狀態 */
    @Getter
    private SwitchType switchFullType = SwitchType.DETAIL;

    /** 在匯出的時候，某些內容需要隱藏 */
    @Getter
    private boolean hasDisplay = true;

    @Getter
    private final String dataTableId = "dtRequire";
    // @Getter
    // private SearchQuery10 searchQuery10;

    @PostConstruct
    public void init() {
        this.initComponent();
        display.execute("doSearchData();");
        // this.search();
    }

    private void initComponent() {
        // this.searchQuery10 = new SearchQuery10(helper,
        // search10LogicHelper,
        // reportCustomFilterLogicComponent,
        // categorySettingService,
        // messageCallBack,
        // display,
        // Lists.newArrayList(loginBean.getRoles()),
        // loginBean.getDep(),
        // loginBean.getUser(),
        // reqStatusMBean
        // );
        // this.searchQuery10.initSetting();
        commHeaderMBean.clearAdvance();
        // this.searchQuery10.loadSetting();
    }

    public void search() {
        queryItems = this.findWithQuery();
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        // searchQuery10.initSetting();
        commHeaderMBean.clearAdvance();
        // this.searchQuery10.loadSetting();
        this.search();
    }

    private List<Search29View> findWithQuery() {

        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();

        builder.append("SELECT "
                + "tr.require_sid,"
                + "tr.require_no,"
                + "tid.field_content,"
                + "tr.urgency,"
                + "tr.has_forward_dep,"
                + "tr.has_forward_member,"
                + "tr.has_link,"
                + "tr.create_dt,"
                + "ckm.big_category_name,"
                + "ckm.middle_category_name,"
                + "ckm.small_category_name,"
                + "tr.dep_sid,"
                + "tr.create_usr,"
                + "tr.require_status,"
                + "sc.check_attachment,"
                + "tr.has_attachment, "
                + "tr.hope_dt, "
                + "tr.inte_staff, "
                + "tr.require_finish_dt, "
                + "tr.require_finish_code, "
                + "tr.require_establish_dt, "
                // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
                + this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire()

                + "FROM tr_require tr  ");
        buildRequireIndexDictionaryCondition(builder, parameters);
        buildCategoryKeyMappingCondition(builder, parameters);
        buildBasicDataSmallCategoryCondition(builder, parameters);
        // 檢查項目 (系統別)
        // 後方需對 主單 sid 做 group by
        builder.append(this.searchConditionSqlHelper.prepareCommonJoin(SecurityFacade.getUserSid()));
        buildRequireCondition(builder, parameters);

        builder.append(" GROUP BY tr.require_sid ");

        builder.append("  ORDER BY tr.require_finish_dt ASC");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.WAIT_CLOSE, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.WAIT_CLOSE, SecurityFacade.getUserSid());

        // 查詢
        List<Search29View> results = search29QueryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return results;
    }

    private void buildRequireCondition(StringBuilder builder, Map<String, Object> parameters) {

        // ====================================
        // 組管理單位SQL
        // ====================================
        String managerDepConditionSql = wkUserAndOrgLogic.prepareCanViewDepSidsBaseOnMinsterial(SecurityFacade.getUserSid(), false).stream()
                .map(sid -> sid + "")
                .collect(Collectors.joining(", ", " OR  tr.dep_sid IN (", ")"));

        // ====================================
        // 組SQL
        // ====================================
        builder.append("WHERE tr.require_sid IS NOT NULL ");
        // 需求類別製作進度
        builder.append(" AND tr.require_status = '" + RequireStatusType.COMPLETED.name() + "' ");
        builder.append("       AND ( tr.create_usr = " + SecurityFacade.getUserSid() + " ");
        builder.append("       " + managerDepConditionSql + ") ");

    }

    private void buildRequireIndexDictionaryCondition(StringBuilder builder, Map<String, Object> parameters) {
        // 模糊搜尋
        builder.append(" INNER JOIN  tr_index_dictionary tid ON tr.require_sid=tid.require_sid AND tid.field_name='主題' ");

    }

    private void buildCategoryKeyMappingCondition(StringBuilder builder, Map<String, Object> parameters) {
        builder.append(" INNER JOIN  tr_category_key_mapping ckm ON tr.mapping_sid=ckm.key_sid ");
    }

    private void buildBasicDataSmallCategoryCondition(StringBuilder builder, Map<String, Object> parameters) {
        builder.append("LEFT JOIN (SELECT sc.basic_data_small_category_sid,sc.check_attachment FROM "
                + "tr_basic_data_small_category sc) AS sc ON sc.basic_data_small_category_sid = ckm.small_category_sid ");
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Search29View view) {
        view.setReadRecordType(ReadRecordType.HAS_READ);
        Require r = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(r, loginBean.getUser());
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (Search29View) event.getObject();
        this.changeRequireContent(this.queryKeeper);
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Search29View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        try {
            helper.exportExcel(document, loginBean.getUser().getName(), "需求單待辦提醒：" + commHeaderMBean.getReportType().getReportName());
        } catch (Exception e) {
            log.error("exportExcel ERROR", e);
        }
        // helper.exportExcel(document,
        // searchQuery10.getSearch10QueryVO().getStartDate(),
        // searchQuery10.getSearch10QueryVO().getEndDate(),
        // commHeaderMBean.getReportType());
        hasDisplay = true;
    }

    /**
     * 隱藏部分column裡的內容
     */
    public void hideColumnContent() {
        hasDisplay = false;
    }

    public String getAttInfo(Search29View view) {
        Boolean checkAtt = view.getCheckAttachment();
        if (checkAtt) {
            if (view.getHasAttachment()) {
                return "檢核正常";
            } else {
                return "檢核異常";
            }
        } else {
            return "不需檢核";
        }
    }

    /**
     * 列表執行關聯動作
     *
     * @param view
     */
    public void initRelevance(Search29View view) {
        this.querySelection = view;
        this.queryKeeper = this.querySelection;
        Require r = requireService.findByReqNo(view.getRequireNo());
        this.r01MBean.setRequire(r);
        this.r01MBean.getRelevanceMBean().initRelevance();
    }

    public String getRelevanceViewUrl(Search29View view) {
        if (view == null) {
            return "";
        }
        Require r = requireService.findByReqNo(view.getRequireNo());
        if (r == null) {
            return "";
        }
        WorkLinkGroup link = r.getLinkGroup();
        if (link == null) {
            return "";
        }
        return "../require/require04.xhtml" + urlService.createSimpleURLLink(URLServiceAttr.URL_ATTR_L, link.getSid(), view.getRequireNo(), 1);
    }

    /**
     * 列表點選轉寄功能
     * 
     * @param view
     */
    public void openForwardDialog(Search29View view) {
        this.queryKeeper = this.querySelection = view;

        if (SwitchType.DETAIL.equals(switchType)) {
            this.changeRequireContent(this.queryKeeper);
            this.display.update(Lists.newArrayList(
                    "@(.reportUpdateClz)",
                    "title_info_click_btn_id",
                    "require01_title_info_id",
                    "require_template_id",
                    "viewPanelBottomInfoId",
                    "req03botmid",
                    this.dataTableId + "1"));
        }

        RequireForwardDeAndPersonMBean mbean = WorkSpringContextHolder.getBean(RequireForwardDeAndPersonMBean.class);
        mbean.openDialog(view.getRequireNo(), r01MBean);
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search29View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.removeClassByTextBold(dtId, pageCount);
        this.transformHasRead();
        this.checkHelfScreen();
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            display.update("headerTitle");
            display.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Search29View to) {
        querySelection = to;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 去除粗體Class
     *
     * @param dtId
     * @param pageCount
     */
    private void removeClassByTextBold(String dtId, String pageCount) {
        display.execute("removeClassByTextBold('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
        display.execute("changeAlreadyRead('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 變更已閱讀
     */
    private void transformHasRead() {
        querySelection.setReadRecordType(ReadRecordType.HAS_READ);
        queryKeeper = querySelection;
        queryItems.set(queryItems.indexOf(querySelection), querySelection);
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.transformHasRead();
        this.removeClassByTextBold(dtId, pageCount);
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    /**
     * 列表執行追蹤動作
     *
     * @param view
     */
    public void btnAddTrack(Search29View view) {
        Require r = this.highlightAndReturnRequire(view);
        this.r01MBean.setRequire(r);
        this.r01MBean.getTraceActionMBean().initTrace(this.r01MBean);
    }

    /**
     * 列表標註及回傳需求單
     *
     * @param view
     * @return
     */
    private Require highlightAndReturnRequire(Search29View view) {
        this.querySelection = view;
        this.queryKeeper = this.querySelection;
        return requireService.findByReqNo(view.getRequireNo());
    }
}
