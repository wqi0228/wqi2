package com.cy.tech.request.web.controller.require.help;

import com.cy.commons.util.FusionUrlServiceUtils;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.enumerate.PropKeyType;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.group.vo.WorkLinkGroupDetail;
import com.cy.work.group.vo.to.GroupDetailListTo;
import com.google.common.base.Strings;
import java.io.Serializable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author kasim
 */
@Slf4j
@Component
public class Req04ByIssueHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7954344151297195826L;

    @Autowired
    transient private DisplayController displayController;

    private String issueOpenURL = "/techIssue/techIssue11.xhtml?technicalCaseNo=";

    private String issueHalfURL = "/techIssue/techIssue10_halfcontent.xhtml?technicalCaseNo=";

    /**
     * dataTable選擇事件
     *
     * @param url
     * @param listTo
     */
    public void onRowSelect(GroupDetailListTo listTo, String urlProtocol) {
        this.hideReq(listTo);
        this.transformHasRead(listTo);
        this.removeClassByTextBold(listTo);
        String url = buildContextPath() + issueHalfURL
                + listTo.getSelectedByIssue().getSourceNo();
        if (!Strings.isNullOrEmpty(urlProtocol)) {
            if (!url.contains(urlProtocol)) {
                if (url.startsWith("http:")) {
                    url = url.replace("http:", urlProtocol);
                } else if (url.startsWith("https:")) {
                    url = url.replace("https:", urlProtocol);
                }
            }
        }
        log.info("onRowSelectByReq URL [" + url + "]");
        displayController.execute("changeIframeUrl('" + url + "');");
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param to
     */
    public void btnOpenUrl(WorkLinkGroupDetail to, GroupDetailListTo listTo) {
        this.highlightReportTo(to, listTo);
        this.removeClassByTextBold(listTo);
        this.transformHasRead(listTo);
    }

    /**
     * 變更已閱讀
     *
     * @param listTo
     */
    private void transformHasRead(GroupDetailListTo listTo) {
        listTo.getSelectedByIssue().setReadType("HASREAD");
        listTo.getGroupDetailsByIssue().set(
                listTo.getGroupDetailsByIssue().indexOf(listTo.getSelectedByIssue()), listTo.getSelectedByIssue());
    }

    /**
     * 去除粗體Class
     *
     * @param listTo
     */
    private void removeClassByTextBold(GroupDetailListTo listTo) {
        displayController.execute("removeClassByTextBold('" + listTo.getDataTable_issue() + "'," + this.getRowIndex(listTo) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param listTo
     * @return
     */
    private int getRowIndex(GroupDetailListTo listTo) {
        return listTo.getGroupDetailsByIssue().indexOf(listTo.getSelectedByIssue()) % 50;
    }

    /**
     * 重建資料
     *
     * @param user
     * @param to
     */
    public void buildCol(User user, WorkLinkGroupDetail to) {
        if (to == null) {
            return;
        }
        if (to.getReadType() == null) {
            if (to.getReadRecord().indexOf("\"reader\":" + user.getSid() + ",\"read\":\"HASREAD\"") > 1) {
                to.setReadType("HASREAD");
            } else if (to.getReadRecord().indexOf("\"reader\":" + user.getSid() + ",\"read\":\"WAIT_READ\"") > 1) {
                to.setReadType("WAIT_READ");
            } else {
                to.setReadType("UNREAD");
            }
        }
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param to
     * @param listTo
     */
    private void highlightReportTo(WorkLinkGroupDetail to, GroupDetailListTo listTo) {
        listTo.setSelectedByIssue(to);
        this.hideReq(listTo);
        displayController.execute("selectRow('" + listTo.getWidgetVar_issue() + "'," + this.getRowIndex(listTo) + ");");
    }

    private void hideReq(GroupDetailListTo listTo) {
        listTo.setSelectedByReq(null);
        displayController.execute(" PF('" + listTo.getWidgetVar_req() + "').unselectAllRows();");
    }

    /**
     * 取得案件單另開分頁網址
     *
     * @param sourceNo
     * @return
     */
    public String getIssueURL(String sourceNo) {
        return buildContextPath() + issueOpenURL + sourceNo;
    }

    private String buildContextPath() {
        return FusionUrlServiceUtils.getUrlByPropKey(PropKeyType.TECH_ISSUE_AP_URL.getValue());
    }
}
