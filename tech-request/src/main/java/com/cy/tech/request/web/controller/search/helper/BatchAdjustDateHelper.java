package com.cy.tech.request.web.controller.search.helper;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.service.send.test.BatchAdjustDateService;
import com.cy.tech.request.logic.utils.DateUtils;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.repository.result.BatchAdjustDateVO;
import com.cy.tech.request.vo.worktest.enums.WorkTestInfoHistoryBehavior;
import com.cy.tech.request.web.controller.view.vo.BatchAdjustDateQueryVO;
import com.cy.tech.request.web.controller.view.vo.BatchAdjustDateQueryVO.QueryDateType;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * @author aken_kao
 */
@Service
public class BatchAdjustDateHelper {
    @Autowired
    private BatchAdjustDateService adjustOnlineService;
    @Autowired
    private ReqularPattenUtils reqularUtils;

    @Autowired
    private NativeSqlRepository nativeSqlRepository;

    public List<BatchAdjustDateVO> findBatchAdjustDateList(
            BatchAdjustDateQueryVO queryVO,
            User loginUser,
            boolean isQAManger) {

        // ====================================
        // 組SQL
        // ====================================
        Map<String, Object> parameters = Maps.newHashMap();

        StringBuffer sql = new StringBuffer();
        sql.append("SELECT tr.create_dt                AS trCreatedDate, ");
        sql.append("       tr.require_no               AS requireNo, ");
        sql.append("       dict.field_content          AS requireTheme, ");
        sql.append("       testInfo.dep_sid            AS wtCreateDeptSid, ");
        sql.append("       testInfo.create_usr         AS wtCreateUserSid, ");
        sql.append("       testInfo.testinfo_theme     AS wtTheme, ");
        sql.append("       testInfo.testinfo_status    AS wtStatus, ");
        sql.append("       testInfo.expect_online_date AS wtExpectOnlineDate, ");
        sql.append("       testInfo.qa_schedule_status AS wtQaScheduleStatus, ");
        sql.append("       testInfo.test_date          AS testDate, ");
        sql.append("       testInfo.testinfo_no        AS testInfoNo, ");
        sql.append("       testInfo.testinfo_sid       AS testInfoSid, ");
        sql.append("       testInfo.qa_audit_status    AS qaAuditStatus, ");
        sql.append("       testInfo.commit_status      AS commitStatus ");
        sql.append("FROM   (SELECT * ");
        sql.append("        FROM   work_test_info ");
        sql.append("        WHERE  qa_schedule_status = 'JOIN_SCHEDULE' ");
        sql.append("               AND testinfo_status != 'CANCEL_TEST') testInfo ");
        sql.append("       INNER JOIN (SELECT require_sid, ");
        sql.append("                          require_no, ");
        sql.append("                          create_dt ");
        sql.append("                   FROM   tr_require ");
        sql.append("                   WHERE  require_status IN ( 'PROCESS', 'COMPLETED' )) tr ");
        sql.append("               ON tr.require_sid = testInfo.testinfo_source_sid ");
        sql.append("       LEFT JOIN (SELECT require_sid, ");
        sql.append("                         field_content ");
        sql.append("                  FROM   tr_index_dictionary ");
        sql.append("                  WHERE  field_name = '主題') dict ");
        sql.append("              ON dict.require_sid = tr.require_sid ");
        sql.append("WHERE  1 = 1 ");

        // 查詢條件:需求單號
        if (WkStringUtils.notEmpty(queryVO.getRequireNo())) {
            sql.append("       AND tr.require_no = :requireNo ");
            parameters.put("requireNo", queryVO.getRequireNo());
        }

        // 查詢條件：起迄日
        String dateColumnName = QueryDateType.REQUIRE_DATE.equals(queryVO.getQueryDateType())
                ? "tr.create_dt"
                : "testInfo.test_date";

        if (queryVO.getDateIntervalVO() != null
                && queryVO.getDateIntervalVO().getStartDate() != null) {
            sql.append("       AND " + dateColumnName + " >= :startDate ");
            parameters.put("startDate", queryVO.getDateIntervalVO().getStartDate());
        }

        if (queryVO.getDateIntervalVO() != null
                && queryVO.getDateIntervalVO().getEndDate() != null) {
            sql.append("       AND " + dateColumnName + " <= :endDate ");
            Date endDate = DateUtils.convertToEndOfDay(queryVO.getDateIntervalVO().getEndDate());
            parameters.put("endDate", endDate);
        }

        String requireTheme = reqularUtils.replaceIllegalSqlLikeStr(queryVO.getRequireTheme());
        if (WkStringUtils.notEmpty(requireTheme)) {
            sql.append("       AND dict.field_content LIKE :requireTheme ");
            parameters.put("requireTheme", "%" + requireTheme + "%");

        }
        sql.append("ORDER  BY testInfo.test_date DESC");

        // ====================================
        // monitor
        // ====================================
        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.BATCH_ADJUST_DATE, sql.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.BATCH_ADJUST_DATE, SecurityFacade.getUserSid());

        // ====================================
        // 查詢
        // ====================================
        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        List<BatchAdjustDateVO> resultList = this.nativeSqlRepository.getResultList(
                sql.toString(), parameters, BatchAdjustDateVO.class);

        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((resultList == null) ? 0 : resultList.size());
        if (WkStringUtils.isEmpty(resultList)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();
        if (isQAManger) {
            resultList = adjustOnlineService.convertForQAManager(resultList);
        } else {
            resultList = adjustOnlineService.convert(resultList, loginUser);
        }
        // 解析資料-結束
        usageRecord.parserDataEnd();

        // ====================================
        // 儲存使用記錄
        // ====================================
        usageRecord.saveUsageRecord();

        return resultList;
    }

    public void batchAdjustDate(List<BatchAdjustDateVO> selectedResultList, WorkTestInfoHistoryBehavior behavior, Date adjustDate, User loginUser) {
        List<String> testInfoSids = selectedResultList.stream().map(BatchAdjustDateVO::getTestInfoSid).collect(Collectors.toList());
        adjustOnlineService.batchAdjustDate(testInfoSids, behavior, adjustDate, loginUser);
    }

    public List<BatchAdjustDateVO> findWorkTestInfoByBehavior(String requireNo, WorkTestInfoHistoryBehavior behavior,
            User loginUser, boolean isQAManager) {
        return adjustOnlineService.findWorkTestInfoByBehavior(requireNo, behavior, loginUser, isQAManager);
    }
}
