/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.attach;

import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.vo.Attachment;
import com.google.common.base.Strings;
import java.io.Serializable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 控制附加檔案上傳彈跳視窗
 *
 * @author shaun
 */
@NoArgsConstructor
@Controller
//@Scope(WebApplicationContext.SCOPE_REQUEST)　
@Scope("view")
@SuppressWarnings({ "rawtypes", "unchecked" })
public class AttachDlgMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2548282496140431725L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private DisplayController display;

    /** 附加檔案模型物件 */
    @Getter
    private IAttachDlg mbean;
    /** 附加檔案依附主體 */
    @Getter
    transient private Object dependEntity;
    /** 上傳檔案最大容許值 */
    @Getter
    private Integer uploadFileSize;
    /** 關閉上傳視窗後，要更新的外部ID */
    @Getter
    private String whenCloseDlgUpdate;
    /** 關閉刪除視窗時，需更新ID */
    @Getter
    private String whenDelDoUpdate;
    /** 關閉刪除視窗後，需執行javascript */
    @Getter
    private String whenDelDoJs;
    /** 附加檔案的view dlg widget */
    private String attachViewWv;

    /**
     * attach_upload_dlg_wv.xhtml
     * <P/>
     * 開啟上傳附加檔案視窗
     *
     * @param mbean
     * @param dependEntity
     * @param uploadFileSize
     * @param whenCloseDlgUpdate
     */
    public void openUploadDlg(
            IAttachDlg mbean,
            Object dependEntity,
            Integer uploadFileSize,
            String whenCloseDlgUpdate) {
        this.mbean = mbean;
        this.dependEntity = dependEntity;
        this.uploadFileSize = uploadFileSize;
        this.whenCloseDlgUpdate = whenCloseDlgUpdate;
        if (this.mbean != null) {
            this.mbean.onUploadDialogOpen(this.uploadFileSize);
        }
    }

    /**
     * attach_upload_dlg_wv.xhtml
     * <P/>
     * 開啟移除附加檔案提示視窗
     *
     * @param mbean
     * @param dependEntity
     * @param selDelAttach
     * @param whenDelDoUpdate
     * @param whenDelDoJs
     */
    public void openDelDlg(
            IAttachDlg mbean,
            Object dependEntity,
            Attachment selDelAttach,
            String whenDelDoUpdate,
            String whenDelDoJs,
            String attachViewWv) {
        this.mbean = mbean;
        this.dependEntity = dependEntity;
        this.whenDelDoUpdate = whenDelDoUpdate;
        this.whenDelDoJs = whenDelDoJs;
        this.attachViewWv = attachViewWv;
        if (this.mbean != null && this.dependEntity != null && selDelAttach != null) {
            this.mbean.initRecycle(selDelAttach);
        }
    }

    public void delete(Require01MBean r01MBean) {
        this.mbean.whenDeleteAttachment(r01MBean, this.dependEntity);
        requireService.updateRecordByDelReqAttachment(r01MBean.getRequire(), loginBean.getUser());
        this.closeAttachViewDlg();
    }

    private void closeAttachViewDlg() {
        if (Strings.isNullOrEmpty(this.attachViewWv)) {
            return;
        }
        if (this.mbean.findAttachCnt(this.dependEntity) == 0) {
            display.hidePfWidgetVar(this.attachViewWv);
        }
    }

}
