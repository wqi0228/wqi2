package com.cy.tech.request.web.controller.component.singleselecttree;

import java.io.Serializable;
import java.util.List;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.viewcomponent.helper.PickerComponentHelper;

import lombok.Getter;
import lombok.Setter;

/**
 * 單選樹選擇器
 * @author allen1214_wu
 */
public class SingleSelectTreePicker implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -2343347372623942878L;

    // ========================================================================
	// 服務元件區
	// ========================================================================
	private PickerComponentHelper pickerHelper = PickerComponentHelper.getInstance();

	// ========================================================================
	// 變數區
	// ========================================================================
	/**
	 * 所有項目
	 */
	@Getter
	private List<WkItem> allItems;

	/**
	 * 樹節點結構資料
	 */
	@Getter
	@Setter
	private TreeNode rootNode = new DefaultTreeNode();

	/**
	 * 搜尋關鍵字
	 */
	@Getter
	@Setter
	private String serachItemNameKeyword;

	/**
	 * 已選擇項目
	 */
	@Getter
	@Setter
	private TreeNode selectedItem;

	/**
	 * 
	 */
	@Getter
	private SingleSelectTreeCallback singleSelectTreeCallback;

	// ========================================================================
	// 方法區
	// ========================================================================
	/**
	 * 建構子
	 * 
	 * @param config 建構參數容器
	 */
	public SingleSelectTreePicker(SingleSelectTreePickerConfig singleSelectTreePickerConfig) {

		// ====================================
		// 取得傳入參數
		// ====================================
		this.singleSelectTreeCallback = singleSelectTreePickerConfig.getSingleSelectTreeCallback();

		// ====================================
		// 由外部取得所有項目
		// ====================================
		this.allItems = singleSelectTreeCallback.prepareAllItems();

		// ====================================
		// 建立項目樹
		// ====================================
		this.selectedItem = this.pickerHelper.buildSingleSelectTree(
		        this.rootNode,
		        this.allItems,
		        singleSelectTreePickerConfig.getDefaultSelectedItemSid());
	}

	/**
	 * 回傳已選擇項目的 Sid (未選擇時，回傳 null )
	 * 
	 * @return depSid
	 */
	public String getSelectedSid() {

		WkItem selectedItem = this.getSelectedWkItem();
		if (selectedItem == null || WkStringUtils.isEmpty(selectedItem.getSid())) {
			return null;
		}

		return selectedItem.getSid();
	}

	/**
	 * @return
	 */
	public WkItem getSelectedWkItem() {
		if (this.selectedItem == null || this.selectedItem.getData() == null) {
			return null;
		}

		return (WkItem) this.selectedItem.getData();
	}

	/**
	 * 事件：搜尋
	 */
	public void event_searchItemNameByKeyword() {
		// 重新轉換查詢關鍵字, 忽略大小寫差異
		String keyword = WkStringUtils.safeTrim(serachItemNameKeyword).toUpperCase();
		// 遞迴展開名稱符合或已選的節點
		this.pickerHelper.tree_searchItemNameByKeyword_recursive(
		        this.rootNode,
		        keyword,
		        "sstreeStyle-highlight-keyword",
		        WkStringUtils.isNumber(keyword),
		        false);
	}

	/**
	 * 事件： 擊點項目時的動作
	 */
	public void event_clickItem() {
		if (this.singleSelectTreeCallback != null) {
			this.singleSelectTreeCallback.clickItem();
		}
	}

	/**
	 * 清除已選擇
	 */
	public void clearSelectedItem() {
		// 清除選擇項目
		this.selectedItem = null;
		// 清除搜尋資訊
		this.serachItemNameKeyword = "";
		// 收合所有節點
		this.setExpandedToAllNode(this.rootNode, false);
	}

	/**
	 * 展開或收合所有節點
	 * 
	 * @param treeNode
	 * @param isExpanded
	 */
	private void setExpandedToAllNode(TreeNode treeNode, boolean isExpanded) {
		if (treeNode == null) {
			return;
		}
		treeNode.setExpanded(isExpanded);
		if (WkStringUtils.notEmpty(treeNode.getChildren())) {
			for (TreeNode childrenNode : treeNode.getChildren()) {
				setExpandedToAllNode(childrenNode, isExpanded);
			}
		}
	}
}
