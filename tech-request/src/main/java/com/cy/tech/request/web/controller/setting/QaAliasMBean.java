package com.cy.tech.request.web.controller.setting;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;

import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.send.test.QaAliasService;
import com.cy.tech.request.vo.value.to.WorkTestQaAliasVO;
import com.cy.tech.request.vo.value.to.WorkTestScheduleVO;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author aken_kao
 */
@Slf4j
@Controller
@Scope("view")
public class QaAliasMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5590028662114362003L;
    @Autowired
    private QaAliasService qaAliasService;
    @Autowired
    private WorkBackendParamHelper workBackendParamHelper;

    @Getter
    @Setter
    private WorkTestScheduleVO scheduleVO;

    @Getter
    @Setter
    private List<WorkTestQaAliasVO> qaAliasList = Lists.newArrayList();

    @PostConstruct
    public void init() {
        this.prepareQAUser();
    }

    private void prepareQAUser() {
        // 取得 QAUser
        List<User> qaUsers = workBackendParamHelper.getQAUsers();
        if (WkStringUtils.isEmpty(qaUsers)) {
            return;
        }

        // 排序
        qaUsers = qaUsers.stream()
                .sorted(Comparator.comparing(User::getId))
                .collect(Collectors.toList());

        // 轉為VO
        this.qaAliasList = qaAliasService.convertToQaAliasList(qaUsers);

        // 若尚未設定，暱稱給空值
        for (WorkTestQaAliasVO vo : qaAliasList) {
            if (vo.getSid() == null) {
                vo.setUserAlias(null);
            }
        }
    }

    public void onRowEdit(RowEditEvent event) {
        try {
            qaAliasService.saveOrUpdate((WorkTestQaAliasVO) event.getObject(), SecurityFacade.getUserSid());
        } catch (DataIntegrityViolationException e) {
            MessagesUtils.showError("重複的keyword，請重新設定！");
            init();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
