/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search.helper;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search03QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.BaseSearchView;
import com.cy.tech.request.logic.search.view.Search03View;
import com.cy.tech.request.logic.service.RequireCheckItemService;
import com.cy.tech.request.logic.service.setting.SettingCheckConfirmRightService;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.web.view.to.search.query.SearchQuery03;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.logic.lib.manager.WorkCommonReadRecordManager;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 輔助 Search03MBean
 *
 * @author kasim
 */
@Component
public class Search03Helper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1383092908337053990L;
    @Autowired
    transient private Search03QueryService search03QueryService;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SettingCheckConfirmRightService settingCheckConfirmRightService;
    @Autowired
    transient private SearchResultHelper searchResultHelper;
    @Autowired
    transient private RequireCheckItemService requireCheckItemService;
    @Autowired
    transient private WorkCommonReadRecordManager workCommonReadRecordManager;

    /**
     * 取得 關聯檢視 網址
     *
     * @param view
     * @return
     */
    public String getRelevanceViewUrl(Search03View view) {
        if (view == null) {
            return "";
        }
        return helper.getRelevanceViewUrl(view.getRequireNo());
    }

    /**
     * 查詢
     *
     * @param loginDep
     * @param loginUser
     * @param query
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Search03View> search(String compId, Org loginDep, User loginUser, SearchQuery03 query) {

        // ====================================
        // 取得登入者檢查權限
        // ====================================
        List<RequireCheckItemType> loginUserCanChekItems = settingCheckConfirmRightService.findUserCheckRights(
                loginUser.getSid());

        // 沒有任何權限時，直接回傳空值
        if (WkStringUtils.isEmpty(loginUserCanChekItems)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 兜組 SQL
        // ====================================
        String requireNo = reqularUtils.getRequireNo(compId, query.getFuzzyText());
        StringBuilder builder = new StringBuilder();
        Map<String, Object> parameters = Maps.newHashMap();
        builder.append("SELECT DISTINCT tr.require_sid, ");
        builder.append("                tr.require_no, ");
        builder.append("                tid.field_content, ");
        builder.append("                tr.urgency, ");
        builder.append("                tr.has_forward_dep, ");
        builder.append("                tr.has_forward_member, ");
        builder.append("                tr.has_link, ");
        builder.append("                tr.create_dt, ");
        builder.append("                ckm.big_category_name, ");
        builder.append("                ckm.middle_category_name, ");
        builder.append("                ckm.small_category_name, ");
        builder.append("                tr.dep_sid, ");
        builder.append("                tr.create_usr, ");
        builder.append("                tr.require_status, ");

        this.builderHasTraceColumn(loginDep, loginUser, builder);
        builder.append("                GROUP_CONCAT(IF(isnull(checkitem.check_date), 'N', 'Y') SEPARATOR ',') AS isCheckeds, ");
        // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
        builder.append(this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire());

        builder.append("FROM ( ");
        this.buildRequireCondition(loginUser, query, requireNo, builder);
        this.buildRequireIndexDictionaryCondition(query, requireNo, builder, parameters);
        this.buildCategoryKeyMappingCondition(query, requireNo, builder);

        // 檢查項目 (系統別)
        // 後方需對 tr.require_sid 做 group by
        builder.append(this.searchConditionSqlHelper.prepareCommonJoin(SecurityFacade.getUserSid()));
        builder.append(" WHERE 1=1 ");

        // 查詢條件：是否閱讀
        if (WkStringUtils.isEmpty(requireNo)) {
            builder.append(
                    this.workCommonReadRecordManager.prepareWhereConditionSQL(
                            ReadRecordType.safeValueOf(query.getSelectReadRecordType()), "readRecord"));
        }

        builder.append(" GROUP BY tr.require_sid ");
        builder.append("ORDER BY tr.update_dt DESC ");

        // ====================================
        // 查詢
        // ====================================
        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.WAIT_CHECK, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.WAIT_CHECK, SecurityFacade.getUserSid());

        List<Search03View> resultList = search03QueryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // ====================================
        // 後續處理
        // ====================================
        if (WkStringUtils.notEmpty(resultList)) {
            // 後續處理-開始
            usageRecord.afterProcessStart();
            // ====================================
            // 過濾可檢查項目並準備檢查資訊
            // ====================================
            // 收集 requireSid
            List<String> requireSids = resultList.stream()
                    .map(Search03View::getSid)
                    .collect(Collectors.toList());

            // 查詢已檢查項目
            Map<String, List<RequireCheckItemType>> checkedItemsMapByRequireSid = requireCheckItemService.prepareCheckedItemsByRequireSid(
                    requireSids);

            resultList = resultList.stream()
                    .filter(search03View -> this.isCanShowByCheckItem(
                            search03View.getRequireStatus(),
                            search03View.getItemCheckeds(),
                            search03View.getCheckItemTypes(),
                            checkedItemsMapByRequireSid.get(search03View.getSid()),
                            loginUserCanChekItems,
                            query.getCheckItemTypes()))
                    .collect(Collectors.toList());

            // 準備檢查項目資訊欄位
            this.searchResultHelper.prepareCheckConfirmInfo(
                    (List<BaseSearchView>) (List<?>) resultList,
                    loginUser.getSid(),
                    true);

            // 後續處理-結束
            usageRecord.afterProcessEnd();
        }

        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return resultList;

    }

    /**
     * 依據案件的檢查項目&狀態，判斷是否需顯示該筆資料
     * 
     * @param requireStatus
     * @param itemCheckeds            項目檢查狀態
     * @param caseCheckItemTypes      『該單據被勾選』的項目
     * @param checkedItemTypes        『已經檢查完成』的項目
     * @param canCheckItems           『登入者可檢查』的項目
     * @param conditionCheckItemTypes 查詢條件過濾
     * @return
     */
    private boolean isCanShowByCheckItem(
            RequireStatusType requireStatus,
            List<Boolean> itemCheckeds,
            List<RequireCheckItemType> caseCheckItemTypes,
            List<RequireCheckItemType> checkedItemTypes,
            List<RequireCheckItemType> canCheckItems,
            List<RequireCheckItemType> conditionCheckItemTypes) {

        if (checkedItemTypes == null) {
            checkedItemTypes = Lists.newArrayList();
        }

        // ====================================
        // 主單不是待檢查確認的 不顯示 (應該不會出現)
        // ====================================
        if (!RequireStatusType.WAIT_CHECK.equals(requireStatus)) {
            return false;
        }

        // ====================================
        // REQ-1593 異常單據-強制顯示
        // 全部都檢查了, 但是主單狀態還是為『待檢查確認』
        // ====================================
        boolean isAllChecked = WkCommonUtils.safeStream(itemCheckeds)
                .allMatch(itemChecked -> itemChecked);

        // 全部都檢查了 (但單據還在待檢查確認)
        if (isAllChecked) {
            return true;
        }

        // ====================================
        // 排除使用者無檢查權限
        // ====================================
        boolean isCanChecked = false;
        for (int i = 0; i < caseCheckItemTypes.size(); i++) {
            // 案件的檢查項目
            RequireCheckItemType currCheckItemType = caseCheckItemTypes.get(i);
            // 該檢查項目是否已經檢查了
            boolean isChecked = checkedItemTypes.contains(currCheckItemType);

            // 該『需求單』有任何一個『檢查項目』，登入者
            // 1.有檢查權限
            // 2.且未檢查
            if (canCheckItems.contains(currCheckItemType) && !isChecked) {
                isCanChecked = true;
                break;
            }

        }
        // 無檢查權限 out
        if (!isCanChecked) {
            return false;
        }

        // ====================================
        // 過濾僅顯示『查詢條件』包含的檢查項目
        // ====================================
        boolean isByCondition = this.searchResultHelper.filterCheckItems(
                caseCheckItemTypes,
                conditionCheckItemTypes,
                false,
                null);

        if (!isByCondition) {
            return false;
        }

        return true;

    }

    /**
     * 是否有追蹤資料
     *
     * @param loginDep
     * @param loginUser
     * @param builder
     */
    private void builderHasTraceColumn(Org loginDep, User loginUser, StringBuilder builder) {
        builder.append("( ").append("\n");
        builder.append("    SELECT CASE WHEN (COUNT(trace.tracesid) > 0) THEN 'TRUE' ELSE 'FALSE' END ").append("\n");
        builder.append("    FROM work_trace_info trace ").append("\n");
        builder.append("        WHERE trace.trace_source_no = tr.require_no ").append("\n");
        builder.append("        AND trace.trace_source_type = 'TECH_REQUEST' ").append("\n");
        builder.append("        AND ( ").append("\n");
        builder.append("            trace.trace_dep = ").append(loginDep.getSid()).append("\n");
        builder.append("            OR ").append("\n");
        builder.append("            trace.trace_usr = ").append(loginUser.getSid()).append("\n");
        builder.append("        ) ").append("\n");
        builder.append("        AND trace.trace_status = 'UN_FINISHED' ").append("\n");
        builder.append("        AND trace.Status = '0' ").append("\n");
        builder.append(") AS hasTrace, ").append("\n");
    }

    /**
     * 組合需求單部分
     * 
     * @param loginUser         登入者
     * @param query             SearchQuery03
     * @param requireNo         需求單號
     * @param canCheckItemTypes 登入者可檢查的檢查類別
     * @param builder
     */
    private void buildRequireCondition(
            User loginUser,
            SearchQuery03 query,
            String requireNo,
            StringBuilder builder) {

        builder.append("    SELECT * ");
        builder.append("    FROM tr_require tr ");
        builder.append("    WHERE 1=1 ").append("\n");

        // 單據狀態為待檢查
        builder.append("    AND tr.require_status = '" + RequireStatusType.WAIT_CHECK.name() + "' ");

        // 需求單號
        if (!Strings.isNullOrEmpty(requireNo)) {
            builder.append("    AND tr.require_no = '").append(requireNo).append("' ").append("\n");
        }

        //////////////////// 以下為進階搜尋條件//////////////////////////////
        if (Strings.isNullOrEmpty(requireNo)) {
            // 異動區間
            if (query.getStartUpdatedDate() != null && query.getEndUpdatedDate() != null) {
                builder.append("    AND tr.update_dt BETWEEN '")
                        .append(helper.transStrByStartDate(query.getStartUpdatedDate())).append("' AND '")
                        .append(helper.transStrByEndDate(query.getEndUpdatedDate())).append("' ").append("\n");
            } else if (query.getStartUpdatedDate() != null) {
                builder.append("    AND tr.update_dt >= '")
                        .append(helper.transStrByStartDate(query.getStartUpdatedDate())).append("' ").append("\n");
            } else if (query.getEndUpdatedDate() != null) {
                builder.append("    AND tr.update_dt <= '")
                        .append(helper.transStrByEndDate(query.getEndUpdatedDate())).append("' ").append("\n");
            }
            // 緊急度
            if (query.getUrgencyList() != null && !query.getUrgencyList().isEmpty()) {
                String urgencyStr = query.getUrgencyList().stream()
                        .collect(Collectors.joining(","));
                builder.append("    AND tr.urgency IN (").append(urgencyStr).append(") ").append("\n");
            }
            // 需求單號
            if (!Strings.isNullOrEmpty(query.getRequireNo())) {
                String textNo = "'%" + reqularUtils.replaceIllegalSqlLikeStr(query.getRequireNo()) + "%'";
                builder.append("    AND tr.require_no LIKE ").append(textNo).append(" ").append("\n");
            }

            // 轉發部門
            if (query.getForwardDepts() != null && !query.getForwardDepts().isEmpty()) {
                String forwardDepts = query.getForwardDepts().stream()
                        .collect(Collectors.joining(","));
                builder.append("    AND tr.require_sid IN ( ").append("\n");
                builder.append("        SELECT tai.require_sid FROM tr_alert_inbox tai  ").append("\n");
                builder.append("            WHERE tai.forward_type = 'FORWARD_DEPT' ").append("\n");
                builder.append("            AND tai.receive_dep IN (").append(forwardDepts).append(") ").append("\n");
                builder.append("    ) ").append("\n");
            }

        }
        builder.append(" GROUP BY tr.require_sid ) AS tr ").append("\n");
    }

    /**
     * 組合模糊查詢語句
     *
     * @param query
     * @param requireNo
     * @param builder
     */
    private void buildRequireIndexDictionaryCondition(SearchQuery03 query, String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN ( ").append("\n");
        builder.append("    SELECT tid.require_sid,tid.field_content FROM tr_index_dictionary tid ").append("\n");
        builder.append("        WHERE tid.field_name='主題' ").append("\n");
        // 模糊搜尋
        if (Strings.isNullOrEmpty(requireNo)
                && !Strings.isNullOrEmpty(query.getFuzzyText())) {
            String text = "%" + reqularUtils.replaceIllegalSqlLikeStr(query.getFuzzyText()) + "%";
            builder.append("        AND ( ").append("\n");
            builder.append("            tid.require_sid IN ( ").append("\n");
            builder.append("                SELECT DISTINCT tid1.require_sid FROM tr_index_dictionary tid1 ").append("\n");
            builder.append("                    WHERE tid1.field_content LIKE :text ").append("\n");
            builder.append("            ) ").append("\n");
            builder.append("            OR ").append("\n");
            builder.append("            tid.require_sid IN ( ").append("\n");
            builder.append("               SELECT DISTINCT trace.require_sid FROM tr_require_trace trace ").append("\n");
            builder.append("                  WHERE trace.require_trace_type = 'REQUIRE_INFO_MEMO' ").append("\n");
            builder.append("                   AND trace.require_trace_content LIKE :text ").append("\n");
            builder.append("            ) ").append("\n");
            builder.append("        ) ").append("\n");
            parameters.put("text", text);
        }
        builder.append(") AS tid ON tr.require_sid=tid.require_sid ").append("\n");
    }

    /**
     * 組合模板查詢語句
     *
     * @param query
     * @param requireNo
     * @param builder
     */
    private void buildCategoryKeyMappingCondition(SearchQuery03 query, String requireNo, StringBuilder builder) {
        builder.append("INNER JOIN ( ").append("\n");
        builder.append("    SELECT * FROM tr_category_key_mapping ckm ").append("\n");
        builder.append("        WHERE 1=1 ").append("\n");
        if (Strings.isNullOrEmpty(requireNo)) {
            // 需求類別
            if (!Strings.isNullOrEmpty(query.getSelectBigCategorySid())
                    && query.getBigDataCateSids().isEmpty()) {
                builder.append("        AND ckm.big_category_sid = '")
                        .append(query.getSelectBigCategorySid()).append("' ").append("\n");
            }
            // 類別組合
            if (!query.getBigDataCateSids().isEmpty()
                    || !query.getMiddleDataCateSids().isEmpty()
                    || !query.getSmallDataCateSids().isEmpty()) {
                String bigs = "'isEmpty'";
                String middles = "'isEmpty'";
                String smalls = "'isEmpty'";
                if (!query.getBigDataCateSids().isEmpty()) {
                    bigs = query.getBigDataCateSids().stream()
                            .collect(Collectors.joining("','", "'", "'"));
                }
                if (!query.getMiddleDataCateSids().isEmpty()) {
                    middles = query.getMiddleDataCateSids().stream()
                            .collect(Collectors.joining("','", "'", "'"));
                }
                if (!query.getSmallDataCateSids().isEmpty()) {
                    smalls = query.getSmallDataCateSids().stream()
                            .collect(Collectors.joining("','", "'", "'"));
                }
                if (!Strings.isNullOrEmpty(query.getSelectBigCategorySid())) {
                    if ("'isEmpty'".equals(bigs)) {
                        bigs = "'" + query.getSelectBigCategorySid() + "'";
                    } else {
                        bigs = bigs + ",'" + query.getSelectBigCategorySid() + "'";
                    }
                }
                builder.append("        AND( ").append("\n");
                builder.append("            ckm.big_category_sid IN (").append(bigs).append(") ").append("\n");
                builder.append("            OR ").append("\n");
                builder.append("            ckm.middle_category_sid IN (").append(middles).append(") ").append("\n");
                builder.append("            OR ").append("\n");
                builder.append("            ckm.small_category_sid IN (").append(smalls).append(")").append("\n");
                builder.append("        ) ").append("\n");
            }
        }
        builder.append(") AS ckm ON tr.mapping_sid=ckm.key_sid ").append("\n");
    }

}
