package com.cy.tech.request.web.pf.utils;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;

import org.omnifaces.util.Faces;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 提供 View Controller 使用於 primefaces 中顯示的相關的工具方法
 *
 * @author cosmo
 */
@Slf4j
@Component
public class DisplayController implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1455874013772200101L;
    private static DisplayController instance;

    public static DisplayController getInstance() {
        return instance;
    }
    
    @Getter
    @Setter
    // 預設給一個Browser高度
    private Integer availableHeight = 675;

    @Override
    public void afterPropertiesSet() throws Exception {
        DisplayController.instance = this;
    }
    /**
     * 顯現某個具有 widgetVar 的 PF 元件 (PF5.0)
     *
     * @author cosmo
     * @param widgetVar : PF 元件 widgetVar 名稱
     */
    public void showPfWidgetVar(String widgetVar) {
        RequestContext.getCurrentInstance().execute("showPF('" + widgetVar + "');");
    }

    /**
     * 隱藏某個具有 widgetVar 的 PF 元件 (PF5.0)
     *
     * @author cosmo
     * @param widgetVar : PF 元件 widgetVar 名稱
     */
    public void hidePfWidgetVar(String widgetVar) {
        RequestContext.getCurrentInstance().execute("hidePF('" + widgetVar + "');");
    }

    /**
     * 顯現某個具有 widgetVar 的 PF 元件
     *
     * @author steve_chen
     * @param widgetVar : PF 元件 widgetVar 名稱
     */
    public void showWidgetVar(String widgetVar) {
        RequestContext.getCurrentInstance().execute(widgetVar + ".show()");
    }

    /**
     * 隱藏某個具有 widgetVar 的 PF 元件
     *
     * @author steve_chen
     * @param widgetVar : PF 元件 widgetVar 名稱
     */
    public void hideWidgetVar(String widgetVar) {
        RequestContext.getCurrentInstance().execute(widgetVar + ".hide()");
    }

    /**
     * update javascript from beans.
     *
     * @author steve_chen
     * @param id : PF 元件 ID
     */
    public void update(String id) {
        RequestContext.getCurrentInstance().update(id);
    }

    /**
     * 更新多筆
     *
     * @param ids
     */
    public void update(List<String> ids) {
        RequestContext.getCurrentInstance().update(ids);
    }

    /**
     * 清除某個具有 widgetVar 及 filter 功能的 PF 元件的 filter
     *
     * @param widgetVar
     */
    public void clearFilter(String widgetVar) {
        RequestContext.getCurrentInstance().execute("PF('" + widgetVar + "').clearFilters()");
    }

    /**
     * 執行某個具有 widgetVar 及 filter 功能的 PF 元件的 filter
     *
     * @param widgetVar
     */
    public void doFilter(String widgetVar) {
        RequestContext.getCurrentInstance().execute("PF('" + widgetVar + "').filter()");
    }

    /**
     * Execute javascript from beans.
     *
     * @param script : 要執行的 javascript
     */
    public void execute(String script) {
        //去前後空白
        script = WkStringUtils.safeTrim(script);
        //開發防呆
        if(WkStringUtils.isEmpty(script)) {
           WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "傳入 script 為空!");
           return;
        }
        //最後面加上分號
        if(!script.endsWith(";")) {
            script += ";";
        }
        //注入 client 待執行
        RequestContext.getCurrentInstance().execute(script);
    }

    /**
     * 獲得Request Parameter資訊
     *
     * @param
     * @return
     */
    public String getRequestParameterMap(String key) {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        return params.get(key);
    }

    /**
     * put session
     *
     * @param key
     */
    public void putSession(String key, String value) {
        Faces.getSessionMap().put(key, value);
    }

    /**
     * 移除session 參數
     *
     * @param key
     */
    public void removeSession(String key) {
        Faces.getSessionMap().remove(key);
    }

    /**
     * 判斷是否有session 參數
     *
     * @param key
     * @return boolean
     */
    public boolean containsKey(String key) {
        return Faces.getSessionMap().containsKey(key);
    }

    /**
     * 取得session 參數
     *
     * @param key
     * @return Object
     */
    public Object getSessionValue(String key) {
        return Faces.getSessionMap().get(key);
    }
    
    /**
     * reset datatable
     * 一併清除datatable filter
     * @param tableId
     */
    public void resetDataTable(String tableId) {
        DataTable table = (DataTable)Faces.getViewRoot().findComponent("formTemplate:" + tableId);
        if (table != null) {
            // reset table state
            ValueExpression ve = table.getValueExpression("sortBy");
            if (ve != null) {
                table.setValueExpression("sortBy", null);
            }

            ve = table.getValueExpression("filterBy");
            if (ve != null) {
                table.setValueExpression("filterBy", null);
            }

            ve = table.getValueExpression("filteredValue");
            if (ve != null) {
                table.setValueExpression("filteredValue", null);
            }

            table.reset();
        }
    }
    
    public void assignHeight(){
        try {
            availableHeight = Integer.valueOf(getRequestParameterMap("hieght"));
        } catch(Exception e) {
            log.warn(e.getMessage(), e);
        }
    }
    
    public void clearDtatableFilters(String tableId) {
        DataTable table = (DataTable)Faces.getViewRoot().findComponent("formTemplate:" + tableId);
        if (table != null) {
            // reset table state
            ValueExpression ve = table.getValueExpression("sortBy");
            if (ve != null) {
                table.setValueExpression("sortBy", null);
            }

            ve = table.getValueExpression("filterBy");
            if (ve != null) {
                table.setValueExpression("filterBy", null);
            }

            ve = table.getValueExpression("filteredValue");
            if (ve != null) {
                table.setValueExpression("filteredValue", null);
            }

            table.reset();
        }
    }
}
