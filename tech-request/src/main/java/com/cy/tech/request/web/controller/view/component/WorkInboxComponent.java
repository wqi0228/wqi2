/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.component;

import com.cy.commons.vo.User;
import com.cy.tech.request.web.callback.UpdateCallBack;
import com.cy.tech.request.web.controller.logic.component.ReadLogicComponent;
import com.cy.tech.request.web.controller.logic.component.UserLogicComponents;
import com.cy.tech.request.web.controller.logic.component.WorkInboxLogicComponents;
import com.cy.tech.request.web.controller.view.vo.InBoxGroupItemVO;
import com.cy.tech.request.web.controller.view.vo.SentBackReadStatusVO;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
public class WorkInboxComponent implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4722731156114824754L;
    @Getter
    private List<InBoxGroupItemVO> sentBackups;
    @Getter
    private List<SentBackReadStatusVO> sentBackReadStatus;
    @Setter
    @Getter
    private List<SentBackReadStatusVO> selSentBackReadStatus;

    private Integer loginUserSid;

    private String requireSid;
    @Getter
    private boolean showSentBackup;
    @Setter
    private boolean canShowSendBackUp = false;
    @Getter
    private String needToReadPersonNumber;
    @Getter
    private String readedPersonNumber;
    @Getter
    private String cancelPersonNumber;
    @Getter
    private boolean depSentBackReadStatus;

    @SuppressWarnings("unused")
    private boolean showSendTab = false;

    private UpdateCallBack updateCallBack;

    private MessageCallBack messageCallBack;

    public WorkInboxComponent(Integer loginUserSid, UpdateCallBack updateCallBack,
            MessageCallBack messageCallBack) {
        this.loginUserSid = loginUserSid;
        this.messageCallBack = messageCallBack;
        this.updateCallBack = updateCallBack;
    }

    public void loadData(String requireSid) {
        this.requireSid = requireSid;
        loadData();
    }

    public void clickGroupToStatus(String inbox_group_sid) {
        this.selSentBackReadStatus = null;
        sentBackReadStatus = WorkInboxLogicComponents.getInstance().getSentBackReadStatusVOByInboxGroupSid(inbox_group_sid);
        List<SentBackReadStatusVO> deps = sentBackReadStatus.stream().filter(each -> each.isTypeDep())
                .collect(Collectors.toList());
        if (deps != null && !deps.isEmpty()) {
            depSentBackReadStatus = true;
        } else {
            depSentBackReadStatus = false;
        }
        List<User> readedUser = ReadLogicComponent.getInstance().getReadedUser(requireSid);
        Integer needToReadCount = 0;
        Integer readedCount = 0;
        Integer cancelCount = 0;
        for (SentBackReadStatusVO item : sentBackReadStatus) {
            try {
                if (depSentBackReadStatus) {
                    List<User> depUsers = UserLogicComponents.getInstance().findUserByDepSid(item.getDepSid());
                    needToReadCount = needToReadCount + depUsers.size();
                    if (Strings.isNullOrEmpty(item.getCancelTime())) {
                        List<User> readDepUsers = readedUser.stream().filter(each -> each.getPrimaryOrg() != null && each.getPrimaryOrg().getSid().equals(item.getDepSid()))
                                .collect(Collectors.toList());
                        readedCount = readedCount + readDepUsers.size();
                    } else {
                        cancelCount = cancelCount + depUsers.size();
                    }
                } else {
                    needToReadCount = needToReadCount + 1;
                    if (Strings.isNullOrEmpty(item.getCancelTime())) {
                        if (!Strings.isNullOrEmpty(item.getReadTime())) {
                            readedCount = readedCount + 1;
                        }
                    } else {
                        cancelCount = cancelCount + 1;
                    }
                }
            } catch (Exception e) {
                log.error("clickGroupToStatus", e);
            }
        }
        needToReadCount = needToReadCount - cancelCount;
        needToReadPersonNumber = String.valueOf(needToReadCount);
        readedPersonNumber = String.valueOf(readedCount);
        cancelPersonNumber = String.valueOf(cancelCount);
        DisplayController.getInstance().update("dlgIssueSentBackup_view");
        DisplayController.getInstance().update("dlgIssueSentBackup_view_dtInbox");
        DisplayController.getInstance().showPfWidgetVar("dlgIssueSentBackup");
    }

    public void cancelWorkInBox() {
        try {
            List<String> sids = Lists.newArrayList();
            selSentBackReadStatus.forEach(item -> {
                sids.add(item.getInbox_sid());
            });
            WorkInboxLogicComponents.getInstance().doCancelWorkInBox(sids, loginUserSid);
            updateCallBack.doUpdateData();
            DisplayController.getInstance().hidePfWidgetVar("dlgIssueSentBackup");
        } catch (Exception e) {
            messageCallBack.showMessage(e.getMessage());
            log.error("cancelWorkInBox", e);
        }
    }

    public void loadData() {
        sentBackups = WorkInboxLogicComponents.getInstance().getInBoxGroupItemBySendUserSid(requireSid, loginUserSid);
        if (canShowSendBackUp) {
            if (sentBackups == null || sentBackups.isEmpty()) {
                showSentBackup = false;
            } else {
                showSentBackup = true;
            }
        } else {
            showSentBackup = false;
        }

    }

}
