package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.List;

import com.google.common.base.Strings;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author aken_kao
 */
@Slf4j
public abstract class PaginationMBean<T> implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8293753654845571721L;

    /** 所有的需求單 */
    @Getter
    @Setter
    protected List<T> queryItems;

    @Getter
    @Setter
    protected T querySelection;

    /** 上下筆移動keeper */
    @Getter
    protected T queryKeeper;

    /**
     * 上下筆移動 require03_title_btn.xhtml require this method
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
    }

    /**
     * open window, 上一筆（分頁）
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.doPaginationAction(widgetVar, pageCount, querySelection);
    }

    /**
     * open window, 下一筆（分頁）
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.doPaginationAction(widgetVar, pageCount, querySelection);
    }

    /**
     * 重設定上下筆資訊
     */
    protected abstract void resetUpdownInfo();

    /**
     * 將選取的record(<tr>)給色
     */
    protected abstract void highlightReportTo(String widgetVar, String pageCount, T selectedResult);

    /**
     * 變更需求單內容
     */
    protected abstract void changeRequireContent(T view);

    /**
     * 另開window 需求明細時，初始上下頁按鈕
     */
    public void doPaginationAction(String widgetVar, String pageCount, T selectedResult) {
        this.highlightReportTo(widgetVar, pageCount, selectedResult);
        this.resetUpdownInfo();
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    protected int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }
    
}
