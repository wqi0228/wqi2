/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.enums.ReqSubType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search26QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.view.Search26BaseView;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.onpg.OnpgService;
import com.cy.tech.request.logic.service.othset.OthSetService;
import com.cy.tech.request.logic.service.pt.PtService;
import com.cy.tech.request.logic.service.send.test.SendTestService;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.search.helper.Search26ExcelHelper;
import com.cy.tech.request.web.controller.search.helper.Search26TreeHelper;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.searchheader.CommonHeaderMBean;
import com.cy.tech.request.web.controller.searchheader.Search26HeaderMBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.enums.ReadRecordType;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;

/**
 * 需求製作進度查詢
 *
 * @author jason_h
 */
@Controller
@Scope("view")
public class Search26MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8906729939695934076L;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private Search26HeaderMBean headerMBean;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private Search26QueryService queryService;
    @Autowired
    transient private Search26TreeHelper treeHelper;
    @Autowired
    transient private Search26ExcelHelper excelHelper;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private PtService ptService;
    @Autowired
    transient private SendTestService sendTestService;
    @Autowired
    transient private OnpgService opService;
    @Autowired
    transient private OthSetService osService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    private Require01MBean r01MBean;

    /** 需求製作進度樹快取(上下筆對焦用) */
    @Getter
    private Map<Integer, TreeNode> treeCache;
    /** 需求製作進度樹(顯示用) */
    @Getter
    @Setter
    private TreeNode queryRoot;
    /** 選擇節點 */
    @Getter
    @Setter
    private TreeNode querySelection;
    /** 上下筆移動keeper */
    @Getter
    private Search26BaseView queryKeeper;
    /** 切換模式 */
    @Getter
    private SwitchType switchType = SwitchType.CONTENT;
    /** 切換模式 - 全畫面狀態 */
    @Getter
    private SwitchType switchFullType = SwitchType.DETAIL;
    /** 在匯出的時候，某些內容需要隱藏 */
    @Getter
    private boolean hasDisplay = true;
    @Getter
    private final String dataTableId = "dtRequire";

    @PostConstruct
    public void init() {
        this.search();
    }

    public void search() {
        List<Search26BaseView> queryItems = this.findWithQuery();
        queryRoot = treeHelper.createRoot(queryItems);
        treeCache = treeHelper.createTreeCache(queryRoot);
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        headerMBean.clear();
        this.search();
    }

    private List<Search26BaseView> findWithQuery() {
        String requireNo = reqularUtils.getRequireNo(loginBean.getCompanyId(), commHeaderMBean.getFuzzyText());
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        this.buildReqRootCondition(requireNo, builder, parameters);
        this.buildPTCondition(requireNo, builder, parameters);
        this.buildWTCondition(requireNo, builder, parameters);
        this.buildOPCondition(requireNo, builder, parameters);
        this.buildOSCondition(requireNo, builder, parameters);
        builder.append("   ORDER BY req_create_dt DESC ,sub_type,sub_create_dt DESC ");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.REQUIRE_MAKE_PROGESS, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.REQUIRE_MAKE_PROGESS, SecurityFacade.getUserSid());

        List<Search26BaseView> results = queryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return results;
    }

    private void buildReqRootCondition(String requireNo, StringBuilder builder, Map<String, Object> param) {
        this.buildReqTemplateCondition(requireNo, builder, param, true);
        this.buildReqSidIsNotNull(builder);
        this.buildUnionAll(builder);
    }

    private void buildPTCondition(String requireNo, StringBuilder builder, Map<String, Object> param) {
        this.buildReqTemplateCondition(requireNo, builder, param, false);
        builder.append(" INNER JOIN ");
        builder.append(" ( SELECT ");
        builder.append("      pt_check_source_sid, ");
        builder.append("      ('").append(ReqSubType.PT).append("') AS sub_type, ");
        builder.append("      pt_check_sid          AS sub_sid, ");
        builder.append("      pt_check_theme        AS sub_theme, ");
        builder.append("      pt_notice_member      AS sub_notice, ");
        builder.append("      create_dt             AS sub_create_dt, ");
        builder.append("      pt_check_status       AS sub_status, ");
        builder.append("      pt_check_estimate_dt  AS sub_es_dt ");
        builder.append("   FROM work_pt_check ");
        builder.append(" ) AS pt ON pt.pt_check_source_sid = tr.require_sid ");
        this.buildReqSidIsNotNull(builder);
        this.buildUnionAll(builder);
    }

    private void buildWTCondition(String requireNo, StringBuilder builder, Map<String, Object> param) {
        this.buildReqTemplateCondition(requireNo, builder, param, false);
        builder.append(" INNER JOIN ");
        builder.append(" ( SELECT ");
        builder.append("      testinfo_source_sid, ");
        builder.append("      ('").append(ReqSubType.WT).append("') AS sub_type, ");
        builder.append("      testinfo_sid          AS sub_sid, ");
        builder.append("      testinfo_theme        AS sub_theme, ");
        builder.append("      testinfo_deps         AS sub_notice, ");
        builder.append("      create_dt             AS sub_create_dt, ");
        builder.append("      testinfo_status       AS sub_status, ");
        builder.append("      testinfo_estimate_dt  AS sub_es_dt ");
        builder.append("   FROM work_test_info ");
        builder.append(" ) AS wt ON wt.testinfo_source_sid = tr.require_sid ");
        this.buildReqSidIsNotNull(builder);
        this.buildUnionAll(builder);
    }

    private void buildOPCondition(String requireNo, StringBuilder builder, Map<String, Object> param) {
        this.buildReqTemplateCondition(requireNo, builder, param, false);
        builder.append(" INNER JOIN ");
        builder.append(" ( SELECT ");
        builder.append("      onpg_source_sid, ");
        builder.append("      ('").append(ReqSubType.OP).append("') AS sub_type, ");
        builder.append("      onpg_sid          AS sub_sid, ");
        builder.append("      onpg_theme        AS sub_theme, ");
        builder.append("      onpg_deps         AS sub_notice, ");
        builder.append("      create_dt         AS sub_create_dt, ");
        builder.append("      onpg_status       AS sub_status, ");
        builder.append("      onpg_estimate_dt  AS sub_es_dt ");
        builder.append("   FROM work_onpg ");
        builder.append(" ) AS op ON op.onpg_source_sid = tr.require_sid ");
        this.buildReqSidIsNotNull(builder);
        this.buildUnionAll(builder);
    }

    private void buildOSCondition(String requireNo, StringBuilder builder, Map<String, Object> param) {
        this.buildReqTemplateCondition(requireNo, builder, param, false);
        builder.append(" INNER JOIN ");
        builder.append(" ( SELECT ");
        builder.append("      require_sid, ");
        builder.append("      ('").append(ReqSubType.OS).append("') AS sub_type, ");
        builder.append("      os_sid         AS sub_sid, ");
        builder.append("      os_theme       AS sub_theme, ");
        builder.append("      os_notice_deps AS sub_notice, ");
        builder.append("      create_dt      AS sub_create_dt, ");
        builder.append("      os_status      AS sub_status, ");
        builder.append("      (NULL)         AS sub_es_dt ");
        builder.append("   FROM tr_os ");
        builder.append(" ) AS os ON os.require_sid = tr.require_sid ");
        this.buildReqSidIsNotNull(builder);
    }

    private void buildReqTemplateCondition(String requireNo, StringBuilder builder, Map<String, Object> param, boolean isRoot) {
        this.buildSelField(builder, isRoot);
        this.buildRequireCondition(requireNo, builder, param);
        this.buildRequireIndexDictionaryCondition(requireNo, builder, param);
        this.buildAssignInfoCondition(builder, param, isRoot);
        this.buildCategoryKeyMappingCondition(requireNo, builder, param);
    }

    private void buildSelField(StringBuilder builder, boolean isRoot) {
        builder.append(" SELECT DISTINCT ");
        builder.append(" tr.require_sid               AS req_sid, ");
        builder.append(" tr.require_no                AS req_no, ");
        builder.append(" tid.field_content            AS req_theme, ");
        builder.append(" tr.urgency                   AS req_urgency, ");
        builder.append(" tr.create_dt                 AS req_create_dt, ");
        builder.append(" tr.require_establish_dt      AS req_establish_dt, ");

        if (isRoot) {
            builder.append(" tr.require_status            AS req_status, ");
            builder.append(" tr.hope_dt                   AS req_hope_dt, ");
            builder.append(" tr.require_finish_dt         AS req_finish_dt, ");
            builder.append(" tr.close_dt                  AS req_close_dt, ");
            builder.append(" tr.dep_sid                   AS req_create_dep, ");
            builder.append(" tr.create_usr                AS req_create_usr, ");
            builder.append(" tr.inte_staff                AS req_inte_staff, ");
            builder.append(" ai.info                      AS req_assign_info, ");
            builder.append(" ('").append(ReqSubType.ROOT).append("') AS sub_type, ");
            builder.append(" (NULL) AS sub_sid, ");
            builder.append(" (NULL) AS sub_theme, ");
            builder.append(" (NULL) AS sub_notice, ");
            builder.append(" (NULL) AS sub_create_dt, ");
            builder.append(" (NULL) AS sub_status, ");
            builder.append(" (NULL) AS sub_es_dt ");
        } else {
            builder.append(" (NULL) AS req_status, ");
            builder.append(" (NULL) AS req_hope_dt, ");
            builder.append(" (NULL) AS req_finish_dt, ");
            builder.append(" (NULL) AS req_close_dt, ");
            builder.append(" (NULL) AS req_create_dep, ");
            builder.append(" (NULL) AS req_create_usr, ");
            builder.append(" (NULL) AS req_inte_staff, ");
            builder.append(" (NULL) AS req_assign_info, ");
            builder.append(" sub_type, ");
            builder.append(" sub_sid, ");
            builder.append(" sub_theme, ");
            builder.append(" sub_notice, ");
            builder.append(" sub_create_dt, ");
            builder.append(" sub_status, ");
            builder.append(" sub_es_dt ");
        }
        builder.append(" FROM ");
    }

    private void buildRequireCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("( SELECT ");
        builder.append("    tr.require_sid, ");
        builder.append("    tr.require_no, ");
        builder.append("    tr.mapping_sid, ");
        builder.append("    tr.urgency, ");
        builder.append("    tr.create_dt, ");
        builder.append("    tr.require_status, ");
        builder.append("    tr.require_finish_dt, ");
        builder.append("    tr.close_dt, ");
        builder.append("    tr.dep_sid, ");
        builder.append("    tr.create_usr, ");
        builder.append("    tr.inte_staff, ");
        builder.append("    tr.hope_dt, ");
        builder.append("    tr.require_establish_dt  ");

        builder.append("  FROM tr_require tr WHERE 1 ");
        // 需求單號
        if (!Strings.isNullOrEmpty(requireNo)) {
            builder.append(" AND tr.require_no = :requireNo");
            parameters.put("requireNo", requireNo);
        }
        // 立單區間
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getStartDate() != null && commHeaderMBean.getEndDate() != null) {
            builder.append(" AND tr.create_dt BETWEEN :startDt AND :endDt");
            parameters.put("startDt", helper.transStartDate(commHeaderMBean.getStartDate()));
            parameters.put("endDt", helper.transEndDate(commHeaderMBean.getEndDate()));
        }
        // 需求製作進度
        builder.append(" AND tr.require_status IN (:reqStatus)");
        if (Strings.isNullOrEmpty(requireNo)) {
            parameters.put("reqStatus", headerMBean.createQueryReqStatus());
        } else {
            parameters.put("reqStatus", headerMBean.createQueryAllReqStatus());
        }

        // 建單單位
        if (!headerMBean.getIsQueryAssignUnit()) {
            builder.append(" AND tr.dep_sid IN (:depSids)");
            parameters.put("depSids", commHeaderMBean.getRequireDepts() == null || commHeaderMBean.getRequireDepts().isEmpty()
                    ? "nerverFind"
                    : commHeaderMBean.getRequireDepts());
        }
        builder.append(") AS tr ");
    }

    private void buildRequireIndexDictionaryCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        // 模糊搜尋
        builder.append(" INNER JOIN ");
        builder.append(" ( SELECT ");
        builder.append("     tid.require_sid, ");
        builder.append("     tid.field_content ");
        builder.append("   FROM tr_index_dictionary tid WHERE 1 ");
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.isFuzzyQuery()) {
            String text = "%" + reqularUtils.replaceIllegalSqlLikeStr(commHeaderMBean.getFuzzyText()) + "%";
            builder.append(" AND (tid.require_sid IN "
                    + "            (SELECT DISTINCT tid1.require_sid FROM tr_index_dictionary tid1 "
                    + "              WHERE tid1.field_content LIKE :text)"
                    + "          OR tid.require_sid IN "
                    + "            (SELECT DISTINCT trace.require_sid FROM tr_require_trace trace"
                    + "              WHERE trace.require_trace_type = 'REQUIRE_INFO_MEMO' AND trace.require_trace_content LIKE :text)"
                    + "        ) ");
            parameters.put("text", text);
        }
        builder.append(" AND tid.field_name='主題') AS tid ON tr.require_sid=tid.require_sid ");
    }

    private void buildCategoryKeyMappingCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append(" INNER JOIN (SELECT ckm.key_sid  FROM tr_category_key_mapping ckm WHERE 1 ");
        // 需求類別
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getSelectBigCategory() != null) {
            builder.append(" AND ckm.big_category_sid = :big");
            parameters.put("big", commHeaderMBean.getSelectBigCategory().getSid());
        }
        builder.append(") AS ckm ON ckm.key_sid = tr.mapping_sid ");
    }

    private void buildAssignInfoCondition(StringBuilder builder, Map<String, Object> parameters, boolean isRoot) {
        if (headerMBean.getIsQueryAssignUnit()) {
            // 查詢index
            builder.append(" INNER JOIN (SELECT assi.require_sid FROM tr_assign_send_search_info assi ");
            builder.append("               WHERE assi.type = 0");
            builder.append("                 AND assi.dep_sid IN (:depSids) ");
            builder.append(" ) AS assi ON tr.require_sid = assi.require_sid ");
            parameters.put("depSids", commHeaderMBean.getRequireDepts() == null || commHeaderMBean.getRequireDepts().isEmpty()
                    ? "nerverFind"
                    : commHeaderMBean.getRequireDepts());
        }
        if (!isRoot) {
            return;
        }
        // 取得最新一筆分派記錄
        if (headerMBean.getIsQueryAssignUnit()) {
            builder.append(" INNER JOIN (");
        } else {
            builder.append(" LEFT JOIN (");
        }

        // builder.append(" (SELECT DISTINCT ai.require_sid,ai.info FROM
        // tr_assign_send_info ai JOIN ");
        // builder.append(" (SELECT MAX(ai.create_dt) as maxTime FROM
        // tr_assign_send_info ai GROUP BY ai.require_sid) ");
        // builder.append(" AS temp ON ai.create_dt IN (temp.maxTime) ");
        // builder.append(" WHERE ai.type = 0 ");
        // builder.append(" ORDER BY ai.create_dt DESC");

        // for 新資料結構, tr_assign_send_info 同一類別僅會有一筆, 不再需要取 maxTime
        builder.append("               SELECT DISTINCT ai.require_sid, ");
        builder.append("                                   ai.info ");
        builder.append("                   FROM   tr_assign_send_info ai ");
        builder.append("                   WHERE  ai.type = 0 ");
        builder.append("                   ORDER  BY ai.create_dt DESC ");

        if (headerMBean.getIsQueryAssignUnit()) {
            builder.append(" ) AS ai ON assi.require_sid = ai.require_sid ");
        } else {
            builder.append(" ) AS ai ON tr.require_sid = ai.require_sid ");
        }
    }

    private void buildReqSidIsNotNull(StringBuilder builder) {
        builder.append(" WHERE tr.require_sid IS NOT NULL ");
    }

    private void buildUnionAll(StringBuilder builder) {
        builder.append(" UNION ALL ");
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = (Search26BaseView) querySelection.getData();
            } else if (this.queryKeeper == null) {
                this.querySelection = treeCache.get(0);
                treeHelper.resetExpanded(this.querySelection, treeCache);
                this.queryKeeper = (Search26BaseView) querySelection.getData();
            }
            this.changeRequireContent(queryKeeper);
            this.moveScreenTab();
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Search26BaseView view) {
        view.setReadRecordType(ReadRecordType.HAS_READ);
        Require r = requireService.findByReqNo(view.getReqNo());
        switch (view.getSubType()) {
        case ROOT:
            loadManager.reloadReqForm(r, loginBean.getUser(), RequireBottomTabType.ASSIGN_SEND_INFO);
            break;
        case PT:
            loadManager.reloadReqForm(r, loginBean.getUser(), RequireBottomTabType.PT_CHECK_INFO);
            break;
        case WT:
            loadManager.reloadReqForm(r, loginBean.getUser(), RequireBottomTabType.SEND_TEST_INFO);
            break;
        case OP:
            loadManager.reloadReqForm(r, loginBean.getUser(), RequireBottomTabType.ONPG);
            break;
        case OS:
            loadManager.reloadReqForm(r, loginBean.getUser(), RequireBottomTabType.OTH_SET);
            break;
        }
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onNodeSelect(NodeSelectEvent event) {
        if (event == null || event.getTreeNode() == null || event.getTreeNode().getData() == null) {
            return;
        }
        this.querySelection = event.getTreeNode();
        treeHelper.resetExpanded(this.querySelection, treeCache);
        this.queryKeeper = (Search26BaseView) event.getTreeNode().getData();
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.changeRequireContent(this.queryKeeper);
            this.moveScreenTab();
        }
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Search26BaseView view) {
        this.querySelection = treeCache.get(view.getIndex());
        this.queryKeeper = (Search26BaseView) querySelection.getData();
        treeHelper.resetExpanded(querySelection, treeCache);
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
        this.moveScreenTab();
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = treeCache.get(queryKeeper.getIndex());
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = queryKeeper.getIndex();
        index += action;
        if (index < 0 || index >= this.treeCache.size()) {
            return;
        }
        this.querySelection = treeCache.get(index);
        treeHelper.resetExpanded(this.querySelection, treeCache);
        this.queryKeeper = (Search26BaseView) querySelection.getData();
        this.changeRequireContent(this.queryKeeper);
        this.moveScreenTab();
    }

    public void moveScreenTab() {
        List<String> sids;
        String pfTabWv;
        this.r01MBean.getBottomTabMBean().resetTabIdx(this.r01MBean);
        switch (queryKeeper.getSubType()) {
        case PT:
            r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.PT_CHECK_INFO);
            sids = ptService.findSidsBySourceSid(r01MBean.getRequire().getSid());
            pfTabWv = "pt_acc_panel_layer_zero";
            break;
        case WT:
            r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.SEND_TEST_INFO);
            sids = sendTestService.findSidsBySourceSid(r01MBean.getRequire().getSid());
            pfTabWv = "st_acc_panel_layer_zero";
            break;
        case OP:
            r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.ONPG);
            sids = opService.findSidsBySourceSid(r01MBean.getRequire().getSid());
            pfTabWv = "op_acc_panel_layer_zero";
            break;
        case OS:
            r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.OTH_SET);
            sids = osService.findSidsByRequire(r01MBean.getRequire());
            pfTabWv = "os_acc_panel_layer_zero";
            break;
        default:
            return;
        }
        this.doScrollBottomTabToSid(queryKeeper.getSubSid(), sids, pfTabWv);
    }

    private void doScrollBottomTabToSid(String indicateSid, List<String> sids, String pfTabWv) {
        if (Strings.isNullOrEmpty(pfTabWv)) {
            return;
        }
        if (sids.indexOf(indicateSid) != -1) {
            display.execute("PF('" + pfTabWv + "').select(" + sids.indexOf(indicateSid) + ");");
            for (int i = 0; i < sids.size(); i++) {
                if (i != sids.indexOf(indicateSid)) {
                    display.execute("PF('" + pfTabWv + "').unselect(" + i + ");");
                }
            }
        }
    }

    public boolean disableUp() {
        if (queryKeeper == null || queryKeeper.getIndex() == null) {
            return true;
        }
        return queryKeeper.getIndex() == 0;
    }

    public boolean disableDown() {
        if (queryKeeper == null || queryKeeper.getIndex() == null) {
            return true;
        }
        return queryKeeper.getIndex() == treeCache.size() - 1;
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        excelHelper.exportExcel(document, commHeaderMBean.getStartDate(), commHeaderMBean.getEndDate(), commHeaderMBean.getReportType());
        hasDisplay = true;
    }

    /**
     * 隱藏部分column裡的內容
     */
    public void hideColumnContent() {
        hasDisplay = false;
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search26BaseView to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            display.update("doSearchData_id");
            display.update("headerTitle");
            display.update("searchBody");
            return true;
        }
        display.update("searchBody");
        return false;
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Search26BaseView to) {
        querySelection = treeCache.get(to.getIndex());
        queryKeeper = to;
        treeHelper.resetExpanded(querySelection, treeCache);
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getReqNo());
        upDownBean.resetUpDown(queryKeeper.getIndex(), treeCache.size());
        switch (queryKeeper.getSubType()) {
        case ROOT:
            this.callToRootTab();
            break;
        case PT:
            if (Strings.isNullOrEmpty(queryKeeper.getSubSid())) {
                callToRootTab();
                return;
            }
            upDownBean.resetTabInfo(RequireBottomTabType.PT_CHECK_INFO, queryKeeper.getSubSid());
            break;
        case WT:
            if (Strings.isNullOrEmpty(queryKeeper.getSubSid())) {
                callToRootTab();
                return;
            }
            upDownBean.resetTabInfo(RequireBottomTabType.SEND_TEST_INFO, queryKeeper.getSubSid());
            break;
        case OP:
            if (Strings.isNullOrEmpty(queryKeeper.getSubSid())) {
                callToRootTab();
                return;
            }
            upDownBean.resetTabInfo(RequireBottomTabType.ONPG, queryKeeper.getSubSid());
            break;
        case OS:
            if (Strings.isNullOrEmpty(queryKeeper.getSubSid())) {
                callToRootTab();
                return;
            }
            upDownBean.resetTabInfo(RequireBottomTabType.OTH_SET, queryKeeper.getSubSid());
            break;
        }
    }

    private void callToRootTab() {
        upDownBean.resetTabInfo(RequireBottomTabType.ASSIGN_SEND_INFO, queryKeeper.getReqSid());
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = querySelection != null ? ((Search26BaseView) querySelection.getData()).getIndex() : queryKeeper.getIndex();
        if (index > 0) {
            index--;
            querySelection = treeCache.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = querySelection != null ? ((Search26BaseView) querySelection.getData()).getIndex() : queryKeeper.getIndex();
        if (treeCache.size() > index + 1) {
            index++;
            querySelection = treeCache.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = (Search26BaseView) querySelection.getData();
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    public void expandedAll() {
        treeHelper.expandedAll(treeCache);
    }

    public void foldAll() {
        treeHelper.foldAll(treeCache);
    }

}
