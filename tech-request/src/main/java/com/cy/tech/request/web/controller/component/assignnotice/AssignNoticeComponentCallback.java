package com.cy.tech.request.web.controller.component.assignnotice;

import java.io.Serializable;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.work.common.exception.SystemDevelopException;

public class AssignNoticeComponentCallback implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1697972445125231222L;

    /**
     * 準備分派單位已選擇項目
     * 
     * @return
     * @throws SystemDevelopException 
     */
    public List<Integer> prepareAssignSelectedDeps() throws SystemDevelopException{
        throw new SystemDevelopException("AssignNoticeComponentCallback: 未實做 prepareAssignSelectedDeps");
    }
    
    /**
     * 準備通知單位已選擇項目
     * 
     * @return
     */
    public List<Integer> prepareNoticeSelectedDeps() throws SystemDevelopException {
        throw new SystemDevelopException("AssignNoticeComponentCallback: 未實做 prepareNoticeSelectedDeps");
    }
    
    /**
     * 準備檢查確認時，預設分派單位
     * @param checkItemTypeName 選擇的檢查項目
     * @return 預設分派單位
     * @throws SystemDevelopException 未實做時拋出
     */
    public List<Integer> prepareCheckConfirmDefaultAssignDeps(String checkItemTypeName) throws SystemDevelopException{
        throw new SystemDevelopException("AssignNoticeComponentCallback: 未實做 prepareCheckConfirmDefaultAssignDeps");
    }
    
    /**
     * 準備檢查確認時，預設通知單位
     * @param checkItemTypeName 選擇的檢查項目
     * @return 預設分派單位
     * @throws SystemDevelopException 未實做時拋出
     */
    public List<Integer> prepareCheckConfirmDefaultNoticeDeps(String checkItemTypeName) throws SystemDevelopException{
        throw new SystemDevelopException("AssignNoticeComponentCallback: 未實做 prepareCheckConfirmDefaultNoticeDeps");
    }
    
    /**
     * 準備鎖定的單位 sid
     * @return
     * @throws Exception
     */
    public List<Integer> prepareAssignDisableDepSids() throws SystemDevelopException {
        throw new SystemDevelopException("AssignNoticeComponentCallback: 未實做 prepareAssignDisableDepSids");
    }

    /**
     * 點選確認
     * 
     * @return
     * @throws SystemDevelopException 
     */
    public void btnConfirm(ActionEvent event, List<Integer> assignDepSids, List<Integer> noticeDepSids) throws SystemDevelopException{
        throw new SystemDevelopException("AssignNoticeComponentCallback: 未實做 btnConfirm");
    }
    
    /**
     * 是否為『檢查確認』功能使用
     */
    public boolean isCheckConfirm(){
        return false;
    }
    
    /**
     * 準備檢查項目
     */
    public List<SelectItem> prepareCheckItems(){
        throw new SystemDevelopException("AssignNoticeComponentCallback: 未實做 prepareCheckItems");
    }
    
    /**
     * @param checkItemName
     */
    public void onCheckItemSelected(RequireCheckItemType checkItemType) {
        throw new SystemDevelopException("AssignNoticeComponentCallback: 未實做 onCheckItemSelected");
    }
    
}
