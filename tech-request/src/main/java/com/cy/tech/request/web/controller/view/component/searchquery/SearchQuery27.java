/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.component.searchquery;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.enumerate.DateType;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.service.CategorySettingService;
import com.cy.tech.request.logic.vo.CustomerTo;
import com.cy.tech.request.logic.vo.query.search.SearchQuery;
import com.cy.tech.request.vo.category.BasicDataBigCategory;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.Search27QueryColumn;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterArrayStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterDetailVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterVO;
import com.cy.tech.request.web.listener.CategoryTreeCallBack;
import com.cy.tech.request.web.logic.helper.Search27LogicHelper;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.LocalDate;

/**
 * 查詢物件
 *
 * @author kasim
 */
@Slf4j
@Data
@EqualsAndHashCode(callSuper = true)
public class SearchQuery27 extends SearchQuery implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2814269998491535553L;
    private Org comp;
    /** 是否GM */
    private boolean hasGm = false;
    /** GM單位 */
    private List<Integer> gmOrgs;
    /** GM人員者 */
    private List<Integer> gmUsers;

    /** 需求類別 */
    private BasicDataBigCategory selectBigCategory;

    /** 需求來源 All | 立 | 分 | 轉 */
    private String reqSource;

    /** 單位挑選 */
    private List<String> requireDepts = Lists.newArrayList();

    /** 是否閱讀 */
    private ReadRecordType selectReadRecordType;

    /** 類別組合 */
    private List<String> bigDataCateSids = Lists.newArrayList();
    private List<String> middleDataCateSids = Lists.newArrayList();
    private List<String> smallDataCateSids = Lists.newArrayList();

    /** 分類 */
    private List<String> otherCategorySids = Lists.newArrayList();

    /** 立單區間(起) */
    private Date startDate;

    /** 立單區間(訖) */
    private Date endDate;

    /** 主題 */
    private String theme;

    /** 內容 */
    private String content;

    /** 需求人員 */
    private String trCreatedUserName;

    /** 模糊搜尋 */
    private String fuzzyText;

    /** 廳主 */
    private List<CustomerTo> customers;

    /** 是否廳主分頁 */
    private Boolean isCustomerPage = Boolean.FALSE;

    /** 廳主(分頁條件) */
    private CustomerTo customerPage;

    /** 異動區間(起) */
    private Date startUpdatedDate;

    /** 異動區間(訖) */
    private Date endUpdatedDate;

    /** 緊急度 */
    private List<String> urgencyList;

    /** 需求單號 */
    private String requireNo;

    /** 時間切換的index */
    private int dateTypeIndex;

    /** 是否包含內部需求 */
    private Boolean isContainsInternalReq = Boolean.TRUE;

    private CategoryTreeCallBack categoryTreeCallBack;

    private CategorySettingService categorySettingService;

    private Search27LogicHelper search27LogicHelper;

    private Search27QueryColumn dateTimeType = Search27QueryColumn.CREATE_DATE;

    /**
     * 
     */
    private String selectedDateRangeTypeStr = DateType.TODAY.name();

    /**
     * 檢查人員暱稱模糊查詢
     */
    private String searchCheckUserName;

    /**
     * 建構式
     *
     * @param dep
     * @param user
     * @param hasGm
     * @param reportType
     * @param categoryTreeCallBack
     * @param categorySettingService
     * @param search27LogicHelper
     */
    public SearchQuery27(
            Org comp,
            Org dep,
            User user,
            boolean hasGm,
            ReportType reportType,
            CategoryTreeCallBack categoryTreeCallBack,
            CategorySettingService categorySettingService,
            Search27LogicHelper search27LogicHelper) {

        this.comp = comp;
        this.dep = dep;
        this.user = user;
        this.hasGm = hasGm;
        this.reportType = reportType;
        this.categoryTreeCallBack = categoryTreeCallBack;
        this.categorySettingService = categorySettingService;
        this.search27LogicHelper = search27LogicHelper;
        this.init();
        if (hasGm) {
            gmOrgs = search27LogicHelper.getGmOrgs(dep);
            gmUsers = search27LogicHelper.getGmUsers(gmOrgs);
        }
    }

    /**
     * 初始化
     */
    public void init() {
        this.initSetting(Lists.newArrayList());
        this.customers = Lists.newArrayList();
        this.isCustomerPage = Boolean.FALSE;
        this.customerPage = null;
        this.clearAdvance();
    }

    /**
     * 初始化共用設定
     *
     * @param smallCateSids
     */
    private void initSetting(List<String> smallCateSids) {

        // 共用查詢條件初始化
        this.publicConditionInit();

        this.dateTypeIndex = DateType.TODAY.ordinal();
        this.bigDataCateSids = Lists.newArrayList();
        this.middleDataCateSids = Lists.newArrayList();
        this.smallDataCateSids = smallCateSids;
        this.selectBigCategory = null;
        this.fuzzyText = null;
        this.trCreatedUserName = null;
        this.theme = null;
        this.content = null;
        if (hasGm) {
            reqSource = "All";
        } else {
            this.reqSource = null;
        }

        this.selectReadRecordType = null;
        this.requireDepts = Lists.newArrayList();
        this.startDate = new Date();
        this.endDate = new Date();
        this.otherCategorySids = Lists.newArrayList();
        this.initDefaultDepts();
        this.isContainsInternalReq = Boolean.TRUE;
        this.selectedDateRangeTypeStr = DateType.TODAY.name();
    }

    /**
     * 初始化 單位
     */
    public void initDefaultDepts() {
        if (hasGm && "All".equals(reqSource)) {
            this.requireDepts = search27LogicHelper.findChildOrgs(comp);
            return;
        }
        this.requireDepts = search27LogicHelper.initDefaultDepts(dep.getSid());
    }

    /**
     * 清除進階選項
     */
    public void clearAdvance() {
        startUpdatedDate = null;
        endUpdatedDate = null;
        urgencyList = null;
        requireNo = null;
    }

    /**
     * 載入自訂搜尋條件設定
     *
     * @param smallCateSids
     */
    public void loadDefaultSetting(List<String> smallCateSids) {
        this.initSetting(smallCateSids);
        try {
            List<ReportCustomFilterVO> reportCustomFilterVOs = ReportCustomFilterLogicComponent.getInstance()
                    .getReportCustomFilter(Search27QueryColumn.Search27Query, user.getSid());
            if (reportCustomFilterVOs == null || reportCustomFilterVOs.isEmpty()) {
                return;
            }
            reportCustomFilterVOs.get(0).getReportCustomFilterDetailVOs().forEach(item -> {
                if (item instanceof ReportCustomFilterStringVO) {
                    if (Search27QueryColumn.DemandType.equals(item.getSearchReportCustomEnum())) {
                        this.settingDemandType(item);
                    } else if (Search27QueryColumn.DemandSource.equals(item.getSearchReportCustomEnum())) {
                        this.settingDemandSource(item);
                    } else if (Search27QueryColumn.DemandProcess.equals(item.getSearchReportCustomEnum())) {
                        this.settingDemandProcess(item);
                    } else if (Search27QueryColumn.ReadStatus.equals(item.getSearchReportCustomEnum())) {
                        this.settingReadStatus(item);
                    } else if (Search27QueryColumn.DemandPerson.equals(item.getSearchReportCustomEnum())) {
                        this.settingDemandPerson(item);
                    } else if (Search27QueryColumn.DateIndex.equals(item.getSearchReportCustomEnum())) {
                        this.settingDateIndex(item);
                    } else if (Search27QueryColumn.SearchText.equals(item.getSearchReportCustomEnum())) {
                        this.settingFuzzyText(item);
                    } else if (Search27QueryColumn.DateTimeType.equals(item.getSearchReportCustomEnum())) {
                        ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) item;
                        this.dateTimeType = Search27QueryColumn.valueOf(reportCustomFilterStringVO.getValue());
                    }
                } else if (item instanceof ReportCustomFilterArrayStringVO) {
                    if (Search27QueryColumn.WaitReadReson.equals(item.getSearchReportCustomEnum())) {
                        this.settingWaitReadReson(item);
                    } else if (Search27QueryColumn.CategoryCombo.equals(item.getSearchReportCustomEnum())) {
                        this.settingCategoryCombo(item);
                    }
                }
            });
        } catch (Exception e) {
            log.error("loadDefaultSetting ERROR", e);
        }
    }

    /**
     * 變更預設選項 - 需求類別
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandType(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                this.selectBigCategory = categorySettingService.findBigBySid(reportCustomFilterStringVO.getValue());
            }
        } catch (Exception e) {
            log.error("settingDemandType", e);
        }
    }

    /**
     * 變更預設選項 - 需求來源(單位挑選)
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandSource(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            this.reqSource = reportCustomFilterStringVO.getValue();
            if (Strings.isNullOrEmpty(reqSource) || reqSource.equals("立")) {
                requireDepts.addAll(search27LogicHelper.initDefaultDepts(dep.getSid()));
            } else {
                requireDepts.addAll(search27LogicHelper.initAllDepts());
            }
        } catch (Exception e) {
            log.error("settingDemandSource", e);
        }
    }

    /**
     * 變更預設選項 - 製作進度
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandProcess(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                this.selectRequireStatusType = RequireStatusType.valueOf(reportCustomFilterStringVO.getValue());
            }
        } catch (Exception e) {
            log.error("settingDemandSource", e);
        }
    }

    /**
     * 變更預設選項 - 需求人員
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandPerson(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            this.trCreatedUserName = reportCustomFilterStringVO.getValue();
        } catch (Exception e) {
            log.error("trCreatedUserName", e);
        }
    }

    /**
     * 變更預設選項 - 是否閱讀
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingReadStatus(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                this.selectReadRecordType = ReadRecordType.valueOf(reportCustomFilterStringVO.getValue());
            }
        } catch (Exception e) {
            log.error("settingDemandSource", e);
        }
    }

    /**
     * 變更預設選項 - 時間切換的index(立單區間)
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDateIndex(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                this.dateTypeIndex = Integer.valueOf(reportCustomFilterStringVO.getValue());
                this.selectedDateRangeTypeStr=DateType.values()[dateTypeIndex].name();
                if (DateType.PREVIOUS_MONTH.ordinal() == this.dateTypeIndex) {
                    getDateInterval(DateType.PREVIOUS_MONTH.name());
                } else if (DateType.THIS_MONTH.ordinal() == this.dateTypeIndex) {
                    getDateInterval(DateType.THIS_MONTH.name());
                } else if (DateType.NEXT_MONTH.ordinal() == this.dateTypeIndex) {
                    getDateInterval(DateType.NEXT_MONTH.name());
                } else if (DateType.TODAY.ordinal() == this.dateTypeIndex) {
                    getDateInterval(DateType.TODAY.name());
                } else if (DateType.NULL.ordinal() == this.dateTypeIndex) {
                    getDateInterval(DateType.NULL.name());
                }
            }
        } catch (Exception e) {
            log.error("settingFuzzyText", e);

        }
    }

    /**
     * 變更預設選項 - 模糊搜尋
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingFuzzyText(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            this.fuzzyText = reportCustomFilterStringVO.getValue();
        } catch (Exception e) {
            log.error("settingFuzzyText", e);
        }
    }

    /**
     * 變更預設選項 - 待閱原因
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingWaitReadReson(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            this.selectReqToBeReadType.clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                this.selectReqToBeReadType.addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingDepartment", e);
        }
    }

    /**
     * 變更預設選項 - 類別組合
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingCategoryCombo(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            this.smallDataCateSids.clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                this.smallDataCateSids.addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
            categoryTreeCallBack.loadSelCate(smallDataCateSids);
        } catch (Exception e) {
            log.error("settingCategoryCombo", e);
        }
    }

    /**
     * 變更第２分類查詢選項
     *
     * @param categorySids
     */
    public void changeByOtherCategory(List<String> categorySids) {
        this.selectBigCategory = null;
        this.smallDataCateSids = categorySids;
    }

    public void changeDateRange() {
        this.getDateInterval(this.selectedDateRangeTypeStr);
    }

    /**
     * 取得日期區間
     *
     * @param dateType
     */
    public void getDateInterval(String dateTypeStr) {

        DateType dateType = DateType.safeValueOf(dateTypeStr);
        if (dateType == null) {
            return;
        }

        dateTypeIndex = dateType.ordinal();

        switch (dateType) {
        case PREVIOUS_MONTH:
            LocalDate lastDate = new LocalDate(startDate).minusMonths(1);
            startDate = lastDate.dayOfMonth().withMinimumValue().toDate();
            endDate = lastDate.dayOfMonth().withMaximumValue().toDate();
            break;

        case THIS_MONTH:
            startDate = new LocalDate().dayOfMonth().withMinimumValue().toDate();
            endDate = new LocalDate().dayOfMonth().withMaximumValue().toDate();
            break;

        case NEXT_MONTH:
            if (startDate == null) {
                startDate = new Date();
            }
            LocalDate nextDate = new LocalDate(startDate).plusMonths(1);
            startDate = nextDate.dayOfMonth().withMinimumValue().toDate();
            endDate = nextDate.dayOfMonth().withMaximumValue().toDate();
            break;

        case TODAY:
            startDate = new Date();
            endDate = new Date();
            break;

        case NULL:
            startDate = null;
            endDate = null;
            break;

        default:
            break;
        }

    }

    /**
     * 是否有選擇類別資料
     *
     * @return
     */
    public boolean isSelCateSids() {
        return !bigDataCateSids.isEmpty() || !middleDataCateSids.isEmpty() || !smallDataCateSids.isEmpty();
    }

    /**
     * 移除類別選項
     *
     * @param removeCateSids
     */
    public void removeCateSids(List<String> removeCateSids) {
        if (WkStringUtils.notEmpty(removeCateSids)) {
            bigDataCateSids.removeAll(removeCateSids);
            middleDataCateSids.removeAll(removeCateSids);
            smallDataCateSids.removeAll(removeCateSids);
        }
    }

    /**
     * 取得挑選單位中純GM單位(如果沒有則回傳所有GM)
     */
    public List<Integer> getOnlyGmOrgs() {
        if (requireDepts == null || requireDepts.isEmpty()) {
            return gmOrgs;
        }
        return gmOrgs.stream()
                .filter(each -> requireDepts.contains(String.valueOf(each)))
                .collect(Collectors.toList());
    }

    /**
     * 取得挑選單位中NOT GM單位
     */
    public List<Integer> getOnlyNotGmOrgs() {
        if (requireDepts == null || requireDepts.isEmpty()) {
            return Lists.newArrayList();
        }
        return requireDepts.stream()
                .map(each -> Integer.valueOf(each))
                .filter(each -> !gmOrgs.contains(each))
                .collect(Collectors.toList());
    }

}
