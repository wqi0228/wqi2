/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.setting;

import com.cy.tech.request.logic.service.customer.ReqWorkCustomerHelper;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import java.io.Serializable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 客戶資料維護
 *
 * @author kasim
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope("view")
public class Setting08MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8467497845673827781L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private ReqWorkCustomerHelper customerService;
    @Autowired
    transient private DisplayController displayController;

    @Getter
    private String msg;

    /**
     * 檢核轉入來源
     */
    public void btnCheck() {
        msg = customerService.checkConnection();
        log.info(msg);
        displayController.showPfWidgetVar("dlgTransferMsg");
    }

    /**
     * 執行手動轉入作業
     */
    public void btnTransfer() {
        msg = customerService.transferCustomerSource(loginBean.getUser(), 7);
        log.info(msg);
        displayController.showPfWidgetVar("dlgTransferMsg");
    }

}
