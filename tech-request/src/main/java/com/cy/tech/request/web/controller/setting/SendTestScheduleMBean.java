package com.cy.tech.request.web.controller.setting;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.send.test.SendTestScheduleService;
import com.cy.tech.request.logic.utils.DateUtils;
import com.cy.tech.request.vo.setting.WorkTestSchedule;
import com.cy.tech.request.vo.value.to.WorkTestScheduleVO;
import com.cy.tech.request.web.controller.to.ColorCodeVO;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.tech.request.web.primefaces.custom.WorkScheduleModel;
import com.cy.work.backend.logic.WorkBackendParamService;
import com.cy.work.backend.vo.enums.WkBackendParam;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author aken_kao
 */
@Slf4j
@Controller
@Scope("view")
public class SendTestScheduleMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6589626925557340410L;
    @Autowired
    private SendTestScheduleService sendTestScheduleService;
	@Autowired
	private transient WorkBackendParamService workBackendParamService;
    @Autowired
    private DisplayController displayController;
    
    @Getter
    @Setter
    private WorkScheduleModel eventModel;
    @Getter
    @Setter
    private WorkTestScheduleVO scheduleVO;
    @Getter
    private List<ColorCodeVO> colors = Lists.newArrayList();
    /** 切換月份 */
    @Getter
    private Date switchDateMonth;
    
    @PostConstruct
    public void init() {
        eventModel = new WorkScheduleModel();
        for(String color : this.workBackendParamService.findStringsByKeyword(WkBackendParam.COLOR_CODE)){
            String[] arr = color.split(":");
            ColorCodeVO colorCodeVO = new ColorCodeVO();
            colorCodeVO.setColorCode(arr[0]);
            colorCodeVO.setDescription(arr[1]);
            colors.add(colorCodeVO);
        }
    }

    
    public void searchSchedule(Date startDate, Date endDate, boolean skipTitle) {
        eventModel.clear();
        List<WorkTestScheduleVO> eventList = sendTestScheduleService.findByDateRange(startDate, endDate);
        for (WorkTestScheduleVO vo : eventList) {
            Date date = vo.getStartDate();
            while(date.before(vo.getEndDate())) {
                DefaultScheduleEvent event = new DefaultScheduleEvent();
                event.setTitle(vo.getScheduleName());
                event.setStartDate(date);
                if(skipTitle){
                    Date today = DateUtils.convertToOriginOfDay(new Date());
                    if(date.before(today)){
                        date = new DateTime(date).plusDays(1).toDate();
                        continue;
                    }
                }
                event.setDescription(vo.getScheduleName());
                event.setEndDate(DateUtils.convertToEndOfDay(date));
                event.setStyleClass(String.format("c_%s", vo.getColorCode()));
                event.setId(vo.getEventId());
                eventModel.addEvent(event);
                
                date = new DateTime(date).plusDays(1).toDate();
            }
        }
    }

    public void searchAfterSchedule(){
        searchSchedule(null, DateUtils.convertToOriginOfDay(new Date()), true);
    }
    
    /**
     * saveOrUpdate Event
     */
    public void addOrUpdateEvent() {
        DefaultScheduleEvent event = new DefaultScheduleEvent();
        if(scheduleVO.getEventId() != null){
            event = (DefaultScheduleEvent)eventModel.getEvent(scheduleVO.getEventId());
        }
        event.setTitle(scheduleVO.getScheduleName());
        event.setStartDate(scheduleVO.getStartDate());
        event.setEndDate(DateUtils.convertToEndOfDay(scheduleVO.getEndDate()));
        event.setStyleClass(String.format("c_%s", scheduleVO.getColorCode()));
        
        eventModel.addOrUpdateEvent(event);
        scheduleVO.setEventId(event.getId());
    }

    public void saveOrUpdate() {
        try {
            validate();
            addOrUpdateEvent();
            sendTestScheduleService.saveOrUpdate(scheduleVO, SecurityFacade.getUserSid());
            displayController.hidePfWidgetVar("eventDialog");
            //新增事件後切換月份
            switchDateMonth = scheduleVO.getStartDate();
        } catch (Exception e) {
            MessagesUtils.showError(e.getMessage());
        } finally {
            searchSchedule(null, null, false); //reload
        }
    }

    /**
     * 驗證
     * @throws Exception
     */
    public void validate() throws Exception {
        if (scheduleVO.getStartDate() == null || scheduleVO.getEndDate() == null) {
            throw new IllegalArgumentException("請選擇起訖日期！");
        }
        Preconditions.checkArgument(StringUtils.isNotBlank(scheduleVO.getScheduleName()), "請輸入事件名稱！");
        Preconditions.checkArgument(StringUtils.isNotBlank(scheduleVO.getColorCode()), "請選擇任一顏色！");
    }

    /**
     * 刪除
     * @param eventId
     */
    public void delete(String eventId) {
        DefaultScheduleEvent event = (DefaultScheduleEvent) eventModel.getEvent(eventId);
        try {
            eventModel.deleteEvent(event);
            sendTestScheduleService.inactiveData(eventId, SecurityFacade.getUserSid());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            searchSchedule(null, null, false); //reload
        }
    }

    
    public void onDateSelect(SelectEvent selectEvent) {
        scheduleVO = new WorkTestScheduleVO();
        Date selectedDate = (Date) selectEvent.getObject();
        scheduleVO.setStartDate(selectedDate);
        scheduleVO.setEndDate(selectedDate);
    }

    public void onEventSelect(SelectEvent selectEvent) {
        DefaultScheduleEvent event = (DefaultScheduleEvent) selectEvent.getObject();
        WorkTestSchedule schedule = sendTestScheduleService.findByEventId(event.getId());
        scheduleVO = new WorkTestScheduleVO();
        scheduleVO.setScheduleName(schedule.getScheduleName());
        scheduleVO.setStartDate(schedule.getStartDate());
        scheduleVO.setEndDate(schedule.getEndDate());
        scheduleVO.setColorCode(schedule.getColorCode());
        scheduleVO.setEventId(schedule.getEventId());
        scheduleVO.setIsFull(schedule.isFull() ? "Y" : "N");
    }
}
