package com.cy.tech.request.web.schedule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.cy.tech.request.logic.service.send.test.SendTestService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class Scheduler {

	@Autowired
	private SendTestService sendTestService;

	boolean isFirst_autoConvertStatus = true;
	/**
	 * 間隔時間 5 分鐘
	 */
	private final long autoConvertStatusDelay = (5 * 60 * 1000);

	/**
	 * 送測單，自動更新狀態為測試中
	 */
	@Scheduled(fixedDelay = autoConvertStatusDelay)
	public void autoConvertStatus() {

		try {
			sendTestService.systemAutoUpdateTestingStatus();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		// 為避免遺忘此排程，故第一次運作時，印出執行訊息
		if (isFirst_autoConvertStatus) {
			log.info(String.format(
			        "【排程】送測單，自動更新狀態為測試中, 執行間隔時間=%s 分, ",
			        (this.autoConvertStatusDelay / 1000 / 60 )));
			isFirst_autoConvertStatus = false;
		}
	}
}
