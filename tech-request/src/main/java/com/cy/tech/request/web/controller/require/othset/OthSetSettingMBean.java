/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require.othset;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.config.exception.OthSetEditExceptions;
import com.cy.tech.request.logic.helper.systemnotify.SysNotifyHelper;
import com.cy.tech.request.logic.service.othset.OthSetService;
import com.cy.tech.request.logic.service.othset.OthSetThemeService;
import com.cy.tech.request.logic.vo.OthSetTo;
import com.cy.tech.request.vo.enums.SubNoticeType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.othset.OthSet;
import com.cy.tech.request.vo.require.othset.OthSetTheme;
import com.cy.tech.request.vo.require.othset.OthSetThemeDetail;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerCallback;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerCallbackDefaultImplType;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerComponent;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerConfig;
import com.cy.tech.request.web.controller.component.mipker.helper.MultItemPickerByOrgHelper;
import com.cy.tech.request.web.controller.component.reqconfirm.ReqConfirmClientHelper;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.helper.AssignDepHelper;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.values.OrganizationTreeMBean;
import com.cy.tech.request.web.controller.view.vo.OthSetSettingVO;
import com.cy.tech.request.web.pf.utils.ConfirmCallbackDialogController;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonContentUtils;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 其它資訊設定 控制<BR/>
 * 對應 othset_setting_dialog.xhtml
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope("view")
public class OthSetSettingMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7369534930402789365L;
    // @Autowired
    // transient private LoginBean loginBean;
    @Autowired
    transient private OthSetService osService;
    @Autowired
    transient private OthSetThemeService osThemeService;
    @Autowired
    transient private DisplayController display;
    @Getter
    @Autowired
    private OrganizationTreeMBean orgTreeMBean;
    @Autowired
    transient private AssignDepHelper assignDepHelper;
    @Autowired
    transient private DisplayController displayController;
    @Autowired
    transient private SysNotifyHelper sysNotifyHelper;
    @Autowired
    transient private MultItemPickerByOrgHelper multItemPickerByOrgHelper;
    @Autowired
    transient private ConfirmCallbackDialogController confirmCallbackDialogController;

    /** 編輯用主檔 */
    @Getter
    @Setter
    private OthSet editOthset;
    /** 備份資料 - 用來檢核 */
    private OthSetTo tempTo;

    /** 編輯狀態控制 */
    @Getter
    private OsEditType editType;

    private enum OsEditType {
        /** 檢視 */
        IS_VIEW,
        /** 新增 */
        IS_NEW,
        /** 編輯 */
        IS_EDIT,;
    }

    /** 主題 */
    @Getter
    private List<SelectItem> themes;

    /**
     * 歸屬單位選項
     */
    @Getter
    private List<SelectItem> ownerDepSelectItems;
    /**
     * 選擇的歸屬單位
     */
    @Getter
    @Setter
    private WkItem selectOwnerDepItem;

    /**
     * 物件初始化
     */
    @PostConstruct
    public void init() {
        this.editType = OsEditType.IS_VIEW;
        editOthset = new OthSet();
    }

    /**
     * 開啟新增對話框 開啟Dialog時觸發 require_title_btn.xhtml
     * 
     * @param r01MBean
     */
    public void openDialogByNewAdd(Require01MBean r01MBean) {
        // 取得主單 bean
        if (r01MBean == null) {
            MessagesUtils.showWarn("資訊已逾期，請重新整理畫面");
            return;
        }
        Require require = r01MBean.getRequire();

        try {
            // 元件變數初始化
            this.init();
            // 主題下拉選單選項
            this.themes = prepareThemeItems();
            // 建立編輯主檔
            editOthset = osService.createEmptyOthSet(
                    WkUserCache.getInstance().findBySid(SecurityFacade.getUserSid()),
                    require);

            // 準備單據歸屬單位選單資料
            List<WkItem> ownerDepItems = assignDepHelper.prepareLoginUserCaseOwnerDepsByAssignDep(require.getSid());
            if (ownerDepItems.size() == 1) {
                // 僅有一筆時, 不做下拉選單
                this.ownerDepSelectItems = null;
                this.selectOwnerDepItem = ownerDepItems.get(0);
            } else {
                // 多筆時，不預設選擇項目
                this.selectOwnerDepItem = null;
                // 將部門資料轉 SelectItem
                this.ownerDepSelectItems = ownerDepItems.stream()
                        .map(wkitem -> new SelectItem(wkitem, wkitem.getName()))
                        .collect(Collectors.toList());
            }

            // 定義為新增模式
            this.editType = OsEditType.IS_NEW;

        } catch (SystemDevelopException e) {
            MessagesUtils.showError("開發時期錯誤->" + e.getMessage());
            return;
        } catch (SystemOperationException e) {
            MessagesUtils.showError("系統處理錯誤，請嘗試重整頁面！" + e.getMessage());
            return;
        } catch (Exception e) {
            String message = WkMessage.EXECTION + "[" + e.getMessage() + "]";
            log.error(message, e);
            MessagesUtils.showError(message);
            return;
        }

        // othset_setting_dlg_wv
        displayController.showPfWidgetVar("othset_setting_dlg_wv");

    }

    /**
     * 開啟編輯視窗
     * 
     * @param testInfo
     */
    public void initByEdit(String os_no) {

        try {

            editOthset = osService.findByOsNo(os_no);
            tempTo = osService.findToByOsNo(os_no);

            // 準備單據歸屬單位選單資料
            this.selectOwnerDepItem = this.assignDepHelper.trnsOrgSidToWkItem(editOthset.getCreateDep().getSid());
            this.ownerDepSelectItems = null; // 不顯示下拉選單

            this.editType = OsEditType.IS_EDIT;

        } catch (SystemOperationException e) {
            MessagesUtils.showError("系統處理錯誤，請嘗試重整頁面！" + e.getMessage());
            return;
        } catch (Exception e) {
            String message = WkMessage.EXECTION + "[" + e.getMessage() + "]";
            log.error(message, e);
            MessagesUtils.showError(message);
            return;
        }

        // othset_setting_dlg_wv
        displayController.showPfWidgetVar("othset_setting_dlg_wv");
    }

    public void cancelEdit() {
        if (editType.equals(OsEditType.IS_EDIT)) {
            editOthset = osService.findByOsNo(editOthset.getOsNo());
        }
        this.editType = OsEditType.IS_VIEW;
    }

    private List<SelectItem> prepareThemeItems() {
        List<OthSetTheme> allTheme = osThemeService.findAll();
        if (WkStringUtils.isEmpty(allTheme)) {
            return Lists.newArrayList();
        }

        List<SelectItem> themeItems = Lists.newArrayList();
        for (OthSetTheme othSetTheme : allTheme) {
            themeItems.add(new SelectItem(othSetTheme.getSid(), othSetTheme.getTheme()));
        }
        return themeItems;
    }

    public void checkInputInfo(Require01MBean r01MBean) {

        // ====================================
        // 輸入檢核
        // ====================================
        if (this.selectOwnerDepItem == null) {
            MessagesUtils.showWarn("請選擇『單據歸屬單位』!");
            return;
        }

        if (WkStringUtils.isEmpty(editOthset.getTheme())) {
            MessagesUtils.showWarn("請輸入『主題』!");
            return;
        }
        if (WkStringUtils.isEmpty(editOthset.getNoticeDeps().getValue())) {
            MessagesUtils.showWarn("請選擇『通知單位』!");
            return;
        }
        if (WkStringUtils.isEmpty(
                WkJsoupUtils.getInstance().clearCssTag(editOthset.getContentCss()))) {
            MessagesUtils.showWarn("『資訊內容』不可為空白!");
            return;
        }

        this.saveOthSetByEdit(r01MBean);
        this.saveOthSetEntity(r01MBean);
    }

    /**
     * 編輯狀態
     *
     * @param r01MBean
     */
    private void saveOthSetByEdit(Require01MBean r01MBean) {
        if (!editType.equals(OsEditType.IS_EDIT)) {
            return;
        }
        try {
            OthSet beforeOthSet = osService.findByOsNo(editOthset.getOsNo());
            List<String> beforeNoticeDepSids = Lists.newArrayList();
            if (WkStringUtils.notEmpty(beforeOthSet.getNoticeDeps().getValue())) {
                beforeNoticeDepSids = Lists.newArrayList(beforeOthSet.getNoticeDeps().getValue());
            }

            List<String> afterNoticeDepSids = Lists.newArrayList();
            if (WkStringUtils.notEmpty(editOthset.getNoticeDeps().getValue())) {
                afterNoticeDepSids = Lists.newArrayList(editOthset.getNoticeDeps().getValue());
            }

            // ====================================
            // 儲存編輯資料
            // ====================================
            osService.saveByEditOthset(r01MBean.getRequire(),
                    editOthset,
                    WkUserCache.getInstance().findBySid(SecurityFacade.getUserSid()),
                    tempTo);

            // ====================================
            // 處理系統通知 (新增其他資訊單)
            // ====================================
            try {
                this.sysNotifyHelper.processForAddOtherSet(
                        r01MBean.getRequire().getSid(),
                        editOthset.getTheme(),
                        beforeNoticeDepSids,
                        afterNoticeDepSids,
                        SecurityFacade.getUserSid());
            } catch (Exception e) {
                log.error("執行系統通知失敗!" + e.getMessage(), e);
            }

            display.hidePfWidgetVar("othset_setting_dlg_wv");
            this.editType = OsEditType.IS_VIEW;
        } catch (IllegalAccessException e) {
            log.debug("檢核失敗", e);
            r01MBean.getOthSetBottomMBean().recoveryView(r01MBean, editOthset);
            MessagesUtils.showError(e.getMessage());
        } catch (OthSetEditExceptions e) {
            log.error("存檔失敗", e.getMessage());
            r01MBean.getOthSetBottomMBean().recoveryView(r01MBean, editOthset);
            MessagesUtils.showError(e.getMessage());
        } catch (Exception e) {
            log.error(r01MBean.getRequire().getRequireNo() + ":" + WkUserCache.getInstance().findBySid(SecurityFacade.getUserSid()).getName() + ":編輯其它設定資訊失敗..." + e.getMessage(), e);
            r01MBean.reBuildeByUpdateFaild();
            MessagesUtils.showError(e.getMessage());
        } finally {
            this.updateScreen(r01MBean);
        }
    }

    /**
     * 存檔
     *
     * @param r01MBean
     */
    private void saveOthSetEntity(Require01MBean r01MBean) {
        if (!editType.equals(OsEditType.IS_NEW)) {
            return;
        }
        try {

            // 新增時, 取得歸屬單位
            Org createDep = WkOrgCache.getInstance().findBySid(
                    Integer.valueOf(this.selectOwnerDepItem.getSid()));
            editOthset.setCreateDep(createDep);

            // 新增
            osService.saveByNewOthset(r01MBean.getRequire(),
                    editOthset,
                    WkUserCache.getInstance().findBySid(SecurityFacade.getUserSid()));

            log.info("{} 新增其他設定資訊單:{}", SecurityFacade.getUserId(), editOthset.getOsNo());
            this.editType = OsEditType.IS_VIEW;

            this.updateScreen(r01MBean);

        } catch (UserMessageException e) {
            r01MBean.reBuildeByUpdateFaild();
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            log.error(r01MBean.getRequire().getRequireNo() + ":" + WkUserCache.getInstance().findBySid(SecurityFacade.getUserSid()).getName() + ":建立其它設定資訊失敗..." + e.getMessage(), e);
            r01MBean.reBuildeByUpdateFaild();
            MessagesUtils.showError(e.getMessage());
            return;
        }
    }

    private void updateScreen(Require01MBean r01MBean) {
        r01MBean.reBuildeByUpdateFaild();
        r01MBean.getTraceMBean().clear();
        r01MBean.getOthSetSettingViewComponents().loadData();

        r01MBean.getBottomTabMBean().resetTabIdx(r01MBean);
        r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.OTH_SET);

        display.hidePfWidgetVar("othset_setting_dlg_wv");// 主設定視窗
        int idx = 0;
        try {
            List<OthSetSettingVO> othSetSettingVOs = r01MBean.getOthSetSettingViewComponents().getOthSetSettingVOs();
            if (WkStringUtils.notEmpty(othSetSettingVOs)) {
                idx = othSetSettingVOs.stream()
                        .map(OthSetSettingVO::getSid)
                        .collect(Collectors.toList())
                        .indexOf(this.editOthset.getSid());
            }
            if (idx == -1) {
                idx = 0;
            }
        } catch (Exception e) {
        }
        display.execute("accPanelSelectTab('os_acc_panel_layer_zero'," + idx + ");" + ReqConfirmClientHelper.CLIENT_SRIPT_CLOSE_PANEL);
    }

    /**
     * 主題變更<BR/>
     * 連動資訊內容、通知部門
     *
     * @param r01MBean
     */
    public void whenThemeChange(Require01MBean r01MBean) {

        // ====================================
        // 查詢
        // ====================================
        OthSetTheme othSetTheme = this.osThemeService.findBySid(this.editOthset.getTheme_sid());

        if (othSetTheme == null) {
            return;
        }

        // ====================================
        // 帶入預設值
        // ====================================
        // 主題
        editOthset.setTheme(othSetTheme.getTheme());
        // 內容
        editOthset.setContentCss(othSetTheme.getContext());

        // 取得新的通知單位
        List<Integer> noticeDeps = othSetTheme.getNoticeDetails().stream().map(OthSetThemeDetail::getDepSid)
                .collect(Collectors.toList());

        // ====================================
        //
        // ====================================
        // 預設值為空時, 開啟視窗
        if (CollectionUtils.isEmpty(noticeDeps)) {
            // this.assignDepOpenDialog(r01MBean);
            return;
        }

        // 將通知單位異動為該主題的預設通知單位
        editOthset.getNoticeDeps().setValue(
                noticeDeps.stream().map(sid -> sid + "").collect(Collectors.toList()));

        // 提示訊息
        String message = ""
                + "<span class='WS1-1-2b'>"
                + "通知單位已經被異動為【" + editOthset.getTheme() + "】的預設通知單位. 如下:"
                + "</span>"
                + "<br/><br/>"
                + WkCommonContentUtils.prepareDepNameWithDepByDataListStyle(
                        noticeDeps,
                        OrgLevel.MINISTERIAL,
                        true,
                        ">&nbsp;",
                        "",
                        false)
                + "<br/><br/>"
                + "請確認!"
                + "<br/><br/>";

        MessagesUtils.showInfo(message);
    }

    /**
     * 讀取 子程序 通知異動紀錄
     *
     * @param r01MBean
     * @param ptCheck
     */
    public void btnLoadSubNoticeInfo(Require01MBean r01MBean, OthSetSettingVO othSetVo) {
        r01MBean.loadSubNoticeInfo(othSetVo.getRequire_sid(), othSetVo.getOs_sid(),
                Lists.newArrayList(SubNoticeType.OS_INFO_DEP));
    }

    // ========================================================================
    // 通知單位元件
    // ========================================================================
    /**
     * 分享部門-編輯視窗
     */
    @Getter
    private final String ASSIGN_DEP_DLG_NAME = "ASSIGN_DEP_DLG_NAME_" + this.getClass().getSimpleName();

    /**
     * 分享部門-編輯視窗內容區
     */
    @Getter
    private final String ASSIGN_DEP_DLG_CONTENT = "ASSIGN_DEP_DLG_CONTENT_" + this.getClass().getSimpleName();
    /**
     * 分享部門-編輯視窗-部門選擇器ID
     */
    @Getter
    private final String ASSIGN_DEP_PICKER_ID = "ASSIGN_DEP_PICKER_ID_" + this.getClass().getSimpleName();

    /**
     * 分派單位選擇器
     */
    @Getter
    private transient MultItemPickerComponent assignDepPicker;

    /**
     * 記錄開啟時傳入的 Require01MBean
     */
    private transient Require01MBean require01MBean;

    /**
     * 開啟通知單位編輯視窗
     */
    public void assignDepOpenDialog(Require01MBean require01MBean) {

        this.require01MBean = require01MBean;

        try {
            // 選擇器設定資料
            MultItemPickerConfig config = new MultItemPickerConfig();
            config.setTreeModePrefixName("單位");
            config.setContainFollowing(true);
            config.setEnableGroupMode(true); // 開啟群組功能
            config.setItemComparator(this.multItemPickerByOrgHelper.parpareComparator());

            // 傳入 ID , 自動清除 filter
            config.setComponentID(this.ASSIGN_DEP_PICKER_ID);

            // 部分實作方法用 Default
            this.assignDepPickerCallback.setDefaultImplType(MultItemPickerCallbackDefaultImplType.DEP);

            // 選擇器初始化
            this.assignDepPicker = new MultItemPickerComponent(config, assignDepPickerCallback);

        } catch (Exception e) {
            String message = "通知單位選單初始化失敗!" + e.getMessage();
            MessagesUtils.showError(message);
            log.error(message, e);
            return;
        }

        // 更新畫面
        this.displayController.update(this.ASSIGN_DEP_DLG_CONTENT);
        // 開啟 dialog
        this.displayController.showPfWidgetVar(this.ASSIGN_DEP_DLG_NAME);
    }

    /**
     * 確認
     * 
     * @param isPreConfirm true:call by view , false:call by confirm callback
     */
    public void assignDepDepConfirm(boolean isPreConfirm) {

        // 防呆
        if (this.editOthset == null) {
            MessagesUtils.showWarn(WkMessage.NEED_RELOAD);
            return;
        }

        // ====================================
        // 檢察為空
        // ====================================
        if (WkStringUtils.isEmpty(this.assignDepPicker.getSelectedSid())) {
            MessagesUtils.showError("未挑選通知部門！！");
            return;
        }

        // ====================================
        // 取得異動前後單位清單
        // ====================================
        // 取得選單上已選擇的單位
        List<Integer> afterDepSids = this.assignDepPicker.getSelectedSid().stream()
                .filter(userSid -> WkStringUtils.isNumber(userSid))
                .map(userSid -> Integer.parseInt(userSid))
                .collect(Collectors.toList());

        // 取得異動前單位
        List<Integer> beforeDepSids = Lists.newArrayList();
        if (this.editOthset.getNoticeDeps() != null
                && this.editOthset.getNoticeDeps().getValue() != null) {
            // 轉數字
            beforeDepSids = this.editOthset.getNoticeDeps().getValue().stream()
                    .distinct()
                    .filter(userSid -> WkStringUtils.isNumber(userSid))
                    .map(userSid -> Integer.parseInt(userSid))
                    .collect(Collectors.toList());
        }

        // 比對是否未異動
        if (WkCommonUtils.compare(beforeDepSids, afterDepSids)) {
            MessagesUtils.showInfo("未異動通知單位!");
            return;
        }
        // ====================================
        // 確認訊息
        // ====================================
        // 不為新增時, 跳出異動的確認訊息
        if (isPreConfirm && WkStringUtils.notEmpty(this.editOthset.getSid())) {
            String drffMessage = WkCommonContentUtils.prepareDiffMessageForDepartment(
                    beforeDepSids,
                    afterDepSids,
                    "新增通知單位",
                    "移除通知單位");

            drffMessage = "<span class='WS1-1-3b'>異動【通知單位】結果如下：</span>"
                    + "<br/><br/>"
                    + drffMessage
                    + "<br/><br/>";

            this.confirmCallbackDialogController.showConfimDialog(
                    drffMessage,
                    Lists.newArrayList(),
                    () -> this.assignDepDepConfirm(false));
            return;
        }

        // ====================================
        // 更新主檔容器資料內的資料
        // ====================================
        this.editOthset.getNoticeDeps().setValue(this.assignDepPicker.getSelectedSid());

        // ====================================
        // 關閉對話框
        // ====================================
        this.displayController.hidePfWidgetVar(this.ASSIGN_DEP_DLG_NAME);
    }

    public String prepareNoticeDepsTooltip(OthSet othSet) {
        if (othSet != null &&
                othSet.getNoticeDeps() != null &&
                WkStringUtils.notEmpty(othSet.getNoticeDeps().getValue())) {

            Set<Integer> depSids = Sets.newHashSet();
            for (String depSid : othSet.getNoticeDeps().getValue()) {
                if (!WkStringUtils.isNumber(depSid)) {
                    continue;
                }
                depSids.add(Integer.parseInt(depSid));
            }

            return WkOrgUtils.prepareDepsNameByTreeStyle(Lists.newArrayList(depSids), 50);
        }
        return "<div class='WS1-1-2'>尚未選擇通知單位!</div>";
    }

    /**
     * 分派單位
     */
    private final MultItemPickerCallback assignDepPickerCallback = new MultItemPickerCallback() {

        /**
         * 
         */
        private static final long serialVersionUID = 7471933737986211470L;

        /**
         * 取得已選擇部門
         */
        @Override
        public List<WkItem> prepareSelectedItems() throws SystemDevelopException {

            // 防呆
            if (editOthset == null
                    || editOthset.getNoticeDeps() == null
                    || editOthset.getNoticeDeps().getValue() == null) {
                return Lists.newArrayList();

            }

            // 由外部取回已選擇部門, 並轉換為 WkItem 物件
            return editOthset.getNoticeDeps().getValue().stream()
                    .distinct() // 去重複
                    .filter(depSid -> WkStringUtils.isNumber(depSid)) // 轉數字
                    .map(depSid -> multItemPickerByOrgHelper.createDepItemByDepSid(Integer.valueOf(depSid))) // 轉 wkitem
                    .collect(Collectors.toList());
        }

        /**
         * 準備 disable 的單位
         */
        @Override
        public List<String> prepareDisableItemSids() throws SystemDevelopException {
            // 鎖定需求單位 (需求單位不可移除)
            return Lists.newArrayList(require01MBean.getRequire().getCreateDep().getSid() + "");
        }
    };
}
