package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search24QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.Search24View;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.customer.ReqWorkCustomerHelper;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.searchheader.CommonHeaderMBean;
import com.cy.tech.request.web.controller.searchheader.Search24HeaderMBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.logic.lib.manager.WorkCommonReadRecordManager;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.customer.vo.WorkCustomer;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author kasim
 */
@Controller
@Scope("view")
@Slf4j
public class Search24MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2645223569791080727L;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private Search24HeaderMBean headerMBean;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private ReqWorkCustomerHelper reqWorkCustomerHelper;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private Search24QueryService search24QueryService;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchResultHelper searchResultHelper;
    @Autowired
    transient private WorkCommonReadRecordManager workCommonReadRecordManager;

    private List<Search24View> allQueryItems;
    /** 所有的需求單 */
    @Getter
    @Setter
    private List<Search24View> queryItems;

    /** 選擇的需求單 */
    @Getter
    @Setter
    private Search24View querySelection;

    /** 上下筆移動keeper */
    @Getter
    private Search24View queryKeeper;

    /** 切換模式 */
    @Getter
    private SwitchType switchType = SwitchType.CONTENT;

    /** 切換模式 - 全畫面狀態 */
    @Getter
    private SwitchType switchFullType = SwitchType.DETAIL;

    /** 在匯出的時候，某些內容需要隱藏 */
    @Getter
    private boolean hasDisplay = true;

    @PostConstruct
    public void init() {
        allQueryItems = Lists.newArrayList();
        this.search();
    }

    /**
     * 查詢
     */
    public void search() {
        allQueryItems = this.findWithQuery();
        this.filterCustomerPage(Boolean.TRUE);
    }

    private List<Search24View> findWithQuery() {
        String requireNo = reqularUtils.getRequireNo(loginBean.getCompanyId(), commHeaderMBean.getFuzzyText());
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT "
                + "tr.require_sid,"
                + "tr.require_no,"
                + "tr.customer,"
                + "tr.author,"
                + "tr.create_dt,"
                + "tr.dep_sid,"
                + "tr.create_usr,"
                + "tr.require_status,"
                + "tr.inte_staff,"
                + "tr.read_reason,"
                + "tr.hope_dt,"
                + "tr.update_dt,"
                + "tr.urgency,"
                + "ckm.big_category_name,"
                + "ckm.middle_category_name,"
                + "ckm.small_category_name,"
                + "tid.field_content,"
                + "mappTrans.issueCreatedDate,"
                + "mappTrans.issueTheme,"
                + "mappTrans.issueNo,"
                + "mappTrans.issueDispatchStatus,"
                + "mappTrans.issueStatus,"
                + "mappTrans.issueExpectedDate,"
                + "mappTrans.issueResponsible, "
                // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
                + this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire()
                + " FROM ");
        this.buildRequireCondition(requireNo, builder, parameters);
        this.buildRequireIndexDictionaryCondition(requireNo, builder, parameters);
        this.buildCategoryKeyMappingCondition(builder);
        this.buildIssueCondition(requireNo, builder, parameters);
        // 檢查項目 (系統別)
        // 後方需對 主單 sid 做 group by
        builder.append(this.searchConditionSqlHelper.prepareCommonJoin(SecurityFacade.getUserSid()));

        builder.append("WHERE tr.require_sid IS NOT NULL ");

        // 查詢條件：是否閱讀
        if (WkStringUtils.isEmpty(requireNo)) {
            builder.append(
                    this.workCommonReadRecordManager.prepareWhereConditionSQL(
                            headerMBean.getSelectReadRecordType(), "readRecord"));
        }

        builder.append(" GROUP BY tr.require_sid ");
        builder.append("  ORDER BY tr.update_dt DESC");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.TRANSFER_FROM_TECH, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.TRANSFER_FROM_TECH, SecurityFacade.getUserSid());

        // 查詢
        List<Search24View> resultList = search24QueryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // ====================================
        // 後續處理
        // ====================================
        if (WkStringUtils.notEmpty(resultList)) {
            // 後續處理-開始
            usageRecord.afterProcessStart();
            // 查詢條件過濾：系統別
            resultList = resultList.stream()
                    .filter(each -> this.searchResultHelper.filterCheckItems(
                            each.getCheckItemTypes(),
                            commHeaderMBean.getCheckItemTypes(),
                            false,
                            null))
                    .collect(Collectors.toList());
            // 後續處理-結束
            usageRecord.afterProcessEnd();
        }

        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return resultList;

    }

    /**
     * 過濾需求單條件
     *
     * @param builder
     * @param parameters
     */
    private void buildRequireCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("(SELECT * FROM tr_require tr WHERE 1=1 ");

        // 限制條件:需求類別製作進度
        String requireStatusTypeSql = Lists.newArrayList(
                RequireStatusType.PROCESS,
                RequireStatusType.SUSPENDED,
                RequireStatusType.COMPLETED).stream()
                .map(each -> each.name())
                .collect(Collectors.joining("','", "'", "'"));

        builder.append(" AND tr.require_status IN (" + requireStatusTypeSql + ")");

        // 需求單號
        if (!Strings.isNullOrEmpty(requireNo)) {
            builder.append(" AND tr.require_no = :requireNo");
            parameters.put("requireNo", requireNo);
        }
        // 立單區間
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getStartDate() != null && commHeaderMBean.getEndDate() != null) {
            builder.append(" AND tr.create_dt BETWEEN :startDate AND :endDate ");
            parameters.put("startDate", helper.transStartDate(commHeaderMBean.getStartDate()));
            parameters.put("endDate", helper.transEndDate(commHeaderMBean.getEndDate()));
        } else if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getStartDate() != null) {
            builder.append(" AND tr.create_dt >= :startDate ");
            parameters.put("startDate", helper.transStartDate(commHeaderMBean.getStartDate()));
        } else if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getEndDate() != null) {
            builder.append(" AND tr.create_dt <= :endDate ");
            parameters.put("endDate", helper.transEndDate(commHeaderMBean.getEndDate()));
        }

        // 廳主
        if (Strings.isNullOrEmpty(requireNo) && headerMBean.getCustomers() != null && !headerMBean.getCustomers().isEmpty()
                && headerMBean.getCustomers().size() != headerMBean.getCustomerItems().size()) {
            builder.append(" AND tr.customer IN (:customers)");
            parameters.put("customers", headerMBean.getCustomerSids());
        }

        // 填單人員
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(commHeaderMBean.getTrCreatedUserName())) {
            List<Integer> userSids = WkUserUtils.findByNameLike(commHeaderMBean.getTrCreatedUserName(), SecurityFacade.getCompanyId());
            if (WkStringUtils.isEmpty(userSids)) {
                userSids.add(-999);
            }
            builder.append(" AND tr.create_usr IN (:userSids)");
            parameters.put("userSids", userSids.isEmpty() ? "" : userSids);
        }
        builder.append(") AS tr ");
    }

    /**
     * 模糊查詢
     *
     * @param builder
     * @param parameters
     */
    private void buildRequireIndexDictionaryCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT tid.require_sid,tid.field_content FROM tr_index_dictionary tid WHERE 1=1");
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.isFuzzyQuery()) {
            String text = "%" + reqularUtils.replaceIllegalSqlLikeStr(commHeaderMBean.getFuzzyText()) + "%";
            builder.append(" AND (tid.require_sid"
                    + " IN (SELECT DISTINCT tid1.require_sid FROM tr_index_dictionary tid1 WHERE tid1.field_content LIKE :text)"
                    + " OR tid.require_sid IN (SELECT DISTINCT trace.require_sid FROM tr_require_trace trace WHERE "
                    + "trace.require_trace_type = 'REQUIRE_INFO_MEMO' AND trace.require_trace_content LIKE :text))");
            parameters.put("text", text);
        }
        builder.append(" AND tid.field_name='主題') AS tid ON tr.require_sid=tid.require_sid ");
    }

    /**
     * JOIN 類別資訊
     *
     * @param builder
     * @param parameters
     */
    private void buildCategoryKeyMappingCondition(StringBuilder builder) {
        builder.append("INNER JOIN (SELECT * FROM tr_category_key_mapping ckm WHERE 1=1");
        builder.append(") AS ckm ON tr.mapping_sid=ckm.key_sid ");
    }

    /**
     * JOIN 對照表及案件單
     *
     * @param builder
     * @param parameters
     */
    private void buildIssueCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append(" INNER JOIN ( ");
        builder.append(" SELECT mappTrans.source_sid, mappTrans.target_sid ");
        builder.append(",issueCreatedDate,issueTheme ");
        builder.append(",issueNo,issueStatus ");
        builder.append(",issueExpectedDate,issueResponsible ");
        builder.append(",issueDispatchStatus ");
        builder.append(" FROM wk_mapp_create_trans mappTrans ");
        builder.append(" INNER JOIN ( ");
        builder.append(" SELECT tc.IssueSid ");
        builder.append(" ,tc.Created AS issueCreatedDate ");
        builder.append(" ,tc.Theme AS issueTheme ");
        builder.append(" ,tc.TechnicalCaseNo AS issueNo ");
        builder.append(" ,tc.PaperCode AS issueStatus ");
        builder.append(" ,tc.ExpectedDay AS issueExpectedDate ");
        builder.append(" ,tc.Responsible AS issueResponsible ");
        builder.append(" ,issueDispatchStatus ");
        builder.append(" FROM tc_technical_case tc ");
        builder.append(" left JOIN ( ");
        builder.append(" SELECT dispatch.IssueSid,dispatch.MasterPaperCode AS issueDispatchStatus ");
        builder.append(" FROM tc_dispatch dispatch ");
        builder.append(" ) AS dispatch ON tc.IssueSid=dispatch.IssueSid ");
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(headerMBean.getIssueStatus())) {
            builder.append(" where tc.PaperCode= :issueStatus ");
            parameters.put("issueStatus", headerMBean.getIssueStatus());
        }
        builder.append(" ) AS tc ON tc.IssueSid=mappTrans.source_sid ");
        builder.append(" ) AS mappTrans ON tr.require_sid=mappTrans.target_sid ");
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        headerMBean.clear();
        this.search();
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Search24View view) {
        view.setReadRecordType(ReadRecordType.HAS_READ);
        Require r = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(r, loginBean.getUser());
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (Search24View) event.getObject();
        this.changeRequireContent(this.queryKeeper);
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Search24View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        helper.exportExcel(document, commHeaderMBean.getStartDate(), commHeaderMBean.getEndDate(), commHeaderMBean.getReportType());
        hasDisplay = true;
    }

    /**
     * 隱藏部分column裡的內容
     */
    public void hideColumnContent() {
        hasDisplay = false;
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search24View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.removeClassByTextBold(dtId, pageCount);
        this.transformHasRead();
        this.checkHelfScreen();
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            display.update("headerTitle");
            display.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Search24View to) {
        querySelection = to;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 去除粗體Class
     *
     * @param dtId
     * @param pageCount
     */
    private void removeClassByTextBold(String dtId, String pageCount) {
        display.execute("removeClassByTextBold('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
        display.execute("changeAlreadyRead('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 變更已閱讀
     */
    private void transformHasRead() {
        querySelection.setReadRecordType(ReadRecordType.HAS_READ);
        queryKeeper = querySelection;
        queryItems.set(queryItems.indexOf(querySelection), querySelection);
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.transformHasRead();
        this.removeClassByTextBold(dtId, pageCount);
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    /**
     * 列表標註及回傳需求單
     *
     * @param view
     * @return
     */
    @SuppressWarnings("unused")
    private Require highlightAndReturnRequire(Search24View view) {
        this.querySelection = view;
        this.queryKeeper = this.querySelection;
        return requireService.findByReqNo(view.getRequireNo());
    }

    /**
     * 變更 廳主
     */
    public void chgCustomerPage() {
        this.filterCustomerPage(Boolean.FALSE);
    }

    /**
     * 過濾 廳主
     *
     * @param isSearch
     */
    public void filterCustomerPage(Boolean isSearch) {
        if (headerMBean.getIsCustomerPage()) {
            if (headerMBean.getCustomerPage() == null && !headerMBean.getCustomerPageItems().isEmpty()) {
                headerMBean.setCustomerPage(headerMBean.getCustomerPageItems().get(0));
            }
        }
        this.bulidQueryItemsAndCustomerPageItems(isSearch);
    }

    /**
     * 建立廳主分頁選項
     */
    private void bulidQueryItemsAndCustomerPageItems(Boolean isSearch) {
        queryItems = Lists.newArrayList();
        List<WorkCustomer> customerPageItems = Lists.newArrayList();
        allQueryItems.stream().forEach(v -> {
            if (isSearch && v.getCustomer() != null && !reqWorkCustomerHelper.containCustomer(customerPageItems, v.getCustomer())) {
                customerPageItems.add(reqWorkCustomerHelper.findCopyOne(v.getCustomer()));
            }
            if (!(headerMBean.getIsCustomerPage()
                    && (headerMBean.getCustomerPage() != null && !headerMBean.getCustomerPage().getSid().equals(v.getCustomer())))) {
                queryItems.add(v);
            }
        });
        if (isSearch) {
            headerMBean.setCustomerPageItems(customerPageItems);
            if (headerMBean.getIsCustomerPage()
                    && (customerPageItems.isEmpty() || !customerPageItems.contains(headerMBean.getCustomerPage()))) {
                headerMBean.setCustomerPage(null);
                this.filterCustomerPage(Boolean.FALSE);
            }
        }
    }

}
