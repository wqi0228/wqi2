/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.view.to.search.query;

import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.vo.query.search.SearchQuery;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.LocalDate;

/**
 * Search03MBean 查詢欄位
 *
 * @author kasim
 */
public class SearchQuery03 extends SearchQuery implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8411609689759073430L;
    @Getter
    private boolean disableQueryItem;
    @Getter
    @Setter
    /** 選擇的大類 */
    private String selectBigCategorySid;
    @Getter
    @Setter
    /** 類別組合(大類 sid) */
    private List<String> bigDataCateSids = Lists.newArrayList();
    @Getter
    @Setter
    /** 類別組合(中類 sid) */
    private List<String> middleDataCateSids = Lists.newArrayList();
    @Getter
    @Setter
    /** 類別組合(小類 sid) */
    private List<String> smallDataCateSids = Lists.newArrayList();
    @Getter
    @Setter
    /** 執行人員 */
    private String execUser;
    @Getter
    @Setter
    /** 要模糊查詢的字串 */
    private String fuzzyText;
    @Getter
    @Setter
    /** 時間切換的index */
    private int dateTypeIndex;
    @Getter
    @Setter
    /** 起始時間 */
    private Date startDate;
    @Getter
    @Setter
    /** 結束時間 */
    private Date endDate;
    @Getter
    @Setter
    /** 選擇的閱讀類型 */
    private String selectReadRecordType;
    @Getter
    @Setter
    /** 異動啟始日 */
    private Date startUpdatedDate;
    @Getter
    @Setter
    /** 異動結束日 */
    private Date endUpdatedDate;
    @Getter
    @Setter
    /** 轉發單位 */
    private List<String> forwardDepts = Lists.newArrayList();
    @Getter
    @Setter
    /** 緊急度 */
    private List<String> urgencyList;
    @Getter
    @Setter
    /** 需求單號 */
    private String requireNo;
    

    public SearchQuery03(ReportType reportType) {
        this.reportType = reportType;
    }

    /**
     * 清除
     */
    public void clear() {
        this.init();
    }

    /**
     * 初始化
     */
    private void init() {
    	// 共用查詢條件初始化
    	this.publicConditionInit();
    	
        this.disableQueryItem = true;
        this.selectBigCategorySid = null;
        this.bigDataCateSids = Lists.newArrayList();
        this.middleDataCateSids = Lists.newArrayList();
        this.smallDataCateSids = Lists.newArrayList();
        this.execUser = null;
        this.fuzzyText = null;
        this.dateTypeIndex = 4;
        this.startDate = null;
        this.endDate = null;
        this.selectReadRecordType = null;
        this.clearAdvance();
    }

    /**
     * 清除進階選項
     */
    public void clearAdvance() {
        startUpdatedDate = null;
        endUpdatedDate = null;
        forwardDepts.clear();
        urgencyList = null;
        requireNo = null;
    }

    /**
     * 變換 待檢查 | 已檢查 | 已分派
     */
    public void changeByOption() {
        disableQueryItem = false;
        execUser = null;
        this.changeDateIntervalToDay();
    }

    /**
     * 上個月
     */
    public void changeDateIntervalPreMonth() {
        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate lastDate = new LocalDate(date).minusMonths(1);
        this.dateTypeIndex = 0;
        this.startDate = lastDate.dayOfMonth().withMinimumValue().toDate();
        this.endDate = lastDate.dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 本月份
     */
    public void changeDateIntervalThisMonth() {
        this.dateTypeIndex = 1;
        this.startDate = new LocalDate().dayOfMonth().withMinimumValue().toDate();
        this.endDate = new LocalDate().dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 下個月
     */
    public void changeDateIntervalNextMonth() {
        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate nextDate = new LocalDate(date).plusMonths(1);
        this.dateTypeIndex = 2;
        this.startDate = nextDate.dayOfMonth().withMinimumValue().toDate();
        this.endDate = nextDate.dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 今日
     */
    public void changeDateIntervalToDay() {
        this.dateTypeIndex = 3;
        this.startDate = new Date();
        this.endDate = new Date();
    }
}
