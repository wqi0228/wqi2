/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require.send.test;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.apache.commons.collections.CollectionUtils;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.config.exception.TestInfoEditExceptions;
import com.cy.tech.request.logic.helper.systemnotify.SysNotifyHelper;
import com.cy.tech.request.logic.service.send.test.SendTestScheduleService;
import com.cy.tech.request.logic.service.send.test.SendTestService;
import com.cy.tech.request.logic.utils.DateUtils;
import com.cy.tech.request.logic.vo.WorkTestInfoTo;
import com.cy.tech.request.vo.enums.SubNoticeType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.vo.worktest.enums.WorkTestInfoStatus;
import com.cy.tech.request.web.common.PermissionHelper;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerCallback;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerCallbackDefaultImplType;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerComponent;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerConfig;
import com.cy.tech.request.web.controller.component.mipker.helper.MultItemPickerByOrgHelper;
import com.cy.tech.request.web.controller.component.reqconfirm.ReqConfirmClientHelper;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.helper.AssignDepHelper;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.values.OrganizationTreeMBean;
import com.cy.tech.request.web.controller.values.OrgsListCompant;
import com.cy.tech.request.web.controller.view.vo.GroupVO;
import com.cy.tech.request.web.pf.utils.ConfirmCallbackDialogController;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.utils.WkCommonContentUtils;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 送測 控制<BR/>
 * 對應 send_test_setting_dialog.xhtml
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope("view")
public class SendTestSettingMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4179170918984348364L;
    @Autowired
    private transient WorkBackendParamHelper workBackendParamHelper;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private SendTestService sendTestService;
    @Autowired
    transient private DisplayController display;
    @Getter
    @Autowired
    private OrganizationTreeMBean orgTreeMBean;
    @Autowired
    private SendTestScheduleService sendTestScheduleService;
    @Autowired
    transient private AssignDepHelper assignDepHelper;
    @Autowired
    transient private DisplayController displayController;
    @Autowired
    transient private SysNotifyHelper sysNotifyHelper;
    @Autowired
    transient private MultItemPickerByOrgHelper multItemPickerByOrgHelper;
    @Autowired
    transient private ConfirmCallbackDialogController confirmCallbackDialogController;

    @Getter
    @Setter
    private OrgsListCompant orgsListCompant = new OrgsListCompant();
    /** 編輯用主檔 */
    @Getter
    @Setter
    private WorkTestInfo editTestInfo;
    /** 備份資料 - 用來檢測 */
    private WorkTestInfoTo tempTestInfoTo;

    @Getter
    @Setter
    private String editFbId;

    @Getter
    /** 編輯狀態控制 */
    private StEditType editType;
    @Getter
    private List<GroupVO> groupVOs;
    @Getter
    private List<SelectItem> groupItems;
    @Getter
    @Setter
    private String selGroupVOSid;
    @Getter
    @Setter
    private List<String> qaDepSids = Lists.newArrayList();

    /**
     * 歸屬單位選項
     */
    @Getter
    private List<SelectItem> ownerDepSelectItems;
    /**
     * 選擇的歸屬單位
     */
    @Getter
    @Setter
    private WkItem selectOwnerDepItem;

    public enum StEditType {
        /** 檢視 */
        IS_VIEW,
        /** 新增 */
        IS_NEW,
        /** 編輯 */
        IS_EDIT,
        /** 編輯FBID */
        IS_EDIT_FB_ID,
        /** 編輯通知 */
        IS_EDIT_NOTIFY_UNIT,
        /** 退測 */
        IS_RETEST,;
    }

    /**
     * 物件初始化
     */
    @PostConstruct
    public void init() {
        this.editType = StEditType.IS_VIEW;
        editTestInfo = new WorkTestInfo();
        this.editFbId = "";
        selGroupVOSid = "";
        groupVOs = Lists.newArrayList();
        groupItems = Lists.newArrayList();
        // QA部門
        qaDepSids = workBackendParamHelper.getQADepSids().stream()
                .map(each -> each + "")
                .collect(Collectors.toList());
    }

    /**
     * 開啟新增對話框
     * 
     * @param r01MBean
     */
    public void openDialogByNewAdd(Require01MBean r01MBean) {

        // 取得主單 bean
        if (r01MBean == null) {
            MessagesUtils.showWarn("資訊已逾期，請重新整理畫面");
            return;
        }
        Require require = r01MBean.getRequire();

        try {
            // 元件變數初始化
            this.init();
            // 建立編輯主檔
            this.editTestInfo = sendTestService.createEmptyTestInfo(loginBean.getUser(), require);

            // 準備單據歸屬單位選單資料
            List<WkItem> ownerDepItems = assignDepHelper.prepareLoginUserCaseOwnerDepsByAssignDep(require.getSid());
            if (ownerDepItems.size() == 1) {
                // 僅有一筆時, 不做下拉選單
                this.ownerDepSelectItems = null;
                this.selectOwnerDepItem = ownerDepItems.get(0);
            } else {
                // 多筆時，不預設選擇項目
                this.selectOwnerDepItem = null;
                // 將部門資料轉 SelectItem
                this.ownerDepSelectItems = ownerDepItems.stream()
                        .map(wkitem -> new SelectItem(wkitem, wkitem.getName()))
                        .collect(Collectors.toList());
            }

            // 定義為新增模式
            this.editType = StEditType.IS_NEW;

        } catch (SystemDevelopException e) {
            MessagesUtils.showError("開發時期錯誤->" + e.getMessage());
            return;
        } catch (SystemOperationException e) {
            MessagesUtils.showError("系統處理錯誤，請嘗試重整頁面！" + e.getMessage());
            return;
        } catch (Exception e) {
            String message = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            log.error(message, e);
            MessagesUtils.showError(message);
            return;
        }

        // send_test_setting_dlg_wv
        displayController.showPfWidgetVar("send_test_setting_dlg_wv");

    }

    /**
     * 開啟編輯視窗
     * 
     * @param testInfo
     */
    public void initByEdit(WorkTestInfo testInfo) {

        try {

            editTestInfo = sendTestService.findByTestinfoNo(testInfo.getTestinfoNo());
            tempTestInfoTo = sendTestService.findToByTestinfoNo(testInfo.getTestinfoNo());
            this.recoverFbId();

            // 準備單據歸屬單位選單資料
            this.selectOwnerDepItem = this.assignDepHelper.trnsOrgSidToWkItem(testInfo.getCreateDep().getSid());
            this.ownerDepSelectItems = null; // 不顯示下拉選單

            this.editType = StEditType.IS_EDIT;

        } catch (SystemOperationException e) {
            MessagesUtils.showError("系統處理錯誤，請嘗試重整頁面！" + e.getMessage());
            return;
        } catch (Exception e) {
            String message = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            log.error(message, e);
            MessagesUtils.showError(message);
            return;
        }

        // send_test_setting_dlg_wv
        displayController.showPfWidgetVar("send_test_setting_dlg_wv");
    }

    public void initByRetest(WorkTestInfo testInfo) {
        editTestInfo = sendTestService.findByTestinfoNo(testInfo.getTestinfoNo());
        tempTestInfoTo = sendTestService.findToByTestinfoNo(testInfo.getTestinfoNo());
        this.editType = StEditType.IS_RETEST;
    }

    public void cancelEdit() {
        if (this.editType.equals(StEditType.IS_EDIT) || this.editType.equals(StEditType.IS_RETEST) || this.editType.equals(StEditType.IS_EDIT_NOTIFY_UNIT)) {
            editTestInfo = sendTestService.findByTestinfoNo(editTestInfo.getTestinfoNo());
        }
        this.editType = StEditType.IS_VIEW;
    }

    public void sendTestSettingConfirm(Require01MBean r01MBean, boolean isPreConfirmFB) {
        // ====================================
        // 防呆
        // ====================================
        if (this.editTestInfo == null) {
            MessagesUtils.showWarn(WkMessage.SESSION_TIMEOUT);
            return;
        }

        // ====================================
        // 輸入檢核
        // ====================================
        // 檢查送測日
        if (this.editTestInfo.getTestDate() == null) {
            MessagesUtils.showWarn("請輸入送測日!");
            return;
        }

        if (!StEditType.IS_RETEST.equals(this.editType)
                && this.editTestInfo.getTestDate().before(DateUtils.convertToOriginOfDay(new Date()))) {
            MessagesUtils.showWarn("送測日不可早於系統日！！");
            return;
        }

        // 檢查預計完成日
        if (this.editTestInfo.getEstablishDate() == null) {
            MessagesUtils.showWarn("請輸入預計完成日!");
            return;
        }

        if (this.editTestInfo.getEstablishDate().before(DateUtils.convertToOriginOfDay(new Date()))) {
            MessagesUtils.showWarn("預計完成日不可早於系統日！！");
            return;
        }

        // 檢查送測單位
        if (this.editTestInfo.getSendTestDep() == null
                || WkStringUtils.isEmpty(this.editTestInfo.getSendTestDep())) {
            MessagesUtils.showWarn("請至少選擇一個送測單位!");
            return;
        }

        // 檢查主題
        if (WkStringUtils.isEmpty(this.editTestInfo.getTheme())) {
            MessagesUtils.showWarn("請輸入送測主題!");
            return;
        }

        // 檢查內容
        String content = WkJsoupUtils.getInstance().clearCssTag(this.editTestInfo.getContentCss());
        if (WkStringUtils.isEmpty(content)) {
            MessagesUtils.showWarn("請輸入送測內容!");
            return;
        }

        // 檢查FB 格式
        try {
            this.sendTestService.verifyFbId(this.editFbId);
        } catch (Exception e) {
            MessagesUtils.showWarn(" FB ID 格式不符請確認！！");
        }

        // ====================================
        // 編輯時, 若FB ID 被清空, 跳出確認窗
        // ====================================
        if (isPreConfirmFB // 此次需檢查
                && StEditType.IS_EDIT.equals(this.editType) // 編輯模式
                && WkStringUtils.notEmpty(editTestInfo.getSid()) // 再次判斷為編輯
                && WkStringUtils.notEmpty(this.editTestInfo.getFbId()) // 原 FBID 不為空
                && WkStringUtils.isEmpty(this.editFbId)) { // 此次 FBID 被清空

            this.confirmCallbackDialogController.showConfimDialog(
                    "請確認是否要將FBID資訊清空！",
                    Lists.newArrayList(),
                    () -> this.sendTestSettingConfirm(r01MBean, false));

            return;
        }

        // ====================================
        // 儲存-狀態為-編輯、編輯通知
        // ====================================
        if (Lists.newArrayList(
                StEditType.IS_EDIT,
                StEditType.IS_EDIT_NOTIFY_UNIT)
                .contains(this.editType)) {
            this.saveTestByEdit(r01MBean);
            return;
        }

        // ====================================
        // 儲存-狀態為-退測
        // ====================================
        if (this.editType.equals(StEditType.IS_RETEST)) {
            this.saveTestInfoByRetest(r01MBean);
            this.editType = StEditType.IS_VIEW;
            return;
        }

        return;

    }

    /**
     * 新增 送測單主檔
     * 
     * @param r01MBean
     */
    public void checkInputInfo(Require01MBean r01MBean) {

        // ====================================
        // 防呆
        // ====================================
        if (this.editTestInfo == null) {
            MessagesUtils.showWarn(WkMessage.SESSION_TIMEOUT);
            return;
        }

        // ====================================
        // 輸入檢核
        // ====================================
        // 檢查送測日
        if (this.editTestInfo.getTestDate() == null) {
            MessagesUtils.showWarn("請輸入送測日!");
            return;
        }

        if (!StEditType.IS_RETEST.equals(this.editType)
                && this.editTestInfo.getTestDate().before(DateUtils.convertToOriginOfDay(new Date()))) {
            MessagesUtils.showWarn("送測日不可早於系統日！！");
            return;
        }

        // 檢查預計完成日
        if (this.editTestInfo.getEstablishDate() == null) {
            MessagesUtils.showWarn("請輸入預計完成日!");
            return;
        }

        if (this.editTestInfo.getEstablishDate().before(DateUtils.convertToOriginOfDay(new Date()))) {
            MessagesUtils.showWarn("預計完成日不可早於系統日！！");
            return;
        }

        // 檢查送測單位
        if (this.editTestInfo.getSendTestDep() == null
                || WkStringUtils.isEmpty(this.editTestInfo.getSendTestDep())) {
            MessagesUtils.showWarn("請至少選擇一個送測單位!");
            return;
        }

        // 檢查主題
        if (WkStringUtils.isEmpty(this.editTestInfo.getTheme())) {
            MessagesUtils.showWarn("請輸入送測主題!");
            return;
        }

        // 檢查內容
        String content = WkJsoupUtils.getInstance().clearCssTag(this.editTestInfo.getContentCss());
        if (WkStringUtils.isEmpty(content)) {
            MessagesUtils.showWarn("請輸入送測內容!");
            return;
        }

        // 檢查FB 格式
        try {
            this.sendTestService.verifyFbId(this.editFbId);
        } catch (Exception e) {
            MessagesUtils.showWarn(" FB ID 格式不符請確認！！");
        }

        // ====================================
        // 編輯時, 若FB ID 被清空, 跳出確認窗
        // ====================================
        if (this.editType.equals(StEditType.IS_EDIT) && !Strings.isNullOrEmpty(editTestInfo.getSid())
                && editTestInfo.getFbId() != null
                && Strings.isNullOrEmpty(editFbId)) {
            display.showPfWidgetVar("send_test_setting_confirm_fb_change_null_dlg_wv");
            return;
        }

        // ====================================
        // 儲存-狀態為-編輯、編輯通知
        // ====================================
        if (this.editType.equals(StEditType.IS_EDIT) || this.editType.equals(StEditType.IS_EDIT_NOTIFY_UNIT)) {
            this.saveTestByEdit(r01MBean);
            return;
        }

        // ====================================
        // 儲存-狀態為-退測
        // ====================================
        if (this.editType.equals(StEditType.IS_RETEST)) {
            this.saveTestInfoByRetest(r01MBean);
            this.editType = StEditType.IS_VIEW;
            return;
        }

        // ====================================
        // 儲存-狀態為新增 （FB欄位非空）
        // ====================================
        if (WkStringUtils.isEmpty(editTestInfo.getSid()) && WkStringUtils.notEmpty(editFbId)) {
            // 新增時, 取得歸屬單位
            Org createDep = WkOrgCache.getInstance().findBySid(
                    Integer.valueOf(this.selectOwnerDepItem.getSid()));
            editTestInfo.setCreateDep(createDep);

            editTestInfo.setFbId(Integer.valueOf(editFbId));
            this.saveTestInfoEntity(r01MBean, false);
            display.execute("moveScrollBottom();");
        } else {
            // FBID 為空時, 跳轉FB視窗
            display.showPfWidgetVar("send_test_setting_confirm_trans_fb_dlg_wv");// 轉FB視窗
        }

        display.hidePfWidgetVar("send_test_setting_dlg_wv");// 主設定視窗
    }

    /**
     *
     * @param r01MBean
     */
    public void saveTestByEdit(Require01MBean r01MBean) {
        if (this.editType.equals(StEditType.IS_EDIT)) {
            if (!Strings.isNullOrEmpty(editFbId)) {
                editTestInfo.setFbId(Integer.valueOf(editFbId));
            } else {
                editTestInfo.setFbId(null);
            }
        }
        this.saveTestInfoByEdit(r01MBean);
        this.editType = StEditType.IS_VIEW;
    }

    public void recoverFbId() {
        if (editTestInfo.getFbId() != null) {
            this.editFbId = String.valueOf(editTestInfo.getFbId());
        }
    }

    /**
     * 編輯狀態
     *
     * @param r01MBean
     */
    private void saveTestInfoByEdit(Require01MBean r01MBean) {
        try {
            sendTestService.saveByEditTestInfo(r01MBean.getRequire(),
                    editTestInfo,
                    loginBean.getUser(),
                    tempTestInfoTo);
        } catch (IllegalAccessException e) {
            log.debug("檢核失敗", e);
            r01MBean.getSendTestBottomMBean().recoveryView(r01MBean, editTestInfo);
            MessagesUtils.showError(e.getMessage());
        } catch (TestInfoEditExceptions e) {
            log.error("存檔失敗", e.getMessage());
            r01MBean.getSendTestBottomMBean().recoveryView(r01MBean, editTestInfo);
            MessagesUtils.showError(e.getMessage());
        } catch (Exception e) {
            log.error(r01MBean.getRequire().getRequireNo() + ":" + editTestInfo.getTestinfoNo() + ":" + loginBean.getUser().getName() + ":編輯送測失敗..."
                    + e.getMessage(), e);
            r01MBean.getSendTestBottomMBean().recoveryView(r01MBean, editTestInfo);
            MessagesUtils.showError(e.getMessage());
        } finally {
            this.updateScreen(r01MBean);
        }
    }

    private void updateScreen(Require01MBean r01MBean) {
        r01MBean.reBuildeRequire();
        r01MBean.getSendTestBottomMBean().initTabInfo(r01MBean);
        display.update("viewPanelBottomInfoTabId");
        display.hidePfWidgetVar("send_test_setting_dlg_wv");// 主設定視窗
        if (CollectionUtils.isNotEmpty(r01MBean.getSendTestBottomMBean().getTestinfos())) {
            int idx = r01MBean.getSendTestBottomMBean().getTestinfos().indexOf(editTestInfo);
            display.execute("accPanelSelectTab('st_acc_panel_layer_zero'," + idx + ")");
        }
    }

    /**
     * 重測狀態
     *
     * @param r01MBean
     */
    private void saveTestInfoByRetest(Require01MBean r01MBean) {
        try {
            sendTestService.saveByRetestTestInfo(r01MBean.getRequire(),
                    editTestInfo,
                    loginBean.getUser(),
                    tempTestInfoTo);
        } catch (IllegalAccessException e) {
            log.debug("檢核失敗", e);
            r01MBean.getSendTestBottomMBean().recoveryView(r01MBean, editTestInfo);
            MessagesUtils.showError(e.getMessage());
        } catch (TestInfoEditExceptions e) {
            log.error("存檔失敗", e.getMessage());
            r01MBean.getSendTestBottomMBean().recoveryView(r01MBean, editTestInfo);
            MessagesUtils.showError(e.getMessage());
        } catch (Exception e) {
            log.error(r01MBean.getRequire().getRequireNo() + ":" + editTestInfo.getTestinfoNo() + ":" + loginBean.getUser().getName() + ":建立重測失敗..."
                    + e.getMessage(), e);
            r01MBean.getSendTestBottomMBean().recoveryView(r01MBean, editTestInfo);
            MessagesUtils.showError(e.getMessage());
        } finally {
            this.updateScreen(r01MBean);
            r01MBean.getSendTestBottomMBean().reloadWorkTestInfo(editTestInfo.getTestinfoNo());
        }
    }

    /**
     * 存檔
     *
     * @param r01MBean
     * @param trans2FB 是否轉FB
     */
    public void saveTestInfoEntity(Require01MBean r01MBean, Boolean trans2FB) {
        WorkTestInfo testInfo = null;
        try {
            testInfo = sendTestService.saveByNewTestInfo(r01MBean.getRequire(),
                    editTestInfo,
                    WkUserCache.getInstance().findBySid(SecurityFacade.getUserSid()));

//			// 推播通知
//			// 改由 settingSysNotify 處理
//			reqNotifyCustomizeService.createNotify(r01MBean.getRequire(), editTestInfo.getTheme(), loginBean.getUser(),
//			        NotifyType.SEND_TEST_NOTIFY, testInfo.getSendTestDep().getValue());

            log.info("{} 新增送測單:{}", SecurityFacade.getUserId(), testInfo.getTestinfoNo());
        } catch (ProcessRestException e) {
            log.error(r01MBean.getRequire().getRequireNo() + ":" + loginBean.getUser().getName() + ":建立送測失敗(流程異常)..." + e.getMessage(), e);
            r01MBean.reBuildeByUpdateFaild();
            MessagesUtils.showError("流程系統異常！！");
            return;
        } catch (IllegalAccessException e) {
            log.debug("檢核失敗", e);
            r01MBean.reBuildeByUpdateFaild();
            MessagesUtils.showError(e.getMessage());
        } catch (Exception e) {
            log.error(r01MBean.getRequire().getRequireNo() + ":" + loginBean.getUser().getName() + ":建立送測失敗..." + e.getMessage(), e);
            r01MBean.reBuildeByUpdateFaild();
            MessagesUtils.showError(e.getMessage());
            return;
        }

        if (trans2FB && testInfo != null) {
            try {
                log.info("{}執行轉FB from popup dialog, 送測單號:{}", SecurityFacade.getUserId(), testInfo.getTestinfoNo());
                sendTestService.createFBCase(r01MBean.getRequire(), testInfo, loginBean.getUser());
            } catch (Exception e) {
                log.error(r01MBean.getRequire().getRequireNo() + ":" + loginBean.getUser().getName() + ":送測轉FB失敗..." + e.getMessage(), e);
                String errorMsg = "建立失敗！\n請自行於FB SERVER建立完畢後，將fb case no輸入至送測單中。\n原因:" + e.getMessage();
                MessagesUtils.showError(errorMsg);
            }
        }

        r01MBean.getTraceMBean().clear();
        r01MBean.getSendTestBottomMBean().initTabInfo(r01MBean);
        r01MBean.getBottomTabMBean().resetTabIdx(r01MBean);
        r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.SEND_TEST_INFO);
        r01MBean.getTitleBtnMBean().clear();
        display.execute("hideLoad();accPanelSelectTab('st_acc_panel_layer_zero',0);" + ReqConfirmClientHelper.CLIENT_SRIPT_CLOSE_PANEL);
    }

    /**
     * 讀取 子程序 通知異動紀錄
     *
     * @param r01MBean
     * @param ptCheck
     */
    public void btnLoadSubNoticeInfo(Require01MBean r01MBean, WorkTestInfo testInfo) {
        r01MBean.loadSubNoticeInfo(testInfo.getSourceSid(), testInfo.getSid(),
                Lists.newArrayList(SubNoticeType.TEST_INFO_DEP));
    }

    public void onDateSelect(SelectEvent selectEvent) {
        Date date = (Date) selectEvent.getObject();
        if (sendTestService.isContainsQADepts(editTestInfo) && sendTestScheduleService.isFullByDate(date)) {
            MessagesUtils.showInfo("你所挑選的送測日排程已滿!");
        }
    }

    // ========================================================================
    // 送測單位元件
    // ========================================================================
    /** * 送測單位-編輯視窗 */
    @Getter
    private final String SEND_TEST_DEP_DLG_NAME = "SEND_TEST_DEP_DLG_NAME_" + this.getClass().getSimpleName();
    /** * 分享部門-編輯視窗內容區 */
    @Getter
    private final String SEND_TEST_DEP_DLG_CONTENT = "SEND_TEST_DEP_DLG_CONTENT_" + this.getClass().getSimpleName();
    /** * 分享部門-編輯視窗-部門選擇器ID */
    @Getter
    private final String SEND_TEST_DEP_PICKER_ID = "SEND_TEST_DEP_PICKER_ID_" + this.getClass().getSimpleName();
    /** * 分派單位選擇器 */
    @Getter
    private transient MultItemPickerComponent sendTestDepPicker;

    /** * 記錄開啟編輯的來源 */
    private transient String openDialogEventFrom = "";
    /** * Require01MBean */
    private transient Require01MBean require01MBean;

    /**
     * 開啟送測單位編輯視窗
     * 
     * @param require01MBean Require01MBean
     * @param openDialogFrom 動作觸發來源來原
     */
    public void sendTestDepOpenDialog(Require01MBean require01MBean, String openDialogEventFrom) {

        this.require01MBean = require01MBean;
        this.openDialogEventFrom = openDialogEventFrom;

        try {
            // 選擇器設定資料
            MultItemPickerConfig config = new MultItemPickerConfig();
            config.setTreeModePrefixName("單位");
            config.setContainFollowing(true);
            config.setEnableGroupMode(true); // 開啟群組功能
            config.setItemComparator(this.multItemPickerByOrgHelper.parpareComparator());
            // 傳入 ID , 自動清除 filter
            config.setComponentID(SEND_TEST_DEP_PICKER_ID);

            // 部分實作方法用 Default
            this.sendTestDepPickerCallback.setDefaultImplType(MultItemPickerCallbackDefaultImplType.DEP);

            // 選擇器初始化
            this.sendTestDepPicker = new MultItemPickerComponent(config, this.sendTestDepPickerCallback);

        } catch (Exception e) {
            String message = "通知單位選單初始化失敗!" + e.getMessage();
            MessagesUtils.showError(message);
            log.error(message, e);
            return;
        }

        // 更新畫面
        this.displayController.update(this.SEND_TEST_DEP_DLG_CONTENT);
        // 開啟 dialog
        this.displayController.showPfWidgetVar(this.SEND_TEST_DEP_DLG_NAME);
    }

    public void sendTestDepOpenDialogByEditOnly(Require01MBean r01MBean, WorkTestInfo testInfo) {
        this.editTestInfo = sendTestService.findByTestinfoNo(testInfo.getTestinfoNo());
        this.tempTestInfoTo = sendTestService.findToByTestinfoNo(testInfo.getTestinfoNo());
        this.sendTestDepOpenDialog(r01MBean, "EDIT_ONLY");
    }

    /**
     * 確認
     * 
     * @param isPreConfirm true:call by view , false:call by confirm callback
     */
    public void sendTestDepConfirm(boolean isPreConfirm) {

        // 防呆
        if (this.editTestInfo == null
                || this.require01MBean == null
                || this.require01MBean.getRequire() == null
                || this.require01MBean.getRequire().getCreateDep() == null) {
            MessagesUtils.showWarn(WkMessage.NEED_RELOAD);
            return;
        }

        // ====================================
        // 檢察為空
        // ====================================
        if (WkStringUtils.isEmpty(this.sendTestDepPicker.getSelectedSid())) {
            MessagesUtils.showError("未挑選送測部門！！");
            return;
        }

        // ====================================
        // 取得異動前後單位清單
        // ====================================
        // 取得選單上已選擇的單位
        List<Integer> afterDepSids = this.sendTestDepPicker.getSelectedSid().stream()
                .filter(userSid -> WkStringUtils.isNumber(userSid))
                .map(userSid -> Integer.parseInt(userSid))
                .collect(Collectors.toList());

        // 取得異動前單位
        List<Integer> beforeDepSids = Lists.newArrayList();
        if (this.editTestInfo.getSendTestDep() != null
                && this.editTestInfo.getSendTestDep().getValue() != null) {
            // 轉數字
            beforeDepSids = this.editTestInfo.getSendTestDep().getValue().stream()
                    .distinct()
                    .filter(userSid -> WkStringUtils.isNumber(userSid))
                    .map(userSid -> Integer.parseInt(userSid))
                    .collect(Collectors.toList());
        }

        // 比對是否未異動
        if (WkCommonUtils.compare(beforeDepSids, afterDepSids)) {
            MessagesUtils.showInfo("未異動送測單位!");
            return;
        }

        // ====================================
        // 檢察 1.需求單位 2.QA 單位至少必須存在一個
        // ====================================
        List<Integer> mustBeAliveDepSids = Lists.newArrayList();
        // 1.需求單位
        mustBeAliveDepSids.add(this.require01MBean.getRequire().getCreateDep().getSid());
        // 2.QA單位
        mustBeAliveDepSids.addAll(this.workBackendParamHelper.getQADepSids());
        // 逐筆檢察
        boolean isAlive = false;
        for (Integer depSids : mustBeAliveDepSids) {
            if (afterDepSids.contains(depSids)) {
                isAlive = true;
                break;
            }
        }
        if (!isAlive) {
            String message = ""
                    + "<span class='WS1-1-2b'>"
                    + "不可同時移除需求單位與QA軟體測試單位!，以下單位至少必須存在一個!："
                    + "</span>"
                    + "<br/><br/>"
                    + WkCommonContentUtils.prepareDepNameWithDepByDataListStyle(
                            mustBeAliveDepSids,
                            OrgLevel.MINISTERIAL,
                            true,
                            ">&nbsp;",
                            "",
                            false)
                    + "<br/><br/>";

            MessagesUtils.showInfo(message);
            return;
        }

        // ====================================
        // 確認訊息
        // ====================================
        // 不為新增時, 跳出異動的確認訊息
        if (isPreConfirm && WkStringUtils.notEmpty(this.editTestInfo.getSid())) {
            String drffMessage = WkCommonContentUtils.prepareDiffMessageForDepartment(
                    beforeDepSids,
                    afterDepSids,
                    "新增送測單位",
                    "移除送測單位");

            drffMessage = "<span class='WS1-1-3b'>異動【送測單位】結果如下：</span>"
                    + "<br/><br/>"
                    + drffMessage
                    + "<br/><br/>";

            this.confirmCallbackDialogController.showConfimDialog(
                    drffMessage,
                    Lists.newArrayList(),
                    () -> this.sendTestDepConfirm(false));
            return;
        }

        // ====================================
        // 更新主檔容器資料內的資料
        // ====================================
        this.editTestInfo.getSendTestDep().setValue(
                afterDepSids.stream().map(each -> each + "").collect(Collectors.toList()));

        // ====================================
        // 單獨編輯通知單位 (非新增、非ON程式編輯模式)
        // ====================================
        if (this.editTestInfo.getSid() != null && !"EDIT_ALL".equals(openDialogEventFrom)) {
            try {

                // 儲存編輯資料
                sendTestService.saveOrUpdate(editTestInfo, loginBean.getUser(), "sendTestDepts",PermissionHelper.getInstance().isQAManager());

                try {
                    this.sysNotifyHelper.processForWorkTest(
                            this.require01MBean.getRequire().getSid(),
                            this.editTestInfo.getTheme(),
                            beforeDepSids.stream().map(each -> each + "").collect(Collectors.toList()),
                            afterDepSids.stream().map(each -> each + "").collect(Collectors.toList()),
                            SecurityFacade.getUserSid());
                } catch (Exception e) {
                    log.error("執行系統通知失敗!" + e.getMessage(), e);
                }

                // 取得最新資料
                this.editTestInfo = sendTestService.findByTestinfoNo(this.editTestInfo.getTestinfoNo());

                // 更新畫面
                this.updateScreen(this.require01MBean);

                // 更新單張送測單
                display.update("wt_detail");

            } catch (Exception e) {
                String message = "通知單位設定時發生錯誤! [" + e.getMessage() + "]";
                log.error(message, e);
                MessagesUtils.showError(message);
                return;
            }
        }

        // ====================================
        // 關閉對話框
        // ====================================
        this.displayController.hidePfWidgetVar(this.SEND_TEST_DEP_DLG_NAME);
    }

    /**
     * 準備通知單位 tool tip 內容
     * 
     * @param ptCheck
     * @return
     */
    public String prepareSendTestDepsTooltip(WorkTestInfo workTestInfo) {
        if (workTestInfo != null
                && workTestInfo.getSendTestDep() != null
                && workTestInfo.getSendTestDep().getValue() != null) {

            List<Integer> depSids = this.editTestInfo.getSendTestDep().getValue().stream()
                    .distinct()
                    .filter(userSid -> WkStringUtils.isNumber(userSid))
                    .map(userSid -> Integer.parseInt(userSid))
                    .collect(Collectors.toList());

            return WkOrgUtils.prepareDepsNameByTreeStyle(Lists.newArrayList(depSids), 50);
        }
        return "";
    }

    /**
     * 分派單位
     */
    private final MultItemPickerCallback sendTestDepPickerCallback = new MultItemPickerCallback() {

        /**
         * 
         */
        private static final long serialVersionUID = 4160994226592879343L;

        /** * 取得已選擇部門 */
        @Override
        public List<WkItem> prepareSelectedItems() throws SystemDevelopException {

            // 防呆
            if (editTestInfo == null || editTestInfo.getSendTestDep() == null || editTestInfo.getSendTestDep().getValue() == null) {
                return Lists.newArrayList();
            }

            // 由外部取回已選擇部門, 並轉換為 WkItem 物件
            return editTestInfo.getSendTestDep().getValue().stream().distinct() // 去重複
                    .filter(depSid -> WkStringUtils.isNumber(depSid)) // 防呆檢察非數字
                    .map(depSid -> multItemPickerByOrgHelper.createDepItemByDepSid(Integer.valueOf(depSid))) // 轉 wkitem
                    .collect(Collectors.toList());
        }

        /*** 準備 不可移除 的單位 */
        @Override
        public List<String> prepareDisableItemSids() throws SystemDevelopException {
            // 說明:若已納排, 則不可移除 QA 單位
            // 已審核
            boolean isApproved = WorkTestInfoStatus.APPROVED.equals(editTestInfo.getQaAuditStatus());
            // 已加入排程
            boolean isJoinSchedule = WorkTestInfoStatus.JOIN_SCHEDULE.equals(editTestInfo.getQaScheduleStatus());

            if (isApproved && isJoinSchedule) {
                // 由參數中取得 QA 單位
                return workBackendParamHelper.getQADepSids().stream().map(each -> each + "").collect(Collectors.toList());

            }

            return Lists.newArrayList();
        }

    };

}
