/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require.pt;

import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.tech.request.logic.service.FormViewUpdateService;
import com.cy.tech.request.logic.service.pt.PtHistoryService;
import com.cy.tech.request.logic.service.pt.PtReplyService;
import com.cy.tech.request.logic.service.pt.PtService;
import com.cy.tech.request.vo.pt.PtAlreadyReply;
import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.pt.PtHistory;
import com.cy.tech.request.vo.pt.PtReply;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.require.IRequireBottom;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.utils.WkEntityUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;
import org.primefaces.event.TabChangeEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class PtBottomMBean implements IRequireBottom, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6335847666339679440L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private FormViewUpdateService formViewUpdateService;
    @Autowired
    transient private PtService ptService;
    @Autowired
    transient private PtReplyService ptrService;
    @Autowired
    transient private PtHistoryService pthService;
    @Autowired
    transient private WkEntityUtils entityUtils;
    @Autowired
    transient private WkJsoupUtils jsoupUtils;

    @Getter
    private List<PtCheck> ptChecks;
    @Getter
    private PtReply editReply;
    @Getter
    private PtAlreadyReply editAlreadyReply;
    /** 歷程用 */
    @Getter
    private PtHistory editHistory;

    private PtReply backupReply;
    private PtAlreadyReply backupAlreadyReply;

    @Getter
    private Boolean isEditReply;
    @Getter
    private Boolean isEditAlreadyReply;

    @PostConstruct
    public void init() {
        ptChecks = null;

        editReply = new PtReply();
        editAlreadyReply = new PtAlreadyReply();
        editHistory = new PtHistory();

        backupReply = new PtReply();
        backupAlreadyReply = new PtAlreadyReply();

        isEditReply = Boolean.FALSE;
        isEditAlreadyReply = Boolean.FALSE;
    }

    @Override
    public void initTabInfo(Require01MBean r01MBean) {
        Require require = r01MBean.getRequire();
        if (require == null || !require.getHasPrototype()) {
            return;
        }
        if (!r01MBean.getBottomTabMBean().currentTabByName(RequireBottomTabType.PT_CHECK_INFO)) {
            return;
        }
        ptChecks = ptService.initTabInfo(require);
    }

    public List<PtCheck> findPtChecks(Require01MBean r01MBean) {
        if (ptChecks == null) {
            this.initTabInfo(r01MBean);
        }
        return ptChecks;
    }

    public void updateTabInfo(Require require) {
        ptChecks = ptService.initTabInfo(require);
    }

    public PtCheck findRefreByIdx() {
        if (!Faces.getRequestParameterMap().containsKey("ptCheckIdx")) {
            return null;
        }
        int readInboxIdx = Integer.valueOf(Faces.getRequestParameterMap().get("ptCheckIdx"));
        return this.ptChecks.get(readInboxIdx);
    }

    public List<PtHistory> findHistorys(PtCheck ptCheck) {
        return pthService.findHistorys(ptCheck);
    }

    public List<PtAlreadyReply> findAlreadyReplys(PtHistory history) {
        if (history == null || history.getReply() == null) {
            return Lists.newArrayList();
        }
        return ptrService.findAlreadyReplys(history.getReply());
    }

    public void initReply(PtCheck ptCheck) {
        isEditReply = Boolean.FALSE;
        editReply = ptrService.createEmptyReply(ptCheck, loginBean.getUser());
    }

    public void clickSaveReply() {
        try {
            Preconditions.checkArgument(!Strings.isNullOrEmpty(jsoupUtils.clearCssTag(editReply.getContentCss())), "回覆不可為空白，請重新輸入！！");
            display.hidePfWidgetVar("pt_reply_dlg_wv");
            if (editReply == null || editReply.getPtCheck() == null) {
                MessagesUtils.showWarn(WkMessage.NEED_RELOAD);
                if (editReply == null) {
                    log.warn("editReply==null");
                } else if (editReply.getPtCheck() == null) {
                    log.warn("editReply.getPtCheck()==null");
                }
                return;
            }
            
            if(editReply.getPtCheck().getEstablishDate()==null) {
                PtCheck ptCheck = this.ptService.findByPtNo(editReply.getPtCheck().getPtNo());
                if(ptCheck==null) {
                    MessagesUtils.showWarn(WkMessage.NEED_RELOAD);
                    log.warn(editReply.getPtCheck().getPtNo() +" not find!" );
                    return;
                }
                editReply.setPtCheck(ptCheck);
            }

            if (isEditReply) {
                ptrService.saveByEditReply(editReply, loginBean.getUser());
                isEditReply = Boolean.FALSE;
            } else {
                ptrService.saveByNewReply(editReply);
            }
        } catch (IllegalArgumentException e) {
            log.debug("檢核失敗", e);
            MessagesUtils.showError(e.getMessage());
        } catch (IllegalAccessException e) {
            log.error(e.getMessage(), e);
            this.recoveryView(null, editReply.getPtCheck());
            MessagesUtils.showError(e.getMessage());
        }
    }

    public void initEditReply(PtReply reply) {
        this.editReply = reply;
        isEditReply = Boolean.TRUE;
        entityUtils.copyProperties(editReply, backupReply);
    }

    public void cancelEditReply() {
        if (isEditReply) {
            entityUtils.copyProperties(backupReply, editReply);
            isEditReply = Boolean.FALSE;
        }
    }

    public void initAlreadyReply(PtReply reply) {
        isEditAlreadyReply = Boolean.FALSE;
        editAlreadyReply = ptrService.createEmptyAlreadyReply(reply, loginBean.getUser());
    }

    public void clickSaveAlreadyReply() {
        try {
            Preconditions.checkArgument(!Strings.isNullOrEmpty(jsoupUtils.clearCssTag(editAlreadyReply.getContentCss())), "回覆不可為空白，請重新輸入！！");
            if (isEditAlreadyReply) {
                ptrService.saveByEditAlreadyReply(editAlreadyReply, loginBean.getUser());
                isEditAlreadyReply = Boolean.FALSE;
            } else {
                ptrService.saveByNewAlreadyReply(editAlreadyReply);
            }
            display.hidePfWidgetVar("pt_reply_already_dlg_wv");
            this.whenSaveAReplyOpenTab(editAlreadyReply);
        } catch (IllegalArgumentException e) {
            log.debug(e.getMessage(), e);
            MessagesUtils.showError(e.getMessage());
        }
    }

    private void whenSaveAReplyOpenTab(PtAlreadyReply aReply) {
        PtCheck ptCheck = aReply.getPtCheck();
        PtReply reply = aReply.getReply();
        int idx = ptCheck.getHistorys().indexOf(reply.getHistory());
        display.execute("accPanelSelectTab('pt_acc_panel_layer_one_" + ptCheck.getSid() + "'," + idx + ")");
    }

    public void initAlreadyReplyEdit(PtAlreadyReply aReply) {
        this.editAlreadyReply = aReply;
        isEditAlreadyReply = Boolean.TRUE;
        entityUtils.copyProperties(editAlreadyReply, backupAlreadyReply);
    }

    public void cancelEditAlreadyReply() {
        if (isEditAlreadyReply) {
            entityUtils.copyProperties(backupAlreadyReply, editAlreadyReply);
            isEditAlreadyReply = Boolean.FALSE;
            this.whenSaveAReplyOpenTab(editAlreadyReply);
        }
    }

    public void createEmptyHistory(PtCheck ptCheck) {
        editHistory = pthService.createEmptyHistory(ptService.copy(ptCheck), loginBean.getUser());
    }

    public void openAttachByHistory(PtHistory history, PtHistoryAttachMBean pthaMBean) {
        pthaMBean.init(history);
        this.editHistory = history;
    }

    /**
     * 重新實作
     *
     * @param r01MBean
     */
    public void clickRedo(Require01MBean r01MBean) {
        try {
            ptService.executeRedo(r01MBean.getRequire(), editHistory, loginBean.getUser());
            r01MBean.getRequire().setRedoCode(Boolean.TRUE);
            this.initTabInfo(r01MBean);
            this.changeItemByItems(editHistory.getPtCheck());
            r01MBean.getTraceMBean().clear();
            r01MBean.getTitleBtnMBean().clear();
            display.hidePfWidgetVar("pt_redo_dlg_wv");
        } catch (ProcessRestException | IllegalAccessException e) {
            log.error(e.getMessage(), e);
            this.recoveryView(r01MBean, editHistory.getPtCheck());
            MessagesUtils.showError(e.getMessage());
        }
    }

    private void changeItemByItems(PtCheck ptCheck) {
        int idx = this.ptChecks.indexOf(ptCheck);
        ptChecks.set(idx, ptCheck);
    }

    /**
     * 點擊功能符合需求鍵
     *
     * @param r01MBean
     */
    public void clickFunctionConform(Require01MBean r01MBean, PtCheck ptCheck) {
        try {
            ptService.executorFunctionConform(r01MBean.getRequire(), ptService.copy(ptCheck), loginBean.getUser());
            r01MBean.getTitleBtnMBean().clear();
            r01MBean.getTraceMBean().clear();
            r01MBean.getBottomTabMBean().resetTabIdx(r01MBean);
            r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.TRACE);
        } catch (IllegalAccessException e) {
            log.debug("檢核失敗", e);
            this.recoveryView(r01MBean, ptCheck);
            MessagesUtils.showError(e.getMessage());
        }
    }

    public void updateReadRecord(TabChangeEvent event) {
        try {
            PtCheck pc = (PtCheck) event.getData();
            formViewUpdateService.updatePtRecord(pc, loginBean.getUser());
        } catch (Exception e) {
            log.error("更新原型確認閱讀記錄失敗。" + e.getMessage(), e);
        }
    }

    public void reBuildPtCheck(PtCheck ptCheck) {
        if (ptCheck == null || Strings.isNullOrEmpty(ptCheck.getPtNo())) {
            return;
        }
        PtCheck nPt = ptService.findByPtNo(ptCheck.getPtNo());
        int idx = this.ptChecks.indexOf(ptCheck);
        ptChecks.set(idx, nPt);
    }

    public void recoveryView(Require01MBean r01MBean, PtCheck ptCheck) {
        if (r01MBean == null) {
            r01MBean = (Require01MBean) Faces.getApplication().getELResolver().getValue(Faces.getELContext(), null, "require01MBean");
        }
        r01MBean.reBuildeRequire();
        this.reBuildPtCheck(ptCheck);
        r01MBean.getTraceMBean().clear();
        r01MBean.getTitleBtnMBean().clear();
    }

}
