package com.cy.tech.request.web.controller.enums;

import com.cy.work.common.enums.ReadRecordType;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.SelectItem;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kasim
 */
@Controller
public class ReadRecordTypeMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5073349006378375983L;
    private static final List<ReadRecordType> EXCLUDE_STATUS = Lists.newArrayList(ReadRecordType.SYSTEM_UPDATE);
    

    public SelectItem[] getValues() {
        SelectItem[] items = new SelectItem[ReadRecordType.values().length];
        int i = 0;
        for (ReadRecordType each : ReadRecordType.values()) {
            items[i++] = new SelectItem(each, each.getValue());
        }
        return items;
    }

    public SelectItem[] getValuesExcept(String... exceptValues) {
        List<String> exceptList = Lists.newArrayList(exceptValues);
        int exceptSize = exceptList.isEmpty() || Strings.isNullOrEmpty(exceptList.get(0)) ? 0 : exceptList.size();
        SelectItem[] items = new SelectItem[ReadRecordType.values().length - exceptSize];
        int i = 0;
        for (ReadRecordType each : ReadRecordType.values()) {
            if (!exceptList.contains(each.name())) {
                items[i++] = new SelectItem(each, each.getValue());
            }
        }
        return items;
    }
    
    public SelectItem[] buildReadRecordTypes(){
        List<ReadRecordType> resultList = Lists.newArrayList(ReadRecordType.values());
        resultList.removeAll(EXCLUDE_STATUS);
        SelectItem[] items = new SelectItem[resultList.size()];
        resultList.forEach(each -> items[resultList.indexOf(each)] = new SelectItem(each, each.getValue()));
        return items;
    }
}
