/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.enums;

import com.cy.tech.request.web.pf.utils.CommonBean;
import com.cy.work.customer.vo.enums.EnableType;
import java.io.Serializable;
import javax.faces.model.SelectItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kasim
 */
@Controller
public class EnableTypeBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7481376079292022568L;
    @Autowired
    transient private CommonBean common;

    public SelectItem[] getValues() {
        SelectItem[] items = new SelectItem[EnableType.values().length];
        int i = 0;
        for (EnableType each : EnableType.values()) {
            String label = common.get(each);
            items[i++] = new SelectItem(each, label);
        }
        return items;
    }
}
