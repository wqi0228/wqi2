/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.view.to.search.query;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.cy.commons.vo.Org;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.vo.enums.Search02QueryColumn;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.view.component.CategoryTreeComponent;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterArrayStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterDetailVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterVO;
import com.cy.tech.request.web.listener.CategoryTreeCallBack;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.listener.ReportCustomFilterCallback;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Slf4j
public class SearchQuery02CustomFilter implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8497944985961976982L;
    /** 登入者Sid */
    private Integer loginUserSid;
    @Getter
    private SearchQuery02 tempSearchQuery02;
    /** 該登入者在需求單,自訂查詢條件List */
    private List<ReportCustomFilterVO> reportCustomFilterVOs;
    /** 該登入者在需求單,挑選自訂查詢條件物件 */
    private ReportCustomFilterVO selReportCustomFilterVO;
    /** ReportCustomFilterLogicComponent */
    private ReportCustomFilterLogicComponent reportCustomFilterLogicComponent;
    /** 類別樹 Component */
    @Getter
    private CategoryTreeComponent categoryTreeComponent;
    /** MessageCallBack */
    private MessageCallBack messageCallBack;
    /** DisplayController */
    private DisplayController display;
    /** OrganizationService */
    // private OrganizationService orgService;
    /** ReportCustomFilterCallback */
    private ReportCustomFilterCallback reportCustomFilterCallback;

    /**
     * 可閱單位
     */
    private Set<Integer> canViewDepSids = Sets.newHashSet();

    public SearchQuery02CustomFilter(
            String compId,
            Integer loginUserSid,
            ReportCustomFilterLogicComponent reportCustomFilterLogicComponent,
            MessageCallBack messageCallBack,
            DisplayController display,
            ReportCustomFilterCallback reportCustomFilterCallback,
            List<Long> roleSids,
            Org dep,
            Set<Integer> canViewDepSids) {
        this.loginUserSid = loginUserSid;
        this.reportCustomFilterLogicComponent = reportCustomFilterLogicComponent;
        this.messageCallBack = messageCallBack;
        this.display = display;
        this.categoryTreeComponent = new CategoryTreeComponent(categoryTreeCallBack);
        this.reportCustomFilterCallback = reportCustomFilterCallback;
        this.tempSearchQuery02 = new SearchQuery02(ReportType.SIGN);
        this.canViewDepSids = canViewDepSids;
    }

    public void loadDefaultSetting(
            Set<Integer> requireDepts,
            SearchQuery02 searchQuery) {

        searchQuery.clear(requireDepts);

        reportCustomFilterVOs = reportCustomFilterLogicComponent.getReportCustomFilter(Search02QueryColumn.Search02Query, loginUserSid);
        if (reportCustomFilterVOs != null && !reportCustomFilterVOs.isEmpty()) {
            selReportCustomFilterVO = reportCustomFilterVOs.get(0);
            loadSettingData(searchQuery);
        }
    }

    /** 載入挑選的自訂搜尋條件 */
    public void loadSettingData(SearchQuery02 searchQuery) {
        selReportCustomFilterVO.getReportCustomFilterDetailVOs().forEach(item -> {
            if (Search02QueryColumn.DemandType.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDemandType(item, searchQuery);
            } else if (Search02QueryColumn.WaitApprovingPerson.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingWaitApprovingPerson(item, searchQuery);
            } else if (Search02QueryColumn.Department.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingDepartment(item, searchQuery);
            } else if (Search02QueryColumn.DemandPerson.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDemandPerson(item, searchQuery);
            } else if (Search02QueryColumn.CategoryCombo.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingCategoryCombo(item, searchQuery);
            } else if (Search02QueryColumn.ApprovingProcess.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingApprovingProcess(item, searchQuery);
            } else if (Search02QueryColumn.DateIndex.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDateIndex(item, searchQuery);
            } else if (Search02QueryColumn.SearchText.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingFuzzyText(item, searchQuery);
            }
        });
    }

    /**
     * 塞入需求單位審核狀態
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingApprovingProcess(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery02 searchQuery) {
        try {
            searchQuery.getSelectStatuses().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQuery.getSelectStatuses().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingCategoryCombo", e);
        }
    }

    /**
     * 塞入立單區間Type
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDateIndex(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery02 searchQuery) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQuery.setDateTypeIndex(Integer.valueOf(reportCustomFilterStringVO.getValue()));
            switch (searchQuery.getDateTypeIndex()) {
            case 0:
                searchQuery.changeDateIntervalPreMonth();
                break;
            case 1:
                searchQuery.changeDateIntervalThisMonth();
                break;
            case 2:
                searchQuery.changeDateIntervalNextMonth();
                break;
            case 3:
                searchQuery.changeDateIntervalToDay();
                break;
            default:
                break;
            }
        } catch (Exception e) {
            log.error("settingFuzzyText", e);
        }
    }

    /**
     * 塞入需求類別
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingCategoryCombo(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery02 searchQuery) {
        try {
            searchQuery.getSmallDataCateSids().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQuery.getSmallDataCateSids().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingCategoryCombo", e);
        }
    }

    /**
     * 塞入模糊搜尋
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingFuzzyText(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery02 searchQuery) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQuery.setFuzzyText(reportCustomFilterStringVO.getValue());
        } catch (Exception e) {
            log.error("settingFuzzyText", e);
        }
    }

    /**
     * 塞入需求人員
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandPerson(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery02 searchQuery) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQuery.setTrCreatedUserName(reportCustomFilterStringVO.getValue());
        } catch (Exception e) {
            log.error("trCreatedUserName", e);
        }
    }

    /**
     * 塞入需求單位
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDepartment(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery02 searchQuery) {
        try {
            searchQuery.getRequireDepts().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (WkStringUtils.notEmpty(reportCustomFilterArrayStringVO.getArrayStrings())) {
                Set<Integer> customrDepSids = reportCustomFilterArrayStringVO.getArrayStrings().stream()
                        .filter(depStr -> WkStringUtils.isNumber(depStr))
                        .map(depStr -> Integer.parseInt(depStr))
                        .filter(depSid -> this.canViewDepSids.contains(depSid)) // 自訂條件需存在於可閱單位清單
                        .collect(Collectors.toSet());

                searchQuery.setRequireDepts(customrDepSids);
            }
        } catch (Exception e) {
            log.error("settingDepartment", e);
        }
    }

    /**
     * 塞入待審核人員
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingWaitApprovingPerson(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery02 searchQuery) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQuery.setPendingSignOnUserName(reportCustomFilterStringVO.getValue());
        } catch (Exception e) {
            log.error("settingDemandType", e);
        }
    }

    /**
     * 塞入需求類別
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandType(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery02 searchQuery) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQuery.setSelectBigCategorySid(reportCustomFilterStringVO.getValue());
        } catch (Exception e) {
            log.error("settingDemandType", e);
        }
    }

    /** 儲存自訂搜尋條件 */
    public void saveReportCustomFilter() {
        try {
            ReportCustomFilterDetailVO demandType = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search02QueryColumn.DemandType,
                    (tempSearchQuery02.getSelectBigCategorySid() != null) ? tempSearchQuery02.getSelectBigCategorySid() : "");
            ReportCustomFilterDetailVO waitAppringPerson = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search02QueryColumn.WaitApprovingPerson,
                    (tempSearchQuery02.getPendingSignOnUserName() != null) ? tempSearchQuery02.getPendingSignOnUserName() : "");
            ReportCustomFilterDetailVO department = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search02QueryColumn.Department, tempSearchQuery02.getRequireDepts());
            ReportCustomFilterDetailVO demandPerson = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search02QueryColumn.DemandPerson, tempSearchQuery02.getTrCreatedUserName());
            ReportCustomFilterDetailVO searchText = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search02QueryColumn.SearchText, tempSearchQuery02.getFuzzyText());
            ReportCustomFilterDetailVO categoryCombo = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search02QueryColumn.CategoryCombo, tempSearchQuery02.getSmallDataCateSids());
            ReportCustomFilterDetailVO dateIndex = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search02QueryColumn.DateIndex,
                    String.valueOf(tempSearchQuery02.getDateTypeIndex()));
            ReportCustomFilterDetailVO approvingProcess = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search02QueryColumn.ApprovingProcess,
                    tempSearchQuery02.getSelectStatuses());
            List<ReportCustomFilterDetailVO> saveDetails = Lists.newArrayList();
            saveDetails.add(demandType);
            saveDetails.add(waitAppringPerson);
            saveDetails.add(department);
            saveDetails.add(demandPerson);
            saveDetails.add(searchText);
            saveDetails.add(categoryCombo);
            saveDetails.add(dateIndex);
            saveDetails.add(approvingProcess);
            reportCustomFilterLogicComponent.saveReportCustomFilter(Search02QueryColumn.Search02Query, loginUserSid, (selReportCustomFilterVO != null) ? selReportCustomFilterVO.getIndex() : null,
                    true, saveDetails);
            reportCustomFilterCallback.reloadDefault("");
            display.update("headerTitle");
            display.execute("doSearchData();");
            display.hidePfWidgetVar("dlgReportCustomFilter");
        } catch (Exception e) {
            this.messageCallBack.showMessage(e.getMessage());
            log.error("saveReportCustomFilter ERROR", e);
        }
    }

    /** 類別樹 Component CallBack */
    private final CategoryTreeCallBack categoryTreeCallBack = new CategoryTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 7528320787160624291L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelCate() {
            categoryTreeComponent.selCate();
            tempSearchQuery02.getBigDataCateSids().clear();
            tempSearchQuery02.getMiddleDataCateSids().clear();
            tempSearchQuery02.getSmallDataCateSids().clear();
            tempSearchQuery02.getBigDataCateSids().addAll(Lists.newArrayList(categoryTreeComponent.getBigDataCateSids()));
            tempSearchQuery02.getMiddleDataCateSids().addAll(Lists.newArrayList(categoryTreeComponent.getMiddleDataCateSids()));
            tempSearchQuery02.getSmallDataCateSids().addAll(Lists.newArrayList(categoryTreeComponent.getSmallDataCateSids()));
        }
    };

    /**
     * 開啟 類別樹
     */
    public void btnOpenCategoryTree() {
        try {
            categoryTreeComponent.init();
            categoryTreeComponent.selectedItem(tempSearchQuery02.getSmallDataCateSids());
            display.showPfWidgetVar("defaultDlgCate");
        } catch (Exception e) {
            log.error("btnOpenCategoryTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }
}
