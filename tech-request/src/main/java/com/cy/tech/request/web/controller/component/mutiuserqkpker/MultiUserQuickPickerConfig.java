/**
 * 
 */
package com.cy.tech.request.web.controller.component.mutiuserqkpker;

import java.io.Serializable;
import java.util.Set;

import com.cy.work.common.enums.ShowMode;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author allen1214_wu
 */
@NoArgsConstructor
@Getter
@Setter
public class MultiUserQuickPickerConfig implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8377697759336695697L;

    /**
     * 是否開啟群組功能
     */
    private boolean enableGroupMode = false;

    /**
     * 已選區預設顯示模式
     */
    private ShowMode selectedAreaDefaultShowMode = ShowMode.LIST;
    
    /**
     * 預設展開的 user 列表
     */
    private Set<Integer> defaultExpandUserSids = Sets.newHashSet();
    
    /**
     * 
     */
    private String userNodeIcon = "fa-user";
    
    /**
     * 主管標記
     */
    private String managerMark = "<i class='fa fa-star' aria-hidden='true'></i>";

    /**
     * 代理主管標記
     */
    private String agentMark = "(兼)";
}
