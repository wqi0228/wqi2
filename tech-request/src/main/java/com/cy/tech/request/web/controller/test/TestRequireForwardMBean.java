package com.cy.tech.request.web.controller.test;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkRoleCache;
import com.cy.work.common.cache.WkUserWithRolesCache;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Controller
@Scope("view")
public class TestRequireForwardMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4342465802128911353L;

    // ========================================================================
    // service
    // ========================================================================
    @Autowired
    transient private NativeSqlRepository nativeSqlRepository;

    // ========================================================================
    //
    // ========================================================================
    /**
     * 需求單號
     */
    @Getter
    @Setter
    private String requireNo = "";

    private final String adminRoleName = "需求單系統管理員";

    // ========================================================================
    //
    // ========================================================================
    /**
     * 
     */
    @PostConstruct
    public void init() {

        List<Long> roleSids = WkRoleCache.getInstance().findSidByRoleNameAndCompID(adminRoleName, SecurityFacade.getCompanyId());
        if (WkStringUtils.isEmpty(roleSids)) {
            MessagesUtils.showWarn("未建立角色『" + adminRoleName + "』");
            return;
        }

        if (roleSids.size() > 1) {
            MessagesUtils.showWarn("找到多個角色『" + adminRoleName + "』");
            return;
        }

        List<Integer> userSids = WkUserWithRolesCache.getInstance().findUserByRoleSid(roleSids.get(0));

        if (WkStringUtils.isEmpty(userSids)) {
            MessagesUtils.showWarn("角色『" + adminRoleName + "』找不到非停用的成員");
            return;
        }

        List<Map<String, Object>> dataRows = this.nativeSqlRepository.selectRowsBySingleField(
                "tr_require",
                Lists.newArrayList("require_no", "create_usr", "create_dt"),
                "create_usr",
                userSids);

        if (WkStringUtils.isEmpty(dataRows)) {
            MessagesUtils.showWarn("未找到『" + adminRoleName + "』開的單據");
            return;
        }

        dataRows = dataRows.stream()
                .sorted(Comparator.comparing(
                        rowDataMap -> {
                            if (rowDataMap.get("create_dt") == null) {
                                //為草稿, 排最後 (日期最小)
                                return new Date(0);
                            }
                            return (Date) rowDataMap.get("create_dt");
                        },
                        Comparator.reverseOrder()))
                .collect(Collectors.toList());

        // 預設『需求單系統管理者』最後一筆
        this.requireNo = dataRows.get(0).get("require_no") + "";

        // 但有登入者開的單優先
        for (Map<String, Object> dataMap : dataRows) {
            if (WkCommonUtils.compareByStr(SecurityFacade.getUserSid(), dataMap.get("create_usr"))) {
                this.requireNo = dataMap.get("require_no") + "";
                return;
            }
        }
    }
}
