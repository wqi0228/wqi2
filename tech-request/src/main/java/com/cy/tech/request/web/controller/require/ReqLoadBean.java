/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require;

import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.anew.manager.TrIssueMappTransManager;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.URLService.URLServiceAttr;
import com.cy.tech.request.logic.service.helper.URLHelper;
import com.cy.tech.request.logic.service.send.test.SendTestService;
import com.cy.tech.request.vo.constants.ReqPermission;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.web.anew.config.ReqWebConstants;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.search.TableUpDownBean;
import com.cy.tech.request.web.controller.search.WorkTestUpDownBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.cache.WkUserWithRolesCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.mapp.create.trans.vo.MappCreateTrans;
import com.cy.work.notify.logic.manager.WorkNotifyManager;
import com.cy.work.notify.vo.enums.NotifyType;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;

import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 需求單載入管理
 *
 * @author shaun
 */
@Slf4j
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class ReqLoadBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7792028619332835119L;
    public final static String TAB_TYPE = "TAB_TYPE";
    public final static String TAB_SID = "TAB_SID";

    @Autowired
    private ReqWebConstants reqWebConstants;
    @Autowired
    private TrIssueMappTransManager issueMappTransManager;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private SendTestService sendTestService;
    @Autowired
    transient private WorkTestUpDownBean workTestUpDownBean;
    @Autowired
    transient private URLService urlService;
    @Autowired
    transient private URLHelper uRLHelper;
    @Autowired
    transient private DisplayController display;
    @Autowired
    private WorkNotifyManager notifyManager;

    /**
     * 載入需求單模版畫面
     *
     * @param require
     * @param executor
     */
    public void reloadReqForm(Require require, User executor) {
        if (require == null) {
            return;
        }
        Require01MBean r01MBean = reloadReqFormInfo(require, executor);
        r01MBean.getBottomTabMBean().resetTabIdx(r01MBean);
        r01MBean.getBottomTabMBean().changeTabByTabAuto();
    }

    /**
     * 載入需求單模版畫面
     *
     * @param require
     * @param executor
     */
    public void reloadReqFormByAssign(Require require, User executor) {
        if (require == null) {
            return;
        }
        Require01MBean r01MBean = reloadReqFormInfo(require, executor);
        r01MBean.getBottomTabMBean().resetTabIdx(r01MBean);
        r01MBean.getBottomTabMBean().changeTabByTabAuto();
        display.execute("moveScrollTop();");
    }

    /**
     * 載入需求單模版畫面(指定TAB)
     *
     * @param require
     * @param executor
     * @param tabType
     */
    public void reloadReqForm(Require require, User executor, RequireBottomTabType tabType) {
        if (require == null) {
            return;
        }
        Require01MBean r01MBean = reloadReqFormInfo(require, executor);
        r01MBean.initTabInfo(tabType);
        r01MBean.getBottomTabMBean().resetTabIdx(r01MBean);
        r01MBean.getBottomTabMBean().changeTabByTabType(tabType);
        if (tabType != null && RequireBottomTabType.ASSIGN_SEND_INFO.equals(tabType)) {
            display.execute("moveScrollTop();");
        }
    }

    private Require01MBean reloadReqFormInfo(Require require, User executor) {
        Require01MBean r01MBean = (Require01MBean) Faces.getApplication().getELResolver().getValue(Faces.getELContext(), null, "require01MBean");
        r01MBean.initByTable(require);
        r01MBean.getTemplateMBean().initByTable(require);
        this.clearPageMem(r01MBean);
        return r01MBean;
    }

    /**
     * 清除TAB暫存記憶體
     *
     * @param r01MBean
     */
    public void clearPageMem(Require01MBean r01MBean) {
        r01MBean.getTitleBtnMBean().clear();
        r01MBean.getReqAttachMBean().init();
        r01MBean.getTraceMBean().clear();
        r01MBean.getAssignInfo().initTabInfo(r01MBean);
        r01MBean.getPtBottomMBean().init();
        r01MBean.getSendTestBottomMBean().init();
        r01MBean.getOnpgBottomMBean().init();
        r01MBean.getOthSetBottomMBean().init();
        r01MBean.getFogbugzMBean().init();
        r01MBean.getReqBpmMBean().init();
        r01MBean.getGroupLinkMBean().clear();
        r01MBean.getFavoriteMBean().init();
        r01MBean.getTraceActionMBean().init();
        r01MBean.getIxReportMBean().clear();
        r01MBean.getIxPersonalMBean().clear();
        r01MBean.getIxInstructionMBean().clear();
        if (r01MBean.getOthSetSettingViewComponents() != null) {
            r01MBean.getOthSetSettingViewComponents().loadData();
        }
    }

    /**
     * 根據條件進行檢查
     *
     * @param load
     * @return 錯誤代碼
     */
    public boolean checkLoad(IRequireLoad load) {
        // 檢查是否為案件單轉入
        if (this.checkIsIssueCreateMode(load)) {
            log.debug("checkLoad:案件單轉入");
            return true;
        }
        // 檢查是否為案件單轉入(ONPG)
        if (this.checkIsIssueCreateOnpgMode(load)) {
            log.debug("checkLoad:案件單轉入(ONPG)");
            return true;
        }
        // 檢查是否為複製單
        if (this.checkIsCopyMode(load)) {
            log.debug("checkLoad:複製單");
            return true;
        }
        // 檢查是否為新建單
        if (this.checkIsNewRequire(load)) {
            log.debug("checkLoad:新建單");
            return true;
        }

        // 表格上下筆
        if (this.checkIsUpdownBeanPara(load)) {
            log.debug("checkLoad:表格上下筆");
            return true;
        }

        // 管理員以單號開啟連結
        if (this.checkIsParaUrlForAdmin(load)) {
            log.debug("checkLoad:管理員以單號開啟連結");
            return Boolean.TRUE;
        }

        // 送測單上下筆
        if (this.checkWorkTestUpdownBeanPara(load)) {
            log.debug("checkLoad:送測單上下筆");
            return Boolean.TRUE;
        }

        // 送測單上下筆
        if (this.checkIsWorkTestUrlPara(load)) {
            log.debug("checkLoad:送測單連結開啟");
            return Boolean.TRUE;
        }

        if (this.checkIsUrlPara(load)) {
            log.debug("checkLoad:需求單連結開啟");
            return Boolean.TRUE;
        }

        return Boolean.FALSE;
    }

    /**
     * 檢查是否為案件單轉入
     *
     * @param load
     * @return
     */
    private Boolean checkIsIssueCreateMode(IRequireLoad load) {
        if (Faces.getRequestURI().endsWith(reqWebConstants.getRequire01XhtmlKey())
                && Faces.getRequestParameterMap().containsKey(reqWebConstants.getFromIssueCreateKey())) {
            String transSid = Faces.getRequestParameterMap().get(reqWebConstants.getFromIssueCreateKey());
            MappCreateTrans mappTrans = issueMappTransManager.getNoMappTransBySidAndUser(
                    transSid, loginBean.getUser().getSid());
            if (mappTrans != null) {
                load.initByIssueCreate(mappTrans);
                return true;
            }
        }
        return false;
    }

    /**
     * 檢查是否為案件單轉入(ONPG)
     *
     * @param load
     * @return
     */
    private Boolean checkIsIssueCreateOnpgMode(IRequireLoad load) {
        if (Faces.getRequestURI().endsWith(reqWebConstants.getRequire02XhtmlKey())
                && Faces.getRequestParameterMap().containsKey(reqWebConstants.getFromIssueCreateOnpgKey())) {
            String transSid = Faces.getRequestParameterMap().get(reqWebConstants.getFromIssueCreateOnpgKey());
            MappCreateTrans mappTrans = issueMappTransManager.getNoOnpgTransBySidAndUser(
                    transSid, loginBean.getUser().getSid());
            if (mappTrans != null) {
                load.initByIssueCreateOnpg(mappTrans);
                return true;
            }
        }
        return false;
    }

    /**
     * 檢查是否為複製單
     *
     * @return
     */
    private Boolean checkIsCopyMode(IRequireLoad load) {
        if (Faces.getRequestURI().endsWith(reqWebConstants.getRequire01XhtmlKey())
                && Faces.getRequestParameterMap().containsKey(reqWebConstants.getCopyKey())) {
            String reqSid = Faces.getRequestParameterMap().get(reqWebConstants.getCopyKey());
            Require source = requireService.findByReqSid(reqSid);
            load.initByCopy(source);
            return true;
        }
        return false;
    }

    /**
     * 檢查是否為新建單
     *
     * @param load
     * @return
     */
    private Boolean checkIsNewRequire(IRequireLoad load) {
        if (Faces.getRequestURI().endsWith(reqWebConstants.getRequire01XhtmlKey())
                && !Faces.getRequestParameterMap().containsKey(reqWebConstants.getCopyKey())) {
            load.initByNewRequire();
            return true;
        }
        return false;
    }

    /**
     * 檢查表格上下筆是否有相關載入參數
     *
     * @param load
     * @return
     */
    private Boolean checkIsUpdownBeanPara(IRequireLoad load) {
        if (upDownBean.isEmptyCurrRow()) {
            return false;
        }
        String requireNo = upDownBean.getCurrRow();
        Require source = requireService.findByReqNo(requireNo);
        if (source == null) {
            upDownBean.clearAllRec();
            return false;
        }
        load.initByTable(source);
        // 將通知『已讀』
        this.cancelNotify(null, source.getRequireNo());

        return uRLHelper.returnAuthInterceptor(requireNo, "報表");
    }

    /**
     * 檢查送測單上下筆是否有相關載入參數
     *
     * @param load
     * @return
     */
    private Boolean checkWorkTestUpdownBeanPara(IRequireLoad load) {
        if (workTestUpDownBean.isEmptyCurrRow()) {
            return false;
        }
        String testInfoNo = workTestUpDownBean.getCurrRow();
        WorkTestInfo source = sendTestService.findByTestinfoNo(testInfoNo);
        if (source == null) {
            workTestUpDownBean.clearAllRec();
            return false;
        }
        return load.loadSuccessWorkTestInfoByUrlParam(source);
    }

    /**
     * 有需求單系統管理員角色時, 可以以單號開啟連結
     * 
     * @param load
     * @return
     */
    private Boolean checkIsParaUrlForAdmin(IRequireLoad load) {
        // ====================================
        // 檢核是否有管理員角色
        // ====================================
        boolean isAdmin = WkUserUtils.isUserHasRole(
                SecurityFacade.getUserSid(),
                SecurityFacade.getCompanyId(),
                ReqPermission.ROLE_SYS_ADMIN);

        if (!isAdmin) {
            return false;
        }

        // ====================================
        // 取得傳入參數判斷
        // ====================================
        // 以SID開啟
        String paramSid = Faces.getRequestParameterMap().get(URLServiceAttr.URL_ATTR_ADMIN_BY_SID.getAttr());
        // 以單號開啟
        String paramNo = Faces.getRequestParameterMap().get(URLServiceAttr.URL_ATTR_ADMIN_BY_NO.getAttr());
        
        if(WkStringUtils.isEmpty(paramSid) && WkStringUtils.isEmpty(paramNo)) {
            return false;
        }
        
        // ====================================
        // 依據傳入參數判斷
        // ====================================
        Require require = null;

        if (WkStringUtils.notEmpty(paramSid)) {
            require = this.requireService.findByReqSid(paramSid);
            if (require == null) {
                log.warn("admin以傳入sid開啟單據，但找不到資料！SID:[{}]", paramSid);
                return false;
            }
        } else if (WkStringUtils.notEmpty(paramNo)) {
            require = this.requireService.findByReqNo(paramNo);
            if (require == null) {
                log.warn("admin以傳入單號開啟單據，但找不到資料！單號:[{}]", paramNo);
                return false;
            }
        }

        // ====================================
        // 以單號開啟連結
        // ====================================
        // 初始化需求單
        load.initByUrl(require.getRequireNo());
        // return
        return uRLHelper.returnAuthInterceptor(require.getRequireNo(), "admin");
    }

    /**
     * 檢查URL是否有相關載入參數
     *
     * @param load
     * @return
     */
    private Boolean checkIsUrlPara(IRequireLoad load) {

        // ====================================
        // 取得需求單參數
        // ====================================
        String param_m = Faces.getRequestParameterMap().get(URLServiceAttr.URL_ATTR_M.getAttr());
        String notifyType = Faces.getRequestParameter("notifyType");

        // 不是需求單
        if (WkStringUtils.isEmpty(param_m)) {
            return false;
        }

        // 去空白
        param_m = WkStringUtils.safeTrim(param_m);

        // ====================================
        // 取得需求單號
        // ====================================
        // 解析參數
        String sourceNo = urlService.parseIllegalUrlParam(
                param_m,
                WkUserCache.getInstance().findBySid(SecurityFacade.getUserSid()),
                SecurityFacade.getCompanyId());

        // 單號回傳為空
        if (WkStringUtils.isEmpty(sourceNo)) {
            log.warn("以需求單連結開啟單據：無權限，或法解析單號!");
            load.reloadToIllegalPage(URLService.ERROR_CODE_NO_PERMISSION);
            return false;
        }

        // 無權限
        if (sourceNo.contains(URLService.ILLEAL_ACCESS)) {
            log.warn("以需求單連結開啟單據：但無可閱權限");
            load.reloadToIllegalPage(URLService.ERROR_CODE_NO_PERMISSION);
            return false;
        }

        // ====================================
        // 初始化需求單
        // ====================================
        load.initByUrl(sourceNo);

        // ====================================
        // 將通知『已讀』
        // ====================================
        this.cancelNotify(notifyType, sourceNo);

        return true;
    }

    /**
     * 檢查為送測單連結開啟
     * 
     * @param load
     * @return
     */
    private Boolean checkIsWorkTestUrlPara(IRequireLoad load) {
        // ====================================
        // 取得送測單參數
        // ====================================
        String testInfoNo = Faces.getRequestParameter("testInfoNo");

        // 不是送測單
        if (WkStringUtils.isEmpty(testInfoNo)) {
            return false;
        }

        // 去空白
        testInfoNo = WkStringUtils.safeTrim(testInfoNo);

        // 錯誤的格式
        if (testInfoNo.length() != 13) {
            log.warn("以送測單連結開啟單據：傳入送測單號應為13碼!");
            return false;
        }

        // ====================================
        // 查詢送測單資料
        // ====================================
        // 查詢
        WorkTestInfo workTestInfo = sendTestService.findByTestinfoNo(testInfoNo);
        if (workTestInfo == null) {
            log.warn("以送測單連結開啟單據：傳入送測單號查不到![{}]", testInfoNo);
            return false;
        }
        // ====================================
        // 讀取資料
        // ====================================
        return load.loadSuccessWorkTestInfoByUrlParam(workTestInfo);
    }

    /**
     * 將通知訊息『已讀』
     * 
     * @param notifyType
     * @param sourceNo
     */
    private void cancelNotify(String notifyType, String sourceNo) {

        Set<NotifyType> notifyTypes = Sets.newHashSet();

        // 加入 url 傳入的 type
        try {
            if (WkStringUtils.notEmpty(notifyType)) {
                notifyTypes.add(NotifyType.valueOf(notifyType));
            }
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
        }

        // 加入固定已讀類
        // - 單位被分派通知
        notifyTypes.add(NotifyType.REQUEST_DISPATCH_NOTIFY);
        // - 單位被通知通知
        notifyTypes.add(NotifyType.REQUEST_UNIT_GOT_NOTIFY);
        // - 系統別動通知
        notifyTypes.add(NotifyType.REQUEST_CONFIRM_ITEM_CHANGE);

        // 將類型通知已讀
        try {
            this.notifyManager.readNotify(SecurityFacade.getUserSid(), Lists.newArrayList(notifyTypes), sourceNo);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 是否有可閱權限
     * 
     * @param requireNo
     * @return
     */
    public boolean hasReadPermission(String requireNo) {

        // ====================================
        // 檢核可閱權限
        // ====================================
        // 查詢登入者所有角色
        List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
                SecurityFacade.getUserSid(),
                SecurityFacade.getCompanyId());

        // 檢查可閱權限
        return urlService.hasReadPermission(
                requireNo,
                WkUserCache.getInstance().findBySid(SecurityFacade.getUserSid()),
                SecurityFacade.getCompanyId(),
                roleSids);
    }

    public Map<String, Object> maybeCreateUrlAttrMap() {
        Map<String, Object> urlParam = Maps.newHashMap();
        if (this.hasIndicateAttr(URLServiceAttr.URL_ATTR_TAB_PT)) {
            this.fillUrlParam(urlParam, RequireBottomTabType.PT_CHECK_INFO, URLServiceAttr.URL_ATTR_TAB_PT);
            return urlParam;
        }
        if (this.hasIndicateAttr(URLServiceAttr.URL_ATTR_TAB_ST)) {
            this.fillUrlParam(urlParam, RequireBottomTabType.SEND_TEST_INFO, URLServiceAttr.URL_ATTR_TAB_ST);
            return urlParam;
        }
        if (this.hasIndicateAttr(URLServiceAttr.URL_ATTR_TAB_OP)) {
            this.fillUrlParam(urlParam, RequireBottomTabType.ONPG, URLServiceAttr.URL_ATTR_TAB_OP);
            return urlParam;
        }
        if (this.hasIndicateAttr(URLServiceAttr.URL_ATTR_TAB_OS)) {
            this.fillUrlParam(urlParam, RequireBottomTabType.OTH_SET, URLServiceAttr.URL_ATTR_TAB_OS);
            return urlParam;
        }
        if (this.hasIndicateAttr(URLServiceAttr.URL_ATTR_TAB_AS)) {
            this.fillUrlParam(urlParam, RequireBottomTabType.ASSIGN_SEND_INFO, URLServiceAttr.URL_ATTR_TAB_AS);
            return urlParam;
        }
        // 收件俠
        if (this.hasIndicateAttr(URLServiceAttr.URL_ATTR_TAB_INBOX_REPORT)) {
            this.fillUrlParam(urlParam, RequireBottomTabType.INBOX_REPORT, URLServiceAttr.URL_ATTR_TAB_INBOX_REPORT);
            return urlParam;
        }
        if (this.hasIndicateAttr(URLServiceAttr.URL_ATTR_TAB_INBOX_PERSONAL)) {
            this.fillUrlParam(urlParam, RequireBottomTabType.INBOX_PERSONAL, URLServiceAttr.URL_ATTR_TAB_INBOX_PERSONAL);
            return urlParam;
        }
        if (this.hasIndicateAttr(URLServiceAttr.URL_ATTR_TAB_INBOX_INSTRUCTION)) {
            this.fillUrlParam(urlParam, RequireBottomTabType.INBOX_INSTRUCTION, URLServiceAttr.URL_ATTR_TAB_INBOX_INSTRUCTION);
            return urlParam;
        }
        if (this.hasIndicateAttr(URLServiceAttr.URL_ATTR_TAB_INBOX_SEND)) {
            this.fillUrlParam(urlParam, RequireBottomTabType.INBOX_SEND, URLServiceAttr.URL_ATTR_TAB_INBOX_SEND);
            return urlParam;
        }
        return null;
    }

    private Boolean hasIndicateAttr(URLServiceAttr attr) {
        return Faces.getRequestParameterMap().containsKey(attr.getAttr());
    }

    private void fillUrlParam(Map<String, Object> urlParam, RequireBottomTabType tabTyle, URLServiceAttr attr) {
        urlParam.put(TAB_TYPE, tabTyle);
        urlParam.put(TAB_SID, Faces.getRequestParameterMap().get(attr.getAttr()));
    }

}
