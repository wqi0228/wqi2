/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require.onpg;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.FormViewUpdateService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.jdbc.OnpgJdbcRepo;
import com.cy.tech.request.logic.service.onpg.OnpgCheckMemoService;
import com.cy.tech.request.logic.service.onpg.OnpgCheckRecordService;
import com.cy.tech.request.logic.service.onpg.OnpgHistoryService;
import com.cy.tech.request.logic.service.onpg.OnpgService;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.onpg.WorkOnpgCheckMemo;
import com.cy.tech.request.vo.onpg.WorkOnpgCheckRecord;
import com.cy.tech.request.vo.onpg.WorkOnpgCheckRecordReply;
import com.cy.tech.request.vo.onpg.WorkOnpgHistory;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgCheckRecordReplyType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.require.IRequireBottom;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkEntityUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;
import org.primefaces.event.TabChangeEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class OnpgBottomMBean implements IRequireBottom, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 637898374171577703L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private FormViewUpdateService formViewUpdateService;
    @Autowired
    transient private OnpgService opService;
    @Autowired
    transient private OnpgCheckRecordService ckrService;
    @Autowired
    transient private OnpgHistoryService ophService;
    @Autowired
    transient private OnpgCheckMemoService memoService;
    @Autowired
    transient private WkEntityUtils entityUtils;
    @Autowired
    transient private WkJsoupUtils jsoupUtils;
    @Autowired
    private OnpgJdbcRepo onpgJdbcRepo;
    @Autowired
    private RequireService requireService;

    @Getter
    private List<WorkOnpg> onpgs;
    @Getter
    private WorkOnpgCheckRecord editCheckRecord;
    @Getter
    private WorkOnpgCheckRecordReply editCheckRecordReply;
    /** 歷程用 */
    @Getter
    private WorkOnpgHistory editHistory;
    @Getter
    private WorkOnpgCheckMemo editCheckMemo;

    private WorkOnpgCheckRecord backupCheckRecord;
    private WorkOnpgCheckRecordReply backupCheckRecordReply;
    private WorkOnpgCheckMemo backupCheckMemo;

    private Boolean isEditCheckMemo;
    private Boolean isEditCheckRecord;
    private Boolean isEditCheckRecordReply;

    @PostConstruct
    public void init() {
        onpgs = null;

        editCheckRecord = new WorkOnpgCheckRecord();
        editCheckRecordReply = new WorkOnpgCheckRecordReply();
        editHistory = new WorkOnpgHistory();
        editCheckMemo = new WorkOnpgCheckMemo();

        backupCheckRecord = new WorkOnpgCheckRecord();
        backupCheckRecordReply = new WorkOnpgCheckRecordReply();
        backupCheckMemo = new WorkOnpgCheckMemo();

        isEditCheckRecord = Boolean.FALSE;
        isEditCheckRecordReply = Boolean.FALSE;
        isEditCheckMemo = Boolean.FALSE;
    }

    @Override
    public void initTabInfo(Require01MBean r01MBean) {
        // log.debug("op initTabInfo");
        Require require = r01MBean.getRequire();
        if (require == null || !require.getHasOnpg()) {
            // log.debug("op require == null || !require.getHasOnpg()");
            return;
        }
        if (!r01MBean.getBottomTabMBean().currentTabByName(RequireBottomTabType.ONPG)) {
            // log.debug("op
            // !r01MBean.getBottomTabMBean().currentTabByName(RequireBottomTabType.ONPG)");
            return;
        }
        onpgs = opService.initTabInfo(require);
        // log.debug("onpgs size " + onpgs.size());
    }

    public List<WorkOnpg> findOnpgs(Require01MBean r01MBean) {
        if (onpgs == null) {
            this.initTabInfo(r01MBean);
        }
        return onpgs;
    }

    public void updateTabInfo(Require require) {
        onpgs = opService.initTabInfo(require);
    }

    public List<WorkOnpgHistory> findHistorys(WorkOnpg onpg) {
        return ophService.findHistorys(onpg);
    }

    public List<WorkOnpgCheckRecordReply> findCkrReplys(WorkOnpgHistory history) {
        if (history == null || history.getCheckRecord() == null) {
            return Lists.newArrayList();
        }
        return ckrService.findCheckRecordReplys(history.getCheckRecord());
    }

    public void initCheckRecord(WorkOnpg onpg, WorkOnpgCheckRecordReplyType replyType) {
        isEditCheckRecord = Boolean.FALSE;
        editCheckRecord = ckrService.createEmptyCheckRecord(onpg, replyType, loginBean.getUser());
    }

    /**
     * 檢查記錄(一般回覆、GM回覆、QA回覆) - 確認
     */
    public void clickSaveCheckRecord() {
        try {
            String content = jsoupUtils.clearCssTag(editCheckRecord.getContentCss());

            if (WkStringUtils.isEmpty(content)) {
                MessagesUtils.showWarn("內容不可為空白，請重新輸入！！");
                return;
            }

            Preconditions.checkArgument(!Strings.isNullOrEmpty((content)), "內容不可為空白，請重新輸入！！");
            display.hidePfWidgetVar("onpg_ckr_dlg_wv");
            String cateSmallSid = this.findCateSmallSidByOnpgSid(editCheckRecord.getOnpg().getSid());
            if (isEditCheckRecord) {
                String oldContent = jsoupUtils.clearCssTag(ckrService.findBySid(editCheckRecord).getContentCss());

                ckrService.saveByEditCheckRecord(
                        editCheckRecord,
                        loginBean.getUser(),
                        !content.equals(oldContent),
                        cateSmallSid);

                isEditCheckRecord = Boolean.FALSE;
            } else {
                ckrService.saveByNewCheckRecord(
                        editCheckRecord,
                        loginBean.getUser(),
                        cateSmallSid);
            }
        } catch (IllegalAccessException e) {
            log.error(e.getMessage(), e);
            this.recoveryView(null, editCheckRecord.getOnpg());
            MessagesUtils.showError(e.getMessage());
        }
    }

    public void initCheckRecordEdit(WorkOnpgCheckRecord reply) {
        this.editCheckRecord = reply;
        isEditCheckRecord = Boolean.TRUE;
        entityUtils.copyProperties(editCheckRecord, backupCheckRecord);
    }

    public void cancelEditCheckRecord() {
        if (isEditCheckRecord) {
            entityUtils.copyProperties(backupCheckRecord, editCheckRecord);
            isEditCheckRecord = Boolean.FALSE;
        }
    }

    public void initCheckRecordReply(WorkOnpgCheckRecord reply) {
        isEditCheckRecordReply = Boolean.FALSE;
        editCheckRecordReply = ckrService.createEmptyCheckRecordReply(reply, loginBean.getUser());
    }

    /**
     * 檢查記錄-回覆
     */
    public void clickSaveCheckRecordReply() {
        try {
            String content = jsoupUtils.clearCssTag(editCheckRecordReply.getContentCss());
            Preconditions.checkArgument(!Strings.isNullOrEmpty(content), "回覆不可為空白，請重新輸入！！");
            String cateSmallSid = this.findCateSmallSidByOnpgSid(editCheckRecordReply.getOnpg().getSid());
            if (isEditCheckRecordReply) {
                String oldContent = jsoupUtils.clearCssTag(ckrService.findBySid(editCheckRecordReply).getContentCss());
                ckrService.saveByEditCheckRecordReply(editCheckRecordReply, loginBean.getUser(),
                        !content.equals(oldContent), cateSmallSid);
                isEditCheckRecordReply = Boolean.FALSE;
            } else {
                ckrService.saveByNewCheckRecordReply(editCheckRecordReply, loginBean.getUser().getSid(), cateSmallSid);
            }
            display.hidePfWidgetVar("onpg_ckr_reply_dlg_wv");
            this.whenSaveCheckRecordReplyOpenTab(editCheckRecordReply);
        } catch (IllegalArgumentException e) {
            log.debug(e.getMessage(), e);
            MessagesUtils.showError(e.getMessage());
        }
    }

    private void whenSaveCheckRecordReplyOpenTab(WorkOnpgCheckRecordReply ckrReply) {
        WorkOnpg onpg = ckrReply.getOnpg();
        WorkOnpgCheckRecord ckr = ckrReply.getCheckRecord();
        int idx = onpg.getHistorys().indexOf(ckr.getHistory());
        display.execute("accPanelSelectTab('op_acc_panel_layer_one_" + onpg.getSid() + "'," + idx + ")");
    }

    public void initCheckRecordReplyEdit(WorkOnpgCheckRecordReply ckrReply) {
        this.editCheckRecordReply = ckrReply;
        isEditCheckRecordReply = Boolean.TRUE;
        entityUtils.copyProperties(editCheckRecordReply, backupCheckRecordReply);
    }

    public void cancelEditCheckRecordReply() {
        if (isEditCheckRecordReply) {
            entityUtils.copyProperties(backupCheckRecordReply, editCheckRecordReply);
            isEditCheckRecordReply = Boolean.FALSE;
            this.whenSaveCheckRecordReplyOpenTab(editCheckRecordReply);
        }
    }

    public void createEmptyHistory(WorkOnpg onpg) {
        editHistory = ophService.createEmptyHistory(onpg, loginBean.getUser());
    }

    public void openAttachByHistory(WorkOnpgHistory history, OnpgHistoryAttachMBean sthaMBean) {
        sthaMBean.init(history);
        this.editHistory = history;
    }

    /**
     * ON程式取消
     */
    public void clickSaveCancelHistory() {
        if (WkStringUtils.isEmpty(jsoupUtils.clearCssTag(editHistory.getReasonCss()))) {
            MessagesUtils.showWarn("請輸入取消原因！！");
            return;
        }

        try {
            ophService.saveCancelHistory(editHistory);
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);
            return;
        }
        display.hidePfWidgetVar("onpg_cancel_dlg_wv");
    }

    public void initCheckMemo(WorkOnpg onpg) {
        editCheckMemo = memoService.createEmptyCheckMemo(onpg, loginBean.getUser());
    }

    /**
     * 檢查註記視窗-確認
     */
    public void clickSaveCheckMemo() {
        try {
            Preconditions.checkArgument(!Strings.isNullOrEmpty(jsoupUtils.clearCssTag(editCheckMemo.getContentCss())), "請輸入註記資訊！！");
            if (isEditCheckMemo) {
                memoService.saveByEditCheckMemo(editCheckMemo, loginBean.getUser());
                isEditCheckMemo = Boolean.FALSE;
            } else {
                memoService.saveByNewCheckMemo(editCheckMemo);
            }
            display.hidePfWidgetVar("onpg_check_memo_dlg_wv");
        } catch (IllegalArgumentException e) {
            log.debug("檢核失敗", e);
            MessagesUtils.showError(e.getMessage());
        } catch (IllegalAccessException e) {
            log.error(e.getMessage(), e);
            this.recoveryView(null, editCheckMemo.getOnpg());
            MessagesUtils.showError(e.getMessage());
        }
    }

    public void initCheckMemoEdit(WorkOnpgCheckMemo checkMemo) {
        this.editCheckMemo = checkMemo;
        isEditCheckMemo = Boolean.TRUE;
        entityUtils.copyProperties(editCheckMemo, backupCheckMemo);
    }

    public void cancelCheckMemoEdit() {
        if (isEditCheckMemo && !Strings.isNullOrEmpty(editCheckMemo.getSid())) {
            entityUtils.copyProperties(backupCheckMemo, editCheckMemo);
            isEditCheckMemo = Boolean.FALSE;
        }
    }

    /**
     * 檢查完成
     * 
     * @param r01MBean
     */
    public void clickSaveTestCompleteHistory(Require01MBean r01MBean) {
        try {
            ophService.saveCompleteHistory(
                    r01MBean.getRequire(),
                    editHistory,
                    loginBean.getDep().getSid(),
                    this.checkComplateBySmallCategoryRole(
                            r01MBean.getRequire().getMapping().getSmall().getExecOnpgOkRole()));
        } catch (IllegalAccessException e) {
            log.error(r01MBean.getRequire().getRequireNo() + ":" + editHistory.getOnpgNo() + ":" + loginBean.getUser().getName() + ":執行ON程式檢查完成失敗..."
                    + e.getMessage(), e);
            this.recoveryView(null, editHistory.getOnpg());
            MessagesUtils.showError(e.getMessage());
        }
        display.hidePfWidgetVar("onpg_check_complete_dlg_wv");
        r01MBean.reBuildeRequire();
        r01MBean.getTraceMBean().clear();
        r01MBean.getTitleBtnMBean().clear();
        r01MBean.getPtBottomMBean().init();
        r01MBean.getSendTestBottomMBean().init();
    }

    /**
     * 判斷登入者是否有符合小類 可On pg Ok 角色
     *
     * @param execOnpgOkRole
     * @return
     */
    private boolean checkComplateBySmallCategoryRole(String execOnpgOkRole) {
        if (Strings.isNullOrEmpty(execOnpgOkRole)) {
            return false;
        }
        List<String> permissions = Lists.newArrayList(execOnpgOkRole.split(","));

        return WkUserUtils.isUserHasRole(
                SecurityFacade.getUserSid(),
                SecurityFacade.getCompanyId(),
                permissions);
    }

    public String createSearch17ViewUrlLink(WorkOnpg onpg) {
        return opService.createSearch17ViewUrlLink(onpg);
    }

    public void updateReadRecord(TabChangeEvent event) {
        try {
            WorkOnpg onpg = (WorkOnpg) event.getData();
            formViewUpdateService.updateOnpgRecord(onpg, loginBean.getUser());
        } catch (Exception e) {
            log.error("更新ON程式閱讀記錄失敗。" + e.getMessage(), e);
        }
    }

    public void reBuildOnpg(WorkOnpg onpg) {
        if (onpg == null || Strings.isNullOrEmpty(onpg.getOnpgNo())) {
            return;
        }
        WorkOnpg nOnpg = opService.findByOnpgNo(onpg.getOnpgNo());
        int idx = this.onpgs.indexOf(nOnpg);
        onpgs.set(idx, nOnpg);
    }

    public void recoveryView(Require01MBean r01MBean, WorkOnpg onpg) {
        if (r01MBean == null) {
            r01MBean = (Require01MBean) Faces.getApplication().getELResolver().getValue(Faces.getELContext(), null, "require01MBean");
        }
        r01MBean.reBuildeRequire();
        this.reBuildOnpg(onpg);
        r01MBean.getTraceMBean().clear();
        r01MBean.getTitleBtnMBean().clear();
    }

    /**
     * 取得小類sid
     *
     * @param onpgSid
     * @return
     */
    private String findCateSmallSidByOnpgSid(String onpgSid) {
        return onpgJdbcRepo.findCateSmallSidByOnpgSid(onpgSid);
    }

    /**
     * 需求完成時增加更新需求完成人員 2017-06-01
     *
     * @param onpg
     * @param require
     */
    public void createEmptyHistoryAndUpdateReqFinishUser(Require require, WorkOnpg onpg) {
        editHistory = ophService.createEmptyHistory(onpg, loginBean.getUser());
        // ON程式結案後不得再異動完成需求人員
        if (!RequireStatusType.CLOSE.equals(require.getRequireStatus())) {
            requireService.updateReqFinishHelper(require, loginBean.getUser());
        }
    }
}
