/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.BasicDataCategoryType;
import com.cy.tech.request.logic.service.CategoryCreateService;
import com.cy.tech.request.logic.service.TemplateService;
import com.cy.tech.request.logic.vo.MappingTreeTo;
import com.cy.tech.request.vo.category.BasicDataBigCategory;
import com.cy.tech.request.vo.category.BasicDataSmallCategory;
import com.cy.tech.request.vo.enums.ReqCateType;
import com.cy.work.common.enums.UrgencyType;
import com.cy.tech.request.vo.template.CategoryKeyMapping;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.backend.logic.WorkBackendParamService;
import com.cy.work.backend.vo.enums.WkBackendParam;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 需求單類別組合挑選 (樹狀元件)
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope("view")
public class RequireCategoryTreeMBean implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 7315590326467778218L;
    @Autowired
	transient private LoginBean loginBean;
	@Autowired
	transient private CustomerMBean customerMBean;
	@Autowired
	transient private TemplateService tempService;
	@Autowired
	transient private DisplayController display;
	@Autowired
	private transient WorkBackendParamService workBackendParamService;
	@Autowired
	transient private CategoryCreateService categoryCreateService;
	@Getter
	@Setter
	private String searchText;
	@Getter
	@Setter
	transient private TreeNode root;
	@Getter
	@Setter
	transient private TreeNode selectNode;
	@Getter
	@Setter
	private Boolean showFrame = Boolean.FALSE;

	@PostConstruct
	public void init() {
		searchText = "";
		this.searchCategoryTreeBySearchText();
	}

	/**
	 * 取得活動中（非停用）模版
	 */
	public void searchCategoryTreeBySearchText() {

		boolean expend = !Strings.isNullOrEmpty(searchText);

		// ====================================
		// 搜尋全部活動中（非停用）模版的最後一個版本
		// ====================================
		List<CategoryKeyMapping> activeMapping = tempService.findByFuzzyTextAndMaxVersion(this.searchText);

		// ====================================
		// 建立樹狀清單
		// ====================================
		root = new DefaultTreeNode("Root", null);
		Map<String, TreeNode> middleLv = Maps.newHashMap();
		Map<String, TreeNode> bigLv = Maps.newHashMap();

		// 由系統參數中, 取得案件單轉需求單的類別(中類名稱) , 用於排除此類項目
		List<String> notIncludeMiddNames = this.workBackendParamService.findStringsByKeyword(WkBackendParam.TECH_ISSUE_CREATE_MIDDLE_NAME);

		// 依據大類逐筆處理
		for (CategoryKeyMapping each : activeMapping) {

			// 檢查使用者是否有該大類使用權限
			if (this.checkPermission(each)) {
				continue;
			}

			// 大類名稱
			String bigName = each.getBigName();
			// 中類名稱
			String middleName = each.getMiddleName();
			// 比對需排除的中類名稱
			if (notIncludeMiddNames.contains(middleName)) {
				continue;
			}

			// 組大類 node
			if (!bigLv.containsKey(bigName)) {
				TreeNode bigNode = new DefaultTreeNode(new MappingTreeTo(each, BasicDataCategoryType.BIG, bigName), root);
				bigNode.setExpanded(true);
				bigLv.put(bigName, bigNode);
			}

			// 組中類 node
			if (!middleLv.containsKey(middleName)) {
				TreeNode bigNode = bigLv.get(bigName);
				TreeNode middleNode = new DefaultTreeNode(new MappingTreeTo(each, BasicDataCategoryType.MIDDLE, middleName), bigNode);
				middleNode.setExpanded(expend);
				middleLv.put(middleName, middleNode);
			}

			// 組小類 node
			TreeNode middleNode = middleLv.get(middleName);
			new DefaultTreeNode(new MappingTreeTo(each, BasicDataCategoryType.SMALL, each.getSmallName()), middleNode);
		}
	}

	/**
	 *  當使用者點擊新增需求單時，可以選擇的類別樹選項，<BR/>
	 * 若該大類有設定可使用的單位， 但點擊新增的使用者所歸<BR/>
	 * 屬的單位無使用權限，則於類別樹不顯示此項目
	 *
	 * @param mapping
	 * @return
	 */
	private boolean checkPermission(CategoryKeyMapping mapping) {
		BasicDataBigCategory big = mapping.getBig();
		if (big.getCanUseDepts() == null
		        || big.getCanUseDepts().getValue() == null
		        || big.getCanUseDepts().getValue().isEmpty()) {
			return false;
		}
		return !big.getCanUseDepts().getValue().contains(loginBean.getDep().getSid().toString());
	}

	public void onNodeExpand(NodeExpandEvent event) {
		event.getTreeNode().setExpanded(true);
	}

	public void onNodeCollapse(NodeCollapseEvent event) {
		event.getTreeNode().setExpanded(false);
	}

	/**
	 * 事件：點選類別節點
	 * 
	 * @param r01MBean
	 * @param templateMBean
	 * @param to
	 */
	public void initTemplate(Require01MBean r01MBean, RequireTemplateFieldMBean templateMBean, MappingTreeTo to) {

		// ====================================
		// 點選項目為小類
		// ====================================
		if (to.getType().equals(BasicDataCategoryType.SMALL)) {
			// 帶入設定模版
			this.clickSelectTreeNode(r01MBean, templateMBean, to.getType(), to.getMapping());
			return;
		}

		// ====================================
		// 點選項目為大類
		// ====================================
		if (to.getType().equals(BasicDataCategoryType.BIG)) {
			// 展開此節點
			TreeNode bigNode = root.getChildren().stream()
			        .filter(node -> ((MappingTreeTo) node.getData()).getType().equals(to.getType())
			                && ((MappingTreeTo) node.getData()).getName().equals(to.getName()))
			        .findFirst().get();
			bigNode.setExpanded(!bigNode.isExpanded());
		}

		// ====================================
		// 點選項目為中類
		// ====================================
		if (to.getType().equals(BasicDataCategoryType.MIDDLE)) {
			// 展開此節點
			root.getChildren().forEach(big -> big.getChildren().forEach(middle -> {
				MappingTreeTo mTo = (MappingTreeTo) middle.getData();
				if (mTo.getName().equals(to.getName())) {
					middle.setExpanded(!middle.isExpanded());
				}
			}));
		}
	}

	/**
	 * 點擊樹狀圖後發生<BR/>
	 * 初始化成功狀態:RequireStep.NEW_IN_PAGE
	 *
	 * @param r01MBean
	 * @param templateMBean
	 * @param type
	 * @param selectMapping
	 */
	public void clickSelectTreeNode(
	        Require01MBean r01MBean,
	        RequireTemplateFieldMBean templateMBean,
	        BasicDataCategoryType type,
	        CategoryKeyMapping selectMapping) {
		showFrame = Boolean.FALSE;
		if (type.equals(BasicDataCategoryType.SMALL)) {
			r01MBean.init();
			customerMBean.updateCustomer(r01MBean, selectMapping);
			// 初始化需求單緊急度
			UrgencyType uType = selectMapping.getMiddle().getUrgency();
			r01MBean.getRequire().setUrgency(uType);

			// REQ-1440 為『內部需求』時，主責單位+負責人員預設為【開單者】
			if (selectMapping.getBig() != null && ReqCateType.INTERNAL.equals(selectMapping.getBig().getReqCateType())) {
				r01MBean.getRequire().setInChargeDep(SecurityFacade.getPrimaryOrgSid());
				r01MBean.getRequire().setInChargeUsr(SecurityFacade.getUserSid());
			}

			// 初始化選擇的鍵值模版
			templateMBean.initBySelectMapping(selectMapping);
			templateMBean.toggleDisabled(false);
			// 設定01MBean顯示方式
			r01MBean.setSetp(RequireStep.NEW_IN_PAGE);
			display.update("viewPanel");
			display.execute("toggleLayout('block')");
		}
	}

	public Integer getSmallCategoryDefaultDueDay() {
		Integer plusDays = 7;
		try {
			if (selectNode != null) {
				if (selectNode.getData() instanceof MappingTreeTo) {
					String sid = ((MappingTreeTo) selectNode.getData()).getMapping().getSmall().getSid();
					BasicDataSmallCategory category = categoryCreateService.findSmallCategoryDefaultDueDayBySid(sid);
					plusDays = category != null ? category.getDefaultDueDays() : 7;
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return plusDays;
	}

}
