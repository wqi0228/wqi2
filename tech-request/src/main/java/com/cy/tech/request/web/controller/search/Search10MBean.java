/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search10QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.Search10View;
import com.cy.tech.request.logic.service.CategorySettingService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.URLService.URLServiceAttr;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.enums.ReqStatusMBean;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.require.RequireForwardDeAndPersonMBean;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.searchheader.CommonHeaderMBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.view.component.searchquery.SearchQuery10;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkUserWithRolesCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.logic.lib.manager.WorkCommonReadRecordManager;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.group.vo.WorkLinkGroup;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 未結案單據
 *
 * @author jason_h
 */
@Controller
@Scope("view")
@Slf4j
public class Search10MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2758696122710288311L;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private Search10QueryService search10QueryService;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private ReportCustomFilterLogicComponent reportCustomFilterLogicComponent;
    @Autowired
    transient private CategorySettingService categorySettingService;
    @Autowired
    transient private URLService urlService;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private Require01MBean r01MBean;
    @Autowired
    transient private ReqStatusMBean reqStatusMBean;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchResultHelper searchResultHelper;
    @Autowired
    transient private WorkCommonReadRecordManager workCommonReadRecordManager;

    /** 所有的需求單 */
    @Getter
    @Setter
    private List<Search10View> queryItems;

    /** 選擇的需求單 */
    @Getter
    @Setter
    private Search10View querySelection;

    /** 上下筆移動keeper */
    @Getter
    private Search10View queryKeeper;

    /** 切換模式 */
    @Getter
    private SwitchType switchType = SwitchType.CONTENT;

    /** 切換模式 - 全畫面狀態 */
    @Getter
    private SwitchType switchFullType = SwitchType.DETAIL;

    /** 在匯出的時候，某些內容需要隱藏 */
    @Getter
    private boolean hasDisplay = true;

    @Getter
    private final String dataTableId = "dtRequire";
    @Getter
    private SearchQuery10 searchQuery10;

    @PostConstruct
    public void init() {
        this.initComponent();
        display.execute("doSearchData();");
        // this.search();
    }

    private void initComponent() {

        // 查詢登入者所有角色
        List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
                SecurityFacade.getUserSid(),
                SecurityFacade.getCompanyId());

        this.searchQuery10 = new SearchQuery10(loginBean.getCompanyId(), helper,
                reportCustomFilterLogicComponent,
                categorySettingService,
                messageCallBack,
                display,
                roleSids,
                loginBean.getDep(),
                loginBean.getUser(),
                reqStatusMBean);
        this.searchQuery10.initSetting();
        commHeaderMBean.clearAdvance();
        this.searchQuery10.loadSetting();
    }

    public void search() {
        queryItems = this.findWithQuery();
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        searchQuery10.initSetting();
        commHeaderMBean.clearAdvance();
        this.searchQuery10.loadSetting();
        this.search();
    }

    private List<Search10View> findWithQuery() {
        String requireNo = reqularUtils.getRequireNo(loginBean.getCompanyId(), searchQuery10.getSearch10QueryVO().getFuzzyText());
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT "
                + "tr.require_sid,"
                + "tr.require_no,"
                + "tid.field_content,"
                + "tr.urgency,"
                + "tr.has_forward_dep,"
                + "tr.has_forward_member,"
                + "tr.has_link,"
                + "tr.create_dt,"
                + "ckm.big_category_name,"
                + "ckm.middle_category_name,"
                + "ckm.small_category_name,"
                + "tr.dep_sid,"
                + "tr.create_usr,"
                + "tr.require_status,"
                + "sc.check_attachment,"
                + "tr.has_attachment, "
                + "tr.hope_dt, "
                + "tr.inte_staff, "
                + "tr.require_establish_dt, "
                + "tr.require_finish_dt, "
                + this.builderHasTraceColumn()
                + this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire()
                + "FROM ");

        buildRequireCondition(requireNo, builder, parameters);
        buildRequireIndexDictionaryCondition(requireNo, builder, parameters);
        buildCategoryKeyMappingCondition(requireNo, builder, parameters);
        buildBasicDataSmallCategoryCondition(builder, parameters);
        // 檢查項目 (系統別)
        // 後方需對 tr.require_sid 做 group by
        builder.append(this.searchConditionSqlHelper.prepareCommonJoin(SecurityFacade.getUserSid()));

        builder.append("WHERE tr.require_sid IS NOT NULL ");

        // 查詢條件：是否閱讀
        if (WkStringUtils.isEmpty(requireNo)) {
            builder.append(
                    this.workCommonReadRecordManager.prepareWhereConditionSQL(
                            searchQuery10.getSearch10QueryVO().getSelectReadRecordType(), "readRecord"));
        }

        builder.append(" GROUP BY tr.require_sid ");
        builder.append("  ORDER BY tr.create_dt ASC");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.NOT_CLOSE, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.NOT_CLOSE, SecurityFacade.getUserSid());

        // 查詢
        List<Search10View> resultList = search10QueryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // ====================================
        // 後續處理
        // ====================================
        if (WkStringUtils.notEmpty(resultList)) {
            // 後續處理-開始
            usageRecord.afterProcessStart();
            // 查詢條件過濾：系統別
            resultList = resultList.stream()
                    .filter(each -> this.searchResultHelper.filterCheckItems(
                            each.getCheckItemTypes(),
                            searchQuery10.getSearch10QueryVO().getCheckItemTypes(),
                            false,
                            null))
                    .collect(Collectors.toList());

            // 後續處理-結束
            usageRecord.afterProcessEnd();
        }

        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return resultList;
    }

    private void buildRequireCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("(SELECT * FROM tr_require tr WHERE 1=1");
        // 立單區間
        if (Strings.isNullOrEmpty(requireNo) && searchQuery10.getSearch10QueryVO().getStartDate() != null
                && searchQuery10.getSearch10QueryVO().getEndDate() != null) {
            builder.append(" AND tr.create_dt BETWEEN :startDate AND :endDate");
            parameters.put("startDate", helper.transStartDate(searchQuery10.getSearch10QueryVO().getStartDate()));
            parameters.put("endDate", helper.transEndDate(searchQuery10.getSearch10QueryVO().getEndDate()));
        }

        // 結案碼(未結案單據：false ； 歷史單據：true)
        builder.append(" AND tr.close_code = 0");

        // 需求單位
        builder.append(" AND tr.dep_sid IN (:depSids)");
        parameters.put("depSids", searchQuery10.getSearch10QueryVO().getRequireDepts() == null || searchQuery10.getSearch10QueryVO().getRequireDepts().isEmpty()
                ? ""
                : searchQuery10.getSearch10QueryVO().getRequireDepts());

        // 需求類別製作進度
        builder.append(" AND tr.require_status IN (:requireStatus)");
        if (Strings.isNullOrEmpty(requireNo)) {
            parameters.put("requireStatus", searchQuery10.createQueryReqStatus());
        } else {
            parameters.put("requireStatus", searchQuery10.createQueryAllReqStatus());
        }

        // 需求人員
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(searchQuery10.getSearch10QueryVO().getTrCreatedUserName())) {
            List<Integer> userSids = WkUserUtils.findByNameLike(searchQuery10.getSearch10QueryVO().getTrCreatedUserName(), SecurityFacade.getCompanyId());
            if (WkStringUtils.isEmpty(userSids)) {
                userSids.add(-999);
            }

            builder.append(" AND tr.create_usr IN (:userSids)");
            parameters.put("userSids", userSids.isEmpty() ? "" : userSids);
        }
        //////////////////// 以下為進階搜尋條件//////////////////////////////
        // 異動區間
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getStartUpdatedDate() != null && commHeaderMBean.getEndUpdatedDate() != null) {
            builder.append(" AND tr.update_dt BETWEEN :startUpdatedDate AND :endUpdatedDate");
            parameters.put("startUpdatedDate", helper.transStartDate(commHeaderMBean.getStartUpdatedDate()));
            parameters.put("endUpdatedDate", helper.transEndDate(commHeaderMBean.getEndUpdatedDate()));
        }
        // 緊急度
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getUrgencyTypeList() != null && !commHeaderMBean.getUrgencyTypeList().isEmpty()) {
            builder.append(" AND tr.urgency IN (:urgencyList)");
            parameters.put("urgencyList", commHeaderMBean.getUrgencyTypeList());
        }
        // 需求單號
        if (!Strings.isNullOrEmpty(requireNo)) {
            builder.append(" AND tr.require_no = :requireNo");
            parameters.put("requireNo", requireNo);
        }
        // 需求單號
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(commHeaderMBean.getRequireNo())) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(commHeaderMBean.getRequireNo()) + "%";
            builder.append(" AND tr.require_no LIKE :requireNo");
            parameters.put("requireNo", textNo);
        }
        // 轉發部門
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getForwardDepts() != null && !commHeaderMBean.getForwardDepts().isEmpty()) {
            builder.append(" AND tr.require_sid IN (SELECT tai.require_sid FROM tr_alert_inbox tai WHERE tai.forward_type='FORWARD_DEPT'"
                    + " AND tai.receive_dep IN (:forwardDepts))");
            parameters.put("forwardDepts", commHeaderMBean.getForwardDepts());
        }
        builder.append(") AS tr ");
    }

    private void buildRequireIndexDictionaryCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        // 模糊搜尋
        builder.append("INNER JOIN (SELECT tid.require_sid,tid.field_content FROM tr_index_dictionary tid WHERE 1=1");
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(searchQuery10.getSearch10QueryVO().getFuzzyText())) {
            String text = "%" + reqularUtils.replaceIllegalSqlLikeStr(searchQuery10.getSearch10QueryVO().getFuzzyText()) + "%";
            builder.append(" AND (tid.require_sid"
                    + " IN (SELECT DISTINCT tid1.require_sid FROM tr_index_dictionary tid1 WHERE tid1.field_content LIKE :text)"
                    + " OR tid.require_sid IN (SELECT DISTINCT trace.require_sid FROM tr_require_trace trace WHERE "
                    + "trace.require_trace_type = 'REQUIRE_INFO_MEMO' AND trace.require_trace_content LIKE :text))");
            parameters.put("text", text);
        }
        builder.append(" AND tid.field_name='主題') AS tid ON tr.require_sid=tid.require_sid ");
    }

    private void buildCategoryKeyMappingCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_category_key_mapping ckm WHERE 1=1");
        // 類別組合
        if (Strings.isNullOrEmpty(requireNo)
                && (!searchQuery10.getSearch10QueryVO().getBigDataCateSids().isEmpty() || !searchQuery10.getSearch10QueryVO().getMiddleDataCateSids().isEmpty()
                        || !searchQuery10.getSearch10QueryVO().getSmallDataCateSids().isEmpty())) {
            builder.append(" AND ckm.big_category_sid IN (:bigs) OR ckm.middle_category_sid IN (:middles) OR ckm.small_category_sid IN "
                    + "(:smalls)");
            // 加入單獨選擇的大類
            if (searchQuery10.getSearch10QueryVO().getSelectBigCategory() != null) {
                searchQuery10.getSearch10QueryVO().getBigDataCateSids().add(searchQuery10.getSearch10QueryVO().getSelectBigCategory().getSid());
            }
            parameters.put("bigs",
                    searchQuery10.getSearch10QueryVO().getBigDataCateSids().isEmpty() ? "" : searchQuery10.getSearch10QueryVO().getBigDataCateSids());
            parameters.put("middles",
                    searchQuery10.getSearch10QueryVO().getMiddleDataCateSids().isEmpty() ? "" : searchQuery10.getSearch10QueryVO().getMiddleDataCateSids());
            parameters.put("smalls",
                    searchQuery10.getSearch10QueryVO().getSmallDataCateSids().isEmpty() ? "" : searchQuery10.getSearch10QueryVO().getSmallDataCateSids());
        }
        // 需求類別
        if (Strings.isNullOrEmpty(requireNo) && searchQuery10.getSearch10QueryVO().getSelectBigCategory() != null
                && !Strings.isNullOrEmpty(searchQuery10.getSearch10QueryVO().getSelectBigCategory().getSid())) {
            builder.append(" AND ckm.big_category_sid = :big");
            parameters.put("big", searchQuery10.getSearch10QueryVO().getSelectBigCategory().getSid());
        }
        builder.append(") AS ckm ON tr.mapping_sid=ckm.key_sid ");
    }

    private void buildBasicDataSmallCategoryCondition(StringBuilder builder, Map<String, Object> parameters) {
        builder.append("LEFT JOIN (SELECT sc.basic_data_small_category_sid,sc.check_attachment FROM "
                + "tr_basic_data_small_category sc) AS sc ON sc.basic_data_small_category_sid = ckm.small_category_sid ");
    }

    /**
     * 是否有追蹤資料
     *
     * @return
     */
    private String builderHasTraceColumn() {
        StringBuilder builder = new StringBuilder();
        builder.append(" (SELECT CASE WHEN (COUNT(trace.tracesid) > 0) THEN 'TRUE' ELSE 'FALSE' END ");
        builder.append(" FROM work_trace_info trace ");
        builder.append(" WHERE trace.trace_source_no = tr.require_no");
        builder.append(" AND trace.trace_source_type = 'TECH_REQUEST'");
        builder.append(" AND (trace.trace_dep = ").append(loginBean.getDep().getSid());
        builder.append(" OR trace.trace_usr = ").append(loginBean.getUser().getSid()).append(")");
        builder.append(" AND trace.trace_status = 'UN_FINISHED'");
        builder.append(" AND trace.Status = '0'");
        builder.append(") AS hasTrace, ");
        return builder.toString();
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Search10View view) {
        view.setReadRecordType(ReadRecordType.HAS_READ);
        Require r = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(r, loginBean.getUser());
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (Search10View) event.getObject();
        this.changeRequireContent(this.queryKeeper);
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Search10View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        helper.exportExcel(document, searchQuery10.getSearch10QueryVO().getStartDate(), searchQuery10.getSearch10QueryVO().getEndDate(),
                commHeaderMBean.getReportType());
        hasDisplay = true;
    }

    /**
     * 隱藏部分column裡的內容
     */
    public void hideColumnContent() {
        hasDisplay = false;
    }

    public String getAttInfo(Search10View view) {
        Boolean checkAtt = view.getCheckAttachment();
        if (checkAtt) {
            if (view.getHasAttachment()) {
                return "檢核正常";
            } else {
                return "檢核異常";
            }
        } else {
            return "不需檢核";
        }
    }

    /**
     * 列表執行關聯動作
     *
     * @param view
     */
    public void initRelevance(Search10View view) {
        this.querySelection = view;
        this.queryKeeper = this.querySelection;
        Require r = requireService.findByReqNo(view.getRequireNo());
        this.r01MBean.setRequire(r);
        this.r01MBean.getRelevanceMBean().initRelevance();
    }

    public String getRelevanceViewUrl(Search10View view) {
        if (view == null) {
            return "";
        }
        Require r = requireService.findByReqNo(view.getRequireNo());
        if (r == null) {
            return "";
        }
        WorkLinkGroup link = r.getLinkGroup();
        if (link == null) {
            return "";
        }
        return "../require/require04.xhtml" + urlService.createSimpleURLLink(URLServiceAttr.URL_ATTR_L, link.getSid(), view.getRequireNo(), 1);
    }

    /**
     * 列表點選轉寄功能
     * 
     * @param view
     */
    public void openForwardDialog(Search10View view) {
        this.queryKeeper = this.querySelection = view;

        if (SwitchType.DETAIL.equals(switchType)) {
            this.changeRequireContent(this.queryKeeper);
            this.display.update(Lists.newArrayList(
                    "@(.reportUpdateClz)",
                    "title_info_click_btn_id",
                    "require01_title_info_id",
                    "require_template_id",
                    "viewPanelBottomInfoId",
                    "req03botmid",
                    this.dataTableId + "1"));
        }

        RequireForwardDeAndPersonMBean mbean = WorkSpringContextHolder.getBean(RequireForwardDeAndPersonMBean.class);
        mbean.openDialog(view.getRequireNo(), r01MBean);
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search10View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.removeClassByTextBold(dtId, pageCount);
        this.transformHasRead();
        this.checkHelfScreen();
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            display.update("headerTitle");
            display.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Search10View to) {
        querySelection = to;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 去除粗體Class
     *
     * @param dtId
     * @param pageCount
     */
    private void removeClassByTextBold(String dtId, String pageCount) {
        display.execute("removeClassByTextBold('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
        display.execute("changeAlreadyRead('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 變更已閱讀
     */
    private void transformHasRead() {
        querySelection.setReadRecordType(ReadRecordType.HAS_READ);
        queryKeeper = querySelection;
        queryItems.set(queryItems.indexOf(querySelection), querySelection);
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.transformHasRead();
        this.removeClassByTextBold(dtId, pageCount);
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    /**
     * 列表執行追蹤動作
     *
     * @param view
     */
    public void btnAddTrack(Search10View view) {
        Require r = this.highlightAndReturnRequire(view);
        this.r01MBean.setRequire(r);
        this.r01MBean.getTraceActionMBean().initTrace(this.r01MBean);
    }

    /**
     * 列表標註及回傳需求單
     *
     * @param view
     * @return
     */
    private Require highlightAndReturnRequire(Search10View view) {
        this.querySelection = view;
        this.queryKeeper = this.querySelection;
        return requireService.findByReqNo(view.getRequireNo());
    }

    /** 訊息呼叫 */
    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 7071598885956439405L;

        @Override
        public void showMessage(String m) {
            MessagesUtils.showError(m);
        }
    };
}
