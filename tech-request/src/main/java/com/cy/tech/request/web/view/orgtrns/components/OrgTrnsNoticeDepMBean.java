package com.cy.tech.request.web.view.orgtrns.components;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.orgtrns.OrgTrnsAssignNoticeService;
import com.cy.tech.request.logic.service.orgtrns.OrgTrnsNoticeDepService;
import com.cy.tech.request.logic.service.orgtrns.OrgTrnsService;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsBaseComponentPageVO;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsDtVO;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsWorkVerifyVO;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.RequireTransProgramType;
import com.cy.tech.request.web.pf.utils.ConfirmCallbackDialogController;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 通知單位轉檔 MBean
 */
@Component
@Scope("view")
@Slf4j
public class OrgTrnsNoticeDepMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6064255287137812916L;
    // ========================================================================
    // 服務元件區
    // ========================================================================
    @Autowired
    private transient DisplayController displayController;
    @Autowired
    private transient ConfirmCallbackDialogController confirmCallbackDialogController;
    @Autowired
    private transient OrgTrnsNoticeDepService orgTrnsNoticeDepService;
    @Autowired
    private transient OrgTrnsAssignNoticeService orgTrnsAssignNoticeService;
    @Autowired
    private transient OrgTrnsService orgTrnsService;
    @Autowired
    private transient WorkBackendParamHelper workBackendParamHelper;
    @Autowired
    private transient EntityManager entityManager;

    // ========================================================================
    // 變數區
    // ========================================================================
    @Getter
    @Setter
    private OrgTrnsBaseComponentPageVO cpontVO = new OrgTrnsBaseComponentPageVO();

    /**
     * 待轉筆數資料
     */
    Map<String, Integer> waitTransCountMapByDataKey;

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * 全轉
     * 
     * @param selVerifyDtVO
     * @param trnsTypeStr
     */
    public void beforeAllTrans(OrgTrnsWorkVerifyVO selVerifyDtVO, String trnsTypeStr) {
        // ====================================
        // 檢核
        // ====================================
        if (selVerifyDtVO == null) {
            MessagesUtils.showError("找不到選擇的資料!");
            return;
        }

        RequireTransProgramType trnsType = null;
        try {
            trnsType = RequireTransProgramType.valueOf(trnsTypeStr);
        } catch (Exception e) {
        }

        if (trnsType == null) {
            MessagesUtils.showError("傳入類別錯誤! [" + trnsTypeStr + "]");
            return;
        }

        // ====================================
        // 初始化VO
        // ====================================
        this.cpontVO = new OrgTrnsBaseComponentPageVO();

        // 紀錄傳入參數
        this.cpontVO.setSelVerifyDtVO(selVerifyDtVO);
        this.cpontVO.setTrnsType(trnsType);

        // ====================================
        // 查詢
        // ====================================
        try {
            // 將全部查到的資料放進放進『已選擇』
            this.cpontVO.setSelectedDtVOList(
                    this.orgTrnsNoticeDepService.queryNoticeDep(
                            this.getCpontVO().getSelVerifyDtVO().getBeforeOrgSid(),
                            this.getCpontVO().getTrnsType()));
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            MessagesUtils.showError("系統錯誤!" + e.getMessage());
            log.error(e.getMessage(), e);
            return;
        }

        if (WkStringUtils.isEmpty(this.cpontVO.getSelectedDtVOList())) {
            MessagesUtils.showInfo("沒有需要轉檔的資料!");
            return;
        }

        // ====================================
        // 確認視窗
        // ====================================
        String confimInfo = "待轉筆數：【%s】，確定要轉檔嗎？";
        confimInfo = String.format(confimInfo, WkHtmlUtils.addBlueBlodClass(this.cpontVO.getSelectedDtVOList().size() + ""));

        this.confirmCallbackDialogController.showConfimDialog(
                confimInfo,
                "",
                () -> this.btnTrnsSelected());

    }

    /**
     * 畫面功能:開啟操作視窗
     */
    public void openTrnsWindow(OrgTrnsWorkVerifyVO selVerifyDtVO, String trnsTypeStr) {

        // ====================================
        // 檢核
        // ====================================
        if (selVerifyDtVO == null) {
            MessagesUtils.showError("找不到選擇的資料!");
            return;
        }

        RequireTransProgramType trnsType = null;
        try {
            trnsType = RequireTransProgramType.valueOf(trnsTypeStr);
        } catch (Exception e) {
        }

        if (trnsType == null) {
            MessagesUtils.showError("傳入類別錯誤! [" + trnsTypeStr + "]");
            return;
        }

        // ====================================
        // 初始化VO
        // ====================================
        this.cpontVO = new OrgTrnsBaseComponentPageVO();

        // 紀錄傳入參數
        this.cpontVO.setSelVerifyDtVO(selVerifyDtVO);
        this.cpontVO.setTrnsType(trnsType);

        // ====================================
        // 查詢
        // ====================================
        boolean isSuccess = this.btnQuery();
        // 打開視窗
        if (isSuccess) {
            this.displayController.showPfWidgetVar("dlgNoticeDepsTransform");
        }
    }

    /**
     * 畫面功能:查詢
     */
    public boolean btnQuery() {
        // ====================================
        // 頁面初始化
        // ====================================
        // 清空
        this.cpontVO.setSelectedDtVOList(null);
        // 更新頁面
        this.displayController.update("dlgNoticeDepsTransform_id");
        this.displayController.clearDtatableFilters("dlgNoticeDepsTransform_Datatable");

        if (this.getCpontVO() == null || this.getCpontVO().getSelVerifyDtVO() == null) {
            MessagesUtils.showError("資料已遺失, 請重新刷新頁面");
            return false;
        }

        // ====================================
        // 查詢
        // ====================================
        try {
            this.cpontVO.setShowDtVOList(
                    this.orgTrnsNoticeDepService.queryNoticeDep(
                            this.getCpontVO().getSelVerifyDtVO().getBeforeOrgSid(),
                            this.getCpontVO().getTrnsType()));
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return false;
        } catch (Exception e) {
            MessagesUtils.showError("系統錯誤!" + e.getMessage());
            log.error(e.getMessage(), e);
            return false;
        }

        return true;
    }

    /**
     * 畫面功能:轉換選擇資料
     */
    public void btnTrnsSelected() {

        // ====================================
        // 檢核
        // ====================================
        if (WkStringUtils.isEmpty(this.cpontVO.getSelectedDtVOList())) {
            MessagesUtils.showError("未選擇單據");
            return;
        }

        // ====================================
        // 均分,避免執行太大量時, 時間過長
        // ====================================
        List<List<OrgTrnsDtVO>> lists = WkCommonUtils.averageAssign(
                this.cpontVO.getSelectedDtVOList(), 
                workBackendParamHelper.findReqOrgTrnsCount());

        // ====================================
        // 轉檔
        // ====================================
        List<String> processMessages = Lists.newArrayList();

        for (List<OrgTrnsDtVO> currProcesslist : lists) {
            
            if(WkStringUtils.isEmpty(currProcesslist)) {
                continue;
            }
            
            try {

                if (RequireTransProgramType.REQUIRE.equals(this.cpontVO.getTrnsType())) {
                    // 需求單專用

                    // 建立轉檔前後對應關係
                    Map<Integer, Integer> trnsDepMapping = Maps.newHashMap();
                    trnsDepMapping.put(
                            this.cpontVO.getSelVerifyDtVO().getBeforeOrgSid(),
                            this.cpontVO.getSelVerifyDtVO().getAfterOrgSid());

                    this.orgTrnsAssignNoticeService.trnsAssignSendInfo(
                            currProcesslist,
                            trnsDepMapping,
                            AssignSendType.SEND,
                            new Date());

                } else {

                    // 其他子單
                    this.orgTrnsNoticeDepService.processTrns(
                            this.cpontVO.getSelVerifyDtVO(),
                            SecurityFacade.getUserSid(),
                            this.cpontVO.getTrnsType(),
                            currProcesslist);

                }

                this.entityManager.clear();

                processMessages.add(currProcesslist.size() + "筆執行成功");

            } catch (Exception e) {
                String msg = "執行失敗!" + e.getMessage();
                processMessages.add(WkHtmlUtils.addRedBlodClass(currProcesslist.size() + "筆" + msg));
                log.error(msg, e);
                continue;
            }
        }

        MessagesUtils.showInfo(String.join("<br/>", processMessages));

        // ====================================
        // 重新查詢
        // ====================================
        this.btnQuery();
    }

    /**
     * 計算待轉筆數
     * 
     * @param orgTransMappingTos
     * @throws UserMessageException
     */
    public void executeCountAllWaitTrans(List<OrgTrnsWorkVerifyVO> orgTransMappingTos) throws UserMessageException {
        this.waitTransCountMapByDataKey = this.orgTrnsNoticeDepService.countAllWaitTrans(orgTransMappingTos);
    }

    /**
     * 取得待轉筆數字串
     * 
     * @param verifyVO
     * @param programTypeStr
     * @return
     */
    public String getWaitTransCount(OrgTrnsWorkVerifyVO verifyVO, String programTypeStr) {
        return this.orgTrnsService.prepareWaitTransCountInfo(
                this.waitTransCountMapByDataKey,
                verifyVO,
                programTypeStr);
    }
}
