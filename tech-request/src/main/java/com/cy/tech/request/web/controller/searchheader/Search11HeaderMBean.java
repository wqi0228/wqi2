/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.searchheader;

import com.cy.tech.request.logic.service.SearchService;
import com.cy.work.common.enums.ReadRecordType;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 歷史單據 的搜尋表頭
 *
 * @author jason_h
 */
@Controller
@Scope("view")
public class Search11HeaderMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7614966525922835463L;
    @Autowired
    transient private SearchService searchServie;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;

    @PostConstruct
    private void init() {
        this.clear();
    }

    /**
     * 清除/還原選項
     */
    public void clear() {
        this.initCommHeader();
    }

    /**
     * 初始化commHeader
     */
    private void initCommHeader() {
        commHeaderMBean.clear();
        commHeaderMBean.clearAdvance();
        commHeaderMBean.initDefaultRequireDepts(true);
        commHeaderMBean.setStartDate(searchServie.transStartDate(searchServie.addDay(Calendar.getInstance(), -30)));
        commHeaderMBean.setEndDate(new Date());
        //共用查詢條件初始化
        commHeaderMBean.publicConditionInit();
    }

    public SelectItem[] getReadRecordTypeItems() {
        ReadRecordType[] types = ReadRecordType.values();
        SelectItem[] items = new SelectItem[types.length];
        for (int i = 0; i < types.length; i++) {
            items[i] = new SelectItem(types[i], types[i].getValue());
        }
        return items;
    }
}
