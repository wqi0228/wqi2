/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.jsf.converter;

import com.cy.commons.vo.Org;
import com.cy.tech.request.logic.service.OrganizationService;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 前端JSF、PF元件轉換組織
 *
 * @author shaun
 */
@Component("orgConverter")
public class OrgConverter implements Converter {

    @Autowired
    transient private OrganizationService orgService;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if ((value == null) || "".equals(value.trim()) || "null".equals(value.trim())) {
            return null;
        }
        return orgService.findBySid(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value != null && value instanceof Org) {
            return String.valueOf(((Org) value).getSid());
        }
        if (value != null && value instanceof String) {
            return (String) value;
        }
        if (value != null && value instanceof Integer) {
            return ((Integer) value).toString();
        }
        return "";
    }

}
