/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require;

import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author shaun
 */
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class RequireBottomTabMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 31946729803023399L;

    @Getter
    @Setter
    private int tabIndex = 0;

    /** 轉入需求單 */
    private final RequireBottomTabType transferFromTechTab = RequireBottomTabType.TRANSFER_FROM_TECH;
    /** 追蹤 */
    private final RequireBottomTabType traceTab = RequireBottomTabType.TRACE;
    /** 需求單位簽核進度 */
    private final RequireBottomTabType requireUnitSignTab = RequireBottomTabType.REQUIT_UNIT_SIGN;
    /** 技術單位主管簽核進度 */
    private final RequireBottomTabType techManagerSignTab = RequireBottomTabType.TECH_MANAGER_SIGN;
    /** 原型確認資訊 */
    private final RequireBottomTabType ptInfoTab = RequireBottomTabType.PT_CHECK_INFO;
    /** 送測資訊 */
    private final RequireBottomTabType sendTestInfoTab = RequireBottomTabType.SEND_TEST_INFO;
    /** ON程式 */
    private final RequireBottomTabType onpgTab = RequireBottomTabType.ONPG;
    /** 其它設定資訊 */
    private final RequireBottomTabType othsetTab = RequireBottomTabType.OTH_SET;
    /** 分派&通知資訊 */
    private final RequireBottomTabType assignSendTab = RequireBottomTabType.ASSIGN_SEND_INFO;
    /** 分派資訊 */
    // private final RequireBottomTabType assignInfoTab =
    // RequireBottomTabType.ASSIGN_INFO;
    /** 轉FB記錄 */
    private final RequireBottomTabType deliverFbRec = RequireBottomTabType.DELIVER_FB_REC;
    /** 附加檔案 */
    private final RequireBottomTabType attachmentTab = RequireBottomTabType.ATTACHMENT;
    /** 關聯群組 */
    private final RequireBottomTabType linkGroupTab = RequireBottomTabType.LINK_GROUP;
    /** 收呈報 */
    private final RequireBottomTabType inboxReport = RequireBottomTabType.INBOX_REPORT;
    /** 收呈報 */
    private final RequireBottomTabType inboxPersonal = RequireBottomTabType.INBOX_PERSONAL;
    /** 收呈報 */
    private final RequireBottomTabType inboxInstruction = RequireBottomTabType.INBOX_INSTRUCTION;
    /** 寄件備份 */
    private final RequireBottomTabType inboxSend = RequireBottomTabType.INBOX_SEND;

    /** 全部tab的集合 */
    private final List<RequireBottomTabType> tabSet = Lists.newArrayList(
            transferFromTechTab, traceTab, requireUnitSignTab, techManagerSignTab, ptInfoTab, sendTestInfoTab,
            onpgTab, othsetTab, assignSendTab, deliverFbRec, attachmentTab,
            linkGroupTab, inboxReport, inboxPersonal, inboxInstruction, inboxSend);

    /**
     * 整理Tab index位置<BR/>
     * 設定變數值的位置不可任意上下變動
     *
     * @param require
     * @return
     */
    public int resetTabIdx(Require01MBean r01MBean) {
        Require require = r01MBean.getRequire();
        int cursorIdx = r01MBean.getTraceMBean().countRequireTraceByTransferFromTech(r01MBean)>0 ? 1 : 0;
        transferFromTechTab.setTabIdx(cursorIdx);
        traceTab.setTabIdx(require.getHasTrace() ? ++cursorIdx : 0);
        requireUnitSignTab.setTabIdx(require.getHasReqUnitSign() ? ++cursorIdx : 0);
        techManagerSignTab.setTabIdx(require.getHasTechManagerSign() ? ++cursorIdx : 0);
        ptInfoTab.setTabIdx(require.getHasPrototype() ? ++cursorIdx : 0);
        sendTestInfoTab.setTabIdx(require.getHasTestInfo() ? ++cursorIdx : 0);
        onpgTab.setTabIdx(require.getHasOnpg() ? ++cursorIdx : 0);
        othsetTab.setTabIdx(require.getHasOthSet() ? ++cursorIdx : 0);
        assignSendTab.setTabIdx(require.getHasAssign() ? ++cursorIdx : 0);
        deliverFbRec.setTabIdx(require.getHasFbRecord() ? ++cursorIdx : 0);
        attachmentTab.setTabIdx(require.getHasAttachment() ? ++cursorIdx : 0);
        linkGroupTab.setTabIdx(require.getHasLink() ? ++cursorIdx : 0);
        inboxReport.setTabIdx(r01MBean.getIxReportMBean().showTab(require) ? ++cursorIdx : 0);
        inboxPersonal.setTabIdx(r01MBean.getIxPersonalMBean().showTab(require) ? ++cursorIdx : 0);
        inboxInstruction.setTabIdx(r01MBean.getIxInstructionMBean().showTab(require) ? ++cursorIdx : 0);
        inboxSend.setTabIdx(r01MBean.getWorkInboxComponent().isShowSentBackup() ? ++cursorIdx : 0);
        return cursorIdx;
    }

    /**
     * 切換頁籤
     *
     * @param type
     */
    public void changeTabByTabType(RequireBottomTabType type) {
        for (RequireBottomTabType each : tabSet) {
            if (each.equals(type) && each.getTabIdx() != 0) {
                tabIndex = each.getTabIdx() - 1;
                return;
            }
        }
        this.changeTabByTabAuto();
    }

    public void changeTabByTabAuto() {
        for (RequireBottomTabType each : tabSet) {
            if (each.getTabIdx() != 0) {
                tabIndex = each.getTabIdx() - 1;
                break;
            }
        }
    }

    public void onCheangeTab(Require01MBean r01MBean) {
        this.resetTabIdx(r01MBean);
    }

    /**
     * 頁籤是否在指定位置
     *
     * @param type
     * @return
     */
    public boolean currentTabByName(RequireBottomTabType type) {
        if (type.equals(RequireBottomTabType.TRANSFER_FROM_TECH)) {
            return transferFromTechTab.getTabIdx() == this.tabIndex + 1;
        }
        // 一般
        if (type.equals(RequireBottomTabType.TRACE)) {
            return traceTab.getTabIdx() == this.tabIndex + 1;
        }
        if (type.equals(RequireBottomTabType.ASSIGN_SEND_INFO)) {
            return assignSendTab.getTabIdx() == this.tabIndex + 1;
        }
        // 子程序
        if (type.equals(RequireBottomTabType.PT_CHECK_INFO)) {
            return ptInfoTab.getTabIdx() == this.tabIndex + 1;
        }
        if (type.equals(RequireBottomTabType.SEND_TEST_INFO)) {
            return sendTestInfoTab.getTabIdx() == this.tabIndex + 1;
        }
        if (type.equals(RequireBottomTabType.ONPG)) {
            return onpgTab.getTabIdx() == this.tabIndex + 1;
        }
        if (type.equals(RequireBottomTabType.OTH_SET)) {
            return othsetTab.getTabIdx() == this.tabIndex + 1;
        }
        // 收件夾
        if (type.equals(RequireBottomTabType.INBOX_REPORT)) {
            return inboxReport.getTabIdx() == this.tabIndex + 1;
        }
        if (type.equals(RequireBottomTabType.INBOX_PERSONAL)) {
            return inboxPersonal.getTabIdx() == this.tabIndex + 1;
        }
        if (type.equals(RequireBottomTabType.INBOX_INSTRUCTION)) {
            return inboxInstruction.getTabIdx() == this.tabIndex + 1;
        }
        if (type.equals(RequireBottomTabType.INBOX_SEND)) {
            return inboxSend.getTabIdx() == this.tabIndex + 1;
        }
        return false;
    }

}
