/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import com.cy.tech.request.vo.require.Require;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.text.WordUtils;
import org.omnifaces.util.Faces;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 上下筆移動控制 <BR/>
 * 移動容器 field name 需統一命名為<BR/>
 * <BR/>
 * queryItems (datatable)<BR/>
 * querySelection (selection)<BR/>
 * queryKeeper (selection backup) <BR/>
 * <BR/>
 * 需實作 <BR/>
 * toggleSearchBody method <BR/>
 * moveRequireTemplateSelect method <BR/>
 * <BR/>
 * 參考 require03_title_btn.xhtml
 *
 * <BR/>
 *
 * @author shaun
 */
@Controller
@Scope("view")
@Slf4j
public class SearchUpDownMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7589561156425645814L;
    /** Keeper 非 Require 時，該 SearchMBean 需實作此method */
    public final static String METHOD_UPDATE_QUERY_KEEPER = "updateQueryKeeper";
    /** queryItems 非 List Require 時，該 SearchMBean 需實作此method */
    public final static String METHOD_UPDATE_QUERY_ITEMS = "updateQueryItems";
    /** Keeper 非 Require 時，該 SearchMBean 需實作此method */
    public final static String METHOD_FIND_REQUIRE_BY_KEEPER = "findRequireByKeeper";

    public final static String FIELD_QUERY_ITEMS = "queryItems";
    public final static String FIELD_QUERY_KEEPER = "queryKeeper";
    public final static String FIELD_QUERY_SELECTION = "querySelection";
    public final static String FIELD_DATA_TABLE_ID = "dataTableId";

    /**
     * 尋找控制MBean
     *
     * @return
     */
    public Object findSearchMBean() {
        String viewMBeanName = Faces.getViewId().replaceAll("/.*/|.xhtml+", "").concat("MBean");
        return Faces.getApplication().getELResolver().getValue(Faces.getELContext(), null, viewMBeanName);
    }

    /**
     * 搜尋報表中主要 datatable value
     *
     * @return
     */
    public Object findSearchMBeanQueryItems() {
        return this.findFieldValue(FIELD_QUERY_ITEMS, this.findSearchMBean());
    }

    /**
     * 搜尋報表中記憶上下筆移動參照
     *
     * @return
     */
    public Object findSearchMBeanKeeper() {
        return this.findFieldValue(FIELD_QUERY_KEEPER, this.findSearchMBean());
    }

    /**
     * 搜尋報表中主要 datatable selection
     *
     * @return
     */
    public Object findSearchMBeanSelection() {
        return this.findFieldValue(FIELD_QUERY_SELECTION, this.findSearchMBean());
    }

    private Object findFieldValue(String fieldName, Object searchMBean) {
        try {
            if (searchMBean.getClass().getSimpleName().contains("EnhancerBySpringCGLIB")) {
                log.debug("MBean來自於Spring CGLIB... " + searchMBean.getClass().getSimpleName());
                return this.findFieldValueBySpringProxy(fieldName, searchMBean);
            }
            Field field = searchMBean.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            return field.get(searchMBean);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            log.error(ex.getMessage(), ex);
        }
        return null;
    }

    private Object findFieldValueBySpringProxy(String fieldName, Object searchMBean) {
        String refltMethod = "get" + WordUtils.capitalize(fieldName);
        try {
            Method mehtod = searchMBean.getClass().getDeclaredMethod(refltMethod);
            return mehtod.invoke(searchMBean);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            log.error("嘗試反射方法取得對應值失敗..." + ex.getMessage(), ex);
        }
        return null;
    }

    public void resetSearchMBeanKeeper(Object searchMBean, Require require) {
        try {
            Field field = searchMBean.getClass().getDeclaredField(FIELD_QUERY_KEEPER);
            field.setAccessible(true);
            field.set(searchMBean, require);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    public String findSearchMBeranTableId() {
        return (String) this.findFieldValue(FIELD_DATA_TABLE_ID, this.findSearchMBean());
    }

    public void callSearchMBeanMethod(String methodName) {
        Object searchMBean = this.findSearchMBean();
        try {
            Method method = searchMBean.getClass().getDeclaredMethod(methodName);
            method.setAccessible(true);
            method.invoke(searchMBean);
        } catch (NoSuchMethodException | SecurityException ex) {
            if (ex instanceof NoSuchMethodException) {
                log.debug("searchMbean :" + searchMBean.getClass().getSimpleName() + "not has this method : " + methodName);
                return;
            }
            log.error(ex.getMessage(), ex);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    @SuppressWarnings("rawtypes")
    public void callSearchMBeanMethod(String methodName, Object... params) {
        Object searchMBean = this.findSearchMBean();
        try {
            Class[] paraClz = new Class[params.length];
            for (int i = 0; i < params.length; i++) {
                paraClz[i] = params[i].getClass();
            }
            Method method = searchMBean.getClass().getDeclaredMethod(methodName, paraClz);
            method.setAccessible(true);
            method.invoke(searchMBean, params);
        } catch (NoSuchMethodException | SecurityException ex) {
            if (ex instanceof NoSuchMethodException) {
                log.debug("searchMbean :" + searchMBean.getClass().getSimpleName() + "not has this method : " + methodName);
                return;
            }
            log.error(ex.getMessage(), ex);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    public Object callSearchMBeanMethodByGet(String methodName) {
        Object searchMBean = this.findSearchMBean();
        try {
            Method method = searchMBean.getClass().getDeclaredMethod(methodName);
            method.setAccessible(true);
            return method.invoke(searchMBean);
        } catch (NoSuchMethodException | SecurityException ex) {
            if (ex instanceof NoSuchMethodException) {
                log.debug("searchMbean :" + searchMBean.getClass().getSimpleName() + "not has this method : " + methodName);
                return null;
            }
            log.error(ex.getMessage(), ex);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            log.error(ex.getMessage(), ex);
        }
        return null;
    }

    @SuppressWarnings("rawtypes")
    public Object callSearchMBeanMethodByGet(String methodName, Object... params) {
        Object searchMBean = this.findSearchMBean();
        try {
            Class[] paraClz = new Class[params.length];
            for (int i = 0; i < params.length; i++) {
                paraClz[i] = params[i].getClass();
            }
            Method method = searchMBean.getClass().getDeclaredMethod(methodName, paraClz);
            method.setAccessible(true);
            return method.invoke(searchMBean, params);
        } catch (NoSuchMethodException | SecurityException ex) {
            if (ex instanceof NoSuchMethodException) {
                log.debug("searchMbean :" + searchMBean.getClass().getSimpleName() + "not has this method : " + methodName);
                return null;
            }
            log.error(ex.getMessage(), ex);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            log.error(ex.getMessage(), ex);
        }
        return null;
    }
}
