package com.cy.tech.request.web.newsearch.newsearch03;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.service.URLService.URLServiceAttr;
import com.cy.tech.request.web.newsearch.BaseNewSearchController;
import com.cy.tech.request.web.newsearch.INewSearchController;
import com.cy.tech.request.web.newsearch.NewSearchConditionVO;
import com.cy.tech.request.web.newsearch.NewSearchService;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.vo.WkItem;
import com.cy.work.viewcomponent.treepker.TreePickerCallback;
import com.cy.work.viewcomponent.treepker.TreePickerComponent;
import com.cy.work.viewcomponent.treepker.helper.TreePickerDepHelper;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Controller
@Scope("view")
@Slf4j
public class NewSearch03Controller extends BaseNewSearchController implements INewSearchController, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8138748707406988528L;

    // ========================================================================
    // 服務區
    // ========================================================================
    @Autowired
    private transient NewSearchService newSearchService;
    @Autowired
    private transient TreePickerDepHelper treePickerDepHelper;

    // ========================================================================
    // 變數區
    // ========================================================================
    /**
     * 被通知單位選擇器
     */
    @Getter
    @Setter
    public transient TreePickerComponent noticeDepTreePicker;

    // ========================================================================
    // 初始化
    // ========================================================================
    @PostConstruct
    public void init() {
        // ====================================
        // 初始化查詢條件
        // ====================================
        this.initQueryCondition();

        // ====================================
        // 初始化元件
        // ====================================
        // 初始化類別組合樹
        this.categoryTreePicker = new TreePickerComponent(categoryPickerCallback, "類別組合");
        // 初始化分派/通知單位樹
        this.noticeDepTreePicker = new TreePickerComponent(noticeDepTreeCallback, "被通知單位");

        // ====================================
        // 初始化畫面後 , 觸發查詢
        // ====================================
        this.displayController.execute("doSearchData()");
    }

    public void initQueryCondition() {

        // ====================================
        // 初始化查詢條件 VO
        // ====================================
        this.conditionVO = new NewSearchConditionVO();

        // 可閱部門-上升到部，不包含特殊可閱
        Set<Integer> depSids = this.wkUserAndOrgLogic.prepareCanViewDepSidsBaseOnMinsterial(SecurityFacade.getUserSid(), false);
        this.conditionVO.setFilterSendDepSids(depSids);

        // 單據狀態，預設選擇未結案
        this.conditionVO.setSelectedColseCode(Lists.newArrayList("0"));

        // 首頁過來的帶『未閱讀』
        if (Faces.getRequestParameterMap().containsKey(URLServiceAttr.URL_ATTR_NEWSEARCH03_CONDITION_TYPE.getAttr())) {
            this.conditionVO.setSelectReadRecordType(ReadRecordType.UN_READ);
        }

    }

    // ========================================================================
    // 頁面功能
    // ========================================================================
    public void search() {
        try {
            this.queryItems = this.newSearchService.queryByCondition(ReportType.NEW_SEARCH03, conditionVO);
        } catch (Exception e) {
            log.error("查詢失敗，請洽系統人員! [" + e.getMessage() + "]", e);
            MessagesUtils.showError("查詢失敗，請洽系統人員! [" + e.getMessage() + "]");
            return;
        }
    }

    public void clear() {
        this.initQueryCondition();
        try {
            this.categoryTreePicker.rebuild();
            this.noticeDepTreePicker.rebuild();
        } catch (Exception e) {
            String message = WkMessage.EXECTION + "[" + e.getMessage() + "]";
            log.error(message, e);
            MessagesUtils.showError(message);
            return;
        }
    }

    // ========================================================================
    // 通知單位選單
    // ========================================================================
    /**
     * 開啟通知單位選擇視窗
     */
    public void event_dialog_noticeDepTree_open() {

        try {
            this.noticeDepTreePicker.rebuild();
        } catch (Exception e) {
            String message = "開啟【被通知單位】設定視窗失敗！" + e.getMessage();
            log.error(message, e);
            MessagesUtils.showError(message);
            return;
        }

        this.displayController.showPfWidgetVar("wv_dlg_assignNoticeDep");
    }

    /**
     * 通知單位選擇視窗：確認
     */
    public void event_dialog_noticeDepTree_confirm() {
        // 取得選擇的單位
        this.conditionVO.setFilterSendDepSids(this.noticeDepTreePicker.getSelectedItemIntegerSids());
        // 關閉視窗
        this.displayController.hidePfWidgetVar("wv_dlg_assignNoticeDep");
    }

    /**
     * 通知單位選單 - callback 事件
     */
    private final TreePickerCallback noticeDepTreeCallback = new TreePickerCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = 4414127323755248223L;

        @Override
        public List<WkItem> prepareAllItems() throws Exception {

            // 可閱單位
            // 主要部門上升到部
            // 主要部門含以下
            // 管理部門上升到部
            // 管理部門含以下
            // 特殊可閱
            Set<Integer> allCanUseDeps = wkUserAndOrgLogic.prepareCanViewDepSidsBaseOnPanel(
                    SecurityFacade.getUserSid(),
                    true);

            return treePickerDepHelper.prepareDepItems(
                    SecurityFacade.getCompanyId(),
                    Lists.newArrayList(allCanUseDeps));
        }

        /**
         * 準備已選擇項目
         * 
         * @return
         */
        @Override
        public List<String> prepareSelectedItemSids() throws Exception {
            // 回傳
            return conditionVO.getFilterSendDepSids().stream()
                    .map(sid -> sid + "")
                    .collect(Collectors.toList());
        }

        @Override
        public List<String> prepareDisableItemSids() throws Exception {
            return Lists.newArrayList();
        }
    };
}
