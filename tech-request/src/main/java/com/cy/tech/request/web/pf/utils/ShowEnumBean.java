
package com.cy.tech.request.web.pf.utils;

import java.io.Serializable;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.enums.OthSetHistoryBehavior;
import com.cy.tech.request.vo.enums.OthSetStatus;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgCheckRecordReplyType;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgHistoryBehavior;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.tech.request.vo.pt.enums.PtHistoryBehavior;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.cy.tech.request.vo.worktest.enums.WorkTestInfoStatus;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.enums.WorkSourceType;

/**
 * 取得列舉顯示值
 * 
 * @author allen1214_wu
 */
@Component
public class ShowEnumBean implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5106194644717074185L;
    private static ShowEnumBean instance;

    public static ShowEnumBean getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        ShowEnumBean.instance = this;
    }

    /**
     * 原型確認類別
     * 
     * @param enumType
     * @return
     */
    public String getPtStatus(PtStatus enumType) {
        if (enumType == null) {
            return "";
        }
        return enumType.getLabel();
    }

    /**
     * showEnumBean.getPtHistoryBehavior
     * 
     * @param enumType
     * @return
     */
    public String getPtHistoryBehavior(PtHistoryBehavior enumType) {
        if (enumType == null) {
            return "";
        }
        return enumType.getLabel();
    }

    /**
     * @param enumType
     * @return
     */
    public String getWorkOnpgStatus(WorkOnpgStatus enumType) {
        if (enumType == null) {
            return "";
        }
        return enumType.getLabel();
    }

    public String getWorkOnpgHistoryBehavior(Object enumType) {

        if (enumType == null) {
            return "";
        }

        if (enumType instanceof WorkOnpgHistoryBehavior) {
            return ((WorkOnpgHistoryBehavior) enumType).getLabel();
        }

        if (enumType instanceof WorkOnpgCheckRecordReplyType) {
            return ((WorkOnpgCheckRecordReplyType) enumType).getLabel();
        }

        return "";

    }

    /**
     * showEnumBean.getWorkTestStatus
     * 
     * @param enumType
     * @return
     */
    public String getWorkTestStatus(WorkTestStatus enumType) {
        if (enumType == null) {
            return "";
        }
        return enumType.getValue();
    }

    /**
     * @param enumType
     * @return
     */
    public String getWorkTestInfoStatus(WorkTestInfoStatus enumType) {
        if (enumType == null) {
            return "";
        }
        return enumType.getValue();
    }

    /**
     * @param enumType
     * @return
     */
    public String geWorkTestStatus(WorkTestStatus enumType) {
        if (enumType == null) {
            return "";
        }
        return enumType.getValue();
    }

    /**
     * @param enumType
     * @return
     */
    public String getOthSetStatus(OthSetStatus enumType) {
        if (enumType == null) {
            return "";
        }
        return enumType.getVal();
    }

    /**
     * @param enumType
     * @return
     */
    public String getOthSetHistoryBehavior(OthSetHistoryBehavior enumType) {
        if (enumType == null) {
            return "";
        }
        return enumType.getVal();
    }

    /**
     * 簽核進度<br/>
     * showEnumBean.getInstanceStatus
     * 
     * @param enumType
     * @return
     */
    public String getInstanceStatus(InstanceStatus enumType) {
        if (enumType == null) {
            return "";
        }
        return enumType.getLabel();
    }

    /**
     * @param enumType
     * @return
     */
    public String getActivation(Activation enumType) {
        if (enumType == null) {
            return "";
        }
        if (Activation.ACTIVE.equals(enumType)) {
            return "正常";
        }
        if (Activation.INACTIVE.equals(enumType)) {
            return "停用";
        }
        return "";
    }

    /**
     * @param enumType
     * @return
     */
    public String getWorkSourceType(WorkSourceType enumType) {
        if (enumType == null) {
            return "";
        }
        return enumType.getValue();
    }

    /**
     * 需求製作進度<br/>
     * showEnumBean.getRequireStatusType
     * 
     * @param enumType
     * @return
     */
    public String getRequireStatusType(RequireStatusType enumType) {
        if (enumType == null) {
            return "";
        }
        return enumType.getValue();
    }

    /**
     * 追蹤類型<br/>
     * showEnumBean.getRequireTraceType
     * 
     * @param enumType
     * @return
     */
    public String getRequireTraceType(RequireTraceType enumType) {
        if (enumType == null) {
            return "";
        }
        return enumType.getLabel();
    }

    /**
     * 緊急度<br/>
     * showEnumBean.getUrgencyType
     * 
     * @param enumType
     * @return
     */
    public String getUrgencyType(UrgencyType enumType) {
        if (enumType == null) {
            return "";
        }
        return enumType.getValue();
    }

    /**
     * showEnumBean.getReadRecordType
     * 
     * @param enumType
     * @return
     */
    public String getReadRecordType(ReadRecordType enumType) {
        if (enumType == null) {
            return "";
        }
        return enumType.getValue();
    }

    /**
     * 待閱原因
     * showEnumBean.getReqToBeReadType
     * 
     * @param enumType
     * @return
     */
    public String getReqToBeReadType(ReqToBeReadType enumType) {
        if (enumType == null) {
            return "";
        }
        return enumType.getLabel();
    }

    //

}
