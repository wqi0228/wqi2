package com.cy.tech.request.web.controller.component.mutiuserqkpker;

import java.io.Serializable;
import java.util.List;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.web.controller.component.mutiuserqkpker.model.MultiUserQuickPickerUserGroupData;
import com.cy.work.common.exception.SystemDevelopException;
import lombok.NoArgsConstructor;

/**
 * @author allen
 *
 */
@NoArgsConstructor
public class MultiUserQuickPickerCallback implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5006079154943552508L;

    /**
     * @return 群組項目
     * @throws SystemDevelopException
     */
    public List<MultiUserQuickPickerUserGroupData> prepareGroupItems() throws SystemDevelopException {

        // throw new SystemDevelopException("有開啟群組功能，但未實做準備群組資料功能");
        
        return MultiUserQuickPickerHelper.getInstance().findUserGroups(SecurityFacade.getUserSid());
    }
}
