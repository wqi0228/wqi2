/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.anew.components.vo;

import java.io.Serializable;
import lombok.Getter;

/**
 * DialogOnpgSettingComponent 操作物件
 *
 * @author kasim
 */
public class DialogOnpgSettingTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1442970815437800542L;
    @Getter
    /** 執行者名稱 */
    private String confirmExecName;
    @Getter
    /** 執行時間 */
    private String confirmExecTime;

    public DialogOnpgSettingTo() {

    }

    public DialogOnpgSettingTo(String confirmExecName, String confirmExecTime) {
        this.confirmExecName = confirmExecName;
        this.confirmExecTime = confirmExecTime;
    }
}
