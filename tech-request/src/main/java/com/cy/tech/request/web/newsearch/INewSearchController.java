/**
 * 
 */
package com.cy.tech.request.web.newsearch;

/**
 * @author allen1214_wu
 * NewSearchController interface
 */
public interface INewSearchController {
	
	/**
	 * 實做查詢
	 */
	public void search();

	/**
	 * 實做清除查詢條件
	 */
	public void clear();
}
