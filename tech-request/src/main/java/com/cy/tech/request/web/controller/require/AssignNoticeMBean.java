/**
 * 
 */
package com.cy.tech.request.web.controller.require;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.AssignNoticeService;
import com.cy.tech.request.logic.service.AssignSendInfoService;
import com.cy.tech.request.logic.service.FogbugzService;
import com.cy.tech.request.logic.service.RequireCheckItemService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.pmis.PmisService;
import com.cy.tech.request.logic.service.setting.SettingCheckConfirmRightService;
import com.cy.tech.request.logic.service.setting.SettingDefaultAssignSendDepService;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.CheckItemStatus;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.require.AssignSendInfo;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireCheckItem;
import com.cy.tech.request.vo.value.to.AssignSendGroupsTo;
import com.cy.tech.request.vo.value.to.SetupInfoTo;
import com.cy.tech.request.web.controller.component.assignnotice.AssignNoticeComponent;
import com.cy.tech.request.web.controller.component.assignnotice.AssignNoticeComponentCallback;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.ConfirmCallbackDialogController;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 分派/通知
 * 
 * @author allen1214_wu
 *
 */
@NoArgsConstructor
@Controller
@Scope("view")
@Slf4j
public class AssignNoticeMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3921575217133254498L;
    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private transient LoginBean loginBean;
    @Autowired
    private transient DisplayController displayController;
    @Autowired
    private transient ConfirmCallbackDialogController confirmCallbackDialogController;
    @Autowired
    private transient AssignSendInfoService assignSendInfoService;
    @Autowired
    private transient AssignNoticeService assignNoticeService;
    @Autowired
    private transient RequireService requireService;
    @Autowired
    private transient FogbugzService fbService;
    @Autowired
    private transient PmisService pmisService;
    @Autowired
    private transient Require01MBean r01MBean;
    @PersistenceContext
    private transient EntityManager entityManager;
    @Autowired
    private transient RequireCheckItemService requireCheckItemService;
    @Autowired
    private transient SettingCheckConfirmRightService settingCheckConfirmRightService;
    @Autowired
    private transient SettingDefaultAssignSendDepService settingDefaultAssignSendDepService;

    // ========================================================================
    // 變數
    // ========================================================================
    /**
     * 分派、通知 component
     */
    @Getter
    private AssignNoticeComponent assignNoticeComponent;

    /**
     * 選定的需求單 sid
     */
    private String requireSid;

    /**
     * 同時進行檢查確認
     */
    @Getter
    private boolean isInitByConfirmCheck = false;

    /**
     * 開啟設定視窗的時間, 用以鎖定多視窗操作
     */
    private Long effectiveTime;

    // ========================================================================
    // 變數預先運算變數
    // ========================================================================
    /**
     * 初始值：檢查項目
     */
    private List<SelectItem> initCheckConfirmItem;

    /**
     * 初始值：預設已選擇檢查項目
     */
    private List<RequireCheckItemType> initSelectedCheckItems;
    /**
     * 初始值：已選擇分派單位
     */
    private List<Integer> initAssignDepSids;
    /**
     * 初始值：已選擇通知單位
     */
    private List<Integer> initNoticeDepSids;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * @param r01MBean
     */
    public void openDialog(boolean isInitByConfirmCheck) {

        // ====================================
        // 初始化參數
        // ====================================
        // 防呆
        if (r01MBean == null || r01MBean.getRequire() == null || r01MBean.getRequire().getSid() == null) {
            String message = WkMessage.PROCESS_FAILED + "[初始化失敗]";
            log.error(message);
            MessagesUtils.showError(message);
            return;
        }
        // 需求單 sid
        this.requireSid = r01MBean.getRequire().getSid();
        // 是否同時進行檢查確認
        this.isInitByConfirmCheck = isInitByConfirmCheck;
        // 紀錄開啟編輯視窗時間
        this.effectiveTime = System.currentTimeMillis();

        try {
            // ====================================
            // 判定設定模式
            // ====================================
            // 準備檢查項目
            this.prepareCheckConfirmItems();
            // 初始化已選擇的分派單位
            this.initAssignDepSids = this.prepareSelectedDeps(
                    requireSid,
                    AssignSendType.ASSIGN,
                    this.initCheckConfirmItem,
                    this.initSelectedCheckItems);
            // 初始化已選擇的通知單位
            this.initNoticeDepSids = this.prepareSelectedDeps(
                    requireSid,
                    AssignSendType.SEND,
                    this.initCheckConfirmItem,
                    this.initSelectedCheckItems);

            // ====================================
            // 初始化 dialog 元件
            // ====================================
            // 初始化 AssignNoticeComponent
            this.assignNoticeComponent = new AssignNoticeComponent(this.assignNoticeComponentCallback);
            // 放入算好的檢查項目
            this.assignNoticeComponent.setSelectedCheckItemTypes(this.initSelectedCheckItems);
            // 開啟視窗
            this.assignNoticeComponent.openDialog();
        } catch (Exception e) {
            String message = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            log.error(message, e);
            MessagesUtils.showError(message);
            return;
        }

        // ====================================
        // 畫面控制
        // ====================================
        // 開啟視窗
        displayController.showPfWidgetVar("assignNoticeSettingDlg");

    }

    /**
     * 確認
     * 
     * @param assignDepSids
     * @param noticeDepSids
     * @param isInitByConfirmCheck
     */
    private void processConfirm(
            List<Integer> assignDepSids,
            List<Integer> noticeDepSids,
            boolean isConfirmCheck) {

        List<String> errorMessage = Lists.newArrayList();
        Date sysDate = new Date();

        // ====================================
        // 執行檢查確認 + 分派
        // ====================================
        // 執行
        try {
            this.requireService.executeConfirmCheckAndAssignNotice(
                    requireSid,
                    effectiveTime,
                    assignDepSids,
                    noticeDepSids,
                    isConfirmCheck,
                    this.assignNoticeComponent.getSelectedCheckItemTypes(),
                    SecurityFacade.getUserSid(),
                    sysDate);
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            displayController.hidePfWidgetVar("assignNoticeSettingDlg");
            return;
        } catch (SystemOperationException e) {
            log.error(e.getMessage(), e);
            displayController.hidePfWidgetVar("assignNoticeSettingDlg");
            MessagesUtils.show(e);
            return;
        }

        // ====================================
        // 自動更新轉PMIS的需求單
        // ====================================
        try {

            if (pmisService.hasPmisHistory(r01MBean.getRequire().getRequireNo())) {
                r01MBean.transferToPMIS();
            }
        } catch (Exception ex) {
            log.error("轉PMIS失敗!" + ex.getMessage(), ex);
            errorMessage.add("轉PMIS失敗!");
            // 以下流程可繼續跑
        }

        // ====================================
        // 更新全頁面內容
        // ====================================
        try {
            r01MBean.reBuildeByUpdateFaild();
            r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.ASSIGN_SEND_INFO);
            // 重新讀取分派資料
            r01MBean.getAssignInfo().initTabInfo(r01MBean);
            // 重新讀取需求單資料
            r01MBean.setRequire(requireService.findByReqSid(requireSid));
        } catch (Exception ex) {
            log.error("畫面更新失敗!" + ex.getMessage(), ex);
            errorMessage.add("畫面更新失敗!");
            // 以下流程可繼續跑
        }

        // ====================================
        // 錯誤訊息
        // ====================================
        if (errorMessage.size() > 0) {
            MessagesUtils.showError(String.join("\r\n", errorMessage));
        }

        // ====================================
        // 未檢核完成訊息
        // ====================================
        if (isConfirmCheck) {
            List<RequireCheckItemType> waitCheckItems = requireCheckItemService.findWaitCheckItems(requireSid);
            if (WkStringUtils.notEmpty(waitCheckItems)) {
                String message = "<br/>"
                        + "單據尚有項目未檢查完成，可通知相關人員進行檢查!"
                        + "<br/>"
                        + "<br/>"
                        + "待檢查項目：["
                        + waitCheckItems.stream().map(RequireCheckItemType::getDescr).collect(Collectors.joining("]、["))
                        + "]";

                MessagesUtils.showInfo(message);
            }
        }

        // ====================================
        // 畫面控制
        // ====================================
        // 關閉視窗
        displayController.hidePfWidgetVar("assignNoticeSettingDlg");

    }

    /**
     * 已存在設定資訊
     * 
     * @return
     */
    public boolean isExstedSettingInfo(String requireSid, AssignSendType type) {

        // ====================================
        // 查詢已設定資料
        // ====================================
        AssignSendInfo info = this.assignSendInfoService.findLastActiveByRequireAndType(requireSid, type);
        if (info == null) {
            return false;
        }

        // ====================================
        // 判斷
        // ====================================
        // 一般設定
        SetupInfoTo setupInfo = info.getInfo();
        if (setupInfo != null
                && (WkStringUtils.notEmpty(setupInfo.getDepartment())
                        || WkStringUtils.notEmpty(setupInfo.getUsers()))) {
            return true;
        }

        // 群組設定 (目前未使用)
        AssignSendGroupsTo groupInfo = info.getGroupInfo();
        if (groupInfo != null && WkStringUtils.notEmpty(setupInfo.getDepartment())) {
            return true;
        }

        return false;
    }

    /**
     * 準備檢查項目選項
     * 
     * @return
     */
    private void prepareCheckConfirmItems() {

        this.initCheckConfirmItem = Lists.newArrayList();
        this.initSelectedCheckItems = Lists.newArrayList();

        // ====================================
        // 查詢單據的檢查項目 (系統別)
        // ====================================
        // 查詢
        List<RequireCheckItem> requireCheckItems = requireCheckItemService.findCheckItemsByRequireSid(requireSid);

        // ====================================
        // 查詢個人可檢查項目
        // ====================================
        // 查詢可檢查項目權限
        List<RequireCheckItemType> canCheckItems = settingCheckConfirmRightService.findUserCheckRights(
                SecurityFacade.getUserSid());

        // ====================================
        // 產生選擇項目
        // ====================================
        for (RequireCheckItem requireCheckItem : requireCheckItems) {

            // 檢查項目, 預設顯示為項目名稱
            SelectItem selectItem = new SelectItem(
                    requireCheckItem.getCheckItemType(),
                    requireCheckItem.getCheckItemType().getDescr());
            selectItem.setEscape(false);

            // 加入列表
            this.initCheckConfirmItem.add(selectItem);

            // 以下依據狀態判斷顯示方式
            String statusText = requireCheckItem.getCheckItemType().getDescr() + "&nbsp;";

            // ----------------
            // 已檢查
            // ----------------
            if (requireCheckItem.getCheckDate() != null) {
                // 準備顯示字串
                // 檢查狀態：【已檢查】
                statusText += WkHtmlUtils.addClass(
                        CheckItemStatus.CHECKED.getDescr(),
                        CheckItemStatus.CHECKED.getCssClass());
                // 檢查時間
                statusText += "&nbsp;" + WkDateUtils.formatDate(
                        requireCheckItem.getCheckDate(),
                        WkDateUtils.YYYY_MM_DD_HH24_mm_ss);
                // 檢查人員
                statusText += "&nbsp;(" + WkUserUtils.findNameBySid(requireCheckItem.getCheckUserSid()) + ")";
                selectItem.setLabel(statusText);

                // 已檢查項目-鎖定
                selectItem.setDisabled(true);
                // 加入已選擇項目
                if (!initSelectedCheckItems.contains(requireCheckItem.getCheckItemType())) {
                    initSelectedCheckItems.add(requireCheckItem.getCheckItemType());
                }
            }

            // ----------------
            // 待檢查
            // ----------------
            else {

                // 可檢查:【待檢查】
                if (canCheckItems.contains(requireCheckItem.getCheckItemType())) {
                    statusText += WkHtmlUtils.addClass(
                            CheckItemStatus.WAIT_CHECK.getDescr(),
                            CheckItemStatus.WAIT_CHECK.getCssClass());
                    selectItem.setLabel(statusText);
                }
                // 無權限檢查:【非項目檢查人員】
                else {
                    statusText += WkHtmlUtils.addClass(
                            CheckItemStatus.NO_RIGHT.getDescr(),
                            CheckItemStatus.NO_RIGHT.getCssClass());
                    selectItem.setLabel(statusText);
                    // 無權限項目-鎖定
                    selectItem.setDisabled(true);
                }
            }
        }

        // ====================================
        // 預設勾選狀態
        // ====================================
        // 1.可檢查項目數量(enable) = 1 時，預設勾選 (省略使用者操作步驟)
        // 2.可檢查項目數量(enable) > 1 時，預設不勾選 (操作使用者『需』明確確認)

        // 算出 enable (待檢查)狀態項目 數量
        long waitCheckItemCount = this.initCheckConfirmItem.stream()
                .filter(selectItem -> !selectItem.isDisabled())
                .count();

        // 【待檢查】項目預設勾選
        if (waitCheckItemCount == 1) {
            for (SelectItem selectItem : this.initCheckConfirmItem) {
                if (selectItem.getLabel().contains(CheckItemStatus.WAIT_CHECK.getDescr())) {

                    // 加入已選擇項目
                    RequireCheckItemType checkItemType = (RequireCheckItemType) selectItem.getValue();
                    if (!initSelectedCheckItems.contains(checkItemType)) {
                        initSelectedCheckItems.add(checkItemType);
                    }
                }
            }
        }
    }

    /**
     * @param requireSid
     * @param assignSendType
     * @return
     */
    private List<Integer> prepareSelectedDeps(
            String requireSid,
            AssignSendType assignSendType,
            List<SelectItem> checkItems,
            List<RequireCheckItemType> selectedCheckItemTypes) {

        // ====================================
        // 查詢需求單資料
        // ====================================
        // 查詢
        Require require = requireService.findByReqSid(requireSid);
        // 防呆
        if (require == null || require.getMapping() == null || require.getMapping().getSmall() == null) {
            return Lists.newArrayList();
        }

        // ====================================
        // 處理非檢查確認邏輯
        // ====================================
        // 回傳已設定資料
        if (!isInitByConfirmCheck) {

            // 查詢已設定資料
            AssignSendInfo info = assignSendInfoService.findLastActiveByRequireAndType(requireSid, assignSendType);

            if (info == null || info.getInfo() == null || WkStringUtils.isEmpty(info.getInfo().getDepartment())) {
                return Lists.newArrayList();
            }

            // str 轉 int
            return WkStringUtils.safeConvertStr2Int(info.getInfo().getDepartment());
        }

        // ====================================
        // 是否帶入預設單位判斷
        // ====================================
        // 初始的檢查項目選項，有任一『可檢查』，但『未勾選』時，不帶入預設單位
        // 狀況1.僅有一個可檢查項目，會預設『已勾選』
        // 狀況2.複數可檢查項目，會預設『全都不勾選』
        RequireCheckItemType defaultLoadCheckItemType = null;
        for (SelectItem selectItem : checkItems) {
            // 可檢查
            if (!selectItem.isDisabled()) {
                RequireCheckItemType currCheckItemType = RequireCheckItemType.safeValueOf(selectItem.getValue() + "");
                // 為勾選
                if (selectedCheckItemTypes == null || !selectedCheckItemTypes.contains(currCheckItemType)) {
                    return Lists.newArrayList();
                }
                // 可檢查，且已勾選 (預設要載入的檢查項目預設單位)
                defaultLoadCheckItemType = currCheckItemType;
            }
        }

        // ====================================
        // 查詢預設單位
        // ====================================
        // 防呆, 應該不可能發生
        if (defaultLoadCheckItemType == null) {
            log.warn("沒有取到預設的檢查項目!");
            return Lists.newArrayList();
        }
        // 查詢設定的預設檢查單位
        return this.prepareDefaultDepSids(this.requireSid, defaultLoadCheckItemType, assignSendType);
    }

    /**
     * 準備預設單位
     * 
     * @param requireSid     需求單 SID
     * @param checkItemType  檢查項目
     * @param assignSendType 分派/通知類別
     * @return 預設單位
     */
    private List<Integer> prepareDefaultDepSids(String requireSid, RequireCheckItemType checkItemType, AssignSendType assignSendType) {
        // ====================================
        // 查詢需求單資料
        // ====================================
        // 查詢
        Require require = requireService.findByReqSid(requireSid);
        // 防呆
        if (require == null || require.getMapping() == null || require.getMapping().getSmall() == null) {
            return Lists.newArrayList();
        }

        // ====================================
        // 查詢設定資料
        // ====================================
        return settingDefaultAssignSendDepService.findDepSidsByUnqKey(
                require.getMapping().getSmall().getSid(),
                checkItemType,
                assignSendType);
    }

    /**
     * 實做選擇元件 callback 事件
     */
    private final AssignNoticeComponentCallback assignNoticeComponentCallback = new AssignNoticeComponentCallback() {

        /**
         * 
         */
        private static final long serialVersionUID = -1748438873528674808L;

        /**
         * 準備『分派』單位已選擇項目
         * 
         * @return
         */
        @Override
        public List<Integer> prepareAssignSelectedDeps() throws SystemDevelopException {
            return initAssignDepSids;
        }

        /**
         * 準備『通知』單位已選擇項目
         * 
         * @return
         */
        @Override
        public List<Integer> prepareNoticeSelectedDeps() throws SystemDevelopException {
            return initNoticeDepSids;
        }

        /**
         * 是否為『檢查確認』功能使用
         */
        @Override
        public boolean isCheckConfirm() {
            return isInitByConfirmCheck;
        }

        /**
         * 準備檢查項目
         */
        @Override
        public List<SelectItem> prepareCheckItems() {
            return initCheckConfirmItem;
        }

        /**
         * @param checkItemName
         */
        @Override
        public void onCheckItemSelected(RequireCheckItemType checkItemType) {

            // 規則:合併
            // 1.勾選『檢查項目時』，加入該項目的『預設單位』
            // 2.若畫面已經有選擇單位，則保留已選擇單位
            // -> 合併

            // ====================================
            // 為取消選擇 -> 不做動
            // ====================================
            if (checkItemType == null) {
                return;
            }

            // ====================================
            // 處理分派單位
            // ====================================
            assignNoticeComponent.setAssignDepSids(
                    this.pepareMargeSelectedAndDefaultDepSids(
                            assignNoticeComponent.getAssignDepSids(),
                            checkItemType,
                            AssignSendType.ASSIGN));

            // ====================================
            // 處理通知單位
            // ====================================
            assignNoticeComponent.setNoticeDepSids(
                    this.pepareMargeSelectedAndDefaultDepSids(
                            assignNoticeComponent.getNoticeDepSids(),
                            checkItemType,
                            AssignSendType.SEND));

        }

        /**
         * 合併選擇單位和預設單位
         * 
         * @param selectedDepSids 已選擇單位
         * @param checkItemType   檢查項目
         * @param assignSendType  分派/通知類別
         * @return 合併後部門
         */
        private List<Integer> pepareMargeSelectedAndDefaultDepSids(
                List<Integer> selectedDepSids,
                RequireCheckItemType checkItemType,
                AssignSendType assignSendType) {

            // 取得預設
            List<Integer> defaultDepSids = prepareDefaultDepSids(
                    requireSid,
                    checkItemType,
                    assignSendType);

            // 合併
            Set<Integer> margedDepSids = Sets.newHashSet();
            if (WkStringUtils.notEmpty(selectedDepSids)) {
                margedDepSids.addAll(selectedDepSids);
            }
            if (WkStringUtils.notEmpty(defaultDepSids)) {
                margedDepSids.addAll(defaultDepSids);
            }

            return Lists.newArrayList(margedDepSids);
        }

        /**
         * 準備鎖定的分派單位 sid
         * 
         * @return
         * @throws Exception
         */
        @Override
        public List<Integer> prepareAssignDisableDepSids() throws SystemDevelopException {

            // 注意：此部分邏輯需與檢核邏輯同步
            // AssignNoticeService.logicCheck

            Set<Integer> disableDepSids = Sets.newHashSet();

            // ====================================
            // 查詢主檔
            // ====================================
            Require require = requireService.findByReqSid(requireSid);

            // ====================================
            // 檢查已需求完成單位
            // ====================================
            // 因需求完成單位 , 可能為多筆子單位收束而成, 故在此無法檢查

            // ====================================
            // 查詢已開子單單位
            // ====================================
            // 原型確認
            disableDepSids.addAll(
                    assignNoticeService.logicCheck_hasSubCase_ptCheck(
                            require,
                            initAssignDepSids,
                            null));
            // 送測
            disableDepSids.addAll(
                    assignNoticeService.logicCheck_hasSubCase_workTest(
                            require,
                            initAssignDepSids,
                            null));
            // ONPG
            disableDepSids.addAll(
                    assignNoticeService.logicCheck_hasSubCase_onpg(
                            require,
                            initAssignDepSids,
                            null));
            // 其他設定資訊
            disableDepSids.addAll(
                    assignNoticeService.logicCheck_hasSubCase_otherSet(
                            require,
                            initAssignDepSids,
                            null));

            // ====================================
            // 檢查轉 FB 單位不可移除
            // ====================================
            // 查詢已轉 FB 單位 (並轉 orgSid)
            List<Integer> fogbugzOrgSids = fbService.findCreatedOrgByRequire(require)
                    .stream()
                    .map(Org::getSid)
                    .collect(Collectors.toList());

            // 逐筆檢查是否有轉 FB
            for (Integer assignedDepSid : initAssignDepSids) {
                if (fogbugzOrgSids.contains(assignedDepSid)) {
                    disableDepSids.add(assignedDepSid);
                }
            }

            return Lists.newArrayList(disableDepSids);
        }

        /**
         *
         */
        @Override
        public void btnConfirm(
                ActionEvent event,
                List<Integer> newAssignDepSids,
                List<Integer> newNoticeDepSids) {

            // ====================================
            // 檢核
            // ====================================
            // 為檢查確認時，檢查『檢查項目』是否有勾選
            if (isInitByConfirmCheck) {
                // 測試檢查項目傳入值
                // 注意： assignNoticeComponent.getSelectedCheckItemTypes()
                // 僅會傳入畫面上『沒有』disable 的勾選項目
                // 故在此不用排除【已檢查】的勾選項目
                if (WkStringUtils.isEmpty(assignNoticeComponent.getSelectedCheckItemTypes())) {
                    MessagesUtils.showInfo("請選擇【檢查項目】！");
                    return;
                }
            }

            // ====================================
            // 主檔查詢
            // ====================================
            Require require = requireService.findByReqSid(requireSid);

            // ====================================
            // 查詢已存在的分派/通知資料
            // ====================================
            // 查詢
            Map<AssignSendType, AssignSendInfo> resultMap = assignSendInfoService.findByRequireSidAndStatus(require.getSid());
            // 取得原分派資料
            AssignSendInfo oldAssignInfo = resultMap.get(AssignSendType.ASSIGN);
            List<Integer> oldAssignDepSids = assignSendInfoService.prepareDeps(oldAssignInfo);
            // 取得原通知資料
            AssignSendInfo oldNoticeInfo = resultMap.get(AssignSendType.SEND);
            List<Integer> oldNoticeDepSids = assignSendInfoService.prepareDeps(oldNoticeInfo);

            // ====================================
            // 因分派/通知不可同時存在，於檢查確認時加入『分派優先原則』 
            // ====================================
            // 當分派單位存在時，自動將通知單位移除 (以分派為主)
            List<Integer> removeNewNoticeDepSid = Lists.newArrayList();
            List<Integer> removeOldNoticeDepSid = Lists.newArrayList();

            if (isInitByConfirmCheck) {
                // 1.若此次通知的單位，存在『已被分派』清單中，移除此次被通知的單位
                for (Integer newNoticeDepSid : newNoticeDepSids) {
                    if (oldAssignDepSids.contains(newNoticeDepSid)) {
                        removeNewNoticeDepSid.add(newNoticeDepSid);
                    }
                }

                // 2.若此次分派的單位，已存在於『已被通知單位』清單中，則自動移除通知單位
                for (Integer oldNoticeDepSid : oldNoticeDepSids) {
                    if (newAssignDepSids.contains(oldNoticeDepSid)) {
                        removeOldNoticeDepSid.add(oldNoticeDepSid);
                    }
                }
            }

            String removeNoticeDepBecauseAlreadyAssign = "";
            if (WkStringUtils.notEmpty(removeNewNoticeDepSid)) {
                // 組說明訊息
                removeNoticeDepBecauseAlreadyAssign += ""
                        + "<span class='WS1-1-3'>以下單位已經被分派，無需通知，將自動移除：</span>"
                        + "<br/>" +
                        WkOrgUtils.findNameBySid(removeNewNoticeDepSid, "、")
                        + "<br/><br/>";

                // 從此次選擇的通知單位中移除
                newNoticeDepSids.removeAll(removeNewNoticeDepSid);
            }

            String removeNoticeDepBecauseNewAssign = "";
            if (WkStringUtils.notEmpty(removeOldNoticeDepSid)) {
                removeNoticeDepBecauseNewAssign += ""
                        + "<span class='WS1-1-3'>以下單位將由通知單位轉為分派單位：</span>"
                        + "<br/>" +
                        WkOrgUtils.findNameBySid(removeOldNoticeDepSid, "、")
                        + "<br/><br/>";
            }

            // ====================================
            // 檢查確認時，合併原本已分派/通知的單位
            // ====================================
            // 說明：
            // 檢查確認時，未避免已分派/通知的單位, 干擾到GM的判斷，故僅顯示預設單位
            // 所以後續需合併在檢查前『已分派/通知』的單位
            if (isInitByConfirmCheck) {
                // 合併
                newAssignDepSids.addAll(oldAssignDepSids);
                // 去除重複
                newAssignDepSids = newAssignDepSids.stream().distinct().collect(Collectors.toList());

                // 合併
                newNoticeDepSids.addAll(oldNoticeDepSids);
                // 去除重複
                newNoticeDepSids = newNoticeDepSids.stream().distinct().collect(Collectors.toList());
                // 檢查確認時，移除已分派的通知單位
                if (WkStringUtils.notEmpty(removeOldNoticeDepSid)) {
                    newNoticeDepSids.removeAll(removeOldNoticeDepSid);
                }
            }

            // ====================================
            // 邏輯檢核
            // ====================================
            try {
                assignNoticeService.logicCheck(
                        requireSid,
                        effectiveTime,
                        loginBean.getUser(),
                        oldAssignInfo,
                        oldNoticeInfo,
                        oldAssignDepSids,
                        oldNoticeDepSids,
                        newAssignDepSids,
                        newNoticeDepSids,
                        isInitByConfirmCheck
                        );

            } catch (UserMessageException ex) {
                MessagesUtils.show(ex);
                log.info("執行檢核未通過:" + ex.getMessage());
                // r01MBean.reBuildeByUpdateFaild();
                // displayController.hidePfWidgetVar("assignNoticeSettingDlg");
                return;
            } catch (Exception ex) {
                MessagesUtils.showError("檢核資料時發生錯誤，請恰系統人員!");
                log.error("檢核資料時發生錯誤，請恰系統人員! : " + ex.getMessage(), ex);
                return;
            }
            // ====================================
            // 邏輯, 大於部級的單位，分派轉通知
            // ====================================
            // 移轉分派單位到通知單位容器
            Set<Integer> trnsDepSids = assignNoticeService.trnsAssignToNoticeByMoreMinisterial(
                    require.getSid(),
                    newAssignDepSids,
                    newNoticeDepSids);

            // 兜組移轉單位提示訊息
            String trnsAssignToNoticeMessage = "";
            if (WkStringUtils.notEmpty(trnsDepSids)) {
                trnsAssignToNoticeMessage += ""
                        + "<span class='WS1-1-3'>處級以上無須執行領單，以下部門將自動轉為通知單位：</span>"
                        + "<br/>" +
                        WkOrgUtils.findNameBySid(trnsDepSids, "、")
                        + "<br/><br/>";
            }

            // ====================================
            // 邏輯, 參數設定：禁止分派
            // ====================================
            // 移轉分派單位到通知單位容器
            trnsDepSids = assignNoticeService.trnsAssignToNoticeByAntiList(
                    require.getSid(),
                    newAssignDepSids,
                    newNoticeDepSids);

            // 兜組移轉單位提示訊息
            if (WkStringUtils.notEmpty(trnsDepSids)) {
                trnsAssignToNoticeMessage += ""
                        + "<span class='WS1-1-3'>以下單位無法分派，將轉為通知單位：</span>"
                        + "<br/>" +
                        WkOrgUtils.findNameBySid(trnsDepSids, "、")
                        + "<br/><br/>";
            }

            // ====================================
            // 檢查是否未異動
            // ====================================
            // 兜組加減派訊息
            String modifyDepMessage = assignNoticeService.prepareModifyDepsInfo(
                    oldAssignDepSids,
                    newAssignDepSids,
                    oldNoticeDepSids,
                    newNoticeDepSids);

            // 加減派訊息為空時，代表未異動
            if (WkStringUtils.isEmpty(modifyDepMessage)) {
                if (isInitByConfirmCheck) {
                    modifyDepMessage = "此次檢查未異動分派/通知單位!";
                } else {
                    // 不為檢查分派時，直接關閉
                    MessagesUtils.showInfo("此次未異動分派/通知單位!");
                    // 關閉視窗
                    displayController.hidePfWidgetVar("assignNoticeSettingDlg");
                    return;
                }
            }

            // ====================================
            // 兜組確認訊息
            // ====================================
            // title
            String messageTitle = "";
            if (isInitByConfirmCheck) {
                messageTitle = "進行『檢查確認』，並將根據您所設定的資訊做【分派/通知】";
            } else {
                messageTitle = "系統將根據您所設定的資訊進行【分派/通知】：";
            }

            // all message
            String confirmMessage = ""
                    + "<br/>"
                    + messageTitle
                    + "<hr/>"
                    + "<br/>"
                    + removeNoticeDepBecauseAlreadyAssign
                    + removeNoticeDepBecauseNewAssign
                    + trnsAssignToNoticeMessage
                    + modifyDepMessage;

            // ====================================
            // 呼叫確認視窗
            // ====================================
            final List<Integer> fianlNewAssignDepSids = Lists.newArrayList(newAssignDepSids);
            final List<Integer> fianlNewNoticeDepSids = Lists.newArrayList(newNoticeDepSids);

            confirmCallbackDialogController.showConfimDialog(
                    confirmMessage,
                    Lists.newArrayList(),
                    () -> processConfirm(
                            fianlNewAssignDepSids,
                            fianlNewNoticeDepSids,
                            isInitByConfirmCheck));
        }
    };
}
