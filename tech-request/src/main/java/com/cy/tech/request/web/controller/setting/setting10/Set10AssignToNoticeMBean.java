/**
 * 
 */
package com.cy.tech.request.web.controller.setting.setting10;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.enums.OrgType;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.setting.TrnsAssignToNoticeService;
import com.cy.work.common.constant.WkConstants;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.viewcomponent.treepker.TreePickerCallback;
import com.cy.work.viewcomponent.treepker.TreePickerComponent;
import com.cy.work.viewcomponent.treepker.helper.TreePickerDepHelper;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu 分派單位轉通知單位
 */
@Slf4j
@Controller
@Scope("view")
public class Set10AssignToNoticeMBean implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -5637244167734652028L;
    // ========================================================================
	// 服務元件
	// ========================================================================
	@Autowired
	private transient WkOrgCache wkOrgCache;
	@Autowired
	private transient TrnsAssignToNoticeService trnsAssignToNoticeService;
	@Autowired
	private transient DisplayController displayController;
	@Autowired
	private transient TreePickerDepHelper treePickerDepHelper;

	// ========================================================================
	// view 變數
	// ========================================================================
	/**
	 * 轉檔部門
	 */
	@Getter
	@Setter
	public String trnsDepSidsStr;

	/**
	 * 轉檔部門
	 */
	@Getter
	@Setter
	public List<Integer> assignDepSids;

	// ========================================================================
	// 方法區
	// ========================================================================
	@PostConstruct
	public void init() {
		// 初始化分派/通知單位樹
		this.assignDepTreePicker = new TreePickerComponent(assignDepTreeCallback, "分派/通知單位");
	}

	/**
	 * 轉檔
	 */
	public void trnsData(Set<Integer> trnsDepSids) {
		if (WkStringUtils.isEmpty(trnsDepSids)) {
			MessagesUtils.showWarn("尚未選擇轉檔部門!");
			return;
		}

		log.info("開始處理\r\n" + WkOrgUtils.findNameBySid(Lists.newArrayList(trnsDepSids), "、"));

		User admin = WkUserCache.getInstance().findBySid(WkConstants.ADMIN_USER_SID);
		List<String> errorMessages = Lists.newArrayList();
		try {

			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			Calendar cal = Calendar.getInstance(TimeZone.getDefault());
			cal.setTime(new Date());

			Date minDate = sdf.parse("20160101");

			while (true) {

				// 最大單號
				String maxRequireNo = "TGTR"
				        + WkStringUtils.padding(cal.get(Calendar.YEAR) + "", '0', 4, true)
				        + WkStringUtils.padding((cal.get(Calendar.MONTH) + 1) + "", '0', 2, true)
				        + WkStringUtils.padding(cal.get(Calendar.DAY_OF_MONTH) + "", '0', 2, true)
				        + "999";

				// 減一個月
				cal.add(Calendar.MONTH, -1);

				// 最小單號
				String minRequireNo = "TGTR"
				        + WkStringUtils.padding(cal.get(Calendar.YEAR) + "", '0', 4, true)
				        + WkStringUtils.padding((cal.get(Calendar.MONTH) + 1) + "", '0', 2, true)
				        + WkStringUtils.padding(cal.get(Calendar.DAY_OF_MONTH) + "", '0', 2, true)
				        + "000";

				String currErrorMessage = this.trnsAssignToNoticeService.process(
				        trnsDepSids,
				        minRequireNo,
				        maxRequireNo,
				        admin,
				        new Date());

				if (WkStringUtils.notEmpty(currErrorMessage)) {
					errorMessages.add(currErrorMessage);
				}

				if (WkDateUtils.isBefore(cal.getTime(), minDate, true)) {
					break;
				}

			}
		} catch (UserMessageException e) {
			MessagesUtils.show(e);
			return;
		} catch (Exception e) {
			MessagesUtils.showError("執行失敗!\r\n" + e.getMessage());
			log.error("執行失敗!" + e.getMessage(), e);
			return;
		}

		MessagesUtils.showInfo("轉檔完成\r\n" + String.join("\r\n", errorMessages));
	}

	/**
	 * 轉大於部級的單位
	 * 
	 * @throws UserMessageException
	 */
	public void trnsMoreThenMinisterialDepData() throws UserMessageException {

		// ====================================
		// 取得大於部級單位
		// ====================================
		Set<Integer> trnsDepSids = this.findMoreThenMinisterialDepSids();

		// ====================================
		// 執行轉檔
		// ====================================
		this.trnsData(trnsDepSids);

	}

	/**
	 * 以輸入單位代號轉檔
	 */
	public void trnsByDeps() {

		// ====================================
		// 收集轉檔單位SID
		// ====================================
		Set<Integer> trnsDepSids = Sets.newHashSet();

		for (String trnsDepStr : WkStringUtils.safeTrim(this.trnsDepSidsStr).split(",")) {
			trnsDepStr = WkStringUtils.safeTrim(trnsDepStr);
			if (WkStringUtils.isEmpty(trnsDepStr)) {
				continue;
			}
			if (!WkStringUtils.isNumber(trnsDepStr)) {
				MessagesUtils.showError("輸入單位SID：「" + trnsDepStr + "」非數字");
				return;
			}

			Integer trnsDepSid = Integer.parseInt(trnsDepStr);

			if (WkOrgCache.getInstance().findBySid(trnsDepSid) == null) {
				MessagesUtils.showError("輸入單位SID：「" + trnsDepSid + "」找不到單位資料");
				return;
			}

			trnsDepSids.add(trnsDepSid);
		}

		// ====================================
		// 執行轉檔
		// ====================================
		this.trnsData(trnsDepSids);

	}

	/**
	 * 取得大於部級的單位
	 * 
	 * @param compID
	 * @return
	 * @throws UserMessageException
	 */
	private Set<Integer> findMoreThenMinisterialDepSids() throws UserMessageException {

		String compID = SecurityFacade.getCompanyId();

		if (WkStringUtils.isEmpty(compID)) {
			throw new UserMessageException("取不到登入者公司別，請重新登入", InfomationLevel.WARN);
		}

		// 取得所有org 資料
		List<Org> allOrg = wkOrgCache.findAll();
		if (WkStringUtils.isEmpty(allOrg)) {
			throw new UserMessageException("系統異常，查無 Org 資料!", InfomationLevel.WARN);
		}

		// 取得所有大於部級的單位
		Set<Integer> moreThenMinisterialDepSids = allOrg.stream()
		        .filter(org -> OrgType.DEPARTMENT.equals(org.getType())) // 屬性為部門
		        .filter(org -> {
			        if (org == null) {
				        return false;
			        } else if (org.getCompany() == null) {
				        log.warn("單位:[(" + org.getSid() + ")" + WkOrgUtils.getOrgName(org) + " 沒有公司別資料");
				        return false;
			        }
			        // 比對是否為傳入公司
			        return compID.equals(org.getCompany().getId());
		        }) // 過濾為登入公司
		        .filter(org -> Activation.ACTIVE.equals(org.getStatus())) // 過濾掉停用單位
		        .filter(org -> WkOrgUtils.compareOrgLevel(org.getLevel(), OrgLevel.MINISTERIAL) > 0) // 過濾需為組級以上單位
		        .map(Org::getSid)
		        .collect(Collectors.toSet());

		return moreThenMinisterialDepSids;
	}

	// ========================================================================
	// 派工/通知部門
	// ========================================================================
	/**
	 * 分派/通知部門樹
	 */
	@Getter
	@Setter
	private transient TreePickerComponent assignDepTreePicker;

	/**
	 * 分派通知單位選單 - 開啟選單
	 */
	public void event_dialog_assignNoticeDepTree_open() {

		try {
			this.assignDepTreePicker.rebuild();
		} catch (Exception e) {
			log.error("開啟【分派通知單位】設定視窗失敗", e);
			MessagesUtils.showError("開啟【分派通知單位】設定視窗失敗");
			return;
		}

		displayController.showPfWidgetVar("wv_dlg_assignNoticeDep");
	}

	/**
	 * 分派通知單位選單 - 關閉選單
	 */
	public void event_dialog_assignNoticeDepTree_confirm() {

		// 取得元件中被選擇的項目
		this.assignDepSids = Lists.newArrayList(assignDepTreePicker.getSelectedItemIntegerSids());

		// 畫面控制
		displayController.hidePfWidgetVar("wv_dlg_assignNoticeDep");
	}

	/**
	 * 分派通知單位選單 - callback 事件
	 */
	private final TreePickerCallback assignDepTreeCallback = new TreePickerCallback() {
		/**
         * 
         */
        private static final long serialVersionUID = -3131941986794918168L;

        @Override
		public List<WkItem> prepareAllItems() throws Exception {

			// 取得公司
			Org comp = wkOrgCache.findById(SecurityFacade.getCompanyId());

			// 取得公司下所有單位
			List<Integer> allCompDepSids = wkOrgCache.findAllChild(comp.getSid())
			        .stream().map(Org::getSid)
			        .collect(Collectors.toList());

			return treePickerDepHelper.prepareDepItems(
			        SecurityFacade.getCompanyId(),
			        Lists.newArrayList(allCompDepSids));
		}

		/**
		 * 準備已選擇項目
		 * 
		 * @return
		 */
		@Override
		public List<String> prepareSelectedItemSids() throws Exception {

			if (WkStringUtils.isEmpty(assignDepSids)) {
				return Lists.newArrayList();
			}

			// 回傳
			return assignDepSids.stream()
			        .map(sid -> sid + "")
			        .collect(Collectors.toList());
		}

		@Override
		public List<String> prepareDisableItemSids() throws Exception {
			// 沒有不可選的資料
			return Lists.newArrayList();
		}
	};

}
