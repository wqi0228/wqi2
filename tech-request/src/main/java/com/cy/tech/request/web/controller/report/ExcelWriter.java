package com.cy.tech.request.web.controller.report;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;

import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author aken_kao
 *
 */
public class ExcelWriter {
    @Getter
    @Setter
    private int rowIndex = 0;
    @Getter
    @Setter
    private int columnIndex = 0;
    @Getter
    @Setter
    private HSSFWorkbook workbook;
    @Getter
    @Setter
    private HSSFSheet sheet;
    @Getter
    @Setter
    private HSSFSheet[] sheets;
    @Getter
    private Map<String, CellStyle> styleMap;
    
    public void output(OutputStream outputStream) throws IOException{
        getHSSFWorkbook().write(outputStream);
    }
    
    public void drawImage(byte[] pictureData, int rowIndex, int columnIndex){
        int pictureIdx = getHSSFWorkbook().addPicture(pictureData, Workbook.PICTURE_TYPE_PNG);
        CreationHelper helper = getHSSFWorkbook().getCreationHelper();
        @SuppressWarnings("rawtypes")
        Drawing drawing = getHSSFSheet().createDrawingPatriarch();
        ClientAnchor anchor = helper.createClientAnchor();
        anchor.setCol1(columnIndex);
        anchor.setRow1(rowIndex);
        Picture pict = drawing.createPicture(anchor, pictureIdx);
        pict.resize();

    }
    
    
    public void setColumnWidth(int columnIndex, int columnWidth){
        getHSSFSheet().setColumnWidth(columnIndex, 256 * columnWidth);
    }
    
    public void setTitleAndWidth(String[] title, int columnIndex){
        setTitleAndWidth(title, 0, columnIndex);
    }
    
    public void setTitleAndWidth(String[] title, int rowIndex, int columnIndex){
        for (int i = 0; i < title.length; i++) {
            String[]attr = title[i].split(":", -1);
            setCellValue(rowIndex, columnIndex, attr[0], HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
            setColumnWidth(columnIndex, Integer.valueOf(attr[1]));
            columnIndex++;
        }
    }
    
    /**
     * 跨欄，繪製多個data table用
     */
    public int acrossColumn(){
        columnIndex = getRow(rowIndex).getLastCellNum() + 1;
        return columnIndex;
    }
    
    /**
     * 合併儲存格
     */
    public void mergeCell(int sCellIndex, int eCellIndex){
        mergeCell(rowIndex, rowIndex, sCellIndex, eCellIndex);
    }
    
    /**
     * 合併儲存格
     */
    public void mergeCell(int sRowIndex, int eRowIndex, int sCellIndex, int eCellIndex){
        CellRangeAddress region = new CellRangeAddress(sRowIndex, eRowIndex, sCellIndex, eCellIndex);
        getHSSFSheet().addMergedRegion(region);
        
        RegionUtil.setBorderTop(BorderStyle.THIN, region, sheet);
        RegionUtil.setBorderBottom(BorderStyle.THIN, region, sheet);
        RegionUtil.setBorderLeft(BorderStyle.THIN, region, sheet);
        RegionUtil.setBorderRight(BorderStyle.THIN, region, sheet);
    }
    
    public void setCellValue(int rowIndex, int cellIndex, String value, HorizontalAlignment horizontal, VerticalAlignment vertical){
        HSSFCell cell = getCell(rowIndex, cellIndex);
        cell.setCellValue(value);
        
        CellStyle style = getCellStyle(horizontal, vertical);
        cell.setCellStyle(style);
    }
    
    public void setCellValue(int cellIndex, String value){
        setCellValue(rowIndex, cellIndex, value, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
    }
    
    public void setLeftCellValue(int cellIndex, String value){
        setCellValue(rowIndex, cellIndex, value, HorizontalAlignment.LEFT, VerticalAlignment.CENTER);
    }
    
    public void setRightCellValue(int cellIndex, String value){
        setCellValue(rowIndex, cellIndex, value, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER);
    }
    
    public int getNextRowIndex(){
        return ++rowIndex;
    }
    
    
    public HSSFWorkbook getHSSFWorkbook() {
        if (workbook == null) {
            workbook = new HSSFWorkbook();
            styleMap = Maps.newHashMap();
        }
        return workbook;
    }
    
    public HSSFSheet getHSSFSheet() {
        if(sheet == null){
            sheet = getHSSFWorkbook().createSheet();
        }
        return sheet;
    }
    
    public HSSFRow getRow(int rowIndex){
        HSSFRow row = null;
        if(getHSSFSheet().getRow(rowIndex) == null){
            row = getHSSFSheet().createRow(rowIndex);
        } else {
            row = getHSSFSheet().getRow(rowIndex);
        }
        return row;
    }
    
    public HSSFCell getCell(int rowIndex, int cellIndex){
        HSSFCell cell = null;
        if(getRow(rowIndex).getCell(cellIndex) == null){
            cell = getRow(rowIndex).createCell(cellIndex);
        } else {
            cell = getRow(rowIndex).getCell(cellIndex);
        }
        return cell;
    }
    
    /**
     * 邊框
     */
    private void setBorder(CellStyle style){
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);
    }
    
    private CellStyle getCellStyle(HorizontalAlignment horizontal, VerticalAlignment vertical) {
        String key = String.format("%s_%s", horizontal.name(), vertical.name());
        CellStyle style = null;
        if(styleMap.get(key) == null) {
            style = getWorkbook().createCellStyle();
            setBorder(style);
            style.setAlignment(horizontal);
            style.setVerticalAlignment(vertical);
            styleMap.put(key, style);
        } else {
            style = styleMap.get(key);
        }
        return style;
    }
}
