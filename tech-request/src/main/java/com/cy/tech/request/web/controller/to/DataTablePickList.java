package com.cy.tech.request.web.controller.to;

import com.cy.commons.interfaces.BaseEntity;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author kasim
 */
@SuppressWarnings("rawtypes")
public class DataTablePickList<T extends BaseEntity> implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6013314846243121525L;
    @Getter
    @Setter
    transient private List<T> targetData;
    @Getter
    @Setter
    transient private T targetSelected;

    @Getter
    @Setter
    transient private List<T> sourceData;
    @Getter
    @Setter
    transient private T sourceSelected;

    public DataTablePickList(Class<T> clazz) {
        Preconditions.checkState(clazz != null, "clazz must not be null");
        init();
    }

    /**
     * 初始化
     */
    public void init() {
        sourceData = Lists.newArrayList();
        targetData = Lists.newArrayList();
    }

    /**
     * 增加資料
     */
    public void btnAddRow() {
        if (sourceSelected == null) {
            return;
        }
        sourceData.remove(sourceSelected);
        targetData.add(sourceSelected);
        sourceSelected = null;
        targetSelected = null;
    }

    /**
     * 增加資料
     */
    public void btnAddAllRow() {
        if (sourceData == null || sourceData.isEmpty()) {
            return;
        }
        targetData.addAll(sourceData);
        sourceData = Lists.newArrayList();
        sourceSelected = null;
        targetSelected = null;
    }

    /**
     * 移除資料
     */
    public void btnRemoveRow() {
        if (targetSelected == null) {
            return;
        }
        targetData.remove(targetSelected);
        sourceData.add(targetSelected);
        sourceSelected = null;
        targetSelected = null;
    }

    /**
     * 移除資料
     */
    public void btnRemoveAllRow() {
        if (targetData == null || targetData.isEmpty()) {
            return;
        }
        sourceData.addAll(targetData);
        targetData = Lists.newArrayList();
        sourceSelected = null;
        targetSelected = null;
    }
}
