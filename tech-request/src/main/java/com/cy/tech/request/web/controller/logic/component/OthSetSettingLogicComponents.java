/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.logic.component;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.tros.TrOsService;
import com.cy.tech.request.logic.utils.ToolsDate;
import com.cy.tech.request.logic.vo.AttachmentVO;
import com.cy.tech.request.logic.vo.SimpleDateFormatEnum;
import com.cy.tech.request.logic.vo.TrOsVO;
import com.cy.tech.request.vo.enums.OthSetHistoryBehavior;
import com.cy.tech.request.vo.enums.OthSetStatus;
import com.cy.tech.request.web.controller.view.vo.HistoryVO;
import com.cy.tech.request.web.controller.view.vo.OthSetSettingVO;
import com.cy.tech.request.web.controller.view.vo.SubReplyVO;
import com.cy.tech.request.web.listener.AttachMaintainCallBack;
import com.cy.tech.request.web.listener.ReplyCallBack;
import com.cy.tech.request.web.listener.TabLoadCallBack;
import com.cy.tech.request.web.listener.UploadAttCallBack;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class OthSetSettingLogicComponents implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6250939939890688179L;
    private static OthSetSettingLogicComponents instance;
    @Autowired
    private TrOsService trOsService;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private RequireService requireService;

    public static OthSetSettingLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        OthSetSettingLogicComponents.instance = this;
    }

    public List<OthSetSettingVO> getOthSetSettingVOByRequestSid(String request_Sid, Integer loginUserSid,
            Integer loginUserDepSid, UploadAttCallBack uploadAttCallBack,
            TabLoadCallBack tabLoadCallBack, ReplyCallBack replyCallBack, AttachMaintainCallBack attachMaintainCallBack) {
        Boolean requireCloseCode = requireService.getCloseCodeBySid(request_Sid);
        Org requireCreateDep = requireService.getCreateDepBySid(request_Sid);
        List<TrOsVO> trOsVO = trOsService.getTrOsByRequestSid(request_Sid);
        List<OthSetSettingVO> othSetSettingVO = Lists.newArrayList();
        trOsVO.forEach(item -> {
            List<HistoryVO> historyVOs = Lists.newArrayList();
            item.getTrOsHistoryVOs().forEach(historyItem -> {
                User createUser = WkUserCache.getInstance().findBySid(historyItem.getCreate_usr());
                Org createDep = WkOrgCache.getInstance().findBySid(createUser.getPrimaryOrg().getSid());
                String content = "";
                boolean showAttachmentBtn = false;
                boolean showModifyBtn = false;
                boolean showExpandAndCompress = false;
                boolean showReplyAndReplyBtn = false;
                List<SubReplyVO> subReplyVOs = Lists.newArrayList();
                if (historyItem.getBehavior().equals(OthSetHistoryBehavior.REPLY)) {
                    content = historyItem.getTrOsReplyVO().getReply_content_css();
                    showAttachmentBtn = (historyItem.getTrOsReplyVO().getTrOsAttachmentVOs() != null
                            && !historyItem.getTrOsReplyVO().getTrOsAttachmentVOs().isEmpty());
                    historyItem.getTrOsReplyVO().getTrOsReplyAndReplyVO().forEach(replyAndReplyItem -> {
                        boolean showReplyAndReplyAttachmentBtn = (replyAndReplyItem.getTrOsAttachmentVOs() != null
                                && !replyAndReplyItem.getTrOsAttachmentVOs().isEmpty());
                        boolean showReplyAndReplyModifyBtn = true;
                        boolean disableModifyBtn = !(replyAndReplyItem.getReply_person_sid().equals(loginUserSid));
                        User replyAndReplyCreateUser = WkUserCache.getInstance().findBySid(replyAndReplyItem.getReply_person_sid());
                        Org replyAndReplyCreateDep = WkOrgCache.getInstance().findBySid(replyAndReplyCreateUser.getPrimaryOrg().getSid());
                        SubReplyVO subReplyVO = new SubReplyVO(
                                replyAndReplyItem.getOs_reply_and_reply_sid(),
                                replyAndReplyItem.getOs_sid(),
                                historyItem.getOs_history_sid(),
                                replyAndReplyItem.getOs_history_sid(),
                                showReplyAndReplyModifyBtn,
                                showReplyAndReplyAttachmentBtn,
                                disableModifyBtn,
                                replyAndReplyCreateUser.getSid(),
                                replyAndReplyCreateDep.getSid(),
                                replyAndReplyCreateUser.getName(),
                                WkOrgUtils.getOrgName(replyAndReplyCreateDep),
                                ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS_slide.getValue(), replyAndReplyItem.getReply_udt()),
                                replyAndReplyItem.getReply_content_css(),
                                replyCallBack,
                                subReplyVOs.size()
                        );
                        subReplyVOs.add(subReplyVO);
                    });

                } else {
                    showAttachmentBtn = (historyItem.getTrOsReplyVO().getTrOsAttachmentVOs() != null
                            && !historyItem.getTrOsReplyVO().getTrOsAttachmentVOs().isEmpty());
                    content = historyItem.getContent_css();
                }

                if (historyItem.getBehavior().equals(OthSetHistoryBehavior.REPLY)
                        && (subReplyVOs == null || subReplyVOs.isEmpty())
                        && historyItem.getCreate_usr().equals(loginUserSid)) {
                    showModifyBtn = true;
                }
                if (historyItem.getBehavior().equals(OthSetHistoryBehavior.REPLY)
                        && (subReplyVOs != null && !subReplyVOs.isEmpty())) {
                    showExpandAndCompress = true;
                }
                if (historyItem.getBehavior().equals(OthSetHistoryBehavior.REPLY)) {
                    showReplyAndReplyBtn = true;
                }

                HistoryVO hv = new HistoryVO(
                        historyItem.getOs_sid(),
                        historyItem.getOs_history_sid(),
                        historyItem.getOs_reply_sid(),
                        showModifyBtn,
                        showAttachmentBtn,
                        showExpandAndCompress,
                        showReplyAndReplyBtn,
                        historyItem.getBehavior().getVal(),
                        historyItem.getCreate_usr(),
                        createUser.getPrimaryOrg().getSid(),
                        createUser.getName(),
                        WkOrgUtils.getOrgName(createDep),
                        ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS_slide.getValue(), historyItem.getUpdate_dt()),
                        content,
                        subReplyVOs,
                        replyCallBack,
                        historyVOs.size()
                );
                historyVOs.add(hv);
            });
            User oth_createUser = WkUserCache.getInstance().findBySid(item.getCreate_usr());
            Org oth_createDep = WkOrgCache.getInstance().findBySid(item.getDep_Sid());
            List<String> notifyDepNamess = Lists.newArrayList();
            StringBuilder notifyDepsSb = new StringBuilder();
            List<AttachmentVO> attachmentVOs = Lists.newArrayList();
            boolean showCancelBtn = false;
            if (item.getCancelDate() != null) {
                showCancelBtn = true;
            }
            boolean showFinishBtn = false;
            if (item.getFinishDate() != null) {
                showFinishBtn = true;
            }

            item.getNoticeDepSids().forEach(notifiyDepSidStr -> {
                notifyDepNamess.add(WkOrgCache.getInstance().findNameBySid(notifiyDepSidStr));
            });
            Collections.sort(notifyDepNamess, (p1, p2) -> p1.compareTo(p2));
            notifyDepNamess.forEach(notifyDepsNameItem -> {
                if (!Strings.isNullOrEmpty(notifyDepsSb.toString())) {
                    notifyDepsSb.append(",");
                }
                notifyDepsSb.append(notifyDepsNameItem);
            });
            item.getTrOsAttachmentVOs().forEach(traItem -> {
                AttachmentVO av = new AttachmentVO(traItem.getOs_attachment_sid(), traItem.getFile_name(), traItem.getDescription(),
                        traItem.getFile_name(), false, String.valueOf(traItem.getCreate_usr()),
                        ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashSlide.getValue(), traItem.getCreate_dt()));
                try {
                    User user = WkUserCache.getInstance().findBySid(traItem.getCreate_usr());
                    av.setCreateUserSId(String.valueOf(user.getSid()));
                    Org dep = WkOrgCache.getInstance().findBySid(user.getPrimaryOrg().getSid());
                    //String userInfo = orgLogicComponents.showParentDep(dep) + "-" + user.getName();
                    av.setUserInfo(WkOrgUtils.getOrgName(dep) + "-" + user.getName());
                    av.setShowEditBtn(true);
                    av.setCreateTime(ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS_slide.getValue(), traItem.getCreate_dt()));
                    av.setParamCreateTime(ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS_slide.getValue(), traItem.getCreate_dt()));
                    attachmentVOs.add(av);
                } catch (Exception e) {
                    log.error("transWRAttachmentToAttachmentVO", e);
                }
            });

            OthSetSettingVO osv = new OthSetSettingVO(
                    item.getOs_sid(),
                    item.getOs_no(),
                    oth_createUser.getPrimaryOrg().getSid(),
                    WkOrgUtils.getOrgName(oth_createDep),
                    oth_createUser.getSid(),
                    oth_createUser.getName(),
                    ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS_slide.getValue(), item.getCreate_dt()),
                    item.getOsStatus().getVal(),
                    showCancelBtn,
                    ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashSlide.getValue(), item.getCancelDate()),
                    showFinishBtn,
                    ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashSlide.getValue(), item.getFinishDate()),
                    item.getTheme(),
                    item.getContentCss(),
                    item.getNoteCss(),
                    notifyDepNamess,
                    notifyDepsSb.toString(),
                    attachmentVOs,
                    historyVOs,
                    replyCallBack
            );

            osv.loadAttachmentCompant(item.getOs_sid(), item.getOs_no(), item.getRequire_sid(), item.getRequire_no(),
                    "", loginUserSid, loginUserDepSid, uploadAttCallBack, tabLoadCallBack,
                    attachMaintainCallBack);

            boolean isDisableEdit = disableEdit(requireCloseCode, item.getOsStatus(),
                    item.getNoticeDepSids(), item.getCreate_usr(), loginUserSid);
            //目前觀察不管如何都可進行回覆
            boolean isDisableReply = false;

            boolean isDisableCancel = disableCancel(requireCloseCode, item.getOsStatus(),
                    item.getNoticeDepSids(), item.getCreate_usr(), loginUserSid);

            boolean isDisableFinish = disableFinish(requireCloseCode, item.getOsStatus(),
                    item.getNoticeDepSids(), requireCreateDep, loginUserSid);
            boolean showExpandAndCompressBtn = (historyVOs != null && !historyVOs.isEmpty());
            osv.loadButton(isDisableEdit, isDisableFinish, isDisableCancel, isDisableReply, showExpandAndCompressBtn);
            othSetSettingVO.add(osv);
        });
        return othSetSettingVO;
    }

    /**
     * 判斷是否可完成基本資訊
     *
     * @param requireCloseCode 需求單CloseCode
     * @param status 基本資訊狀態
     * @param noticeDepSids 通知單位Sid List (String)
     * @param createTrOsUserSid 建立基本資訊UserSid
     * @param loginUserSid 登入者Sid
     * @return
     */
    private boolean disableFinish(Boolean requireCloseCode, OthSetStatus status,
            List<String> noticeDepSids, Org requireCreateDep, Integer loginUserSid) {
        //檢測需求單是否已Close
        if (status.equals(OthSetStatus.FINISH) && requireCloseCode) {
            return true;
        }
        //檢測基本資訊狀態
        if (!status.equals(OthSetStatus.PROCESSING)) {
            return true;
        }
        requireCreateDep = WkOrgCache.getInstance().findBySid(requireCreateDep.getSid());
        //需求單位向上層級的所有主管
        List<User> fillDocUnitManagers = orgService.findOrgManagers(requireCreateDep);
        User loginUser = WkUserCache.getInstance().findBySid(loginUserSid);
        //通知單位執行 且 通知單位中有需求單位,需求單位成員不可執行此功能
        if (fillDocUnitManagers.contains(loginUser) || loginUser.getPrimaryOrg().getSid().equals(requireCreateDep.getSid())) {
            return false;
        }
        //檢測登入者是否為通知單位人員
        if (noticeDepSids.contains(String.valueOf(loginUser.getPrimaryOrg().getSid()))) {
            return false;
        }
        return true;
    }

    /**
     * 判斷是否可取消基本資訊
     *
     * @param requireCloseCode 需求單CloseCode
     * @param status 基本資訊狀態
     * @param noticeDepSids 通知單位Sid List (String)
     * @param createTrOsUserSid 建立基本資訊UserSid
     * @param loginUserSid 登入者Sid
     * @return
     */
    private boolean disableCancel(Boolean requireCloseCode, OthSetStatus status,
            List<String> noticeDepSids, Integer createTrOsUserSid, Integer loginUserSid) {
        //檢測需求單是否已Close
        if (status.equals(OthSetStatus.INVALID) && requireCloseCode) {
            return true;
        }
        //檢測基本資訊狀態
        if (!status.equals(OthSetStatus.PROCESSING)) {
            return true;
        }
        User loginUser = WkUserCache.getInstance().findBySid(loginUserSid);
        User createTrOsUser = WkUserCache.getInstance().findBySid(createTrOsUserSid);
        Org createTrOsUserDep = WkOrgCache.getInstance().findBySid(createTrOsUser.getPrimaryOrg().getSid());
        List<User> canClickDisableList = orgService.findOrgManagers(createTrOsUserDep);
        if (!canClickDisableList.contains(createTrOsUser)) {
            canClickDisableList.add(createTrOsUser);
        }
        if (canClickDisableList.contains(loginUser)) {
            return false;
        }
        return true;
    }

    /** *
     * 判斷是否可編輯基本資訊
     *
     * @param requireCloseCode 需求單CloseCode
     * @param status 基本資訊狀態
     * @param noticeDepSids 通知單位Sid List (String)
     * @param createTrOsUserSid 建立基本資訊UserSid
     * @param loginUserSid 登入者Sid
     * @return
     */
    private boolean disableEdit(Boolean requireCloseCode, OthSetStatus status,
            List<String> noticeDepSids, Integer createTrOsUserSid, Integer loginUserSid) {
        //檢測需求單是否已Close
        if (status.equals(OthSetStatus.FINISH) && requireCloseCode) {
            return true;
        }
        //檢測基本資訊狀態
        if (!status.equals(OthSetStatus.PROCESSING)) {
            return true;
        }
        User loginUser = WkUserCache.getInstance().findBySid(loginUserSid);
        //檢測登入者是否為通知單位人員
        if (noticeDepSids.contains(String.valueOf(loginUser.getPrimaryOrg().getSid()))) {
            return false;
        }
        User createTrOsUser = WkUserCache.getInstance().findBySid(createTrOsUserSid);
        Org createTrOsUserDep = WkOrgCache.getInstance().findBySid(createTrOsUser.getPrimaryOrg().getSid());
        List<User> canClickDisableList = orgService.findOrgManagers(createTrOsUserDep);
        if (!canClickDisableList.contains(createTrOsUser)) {
            canClickDisableList.add(createTrOsUser);
        }
        if (canClickDisableList.contains(loginUser)) {
            return false;
        }
        return true;
    }

}
