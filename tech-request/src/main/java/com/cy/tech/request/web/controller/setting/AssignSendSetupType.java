/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.setting;

import lombok.Getter;

/**
 * 分派通知類型
 *
 * @author shaun
 */
public enum AssignSendSetupType {

    ASSIGN("分派設定"), SEND("寄發通知");

    @Getter
    private final String typeName;

    AssignSendSetupType(String typeName) {
        this.typeName = typeName;
    }
}
