package com.cy.tech.request.web.controller.view.component;

import java.util.List;

import com.cy.tech.request.logic.vo.CateNodeTo;
import com.cy.tech.request.web.listener.CategoryTreeCallBack;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author aken_kao
 */
@Slf4j
public class CategoryCombineVO {

    @Getter
    @Setter
    /** 類別組合(大類 sid) */
    private List<String> bigDataCateSids;
    @Getter
    @Setter
    /** 類別組合(中類 sid) */
    private List<String> middleDataCateSids;
    @Getter
    @Setter
    /** 類別組合(小類 sid) */
    private List<String> smallDataCateSids;

    @Getter
    /** 類別樹 Component */
    private CategoryTreeComponent categoryTreeComponent;

    public CategoryCombineVO(){
        bigDataCateSids = Lists.newArrayList();
        middleDataCateSids = Lists.newArrayList();
        smallDataCateSids = Lists.newArrayList();
        categoryTreeComponent = new CategoryTreeComponent(categoryTreeCallBack);
    }
    
    public void init() {
        bigDataCateSids = Lists.newArrayList();
        middleDataCateSids = Lists.newArrayList();
        smallDataCateSids = Lists.newArrayList();
        categoryTreeComponent = new CategoryTreeComponent(categoryTreeCallBack);
        categoryTreeComponent.init();
    }
    
    public void btnOpenCategoryTree() {
        try {
            categoryTreeComponent.init();
            categoryTreeComponent.selectedItem(smallDataCateSids);
        } catch (Exception e) {
            log.error("btnOpenCategoryTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public CateNodeTo getSingleCateNodeTo() {
        if(categoryTreeComponent.getSingleSelectedNode() != null) {
            return (CateNodeTo)categoryTreeComponent.getSingleSelectedNode().getData();
        }
        return null;
    }
    
    public void setHighlightCategory(List<String> list) {
        categoryTreeComponent.setHighlightCategory(list);
    }
    
    /** 類別樹 Component CallBack */
    private final CategoryTreeCallBack categoryTreeCallBack = new CategoryTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -9003216716710424669L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelCate() {
            categoryTreeComponent.selCate();
            bigDataCateSids = categoryTreeComponent.getBigDataCateSids();
            middleDataCateSids = categoryTreeComponent.getMiddleDataCateSids();
            smallDataCateSids = categoryTreeComponent.getSmallDataCateSids();
            categoryTreeComponent.clearCateSids();
        }

        @Override
        public void loadSelCate(List<String> smallDataCateSids) {
            categoryTreeComponent.init();
            categoryTreeComponent.selectedItem(smallDataCateSids);
            this.confirmSelCate();
        }

        @Override
        public void actionSelectAll() {
            //do nothing
        }

        @Override
        public void onNodeSelect() {
            //do nothing
        }

        @Override
        public void onNodeUnSelect() {
            //do nothing
        }
    };
    
    /** 訊息呼叫 */
    private final MessageCallBack messageCallBack = new MessageCallBack() {
        
        /**
         * 
         */
        private static final long serialVersionUID = -6575966369027327347L;

        @Override
        public void showMessage(String m) {
            MessagesUtils.showError(m);
        }
    };

}
