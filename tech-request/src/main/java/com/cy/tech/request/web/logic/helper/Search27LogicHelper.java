/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.logic.helper;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.UserService;
import com.google.common.collect.Lists;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 需求單查詢 - 邏輯Helper
 *
 * @author kasim
 */
@Component
public class Search27LogicHelper {

    @Autowired
    private OrganizationService orgService;
    @Autowired
    private UserService userService;

    public List<String> initAllDepts() {
        List<String> requireDepts = orgService.findAll().stream()
                .map(each -> each.getSid().toString())
                .collect(Collectors.toList());
        return requireDepts;
    }

    /**
     * 取得預設需求單位
     *
     * @param loginDepSid
     */
    public List<String> initDefaultDepts(Integer loginDepSid) {
        List<String> requireDepts = Lists.newArrayList(String.valueOf(loginDepSid));
        this.initDefaultDepts(orgService.findBySid(loginDepSid), requireDepts);
        return requireDepts;
    }

    /**
     * 取得預設需求單位
     *
     * @param parent
     * @param defaultDepts
     */
    private void initDefaultDepts(Org parent, List<String> defaultDepts) {
        List<Org> children = orgService.findByParentOrg(parent);
        if (children.isEmpty()) {
            return;
        }
        defaultDepts.addAll(this.getOrgSids(children));
        children.forEach(each -> this.initDefaultDepts(each, defaultDepts));
    }

    /**
     * 取得部門sid
     *
     * @param children
     * @return
     */
    private List<String> getOrgSids(List<Org> children) {
        return children.stream()
                .map(Org::getSid)
                .map(Object::toString)
                .collect(Collectors.toList());
    }

    /**
     * @param org
     * @return
     */
    public List<String> findChildOrgs(Org org) {
        return orgService.findChildOrgs(org).stream()
                .map(each -> each.getSid().toString())
                .collect(Collectors.toList());
    }

    /**
     * 得到GM單位
     *
     * @param dep
     * @return
     */
    public List<Integer> getGmOrgs(Org dep) {
        return orgService.findChildOrgsByMinisterial(dep).stream()
                .map(each -> each.getSid())
                .collect(Collectors.toList());
    }

    /**
     * 得到GM使用者
     *
     * @param gmOrgs
     * @return
     */
    public List<Integer> getGmUsers(List<Integer> gmOrgs) {
        return userService.findUserByPrimaryIn(gmOrgs, Activation.ACTIVE).stream()
                .map(each -> each.getSid())
                .collect(Collectors.toList());
    }

}
