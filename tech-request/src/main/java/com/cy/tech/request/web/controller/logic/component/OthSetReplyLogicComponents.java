/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.logic.component;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.tros.TrOsReplyService;
import com.cy.tech.request.logic.utils.ToolsDate;
import com.cy.tech.request.logic.vo.SimpleDateFormatEnum;
import com.cy.tech.request.vo.require.tros.TrOsReply;
import com.cy.tech.request.vo.require.tros.TrOsReplyAndReply;
import com.cy.tech.request.web.controller.view.vo.ReplyEditVO;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;

import java.io.Serializable;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 回覆邏輯元件
 * @author brain0925_liao
 */
@Component
public class OthSetReplyLogicComponents implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3649154400772093873L;
    private static OthSetReplyLogicComponents instance;
    @Autowired
    private TrOsReplyService trOsReplyService;
    public static OthSetReplyLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        OthSetReplyLogicComponents.instance = this;
    }

    /**
     * 載入ReplyEditVO 物件 (回覆的回覆)
     * @param os_reply_and_reply_sid 回覆的回覆Sid
     * @param os_history_sid HistorySid
     * @return 
     */
    public ReplyEditVO getReplyEditByReplyReplySid(String os_reply_and_reply_sid, String os_history_sid) {
        TrOsReplyAndReply trOsReplyAndReply = trOsReplyService.getTrOsReplyAndReplyBySid(os_reply_and_reply_sid);
        Org dep = WkOrgCache.getInstance().findBySid(trOsReplyAndReply.getReply_person_dep());
        User user = WkUserCache.getInstance().findBySid(trOsReplyAndReply.getReply_person_sid());
        return new ReplyEditVO(
        		WkOrgUtils.getOrgName(dep),
                user.getName(),
                ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS_slide.getValue(), trOsReplyAndReply.getReply_dt()),
                trOsReplyAndReply.getReply_content_css(),
                trOsReplyAndReply.getOs_sid(),
                trOsReplyAndReply.getOs_no(),
                trOsReplyAndReply.getRequire_sid(),
                trOsReplyAndReply.getRequire_no(),
                os_history_sid,
                trOsReplyAndReply.getOs_reply_sid(), trOsReplyAndReply.getSid()
        );
    }

    /**
     * 載入ReplyEditVO 物件 (回覆)
     * @param os_reply_sid 回覆Sid
     * @param os_history_sid HistorySid
     * @return 
     */
    public ReplyEditVO getReplyEditByReplySid(String os_reply_sid, String os_history_sid) {
        TrOsReply trOsReply = trOsReplyService.getTrOsReplyBySid(os_reply_sid);
        Org dep = WkOrgCache.getInstance().findBySid(trOsReply.getReply_person_dep());
        User user = UserLogicComponents.getInstance().findBySid(trOsReply.getReply_person_sid());
        return new ReplyEditVO(
        		WkOrgUtils.getOrgName(dep),
                user.getName(),
                ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS_slide.getValue(), trOsReply.getReply_dt()),
                trOsReply.getReply_content_css(),
                trOsReply.getOs_sid(),
                trOsReply.getOs_no(),
                trOsReply.getRequire_sid(),
                trOsReply.getRequire_no(),
                os_history_sid,
                trOsReply.getSid(), ""
        );
    }

}
