/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.pf.utils;

import com.cy.work.common.vo.Attachment;
import com.cy.work.common.vo.AttachmentService;
import com.google.common.collect.Maps;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.faces.context.FacesContext;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.omnifaces.util.Faces;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class PFAttachmentUtils implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1734452565967639446L;

    /**
     * 取得 PrimeFaces 的 fileDownload 標籤所需要的 StreamedContent 物件實體.
     *
     * @param attachService
     * @param attachment
     * @return
     * @throws FileNotFoundException
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public StreamedContent createStreamedContent(AttachmentService attachService, Attachment attachment)
              throws FileNotFoundException {
        String fileName = attachment.getFileName();
        String mimeType = this.getNoneNullMimeType(Faces.getContext(), fileName);
        FileInputStream stream = new FileInputStream(attachService.getServerSideFile(attachment));
        return new DefaultStreamedContent(stream, mimeType);
    }

    /**
     * 判斷 file 的 MIME 類型，若無法判斷類型，則回傳預設類型.
     *
     * @param ctx
     * @param fileName 檔案名稱.
     * @return 檔案的 MIME 類型或預設的 MIME 類型.
     */
    private String getNoneNullMimeType(FacesContext ctx, String fileName) {
        String mimeType = ctx.getExternalContext().getMimeType(fileName);
        return (mimeType == null) ? AttachmentService.DEFAULT_MIME_TYPE : mimeType;
    }

    /**
     * 將多個附加檔案的檔案內容壓縮成一個 zip 檔案再建立 StreamedContent.<br>
     * 下載完成後該檔案會自動刪除.
     *
     * @param attachService
     * @param attachments
     * @return
     * @throws IOException
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public StreamedContent createWrappedStreamedContent(AttachmentService attachService, List attachments) throws IOException {
        BufferedInputStream in = null;
        File tempFile = null;
        FileInputStream fis = null;
        ZipOutputStream out = null;
        Map<String, Integer> frequencies = Maps.newHashMap();
        boolean exceptionHappened = false;
        
        try {
            tempFile = File.createTempFile("wrapped-", ".zip");
            out = new ZipOutputStream(new FileOutputStream(tempFile));
            for (Object each : attachments) {
                Attachment attachment = (Attachment) each;
                // 為了替相同檔名的檔案加上編號，在讀取檔案的過程中用 Map 統計每個檔名的出現次數
                String fileName = attachment.getFileName();
                if (frequencies.containsKey(fileName)) {
                    frequencies.put(fileName, frequencies.get(fileName) + 1);
                } else {
                    frequencies.put(fileName, 1);
                }
                
                Integer frequency = frequencies.get(fileName);
                String outputFileName = (frequency.equals(1))
                          ? fileName
                          : this.insertFileNumber(fileName, frequencies.get(fileName));
                fis = new FileInputStream(attachService.getServerSideFile(attachment));
                in = new BufferedInputStream(fis);
                out.putNextEntry(new ZipEntry(outputFileName));
                IOUtils.copy(in, out);
                out.closeEntry();
                in.close();
                fis.close();
            }
        } catch (IOException e) {
            log.error("createWrappedStreamedContent", e);
            // out 關閉以後才能刪除 tempFile, 但是關閉 out 的責任屬於 finally 區塊
            // 故使用 exceptionHappened 將發生 Exception 的事實傳遞到 finally 區塊
            exceptionHappened = true;
            throw e;
        } finally {
            frequencies.clear();
            IOUtils.closeQuietly(out);
            IOUtils.closeQuietly(in);
            IOUtils.closeQuietly(fis);
            if (exceptionHappened) {
                FileUtils.deleteQuietly(tempFile);
            }
        }
        return new DefaultStreamedContent(new AutoDeletedFileInputStream(tempFile), "application/zip");
    }

    /**
     * 在檔名中插入序號. 在壓縮多檔案的時候若遇到原始檔名相同的情況就會需要編號.
     *
     * @param fileName 原始檔名.
     * @param number 序號.
     * @return 將序號插入副檔名之前的結果.
     */
    private String insertFileNumber(String fileName, Integer number) {
        int dotIndex = fileName.lastIndexOf(".");
        String beforeDot = (dotIndex == -1) ? fileName : fileName.substring(0, dotIndex);
        String afterDot = (dotIndex == -1) ? "" : fileName.substring(dotIndex);
        return beforeDot + "(" + number + ")" + afterDot;
    }
    
    private class AutoDeletedFileInputStream extends FileInputStream {
        
        private File file = null;
        
        public AutoDeletedFileInputStream(File file) throws FileNotFoundException {
            super(file);
            this.file = file;
        }
        
        @Override
        public void close() throws IOException {
            super.close();
            file.delete();
        }
    }
    
}
