/**
 * 
 */
package com.cy.tech.request.web.controller.setting.setting10.muti;

import java.util.List;
import java.util.Set;

import com.cy.commons.util.thread.CommonsCallable;
import com.cy.commons.util.thread.CommonsCallableResultTo;
import com.cy.tech.request.logic.service.setting.onetimetrns.OneTimeTrnsForRequireDepRebuildService;
import com.cy.tech.request.logic.service.setting.onetimetrns.to.RequireDepRebuildTo;
import com.cy.work.common.exception.UserMessageException;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Slf4j
public class RequireDepRebuildCallable implements CommonsCallable {

    private String requireSid;
    private String requireNo;
    private Set<Integer> beforeAssignDepSids;
    private Set<Integer> beforeNoticeDepSids;
    private List<RequireDepRebuildTo> requireDepRebuildTos;
    private OneTimeTrnsForRequireDepRebuildService requireDepRebuildService;

    public RequireDepRebuildCallable(
            String requireSid,
            String requireNo,
            Set<Integer> beforeAssignDepSids,
            Set<Integer> beforeNoticeDepSids,
            List<RequireDepRebuildTo> requireDepRebuildTos,
            OneTimeTrnsForRequireDepRebuildService requireDepRebuildService) {
        this.requireSid = requireSid;
        this.requireNo = requireNo;
        this.beforeAssignDepSids = beforeAssignDepSids;
        this.beforeNoticeDepSids = beforeNoticeDepSids;
        this.requireDepRebuildTos = requireDepRebuildTos;
        this.requireDepRebuildService = requireDepRebuildService;

    }

    @Override
    public CommonsCallableResultTo call() throws Exception {

        String errorMessage = "";

        try {

            this.requireDepRebuildService.process(
                    requireSid,
                    requireNo,
                    beforeAssignDepSids,
                    beforeNoticeDepSids,
                    requireDepRebuildTos);
            
            log.debug("OK:[{}]", this.requireNo);

        } catch (UserMessageException e) {
            errorMessage = String.format(
                    e.getMessage() + "，requireSid:[%s], requireNo:[%s]",
                    requireSid,
                    requireNo);
            log.info(errorMessage);
        } catch (Exception e) {
            errorMessage = String.format(
                    e.getMessage() + "，requireSid:[%s], requireNo:[%s]",
                    requireSid,
                    requireNo);
            log.error(errorMessage, e);
        }

        return new RequireDepRebuildCallableVO(this.requireSid, errorMessage);

    }

    @Override
    public String getCallableKey() { return this.requireSid; }

}
