/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.setting.setting10;

import com.cy.commons.vo.Org;
import com.cy.tech.request.logic.service.AssignSendInfoService;
import com.cy.tech.request.logic.vo.AssignBacksageTo;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.web.controller.setting.Setting10MBean;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 需求單異動分派資訊後台
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope("view")
public class Set10AssignModify implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4435720271629662349L;
    @Autowired
    transient private AssignSendInfoService assiSvc;
    @Autowired
    transient private Setting10MBean set10Bean;

    /** 需求製作進度 */
    @Getter
    @Setter
    private List<String> selReqTypes;
    /** 起始時間 */
    @Getter
    @Setter
    private Date startDate;
    /** 結束時間 */
    @Getter
    @Setter
    private Date endDate;
    /** 轉換單號 */
    @Getter
    @Setter
    private String transNos;
    /** 來源對應部門 */
    @Getter
    @Setter
    private Org sourceMappOrg;
    /** 目標新增部門 */
    @Getter
    @Setter
    private Org targetMappOrg;
    /** hide 執行分派數量查詢 */
    @Getter
    @Setter
    private Boolean showExecute;
    /** hide 執行分派 */
    @Getter
    @Setter
    private Boolean showDownloadLog;
    /** 下載用記錄檔 */
    @Getter
    private StreamedContent file;
    /** 重新分派物件 */
    private List<AssignBacksageTo> doAssignItems;

    /** 還原部門 */
    @Getter
    @Setter
    private Org recoveryOrg;
    /** 還原對應單號 */
    @Getter
    @Setter
    private String recoveryReqNos;
    /** 還原分派物件 */
    private List<AssignBacksageTo> doRecoveryItems;

    @PostConstruct
    void init() {
        showDownloadLog = false;
        showExecute = false;
        selReqTypes = Lists.newArrayList(RequireStatusType.PROCESS.name());
    }

    /**
     * 計算執行數量
     */
    public void countExecute() {
        if (this.isNotDownloadRec()) {
            return;
        }
        set10Bean.clearInfoScreenMsg();
        if (this.hasProblemInput()) {
            return;
        }
        doAssignItems = assiSvc.findBackstageModifyItems(
                sourceMappOrg.getSid(),
                targetMappOrg.getSid(),
                selReqTypes,
                startDate,
                endDate,
                this.getQueryNos());
        String handlerMsg = assiSvc.htmlHandlerMsg(doAssignItems, sourceMappOrg.getSid(), targetMappOrg.getSid(), this.getQueryNos(), true);
        set10Bean.addInfoMsg(handlerMsg);
        if (this.notHasModifyItems(doAssignItems)) {
            return;
        }
        showExecute = true;
        showDownloadLog = false;
    }

    /**
     * 取得轉入單位
     *
     * @return
     */
    private List<String> getQueryNos() {
        if (this.checkTransNos()) {
            return Lists.newArrayList(transNos.trim().split(","));
        }
        return Lists.newArrayList();
    }

    /**
     * 判斷是否有輸入轉入單號
     *
     * @return
     */
    private boolean checkTransNos() {
        return !Strings.isNullOrEmpty(transNos) && !Strings.isNullOrEmpty(transNos.trim());
    }

    /**
     * 檢查輸入
     */
    private boolean hasProblemInput() {
        try {
            Preconditions.checkArgument(this.selReqTypes != null && !selReqTypes.isEmpty(), "未選擇需求製作進度");
            if (!this.checkTransNos()) {
                Preconditions.checkNotNull(this.startDate, "未輸入立單啟始日期");
                Preconditions.checkNotNull(this.endDate, "未輸入立單結束日期");
            }
            Preconditions.checkNotNull(this.sourceMappOrg, "未輸入來源對應部門");
            Preconditions.checkNotNull(this.targetMappOrg, "未輸入目標新增部門");
            Preconditions.checkArgument(!this.sourceMappOrg.equals(this.targetMappOrg), "來源部門不可等同新增部門");
            return false;
        } catch (NullPointerException | IllegalArgumentException e) {
            MessagesUtils.showError(e.getMessage());
            return true;
        }
    }

    /**
     * 檢查是否無處理對象
     *
     * @return
     */
    private boolean notHasModifyItems(List<AssignBacksageTo> itmes) {
        if (itmes == null || itmes.isEmpty()) {
            MessagesUtils.showInfo("沒有可分派處理資料！！");
            return true;
        }
        return false;
    }

    public void downloadHandlerInfo() {
        try {
            String downLoadMsg = assiSvc.downloadHandlerMsg(doAssignItems, sourceMappOrg.getSid(), targetMappOrg.getSid());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            String fileName = "BackstageAssignLog" + sdf.format(new Date()) + ".txt";
            InputStream is = new ByteArrayInputStream(downLoadMsg.getBytes(Charset.forName("UTF-8")));
            is.close();
            file = new DefaultStreamedContent(is, "text/plain", fileName);
            showDownloadLog = false;
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    private boolean isNotDownloadRec() {
        try {
            Preconditions.checkArgument(!this.showDownloadLog, "尚未下載本次記錄！！");
            return false;
        } catch (NullPointerException | IllegalArgumentException e) {
            MessagesUtils.showError(e.getMessage());
            return true;
        }
    }

    /**
     * 執行
     */
    public void execute() {
        set10Bean.clearInfoScreenMsg();
        if (this.hasProblemInput() || this.notHasModifyItems(doAssignItems)) {
            return;
        }
        //開始插入分派資料
        assiSvc.modifyBackstageModifyItems(this.targetMappOrg.getSid(), doAssignItems);
        String handlerMsg = assiSvc.htmlHandlerMsg(doAssignItems, sourceMappOrg.getSid(), targetMappOrg.getSid(), this.getQueryNos(), false);
        set10Bean.addInfoMsg(handlerMsg);
        log.info("執行後台分派完成\n" + assiSvc.downloadHandlerMsg(doAssignItems, sourceMappOrg.getSid(), targetMappOrg.getSid()));
        showDownloadLog = true;
        showExecute = false;
    }

    public void dropThisExec() {
        showExecute = false;
        showDownloadLog = false;
    }

    //////////////////////
    /**
     * 計算執行數量
     */
    public void countRecovery() {
        if (this.isNotDownloadRec()) {
            return;
        }
        set10Bean.clearInfoScreenMsg();
        if (this.hasProblemInputOfRecovery()) {
            return;
        }
        List<String> reqNos = Arrays.asList(recoveryReqNos.replace("\r", "").replace("\n", "").replace(" ", "").split(","));
        doRecoveryItems = assiSvc.findBackstageModifyRecoveryItems(recoveryOrg.getSid(), reqNos);
        String handlerMsg = assiSvc.htmlHandlerMsgByRecovery(doRecoveryItems, recoveryOrg.getSid(), true);
        set10Bean.addInfoMsg(handlerMsg);
        if (this.notHasModifyItems(doRecoveryItems)) {
            return;
        }
        showExecute = true;
        showDownloadLog = false;
    }

    private boolean hasProblemInputOfRecovery() {
        try {
            Preconditions.checkNotNull(this.recoveryOrg, "未輸入還原部門");
            Preconditions.checkArgument(!Strings.isNullOrEmpty(this.recoveryReqNos), "未輸入還原單號");
            return false;
        } catch (NullPointerException | IllegalArgumentException e) {
            MessagesUtils.showError(e.getMessage());
            return true;
        }
    }

    /**
     * 執行還原
     */
    public void executeRecovery() {
        set10Bean.clearInfoScreenMsg();
        if (this.hasProblemInputOfRecovery() || this.notHasModifyItems(doRecoveryItems)) {
            return;
        }
        //開始還原分派資料
        assiSvc.modifyBackstageRecoveryItems(this.recoveryOrg.getSid(), doAssignItems);
        String handlerMsg = assiSvc.htmlHandlerMsgByRecovery(doRecoveryItems, recoveryOrg.getSid(), false);
        set10Bean.addInfoMsg(handlerMsg);
        log.info("執行分派還原完成\n" + assiSvc.downloadRecoveryMsg(doRecoveryItems, recoveryOrg.getSid()));
        showDownloadLog = true;
        showExecute = false;
    }

    public void downloadRecoveryInfo() {
        try {
            String downLoadMsg = assiSvc.downloadRecoveryMsg(doRecoveryItems, recoveryOrg.getSid());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            String fileName = "BackstageAssignRecoveryLog" + sdf.format(new Date()) + ".txt";
            InputStream is = new ByteArrayInputStream(downLoadMsg.getBytes(Charset.forName("UTF-8")));
            is.close();
            file = new DefaultStreamedContent(is, "text/plain", fileName);
            showDownloadLog = false;
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }
    }

}
