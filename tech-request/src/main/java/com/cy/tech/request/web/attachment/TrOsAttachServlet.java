/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.attachment;

import com.cy.tech.request.logic.vo.AttachmentVO;
import com.cy.tech.request.web.attachment.util.AttachmentUtils;
import com.cy.tech.request.web.attachment.util.AttachServlet;
import com.cy.tech.request.web.controller.logic.component.OthSetSettingAttachmentLogicComponents;
import java.io.File;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author brain0925_liao
 */
@WebServlet("/TrOsAttachServlet")
public class TrOsAttachServlet extends HttpServlet implements AttachServlet {
    //要覆寫

    /**
     * 
     */
    private static final long serialVersionUID = -5901023517366121420L;
    public final static String SERVLET_NAME = TrOsAttachServlet.class.getSimpleName();

    @Override
    public AttachmentVO getAttachAttachmentVOBySid(Object attachmentSid) {
         return OthSetSettingAttachmentLogicComponents.getInstance().getAttachmentVOByAttachment(String.valueOf(attachmentSid));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AttachServlet.super.doGet(request, response, super.getServletContext());
    }

    @Override
    public File getServerSideFile(AttachmentVO attachment) {
        return AttachmentUtils.getServerSideFile(attachment, "tech-request");
    }
}
