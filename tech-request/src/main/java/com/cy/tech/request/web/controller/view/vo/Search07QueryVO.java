/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.cy.tech.request.logic.vo.query.search.SearchQuery;
import com.cy.tech.request.vo.category.BasicDataBigCategory;
import com.cy.tech.request.vo.enums.ReqConfirmDepProgStatus;
import com.cy.tech.request.vo.enums.Search07QueryColumn;
import com.cy.work.common.enums.ReadRecordType;
import com.google.common.collect.Lists;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @author brain0925_liao
 */
@EqualsAndHashCode(callSuper = true, of = {})
public class Search07QueryVO extends SearchQuery implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5292242172865593843L;
    @Getter
    @Setter
    private Date startDate;
    @Getter
    @Setter
    private Date endDate;
    @Getter
    @Setter
    private ReadRecordType selectReadRecordType;
    @Getter
    @Setter
    private List<String> selectReqToBeReadType = Lists.newArrayList();
    @Setter
    private List<String> selectFinishCode = Lists.newArrayList();
    @Getter
    @Setter
    private List<String> selectedColseCode = Lists.newArrayList();
    /** 選擇的大類 */
    @Getter
    @Setter
    private BasicDataBigCategory selectBigCategory;
    /** 需求完成碼 */
    @Getter
    @Setter
    private List<String> reqFinishCode = Lists.newArrayList();
    /** 時間切換的index */
    @Getter
    @Setter
    private int dateTypeIndex;
    @Getter
    @Setter
    private String dateTypeIndexStr;
    @Getter
    @Setter
    private String fuzzyText;

    /** 被分派單位 */
    // @Getter
    // @Setter
    private List<Integer> assignDepSids = Lists.newArrayList();

    /** 類別組合 */
    @Getter
    @Setter
    private List<String> bigDataCateSids = Lists.newArrayList();
    @Getter
    @Setter
    private List<String> middleDataCateSids = Lists.newArrayList();
    @Getter
    @Setter
    private List<String> smallDataCateSids = Lists.newArrayList();
    @Getter
    @Setter
    private Search07QueryColumn dateTimeType = Search07QueryColumn.UPDATE_DATE;

    /**
     * 需求確認完成進度
     */
    @Getter
    @Setter
    private ReqConfirmDepProgStatus reqConfirmDepProgStatus;

    /**
     * 單位負責人
     */
    @Getter
    @Setter
    private String ownerName;

    /**
     * @return the requireDepts
     */
    public List<Integer> getAssignDepSids() { return assignDepSids; }

    /**
     * @param assignDepSids the requireDepts to set
     */
    public void setRequireDepts(List<Integer> assignDepSids) { this.assignDepSids = assignDepSids; }

}
