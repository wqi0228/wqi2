package com.cy.tech.request.web.controller.searchheader;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.joda.time.LocalDate;
import org.omnifaces.util.Faces;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.DateType;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.SpecificPermissionService;
import com.cy.tech.request.logic.service.setting.SettingCheckConfirmRightService;
import com.cy.tech.request.logic.vo.query.search.SearchQuery;
import com.cy.tech.request.vo.category.BasicDataBigCategory;
import com.cy.tech.request.vo.constants.ReqPermission;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.web.controller.search.CategoryTreeMBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.values.OrganizationTreeMBean;
import com.cy.work.common.cache.WkUserWithRolesCache;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.logic.WkUserAndOrgLogic;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;

/**
 * 共用的查詢表頭
 *
 * @author jason_h
 */
@Controller
@Scope("view")
public class CommonHeaderMBean extends SearchQuery implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5554708964608898487L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private OrganizationService orgService;
    @Autowired
    transient private CategoryTreeMBean cateTreeMBean;
    @Autowired
    transient private SpecificPermissionService spService;
    @Autowired
    transient private SettingCheckConfirmRightService settingCheckConfirmRightService;
    @Autowired
    transient private WkUserAndOrgLogic wkUserAndOrgLogic;

    @Getter
    @Autowired
    private OrganizationTreeMBean orgTreeMBean;

    /** 報表類型 */
    @Getter
    @Setter
    private ReportType reportType;
    /** 選擇的大類 */
    @Getter
    @Setter
    private BasicDataBigCategory selectBigCategory;
    /** 需求單位 */
    @Getter
    @Setter
    private List<String> requireDepts = Lists.newArrayList();
    /** 通知單位 */
    @Getter
    @Setter
    private List<String> noticeDepts = Lists.newArrayList();
    /** 轉發單位 */
    @Getter
    @Setter
    private List<String> forwardDepts = Lists.newArrayList();
    /** 要模糊查詢的字串 */
    @Getter
    @Setter
    private String fuzzyText;
    /** 起始時間 */
    @Getter
    @Setter
    private Date startDate;
    /** 結束時間 */
    @Getter
    @Setter
    private Date endDate;
    /** 時間切換的index */
    @Getter
    @Setter
    private int dateTypeIndex;

    ////////////////////// 以下為進階搜尋 /////////////////
    /** 異動啟始日 */
    @Getter
    @Setter
    private Date startUpdatedDate;
    /** 異動結束日 */
    @Getter
    @Setter
    private Date endUpdatedDate;
    /** 緊急度 */
    @Getter
    @Setter
    private List<String> urgencyList;
    /** 需求單號 */
    @Getter
    @Setter
    private String requireNo;
    /** 組織樹搜尋字串 */
    @Getter
    @Setter
    private String deptText;
    /** 組織樹搜尋字串(for 進階搜尋-轉發至) */
    @Getter
    @Setter
    private String deptTextOfForward;
    /** 組織樹搜尋字串(for 通知單位) */
    @Getter
    @Setter
    private String deptTextOfNotice;
    /** 單據建立者 */
    @Getter
    @Setter
    private String trCreatedUserName;

    @PostConstruct
    private void init() {
        decideReportType();
        decideDateType();
    }

    /**
     * 決定報表類型
     */
    private void decideReportType() {
        String viewId = Faces.getViewId();
        reportType = decideType(viewId);
    }

    private ReportType decideType(String viewId) {
        for (ReportType type : ReportType.values()) {
            if (viewId.contains(type.getViewId())) {
                return type;
            }
        }
        throw new IllegalArgumentException("沒有此頁面>> " + viewId);
    }

    /**
     * 決定查詢日期類型
     */
    private void decideDateType() {
        dateTypeIndex = reportType.getDateType().ordinal();
        this.getDateInterval(reportType.getDateType().name());
    }

    /**
     * 取得日期區間
     *
     * @param dateType
     */
    public void getDateInterval(String dateType) {
        if (DateType.PREVIOUS_MONTH.name().equals(dateType)) {
            if (startDate == null) {
                startDate = new Date();
            }
            LocalDate lastDate = new LocalDate(startDate).minusMonths(1);
            this.startDate = lastDate.dayOfMonth().withMinimumValue().toDate();
            this.endDate = lastDate.dayOfMonth().withMaximumValue().toDate();
            dateTypeIndex = DateType.PREVIOUS_MONTH.ordinal();
        } else if (DateType.THIS_MONTH.name().equals(dateType)) {
            this.startDate = new LocalDate().dayOfMonth().withMinimumValue().toDate();
            this.endDate = new LocalDate().dayOfMonth().withMaximumValue().toDate();
            dateTypeIndex = DateType.THIS_MONTH.ordinal();
        } else if (DateType.NEXT_MONTH.name().equals(dateType)) {
            if (startDate == null) {
                startDate = new Date();
            }
            LocalDate nextDate = new LocalDate(startDate).plusMonths(1);
            this.startDate = nextDate.dayOfMonth().withMinimumValue().toDate();
            this.endDate = nextDate.dayOfMonth().withMaximumValue().toDate();
            dateTypeIndex = DateType.NEXT_MONTH.ordinal();
        } else if (DateType.TODAY.name().equals(dateType)) {
            this.startDate = new Date();
            this.endDate = new Date();
            dateTypeIndex = DateType.TODAY.ordinal();
        } else if (DateType.NULL.name().equals(dateType)) {
            this.startDate = null;
            this.endDate = null;
            dateTypeIndex = DateType.NULL.ordinal();
        }
    }

    /**
     * 初始預設的需求單位 + 特殊權限
     *
     * @param containInActiveDep 是否包含停用部門
     */
    public void initDefaultRequireDepts(Boolean containInActiveDep) {
        requireDepts.clear();
        if (this.checkRolePermission()) {
            return;
        }
        this.initDefDeptsAndSpDepts(containInActiveDep, true);
    }

    private void initDefDeptsAndSpDepts(Boolean containInActiveDep, Boolean includeSpDepts) {

        this.requireDepts = wkUserAndOrgLogic.prepareCanViewDepSidBaseOnOrgLevelWithParallel(
                SecurityFacade.getUserSid(),
                OrgLevel.MINISTERIAL,
                false) //全部排除特殊可閱部門
                .stream()
                .map(sid -> String.valueOf(sid))
                .collect(Collectors.toList());
    }

    /**
     * 初始預設的需求單位 + 控制特殊權限組織打勾
     *
     * @param containInActiveDep 是否包含停用部門
     */
    public void initDefaultRequireDepts(Boolean containInActiveDep, Boolean includeSpDepts) {
        requireDepts.clear();
        if (this.checkRolePermission()) {
            return;
        }
        this.initDefDeptsAndSpDepts(containInActiveDep, includeSpDepts);
    }

    /**
     * 初始化公司樹
     *
     * @param containInActiveDep
     */
    public void initReqDeptsByComp(boolean containInActiveDep) {
        requireDepts.clear();
        Org comp = loginBean.getComp();
        requireDepts.add(String.valueOf(comp.getSid()));
        this.initDefaultDepts(comp, requireDepts, containInActiveDep);
    }

    /**
     * 確認角色權限
     */
    private boolean checkRolePermission() {
        // 假如登入者具備「分類確認檢查員」角色，則不限制特定單位(可查詢全部)
        if (this.hasPermissionCateCheckMember()) {
            return true;
        }
        // 假如登入者具備「送測管理員」角色，則不限制特定單位(可查詢全部)
        if (this.hasPermissionSendTestManager()) {
            return true;
        }
        // 假如登入者具備「送測異動報表閱覽者」角色，則不限制特定單位(可查詢全部)
        return this.hasPermissionSendTestModifyReportViewer();
    }

    private void initDefaultDepts(Org parent, List<String> defaultDepts, Boolean containInActiveDep) {
        List<Org> children = containInActiveDep
                ? orgService.findByParentOrg(parent)
                : orgService.findByParentOrg(parent, Activation.ACTIVE);
        if (children.isEmpty()) {
            return;
        }
        defaultDepts.addAll(this.getOrgSids(children));
        children.forEach(each -> this.initDefaultDepts(each, defaultDepts, containInActiveDep));
    }

    private List<String> getOrgSids(List<Org> children) {
        return children.stream()
                .map(Org::getSid)
                .map(Object::toString)
                .collect(Collectors.toList());
    }

    /**
     * 初始化特殊權限部門
     *
     * @param defaultDepts
     * @param user
     */
    private void initUserSpDepts(List<String> defaultDepts) {

        // 查詢登入者所有角色
        List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
                SecurityFacade.getUserSid(),
                SecurityFacade.getCompanyId());

        // 特殊權限的部門
        List<Org> spDepts = spService.findSpecificDeptsByRoleSidIn(roleSids);
        spDepts.forEach(each -> {
            if (!defaultDepts.contains(String.valueOf(each.getSid()))) {
                defaultDepts.add(String.valueOf(each.getSid()));
            }
        });
    }

    public void clear() {
        this.publicConditionInit();
        selectBigCategory = null;
        fuzzyText = null;
        trCreatedUserName = null;
        decideDateType();
        // 清除類別組合
        cateTreeMBean.clearCate();
    }

    /**
     * 清除進階選項
     */
    public void clearAdvance() {
        startUpdatedDate = null;
        endUpdatedDate = null;
        urgencyList = null;
        requireNo = null;
        forwardDepts.clear();
    }

    /**
     * 清除進階選項(沒有異動日期)-for 原型確認一覽表
     */
    public void clearAdvanceWithoutUpdateDate() {
        urgencyList = null;
        requireNo = null;
        forwardDepts.clear();
    }

    /**
     * 清除進階選項(沒有轉發至)-for 退件暫存單據查詢
     */
    public void clearAdvanceWithoutForward() {
        startUpdatedDate = null;
        endUpdatedDate = null;
        urgencyList = null;
        requireNo = null;
    }

    /**
     * 清除進階選項(沒有轉發至 & 緊急度)-for 送測
     */
    public void clearAdvanceWithoutForwardAndUrgency() {
        startUpdatedDate = null;
        endUpdatedDate = null;
        requireNo = null;
    }

    public List<Integer> getUrgencyTypeList() {
        if (this.urgencyList == null) {
            return Lists.newArrayList();
        }
        return urgencyList.stream()
                .map(UrgencyType::valueOf)
                .map(UrgencyType::ordinal)
                .collect(Collectors.toList());
    }

    /**
     * 類別設定用setting01.xthml
     */
    public void initOrgBySetting(List<String> reqDepts) {
        orgTreeMBean.setSelectAllNode(false);
        orgTreeMBean.initRoot(loginBean.getComp());
        this.requireDepts = reqDepts;
        orgTreeMBean.markSelectedNode(requireDepts, orgTreeMBean.getOrgTreeRoot());
    }

    public void initOrgTree() {
        this.initOrgTree(true, false);
    }

    /**
     * 初始化組織樹
     *
     * @param isJustActiveDep 勾選 - 不包含停用部門
     * @param isSeleAll       勾選 - 全選
     */
    public void initOrgTree(boolean isJustActiveDep, boolean isSeleAll) {
        if (this.initOrgTreeByBase(isJustActiveDep, isSeleAll)) {
            return;
        }
        this.decideRootFromComp(false);
    }

    private boolean initOrgTreeByBase(boolean isJustActiveDep, boolean isSeleAll) {
        orgTreeMBean.setSelectJustActive(isJustActiveDep);
        orgTreeMBean.setSelectAllNode(isSeleAll);
        deptText = "";
        if (hasPermissionCateCheckMember() || hasPermissionSendTestModifyReportViewer()) {
            orgTreeMBean.setSelectAllNode(false);
            orgTreeMBean.initRoot(loginBean.getComp());
            orgTreeMBean.resetSelectAll();
            return true;
        }
        orgTreeMBean.resetSelectAll();
        return false;
    }

    private void decideRootFromComp(boolean rootIsComp) {
        // 建立樹節點
        orgTreeMBean.initRootByDeptLevel(SecurityFacade.getUserSid(), rootIsComp);

        // 預設選擇單位
        orgTreeMBean.markSelectedNode(requireDepts, orgTreeMBean.getOrgTreeRoot());
    }

    /**
     * 初始化組織樹(全集團)
     *
     * @param isJustActiveDep 勾選 - 不包含停用部門
     * @param isSeleAll       勾選 - 全選
     */
    public void initOrgTree(boolean isJustActiveDep, boolean isSeleAll, boolean rootIsComp) {
        if (this.initOrgTreeByBase(isJustActiveDep, isSeleAll)) {
            return;
        }
        this.decideRootFromComp(rootIsComp);
    }

    /**
     * 分類確認檢查員權限<BR/>
     * REJECT
     *
     * @return
     */
    private boolean hasPermissionCateCheckMember() {
        return settingCheckConfirmRightService.hasCanCheckUserRight(SecurityFacade.getUserSid())
                && (ReportType.WAIT_CHECK.equals(reportType)
                        || ReportType.REJECT.equals(reportType));
    }

    /**
     * 送測管理員權限<BR/>
     * WORK_TEST_INFO
     *
     * @return
     */
    private boolean hasPermissionSendTestManager() {

        // 具有 送測管理員 角色
        boolean hasPermissionRole = WkUserUtils.isUserHasRole(
                SecurityFacade.getUserSid(),
                SecurityFacade.getCompanyId(),
                ReqPermission.ST_MGR);

        return hasPermissionRole &&
                ReportType.WORK_TEST_INFO.equals(reportType);
    }

    /**
     * 送測異動報表閱覽者權限<BR/>
     * WORK_TEST_HISTORY
     *
     * @return
     */
    private boolean hasPermissionSendTestModifyReportViewer() {

        // 具有 送測異動報表閱覽者 角色
        boolean hasPermissionRole = WkUserUtils.isUserHasRole(
                SecurityFacade.getUserSid(),
                SecurityFacade.getCompanyId(),
                ReqPermission.ST_HISTORY_VIEWER);

        return hasPermissionRole
                && ReportType.WORK_TEST_HISTORY.equals(reportType);
    }

    public void searchDep() {
        if (Strings.isNullOrEmpty(deptText)) {
            orgTreeMBean.setOrgTreeRoot(null);
            orgTreeMBean.initRootByDeptLevel(SecurityFacade.getUserSid(), false);
            return;
        }
        orgTreeMBean.searchNodeByText(deptText, orgTreeMBean.getOrgTreeRoot());
    }

    public void saveRequireDepts() {
        requireDepts.clear();
        for (TreeNode node : orgTreeMBean.getOrgTreeSelect()) {
            Org dept = (Org) node.getData();
            requireDepts.add(String.valueOf(dept.getSid()));
        }
    }

    /**
     * 初始化公司樹
     *
     * @param containInActiveDep
     */
    public void initForwardDeptsByComp(boolean containInActiveDep) {
        forwardDepts.clear();
        Org comp = loginBean.getComp();
        forwardDepts.add(String.valueOf(comp.getSid()));
        this.initDefaultDepts(comp, forwardDepts, containInActiveDep);
    }

    public void initForwardOrgTreeByComp(boolean isJustActiveDep, boolean isSeleAll) {
        orgTreeMBean.setSelectJustActive(isJustActiveDep);
        orgTreeMBean.setSelectAllNode(isSeleAll);
        deptTextOfForward = "";
        orgTreeMBean.initRoot(loginBean.getComp());
        orgTreeMBean.markSelectedNode(forwardDepts, orgTreeMBean.getOrgTreeRoot());
    }

    public void initForwardOrgTree() {
        deptTextOfForward = "";
        orgTreeMBean.initRoot(loginBean.getComp());
        orgTreeMBean.markSelectedNode(forwardDepts, orgTreeMBean.getOrgTreeRoot());
    }

    public void searchForwardDep() {
        if (Strings.isNullOrEmpty(deptTextOfForward)) {
            orgTreeMBean.initRoot(loginBean.getComp());
            return;
        }
        orgTreeMBean.searchNodeByText(deptTextOfForward, orgTreeMBean.getOrgTreeRoot());
    }

    public void saveForwardDepts() {
        forwardDepts.clear();
        for (TreeNode node : orgTreeMBean.getOrgTreeSelect()) {
            Org dept = (Org) node.getData();
            forwardDepts.add(String.valueOf(dept.getSid()));
        }
    }

    public boolean isAdvanceCondition() {
        if (startUpdatedDate != null && endUpdatedDate != null) {
            return true;
        }
        if (urgencyList != null && !urgencyList.isEmpty()) {
            return true;
        }
        if (!Strings.isNullOrEmpty(requireNo)) {
            return true;
        }
        return forwardDepts != null && !forwardDepts.isEmpty();
    }

    /**
     * 是否為模糊查詢
     *
     * @return
     */
    public boolean isFuzzyQuery() { return !Strings.isNullOrEmpty(fuzzyText); }

    /**
     * 是否為CategoryKeyMapping entity本身的欄位查詢
     *
     * @return
     */
    public boolean isCategoryKeyMappingCondition() { return selectBigCategory != null; }

    /**
     * 取得待閱原因字串 List
     *
     * @param types             待閱原因 List
     * @param toEmptyReasonType
     * @return
     */
    public List<String> getWaitReadReasonQueryStr(List<WaitReadReasonType> types, WaitReadReasonType toEmptyReasonType) {
        if (types == null) {
            return Lists.newArrayList();
        }
        List<String> names = Lists.newArrayList();
        types.stream().forEach(type -> {
            if (type.equals(toEmptyReasonType)) {
                names.add("");
            } else {
                names.add(type.name());
            }
        });
        return names;
    }

    /**
     * 初始化預設的通知單位
     *
     * @param containInActiveDep 是否包含停用部門
     */
    public void initDefaultNoticeDepts(Boolean containInActiveDep) {
        noticeDepts.clear();
        if (this.checkRolePermission()) {
            return;
        }
        Org dept = loginBean.getDep();
        noticeDepts.add(String.valueOf(dept.getSid()));
        this.initDefaultDepts(dept, noticeDepts, containInActiveDep);
        this.initUserSpDepts(noticeDepts);
    }

    public void initOrgTreeNotice(boolean isJustActiveDep, boolean isSeleAll) {
        orgTreeMBean.setSelectJustActive(isJustActiveDep);
        orgTreeMBean.setSelectAllNode(isSeleAll);
        deptText = "";
        if (hasPermissionCateCheckMember() || hasPermissionSendTestModifyReportViewer()) {
            orgTreeMBean.setSelectAllNode(false);
            orgTreeMBean.initRoot(loginBean.getComp());
            orgTreeMBean.resetSelectAll();
            return;
        }
        orgTreeMBean.initRootByDeptLevel(SecurityFacade.getUserSid(), false);
        orgTreeMBean.markSelectedNode(noticeDepts, orgTreeMBean.getOrgTreeRoot());
        orgTreeMBean.resetSelectAll();
    }

    public void searchNoticeDep() {
        if (Strings.isNullOrEmpty(deptTextOfNotice)) {
            this.initOrgTreeNotice(orgTreeMBean.isSelectJustActive(), orgTreeMBean.isSelectAllNode());
            // orgTreeMBean.initRoot(loginBean.getComp());
            return;
        }
        orgTreeMBean.searchNodeByText(deptTextOfNotice, orgTreeMBean.getOrgTreeRoot());
    }

    public void saveNoticeDepts() {
        noticeDepts.clear();
        for (TreeNode node : orgTreeMBean.getOrgTreeSelect()) {
            Org dept = (Org) node.getData();
            noticeDepts.add(String.valueOf(dept.getSid()));
        }
    }

    public String createNoticeRegexpParam() {
        return noticeDepts.stream().map(each -> "\"" + each + "\"").collect(Collectors.joining("|"));
    }

    /**
     * 轉換List<Org>為報表查詢用參數
     *
     * @param orgs
     * @return
     */
    public List<Integer> createOrgsInParam(List<Org> orgs) {
        return orgs.stream().map(Org::getSid).collect(Collectors.toList());
    }
}
