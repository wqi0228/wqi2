/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.tech.request.logic.service.CustomColumnService;
import com.cy.tech.request.vo.column.CustomColumn;
import com.cy.tech.request.vo.value.to.CustomColumnDetailTo;
import com.cy.tech.request.vo.value.to.CustomColumnPackTo;
import com.cy.tech.request.vo.value.to.CustomColumnTo;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.el.ValueExpression;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;
import org.primefaces.component.api.UIColumn;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.treetable.TreeTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kasim
 */
@Controller
@Scope("view")
@Slf4j
public class CustomColumnMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1475483005839586635L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private CustomColumnService customColumnService;
    @Autowired
    transient private WkJsonUtils jsonUtils;

    /** 客製化欄位資料 */
    private CustomColumn customColumn;
    private List<CustomColumnDetailTo> customColumnDetailTos;
    @Getter
    @Setter
    /** 分頁筆數 */
    private String pageCount;
    /** 全選標註 */
    @Getter
    @Setter
    private boolean allDefineField;
    /** 全反選標註 */
    @Getter
    @Setter
    private boolean unAllDefineField;
    /** 欄位選擇資料 */
    @Getter
    @Setter
    private LinkedHashMap<String, Boolean> columnFields;
    /** 是否顯示寬度存檔按鈕 */
    @Getter
    private boolean showWithSaveBtn = false;

    @PostConstruct
    public void init() {
        String path = getPathUrl();
        customColumn = customColumnService.findByUser(loginBean.getUser());
        this.pageCount = "50";
        if (customColumn == null) {
            this.customColumn = new CustomColumn();
            this.customColumn.setUser(loginBean.getUser());
            this.customColumn.setInfo(new CustomColumnTo());
        } else if (customColumn.getInfo().getDetailMap().containsKey(path)) {
            customColumnDetailTos = customColumn.getInfo().getDetailMap().get(path).getDetails();
            pageCount = customColumn.getInfo().getDetailMap().get(path).getPageCount();
        }
    }

    /**
     * 取得當前頁面URL
     */
    private String getPathUrl() {
        return Faces.getRequest().getRequestURI().replace(Faces.getRequest().getContextPath(), "");
    }

    /**
     * 開啟個人化設定
     *
     * @param tableId
     */
    public void btnDefineColumn(String tableId) {
        initDefineColumn();
        Object table = getDataTable(tableId);
        List<String> exceptColumns = this.createExceptColumn(table);
        String headerText;
        boolean ckField;
        CustomColumnDetailTo obj;
        for (UIColumn column : this.findTableColumn(table)) {
            headerText = column.getHeaderText();
            if (Strings.isNullOrEmpty(headerText) || exceptColumns.contains(headerText)) {
                continue;
            }
            ckField = this.findColumnDefaultShowValue(column);
            if (customColumnDetailTos != null) {
                obj = customColumnService.filterCustomColumnDetailTo(headerText, customColumnDetailTos);
                ckField = obj != null;
            }
            if (allDefineField && !ckField) {
                allDefineField = false;
            }
            if (unAllDefineField && ckField) {
                unAllDefineField = false;
            }
            columnFields.put(column.getHeaderText(), ckField);
        }
    }

    /**
     * 初始化開啟個人化設定
     */
    private void initDefineColumn() {
        allDefineField = true;
        unAllDefineField = true;
        columnFields = Maps.newLinkedHashMap();
    }

    /**
     * 取得 DataTable 物件
     *
     * @param tableId
     * @return DataTable
     */
    private Object getDataTable(String tableId) {
        Object tableObj = Faces.getViewRoot().findComponent("formTemplate:" + tableId);
        if (tableObj == null) {
            log.error("請確認輸入表格ID(" + tableId + ")是否存在！");
            return null;
        } else {
            return tableObj;
        }
    }

    /**
     * 建立除外欄位
     *
     * @param dataTable
     * @return
     */
    private List<String> createExceptColumn(Object table) {
        List<String> exceptionThemes = Lists.newArrayList("送測主題", "ON程式主題", "設定主題", "單據主題", "項目名稱");
        List<String> result = Lists.newArrayList("序", "部", "個", "關", exceptAndSaveColumn());
        for (UIColumn column : this.findTableColumn(table)) {
            if (column.getHeaderText() == null) {
                continue;
            }
            String hText = column.getHeaderText();
            if (exceptionThemes.contains(hText)) {
                result.add(hText);
                return result;
            }
        }
        result.add("主題");
        return result;
    }

    private List<UIColumn> findTableColumn(Object table) {
        if (table instanceof DataTable) {
            return ((DataTable) table).getColumns();
        }
        if (table instanceof TreeTable) {
            return ((TreeTable) table).getColumns();
        }
        return Lists.newArrayList();
    }

    /**
     * 除外欄位但必須記錄
     *
     * @return
     */
    private String exceptAndSaveColumn() {
        return "收費金額";
    }

    /**
     * 檢測預設不顯示欄位
     *
     * @param column
     * @return
     */
    private boolean findColumnDefaultShowValue(UIColumn column) {
        ValueExpression ve = column.getValueExpression("rendered");
        if (ve == null) {
            return true;
        }
        String express = ve.getExpressionString();
        if (!express.contains(COLUMN_SHOW_MASK)) {
            return true;
        }
        String[] expressArray = express.split(",");
        if (expressArray.length < 2) {
            return true;
        }
        return expressArray[1].contains("true");
    }

    /**
     * 全選個人化顯示設定
     */
    public void selectAllDefineField() {
        if (!allDefineField) {
            return;
        }
        unAllDefineField = false;
        customColumnService.changeColumnFields(columnFields, true);
    }

    /**
     * 取消全選個人化顯示設定
     */
    public void unSelectAllDefineField() {
        if (!unAllDefineField) {
            return;
        }
        allDefineField = false;
        customColumnService.changeColumnFields(columnFields, false);
    }

    /**
     * 進行欄位顯示與否存檔
     */
    public void btnSaveDefineColumn() {
        String pathUrl = getPathUrl();
        if (pathUrl.contains("search12")) {
            columnFields.put(exceptAndSaveColumn(), Boolean.TRUE);
        }
        List<CustomColumnDetailTo> customCols = customColumnService.updateCustomColumnDetailTos(
                getPathUrl(), columnFields, customColumnDetailTos);
        saveDefineColumn(customCols);
    }

    /**
     * 存檔
     *
     * @param customCols
     */
    private void saveDefineColumn(List<CustomColumnDetailTo> customCols) {
        String pathUrl = this.getPathUrl();
        this.customColumnDetailTos = customCols;
        CustomColumnPackTo packTo = new CustomColumnPackTo(pathUrl, this.pageCount, customCols);
        Map<String, CustomColumnPackTo> packMap = customColumn.getInfo().getDetailMap();
        if (packMap == null) {
            packMap = Maps.newHashMap();
        }
        packMap.put(pathUrl, packTo);
        this.customColumn.getInfo().setDetailMap(packMap);
        customColumn = customColumnService.save(customColumn);
        showWithSaveBtnToTrue();
    }

    /**
     * 顯示欄位寬度存檔按鈕
     */
    public void showWithSaveBtnToTrue() {
        showWithSaveBtn = true;
    }

    /**
     * 進行欄位寬度存檔
     */
    public void saveColumnWith() {
        try {
            String columnvalue = Faces.getRequestParameterMap().get("columnvalue");
            List<CustomColumnDetailTo> customCols = jsonUtils.fromJsonToList(columnvalue, CustomColumnDetailTo.class);
            saveDefineColumn(customCols);
            showWithSaveBtn = false;
        } catch (Exception e) {
            log.error("儲存欄位失敗..." + e.getMessage(), e);
            MessagesUtils.showError(e.getMessage());
        }
    }

    /**
     * dataTable顯示欄位判斷<BR/>
     * 預設開啟
     *
     * @param headerText
     * @return boolean
     */
    public boolean chekcDataTableColumn(String headerText) {
        return customColumnService.chekcDataTableColumn(headerText, customColumnDetailTos);
    }

    /**
     * dataTable顯示欄位判斷<BR/>
     * 欄位預設開啟關閉處理<BR/>
     * true = 預設開啟,false = 預設關閉
     *
     * @param headerText
     * @param
     * @return
     */
    public boolean chekcDataTableColumn(String headerText, boolean show) {
        if (show) {
            return this.chekcDataTableColumn(headerText);
        }
        return !(customColumnDetailTos == null || !customColumnService.chekcDataTableColumn(headerText, customColumnDetailTos));
    }

    // 變動chekcDataTableColumn method name時需同步變更對應value
    private final static String COLUMN_SHOW_MASK = "chekcDataTableColumn";

    /**
     * dataTable顯示欄位寬度
     *
     * @param headerText
     * @return boolean
     */
    public String getColumnWidth(String headerText) {
        return customColumnService.getColumnWidth(headerText, customColumnDetailTos);
    }

    /**
     * dataTable顯示欄位寬度
     * @param headerText
     * @param defultWidth
     * @return
     */
    public String getColumnStyle(String headerText, Integer defultWidth) {
        String styleStr = "";
        // 寬度
        String width = customColumnService.getColumnWidth(headerText, customColumnDetailTos);
        if (WkStringUtils.isEmpty(width)) {
            width = defultWidth+"";
        }
        styleStr += "width:" + width + "px";
        return styleStr;
    }
}
