
package com.cy.tech.request.web.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.cy.system.rest.client.util.SystemCacheUtils;
import com.cy.tech.request.logic.config.ReqEhCacheHelper;
import com.cy.work.common.cache.WkCommonCache;

/**
 *
 * @author allen
 */
@Slf4j
@WebServlet(name = "/service/cache/clearAllCache", urlPatterns = "/service/cache/clearAllCache")
public class ClearCacheServlet extends HttpServlet implements ApplicationContextAware {

    /**
     * 
     */
    private static final long serialVersionUID = 3445536241490996870L;
    private static ApplicationContext applicationContext = null;

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ClearCacheServlet.applicationContext = applicationContext;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {

            log.info("開始清除 SYSTEM-REST 快取");
            SystemCacheUtils.getInstance().clearAllCache();
            log.info("開始清除 WK-COMMON 快取");
            WkCommonCache.getInstance().clearAllCache();
            log.info("開始清除 需求單 EhCache 快取");
            ReqEhCacheHelper.getInstance().clearCache();
            log.info("所有快取清除完畢");

            response.setCharacterEncoding("UTF-8");
            response.setHeader("content-type", "text/html;charset=UTF-8");
            response.getWriter().write("所有快取清除完畢!");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

}
