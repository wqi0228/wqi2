package com.cy.tech.request.web.newsearch;

import java.util.List;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.tech.request.logic.service.NewSerachService;
import com.google.common.collect.Lists;

/**
 * @author allen1214_wu
 *
 */
@Component
public class NewSearchHelper {

    @Autowired
    private NewSerachService serachService;

    /**
     * 準備需求類別選項
     * 
     * @return
     */
    public List<SelectItem> prepareSelectItemBigCategory() {
        List<SelectItem> results = Lists.newArrayList(new SelectItem("", "全部"));
        // 查詢
        List<SelectItem> bigCategorys = this.serachService.findActiveBigCategory().stream()
                .map(vo -> new SelectItem(vo.getSid(), vo.getName()))
                .collect(Collectors.toList());
        results.addAll(bigCategorys);
        return results;
    }
}
