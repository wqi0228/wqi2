/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.common;

import java.io.Serializable;
import java.util.List;
import javax.faces.model.SelectItem;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 狀態Item Bean
 * @author brain0925_liao
 */
@Controller
@Scope("request")
public class ActivationItemBean implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 2274510389633547322L;

    /**取得狀態SelectItems*/
    public List<SelectItem> getActivationItems() {        
        return ActivationItemSetting.getActivationItems();
    }
}

