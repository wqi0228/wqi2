/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search.helper;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.URLService.URLServiceAttr;
import com.cy.tech.request.logic.utils.SimpleDateFormatEnum;
import com.cy.tech.request.logic.utils.ToolsDate;
import com.cy.tech.request.vo.require.AlertInbox;
import com.cy.tech.request.vo.require.AlertInboxSendGroup;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkSqlUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.group.vo.WorkLinkGroup;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;

/**
 * @author jason_h
 */
@Component
public class SearchHelper implements Serializable, InitializingBean {

    /**
     * 
     */
    private static final long serialVersionUID = -7575678941392976662L;

    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static SearchHelper instance;

    /**
     * @return instance
     */
    public static SearchHelper getInstance() { return instance; }

    private static void setInstance(SearchHelper instance) { SearchHelper.instance = instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    // ========================================================================
    // 服務
    // ========================================================================

    @Autowired
    transient private SearchService searchService;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private URLService urlService;

    /**
     * 查詢啟始日的時間調整為00-00-00
     *
     * @param startDate
     * @return
     */
    public Date transStartDate(Date startDate) {
        return searchService.transStartDate(startDate);
    }

    /**
     * 查詢結束日的時間調整為23-59-59
     *
     * @param endDate
     * @return
     */
    public Date transEndDate(Date endDate) {
        return searchService.transEndDate(endDate);
    }

    /**
     * 建立主題快取 Key = 需求單號
     *
     * @param requires
     * @return
     */
    public Map<String, String> createThemeCache(List<Require> requires) {
        return searchService.createThemeCache(requires);
    }

    public Map<String, String> createThemeCacheFromInbox(List<AlertInbox> inboxs) {
        return searchService.createThemeCacheFromInbox(inboxs);
    }

    public Map<String, String> createThemeCacheFromInboxSendGroups(List<AlertInboxSendGroup> groups) {
        return searchService.createThemeCacheFromInboxSendGroups(groups);
    }

    public String getThemeStr(Require r) {
        return searchService.getThemeStr(r);
    }

    public String getCashStr(Require r) {
        return searchService.getCashStr(r);
    }

    public Map<String, String> createAmountCashCache(List<Require> requires) {
        return searchService.createAmountCashCache(requires);
    }

    /**
     * 匯出excel
     *
     * @param document
     * @param start
     * @param end
     * @param type
     */
    public void exportExcel(Object document, Date start, Date end, ReportType type) {
        exportExcel(document, start, end, type, Boolean.FALSE);
    }

    public void exportExcel(Object document, String exportName, String titleMame) {
        HSSFWorkbook wb = (HSSFWorkbook) document;
        HSSFSheet sheet = wb.getSheetAt(0);
        int row = 5;
        int rowIndex = 0;
        String[] strs = new String[row];
        strs[rowIndex++] = titleMame;
        strs[rowIndex++] = "匯出日期：" + ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateTime.getValue(), new Date());
        strs[rowIndex++] = "匯出人員：" + exportName;

        for (int i = 0; i < row; i++) {
            int totalRows = sheet.getLastRowNum();
            sheet.shiftRows(i, totalRows, 1);
            HSSFRow headerRow = sheet.getRow(i);
            if (headerRow == null) {
                headerRow = sheet.createRow(i);
            }
            HSSFCell cell = headerRow.getCell(0);
            if (cell == null) {
                cell = headerRow.createCell(0);
            }
            cell.setCellValue(strs[i]);
        }
    }

    /**
     * 匯出excel
     *
     * @param document
     * @param start
     * @param end
     * @param type
     */
    public void exportExcel(Object document, Date start, Date end, ReportType type, Boolean showCreateDate) {
        HSSFWorkbook wb = (HSSFWorkbook) document;
        HSSFSheet sheet = wb.getSheetAt(0);
        int row = 5;
        int rowIndex = 0;
        if (!showCreateDate) {
            row = 4;
        }
        String[] strs = new String[row];
        strs[rowIndex++] = "報表產生日：" + new LocalDate().toString("yyyy/MM/dd");
        if (showCreateDate) {
            strs[rowIndex++] = "立單日期：" + new LocalDate(start).toString("yyyy/MM/dd") + "~"
                    + new LocalDate(end).toString("yyyy/MM/dd");
        }
        strs[rowIndex++] = "報表類型：" + type.getReportName();

        for (int i = 0; i < row; i++) {
            int totalRows = sheet.getLastRowNum();
            sheet.shiftRows(i, totalRows, 1);
            HSSFRow headerRow = sheet.getRow(i);
            if (headerRow == null) {
                headerRow = sheet.createRow(i);
            }
            HSSFCell cell = headerRow.getCell(0);
            if (cell == null) {
                cell = headerRow.createCell(0);
            }
            cell.setCellValue(strs[i]);
        }
    }

    /**
     * 查詢啟始日的時間調整為00-00-00
     *
     * @param startDate
     * @return
     */
    public String transStrByStartDate(Date startDate) {
        return new DateTime(startDate).toString("yyyy-MM-dd") + " 00:00:00";
    }

    /**
     * 查詢結束日的時間調整為23-59-59
     *
     * @param endDate
     * @return
     */
    public String transStrByEndDate(Date endDate) {
        return new DateTime(endDate).toString("yyyy-MM-dd") + " 23:59:59";
    }

    /**
     * 取得 關聯檢視 網址
     *
     * @param requireNo
     * @return
     */
    public String getRelevanceViewUrl(String requireNo) {
        if (Strings.isNullOrEmpty(requireNo)) {
            return "";
        }
        Require r = requireService.findByReqNo(requireNo);
        if (r == null) {
            return "";
        }
        WorkLinkGroup link = r.getLinkGroup();
        if (link == null) {
            return "";
        }
        return "../require/require04.xhtml" + urlService.createSimpleURLLink(URLServiceAttr.URL_ATTR_L, link.getSid(), requireNo, 1);
    }

    /**
     * 組合模糊查詢語句, 並取得主題資料
     * 
     * @param keyword_Theme     主題
     * @param keyword_FuzzyText 模糊查詢字串
     * @param requireNo
     */
    public String buildRequireIndexDictionaryCondition(
            String keyword_Theme,
            String keyword_FuzzyText,
            String requireNo) {

        StringBuilder builder = new StringBuilder();

        builder.append("INNER JOIN ( ");
        builder.append("    SELECT tid.require_sid, ");
        builder.append("           tid.field_content ");
        builder.append("      FROM tr_index_dictionary tid ");
        builder.append("     WHERE tid.field_name='主題' ");

        // 過濾主題欄位
        if (WkStringUtils.isEmpty(requireNo)
                && WkStringUtils.notEmpty(keyword_Theme)) {
            builder.append("     AND " + WkSqlUtils.prepareConditionSQLByMutiKeyword("tid.field_content", keyword_Theme));
        }

        // 模糊搜尋
        if (WkStringUtils.isEmpty(requireNo)
                && !Strings.isNullOrEmpty(keyword_FuzzyText)) {
            builder.append("        AND ( ");
            builder.append("            tid.require_sid IN ( ");
            builder.append("                SELECT DISTINCT ftid.require_sid ");
            builder.append("                  FROM tr_index_dictionary ftid ");
            builder.append("                  WHERE " + WkSqlUtils.prepareConditionSQLByMutiKeyword("ftid.field_content", keyword_FuzzyText) + " ");
            builder.append("            ) ");
            builder.append("            OR ");
            builder.append("            tid.require_sid IN ( ");
            builder.append("               SELECT DISTINCT trace.require_sid  ");
            builder.append("                 FROM tr_require_trace trace ");
            builder.append("                WHERE trace.require_trace_type = 'REQUIRE_INFO_MEMO' ");
            builder.append("                  AND " + WkSqlUtils.prepareConditionSQLByMutiKeyword("trace.require_trace_content", keyword_FuzzyText) + " ");
            builder.append("            ) ");
            builder.append("        ) ");
        }
        builder.append(") AS tid ON tr.require_sid = tid.require_sid ");

        return builder.toString();
    }

    public String appendRowCss(UrgencyType urgencyType, ReadRecordType readRecordType) {
        String cssClass = "";
        if (UrgencyType.URGENT.equals(urgencyType)) {
            cssClass += " urgencyRow ";
        }

        Set<ReadRecordType> readRecordTypes = Sets.newHashSet(
                ReadRecordType.WAIT_READ,
                ReadRecordType.UN_READ);

        if (readRecordTypes.contains(readRecordType)) {
            cssClass += " textBold ";
        }
        return cssClass;
    }

    public String appendRowCss(ReadRecordType readRecordType) {
        String cssClass = "";

        Set<ReadRecordType> readRecordTypes = Sets.newHashSet(
                ReadRecordType.WAIT_READ,
                ReadRecordType.UN_READ);

        if (readRecordTypes.contains(readRecordType)) {
            cssClass += " textBold ";
        }
        return cssClass;
    }
}
