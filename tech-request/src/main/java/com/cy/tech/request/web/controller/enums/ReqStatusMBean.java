/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.enums;

import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.model.SelectItem;
import org.springframework.stereotype.Controller;

/**
 *
 * @author shaun
 */
@Controller
public class ReqStatusMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1876422233443223058L;

    public static final List<RequireStatusType> STATUS1 = Lists.newArrayList(RequireStatusType.NEW_INSTANCE,
            RequireStatusType.WAIT_CHECK, RequireStatusType.ROLL_BACK_NOTIFY, RequireStatusType.PROCESS,
            RequireStatusType.SUSPENDED, RequireStatusType.COMPLETED);
    
    public static final List<RequireStatusType> STATUS2 = Lists.newArrayList(RequireStatusType.PROCESS,
            RequireStatusType.SUSPENDED);

    /**
     * 取得製作進度，排除不要的進度
     *
     * @param excludes
     * @return
     */
    public List<RequireStatusType> createExcludeStatus(List<RequireStatusType> excludes) {
        List<RequireStatusType> searchList = Lists.newArrayList(RequireStatusType.values());
        if(WkStringUtils.notEmpty(excludes)) {
            searchList.removeAll(excludes);
        }
        return searchList;
    }

    /**
     * 建立PF元件
     *
     * @param status
     * @return
     */
    public SelectItem[] buildItem(List<RequireStatusType> status) {
        SelectItem[] items = new SelectItem[status.size()];
        status.forEach(each -> items[status.indexOf(each)] = new SelectItem(each, each.getValue()));
        return items;
    }

    /**
     * for 逾期未結案報表 需求製作進度
     * @return
     */
    public SelectItem[] buildUnclosedCaseTypes() {
        SelectItem[] items = new SelectItem[STATUS1.size()];
        STATUS1.forEach(each -> items[STATUS1.indexOf(each)] = new SelectItem(each, each.getValue()));
        return items;
    }
    
    /**
     * for 逾期未完工報表 需求製作進度
     * @return
     */
    public SelectItem[] buildUnfinishedCaseTypes() {
        SelectItem[] items = new SelectItem[STATUS2.size()];
        STATUS2.forEach(each -> items[STATUS2.indexOf(each)] = new SelectItem(each, each.getValue()));
        return items;
    }
    /**
     * 建立需求狀態查詢list(for sql)
     *
     * @param select
     * @param allStatus
     * @return
     */
    public List<String> createQueryReqStatus(RequireStatusType select, List<RequireStatusType> allStatus) {
        if (select == null) {
            return allStatus.stream()
                      .map(each -> each.name())
                      .collect(Collectors.toList());
        }
        return Lists.newArrayList(select.name());
    }

    /**
     * 建立需求狀態查詢list(for sql)
     *
     * @param selects
     * @param allStatus
     * @return
     */
    public List<String> createQueryReqStatus(List<String> selects, List<RequireStatusType> allStatus) {
        if (selects == null || selects.isEmpty()) {
            return allStatus.stream()
                      .map(each -> each.name())
                      .collect(Collectors.toList());
        }
        return selects;
    }

}
