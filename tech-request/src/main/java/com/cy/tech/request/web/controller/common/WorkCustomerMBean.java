/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.common;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.tech.request.logic.service.customer.ReqWorkCustomerHelper;
import com.cy.work.customer.vo.enums.EnableType;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Controller
@Scope("request")
@Slf4j
public class WorkCustomerMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5804275810657075363L;
    @Autowired
    transient private ReqWorkCustomerHelper reqWorkCustomerHelper;

    public List<SelectItem> getEnableCustomerItems() {
        List<SelectItem> selectItems = Lists.newArrayList();
        try {
            reqWorkCustomerHelper.findByEnable(EnableType.ENABLE).forEach(item -> {
                String customerType = item.getCustomerType() == null ? "" : ("(" + item.getCustomerType().getName() + ")");                
                String label = item.getName() + "@" + item.getLoginCode() + customerType;
                selectItems.add(new SelectItem(item.getSid(), label));
            });
        } catch (Exception e) {
            log.error("getEnableCustomerItems error", e);
        }
        return selectItems;
    }

}
