/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.jsf.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.primefaces.component.selectcheckboxmenu.SelectCheckboxMenu;
import org.primefaces.component.selectmanymenu.SelectManyMenu;
import org.primefaces.component.selectonelistbox.SelectOneListbox;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.tech.request.logic.service.customer.ReqWorkCustomerHelper;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.customer.vo.WorkCustomer;
import com.google.common.base.Strings;

/**
 * 客戶資料 轉換器
 *
 * @author kasim
 */
@Component("customerConverter")
public class CustomerConverter implements Converter {

    @Autowired
    transient private WkStringUtils stringUtils;
    @Autowired
    transient private ReqWorkCustomerHelper workCustomerService;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (Strings.isNullOrEmpty(value) || !stringUtils.isNumeric(value)) {
            return null;
        }
        return workCustomerService.findCopyOne(Long.valueOf(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof WorkCustomer) {
            if (component instanceof SelectOneMenu
                    || component instanceof SelectCheckboxMenu
                    || component instanceof SelectManyMenu
                    || component instanceof SelectOneListbox) {
                return String.valueOf(((WorkCustomer) value).getSid());
            }
        }
        return null;
    }
}
