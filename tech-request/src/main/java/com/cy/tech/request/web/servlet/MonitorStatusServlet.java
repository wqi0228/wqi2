/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 *
 * @author shaun
 */
@Slf4j
@WebServlet(name = "/monitor/status", urlPatterns = "/monitor/status")
public class MonitorStatusServlet extends HttpServlet implements ApplicationContextAware {

    /**
     * 
     */
    private static final long serialVersionUID = 2452361440256845088L;
    private static ApplicationContext applicationContext = null;

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        MonitorStatusServlet.applicationContext = applicationContext;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
              throws ServletException, IOException {
        try {
            response.getWriter().write("OK");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

}
