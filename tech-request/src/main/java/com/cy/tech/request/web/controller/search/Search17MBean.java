/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.apache.commons.lang3.StringUtils;
import org.omnifaces.util.Faces;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.view.Search17View;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.URLService.URLServiceAttr;
import com.cy.tech.request.logic.service.onpg.OnpgService;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.search.helper.Search17Helper;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.searchheader.CommonHeaderMBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.view.component.CategoryTreeComponent;
import com.cy.tech.request.web.controller.view.component.ReportOrgTreeComponent;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.listener.CategoryTreeCallBack;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.listener.ReportCustomFilterCallback;
import com.cy.tech.request.web.listener.ReportOrgTreeCallBack;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.tech.request.web.view.to.search.query.SearchQuery17;
import com.cy.tech.request.web.view.to.search.query.SearchQuery17CustomFilter;
import com.cy.work.common.cache.WkCommonCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.cache.WkUserWithRolesCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.utils.WkOrgUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * ON程式異動明細表
 *
 * @author jason_h
 */
@Slf4j
@Controller
@Scope("view")
public class Search17MBean implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 1044826240938216659L;
    @Autowired
	transient private LoginBean loginBean;
	@Autowired
	transient private Search17Helper searchHelper;
	@Autowired
	transient private TableUpDownBean upDownBean;
	@Autowired
	transient private CommonHeaderMBean commHeaderMBean;
	@Autowired
	transient private ReqLoadBean loadManager;
	@Autowired
	transient private SearchHelper helper;
	@Autowired
	transient private URLService urlService;
	@Autowired
	transient private RequireService requireService;
	@Autowired
	transient private DisplayController display;
	@Autowired
	transient private OnpgService opService;
	@Autowired
	transient private Require01MBean r01MBean;
	@Autowired
	transient private WkCommonCache wkCommonCache;

	@Getter
	/** 類別樹 Component */
	private CategoryTreeComponent categoryTreeComponent;
	@Getter
	/** 報表 組織樹 Component */
	private ReportOrgTreeComponent orgTreeComponent;
	@Getter
	/** 報表 組織樹 Component */
	private ReportOrgTreeComponent orgTreeNoticeComponent;

	@Getter
	/** 切換模式 - 全畫面狀態 */
	private SwitchType switchFullType = SwitchType.DETAIL;
	@Getter
	/** 切換模式 */
	private SwitchType switchType = SwitchType.CONTENT;
	@Getter
	/** 列表 id */
	private final String dataTableId = "dtRequire";
	@Getter
	/** 在匯出的時候，某些內容需要隱藏 */
	private boolean hasDisplay = true;
	@Getter
	/** 查詢物件 */
	private SearchQuery17 searchQuery;

	@Getter
	@Setter
	/** 所有的ON程式單 */
	private List<Search17View> queryItems;
	private List<Search17View> tempItems;
	@Getter
	@Setter
	/** 選擇的ON程式單 */
	private Search17View querySelection;
	@Getter
	/** 上下筆移動keeper */
	private Search17View queryKeeper;

	@Getter
	private SearchQuery17CustomFilter searchQuery17CustomFilter;
	@Autowired
	transient private ReportCustomFilterLogicComponent reportCustomFilterLogicComponent;
	@Setter
	@Getter
	/** dataTable filter */
	private String createDepName;
	@Getter
	/** dataTable filter items */
	private List<SelectItem> createDepNameItems;

	@PostConstruct
	public void init() {
		String onpgNo = this.getRequestParameter();
		this.initComponents(onpgNo);
		this.clear();
	}

	public void openDefaultSetting() {

		// 查詢登入者所有角色
		List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
		        SecurityFacade.getUserSid(),
		        SecurityFacade.getCompanyId());

		try {
			this.searchQuery17CustomFilter.loadDefaultSetting(
			        searchHelper.getDefaultDepSids(
			                loginBean.getDep().getSid(),
			                roleSids),
			        this.searchQuery17CustomFilter.getTempSearchQuery17());
		} catch (Exception e) {
			log.error("openDefaultSetting ERROR", e);
		}
	}

	/**
	 * 取得參數
	 *
	 * @return
	 */
	private String getRequestParameter() {
		String param_s = Faces.getRequestParameterMap().get(URLServiceAttr.URL_ATTR_S.getAttr());
		if (StringUtils.isNotBlank(param_s)) {

			String onpgNo = urlService.parseIllegalUrlParam(
			        param_s,
			        WkUserCache.getInstance().findBySid(SecurityFacade.getUserSid()),
			        SecurityFacade.getCompanyId());

			if (onpgNo.contains(URLService.ILLEAL_ACCESS)) {
				this.reloadToIllegalPage(onpgNo.replace(URLService.ILLEAL_ACCESS, ""));
			}
			return onpgNo;
		}
		return null;
	}

	private void reloadToIllegalPage(String errorCode) {
		try {
			Faces.getExternalContext().redirect("../error/illegal_read.xhtml?" + URLService.ERROR_CODE_ATTR + "=" + errorCode);
			Faces.getContext().responseComplete();
		} catch (IOException ex) {
			log.error("導向讀取失敗頁面失敗...", ex);
		}
	}

	/**
	 * 初始化 元件
	 *
	 * @param onpgNo
	 */
	private void initComponents(String onpgNo) {
		this.searchQuery = new SearchQuery17(ReportType.WORK_ON_HISTORY);
		this.categoryTreeComponent = new CategoryTreeComponent(categoryTreeCallBack);

		// 查詢登入者所有角色
		List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
		        SecurityFacade.getUserSid(),
		        SecurityFacade.getCompanyId());

		this.orgTreeComponent = new ReportOrgTreeComponent(
		        loginBean.getCompanyId(),
		        loginBean.getDep(),
		        roleSids,
		        true,
		        reportOrgTreeCallBack);

		this.orgTreeNoticeComponent = new ReportOrgTreeComponent(
		        loginBean.getCompanyId(),
		        loginBean.getDep(),
		        roleSids,
		        false,
		        reportOrgTreeNoticeCallBack);

		this.searchQuery17CustomFilter = new SearchQuery17CustomFilter(
				loginBean.getCompanyId(), loginBean.getUserSId(),
		        loginBean.getDep().getSid(),
		        loginBean.getComp().getSid(),
		        reportCustomFilterLogicComponent,
		        messageCallBack,
		        display, reportCustomFilterCallback,
		        roleSids,
		        loginBean.getDep());
		
		searchQuery.initDefaultOnpgNo(onpgNo);
		this.searchQuery17CustomFilter.getTempSearchQuery17().initDefaultOnpgNo(onpgNo);
	}

	/**
	 * 還原預設值並查詢
	 */
	public void clear() {
		this.clearQuery();
		this.search();
	}

	/**
	 * 清除/還原選項
	 */
	private void clearQuery() {

		// 查詢登入者所有角色
		List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
		        SecurityFacade.getUserSid(),
		        SecurityFacade.getCompanyId());

		try {
			this.searchQuery17CustomFilter.loadDefaultSetting(
			        searchHelper.getDefaultDepSids(
			                loginBean.getDep().getSid(),
			                roleSids),
			        this.searchQuery);
			categoryTreeComponent.clearCate();
		} catch (Exception e) {
			log.error("clearQuery ERROR", e);
		}
	}

	public void search() {
		tempItems = searchHelper.search(loginBean.getCompanyId(), loginBean.getUser(), searchQuery);
		this.createDepName = null;
		this.createDepNameItems = this.tempItems.stream()
		        .map(each -> each.getCreateDepName())
		        .collect(Collectors.toSet()).stream()
		        .map(each -> new SelectItem(each, each))
		        .collect(Collectors.toList());
		this.doChangeFilter();
	}

	/**
	 * 進行dataTable filter
	 */
	public void doChangeFilter() {
		this.queryItems = this.filterDataTable();
	}

	/**
	 * filter
	 *
	 * @return
	 */
	private List<Search17View> filterDataTable() {
		return tempItems.stream()
		        .filter(each -> Strings.isNullOrEmpty(createDepName)
		                || createDepName.equals(each.getCreateDepName()))
		        .collect(Collectors.toList());
	}

	/**
	 * 隱藏部分column裡的內容
	 */
	public void hideColumnContent() {
		hasDisplay = false;
	}

	/**
	 * 匯出excel
	 *
	 * @param document
	 */
	public void exportExcel(Object document) {
		helper.exportExcel(document, commHeaderMBean.getStartDate(), commHeaderMBean.getEndDate(), commHeaderMBean.getReportType());
		hasDisplay = true;
	}

	/**
	 * 半版row選擇
	 *
	 * @param event
	 */
	public void onRowSelect(SelectEvent event) {
		this.queryKeeper = this.querySelection = (Search17View) event.getObject();
		this.changeRequireContent(this.queryKeeper);
		this.moveScreenTab();
	}

	/**
	 * 開啟分頁
	 *
	 * @param dtId
	 * @param widgetVar
	 * @param pageCount
	 * @param to
	 */
	public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search17View to) {
		this.highlightReportTo(widgetVar, pageCount, to);
		this.resetUpdownInfo();
		this.checkHelfScreen();
	}

	/**
	 * highlight列表位置
	 *
	 * @param widgetVar
	 * @param pageCount
	 * @param to
	 */
	private void highlightReportTo(String widgetVar, String pageCount, Search17View to) {
		querySelection = to;
		queryKeeper = querySelection;
		display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
	}

	/**
	 * 取得索引位置
	 *
	 * @param pageCountStr
	 * @return
	 */
	private int getRowIndex(String pageCountStr) {
		Integer pageCount = 50;
		if (!Strings.isNullOrEmpty(pageCountStr)) {
			try {
				pageCount = Integer.valueOf(pageCountStr);
			} catch (Exception e) {
				log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
			}
		}
		return queryItems.indexOf(querySelection) % pageCount;
	}

	/**
	 * 重設定上下筆資訊
	 */
	private void resetUpdownInfo() {
		upDownBean.setCurrRow(queryKeeper.getRequireNo());
		upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
		upDownBean.resetTabInfo(RequireBottomTabType.ONPG, queryKeeper.getOnpgSid());
	}

	private boolean checkHelfScreen() {
		if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
		        || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
			this.normalScreenReport();
			display.update("headerTitle");
			display.update("searchBody");
			return true;
		}
		return false;
	}

	/**
	 * 切換 - 返回報表
	 */
	public void normalScreenReport() {
		this.querySelection = this.queryKeeper;
		switchFullType = SwitchType.DETAIL;
		this.toggleSearchBody();
	}

	/**
	 * 切換 - 全畫面需求單
	 *
	 * @param view
	 */
	public void fullScreenForm(Search17View view) {
		this.queryKeeper = this.querySelection = view;
		switchFullType = SwitchType.FULLCONTENT;
		this.toggleSearchBody();
		this.moveScreenTab();
	}

	/**
	 * 切換查詢表身
	 */
	public void toggleSearchBody() {
		if (switchType.equals(SwitchType.CONTENT)) {
			switchType = SwitchType.DETAIL;
			if (querySelection != null) {
				queryKeeper = querySelection;
			} else if (this.queryKeeper == null) {
				this.querySelection = this.queryKeeper = this.queryItems.get(0);
			}
			this.changeRequireContent(queryKeeper);
			this.moveScreenTab();
			return;
		}
		if (switchType.equals(SwitchType.DETAIL)) {
			switchFullType = SwitchType.DETAIL;
			switchType = SwitchType.CONTENT;
		}
	}

	/**
	 * 變更需求單內容
	 *
	 * @param view
	 */
	private void changeRequireContent(Search17View view) {
	    view.setReadRecordType(ReadRecordType.HAS_READ);
		Require require = requireService.findByReqNo(view.getRequireNo());
		loadManager.reloadReqForm(require, loginBean.getUser(), RequireBottomTabType.ONPG);
	}

	/**
	 * 上一筆（分頁）
	 *
	 * @param dtId
	 * @param widgetVar
	 */
	public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
		int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
		if (index > 0) {
			index--;
			querySelection = queryItems.get(index);
		}
		this.refreshViewByOpener(dtId, widgetVar, pageCount);
	}

	/**
	 * 下一筆（分頁）
	 *
	 * @param dtId
	 * @param widgetVar
	 */
	public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
		int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
		if (queryItems.size() > index + 1) {
			index++;
			querySelection = queryItems.get(index);
		}
		this.refreshViewByOpener(dtId, widgetVar, pageCount);
	}

	/**
	 * 刷新列表（分頁）
	 *
	 * @param dtId
	 * @param widgetVar
	 * @param pageCount
	 */
	private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
		queryKeeper = querySelection;
		this.highlightReportTo(widgetVar, pageCount, queryKeeper);
		this.resetUpdownInfo();
		this.checkHelfScreen();
	}

	/**
	 * 上下筆移動
	 *
	 * @param action
	 */
	public void moveRequireTemplateSelect(int action) {
		int index = this.queryItems.indexOf(this.queryKeeper);
		index += action;
		if (index < 0 || index >= this.queryItems.size()) {
			return;
		}
		this.querySelection = this.queryKeeper = this.queryItems.get(index);
		this.changeRequireContent(this.querySelection);
		this.moveScreenTab();
	}

	/**
	 * 開啟 類別樹
	 */
	public void btnOpenCategoryTree() {
		try {
			categoryTreeComponent.init();
			display.showPfWidgetVar("dlgCate");
		} catch (Exception e) {
			log.error("btnOpenCategoryTree Error", e);
			messageCallBack.showMessage(e.getMessage());
		}
	}

	/**
	 * 開啟 單位挑選 組織樹
	 */
	public void btnOpenOrgTree() {
		try {
			orgTreeComponent.initOrgTree(loginBean.getDep(), searchQuery.getRequireDepts(), false, false);
			display.showPfWidgetVar("dlgOrgTree");
		} catch (Exception e) {
			log.error("btnOpenOrgTree Error", e);
			messageCallBack.showMessage(e.getMessage());
		}
	}

	/**
	 * 開啟 轉發至 組織樹
	 */
	public void btnOpenNoticeOrgTree() {
		try {
			orgTreeNoticeComponent.initOrgTree(loginBean.getDep(), searchQuery.getNoticeDepts(), false, false);
			display.showPfWidgetVar("dlgOrgTreeNotice");
		} catch (Exception e) {
			log.error("btnOpenForwardOrgTree Error", e);
			messageCallBack.showMessage(e.getMessage());
		}
	}

	/** 訊息呼叫 */
	private final MessageCallBack messageCallBack = new MessageCallBack() {
		/**
         * 
         */
        private static final long serialVersionUID = -6950542614995408866L;

        @Override
		public void showMessage(String m) {
			MessagesUtils.showError(m);
		}
	};

	/** 類別樹 Component CallBack */
	private final CategoryTreeCallBack categoryTreeCallBack = new CategoryTreeCallBack() {
		/**
         * 
         */
        private static final long serialVersionUID = -2963427354477235585L;

        @Override
		public void showMessage(String m) {
			messageCallBack.showMessage(m);
		}

		@Override
		public void confirmSelCate() {
			categoryTreeComponent.selCate();
			searchQuery.setBigDataCateSids(categoryTreeComponent.getBigDataCateSids());
			searchQuery.setMiddleDataCateSids(categoryTreeComponent.getMiddleDataCateSids());
			searchQuery.setSmallDataCateSids(categoryTreeComponent.getSmallDataCateSids());
			categoryTreeComponent.clearCateSids();
		}

		@Override
		public void loadSelCate(List<String> smallDataCateSids) {
			categoryTreeComponent.init();
			categoryTreeComponent.selectedItem(smallDataCateSids);
			this.confirmSelCate();
		}

	};

	/** 報表 組織樹 Component CallBack */
	private final ReportOrgTreeCallBack reportOrgTreeCallBack = new ReportOrgTreeCallBack() {
		/**
         * 
         */
        private static final long serialVersionUID = -6954400649097308070L;

        @Override
		public void showMessage(String m) {
			messageCallBack.showMessage(m);
		}

		@Override
		public void confirmSelOrg() {
			searchQuery.setRequireDepts(orgTreeComponent.getSelOrgSids());
		}
	};

	/** 報表 組織樹 Component CallBack */
	private final ReportOrgTreeCallBack reportOrgTreeNoticeCallBack = new ReportOrgTreeCallBack() {
		/**
         * 
         */
        private static final long serialVersionUID = 8711071241585062676L;

        @Override
		public void showMessage(String m) {
			messageCallBack.showMessage(m);
		}

		@Override
		public void confirmSelOrg() {
			searchQuery.setNoticeDepts(orgTreeNoticeComponent.getSelOrgSids());
		}
	};

	public String getNoticeDep(Search17View view) {
		// cache 中取回
		String cacheName = "Search17View-getNoticeDep";
		List<String> keys = Lists.newArrayList(view.getOnpgSid());
		String orgNames = wkCommonCache.getCache(cacheName, keys, 5);
		if (orgNames != null) {
			return orgNames;
		}

		// 組單位名稱
		List<String> orgSids = Optional.ofNullable(view.getNoticeDep())
		        .map(send -> send.getValue())
		        .orElse(Lists.newArrayList());
		orgNames = WkOrgUtils.findNameBySidStrs(orgSids, "、");

		// put 快取
		wkCommonCache.putCache(cacheName, keys, orgNames);

		return orgNames;
	}

	public void moveScreenTab() {
		this.r01MBean.getBottomTabMBean().resetTabIdx(this.r01MBean);
		this.r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.ONPG);

		List<String> sids = opService.findSidsBySourceSid(this.r01MBean.getRequire().getSid());
		String indicateSid = queryKeeper.getOnpgSid();
		if (sids.indexOf(indicateSid) != -1) {
			display.execute("PF('op_acc_panel_layer_zero').select(" + sids.indexOf(indicateSid) + ");");
			for (int i = 0; i < sids.size(); i++) {
				if (i != sids.indexOf(indicateSid)) {
					display.execute("PF('op_acc_panel_layer_zero').unselect(" + i + ");");
				}
			}
		}
	}

	private final ReportCustomFilterCallback reportCustomFilterCallback = new ReportCustomFilterCallback() {
		/**
         * 
         */
        private static final long serialVersionUID = 559246227118037095L;

        @Override
		public void reloadDefault(String index) {

			// 查詢登入者所有角色
			List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
			        SecurityFacade.getUserSid(),
			        SecurityFacade.getCompanyId());

			searchQuery17CustomFilter.loadDefaultSetting(
			        searchHelper.getDefaultDepSids(
			                loginBean.getDep().getSid(),
			                roleSids),
			        searchQuery);
			categoryTreeCallBack.loadSelCate(searchQuery.getSmallDataCateSids());
		}
	};

}
