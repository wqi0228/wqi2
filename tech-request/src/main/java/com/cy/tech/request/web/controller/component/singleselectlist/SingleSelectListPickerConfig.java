/**
 * 
 */
package com.cy.tech.request.web.controller.component.singleselectlist;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * SingleSelectListPicker 建構參數設定物件
 * 
 * @author allen1214_wu
 */
@Getter
@Setter
public class SingleSelectListPickerConfig implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 6448928553619338621L;
    public SingleSelectListPickerConfig(SingleSelectListCallback singleSelectListCallback) {
		this.singleSelectListCallback = singleSelectListCallback;
	}

	/**
	 * Callback
	 */
	private SingleSelectListCallback singleSelectListCallback;
	/**
	 * 預設選擇項目Sid
	 */
	private String defaultSelectedItemSid;
}
