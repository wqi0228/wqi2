package com.cy.tech.request.web.controller.setting;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.tech.request.logic.service.AlertInboxService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.repository.result.TransRequireVO;
import com.cy.tech.request.web.controller.view.component.UserAutoComplete;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.google.common.base.Preconditions;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author aken_kao
 */
@Slf4j
@Controller
@Scope("view")
public class TransDeptDispatchMBean extends AbstractBatchTransDepts {
	/**
     * 
     */
    private static final long serialVersionUID = 7434187097061512732L;
    @Autowired
	private RequireService requireService;
	@Autowired
	private AlertInboxService alertInboxService;

	@Getter
	@Setter
	private String action;
	@Getter
	@Setter
	private UserAutoComplete userAutoComplete = new UserAutoComplete();

	@Override
	protected void validate() {
		Preconditions.checkArgument(CollectionUtils.isNotEmpty(originTreeVO.getSelectedDeptSids()), "請選擇轉寄單位!");
	}

	public void search() {
		try {
			validate();
			resultList = requireService.findAlertBoxByDepts(originTreeVO.getSelectedDeptSids());
			assignSuccessIcon();
		} catch (Exception e) {
			MessagesUtils.showError(e.getMessage());
		} finally {
			disabledExportBtn = CollectionUtils.isEmpty(resultList);
			selectedAll = false;
			selectedResultList.clear();
		}
	}

	public void update() {
		try {
			Preconditions.checkArgument(StringUtils.isNotBlank(action), "請選擇動作!");

			List<String> requireSids = selectedResultList.stream().map(TransRequireVO::getSid).collect(Collectors.toList());
			List<String> updatedList = null;
			if ("PLUS".equals(action)) {
				Preconditions.checkArgument(CollectionUtils.isNotEmpty(transOrgTreeVO.getSelectedDeptSids()), "請選擇轉換單位!");
				updatedList = alertInboxService.addTransDept(transOrgTreeVO.getSelectedDepts().get(0).getSid(), requireSids,
				        originTreeVO.getSelectedDeptSids());

				for (String requireNo : updatedList) {
					log.info("單號:{}, {} - {}",
					        requireNo,
					        action,
					        WkOrgUtils.getOrgName(transOrgTreeVO.getSelectedDepts().get(0)));
				}
			} else if ("MINUS".equals(action)) {
				updatedList = alertInboxService.minusTransDept(originTreeVO.getSelectedDeptSids(), requireSids);

				for (String requireNo : updatedList) {
					log.info("單號:{}, {} - {}", requireNo, action, originTreeVO.getSelectedDeptNames());
				}
			}

			updatedSet.addAll(updatedList);
			search();
			MessagesUtils.showInfo("轉單成功!");
		} catch (Exception e) {
			MessagesUtils.showError(e.getMessage());
		}

	}

}
