/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.values;

import com.cy.work.common.utils.WkWebPageUtils;
import com.google.common.base.Strings;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 前端頁面取得組織相關資訊用
 *
 * @author shaun
 */
@Controller
@Scope(WebApplicationContext.SCOPE_APPLICATION)
public class CssBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1176885851199953930L;
    @Autowired
    transient private WkWebPageUtils pageUtils;

    /**
     * 處理OuputLabel無法接受的格式
     *
     * @param value
     * @return
     */
    public String warp(String value) {
        return pageUtils.addWrapTag(value);
    }

    /**
     * 處理OuputLabel無法接受的格式
     *
     * @param value
     * @return
     */
    public String warpTemp(String value) {
        if (Strings.isNullOrEmpty(value)) {
            return pageUtils.addWrapTag(value);
        } else {
            // return pageUtils.addWrapTag(HtmlUtils.htmlUnescape(value));
            return pageUtils.addWrapTag(value);
        }
    }

}
