/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.attachment;

import com.cy.tech.request.logic.vo.AttachmentVO;
import com.cy.tech.request.vo.enums.OthSetAttachmentBehavior;
import com.cy.tech.request.web.attachment.util.AttachmentCondition;
import com.cy.tech.request.web.attachment.util.AttachmentUploadCompant;
import com.cy.tech.request.web.controller.logic.component.OthSetSettingAttachmentLogicComponents;
import com.cy.tech.request.web.listener.ReloadAttCallBack;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.google.common.base.Strings;
import java.util.List;

/**
 * 其他設定資訊( 回覆,回覆的回覆,完成,取消)附件上傳元件
 *
 * @author brain0925_liao
 */
public class TrOsReplyUploadAttachCompant extends AttachmentUploadCompant {

    /**
     * 
     */
    private static final long serialVersionUID = -4031104135174077510L;
    /** 其他設定資訊Sid */
    private String os_sid;
    /** 其他設定資訊單號 */
    private String os_no;
    /** 需求單Sid */
    private String require_sid;
    /** 需求單單號 */
    private String require_no;
    /** HistorySid */
    private String os_history_sid;
    /** 上傳者Sid */
    private Integer lgoinUserSid;
    /** 上傳者部門Sid */
    private Integer loginUserDepSid;
    /** ReloadAttCallBack */
    private final ReloadAttCallBack reloadAttCallBack;
    /** OthSetAttachmentBehavior 行為 */
    private OthSetAttachmentBehavior othSetAttachmentBehavior;
    /**上傳完畢,更新View ID*/
    private String uploadFinishView;

    public TrOsReplyUploadAttachCompant(ReloadAttCallBack reloadAttCallBack) {
        this.reloadAttCallBack = reloadAttCallBack;
    }

    public void loadAttData(String os_sid, String os_no, String require_sid,
            String require_no, String os_history_sid, Integer lgoinUserSid,
            Integer loginUserDepSid, boolean autoMappingEntity,
            OthSetAttachmentBehavior othSetAttachmentBehavior, String uploadFinishView) {
        this.os_sid = os_sid;
        this.os_no = os_no;
        this.require_sid = require_sid;
        this.require_no = require_no;
        this.os_history_sid = os_history_sid;
        this.lgoinUserSid = lgoinUserSid;
        this.loginUserDepSid = loginUserDepSid;
        this.othSetAttachmentBehavior = othSetAttachmentBehavior;
        this.uploadFinishView = uploadFinishView;
        AttachmentCondition attachmentCondition = new AttachmentCondition(os_history_sid, "", autoMappingEntity);
        super.loadAttachment(attachmentCondition);
    }

    @Override
    protected List<AttachmentVO> getPersistedAttachments(String os_history_sid) {
        return OthSetSettingAttachmentLogicComponents.getInstance().getAttachmentVOByOSSidAndHistorySId(os_sid, os_history_sid);
    }

    @Override
    public Integer getFileSizeLimit() {
        return 50;
    }

    @Override
    public void finishUpload() {
        if (!Strings.isNullOrEmpty(uploadFinishView)) {
            DisplayController.getInstance().update(uploadFinishView);
        }

    }

    @Override
    public String getDirectoryName() {
        return "tech-request";
    }

    @Override
    protected AttachmentVO createAndPersistAttachment(String fileName, AttachmentCondition attachmentCondition) {

        if (attachmentCondition.isAutoMappingEntity()) {
            AttachmentVO result = OthSetSettingAttachmentLogicComponents.getInstance().createAttachment(fileName,
                    os_sid, os_no, require_sid, require_no, os_history_sid, lgoinUserSid, loginUserDepSid, othSetAttachmentBehavior);
            reloadAttCallBack.reloadUploadAtt(os_history_sid);
            result.setChecked(true);
            return result;
        } else {
            AttachmentVO result = OthSetSettingAttachmentLogicComponents.getInstance().createAttachment(fileName,
                    "", "", "", "", "", lgoinUserSid, loginUserDepSid, othSetAttachmentBehavior);
            reloadAttCallBack.reloadUploadAtt("");
            result.setChecked(true);
            return result;
        }
    }

}
