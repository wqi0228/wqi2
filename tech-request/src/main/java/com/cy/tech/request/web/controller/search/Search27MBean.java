/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;

import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.anew.manager.TrIssueMappTransManager;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.view.Search27View;
import com.cy.tech.request.logic.service.CategorySettingService;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.URLService.URLServiceAttr;
import com.cy.tech.request.logic.service.customer.ReqWorkCustomerHelper;
import com.cy.tech.request.logic.vo.CustomerTo;
import com.cy.tech.request.logic.vo.query.search.SearchQuery;
import com.cy.tech.request.vo.constants.ReqPermission;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.require.RequireForwardDeAndPersonMBean;
import com.cy.tech.request.web.controller.search.helper.Search27Helper;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.view.component.CategoryTreeComponent;
import com.cy.tech.request.web.controller.view.component.ReportOrgTreeComponent;
import com.cy.tech.request.web.controller.view.component.searchquery.SearchQuery27;
import com.cy.tech.request.web.controller.view.component.searchquery.SearchQuery27CustomFilter;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.listener.CategoryTreeCallBack;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.listener.ReportCustomFilterCallback;
import com.cy.tech.request.web.listener.ReportOrgTreeCallBack;
import com.cy.tech.request.web.logic.helper.Search27LogicHelper;
import com.cy.tech.request.web.pf.utils.CommonBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.tech.request.web.view.logic.TROtherCategoryHelper;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserWithRolesCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.customer.vo.enums.EnableType;
import com.cy.work.group.vo.WorkLinkGroup;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 需求單查詢 for gm
 *
 * @author kasim
 */
@Controller
@Scope("view")
@Slf4j
@ManagedBean
public class Search27MBean extends BaseSearchMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 270743408788546002L;
    @Autowired
    transient private TrIssueMappTransManager issueMappTransManager;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private ReqWorkCustomerHelper reqWorkCustomerHelper;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private URLService urlService;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private Require01MBean r01MBean;
    @Autowired
    transient private Search27Helper search27Helper;
    @Autowired
    transient private OrganizationService orgService;
    @Autowired
    transient private WorkBackendParamHelper workBackendParamHelper;
    @Autowired
    private ReportCustomFilterLogicComponent reportCustomFilterLogicComponent;
    @Autowired
    private CategorySettingService categorySettingService;
    @Autowired
    private Search27LogicHelper search27LogicHelper;
    @Autowired
    private TROtherCategoryHelper otherCategoryHelper;

    @Getter
    /** 類別樹 Component */
    private CategoryTreeComponent categoryTreeComponent;
    @Getter
    /** 報表 組織樹 Component */
    private ReportOrgTreeComponent orgTreeComponent;
    @Getter
    /** 查詢物件 */
    private SearchQuery27 searchQuery;

    /**
     * 公用實做方法, 回傳查詢條件物件
     */
    @Override
    public SearchQuery getQueryCondition() { return this.searchQuery; }

    @Getter
    private SearchQuery27CustomFilter search27ReportCustomFilterComponent;

    private List<Search27View> allQueryItems;
    @Getter
    @Setter
    /** 所有的需求單 */
    private List<Search27View> queryItems;
    @Getter
    @Setter
    /** 選擇的需求單 */
    private Search27View querySelection;
    @Getter
    /** 上下筆移動keeper */
    private Search27View queryKeeper;
    @Getter
    /** 切換模式 */
    private SwitchType switchType = SwitchType.CONTENT;
    @Getter
    /** 切換模式 - 全畫面狀態 */
    private SwitchType switchFullType = SwitchType.DETAIL;
    @Getter
    /** 在匯出的時候，某些內容需要隱藏 */
    private boolean hasDisplay = true;
    @Getter
    private final String dataTableId = "dtRequire";
    /** 分派快取 */
    private List<String> hasCreateCache;
    /** 追蹤快取 */
    private List<String> hasTraceCache;
    /** 分派快取 */
    private List<String> hasAssignCache;
    /** 有轉寄給自己快取 */
    private List<String> hasForwardSelfCache;
    /** 有轉入需求單的sid */
    private List<String> hasTransIssue;
    @Getter
    /** 廳主選項 */
    private List<CustomerTo> customerItems;
    @Getter
    /** 廳主分頁選項 */
    private List<CustomerTo> customerPageItems;
    @Getter
    /** 第２分類選項 */
    private List<SelectItem> otherCategoryItems;
    @Getter
    /** 需求來源選項 */
    private List<SelectItem> reqSourceItems;
    /** 特殊處理類別名稱 */
    private String categoryName = "內部需求";

    @PostConstruct
    @Override
    public void init() {
        super.init();
        this.initComponent();
        allQueryItems = Lists.newArrayList();
        this.search();
    }

    /**
     * 初始化元件
     */
    private void initComponent() {

        // 具 GM 權限 (GM後台維護 + 需求單系統管理員)
        boolean hasGmRight = workBackendParamHelper.isGM(SecurityFacade.getUserSid())
                || WkUserUtils.isUserHasRole(
                        SecurityFacade.getUserSid(),
                        SecurityFacade.getCompanyId(),
                        ReqPermission.ROLE_SYS_ADMIN);

        search27ReportCustomFilterComponent = new SearchQuery27CustomFilter(
                loginBean.getUserSId(),
                loginBean.getDep().getSid(),
                reportCustomFilterLogicComponent,
                messageCallBack,
                display,
                categorySettingService,
                reportCustomFilterCallback);

        // 查詢登入者所有角色
        List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
                SecurityFacade.getUserSid(),
                SecurityFacade.getCompanyId());

        orgTreeComponent = new ReportOrgTreeComponent(
                loginBean.getCompanyId(),
                loginBean.getDep(),
                roleSids,
                true,
                reportOrgTreeCallBack);

        searchQuery = new SearchQuery27(
                loginBean.getComp(),
                loginBean.getDep(),
                loginBean.getUser(),
                hasGmRight,
                ReportType.REQUIRE_FOR_GM,
                categoryTreeCallBack,
                categorySettingService,
                search27LogicHelper);

        categoryTreeComponent = new CategoryTreeComponent(categoryTreeCallBack);
        categoryTreeComponent.init();
        this.initCustomerItems();
        this.initOtherCategoryItems();
        this.initReqSourceItems(hasGmRight);
        this.clearQuery();
        searchQuery.loadDefaultSetting(searchQuery.getSmallDataCateSids());

    }

    /**
     * 取得 廳主選項
     */
    private void initCustomerItems() {
        customerItems = Lists.newArrayList();
        reqWorkCustomerHelper.findByEnable(EnableType.ENABLE).forEach(each -> {
            customerItems.add(new CustomerTo(each.getSid(), each.getAlias(), each.getName(), each.getLoginCode(),
                    CommonBean.getInstance().get(each.getCustomerType())));
        });
    }

    /**
     * 取得 第2分類選項
     */
    private void initOtherCategoryItems() {
        otherCategoryItems = otherCategoryHelper.bulidItemsByActive();
    }

    /**
     * 取得 需求來源選項
     *
     * @param hasGm
     */
    private void initReqSourceItems(Boolean hasGm) {
        List<String> reqSources = Lists.newArrayList();
        if (hasGm) {
            reqSources.add("All");
        }
        reqSources.addAll(Lists.newArrayList("立", "分", "轉"));
        reqSourceItems = reqSources.stream()
                .map(each -> new SelectItem(each, each))
                .collect(Collectors.toList());
    }

    /**
     * 清除/還原選項
     */
    private void clearQuery() {
        searchQuery.init();
        categoryTreeComponent.setIsAllSel(Boolean.TRUE);
        categoryTreeComponent.actionSelectALL();
        categoryTreeCallBack.confirmSelCate();
    }

    /**
     * 查詢
     */
    public void search() {
        try {
            allQueryItems = search27Helper.findWithQuery(loginBean.getCompanyId(), searchQuery, customerItems, loginBean.getUserSId());
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        }
        List<String> reqSids = allQueryItems.stream().map(Search27View::getSid).collect(Collectors.toList());
        if (!Strings.isNullOrEmpty(searchQuery.getReqSource()) && "All".equals(searchQuery.getReqSource())) {
            this.bulidHasCacheByGm(reqSids);
        } else {
            this.hasCreateCache = null;
            this.hasAssignCache = search27Helper.createAssignCache(searchQuery, reqSids);
            this.hasForwardSelfCache = search27Helper.createForwardSelfCache(searchQuery, reqSids);
        }
        this.hasTraceCache = search27Helper.createTraceCache(searchQuery, reqSids);
        this.hasTransIssue = issueMappTransManager.createTransFromIssue(reqSids);
        this.filterCustomerPage(Boolean.TRUE);
    }

    /**
     * 建立GM的 立/分/轉 Cache
     *
     * @param reqSids
     */
    private void bulidHasCacheByGm(List<String> reqSids) {
        if (searchQuery.getRequireDepts() == null || searchQuery.getRequireDepts().isEmpty()) {
            this.initCache();
            return;
        }
        Map<Integer, List<String>> temp = Maps.newHashMap();
        boolean ck = searchQuery.getRequireDepts().stream()
                .anyMatch(each -> {
                    this.addSearchOrgByCacheMap(each, temp);
                    return temp.size() > 1;
                });
        if (!ck && !temp.isEmpty()) {
            List<String> orgSids = temp.entrySet().stream()
                    .flatMap(each -> each.getValue().stream())
                    .collect(Collectors.toList());
            this.hasCreateCache = search27Helper.createCreateCache(orgSids, reqSids);
            this.hasAssignCache = search27Helper.createAssignCache(orgSids, reqSids);
            this.hasForwardSelfCache = search27Helper.createForwardSelfCache(orgSids, reqSids);
            return;
        }
        this.initCache();
    }

    /**
     * 初始化 Cache
     */
    private void initCache() {
        this.hasCreateCache = Lists.newArrayList();
        this.hasAssignCache = Lists.newArrayList();
        this.hasForwardSelfCache = Lists.newArrayList();
    }

    /**
     * 判斷GM 查詢 立/分/轉 所欲查詢的org
     *
     * @param orgSid
     * @param temp
     */
    private void addSearchOrgByCacheMap(String orgSid, Map<Integer, List<String>> temp) {
        Org org = WkOrgCache.getInstance().findBySid(orgSid);
        if (org == null) {
            log.error("無法取得部門物件 org sid：" + orgSid + "!!");
            return;
        }
        Org parentOrg = orgService.getBasicOrgByMinisterial(org);
        if (parentOrg == null) {
            log.error("無法取得基本部門物件(處) org sid：" + orgSid + "!!");
            return;
        }
        List<String> values = Lists.newArrayList();
        if (temp.containsKey(parentOrg.getSid())) {
            values = temp.get(parentOrg.getSid());
        }
        values.add(orgSid);
        temp.put(parentOrg.getSid(), values);
    }

    /**
     * 過濾 廳主
     *
     * @param isSearch
     */
    public void filterCustomerPage(Boolean isSearch) {
        if (searchQuery.getIsCustomerPage()) {
            if (searchQuery.getCustomerPage() == null && !customerPageItems.isEmpty()) {
                searchQuery.setCustomerPage(customerPageItems.get(0));
            }
        }
        this.bulidQueryItemsAndCustomerPageItems(isSearch);
    }

    /**
     * 建立廳主分頁選項
     */
    private void bulidQueryItemsAndCustomerPageItems(Boolean isSearch) {
        queryItems = Lists.newArrayList();
        Set<Long> customerSet = Sets.newHashSet();
        allQueryItems.stream().forEach(v -> {
            if (isSearch && v.getCustomer() != null) {
                customerSet.add(v.getCustomer());
            }
            if (!(searchQuery.getIsCustomerPage()
                    && (searchQuery.getCustomerPage() != null && !searchQuery.getCustomerPage().getSid().equals(v.getCustomer())))) {
                queryItems.add(v);
            }
        });

        if (isSearch) {
            List<CustomerTo> customers = Lists.newArrayList();
            try {
                customers = reqWorkCustomerHelper.findTosBySids(customerSet);
            } catch (Exception e) {
                log.error("查詢失敗：" + e.getMessage(), e);
            }

            customerPageItems = Lists.newArrayList(customers);
            if (searchQuery.getIsCustomerPage()
                    && (customers.isEmpty() || !customers.contains(searchQuery.getCustomerPage()))) {
                searchQuery.setCustomerPage(null);
                this.filterCustomerPage(Boolean.FALSE);
            }
        }
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        this.clearQuery();
        this.searchQuery.loadDefaultSetting(searchQuery.getSmallDataCateSids());
        this.search();
    }

    /**
     * 變更需求來源選項
     */
    public void changeReqSource() {
        if ("立".equals(searchQuery.getReqSource())) {
            searchQuery.initDefaultDepts();
        } else {
            List<String> requireDepts = WkOrgCache.getInstance().findAllDepByCompID(SecurityFacade.getCompanyId()).stream()
                    .filter(org -> org != null && org.getSid() != null)
                    .map(org -> String.valueOf(org.getSid()))
                    .collect(Collectors.toList());
            searchQuery.setRequireDepts(requireDepts);
        }
        this.search();
    }

    /**
     * 變更第２分類查詢選項
     */
    public void changeByOtherCategory() {
        try {
            if (searchQuery.getOtherCategorySids() != null
                    && !searchQuery.getOtherCategorySids().isEmpty()) {
                searchQuery.changeByOtherCategory(Lists.newArrayList(searchQuery.getOtherCategorySids().stream()
                        .map(each -> otherCategoryHelper.findToBySid(each))
                        .flatMap(each -> each.getCateSids().stream())
                        .collect(Collectors.toSet())));
            } else {
                searchQuery.changeByOtherCategory(Lists.newArrayList());
            }
        } catch (Exception e) {
            log.error("changeByOtherCategory Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 變更是否包含內部需求
     */
    public void changeByContainsInternalReq() {
        try {
            categoryTreeComponent.removeSelByCategoryName(categoryName, searchQuery.getIsContainsInternalReq());
            categoryTreeCallBack.confirmSelCate();
            this.search();
        } catch (Exception e) {
            log.error("changeByContainsInternalReq Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 依廳主分頁
     */
    public void chgCustomerPage() {
        this.filterCustomerPage(Boolean.FALSE);
    }

    /**
     * 開啟檢視網址
     */
    public String getRelevanceViewUrl(Search27View view) {
        if (view == null) {
            return "";
        }
        Require r = requireService.findByReqNo(view.getRequireNo());
        if (r == null) {
            return "";
        }
        WorkLinkGroup link = r.getLinkGroup();
        if (link == null) {
            return "";
        }
        return "../require/require04.xhtml" + urlService.createSimpleURLLink(URLServiceAttr.URL_ATTR_L, link.getSid(), view.getRequireNo(), 1);
    }

    /**
     * 隱藏部分column裡的內容
     */
    public void hideColumnContent() {
        hasDisplay = false;
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        helper.exportExcel(document, searchQuery.getStartDate(), searchQuery.getEndDate(), searchQuery.getReportType());
        hasDisplay = true;
    }

    /**
     * 清除進階查詢條件
     */
    public void btnClearAdvance() {
        try {
            searchQuery.clearAdvance();
        } catch (Exception e) {
            log.error("btnClearAdvance Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Search27View view) {
        view.setReadRecordType(ReadRecordType.HAS_READ);
        Require r = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(r, loginBean.getUser());
    }

    /**
     * 列表點選轉寄功能
     * 
     * @param view
     */
    public void openForwardDialog(Search27View view) {
        this.queryKeeper = this.querySelection = view;

        if (SwitchType.DETAIL.equals(switchType)) {
            this.changeRequireContent(this.queryKeeper);
            this.display.update(Lists.newArrayList(
                    "@(.reportUpdateClz)",
                    "title_info_click_btn_id",
                    "require01_title_info_id",
                    "require_template_id",
                    "viewPanelBottomInfoId",
                    "req03botmid",
                    this.dataTableId + "1"));
        }

        RequireForwardDeAndPersonMBean mbean = WorkSpringContextHolder.getBean(RequireForwardDeAndPersonMBean.class);
        mbean.openDialog(view.getRequireNo(), r01MBean);
    }

    /**
     * 列表執行關聯動作
     *
     * @param view
     */
    public void initRelevance(Search27View view) {
        this.querySelection = view;
        this.queryKeeper = this.querySelection;
        Require r = requireService.findByReqNo(view.getRequireNo());
        this.r01MBean.setRequire(r);
        this.r01MBean.getRelevanceMBean().initRelevance();
    }

    /**
     * 列表執行追蹤動作
     *
     * @param view
     */
    public void btnAddTrack(Search27View view) {
        Require r = this.highlightAndReturnRequire(view);
        this.r01MBean.setRequire(r);
        this.r01MBean.getTraceActionMBean().initTrace(this.r01MBean);
    }

    /**
     * 列表標註及回傳需求單
     *
     * @param view
     * @return
     */
    private Require highlightAndReturnRequire(Search27View view) {
        this.querySelection = view;
        this.queryKeeper = this.querySelection;
        return requireService.findByReqNo(view.getRequireNo());
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search27View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.removeClassByTextBold(dtId, pageCount);
        this.transformHasRead();
        this.checkHelfScreen();
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Search27View to) {
        querySelection = to;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
    }

    /**
     * 去除粗體Class
     *
     * @param dtId
     * @param pageCount
     */
    private void removeClassByTextBold(String dtId, String pageCount) {
        display.execute("removeClassByTextBold('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
        display.execute("changeAlreadyRead('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 變更已閱讀
     */
    private void transformHasRead() {
        querySelection.setReadRecordType(ReadRecordType.HAS_READ);
        queryKeeper = querySelection;
        queryItems.set(queryItems.indexOf(querySelection), querySelection);
    }

    /**
     * 檢核版面狀態
     *
     * @return
     */
    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            display.update("headerTitle");
            display.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Search27View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (Search27View) event.getObject();
        this.changeRequireContent(this.queryKeeper);
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.transformHasRead();
        this.removeClassByTextBold(dtId, pageCount);
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    /**
     * 判斷是否有追蹤資料
     *
     * @param view
     * @return
     */
    public String showTrace(Search27View view) {
        if (view == null || CollectionUtils.isEmpty(hasTraceCache)) {
            return "　";
        }
        return hasTraceCache.contains(view.getSid()) ? "★" : "　";
    }

    /**
     * 判斷是否有分派資料
     *
     * @param view
     * @return
     */
    public String showCreate(Search27View view) {
        if (view == null) {
            return "　";
        }
        if (hasCreateCache == null) {
            return view.getCreate() ? "Y" : "　";
        }
        return hasCreateCache.contains(view.getSid()) ? "Y" : "　";
    }

    /**
     * 判斷是否有分派資料
     *
     * @param view
     * @return
     */
    public String showAssign(Search27View view) {
        if (view == null || CollectionUtils.isEmpty(hasAssignCache)) {
            return "　";
        }
        return hasAssignCache.contains(view.getSid()) ? "Y" : "　";
    }

    /**
     * 判斷是否有轉寄資料
     *
     * @param view
     * @return
     */
    public String showForwardSelf(Search27View view) {
        if (view == null || CollectionUtils.isEmpty(hasForwardSelfCache)) {
            return "　";
        }
        return hasForwardSelfCache.contains(view.getSid()) ? "Y" : "　";
    }

    /**
     * 判斷是否有轉入需求單資料
     *
     * @param view
     * @return
     */
    public String hasTransByIssue(Search27View view) {
        if (view == null || CollectionUtils.isEmpty(hasTransIssue)) {
            return "　";
        }
        return hasTransIssue.contains(view.getSid()) ? "Y" : "　";
    }

    /**
     * 檢查附加狀態
     *
     * @param view
     * @return
     */
    public String getAttInfo(Search27View view) {
        Boolean checkAtt = view.getCheckAttachment();
        if (checkAtt) {
            if (view.getHasAttachment()) {
                return "檢核正常";
            } else {
                return "檢核異常";
            }
        } else {
            return "不需檢核";
        }
    }

    /**
     * 開啟查詢 類別樹
     */
    public void btnOpenCategoryTree() {
        try {
            if (searchQuery.getSmallDataCateSids() != null && !searchQuery.getSmallDataCateSids().isEmpty()) {
                categoryTreeComponent.selectedItem(searchQuery.getSmallDataCateSids());
            }
            display.showPfWidgetVar("dlgCate");
        } catch (Exception e) {
            log.error("btnOpenCategoryTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 開啟查詢 單位挑選 組織樹
     */
    public void btnOpenOrgTree() {
        try {
            Boolean isCreateFlag = !"立".equals(searchQuery.getReqSource());
            if (isCreateFlag) {
                orgTreeComponent.initOrgTree(loginBean.getComp(), searchQuery.getRequireDepts(), false, isCreateFlag);
            } else {
                orgTreeComponent.initOrgTree(loginBean.getDep(), searchQuery.getRequireDepts(), false, isCreateFlag);
            }
            display.showPfWidgetVar("dlgOrgTree");
        } catch (Exception e) {
            log.error("btnOpenOrgTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /** 訊息呼叫 */
    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -4139166756990792438L;

        @Override
        public void showMessage(String m) {
            MessagesUtils.showError(m);
        }
    };

    /** 查詢欄位自訂義 Component CallBack */
    private final ReportCustomFilterCallback reportCustomFilterCallback = new ReportCustomFilterCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = -1818617361378999066L;

        @Override
        public void reloadDefault(String index) {
            categoryTreeComponent.clearCate();
            searchQuery.loadDefaultSetting(Lists.newArrayList());
            search();
        }
    };

    /** 類別樹 Component CallBack */
    private final CategoryTreeCallBack categoryTreeCallBack = new CategoryTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -5408247893153646289L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelCate() {
            categoryTreeComponent.selCate();
            searchQuery.setBigDataCateSids(categoryTreeComponent.getBigDataCateSids());
            searchQuery.setMiddleDataCateSids(categoryTreeComponent.getMiddleDataCateSids());
            searchQuery.setSmallDataCateSids(categoryTreeComponent.getSmallDataCateSids());
            categoryTreeComponent.clearCateSids();
        }

        @Override
        public void loadSelCate(List<String> smallDataCateSids) {
            categoryTreeComponent.selectedItem(smallDataCateSids);
            this.confirmSelCate();
        }

        @Override
        public void actionSelectAll() {
            if (categoryTreeComponent.getIsAllSel() && !searchQuery.getIsContainsInternalReq()) {
                searchQuery.setIsContainsInternalReq(Boolean.TRUE);
                display.update("id_headerPanel_isContainsInternalReq");
            } else if (!categoryTreeComponent.getIsAllSel() && searchQuery.getIsContainsInternalReq()) {
                searchQuery.setIsContainsInternalReq(Boolean.FALSE);
                display.update("id_headerPanel_isContainsInternalReq");
            }
        }

        @Override
        public void onNodeSelect() {
            if (!searchQuery.getIsContainsInternalReq() && categoryTreeComponent.checkSelByCategoryName(categoryName)) {
                searchQuery.setIsContainsInternalReq(Boolean.TRUE);
                display.update("id_headerPanel_isContainsInternalReq");
            }
        }

        @Override
        public void onNodeUnSelect() {
            if (searchQuery.getIsContainsInternalReq() && !categoryTreeComponent.checkSelByCategoryName(categoryName)) {
                searchQuery.setIsContainsInternalReq(Boolean.FALSE);
                display.update("id_headerPanel_isContainsInternalReq");
            }
        }
    };

    /** 報表 組織樹 Component CallBack */
    private final ReportOrgTreeCallBack reportOrgTreeCallBack = new ReportOrgTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 4103759450048741739L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelOrg() {
            searchQuery.setRequireDepts(orgTreeComponent.getSelOrgSids());
        }
    };

    /**
     * 畫面用全部製作進度查詢
     *
     * @return
     */
    public SelectItem[] getReqStatusItems() { return search27Helper.getReqStatusItems(); }
}
