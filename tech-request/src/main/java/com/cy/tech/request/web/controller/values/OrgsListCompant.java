package com.cy.tech.request.web.controller.values;

import com.cy.commons.vo.Org;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author jason_h
 */
public class OrgsListCompant implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3707306698077769692L;

    // 未選取清單
    @Getter
    @Setter
    private List<Org> unSelList = Lists.newArrayList();

    // 已選取清單
    @Getter
    @Setter
    private List<Org> hadSelList = Lists.newArrayList();

    // 未選取清單的選擇項目
    @Getter
    @Setter
    private List<Org> unSelItems = Lists.newArrayList();

    // 已選取清單的選擇項目
    @Getter
    @Setter
    private List<Org> hadSelItems = Lists.newArrayList();

    /** disable item(不可取消已經選擇清單) */
    @Getter
    @Setter
    private List<Org> disableHadList = Lists.newArrayList();

    /**
     * Creates a new instance of OrgsListCompant
     */
    public OrgsListCompant() {
    }

    public void pickToHadSelList() {
        if (WkStringUtils.isEmpty(unSelList)) {
            return;
        }

        if (WkStringUtils.isEmpty(unSelItems)) {
            hadSelList.add(unSelList.get(0));
            hadSelItems.add(unSelList.get(0));
            unSelList.remove(0);
            return;
        }

        if (WkStringUtils.isEmpty(unSelItems)) {
            unSelList.removeAll(unSelItems);
        }

        hadSelItems = unSelItems;
        unSelItems = Lists.newArrayList();
        hadSelList.addAll(hadSelItems);
    }

    /**
     * 從已選取退回未選取
     */
    public void pickToUnSelList() {
        if (hadSelList.isEmpty()) {
            return;
        }
        List<Org> temp = Lists.newArrayList(hadSelList);
        temp.removeAll(disableHadList);
        if (temp.isEmpty()) {
            return;
        }

        if (hadSelItems == null || hadSelItems.isEmpty()) {
            Org o = temp.get(0);
            unSelList.add(o);
            unSelItems.add(o);
            hadSelList.remove(o);
            return;
        }

        hadSelList.removeAll(hadSelItems);
        unSelItems = hadSelItems;
        hadSelItems = Lists.newArrayList();
        unSelList.addAll(unSelItems);
    }

    public void pickAllToHadSelList() {
        if (unSelList.isEmpty()) {
            return;
        }

        hadSelList.addAll(unSelList);
        hadSelItems.addAll(unSelList);
        unSelList = Lists.newArrayList();
        unSelItems = Lists.newArrayList();
    }

    /**
     * 從已選取全部退回未選取
     */
    public void pickAllToUnSelList() {
        if (hadSelList.isEmpty()) {
            return;
        }
        List<Org> temp = Lists.newArrayList(hadSelList);
        temp.removeAll(disableHadList);
        if (temp.isEmpty()) {
            return;
        }
        unSelList.addAll(temp);
        unSelItems.addAll(temp);
        hadSelList = Lists.newArrayList(disableHadList);
        hadSelItems = Lists.newArrayList();
    }

    public void clear() {
        unSelList = Lists.newArrayList();
        hadSelList = Lists.newArrayList();
        unSelItems = Lists.newArrayList();
        hadSelItems = Lists.newArrayList();
    }

    /**
     * 初始化
     *
     * @param unSelList
     * @param hadSelList
     */
    public void init(List<Org> unSelList, List<Org> hadSelList) {
        this.init(unSelList, hadSelList, Lists.newArrayList());
    }

    /**
     * 初始化
     *
     * @param unSelList
     * @param hadSelList
     * @param disableHadOrg
     */
    public void init(List<Org> unSelList, List<Org> hadSelList, Org disableHadOrg) {
        this.init(unSelList, hadSelList, Lists.newArrayList(disableHadOrg));
    }

    /**
     * 初始化
     *
     * @param unSelList
     * @param hadSelList
     * @param disableHadList
     */
    public void init(List<Org> unSelList, List<Org> hadSelList, List<Org> disableHadList) {
        this.unSelList = Lists.newArrayList(unSelList);
        this.hadSelList = Lists.newArrayList(hadSelList);
        this.disableHadList = Lists.newArrayList(disableHadList);
    }

    public void dblSelectFromUnSel(SelectEvent event) {
        Org selOrg = (Org) event.getObject();
        unSelItems.remove(selOrg);
        unSelList.remove(selOrg);

        hadSelItems.add(selOrg);
        hadSelList.add(selOrg);
    }

    public void dblSelectFromHadSel(SelectEvent event) {
        Org selOrg = (Org) event.getObject();
        hadSelItems.remove(selOrg);
        hadSelList.remove(selOrg);

        unSelItems.add(selOrg);
        unSelList.add(selOrg);
    }

    /**
     * 判斷是否可以退回未選取
     */
    public Boolean checkDisableHadList(Org org) {
        return disableHadList.contains(org);
    }

}
