/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require;

import lombok.Getter;

/**
 * 需求單執行類型
 */
public enum RequireStep {

    /** 樹狀圖選擇類別 */
    NEW_IN_TREE("新增需求單"),
    /** 複製 */
    NEW_IN_TREE_COPY("複製模式"),
    /** 新增單據畫面 */
    NEW_IN_PAGE("新增需求單"),
    /** 載入單據畫面 */
    LOAD_IN_PAGE("需求單維護作業"),
    /** 載入單據畫面 - 編輯中 */
    LOAD_IN_PAGE_EDIT("需求單維護作業"),;

    @Getter
    private final String header;

    RequireStep(String name) {
        this.header = name;
    }

}
