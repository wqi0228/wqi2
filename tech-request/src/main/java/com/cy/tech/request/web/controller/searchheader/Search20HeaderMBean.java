/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.searchheader;

import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.web.controller.enums.ReqStatusMBean;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

/**
 * 退件暫存單據查詢的搜尋表頭
 *
 * @author jason_h
 */
@Controller
@Scope("view")
public class Search20HeaderMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4897482090374541432L;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private ReqStatusMBean reqStatusUtils;

    /** 退件人員名稱 */
    @Getter
    @Setter
    private String rejectUserName;
    /** 退件原因 */
    @Getter
    @Setter
    private String rejectReason;
    /** 退件啟始時間 */
    @Getter
    @Setter
    private Date startRejectedDate;
    /** 退件結束時間 */
    @Getter
    @Setter
    private Date endRejectedDate;
    /** 選擇的需求製作進度 */
    @Getter
    @Setter
    private RequireStatusType selectRequireStatusType = RequireStatusType.ROLL_BACK_NOTIFY;

    @PostConstruct
    private void init() {
        this.clear();
    }

    /**
     * 清除/還原選項
     */
    public void clear() {
        this.initCommHeader();
        this.initHeader();
    }

    /**
     * 初始化commHeader
     */
    private void initCommHeader() {
        commHeaderMBean.clear();
        commHeaderMBean.clearAdvanceWithoutForward();
        commHeaderMBean.initDefaultRequireDepts(false);
    }

    /**
     * 初始化Header
     */
    private void initHeader() {
        rejectUserName = null;
        rejectReason = null;
        selectRequireStatusType = RequireStatusType.ROLL_BACK_NOTIFY;
        startRejectedDate = null;
        endRejectedDate = null;
    }

    /**
     * 清除/還原進階選項
     */
    public void clearAdvance() {
        startRejectedDate = null;
        endRejectedDate = null;
        commHeaderMBean.clearAdvanceWithoutForward();
    }

    /**
     * 是否為RequireTrace entity本身的欄位查詢
     *
     * @return
     */
    public boolean isRequireTraceCondition() {
        if (!Strings.isNullOrEmpty(rejectUserName)) {
            return true;
        }
        if (!Strings.isNullOrEmpty(rejectReason)) {
            return true;
        }
        if (startRejectedDate != null && endRejectedDate != null) {
            return true;
        }
        return false;
    }

    /**
     * 建立需求狀態查詢 List
     *
     * @return
     */
    public List<String> createQueryReqStatus() {
        return reqStatusUtils.createQueryReqStatus(this.selectRequireStatusType, this.getAllReqStatus());
    }

    /**
     * 取得此報表全部製作進度查詢
     *
     * @return
     */
    private List<RequireStatusType> getAllReqStatus() {
        return reqStatusUtils.createExcludeStatus(
                Lists.newArrayList(
                        RequireStatusType.NEW_INSTANCE,
                        RequireStatusType.DRAFT
                )
        );
    }

    /**
     * 畫面用全部製作進度查詢
     *
     * @return
     */
    public SelectItem[] getReqStatusItems() {
        return reqStatusUtils.buildItem(this.getAllReqStatus());
    }

    /**
     * 建立需求狀態查詢 List
     *
     * @return
     */
    public List<String> createQueryAllReqStatus() {
        return this.getAllReqStatus().stream()
                .map(each -> each.name())
                .collect(Collectors.toList());
    }
}
