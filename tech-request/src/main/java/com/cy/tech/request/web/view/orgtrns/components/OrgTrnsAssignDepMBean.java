package com.cy.tech.request.web.view.orgtrns.components;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.cy.tech.request.logic.service.orgtrns.OrgTrnsAssignService;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsBaseComponentPageVO;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsDtVO;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsWorkVerifyVO;
import com.cy.tech.request.web.pf.utils.ConfirmCallbackDialogController;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.HikariPoolMXBean;

/**
 * 回覆分派單位資料轉檔 MBean
 */
@Component
@Scope("view")
@Slf4j
public class OrgTrnsAssignDepMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1887427460049722701L;
    // ========================================================================
    // 服務元件區
    // ========================================================================
    @Autowired
    private transient DisplayController displayController;
    @Autowired
    private transient OrgTrnsAssignService orgTrnsAssignService;
    @Autowired
    private transient WorkBackendParamHelper workBackendParamHelper;
    @Autowired
    private transient ConfirmCallbackDialogController confirmCallbackDialogController;
    @Autowired
    private transient EntityManager entityManager;

    // ========================================================================
    // 變數區
    // ========================================================================
    @Getter
    @Setter
    private OrgTrnsBaseComponentPageVO cpontVO = new OrgTrnsBaseComponentPageVO();

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * 畫面功能:開啟操作視窗
     */
    public void openTrnsWindow(List<OrgTrnsWorkVerifyVO> allTrnsWorks) {

        // ====================================
        // 檢核
        // ====================================
        if (WkStringUtils.isEmpty(allTrnsWorks)) {
            MessagesUtils.showWarn("無組織異動設定資料！");
            return;
        }

        // ====================================
        // 初始化VO
        // ====================================
        this.cpontVO = new OrgTrnsBaseComponentPageVO();
        this.cpontVO.setAllTrnsWorks(allTrnsWorks);

        // ====================================
        // 查詢
        // ====================================
        if (this.btnQuery()) {
            // 打開視窗
            this.displayController.showPfWidgetVar("dlgAssignDepsTransform");
        }

    }

    /**
     * 畫面功能:查詢
     */
    public boolean btnQuery() {

        // ====================================
        // 查詢
        // ====================================
        try {
            this.cpontVO.setShowDtVOList(
                    this.orgTrnsAssignService.queryTrnsData(
                            this.getCpontVO().getAllTrnsWorks()));

        } catch (Exception e) {
            MessagesUtils.showError("系統錯誤!" + e.getMessage());
            log.error(e.getMessage(), e);
            return false;
        }

        // ====================================
        // 頁面初始化
        // ====================================
        this.displayController.clearDtatableFilters("dlgAssignDepsTransform_Datatable");
        // 更新頁面
        this.displayController.update("dlgAssignDepsTransform_id");

        return true;
    }

    /**
     * 畫面功能:轉換選擇資料
     */
    public void btnTrnsSelected(boolean isSignle) {

        // ====================================
        // 檢核
        // ====================================
        if (WkStringUtils.isEmpty(this.cpontVO.getSelectedDtVOList())) {
            MessagesUtils.showError("未選擇單據");
            return;
        }

        // ====================================
        // 均分,避免執行太大量時, 時間過長
        // ====================================
        List<List<OrgTrnsDtVO>> lists = WkCommonUtils.averageAssign(
                this.cpontVO.getSelectedDtVOList(),
                isSignle ? 1 : this.workBackendParamHelper.findReqOrgTrnsCount());

        // ====================================
        // 轉檔
        // ====================================
        List<String> processMessages = Lists.newArrayList();

        for (List<OrgTrnsDtVO> currProcesslist : lists) {

            if (WkStringUtils.isEmpty(currProcesslist)) {
                continue;
            }

            try {
                this.orgTrnsAssignService.processTrns(
                        currProcesslist,
                        this.cpontVO.getShowDtVOList(),
                        new Date());

                processMessages.add(currProcesslist.size() + "筆執行成功");

            } catch (Exception e) {
                String msg = "執行失敗!" + e.getMessage();
                processMessages.add(WkHtmlUtils.addRedBlodClass(currProcesslist.size() + "筆" + msg));
                log.error(msg, e);
            }
        }

        entityManager.clear();
        MessagesUtils.showInfo(String.join("<br/>", processMessages));

        // ====================================
        // 重新查詢
        // ====================================
        this.btnQuery();
    }

    /**
     * 
     */
    public void btnAllTrans(List<OrgTrnsWorkVerifyVO> allTrnsWorks) {

        // ====================================
        // 查詢
        // ====================================

        try {
            log.debug("查詢前");
            this.memonyMonitor();
            List<OrgTrnsDtVO> results = this.orgTrnsAssignService.queryTrnsData(allTrnsWorks);

            this.cpontVO.setAllTrnsWorks(allTrnsWorks);
            this.cpontVO.setShowDtVOList(results);
            this.cpontVO.setSelectedDtVOList(results);

            log.debug("查詢後");
            this.memonyMonitor();

        } catch (Exception e) {
            MessagesUtils.showError("系統錯誤!" + e.getMessage());
            log.error(e.getMessage(), e);
            return;
        }

        if (WkStringUtils.isEmpty(this.cpontVO.getSelectedDtVOList())) {
            MessagesUtils.showInfo("沒有需要轉檔的資料!");
            return;
        }

        // ====================================
        // 確認視窗
        // ====================================
        String confimInfo = "待轉筆數：【%s】，確定要轉檔嗎？";
        confimInfo = String.format(
                confimInfo,
                WkHtmlUtils.addBlueBlodClass(this.cpontVO.getSelectedDtVOList().size() + ""));

        this.confirmCallbackDialogController.showConfimDialog(
                confimInfo,
                "",
                () -> this.confirmAllTrans());

    }

    private void confirmAllTrans() {
        
        HikariDataSource hikariDs  = (HikariDataSource) WorkSpringContextHolder.getBean("dataSource");
        HikariPoolMXBean poolBean = hikariDs.getHikariPoolMXBean();
        poolBean.softEvictConnections();

        // ====================================
        // 均分,避免執行太大量時, 時間過長
        // ====================================
        List<List<OrgTrnsDtVO>> lists = WkCommonUtils.averageAssign(
                this.cpontVO.getSelectedDtVOList(),
                1000);

        // ====================================
        // 轉檔
        // ====================================
        List<String> processMessages = Lists.newArrayList();
        
        log.debug("轉檔前");
        this.memonyMonitor();

        for (List<OrgTrnsDtVO> currProcesslist : lists) {

            if (WkStringUtils.isEmpty(currProcesslist)) {
                continue;
            }

            try {
                this.orgTrnsAssignService.processTrns(
                        currProcesslist,
                        this.cpontVO.getShowDtVOList(),
                        new Date());

                processMessages.add(currProcesslist.size() + "筆執行成功");

            } catch (Exception e) {
                String msg = "執行失敗!" + e.getMessage();
                processMessages.add(WkHtmlUtils.addRedBlodClass(currProcesslist.size() + "筆" + msg));
                log.error(msg, e);
            }
        }
        
        log.debug("轉檔後");
        this.memonyMonitor();

        entityManager.clear();
        MessagesUtils.showInfo(String.join("<br/>", processMessages));
    }

    private static final long MEGABYTE = 1024L * 1024L;

    public static long bytesToMegabytes(long bytes) {
        return bytes / MEGABYTE;
    }

    public void memonyMonitor() {
        Runtime runtime = Runtime.getRuntime();
        // Run the garbage collector
        runtime.gc();
        // Calculate the used memory
        long memory = runtime.totalMemory() - runtime.freeMemory();
        // System.out.println("Free memory is bytes: " + runtime.freeMemory());
        System.out.println("Free memory is megabytes: " + bytesToMegabytes(runtime.freeMemory()));
        // System.out.println("Used memory is bytes: " + memory);
        System.out.println("Used memory is megabytes: "
                + bytesToMegabytes(memory));
    }
}
