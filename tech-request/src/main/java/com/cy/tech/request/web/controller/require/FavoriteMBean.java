/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.logic.service.FavoriteService;
import com.cy.tech.request.vo.require.Favorite;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.google.common.base.Optional;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 收藏夾 控制
 *
 * @author shaun
 */
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class FavoriteMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5137866001273942196L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private FavoriteService favoritesService;

    private final static String COLOR_RED = "red";

    private String colorStr;

    private Optional<Favorite> userFavorite;

    @PostConstruct
    public void init() {
        colorStr = null;
    }

    public String findColor(Require01MBean r01MBean) {
        if (colorStr == null) {
            this.update(r01MBean.getRequire());
        }
        return colorStr;
    }

    public void update(Require require) {
        userFavorite = favoritesService.findUserFavorite(require, loginBean.getUser());
        if (!userFavorite.isPresent()) {
            colorStr = "";
            return;
        }
        Favorite f = userFavorite.get();
        if (f.getStatus().equals(Activation.ACTIVE)) {
            colorStr = COLOR_RED;
        } else {
            colorStr = "";
        }
    }

    public void toggle(Require01MBean r01MBean) {
        favoritesService.toggle(r01MBean.getRequire(), loginBean.getUser(), userFavorite);
        this.update(r01MBean.getRequire());
        r01MBean.getTitleBtnMBean().clear();
    }

}
