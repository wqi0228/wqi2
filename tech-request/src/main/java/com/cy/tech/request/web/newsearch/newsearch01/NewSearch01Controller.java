package com.cy.tech.request.web.newsearch.newsearch01;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.SpecificPermissionService;
import com.cy.tech.request.web.newsearch.BaseNewSearchController;
import com.cy.tech.request.web.newsearch.NewSearchConditionDateRangeType;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkUserWithRolesCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.viewcomponent.treepker.TreePickerCallback;
import com.cy.work.viewcomponent.treepker.TreePickerComponent;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Controller
@Scope("view")
@Slf4j
public class NewSearch01Controller extends BaseNewSearchController implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6884117663489892836L;

    // ========================================================================
    // 工具區
    // ========================================================================
    @Autowired
    private transient SpecificPermissionService spService;

    // ========================================================================
    // 查詢選項區
    // ========================================================================
    /**
     * 查詢選項:日期區間類型
     */
    @Getter
    private final List<SelectItem> selectItems_dateRangeTypes = Lists.newArrayList(
            new SelectItem(NewSearchConditionDateRangeType.ASSIGN_DATE, NewSearchConditionDateRangeType.ASSIGN_DATE.getLable()),
            new SelectItem(NewSearchConditionDateRangeType.NOTICE_DATE, NewSearchConditionDateRangeType.NOTICE_DATE.getLable()),
            new SelectItem(NewSearchConditionDateRangeType.UPDATE_DATE, NewSearchConditionDateRangeType.UPDATE_DATE.getLable()));

    // ========================================================================
    // 初始化
    // ========================================================================
    @PostConstruct
    public void init() {
        // ====================================
        // 初始化元件
        // ====================================
        // 初始化類別組合樹
        this.categoryTreePicker = new TreePickerComponent(categoryPickerCallback, "類別組合");
        // 初始化分派/通知單位樹
        this.assignNoticeDepTreePicker = new TreePickerComponent(assignNoticeDepTreeCallback, "分派通知單位");
        // 初始化轉寄部門樹
        this.forwardDepTreePicker = new TreePickerComponent(forwardDepTreeCallback, "轉寄部門");

        // ====================================
        // 查詢預設值
        // ====================================
        // 日期區間類型
        this.conditionVO.setDateRangeType(NewSearchConditionDateRangeType.ASSIGN_DATE);
    }

    // ========================================================================
    // 頁面功能
    // ========================================================================
    public void search() {

    }

    public void clear() {

    }

    // ========================================================================
    // 派工/通知部門
    // ========================================================================
    /**
     * 分派/通知部門樹
     */
    @Getter
    @Setter
    public transient TreePickerComponent assignNoticeDepTreePicker;

    /**
     * 初始化分派通知樹所需要的資料
     */
    private List<WkItem> initAssignNoticeDepTree() {

        // ====================================
        // 取得可選擇部門
        // ====================================
        Set<Integer> allCanUseDeps = Sets.newHashSet();
        // 規則1:基準部門向下所有部門
        // 先預設基準部門為自己的部門
        Integer baseDepSid = SecurityFacade.getPrimaryOrgSid();
        Org baseDep = wkOrgCache.findBySid(baseDepSid);
        if (baseDep != null) {
            // 為組時, 基準部門上升到部
            if (OrgLevel.THE_PANEL.equals(baseDep.getLevel())
                    && baseDep.getParent() != null
                    && OrgLevel.MINISTERIAL.equals(baseDep.getParent().getLevel())) {
                baseDep = baseDep.getParent().toOrg();
            }

            // 加入基準部門
            allCanUseDeps.add(baseDep.getSid());

            // 取得向下所有部門
            List<Org> childDeps = wkOrgCache.findAllChild(baseDep.getSid());
            for (Org childDep : childDeps) {
                allCanUseDeps.add(childDep.getSid());
            }
        }

        // 規則2:特殊權限的部門
        // 查詢登入者所有角色
        List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
                SecurityFacade.getUserSid(),
                SecurityFacade.getCompanyId());

        List<Org> spDepts = spService.findSpecificDeptsByRoleSidIn(roleSids);

        if (WkStringUtils.notEmpty(spDepts)) {
            for (Org spDep : spDepts) {
                allCanUseDeps.add(spDep.getSid());
            }
        }

        // ====================================
        // 計算
        // ====================================
        return this.treePickerDepHelper.prepareDepItems(
                SecurityFacade.getCompanyId(),
                Lists.newArrayList(allCanUseDeps));

    }

    /**
     * 分派通知單位選單 - 開啟選單
     */
    public void event_dialog_assignNoticeDepTree_open() {

        try {
            this.assignNoticeDepTreePicker.rebuild();
        } catch (Exception e) {
            log.error("開啟【分派通知單位】設定視窗失敗", e);
            MessagesUtils.showError("開啟【分派通知單位】設定視窗失敗");
            return;
        }

        displayController.showPfWidgetVar("wv_dlg_assignNoticeDep");
    }

    /**
     * 分派通知單位選單 - 關閉選單
     */
    public void event_dialog_assignNoticeDepTree_confirm() {

        // ====================================
        // 收集資料
        // ====================================
        // 取得元件中被選擇的項目
        this.conditionVO.setAssignSendDepSidsFromView(
                this.assignNoticeDepTreePicker.getSelectedItemIntegerSids());

        // ====================================
        // 畫面控制
        // ====================================
        displayController.hidePfWidgetVar("wv_dlg_assignNoticeDep");
    }

    /**
     * 分派通知單位選單 - callback 事件
     */
    private final TreePickerCallback assignNoticeDepTreeCallback = new TreePickerCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = 4414127323755248223L;

        @Override
        public List<WkItem> prepareAllItems() throws Exception {
            // 回傳已經算好的資料
            // initAssignNoticeDepTree();
            return initAssignNoticeDepTree();
        }

        /**
         * 準備已選擇項目
         * 
         * @return
         */
        @Override
        public List<String> prepareSelectedItemSids() throws Exception {

            // 初始化
            if (WkStringUtils.isEmpty(conditionVO.getAssignSendDepSidsFromView())) {
                conditionVO.setAssignSendDepSidsFromView(Sets.newHashSet(SecurityFacade.getPrimaryOrgSid()));
            }
            // 回傳
            return conditionVO.getAssignSendDepSidsFromView().stream().map(sid -> sid + "").collect(Collectors.toList());
        }

        @Override
        public List<String> prepareDisableItemSids() throws Exception {
            // 收集不可選的資料
            List<String> disableDepSids = Lists.newArrayList();
            for (WkItem wkItem : assignNoticeDepTreePicker.getAllItems()) {
                if (wkItem.isDisable()) {
                    disableDepSids.add(wkItem.getSid());
                }
            }
            return disableDepSids;
        }
    };

}
