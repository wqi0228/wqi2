/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.component.searchquery;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.enumerate.DateType;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.service.CategorySettingService;
import com.cy.tech.request.logic.vo.CustomerTo;
import com.cy.tech.request.logic.vo.query.search.SearchQuery;
import com.cy.tech.request.vo.category.BasicDataBigCategory;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.Search01QueryColumn;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterArrayStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterDetailVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterVO;
import com.cy.tech.request.web.listener.CategoryTreeCallBack;
import com.cy.tech.request.web.logic.helper.Search01LogicHelper;

import com.cy.work.common.enums.ReadRecordType;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.LocalDate;

/**
 * @author kasim
 */
@Slf4j
public class SearchQuery01 extends SearchQuery implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -286190791560631507L;

    /** 需求類別 */
    @Setter
    @Getter
    private BasicDataBigCategory selectBigCategory;

    /** 單位挑選 */
    @Setter
    @Getter
    private Set<Integer> requireDepts = Sets.newHashSet();

    /** 需求來源 立 | 分 | 轉 */
    @Setter
    @Getter
    private String reqSource;

    /** 需求製作進度 */
    @Setter
    @Getter
    private RequireStatusType selectRequireStatusType;

    /** 是否閱讀 */
    @Setter
    @Getter
    private ReadRecordType selectReadRecordType;

    /** 需求人員 */
    @Setter
    @Getter
    private String trCreatedUserName;

    /** 類別組合 */
    @Setter
    @Getter
    private List<String> bigDataCateSids = Lists.newArrayList();
    @Setter
    @Getter
    private List<String> middleDataCateSids = Lists.newArrayList();
    @Setter
    @Getter
    private List<String> smallDataCateSids = Lists.newArrayList();

    /** 立單區間(起) */
    @Setter
    @Getter
    private Date startDate;

    /** 立單區間(訖) */
    @Setter
    @Getter
    private Date endDate;

    /** 模糊搜尋 */
    @Setter
    @Getter
    private String fuzzyText;

    /** 廳主 */
    @Setter
    @Getter
    private List<CustomerTo> customers;

    /** 是否廳主分頁 */
    @Setter
    @Getter
    private Boolean isCustomerPage = Boolean.FALSE;

    /** 廳主(分頁條件) */
    @Setter
    @Getter
    private CustomerTo customerPage;

    /** 異動區間(起) */
    @Setter
    @Getter
    private Date startUpdatedDate;

    /** 異動區間(訖) */
    @Setter
    @Getter
    private Date endUpdatedDate;

    /** 轉發至 */
    @Setter
    @Getter
    private List<String> forwardDepts = Lists.newArrayList();

    /** 緊急度 */
    @Setter
    @Getter
    private List<String> urgencyList;

    /** 需求單號 */
    @Setter
    @Getter
    private String requireNo;

    /** 時間切換的index */
    @Setter
    @Getter
    private int dateTypeIndex;
    @Setter
    @Getter
    private CategoryTreeCallBack categoryTreeCallBack;

    @Setter
    @Getter
    private CategorySettingService categorySettingService;

    @Setter
    @Getter
    private Search01LogicHelper search01LogicHelper;

    @Setter
    @Getter
    private Search01QueryColumn dateTimeType = Search01QueryColumn.CREATE_DATE;

    /**
     * 建構式
     *
     * @param user
     * @param dep
     * @param reportType
     */
    public SearchQuery01(Org dep, User user, ReportType reportType, CategoryTreeCallBack categoryTreeCallBack,
            CategorySettingService categorySettingService, Search01LogicHelper search01LogicHelper) {
        this.dep = dep;
        this.user = user;
        this.reportType = reportType;
        this.categoryTreeCallBack = categoryTreeCallBack;
        this.categorySettingService = categorySettingService;
        this.search01LogicHelper = search01LogicHelper;
        this.init();
    }

    /**
     * 初始化
     */
    public void init() {
        initSetting();
        this.customers = Lists.newArrayList();
        this.isCustomerPage = Boolean.FALSE;
        this.customerPage = null;
        this.clearAdvance();
    }

    /** 初始化共用設定 */
    private void initSetting() {

        // 共用查詢條件初始化
        this.publicConditionInit();

        this.dateTypeIndex = reportType.getDateType().ordinal();
        this.bigDataCateSids = Lists.newArrayList();
        this.middleDataCateSids = Lists.newArrayList();
        this.smallDataCateSids = Lists.newArrayList();
        this.selectBigCategory = null;
        this.fuzzyText = null;
        this.trCreatedUserName = null;
        this.reqSource = null;
        this.selectRequireStatusType = null;
        this.selectReadRecordType = null;
        initDefCreateReqIntervalDate();
        this.requireDepts = Sets.newHashSet();
    }

    /** 載入自訂搜尋條件設定 */
    public void loadDefaultSetting(Set<Integer> defaultViewDepSids) {
        initSetting();
        // 預設選擇單位為所有相關單位,不包含可閱
        this.requireDepts = defaultViewDepSids.stream().collect(Collectors.toSet());
        try {
            List<ReportCustomFilterVO> reportCustomFilterVOs = ReportCustomFilterLogicComponent.getInstance()
                    .getReportCustomFilter(Search01QueryColumn.Search01Query, user.getSid());

            if (reportCustomFilterVOs != null && !reportCustomFilterVOs.isEmpty()) {
                ReportCustomFilterVO selReportCustomFilterVO = reportCustomFilterVOs.get(0);
                selReportCustomFilterVO.getReportCustomFilterDetailVOs().forEach(item -> {
                    if (Search01QueryColumn.DemandType.equals(item.getSearchReportCustomEnum())
                            && item instanceof ReportCustomFilterStringVO) {
                        settingDemandType(item);
                    } else if (Search01QueryColumn.DemandSource.equals(item.getSearchReportCustomEnum())
                            && item instanceof ReportCustomFilterStringVO) {
                        settingDemandSource(item);
                    } else if (Search01QueryColumn.DemandProcess.equals(item.getSearchReportCustomEnum())
                            && item instanceof ReportCustomFilterStringVO) {
                        settingDemandProcess(item);
                    } else if (Search01QueryColumn.ReadStatus.equals(item.getSearchReportCustomEnum())
                            && item instanceof ReportCustomFilterStringVO) {
                        settingReadStatus(item);
                    } else if (Search01QueryColumn.WaitReadReson.equals(item.getSearchReportCustomEnum())
                            && item instanceof ReportCustomFilterArrayStringVO) {
                        settingWaitReadReson(item);
                    } else if (Search01QueryColumn.DemandPerson.equals(item.getSearchReportCustomEnum())
                            && item instanceof ReportCustomFilterStringVO) {
                        settingDemandPerson(item);
                    } else if (Search01QueryColumn.CategoryCombo.equals(item.getSearchReportCustomEnum())
                            && item instanceof ReportCustomFilterArrayStringVO) {
                        settingCategoryCombo(item);
                    } else if (Search01QueryColumn.DateIndex.equals(item.getSearchReportCustomEnum())
                            && item instanceof ReportCustomFilterStringVO) {
                        settingDateIndex(item);
                    } else if (Search01QueryColumn.SearchText.equals(item.getSearchReportCustomEnum())
                            && item instanceof ReportCustomFilterStringVO) {
                        settingFuzzyText(item);
                    } else if (Search01QueryColumn.DateTimeType.equals(item.getSearchReportCustomEnum())
                            && item instanceof ReportCustomFilterStringVO) {
                        ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) item;
                        this.dateTimeType = Search01QueryColumn.valueOf(reportCustomFilterStringVO.getValue());
                    }
                });
            }
        } catch (Exception e) {
            log.error("loadDefaultSetting ERROR", e);
        }
    }

    private void settingDemandType(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                this.selectBigCategory = categorySettingService.findBigBySid(reportCustomFilterStringVO.getValue());
            }
        } catch (Exception e) {
            log.error("settingDemandType", e);
        }
    }

    /**
     * 立/分/轉
     * 
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandSource(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            this.reqSource = reportCustomFilterStringVO.getValue();
        } catch (Exception e) {
            log.error("settingDemandSource", e);
        }
    }

    private void settingWaitReadReson(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            this.selectReqToBeReadType.clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                this.selectReqToBeReadType.addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingDepartment", e);
        }
    }

    private void settingDemandProcess(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                this.selectRequireStatusType = RequireStatusType.valueOf(reportCustomFilterStringVO.getValue());
            }
        } catch (Exception e) {
            log.error("settingDemandSource", e);
        }
    }

    private void settingReadStatus(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                this.selectReadRecordType = ReadRecordType.valueOf(reportCustomFilterStringVO.getValue());
            }
        } catch (Exception e) {
            log.error("settingDemandSource", e);
        }
    }

    private void settingDemandPerson(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            this.trCreatedUserName = reportCustomFilterStringVO.getValue();
        } catch (Exception e) {
            log.error("trCreatedUserName", e);
        }
    }

    private void settingCategoryCombo(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            this.smallDataCateSids.clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                this.smallDataCateSids.addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
            categoryTreeCallBack.loadSelCate(smallDataCateSids);
        } catch (Exception e) {
            log.error("settingCategoryCombo", e);
        }
    }

    private void settingDateIndex(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                this.dateTypeIndex = Integer.valueOf(reportCustomFilterStringVO.getValue());
                if (DateType.PREVIOUS_MONTH.ordinal() == this.dateTypeIndex) {
                    getDateInterval(DateType.PREVIOUS_MONTH.name());
                } else if (DateType.THIS_MONTH.ordinal() == this.dateTypeIndex) {
                    getDateInterval(DateType.THIS_MONTH.name());
                } else if (DateType.NEXT_MONTH.ordinal() == this.dateTypeIndex) {
                    getDateInterval(DateType.NEXT_MONTH.name());
                } else if (DateType.TODAY.ordinal() == this.dateTypeIndex) {
                    getDateInterval(DateType.TODAY.name());
                } else if (DateType.NULL.ordinal() == this.dateTypeIndex) {
                    getDateInterval(DateType.NULL.name());
                }
            }
        } catch (Exception e) {
            log.error("settingFuzzyText", e);

        }
    }

    private void settingFuzzyText(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            this.fuzzyText = reportCustomFilterStringVO.getValue();
        } catch (Exception e) {
            log.error("settingFuzzyText", e);
        }
    }

    /**
     * 初始化 立單區間 區間
     */
    public void initDefCreateReqIntervalDate() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -1);
        this.startDate = c.getTime();
        this.endDate = new Date();
    }

    /**
     * 取得日期區間
     *
     * @param dateType
     */
    public void getDateInterval(String dateType) {
        if (DateType.PREVIOUS_MONTH.name().equals(dateType)) {
            if (startDate == null) {
                startDate = new Date();
            }
            LocalDate lastDate = new LocalDate(startDate).minusMonths(1);
            startDate = lastDate.dayOfMonth().withMinimumValue().toDate();
            endDate = lastDate.dayOfMonth().withMaximumValue().toDate();
            dateTypeIndex = DateType.PREVIOUS_MONTH.ordinal();
        } else if (DateType.THIS_MONTH.name().equals(dateType)) {
            startDate = new LocalDate().dayOfMonth().withMinimumValue().toDate();
            endDate = new LocalDate().dayOfMonth().withMaximumValue().toDate();
            dateTypeIndex = DateType.THIS_MONTH.ordinal();
        } else if (DateType.NEXT_MONTH.name().equals(dateType)) {
            if (startDate == null) {
                startDate = new Date();
            }
            LocalDate nextDate = new LocalDate(startDate).plusMonths(1);
            startDate = nextDate.dayOfMonth().withMinimumValue().toDate();
            endDate = nextDate.dayOfMonth().withMaximumValue().toDate();
            dateTypeIndex = DateType.NEXT_MONTH.ordinal();
        } else if (DateType.TODAY.name().equals(dateType)) {
            startDate = new Date();
            endDate = new Date();
            dateTypeIndex = DateType.TODAY.ordinal();
        } else if (DateType.NULL.name().equals(dateType)) {
            startDate = null;
            endDate = null;
            dateTypeIndex = DateType.NULL.ordinal();
        }
    }

    /**
     * 清除進階選項
     */
    public void clearAdvance() {
        startUpdatedDate = null;
        endUpdatedDate = null;
        urgencyList = null;
        requireNo = null;
        forwardDepts.clear();
    }

    /**
     * 是否有選擇類別資料
     *
     * @return
     */
    public boolean isSelCateSids() { return !bigDataCateSids.isEmpty() || !middleDataCateSids.isEmpty() || !smallDataCateSids.isEmpty(); }

}
