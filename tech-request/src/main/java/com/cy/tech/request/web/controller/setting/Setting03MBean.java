/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.setting;

import com.cy.work.common.utils.WkEntityUtils;
import com.cy.tech.request.logic.service.TemplateService;
import com.cy.tech.request.vo.enums.ComType;
import com.cy.tech.request.vo.template.TemplateBaseDataField;
import com.cy.tech.request.vo.template.CategoryKeyMapping;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import com.cy.tech.request.vo.template.component.ComBase;
import com.cy.tech.request.vo.value.to.TemplateComMapParserTo;
import com.cy.tech.request.vo.value.to.TemplateDefaultValueTo;
import com.cy.tech.request.web.controller.require.RequireTemplateFieldMBean;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.event.DragDropEvent;
import org.primefaces.event.ReorderEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 需求單模版與欄位設定
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope("view")
public class Setting03MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6035429898252824485L;
    /** 模版服務 */
    @Autowired
    transient private TemplateService tmpService;
    /** 物件複製輔助工具 */
    @Autowired
    transient private WkEntityUtils entityHelper;

    /** 搜尋文字 */
    @Getter
    @Setter
    private String searchText;
    /** 是否為維護 */
    @Getter
    @Setter
    private Boolean maintenance;
    /** 維護抬頭 */
    @Getter
    @Setter
    private String maintenanceHeader;
    /** 是否為維護欄位 */
    @Getter
    @Setter
    private Boolean maintenanceField;
    /** 維護欄位抬頭 */
    @Getter
    @Setter
    private String maintenanceFieldHeader;
    /** 模版鍵值列表 */
    @Getter
    @Setter
    private List<CategoryKeyMapping> viewMappingItems;
    /** 被選擇的模版鍵值 */
    @Getter
    @Setter
    private CategoryKeyMapping selectMapping;
    /** 被選擇的模版鍵值(複本) */
    @Getter
    @Setter
    private CategoryKeyMapping foucsSelectMapping;
    /** 此模版所擁有的版本列表 */
    @Getter
    @Setter
    private List<Integer> foucsMappingVersionList;
    /** 當前編輯的模版版本號碼 */
    @Getter
    @Setter
    private Integer foucsMappingVersion;
    /** 被選擇的模版欄位 */
    @Getter
    @Setter
    private FieldKeyMapping selectedMappField;
    /** 編輯中的模版欄位(複本) */
    @Getter
    @Setter
    private FieldKeyMapping editMappField;
    /** 被選中的元件列表 */
    @Getter
    @Setter
    private ComType selectComType;
    /** 模版欄位元件預設值集合體 */
    @Getter
    @Setter
    private List<TemplateComMapParserTo> templateComMapDefaultValueParserTos;
    /** 模版欄位元件互動值集合體 */
    @Getter
    @Setter
    private List<TemplateComMapParserTo> templateComMapInteractValueParserTos;
    /** 選擇的欄位模版 */
    @Getter
    @Setter
    private TemplateBaseDataField selectTemplateBaseField;
    /** 模版順序渲染寬度控制 */
    @Getter
    @Setter
    private String selectedTempMappFieldsControllRenderedWidth = "300px";

    /**
     * Rander前初始化
     */
    @PostConstruct
    public void init() {
        this.setSearchText("");
        this.searchByStatusAndTextData();
    }

    /**
     * 搜尋模版鍵值
     */
    public void searchByStatusAndTextData() {
        this.setMaintenance(Boolean.FALSE);
        this.setMaintenanceHeader("檢視");
        viewMappingItems = tmpService.findByFuzzyTextAndMaxVersion(searchText);
    }

    private RequireTemplateFieldMBean templateMBean;

    /**
     * 進入維護畫面 新增 | 編輯
     *
     * @param maintenaceHeader
     * @param templateMBean
     */
    public void startMaintenace(String maintenaceHeader, RequireTemplateFieldMBean templateMBean) {
        this.templateMBean = templateMBean;
        templateComMapDefaultValueParserTos = Lists.newArrayList();
        templateComMapInteractValueParserTos = Lists.newArrayList();
        selectComType = null;
        selectedMappField = null;
        foucsSelectMapping = selectMapping;
        //這裡開始拉版號
        foucsMappingVersionList = tmpService.findByMappingIdAllVersionList(foucsSelectMapping.getId());
        foucsMappingVersion = foucsMappingVersionList.get(0);
        this.changeMappingVersion(maintenaceHeader);
    }

    /**
     * 切換Mapping版本
     *
     * @param maintenanceHeader
     */
    public void changeMappingVersion(String maintenanceHeader) {
        foucsSelectMapping.setVersion(foucsMappingVersion);
        this.templateMBean.setTemplateItem(tmpService.loadTemplateFieldByMapping(foucsSelectMapping));
        this.templateMBean.setMapping(foucsSelectMapping);
        this.templateMBean.setComValueMap(tmpService.loadTemplateFieldComBaseMap(this.templateMBean.getTemplateItem(), templateMBean.getTriggerOnChangeEvent()));
        this.setMaintenance(Boolean.TRUE);
        this.setMaintenanceHeader(maintenanceHeader);
    }

    /**
     * 清除模版內容
     */
    public void clearTemplateValue() {
        this.templateMBean.clearAllValue();
    }

    /**
     * 開始維護欄位設定
     */
    public void startMaintenaceField() {
        maintenanceField = Boolean.TRUE;
        maintenanceFieldHeader = "編輯";
    }

    public void save() {
        try {
            this.checkInputInfo();
            tmpService.saveFieldMapping(foucsSelectMapping, this.templateMBean.getTemplateItem());
        } catch (IllegalArgumentException e) {
            log.debug(e.getMessage(), e);
            MessagesUtils.showError(e.getMessage());
            return;
        } catch (Exception e) {
            log.error("存檔失敗..." + e.getMessage(), e);
            MessagesUtils.showError(e.getMessage());
            return;
        }
        log.debug("執行存檔..." + toString());
        this.templateMBean.clearAllValue();
        this.init();
        this.selectMapping = this.foucsSelectMapping;
    }

    /**
     * 儲存前檢查<BR/>
     * 暫時想不到要檢查什麼鬼...
     */
    private void checkInputInfo() {

    }

    public void saveNewVersion() {
        try {
            this.checkInputInfo();
            Integer newVersion = this.foucsMappingVersionList.get(0) + 1;
            foucsSelectMapping.setVersion(newVersion);
            tmpService.saveByNewVersion(foucsSelectMapping, this.templateMBean.getTemplateItem());
            log.debug("執行存檔..." + toString());
            this.templateMBean.clearAllValue();
            this.init();
            this.selectMapping = this.foucsSelectMapping;
        } catch (IllegalArgumentException e) {
            log.debug(e.getMessage(), e);
            MessagesUtils.showError(e.getMessage());
        } catch (Exception e) {
            log.error("存檔失敗..." + e.getMessage(), e);
            MessagesUtils.showError(e.getMessage());
        }
    }

    /**
     * 離開維護畫面
     */
    public void exitMaintenace() {
        log.debug("exitMaintenace..." + toString());
        this.selectedMappField = null;
        this.editMappField = null;
        this.templateMBean.clearAllValue();
        this.exitMaintenaceField();
        this.setMaintenance(Boolean.FALSE);
        this.setMaintenanceHeader("檢視");
        this.selectMapping = this.foucsSelectMapping;
    }

    /**
     * 離開維護欄位畫面 (Layout South)
     */
    public void exitMaintenaceField() {
        maintenanceField = Boolean.FALSE;
        maintenanceFieldHeader = "檢視";
    }

    /**
     * 取消維護欄位內容
     */
    public void cancelMaintenaceField() {
        //還原欄位內容
        entityHelper.copyProperties(selectedMappField, editMappField);
        this.exitMaintenaceField();
    }

    /**
     * 將預設值暫存在欄位物件
     *
     */
    public void saveMaintenaceField() {
        entityHelper.copyProperties(editMappField, selectedMappField);
        //預設值
        TemplateDefaultValueTo defaultTo = tmpService.createTemplateDefaultValue(templateComMapDefaultValueParserTos);
        selectedMappField.setDefaultValue(defaultTo);
        //塞入互動預設值
        if (!Strings.isNullOrEmpty(selectedMappField.getInteractComId())) {
            TemplateDefaultValueTo interactTo = tmpService.createTemplateDefaultValue(templateComMapInteractValueParserTos);
            selectedMappField.setInteractComValue(interactTo);
        }
        ComBase com = templateMBean.getComValueMap().get(selectedMappField.getComId());
        com.setDefValue(selectedMappField);
        this.exitMaintenaceField();
    }

    /**
     * 元件列表托放加入
     *
     * @param ddEvent
     */
    public void onComTypeDrop(DragDropEvent ddEvent) {
        ComType comType = (ComType) ddEvent.getData();
        this.addComTypeToTmpMappFieldList(comType);
        selectComType = comType;
    }

    /**
     * 元件列表雙點擊加入
     */
    public void doubleClickComTypeList() {
        if (selectComType == null) {
            return;
        }
        this.addComTypeToTmpMappFieldList(selectComType);
    }

    /**
     * 1.元件列表托放加入 <BR/>
     * 2.元件列表雙點擊加入
     *
     * @param type
     */
    private void addComTypeToTmpMappFieldList(ComType type) {
        FieldKeyMapping to = tmpService.createEmptyFieldKeyMapping(foucsSelectMapping, type);
        this.templateMBean.getTemplateItem().add(to);
        ComBase comBase = tmpService.createEmptyComBaseRefByComType(type);
        comBase.setDefValue(to);
        this.templateMBean.getComValueMap().put(to.getComId(), comBase);
        selectedMappField = to;
        this.exitMaintenaceField();
        this.initSettingPanel();
    }

    /**
     * 當發生托拉Table內部元件時，發生事件<BR/>
     * 目前無托放需求
     *
     * @param event
     */
    public void onRowReorder(ReorderEvent event) {
        //event.getFromIndex() + ", To:" + event.getToIndex()
    }

    /**
     * 移除模版欄位
     *
     * @param to
     */
    public void removerTemplateField(FieldKeyMapping to) {
        if (this.templateMBean.getComValueMap().containsKey(to.getComId())) {
            this.templateMBean.getComValueMap().remove(to.getComId());
        }
        if (this.templateMBean.getTemplateItem().contains(to)) {
            this.templateMBean.getTemplateItem().remove(to);
        }
        editMappField = null;
        selectedMappField = null;
        this.exitMaintenaceField();
    }

    /**
     * 觸發模版欄位設定
     *
     */
    public void initSettingPanel() {
        this.exitMaintenaceField();
        editMappField = new FieldKeyMapping();
        entityHelper.copyProperties(selectedMappField, editMappField);
        //模版欄位預設值
        templateComMapDefaultValueParserTos = tmpService.createComMapParserDefaultValueTos(selectedMappField);
        //模版欄位互動值
        templateComMapInteractValueParserTos = tmpService.createComMapParserInteractValueTos(selectedMappField);
    }

    /**
     * 套用欄位模版
     *
     * @param templateBaseField
     */
    public void applyTemplateBaseField(TemplateBaseDataField templateBaseField) {
        entityHelper.copyProperties(templateBaseField, editMappField);
        selectTemplateBaseField = null;
    }

    /**
     * 控制模版順序寬度
     *
     * @param event
     */
    public void selectedTempMappFieldsToggleEvent(ToggleEvent event) {
        Visibility visibility = event.getVisibility();
        if (visibility.equals(Visibility.HIDDEN)) {
            selectedTempMappFieldsControllRenderedWidth = "0px";
            selectedMappField = null;
            editMappField = null;
        } else {
            selectedTempMappFieldsControllRenderedWidth = "300px";
        }
    }

}
