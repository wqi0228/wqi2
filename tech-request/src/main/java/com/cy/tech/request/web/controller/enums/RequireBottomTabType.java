/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * 需求單下方頁籤型態
 *
 * @author shaun
 */
/**
 * @author allen1214_wu
 *
 */
public enum RequireBottomTabType {

    /** 1. 轉入需求單 */
    TRANSFER_FROM_TECH,
    /** 2. 追蹤 */
    TRACE,
    /** 3. 需求單位簽核進度 */
    REQUIT_UNIT_SIGN,
    /** 4. 技術單位主管簽核進度 */
    TECH_MANAGER_SIGN,
    /** 5. 原型確認 */
    PT_CHECK_INFO,
    /** 6. 送測資訊 */
    SEND_TEST_INFO,
    /** 7. ON程式 */
    ONPG,
    /** 8. 其它設定資訊 */
    OTH_SET,
    /** 9. 分派&通知資訊 */
    ASSIGN_SEND_INFO,
    /**
     * 分派/通知資訊
     */
    //ASSIGN_INFO,
    /** 10. 轉FB記錄 */
    DELIVER_FB_REC,
    /** 11. 附加檔案 */
    ATTACHMENT,
    /** 12. 關聯群組 */
    LINK_GROUP,
    /** 13. 收呈報 */
    INBOX_REPORT,
    /** 14. 收個人 */
    INBOX_PERSONAL,
    /** 15. 收指示 */
    INBOX_INSTRUCTION,
    /** 16. 寄件備份 */
    INBOX_SEND;

    /** 對應的tab編號 */
    @Getter
    @Setter
    private int tabIdx;

}
