/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.enums;

import lombok.Getter;

/**
 * 報表切換內容類型
 *
 * @author jason_h
 */
public enum SwitchType {
    /** 切換內容 */
    CONTENT("切換內容"),
    /** 切換明細 */
    DETAIL("切換明細"),
    /** 切換全畫面內容明細 */
    FULLCONTENT("切換全畫面內容明細");

    @Getter
    private final String typeName;

    SwitchType(String typeName) {
        this.typeName = typeName;
    }

}
