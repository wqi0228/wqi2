package com.cy.tech.request.web.jsf.converter;

import com.cy.tech.request.vo.enums.RequireStatusType;
import javax.faces.convert.EnumConverter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author kasim
 */
@FacesConverter(value = "requireStatusTypeConverter")
public class RequireStatusTypeConverter extends EnumConverter {

    public RequireStatusTypeConverter() {
        super(RequireStatusType.class);
    }
}
