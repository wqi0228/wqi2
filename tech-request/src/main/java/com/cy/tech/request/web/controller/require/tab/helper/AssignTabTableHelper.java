/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require.tab.helper;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.AssignSendInfoService;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.vo.AssignTabHistoryTo;
import com.cy.tech.request.logic.vo.AssignTabInfoTo;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.require.AssignSendInfo;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 分派頁籤顯示輔助(歷程)
 *
 * @author shaun
 */
@Component
public class AssignTabTableHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8853042743833368217L;
    @Autowired
    private AssignSendInfoService asiService;
    @Autowired
    private OrganizationService orgService;

    /**
     * 建立分派資訊
     *
     * @param require
     * @return
     */
    @Transactional(readOnly = true)
    public List<AssignTabInfoTo> createTabInfo(Require require) {
        List<AssignSendInfo> assignInfo = asiService.findByRequireAndType(require, AssignSendType.ASSIGN);
        //通知部份若重新啟用，需解開以下註解 20160325 by shaun
        //List<AssignSendInfo> sendInfo = asiService.findByRequireAndType(require, AssignSendType.SEND);
        //if (assignInfo.size() != sendInfo.size()) {
        //    log.error("分派通知資料未成對...無法建立需求單分派通知頁籤資訊..." + require.getRequireNo());
        //    return Lists.newArrayList();
        //}
        return assignInfo.stream().map(assign -> {
            //通知部份若重新啟用，需解開以下註解 20160325 by shaun
            //AssignSendInfo send = sendInfo.get(assignInfo.indexOf(assign));
            //AssignSendTabTo tabTo = this.createAssignSendTabTo(assign, send);
            AssignTabInfoTo tabTo = this.createAssignSendTabTo(assign, null);
            return tabTo;
        }).collect(Collectors.toList());
    }

    /**
     * 建立分派資訊內含物件
     *
     * @param assign
     * @param send
     * @return
     */
    public AssignTabInfoTo createAssignSendTabTo(AssignSendInfo assign, AssignSendInfo send) {
        AssignTabInfoTo tabTo = new AssignTabInfoTo();
        tabTo.setCreateName(this.findUserName(assign.getCreatedUser().getSid()));
        tabTo.setCreateTime(assign.getCreatedDate());

        List<String> assignDepts = Lists.newArrayList();
        List<String> assignUsers = Lists.newArrayList();
        assignDepts.addAll(assign.getInfo().getDepartment());
        assignUsers.addAll(assign.getInfo().getUsers());
        assign.getGroupInfo().getGroups().forEach(each -> {
            assignDepts.addAll(each.getInfo().getDepartment());
            assignUsers.addAll(each.getInfo().getUsers());
        });
        //tabTo.setAssignUnit(this.createDetpStrList(assignDepts));
        //tabTo.setAssignUser(this.createUserStrList(assignUsers));
        tabTo.setAssignUnit(assignDepts);
        //tabTo.setAssignUser(assignUsers);
//通知部份若重新啟用，需解開以下註解 20160325 by shaun
//        List<String> sendDepts = Lists.newArrayList();
//        List<String> sendUsers = Lists.newArrayList();
//        sendDepts.addAll(send.getInfo().getDepartment());
//        sendUsers.addAll(send.getInfo().getUsers());
//        send.getGroupInfo().getGroups().forEach(each -> {
//            sendDepts.addAll(each.getInfo().getDepartment());
//            sendUsers.addAll(each.getInfo().getUsers());
//        });
//        //tabTo.setSendUnit(this.createDetpStrList(sendDepts));
//        //tabTo.setSendUser(this.createUserStrList(sendUsers));
//        tabTo.setSendUnitSids(sendDepts);
//        tabTo.setSendUserSids(sendUsers);
        return tabTo;
    }

    /**
     * 尋找使用者名稱
     *
     * @param sid
     * @return
     */
    private String findUserName(Integer sid) {
        User u = WkUserCache.getInstance().findBySid(sid);
        String orgStr = orgService.showParentDep(WkOrgCache.getInstance().findBySid(u.getPrimaryOrg().getSid()));
        return orgStr + "-" + u.getName();
    }

    /**
     * 建立分派歷程
     *
     * @param items
     * @return
     */
    public List<AssignTabHistoryTo> createHistoryRowInfo(List<AssignTabInfoTo> items) {
        if (items.isEmpty()) {
            return Lists.newArrayList();
        }
        String styleClz = "assign_send_row_clz";
        List<AssignTabInfoTo> parserItems = Lists.newArrayList(items);
        Collections.reverse(parserItems);//反向解析
        //當前含有部門(首筆開始記錄，以判斷後續加減派)
        List<AssignTabHistoryTo> rows = Lists.newArrayList();
        //首次分派記錄
        AssignTabInfoTo firstTabTo = parserItems.get(0);
        firstTabTo.getAssignUnit()
                .forEach(unitSid -> rows.add(0, new AssignTabHistoryTo(firstTabTo, unitSid, this.findUnitName(unitSid), "分派", "")));
        //總筆數未超過1筆時結束分析
        if (parserItems.size() == 1) {
            return rows;
        }
        //首次分派組織為前筆參考對像
        final List<String> prevUnits = firstTabTo.getAssignUnit();
        //當筆組織參考對像，迴圈索引從【1】開始
        final List<String> currUnits = Lists.newArrayList();
        for (int i = 1; i < parserItems.size(); i++) {
            String currStyleClz = i % 2 != 0 ? styleClz : "";
            AssignTabInfoTo currTabTo = parserItems.get(i);
            currUnits.clear();
            currUnits.addAll(currTabTo.getAssignUnit());
            //加派：當筆有，前筆無
            currUnits.stream().filter(currUnitSid -> !prevUnits.contains(currUnitSid))
                    .forEach(currUnitSid -> rows.add(0, new AssignTabHistoryTo(currTabTo, currUnitSid, this.findUnitName(currUnitSid), "加派", currStyleClz)));
            //減派：前筆有，當筆無
            prevUnits.stream().filter(prevUnitSid -> !currUnits.contains(prevUnitSid))
                    .forEach(prevUnitSid -> rows.add(0, new AssignTabHistoryTo(currTabTo, prevUnitSid, this.findUnitName(prevUnitSid), "減派", currStyleClz + " assign_send_row_sub_clz")));
            //當筆移轉至前筆記錄
            prevUnits.clear();
            prevUnits.addAll(currTabTo.getAssignUnit());
        }
        return rows;
    }

    private String findUnitName(String unitSid) {
        return orgService.showParentDep(WkOrgCache.getInstance().findBySid(unitSid));
    }

    /**
     * 建立分派結果
     *
     * @param tabTo
     * @return
     */
    public List<AssignTabHistoryTo> createResultRowInfo(AssignTabInfoTo tabTo) {
        return tabTo.getAssignUnit().stream()
                .map(each -> new AssignTabHistoryTo(tabTo, each, this.findUnitName(each), "", ""))
                .collect(Collectors.toList());
    }
}
