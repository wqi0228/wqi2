package com.cy.tech.request.web.pf.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Properties;

import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PomPropertiesLoader {

    private static HashMap<String, String> dataMap;

    static {
        // 初始化
        PomPropertiesLoader.dataMap = Maps.newHashMap();

        final Properties properties = new Properties();
        try {
            properties.load(PomPropertiesLoader.class.getClassLoader().getResourceAsStream("application.properties"));

            for (Entry<Object, Object> valuePair : properties.entrySet()) {
                dataMap.put(valuePair.getKey() + "", valuePair.getValue() + "");
                log.info(valuePair.getKey() + "[" + valuePair.getValue() + "]");
            }
        } catch (IOException e) {
            log.error("取得pom設定資訊失敗！", e);
        }
    }

    /**
     * @param key
     * @return
     * @throws Exception
     */
    public static String getProperty(String key) throws Exception {
        if (dataMap.containsKey(key)) {
            return dataMap.get(key);
        }
        throw new Exception("系統參數:[" + key + "] 不存在!");
    }
}
