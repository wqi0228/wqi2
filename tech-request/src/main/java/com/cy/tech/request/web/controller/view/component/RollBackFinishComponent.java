/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.component;

import com.cy.tech.request.logic.utils.ToolsDate;
import com.cy.tech.request.logic.vo.SimpleDateFormatEnum;
import com.cy.tech.request.web.controller.view.vo.RollBackFinishVO;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.listener.TabLoadCallBack;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;

/**
 * 反需求完成元件
 *
 * @author brain0925_liao
 */
public class RollBackFinishComponent implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2153911831862389247L;

    @Getter
    private RollBackFinishVO rollBackFinishVO;

    @SuppressWarnings("unused")
    private String requireSid;

    @SuppressWarnings("unused")
    private TabLoadCallBack tabLoadCallBack;

    @SuppressWarnings("unused")
    private MessageCallBack messageCallBack;

    public RollBackFinishComponent(TabLoadCallBack tabLoadCallBack, MessageCallBack messageCallBack) {
        this.rollBackFinishVO = new RollBackFinishVO();
        this.tabLoadCallBack = tabLoadCallBack;
        this.messageCallBack = messageCallBack;
    }

    public void loadData(String requireSid, String userShowName) {
        this.requireSid = requireSid;
        this.rollBackFinishVO.setAddInfo("");
        this.rollBackFinishVO.setUserName(userShowName);
        this.rollBackFinishVO.setAddTime(ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS_slide.getValue(), new Date()));

    }

}
