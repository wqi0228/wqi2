/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search.helper;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search13QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.Search13View;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.tech.request.web.controller.enums.ReqStatusMBean;
import com.cy.tech.request.web.controller.view.vo.Search13QueryVO;
import com.cy.work.common.logic.lib.manager.WorkCommonReadRecordManager;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 輔助 Search13MBean
 *
 * @author kasim
 */
@Component
public class Search13Helper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8299051637411464088L;
    @Autowired
    transient private ReqStatusMBean reqStatusUtils;
    @Autowired
    transient private SearchHelper searchHelper;
    @Autowired
    transient private UserService userService;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private Search13QueryService search13QueryService;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchResultHelper searchResultHelper;
    @Autowired
    transient private WorkCommonReadRecordManager workCommonReadRecordManager;

    /**
     * 畫面用全部製作進度查詢
     *
     * @return
     */
    public SelectItem[] getReqStatusItems() { return reqStatusUtils.buildItem(this.getAllReqStatus()); }

    /**
     * 取得此報表全部製作進度查詢
     *
     * @return
     */
    private List<RequireStatusType> getAllReqStatus() { return reqStatusUtils.createExcludeStatus(Lists.newArrayList(RequireStatusType.DRAFT)); }

    /**
     * @return
     */
    public SelectItem[] getWorkTestStatusItems() {
        List<WorkTestStatus> wrkTestStatuss = this.workTestStatusItems();
        SelectItem[] items = new SelectItem[wrkTestStatuss.size()];
        int i = 0;
        for (WorkTestStatus status : wrkTestStatuss) {
            items[i] = new SelectItem(status.name(), status.getValue());
            i++;
        }
        return items;
    }

    /**
     * 可以的WorkTestStatus選項
     *
     * @return
     */
    public List<WorkTestStatus> workTestStatusItems() {
        return Lists.newArrayList(
                WorkTestStatus.SONGCE,
                WorkTestStatus.TESTING,
                WorkTestStatus.RETEST,
                WorkTestStatus.QA_TEST_COMPLETE,
                WorkTestStatus.TEST_COMPLETE,
                WorkTestStatus.ROLL_BACK_TEST,
                WorkTestStatus.CANCEL_TEST);
    }

    public List<String> workTestStatusItemStrs() {
        return this.workTestStatusItems().stream()
                .map(each -> each.name())
                .collect(Collectors.toList());
    }

    /**
     * @param queryVO
     * @return
     */
    public List<Search13View> findWithQuery(String compId, Search13QueryVO queryVO, User user) {
        String requireNo = reqularUtils.getRequireNo(compId, queryVO.getFuzzyText());
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT "
                + "wti.testinfo_sid,"
                + "tr.require_no,"
                + "tid.field_content,"
                + "tr.urgency,"
                + "wti.create_dt,"
                + "wti.dep_sid,"
                + "wti.create_usr,"
                + "wti.testinfo_theme,"
                + "ckm.big_category_name,"
                + "ckm.middle_category_name,"
                + "ckm.small_category_name,"
                + "tr.dep_sid AS requireDep,"
                + "tr.create_usr AS requireUser,"
                + "wti.testinfo_status,"
                + "wti.read_reason,"
                + "tih.reason,"
                + "wti.testinfo_estimate_dt,"
                + "wti.testinfo_finish_dt,"
                + "wti.read_update_dt,"
                + "wti.testinfo_no,"
                + "wti.testinfo_deps,"
                + "tr.require_status,"
                + "wti.qa_audit_status,"
                + "wti.test_date,"
                + "wti.expect_online_date, "
                // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
                + this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire()
                + " FROM ");

        this.buildWorkTestInfoCondition(requireNo, queryVO, user, builder, parameters);
        this.buildRequireCondition(requireNo, queryVO, builder, parameters);
        this.buildRequireIndexDictionaryCondition(builder, parameters);
        this.buildCategoryKeyMappingCondition(requireNo, queryVO, builder, parameters);
        this.buildWorkTestInfoHistoryCondition(builder, parameters);

        // 檢查項目 (系統別)
        // 後方需對 主單 sid 做 group by
        builder.append(this.searchConditionSqlHelper.prepareSubFormCommonJoin(
                SecurityFacade.getUserSid(),
                FormType.WORKTESTSIGNINFO,
                "wti"));

        builder.append("WHERE 1=1 ");

        // 查詢條件：是否閱讀
        if (WkStringUtils.isEmpty(requireNo)) {
            builder.append(
                    this.workCommonReadRecordManager.prepareWhereConditionSQL(
                            queryVO.getSelectReadRecordType(), "readRecord"));
        }

        builder.append(" GROUP BY wti.testinfo_sid ");
        builder.append(" ORDER BY wti.test_date ASC,wti.create_dt ASC");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.WORK_TEST_INFO, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.WORK_TEST_INFO, SecurityFacade.getUserSid());

        // 查詢
        List<Search13View> resultList = search13QueryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // ====================================
        // 後續處理
        // ====================================
        if (WkStringUtils.notEmpty(resultList)) {
            // 後續處理-開始
            usageRecord.afterProcessStart();

            // 查詢條件過濾：系統別
            resultList = resultList.stream()
                    .filter(each -> this.searchResultHelper.filterCheckItems(
                            each.getCheckItemTypes(),
                            queryVO.getCheckItemTypes(),
                            false,
                            null))
                    .collect(Collectors.toList());

            // 後續處理-結束
            usageRecord.afterProcessEnd();
        }

        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return resultList;
    }

    /**
     * @param requireNo
     * @param queryVO
     * @param user
     * @param hasST_MGR
     * @param builder
     * @param parameters
     */
    private void buildWorkTestInfoCondition(
            String requireNo,
            Search13QueryVO queryVO,
            User user,
            StringBuilder builder,
            Map<String, Object> parameters) {
        builder.append("(SELECT * FROM work_test_info wti WHERE 1=1");

        // 送測區間
        if (Strings.isNullOrEmpty(requireNo) && queryVO.getStartDate() != null && queryVO.getEndDate() != null
                && Search13QueryVO.SortType.CREATE_DESC.name().equals(queryVO.getSortType())) {
            builder.append(" AND wti.test_date BETWEEN :startDate AND :endDate");
            parameters.put("startDate", searchHelper.transStartDate(queryVO.getStartDate()));
            parameters.put("endDate", searchHelper.transEndDate(queryVO.getEndDate()));
        }

        // 異動區間
        if (Strings.isNullOrEmpty(requireNo) && queryVO.getStartDate() != null && queryVO.getEndDate() != null
                && Search13QueryVO.SortType.UPDATE_DESC.name().equals(queryVO.getSortType())) {
            builder.append(" AND wti.read_update_dt BETWEEN :startDate AND :endDate");
            parameters.put("startDate", searchHelper.transStartDate(queryVO.getStartDate()));
            parameters.put("endDate", searchHelper.transEndDate(queryVO.getEndDate()));
        }

        // 技術需求單類型
        builder.append(" AND wti.testinfo_source_type='TECH_REQUEST'");

        // 填送測單的單位 或 收到送測單的單位
        builder.append(" AND (wti.dep_sid IN (:depSids) OR wti.testinfo_deps REGEXP :depSid)");
        parameters.put("depSids", queryVO.getRequireDepts() == null || queryVO.getRequireDepts().isEmpty()
                ? "nerverFind"
                : queryVO.getRequireDepts());
        parameters.put("depSid", Strings.isNullOrEmpty(this.createNoticeRegexpParam(queryVO))
                ? "nerverFind"
                : this.createNoticeRegexpParam(queryVO));

        // 填單人員
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(queryVO.getTrCreatedUserName())) {
            List<Integer> userSids = userService.findAllByLike(queryVO.getTrCreatedUserName()).stream().map(User::getSid).collect(Collectors.toList());
            builder.append(" AND wti.create_usr IN (:userSids)");
            parameters.put("userSids", userSids.isEmpty() ? "" : userSids);
        }

        // 待閱原因
        List<String> selectWaitReadReasonTypeNames = WkCommonUtils.safeStream(queryVO.getReadReason())
                .map(WaitReadReasonType::name)
                .collect(Collectors.toList());
        builder.append(
                this.searchConditionSqlHelper.prepareWhereConditionForWaitReadReason(
                        requireNo,
                        WaitReadReasonType.valueNames(FormType.WORKTESTSIGNINFO),
                        selectWaitReadReasonTypeNames,
                        WaitReadReasonType.TEST_NOT_WAITREASON.name(),
                        "wti.read_reason"));

        // 送測狀態 (為全選狀態時，不做過濾)
        if (Strings.isNullOrEmpty(requireNo)
                && WkStringUtils.notEmpty(queryVO.getTestStatus())) {
            builder.append(" AND wti.testinfo_status IN (:testStatus)");
            parameters.put("testStatus", queryVO.getTestStatus());
        }

        // 模糊搜尋
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(queryVO.getFuzzyText())) {
            String text = "%" + reqularUtils.replaceIllegalSqlLikeStr(queryVO.getFuzzyText()) + "%";
            builder.append(" AND (wti.testinfo_content LIKE :text OR wti.testinfo_theme LIKE :text OR testinfo_note LIKE :text OR ");
            builder.append(" wti.testinfo_sid IN (SELECT wtr.testinfo_sid FROM work_test_reply wtr WHERE wtr.reply_content LIKE :text) OR ");
            builder.append(" wti.testinfo_sid IN (SELECT wtar.testinfo_sid FROM work_test_already_reply wtar WHERE wtar.reply_content LIKE :text) OR ");
            builder.append(" wti.testinfo_sid IN (SELECT wtqr.testinfo_sid FROM work_test_qa_report wtqr WHERE wtqr.report_content LIKE :text) ");
            builder.append("     )");
            parameters.put("text", text);
        }
        builder.append(") AS wti ");
    }

    /**
     * @param requireNo
     * @param queryVO
     * @param builder
     * @param parameters
     */
    private void buildRequireCondition(String requireNo, Search13QueryVO queryVO, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_require tr WHERE 1=1");
        // 需求單號
        if (!Strings.isNullOrEmpty(requireNo)) {
            builder.append(" AND tr.require_no = :requireNo");
            parameters.put("requireNo", requireNo);
        }
        //////////////////// 以下為進階搜尋條件//////////////////////////////
        // 需求單-立單日期區間
        if (Strings.isNullOrEmpty(requireNo) && queryVO.getStartUpdatedDate() != null && queryVO.getEndUpdatedDate() != null) {
            builder.append(" AND tr.create_dt BETWEEN :startDateOfAdvance AND :endDateOfAdvance");
            parameters.put("startDateOfAdvance", searchHelper.transStartDate(queryVO.getStartUpdatedDate()));
            parameters.put("endDateOfAdvance", searchHelper.transEndDate(queryVO.getEndUpdatedDate()));
        }
        // 需求單號
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(queryVO.getRequireNo())) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(queryVO.getRequireNo()) + "%";
            builder.append(" AND tr.require_no LIKE :requireNo");
            parameters.put("requireNo", textNo);
        }

        // 需求類別製作進度
        builder.append(" AND tr.require_status IN (:requireStatus)");
        if (Strings.isNullOrEmpty(requireNo)) {
            parameters.put("requireStatus", this.createQueryReqStatus(queryVO.getSelectRequireStatusType()));
        } else {
            parameters.put("requireStatus", this.createQueryAllReqStatus());
        }

        builder.append(") AS tr ON tr.require_no=wti.testinfo_source_no ");
    }

    /**
     * @param builder
     * @param parameters
     */
    private void buildRequireIndexDictionaryCondition(StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_index_dictionary tid WHERE "
                + "tid.field_name='主題') AS tid ON "
                + "tr.require_sid=tid.require_sid ");
    }

    /**
     * @param requireNo
     * @param queryVO
     * @param builder
     * @param parameters
     */
    private void buildCategoryKeyMappingCondition(String requireNo, Search13QueryVO queryVO, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_category_key_mapping ckm WHERE 1=1");
        // 類別組合 (REQ-1587後. 僅認定勾選的小類-不會有大中類勾選，但底下小類不勾選的狀況)
        if (Strings.isNullOrEmpty(requireNo)
                && WkStringUtils.notEmpty(queryVO.getSmallDataCateSids())) {

            builder.append(" AND  ckm.small_category_sid IN (:smalls) ");
            parameters.put("smalls", queryVO.getSmallDataCateSids().isEmpty() ? "" : queryVO.getSmallDataCateSids());
        }
        // 需求類別
        if (Strings.isNullOrEmpty(requireNo)
                && WkStringUtils.notEmpty(queryVO.getSelectBigCategory())) {
            builder.append(" AND ckm.big_category_sid = :big ");
            parameters.put("big", queryVO.getSelectBigCategory());
        }

        builder.append(") AS ckm ON tr.mapping_sid=ckm.key_sid ");
    }

    /**
     * @param builder
     * @param parameters
     */
    private void buildWorkTestInfoHistoryCondition(StringBuilder builder, Map<String, Object> parameters) {
        builder.append("LEFT JOIN (SELECT tih.testinfo_history_sid,tih.reason,tih.testinfo_sid,MAX(tih.create_dt) "
                + "FROM work_test_info_history tih "
                + "WHERE tih.testinfo_source_type ='TECH_REQUEST' GROUP BY tih.testinfo_sid) "
                + "AS tih ON tih.testinfo_sid=wti.testinfo_sid ");
    }

    /**
     * @param queryVO
     * @return
     */
    private String createNoticeRegexpParam(Search13QueryVO queryVO) {
        return queryVO.getNoticeDepts().stream().map(each -> "\"" + each + "\"").collect(Collectors.joining("|"));
    }

    /**
     * @return
     */
    public List<WaitReadReasonType> getTestValues() {
        return Lists.newArrayList(WaitReadReasonType.values()).stream()
                .filter(type -> FormType.WORKTESTSIGNINFO.equals(type.getFormType()))
                .collect(Collectors.toList());
    }

    /**
     * 建立需求狀態查詢 List
     *
     * @param selectRequireStatusType
     * @return
     */
    private List<String> createQueryReqStatus(RequireStatusType selectRequireStatusType) {
        return reqStatusUtils.createQueryReqStatus(
                selectRequireStatusType, this.getAllReqStatus());
    }

    /**
     * 建立需求狀態查詢 List
     *
     * @return
     */
    private List<String> createQueryAllReqStatus() {
        return this.getAllReqStatus().stream()
                .map(each -> each.name())
                .collect(Collectors.toList());
    }

}
