/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.logic.component;

import com.cy.work.backend.logic.WorkCalendarHelper;
import java.io.Serializable;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 工作日邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WorkCalendarLogicComponent implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2514448745735845796L;
    private static WorkCalendarLogicComponent instance;
    @Autowired
    private WorkCalendarHelper workCalendarHelper;

    public static WorkCalendarLogicComponent getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WorkCalendarLogicComponent.instance = this;
    }

    /**
     * 取得該日期 - X 個工作天日期
     *
     * @param date
     * @param totalCount
     * @return
     */
    public Date getSpecialWorkDay(Date date, int totalCount) {
        try {
            int count = 0;
            Date finalDate = date;
            do {
                count = count + 1;
                finalDate = getWorkDay(new LocalDate(finalDate).minusDays(1).toDate());
            } while (count < totalCount);
            return finalDate;
        } catch (Exception e) {
            log.error("getSpecialWorkDay ERROR", e);
        }
        return date;
    }

    private Date getWorkDay(Date date) {
        if (workCalendarHelper.isHoilday(date)) {
            LocalDate preDate = new LocalDate(date).minusDays(1);
            return getWorkDay(preDate.toDate());
        }
        return date;
    }

}
