/**
 * 
 */
package com.cy.tech.request.web.controller.setting.setting10.muti;

import java.io.Serializable;

import com.cy.commons.util.thread.CommonsCallableResultTo;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author allen1214_wu
 */
@AllArgsConstructor
public class RequireDepRebuildCallableVO implements CommonsCallableResultTo, Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 6350687525161802834L;

    private String requireSid;
    @Getter
    private String errorMessage;

    @Override
    public String getCallableKey() { return this.requireSid; }

}
