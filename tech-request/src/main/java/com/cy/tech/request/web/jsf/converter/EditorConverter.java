/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.jsf.converter;

import com.cy.work.common.utils.WkJsoupUtils;
import com.google.common.base.Strings;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.primefaces.component.editor.Editor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.util.HtmlUtils;

/**
 *
 * @author kasim
 */
@Component("editorConverter")
public class EditorConverter implements Converter {

    @Autowired
    transient private WkJsoupUtils jsoupUtils;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (Strings.isNullOrEmpty(value)) {
            return null;
        }
        if (value instanceof String && component instanceof Editor) {
            return jsoupUtils.removeRegexpByEditor(value);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (component instanceof Editor && value instanceof String) {
            return HtmlUtils.htmlEscape((String) value);
        }
        return null;
    }

}
