/**
 * 
 */
package com.cy.tech.request.web.controller.setting.setting10;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.tech.request.logic.service.onetimetrns.TrnsDepRequireConfirmStatusService;
import com.cy.tech.request.logic.vo.onetimetrns.TrnsDepRequireConfirmStatusVO;
import com.cy.tech.request.vo.enums.ReqConfirmDepCompleteType;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.web.controller.setting.Setting10MBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 *
 */
@Slf4j
@Controller
@Scope("view")
public class Set10V70TrnsDepRequireConfirmStatusMBean implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 5601614126191297928L;
    @Autowired
	transient private Setting10MBean set10Bean;
	@Autowired
	transient private TrnsDepRequireConfirmStatusService trnsDepRequireConfirmStatusService;
	@Autowired
	transient private DisplayController displayController;

	// ========================================================================
	// view 變數
	// ========================================================================
	/**
	 * 檔案名稱
	 */
	@Getter
	public String fileName;
	/**
	 * 
	 */
	@Getter
	public String contentType;
	/**
	 * 
	 */
	@Getter
	public String fileSize;

	/**
	 * 
	 */
	@Getter
	@Setter
	public String textContent;

	/**
	 * 
	 */
	@Getter
	public List<TrnsDepRequireConfirmStatusVO> trnsDatas;

	@Getter
	public boolean uploadDone = false;

	// ========================================================================
	// 方法區
	// ========================================================================
	@PostConstruct
	public void init() {
	}

	/**
	 * 轉檔
	 */
	public void trnsData() {

		this.set10Bean.clearInfoScreenMsg();
		this.uploadDone = false;

		// ====================================
		// 檢核
		// ====================================
		if (WkStringUtils.isEmpty(this.trnsDatas)) {
			MessagesUtils.showWarn("沒有可轉檔的資料 (未上傳檔案?)");
			return;
		}

		// ====================================
		// 開始處理
		// ====================================
		try {

			String logMessage = "開始處理轉檔!";
			this.set10Bean.addInfoMsg("\r\n" + logMessage);
			log.info(logMessage);
			Long startTime = System.currentTimeMillis();

			// 處理
			String result = this.trnsDepRequireConfirmStatusService.process(trnsDatas);
			this.set10Bean.addInfoMsg(result);

			logMessage = WkCommonUtils.prepareCostMessage(startTime, "總耗時");
			log.info(logMessage);
			this.set10Bean.addInfoMsg("\r\n" + logMessage);
		} catch (Exception e) {
			this.set10Bean.addInfoMsg("\r\n執行錯誤!");
			MessagesUtils.showError("執行錯誤!");
			log.error("執行錯誤", e);
			return;
		}

		this.set10Bean.addInfoMsg("\r\n轉檔完成");
		MessagesUtils.showInfo("轉檔完成");
	}

	public void handleFileUpload(FileUploadEvent event) {

		this.displayController.update("setting10_TrnsDepRequireConfirmStatus_btn");
		this.uploadDone = false;

		// ====================================
		// 取得上傳檔案
		// ====================================
		this.initFileInfo();
		UploadedFile uploadFile = event.getFile();

		if (uploadFile == null) {
			MessagesUtils.showError("上傳檔案為 null");
			return;
		}
		if (uploadFile.getSize() == 0) {
			MessagesUtils.showError("上傳檔案無內容");
			return;
		}

		if (!"text/plain".equals(uploadFile.getContentType())) {
			MessagesUtils.showError("檔案類型錯誤, 需上傳純文字檔");
			return;
		}

		// ====================================
		// byte to text
		// ====================================
		this.prepareFileInfo(uploadFile);

		try {
			this.textContent = new String(uploadFile.getContents(), "BIG5");
		} catch (UnsupportedEncodingException e) {
			MessagesUtils.showError("內容解碼錯誤!" + e.getMessage());
			log.error("內容解碼錯誤!", e);
			return;
		}

		// ====================================
		// text to vo
		// ====================================
		try {
			this.parserTextContent();
		} catch (UserMessageException e) {
			MessagesUtils.show(e);
			return;
		}

		this.uploadDone = true;

	}

	private void parserTextContent() throws UserMessageException {

		this.trnsDatas = Lists.newArrayList();

		Org comp = WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId());
		if (comp == null) {
			MessagesUtils.showError("找不到登入者公司!");
			return;
		}

		// 統一斷行符號
		String tempText = this.textContent.replaceAll("\r\n", "\r");
		int lineNum = 0;
		for (String lineText : tempText.split("\r")) {

			// ====================================
			// 檢查單行資料
			// ====================================
			if (WkStringUtils.isEmpty(lineText)) {
				log.info("ˊ傳入檔案第{}行資料為空", lineNum);
				continue;
			}
			lineNum++;
			// 去空白
			lineText = WkStringUtils.safeTrim(lineText);

			// 以『分隔符號 \t』進行資料解析
			String[] colTexts = lineText.split("\t");
			if (colTexts.length < 2) {
				throw new UserMessageException(
				        "第" + lineNum + "行資料,欄位數不足!【" + lineText + "】",
				        InfomationLevel.WARN);
			}

			// ====================================
			// 解析欄位資料
			// ====================================
			TrnsDepRequireConfirmStatusVO vo = new TrnsDepRequireConfirmStatusVO();
			this.trnsDatas.add(vo);

			// 需求單號TGTR20181225037
			String requireNo = WkStringUtils.safeTrim(colTexts[0]);
			if (requireNo.length() != 15 || !requireNo.startsWith("TGTR")) {
				throw new UserMessageException(
				        "第" + lineNum + "行資料,需求單號格式錯誤!【" + requireNo + "】",
				        InfomationLevel.WARN);
			}
			vo.setRequireNo(requireNo);

			// 單位
			String importDepName = WkStringUtils.safeTrim(colTexts[1]);
			List<Org> deps = WkOrgUtils.findDepByNameLikeAndActive(comp.getSid(), importDepName);
			// 名稱需全文符合
			deps = deps.stream()
			        .filter(org -> (org.getName() + "").equals(importDepName))
			        .collect(Collectors.toList());

			if (WkStringUtils.isEmpty(deps)) {
				throw new UserMessageException(
				        "第" + lineNum + "行資料,單位名稱找不到符合資料!【" + importDepName + "】",
				        InfomationLevel.WARN);
			}
			if (deps.size() > 1) {
				throw new UserMessageException(
				        "第" + lineNum + "行資料,單位名稱有重複資料, 無法辨識是那個單位!【" + importDepName + "】",
				        InfomationLevel.WARN);
			}
			vo.setImportDepName(importDepName);
			vo.setDepSid(deps.get(0).getSid());
			vo.setCheckDepDesc(
			        WkOrgUtils.prepareBreadcrumbsByDepNameAndMakeup(
			                deps.get(0).getSid(),
			                OrgLevel.DIVISION_LEVEL,
			                false, "-"));

			// 狀態
			if (colTexts.length > 2) {
				String importStatus = WkStringUtils.safeTrim(colTexts[2]);

				ReqConfirmDepCompleteType trnsToCompleteType = null;
				if (WkStringUtils.isEmpty(importStatus)) {
					trnsToCompleteType = null;
				} else if ("自行領單".equals(importStatus)) {
					trnsToCompleteType = null;
				} else if ("無須處理".equals(importStatus)) {
					trnsToCompleteType = ReqConfirmDepCompleteType.WONT_DO;
				} else if ("已執行/確認".equals(importStatus)) {
					trnsToCompleteType = ReqConfirmDepCompleteType.FINISH;
				} else {
					throw new UserMessageException(
					        "第" + lineNum + "行資料,『狀態』欄位資料無法解析!【" + importStatus + "】",
					        InfomationLevel.WARN);
				}

				vo.setImportStatus(importStatus);
				vo.setTrnsToCompleteType(trnsToCompleteType);
			}

		}

	}

	private void initFileInfo() {
		this.fileName = "";
		this.contentType = "";
		this.fileSize = "";
		this.textContent = "";
		this.trnsDatas = Lists.newArrayList();
	}

	private void prepareFileInfo(UploadedFile uploadFile) {
		if (uploadFile == null) {
			initFileInfo();
			return;
		}
		this.fileName = uploadFile.getFileName();
		this.contentType = uploadFile.getContentType();
		double size = uploadFile.getSize();
		this.fileSize = size + "byte";
	}
}
