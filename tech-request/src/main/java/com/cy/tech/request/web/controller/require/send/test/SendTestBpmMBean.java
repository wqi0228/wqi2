/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require.send.test;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.tech.request.logic.service.send.test.SendTestBpmService;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.vo.value.to.BpmTaskTo;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 功能按鍵顯示處理
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class SendTestBpmMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7337870960185300220L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private SendTestBpmService stBpmService;
    @Autowired
    transient private DisplayController display;

    /** 暫存物件 - 退回 | 作廢 用 */
    @Getter
    private WorkTestInfo refreTestInfo;

    @Getter
    private SelectItem[] rollbackItems;

    /** 選擇的退回物件 */
    @Getter
    @Setter
    private Integer rollbackSelectIndex;

    /** 退回理由 */
    @Getter
    @Setter
    private String rollbackComment;

    /** 作廢追蹤理由 */
    @Getter
    @Setter
    private String invaildTraceReason;

    /**
     * 顯示按鈕 - 簽名
     * @param testInfo
     * @return
     */
    public boolean showSignBtn(WorkTestInfo testInfo) {
        return stBpmService.showSignBtn(testInfo, loginBean.getUser());
    }

    /**
     * 顯示按鈕 - 退回
     * @param testInfo
     * @return
     */
    public boolean showRollBackBtn(WorkTestInfo testInfo) {
        return stBpmService.showRollBackBtn(testInfo, loginBean.getUser());
    }

    /**
     * 顯示按鈕 - 復原
     * @param testInfo
     * @return
     */
    public boolean showRecoveryBtn(WorkTestInfo testInfo) {
        return stBpmService.showRecoveryBtn(testInfo, loginBean.getUser());
    }

    /**
     * 作廢
     * @param testInfo
     * @return
     */
    public boolean showInvaildBtn(WorkTestInfo testInfo) {
        return stBpmService.showInvaildBtn(testInfo, loginBean.getUser());
    }

    /**
     * 簽名
     * @param r01MBean
     * @param testInfo
     */
    public void clickSign(Require01MBean r01MBean, WorkTestInfo testInfo) {
        try {
            stBpmService.doSign(testInfo, loginBean.getUser(), "");
            r01MBean.getTitleBtnMBean().clear();
            if (testInfo.getSignInfo().getInstanceStatus().equals(InstanceStatus.APPROVED)) {
                r01MBean.getTraceMBean().clear();
            }
        } catch (Exception e) {
            log.error(testInfo.getTestinfoNo() + ":" + loginBean.getUser().getName() + ":簽核失敗..." + e.getMessage(), e);
            r01MBean.getSendTestBottomMBean().recoveryView(r01MBean, testInfo);
            MessagesUtils.showError(e.getMessage());
        }
    }

    public void clickRecovery(Require01MBean r01MBean, WorkTestInfo testInfo) {
        try {
            stBpmService.doRecovery(testInfo, loginBean.getUser());
        } catch (Exception e) {
            log.error(testInfo.getTestinfoNo() + ":" + loginBean.getUser().getName() + ":復原失敗..." + e.getMessage(), e);
            r01MBean.getSendTestBottomMBean().recoveryView(r01MBean, testInfo);
            MessagesUtils.showError(e.getMessage());
        }
    }

    /**
     * 執行退回前判斷
     *
     * @param testInfo
     */
    public void doRollBackBefor(WorkTestInfo testInfo) {
        BpmTaskTo taskTo = testInfo.getSignInfo().getTaskTo();
        rollbackComment = "";
        List<ProcessTaskBase> copy = Lists.newArrayList(taskTo.getTasks());
        copy.remove(copy.size() - 1);
        rollbackItems = new SelectItem[copy.size()];
        int index = 0;
        for (ProcessTaskBase each : copy) {
            String label = each.getTaskName().contains("申請人") ? "申請人" : each.getRoleName();
            SelectItem item = new SelectItem(taskTo.getTasks().indexOf(each), label);
            rollbackItems[index++] = item;
        }
        refreTestInfo = testInfo;
    }

    public void clickRollBack(Require01MBean r01MBean) {
        try {
            BpmTaskTo taskTo = refreTestInfo.getSignInfo().getTaskTo();
            stBpmService.doRollBack(refreTestInfo, loginBean.getUser(), (ProcessTaskHistory) taskTo.getTasks().get(rollbackSelectIndex), rollbackComment);
            
            if(r01MBean.getSendTestBottomMBean().isLoadWorkTestInfoByUrlParam()) {
                r01MBean.getSendTestBottomMBean().reloadWorkTestInfo(refreTestInfo.getTestinfoNo());
            } else {
                List<WorkTestInfo> infos = r01MBean.getSendTestBottomMBean().findTestInfos(r01MBean);
                display.execute("PF('st_acc_panel_layer_zero').select(" + infos.indexOf(refreTestInfo) + ")");
            }
        } catch (Exception e) {
            log.error(refreTestInfo.getTestinfoNo() + ":" + loginBean.getUser().getName() + ":退回失敗..." + e.getMessage(), e);
            r01MBean.getSendTestBottomMBean().recoveryView(r01MBean, refreTestInfo);
            MessagesUtils.showError(e.getMessage());
        }
    }

    /**
     * 執行作廢前清空理由
     *
     * @param testInfo
     */
    public void initOpenInvaildDialog(WorkTestInfo testInfo) {
        this.invaildTraceReason = "";
        refreTestInfo = testInfo;
    }

    public void clickInvaild(Require01MBean r01MBean) {
        try {
            stBpmService.doInvaild(r01MBean.getRequire(), refreTestInfo, loginBean.getUser(), invaildTraceReason);
            r01MBean.getTraceMBean().clear();
            
            if(r01MBean.getSendTestBottomMBean().isLoadWorkTestInfoByUrlParam()) {
                r01MBean.getSendTestBottomMBean().reloadWorkTestInfo(refreTestInfo.getTestinfoNo());
            } else {
                List<WorkTestInfo> infos = r01MBean.getSendTestBottomMBean().findTestInfos(r01MBean);
                display.execute("PF('st_acc_panel_layer_zero').select(" + infos.indexOf(refreTestInfo) + ")");
            }
        } catch (Exception e) {
            log.error(refreTestInfo.getTestinfoNo() + ":" + loginBean.getUser().getName() + ":作廢失敗..." + e.getMessage(), e);
            r01MBean.getSendTestBottomMBean().recoveryView(r01MBean, refreTestInfo);
            MessagesUtils.showError(e.getMessage());
        }
    }

    public void assignRefre(WorkTestInfo testInfo) {
        refreTestInfo = testInfo;
    }

}
