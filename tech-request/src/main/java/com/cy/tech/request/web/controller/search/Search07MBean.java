/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.omnifaces.util.Faces;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.enums.Search07ConditionType;
import com.cy.tech.request.logic.search.service.ReqConfirmDepSearchHelper;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search07QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.Search07View;
import com.cy.tech.request.logic.service.CategorySettingService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.URLService.URLServiceAttr;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepService;
import com.cy.tech.request.logic.utils.DateUtils;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.logic.vo.RequireConfirmDepVO;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.ReqConfirmDepProgStatus;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.enums.Search07QueryColumn;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.common.SelectItemBean;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.searchheader.CommonHeaderMBean;
import com.cy.tech.request.web.controller.searchheader.Search07HeaderMBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.view.component.searchquery.SearchQuery07;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.logic.lib.manager.WorkCommonReadRecordManager;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.viewcomponent.treepker.TreePickerCallback;
import com.cy.work.viewcomponent.treepker.TreePickerComponent;
import com.cy.work.viewcomponent.treepker.helper.TreePickerDepHelper;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 已分派單據查詢
 *
 * @author jason_h
 */
@Controller
@Scope("view")
@Slf4j
public class Search07MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6156730521491162124L;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private Search07QueryService search07QueryService;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private DisplayController displayController;
    @Autowired
    transient private ReportCustomFilterLogicComponent reportCustomFilterLogicComponent;
    @Autowired
    transient private CategorySettingService categorySettingService;
    @Autowired
    transient private UserService userService;
    @Autowired
    transient private TreePickerDepHelper treePickerDepHelper;
    @Autowired
    transient private RequireConfirmDepService requireConfirmDepService;
    @Autowired
    transient private SelectItemBean selectItemBean;
    @Autowired
    transient private Search07HeaderMBean search07HeaderMBean;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchResultHelper searchResultHelper;
    @Autowired
    transient private ReqConfirmDepSearchHelper reqConfirmDepSearchHelper;
    @Autowired
    transient private WorkCommonReadRecordManager workCommonReadRecordManager;

    /** 所有的需求單 */
    @Getter
    @Setter
    private List<Search07View> queryItems;
    /** 選擇的需求單 */
    @Getter
    @Setter
    private Search07View querySelection;
    /** 上下筆移動keeper */
    @Getter
    private Search07View queryKeeper;
    /** 切換模式 */
    @Getter
    private SwitchType switchType = SwitchType.CONTENT;
    /** 切換模式 - 全畫面狀態 */
    @Getter
    private SwitchType switchFullType = SwitchType.DETAIL;
    /** 在匯出的時候，某些內容需要隱藏 */
    @Getter
    private boolean hasDisplay = true;
    @Getter
    private final String dataTableId = "dtRequire";
    private final String NO_READ_REASON = "";
    @Getter
    private SearchQuery07 search07QueryComponent;

    @PostConstruct
    public void init() {
        queryItems = Lists.newArrayList();
        this.initComponent();
        displayController.execute("doSearchData()");
        // this.search();
    }

    private void initComponent() {
        search07QueryComponent = new SearchQuery07(loginBean.getCompanyId(), helper,
                this.userService,
                reportCustomFilterLogicComponent,
                categorySettingService,
                messageCallBack,
                displayController,
                loginBean.getUserSId());

        search07QueryComponent.initSetting();
        commHeaderMBean.clearAdvance();
        search07QueryComponent.loadSetting();

        // 初始化分派/通知單位樹
        this.assignNoticeDepTreePicker = new TreePickerComponent(assignNoticeDepTreeCallback, "分派/通知單位");
        this.customerFilterAssignNoticeDepTreePicker = new TreePickerComponent(customerFilterAssignNoticeDepTreeCallback, "自訂搜尋條件 - 分派/通知單位");

        // ====================================
        // 由首頁待辦事項連過來的-預設帶查詢條件
        // ====================================
        this.prepareDefaultConditionByParam();
    }

    /**
     * 由 portal 待辦事項 - 連過來時的『預設查詢條件』
     */
    private void prepareDefaultConditionByParam() {
        // ====================================
        // 解析查詢類別 （首頁）
        // ====================================
        // 取得傳入參數
        String paramValue = Faces.getRequestParameterMap().get(URLServiceAttr.URL_ATTR_SEARCH07_CONDITION_TYPE.getAttr());
        // 解析類別
        Search07ConditionType type = Search07ConditionType.parseVal(paramValue);
        if (type == null) {
            return;
        }

        // ====================================
        // 設定預設查詢條件
        // ====================================
        // 清除預設查詢條件
        search07QueryComponent.initSetting();
        commHeaderMBean.clearAdvance();

        // 有查詢條件時, 預設清空查詢日期
        search07QueryComponent.getSearch07QueryVO().setStartDate(null);
        search07QueryComponent.getSearch07QueryVO().setEndDate(null);

        switch (type) {
        case NOT_FINISH_BY_DEP:
            // ====================================
            // 部門未完成需求
            // ====================================
            // 需求確認狀態：進行中
            search07QueryComponent.getSearch07QueryVO().setReqConfirmDepProgStatus(ReqConfirmDepProgStatus.IN_PROCESS);
            // 排除已結案
            search07QueryComponent.getSearch07QueryVO().setSelectedColseCode(Lists.newArrayList("0"));
            break;
        case NOT_FINISH_BY_SELF:
            // ====================================
            // 個人未完成需求
            // ====================================
            // 需求確認狀態：進行中
            search07QueryComponent.getSearch07QueryVO().setReqConfirmDepProgStatus(ReqConfirmDepProgStatus.IN_PROCESS);
            // 負責人為自己
            search07QueryComponent.getSearch07QueryVO().setOwnerName(WkUserUtils.findNameBySid(SecurityFacade.getUserSid()));
            // 排除已結案
            search07QueryComponent.getSearch07QueryVO().setSelectedColseCode(Lists.newArrayList("0"));
            break;
        case ASSIGN:
            // ====================================
            // 已分派
            // ====================================
            // 由首頁過來時 ,預設需閱讀
            search07QueryComponent.getSearch07QueryVO().setSelectReadRecordType(ReadRecordType.UN_READ);
            // 排除已結案
            search07QueryComponent.getSearch07QueryVO().setSelectedColseCode(Lists.newArrayList("0"));
            break;
        default:
            break;
        }
    }

    public void search() {

        queryItems.clear();
        try {
            queryItems.addAll(this.findWithQuery());
        } catch (Exception e) {
            String msg = "查詢失敗！[" + e.getMessage() + "]";
            MessagesUtils.showError(msg);
            log.error(msg, e);
        }
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        search07QueryComponent.initSetting();
        commHeaderMBean.clearAdvance();
        // search07QueryComponent.loadSetting();
        this.search();
    }

    /**
     * 載入預設搜尋條件
     */
    public void loadCustSetting() {
        search07QueryComponent.initSetting();
        commHeaderMBean.clearAdvance();
        search07QueryComponent.loadSetting();
        this.search();
    }

    private List<Search07View> findWithQuery() throws UserMessageException {

        // ====================================
        // 準備查詢參數
        // ====================================
        // 需求單號
        String requireNo = reqularUtils.getRequireNo(
                loginBean.getCompanyId(),
                search07QueryComponent.getSearch07QueryVO().getFuzzyText());

        // ====================================
        // 以『確認單位檔 RequireConfirmDep』的角度，查詢和使用者相關的求單 (RequireConfirmDep)
        // ====================================
        // 查詢
        List<RequireConfirmDepVO> requireConfirmDepVOs = this.reqConfirmDepSearchHelper.prepareRequireConfirmDepByLoginUserRelation(
                search07QueryComponent.getSearch07QueryVO().getReqConfirmDepProgStatus(),
                search07QueryComponent.getSearch07QueryVO().getAssignDepSids(),
                SecurityFacade.getUserSid(),
                SecurityFacade.getCompanyId(),
                search07QueryComponent.getSearch07QueryVO().getOwnerName(),
                false,
                true);

        if (WkStringUtils.isEmpty(requireConfirmDepVOs)) {
            return Lists.newArrayList();
        }

        // 收集 require sid
        List<String> requireSids = requireConfirmDepVOs.stream()
                .map(RequireConfirmDepVO::getRequireSid)
                .distinct()
                .collect(Collectors.toList());

        log.debug("requireSids:[{}]筆", requireSids.size());

        if (WkStringUtils.isEmpty(requireSids)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 兜組查詢 SQL
        // ====================================
        Map<String, Object> parameters = Maps.newHashMap();
        String querySql = this.prepareQuerySQL(parameters, requireNo, requireSids);

        // ====================================
        // 查詢
        // ====================================
        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.ASSIGN, querySql, parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.ASSIGN, SecurityFacade.getUserSid());

        List<Search07View> results = search07QueryService.findWithQueryByMultiThread(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(querySql), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        if (WkStringUtils.notEmpty(results)) {

            // 後續處理-開始
            usageRecord.afterProcessStart();

            // ====================================
            // 查詢條件過濾：系統別
            // ====================================

            List<RequireCheckItemType> filterCheckItemTypes = search07QueryComponent.getSearch07QueryVO().getCheckItemTypes();
            // 全不選時，全選時 皆不過濾
            if (WkStringUtils.notEmpty(filterCheckItemTypes)
                    && filterCheckItemTypes.size() != RequireCheckItemType.values().length) {
                results = results.stream()
                        .filter(each -> this.searchResultHelper.filterCheckItems(
                                each.getCheckItemTypes(),
                                search07QueryComponent.getSearch07QueryVO().getCheckItemTypes(),
                                false,
                                null))
                        .collect(Collectors.toList());

            }

            // ====================================
            // 組【需求確認單位】欄位資訊
            // ====================================
            // 取得查詢結果單據的分派單位檔 （包含非責任部門）
            requireSids = results.stream()
                    .map(Search07View::getSid)
                    .distinct()
                    .collect(Collectors.toList());
            Map<String, List<RequireConfirmDepVO>> requireConfirmDepVOMapByRequireSid = this.requireConfirmDepService.queryByReqSidsForNewsearch03(requireSids);

            // 取得與登入者相關的部門 (責任部門)
            List<Integer> matchDepSids = this.requireConfirmDepService.prepareUserMatchDepSids(SecurityFacade.getUserSid());
            // 取得全部部門的排序
            Map<Integer, Integer> orgSortNumberMapByDepSid = WkOrgCache.getInstance().findOrgSortNumberMapByDepSid();
            for (Search07View search07View : results) {
                if (requireConfirmDepVOMapByRequireSid.containsKey(search07View.getSid())) {
                    // 注入【需求確認單位】資料
                    search07View.setRequireConfirmDepVOs(requireConfirmDepVOMapByRequireSid.get(search07View.getSid()));
                    // 組【需求確認單位】欄位資訊
                    this.prepareReqConfirmDepInfo(
                            search07View,
                            matchDepSids,
                            orgSortNumberMapByDepSid);
                }
            }

            // ====================================
            // 排序
            // ====================================
            // 為被分派部門排在最上面
            Comparator<Search07View> comparator = Comparator.comparing(Search07View::isAssignDep).reversed();
            // 立案日期反向排序
            comparator = comparator.thenComparing(Comparator.comparing(Search07View::getAssignDate).reversed());

            // 排序
            results = results.stream()
                    .sorted(comparator)
                    .collect(Collectors.toList());

            // 後續處理-結束
            usageRecord.afterProcessEnd();

        }

        // 使用記錄
        usageRecord.saveUsageRecord();

        return results;

    }

    private String prepareQuerySQL(
            Map<String, Object> parameters,
            String requireNo,
            List<String> requireSids) {

        StringBuilder builder = new StringBuilder();
        // ====================================
        // SELECT
        // ====================================
        builder.append("SELECT "
                + "tr.require_sid,"
                + "tr.require_no,"
                + "tid.field_content,"
                + "tr.urgency,"
                + "tr.create_dt,"
                + "ckm.big_category_name,"
                + "ckm.middle_category_name,"
                + "ckm.small_category_name,"
                + "tr.dep_sid,"
                + "tr.create_usr,"
                + "tr.require_suspended_code,"
                + "tr.require_status,"
                + "tr.read_reason,"
                + "tr.require_finish_code,"
                + "tr.hope_dt,"
                + "tr.inte_staff,"
                + "assi_assign.info assignInfo, "
                + "assi_assign.create_dt assignDate,"
                + "tr.update_dt, "
                + "tr.require_establish_dt, "
                + "tr.require_finish_dt, "
                // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
                + this.searchConditionSqlHelper.prepareCommonSelectColumnByRequireType2());

        // ====================================
        // FROM、JOIN
        // ====================================
        builder.append("  FROM  ");

        this.buildRequireCondition(requireSids, requireNo, builder, parameters);

        // 模糊搜尋
        this.buildRequireIndexDictionaryCondition(requireNo, builder, parameters);

        // 類別
        this.buildCategoryKeyMappingCondition(requireNo, builder, parameters);

        // 分派部門
        this.buildAssignInfoConfition_Join(builder, parameters);

        // 閱讀記錄
        builder.append(searchConditionSqlHelper.prepareSubFormReadRecordJoin(
                SecurityFacade.getUserSid(), FormType.REQUIRE, "tr"));

        // ====================================
        // WHERE
        // ====================================
        builder.append(" WHERE tr.require_sid IS NOT NULL ");

        // 查詢條件：被分派單位
        List<Integer> assignDepSids = search07QueryComponent.getSearch07QueryVO().getAssignDepSids();
        builder.append("AND " + this.prepareDepSql(Sets.newHashSet(assignDepSids)) + " ");

        // 查詢條件：是否閱讀
        if (WkStringUtils.isEmpty(requireNo)) {
            ReadRecordType readRecordType = search07QueryComponent.getSearch07QueryVO().getSelectReadRecordType();
            builder.append(
                    this.workCommonReadRecordManager.prepareWhereConditionSQL(
                            readRecordType, "readRecord"));
        }

        // 查詢條件：區間
        if (Strings.isNullOrEmpty(requireNo)) {
            if (Search07QueryColumn.ASSIGN_DATE.equals(search07QueryComponent.getSearch07QueryVO().getDateTimeType())) {
                Date startDate = DateUtils.convertToOriginOfDay(search07QueryComponent.getSearch07QueryVO().getStartDate());
                Date endDate = DateUtils.convertToEndOfDay(search07QueryComponent.getSearch07QueryVO().getEndDate());
                if (startDate != null && endDate != null) {
                    builder.append(" AND assi_assign.create_dt BETWEEN :startDate AND :endDate ");
                    parameters.put("startDate", startDate);
                    parameters.put("endDate", endDate);
                } else if (startDate != null) {
                    builder.append(" AND assi_assign.create_dt >= :startDate  ");
                    parameters.put("startDate", startDate);
                } else if (endDate != null) {
                    builder.append(" AND assi_assign.create_dt  <= :endDate ");
                    parameters.put("endDate", endDate);
                }
            }
        }
        builder.append("GROUP BY tr.require_sid ");

        return builder.toString();
    }

    private String prepareDepSql(Set<Integer> depSids) {

        String depSidsStr = depSids.stream()
                .sorted(Comparator.comparing(depSid -> depSid)) // index 為升序
                .map(sid -> sid + "")
                .collect(Collectors.joining(","));

        String sql = ""
                + "  ("
                + "     EXISTS ( "
                + "         SELECT search_info.require_sid "
                + "           FROM tr_assign_send_search_info search_info "
                + "          WHERE search_info.type = 0 "
                + "            AND search_info.require_sid = tr.require_sid "
                + "            AND search_info.dep_sid IN (" + depSidsStr + ") ) "
                + "    OR EXISTS ( "
                + "         SELECT rcd.require_sid "
                + "           FROM tr_require_confirm_dep rcd "
                + "          WHERE rcd.status = 0 "
                + "            AND rcd.require_sid = tr.require_sid "
                + "            AND rcd.dep_sid IN (" + depSidsStr + ") ) "
                + "  )";
        return sql;
    }

    /**
     * 組【需求確認單位】欄位資訊
     * 
     * @param search07View
     */
    private void prepareReqConfirmDepInfo(
            Search07View search07View,
            List<Integer> matchDepSids,
            Map<Integer, Integer> orgSortNumberMapByDepSid) {

        List<RequireConfirmDepVO> requireConfirmDepVOs = search07View.getRequireConfirmDepVOs();

        // ====================================
        // 防呆
        // ====================================
        if (WkStringUtils.isEmpty(requireConfirmDepVOs)) {
            return;
        }

        // ====================================
        // 僅有一個分派單位
        // ====================================
        if (requireConfirmDepVOs.size() == 1) {
            search07View.setReqConfirmProgStatus(this.prepareDepInfo(requireConfirmDepVOs.get(0)));
            return;
        }
        // ====================================
        // 有多個分派單位
        // ====================================
        // 分類為『責任單位』、『其他單位』
        List<RequireConfirmDepVO> dutyDep = Lists.newArrayList();
        List<RequireConfirmDepVO> otherDep = Lists.newArrayList();
        for (RequireConfirmDepVO requireConfirmDepVO : requireConfirmDepVOs) {
            if (matchDepSids.contains(requireConfirmDepVO.getDepSid())) {
                dutyDep.add(requireConfirmDepVO);
            } else {
                otherDep.add(requireConfirmDepVO);
            }
        }

        // 排序器：以部門順序排序
        Comparator<RequireConfirmDepVO> comparator = Comparator.comparing(vo -> orgSortNumberMapByDepSid.get(vo.getDepSid()));

        // 組顯示內容：責任部門
        String content = dutyDep.stream()
                .sorted(comparator)
                .map(vo -> this.prepareDepInfo(vo))
                .collect(Collectors.joining("<br/>", WkHtmlUtils.addBlueBlodClass("責任部門：") + "<br/>", "<br/>"));

        if (WkStringUtils.notEmpty(dutyDep) && WkStringUtils.notEmpty(otherDep)) {
            content += "<br/>";
        }

        // 組顯示內容：其他分派部門
        content += otherDep.stream()
                .sorted(comparator)
                .map(vo -> this.prepareDepInfo(vo))
                .collect(Collectors.joining("<br/>", WkHtmlUtils.addGreenBlodClass("其他分派部門：") + "<br/>", "<br/>"));

        search07View.setReqConfirmProgStatus(content);
    }

    /**
     * 分派部門資訊
     * 
     * @param requireConfirmDepVO
     * @return
     */
    private String prepareDepInfo(RequireConfirmDepVO requireConfirmDepVO) {
        String content = "";
        // 部門名稱
        content = WkOrgCache.getInstance().findNameBySid(requireConfirmDepVO.getDepSid());
        // 執行進度
        if (requireConfirmDepVO.getProgStatus() != null) {
            content += "【" + requireConfirmDepVO.getProgStatus().getLabel() + "】";
        }
        // 負責人名稱
        content += this.requireConfirmDepService.prepareOwnerName(
                requireConfirmDepVO.getOwnerSid(),
                requireConfirmDepVO.getDepSid());
        return content;
    }

    /**
     * @param requireNo
     * @param builder
     * @param parameters
     */
    private void buildRequireCondition(
            List<String> requireSids,
            String requireNo,
            StringBuilder builder,
            Map<String, Object> parameters) {

        builder.append("("
                + "SELECT "
                + " tr.require_sid,"
                + " tr.require_no,"
                + " tr.urgency,"
                + " tr.create_dt,"
                + " tr.dep_sid,"
                + " tr.create_usr,"
                + " tr.require_suspended_code,"
                + " tr.require_status,"
                + " tr.read_reason,"
                + " tr.require_finish_code,"
                + " tr.hope_dt,"
                + " tr.inte_staff,"
                + " tr.update_dt, "
                + " tr.require_establish_dt, "
                + " tr.require_finish_dt, "
                + " tr.in_charge_dep, "
                + " tr.in_charge_usr, "
                + " tr.mapping_sid "
                + "FROM tr_require tr "
                + "WHERE 1=1 ");

        // ====================================
        // 固定限制條件 需求單 sid
        // ====================================
        String requireSidsStr = requireSids.stream().collect(Collectors.joining("', '", "'", "'"));
        builder.append("AND tr.require_sid IN(" + requireSidsStr + ")");

        // ====================================
        // 結案碼
        // ====================================
        // 有設定單位確認狀態時, 若為條件為未完成確認, 將結案狀態改為未結案，以減低初始查詢結果數量
        ReqConfirmDepProgStatus reqConfirmDepProgStatus = search07QueryComponent.getSearch07QueryVO().getReqConfirmDepProgStatus();
        if (reqConfirmDepProgStatus != null && !reqConfirmDepProgStatus.isInFinish()) {
            search07QueryComponent.getSearch07QueryVO().setSelectedColseCode(Lists.newArrayList("0"));
        }

        // 結案碼
        if (WkStringUtils.isEmpty(requireNo)) {
            List<String> selectedColseCode = search07QueryComponent.getSearch07QueryVO().getSelectedColseCode();
            if (selectedColseCode.size() == 1) {
                builder.append(" AND tr.close_code = '" + selectedColseCode.get(0) + "' ");
            }
        }

        // ====================================
        // 完成碼
        // ====================================
        // 完成碼 BINARY 是為區分大小寫
        if (Strings.isNullOrEmpty(requireNo)) {
            // 取得選擇的完成碼
            List<String> reqFinishCodes = search07QueryComponent.getSearch07QueryVO().getReqFinishCode();
            if (reqFinishCodes == null) {
                reqFinishCodes = Lists.newArrayList();
            }
            // 非全選時，才加進查詢條件
            if (search07HeaderMBean.getFinishCodeItems().length != reqFinishCodes.size()) {
                builder.append(" AND BINARY tr.require_finish_code IN (:finishCodes)");
                parameters.put("finishCodes", search07QueryComponent.findFilterFinishCodes());
            }
        }

        // ====================================
        // 待閱原因
        // ====================================
        List<String> toBeReadQuery = search07QueryComponent.getToBeReadTypeQuery();
        if (Strings.isNullOrEmpty(requireNo)) {
            // 全選時不過濾
            if (selectItemBean.getReqToBeReadTypeItems().size() != toBeReadQuery.size()) {
                if (toBeReadQuery != null && !toBeReadQuery.isEmpty()) {
                    String sql = ""
                            + " AND ("
                            + "         tr.read_reason IN (:toBeReadTypes) ";

                    if (toBeReadQuery.contains(NO_READ_REASON)) {
                        sql += "        OR ( tr.read_reason IS NULL OR tr.read_reason = '' )";
                    }
                    sql += "        )";

                    builder.append(sql);
                    parameters.put("toBeReadTypes", toBeReadQuery);
                }
            }
        }
        // 異動區間
        if (Strings.isNullOrEmpty(requireNo) && Search07QueryColumn.UPDATE_DATE.equals(search07QueryComponent.getSearch07QueryVO().getDateTimeType())) {
            Date startDate = DateUtils.convertToOriginOfDay(search07QueryComponent.getSearch07QueryVO().getStartDate());
            Date endDate = DateUtils.convertToEndOfDay(search07QueryComponent.getSearch07QueryVO().getEndDate());
            if (startDate != null && endDate != null) {
                builder.append(" AND tr.update_dt BETWEEN :startUpdatedDate AND :endUpdatedDate");
                parameters.put("startUpdatedDate", startDate);
                parameters.put("endUpdatedDate", endDate);
            } else if (startDate != null) {
                builder.append(" AND tr.update_dt >= :startUpdatedDate  ");
                parameters.put("startUpdatedDate", startDate);
            } else if (endDate != null) {
                builder.append(" AND tr.update_dt  <= :endUpdatedDate ");
                parameters.put("endUpdatedDate", endDate);
            }
        }

        //////////////////// 以下為進階搜尋條件//////////////////////////////
        // 緊急度
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getUrgencyTypeList() != null && !commHeaderMBean.getUrgencyTypeList().isEmpty()) {
            builder.append(" AND tr.urgency IN (:urgencyList)");
            parameters.put("urgencyList", commHeaderMBean.getUrgencyTypeList());
        }
        // 需求單號
        if (!Strings.isNullOrEmpty(requireNo)) {
            builder.append(" AND tr.require_no = :requireNo");
            parameters.put("requireNo", requireNo);
        }
        // 需求單號
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(commHeaderMBean.getRequireNo())) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(commHeaderMBean.getRequireNo()) + "%";
            builder.append(" AND tr.require_no LIKE :requireNo");
            parameters.put("requireNo", textNo);
        }
        // 轉發部門
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getForwardDepts() != null && !commHeaderMBean.getForwardDepts().isEmpty()) {
            builder.append(" AND tr.require_sid IN (SELECT tai.require_sid FROM tr_alert_inbox tai WHERE tai.forward_type='FORWARD_DEPT'"
                    + " AND tai.receive_dep IN (:forwardDepts))");
            parameters.put("forwardDepts", commHeaderMBean.getForwardDepts());
        }

        builder.append(") AS tr ");
    }

    /**
     * 模糊搜尋
     * 
     * @param requireNo
     * @param builder
     * @param parameters
     */
    private void buildRequireIndexDictionaryCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        // 模糊搜尋
        builder.append("INNER JOIN (SELECT tid.require_sid,tid.field_content FROM tr_index_dictionary tid WHERE 1=1");
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(search07QueryComponent.getSearch07QueryVO().getFuzzyText())) {
            String text = "%" + reqularUtils.replaceIllegalSqlLikeStr(search07QueryComponent.getSearch07QueryVO().getFuzzyText()) + "%";
            builder.append(" AND (tid.require_sid"
                    + " IN (SELECT DISTINCT tid1.require_sid FROM tr_index_dictionary tid1 WHERE tid1.field_content LIKE :text)"
                    + " OR tid.require_sid IN (SELECT DISTINCT trace.require_sid FROM tr_require_trace trace WHERE "
                    + "trace.require_trace_type = 'REQUIRE_INFO_MEMO' AND trace.require_trace_content LIKE :text))");
            parameters.put("text", text);
        }
        builder.append(" AND tid.field_name='主題') AS tid ON tr.require_sid=tid.require_sid ");
    }

    /**
     * @param requireNo
     * @param builder
     * @param parameters
     */
    private void buildCategoryKeyMappingCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_category_key_mapping ckm WHERE 1=1");
        // 類別組合
        if (Strings.isNullOrEmpty(requireNo) && (!search07QueryComponent.getSearch07QueryVO().getBigDataCateSids().isEmpty()
                || !search07QueryComponent.getSearch07QueryVO().getMiddleDataCateSids().isEmpty()
                || !search07QueryComponent.getSearch07QueryVO().getSmallDataCateSids().isEmpty())) {
            builder.append(" AND ckm.big_category_sid IN (:bigs) OR ckm.middle_category_sid IN (:middles) OR ckm.small_category_sid IN "
                    + "(:smalls)");
            // 加入單獨選擇的大類
            if (search07QueryComponent.getSearch07QueryVO().getSelectBigCategory() != null) {
                search07QueryComponent.getSearch07QueryVO().getBigDataCateSids()
                        .add(search07QueryComponent.getSearch07QueryVO().getSelectBigCategory().getSid());
            }
            parameters.put("bigs", search07QueryComponent.getSearch07QueryVO().getBigDataCateSids().isEmpty() ? ""
                    : search07QueryComponent.getSearch07QueryVO().getBigDataCateSids());
            parameters.put("middles", search07QueryComponent.getSearch07QueryVO().getMiddleDataCateSids().isEmpty() ? ""
                    : search07QueryComponent.getSearch07QueryVO().getMiddleDataCateSids());
            parameters.put("smalls", search07QueryComponent.getSearch07QueryVO().getSmallDataCateSids().isEmpty() ? ""
                    : search07QueryComponent.getSearch07QueryVO().getSmallDataCateSids());
        }
        // 需求類別
        if (Strings.isNullOrEmpty(requireNo) && search07QueryComponent.getSearch07QueryVO().getSelectBigCategory() != null
                && search07QueryComponent.getSearch07QueryVO().getBigDataCateSids().isEmpty()) {
            builder.append(" AND ckm.big_category_sid = :big");
            parameters.put("big", search07QueryComponent.getSearch07QueryVO().getSelectBigCategory().getSid());
        }
        builder.append(") AS ckm ON tr.mapping_sid=ckm.key_sid ");
    }

    /**
     * @param builder
     * @param parameters
     */
    private void buildAssignInfoConfition_Join(StringBuilder builder, Map<String, Object> parameters) {
        // 取得最新一筆分派記錄
        builder.append("LEFT JOIN tr_assign_send_info assi_assign ");
        builder.append("   ON assi_assign.type = 0 ");
        builder.append("  AND assi_assign.require_sid = tr.require_sid ");
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Search07View view) {
        view.setReadRecordType(ReadRecordType.HAS_READ);
        Require r = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(r, loginBean.getUser(), RequireBottomTabType.ASSIGN_SEND_INFO);
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (Search07View) event.getObject();
        this.changeRequireContent(this.queryKeeper);
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Search07View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        helper.exportExcel(document, search07QueryComponent.getSearch07QueryVO().getStartDate(), search07QueryComponent.getSearch07QueryVO().getEndDate(),
                commHeaderMBean.getReportType());
        hasDisplay = true;
    }

    /**
     * 隱藏部分column裡的內容
     */
    public void hideColumnContent() {
        hasDisplay = false;
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search07View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.removeClassByTextBold(dtId, pageCount);
        this.transformHasRead();
        this.checkHelfScreen();
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            displayController.update("headerTitle");
            displayController.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Search07View to) {
        querySelection = to;
        queryKeeper = querySelection;
        displayController.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 去除粗體Class
     *
     * @param dtId
     * @param pageCount
     */
    private void removeClassByTextBold(String dtId, String pageCount) {
        displayController.execute("removeClassByTextBold('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
        displayController.execute("changeAlreadyRead('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 變更已閱讀
     */
    private void transformHasRead() {
        querySelection.setReadRecordType(ReadRecordType.HAS_READ);
        queryKeeper = querySelection;
        queryItems.set(queryItems.indexOf(querySelection), querySelection);
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
        upDownBean.resetTabInfo(RequireBottomTabType.ASSIGN_SEND_INFO, queryKeeper.getSid());
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.transformHasRead();
        this.removeClassByTextBold(dtId, pageCount);
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    /**
     * 查詢啟始日的時間調整為00-00-00
     *
     * @param startDate
     * @return
     */
    public Date transStartDate(Date startDate) {
        if (startDate == null) {
            return null;
        }
        return helper.transStartDate(startDate);
    }

    /**
     * 查詢結束日的時間調整為23-59-59
     *
     * @param endDate
     * @return
     */
    public Date transEndDate(Date endDate) {
        if (endDate == null) {
            return null;
        }
        return helper.transEndDate(endDate);
    }

    /** 訊息呼叫 */
    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -3155502858378184991L;

        @Override
        public void showMessage(String m) {
            MessagesUtils.showError(m);
        }
    };

    // ========================================================================
    // 被分派單位
    // ========================================================================
    /**
     * 被分派單位樹
     */
    @Getter
    @Setter
    private transient TreePickerComponent assignNoticeDepTreePicker;

    /**
     * 被分派單位樹選單 - 開啟選單
     */
    public void event_dialog_assignNoticeDepTree_open() {

        try {
            this.assignNoticeDepTreePicker.rebuild();
        } catch (Exception e) {
            log.error("開啟【被分派單位】設定視窗失敗", e);
            MessagesUtils.showError("開啟【被分派單位】設定視窗失敗");
            return;
        }

        displayController.showPfWidgetVar("wv_dlg_assignNoticeDep");
    }

    /**
     * 被分派單位樹選單 - 關閉選單
     */
    public void event_dialog_assignNoticeDepTree_confirm() {

        // 取得元件中被選擇的項目
        this.search07QueryComponent.getSearch07QueryVO().setRequireDepts(
                Lists.newArrayList(this.assignNoticeDepTreePicker.getSelectedItemIntegerSids()));

        // 畫面控制
        displayController.hidePfWidgetVar("wv_dlg_assignNoticeDep");
    }

    /**
     * 分派通知單位選單 - callback 事件
     */
    private final TreePickerCallback assignNoticeDepTreeCallback = new TreePickerCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = -2685194232972981152L;

        @Override
        public List<WkItem> prepareAllItems() throws Exception {
            return prepareAllAssignNoticeDepItems();
        }

        /**
         * 準備已選擇項目
         * 
         * @return
         */
        @Override
        public List<String> prepareSelectedItemSids() throws Exception {

            // 回傳
            return search07QueryComponent.getSearch07QueryVO().getAssignDepSids().stream()
                    .map(sid -> sid + "")
                    .collect(Collectors.toList());
        }

        @Override
        public List<String> prepareDisableItemSids() throws Exception {
            // 收集不可選的資料
            List<String> disableDepSids = Lists.newArrayList();
            for (WkItem wkItem : assignNoticeDepTreePicker.getAllItems()) {
                if (wkItem.isDisable()) {
                    disableDepSids.add(wkItem.getSid());
                }
            }
            return disableDepSids;
        }
    };

    // ========================================================================
    // 自訂預設尋條件-分派部門
    // ========================================================================
    /**
     * 自訂預設尋條件-分派部門樹
     */
    @Getter
    @Setter
    private transient TreePickerComponent customerFilterAssignNoticeDepTreePicker;

    /**
     * 自訂預設尋條件-分派部門樹 - 開啟選單
     */
    public void event_dialog_customerFilterAssignNoticeDepTree_open() {

        try {
            this.customerFilterAssignNoticeDepTreePicker.rebuild();
        } catch (Exception e) {
            log.error("開啟【被分派單位】設定視窗失敗", e);
            MessagesUtils.showError("開啟【被分派單位】設定視窗失敗");
            return;
        }

        displayController.showPfWidgetVar("wv_dlg_customerFilterAssignNoticeDep");
    }

    /**
     * 自訂預設尋條件-分派通知部門樹 - 關閉選單
     */
    public void event_dialog_customerFilterAssignNoticeDepTree_confirm() {

        // 取得元件中被選擇的項目
        this.search07QueryComponent.getSearch07QueryVOCustomerFilter().setRequireDepts(
                Lists.newArrayList(this.customerFilterAssignNoticeDepTreePicker.getSelectedItemIntegerSids()));

        // 畫面控制
        displayController.hidePfWidgetVar("wv_dlg_customerFilterAssignNoticeDep");
    }

    /**
     * 自訂預設尋條件-分派通知部門樹 - callback 事件
     */
    private final TreePickerCallback customerFilterAssignNoticeDepTreeCallback = new TreePickerCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = -5626084959942107906L;

        @Override
        public List<WkItem> prepareAllItems() throws Exception {
            return prepareAllAssignNoticeDepItems();
        }

        /**
         * 準備已選擇項目
         * 
         * @return
         */
        @Override
        public List<String> prepareSelectedItemSids() throws Exception {
            // 回傳
            return search07QueryComponent.getSearch07QueryVOCustomerFilter().getAssignDepSids().stream()
                    .map(sid -> sid + "")
                    .collect(Collectors.toList());
        }

        @Override
        public List<String> prepareDisableItemSids() throws Exception {
            // 收集不可選的資料
            List<String> disableDepSids = Lists.newArrayList();
            for (WkItem wkItem : customerFilterAssignNoticeDepTreePicker.getAllItems()) {
                if (wkItem.isDisable()) {
                    disableDepSids.add(wkItem.getSid());
                }
            }
            return disableDepSids;
        }
    };

    // ========================================================================
    //
    // ========================================================================
    /**
     * @return
     * @throws Exception
     */
    public List<WkItem> prepareAllAssignNoticeDepItems() throws Exception {
        // ====================================
        // 取得登入者可使用單位
        // ====================================
        List<Integer> allDepSids = WkOrgCache.getInstance().findAllDepSidByCompSid(SecurityFacade.getCompanySid());;

        // ====================================
        // 計算
        // ====================================
        return treePickerDepHelper.prepareDepItems(
                SecurityFacade.getCompanyId(),
                allDepSids);
    }

}
