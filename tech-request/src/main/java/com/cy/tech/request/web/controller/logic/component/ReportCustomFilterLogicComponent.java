/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.logic.component;

import com.cy.tech.request.logic.service.ReportCustomFilterService;
import com.cy.tech.request.vo.enums.SearchReportCustomEnum;
import com.cy.tech.request.vo.require.ReportCustomFilter;
import com.cy.tech.request.vo.value.to.CustomFilterColumnTo;
import com.cy.tech.request.vo.value.to.ReportCustomFilterDetailTo;
import com.cy.tech.request.vo.value.to.ReportCustomFilterTo;
import com.cy.tech.request.web.controller.helper.ReportCustomFilterHelper;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterArrayStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterDetailVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterStringVO;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterVO;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 自訂查詢條件邏輯元件
 *
 * @author brain0925_liao
 */
@Component
public class ReportCustomFilterLogicComponent implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5735449908567453690L;
    private static ReportCustomFilterLogicComponent instance;

    public static ReportCustomFilterLogicComponent getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        ReportCustomFilterLogicComponent.instance = this;
    }
    /** ReportCustomFilterService */
    @Autowired
    private ReportCustomFilterService reportCustomFilterService;
    /** ReportCustomFilterHelper */
    @Autowired
    private ReportCustomFilterHelper reportCustomFilterHelper;

    /**
     * 建立共用物件Detail
     *
     * @param searchReportCustomEnum 報表類型列舉
     * @param arrayStrings
     * @return
     */
    public ReportCustomFilterDetailVO createReportCustomFilterDetailVO(SearchReportCustomEnum searchReportCustomEnum,
            Set<Integer> tempArrayStrings) {
        
        List<String> arrayStrings = Lists.newArrayList();
        if(WkStringUtils.notEmpty(tempArrayStrings)) {
            arrayStrings = tempArrayStrings.stream()
                    .map(num -> num+"")
                    .collect(Collectors.toList());
        }
        
        return new ReportCustomFilterArrayStringVO(searchReportCustomEnum, arrayStrings);
    }
    
    public ReportCustomFilterDetailVO createReportCustomFilterDetailVO(SearchReportCustomEnum searchReportCustomEnum,
            List<String> arrayStrings) {
        return new ReportCustomFilterArrayStringVO(searchReportCustomEnum, arrayStrings);
    }

    /**
     * 建立共用物件Detail
     *
     * @param searchReportCustomEnum 報表類型列舉
     * @param value
     * @return
     */
    public ReportCustomFilterDetailVO createReportCustomFilterDetailVO(SearchReportCustomEnum searchReportCustomEnum, 
    		String value) {
        return new ReportCustomFilterStringVO(searchReportCustomEnum, value);
    }

    /**
     * 儲存自訂搜尋條件
     *
     * @param searchReportCustomEnum 報表類型列舉
     * @param loginUserSid 登入者Sid
     * @param index index
     * @param showDefault 是否為預設
     * @param reportCustomFilterDetailVOs 自訂查詢條件物件
     */
    public void saveReportCustomFilter(SearchReportCustomEnum searchReportCustomEnum,
            Integer loginUserSid, Integer index, boolean showDefault,
            List<ReportCustomFilterDetailVO> reportCustomFilterDetailVOs) {
        ReportCustomFilter reportCustomFilter
                = reportCustomFilterService.getReportCustomFilterByUserSidAndUrl(searchReportCustomEnum.getUrl(), loginUserSid);
        if (reportCustomFilter == null) {
            reportCustomFilter = new ReportCustomFilter();
            reportCustomFilter.setUrl(searchReportCustomEnum.getUrl());
        }
        List<CustomFilterColumnTo> customFilterColumnTos = Lists.newArrayList();
        reportCustomFilterDetailVOs.forEach(item -> {
            CustomFilterColumnTo ct = new CustomFilterColumnTo();
            ct.setKey(item.getSearchReportCustomEnum().getKey());
            ct.setName(item.getSearchReportCustomEnum().getVal());
            ct.setValue(item.getJsonValue());
            ct.setSearchReportCustomType(item.getSearchReportCustomType().name());
            customFilterColumnTos.add(ct);
        });

        ReportCustomFilterTo reportCustomFilterTo = new ReportCustomFilterTo();
        List<ReportCustomFilterDetailTo> allReportCustomFilterDetailTos = Lists.newArrayList();
        //暫時僅開放一筆
//        if (reportCustomFilter.getCustomFilter() != null
//                && reportCustomFilter.getCustomFilter().getReportCustomFilterDetailTos() != null) {
//           
//            reportCustomFilter.getCustomFilter().getReportCustomFilterDetailTos().forEach(item -> {
//                if (index != null && String.valueOf(index).equals(item.getIndex())) {
//                    item.setIsDefault((showDefault) ? "1" : "0");
//                    item.setCustomFilterColumnTos(customFilterColumnTos);
//                }
//                allReportCustomFilterDetailTos.add(item);
//            });
//        }
        //       if (index == null) {
        ReportCustomFilterDetailTo rt = new ReportCustomFilterDetailTo();
        rt.setIsDefault((showDefault) ? "1" : "0");
        rt.setIndex(String.valueOf(allReportCustomFilterDetailTos.size()));
        rt.setCustomFilterColumnTos(customFilterColumnTos);
        allReportCustomFilterDetailTos.add(rt);
        //     }
        reportCustomFilterTo.setReportCustomFilterDetailTos(allReportCustomFilterDetailTos);
        reportCustomFilter.setCustomFilter(reportCustomFilterTo);
        if (Strings.isNullOrEmpty(reportCustomFilter.getSid())) {
            reportCustomFilterService.createReportCustomFilter(reportCustomFilter, loginUserSid);
        } else {
            reportCustomFilterService.updateReportCustomFilter(reportCustomFilter, loginUserSid);
        }
    }

    /**
     * 取得自訂搜尋條件
     *
     * @param searchReportCustomEnum 報表類型列舉
     * @param loginUserSid 登入者Sid
     * @return
     */
    public List<ReportCustomFilterVO> getReportCustomFilter(SearchReportCustomEnum searchReportCustomEnum, Integer loginUserSid) {
        ReportCustomFilter reportCustomFilter
                = reportCustomFilterService.getReportCustomFilterByUserSidAndUrl(searchReportCustomEnum.getUrl(), loginUserSid);
        if (reportCustomFilter == null || reportCustomFilter.getCustomFilter() == null
                || reportCustomFilter.getCustomFilter().getReportCustomFilterDetailTos() == null
                || reportCustomFilter.getCustomFilter().getReportCustomFilterDetailTos().isEmpty()) {
            return null;
        }

        List<ReportCustomFilterVO> reportCustomFilterVOs = Lists.newArrayList();
        reportCustomFilter.getCustomFilter().getReportCustomFilterDetailTos().forEach(item -> {
            if (item.getCustomFilterColumnTos() == null || item.getCustomFilterColumnTos().isEmpty()) {
                return;
            }
            List<ReportCustomFilterDetailVO> reportCustomFilterDetailVOs = Lists.newArrayList();
            item.getCustomFilterColumnTos().forEach(subItem -> {
                ReportCustomFilterDetailVO reportCustomFilterDetailVO = reportCustomFilterHelper.transToReportCustomFilterDetailVO(subItem, searchReportCustomEnum);
                if (reportCustomFilterDetailVO != null) {
                    reportCustomFilterDetailVOs.add(reportCustomFilterDetailVO);
                }
            });
            if (reportCustomFilterDetailVOs.isEmpty()) {
                return;
            }

            boolean showDefault = "1".equals(item.getIsDefault());
            int index = Integer.valueOf(item.getIndex());
            ReportCustomFilterVO reportCustomFilterVO = new ReportCustomFilterVO(showDefault, index, reportCustomFilterDetailVOs);
            reportCustomFilterVOs.add(reportCustomFilterVO);

        });
        if (reportCustomFilterVOs.isEmpty()) {
            return null;
        }

        return reportCustomFilterVOs;
    }

}
