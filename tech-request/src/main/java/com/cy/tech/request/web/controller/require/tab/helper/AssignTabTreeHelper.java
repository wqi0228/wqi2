/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require.tab.helper;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.vo.AssignTabHistoryTo;
import com.cy.work.common.utils.WkStringUtils;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 分派頁籤顯示輔助(樹)
 *
 * @author shaun
 */
@Component
public class AssignTabTreeHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8033779859819710380L;
    @Autowired
    private OrganizationService orgService;

    /**
     * 建立分派歷程樹
     *
     * @param rowInfo
     * @return
     */
    public TreeNode createRoot(List<AssignTabHistoryTo> rowInfo) {
        TreeNode root = new DefaultTreeNode(new AssignTabHistoryTo(), null);
        Map<String, List<AssignTabHistoryTo>> filterMap = this.filterToMap(rowInfo);
        filterMap.values().forEach(each -> this.createChildNode(root, each));
        return root;
    }

    /**
     * 分類各次分派<BR/>
     * 維持原本排序需使用LinkedHashMap
     *
     * @param rowInfo
     * @return
     */
    private Map<String, List<AssignTabHistoryTo>> filterToMap(List<AssignTabHistoryTo> rowInfo) {
        return rowInfo.stream().collect(Collectors.groupingBy(each -> this.groupKey(each), LinkedHashMap::new, toList()));
    }

    /**
     * 各次分派key
     *
     * @param row
     * @return
     */
    private String groupKey(AssignTabHistoryTo row) {
        return row.getExecutorTime() + "-" + row.getExecutorName() + "-" + row.getBehavior();
    }

    /**
     * 建立子節點
     *
     * @param rowInfo
     * @return
     */
    private void createChildNode(TreeNode root, List<AssignTabHistoryTo> rowInfo) {
        if (rowInfo.isEmpty()) {
            return;
        }
        List<AssignTabHistoryTo> topRows = this.findTopUnit(rowInfo);
        if (WkStringUtils.isEmpty(topRows)) {
            return;
        }

        // 移除頂層組織僅餘下層組織
        rowInfo.removeAll(topRows);
        topRows.forEach(row -> {
            TreeNode node = new DefaultTreeNode(row, root);
            node.setExpanded(this.isExpanded(row));
            List<AssignTabHistoryTo> rowOfChilds = this.filterChildsByRows(row, rowInfo);
            if (!rowOfChilds.isEmpty()) {
                this.createChildNode(node, rowOfChilds);
            }
        });
    }

    /**
     * 查找頂點組織
     *
     * @param rowInfo
     * @return
     */
    private List<AssignTabHistoryTo> findTopUnit(List<AssignTabHistoryTo> rowInfo) {
        // 取得此次分派全部的unit sids
        List<String> groupOfUnitSids = rowInfo.stream().map(row -> row.getUnitSid()).collect(Collectors.toList());
        return rowInfo.stream()
                .filter(row -> !this.hasAnyParentUnit(groupOfUnitSids, row))
                .collect(Collectors.toList());
    }

    /**
     * 判斷是否已無上層組織
     *
     * @param groupOfUnitSids
     * @param row
     * @return
     */
    private boolean hasAnyParentUnit(List<String> groupOfUnitSids, AssignTabHistoryTo row) {
        // 取得此分派部門的全部上層部門
        Org unit = orgService.findBySid(row.getUnitSid());
        List<String> unitAllTopSids = orgService.findParentOrgs(unit).stream().map(each -> each.getSid().toString()).collect(Collectors.toList());
        // 群組內分派sid比對過濾分派部門sid全部向上部門sid，如果有任何符合sid即為包含
        return unitAllTopSids.stream().anyMatch(each -> groupOfUnitSids.contains(each));
    }

    /**
     * 群、處展開節點
     *
     * @param row
     * @return
     */
    private boolean isExpanded(AssignTabHistoryTo row) {
        Org unit = orgService.findBySid(row.getUnitSid());
        if (unit == null || unit.getLevel() == null) {
            return false;
        }
        return unit.getLevel().equals(OrgLevel.DIVISION_LEVEL) || unit.getLevel().equals(OrgLevel.GROUPS);
    }

    /**
     * 帶出父節點所擁有的子節點
     *
     * @param parent
     * @param checkRows
     * @return
     */
    private List<AssignTabHistoryTo> filterChildsByRows(AssignTabHistoryTo parent, List<AssignTabHistoryTo> checkRows) {
        return checkRows.stream().filter(row -> {
            Org unit = orgService.findBySid(row.getUnitSid());
            List<String> unitAllTopSids = orgService.findParentOrgs(unit).stream().map(each -> each.getSid().toString()).collect(Collectors.toList());
            return unitAllTopSids.contains(parent.getUnitSid());
        }).collect(Collectors.toList());
    }
}
