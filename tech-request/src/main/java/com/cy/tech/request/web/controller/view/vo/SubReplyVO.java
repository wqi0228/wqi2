/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.vo;

import com.cy.tech.request.web.listener.ReplyCallBack;
import com.cy.tech.request.web.listener.ReplyCallBackCondition;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

/**
 * 子回覆介面物件
 *
 * @author brain0925_liao
 */
public class SubReplyVO {

    public SubReplyVO(String os_reply_and_reply_sid) {
        this.os_reply_and_reply_sid = os_reply_and_reply_sid;
    }

    public SubReplyVO(String os_reply_and_reply_sid, String os_sid, String historySid,
            String replyAndReplyHistorySid, boolean showModifyBtn, boolean showAttachmentBtn, boolean disableModifyBtn,
            Integer createUserSid, Integer createUserDepSid,
            String createUserName, String createUserDepName,
            String lastModifyTime, String content, ReplyCallBack replyCallBack, int index) {
        this.os_reply_and_reply_sid = os_reply_and_reply_sid;
        this.os_sid = os_sid;
        this.historySid = historySid;
        this.replyAndReplyHistorySid = replyAndReplyHistorySid;
        this.showModifyBtn = showModifyBtn;
        this.showAttachmentBtn = showAttachmentBtn;
        this.disableModifyBtn = disableModifyBtn;
        this.createUserSid = createUserSid;
        this.createUserDepSid = createUserDepSid;
        this.createUserName = createUserName;
        this.createUserDepName = createUserDepName;
        this.lastModifyTime = lastModifyTime;
        this.content = content;
        this.replyCallBack = replyCallBack;
        this.index = index;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.os_reply_and_reply_sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SubReplyVO other = (SubReplyVO) obj;
        if (!Objects.equals(this.os_reply_and_reply_sid, other.os_reply_and_reply_sid)) {
            return false;
        }
        return true;
    }

    public void maintainAtt() {
        ReplyCallBackCondition rc = new ReplyCallBackCondition();
        rc.setOs_sid(os_sid);
        rc.setOs_history_sid(replyAndReplyHistorySid);
        replyCallBack.maintainAtt(rc);
    }

    public void modifyReplyAndReply() {
        ReplyCallBackCondition rc = new ReplyCallBackCondition();
        rc.setOs_reply_and_reply_sid(os_reply_and_reply_sid);
        rc.setOs_history_sid(historySid);
        rc.setReply_and_reply_history_sid(replyAndReplyHistorySid);
        replyCallBack.modifyReplyAndReply(rc);
    }
    @Getter
    private int index;

    @Getter
    private String os_reply_and_reply_sid;
    /** 回覆的HistorySid */
    @Getter
    private String historySid;
    @Getter
    private String os_sid;
    /** 回覆的回覆HistorySid */
    @Getter
    private String replyAndReplyHistorySid;

    private ReplyCallBack replyCallBack;

    /** 是否顯示修改回覆按鈕 */
    @Getter
    private boolean showModifyBtn;
    /** 是否有附件 */
    @Getter
    @Setter
    private boolean showAttachmentBtn;
    @Getter
    private boolean disableModifyBtn;
    /** 建立者Sid */
    @Getter
    private Integer createUserSid;
    /** 建立者部門Sid */
    @Getter
    private Integer createUserDepSid;
    /** 建立者名稱 */
    @Getter
    private String createUserName;
    /** 建立者部門名稱 */
    @Getter
    private String createUserDepName;
    /** 回覆最後修改時間 */
    @Getter
    private String lastModifyTime;
    /** 回覆內容 */
    @Getter
    private String content;
}
