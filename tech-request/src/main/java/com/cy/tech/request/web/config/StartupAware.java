/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.config;

import com.cy.fb.rest.client.controller.FBClientConnInfo;
import com.cy.work.backend.logic.WorkCalendarHelper;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.utils.WkCommonUtils;
import com.google.common.base.Strings;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 *
 * @author shaun
 */
@Slf4j
@Component
public class StartupAware implements ApplicationContextAware {
	@Autowired
	transient private WorkCalendarHelper calendarHelper;
	//@Autowired
	//transient private ReqBpmUpdateService bpmUpdateService;
	@Autowired
	transient private FBClientConnInfo fbConnInfo;
	@Value("${tech.request.fb.mail}")
	@Getter
	private String fbConnMail;
	@Value("${tech.request.fb.pass}")
	@Getter
	private String fbConnPass;

	@Override
	public void setApplicationContext(ApplicationContext actx) throws BeansException {

		try {
			log.info("取得網址檢測:{}", WkCommonUtils.findDomainUrlByCompId("TG"));
		} catch (SystemOperationException e) {
			log.error("取得網址檢測失敗!", e);
		}

		// 行事曆
		calendarHelper.start();
		// 設定FB連線帳號資訊
		if (Strings.isNullOrEmpty(fbConnMail)) {
			fbConnMail = "API-ERPTECH-REQUEST";
		}
		if (Strings.isNullOrEmpty(fbConnPass)) {
			fbConnPass = "ecj84web";
		}
		fbConnInfo.setLoginMail(fbConnMail);
		fbConnInfo.setLoginPass(fbConnPass);
		// 建立第一次快取
		// customerService.findAll();
		// 需求系統流程自動重置功能啟動
		// bpmUpdateService.start();
		
		
		// 建立需求單推播自訂義Cache
		// 重構系統通知後，此快取已無用
		// reqNotifyCustomizeCache.startCache();
	}
}
