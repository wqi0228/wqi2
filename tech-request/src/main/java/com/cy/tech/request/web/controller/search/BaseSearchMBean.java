
package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.NotImplementedException;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.helper.component.ComponentHelper;
import com.cy.tech.request.logic.search.view.BaseSearchView;
import com.cy.tech.request.logic.service.RequireReadRecordHelper;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.vo.query.search.SearchQuery;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.component.qkstree.QuickSelectTreeCallback;
import com.cy.tech.request.web.controller.component.qkstree.impl.OrgUserTreeMBean;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.vo.WkItem;
import com.cy.work.logic.WkUserAndOrgLogic;
import com.cy.work.viewcomponent.treepker.TreePickerCallback;
import com.cy.work.viewcomponent.treepker.TreePickerComponent;
import com.cy.work.viewcomponent.treepker.helper.TreePickerDepHelper;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class BaseSearchMBean implements Serializable, ISearchMBean {

    /**
     * 
     */
    private static final long serialVersionUID = 1017688934646680164L;
    // ========================================================================
    // 服務元件
    // ========================================================================
    @Autowired
    transient protected DisplayController displayController;
    @Autowired
    transient protected TreePickerDepHelper treePickerDepHelper;

    @Autowired
    transient protected RequireService requireService;
    @Autowired
    transient protected RequireReadRecordHelper requireReadRecordHelper;
    @Autowired
    transient protected ReqLoadBean loadManager;

    @Autowired
    transient protected WkUserAndOrgLogic wkUserAndOrgLogic;
    @Autowired
    transient protected ComponentHelper componentHelper;
    @Autowired
    transient protected WorkBackendParamHelper workBackendParamHelper;

    // ========================================================================
    //
    // ========================================================================
    /** 選擇的收件資訊 */
    @Getter
    @Setter
    public BaseSearchView querySelection;
    /** 上下筆移動keeper */
    @Getter
    public BaseSearchView queryKeeper;

    /**
     * 變更需求單內容
     *
     * @param view
     */
    protected void changeRequireContent(BaseSearchView view) {
        User loginUser = WkUserCache.getInstance().findBySid(SecurityFacade.getUserSid());
        Require require = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(require, loginUser);
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (BaseSearchView) event.getObject();
        this.changeRequireContent(this.queryKeeper);
    }

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 公用初始化
     */
    public void init() {
        // 初始化主責單位樹
        this.inChargeDepTreePicker = new TreePickerComponent(this.inChargeDepTreeCallback, "主責單位");
    }

    /**
     * 實做取得本頁查詢物件的方法
     * 
     * @return SearchQuery
     */
    public SearchQuery getQueryCondition() {
        throw new NotImplementedException("需實做取得本頁查詢物件的方法");
    }

    // ========================================================================
    // 主責單位
    // ========================================================================
    /**
     * 主責單位部門樹
     */
    @Getter
    @Setter
    private transient TreePickerComponent inChargeDepTreePicker;

    /**
     * 主責單位單位選單 - 開啟選單
     */
    public void event_dialog_inChargeDepTree_open() {

        try {
            this.inChargeDepTreePicker.rebuild();
        } catch (Exception e) {
            log.error("開啟【主責單位單位】設定視窗失敗", e);
            MessagesUtils.showError("開啟【主責單位單位】設定視窗失敗");
            return;
        }

        displayController.showPfWidgetVar("wv_dlg_inChargeDep");
    }

    /**
     * 主責單位單位選單 - 關閉選單
     */
    public void event_dialog_inChargeDepTree_confirm() {

        // 取得元件中被選擇的項目
        this.getQueryCondition().setInChargeDepSids(
                Lists.newArrayList(this.inChargeDepTreePicker.getSelectedItemIntegerSids()));

        // 畫面控制
        displayController.hidePfWidgetVar("wv_dlg_inChargeDep");
    }

    /**
     * 主責單位單位選單 - callback 事件
     */
    private final TreePickerCallback inChargeDepTreeCallback = new TreePickerCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = 8202838041191184324L;

        @Override
        public List<WkItem> prepareAllItems() throws Exception {

            // 給全單位
            Org comp = WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId());
            if (comp == null) {
                return Lists.newArrayList();
            }

            return treePickerDepHelper.prepareDepItems(SecurityFacade.getCompanyId(),
                    WkOrgCache.getInstance().findAllChild(comp.getSid()).stream().map(Org::getSid).collect(Collectors.toList()));
        }

        /**
         * 準備已選擇項目
         * 
         * @return
         */
        @Override
        public List<String> prepareSelectedItemSids() throws Exception {

            if (getQueryCondition().getInChargeDepSids() == null) {
                return Lists.newArrayList();
            }

            // 回傳
            return getQueryCondition().getInChargeDepSids().stream().map(sid -> sid + "").collect(Collectors.toList());
        }

        /**
         *
         */
        @Override
        public List<String> prepareDisableItemSids() throws Exception {
            // 收集不可選的資料
            List<String> disableDepSids = Lists.newArrayList();
            for (WkItem wkItem : inChargeDepTreePicker.getAllItems()) {
                if (wkItem.isDisable()) {
                    disableDepSids.add(wkItem.getSid());
                }
            }
            return disableDepSids;
        }
    };

    // ========================================================================
    // 主要負責人
    // ========================================================================
    /**
     * 人員選擇器
     */
    @Autowired
    private transient OrgUserTreeMBean inChargeUsersPicker;
    @Getter
    private final String inChargeUsersPickerDlgName = "queryCondition_InChargeUsr_Picker_dlg";

    /**
     * 開啟人員選單
     * 
     * @param maintainTypeStr
     */
    public void event_dialog_inChargeUsrTree_open() {

        // ====================================
        // 初始化單位選擇元件
        // ====================================
        try {
            Org comp = WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId());
            if (comp == null) {
                MessagesUtils.showError("公司ID:[" + SecurityFacade.getCompanyId() + "]找不到對應資料!");
                return;
            }

            this.inChargeUsersPicker.init(
                    comp.getSid(),
                    SecurityFacade.getUserSid(),
                    usersPickerCallback);

            this.inChargeUsersPicker.preOpenDlg();

        } catch (Exception e) {
            String message = "人員選單初始化失敗!" + e.getMessage();
            MessagesUtils.showError(message);
            log.error(message, e);
            return;
        }

        // 開啟 dialog
        this.displayController.showPfWidgetVar(inChargeUsersPickerDlgName);
    }

    /**
     * 人員選單點選確認
     */
    public void event_dialog_inChargeUsersTree_confirm() {
        this.getQueryCondition().setInChargeUserSids(this.inChargeUsersPicker.getSelectedDataList());
        // 關閉 dialog
        this.displayController.hidePfWidgetVar(inChargeUsersPickerDlgName);
    }

    /**
     * 人員設定選單 callback
     */
    private final QuickSelectTreeCallback usersPickerCallback = new QuickSelectTreeCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = 6487666037099032409L;

        @SuppressWarnings("unchecked")
        @Override
        public List<Integer> getSelectedDataList() {
            // 取得已設定的人員
            return getQueryCondition().getInChargeUserSids();
        }
    };

    // ========================================================================
    // 樹狀選單
    // ========================================================================
    /** dialog name */
    @Getter
    protected final String TREE_PICKER_DLG_NAME = "TREE_PICKER_DLG_NAME" + this.getClass().getSimpleName();
    /** dialog 內容區 */
    @Getter
    protected final String TREE_PICKER_DLG_CONTENT = "TREE_PICKER_DLG_CONTENT" + this.getClass().getSimpleName();
    /** tree Picker commpoent ID */
    @Getter
    protected final String TREE_PICKER_COMPONENT_ID = "TREE_PICKER_COMPONENT_ID" + this.getClass().getSimpleName();

    /** TreePickerComponent */
    @Getter
    protected transient TreePickerComponent treePicker;
    /** TreePicker 確認按鈕, 要update的區域 */
    @Getter
    protected String treePickerConfirmUpdate;

    /**
     * @param typeStr
     */
    public void treepicker_openDialog(String descr, TreePickerCallback treePickerCallback) {
        // ====================================
        // 初始化選單
        // ====================================
        try {
            this.treePicker = new TreePickerComponent(
                    treePickerCallback,
                    descr);

            this.treePicker.rebuild();
        } catch (Exception e) {
            String errorMessage = "開啟【" + descr + "】設定視窗失敗";
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);
            return;
        }

        // ====================================
        // 畫面處理
        // ====================================
        // 更新 dialog 內容
        this.displayController.update(this.TREE_PICKER_DLG_CONTENT);
        // 顯示 dialog
        this.displayController.showPfWidgetVar(this.TREE_PICKER_DLG_NAME);
    }

    public void treePicker_confirm() {
        MessagesUtils.showError("開發時期錯誤，未實做treePicker_confirm");
    }

    /**
     * @return
     * @throws Exception
     */
    protected List<WkItem> prepareAllCanViewDepItemsByBaseDep() {
        // ====================================
        // 取得登入者可使用單位
        // ====================================
        Set<Integer> allCanUseDeps = wkUserAndOrgLogic.prepareCanViewDepSidBaseOnOrgLevelWithParallel(
                SecurityFacade.getUserSid(),
                OrgLevel.MINISTERIAL,
                true);

        // ====================================
        // 計算
        // ====================================
        return treePickerDepHelper.prepareDepItems(
                SecurityFacade.getCompanyId(),
                Lists.newArrayList(allCanUseDeps));
    }

    /**
     * 取得所有需求項目 for wkitem
     * 
     * @return
     */
    protected List<WkItem> prepareAllCategoryItems() {
        // 取得類別項目
        List<WkItem> allItems = this.componentHelper.prepareAllCategoryItems();
        // 過濾掉停用項目
        return allItems.stream().filter(item -> item.isActive()).collect(Collectors.toList());
    }

}
