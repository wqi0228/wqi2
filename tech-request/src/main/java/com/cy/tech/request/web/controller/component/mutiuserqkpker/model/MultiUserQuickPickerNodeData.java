package com.cy.tech.request.web.controller.component.mutiuserqkpker.model;

import java.io.Serializable;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.utils.WkUserUtils;

import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 *
 */
@Getter
@Setter
public class MultiUserQuickPickerNodeData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6754749773873852336L;
    /**
     * 建構子 for 部門
     * 
     * @param org org
     */
    public MultiUserQuickPickerNodeData(Org org) {
        this.isUserNode = false;
        if (org != null) {
            this.name = org.getName();
            this.sid = "org:" + org.getSid(); //org sid 不會產生作用, 僅識別
            this.active = Activation.ACTIVE.equals(org.getStatus());
        }
    }

    /**
     * 建構子 for user
     * 
     * @param user user
     */
    public MultiUserQuickPickerNodeData(User user) {
        this.isUserNode = true;
        if (user != null) {
            this.name = user.getName();
            this.sid = user.getSid() + "";
            this.active = Activation.ACTIVE.equals(user.getStatus());
            //在 清單模式中, 顯示主要部門 + UserName
            this.selectedListShowName = WkUserUtils.prepareUserNameWithDep(user.getSid(), "-");
        }
    }

    /**
     * SID
     */
    private String sid;
    /**
     * 名稱
     */
    private String name;
    /**
     * 顯示用名稱
     */
    private String selectedListShowName;
    /**
     * 是否為 『可選擇』 節點 (非階層)
     */
    private boolean isUserNode = false;
    /**
     * tree node key
     */
    private String rowKey;
    /**
     * 是否啟用
     */
    private boolean active = false;
    /**
     * 是否為主管
     */
    private String isManager = "0";
    /**
     * 為主管時是否為兼職
     */
    private String isAgent = "0";
}
