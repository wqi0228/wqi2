/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.setting.setting10;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.repository.onpg.WorkOnpgRepo;
import com.cy.tech.request.repository.pt.PtCheckRepo;
import com.cy.tech.request.repository.require.TrSubNoticeInfoRepository;
import com.cy.tech.request.repository.require.othset.OthSetRepo;
import com.cy.tech.request.repository.worktest.WorkTestInfoRepo;
import com.cy.tech.request.vo.enums.SubNoticeType;
import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.require.TrSubNoticeInfo;
import com.cy.tech.request.vo.require.othset.OthSet;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

/**
 * 子程序紀錄轉檔(有效功能至 2017/5/19 )
 *
 * @author kasim
 */
@Slf4j
@Controller
@Scope("view")
public class Set10TransSubNoticeModify implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6365923193276166659L;
    @Autowired
    private PtCheckRepo ptDao;
    @Autowired
    private WorkTestInfoRepo testinfoDao;
    @Autowired
    private WorkOnpgRepo onpgDao;
    @Autowired
    private OthSetRepo othsetDao;
    @Autowired
    private TrSubNoticeInfoRepository subNoticeInfoRepo;

    @Transactional(rollbackFor = Exception.class)
    public void btnTransSubNotice() {
        String msg = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String toDay = sdf.format(new Date());
        if (Integer.valueOf(toDay) > 20170519) {
            msg = "已超過可使用的有效期限";
            MessagesUtils.showError(msg);
            log.info(msg);
            return;
        }
        try {
            List<TrSubNoticeInfo> result1 = Lists.newArrayList();
            List<TrSubNoticeInfo> result2 = Lists.newArrayList();
            List<TrSubNoticeInfo> result3 = Lists.newArrayList();
            List<TrSubNoticeInfo> result4 = Lists.newArrayList();
            List<PtCheck> ptChecks = ptDao.findAll();
            List<WorkTestInfo> workTestInfos = testinfoDao.findAll();
            List<WorkOnpg> workOnpg = onpgDao.findAll();
            List<OthSet> othSets = othsetDao.findAll();

            ptChecks.forEach(each -> {
                TrSubNoticeInfo obj1 = this.createSubChangeRecord(
                        SubNoticeType.PT_NOTICE_MEMBER, each.getSourceSid(), each.getSourceNo(),
                        each.getSid(), each.getPtNo(), each.getNoticeMember(),
                        each.getUpdatedUser().getSid(), each.getUpdatedDate());
                if (obj1 != null) {
                    result1.add(obj1);
                }
                TrSubNoticeInfo obj2 = this.createSubChangeRecord(
                        SubNoticeType.PT_NOTICE_DEP, each.getSourceSid(), each.getSourceNo(),
                        each.getSid(), each.getPtNo(), each.getNoticeDeps(),
                        each.getUpdatedUser().getSid(), each.getUpdatedDate());
                if (obj2 != null) {
                    result1.add(obj2);
                }
            });

            workTestInfos.forEach(each -> {
                TrSubNoticeInfo obj = this.createSubChangeRecord(
                        SubNoticeType.TEST_INFO_DEP, each.getSourceSid(), each.getSourceNo(),
                        each.getSid(), each.getTestinfoNo(), each.getSendTestDep(),
                        each.getUpdatedUser().getSid(), each.getUpdatedDate());
                if (obj != null) {
                    result2.add(obj);
                }
            });

            workOnpg.forEach(each -> {
                TrSubNoticeInfo obj = this.createSubChangeRecord(
                        SubNoticeType.ONPG_INFO_DEP, each.getSourceSid(), each.getSourceNo(),
                        each.getSid(), each.getOnpgNo(), each.getNoticeDeps(),
                        each.getUpdatedUser().getSid(), each.getUpdatedDate());
                if (obj != null) {
                    result3.add(obj);
                }
            });

            othSets.forEach(each -> {
                TrSubNoticeInfo obj = this.createSubChangeRecord(
                        SubNoticeType.OS_INFO_DEP, each.getRequire().getSid(), each.getRequireNo(),
                        each.getSid(), each.getOsNo(), each.getNoticeDeps(),
                        each.getUpdatedUser().getSid(), each.getUpdatedDate());
                if (obj != null) {
                    result4.add(obj);
                }
            });
            msg = "原型確認資料：" + ptChecks.size() + "</br>";
            msg += "產生有效紀錄：" + result1.size() + "</br></br>";
            msg += "送測資料：" + workTestInfos.size() + "</br>";
            msg += "產生有效紀錄：" + result2.size() + "</br></br>";
            msg += "on程式資料：" + workOnpg.size() + "</br>";
            msg += "產生有效紀錄：" + result3.size() + "</br></br>";
            msg += "其他資料設定資料：" + othSets.size() + "</br>";
            msg += "產生有效紀錄：" + result4.size() + "</br></br>";

            List<TrSubNoticeInfo> result = Lists.newArrayList();
            result.addAll(result1);
            result.addAll(result2);
            result.addAll(result3);
            result.addAll(result4);
            subNoticeInfoRepo.save(result);
            log.info(msg);
            MessagesUtils.showInfo(msg);
        } catch (Exception e) {
            log.error("轉入子程序資料失敗", e);
            MessagesUtils.showError("轉入子程序資料失敗，請詳log!!");
        }
    }

    private TrSubNoticeInfo createSubChangeRecord(
            SubNoticeType type,
            String requireSid,
            String requireNo,
            String subProcessSid,
            String subProcessNo,
            JsonStringListTo noticeInfo,
            Integer createdUser,
            Date date) {
        noticeInfo = this.getJsonStringListTo(noticeInfo);
        JsonStringListTo dbInfo = this.getJsonStringListTo(null);
        if (this.checkHasDifference(dbInfo, noticeInfo)) {
            return new TrSubNoticeInfo(
                    UUID.randomUUID().toString(),
                    type,
                    requireSid,
                    requireNo,
                    subProcessSid,
                    subProcessNo,
                    dbInfo,
                    noticeInfo,
                    Activation.ACTIVE,
                    createdUser,
                    date,
                    createdUser,
                    date
            );
        }
        return null;
    }

    /**
     * 取得自訂物件
     *
     * @param to
     * @return
     */
    private JsonStringListTo getJsonStringListTo(JsonStringListTo to) {
        if (to == null) {
            return new JsonStringListTo();
        }
        if (to.getValue() == null) {
            return new JsonStringListTo();
        }
        return to;
    }

    /**
     * 檢查是否有差異
     *
     * @param oldTo
     * @param newTo
     * @return
     */
    private boolean checkHasDifference(JsonStringListTo oldTo, JsonStringListTo newTo) {
        if (oldTo.getValue().size() != newTo.getValue().size()) {
            return true;
        }
        return oldTo.getValue().stream()
                .anyMatch(each -> !newTo.getValue().contains(each));
    }
}
