/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.searchheader;

import com.cy.tech.request.vo.enums.OthSetStatus;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.web.controller.enums.ReqStatusMBean;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * ON程式一覽表 的搜尋表頭
 *
 * @author jason_h
 */
@Controller
@Scope("view")
public class Search19HeaderMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3870802223038604059L;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private ReqStatusMBean reqStatusUtils;

    /** 其它設定狀態 */
    @Getter
    @Setter
    private List<String> osStatus = Lists.newArrayList();
    /** 選擇的需求製作進度 */
    @Getter
    @Setter
    private RequireStatusType selectRequireStatusType;
    /** 選擇的主題 */
    @Getter
    @Setter
    private String selectOthSetTheme;

    @PostConstruct
    private void init() {
        this.clear();
    }

    /**
     * 清除/還原選項
     */
    public void clear() {
        this.initCommHeader();
        this.initHeader();
    }

    /**
     * 初始化commHeader
     */
    private void initCommHeader() {
        commHeaderMBean.clear();
        commHeaderMBean.clearAdvanceWithoutForwardAndUrgency();
        commHeaderMBean.initDefaultRequireDepts(true);
        commHeaderMBean.initDefaultNoticeDepts(true);
        this.initDefaultDateInterval();
    }

    /**
     * 初始化Header
     */
    private void initHeader() {
        selectRequireStatusType = null;
        selectOthSetTheme = "";
        this.initOsStatus();
    }

    private void initDefaultDateInterval() {
        commHeaderMBean.setStartDate(new LocalDate().minusDays(30).toDate());
        commHeaderMBean.setEndDate(new LocalDate().toDate());
    }

    private void initOsStatus() {
        osStatus = Lists.newArrayList();
        for (OthSetStatus s : OthSetStatus.values()) {
            osStatus.add(s.name());
        }
        osStatus.remove(OthSetStatus.FINISH.name());
    }

    public SelectItem[] getOsStatusItems() {
        OthSetStatus[] status = OthSetStatus.values();
        SelectItem[] items = new SelectItem[status.length];
        for (int i = 0; i < status.length; i++) {
            items[i] = new SelectItem(status[i].name(), status[i].getVal());
        }
        return items;
    }

    /**
     * 建立需求狀態查詢 List
     *
     * @return
     */
    public List<String> createQueryReqStatus() {
        return reqStatusUtils.createQueryReqStatus(this.selectRequireStatusType, this.getAllReqStatus());
    }

    /**
     * 取得此報表全部製作進度查詢
     *
     * @return
     */
    private List<RequireStatusType> getAllReqStatus() {
        return reqStatusUtils.createExcludeStatus(
                Lists.newArrayList(RequireStatusType.DRAFT)
        );
    }

    /**
     * 畫面用全部製作進度查詢
     *
     * @return
     */
    public SelectItem[] getReqStatusItems() {
        return reqStatusUtils.buildItem(this.getAllReqStatus());
    }

    /**
     * 建立需求狀態查詢 List
     *
     * @return
     */
    public List<String> createQueryAllReqStatus() {
        return this.getAllReqStatus().stream()
                .map(each -> each.name())
                .collect(Collectors.toList());
    }
}
