/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search25QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.Search25View;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.searchheader.CommonHeaderMBean;
import com.cy.tech.request.web.controller.searchheader.Search25HeaderMBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.view.component.ReportOrgTreeComponent;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.listener.ReportOrgTreeCallBack;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkUserWithRolesCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 執行狀狀況查詢
 *
 * @author jason_h
 */
@Controller
@Scope("view")
@Slf4j
public class Search25MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7541912567113405078L;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private Search25HeaderMBean headerMBean;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private Search25QueryService search25QueryService;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchResultHelper searchResultHelper;

    @Getter
    /** 報表 組織樹 Component */
    private ReportOrgTreeComponent orgTreeComponent;

    /** 查詢報表所指定小類名稱 */
    private final static String DBA_WORK = "DBA工作";

    /** 所有的需求單 */
    @Getter
    @Setter
    private List<Search25View> queryItems;
    /** 選擇的需求單 */
    @Getter
    @Setter
    private Search25View querySelection;
    /** 上下筆移動keeper */
    @Getter
    private Search25View queryKeeper;
    /** 切換模式 */
    @Getter
    private SwitchType switchType = SwitchType.CONTENT;
    /** 切換模式 - 全畫面狀態 */
    @Getter
    private SwitchType switchFullType = SwitchType.DETAIL;
    /** 在匯出的時候，某些內容需要隱藏 */
    @Getter
    private boolean hasDisplay = true;
    @Getter
    private final String dataTableId = "dtRequire";
    @Getter
    @Setter
    /** 需求單位 */
    private List<String> requireDepts;
    @Getter
    @Setter
    /** 緊急度 */
    private UrgencyType urgency;
    @Getter
    @Setter
    /** 週三維護 */
    private Boolean hasWedMaintain;

    @PostConstruct
    public void init() {
        requireDepts = Lists.newArrayList();
        urgency = null;
        hasWedMaintain = null;

        // 查詢登入者所有角色
        List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
                SecurityFacade.getUserSid(),
                SecurityFacade.getCompanyId());

        this.orgTreeComponent = new ReportOrgTreeComponent(
                loginBean.getCompanyId(), loginBean.getDep(), roleSids,
                true, reportOrgTreeCallBack);

        this.search();
    }

    public void search() {
        queryItems = this.findWithQuery();
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        requireDepts = Lists.newArrayList();
        urgency = null;
        hasWedMaintain = null;
        headerMBean.clear();
        this.search();
    }

    private List<Search25View> findWithQuery() {
        String requireNo = reqularUtils.getRequireNo(loginBean.getCompanyId(), commHeaderMBean.getFuzzyText());
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT "
                + "tr.require_sid,"
                + "tr.require_no,"
                + "tid.field_content,"
                + "tr.urgency,"
                + "tr.hope_dt,"
                + "tr.create_dt,"
                + "ckm.big_category_name,"
                + "ckm.middle_category_name,"
                + "ckm.small_category_name,"
                + "tr.dep_sid,"
                + "tr.create_usr,"
                + "tr.require_status,"
                + "tid2.wednesdayMaintain, "
                // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
                + this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire()

                + "FROM ");
        buildRequireCondition(requireNo, builder, parameters);
        buildRequireIndexDictionaryCondition(requireNo, builder, parameters);
        buildCategoryKeyMappingCondition(builder, parameters);
        // 檢查項目 (系統別)
        // 後方需對 主單 sid 做 group by
        builder.append(this.searchConditionSqlHelper.prepareCommonJoin(SecurityFacade.getUserSid()));

        builder.append("WHERE tr.require_sid IS NOT NULL ");
        builder.append(" GROUP BY tr.require_sid ");
        builder.append("  ORDER BY tr.create_dt ");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.EXECUTION_SITUATION, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.EXECUTION_SITUATION, SecurityFacade.getUserSid());

        // 查詢
        List<Search25View> resultList = search25QueryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // ====================================
        // 後續處理
        // ====================================
        if (WkStringUtils.notEmpty(resultList)) {
            // 後續處理-開始
            usageRecord.afterProcessStart();
            // 查詢條件過濾：系統別
            resultList = resultList.stream()
                    .filter(each -> this.searchResultHelper.filterCheckItems(
                            each.getCheckItemTypes(),
                            commHeaderMBean.getCheckItemTypes(),
                            false,
                            null))
                    .collect(Collectors.toList());
            usageRecord.afterProcessEnd();
        }

        // 儲存使用記錄
        usageRecord.saveUsageRecord();
        return resultList;

    }

    private void buildRequireCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("(SELECT * FROM tr_require tr WHERE 1=1 ");
        // 需求單號
        if (!Strings.isNullOrEmpty(requireNo)) {
            builder.append(" AND tr.require_no = :requireNo");
            parameters.put("requireNo", requireNo);
        }
        // 立單區間
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getStartDate() != null && commHeaderMBean.getEndDate() != null) {
            builder.append(" AND tr.create_dt BETWEEN :startDate AND :endDate");
            parameters.put("startDate", helper.transStartDate(commHeaderMBean.getStartDate()));
            parameters.put("endDate", helper.transEndDate(commHeaderMBean.getEndDate()));
        }
        // 期望完成區間
        if (Strings.isNullOrEmpty(requireNo) && headerMBean.getHopeStartDate() != null && headerMBean.getHopeEndDate() != null) {
            builder.append(" AND tr.hope_dt BETWEEN :hopeStartDate AND :hopeEndDate");
            parameters.put("hopeStartDate", helper.transStartDate(headerMBean.getHopeStartDate()));
            parameters.put("hopeEndDate", helper.transEndDate(headerMBean.getHopeEndDate()));
        } else if (Strings.isNullOrEmpty(requireNo) && headerMBean.getHopeEndDate() != null) {
            builder.append(" AND tr.hope_dt <= :hopeEndDate");
            parameters.put("hopeEndDate", helper.transEndDate(headerMBean.getHopeEndDate()));
        }
        // 需求類別製作進度
        builder.append(" AND tr.require_status IN (:requireStatus)");
        if (Strings.isNullOrEmpty(requireNo)) {
            parameters.put("requireStatus", headerMBean.createQueryReqStatus());
        } else {
            parameters.put("requireStatus", headerMBean.createQueryAllReqStatus());
        }
        // 需求人員
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(commHeaderMBean.getTrCreatedUserName())) {
            List<Integer> userSids = WkUserUtils.findByNameLike(commHeaderMBean.getTrCreatedUserName(), SecurityFacade.getCompanyId());
            if (WkStringUtils.isEmpty(userSids)) {
                userSids.add(-999);
            }
            builder.append(" AND tr.create_usr IN (:userSids)");
            parameters.put("userSids", userSids.isEmpty() ? "" : userSids);
        }
        // 需求單位
        if (Strings.isNullOrEmpty(requireNo) && requireDepts != null && !requireDepts.isEmpty()) {
            builder.append(" AND tr.dep_sid IN (:depSids)");
            parameters.put("depSids", requireDepts);
        }
        // 緊急度
        if (Strings.isNullOrEmpty(requireNo) && urgency != null) {
            if (UrgencyType.GENERAL.equals(urgency)) {
                builder.append(" AND tr.urgency = 0");
            } else {
                builder.append(" AND tr.urgency = 1");
            }
        }
        builder.append(") AS tr ");
    }

    private void buildRequireIndexDictionaryCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        // 模糊搜尋
        builder.append("INNER JOIN (SELECT tid.require_sid,tid.field_content FROM tr_index_dictionary tid WHERE 1=1");
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.isFuzzyQuery()) {
            String text = "%" + reqularUtils.replaceIllegalSqlLikeStr(commHeaderMBean.getFuzzyText()) + "%";
            builder.append(" AND (tid.require_sid"
                    + " IN (SELECT DISTINCT tid1.require_sid FROM tr_index_dictionary tid1 WHERE tid1.field_content LIKE :text)"
                    + " OR tid.require_sid IN (SELECT DISTINCT trace.require_sid FROM tr_require_trace trace WHERE "
                    + "trace.require_trace_type = 'REQUIRE_INFO_MEMO' AND trace.require_trace_content LIKE :text))");
            parameters.put("text", text);
        }
        builder.append(" AND tid.field_name='主題') AS tid ON tr.require_sid=tid.require_sid ");
        // 是否為週三維護
        builder.append("INNER JOIN (SELECT require_sid,field_content AS wednesdayMaintain FROM tr_index_dictionary ");
        builder.append(" WHERE field_name='是否為週三維護' ");
        if (Strings.isNullOrEmpty(requireNo) && hasWedMaintain != null) {
            if (hasWedMaintain) {
                builder.append(" AND field_content like '[ \"true\",%'");
            } else {
                builder.append(" AND field_content like '[ \"false\",%'");
            }
        }
        builder.append(") AS tid2 ON tid.require_sid=tid2.require_sid ");
    }

    private void buildCategoryKeyMappingCondition(StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_category_key_mapping ckm WHERE ckm.small_category_name = :smallName ");
        parameters.put("smallName", DBA_WORK);
        builder.append(") AS ckm ON tr.mapping_sid=ckm.key_sid ");
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Search25View view) {
        view.setReadRecordType(ReadRecordType.HAS_READ);
        Require r = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(r, loginBean.getUser());
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (Search25View) event.getObject();
        this.changeRequireContent(this.queryKeeper);
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Search25View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        helper.exportExcel(document, commHeaderMBean.getStartDate(), commHeaderMBean.getEndDate(), commHeaderMBean.getReportType());
        hasDisplay = true;
    }

    /**
     * 隱藏部分column裡的內容
     */
    public void hideColumnContent() {
        hasDisplay = false;
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search25View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            display.update("headerTitle");
            display.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Search25View to) {
        querySelection = to;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    /**
     * 開啟 單位挑選 組織樹
     */
    public void btnOpenOrgTree() {
        try {
            orgTreeComponent.initOrgTree(loginBean.getComp(), requireDepts, false, true);
            display.showPfWidgetVar("dlgOrgTree");
        } catch (Exception e) {
            log.error("btnOpenOrgTree Error", e);
            MessagesUtils.showError(e.getMessage());
        }
    }

    /** 報表 組織樹 Component CallBack */
    private final ReportOrgTreeCallBack reportOrgTreeCallBack = new ReportOrgTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -7693823488340165707L;

        @Override
        public void showMessage(String m) {
            MessagesUtils.showError(m);
        }

        @Override
        public void confirmSelOrg() {
            requireDepts = orgTreeComponent.getSelOrgSids();
        }
    };

}
