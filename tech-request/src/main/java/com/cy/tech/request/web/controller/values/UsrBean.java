/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.values;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.logic.service.send.test.QaAliasService;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 前端頁面取得組織相關資訊用
 *
 * @author shaun
 */
@Slf4j
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class UsrBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6987282366639811578L;
    @Autowired
    transient private UserService userService;
    @Autowired
    private QaAliasService qaAliasService;

    /**
     * AutoComplete 用：使用者名稱
     *
     * @param query
     * @return
     */
    public List<User> autoCompleteForUserName(String query) {
        try {
            return userService.findByUserNameLike(query);
        } catch (Exception ex) {
            log.error("autoCompleteForUserName >> " + ex.getMessage(), ex);
            MessagesUtils.showError("發生錯誤");
            return Lists.newArrayList();
        }
    }

    /**
     * 取得成員名稱 <BR/>
     * accept class <BR/>
     * User、String、Integer
     *
     * @param obj
     * @return
     */
    public String name(Object obj) {
        if (obj == null) {
            return "";
        }
        String result = userService.getUserName(obj);
        if (Strings.isNullOrEmpty(result)) {
            log.error(Faces.getViewId() + " find user name by error class : " + obj.getClass().getSimpleName());
            return "";
        }
        return result;
    }
    
    public String names(String userSids) {
        if (userSids == null) {
            return "";
        }
        String[] arr = userSids.split(",");
        List<String> userNames = Lists.newArrayList();
        for(String sid : arr){
            userNames.add(userService.getUserName(sid));
        }
        return String.join("、", userNames);
    }
    
    /**
     * 取得暱稱
     * @param userSids
     * @return
     */
    public String aliasNames(String userSids) {
        return qaAliasService.convertToQaAliasNames(userSids);
    }

}
