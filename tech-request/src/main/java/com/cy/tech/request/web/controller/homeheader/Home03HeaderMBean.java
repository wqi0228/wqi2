/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.homeheader;

import com.cy.tech.request.vo.enums.ForwardType;
import com.cy.tech.request.web.controller.searchheader.*;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.logic.enumerate.DateType;
import com.cy.tech.request.web.controller.enums.ReqStatusMBean;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 需求單位簽核進度查詢的搜尋表頭
 *
 * @author jason_h
 */
@Controller
@Scope("view")
public class Home03HeaderMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5648171912627275023L;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private ReqStatusMBean reqStatusUtils;

    /** 選擇的需求製作進度 */
    @Getter
    @Setter
    private RequireStatusType selectRequireStatusType;
    /** 選擇的閱讀類型 */
    @Getter
    @Setter
    private ReadRecordType selectReadRecordType;
    /** 選擇的寄發類型 */
    @Getter
    @Setter
    private String selectForwardType;
    /** 收件者 */
    @Getter
    @Setter
    private String receive;
    /** 主題 */
    @Getter
    @Setter
    private String theme;
    /** 內容 */
    @Getter
    @Setter
    private String content;

    /**
     * 清除/還原選項
     */
    public void clear() {
        selectReadRecordType = null;
        selectForwardType = null;
        receive = null;
        theme = null;
        content = null;
        commHeaderMBean.clear();
        this.clearAdvance();
        this.getDateInterval(DateType.TODAY.name());
        //不給預設值否則會造成收不到個人
        //commHeaderMBean.initReqDeptsByComp(false);
    }

    /**
     * 寄發類型
     *
     * @return
     */
    public SelectItem[] getForwardTypeItems() {
        ForwardType[] types = ForwardType.values();
        SelectItem[] items = new SelectItem[types.length];
        for (int i = 0; i < types.length; i++) {
            items[i] = new SelectItem(types[i], types[i].getValue());
        }
        return items;
    }

    public boolean isRequireIndexDictionaryCondition() {
        return !Strings.isNullOrEmpty(commHeaderMBean.getFuzzyText()) || !Strings.isNullOrEmpty(theme) || !Strings.isNullOrEmpty(content);
    }

    public boolean isAdvanceCondition() {
        //立單區間
        if (commHeaderMBean.getStartDate() != null && commHeaderMBean.getEndDate() != null) {
            return true;
        }
        //需求製作進度
        if (selectRequireStatusType != null) {
            return true;
        }
        //緊急度
        if (commHeaderMBean.getUrgencyList() != null && !commHeaderMBean.getUrgencyList().isEmpty()) {
            return true;
        }
        // 需求單號
        if (!Strings.isNullOrEmpty(commHeaderMBean.getRequireNo())) {
            return true;
        }
        return false;
    }

    /**
     * 取得日期區間
     *
     * @param dateType
     */
    public void getDateInterval(String dateType) {
        if (DateType.PREVIOUS_MONTH.name().equals(dateType)) {
            if (commHeaderMBean.getStartUpdatedDate() == null) {
                commHeaderMBean.setStartUpdatedDate(new Date());
            }
            LocalDate lastDate = new LocalDate(commHeaderMBean.getStartUpdatedDate()).minusMonths(1);
            commHeaderMBean.setStartUpdatedDate(lastDate.dayOfMonth().withMinimumValue().toDate());
            commHeaderMBean.setEndUpdatedDate(lastDate.dayOfMonth().withMaximumValue().toDate());
            commHeaderMBean.setDateTypeIndex(DateType.PREVIOUS_MONTH.ordinal());
        } else if (DateType.THIS_MONTH.name().equals(dateType)) {
            commHeaderMBean.setStartUpdatedDate(new LocalDate().dayOfMonth().withMinimumValue().toDate());
            commHeaderMBean.setEndUpdatedDate(new LocalDate().dayOfMonth().withMaximumValue().toDate());
            commHeaderMBean.setDateTypeIndex(DateType.THIS_MONTH.ordinal());
        } else if (DateType.NEXT_MONTH.name().equals(dateType)) {
            if (commHeaderMBean.getStartUpdatedDate() == null) {
                commHeaderMBean.setStartUpdatedDate(new Date());
            }
            LocalDate nextDate = new LocalDate(commHeaderMBean.getStartUpdatedDate()).plusMonths(1);
            commHeaderMBean.setStartUpdatedDate(nextDate.dayOfMonth().withMinimumValue().toDate());
            commHeaderMBean.setEndUpdatedDate(nextDate.dayOfMonth().withMaximumValue().toDate());
            commHeaderMBean.setDateTypeIndex(DateType.NEXT_MONTH.ordinal());
        } else if (DateType.TODAY.name().equals(dateType)) {
            commHeaderMBean.setStartUpdatedDate(new Date());
            commHeaderMBean.setEndUpdatedDate(new Date());
            commHeaderMBean.setDateTypeIndex(DateType.TODAY.ordinal());
        } else if (DateType.NULL.name().equals(dateType)) {
            commHeaderMBean.setStartUpdatedDate(null);
            commHeaderMBean.setEndUpdatedDate(null);
            commHeaderMBean.setDateTypeIndex(DateType.NULL.ordinal());
        }
    }

    /**
     * 清除進階選項
     */
    public void clearAdvance() {
        commHeaderMBean.setStartDate(null);
        commHeaderMBean.setEndDate(null);
        selectRequireStatusType = null;
        commHeaderMBean.clearAdvance();
    }

    /**
     * 建立需求狀態查詢 List
     *
     * @return
     */
    public List<String> createQueryReqStatus() {
        return reqStatusUtils.createQueryReqStatus(this.selectRequireStatusType, this.getAllReqStatus());
    }

    /**
     * 取得此報表全部製作進度查詢
     *
     * @return
     */
    private List<RequireStatusType> getAllReqStatus() {
        return reqStatusUtils.createExcludeStatus(
                Lists.newArrayList(RequireStatusType.DRAFT)
        );
    }

    /**
     * 畫面用全部製作進度查詢
     *
     * @return
     */
    public SelectItem[] getReqStatusItems() {
        return reqStatusUtils.buildItem(this.getAllReqStatus());
    }

    /**
     * 建立需求狀態查詢 List
     *
     * @return
     */
    public List<String> createQueryAllReqStatus() {
        return this.getAllReqStatus().stream()
                .map(each -> each.name())
                .collect(Collectors.toList());
    }
}
