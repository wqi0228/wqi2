package com.cy.tech.request.web.controller.component.singleselectlist;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;

/**
 * 單選列表選擇器
 * 
 * @author allen1214_wu
 */
public class SingleSelectListPicker implements Serializable {

	// ========================================================================
	// 服務元件區
	// ========================================================================

	/**
     * 
     */
    private static final long serialVersionUID = 4459583413997492338L;

    // ========================================================================
	// 變數區
	// ========================================================================
	/**
	 * 所有項目
	 */
	@Getter
	private List<WkItem> keepAllItems;

	/**
	 * 所有項目
	 */
	@Getter
	private List<WkItem> allItems;

	/**
	 * 已選擇項目
	 */
	@Getter
	@Setter
	private WkItem selectedItem;

	/**
	 * 搜尋關鍵字
	 */
	@Getter
	@Setter
	private String serachItemNameKeyword;

	/**
	 * 
	 */
	@Getter
	private SingleSelectListCallback singleSelectListCallback;

	// ========================================================================
	// 方法區
	// ========================================================================
	/**
	 * 建構子
	 * 
	 * @param config 建構參數容器
	 */
	public SingleSelectListPicker(SingleSelectListPickerConfig singleSelectListPickerConfig) {

		// ====================================
		// 取得傳入參數
		// ====================================
		this.singleSelectListCallback = singleSelectListPickerConfig.getSingleSelectListCallback();

		// ====================================
		// 由外部取得所有項目
		// ====================================
		this.allItems = this.singleSelectListCallback.prepareAllItems();
		if (this.allItems == null) {
			this.allItems = Lists.newArrayList();
		}
		// 備份一份
		this.keepAllItems = Lists.newArrayList(this.allItems);

		// ====================================
		// 預設選取項目
		// ====================================
		if (WkStringUtils.notEmpty(singleSelectListPickerConfig.getDefaultSelectedItemSid())) {
			for (WkItem wkItem : allItems) {
				if (WkCommonUtils.compareByStr(wkItem.getSid(), singleSelectListPickerConfig.getDefaultSelectedItemSid())) {
					this.selectedItem = wkItem;
				}
			}
		}
	}

	/**
	 * 回傳已選擇項目的 Sid (未選擇時，回傳 null )
	 * 
	 * @return depSid
	 */
	public String getSelectedSid() {

		WkItem selectedItem = this.getSelectedWkItem();
		if (selectedItem == null || WkStringUtils.isEmpty(selectedItem.getSid())) {
			return null;
		}

		return selectedItem.getSid();
	}

	/**
	 * @return
	 */
	public WkItem getSelectedWkItem() {
		return this.selectedItem;
	}

	/**
	 * 事件：搜尋
	 */
	public void event_searchItemNameByKeyword() {
		// 重新轉換查詢關鍵字, 忽略大小寫差異
		String keyword = WkStringUtils.safeTrim(serachItemNameKeyword).toUpperCase();

		if (WkStringUtils.isEmpty(keyword)) {
			// 關鍵字為空時，顯示全部 (取回原本保存的那一份)
			this.allItems = Lists.newArrayList(this.keepAllItems);
		} else {
			// 重新由備份過濾出最新的
			this.allItems = this.keepAllItems.stream()
			        .filter(wkItem -> {
			            // 保留已選擇的
			            if (WkCommonUtils.compareByStr(wkItem.getSid(), this.getSelectedSid())) {
					        return true;
				        }
			            // 依據輸入的關鍵字過濾
			            return WkStringUtils.safeTrim(wkItem.getName()).toUpperCase().contains(keyword);
			        })
			        .collect(Collectors.toList());
		}
	}

	/**
	 * 事件： 擊點項目時的動作
	 */
	public void event_clickItem() {
		if (this.singleSelectListCallback != null) {
			this.singleSelectListCallback.clickItem();
		}
	}
}
