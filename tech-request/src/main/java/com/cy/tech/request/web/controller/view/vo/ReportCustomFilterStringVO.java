/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.vo;

import com.cy.tech.request.vo.enums.SearchReportCustomEnum;
import com.cy.tech.request.vo.enums.SearchReportCustomType;
import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */
public class ReportCustomFilterStringVO implements ReportCustomFilterDetailVO {

    @Getter
    private SearchReportCustomEnum searchReportCustomEnum;
    @Getter
    private String value;

    public ReportCustomFilterStringVO(SearchReportCustomEnum searchReportCustomEnum, String value) {
        this.searchReportCustomEnum = searchReportCustomEnum;
        this.value = value;
    }

    public String getJsonValue() {
        return value;
    }

    public SearchReportCustomType getSearchReportCustomType() {
        return SearchReportCustomType.STRING;
    }

}
