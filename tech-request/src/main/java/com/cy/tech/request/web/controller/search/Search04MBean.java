/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.view.Search04View;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.setting.SettingCheckConfirmRightService;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.require.RequireForwardDeAndPersonMBean;
import com.cy.tech.request.web.controller.search.helper.Search04Helper;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.view.component.CategoryTreeComponent;
import com.cy.tech.request.web.controller.view.component.ReportOrgTreeComponent;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.listener.CategoryTreeCallBack;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.listener.ReportCustomFilterCallback;
import com.cy.tech.request.web.listener.ReportOrgTreeCallBack;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.tech.request.web.view.to.search.query.SearchQuery04;
import com.cy.tech.request.web.view.to.search.query.SearchQuery04CustomFilter;
import com.cy.work.common.cache.WkUserWithRolesCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 退件資訊查詢
 *
 * @author jason_h
 */
@Controller
@Scope("view")
@Slf4j
public class Search04MBean implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 8834309126947609055L;
    @Autowired
	transient private LoginBean loginBean;
	@Autowired
	transient private Search04Helper searchHelper;
	@Autowired
	transient private TableUpDownBean upDownBean;
	@Autowired
	transient private ReqLoadBean loadManager;
	@Autowired
	transient private SearchHelper helper;
	@Autowired
	transient private RequireService requireService;
	@Autowired
	transient private SettingCheckConfirmRightService settingCheckConfirmRightService;
	@Autowired
	transient private DisplayController display;
	@Autowired
	transient private Require01MBean r01MBean;
	@Autowired
	transient private ReportCustomFilterLogicComponent reportCustomFilterLogicComponent;
	@Getter
	/** 類別樹 Component */
	private CategoryTreeComponent categoryTreeComponent;
	@Getter
	/** 報表 組織樹 Component */
	private ReportOrgTreeComponent orgTreeComponent;

	@Getter
	/** 切換模式 - 全畫面狀態 */
	private SwitchType switchFullType = SwitchType.DETAIL;
	@Getter
	/** 切換模式 */
	private SwitchType switchType = SwitchType.CONTENT;
	@Getter
	/** 列表 id */
	private final String dataTableId = "dtRequire";
	@Getter
	/** 在匯出的時候，某些內容需要隱藏 */
	private boolean hasDisplay = true;
	@Getter
	/** 查詢物件 */
	private SearchQuery04 searchQuery;

	@Getter
	@Setter
	/** 所有的需求單 */
	private List<Search04View> queryItems;
	@Getter
	@Setter
	/** 選擇的需求單 */
	private Search04View querySelection;
	@Getter
	/** 上下筆移動keeper */
	private Search04View queryKeeper;
	@Getter
	private SearchQuery04CustomFilter searchQuery04CustomFilter;

	@PostConstruct
	public void init() {
		this.initComponents();
		this.clear();
	}

	/**
	 * 初始化 元件
	 */
	private void initComponents() {
		
		//查詢登入者所有角色
		List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
			SecurityFacade.getUserSid(),
			SecurityFacade.getCompanyId());	
		
		this.searchQuery = new SearchQuery04(ReportType.REJECT);
		this.categoryTreeComponent = new CategoryTreeComponent(categoryTreeCallBack);
		this.orgTreeComponent = new ReportOrgTreeComponent(
		        loginBean.getCompanyId(),
		        loginBean.getDep(),
		        roleSids,
		        true,
		        reportOrgTreeCallBack);

		this.searchQuery04CustomFilter = new SearchQuery04CustomFilter(
		        loginBean.getCompanyId(),
		        loginBean.getUserSId(),
		        loginBean.getDep().getSid(),
		        loginBean.getComp().getSid(),
		        reportCustomFilterLogicComponent,
		        messageCallBack,
		        display,
		        reportCustomFilterCallback,
		        roleSids,
		        loginBean.getDep());
	}

	public void openDefaultSetting() {
		// 查詢登入者所有角色
		List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
		        SecurityFacade.getUserSid(),
		        SecurityFacade.getCompanyId());

		try {
			this.searchQuery04CustomFilter.loadDefaultSetting(
			        searchHelper.getDefaultDepSids(
			                loginBean.getDep().getSid(),
			                roleSids),
			        this.searchQuery04CustomFilter.getTempSearchQuery04());
		} catch (Exception e) {
			log.error("openDefaultSetting ERROR", e);
		}
	}

	/**
	 * 還原預設值並查詢
	 */
	public void clear() {
		this.clearQuery();
		this.search();
	}

	/**
	 * 清除/還原選項
	 */
	private void clearQuery() {
		try {

			// 查詢登入者所有角色
			List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
			        SecurityFacade.getUserSid(),
			        SecurityFacade.getCompanyId());

			this.searchQuery04CustomFilter.loadDefaultSetting(
			        searchHelper.getDefaultDepSids(
			                loginBean.getDep().getSid(),
			                roleSids),
			        this.searchQuery);
			categoryTreeComponent.clearCate();
			categoryTreeCallBack.loadSelCate(searchQuery.getSmallDataCateSids());
		} catch (Exception e) {
			log.error("openDefaultSetting ERROR", e);
		}
	}

	public void search() {
		queryItems = searchHelper.search(
		        loginBean.getCompanyId(),
		        loginBean.getDep(),
		        loginBean.getUser(),
		        searchQuery);
	}

	/**
	 * 取得 關聯檢視 網址
	 *
	 * @param view
	 * @return
	 */
	public String getRelevanceViewUrl(Search04View view) {
		return searchHelper.getRelevanceViewUrl(view);
	}

	/**
	 * 隱藏部分column裡的內容
	 */
	public void hideColumnContent() {
		hasDisplay = false;
	}

	/**
	 * 匯出excel
	 *
	 * @param document
	 */
	public void exportExcel(Object document) {
		helper.exportExcel(document, searchQuery.getStartDate(), searchQuery.getEndDate(), searchQuery.getReportType());
		hasDisplay = true;
	}

	/**
	 * 半版row選擇
	 *
	 * @param event
	 */
	public void onRowSelect(SelectEvent event) {
		this.queryKeeper = this.querySelection = (Search04View) event.getObject();
		this.changeRequireContent(this.queryKeeper);
	}

	/**
	 * 列表點選轉寄功能
	 * 
	 * @param view
	 */
	public void openForwardDialog(Search04View view) {
	    this.queryKeeper = this.querySelection = view;

	    if (SwitchType.DETAIL.equals(switchType)) {
	        this.changeRequireContent(this.queryKeeper);
	        this.display.update(Lists.newArrayList(
	                "@(.reportUpdateClz)",
	                "title_info_click_btn_id",
	                "require01_title_info_id",
	                "require_template_id",
	                "viewPanelBottomInfoId",
	                "req03botmid",
	                this.dataTableId + "1"));
	    }

	    RequireForwardDeAndPersonMBean mbean = WorkSpringContextHolder.getBean(RequireForwardDeAndPersonMBean.class);
	    mbean.openDialog(view.getRequireNo(), r01MBean);
	}

	/**
	 * 列表執行關聯動作
	 *
	 * @param view
	 */
	public void initRelevance(Search04View view) {
		this.querySelection = view;
		this.queryKeeper = this.querySelection;
		Require r = requireService.findByReqNo(view.getRequireNo());
		this.r01MBean.setRequire(r);
		this.r01MBean.getRelevanceMBean().initRelevance();
	}

	/**
	 * 列表執行追蹤動作
	 *
	 * @param require01MBean
	 * @param view
	 */
	public void btnAddTrack(Search04View view) {
		Require r = this.highlightAndReturnRequire(view);
		this.r01MBean.setRequire(r);
		this.r01MBean.getTraceActionMBean().initTrace(this.r01MBean);
	}

	/**
	 * 列表標註及回傳需求單
	 *
	 * @param view
	 * @return
	 */
	private Require highlightAndReturnRequire(Search04View view) {
		this.querySelection = view;
		this.queryKeeper = this.querySelection;
		return requireService.findByReqNo(view.getRequireNo());
	}

	/**
	 * 開啟分頁
	 *
	 * @param dtId
	 * @param widgetVar
	 * @param pageCount
	 * @param to
	 */
	public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search04View to) {
		this.highlightReportTo(widgetVar, pageCount, to);
		this.resetUpdownInfo();
		this.checkHelfScreen();
	}

	/**
	 * highlight列表位置
	 *
	 * @param widgetVar
	 * @param pageCount
	 * @param to
	 */
	private void highlightReportTo(String widgetVar, String pageCount, Search04View to) {
		querySelection = to;
		queryKeeper = querySelection;
		display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
	}

	/**
	 * 取得索引位置
	 *
	 * @param pageCountStr
	 * @return
	 */
	private int getRowIndex(String pageCountStr) {
		Integer pageCount = 50;
		if (!Strings.isNullOrEmpty(pageCountStr)) {
			try {
				pageCount = Integer.valueOf(pageCountStr);
			} catch (Exception e) {
				log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
			}
		}
		return queryItems.indexOf(querySelection) % pageCount;
	}

	/**
	 * 重設定上下筆資訊
	 */
	private void resetUpdownInfo() {
		upDownBean.setCurrRow(queryKeeper.getRequireNo());
		upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
	}

	private boolean checkHelfScreen() {
		if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
		        || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
			this.normalScreenReport();
			display.update("headerTitle");
			display.update("searchBody");
			return true;
		}
		return false;
	}

	/**
	 * 切換 - 返回報表
	 */
	public void normalScreenReport() {
		this.querySelection = this.queryKeeper;
		switchFullType = SwitchType.DETAIL;
		this.toggleSearchBody();
	}

	/**
	 * 切換 - 全畫面需求單
	 *
	 * @param view
	 */
	public void fullScreenForm(Search04View view) {
		this.queryKeeper = this.querySelection = view;
		switchFullType = SwitchType.FULLCONTENT;
		this.toggleSearchBody();
	}

	/**
	 * 切換查詢表身
	 */
	public void toggleSearchBody() {
		if (switchType.equals(SwitchType.CONTENT)) {
			switchType = SwitchType.DETAIL;
			if (querySelection != null) {
				queryKeeper = querySelection;
			} else if (this.queryKeeper == null) {
				this.querySelection = this.queryKeeper = this.queryItems.get(0);
			}
			this.changeRequireContent(queryKeeper);
			return;
		}
		if (switchType.equals(SwitchType.DETAIL)) {
			switchFullType = SwitchType.DETAIL;
			switchType = SwitchType.CONTENT;
		}
	}

	/**
	 * 變更需求單內容
	 *
	 * @param view
	 */
	private void changeRequireContent(Search04View view) {
	    view.setReadRecordType(ReadRecordType.HAS_READ);
		Require r = requireService.findByReqNo(view.getRequireNo());
		loadManager.reloadReqForm(r, loginBean.getUser());
	}

	/**
	 * 上一筆（分頁）
	 *
	 * @param dtId
	 * @param widgetVar
	 */
	public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
		int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
		if (index > 0) {
			index--;
			querySelection = queryItems.get(index);
		}
		this.refreshViewByOpener(dtId, widgetVar, pageCount);
	}

	/**
	 * 下一筆（分頁）
	 *
	 * @param dtId
	 * @param widgetVar
	 */
	public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
		int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
		if (queryItems.size() > index + 1) {
			index++;
			querySelection = queryItems.get(index);
		}
		this.refreshViewByOpener(dtId, widgetVar, pageCount);
	}

	/**
	 * 刷新列表（分頁）
	 *
	 * @param dtId
	 * @param widgetVar
	 * @param pageCount
	 */
	private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
		queryKeeper = querySelection;
		this.highlightReportTo(widgetVar, pageCount, queryKeeper);
		this.resetUpdownInfo();
		this.checkHelfScreen();
	}

	/**
	 * 上下筆移動
	 *
	 * @param action
	 */
	public void moveRequireTemplateSelect(int action) {
		int index = this.queryItems.indexOf(this.queryKeeper);
		index += action;
		if (index < 0 || index >= this.queryItems.size()) {
			return;
		}
		this.querySelection = this.queryKeeper = this.queryItems.get(index);
		this.changeRequireContent(this.querySelection);
	}

	/**
	 * 開啟 類別樹
	 */
	public void btnOpenCategoryTree() {
		try {
			categoryTreeComponent.init();
			display.showPfWidgetVar("dlgCate");
		} catch (Exception e) {
			log.error("btnOpenCategoryTree Error", e);
			messageCallBack.showMessage(e.getMessage());
		}
	}

	/**
	 * 開啟 單位挑選 組織樹
	 */
	public void btnOpenOrgTree() {
		try {
			Org org = loginBean.getDep();
			if (this.settingCheckConfirmRightService.hasCanCheckUserRight(SecurityFacade.getUserSid())) {
				org = loginBean.getComp();
			}
			orgTreeComponent.initOrgTree(org, searchQuery.getRequireDepts(), true, false);
			display.showPfWidgetVar("dlgOrgTree");
		} catch (Exception e) {
			log.error("btnOpenOrgTree Error", e);
			messageCallBack.showMessage(e.getMessage());
		}
	}

	/** 訊息呼叫 */
	private final MessageCallBack messageCallBack = new MessageCallBack() {
		/**
         * 
         */
        private static final long serialVersionUID = 2938875743603789940L;

        @Override
		public void showMessage(String m) {
			MessagesUtils.showError(m);
		}
	};

	/** 類別樹 Component CallBack */
	private final CategoryTreeCallBack categoryTreeCallBack = new CategoryTreeCallBack() {
		/**
         * 
         */
        private static final long serialVersionUID = 658648532843156711L;

        @Override
		public void showMessage(String m) {
			messageCallBack.showMessage(m);
		}

		@Override
		public void confirmSelCate() {
			categoryTreeComponent.selCate();
			searchQuery.setBigDataCateSids(categoryTreeComponent.getBigDataCateSids());
			searchQuery.setMiddleDataCateSids(categoryTreeComponent.getMiddleDataCateSids());
			searchQuery.setSmallDataCateSids(categoryTreeComponent.getSmallDataCateSids());
			categoryTreeComponent.clearCateSids();
		}

		@Override
		public void loadSelCate(List<String> smallDataCateSids) {
			categoryTreeComponent.init();
			categoryTreeComponent.selectedItem(smallDataCateSids);
			this.confirmSelCate();
		}

	};

	/** 報表 組織樹 Component CallBack */
	private final ReportOrgTreeCallBack reportOrgTreeCallBack = new ReportOrgTreeCallBack() {
		/**
         * 
         */
        private static final long serialVersionUID = 1165262819021677793L;

        @Override
		public void showMessage(String m) {
			messageCallBack.showMessage(m);
		}

		@Override
		public void confirmSelOrg() {
			searchQuery.setRequireDepts(orgTreeComponent.getSelOrgSids());
		}
	};

	private final ReportCustomFilterCallback reportCustomFilterCallback = new ReportCustomFilterCallback() {
		/**
         * 
         */
        private static final long serialVersionUID = -8145653614987769228L;

        @Override
		public void reloadDefault(String index) {
			
			//查詢登入者所有角色
			List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
				SecurityFacade.getUserSid(),
				SecurityFacade.getCompanyId());	
			
			searchQuery04CustomFilter.loadDefaultSetting(
			        searchHelper.getDefaultDepSids(
			                loginBean.getDep().getSid(),
			                roleSids),
			        searchQuery);
			categoryTreeCallBack.loadSelCate(searchQuery.getSmallDataCateSids());
		}
	};

}
