package com.cy.tech.request.web.controller.view.component;

import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 需求單查詢, 開放沙巴需求頁面:進階搜尋 元件
 *
 * @author aken_kao
 *
 */
@Slf4j
@Scope("prototype")
@Component
public class AdvancedSearchVO {

	@Autowired
	transient private DisplayController display;
	@Autowired
	transient private LoginBean loginBean;
	@Getter
	/** 報表 組織樹 Component */
	private ReportOrgTreeComponent orgTreeForwardComponent;

	/** 異動啟始日 */
	@Getter
	@Setter
	public Date startUpdatedDate;
	/** 異動結束日 */
	@Getter
	@Setter
	public Date endUpdatedDate;
	/** 轉發單位 */
	@Getter
	@Setter
	public List<String> forwardDepts;
	/** 緊急度 */
	@Getter
	@Setter
	public List<String> urgencyList;
	/** 需求單號 */
	@Getter
	@Setter
	public String requireNo;

	@PostConstruct
	public void init() {
		startUpdatedDate = null;
		endUpdatedDate = null;
		requireNo = null;
		forwardDepts = Lists.newArrayList();
		urgencyList = Lists.newArrayList();

		// 查詢登入者所有角色
		//List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
		//        SecurityFacade.getUserSid(),
		//        SecurityFacade.getCompanyId());

		//orgTreeForwardComponent = new ReportOrgTreeComponent(
		//        loginBean.getCompanyId(),
		//        loginBean.getDep(),
		//        roleSids,
		//        false,
		//        reportOrgTreeForwardCallBack);
	}

	/**
	 * 清除進階選項
	 */
	public void clearAdvance() {
		startUpdatedDate = null;
		endUpdatedDate = null;
		requireNo = null;
		urgencyList.clear();
		forwardDepts.clear();

	}

	/**
	 * 開啟 轉發至 組織樹
	 */
	public void btnOpenForwardOrgTree() {
		try {
			orgTreeForwardComponent.initOrgTree(loginBean.getComp(), forwardDepts);
			display.showPfWidgetVar("dlgOrgTreeForward");
		} catch (Exception e) {
			log.error("btnOpenForwardOrgTree Error", e);
			messageCallBack.showMessage(e.getMessage());
		}
	}

	/** 訊息呼叫 */
	private final MessageCallBack messageCallBack = new MessageCallBack() {
		/**
         * 
         */
        private static final long serialVersionUID = 6467778309700300638L;

        @Override
		public void showMessage(String m) {
			MessagesUtils.showError(m);
		}
	};
}
