package com.cy.tech.request.web.controller.setting;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.helper.component.ComponentHelper;
import com.cy.tech.request.logic.service.SimpleCategoryService;
import com.cy.tech.request.logic.service.setting.SettingCheckConfirmRightService;
import com.cy.tech.request.logic.service.setting.SettingDefaultAssignSendDepHistoryService;
import com.cy.tech.request.logic.service.setting.SettingDefaultAssignSendDepService;
import com.cy.tech.request.logic.vo.AssignSendDepsInfoVO;
import com.cy.tech.request.vo.category.SimpleMiddleCategoryVO;
import com.cy.tech.request.vo.category.SimpleSmallCategoryVO;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.CategoryType;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.setting.asdep.SettingDefaultAssignSendDepHistoryVO;
import com.cy.tech.request.vo.setting.asdep.SettingDefaultAssignSendDepVO;
import com.cy.tech.request.web.controller.component.assignnotice.AssignNoticeComponent;
import com.cy.tech.request.web.controller.component.assignnotice.AssignNoticeComponentCallback;
import com.cy.tech.request.web.controller.component.singleselecttree.SingleSelectTreeCallback;
import com.cy.tech.request.web.controller.component.singleselecttree.SingleSelectTreePicker;
import com.cy.tech.request.web.controller.component.singleselecttree.SingleSelectTreePickerConfig;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 *
 */
@Slf4j
@Controller
@Scope("view")
public class SettingGmCategoryMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -9039202029959777353L;

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private transient ComponentHelper componentHelper;

    @Autowired
    private transient SimpleCategoryService simpleCategoryService;

    @Autowired
    private transient SettingDefaultAssignSendDepService settingDefaultAssignSendDepService;

    @Autowired
    private transient SettingDefaultAssignSendDepHistoryService settingDefaultAssignSendDepHistoryService;

    @Autowired
    private transient SettingCheckConfirmRightService settingCheckConfirmRightService;

    @Autowired
    private transient DisplayController displayController;

    // ========================================================================
    // 畫面變數
    // ========================================================================
    /**
     * 檢查項目
     */
    @Getter
    private List<RequireCheckItemType> checkItemTypes = Lists.newArrayList(RequireCheckItemType.values());
    /**
     * 需求項目樹選單
     */
    @Getter
    private SingleSelectTreePicker categoryTreePicker;

    /**
     * 選擇的中類資料 vo
     */
    @Getter
    private SimpleMiddleCategoryVO selectedMiddleCategoryVO;

    /**
     * 選擇的中類下，所有小類資料 vo
     */
    @Getter
    private List<SimpleSmallCategoryVO> selectedSmallCategoryVOs;

    /**
     * 分派/通知單位設定元件
     */
    @Getter
    private AssignNoticeComponent asDepComponent;

    /**
     * 分派/通知單位異動記錄
     */
    @Getter
    private List<SettingDefaultAssignSendDepHistoryVO> asDepHistoryVOs;

    // ========================================================================
    // 變數
    // ========================================================================
    /**
     * 所有需求項目
     */
    private List<WkItem> allCategoryItems = Lists.newArrayList();

    /**
     * 登入者檢查項目權限
     */
    List<RequireCheckItemType> userCheckRights = Lists.newArrayList();

    /**
     * 分派/通知單位顯示資料MAP
     */
    private Map<String, AssignSendDepsInfoVO> asDepInfoMap;

    /**
     * 
     */
    private String selectedSmallCategorySid;
    /**
     * 
     */
    private RequireCheckItemType selectedCheckItemType;

    /**
     * 
     */
    private SettingDefaultAssignSendDepVO asDep_selectedAssignDepInfo;
    /**
     * 
     */
    private SettingDefaultAssignSendDepVO asDep_selectedSendDepInfo;

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * 初始化
     */
    @PostConstruct
    public void init() {
        try {
            // ====================================
            // 準備需求項目選擇器
            // ====================================
            String defaultMiddleCategorySid = this.categoryTreePicker_Init();

            // ====================================
            // 取得登入者檢查項目權限
            // ====================================
            this.userCheckRights = this.settingCheckConfirmRightService.findUserCheckRights(
                    SecurityFacade.getUserSid());

            // ====================================
            // 依據回預設選擇的中分類sid, 取得資料
            // ====================================
            this.categoryTreePicker_loadData(defaultMiddleCategorySid);

        } catch (UserMessageException e) {
            MessagesUtils.show(e);
        } catch (Exception e) {
            String messsage = "初始化錯誤!" + e.getMessage();
            log.error(messsage);
            MessagesUtils.showError(messsage);
        }
    }

    // ========================================================================
    // 方法區-需求項目樹
    // ========================================================================
    /**
     * 準備需求項目選擇器
     */
    private String categoryTreePicker_Init() {
        // ====================================
        // 取得所有需求項目
        // ====================================
        // 查詢
        this.allCategoryItems = this.categoryTreePicker_prepareAllCategoryItems();

        // 預設選擇第一中類
        String defaultMiddleCategorySid = "";

        if (WkStringUtils.notEmpty(this.allCategoryItems)) {
            for (WkItem wkItem : this.allCategoryItems) {
                // 取得項目類別
                CategoryType categoryType = componentHelper.getItemCategoryType(wkItem);
                if (CategoryType.MIDDLE.equals(categoryType)) {
                    // 預設選項
                    if (WkStringUtils.isEmpty(defaultMiddleCategorySid)) {
                        defaultMiddleCategorySid = wkItem.getSid();
                    }
                }
            }
        }

        // ====================================
        // 初始化需求項目樹
        // ====================================
        // Picker Config
        SingleSelectTreePickerConfig categoryPickerConfig = new SingleSelectTreePickerConfig(categoryTreePickerCallback);
        categoryPickerConfig.setDefaultSelectedItemSid(defaultMiddleCategorySid);
        // init SingleSelectTreePicker
        this.categoryTreePicker = new SingleSelectTreePicker(categoryPickerConfig);

        return defaultMiddleCategorySid;
    }

    /**
     * @param middleCategorySid
     * @throws UserMessageException
     */
    private void categoryTreePicker_loadData(String middleCategorySid) throws UserMessageException {

        // ====================================
        // 防呆
        // ====================================
        if (WkStringUtils.isEmpty(middleCategorySid)) {
            String message = "未選擇中分類項目!";
            log.warn(message);
            throw new UserMessageException(message, InfomationLevel.WARN);
        }

        // ====================================
        // 初始化選擇項目
        // ====================================
        this.selectedMiddleCategoryVO = null;
        this.selectedSmallCategoryVOs = Lists.newArrayList();

        // ====================================
        // 選擇的中分類資訊
        // ====================================
        this.selectedMiddleCategoryVO = this.simpleCategoryService.findMiddleCategoryBySid(middleCategorySid);
        if (selectedMiddleCategoryVO == null) {
            String message = "查不到中分類項目![" + middleCategorySid + "]";
            log.error(message);
            throw new UserMessageException(message, InfomationLevel.WARN);
        }

        // ====================================
        // 選擇中分類下，所有小類資訊
        // ====================================
        // 查詢
        this.selectedSmallCategoryVOs = this.simpleCategoryService.findSmallCategoryByMiddleCategorySid(
                middleCategorySid);

        // 過濾停用
        this.selectedSmallCategoryVOs = this.selectedSmallCategoryVOs.stream().filter(each -> each.isActive()).collect(Collectors.toList());

        // ====================================
        // 查詢分派/通知單位顯示資訊
        // ====================================
        this.asDepInfoMap = this.asDep_prepareSettingInfo(
                this.selectedSmallCategoryVOs.stream()
                        .map(SimpleSmallCategoryVO::getSid)
                        .collect(Collectors.toList()));

        // ====================================
        // 更新頁面資料
        // ====================================
        this.displayController.update("settingPanel");
    }

    /**
     * @return
     */
    private List<WkItem> categoryTreePicker_prepareAllCategoryItems() {
        // ====================================
        // 準備需求項目樹所有項目
        // ====================================
        // 取得所有項目
        List<WkItem> allItems = componentHelper.prepareAllCategoryItemsWithoutSmall();

        // 過濾停用
        allItems = allItems.stream()
                .filter(WkItem::isActive)
                .collect(Collectors.toList());

        // ====================================
        // 移除找不到父項已停用
        // ====================================
        List<WkItem> tempItems = Lists.newArrayList();
        for (WkItem wkItem : allItems) {

            if (!wkItem.isActive()) {
                continue;
            }

            // 取得類別
            CategoryType categoryType = componentHelper.getItemCategoryType(wkItem);

            // 處理中類
            if (CategoryType.MIDDLE.equals(categoryType)) {
                if (wkItem.getParent() == null || !wkItem.getParent().isActive()) {
                    continue;
                }
            }

            tempItems.add(wkItem);
        }

        allItems = tempItems;
        // ====================================
        // 客製化
        // ====================================
        for (WkItem wkItem : allItems) {
            // 取得項目類別
            CategoryType categoryType = componentHelper.getItemCategoryType(wkItem);

            // 不是中類時，不可選擇
            if (!CategoryType.MIDDLE.equals(categoryType)) {
                wkItem.setSelectable(false);
            }

            // 預設展開大類
            if (CategoryType.BIG.equals(categoryType)) {
                wkItem.setForceExpand(true);
            }
        }

        return allItems;
    }

    /**
     * 需求項目選單 callback
     */
    private final SingleSelectTreeCallback categoryTreePickerCallback = new SingleSelectTreeCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = -2324268271746804229L;

        /**
         * 準備所有的項目
         * 
         * @return
         */
        public List<WkItem> prepareAllItems() throws SystemDevelopException {
            return allCategoryItems;
        }

        /**
         * 擊點項目時的動作
         * 
         * @return
         */
        public void clickItem() {
            // ====================================
            // 取得選擇的中類項目
            // ====================================
            WkItem item = categoryTreePicker.getSelectedWkItem();
            // String name = (item == null) ? "null" : item.getName();
            // log.debug("selected:[" + name + "]");

            // ====================================
            // 載入資訊
            // ====================================
            try {
                categoryTreePicker_loadData(item.getSid());
            } catch (UserMessageException e) {
                MessagesUtils.show(e);
            } catch (Exception e) {
                String messsage = "資料載入錯誤!" + e.getMessage();
                log.error(messsage);
                MessagesUtils.showError(messsage);
            }
        }
    };

    // ========================================================================
    // 方法區-預設分派/通知單位設定
    // ========================================================================
    /**
     * @param smallCategorySids
     * @return
     */
    private Map<String, AssignSendDepsInfoVO> asDep_prepareSettingInfo(List<String> smallCategorySids) {

        if (WkStringUtils.isEmpty(smallCategorySids)) {
            return Maps.newHashMap();
        }

        // ====================================
        // 查詢設定資訊
        // ====================================
        Map<String, SettingDefaultAssignSendDepVO> settingInfoMapByDataKey = this.settingDefaultAssignSendDepService.prepareSettingInfoMap(
                smallCategorySids);

        // ====================================
        // 組裝顯示資訊
        // ====================================
        Map<String, AssignSendDepsInfoVO> displayInfoMap = Maps.newHashMap();
        for (String smallCategorySid : smallCategorySids) {
            for (RequireCheckItemType checkItemType : this.checkItemTypes) {

                // 取得分派單位
                List<Integer> assignDepSids = this.settingDefaultAssignSendDepService.findSettingDepSids(
                        settingInfoMapByDataKey, smallCategorySid, checkItemType, AssignSendType.ASSIGN);
                // 取得通知單位
                List<Integer> sendDepSids = this.settingDefaultAssignSendDepService.findSettingDepSids(
                        settingInfoMapByDataKey, smallCategorySid, checkItemType, AssignSendType.SEND);

                // 設定資訊顯示標籤
                String settingDepinfoTag = WkHtmlUtils.addRedBlodClass("[未設定]");
                if (WkStringUtils.notEmpty(assignDepSids) || WkStringUtils.notEmpty(sendDepSids)) {
                    settingDepinfoTag = WkHtmlUtils.addBlueBlodClass("[明細]");
                }
                // 分派單位設定資訊
                String assignDepInfo = "<table style='border:2px #cccccc solid;'><tr><td>未設定</td></tr></table>";
                if (WkStringUtils.notEmpty(assignDepSids)) {
                    assignDepInfo = WkOrgUtils.prepareDepsNameByTreeStyle(
                            assignDepSids,
                            40);
                }
                // 通知單位設定資訊
                String sendDepInfo = "<table style='border:2px #cccccc solid;'><tr><td>未設定</td></tr></table>";
                if (WkStringUtils.notEmpty(sendDepSids)) {
                    sendDepInfo = WkOrgUtils.prepareDepsNameByTreeStyle(
                            sendDepSids,
                            40);
                }

                AssignSendDepsInfoVO vo = new AssignSendDepsInfoVO();
                vo.setSettingDepinfoTag(settingDepinfoTag);
                vo.setAssignDepInfo(assignDepInfo);
                vo.setSendDepInfo(sendDepInfo);

                // 資料key
                String displayInfoDataKey = this.asDep_prepareDisplayInfoDataKey(smallCategorySid, checkItemType);
                displayInfoMap.put(displayInfoDataKey, vo);
            }
        }
        return displayInfoMap;

    }

    /**
     * @param smallCategorySid
     * @param checkItemType
     * @return
     */
    public String asDep_prepareDisplayInfoDataKey(String smallCategorySid, RequireCheckItemType checkItemType) {
        return smallCategorySid + "@" + checkItemType.name();
    }

    /**
     * @param checkItemType
     * @return
     */
    public boolean asDep_isCanSetting(RequireCheckItemType checkItemType) {
        return this.userCheckRights.contains(checkItemType);
    }

    /**
     * @param smallCategorySid
     * @param checkItemType
     * @param type
     * @return
     */
    public String asDep_getSettingInfo(String smallCategorySid, RequireCheckItemType checkItemType, String type) {
        // ====================================
        // 取得對應 vo
        // ====================================
        AssignSendDepsInfoVO vo = this.asDepInfoMap.get(this.asDep_prepareDisplayInfoDataKey(smallCategorySid, checkItemType));

        if (vo == null) {
            log.warn("取不到對應VO!  smallCategorySid:[{}], checkItemType:[{}] ", smallCategorySid, checkItemType.name());
            return "";
        }

        // ====================================
        // 取得對應 vo
        // ====================================
        switch (type) {
        case "DEPINFO_TAG":
            return vo.getSettingDepinfoTag();
        case "ASSIGN_DEP":
            return vo.getAssignDepInfo();
        case "SEND_DEP":
            return vo.getSendDepInfo();
        default:
            log.error("開發時期錯誤! 傳入 type:[{}]", type);
            break;
        }

        return "";
    }

    /**
     * @param editSmallCategorySid
     * @param selectedCheckItemType
     */
    public void asDep_openDialog(String selectedSmallCategorySid, RequireCheckItemType selectedCheckItemType) {

        // ====================================
        // 初始化參數
        // ====================================
        // 防呆
        if (WkStringUtils.isEmpty(selectedSmallCategorySid)) {
            MessagesUtils.showError("初始化資訊失敗, 請恰資訊人員");
            log.error("開啟視窗時, 傳入參數錯誤! editSmallCategorySid is empty");
            return;
        }
        // 防呆
        if (selectedCheckItemType == null) {
            MessagesUtils.showError("初始化資訊失敗, 請恰資訊人員");
            log.error("開啟視窗時, 傳入參數錯誤! checkItemType is empty");
            return;
        }

        // 記錄下來, 給設定視窗->確認時,更新資料用
        this.selectedSmallCategorySid = selectedSmallCategorySid;
        this.selectedCheckItemType = selectedCheckItemType;

        // ====================================
        // 查詢資料
        // ====================================
        // 分派單位設定資料
        this.asDep_selectedAssignDepInfo = this.settingDefaultAssignSendDepService.findByUnqKey(
                selectedSmallCategorySid,
                selectedCheckItemType,
                AssignSendType.ASSIGN);

        // 通知單位設定資料
        this.asDep_selectedSendDepInfo = this.settingDefaultAssignSendDepService.findByUnqKey(
                selectedSmallCategorySid,
                selectedCheckItemType,
                AssignSendType.SEND);

        try {
            // ====================================
            // 初始化 dialog 元件
            // ====================================
            this.asDepComponent = new AssignNoticeComponent(this.asDepComponentCallback);
            this.asDepComponent.openDialog();
        } catch (Exception e) {
            log.error("開啟分派/通知設定視窗失敗", e);
            MessagesUtils.showError("開啟分派/通知設定視窗失敗, 請恰系統人員");
            return;
        }

        // ====================================
        // 畫面控制
        // ====================================
        // 開啟視窗
        displayController.showPfWidgetVar("assignNoticeSettingDlg");
    }

    /**
     * 實做選擇元件 callback 事件
     */
    private final AssignNoticeComponentCallback asDepComponentCallback = new AssignNoticeComponentCallback() {

        /**
         * 
         */
        private static final long serialVersionUID = 858222875836015395L;

        /**
         * 準備分派單位已選擇項目
         * 
         * @return
         */
        @Override
        public List<Integer> prepareAssignSelectedDeps() throws SystemDevelopException {
            return asDep_selectedAssignDepInfo.getDepsInfo();
        }

        /**
         * 準備通知單位已選擇項目
         * 
         * @return
         */
        @Override
        public List<Integer> prepareNoticeSelectedDeps() throws SystemDevelopException {
            return asDep_selectedSendDepInfo.getDepsInfo();
        }

        /**
         * 準備鎖定的分派單位 sid
         * 
         * @return
         * @throws Exception
         */
        @Override
        public List<Integer> prepareAssignDisableDepSids() throws SystemDevelopException {
            // 沒有需要鎖定的單位
            return Lists.newArrayList();
        }

        /**
         *
         */
        @Override
        public void btnConfirm(ActionEvent event, List<Integer> assignDepSids, List<Integer> noticeDepSids) {

            // ====================================
            // 防呆
            // ====================================
            if (assignDepSids == null) {
                assignDepSids = Lists.newArrayList();
            }
            if (noticeDepSids == null) {
                noticeDepSids = Lists.newArrayList();
            }

            // ====================================
            // 比對是否分派/通知單位有相同資料
            // ====================================
            // 複製分派單位後
            List<Integer> intersectionDeps = Lists.newArrayList(assignDepSids);
            // 取與通知單位的交集
            intersectionDeps.retainAll(noticeDepSids);
            // 警告視窗
            if (WkStringUtils.notEmpty(intersectionDeps)) {
                String orgNames = WkOrgUtils.findNameBySidStrs(
                        intersectionDeps.stream().map(each -> each + "").collect(Collectors.toList()),
                        "、");
                MessagesUtils.showWarn("單位 【" + orgNames + "】 同時存在於分派/通知單位中！");
                return;
            }

            // ====================================
            // 更新設定資料
            // ====================================
            boolean isModify = settingDefaultAssignSendDepService.updateDefaultAsDeps(
                    selectedSmallCategorySid,
                    selectedCheckItemType,
                    assignDepSids,
                    noticeDepSids,
                    SecurityFacade.getUserSid(),
                    new Date());

            // ====================================
            // 為更新資料時，提示訊息
            // ====================================
            if (!isModify) {
                MessagesUtils.showInfo("未異動資料!");
                return;
            }

            // ====================================
            // 為更新資料時，提示訊息
            // ====================================
            try {
                categoryTreePicker_loadData(selectedMiddleCategoryVO.getSid());
            } catch (UserMessageException e) {
                MessagesUtils.show(e);
            } catch (Exception e) {
                String messsage = "資料載入時發生錯誤!" + e.getMessage();
                log.error(messsage);
                MessagesUtils.showError(messsage);
            }

        }
    };

    /**
     * 開啟異動記錄視窗
     * 
     * @param selectedSmallCategorySid
     * @param selectedCheckItemType
     */
    public void asDepHistory_openDialog(
            String selectedSmallCategorySid,
            RequireCheckItemType selectedCheckItemType) {

        // ====================================
        // 查詢異動記錄
        // ====================================
        try {
            this.asDepHistoryVOs = this.settingDefaultAssignSendDepHistoryService.findHistory(
                    selectedSmallCategorySid,
                    selectedCheckItemType);
        } catch (Exception e) {
            String messsage = "查詢異動記錄失敗!" + e.getMessage();
            log.error(messsage);
            MessagesUtils.showError(messsage);
            return;
        }

        // ====================================
        // 畫面處理
        // ====================================
        // 更新畫面資料
        this.displayController.update("assignNoticeSettingHistoryContent");

        // 開啟視窗
        this.displayController.showPfWidgetVar("assignNoticeSettingHistoryDlg");

    }

    /**
     * @param event
     */
    public void other_update(ValueChangeEvent event) {

        // ====================================
        // 取得異動參數
        // ====================================
        String smallCategorySid = (String) event.getComponent().getAttributes().get("smallCategorySid");
        String signType = (String) event.getComponent().getAttributes().get("signType");

        // ====================================
        // 異動資料
        // ====================================
        try {
            this.simpleCategoryService.updateIsSign(
                    smallCategorySid,
                    signType,
                    (Boolean) event.getNewValue(),
                    SecurityFacade.getUserSid());
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
        } catch (Exception e) {
            String messsage = WkMessage.EXECTION + e.getMessage();
            log.error(messsage);
            MessagesUtils.showError(messsage);
        }
    }
}
