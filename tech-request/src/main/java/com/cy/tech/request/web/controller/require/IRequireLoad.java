/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require;

import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.work.mapp.create.trans.vo.MappCreateTrans;

import java.io.Serializable;

/**
 * 需求單載入介面
 *
 * @author shaun
 */
public interface IRequireLoad extends Serializable {

    /**
     * 新建需求單
     */
    public void initByNewRequire();

    /**
     * 複製需求單
     *
     * @param source
     */
    public void initByCopy(Require source);

    /**
     * 依單號進行載入<BR/>
     *
     * @param requireNo
     */
    public void initByUrl(String sourceNo);

    /**
     * 依需求單實體載入
     *
     * @param source
     */
    public void initByTable(Require source);

    /**
     * 案件單轉入ON程式<BR/>
     * 透過小類確定初始化條件
     *
     * @param trans
     */
    public void initByIssueCreate(MappCreateTrans trans);

    /**
     * 案件單轉入ON程式<BR/>
     * 透過小類確定初始化條件
     *
     * @param trans
     */
    public void initByIssueCreateOnpg(MappCreateTrans trans);

    public boolean loadSuccessWorkTestInfoByUrlParam(WorkTestInfo workTestInfo);
    
    public void reloadToIllegalPage(String errorCode);

}
