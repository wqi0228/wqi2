/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.component.searchquery;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.cy.tech.request.logic.service.CategorySettingService;
import com.cy.tech.request.vo.category.BasicDataBigCategory;
import com.cy.tech.request.vo.enums.OverdueUnclosedColumn;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.web.controller.enums.ReqStatusMBean;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.view.component.CategoryCombineVO;
import com.cy.tech.request.web.controller.view.component.OrgTreeVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterArrayStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterDetailVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterVO;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author aken_kao
 */
@Slf4j
public class OverdueUnclosedCustomQueryVO {
    
    private ReportCustomFilterLogicComponent customFilterLogicComponent = WorkSpringContextHolder.getBean(ReportCustomFilterLogicComponent.class);
    private CategorySettingService categorySettingService = WorkSpringContextHolder.getBean(CategorySettingService.class);
    private LoginBean loginBean = WorkSpringContextHolder.getBean(LoginBean.class);
    private DisplayController display = WorkSpringContextHolder.getBean(DisplayController.class);;
    
    @Getter
    @Setter
    /** 需求類別 */
    private List<String> bigCategorySids = Lists.newArrayList();
    @Getter
    @Setter
    /** 單位挑選 */
    private OrgTreeVO orgTreeVO = new OrgTreeVO();
    @Getter
    @Setter
    /** 需求製作進度 */
    private List<String> requireStatusTypes = Lists.newArrayList();
    @Getter
    @Setter
    /** 閱讀類型 */
    private ReadRecordType readRecordType;
    @Getter
    @Setter
    /** 待閱原因 */
    private List<String> reasons = Lists.newArrayList();
    @Getter
    @Setter
    /** 需求人員 */
    private String creator;
    @Getter
    @Setter
    /** 類別組合 */
    private CategoryCombineVO categoryCombineVO = new CategoryCombineVO();
    @Getter
    @Setter
    private String searchText;
    /** 時間切換的index */
    @Setter
    @Getter
    private String dateTypeIndex;
    
    /** 該登入者在需求單,挑選自訂查詢條件物件 */
    private List<ReportCustomFilterVO> reportCustomFilterVOs;
    private ReportCustomFilterVO selReportCustomFilterVO;
    
    public OverdueUnclosedCustomQueryVO(){
        this.orgTreeVO.init();
        for (BasicDataBigCategory category : categorySettingService.findAllBig()) {
            this.bigCategorySids.add(category.getSid());
        }
        for(RequireStatusType type: ReqStatusMBean.STATUS1){
            this.requireStatusTypes.add(type.name());
        }
        for (ReqToBeReadType each : ReqToBeReadType.values()) {
            this.reasons.add(each.name());
        }
    }


    /** 初始化查詢條件預設值 */
    public void initCustomQueryVO() {
        reportCustomFilterVOs = customFilterLogicComponent.getReportCustomFilter(OverdueUnclosedColumn.OVERDUE_UNCLOSED,
                loginBean.getUserSId());
        if (CollectionUtils.isNotEmpty(reportCustomFilterVOs)) {
            selReportCustomFilterVO = reportCustomFilterVOs.get(0);
            loadSettingData();
        }
    }

    /** 儲存自訂搜尋條件 */
    public void saveReportCustomFilter() {
        try {
            ReportCustomFilterDetailVO vo1 = customFilterLogicComponent.createReportCustomFilterDetailVO(OverdueUnclosedColumn.DEMAND_TYPE, bigCategorySids);
            ReportCustomFilterDetailVO vo2 = customFilterLogicComponent.createReportCustomFilterDetailVO(OverdueUnclosedColumn.DEPARTMENT, orgTreeVO.getRequireDepts());
            ReportCustomFilterDetailVO vo3 = customFilterLogicComponent.createReportCustomFilterDetailVO(OverdueUnclosedColumn.DEMAND_PROCESS, requireStatusTypes);
            ReportCustomFilterDetailVO vo4 = customFilterLogicComponent.createReportCustomFilterDetailVO(OverdueUnclosedColumn.READ_STATUS, readRecordType != null ? readRecordType.name() : "");
            ReportCustomFilterDetailVO vo5 = customFilterLogicComponent.createReportCustomFilterDetailVO(OverdueUnclosedColumn.WAIT_READ_REASON, reasons);
            ReportCustomFilterDetailVO vo6 = customFilterLogicComponent.createReportCustomFilterDetailVO(OverdueUnclosedColumn.DEMAND_PERSON, StringUtils.isNotBlank(creator) ? creator : "");
            ReportCustomFilterDetailVO vo7 = customFilterLogicComponent.createReportCustomFilterDetailVO(OverdueUnclosedColumn.CATEGORY_COMBO, categoryCombineVO.getSmallDataCateSids());
            ReportCustomFilterDetailVO vo8 = customFilterLogicComponent.createReportCustomFilterDetailVO(OverdueUnclosedColumn.DATE_INDEX, StringUtils.isNotBlank(dateTypeIndex) ? dateTypeIndex : "");
            ReportCustomFilterDetailVO vo9 = customFilterLogicComponent.createReportCustomFilterDetailVO(OverdueUnclosedColumn.SEARCH_TEXT, StringUtils.isNotBlank(searchText) ? searchText : "");
            List<ReportCustomFilterDetailVO> list = Lists.newArrayList(vo1, vo2, vo3, vo4, vo5, vo6, vo7, vo8, vo9);
            
            customFilterLogicComponent.saveReportCustomFilter(OverdueUnclosedColumn.OVERDUE_UNCLOSED,
                    loginBean.getUserSId(),
                    (selReportCustomFilterVO != null) ? selReportCustomFilterVO.getIndex() : null, true, list);
            display.update("headerTitle");
            display.execute("doSearchData();");
            display.hidePfWidgetVar("dlgReportCustomFilter");
        } catch (Exception e) {
            this.messageCallBack.showMessage(e.getMessage());
            log.error("saveReportCustomFilter ERROR", e);
        }
    }

    /** 清除立單區間Type */
    public void clearDateType() {
        if ("4".equals(dateTypeIndex)) {
            dateTypeIndex = "";
        }
    }
    
    /** 訊息呼叫 */
    private final MessageCallBack messageCallBack = new MessageCallBack() {
        
        /**
         * 
         */
        private static final long serialVersionUID = 2117988495855983287L;

        @Override
        public void showMessage(String m) {
            MessagesUtils.showError(m);
        }
    };
    
    /** 載入挑選的自訂搜尋條件 */
    public void loadSettingData() {
        selReportCustomFilterVO.getReportCustomFilterDetailVOs().forEach(item -> {
            if (OverdueUnclosedColumn.DEMAND_TYPE.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingDemandType(item);
            } else if (OverdueUnclosedColumn.DEPARTMENT.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingDepartment(item);
            } else if (OverdueUnclosedColumn.DEMAND_PROCESS.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingDemandProcess(item);
            } else if (OverdueUnclosedColumn.READ_STATUS.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingReadStatus(item);
            } else if (OverdueUnclosedColumn.WAIT_READ_REASON.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingWaitReadReson(item);
            } else if (OverdueUnclosedColumn.DEMAND_PERSON.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDemandPerson(item);
            } else if (OverdueUnclosedColumn.CATEGORY_COMBO.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingCategoryCombo(item);
            } else if (OverdueUnclosedColumn.DATE_INDEX.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDateIndex(item);
            } else if (OverdueUnclosedColumn.SEARCH_TEXT.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingFuzzyText(item);
            }
        });
    }

    /**
     * 塞入立單區間Type
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDateIndex(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            this.dateTypeIndex = reportCustomFilterStringVO.getValue();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 塞入模糊搜尋
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingFuzzyText(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            this.searchText = reportCustomFilterStringVO.getValue();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }


    /**
     * 塞入需求類別
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingCategoryCombo(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            this.categoryCombineVO.getSmallDataCateSids().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                this.categoryCombineVO.getSmallDataCateSids().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 塞入需求人員
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandPerson(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            this.creator = reportCustomFilterStringVO.getValue();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 塞入待閱原因
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingWaitReadReson(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            this.reasons.clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                this.reasons.addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 塞入是否閱讀：
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingReadStatus(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                this.readRecordType = ReadRecordType.valueOf(reportCustomFilterStringVO.getValue());
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 塞入需求製作進度
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandProcess(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            this.requireStatusTypes.clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                this.requireStatusTypes.addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }


    /**
     * 塞入單位挑選
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDepartment(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            orgTreeVO.getRequireDepts().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                orgTreeVO.getRequireDepts().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 塞入需求類別
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandType(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            this.bigCategorySids.clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                this.bigCategorySids.addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
