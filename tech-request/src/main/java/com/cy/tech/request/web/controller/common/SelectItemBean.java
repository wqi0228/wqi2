/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.common;

import com.cy.tech.request.logic.enumerate.DateType;
import com.cy.tech.request.logic.enumerate.RejectReason;
import com.cy.tech.request.logic.search.enums.ConditionInChargeMode;
import com.cy.tech.request.logic.service.SimpleCategoryService;
import com.cy.tech.request.vo.anew.enums.SignLevelType;
import com.cy.tech.request.vo.enums.OperationMode;
import com.cy.tech.request.vo.enums.ReqConfirmDepProgStatus;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.enums.WorkSourceType;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * 選項
 *
 * @author kasim
 */
@Controller
@ManagedBean
public class SelectItemBean implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -910012986235774564L;
    @Autowired
    transient private SimpleCategoryService simpleCategoryService;

    /**
     * 大類選項
     *
     * @return
     */
    public List<SelectItem> getBigCategoryItems() {
        return this.simpleCategoryService.findAllBigCategory().stream()
                .map(each -> new SelectItem(each.getSid(), each.getName()))
                .collect(Collectors.toList());
    }

    /**
     * 緊急度選項
     *
     * @return
     */
    public List<SelectItem> getUrgencyItems() {
        return Lists.newArrayList(UrgencyType.values()).stream()
                .map(each -> new SelectItem(each.ordinal(), each.getValue()))
                .collect(Collectors.toList());
    }

    /**
     * 需求單位審核狀態選項
     *
     * @return
     */
    public List<SelectItem> getInstanceItems() {
        return Lists.newArrayList(InstanceStatus.NEW_INSTANCE, InstanceStatus.WAITING_FOR_APPROVE,
                InstanceStatus.INVALID, InstanceStatus.RECONSIDERATION, InstanceStatus.APPROVING,
                InstanceStatus.APPROVED, InstanceStatus.CLOSED).stream()
                .map(each -> new SelectItem(each.getValue(), each.getLabel()))
                .collect(Collectors.toList());
    }

    /**
     * 建立 是否閱讀 選項
     *
     * @return
     */
    public List<SelectItem> getReadRecordItems() {
        return Lists.newArrayList(ReadRecordType.values()).stream()
                .filter(each -> !ReadRecordType.SYSTEM_UPDATE.equals(each))
                .map(each -> new SelectItem(each.name(), each.getValue()))
                .collect(Collectors.toList());
    }

    /**
     * 建立需求製作進度選項
     *
     * @return
     */
    public List<SelectItem> getReqStatusItems() {
        return Lists.newArrayList(RequireStatusType.values()).stream()
                .filter(each -> each.isReportQueryCondition()) // (排除草稿)
                .map(each -> new SelectItem(each.name(), each.getValue()))
                .collect(Collectors.toList());
    }

    /**
     * 建立退件原因選項
     *
     * @return
     */
    public List<SelectItem> getRejectReasonItems() {
        return Lists.newArrayList(RejectReason.values()).stream()
                .map(each -> {
                    String label = each.getLabel();
                    return new SelectItem(each, label);
                })
                .collect(Collectors.toList());
    }

    /**
     * 建立結案狀態選項
     *
     * @return
     */
    public List<SelectItem> getClosedCodeItems() {
        return Lists.newArrayList(
                new SelectItem(false, "未結案"),
                new SelectItem(true, "已結案"));
    }

    /**
     * 建立待閱原因選項
     *
     * @return
     */
    public List<SelectItem> getWaitReadReasonTypeItems() {
        return Lists.newArrayList(WaitReadReasonType.PROTOTYPE_NOT_WAITREASON,
                WaitReadReasonType.PROTOTYPE_SIGN_PROCESS,
                WaitReadReasonType.PROTOTYPE_VERIFY_INVAILD,
                WaitReadReasonType.PROTOTYPE_APPROVE,
                WaitReadReasonType.PROTOTYPE_HAS_REPLY,
                WaitReadReasonType.PROTOTYPE_REDO,
                WaitReadReasonType.PROTOTYPE_FUNCTION_CONFORM).stream()
                .map(each -> new SelectItem(each.name(), each.getValue()))
                .collect(Collectors.toList());
    }

    /**
     * 建立退件原因選項
     *
     * @return
     */
    public List<SelectItem> getPtStatusItems() {
        return Lists.newArrayList(PtStatus.values()).stream()
                .map(each -> new SelectItem(each.name(), each.getLabel()))
                .collect(Collectors.toList());
    }

    /**
     * 緊急度選項
     *
     * @return
     */
    public List<SelectItem> getWorkTestStatusItems() {

        return Lists.newArrayList(WorkTestStatus.values()).stream()
                .map(each -> new SelectItem(each.name(), each.getValue()))
                .collect(Collectors.toList());
    }

    public List<SelectItem> getWorkOnpgStatusItems() {
        return Lists.newArrayList(WorkOnpgStatus.values()).stream()
                .map(each -> new SelectItem(each.name(), each.getLabel()))
                .collect(Collectors.toList());
    }

    /**
     * 分派部門需求完成進度
     * 
     * @return
     */
    public List<SelectItem> getReqConfirmDepProgStatusItems() {
        return Lists.newArrayList(ReqConfirmDepProgStatus.values()).stream()
                .map(each -> new SelectItem(each.name(), each.getLabel()))
                .collect(Collectors.toList());
    }

    /**
     * 清單模式、組織樹模式
     * 
     * @return
     */
    public List<SelectItem> getOperationModeItems() {
        return Lists.newArrayList(OperationMode.values()).stream()
                .map(each -> new SelectItem(each, each.getLabel()))
                .collect(Collectors.toList());
    }

    /**
     * 關連類型
     * 
     * @return
     */
    public List<SelectItem> getWorkSourceTypeItems() {
        return Lists.newArrayList(WorkSourceType.values()).stream()
                .map(each -> new SelectItem(each, each.getValue()))
                .collect(Collectors.toList());
    }

    /**
     * 日期區間選項
     * 
     * @return 列舉項目選擇項目
     */
    public List<SelectItem> getDateTypeItemsForQuery() {

        return Lists.newArrayList(DateType.values()).stream()
                .filter(type -> !DateType.NULL.equals(type)) // 過濾掉 null
                .map(type -> new SelectItem(type.name(), type.getDescr()))
                .collect(Collectors.toList());
    }

    /**
     * 查詢條件：主責單位查詢模式
     * 
     * @return
     */
    public List<SelectItem> getConditionInChargeModeItemsForQuery() {
        return Lists.newArrayList(ConditionInChargeMode.values()).stream()
                .map(type -> new SelectItem(type, type.getDescr()))
                .collect(Collectors.toList());
    }

    /**
     * 待閱原因
     * 
     * @return
     */
    public List<SelectItem> getReqToBeReadTypeItems() {
        return Lists.newArrayList(ReqToBeReadType.values()).stream()
                .map(each -> new SelectItem(each.name(), each.getLabel()))
                .collect(Collectors.toList());
    }

    /**
     * 檢查項目
     * 
     * @return
     */
    public List<SelectItem> getRequireCheckItemTypeItems() {
        return Lists.newArrayList(RequireCheckItemType.values()).stream()
                .map(each -> new SelectItem(each, each.getDescr()))
                .collect(Collectors.toList());
    }

    /**
     * 無
     * 
     * @return
     */
    public List<SelectItem> getNone() {
        return Lists.newArrayList(new SelectItem("NONE", "無"));
    }

    /**
     * 檔案上傳限制選項 (10～100MB setp 10)
     * 
     * @return
     */
    public List<SelectItem> getFileUpoadSizeItems() {
        List<SelectItem> items = Lists.newArrayList();
        for (int i = 10; i <= 100; i = i + 10) {
            items.add(new SelectItem(i, i + "MB"));
        }
        return items;
    }
    
    /**
     * 簽核層級選項
     *
     * @return
     */
    public List<SelectItem> getSignLevelTypeItems() {
        return Lists.newArrayList(SignLevelType.values()).stream()
                .map(each -> new SelectItem(each, each.getDescr()))
                .collect(Collectors.toList());
    }

}
