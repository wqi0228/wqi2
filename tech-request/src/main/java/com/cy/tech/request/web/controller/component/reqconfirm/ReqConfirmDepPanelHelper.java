/**
 * 
 */
package com.cy.tech.request.web.controller.component.reqconfirm;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.onpg.OnpgService;
import com.cy.tech.request.logic.service.othset.OthSetService;
import com.cy.tech.request.logic.service.pt.PtService;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepHistoryService;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepService;
import com.cy.tech.request.logic.service.send.test.SendTestService;
import com.cy.tech.request.logic.vo.RequireConfirmDepTraceVO;
import com.cy.tech.request.logic.vo.RequireConfirmDepVO;
import com.cy.work.common.constant.WkConstants;
import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.require.RequireConfirmDepHistory;
import com.cy.tech.request.vo.require.othset.OthSet;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.collect.Lists;
import com.google.gson.Gson;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@NoArgsConstructor
@Component
@Slf4j
public class ReqConfirmDepPanelHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2390783547675578683L;

    // =======================================================================
    // 服務物件區
    // =======================================================================
    /**
     * service: 需求完成確認單位檔
     */
    @Autowired
    private RequireConfirmDepService requireConfirmDepService;

    /**
     * service: 需求完成確認單位 - 歷程檔
     */
    @Autowired
    private RequireConfirmDepHistoryService requireConfirmDepHistoryService;

    /**
     * service: 原型確認
     */
    @Autowired
    private PtService ptService;

    /**
     * service: 送測
     */
    @Autowired
    private SendTestService sendTestService;

    /**
     * service: ONPG
     */
    @Autowired
    private OnpgService onpgService;

    /**
     * service: 其他設定資訊
     */
    @Autowired
    private OthSetService othSetService;

    /**
     * 
     */
    @Autowired
    private WkOrgCache wkOrgCache;

    /**
     * 
     */
    @Autowired
    private WkUserCache wkUserCache;

    // =======================================================================
    // 方法區
    // =======================================================================

    /**
     * 查詢所有需求完成確認部門檔資料
     * 
     * @param requireSid   需求單 sid
     * @param loginUserSid 登入者 sid
     * @return
     */

    public List<RequireConfirmDepVO> prepareRequireConfirmDepInfo(String requireSid, Integer loginUserSid) {

        // ====================================
        // 查詢【需求完成確認單位】
        // ====================================
        List<RequireConfirmDepVO> allRequireConfirmDepVOs = this.requireConfirmDepService.findActiveByRequireSid(
                requireSid);

        // 為空時, 無須繼續往下處理
        if (WkStringUtils.isEmpty(allRequireConfirmDepVOs)) {
            log.info("無 【需求完成確認單位】 資料 ");
            return Lists.newArrayList();
        }

        // 收集 sid
        List<Long> requireConfirmSids = allRequireConfirmDepVOs.stream()
                .map(RequireConfirmDepVO::getSid).collect(Collectors.toList());

        // 準備排序用資料
        // 收集 dep_sid
        List<Integer> reqConfirmDepSids = allRequireConfirmDepVOs.stream()
                .map(RequireConfirmDepVO::getDepSid).collect(Collectors.toList());

        // 依據組織樹排序
        reqConfirmDepSids = WkOrgUtils.sortByOrgTree(reqConfirmDepSids);

        // ====================================
        // 查詢異動歷程檔
        // ====================================
        // 查詢
        List<RequireConfirmDepTraceVO> reqConfirmHistorys = this.prepareRequireConfirmDepInfo_getHistory(
                requireConfirmSids, loginUserSid);

        // reqConfirmHistorys.stream().sorted(Comparator.comparing(RequireConfirmDepTraceVO::getDate).reversed());

        // 依據所屬需求確認單位整理 (groupingBy by reqConfirmDepSid)
        Map<Integer, List<RequireConfirmDepTraceVO>> reqConfirmHistorysMapByReqConfirmDepSid = reqConfirmHistorys.stream()
                .collect(Collectors.groupingBy(
                        RequireConfirmDepTraceVO::getReqConfirmDepSid,
                        Collectors.mapping(
                                vo -> vo,
                                Collectors.toList())));

        // ====================================
        // 逐筆處理
        // ====================================
        for (RequireConfirmDepVO requireConfirmDepVO : allRequireConfirmDepVOs) {
            // 準備顯示資料
            this.prepareRequireConfirmDepInfo_trnsToShowContent(
                    requireConfirmDepVO,
                    reqConfirmHistorysMapByReqConfirmDepSid.get(requireConfirmDepVO.getDepSid()));

            // 依據組織樹排序的編號
            requireConfirmDepVO.setSeqNum(reqConfirmDepSids.indexOf(requireConfirmDepVO.getDepSid()));
        }

        // 依照序號排序
        allRequireConfirmDepVOs = allRequireConfirmDepVOs.stream()
                .sorted(Comparator.comparingInt(RequireConfirmDepVO::getSeqNum))
                .collect(Collectors.toList());

        return allRequireConfirmDepVOs;
    }

    /**
     * 查詢異動歷程檔
     * 
     * @param requireConfirmSids
     */
    private List<RequireConfirmDepTraceVO> prepareRequireConfirmDepInfo_getHistory(
            List<Long> requireConfirmSids,
            Integer loginUserSid) {

        // ====================================
        // 查詢歷程檔
        // ====================================
        List<RequireConfirmDepHistory> requireConfirmDepHistoryEntities = this.requireConfirmDepHistoryService.findRequireConfirmSid(
                requireConfirmSids);

        // ====================================
        // 取得與登入者相關的部門 - 用於比對該則歷程是否與登入者相關
        // ====================================
        List<Integer> relationDepsByLoginUserDep = WkOrgUtils.findUserRelationDepsAndSortByDistance(loginUserSid);

        // ====================================
        // 轉 RequireConfirmDepTraceVO
        // ====================================
        List<RequireConfirmDepTraceVO> results = Lists.newArrayList();
        for (RequireConfirmDepHistory history : requireConfirmDepHistoryEntities) {
            RequireConfirmDepTraceVO historyVO = new RequireConfirmDepTraceVO(
                    // 操作者
                    WkUserUtils.findNameBySid(history.getCreatedUser()),
                    // 異動時間
                    history.getCreatedDate(),
                    // 行為
                    history.getProgType() == null ? "" : history.getProgType().getLabel(),
                    // 說明
                    WkJsoupUtils.getInstance().toCss(history.getMemo()),
                    // 顯示樣式
                    this.prepareTraceRowStyle(history, relationDepsByLoginUserDep, loginUserSid),
                    // 此記錄 歸類於 哪筆需求完成確認單位檔
                    requireConfirmDepService.prepareRequireConfirmDepSid(history.getDepSid()));

            // log.debug(""
            // + historyVO.getName()
            // + ":" + history.getDepSid()
            // + ":" + historyVO.getTraceRowStyle()
            // + ":" + historyVO.getMsg());

            results.add(historyVO);
        }

        return results;
    }

    /**
     * 判斷歷程列顯示樣式
     * 
     * @param history
     * @param relationDepsByLoginUserDep
     * @param loginUserSid
     * @return
     */
    private String prepareTraceRowStyle(
            RequireConfirmDepHistory history,
            List<Integer> relationDepsByLoginUserDep,
            Integer loginUserSid) {

        if (history == null) {
            return "";
        }

        if (WkCommonUtils.compareByStr(history.getCreatedUser(), loginUserSid)) {
            return "reqConfirmDepPanel-traceMsg-self";
        }

        if (!relationDepsByLoginUserDep.contains(history.getDepSid())) {
            return "reqConfirmDepPanel-traceMsg-foreign";
        }

        return "";
    }

    /**
     * 將資料欄位轉換為顯示資訊
     * 
     * @param requireConfirmDepVO
     * @param reqConfirmHistorys  需求完成確認單位歷程
     */
    private void prepareRequireConfirmDepInfo_trnsToShowContent(
            RequireConfirmDepVO requireConfirmDepVO,
            List<RequireConfirmDepTraceVO> reqConfirmHistorys) {

        if (requireConfirmDepVO == null) {
            return;
        }

        // ====================================
        // 轉顯示資料 : 部門
        // ====================================
        Org dep = null;
        if (requireConfirmDepVO.getDepSid() != null) {
            // 部門資料名稱
            dep = WkOrgCache.getInstance().findBySid(requireConfirmDepVO.getDepSid());
            if (dep != null) {
                // 部門名稱
                requireConfirmDepVO.setDepName(WkOrgUtils.getOrgName(dep));
                // tooltip
                requireConfirmDepVO.setDepFullName(WkOrgUtils.prepareDepsNameByTreeStyle(Lists.newArrayList(dep.getSid()), 100));

            } else {
                requireConfirmDepVO.setDepName("找不到對應名稱 [" + requireConfirmDepVO.getDepSid() + "]");
            }
        }

        // ====================================
        // 轉顯示資料 : 負責人
        // ====================================
        if (dep != null) {
            requireConfirmDepVO.setOwnerName(
                    this.requireConfirmDepService.prepareOwnerName(
                            requireConfirmDepVO.getOwnerSid(), dep.getSid()));
        }

        // ====================================
        // 收集歷程資料
        // ====================================
        List<RequireConfirmDepTraceVO> traceMsgs = Lists.newArrayList();

        // 需求完成確認單位歷程
        if (WkStringUtils.notEmpty(reqConfirmHistorys)) {
            traceMsgs.addAll(reqConfirmHistorys);
        }

        // 以發生時間排序
        traceMsgs = traceMsgs.stream()
                .sorted(Comparator.comparingLong(
                        each -> WkDateUtils.safeToTimeMillis(((RequireConfirmDepTraceVO) each).getDate(), Long.MAX_VALUE))
                        .reversed())
                .collect(Collectors.toList());

        requireConfirmDepVO.setTraceMsgs(traceMsgs);

    }

    /**
     * 比對登入部門對應的確認檔
     */
    public Long findLoginUserConfirmDepSid(
            List<RequireConfirmDepVO> allRequireConfirmDepVOs,
            Integer loginUserSid) {

        // 若無確認檔資料無須比對
        if (WkStringUtils.isEmpty(allRequireConfirmDepVOs)) {
            return null;
        }

        // ====================================
        // 以 dep sid 整理資料
        // ====================================
        Map<Integer, RequireConfirmDepVO> requireConfirmDepVOMapByDepSid = allRequireConfirmDepVOs.stream()
                .collect(Collectors.toMap(
                        RequireConfirmDepVO::getDepSid,
                        eachVo -> eachVo));

        // ====================================
        // 取得登入者部門對應的『確認檔』資料
        // ====================================
        // 取得 登入者 所有可操作的單位
        List<Integer> userMatchDepSids = this.requireConfirmDepService.prepareUserMatchDepSids(
                loginUserSid);

        // 比對需求確認單位是否在可操作單位清單中
        for (Integer userMatchDepSid : userMatchDepSids) {
            if (requireConfirmDepVOMapByDepSid.keySet().contains(userMatchDepSid)) {
                // 優先命中
                return requireConfirmDepVOMapByDepSid.get(userMatchDepSid).getSid();
            }
        }
        return null;
    }

    /**
     * 準備【需求單位確認部門-負責人】下拉選單項目
     */
    public List<SelectItem> prepareLoginDepOwnerSelectItems(RequireConfirmDepVO loginUserRequireConfirmDepVO) {

        // ====================================
        // 初始化
        // ====================================
        // 初始化負責人下拉選項
        List<SelectItem> confirmDepUserItems = Lists.newArrayList();

        if (loginUserRequireConfirmDepVO == null) {
            return confirmDepUserItems;
        }

        // ====================================
        // 查詢部門所有使用者
        // ====================================
        // 取得登入者部門資料
        Org loginUserDep = this.wkOrgCache.findBySid(loginUserRequireConfirmDepVO.getDepSid());
        if (loginUserDep == null) {
            log.warn("RequireConfirmDep 檔找不到對應的部門資訊\r\n" + new Gson().toJson(loginUserRequireConfirmDepVO));
            return confirmDepUserItems;
        }

        // 查詢所有部門使用者
        List<User> depUsers = WkUserUtils.findAllUserByDepSid(loginUserDep.getSid());
        // 以暱稱排序
        depUsers = depUsers.stream()
                .sorted(Comparator.comparing(user -> WkStringUtils.safeTrim(user.getName()).toUpperCase()))
                .collect(Collectors.toList());

        // ====================================
        // 建立單位主管選項 (置頂)
        // ====================================
        // 規則, 當 owner sid = -1 時, 代表為部門主管
        // 故於第一筆固定產生 value = -1 的項目

        // 取得部門主管
        User depManager = loginUserDep.getManager().toUser();

        Integer depManagerSid = null;
        if (depManager != null) {
            depManagerSid = depManager.getSid();
            confirmDepUserItems.add(new SelectItem(WkConstants.MANAGER_VIRTAUL_USER_SID, depManager.getName() + " (單位主管)"));
        } else {
            // 部門未設定單位主管 (應該不會發生)
            confirmDepUserItems.add(new SelectItem(WkConstants.MANAGER_VIRTAUL_USER_SID, "單位主管"));
        }

        // ====================================
        // 建立確認單位向下所有使用者選項
        // ====================================
        // 規則
        // 1.以暱稱排序
        // 2.停用者不顯示
        // 3.若停用者已在確認檔中被設定為負責人(ownerSid), 未避免項目亂跳, 此停用使用者項目保留

        // 此 flag 用於檢查確認檔中設定的負責人是否已經在選單
        boolean isUserInMenu = false;

        // 逐筆產生
        for (User user : depUsers) {
            // 以下狀況跳過
            // 單位主管 (上方已產生)
            if (user == null
                    || user.getSid() == null
                    || user.getSid().equals(depManagerSid)) {
                continue;
            }

            // 1.過濾使用者為停用
            // 2.但若已停用使用者已被設為負責人, 則保留項目 (考慮開發排程將停用者改為回部門主管)
            if (Activation.INACTIVE.equals(user.getStatus()) &&
                    loginUserRequireConfirmDepVO.getOwnerSid() != user.getSid()) {
                continue;
            }

            // 產生選項
            confirmDepUserItems.add(new SelectItem(user.getSid(), this.prepareUserName(user)));

            // 判斷被設定的負責人,已存在選項中
            if (WkCommonUtils.compareByStr(loginUserRequireConfirmDepVO.getOwnerSid(), user.getSid())) {
                isUserInMenu = true;
            }
        }

        // ====================================
        // 特殊組織異動規則 (置於最下方)
        // ====================================
        // 若被設定的負責人不在選項(也不是-1[單位主管])中, 代表該使用者已換單位, 要另外產生選項
        if (!isUserInMenu && loginUserRequireConfirmDepVO.getOwnerSid() != -1) {
            // 取得負責人資料
            User foreignUser = this.wkUserCache.findBySid(loginUserRequireConfirmDepVO.getOwnerSid());
            if (foreignUser == null) {
                log.error("找不到設定的負責人資料: RequireConfirmDep.OwnerSid:[" + loginUserRequireConfirmDepVO.getOwnerSid() + "]");
            } else {
                // 將使用者名稱加上單位 （麵包屑 style）
                String foreignUserName = WkUserUtils.prepareUserNameWithDep(foreignUser.getSid(), OrgLevel.THE_PANEL, true, "-");
                if (Activation.INACTIVE.equals(foreignUser.getStatus())) {
                    foreignUserName = "(停用)" + foreignUserName;
                }
                confirmDepUserItems.add(new SelectItem(foreignUser.getSid(), foreignUserName));
            }
        }

        return confirmDepUserItems;
    }

    /**
     * @param user
     * @return
     */
    private String prepareUserName(User user) {
        if (user == null) {
            return "";
        }

        String userName = user.getName();
        if (Activation.INACTIVE.equals(user.getStatus())) {
            userName = "(停用)" + userName;
        }

        return userName;
    }

    /**
     * 檢查部門是否有未完成的子單
     * 
     * @param depSid    需求確認部門 sid （分派收束後）
     * @param requireNo 需求單號
     * @return
     */
    public String validateNotFinish(Integer depSid, String requireNo) {

        if (depSid == null) {
            return "";
        }

        String message = "";

        // ====================================
        // 需求確認單位, 可能由哪些分派部門組成
        // ====================================
        List<Org> createDeps = this.requireConfirmDepService.prepareReqConfirmDepRelationDep(depSid);

        // ====================================
        // 原型確認單
        // ====================================
        List<PtCheck> ptChecks = this.ptService.queryByCompleteStatus(requireNo, createDeps, false);
        if (WkStringUtils.notEmpty(ptChecks)) {
            message += "<span style='text-decoration:underline;font-weight:bold;'>【原型確認單】" + ptChecks.size() + " 則，將強制結案:</span><br/>";
            for (PtCheck ptCheck : ptChecks) {
                message += String.format("★『%s』『%s』：%s<br/>",
                        ptCheck.getPtNo(),
                        ptCheck.getPtStatus().getLabel(),
                        ptCheck.getTheme());
            }
            message += "<br/>";
        }

        // ====================================
        // 送測單
        // ====================================
        List<WorkTestInfo> workTestInfos = this.sendTestService.queryByCompleteStatus(requireNo, createDeps, false);
        if (WkStringUtils.notEmpty(workTestInfos)) {
            message += "<span style='text-decoration:underline;font-weight:bold;'>【送測單】" + workTestInfos.size()
                    + " 則，將<span style='color:red'>不會</span>強制結案:</span><br/>";
            for (WorkTestInfo workTestInfo : workTestInfos) {
                message += String.format("★『%s』『%s』：%s<br/>",
                        workTestInfo.getTestinfoNo(),
                        workTestInfo.getTestinfoStatus().getValue(),
                        workTestInfo.getTheme());
            }

            message += "<br/>";
        }

        // ====================================
        // onpg
        // ====================================
        List<WorkOnpg> workOnpgs = this.onpgService.queryByCompleteStatus(requireNo, createDeps, false);
        if (WkStringUtils.notEmpty(workOnpgs)) {
            message += "<span style='text-decoration:underline;font-weight:bold;'>【ON程式】" + workOnpgs.size() + " 則，將強制結案:</span><br/>";
            for (WorkOnpg workOnpg : workOnpgs) {
                message += String.format("★『%s』『%s』：%s<br/>",
                        workOnpg.getOnpgNo(),
                        workOnpg.getOnpgStatus().getLabel(),
                        workOnpg.getTheme());
            }
            message += "<br/>";
        }

        // ====================================
        // 其他設定資訊
        // ====================================
        List<OthSet> othSets = this.othSetService.queryByCompleteStatus(requireNo, createDeps, false);
        if (WkStringUtils.notEmpty(othSets)) {
            message += "<span style='text-decoration:underline;font-weight:bold;'>【其他設定資訊】" + othSets.size() + " 則，將強制結案:</span><br/>";
            for (OthSet othSet : othSets) {
                message += String.format("★『%s』『%s』：%s<br/>",
                        othSet.getOsNo(),
                        othSet.getOsStatus().getVal(),
                        othSet.getTheme());
            }
            message += "<br/>";
        }

        // ====================================
        // 組回傳說明文字
        // ====================================
        if (WkStringUtils.isEmpty(message)) {
            return "";
        }

        String templetMsg = ""
                + "<span style='font-weight:bold;'>【%s】</span>(含以下部門)，尚有開立單據未完成：<br/>"
                + "<br/>"
                + "%s"
                + "<br/>"
                + "<br/>"
                + "<span style='font-size:18px;font-weight:bold;color:red;'>是否要強制完成？</span>"
                + "";

        return String.format(
                templetMsg,
                WkOrgUtils.findNameBySid(depSid),
                message);
    }

}
