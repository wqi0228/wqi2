package com.cy.tech.request.web.controller.home;

import com.cy.tech.request.vo.template.component.ComWebLayerNotify;
import com.cy.tech.request.web.pf.utils.ConfirmDialogController;
import java.io.Serializable;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author jason_h
 */
@Controller
@Scope("view")
public class ConfirmDlgMBean implements ComWebLayerNotify, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4646760521796858666L;
    @Getter
    @Autowired
    private ConfirmDialogController dlgConfirm;

    /**
     * 設定提示訊息並執行更新
     *
     * @param msg
     */
    public void setMsgAndUpdate(String msg) {
        dlgConfirm.showAlertWithUpdate("確認", msg, "confimDlgTemplate", "confimDlgTemplate");
    }

    /**
     * 設定提示訊息並執行更新
     *
     * @param msg
     */
    public void setMsgAndUpdate(String title, String msg) {
        dlgConfirm.showAlertWithUpdate(title, msg, "confimDlgTemplate", "confimDlgTemplate");
    }

    @Override
    public void notify(String msg) {
        this.setMsgAndUpdate(msg);
    }

}
