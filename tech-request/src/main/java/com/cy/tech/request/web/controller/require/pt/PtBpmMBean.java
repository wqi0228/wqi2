/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require.pt;

import com.cy.tech.request.web.controller.require.*;
import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.tech.request.logic.service.pt.PtBpmService;
import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.vo.value.to.BpmTaskTo;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 功能按鍵顯示處理
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class PtBpmMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1187753144739535455L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private PtBpmService ptBpmService;
    @Autowired
    transient private DisplayController display;

    /** 暫存物件 - 退回 | 作廢 用 */
    @Getter
    private PtCheck refrePt;

    @Getter
    private SelectItem[] rollbackItems;

    /** 選擇的退回物件 */
    @Getter
    @Setter
    private Integer rollbackSelectIndex;

    /** 退回理由 */
    @Getter
    @Setter
    private String rollbackComment;

    /** 作廢追蹤理由 */
    @Getter
    @Setter
    private String invaildTraceReason;

    public boolean showSignBtn(PtCheck ptCheck) {
        return ptBpmService.showSignBtn(ptCheck, loginBean.getUser());
    }

    public boolean showRollBackBtn(PtCheck ptCheck) {
        return ptBpmService.showRollBackBtn(ptCheck, loginBean.getUser());
    }

    public boolean showRecoveryBtn(PtCheck ptCheck) {
        return ptBpmService.showRecoveryBtn(ptCheck, loginBean.getUser());
    }

    public boolean showInvaildBtn(PtCheck ptCheck) {
        return ptBpmService.showInvaildBtn(ptCheck, loginBean.getUser());
    }

    public void clickSign(Require01MBean r01MBean, PtCheck ptCheck) {
        try {
            ptBpmService.doSign(ptCheck, loginBean.getUser(), "");
            r01MBean.getTitleBtnMBean().clear();
            r01MBean.getTraceMBean().clear();
        } catch (Exception e) {
            log.error(ptCheck.getPtNo() + ":" + loginBean.getUser().getName() + ":簽核失敗..." + e.getMessage(), e);
            r01MBean.getPtBottomMBean().recoveryView(r01MBean, ptCheck);
            MessagesUtils.showError(e.getMessage());
        }
    }

    public void clickRecovery(Require01MBean r01MBean, PtCheck ptCheck) {
        try {
            ptBpmService.doRecovery(ptCheck, loginBean.getUser());
        } catch (Exception e) {
            log.error(ptCheck.getPtNo() + ":" + loginBean.getUser().getName() + ":復原失敗..." + e.getMessage(), e);
            r01MBean.getPtBottomMBean().recoveryView(r01MBean, ptCheck);
            MessagesUtils.showError(e.getMessage());
        }
    }

    /**
     * 執行退回前判斷
     *
     * @param ptCheck
     */
    public void doRollBackBefor(PtCheck ptCheck) {
        BpmTaskTo taskTo = ptCheck.getSignInfo().getTaskTo();
        rollbackComment = "";
        List<ProcessTaskBase> copy = Lists.newArrayList(taskTo.getTasks());
        copy.remove(copy.size() - 1);
        rollbackItems = new SelectItem[copy.size()];
        int index = 0;
        for (ProcessTaskBase each : copy) {
            String label = each.getTaskName().contains("申請人") ? "申請人" : each.getRoleName();
            SelectItem item = new SelectItem(taskTo.getTasks().indexOf(each), label);
            rollbackItems[index++] = item;
        }
        refrePt = ptCheck;
    }

    public void clickRollBack(Require01MBean r01MBean) {
        try {
            BpmTaskTo taskTo = refrePt.getSignInfo().getTaskTo();
            ptBpmService.doRollBack(refrePt, loginBean.getUser(), (ProcessTaskHistory) taskTo.getTasks().get(rollbackSelectIndex), rollbackComment);
            List<PtCheck> ptChecks = r01MBean.getPtBottomMBean().findPtChecks(r01MBean);
            display.execute("PF('pt_acc_panel_layer_zero').select(" + ptChecks.indexOf(refrePt) + ")");
        } catch (Exception e) {
            log.error(refrePt.getPtNo() + ":" + loginBean.getUser().getName() + ":退回失敗..." + e.getMessage(), e);
            r01MBean.getPtBottomMBean().recoveryView(r01MBean, refrePt);
            MessagesUtils.showError(e.getMessage());
        }
    }

    /**
     * 執行作廢前清空理由
     *
     * @param ptCheck
     */
    public void initOpenInvaildDialog(PtCheck ptCheck) {
        this.invaildTraceReason = "";
        refrePt = ptCheck;
    }

    public void clickInvaild(Require01MBean r01MBean) {
        try {
            ptBpmService.doInvaild(r01MBean.getRequire(), refrePt, loginBean.getUser(), invaildTraceReason);
            List<PtCheck> ptChecks = r01MBean.getPtBottomMBean().findPtChecks(r01MBean);
            display.execute("PF('pt_acc_panel_layer_zero').select(" + ptChecks.indexOf(refrePt) + ")");
            r01MBean.getTraceMBean().clear();
            r01MBean.getTitleBtnMBean().clear();
        } catch (Exception e) {
            log.error(refrePt.getPtNo() + ":" + loginBean.getUser().getName() + ":作廢失敗..." + e.getMessage(), e);
            r01MBean.getPtBottomMBean().recoveryView(r01MBean, refrePt);
            MessagesUtils.showError(e.getMessage());
        }
    }

    public void assignRefre(PtCheck ptCheck) {
        refrePt = ptCheck;
    }

}
