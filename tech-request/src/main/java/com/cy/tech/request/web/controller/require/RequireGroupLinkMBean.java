package com.cy.tech.request.web.controller.require;

import com.cy.commons.util.FusionUrlServiceUtils;
import com.cy.tech.request.logic.enumerate.PropKeyType;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.group.logic.RelevanceManager;
import com.cy.work.group.vo.WorkLinkGroupDetail;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 關聯TAB控制
 *
 * @author kasim
 */
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class RequireGroupLinkMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4495638431266517414L;
    @Autowired
    transient private URLService urlService;
    @Autowired
    transient private RelevanceManager relevanceManager;
    @Autowired
    transient private SearchService searchHelper;
    @Autowired
    transient private LoginBean loginBean;

    @Getter
    private List<WorkLinkGroupDetail> linkDetails;

    @PostConstruct
    public void clear() {
        linkDetails = null;
    }

    public List<WorkLinkGroupDetail> findDetails(Require require) {
        if (linkDetails == null) {
            linkDetails = Lists.newArrayList();
            if (require.getLinkGroup() != null) {
                linkDetails = relevanceManager.findByLinkGroupAndExcludeNo(
                          require.getLinkGroup(), require.getRequireNo());
            }
        }
        return linkDetails;
    }

    public String getUrl(WorkLinkGroupDetail detail) {
        if (WorkSourceType.TECH_REQUEST.equals(detail.getType())) {
            return urlService.createLoacalURLLink(URLService.URLServiceAttr.URL_ATTR_M,
                      urlService.createSimpleUrlTo(loginBean.getUser().getSid(), detail.getSourceNo(), 1));
        } else if (WorkSourceType.TECH_ISSUE.equals(detail.getType())) {
            String issueURL = FusionUrlServiceUtils.getUrlByPropKey(PropKeyType.TECH_ISSUE_AP_URL.getValue());
            return issueURL + "/techIssue/techIssue11.xhtml?technicalCaseNo=" + detail.getSourceNo();
        }
        return "";
    }

    public String getTheme(WorkLinkGroupDetail detail) {
        if (WorkSourceType.TECH_REQUEST.equals(detail.getType())) {
            return searchHelper.combineFromJsonStr(detail.getTheme());
        } else if (WorkSourceType.TECH_ISSUE.equals(detail.getType())) {
            return detail.getTheme();
        }
        return "";
    }

}
