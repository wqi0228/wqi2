/**
 * 
 */
package com.cy.tech.request.web.controller.setting.backand;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.setting.SettingCheckConfirmRightService;
import com.cy.tech.request.logic.vo.SettingCheckConfirmRightVO;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerCallback;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerComponent;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerConfig;
import com.cy.tech.request.web.controller.component.mipker.helper.MultItemPickerByOrgHelper;
import com.cy.tech.request.web.controller.component.mipker.vo.MultItemPickerShowMode;
import com.cy.tech.request.web.controller.component.qkstree.QuickSelectTreeCallback;
import com.cy.tech.request.web.controller.component.qkstree.impl.OrgUserTreeMBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.common.vo.WkItem;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 後台維護：可檢核權限設定
 * 
 * @author allen1214_wu
 */
@Slf4j
@Controller
@Scope("view")
public class SettingBackandCheckConfirmRightMBean {

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private transient SettingCheckConfirmRightService settingCheckConfirmRightService;
    @Autowired
    private transient MultItemPickerByOrgHelper multItemPickerByOrgHelper;
    @Autowired
    private transient DisplayController displayController;
    @Autowired
    private transient WkOrgCache wkOrgCache;
    @Autowired
    private transient WkUserCache wkUserCache;

    // ========================================================================
    // 元件
    // ========================================================================
    /**
     * 可檢查部門選擇器
     */
    @Getter
    private transient MultItemPickerComponent canCheckDepsPicker;
    /**
     * 人員選擇器
     */
    @Autowired
    @Getter
    private transient OrgUserTreeMBean userQSTreeMBean;

    // ========================================================================
    // view 變數
    // ========================================================================
    /**
     * 所有的設定資料
     */
    @Getter
    private List<SettingCheckConfirmRightVO> allSettings;

    /**
     * 
     */
    @Getter
    private List<UserRightVO> userRights;

    /**
     * 編輯中的設定檔
     */
    private SettingCheckConfirmRightVO targetSetting;
    /**
     * 
     */
    private boolean isCanCheckUserTree = false;

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * 初始化
     */
    @PostConstruct
    public void init() {
        // ====================================
        // 依據『系統別』，查詢目前所有設定
        // ====================================
        try {
            this.allSettings = this.settingCheckConfirmRightService.findAllSetting();
        } catch (Exception e) {
            String message = "查詢『可檢查確認權限』設定失敗![" + e.getMessage() + "]";
            MessagesUtils.showError(message);
            log.error(message, e);
        }
    }

    /**
     * 
     */
    public void showUserRight() {

        // ====================================
        // 取得各系統別可使用名單
        // ====================================
        Map<Integer, UserRightVO> userRightMapByUserSid = Maps.newHashMap();

        try {

            for (RequireCheckItemType checkItemType : RequireCheckItemType.values()) {

                // 查詢系統別的可使用人員
                Set<Integer> userSids = this.settingCheckConfirmRightService.findCanCheckUserSids(checkItemType);

                for (Integer userSid : userSids) {
                    // 取得權限資料容器
                    UserRightVO userRightVO = userRightMapByUserSid.get(userSid);

                    // 資料還不存在, 新建使用者資料
                    if (userRightVO == null) {

                        // 使用者資料檔
                        User user = this.wkUserCache.findBySid(userSid);
                        if (user == null) {
                            continue;
                        }
                        // 使用者主要單位檔
                        Org primaryOrg = this.wkOrgCache.findBySid(user.getPrimaryOrg().getSid());
                        if (primaryOrg == null) {
                            continue;
                        }

                        userRightVO = new UserRightVO();
                        userRightMapByUserSid.put(userSid, userRightVO);
                        // 使用者名稱
                        userRightVO.setUserName(user.getName());
                        // 使用者名稱 （含部門）
                        userRightVO.setFullUserName(WkUserUtils.prepareUserNameWithDep(userSid, OrgLevel.MINISTERIAL, true, "-"));
                        // 部門排序
                        userRightVO.setOrgOrder(this.wkOrgCache.findOrgOrderSeqBySid(primaryOrg.getSid()));
                    }

                    // 加入系統別權限
                    switch (checkItemType) {
                    case NONE:
                        userRightVO.setUserRight_none(true);
                        break;
                    case BBIN:
                        userRightVO.setUserRight_bbin(true);
                        break;
                    case XBB:
                        userRightVO.setUserRight_xbb(true);
                        break;
                    case BBGP:
                        userRightVO.setUserRight_bbgp(true);
                        break;    
                    default:
                        MessagesUtils.showError("系統錯誤，資料中有未判斷的系統別!");
                        // 中斷處理, 直接回頁面
                        return;
                    }
                }
            }

            Comparator<UserRightVO> comparator = Comparator.comparing(UserRightVO::getOrgOrder);
            comparator = comparator.thenComparing(Comparator.comparing(UserRightVO::getUserName));

            // 排序
            this.userRights = userRightMapByUserSid.values().stream()
                    .sorted(comparator)
                    .collect(Collectors.toList());

        } catch (Exception e) {
            String message = WkMessage.EXECTION + "[" + e.getMessage() + "]";
            MessagesUtils.showError(message);
            log.error(message, e);
            return;
        }

        this.displayController.showPfWidgetVar("settingBackandCheckConfirmRight_userCheckRight_dlg");
    }

    /**
     * 單一使用者資料容器
     * 
     * @author allen1214_wu
     */
    @Getter
    @Setter
    public class UserRightVO implements Serializable {
        /**
         * 
         */
        private static final long serialVersionUID = -6121893102747045805L;
        public String userName;
        public String fullUserName;
        public int orgOrder;
        public boolean userRight_none = false;
        public boolean userRight_bbin = false;
        public boolean userRight_xbb = false;
        public boolean userRight_bbgp = false;
    }

    public void userQKtree_confirm() {
        if (this.isCanCheckUserTree) {
            this.canCheckUsers_confirm();
        } else {
            this.checkUserRight_confirm();
        }
    }

    // ========================================================================
    // 『可檢查單位』設定視窗相關
    // ========================================================================
    /**
     * 開啟『可檢查單位』設定視窗
     * 
     * @param targetSetting
     */
    public void canCheckDeps_openDialog(SettingCheckConfirmRightVO targetSetting) {

        // ====================================
        // 防呆
        // ====================================
        if (targetSetting == null) {
            MessagesUtils.showError("找不到選擇資料, 請洽資訊人員！");
            log.error("傳入 SettingCheckConfirmRightVO 為空！");
            return;
        }

        this.targetSetting = targetSetting;

        // ====================================
        // 初始化單位選擇元件
        // ====================================
        try {
            // 選擇器設定資料
            MultItemPickerConfig config = new MultItemPickerConfig();
            config.setDefaultShowMode(MultItemPickerShowMode.TREE);
            config.setTreeModePrefixName("單位");
            config.setContainFollowing(true);
            config.setItemComparator(this.multItemPickerByOrgHelper.parpareComparator());

            // 選擇器初始化
            this.canCheckDepsPicker = new MultItemPickerComponent(config, canCheckDepsMultItemPickerCallback);

        } catch (Exception e) {
            String message = "可檢查單位選單初始化失敗!" + e.getMessage();
            MessagesUtils.showError(message);
            log.error(message, e);
            return;
        }

        // 開啟 dialog
        this.displayController.showPfWidgetVar("settingBackandCheckConfirmRight_canCheckdeps_dlg");

    }

    /**
     * 儲存『可檢查單位』
     */
    public void canCheckDeps_confirm() {

        // ====================================
        // 取得選擇的單位
        // ====================================
        List<Integer> canCheckDepts = this.multItemPickerByOrgHelper.itemsToSids(this.canCheckDepsPicker.getSelectedItems());

        // ====================================
        // 依據檢查項目別，儲存可檢查單位
        // ====================================
        try {
            this.settingCheckConfirmRightService.saveCanCheckDepts(
                    this.targetSetting.getCheckItemType(),
                    canCheckDepts,
                    SecurityFacade.getUserSid());

        } catch (Exception e) {
            String message = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            MessagesUtils.showError(message);
            log.error(message, e);
            return;
        }

        // ====================================
        // 比對前後差異
        // ====================================

        // 增加的
        List<Integer> addDeps = Lists.newArrayList();
        if (WkStringUtils.notEmpty(canCheckDepts)) {
            addDeps = Lists.newArrayList(canCheckDepts);
        }

        if (WkStringUtils.notEmpty(this.targetSetting.getCanCheckDepts())) {
            addDeps.removeAll(this.targetSetting.getCanCheckDepts());
        }

        // 減少的
        List<Integer> deleteDeps = Lists.newArrayList();
        if (WkStringUtils.notEmpty(this.targetSetting.getCanCheckDepts())) {
            deleteDeps = Lists.newArrayList(this.targetSetting.getCanCheckDepts());
        }
        if (WkStringUtils.notEmpty(canCheckDepts)) {
            deleteDeps.removeAll(canCheckDepts);
        }

        String message = "";
        if (WkStringUtils.notEmpty(addDeps)) {
            message += "<div style='text-decoration:underline;font-weight:bold;'>新增</div>";
            message += WkOrgUtils.findNameBySid(addDeps, "<br/>");
        }
        if (WkStringUtils.notEmpty(deleteDeps)) {
            if (WkStringUtils.notEmpty(message)) {
                message += "<br/><br/>";
            }
            message += "<div style='text-decoration:underline;font-weight:bold;'>移除</div>";
            message += WkOrgUtils.findNameBySid(deleteDeps, "<br/>");
        }

        if (WkStringUtils.notEmpty(message)) {
            message = "<br/>" + message + "<hr/><br/><span class='WS1-1-3'>設定將即刻生效!</span>";
        } else {
            message += "設定資料未異動!";
        }

        // ====================================
        // 畫面控制
        // ====================================
        MessagesUtils.showInfo(message);
        // 關閉 dialog
        this.displayController.hidePfWidgetVar("settingBackandCheckConfirmRight_canCheckdeps_dlg");

        // ====================================
        // 重撈資料
        // ====================================
        this.init();
    }

    /**
     * 可執行單位設定選單 callback
     */
    private final MultItemPickerCallback canCheckDepsMultItemPickerCallback = new MultItemPickerCallback() {

        /**
         * 
         */
        private static final long serialVersionUID = -9070029559966748421L;

        /**
         * 取得所有可選擇單位
         */
        @Override
        public List<WkItem> prepareAllItems() throws SystemDevelopException {
            // 取得所有單位
            return multItemPickerByOrgHelper.prepareAllOrgItems();
        }

        /**
         * 取得已選擇部門
         */
        @Override
        public List<WkItem> prepareSelectedItems() throws SystemDevelopException {
            // 取得已設定的部門
            Set<Integer> canCheckDeps = Sets.newHashSet();
            if (WkStringUtils.notEmpty(targetSetting.getCanCheckDepts())) {
                canCheckDeps.addAll(targetSetting.getCanCheckDepts());
            }

            // 轉為 WkItem 物件
            return multItemPickerByOrgHelper.prepareWkItemByDepSids(canCheckDeps);

        }

        /**
         * 準備 disable 的單位
         */
        @Override
        public List<String> prepareDisableItemSids() throws SystemDevelopException {
            // 不需要 Disable
            return Lists.newArrayList();
        }
    };

    // ========================================================================
    // 『可檢查人員』設定視窗相關
    // ========================================================================
    /**
     * 開啟『可檢查人員』設定視窗
     * 
     * @param targetSetting
     */
    public void canCheckUsers_openDialog(SettingCheckConfirmRightVO targetSetting) {

        this.isCanCheckUserTree = true;

        // ====================================
        // 防呆
        // ====================================
        if (targetSetting == null) {
            MessagesUtils.showError("找不到選擇資料, 請洽資訊人員！");
            log.error("傳入 SettingCheckConfirmRightVO 為空！");
            return;
        }

        this.targetSetting = targetSetting;

        // ====================================
        // 初始化單位選擇元件
        // ====================================
        try {

            Org comp = WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId());
            if (comp == null) {
                MessagesUtils.showError("公司ID:[" + SecurityFacade.getCompanyId() + "]找不到對應資料!");
                return;

            }

            this.userQSTreeMBean.init(
                    comp.getSid(),
                    SecurityFacade.getUserSid(),
                    canCheckUserQSTreeCallback);

            this.userQSTreeMBean.preOpenDlg();

        } catch (Exception e) {
            String message = "可檢查人員選單初始化失敗!" + e.getMessage();
            MessagesUtils.showError(message);
            log.error(message, e);
            return;
        }

        // 開啟 dialog
        this.displayController.showPfWidgetVar("settingBackandCheckConfirmRight_canCheckUsers_dlg");

    }

    /**
     * 儲存『可檢查人員』
     */
    private void canCheckUsers_confirm() {

        // ====================================
        // 取得選擇的單位
        // ====================================
        List<Integer> canCheckUsers = this.userQSTreeMBean.getSelectedDataList();

        // ====================================
        // 依據檢查項目別，儲存可檢查單位
        // ====================================
        try {
            this.settingCheckConfirmRightService.saveCanCheckUsers(
                    this.targetSetting.getCheckItemType(),
                    canCheckUsers,
                    SecurityFacade.getUserSid());

        } catch (Exception e) {
            String message = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            MessagesUtils.showError(message);
            log.error(message, e);
            return;
        }

        // ====================================
        // 比對前後差異
        // ====================================

        // 增加的 user
        List<Integer> addUsers = Lists.newArrayList();
        if (WkStringUtils.notEmpty(canCheckUsers)) {
            addUsers = Lists.newArrayList(canCheckUsers);
        }
        if (WkStringUtils.notEmpty(this.targetSetting.getCanCheckUsers())) {
            addUsers.removeAll(this.targetSetting.getCanCheckUsers());
        }

        // 減少的 user
        List<Integer> deleteUsers = Lists.newArrayList();
        if (WkStringUtils.notEmpty(this.targetSetting.getCanCheckUsers())) {
            deleteUsers = Lists.newArrayList(this.targetSetting.getCanCheckUsers());
        }
        deleteUsers.removeAll(canCheckUsers);

        String message = "";
        if (WkStringUtils.notEmpty(addUsers)) {
            message += "<div style='text-decoration:underline;font-weight:bold;'>新增</div>";
            message += WkUserUtils.findNameBySid(addUsers, "<br/>");
        }
        if (WkStringUtils.notEmpty(deleteUsers)) {
            if (WkStringUtils.notEmpty(message)) {
                message += "<br/><br/>";
            }
            message += "<div style='text-decoration:underline;font-weight:bold;'>移除</div>";
            message += WkUserUtils.findNameBySid(deleteUsers, "<br/>");
        }

        if (WkStringUtils.notEmpty(message)) {
            message = "<br/>" + message + "<hr/><br/><span class='WS1-1-3'>設定將即刻生效!</span>";
        } else {
            message += "設定資料未異動!";
        }

        // ====================================
        // 畫面控制
        // ====================================
        MessagesUtils.showInfo(message);
        // 關閉 dialog
        this.displayController.hidePfWidgetVar("settingBackandCheckConfirmRight_canCheckUsers_dlg");

        // ====================================
        // 重撈資料
        // ====================================
        this.init();
    }

    /**
     * 可執行人員設定選單 callback
     */
    private final QuickSelectTreeCallback canCheckUserQSTreeCallback = new QuickSelectTreeCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = 8035568180779567675L;

        @SuppressWarnings("unchecked")
        @Override
        public List<Integer> getSelectedDataList() {

            // 取得已設定的人員
            Set<Integer> canCheckUsers = Sets.newHashSet();
            if (WkStringUtils.notEmpty(targetSetting.getCanCheckUsers())) {
                canCheckUsers.addAll(targetSetting.getCanCheckUsers());
            }

            return Lists.newArrayList(canCheckUsers);
        }
    };

    // ========================================================================
    // 『人員檢查權限試算選擇視窗』
    // ========================================================================
    /**
     * 開啟『人員』選擇窗
     */
    public void checkUserRight_openDialog() {

        this.isCanCheckUserTree = false;

        // ====================================
        // 初始化單位選擇元件
        // ====================================
        try {

            Org comp = WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId());
            if (comp == null) {
                MessagesUtils.showError("公司ID:[" + SecurityFacade.getCompanyId() + "]找不到對應資料!");
                return;
            }

            this.userQSTreeMBean.init(
                    comp.getSid(),
                    SecurityFacade.getUserSid(),
                    checkUserRightQSTreeCallback);

            this.userQSTreeMBean.preOpenDlg();

        } catch (Exception e) {
            String message = "人員選單初始化失敗!" + e.getMessage();
            MessagesUtils.showError(message);
            log.error(message, e);
            return;
        }

        // 開啟 dialog
        this.displayController.showPfWidgetVar("settingBackandCheckConfirmRight_canCheckUsers_dlg");

    }

    /**
     * 確認選擇試算人員
     */
    private void checkUserRight_confirm() {

        // ====================================
        // 取得選擇的單位
        // ====================================
        List<Integer> userSids = this.userQSTreeMBean.getSelectedDataList();
        if (WkStringUtils.isEmpty(userSids)) {
            MessagesUtils.showInfo("未選擇試算人員!");
            return;
        }

        // ====================================
        // 依據檢查項目，儲存可檢查單位
        // ====================================
        try {

            String dataContent = "";
            for (Integer userSid : userSids) {
                // 使用者名稱
                String userName = WkUserUtils.prepareUserNameWithDep(userSid, OrgLevel.MINISTERIAL, true, "-");
                // 系統別
                List<RequireCheckItemType> checkItemTypes = this.settingCheckConfirmRightService.findUserCheckRights(userSid);
                dataContent += String.format("<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><tr/>",
                        userName,
                        checkItemTypes.contains(RequireCheckItemType.NONE) ? "★" : "",
                        checkItemTypes.contains(RequireCheckItemType.BBIN) ? "★" : "",
                        checkItemTypes.contains(RequireCheckItemType.XBB) ? "★" : "");
            }

            String allContent = ""
                    + "<table>"
                    + "<tr>"
                    + "<td>姓名</td>"
                    + "<td>無</td>"
                    + "<td>BBIN</td>"
                    + "<td>XBB</td>"
                    + "</tr>"
                    + dataContent
                    + "</table>";

            MessagesUtils.showInfo(allContent);

        } catch (Exception e) {
            String message = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            MessagesUtils.showError(message);
            log.error(message, e);
            return;
        }

        // 關閉 dialog
        this.displayController.hidePfWidgetVar("settingBackandCheckConfirmRight_canCheckUsers_dlg");

    }

    /**
     * 試算人員選單 callback
     */
    private final QuickSelectTreeCallback checkUserRightQSTreeCallback = new QuickSelectTreeCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = -7284121097455648234L;

        @SuppressWarnings("unchecked")
        @Override
        public List<Integer> getSelectedDataList() {
            // 預設開啟時，不選擇任何人
            return Lists.newArrayList();
        }
    };
}
