package com.cy.tech.request.web.controller.view.component;

import java.util.List;
import java.util.stream.Collectors;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.UserService;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserAutoComplete extends AutoCompleteComponent  {
    
    private UserService userService = WorkSpringContextHolder.getBean(UserService.class);

    // 多選user
    @Getter
    @Setter
    private List<User> selectedUserList = Lists.newArrayList();
    // 單選user
    @Getter
    @Setter
    private User selectedUser;
    
    public List<User> fuzzySearch(String text) {
        text = text.trim();
        List<User> resultList = userService.findAllByLike(text);
        
        filterSelected(resultList, selectedUserList);
        return resultList;
    }
    
    /**
     * add relation tags of itemSelect
     * @param event
     */
    public void itemSelect(SelectEvent event){
        User user = (User)event.getObject();
        log.debug("item select : {}", user.getId());
    }
    
    /**
     * remove tags of itemUnselect
     * @param event
     */
    public void itemUnselect(UnselectEvent event) {
        if(selectedUserList == null){
            selectedUserList = Lists.newArrayList();
        }
    }
    
    public List<Integer> getSelectedUserSids(){
        if(selectedUserList != null){
            return selectedUserList.stream().map(User::getSid).collect(Collectors.toList());
        }
        return Lists.newArrayList();
    }
}
