/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. 
 */
package com.cy.tech.request.web.controller.require.onpg;

import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.onpg.OnpgService;
import com.cy.tech.request.logic.service.onpg.OnpgShowService;
import com.cy.tech.request.logic.vo.WorkOnpgTo;
import com.cy.tech.request.vo.enums.SubNoticeType;
import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerCallback;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerCallbackDefaultImplType;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerComponent;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerConfig;
import com.cy.tech.request.web.controller.component.mipker.helper.MultItemPickerByOrgHelper;
import com.cy.tech.request.web.controller.component.reqconfirm.ReqConfirmClientHelper;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.helper.AssignDepHelper;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.ConfirmCallbackDialogController;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonContentUtils;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * ON程式 控制<BR/>
 * 對應 onpg_setting_dialog.xhtml
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope("view")
public class OnpgSettingMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8518604130259228463L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private OnpgService onpgService;
    @Autowired
    transient private OnpgShowService onpgShowService;
    @Autowired
    transient private DisplayController displayController;
    @Autowired
    transient private AssignDepHelper assignDepHelper;
    @Autowired
    transient private MultItemPickerByOrgHelper multItemPickerByOrgHelper;
    @Autowired
    transient private ConfirmCallbackDialogController confirmCallbackDialogController;

    /** 編輯用主檔 */
    @Getter
    @Setter
    private WorkOnpg editOnpg;
    /** 備份資料 - 用來檢核 */
    private WorkOnpgTo tempOnpgTo;

    /** 編輯狀態控制 */
    private OpEditType editType;
    /**
     * 歸屬單位選項
     */
    @Getter
    private List<SelectItem> ownerDepSelectItems;
    /**
     * 選擇的歸屬單位
     */
    @Getter
    @Setter
    private WkItem selectOwnerDepItem;

    private enum OpEditType {
        /** 檢視 */
        IS_VIEW,
        /** 新增 */
        IS_NEW,
        /** 編輯 */
        IS_EDIT,
        /** 編輯通知 */
        IS_EDIT_NOTIFY_UNIT,;
    }

    /**
     * 物件初始化
     */
    @PostConstruct
    public void init() {
        this.editType = OpEditType.IS_VIEW;
        editOnpg = new WorkOnpg();
    }

    /**
     * 開啟送 on 程式新增對話框
     * 
     * @param r01MBean
     */
    public void openDialogByNewAdd(Require01MBean r01MBean) {
        // 取得主單 bean
        if (r01MBean == null) {
            MessagesUtils.showWarn("資訊已逾期，請重新整理畫面");
            return;
        }
        Require require = r01MBean.getRequire();

        try {
            // 元件變數初始化
            this.init();
            // 建立編輯主檔
            this.editOnpg = onpgService.createEmptyOnpg(
                    require.getSid(),
                    require.getRequireNo(),
                    this.loginBean.getUser(),
                    new Date());

            // 準備單據歸屬單位選單資料
            List<WkItem> ownerDepItems = assignDepHelper.prepareLoginUserCaseOwnerDepsByAssignDep(require.getSid());
            if (ownerDepItems.size() == 1) {
                // 僅有一筆時, 不做下拉選單
                this.ownerDepSelectItems = null;
                this.selectOwnerDepItem = ownerDepItems.get(0);
            } else {
                // 多筆時，不預設選擇項目
                this.selectOwnerDepItem = null;
                // 將部門資料轉 SelectItem
                this.ownerDepSelectItems = ownerDepItems.stream()
                        .map(wkitem -> new SelectItem(wkitem, wkitem.getName()))
                        .collect(Collectors.toList());
            }

            // 定義為新增模式
            this.editType = OpEditType.IS_NEW;

        } catch (SystemOperationException e) {
            MessagesUtils.showError(e.getMessage());
            return;
        } catch (Exception e) {
            String message = WkMessage.EXECTION + "[" + e.getMessage() + "]";
            log.error(message, e);
            MessagesUtils.showError(message);
            return;
        }

        // onpg_setting_dlg_wv
        displayController.showPfWidgetVar("onpg_setting_dlg_wv");
    }

    /**
     * @param onpg
     */
    public void initByEdit(WorkOnpg onpg) {

        // 取得主單 bean
        if (onpg == null) {
            MessagesUtils.showWarn("資訊已逾期，請重新整理畫面");
            return;
        }

        try {
            this.editOnpg = onpgService.findByOnpgNo(onpg.getOnpgNo());
            this.tempOnpgTo = onpgService.findToByOnpgNo(onpg.getOnpgNo());

            // 準備單據歸屬單位選單資料
            this.selectOwnerDepItem = this.assignDepHelper.trnsOrgSidToWkItem(editOnpg.getCreateDep().getSid());
            this.ownerDepSelectItems = null; // 不顯示下拉選單

            this.editType = OpEditType.IS_EDIT;

        } catch (SystemOperationException e) {
            MessagesUtils.showError("系統處理錯誤，請嘗試重整頁面！" + e.getMessage());
            return;
        } catch (Exception e) {
            String message = WkMessage.EXECTION + "[" + e.getMessage() + "]";
            log.error(message, e);
            MessagesUtils.showError(message);
            return;
        }

        // onpg_setting_dlg_wv
        displayController.showPfWidgetVar("onpg_setting_dlg_wv");

    }

    public void closeDialog() {
        if (this.editType.equals(OpEditType.IS_EDIT)) {
            editOnpg = onpgService.findByOnpgNo(editOnpg.getOnpgNo());
        }
        this.editType = OpEditType.IS_VIEW;
    }

    public void cancelEdit(Require01MBean r01MBean) {
        if (Strings.isNullOrEmpty(r01MBean.getIssueCreateOnpgKeySid())) {
            return;
        }
        this.displayController.execute("location.reload();");
    }

    /**
     * 確認/儲存
     * 
     * @param r01MBean
     */
    public void save(Require01MBean r01MBean) {

        // ====================================
        // 輸入檢核
        // ====================================
        if (this.selectOwnerDepItem == null) {
            MessagesUtils.showWarn("請選擇『單據歸屬單位』!");
            return;
        }
        if (this.editOnpg.getEstablishDate() == null) {
            MessagesUtils.showWarn("請輸入『預計完成日』!");
            return;
        }
        if (WkStringUtils.isEmpty(editOnpg.getTheme())) {
            MessagesUtils.showWarn("請輸入『主題』!");
            return;
        }
        if (WkStringUtils.isEmpty(editOnpg.getNoticeDeps().getValue())) {
            MessagesUtils.showWarn("請選擇『通知單位』!");
            this.assignDepOpenDialog(r01MBean, openDialogEventFrom);
            return;
        }
        if (WkStringUtils.isEmpty(
                WkJsoupUtils.getInstance().clearCssTag(editOnpg.getContentCss()))) {
            MessagesUtils.showWarn("『內容』不可為空白!");
            return;
        }

        // ====================================
        // update
        // ====================================
        if (this.editType.equals(OpEditType.IS_EDIT) || this.editType.equals(OpEditType.IS_EDIT_NOTIFY_UNIT)) {
            try {
                // 編輯存檔
                onpgService.saveByEditOnpg(
                        r01MBean.getRequire(),
                        editOnpg,
                        loginBean.getUser(),
                        tempOnpgTo);

                // 變更模式
                this.editType = OpEditType.IS_VIEW;
                // 更新畫面
                this.updateScreen(r01MBean);

            } catch (UserMessageException e) {
                MessagesUtils.show(e);
            } catch (Exception e) {
                log.error(r01MBean.getRequire().getRequireNo() + ":" + loginBean.getUser().getName() + ":編輯ON程式失敗..." + e.getMessage(), e);
                r01MBean.reBuildeByUpdateFaild();
                MessagesUtils.showError(e.getMessage());
            }

            return;
        }

        // ====================================
        // insert
        // ====================================
        try {
            // 新增時, 取得歸屬單位
            Org createDep = WkOrgCache.getInstance().findBySid(
                    Integer.valueOf(this.selectOwnerDepItem.getSid()));
            editOnpg.setCreateDep(createDep);

            // insert
            this.onpgService.saveByNewOnpg(
                    r01MBean.getRequire(),
                    editOnpg,
                    Integer.valueOf(this.selectOwnerDepItem.getSid()),
                    loginBean.getUser(),
                    r01MBean.getIssueCreateOnpgKeySid(),
                    false);

            if (!Strings.isNullOrEmpty(r01MBean.getIssueCreateOnpgKeySid())) {
                r01MBean.clearIssueCreateOnpgKeySid();
            }
            this.editType = OpEditType.IS_VIEW;

            log.info("{} 新增ON程式單:{}", SecurityFacade.getUserId(), editOnpg.getOnpgNo());
            // 更新畫面
            this.updateScreen(r01MBean);

        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            log.error(r01MBean.getRequire().getRequireNo() + ":" + loginBean.getUser().getName() + ":編輯ON程式失敗..." + e.getMessage(), e);
            r01MBean.reBuildeByUpdateFaild();
            MessagesUtils.showError(e.getMessage());
        }
    }

    private void updateScreen(Require01MBean r01MBean) {
        r01MBean.reBuildeByUpdateFaild();
        r01MBean.getTraceMBean().clear();
        r01MBean.getOnpgBottomMBean().initTabInfo(r01MBean);
        r01MBean.getBottomTabMBean().resetTabIdx(r01MBean);
        r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.ONPG);
        r01MBean.getOnpgBottomMBean().initTabInfo(r01MBean);
        this.displayController.hidePfWidgetVar("onpg_setting_dlg_wv");// 主設定視窗

        int idx = 0;
        try {
            idx = r01MBean.getOnpgBottomMBean().getOnpgs().indexOf(this.editOnpg);
            if (idx == -1) {
                idx = 0;
            }
        } catch (Exception e) {
        }
        this.displayController.execute(""
                + "accPanelSelectTab('op_acc_panel_layer_zero', " + idx + ");"
                + ReqConfirmClientHelper.CLIENT_SRIPT_CLOSE_PANEL);
    }

    /**
     * 主題 / 內容 / 備註 / 預計完成日 / 通知 可編輯人員<BR/>
     * on程式填單人員所歸屬單位成員們（包含部門主管 與 處級單位主管）
     *
     * @return
     */
    public boolean disableSetModify() {
        if (this.editType.equals(OpEditType.IS_NEW)) {
            return false;
        }
        if (this.editType.equals(OpEditType.IS_EDIT)) {
            return !onpgShowService.isCreatedAndAboveManager(this.editOnpg, loginBean.getUser());
        }
        return true;
    }

    /**
     * 讀取 子程序 通知異動紀錄
     *
     * @param r01MBean
     * @param ptCheck
     */
    public void btnLoadSubNoticeInfo(Require01MBean r01MBean, WorkOnpg onpg) {
        r01MBean.loadSubNoticeInfo(onpg.getSourceSid(), onpg.getSid(),
                Lists.newArrayList(SubNoticeType.ONPG_INFO_DEP));
    }

    // ========================================================================
    // 通知單位元件
    // ========================================================================
    /**
     * 分享部門-編輯視窗
     */
    @Getter
    private final String ASSIGN_DEP_DLG_NAME = "ASSIGN_DEP_DLG_NAME_" + this.getClass().getSimpleName();

    /**
     * 分享部門-編輯視窗內容區
     */
    @Getter
    private final String ASSIGN_DEP_DLG_CONTENT = "ASSIGN_DEP_DLG_CONTENT_" + this.getClass().getSimpleName();
    /**
     * 分享部門-編輯視窗-部門選擇器ID
     */
    @Getter
    private final String ASSIGN_DEP_PICKER_ID = "ASSIGN_DEP_PICKER_ID_" + this.getClass().getSimpleName();

    /**
     * 分派單位選擇器
     */
    @Getter
    private transient MultItemPickerComponent assignDepPicker;

    /**
     * 記錄開啟時傳入的 Require01MBean
     */
    private transient Require01MBean require01MBean;

    /**
     * 記錄開啟編輯的來源
     */
    private transient String openDialogEventFrom = "";

    /**
     * 開啟通知單位編輯視窗
     * 
     * @param require01MBean Require01MBean
     * @param openDialogFrom 動作觸發來源來原
     */
    public void assignDepOpenDialog(Require01MBean require01MBean, String openDialogEventFrom) {

        this.require01MBean = require01MBean;
        this.openDialogEventFrom = openDialogEventFrom;

        try {
            // 選擇器設定資料
            MultItemPickerConfig config = new MultItemPickerConfig();
            config.setTreeModePrefixName("單位");
            config.setContainFollowing(true);
            config.setEnableGroupMode(true); // 開啟群組功能
            config.setItemComparator(this.multItemPickerByOrgHelper.parpareComparator());

            // 傳入 ID , 自動清除 filter
            config.setComponentID(ASSIGN_DEP_PICKER_ID);

            // 部分實作方法用 Default
            this.assignDepPickerCallback.setDefaultImplType(MultItemPickerCallbackDefaultImplType.DEP);

            // 選擇器初始化
            this.assignDepPicker = new MultItemPickerComponent(config, assignDepPickerCallback);

        } catch (Exception e) {
            String message = "通知單位選單初始化失敗!" + e.getMessage();
            MessagesUtils.showError(message);
            log.error(message, e);
            return;
        }

        // 更新畫面
        this.displayController.update(this.ASSIGN_DEP_DLG_CONTENT);
        // 開啟 dialog
        this.displayController.showPfWidgetVar(this.ASSIGN_DEP_DLG_NAME);
    }

    public void openDialogByEdit(Require01MBean r01MBean, WorkOnpg onpg) {
        editOnpg = onpgService.findByOnpgNo(onpg.getOnpgNo());
        tempOnpgTo = onpgService.findToByOnpgNo(onpg.getOnpgNo());
        this.assignDepOpenDialog(r01MBean, "EDIT_ONLY");
    }

    /**
     * 確認
     * 
     * @param isPreConfirm true:call by view , false:call by confirm callback
     */
    public void assignDepDepConfirm(boolean isPreConfirm) {
        
        // 防呆
        if (this.editOnpg == null
                || this.require01MBean == null
                || this.require01MBean.getRequire() == null
                || this.require01MBean.getRequire().getCreateDep() == null) {
            MessagesUtils.showWarn(WkMessage.NEED_RELOAD);
            return;
        }

        // ====================================
        // 檢察為空
        // ====================================
        if (WkStringUtils.isEmpty(this.assignDepPicker.getSelectedSid())) {
            MessagesUtils.showError("未挑選送測部門！！");
            return;
        }

        // ====================================
        // 取得異動前後單位清單
        // ====================================
        // 取得選單上已選擇的單位
        List<Integer> afterDepSids = this.assignDepPicker.getSelectedSid().stream()
                .filter(userSid -> WkStringUtils.isNumber(userSid))
                .map(userSid -> Integer.parseInt(userSid))
                .collect(Collectors.toList());

        // 取得異動前單位
        List<Integer> beforeDepSids = Lists.newArrayList();
        if (this.editOnpg.getNoticeDeps() != null
                && this.editOnpg.getNoticeDeps().getValue() != null) {
            // 轉數字
            beforeDepSids = this.editOnpg.getNoticeDeps().getValue().stream()
                    .distinct()
                    .filter(userSid -> WkStringUtils.isNumber(userSid))
                    .map(userSid -> Integer.parseInt(userSid))
                    .collect(Collectors.toList());
        }

        // 比對是否未異動
        if (WkCommonUtils.compare(beforeDepSids, afterDepSids)) {
            MessagesUtils.showInfo("未異動通知單位!");
            return;
        }
        // ====================================
        // 確認訊息
        // ====================================
        // 不為新增時, 跳出異動的確認訊息
        if (isPreConfirm && WkStringUtils.notEmpty(this.editOnpg.getSid())) {
            String drffMessage = WkCommonContentUtils.prepareDiffMessageForDepartment(
                    beforeDepSids,
                    afterDepSids,
                    "新增通知單位",
                    "移除通知單位");

            drffMessage = "<span class='WS1-1-3b'>異動【通知單位】結果如下：</span>"
                    + "<br/><br/>"
                    + drffMessage
                    + "<br/><br/>";

            this.confirmCallbackDialogController.showConfimDialog(
                    drffMessage,
                    Lists.newArrayList(),
                    () -> this.assignDepDepConfirm(false));
            return;
        }

        // ====================================
        // 更新主檔容器資料內的資料
        // ====================================
        this.editOnpg.getNoticeDeps().setValue(this.assignDepPicker.getSelectedSid());

        // ====================================
        // 單獨編輯通知單位 (非新增、非ON程式編輯模式)
        // ====================================
        if (this.editOnpg.getSid() != null && !"EDIT_ALL".equals(openDialogEventFrom)) {
            try {

                // 更新通知單位 + 建立異動記錄 + 首頁推撥
                this.onpgService.modifyNoticeDeps(
                        require01MBean.getRequire(),
                        this.editOnpg.getOnpgNo(),
                        this.assignDepPicker.getSelectedSid(),
                        WkUserCache.getInstance().findBySid(SecurityFacade.getUserSid()));

                // 取得最新資料
                this.editOnpg = this.onpgService.findByOnpgNo(this.editOnpg.getOnpgNo());

                // 更新畫面
                this.updateScreen(this.require01MBean);

            } catch (Exception e) {
                String message = "通知單位設定時發生錯誤! [" + e.getMessage() + "]";
                log.error(message, e);
                MessagesUtils.showError(message);
                return;
            }
        }

        // ====================================
        // 關閉對話框
        // ====================================
        this.displayController.hidePfWidgetVar(this.ASSIGN_DEP_DLG_NAME);
    }

    public String prepareNoticeDepsTooltip(WorkOnpg workOnpg) {
        if (workOnpg != null &&
                workOnpg.getNoticeDeps() != null &&
                WkStringUtils.notEmpty(workOnpg.getNoticeDeps().getValue())) {

            Set<Integer> depSids = Sets.newHashSet();
            for (String depSid : workOnpg.getNoticeDeps().getValue()) {
                if (!WkStringUtils.isNumber(depSid)) {
                    continue;
                }
                depSids.add(Integer.parseInt(depSid));
            }

            return WkOrgUtils.prepareDepsNameByTreeStyle(Lists.newArrayList(depSids), 50);
        }
        return "";
    }

    /**
     * 分派單位
     */
    private final MultItemPickerCallback assignDepPickerCallback = new MultItemPickerCallback() {

        /**
         * 
         */
        private static final long serialVersionUID = 2140645172123067415L;

        /**
         * 取得已選擇部門
         */
        @Override
        public List<WkItem> prepareSelectedItems() throws SystemDevelopException {

            // 防呆
            if (editOnpg == null
                    || editOnpg.getNoticeDeps() == null
                    || editOnpg.getNoticeDeps().getValue() == null) {
                return Lists.newArrayList();

            }

            // 由外部取回已選擇部門, 並轉換為 WkItem 物件
            return editOnpg.getNoticeDeps().getValue().stream()
                    .distinct() // 去重複
                    .filter(depSid -> WkStringUtils.isNumber(depSid)) // 轉數字
                    .map(depSid -> multItemPickerByOrgHelper.createDepItemByDepSid(Integer.valueOf(depSid))) // 轉 wkitem
                    .collect(Collectors.toList());
        }

        /**
         * 準備 disable 的單位
         */
        @Override
        public List<String> prepareDisableItemSids() throws SystemDevelopException {
            // 鎖定需求單位 (需求單位不可移除)
            return Lists.newArrayList(require01MBean.getRequire().getCreateDep().getSid() + "");
        }
    };

}
