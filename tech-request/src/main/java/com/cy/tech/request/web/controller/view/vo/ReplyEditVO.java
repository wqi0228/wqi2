/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.vo;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author brain0925_liao
 */
public class ReplyEditVO {

    public ReplyEditVO(String loginDepName, String loginUserName,
            String replyTime, String content,
            String os_sid, String os_no, String require_sid, String require_no,
            String os_history_sid, String os_reply_sid, String os_reply_and_reply_sid) {
        this.loginDepName = loginDepName;
        this.loginUserName = loginUserName;
        this.replyTime = replyTime;
        this.content = content;
        this.os_sid = os_sid;
        this.os_no = os_no;
        this.require_sid = require_sid;
        this.require_no = require_no;
        this.os_history_sid = os_history_sid;
        this.os_reply_sid = os_reply_sid;
        this.os_reply_and_reply_sid = os_reply_and_reply_sid;
    }
    @Getter
    private String os_reply_sid;
    @Getter
    private String loginDepName;
    @Getter
    private String loginUserName;
    @Getter
    private String replyTime;
    @Getter
    @Setter
    private String content;
    @Getter
    private String os_sid;
    @Getter
    private String os_no;
    @Getter
    private String require_sid;
    @Getter
    private String require_no;
    @Getter
    private String os_history_sid;
    @Getter
    private String os_reply_and_reply_sid;
}
