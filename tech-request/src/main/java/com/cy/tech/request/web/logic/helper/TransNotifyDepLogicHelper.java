/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.logic.helper;

import com.cy.tech.request.logic.service.CommonService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.onpg.TrWorkOnpgService;
import com.cy.tech.request.logic.service.othset.TrOsTransService;
import com.cy.tech.request.logic.service.pt.WorkPtCheckService;
import com.cy.tech.request.logic.service.send.test.TrWorkTestInfoService;
import com.cy.tech.request.logic.utils.SimpleDateFormatEnum;
import com.cy.tech.request.logic.utils.ToolsDate;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.RequireTransProgramType;
import com.cy.tech.request.vo.onpg.TrWorkOnpg;
import com.cy.tech.request.vo.pt.WorkPtCheck;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.tros.TrOs;
import com.cy.tech.request.vo.worktest.TrWorkTestInfo;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 通知部門轉單Logic元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class TransNotifyDepLogicHelper implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 7354147451607915508L;
    /** WorkPtCheckService */
	@Autowired
	private WorkPtCheckService ptCheckService;
	/** TrOsTransService */
	@Autowired
	private TrOsTransService trOsTransService;
	/** TrWorkOnpgService */
	@Autowired
	private TrWorkOnpgService trWorkOnpgService;
	/** TrWorkTestInfoService */
	@Autowired
	private TrWorkTestInfoService trWorkTestInfoService;
	/** RequireService */
	@Autowired
	private RequireService requireService;
	/** CommonService */
	@Autowired
	private CommonService commonService;

	/**
	 * 檢核
	 *
	 * @param to
	 */
	public void checkTrans(TransNotifyTo to) {
		Preconditions.checkState(!Strings.isNullOrEmpty(to.getSelTransProgramType()), "請挑選[程序挑選]");
		Preconditions.checkState(to.getSelReqTypes() != null
		        && !to.getSelReqTypes().isEmpty(), "請挑選[需求製作進度]");
		Preconditions.checkState(to.getSelSubProgramTypes() != null
		        && !to.getSelSubProgramTypes().isEmpty(), "請挑選[子程序狀態]");
		Preconditions.checkState(!Strings.isNullOrEmpty(to.getTransNos()), "請輸入[轉換單號]");
		Preconditions.checkState(to.getSourceMappOrg() != null, "請挑選[來源對應部門]");
		Preconditions.checkState(to.getTargetMappOrg() != null, "請挑選[目標新增部門]");
	}

	public void checkRecoveryTrans(TransNotifyTo to) {
		Preconditions.checkState(!Strings.isNullOrEmpty(to.getSelTransProgramType()), "請挑選[程序挑選]");
		Preconditions.checkState(!Strings.isNullOrEmpty(to.getTransNos()), "請輸入[轉換單號]");
		Preconditions.checkState(to.getTargetMappOrg() != null, "請挑選[還原單位]");
	}

	/**
	 * 進行轉單
	 *
	 * @param isTrans
	 * @param to
	 */
	public boolean transNotifyDep(Boolean isTrans, TransNotifyTo to) {
		boolean reuslt = true;
		to.startTrans();
		Map<String, Require> requireMap = Maps.newHashMap();
		String[] transNos = to.getTransNos().split(",");
		for (String each : Lists.newArrayList(transNos)) {
			try {
				to.addCount();
				String transNo = each.trim();
				Preconditions.checkState(!Strings.isNullOrEmpty(transNo), "子程序單號不可為空！！");
				Object sub = this.getSubProgram(RequireTransProgramType.valueOf(to.getSelTransProgramType()), transNo);
				Preconditions.checkNotNull(sub != null, "子程序單號：" + transNo + "，查無資料！！");
				Require require = this.getRequire(sub, requireMap, transNo);
				this.checkRequireStatus(to.getSelReqTypes(), require.getRequireStatus(), transNo);
				this.checkSsubProgramTypes(sub, to.getSelSubProgramTypes(), transNo);
				this.checkDep(sub, to, transNo);
				this.transDep(isTrans, to, sub, require);
			} catch (Exception e) {
				reuslt = false;
				to.addErrCount(e.getMessage());
			}
		}
		return reuslt;
	}

	public boolean recoveryTransNotifyDep(Boolean isTrans, TransNotifyTo to) {
		boolean reuslt = true;
		to.startTrans();
		Map<String, Require> requireMap = Maps.newHashMap();
		String[] transNos = to.getTransNos().split(",");
		for (String each : Lists.newArrayList(transNos)) {
			try {
				to.addCount();
				String transNo = each.trim();
				Preconditions.checkState(!Strings.isNullOrEmpty(transNo), "子程序單號不可為空！！");
				Object sub = this.getSubProgram(RequireTransProgramType.valueOf(to.getSelTransProgramType()), transNo);
				Preconditions.checkNotNull(sub != null, "子程序單號：" + transNo + "，查無資料！！");
				Require require = this.getRequire(sub, requireMap, transNo);
				this.checkRecoveryDep(sub, to, transNo);
				this.transRecoveryDep(isTrans, to, sub, require);
			} catch (Exception e) {
				reuslt = false;
				to.addErrCount(e.getMessage());
			}
		}
		return reuslt;
	}

	/**
	 * 取得子程序
	 *
	 * @param transProgramType
	 * @param transNo
	 * @return
	 */
	private Object getSubProgram(RequireTransProgramType type, String transNo) {
		if (null != type) {
			switch (type) {
			case PTCHECK:
				return ptCheckService.findByPtNo(transNo);
			case WORKTESTSIGNINFO:
				return trWorkTestInfoService.findByTestinfoNo(transNo);
			case OTHSET:
				return trOsTransService.findByOsNo(transNo);
			case WORKONPG:
				return trWorkOnpgService.findByOnpgNo(transNo);
			default:
				break;
			}
		}
		return null;
	}

	/**
	 * 確認需求製作進度
	 *
	 * @param sub
	 * @param reqTypes
	 * @param requireStatus
	 * @param transNo
	 */
	private Require getRequire(Object sub, Map<String, Require> requireMap, String transNo) {
		String reqNo = "";
		if (sub instanceof WorkPtCheck) {
			reqNo = ((WorkPtCheck) sub).getSourceNo();
		} else if (sub instanceof TrWorkTestInfo) {
			reqNo = ((TrWorkTestInfo) sub).getSourceNo();
		} else if (sub instanceof TrOs) {
			reqNo = ((TrOs) sub).getRequire_no();
		} else if (sub instanceof TrWorkOnpg) {
			reqNo = ((TrWorkOnpg) sub).getSourceNo();
		}
		Preconditions.checkState(!Strings.isNullOrEmpty(reqNo), "子程序單號：" + transNo + "，無法取得需求單號！！");
		Require require = null;
		if (requireMap.containsKey(reqNo)) {
			require = requireMap.get(reqNo);
		} else {
			require = requireService.findByReqNo(reqNo);
			requireMap.put(reqNo, require);
		}
		Preconditions.checkNotNull(require, "子程序單號：" + transNo + "，查無需求單！！");
		return require;
	}

	/**
	 * 確認需求製作進度
	 *
	 * @param reqTypes
	 * @param requireStatus
	 * @param transNo
	 */
	private void checkRequireStatus(List<String> reqTypes, RequireStatusType requireStatus, String transNo) {
		Preconditions.checkNotNull(requireStatus, "子程序單號：" + transNo + "，無需求製作進度！！");
		Preconditions.checkState(reqTypes.contains(requireStatus.name()), "子程序單號：" + transNo + "，需求製作進度不符合！！");
	}

	/**
	 * 確認子程序狀態
	 *
	 * @param sub
	 * @param subProgramTypes
	 * @param transNo
	 */
	private void checkSsubProgramTypes(Object sub, List<String> subProgramTypes, String transNo) {
		String type = "";
		if (sub instanceof WorkPtCheck) {
			type = ((WorkPtCheck) sub).getPtStatus().name();
		} else if (sub instanceof TrWorkTestInfo) {
			type = ((TrWorkTestInfo) sub).getTestinfoStatus().name();
		} else if (sub instanceof TrOs) {
			type = ((TrOs) sub).getOsStatus().name();
		} else if (sub instanceof TrWorkOnpg) {
			type = ((TrWorkOnpg) sub).getOnpgStatus().name();
		}
		Preconditions.checkState(!Strings.isNullOrEmpty(type),
		        "子程序單號：" + transNo + "，查無子程序狀態！！");
		Preconditions.checkState(subProgramTypes.contains(type),
		        "子程序單號：" + transNo + "，子程序狀態不符合！！");
	}

	/**
	 * 轉檔
	 *
	 * @param to
	 * @param sub
	 */
	private void checkDep(Object sub, TransNotifyTo to, String transNo) {
		JsonStringListTo noticeDeps = new JsonStringListTo();
		if (sub instanceof WorkPtCheck) {
			noticeDeps = ((WorkPtCheck) sub).getNoticeDeps();
		} else if (sub instanceof TrWorkTestInfo) {
			noticeDeps = ((TrWorkTestInfo) sub).getSendTestDep();
		} else if (sub instanceof TrOs) {
			noticeDeps = ((TrOs) sub).getNoticeDeps();
		} else if (sub instanceof TrWorkOnpg) {
			noticeDeps = ((TrWorkOnpg) sub).getNoticeDeps();
		}
		Preconditions.checkState(noticeDeps != null
		        && noticeDeps.getValue().contains(String.valueOf(to.getSourceMappOrg().getSid())),
		        "子程序單號：" + transNo + "，無" + WkOrgUtils.getOrgName(to.getSourceMappOrg()) + "單位！！");
		Preconditions.checkState(noticeDeps != null
		        && !noticeDeps.getValue().contains(String.valueOf(to.getTargetMappOrg().getSid())),
		        "子程序單號：" + transNo + "，已有" + WkOrgUtils.getOrgName(to.getTargetMappOrg()) + "單位！！");
	}

	/**
	 * 轉檔
	 *
	 * @param to
	 * @param sub
	 */
	private void checkRecoveryDep(Object sub, TransNotifyTo to, String transNo) {
		JsonStringListTo noticeDeps = new JsonStringListTo();
		if (sub instanceof WorkPtCheck) {
			noticeDeps = ((WorkPtCheck) sub).getNoticeDeps();
		} else if (sub instanceof TrWorkTestInfo) {
			noticeDeps = ((TrWorkTestInfo) sub).getSendTestDep();
		} else if (sub instanceof TrOs) {
			noticeDeps = ((TrOs) sub).getNoticeDeps();
		} else if (sub instanceof TrWorkOnpg) {
			noticeDeps = ((TrWorkOnpg) sub).getNoticeDeps();
		}
		Preconditions.checkState(noticeDeps != null
		        && noticeDeps.getValue().contains(String.valueOf(to.getTargetMappOrg().getSid())),
		        "子程序單號：" + transNo + "，無" + WkOrgUtils.getOrgName(to.getTargetMappOrg()) + "單位！！");
	}

	/**
	 * 轉檔
	 *
	 * @param isDoTrans
	 * @param to
	 * @param sub
	 * @param transNo
	 * @param require
	 */
	private void transDep(Boolean isDoTrans, TransNotifyTo to, Object sub, Require require) {
		String result = "";
		if (isDoTrans) {
			result = this.transDep(sub, to.getTargetMappOrg().getSid());
		}
		this.addTransLog(to, require, sub, result);
	}

	/**
	 * 轉檔
	 *
	 * @param isDoTrans
	 * @param to
	 * @param sub
	 * @param transNo
	 * @param require
	 */
	private void transRecoveryDep(Boolean isDoTrans, TransNotifyTo to, Object sub, Require require) {
		String result = "";
		if (isDoTrans) {
			result = this.transRecoveryDep(sub, to.getTargetMappOrg().getSid());
		}
		this.addTransLog(to, require, sub, result);
	}

	/**
	 * 轉檔
	 *
	 * @param sub
	 * @param to
	 */
	private String transRecoveryDep(Object sub, Integer targetOrgSid) {
		try {
			if (sub instanceof WorkPtCheck) {
				WorkPtCheck obj = (WorkPtCheck) sub;
				obj.getNoticeDeps().getValue().remove(String.valueOf(targetOrgSid));
				int count = ptCheckService.updateNoticeDeps(obj.getSid(), obj.getNoticeDeps());
				if (count == 1) {
					return "true";
				}
			} else if (sub instanceof TrWorkTestInfo) {
				TrWorkTestInfo obj = (TrWorkTestInfo) sub;
				obj.getSendTestDep().getValue().remove(String.valueOf(targetOrgSid));
				int count = trWorkTestInfoService.updateSendTestDep(obj.getSid(), obj.getSendTestDep());
				if (count == 1) {
					return "true";
				}
			} else if (sub instanceof TrOs) {
				TrOs obj = (TrOs) sub;
				obj.getNoticeDeps().getValue().remove(String.valueOf(targetOrgSid));
				int count = trOsTransService.updateNoticeDeps(obj.getSid(), obj.getNoticeDeps());
				if (count == 1) {
					return "true";
				}
			} else if (sub instanceof TrWorkOnpg) {
				TrWorkOnpg obj = (TrWorkOnpg) sub;
				obj.getNoticeDeps().getValue().remove(String.valueOf(targetOrgSid));
				int count = trWorkOnpgService.updateNoticeDeps(obj.getSid(), obj.getNoticeDeps());
				if (count == 1) {
					return "true";
				}
			}
		} catch (Exception e) {
			log.error("trans ERROR", e);
			return "false";
		}
		return "false";
	}

	/**
	 * 轉檔
	 *
	 * @param sub
	 * @param to
	 */
	private String transDep(Object sub, Integer targetOrgSid) {
		try {
			if (sub instanceof WorkPtCheck) {
				WorkPtCheck obj = (WorkPtCheck) sub;
				if (!obj.getNoticeDeps().getValue().contains(String.valueOf(targetOrgSid))) {
					obj.getNoticeDeps().getValue().add(String.valueOf(targetOrgSid));
					int count = ptCheckService.updateNoticeDeps(obj.getSid(), obj.getNoticeDeps());
					if (count == 1) {
						return "true";
					}
				}
			} else if (sub instanceof TrWorkTestInfo) {
				TrWorkTestInfo obj = (TrWorkTestInfo) sub;
				if (!obj.getSendTestDep().getValue().contains(String.valueOf(targetOrgSid))) {
					obj.getSendTestDep().getValue().add(String.valueOf(targetOrgSid));
					int count = trWorkTestInfoService.updateSendTestDep(obj.getSid(), obj.getSendTestDep());
					if (count == 1) {
						return "true";
					}
				}
			} else if (sub instanceof TrOs) {
				TrOs obj = (TrOs) sub;
				if (!obj.getNoticeDeps().getValue().contains(String.valueOf(targetOrgSid))) {
					obj.getNoticeDeps().getValue().add(String.valueOf(targetOrgSid));
					int count = trOsTransService.updateNoticeDeps(obj.getSid(), obj.getNoticeDeps());
					if (count == 1) {
						return "true";
					}
				}
			} else if (sub instanceof TrWorkOnpg) {
				TrWorkOnpg obj = (TrWorkOnpg) sub;
				if (!obj.getNoticeDeps().getValue().contains(String.valueOf(targetOrgSid))) {
					obj.getNoticeDeps().getValue().add(String.valueOf(targetOrgSid));
					int count = trWorkOnpgService.updateNoticeDeps(obj.getSid(), obj.getNoticeDeps());
					if (count == 1) {
						return "true";
					}
				}
			}
		} catch (Exception e) {
			log.error("trans ERROR", e);
			return "false";
		}
		return "false";
	}

	/**
	 * 增加log
	 *
	 * @param to
	 * @param require
	 * @param sub
	 * @param isTrans
	 */
	private void addTransLog(TransNotifyTo to, Require require, Object sub, String isTrans) {
		to.addLog("需求單ID：" + require.getSid());
		to.addLog("需求單號：" + require.getRequireNo());
		to.addLog("製作進度：" + commonService.get(require.getRequireStatus()));
		if (sub instanceof WorkPtCheck) {
			to.addLog("子程序：原型確認");
			to.addLog("子程序單號：" + ((WorkPtCheck) sub).getPtNo());
			to.addLog("子程序狀態：" + commonService.get(((WorkPtCheck) sub).getPtStatus()));
			to.addLog("子程序建單日期：" + ToolsDate.transDateToString(
			        SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), ((WorkPtCheck) sub).getCreate_dt()));
		} else if (sub instanceof TrWorkTestInfo) {
			to.addLog("子程序：送測");
			to.addLog("子程序單號：" + ((TrWorkTestInfo) sub).getTestinfoNo());
			to.addLog("子程序狀態：" + commonService.get(((TrWorkTestInfo) sub).getTestinfoStatus()));
			to.addLog("子程序建單日期：" + ToolsDate.transDateToString(
			        SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), ((TrWorkTestInfo) sub).getCreate_dt()));
		} else if (sub instanceof TrOs) {
			to.addLog("子程序：其他資料設定");
			to.addLog("子程序單號：" + ((TrOs) sub).getOs_no());
			to.addLog("子程序狀態：" + commonService.get(((TrOs) sub).getOsStatus()));
			to.addLog("子程序建單日期：" + ToolsDate.transDateToString(
			        SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), ((TrOs) sub).getCreate_dt()));
		} else if (sub instanceof TrWorkOnpg) {
			to.addLog("子程序：ON程式");
			to.addLog("子程序單號：" + ((TrWorkOnpg) sub).getOnpgNo());
			to.addLog("子程序狀態：" + commonService.get(((TrWorkOnpg) sub).getOnpgStatus()));
			to.addLog("子程序建單日期：" + ToolsDate.transDateToString(
			        SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), ((TrWorkOnpg) sub).getCreate_dt()));
		}
		if (!Strings.isNullOrEmpty(isTrans)) {
			to.addLog("異動成功：" + isTrans);
		}
	}

	/**
	 * 通知單位轉發-增加動作
	 * 
	 * @param orderNoList
	 * @param type
	 * @param targetOrgSid
	 * @return
	 */
	public List<String> updateBatchTransDep(List<String> orderNoList, Integer targetOrgSid) {
		List<String> updatedList = Lists.newArrayList();
		for (String orderNo : orderNoList) {
			Object obj = getSubProgram(findTypeByNo(orderNo), orderNo);
			this.transDep(obj, targetOrgSid);
			updatedList.add(orderNo);
		}
		return updatedList;
	}

	/**
	 * 通知單位轉發-還原動作
	 * 
	 * @param orderNoList
	 * @param type
	 * @param targetOrgSid
	 * @return
	 */
	public List<String> updateBatchTransRecoveryDep(List<String> orderNoList, List<Integer> targetOrgSids) {
		List<String> updatedList = Lists.newArrayList();
		for (String orderNo : orderNoList) {
			Object obj = getSubProgram(findTypeByNo(orderNo), orderNo);
			this.transRecoveryDepts(obj, targetOrgSids);
			updatedList.add(orderNo);
		}
		return updatedList;
	}

	private RequireTransProgramType findTypeByNo(String orderNo) {
		RequireTransProgramType type = null;
		String prefix = orderNo.substring(0, 2);
		switch (prefix) {
		case "PT":
			type = RequireTransProgramType.PTCHECK;
			break;
		case "WT":
			type = RequireTransProgramType.WORKTESTSIGNINFO;
			break;
		case "OS":
			type = RequireTransProgramType.OTHSET;
			break;
		case "OP":
			type = RequireTransProgramType.WORKONPG;
			break;
		default:
			break;
		}
		return type;
	}

	/**
	 * 通知單位批次還原
	 * 
	 * @param sub
	 * @param targetOrgSid
	 * @return
	 */
	private String transRecoveryDepts(Object sub, List<Integer> targetOrgSids) {
		try {
			if (sub instanceof WorkPtCheck) {
				WorkPtCheck obj = (WorkPtCheck) sub;
				for (Integer targetOrgSid : targetOrgSids) {
					obj.getNoticeDeps().getValue().remove(String.valueOf(targetOrgSid));
				}
				int count = ptCheckService.updateNoticeDeps(obj.getSid(), obj.getNoticeDeps());
				if (count == 1) {
					return "true";
				}
			} else if (sub instanceof TrWorkTestInfo) {
				TrWorkTestInfo obj = (TrWorkTestInfo) sub;
				for (Integer targetOrgSid : targetOrgSids) {
					obj.getSendTestDep().getValue().remove(String.valueOf(targetOrgSid));
				}
				int count = trWorkTestInfoService.updateSendTestDep(obj.getSid(), obj.getSendTestDep());
				if (count == 1) {
					return "true";
				}
			} else if (sub instanceof TrOs) {
				TrOs obj = (TrOs) sub;
				for (Integer targetOrgSid : targetOrgSids) {
					obj.getNoticeDeps().getValue().remove(String.valueOf(targetOrgSid));
				}
				int count = trOsTransService.updateNoticeDeps(obj.getSid(), obj.getNoticeDeps());
				if (count == 1) {
					return "true";
				}
			} else if (sub instanceof TrWorkOnpg) {
				TrWorkOnpg obj = (TrWorkOnpg) sub;
				for (Integer targetOrgSid : targetOrgSids) {
					obj.getNoticeDeps().getValue().remove(String.valueOf(targetOrgSid));
				}
				int count = trWorkOnpgService.updateNoticeDeps(obj.getSid(), obj.getNoticeDeps());
				if (count == 1) {
					return "true";
				}
			}
		} catch (Exception e) {
			log.error("trans ERROR", e);
			return "false";
		}
		return "false";
	}
}
