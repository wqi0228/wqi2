/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search34QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.value.to.WorkTestQaAliasVO;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.logic.search.view.Search34View;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.send.test.QaAliasService;
import com.cy.tech.request.logic.service.send.test.SendTestService;
import com.cy.tech.request.logic.utils.DateUtils;
import com.cy.tech.request.web.common.PermissionHelper;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.logic.component.UsersMultiSelectComponent;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.require.RequireStep;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.view.vo.Search34QueryVO;
import com.cy.tech.request.web.controller.view.vo.Search34QueryVO.Search34Column;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * QA待審核單據查詢
 *
 * @author marlow_chen
 */
@Controller
@Scope("view")
@Slf4j
public class Search34MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8317342164396707583L;
    @Autowired
    transient private WorkTestUpDownBean workTestUpDownBean;
    @Autowired
    transient private Search34QueryService search34QueryService;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private SendTestService sendTestService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private Require01MBean r01MBean;
    @Autowired
    private QaAliasService qaAliasService;
    @Autowired
    private transient WorkBackendParamHelper workBackendParamHelper;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;

    /** 所有的送測單 */
    @Getter
    @Setter
    private List<Search34View> queryItems;

    /** 選擇的送測單 */
    @Getter
    @Setter
    private Search34View querySelection;

    /** 上下筆移動keeper */
    @Getter
    private Search34View queryKeeper;

    /** 在匯出的時候，某些內容需要隱藏 */
    @Getter
    private boolean hasDisplay = true;
    @Getter
    private final String dataTableId = "dtRequire";

    /** 所有填單單位(Filter下拉選單) */
    @Getter
    private List<String> createDeps;

    /** 送測狀態(Filter下拉選單) */
    @Getter
    private List<String> testinfoStatus;

    /** 文件補齊(Filter下拉選單) */
    @Getter
    private List<String> commitStatus;

    private boolean isRequire = false;
    @Getter
    private UsersMultiSelectComponent<WorkTestQaAliasVO> usersMultiSelectCompt = new UsersMultiSelectComponent<WorkTestQaAliasVO>();
    @Getter
    @Setter
    private List<SelectItem> dateTypeItem;
    @Getter
    @Setter
    private Search34QueryVO queryVO = new Search34QueryVO();
    /** 是否修改人員為主測 */
    private boolean isMaster;

    @PostConstruct
    public void init() {
        dateTypeItem = Lists.newArrayList();
        for (Search34Column column : Search34QueryVO.Search34Column.values()) {
            SelectItem selectItem = new SelectItem();
            selectItem.setLabel(column.getName());
            selectItem.setValue(column.getValue());
            dateTypeItem.add(selectItem);
        }
        queryVO.init();
        display.execute("doSearchData();");
    }

    public void search() {
        queryItems = this.findWithQuery();
        convert(queryItems);

        display.resetDataTable(dataTableId);
    }

    private void convert(List<Search34View> resultList) {
        Set<String> allCreateDeps = new HashSet<String>();
        Set<String> allTestinfoStatus = new HashSet<String>();
        Set<String> allCommitStatus = new HashSet<String>();
        Date compareDate = null;
        int xor = 0;
        
        for (Search34View view : resultList) {
            // 依送測日不同, 於頁面顯示不同row color
            if (!view.getTestDate().equals(compareDate)) {
                compareDate = view.getTestDate();
                xor ^= 1;
            }
            if (xor == 0) {
                view.setHasColor(true);
            }
            if (view.getMasterTester() != null && view.getMasterTester().length != 0) {
                List<User> masterTesters = WkUserCache.getInstance().findBySidStrs(Lists.newArrayList(view.getMasterTester()));
                view.setMasterTesterNames(qaAliasService.convertToQaAliasNames(masterTesters));
            }
            if (view.getSlaveTester() != null && view.getSlaveTester().length != 0) {
                List<User> slaveTesters = WkUserCache.getInstance().findBySidStrs(Lists.newArrayList(view.getSlaveTester()));
                view.setSlaveTesterNames(qaAliasService.convertToQaAliasNames(slaveTesters));
            }
            view.setTestDateStr(this.formatTestDateStr(view.getTestDate()));

            // 組單位名稱,至少到處級
            view.setCreateDept(WkOrgUtils.prepareBreadcrumbsByDepName(view.getCreateDeptSid(), OrgLevel.DIVISION_LEVEL, false, "-"));
            // 填單單位 Filter values
            allCreateDeps.add(view.getCreateDept());
            // 送測狀態 Filter values
            allTestinfoStatus.add(view.getTestinfoStatus());
            // 文件補齊 Filter values
            allCommitStatus.add(view.getCommitStatus());
        }
        this.createDeps = Lists.newArrayList(allCreateDeps);
        this.testinfoStatus = Lists.newArrayList(allTestinfoStatus);
        this.commitStatus = Lists.newArrayList(allCommitStatus);
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        queryVO.init();
        this.search();
    }

    private List<Search34View> findWithQuery() {
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT "
                + "wti.testinfo_sid,"
                + "wti.test_date,"
                + "wti.qa_search_link,"
                + "wti.dep_sid,"
                + "wti.create_usr,"
                + "wti.testinfo_theme,"
                + "wti.master_testers,"
                + "wti.testinfo_estimate_dt,"
                + "wti.testinfo_status,"
                + "wti.commit_status,"
                + "wti.testinfo_finish_dt,"
                + "wti.expect_online_date,"
                + "wti.testinfo_no,"
                + "wti.create_dt,"
                + "tr.dep_sid as reqSid,"
                + "tr.create_usr as reqUser,"
                + "ckm.big_category_name,"
                + "ckm.middle_category_name,"
                + "ckm.small_category_name,"
                + "tid.field_content,"
                + "tr.require_status,"
                + "tr.update_dt,"
                + "wti.testinfo_source_no,"
                + "wti.schedule_finish_date,"
                + "wti.qa_link_name,"
                + "wti.slave_testers,"
                + "wti.qa_audit_status, "
                + "wti.qa_schedule_status, "
                + this.searchConditionSqlHelper.prepareReadRecordSelectColumn()
                + " FROM ");
        buildWorkTestInfoCondition(builder, parameters);
        buildRequireCondition(builder, parameters);
        buildRequireIndexDictionaryCondition(builder, parameters);
        buildCategoryKeyMappingCondition(builder, parameters);

        builder.append(this.searchConditionSqlHelper.prepareSubFormReadRecordJoin(
                SecurityFacade.getUserSid(),
                FormType.WORKTESTSIGNINFO,
                "wti"));

        builder.append(" ORDER BY wti.test_date ASC, wti.create_dt ASC");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.WORK_SCHEDULE, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.WORK_SCHEDULE, SecurityFacade.getUserSid());

        // 查詢
        List<Search34View> resultList = search34QueryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);
        
        //儲存使用記錄
        usageRecord.saveUsageRecord();
        
        return resultList;
    }

    private void buildWorkTestInfoCondition(StringBuilder builder, Map<String, Object> parameters) {
        builder.append("(SELECT * FROM work_test_info WHERE testinfo_source_type='TECH_REQUEST' and testinfo_status != 'SIGN_PROCESS'");
        builder.append(" AND qa_audit_status='APPROVED'"); // 已審核
        builder.append(" AND qa_schedule_status='JOIN_SCHEDULE'"); // 納入排程

        // 送測區間或預計完成日
        if (queryVO.getWeekIntervalVO().getStartDate() != null && queryVO.getWeekIntervalVO().getEndDate() != null) {
            builder.append(" AND " + queryVO.getDateType() + " BETWEEN :startDate AND :endDate");
            parameters.put("startDate", helper.transStartDate(queryVO.getWeekIntervalVO().getStartDate()));
            parameters.put("endDate", helper.transEndDate(queryVO.getWeekIntervalVO().getEndDate()));
        }
        builder.append(") AS wti ");
    }

    private void buildRequireCondition(StringBuilder builder, Map<String, Object> parameters) {
        builder.append(" INNER JOIN tr_require tr ON tr.require_no=wti.testinfo_source_no");
    }

    private void buildRequireIndexDictionaryCondition(StringBuilder builder, Map<String, Object> parameters) {
        builder.append(" INNER JOIN (SELECT require_sid,field_content FROM tr_index_dictionary WHERE field_name='主題') AS tid ");
        builder.append("ON tr.require_sid=tid.require_sid");
    }

    private void buildCategoryKeyMappingCondition(StringBuilder builder, Map<String, Object> parameters) {
        builder.append(" INNER JOIN tr_category_key_mapping ckm ON tr.mapping_sid=ckm.key_sid");
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (Search34View) event.getObject();
        this.changeRequireContent(this.queryKeeper);
        this.moveScreenTab();
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Search34View view) {
        view.setReadRecordType(ReadRecordType.HAS_READ);
        Require require = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(require, loginBean.getUser(), RequireBottomTabType.SEND_TEST_INFO);
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
        this.moveScreenTab();
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        helper.exportExcel(document, queryVO.getWeekIntervalVO().getStartDate(), queryVO.getWeekIntervalVO().getEndDate(), ReportType.WORK_SCHEDULE);
        hasDisplay = true;
    }

    /**
     * 隱藏部分column裡的內容
     */
    public void hideColumnContent() {
        hasDisplay = false;
    }

    public void moveScreenTab() {
        this.r01MBean.getBottomTabMBean().resetTabIdx(this.r01MBean);
        this.r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.SEND_TEST_INFO);
        if (isRequire) {
            this.r01MBean.setSetp(RequireStep.LOAD_IN_PAGE_EDIT);
        }
        List<String> sids = sendTestService.findSidsBySourceSid(this.r01MBean.getRequire().getSid());
        String indicateSid = queryKeeper.getSid();
        if (sids.indexOf(indicateSid) != -1) {
            display.execute("PF('st_acc_panel_layer_zero').select(" + sids.indexOf(indicateSid) + ");");
            for (int i = 0; i < sids.size(); i++) {
                if (i != sids.indexOf(indicateSid)) {
                    display.execute("PF('st_acc_panel_layer_zero').unselect(" + i + ");");
                }
            }
        }
    }

    /**
     * 送測主題開啟分頁
     * 
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void openUrlByLink(String widgetVar, String pageCount, Search34View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.reloadWorkTestUpDownBean();
    }

    /**
     * 重新載入送測單上下筆資訊
     */
    public void reloadWorkTestUpDownBean() {
        workTestUpDownBean.setCurrRow(queryKeeper.getTestinfoNo());
        workTestUpDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
        workTestUpDownBean.resetTabInfo(RequireBottomTabType.SEND_TEST_INFO, queryKeeper.getSid());
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Search34View to) {
        querySelection = to;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 打開主測協測按鈕
     */
    public void prepareUpdateTester(Search34View view, boolean isMaster) {
        this.querySelection = view;
        this.isMaster = isMaster;
        this.usersMultiSelectCompt = new UsersMultiSelectComponent<WorkTestQaAliasVO>();
        display.resetDataTable("testerSource");

        // 初始 未選取清單
        usersMultiSelectCompt.setSourceUsers(
                qaAliasService.convertToQaAliasList(
                        this.workBackendParamHelper.getQAUsers()));

        List<WorkTestQaAliasVO> selected = null;
        String[] selectedTarget = null;
        if (isMaster) {
            // 已選取的協測使用者
            selected = qaAliasService.convertToQaAliasList(view.getSlaveTester());
            selectedTarget = view.getMasterTester();
        } else {
            // 已選取的主測使用者
            selected = qaAliasService.convertToQaAliasList(view.getMasterTester());
            selectedTarget = view.getSlaveTester();
        }

        if (WkStringUtils.notEmpty(selected)) {
            usersMultiSelectCompt.getSourceUsers().removeAll(selected);
        }

        if (WkStringUtils.notEmpty(selectedTarget)) {
            List<WorkTestQaAliasVO> qaAliasList = qaAliasService.convertToQaAliasList(selectedTarget);
            usersMultiSelectCompt.setTargetUsers(qaAliasList);
            usersMultiSelectCompt.getSourceUsers().removeAll(qaAliasList);
        }

        display.update("search34TestersDlg");
    }

    /**
     * 新增/修改測試人員
     * 
     * @param type
     */
    public void updateTesters() {
        WorkTestInfo testInfo = new WorkTestInfo();
        testInfo.setTestinfoNo(querySelection.getTestinfoNo());
        try {
            String userSids = usersMultiSelectCompt.getTargetUsers().stream().map(user -> user.getUserSid().toString()).collect(Collectors.joining(","));

            String column = null;
            if (isMaster) {
                testInfo.setMasterTesters(StringUtils.isBlank(userSids) ? null : userSids);
                column = "masterTesters";
            } else {
                testInfo.setSlaveTesters(StringUtils.isBlank(userSids) ? null : userSids);
                column = "slaveTesters";
            }

            sendTestService.saveOrUpdate(
                    testInfo,
                    loginBean.getUser(),
                    column,
                    PermissionHelper.getInstance().isQAManager()
                    );

            display.hidePfWidgetVar("testersButtonDlg_wv");
            search();
        } catch (IllegalAccessException e) {
            MessagesUtils.showError(e.getMessage());

        } catch (IllegalArgumentException e) {
            MessagesUtils.showError(e.getMessage());

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    // 自訂datatalbe filter logic
    public boolean filterByTestDate(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim();
        if (filterText == null || filterText.equals("")) {
            return true;
        }

        if (value == null) {
            return false;
        }
        String testDate = (String) value;
        return testDate.contains(filterText);
    }

    private String formatTestDateStr(Date testDate) {
        DateTime dateTime = new DateTime(testDate);
        String week = "";
        switch (dateTime.getDayOfWeek()) {
        case DateTimeConstants.SUNDAY:
            week = "日";
            break;
        case DateTimeConstants.MONDAY:
            week = "一";
            break;
        case DateTimeConstants.TUESDAY:
            week = "二";
            break;
        case DateTimeConstants.WEDNESDAY:
            week = "三";
            break;
        case DateTimeConstants.THURSDAY:
            week = "四";
            break;
        case DateTimeConstants.FRIDAY:
            week = "五";
            break;
        case DateTimeConstants.SATURDAY:
            week = "六";
            break;
        }
        return String.format("%s(%s)", DateUtils.YYYY_MM_DD.print(testDate.getTime()), week);
    }

    public void workTestInfoRecordDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.highlightReportTo(widgetVar, pageCount, querySelection);
        this.reloadWorkTestUpDownBean();
    }

    public void workTestInfoRecordUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.highlightReportTo(widgetVar, pageCount, querySelection);
        this.reloadWorkTestUpDownBean();
    }

}