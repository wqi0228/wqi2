package com.cy.tech.request.web.controller.home;

import com.cy.tech.request.logic.service.URLService;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import lombok.Getter;
import org.omnifaces.util.Faces;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 讀取需求單連結失敗 - 訊息控制<BR/>
 *
 * /error/illegal_read.xhtml
 *
 * @author shaun
 */
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class IllegalReadMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8012389033958935458L;
    @Getter
    private String errorMsg;
    @Getter
    private String errorCode;

    @PostConstruct
    void init() {
        if (Faces.getRequestParameterMap().containsKey(URLService.ERROR_CODE_ATTR)) {
            errorCode = Faces.getRequestParameterMap().get(URLService.ERROR_CODE_ATTR);
            if (errorCode.equals(URLService.ERROR_CODE_NO_PERMISSION)) {
                errorMsg = "您無權限讀取此張需求單！！";
            }
            if (errorCode.equals(URLService.ERROR_CODE_URL_LINK_EXPIRED)) {
                errorMsg = "需求單連結過期 ！！";
            }
            if (errorCode.equals(URLService.ERROR_CODE_URL_ATTR_FAILD)) {
                errorMsg = "無效的連結網址 ！！";
            }
            return;
        }
        errorMsg = "需求單連結錯誤！！";
    }

}
