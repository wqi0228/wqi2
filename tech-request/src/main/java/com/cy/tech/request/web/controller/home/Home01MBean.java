/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.home;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.Home01QueryService;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.Home01View;
import com.cy.tech.request.logic.service.FavoriteService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.vo.require.Favorite;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.search.TableUpDownBean;
import com.cy.tech.request.web.controller.searchheader.CommonHeaderMBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 收藏夾 報表
 *
 * @author shaun
 */
@Controller
@Scope("view")
@Slf4j
public class Home01MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3622507843074854025L;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private FavoriteService favoritesService;
    @Autowired
    transient private SearchService searchService;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private DisplayController display;

    @Autowired
    transient private Home01QueryService home01QueryService;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchResultHelper searchResultHelper;
    @Autowired
    transient private CommonHeaderMBean commonHeaderMBean;

    @Getter
    @Setter
    private List<Home01View> queryItems;
    @Getter
    @Setter
    private Home01View querySelection;
    /** 上下筆移動keeper */
    @Getter
    private Home01View queryKeeper;

    @Getter
    @Setter
    private String searchText;
    @Getter
    @Setter
    private Date start;
    @Getter
    @Setter
    private Date end;
    @Getter
    private Boolean showCancelFavoriteBtn;

    /** 切換模式 */
    @Getter
    private SwitchType switchType = SwitchType.CONTENT;

    /** 切換模式 - 全畫面狀態 */
    @Getter
    private SwitchType switchFullType = SwitchType.DETAIL;

    @Getter
    private final String dataTableId = "dtRequire";

    @PostConstruct
    public void init() {
        this.initVar();
        this.search();
    }

    private void initVar() {
        querySelection = null;
        showCancelFavoriteBtn = Boolean.FALSE;
        searchText = "";
        start = null;
        end = null;
        this.commonHeaderMBean.clear();
    }

    /**
     * 搜尋
     */
    public void search() {
        queryItems = this.findWithQuery();
    }

    /**
     * 清除
     */
    public void clear() {
        this.init();
    }

    private List<Home01View> findWithQuery() {
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT "
                + "fav.favorite_sid,"
                + "tr.require_no,"
                + "tid.field_content,"
                + "fav.create_dt, "
                // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
                + this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire()

                + " FROM ");
        buildFavoriteCondition(builder, parameters);
        buildRequireCondition(builder, parameters);
        buildRequireIndexDictionaryCondition(builder, parameters);
        // 檢查項目 (系統別)
        // 後方需對 主單 sid 做 group by
        builder.append(this.searchConditionSqlHelper.prepareCommonJoin(SecurityFacade.getUserSid()));

        builder.append("WHERE fav.require_sid IS NOT NULL ");
        builder.append(" GROUP BY tr.require_sid ");
        builder.append("  ORDER BY fav.create_dt ASC");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.FAVORITES, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.FAVORITES, SecurityFacade.getUserSid());

        // 查詢
        List<Home01View> resultList = home01QueryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // ====================================
        // 後續處理
        // ====================================
        // 後續處理-開始
        usageRecord.afterProcessStart();
        // 查詢條件過濾：系統別
        resultList = resultList.stream()
                .filter(each -> this.searchResultHelper.filterCheckItems(
                        each.getCheckItemTypes(),
                        commonHeaderMBean.getCheckItemTypes(),
                        false,
                        null))
                .collect(Collectors.toList());
        // 後續處理-結束
        usageRecord.afterProcessEnd();

        // ====================================
        // 記錄報表使用記錄
        // ====================================
        usageRecord.saveUsageRecord();

        return resultList;
    }

    private void buildFavoriteCondition(StringBuilder builder, Map<String, Object> parameters) {
        builder.append("( SELECT * FROM tr_favorite fav  WHERE fav.create_usr = :createUser ");
        parameters.put("createUser", loginBean.getUser().getSid());
        if (this.start != null || this.end != null) {
            if (this.start == null) {
                builder.append("    AND fav.create_dt <= :end ");
            } else {
                builder.append("    AND fav.create_dt BETWEEN :start AND :end ");
                parameters.put("start", searchService.transStartDate(start));
            }
            if (this.end == null) {
                end = searchService.getLastMonthDay(Calendar.getInstance());
            }
            parameters.put("end", searchService.transEndDate(end));
        }
        builder.append("    AND fav.status  = 0 ");
        builder.append(") AS fav ");
    }

    private void buildRequireCondition(StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_require tr) AS tr ON tr.require_sid=fav.require_sid ");
    }

    private void buildRequireIndexDictionaryCondition(StringBuilder builder, Map<String, Object> parameters) {
        // 主題 | 內容 | 模糊搜尋
        builder.append("INNER JOIN (SELECT tid.require_sid,tid.field_content FROM tr_index_dictionary tid WHERE 1=1");
        // only 主題
        if (!Strings.isNullOrEmpty(searchText)) {
            String themeText = "%" + reqularUtils.replaceIllegalSqlLikeStr(searchText) + "%";
            builder.append(" AND (tid.field_name = '主題' AND tid.field_content LIKE :themeText)");
            parameters.put("themeText", themeText);
        }
        builder.append(" AND tid.field_name='主題') AS tid ON tr.require_sid=tid.require_sid ");
    }

    /**
     * 加入追蹤
     */
    public void addTrace() {
        MessagesUtils.showError("施工中...");
    }

    /**
     * 取消收藏
     *
     * @param view
     */
    public void cancelFavorite(Home01View view) {
        try {
            if (view == null) {
                MessagesUtils.showWarn("請選擇任一筆收藏資訊...");
                return;
            }
            Favorite favorite = favoritesService.findBySid(view.getSid());
            Optional<Favorite> of = Optional.fromNullable(favorite);
            if (favorite.getRequire() != null) {
                favoritesService.toggle(favorite.getRequire(), loginBean.getUser(), of);
                querySelection = null;
                queryItems.remove(view);
                this.selectRow();
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            MessagesUtils.showError("取消收藏失敗。");
        }
    }

    public void selectRow() {
        showCancelFavoriteBtn = querySelection != null;
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 變更需求單內容
     *
     * @param require
     */
    private void changeRequireContent(Home01View view) {
        Require r = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(r, loginBean.getUser());
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Home01View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(querySelection);
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Home01View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            display.update("headerTitle");
            display.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Home01View to) {
        querySelection = to;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }
}
