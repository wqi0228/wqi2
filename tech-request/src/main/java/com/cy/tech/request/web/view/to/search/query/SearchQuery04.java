/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.view.to.search.query;

import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.vo.query.search.SearchQuery;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.LocalDate;

/**
 * Search04MBean 查詢欄位
 *
 * @author kasim
 */
public class SearchQuery04 extends SearchQuery implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6907712135858349670L;
    @Getter
    @Setter
    /** 選擇的大類 */
    private String selectBigCategorySid;
    @Getter
    @Setter
    /** 類別組合(大類 sid) */
    private List<String> bigDataCateSids = Lists.newArrayList();
    @Getter
    @Setter
    /** 類別組合(中類 sid) */
    private List<String> middleDataCateSids = Lists.newArrayList();
    @Getter
    @Setter
    /** 類別組合(小類 sid) */
    private List<String> smallDataCateSids = Lists.newArrayList();
    @Getter
    @Setter
    /** 需求單位 */
    private List<String> requireDepts = Lists.newArrayList();
    @Getter
    @Setter
    /** 退件原因 */
    private String rejectReason;
    @Getter
    @Setter
    /** 退件人員 */
    private String rejectUserName;
    @Getter
    @Setter
    /** 需求人員 */
    private String trCreatedUserName;
    @Getter
    @Setter
    /** 模糊搜尋 */
    private String fuzzyText;
    @Getter
    @Setter
    /** 時間切換的index */
    private Integer dateTypeIndex;
    @Getter
    @Setter
    /** 立單區間(起) */
    private Date startDate;
    @Getter
    @Setter
    /** 立單區間(訖) */
    private Date endDate;
    @Getter
    @Setter
    /** 異動區間(起) */
    private Date startUpdatedDate;
    @Getter
    @Setter
    /** 異動區間(訖) */
    private Date endUpdatedDate;
    @Getter
    @Setter
    /** 退件區間(起) */
    private Date startRejectedDate;
    @Getter
    @Setter
    /** 退件區間(訖) */
    private Date endRejectedDate;
    @Getter
    @Setter
    /** 緊急度 */
    private List<String> urgencyList;
    @Getter
    @Setter
    /** 需求單號 */
    private String requireNo;

    public SearchQuery04(ReportType reportType) {
        this.reportType = reportType;
    }

    /**
     * 清除
     *
     * @param requireDepts
     */
    public void clear(List<String> requireDepts) {
        this.init();
        this.initDefault(requireDepts);
    }

    /**
     * 初始化
     */
    private void init() {
    	// 共用查詢條件初始化
    	this.publicConditionInit();
    	
        this.selectBigCategorySid = null;
        this.bigDataCateSids = Lists.newArrayList();
        this.middleDataCateSids = Lists.newArrayList();
        this.smallDataCateSids = Lists.newArrayList();
        this.requireDepts = Lists.newArrayList();
        this.rejectReason = null;
        this.rejectUserName = null;
        this.trCreatedUserName = null;
        this.fuzzyText = null;
        this.dateTypeIndex = 5;
        this.startDate = null;
        this.endDate = null;
        this.clearAdvance();
    }

    /**
     * 清除進階選項
     */
    public void clearAdvance() {
        this.startUpdatedDate = null;
        this.endUpdatedDate = null;
        this.startRejectedDate = null;
        this.endRejectedDate = null;
        this.urgencyList = null;
        this.requireNo = null;
    }

    /**
     * 初始化報表預設值
     *
     * @param requireDepts
     */
    private void initDefault(List<String> requireDepts) {
        this.requireDepts = requireDepts;
        this.selectRequireStatusType = RequireStatusType.ROLL_BACK_NOTIFY;
    }

    /**
     * 上個月
     */
    public void changeDateIntervalPreMonth() {
        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate lastDate = new LocalDate(date).minusMonths(1);
        this.dateTypeIndex = 0;
        this.startDate = lastDate.dayOfMonth().withMinimumValue().toDate();
        this.endDate = lastDate.dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 本月份
     */
    public void changeDateIntervalThisMonth() {
        this.dateTypeIndex = 1;
        this.startDate = new LocalDate().dayOfMonth().withMinimumValue().toDate();
        this.endDate = new LocalDate().dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 下個月
     */
    public void changeDateIntervalNextMonth() {
        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate nextDate = new LocalDate(date).plusMonths(1);
        this.dateTypeIndex = 2;
        this.startDate = nextDate.dayOfMonth().withMinimumValue().toDate();
        this.endDate = nextDate.dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 今日
     */
    public void changeDateIntervalToDay() {
        this.dateTypeIndex = 3;
        this.startDate = new Date();
        this.endDate = new Date();
    }
}
