package com.cy.tech.request.web.controller.setting;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.othset.OthSetThemeService;
import com.cy.tech.request.vo.require.othset.OthSetTheme;
import com.cy.tech.request.web.controller.view.component.OrgTreeVO;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.backend.logic.WorkBackendParamService;
import com.cy.work.backend.vo.enums.WkBackendParam;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author aken_kao
 */
@Slf4j
@Controller
@Scope("view")
public class NotifyDeptSettingMBean implements Serializable {
	/**
     * 
     */
    private static final long serialVersionUID = -9012163454413432868L;
    @Autowired
	private transient WorkBackendParamService workBackendParamService;
	@Autowired
	private OthSetThemeService othSetThemeService;

	@Getter
	@Setter
	private OrgTreeVO sendTestOrgTreeVO = new OrgTreeVO();
	@Getter
	@Setter
	private OrgTreeVO onProgramOrgTreeVO = new OrgTreeVO();
	@Getter
	@Setter
	private OrgTreeVO otherSettingOrgTreeVO = new OrgTreeVO();
	@Getter
	@Setter
	private List<OthSetTheme> othSetThemeList = Lists.newArrayList();
	@Getter
	@Setter
	private String othSetThemeSid;

	@PostConstruct
	public void init() {
		othSetThemeList = othSetThemeService.findAll();
		if (CollectionUtils.isNotEmpty(othSetThemeList)) {
			othSetThemeSid = othSetThemeList.get(0).getSid();
		}

		sendTestInit();
		onProgramInit();
		otherSettingInit();
	}

	/**
	 * 初始化-送測通知單位組織樹
	 */
	public void sendTestInit() {
		sendTestOrgTreeVO.setRequireDepts(
		        this.workBackendParamService.findIntsByKeywordWithOutCache(WkBackendParam.WT_DF_SEND_TEST_DEPS).stream()
		                .map(each -> each + "")
		                .collect(Collectors.toList()));
		sendTestOrgTreeVO.initOrgTree(true, false);

	}

	/**
	 * 初始化-ON程式通知單位組織樹
	 */
	public void onProgramInit() {
		// OP_DF_NOTICE_DEPS
		onProgramOrgTreeVO.setRequireDepts(
		        this.workBackendParamService.findIntsByKeywordWithOutCache(WkBackendParam.OP_DF_NOTICE_DEPS).stream()
		                .map(each -> each + "")
		                .collect(Collectors.toList()));
		onProgramOrgTreeVO.initOrgTree(true, false);
	}

	/**
	 * 初始化-其他資料設定通知單位組織樹
	 */
	public void otherSettingInit() {
		OthSetTheme othSetTheme = othSetThemeService.findOne(othSetThemeSid);
		if (othSetTheme != null) {
			otherSettingOrgTreeVO.setRequireDepts(othSetTheme.getNoticeDetails().stream()
			        .map(e -> e.getDepSid().toString()).collect(Collectors.toList()));
		}
		otherSettingOrgTreeVO.initOrgTree(true, false);
	}

	/**
	 * update 送測預設通知單位
	 */
	public void updateSendTest() {
		List<Org> orgList = sendTestOrgTreeVO.getSelectedDepts();
		try {
			String content = String.join(",", orgList.stream().map(org -> org.getSid().toString()).collect(Collectors.toList()));

			this.workBackendParamService.save(WkBackendParam.WT_DF_SEND_TEST_DEPS, content, SecurityFacade.getUserSid());

			sendTestInit();
			MessagesUtils.showInfo("已更新送測預設通知單位！");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			MessagesUtils.showError("新增失敗！" + e.getMessage());
		}
	}

	/**
	 * update ON程式預設通知單位
	 */
	public void updateOnProgram() {
		List<Org> orgList = onProgramOrgTreeVO.getSelectedDepts();
		try {
			String content = String.join(",", orgList.stream().map(org -> org.getSid().toString()).collect(Collectors.toList()));
			this.workBackendParamService.save(WkBackendParam.OP_DF_NOTICE_DEPS, content, SecurityFacade.getUserSid());
			onProgramInit();
			MessagesUtils.showInfo("已更新On程式預設通知單位！");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			MessagesUtils.showError("新增失敗！" + e.getMessage());
		}
	}

	/**
	 * update 其他資料設定預設通知單位
	 */
	public void updateOtherSetting() {
		List<Integer> noticeDeps = otherSettingOrgTreeVO.getSelectedDeptSids();
		try {
			othSetThemeService.saveDetail(othSetThemeSid, noticeDeps);
			otherSettingInit();
			MessagesUtils.showInfo("已更新其他資料設定預設通知單位！");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			MessagesUtils.showError("新增失敗！" + e.getMessage());
		}
	}

	/**
	 * 其他資料設定類別下拉選單 on change
	 */
	public void otherDataOnchange() {
		otherSettingInit();
	}

}
