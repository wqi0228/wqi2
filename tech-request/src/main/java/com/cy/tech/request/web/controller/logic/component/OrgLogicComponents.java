/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.logic.component;

import com.cy.commons.vo.Org;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.work.common.cache.WkOrgCache;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 部門邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class OrgLogicComponents implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7262441265322323211L;

    private static OrgLogicComponents instance;

    @Autowired
    private OrganizationService orgService;

    public static OrgLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        OrgLogicComponents.instance = this;
    }


    /**
     * 取得該部門向上延展的部門
     *
     * @param orgSid 部門Sid
     * @return
     */
    public List<Org> getAllParentOrgs(Integer orgSid) {
        try {
            return orgService.findParentOrgs(orgService.findBySid(orgSid));
        } catch (Exception e) {
            log.error("getAllParentOrgs ERROR", e);
        }
        return Lists.newArrayList();
    }

    /**
     * 取得該使用者管理部門及向下延展的部門
     *
     * @param userSid 使用者Sid
     * @return
     */
    public List<Org> getAllManagerOrgs(Integer userSid) {
        try {
            List<Org> managerOrgs = WkOrgCache.getInstance().findManagerOrgs(userSid);
            List<Org> allManangers = Lists.newArrayList();
            managerOrgs.forEach(item -> {
                if (item != null) {
                    allManangers.addAll(orgService.findChildOrgs(item));
                }
            });
            return allManangers;
        } catch (Exception e) {
            log.error("getAllManagerOrgs ERROR", e);
        }
        return Lists.newArrayList();
    }
    
       /**
     * 取得公司部門 By 公司Sid
     *
     * @param companySid 公司Sid
     * @return
     */
    public List<Org> findOrgsByCompanySid(Integer companySid) {
        List<Org> companyOrgs = Lists.newArrayList();
        orgService.findAll().forEach(item -> {
            if (item.getCompany() != null && item.getCompany().getSid().equals(companySid)) {
                companyOrgs.add(item);
            }
        });
        return companyOrgs;
    }

}
