/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.apache.commons.lang3.StringUtils;
import org.omnifaces.util.Faces;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.view.Search13View;
import com.cy.tech.request.logic.search.view.Search15View;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.URLService.URLServiceAttr;
import com.cy.tech.request.logic.service.send.test.SendTestService;
import com.cy.tech.request.vo.constants.ReqPermission;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.search.helper.Search15Helper;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.view.component.CategoryTreeComponent;
import com.cy.tech.request.web.controller.view.component.ReportOrgTreeComponent;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.listener.CategoryTreeCallBack;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.listener.ReportCustomFilterCallback;
import com.cy.tech.request.web.listener.ReportOrgTreeCallBack;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.tech.request.web.view.to.search.query.SearchQuery15;
import com.cy.tech.request.web.view.to.search.query.SearchQuery15CustomFilter;
import com.cy.work.common.cache.WkCommonCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.cache.WkUserWithRolesCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 送測異動明細表
 *
 * @author jason_h
 */
@Slf4j
@Controller
@Scope("view")
public class Search15MBean implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 888562425577028425L;
    @Autowired
	transient private LoginBean loginBean;
	@Autowired
	transient private Search15Helper searchHelper;
	@Autowired
	transient private TableUpDownBean upDownBean;
	@Autowired
	transient private ReqLoadBean loadManager;
	@Autowired
	transient private SearchHelper helper;
	@Autowired
	transient private URLService urlService;
	@Autowired
	transient private RequireService requireService;
	@Autowired
	transient private DisplayController display;
	@Autowired
	transient private SendTestService sendTestService;
	@Autowired
	transient private Require01MBean r01MBean;
	@Autowired
	transient private WkCommonCache wkCommonCache;

	@Getter
	/** 類別樹 Component */
	private CategoryTreeComponent categoryTreeComponent;
	@Getter
	/** 報表 組織樹 Component */
	private ReportOrgTreeComponent orgTreeComponent;
	@Getter
	/** 報表 組織樹 Component */
	private ReportOrgTreeComponent orgTreeNoticeComponent;

	@Getter
	/** 切換模式 - 全畫面狀態 */
	private SwitchType switchFullType = SwitchType.DETAIL;
	@Getter
	/** 切換模式 */
	private SwitchType switchType = SwitchType.CONTENT;
	@Getter
	/** 列表 id */
	private final String dataTableId = "dtRequire";
	@Getter
	/** 在匯出的時候，某些內容需要隱藏 */
	private boolean hasDisplay = true;
	@Getter
	/** 查詢物件 */
	private SearchQuery15 searchQuery;
	@Getter
	@Setter
	/** 所有的原型確認單據 */
	private List<Search15View> queryItems;
	private List<Search15View> tempItems;
	@Getter
	@Setter
	/** 選擇的原型確認單據 */
	private Search15View querySelection;
	@Getter
	/** 上下筆移動keeper */
	private Search15View queryKeeper;
	@Getter
	private Boolean isHideHeader = Boolean.FALSE;
	@Getter
	private SearchQuery15CustomFilter searchQuery15CustomFilter;
	@Autowired
	transient private ReportCustomFilterLogicComponent reportCustomFilterLogicComponent;
	@Setter
	@Getter
	/** dataTable filter */
	private String createDepName;
	@Getter
	/** dataTable filter items */
	private List<SelectItem> createDepNameItems;

	@PostConstruct
	public void init() {
		// 是否為歷程查詢
		String testInfoNo = this.getRequestParameter();
		this.initComponents(testInfoNo);

		// 送測異動明細表, 載入自訂預設搜尋條件, 但不預設查詢
		if (testInfoNo == null) {
			this.clearQuery();
		} else {
			// 歷程, 預設查詢by單號
			searchQuery.setTestinfoNo(testInfoNo);
			this.search();
		}
	}

	public void openDefaultSetting() {
		try {

			// 查詢登入者所有角色
			List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
			        SecurityFacade.getUserSid(),
			        SecurityFacade.getCompanyId());

			//具有 送測異動報表閱覽者 角色
			boolean hasPermissionRole = WkUserUtils.isUserHasRole(
					SecurityFacade.getUserSid(), 
					SecurityFacade.getCompanyId(),
					ReqPermission.ST_HISTORY_VIEWER);
			
			this.searchQuery15CustomFilter.loadDefaultSetting(
			        searchHelper.getDefaultDepSids(
			                loginBean.getDep().getSid(),
			                roleSids,
			                hasPermissionRole),
			        this.searchQuery15CustomFilter.getTempSearchQuery15());
		} catch (Exception e) {
			log.error("openDefaultSetting ERROR", e);
		}
	}

	/**
	 * 取得參數
	 *
	 * @return
	 */
	private String getRequestParameter() {
		String param_s = Faces.getRequestParameterMap().get(URLServiceAttr.URL_ATTR_S.getAttr());
		if (StringUtils.isNotBlank(param_s)) {
			String testInfoNo = urlService.parseIllegalUrlParam(
			        param_s,
			        WkUserCache.getInstance().findBySid(SecurityFacade.getUserSid()),
			        SecurityFacade.getCompanyId());

			if (testInfoNo.contains(URLService.ILLEAL_ACCESS)) {
				this.reloadToIllegalPage(testInfoNo.replace(URLService.ILLEAL_ACCESS, ""));
			}
			isHideHeader = Boolean.TRUE;
			return testInfoNo;
		}
		return null;
	}

	private void reloadToIllegalPage(String errorCode) {
		try {
			Faces.getExternalContext().redirect("../error/illegal_read.xhtml?" + URLService.ERROR_CODE_ATTR + "=" + errorCode);
			Faces.getContext().responseComplete();
		} catch (IOException ex) {
			log.error("導向讀取失敗頁面失敗...", ex);
		}
	}

	/**
	 * 初始化 元件
	 *
	 * @param testInfoNo
	 */
	private void initComponents(String testInfoNo) {

		// 查詢登入者所有角色
		List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
		        SecurityFacade.getUserSid(),
		        SecurityFacade.getCompanyId());

		this.searchQuery = new SearchQuery15(ReportType.WORK_TEST_HISTORY);
		this.categoryTreeComponent = new CategoryTreeComponent(categoryTreeCallBack);

		this.orgTreeComponent = new ReportOrgTreeComponent(
		        loginBean.getCompanyId(),
		        loginBean.getDep(),
		        roleSids,
		        true,
		        reportOrgTreeCallBack);

		this.orgTreeNoticeComponent = new ReportOrgTreeComponent(
		        loginBean.getCompanyId(),
		        loginBean.getDep(),
		        roleSids,
		        false,
		        reportOrgTreeNoticeCallBack);

		// 具有 送測異動報表閱覽者 角色
		boolean hasPermissionRole = WkUserUtils.isUserHasRole(
		        SecurityFacade.getUserSid(),
		        SecurityFacade.getCompanyId(),
		        ReqPermission.ST_HISTORY_VIEWER);

		this.searchQuery15CustomFilter = new SearchQuery15CustomFilter(loginBean.getCompanyId(), loginBean.getUserSId(),
		        loginBean.getDep().getSid(),
		        loginBean.getComp().getSid(),
		        reportCustomFilterLogicComponent,
		        messageCallBack,
		        display, reportCustomFilterCallback,
		        hasPermissionRole,
		        roleSids,
		        loginBean.getDep());
		searchQuery.initDefaultTestinfoNo(testInfoNo);
		this.searchQuery15CustomFilter.getTempSearchQuery15().initDefaultTestinfoNo(testInfoNo);
	}

	/**
	 * 還原預設值
	 */
	public void clear() {
		switchType = SwitchType.CONTENT;
		this.clearQuery();
		queryItems = Lists.newArrayList();
	}

	/**
	 * 清除/還原選項
	 */
	private void clearQuery() {
		try {

			// 查詢登入者所有角色
			List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
			        SecurityFacade.getUserSid(),
			        SecurityFacade.getCompanyId());

			//具有 送測異動報表閱覽者 角色
			boolean hasPermissionRole = WkUserUtils.isUserHasRole(
					SecurityFacade.getUserSid(), 
					SecurityFacade.getCompanyId(),
					ReqPermission.ST_HISTORY_VIEWER);
			
			this.searchQuery15CustomFilter.loadDefaultSetting(
			        searchHelper.getDefaultDepSids(
			                loginBean.getDep().getSid(),
			                roleSids,
			                hasPermissionRole),
			        this.searchQuery);

			categoryTreeComponent.clearCate();
		} catch (Exception e) {
			log.error("clearQuery ERROR", e);
		}
	}

	public void search() {
		tempItems = searchHelper.search(loginBean.getCompanyId(), loginBean.getUser(), searchQuery);
		this.createDepName = null;
		this.createDepNameItems = this.tempItems.stream()
		        .map(each -> each.getCreateDepName())
		        .collect(Collectors.toSet()).stream()
		        .map(each -> new SelectItem(each, each))
		        .collect(Collectors.toList());
		this.doChangeFilter();
	}

	/**
	 * 進行dataTable filter
	 */
	public void doChangeFilter() {
		this.queryItems = this.filterDataTable();
	}

	/**
	 * filter
	 *
	 * @return
	 */
	private List<Search15View> filterDataTable() {
		return tempItems.stream()
		        .filter(each -> Strings.isNullOrEmpty(createDepName)
		                || createDepName.equals(each.getCreateDepName()))
		        .collect(Collectors.toList());
	}

	/**
	 * 隱藏部分column裡的內容
	 */
	public void hideColumnContent() {
		hasDisplay = false;
	}

	/**
	 * 匯出excel
	 *
	 * @param document
	 */
	public void exportExcel(Object document) {
		helper.exportExcel(document, searchQuery.getStartDate(), searchQuery.getEndDate(), searchQuery.getReportType());
		hasDisplay = true;
	}

	/**
	 * 半版row選擇
	 *
	 * @param event
	 */
	public void onRowSelect(SelectEvent event) {
		this.queryKeeper = this.querySelection = (Search15View) event.getObject();
		this.changeRequireContent(this.queryKeeper);
		this.moveScreenTab();
	}

	/**
	 * 開啟分頁
	 *
	 * @param dtId
	 * @param widgetVar
	 * @param pageCount
	 * @param to
	 */
	public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search15View to) {
		this.highlightReportTo(widgetVar, pageCount, to);
		this.resetUpdownInfo();
		this.checkHelfScreen();
	}

	/**
	 * highlight列表位置
	 *
	 * @param widgetVar
	 * @param pageCount
	 * @param to
	 */
	private void highlightReportTo(String widgetVar, String pageCount, Search15View to) {
		querySelection = to;
		queryKeeper = querySelection;
		display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
	}

	/**
	 * 取得索引位置
	 *
	 * @param pageCountStr
	 * @return
	 */
	private int getRowIndex(String pageCountStr) {
		Integer pageCount = 50;
		if (!Strings.isNullOrEmpty(pageCountStr)) {
			try {
				pageCount = Integer.valueOf(pageCountStr);
			} catch (Exception e) {
				log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
			}
		}
		return queryItems.indexOf(querySelection) % pageCount;
	}

	/**
	 * 重設定上下筆資訊
	 */
	private void resetUpdownInfo() {
		upDownBean.setCurrRow(queryKeeper.getRequireNo());
		upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
		upDownBean.resetTabInfo(RequireBottomTabType.SEND_TEST_INFO, queryKeeper.getTestInfoSid());
	}

	private boolean checkHelfScreen() {
		if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
		        || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
			this.normalScreenReport();
			display.update("headerTitle");
			display.update("searchBody");
			return true;
		}
		return false;
	}

	/**
	 * 切換 - 返回報表
	 */
	public void normalScreenReport() {
		this.querySelection = this.queryKeeper;
		switchFullType = SwitchType.DETAIL;
		this.toggleSearchBody();
	}

	/**
	 * 切換 - 全畫面需求單
	 *
	 * @param view
	 */
	public void fullScreenForm(Search15View view) {
		this.queryKeeper = this.querySelection = view;
		switchFullType = SwitchType.FULLCONTENT;
		this.toggleSearchBody();
		this.moveScreenTab();
	}

	/**
	 * 切換查詢表身
	 */
	public void toggleSearchBody() {
		if (switchType.equals(SwitchType.CONTENT)) {
			switchType = SwitchType.DETAIL;
			if (querySelection != null) {
				queryKeeper = querySelection;
			} else if (this.queryKeeper == null) {
				this.querySelection = this.queryKeeper = this.queryItems.get(0);
			}
			this.changeRequireContent(queryKeeper);
			this.moveScreenTab();
			return;
		}
		if (switchType.equals(SwitchType.DETAIL)) {
			switchFullType = SwitchType.DETAIL;
			switchType = SwitchType.CONTENT;
		}
	}

	/**
	 * 變更需求單內容
	 *
	 * @param view
	 */
	private void changeRequireContent(Search15View view) {
	    view.setReadRecordType(ReadRecordType.HAS_READ);
		Require require = requireService.findByReqNo(view.getRequireNo());
		loadManager.reloadReqForm(require, loginBean.getUser(), RequireBottomTabType.SEND_TEST_INFO);
	}

	/**
	 * 上一筆（分頁）
	 *
	 * @param dtId
	 * @param widgetVar
	 */
	public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
		int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
		if (index > 0) {
			index--;
			querySelection = queryItems.get(index);
		}
		this.refreshViewByOpener(dtId, widgetVar, pageCount);
	}

	/**
	 * 下一筆（分頁）
	 *
	 * @param dtId
	 * @param widgetVar
	 */
	public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
		int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
		if (queryItems.size() > index + 1) {
			index++;
			querySelection = queryItems.get(index);
		}
		this.refreshViewByOpener(dtId, widgetVar, pageCount);
	}

	/**
	 * 刷新列表（分頁）
	 *
	 * @param dtId
	 * @param widgetVar
	 * @param pageCount
	 */
	private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
		queryKeeper = querySelection;
		this.highlightReportTo(widgetVar, pageCount, queryKeeper);
		this.resetUpdownInfo();
		this.checkHelfScreen();
	}

	/**
	 * 上下筆移動
	 *
	 * @param action
	 */
	public void moveRequireTemplateSelect(int action) {
		int index = this.queryItems.indexOf(this.queryKeeper);
		index += action;
		if (index < 0 || index >= this.queryItems.size()) {
			return;
		}
		this.querySelection = this.queryKeeper = this.queryItems.get(index);
		this.changeRequireContent(this.querySelection);
		this.moveScreenTab();
	}

	/**
	 * 開啟 類別樹
	 */
	public void btnOpenCategoryTree() {
		try {
			categoryTreeComponent.init();
			display.showPfWidgetVar("dlgCate");
		} catch (Exception e) {
			log.error("btnOpenCategoryTree Error", e);
			messageCallBack.showMessage(e.getMessage());
		}
	}

	/**
	 * 開啟 單位挑選 組織樹
	 */
	public void btnOpenOrgTree() {
		
		//具有 送測異動報表閱覽者 角色
		boolean hasPermissionRole = WkUserUtils.isUserHasRole(
				SecurityFacade.getUserSid(), 
				SecurityFacade.getCompanyId(),
				ReqPermission.ST_HISTORY_VIEWER);
		
		try {
			if (hasPermissionRole) {
				orgTreeComponent.initOrgTree(loginBean.getComp(), searchQuery.getRequireDepts(), false, false);
			} else {
				orgTreeComponent.initOrgTree(loginBean.getDep(), searchQuery.getRequireDepts(), false, false);
			}
			display.showPfWidgetVar("dlgOrgTree");
		} catch (Exception e) {
			log.error("btnOpenOrgTree Error", e);
			messageCallBack.showMessage(e.getMessage());
		}
	}

	/**
	 * 開啟 轉發至 組織樹
	 */
	public void btnOpenNoticeOrgTree() {
		try {

			// 具有 送測異動報表閱覽者 角色
			boolean hasPermissionRole = WkUserUtils.isUserHasRole(
			        SecurityFacade.getUserSid(),
			        SecurityFacade.getCompanyId(),
			        ReqPermission.ST_HISTORY_VIEWER);

			if (hasPermissionRole) {
				orgTreeNoticeComponent.initOrgTree(loginBean.getComp(), searchQuery.getNoticeDepts(), false, false);
			} else {
				orgTreeNoticeComponent.initOrgTree(loginBean.getDep(), searchQuery.getNoticeDepts(), false, false);
			}
			display.showPfWidgetVar("dlgOrgTreeNotice");
		} catch (Exception e) {
			log.error("btnOpenForwardOrgTree Error", e);
			messageCallBack.showMessage(e.getMessage());
		}
	}

	/** 訊息呼叫 */
	private final MessageCallBack messageCallBack = new MessageCallBack() {
		/**
         * 
         */
        private static final long serialVersionUID = -61644322508477410L;

        @Override
		public void showMessage(String m) {
			MessagesUtils.showError(m);
		}
	};

	/** 類別樹 Component CallBack */
	private final CategoryTreeCallBack categoryTreeCallBack = new CategoryTreeCallBack() {
		/**
         * 
         */
        private static final long serialVersionUID = -2449403490276074107L;

        @Override
		public void showMessage(String m) {
			messageCallBack.showMessage(m);
		}

		@Override
		public void confirmSelCate() {
			categoryTreeComponent.selCate();
			searchQuery.setBigDataCateSids(categoryTreeComponent.getBigDataCateSids());
			searchQuery.setMiddleDataCateSids(categoryTreeComponent.getMiddleDataCateSids());
			searchQuery.setSmallDataCateSids(categoryTreeComponent.getSmallDataCateSids());
			categoryTreeComponent.clearCateSids();
		}

		@Override
		public void loadSelCate(List<String> smallDataCateSids) {
			categoryTreeComponent.init();
			categoryTreeComponent.selectedItem(smallDataCateSids);
			this.confirmSelCate();
		}

	};

	/** 報表 組織樹 Component CallBack */
	private final ReportOrgTreeCallBack reportOrgTreeCallBack = new ReportOrgTreeCallBack() {
		/**
         * 
         */
        private static final long serialVersionUID = 1153575685748885498L;

        @Override
		public void showMessage(String m) {
			messageCallBack.showMessage(m);
		}

		@Override
		public void confirmSelOrg() {
			searchQuery.setRequireDepts(orgTreeComponent.getSelOrgSids());
		}
	};

	/** 報表 組織樹 Component CallBack */
	private final ReportOrgTreeCallBack reportOrgTreeNoticeCallBack = new ReportOrgTreeCallBack() {
		/**
         * 
         */
        private static final long serialVersionUID = -7572589413703416004L;

        @Override
		public void showMessage(String m) {
			messageCallBack.showMessage(m);
		}

		@Override
		public void confirmSelOrg() {
			searchQuery.setNoticeDepts(orgTreeNoticeComponent.getSelOrgSids());
		}
	};

	/**
	 * 送測單位
	 * 
	 * @param view
	 * @return
	 */
	public String getSendTestDepStr(Search13View view) {

		// cache 中取回
		String cacheName = "Search15View-getSendTestDepStr";
		List<String> keys = Lists.newArrayList(view.getTestinfoNo());
		String orgNames = wkCommonCache.getCache(cacheName, keys, 5);
		if (orgNames != null) {
			return orgNames;
		}

		// 組單位名稱
		List<String> orgSids = Optional.ofNullable(view.getSendTestDep())
		        .map(send -> send.getValue())
		        .orElse(Lists.newArrayList());

		orgNames = WkOrgUtils.findNameBySidStrs(orgSids, "、");

		// put 快取
		wkCommonCache.putCache(cacheName, keys, orgNames);

		return orgNames;
	}

	public void moveScreenTab() {
		this.r01MBean.getBottomTabMBean().resetTabIdx(this.r01MBean);
		this.r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.SEND_TEST_INFO);

		List<String> sids = sendTestService.findSidsBySourceSid(this.r01MBean.getRequire().getSid());
		String indicateSid = queryKeeper.getTestInfoSid();
		if (sids.indexOf(indicateSid) != -1) {
			display.execute("PF('st_acc_panel_layer_zero').select(" + sids.indexOf(indicateSid) + ");");
			for (int i = 0; i < sids.size(); i++) {
				if (i != sids.indexOf(indicateSid)) {
					display.execute("PF('st_acc_panel_layer_zero').unselect(" + i + ");");
				}
			}
		}
	}

	private final ReportCustomFilterCallback reportCustomFilterCallback = new ReportCustomFilterCallback() {
		/**
         * 
         */
        private static final long serialVersionUID = 9082900410644751610L;

        @Override
		public void reloadDefault(String index) {

			// 查詢登入者所有角色
			List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
			        SecurityFacade.getUserSid(),
			        SecurityFacade.getCompanyId());

			// 具有 送測異動報表閱覽者 角色
			boolean hasPermissionRole = WkUserUtils.isUserHasRole(
			        SecurityFacade.getUserSid(),
			        SecurityFacade.getCompanyId(),
			        ReqPermission.ST_HISTORY_VIEWER);

			searchQuery15CustomFilter.loadDefaultSetting(
			        searchHelper.getDefaultDepSids(
			                loginBean.getDep().getSid(),
			                roleSids,
			                hasPermissionRole),
			        searchQuery);
			categoryTreeCallBack.loadSelCate(searchQuery.getSmallDataCateSids());
		}
	};

}
