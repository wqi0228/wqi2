/**
 * 
 */
package com.cy.tech.request.web.controller.component.mipker.vo;

import java.io.Serializable;
import java.util.Map;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import com.cy.work.common.vo.WkItem;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 *
 */
public class MultItemPickerTreeVO implements Serializable{


    /**
     * 
     */
    private static final long serialVersionUID = -5384692699826755184L;

    /**
     * 有樹狀顯示模式
     */
    @Setter
    @Getter
    private boolean hasTreeMode = false;
    
    /**
     * 是否全選
     */
    @Getter
    @Setter
    private boolean allSelected = false;
    
    /**
     * 樹節點結構資料
     */
    @Getter
    @Setter
    private TreeNode rootNode = new DefaultTreeNode();
    
    

    /**
     * 畫面傳入選擇的節點
     */
    @Getter
    @Setter
    private TreeNode[] selectedNodes;
    
    /**
     * 
     */
    @Getter
    @Setter
    private String serachItemNameKeyword;
    
    @Getter
    private Map<WkItem, TreeNode> treeNodeMapByItem = Maps.newLinkedHashMap();
    
}
