package com.cy.tech.request.web.controller.test;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.web.controller.component.mutiuserqkpker.MultiUserQuickPickerCallback;
import com.cy.tech.request.web.controller.component.mutiuserqkpker.MultiUserQuickPickerComponent;
import com.cy.tech.request.web.controller.component.mutiuserqkpker.MultiUserQuickPickerConfig;
import com.cy.tech.request.web.controller.component.mutiuserqkpker.MultiUserQuickPickerHelper;
import com.cy.tech.request.web.controller.component.mutiuserqkpker.model.MultiUserQuickPickerUserGroupData;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.exception.SystemDevelopException;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Controller
@Scope("view")
@Slf4j
public class TestMultiUserQuickPickerMBean implements Serializable {

    // ========================================================================
    // 服務區
    // ========================================================================

    /**
     * 
     */
    private static final long serialVersionUID = -5486372231825655251L;
    // ========================================================================
    // 變數區
    // ========================================================================
    @Getter
    private MultiUserQuickPickerComponent userPicker;
    @Getter
    private final String USER_PICKER_COMPNT_ID = "USER_PICKER_COMPNT_ID";

    // ========================================================================
    // 方法區
    // ========================================================================
    @PostConstruct
    public void init() {

        try {
            // 選擇器設定資料
            MultiUserQuickPickerConfig config = new MultiUserQuickPickerConfig();
            // 預設展開登入者
            config.setDefaultExpandUserSids(Sets.newHashSet(SecurityFacade.getUserSid()));
            config.setEnableGroupMode(true);

            // 選擇器初始化
            this.userPicker = new MultiUserQuickPickerComponent(
                    SecurityFacade.getCompanyId(),
                    USER_PICKER_COMPNT_ID,
                    Lists.newArrayList(1342), // 預設選擇 yi-fan
                    config,
                    callback);

        } catch (Exception e) {
            String errorMessage = "建立使用者選單失敗!";
            log.error(errorMessage, e);
            MessagesUtils.showError("建立單位選單失敗, 請恰系統人員!");
            return;
        }
    }

    protected final MultiUserQuickPickerCallback callback = new MultiUserQuickPickerCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = 7346113120992194997L;

        /**
         * @return 群組項目
         * @throws SystemDevelopException
         */
        @Override
        public List<MultiUserQuickPickerUserGroupData> prepareGroupItems() throws SystemDevelopException {
            return MultiUserQuickPickerHelper.getInstance().findUserGroups(SecurityFacade.getUserSid());
        }
    };
}
