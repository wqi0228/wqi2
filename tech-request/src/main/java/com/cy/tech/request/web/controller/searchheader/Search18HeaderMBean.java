/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.searchheader;

import com.cy.commons.vo.Org;
import com.cy.tech.request.logic.search.enums.Search18SubType;
import com.cy.tech.request.logic.service.OrganizationService;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * ON程式異動明細表 的搜尋表頭
 *
 * @author jason_h
 */
@Controller
@Scope("view")
public class Search18HeaderMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 29980975420549827L;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private OrganizationService orgSerivce;

    /** 剩餘到期日 */
    @Getter
    @Setter
    private int remainderExpireDate;
    @Getter
    @Setter
    private Search18SubType confirmFilterType;
    /** 不包含結案 */
    @Getter
    @Setter
    private Boolean excludeClose;

    @PostConstruct
    public void init() {
        this.clear();

    }

    /**
     * 清除/還原選項
     */
    public void clear() {
        this.initCommHeader();
        this.initHeader();
    }

    /**
     * 初始化commHeader
     */
    private void initCommHeader() {
        commHeaderMBean.clear();
        commHeaderMBean.initDefaultRequireDepts(true);
    }

    /**
     * 初始化Header
     */
    private void initHeader() {
        remainderExpireDate = 2;
        confirmFilterType = Search18SubType.ALL;
        excludeClose = Boolean.TRUE;
    }

    /**
     * 確認類型
     *
     * @return
     */
    public SelectItem[] getConfirmTypeItems() {
        Search18SubType[] status = Search18SubType.values();
        SelectItem[] items = new SelectItem[status.length];
        for (int i = 0; i < status.length; i++) {
            items[i] = new SelectItem(status[i], status[i].getShowName());
        }
        return items;
    }

    /**
     * 含主要部門及子部門
     *
     * @param dep
     * @return
     */
    public List<String> createUnitGroupDep(Org dep) {
        return orgSerivce.findChildOrgs(dep).stream()
                  .map(org -> org.getSid().toString())
                  .collect(Collectors.toList());
    }

    /**
     * 查詢向下子部門REGEXP
     *
     * @param dep
     * @return
     */
    public String createOrgsRegexpParam(Org dep) {
        List<Org> queryOrgs = orgSerivce.findChildOrgs(dep);
        return queryOrgs.stream().map(each -> "\"" + each.getSid() + "\"").collect(Collectors.joining("|"));
    }
}
