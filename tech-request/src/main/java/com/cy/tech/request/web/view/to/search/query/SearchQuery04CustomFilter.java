/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.view.to.search.query;

import java.io.Serializable;
import java.util.List;

import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.service.setting.SettingCheckConfirmRightService;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.Search04QueryColumn;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.view.component.CategoryTreeComponent;
import com.cy.tech.request.web.controller.view.component.ReportOrgTreeComponent;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterArrayStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterDetailVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterVO;
import com.cy.tech.request.web.listener.CategoryTreeCallBack;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.listener.ReportCustomFilterCallback;
import com.cy.tech.request.web.listener.ReportOrgTreeCallBack;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
public class SearchQuery04CustomFilter implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -2303491503638769746L;
    /** 登入者Sid */
	private Integer loginUserSid;
	/** 登入者部門UserSid */
	private Integer loginDepSid;
	/** 登入者公司Sid */
	private Integer loginCompSid;
	@Getter
	private SearchQuery04 tempSearchQuery04;
	/** 該登入者在需求單,自訂查詢條件List */
	private List<ReportCustomFilterVO> reportCustomFilterVOs;
	/** 該登入者在需求單,挑選自訂查詢條件物件 */
	private ReportCustomFilterVO selReportCustomFilterVO;
	/** ReportCustomFilterLogicComponent */
	private ReportCustomFilterLogicComponent reportCustomFilterLogicComponent;
	/** 類別樹 Component */
	@Getter
	private CategoryTreeComponent categoryTreeComponent;
	/** MessageCallBack */
	private MessageCallBack messageCallBack;
	/** DisplayController */
	private DisplayController display;
	/** ReportCustomFilterCallback */
	private ReportCustomFilterCallback reportCustomFilterCallback;
	@Getter
	/** 報表 組織樹 Component */
	private ReportOrgTreeComponent orgTreeComponent;

	public SearchQuery04CustomFilter(
	        String compId,
	        Integer loginUserSid,
	        Integer loginDepSid,
	        Integer loginCompSid,
	        ReportCustomFilterLogicComponent reportCustomFilterLogicComponent,
	        MessageCallBack messageCallBack,
	        DisplayController display,
	        ReportCustomFilterCallback reportCustomFilterCallback,
	        List<Long> roleSids,
	        Org dep) {
		this.loginUserSid = loginUserSid;
		this.loginDepSid = loginDepSid;
		this.loginCompSid = loginCompSid;
		this.reportCustomFilterLogicComponent = reportCustomFilterLogicComponent;
		this.messageCallBack = messageCallBack;
		this.display = display;
		this.categoryTreeComponent = new CategoryTreeComponent(categoryTreeCallBack);
		this.orgTreeComponent = new ReportOrgTreeComponent(
		        compId, dep, roleSids, true, reportOrgTreeCallBack);
		this.reportCustomFilterCallback = reportCustomFilterCallback;
		this.tempSearchQuery04 = new SearchQuery04(ReportType.REJECT);
	}

	public void loadDefaultSetting(List<String> requireDepts, SearchQuery04 searchQuery) {
		this.initDefault(requireDepts, searchQuery);
		reportCustomFilterVOs = reportCustomFilterLogicComponent.getReportCustomFilter(Search04QueryColumn.Search04Query, loginUserSid);
		if (reportCustomFilterVOs != null && !reportCustomFilterVOs.isEmpty()) {
			selReportCustomFilterVO = reportCustomFilterVOs.get(0);
			loadSettingData(searchQuery);
		}
	}

	/** 載入挑選的自訂搜尋條件 */
	public void loadSettingData(SearchQuery04 searchQuery) {
		selReportCustomFilterVO.getReportCustomFilterDetailVOs().forEach(item -> {
			if (Search04QueryColumn.DemandType.equals(item.getSearchReportCustomEnum())
			        && item instanceof ReportCustomFilterStringVO) {
				settingDemandType(item, searchQuery);
			} else if (Search04QueryColumn.DemandProcess.equals(item.getSearchReportCustomEnum())
			        && item instanceof ReportCustomFilterStringVO) {
				settingDemandProcess(item, searchQuery);
			} else if (Search04QueryColumn.Department.equals(item.getSearchReportCustomEnum())
			        && item instanceof ReportCustomFilterArrayStringVO) {
				settingDepartment(item, searchQuery);
			} else if (Search04QueryColumn.DemandPerson.equals(item.getSearchReportCustomEnum())
			        && item instanceof ReportCustomFilterStringVO) {
				settingDemandPerson(item, searchQuery);
			} else if (Search04QueryColumn.CategoryCombo.equals(item.getSearchReportCustomEnum())
			        && item instanceof ReportCustomFilterArrayStringVO) {
				settingCategoryCombo(item, searchQuery);
			} else if (Search04QueryColumn.RejectReason.equals(item.getSearchReportCustomEnum())
			        && item instanceof ReportCustomFilterStringVO) {
				settingRejectReason(item, searchQuery);
			} else if (Search04QueryColumn.RejectPerson.equals(item.getSearchReportCustomEnum())
			        && item instanceof ReportCustomFilterStringVO) {
				settingRejectPerson(item, searchQuery);
			} else if (Search04QueryColumn.DateIndex.equals(item.getSearchReportCustomEnum())
			        && item instanceof ReportCustomFilterStringVO) {
				settingDateIndex(item, searchQuery);
			} else if (Search04QueryColumn.SearchText.equals(item.getSearchReportCustomEnum())
			        && item instanceof ReportCustomFilterStringVO) {
				settingFuzzyText(item, searchQuery);
			}
		});
	}

	/**
	 * 塞入退件人員
	 *
	 * @param reportCustomFilterDetailVO
	 */
	private void settingRejectPerson(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery04 searchQuery) {
		try {
			ReportCustomFilterStringVO ReportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
			searchQuery.setRejectUserName(ReportCustomFilterStringVO.getValue());
		} catch (Exception e) {
			log.error("settingCategoryCombo", e);
		}
	}

	/**
	 * 塞入退件原因
	 *
	 * @param reportCustomFilterDetailVO
	 */
	private void settingRejectReason(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery04 searchQuery) {
		try {
			ReportCustomFilterStringVO ReportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
			searchQuery.setRejectReason(ReportCustomFilterStringVO.getValue());
		} catch (Exception e) {
			log.error("settingCategoryCombo", e);
		}
	}

	/**
	 * 塞入立單區間Type
	 *
	 * @param reportCustomFilterDetailVO
	 */
	private void settingDateIndex(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery04 searchQuery) {
		try {
			ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
			searchQuery.setDateTypeIndex(Integer.valueOf(reportCustomFilterStringVO.getValue()));
			switch (searchQuery.getDateTypeIndex()) {
			case 0:
				searchQuery.changeDateIntervalPreMonth();
				break;
			case 1:
				searchQuery.changeDateIntervalThisMonth();
				break;
			case 2:
				searchQuery.changeDateIntervalNextMonth();
				break;
			case 3:
				searchQuery.changeDateIntervalToDay();
				break;
			default:
				break;
			}
		} catch (Exception e) {
			log.error("settingFuzzyText", e);
		}
	}

	/**
	 * 塞入需求類別
	 *
	 * @param reportCustomFilterDetailVO
	 */
	private void settingCategoryCombo(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery04 searchQuery) {
		try {
			searchQuery.getSmallDataCateSids().clear();
			ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
			if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
				searchQuery.getSmallDataCateSids().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
			}
		} catch (Exception e) {
			log.error("settingCategoryCombo", e);
		}
	}

	/**
	 * 塞入模糊搜尋
	 *
	 * @param reportCustomFilterDetailVO
	 */
	private void settingFuzzyText(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery04 searchQuery) {
		try {
			ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
			searchQuery.setFuzzyText(reportCustomFilterStringVO.getValue());
		} catch (Exception e) {
			log.error("settingFuzzyText", e);
		}
	}

	/**
	 * 塞入需求人員
	 *
	 * @param reportCustomFilterDetailVO
	 */
	private void settingDemandPerson(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery04 searchQuery) {
		try {
			ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
			searchQuery.setTrCreatedUserName(reportCustomFilterStringVO.getValue());
		} catch (Exception e) {
			log.error("trCreatedUserName", e);
		}
	}

	/**
	 * 塞入需求單位
	 *
	 * @param reportCustomFilterDetailVO
	 */
	private void settingDepartment(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery04 searchQuery) {
		try {
			searchQuery.getRequireDepts().clear();
			ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
			if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
				searchQuery.getRequireDepts().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
			}
		} catch (Exception e) {
			log.error("settingDepartment", e);
		}
	}

	/**
	 * 塞入需求製作進度
	 *
	 * @param reportCustomFilterDetailVO
	 */
	private void settingDemandProcess(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery04 searchQuery) {
		try {
			ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
			searchQuery.setSelectRequireStatusType(
			        RequireStatusType.safeValueOf(reportCustomFilterStringVO.getValue()));
		} catch (Exception e) {
			log.error("settingDemandType", e);
		}
	}

	/**
	 * 塞入需求類別
	 *
	 * @param reportCustomFilterDetailVO
	 */
	private void settingDemandType(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery04 searchQuery) {
		try {
			ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
			searchQuery.setSelectBigCategorySid(reportCustomFilterStringVO.getValue());
		} catch (Exception e) {
			log.error("settingDemandType", e);
		}
	}

	/** 儲存自訂搜尋條件 */
	public void saveReportCustomFilter() {
		try {
			ReportCustomFilterDetailVO demandType = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search04QueryColumn.DemandType,
			        (tempSearchQuery04.getSelectBigCategorySid() != null) ? tempSearchQuery04.getSelectBigCategorySid() : "");
			ReportCustomFilterDetailVO department = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search04QueryColumn.Department,
			        tempSearchQuery04.getRequireDepts());
			ReportCustomFilterDetailVO demandProcess = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(
			        Search04QueryColumn.DemandProcess,
			        tempSearchQuery04.getSelectRequireStatusType().name());

			ReportCustomFilterDetailVO rejectReason = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search04QueryColumn.RejectReason,
			        tempSearchQuery04.getRejectReason());
			ReportCustomFilterDetailVO rejectPerson = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search04QueryColumn.RejectPerson,
			        tempSearchQuery04.getRejectUserName());
			ReportCustomFilterDetailVO searchText = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search04QueryColumn.SearchText,
			        tempSearchQuery04.getFuzzyText());

			ReportCustomFilterDetailVO demandPerson = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search04QueryColumn.DemandPerson,
			        tempSearchQuery04.getTrCreatedUserName());
			ReportCustomFilterDetailVO categoryCombo = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search04QueryColumn.CategoryCombo,
			        tempSearchQuery04.getSmallDataCateSids());
			ReportCustomFilterDetailVO dateIndex = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search04QueryColumn.DateIndex,
			        (tempSearchQuery04.getDateTypeIndex() != null) ? String.valueOf(tempSearchQuery04.getDateTypeIndex()) : "5");
			List<ReportCustomFilterDetailVO> saveDetails = Lists.newArrayList();
			saveDetails.add(demandType);
			saveDetails.add(demandProcess);
			saveDetails.add(rejectReason);
			saveDetails.add(rejectPerson);
			saveDetails.add(department);
			saveDetails.add(demandPerson);
			saveDetails.add(searchText);
			saveDetails.add(categoryCombo);
			saveDetails.add(dateIndex);
			reportCustomFilterLogicComponent.saveReportCustomFilter(Search04QueryColumn.Search04Query, loginUserSid,
			        (selReportCustomFilterVO != null) ? selReportCustomFilterVO.getIndex() : null, true, saveDetails);
			reportCustomFilterCallback.reloadDefault("");
			display.update("headerTitle");
			display.execute("doSearchData();");
			display.hidePfWidgetVar("dlgReportCustomFilter");
		} catch (Exception e) {
			this.messageCallBack.showMessage(e.getMessage());
			log.error("saveReportCustomFilter ERROR", e);
		}
	}

	public void initDefault(List<String> requireDepts, SearchQuery04 searchQuery) {
		searchQuery.clear(requireDepts);
	}

	/** 類別樹 Component CallBack */
	private final CategoryTreeCallBack categoryTreeCallBack = new CategoryTreeCallBack() {
		/**
         * 
         */
        private static final long serialVersionUID = 8811343250349372042L;

        @Override
		public void showMessage(String m) {
			messageCallBack.showMessage(m);
		}

		@Override
		public void confirmSelCate() {
			categoryTreeComponent.selCate();
			tempSearchQuery04.getBigDataCateSids().clear();
			tempSearchQuery04.getMiddleDataCateSids().clear();
			tempSearchQuery04.getSmallDataCateSids().clear();
			tempSearchQuery04.getBigDataCateSids().addAll(Lists.newArrayList(categoryTreeComponent.getBigDataCateSids()));
			tempSearchQuery04.getMiddleDataCateSids().addAll(Lists.newArrayList(categoryTreeComponent.getMiddleDataCateSids()));
			tempSearchQuery04.getSmallDataCateSids().addAll(Lists.newArrayList(categoryTreeComponent.getSmallDataCateSids()));
		}
	};

	/**
	 * 開啟 類別樹
	 */
	public void btnOpenCategoryTree() {
		try {
			categoryTreeComponent.init();
			categoryTreeComponent.selectedItem(tempSearchQuery04.getSmallDataCateSids());
			display.showPfWidgetVar("defaultDlgCate");
		} catch (Exception e) {
			log.error("btnOpenCategoryTree Error", e);
			messageCallBack.showMessage(e.getMessage());
		}
	}

	/**
	 * 開啟 單位挑選 組織樹
	 */
	public void btnOpenOrgTree() {
		try {
			Org dep = WkOrgCache.getInstance().findBySid(loginDepSid);
			SettingCheckConfirmRightService settingCheckConfirmRightService = WorkSpringContextHolder.getBean(SettingCheckConfirmRightService.class);
			if (settingCheckConfirmRightService.hasCanCheckUserRight(SecurityFacade.getUserSid())) {
				dep = WkOrgCache.getInstance().findBySid(this.loginCompSid);
			}
			orgTreeComponent.initOrgTree(dep, tempSearchQuery04.getRequireDepts(), true, false);
			display.showPfWidgetVar("defaultdlgOrgTree");
		} catch (Exception e) {
			log.error("btnOpenOrgTree Error", e);
			messageCallBack.showMessage(e.getMessage());
		}
	}

	/** 報表 組織樹 Component CallBack */
	private final ReportOrgTreeCallBack reportOrgTreeCallBack = new ReportOrgTreeCallBack() {
		/**
         * 
         */
        private static final long serialVersionUID = -5223758514197496392L;

        @Override
		public void showMessage(String m) {
			messageCallBack.showMessage(m);
		}

		@Override
		public void confirmSelOrg() {
			tempSearchQuery04.getRequireDepts().clear();
			tempSearchQuery04.getRequireDepts().addAll(orgTreeComponent.getSelOrgSids());
		}
	};

	/** 清除立單區間Type */
	public void clearDateType() {
		if (4 == tempSearchQuery04.getDateTypeIndex()) {
			tempSearchQuery04.setDateTypeIndex(5);
		}
	}

}
