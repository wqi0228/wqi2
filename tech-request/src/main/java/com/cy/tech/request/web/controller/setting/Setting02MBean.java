/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.setting;

import com.cy.tech.request.logic.service.TemplateBaseFieldService;
import com.cy.tech.request.vo.template.TemplateBaseDataField;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * 基礎欄位模版資料
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope("view")
public class Setting02MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -411068250690031932L;
    @Autowired
    transient private DisplayController displayController;
    @Autowired
    transient private TemplateBaseFieldService service;
    
    @Getter
    @Setter
    private String searchText;
    @Getter
    @Setter
    private Boolean maintenance;
    @Getter
    @Setter
    private String maintenanceHeader;
    @Getter
    @Setter
    private TemplateBaseDataField maintenBasicField;
    @Getter
    @Setter
    private List<TemplateBaseDataField> items;

    @PostConstruct
    public void init() {
        this.setSearchText("");
        this.searchByStatusAndTextData();
    }

    public void searchByStatusAndTextData() {
        this.setMaintenance(Boolean.FALSE);
        this.setMaintenanceHeader("檢視");
        items = service.findByFazzyText(searchText);
    }

    /**
     * 進入維護畫面 新增 | 編輯
     *
     * @param maintenaceHeader
     * @param id
     */
    public void startMaintenace(String maintenaceHeader, String id) {
        this.setMaintenance(Boolean.TRUE);
        this.setMaintenanceHeader(maintenaceHeader);
        if ("nvl".equals(id)) {
            this.setMaintenBasicField(new TemplateBaseDataField());
        }
        this.updateViewPanel();
    }

    public void save() {
        try {
            this.checkInputInfo();
            service.save(this.getMaintenBasicField());
            log.debug("執行存檔..." + toString());
            this.init();
            this.updateViewPanel();
        } catch (IllegalArgumentException e) {
            log.debug(e.getMessage(), e);
            MessagesUtils.showError(e.getMessage());
        } catch (Exception e) {
            log.error("存檔失敗..." + e.getMessage(), e);
            MessagesUtils.showError(e.getMessage());
        }
    }

    private void checkInputInfo() {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(maintenBasicField.getFieldName()), "欄位名稱不可為空白，請重新輸入！！");
        Preconditions.checkArgument(maintenBasicField.getRequiredInput() != null, "請選擇 是否顯示必要輸入欄位！！");
        Preconditions.checkArgument(maintenBasicField.getRequiredIndex() != null, "請選擇 是否建立索引欄位！！");
        Preconditions.checkArgument(maintenBasicField.getShowFieldName() != null, "請選擇 是否顯示欄位名稱！！");
    }

    /**
     * 離開維護畫面
     */
    public void exitMaintenace() {
        log.debug("exitMaintenace..." + toString());
        this.setMaintenance(Boolean.FALSE);
        this.setMaintenanceHeader("檢視");
        this.updateViewPanel();
    }

    private void updateViewPanel() {
        displayController.update("viewPanel");
    }
}
