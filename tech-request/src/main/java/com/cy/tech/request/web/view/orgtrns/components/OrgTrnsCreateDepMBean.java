package com.cy.tech.request.web.view.orgtrns.components;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.orgtrns.OrgTrnsCreateDepService;
import com.cy.tech.request.logic.service.orgtrns.OrgTrnsService;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsBaseComponentPageVO;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsDtVO;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsWorkVerifyVO;
import com.cy.tech.request.logic.service.pmis.PmisService;
import com.cy.tech.request.vo.enums.RequireTransProgramType;
import com.cy.tech.request.web.pf.utils.ConfirmCallbackDialogController;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 回覆相關資料轉檔 MBean
 */
@Scope("view")
@Slf4j
@Controller
public class OrgTrnsCreateDepMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2597581682578739419L;
    // ========================================================================
    // 服務元件區
    // ========================================================================
    @Autowired
    private transient DisplayController displayController;
    @Autowired
    private transient ConfirmCallbackDialogController confirmCallbackDialogController;
    @Autowired
    private transient OrgTrnsCreateDepService orgTrnsCreateDepService;
    @Autowired
    private transient OrgTrnsService orgTrnsService;
    @Autowired
    private transient PmisService pmisService;
    @Autowired
    private transient EntityManager entityManager;

    // ========================================================================
    // 變數區
    // ========================================================================
    @Getter
    @Setter
    private OrgTrnsBaseComponentPageVO cpontVO = new OrgTrnsBaseComponentPageVO();

    /**
     * 待轉筆數資料
     */
    Map<String, Integer> waitTransCountMapByDataKey;

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * 全轉
     * 
     * @param selVerifyDtVO
     * @param trnsTypeStr
     */
    public void beforeAllTrans(OrgTrnsWorkVerifyVO selVerifyDtVO, String trnsTypeStr) {

        // ====================================
        // 檢核
        // ====================================
        if (selVerifyDtVO == null) {
            MessagesUtils.showError("找不到選擇的資料!");
            return;
        }

        RequireTransProgramType trnsType = null;
        try {
            trnsType = RequireTransProgramType.valueOf(trnsTypeStr);
        } catch (Exception e) {
        }

        if (trnsType == null) {
            MessagesUtils.showError("傳入類別錯誤! [" + trnsTypeStr + "]");
            return;
        }

        // ====================================
        // 初始化VO
        // ====================================
        this.cpontVO = new OrgTrnsBaseComponentPageVO();

        // 紀錄傳入參數
        this.cpontVO.setSelVerifyDtVO(selVerifyDtVO);
        this.cpontVO.setTrnsType(trnsType);

        // ====================================
        // 計算筆數
        // ====================================
        Integer count = 0;
        try {
            count = this.orgTrnsCreateDepService.count(selVerifyDtVO, trnsType);
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            String errorMessage = WkMessage.EXECTION + "(" + e.getMessage() + ")";
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);
            return;
        }

        if (count == 0) {
            MessagesUtils.showInfo("沒有需要轉檔的資料!");
            return;
        }

        // ====================================
        // 確認視窗
        // ====================================
        String confimInfo = "待轉筆數：【%s】，確定要轉檔嗎？";
        confimInfo = String.format(confimInfo, WkHtmlUtils.addBlueBlodClass(count + ""));

        if (RequireTransProgramType.REQUIRE.equals(trnsType)) {
            this.confirmCallbackDialogController.showConfimDialog(
                    confimInfo,
                    "",
                    () -> this.comformAllTransForRequire());
        } else {
            this.confirmCallbackDialogController.showConfimDialog(
                    confimInfo,
                    "",
                    () -> this.comformAllTrans());
        }

    }

    /**
     * 執行全部轉檔 for 子單
     */
    private void comformAllTrans() {
        // ====================================
        // 防呆
        // ====================================
        if (this.cpontVO == null) {
            MessagesUtils.showWarn(WkMessage.NEED_RELOAD + "(選擇資料已被清除)");
            return;
        }

        // ====================================
        // 轉檔
        // ====================================
        try {
            this.orgTrnsCreateDepService.processAllTrns(
                    this.cpontVO.getSelVerifyDtVO(),
                    this.cpontVO.getTrnsType(),
                    SecurityFacade.getUserSid());

        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "(" + e.getMessage() + ")";
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);
            return;
        }

    }

    /**
     * 執行全部轉檔 (for需求單)
     */
    private void comformAllTransForRequire() {

        // ====================================
        // 防呆
        // ====================================
        if (this.cpontVO == null) {
            MessagesUtils.showWarn(WkMessage.NEED_RELOAD + "(選擇資料已被清除)");
            return;
        }

        try {
            // ====================================
            // 查詢要轉檔的資料
            // ====================================
            List<String> requireSids = this.orgTrnsCreateDepService.findNeedTransCaseSids(
                    this.cpontVO.getSelVerifyDtVO(),
                    this.cpontVO.getTrnsType());

            if (WkStringUtils.isEmpty(requireSids)) {
                MessagesUtils.showInfo("已經沒有需要轉檔的資料");
                return;
            }

            // ====================================
            // 轉檔
            // ====================================
            this.processTransRequire(requireSids);

        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "(" + e.getMessage() + ")";
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);
            return;
        }
    }

    /**
     * 畫面功能:開啟操作視窗
     */
    public void openTrnsWindow(OrgTrnsWorkVerifyVO selVerifyDtVO, String trnsTypeStr) {

        // ====================================
        // 檢核
        // ====================================
        if (selVerifyDtVO == null) {
            MessagesUtils.showError("找不到選擇的資料!");
            return;
        }

        RequireTransProgramType trnsType = null;
        try {
            trnsType = RequireTransProgramType.valueOf(trnsTypeStr);
        } catch (Exception e) {
        }

        if (trnsType == null) {
            MessagesUtils.showError("傳入類別錯誤! [" + trnsTypeStr + "]");
            return;
        }

        // ====================================
        // 初始化VO
        // ====================================
        this.cpontVO = new OrgTrnsBaseComponentPageVO();

        // 紀錄傳入參數
        this.cpontVO.setSelVerifyDtVO(selVerifyDtVO);
        this.cpontVO.setTrnsType(trnsType);

        // ====================================
        // 查詢
        // ====================================
        boolean isSuccess = this.btnQuery();
        // 打開視窗
        if (isSuccess) {
            this.displayController.showPfWidgetVar("dlgCreateDepsTransform");
        }
    }

    /**
     * 畫面功能:查詢
     */
    public boolean btnQuery() {
        // ====================================
        // 頁面初始化
        // ====================================
        // 清空
        this.cpontVO.setSelectedDtVOList(null);
        // 更新頁面
        this.displayController.update("dlgCreateDepsTransform_id");
        this.displayController.clearDtatableFilters("dlgCreateDepsTransform_Datatable");

        if (this.getCpontVO() == null || this.getCpontVO().getSelVerifyDtVO() == null) {
            MessagesUtils.showError("資料已遺失, 請重新刷新頁面");
            return false;
        }

        // ====================================
        // 查詢
        // ====================================
        try {
            this.cpontVO.setShowDtVOList(
                    this.orgTrnsCreateDepService.query(
                            this.getCpontVO().getSelVerifyDtVO().getBeforeOrgSid(),
                            this.getCpontVO().getTrnsType()));
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return false;
        } catch (Exception e) {
            MessagesUtils.showError("系統錯誤!" + e.getMessage());
            log.error(e.getMessage(), e);
            return false;
        }

        return true;
    }

    /**
     * 畫面功能:轉換選擇資料
     */
    public void btnTrnsSelected() {

        // ====================================
        // 檢核
        // ====================================
        if (WkStringUtils.isEmpty(this.cpontVO.getSelectedDtVOList())) {
            MessagesUtils.showError("未選擇單據");
            return;
        }

        // ====================================
        // 轉檔
        // ====================================
        try {

            if (RequireTransProgramType.REQUIRE.equals(this.cpontVO.getTrnsType())) {
                // 需求單專用
                List<String> requireSids = this.cpontVO.getSelectedDtVOList().stream()
                        .map(OrgTrnsDtVO::getSid)
                        .collect(Collectors.toList());

                this.processTransRequire(requireSids);

            } else {
                // 其他子單
                int updateRow = this.orgTrnsCreateDepService.processDetailTrns(
                        this.cpontVO.getSelVerifyDtVO(),
                        SecurityFacade.getUserSid(),
                        this.cpontVO.getTrnsType(),
                        this.cpontVO.getSelectedDtVOList());
                MessagesUtils.showInfo("執行成功，共" + updateRow + "筆");
            }

            entityManager.clear();

        } catch (Exception e) {
            String msg = "執行失敗!" + e.getMessage();
            MessagesUtils.showError(msg);
            log.error(msg, e);
            return;
        }

        // ====================================
        // 重新查詢
        // ====================================
        this.btnQuery();
    }

    /**
     * 執行轉需求單
     * 
     * @param requireSids
     */
    private void processTransRequire(List<String> requireSids) {

        List<String> processResultInfos = Lists.newArrayList();

        Set<String> currRequireSids = requireSids.stream().collect(Collectors.toSet());

        // ====================================
        // 查詢有需要轉 PMIS 的資料
        // ====================================
        // 查詢
        Set<String> needTrnasToPmisRequireSids = this.pmisService.findNeedTrnasForOrgTrnas(currRequireSids);
        // 有資料時，將待轉單據分批 (需轉PMIS 和不轉PMIS)
        if (WkStringUtils.notEmpty(needTrnasToPmisRequireSids)) {
            currRequireSids = currRequireSids.stream()
                    .filter(sid -> !needTrnasToPmisRequireSids.contains(sid))
                    .collect(Collectors.toSet());
        }

        // ====================================
        // 轉一般 (不需轉 PMIS 整批 update)
        // ====================================
        if (WkStringUtils.notEmpty(currRequireSids)) {
            try {
                int transCount = this.orgTrnsCreateDepService.processTranBySids(
                        this.cpontVO.getSelVerifyDtVO(),
                        this.cpontVO.getTrnsType(),
                        currRequireSids,
                        SecurityFacade.getUserSid());

                processResultInfos.add("批次轉檔完成，共完成[" + transCount + "]筆");
            } catch (Exception e) {
                String message = "一般轉檔失敗! [" + e.getMessage() + "]";
                log.error(message, e);
                processResultInfos.add(message);
            }
        }

        // ====================================
        // 轉 PMIS (單筆轉)
        // ====================================
        if (WkStringUtils.notEmpty(needTrnasToPmisRequireSids)) {
            int withPmisSeccuss = 0;
            int withPmisfail = 0;

            for (String needTrnasToPmisRequireSid : needTrnasToPmisRequireSids) {
                try {
                    this.orgTrnsCreateDepService.processRequireTranBySidWithPMIS(
                            this.cpontVO.getSelVerifyDtVO(),
                            this.cpontVO.getTrnsType(),
                            needTrnasToPmisRequireSid,
                            SecurityFacade.getUserSid());

                    withPmisSeccuss++;

                } catch (Exception e) {
                    String message = "需同步PMIS類型資料轉檔失敗! [" + needTrnasToPmisRequireSid + "][" + e.getMessage() + "]";
                    log.error("\r\n" + message, e);
                    withPmisfail++;
                }
            }

            if (withPmisSeccuss > 0) {
                processResultInfos.add("需同步PMIS類型資料轉檔完成，共[" + withPmisSeccuss + "]筆");
            }

            if (withPmisfail > 0) {
                processResultInfos.add(
                        WkHtmlUtils.addRedBlodClass(
                                "需同步PMIS類型資料轉檔【失敗】，共[" + withPmisfail + "]筆"));
            }
        }

        MessagesUtils.showInfo(String.join("<br/>", processResultInfos));
    }

    // ========================================================================
    // 計算筆數
    // ========================================================================
    /**
     * 計算待轉筆數
     * 
     * @param orgTransMappingTos
     */
    public void executeCountAllWaitTrans(List<OrgTrnsWorkVerifyVO> orgTransMappingTos) {
        this.waitTransCountMapByDataKey = this.orgTrnsCreateDepService.countAllWaitTrans(orgTransMappingTos);
        //log.debug("\r\n" + WkJsonUtils.getInstance().toPettyJson(this.waitTransCountMapByDataKey));
    }

    /**
     * 取得待轉筆數字串
     * 
     * @param verifyVO
     * @param programTypeStr
     * @return
     */
    public String getWaitTransCount(OrgTrnsWorkVerifyVO verifyVO, String programTypeStr) {
        return this.orgTrnsService.prepareWaitTransCountInfo(
                this.waitTransCountMapByDataKey,
                verifyVO,
                programTypeStr);
    }

}
