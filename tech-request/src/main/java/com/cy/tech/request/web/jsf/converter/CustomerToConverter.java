/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.jsf.converter;

import com.cy.tech.request.logic.vo.CustomerTo;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.primefaces.component.selectcheckboxmenu.SelectCheckboxMenu;
import org.primefaces.component.selectmanymenu.SelectManyMenu;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 自訂物件 - 客戶資料 轉換器
 *
 * @author kasim
 */
@Component("customerToConverter")
public class CustomerToConverter implements Converter {

    @Autowired
    transient private WkStringUtils stringUtils;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (Strings.isNullOrEmpty(value) || !stringUtils.isNumeric(value)) {
            return null;
        }
        return new CustomerTo(Long.valueOf(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof CustomerTo) {
            if (component instanceof SelectOneMenu
                    || component instanceof SelectCheckboxMenu
                    || component instanceof SelectManyMenu) {
                return String.valueOf(((CustomerTo) value).getSid());
            }
        }
        return null;
    }
}
