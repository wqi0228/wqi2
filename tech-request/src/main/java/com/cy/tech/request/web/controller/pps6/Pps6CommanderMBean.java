package com.cy.tech.request.web.controller.pps6;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.bpm.rest.to.RoleTo;
import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.pps6.Pps6CommanderService;
import com.cy.tech.request.logic.service.pps6.vo.Pps6SignFlowTask;
import com.cy.tech.request.logic.service.pps6.vo.Pps6Task;
import com.cy.tech.request.vo.pps6.Pps6ModelerExecSettingVO;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.logic.WkUserAndOrgLogic;
import com.cy.work.viewcomponent.ssutreepicker.SingleSelectUserTreeCallback;
import com.cy.work.viewcomponent.ssutreepicker.SingleSelectUserTreePicker;
import com.cy.work.viewcomponent.ssutreepicker.SingleSelectUserTreePickerConfig;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu TestSingleSelectUserTree 測試頁 mbean
 */
@NoArgsConstructor
@Controller
@Scope("view")
@Slf4j
public class Pps6CommanderMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3251469684523604782L;
    // ========================================================================
    //
    // ========================================================================
    @Autowired
    private transient Pps6CommanderService pps6CommanderService;
    @Autowired
    private transient DisplayController displayController;
    @Autowired
    private transient WkUserAndOrgLogic wkUserAndOrgLogic;
    // ========================================================================
    // 元件ID
    // ========================================================================
    @Getter
    private final String SIGN_FLOW_DLG_WV = "SIGN_FLOW_DLG_WV";
    @Getter
    private final String SIGN_FLOW_DLG_CONTENT = "SIGN_FLOW_DLG_CONTENT";

    @Getter
    private final String DEP_DLG_WV = "DEP_DLG_WV";
    @Getter
    private final String DEP_DLG_CONTENT = "DEP_DLG_CONTENT";

    // ========================================================================
    // 頁面變數區
    // ========================================================================
    @Getter
    private SingleSelectUserTreePicker singleSelectUserTreePicker;
    @Getter
    private List<SelectItem> compItems = Lists.newArrayList();
    @Getter
    @Setter
    private Integer selectedCompSid = 2;
    @Getter
    private String selectedCompInfo = "未選擇";

    /**
     * 線上待簽單據
     */
    @Getter
    private List<Pps6Task> pps6TaskItems;

    /**
     * 線上可待簽單據
     */
    @Getter
    private List<Pps6Task> pps6AgentTaskItems;

    /**
     * 
     */
    @Getter
    private String selectUserInfo;
    @Getter
    private String selectUserID;
    @Getter
    private String selectUserUID;
    @Getter
    private String selectUserUserSid;
    @Getter
    private String selectUserUserOrgSid;

    /**
     * 使用者BPM角色
     */
    @Getter
    private List<RoleTo> userBPMRolesInfo;

    /**
     * 使用者層簽
     */
    @Getter
    private List<RoleTo> userSignFlow;

    /**
     * 使用者擔任部門主管
     */
    @Getter
    private String userDepManagerInfo;

    /**
     * 角色
     */
    @Getter
    private String userRolesInfo;

    /**
     * modeler 執行設定
     */
    @Getter
    private List<Pps6ModelerExecSettingVO> execSettingItems;

    /**
     * 流程簽核資料
     */
    @Getter
    List<Pps6SignFlowTask> pps6SignFlowTasks = Lists.newArrayList();

    /**
     * 需求案件單可閱部門 (含特殊可閱)
     */
    @Getter
    private String spcCanViewDeps;
    @Getter
    private String spcCanViewDepSidStr;
    /**
     * 需求案件單可閱部門 (含特殊可閱)
     */
    @Getter
    private String spcCanViewDepsUptoMinisterial;
    @Getter
    private String spcCanViewDepSidsUptoMinisterialStr;

    /**
     * 可使用選單
     */
    @Getter
    private String menuItemsInfo;
    /**
     * 自己以下部門
     */
    @Getter
    private String underSelfDeps;
    /**
     * 自己以下部門
     */
    @Getter
    private String underSelfDepNames;
    @Getter
    private String underSelfDepPersonCount;

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * 
     */
    @PostConstruct
    public void init() {

        this.prepareCompSelectItem();
        Org comp = WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId());
        if (comp != null) {
            selectedCompSid = comp.getSid();
        }

        // ====================================
        // 初始化元件
        // ====================================
        this.initSingleSelectUserTreePicker();

        for (SelectItem selectItem : compItems) {
            if (WkCommonUtils.compareByStr(selectItem.getValue(), this.selectedCompSid)) {
                this.selectedCompInfo = selectItem.getLabel();
            }
        }

    }

    private void initSingleSelectUserTreePicker() {
        SingleSelectUserTreePickerConfig config = new SingleSelectUserTreePickerConfig(
                // Sets.newHashSet(1928, 1941, 2041, 2035),
                WkOrgCache.getInstance().findAllChildSids(this.selectedCompSid));

        // log.debug("\r\n" + WkOrgUtils.findNameBySid(
        // WkOrgCache.getInstance().findAllChildSids(this.selectedCompSid), "、"));

        // 已選擇的人員所在的單位 orgSid
        // config.setSelectedDepSid(2041);
        // 已選擇的人員 userSid
        // config.setSelectedUserSid(3383);
        // call back 事件實做 (雙擊選項事件)
        config.setCallback(singleSelectUserTreeCallback);
        // 是否在單位、使用者前方顯示 sid
        config.setShowSid(true);
        // 是否顯示 user ID
        config.setShowUserID(true);
        config.setShowOrgID(true);

        config.setShowDepCount(true);

        config.setManagerMark("<i class='fa fa-star' aria-hidden='true'></i>");

        this.singleSelectUserTreePicker = new SingleSelectUserTreePicker(config);
    }

    // ========================================================================
    // 方法區 - 樹選單
    // ========================================================================
    /**
     * 
     */
    public void changeComp() {

        // ====================================
        // 初始化選擇樹
        // ====================================
        this.initSingleSelectUserTreePicker();

        for (SelectItem selectItem : compItems) {
            if (WkCommonUtils.compareByStr(selectItem.getValue(), this.selectedCompSid)) {
                this.selectedCompInfo = selectItem.getLabel();
                break;
            }
        }

        // ====================================
        // 初始化關鍵字查詢區
        // ====================================
        this.serachUserNameKeyword = "";
        this.serachUserItems = Lists.newArrayList();
        this.selectedUserItem = null;

        this.displayController.update("searchUserListPanel");
    }

    /**
     * @return
     */
    public Set<Integer> getAllDeps() {
        Org comp = WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId());
        return WkOrgCache.getInstance().findAllChildSids(comp.getSid());
    }

    /**
     * @return
     */
    public String getSelectedDepName() {
        Integer depSid = this.singleSelectUserTreePicker.getSelectedDepSid();
        if (depSid == null) {
            return "未選擇";
        }
        return WkOrgUtils.prepareBreadcrumbsByDepNameAndMakeup(depSid, OrgLevel.GROUPS, true, "-");
    }

    /**
     * @return
     */
    public String getSelectedUsrName() {
        Integer userSid = this.singleSelectUserTreePicker.getSelectedUserSid();
        if (userSid == null) {
            return "未選擇";
        }
        return WkUserUtils.prepareUserNameWithDep(userSid, OrgLevel.GROUPS, true, "-");
    }

    public void confirm() {
        MessagesUtils.showInfo("選取:" + getSelectedUsrName());
    }

    /**
     * 
     */
    private void prepareCompSelectItem() {
        // 取得所有公司列表
        List<Org> allOrgs = WkOrgCache.getInstance().findCompanies();
        Set<String> secAuthCompIDs = WkUserCache.getInstance().findSecAuth().stream()
                .map(secAuth -> secAuth.getCompId())
                .collect(Collectors.toSet());

        List<CompInfo> compInfos = Lists.newArrayList();

        for (Org comp : allOrgs) {
            if (!Activation.ACTIVE.equals(comp.getStatus())) {
                continue;
            }

            CompInfo compInfo = new CompInfo();
            compInfo.sid = comp.getSid();
            compInfo.id = comp.getId();
            compInfo.name = comp.getName();
            compInfo.authComp = secAuthCompIDs.contains(comp.getId());

            compInfo.employeeCnt = WkUserUtils.findAllUserByDepSid(comp.getSid()).stream()
                    .filter(user -> Activation.ACTIVE.equals(user.getStatus()))
                    .map(User::getSid)
                    .collect(Collectors.toSet())
                    .size();

            compInfo.showName = ""
                    + "[" + compInfo.id + "] "
                    + "【" + compInfo.name + "】"
                    + "(" + compInfo.employeeCnt + "人 , sid: " + compInfo.sid + ")";

            compInfos.add(compInfo);
        }

        // 排序
        Comparator<CompInfo> comparator = Comparator.comparing(CompInfo::isAuthComp, Comparator.reverseOrder());
        comparator = comparator.thenComparing(CompInfo::getEmployeeCnt, Comparator.reverseOrder());
        comparator = comparator.thenComparing(CompInfo::getName);
        compInfos = compInfos.stream().sorted(comparator).collect(Collectors.toList());

        this.compItems = Lists.newArrayList();
        for (CompInfo compInfo : compInfos) {
            this.compItems.add(new SelectItem(compInfo.getSid(), compInfo.getShowName()));
        }

    }

    @Getter
    public class CompInfo {
        public Integer sid;
        public String id;
        public String name;
        public long employeeCnt;
        public boolean authComp;
        public String showName;
    }

    protected final SingleSelectUserTreeCallback singleSelectUserTreeCallback = new SingleSelectUserTreeCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = 6890183536359962800L;

        /**
         * 雙擊項目時的動作
         * 
         * @return
         */
        @Override
        public void doubleClickItem() {

            // ====================================
            // 取得選擇的 user
            // ====================================
            Integer selectUserSid = singleSelectUserTreePicker.getSelectedUserSid();
            if (selectUserSid == null) {
                MessagesUtils.showInfo("未選擇 user !");
                return;
            }
            log.info("選取:" + getSelectedUsrName());

            // ====================================
            // 執行
            // ====================================
            prepareUserData(selectUserSid);

        }
    };

    // ========================================================================
    // 查詢選單
    // ========================================================================
    /**
     * 
     */
    @Getter
    @Setter
    private String serachUserNameKeyword = "";
    /**
     * 查詢結果
     */
    @Getter
    private List<WkItem> serachUserItems;
    @Getter
    @Setter
    private WkItem selectedUserItem;

    public void event_searchUserNameByKeyword() {

        this.serachUserItems = Lists.newArrayList();
        this.serachUserNameKeyword = WkStringUtils.safeTrim(this.serachUserNameKeyword);

        // 關鍵字為空
        if (WkStringUtils.isEmpty(this.serachUserNameKeyword)) {
            return;
        }

        // ====================================
        // 取得所有單位排序序號
        // ====================================
        Map<Integer, Integer> sortSeqMapByOrgSid = WkOrgCache.getInstance().findAllOrgOrderSeqMap();

        for (User user : WkUserCache.getInstance().findAll()) {

            if (!WkCommonUtils.compareByStr(user.getPrimaryOrg().getCompanySid(), this.selectedCompSid)) {
                continue;
            }

            // 忽略大小寫
            String compareUserName = WkStringUtils.safeTrim(user.getName()).toUpperCase();
            String compareUserID = WkStringUtils.safeTrim(user.getId()).toUpperCase();
            String compareKeyword = WkStringUtils.safeTrim(this.serachUserNameKeyword).toUpperCase();

            String userName = WkUserUtils.safetyGetName(user);
            if (!compareUserName.contains(compareKeyword)) {
                if (!compareUserID.contains(compareKeyword)) {
                    continue;
                }
                userName = "【" + userName + "】<span style='color:gray;opacity:0.5;'>" + user.getId() + "</span>";
            }

            WkItem wkItem = new WkItem(user.getSid() + "", userName, Activation.ACTIVE.equals(user.getStatus()));
            // 排序
            wkItem.getOtherInfo().put(
                    "orgSort",
                    sortSeqMapByOrgSid.get(user.getPrimaryOrg().getSid()) + "");

            // 顯示名稱
            String showName = WkJsoupUtils.getInstance().highlightKeyWordByClass(
                    userName,
                    this.serachUserNameKeyword,
                    "WS1-1-3");

            if (!wkItem.isActive()) {
                showName = WkHtmlUtils.addStrikethroughStyle(showName, "停用");
            }

            wkItem.setShowName(showName);

            String orgName = WkOrgUtils.prepareBreadcrumbsByDepNameAndMakeup(
                    user.getPrimaryOrg().getSid(),
                    OrgLevel.GROUPS,
                    true,
                    "-");

            wkItem.getOtherInfo().put(
                    "orgName",
                    orgName);

            this.serachUserItems.add(wkItem);
        }

        this.serachUserItems = this.serachUserItems.stream()
                .sorted(Comparator.comparing(WkItem::getShowName))
                .collect(Collectors.toList());
    }

    /**
     * 
     */
    public void event_selectSearchUser() {
        log.info("選取:" + selectedUserItem.getShowName());
        Integer selectUserSid = Integer.parseInt(this.selectedUserItem.getSid());
        this.prepareUserData(selectUserSid);
    }

    /**
     * 開啟水管圖
     * 
     * @param instanceID
     */
    public void event_openSignFlowDlg(String instanceID) {
        try {
            // 測試
            // instanceID = "A1X2eS2eDGQIaqC2qfO0";
            this.pps6SignFlowTasks = this.pps6CommanderService.prepareFormSignFlow(instanceID);
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        }

        this.displayController.update(SIGN_FLOW_DLG_CONTENT);
        this.displayController.showPfWidgetVar(SIGN_FLOW_DLG_WV);

    }

    /**
     * @param selectUserSid
     */
    private void prepareUserData(Integer selectUserSid) {

        this.execSettingItems = Lists.newArrayList();
        this.pps6TaskItems = Lists.newArrayList();
        this.userBPMRolesInfo = Lists.newArrayList();
        this.userDepManagerInfo = "";
        this.userRolesInfo = "";
        this.selectUserInfo = "";
        this.selectUserUID = "";
        this.selectUserUserSid = "";
        this.selectUserUserOrgSid = "";
        this.selectUserID = "";
        this.menuItemsInfo = "";
        this.underSelfDeps = "";
        this.underSelfDepNames = "";
        this.underSelfDepPersonCount = "";

        // ====================================
        // 查詢執行設定資訊
        // ====================================

        this.selectUserInfo = WkUserUtils.prepareUserNameWithDep(
                selectUserSid, OrgLevel.GROUPS, true, "-");
        if (!WkUserUtils.isActive(selectUserSid)) {
            this.selectUserInfo = WkHtmlUtils.addStrikethroughStyle(selectUserInfo, "停用");
        }

        User user = WkUserCache.getInstance().findBySid(selectUserSid);
        if (user != null) {
            this.selectUserUID = user.getUuid();
            this.selectUserUserSid = selectUserSid + "";
            this.selectUserUserOrgSid = user.getPrimaryOrg().getSid() + "";
            this.selectUserID = user.getId();
        }

        // ====================================
        // 查詢以下單位SID
        // ====================================
        Org selectUserOrg = user.getPrimaryOrg().toOrg();
        if (selectUserOrg != null) {

            List<Org> childOrgs = WkOrgCache.getInstance().findAllChild(user.getPrimaryOrg().getSid());
            childOrgs.add(selectUserOrg);

            List<Integer> childOrgSids = childOrgs.stream()
                    // .filter(org ->Activation.ACTIVE.equals(org.getStatus()))
                    .map(Org::getSid)
                    .collect(Collectors.toList());

            if (WkStringUtils.notEmpty(childOrgs)) {

                childOrgSids = WkOrgUtils.sortByOrgTree(childOrgSids);

                this.underSelfDeps = childOrgSids.stream().map(sid -> sid + "").collect(Collectors.joining(","));
                // this.underSelfDepNames = WkOrgUtils.prepareDepsNameByTreeStyle(
                // selectUserOrg.getCompany().getId(),
                // childOrgSids, 20);

                this.underSelfDepNames = WkOrgUtils.findNameBySid(childOrgSids, "<br/>", true);

            }

            this.underSelfDepPersonCount = WkUserCache.getInstance().findUserSidByOrgsWithManager(childOrgSids, Activation.ACTIVE).size()
                    + "人";
        }

        // ====================================
        // 使用者層簽模擬
        // ====================================
        try {
            this.userSignFlow = this.pps6CommanderService.prepareUserOrgSignFlow(selectUserSid);
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        }

        // ====================================
        // 查詢執行設定資訊
        // ====================================

        try {
            this.execSettingItems = this.pps6CommanderService.prepareRelationExecSetting(selectUserSid);
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        }

        // ====================================
        // 查詢待簽資訊
        // ====================================
        List<Pps6Task> currTaskItems = null;
        try {
            currTaskItems = this.pps6CommanderService.prepareCanSignTask(selectUserSid);
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        }

        // 排序
        Comparator<Pps6Task> comparator = Comparator.comparing(Pps6Task::getDefinitionName, Comparator.nullsLast(Comparator.naturalOrder()));
        comparator = comparator.thenComparing(Comparator.comparing(Pps6Task::getDocumentID, Comparator.nullsLast(Comparator.naturalOrder())));

        // 待簽
        this.pps6TaskItems = WkCommonUtils.safeStream(currTaskItems)
                .filter(pps6Task -> !pps6Task.isAgent())
                .sorted(comparator)
                .collect(Collectors.toList());

        // 可待簽單據
        try {
            this.pps6AgentTaskItems = WkCommonUtils.safeStream(currTaskItems)
                    .filter(pps6Task -> pps6Task.isAgent())
                    .sorted(comparator)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            MessagesUtils.showError("查詢可待簽單據失敗");
        }

        // ====================================
        // 查詢使用者BPM角色
        // ====================================
        try {
            this.userBPMRolesInfo = this.pps6CommanderService.prepareUserBPMRole(selectUserSid);
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        }

        // ====================================
        // 查詢使用者管理部門
        // ====================================
        try {
            this.userDepManagerInfo = this.pps6CommanderService.prepareUserManagerDeps(selectUserSid);
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        }

        // ====================================
        // 查詢使用者角色
        // ====================================
        try {
            this.userRolesInfo = this.pps6CommanderService.prepareUserRolesInfo(selectUserSid);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            MessagesUtils.showError("取得使用者角色失敗");
            return;
        }

        // ====================================
        // 查詢使用者可使用選單
        // ====================================
        try {
            this.menuItemsInfo = this.pps6CommanderService.prepareMenuItemsInfo(selectUserSid);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            MessagesUtils.showError("取得使用者可使用選單失敗");
            return;
        }

        // ====================================
        // 案件需求單特殊可閱
        // ====================================
        List<Integer> spcCanViewDepSids = Lists.newArrayList(wkUserAndOrgLogic.prepareCanViewDepSids(selectUserSid, true));
        this.spcCanViewDepSidStr = String.join(",", spcCanViewDepSids.stream().map(e -> e + "").collect(Collectors.toList()));
        this.spcCanViewDeps = WkOrgUtils.prepareDepsNameByTreeStyle(spcCanViewDepSids, 40);

        List<Integer> spcCanViewDepSidsUptoMinisterial = Lists.newArrayList(wkUserAndOrgLogic.prepareCanViewDepSidsBaseOnPanel(selectUserSid, true));
        this.spcCanViewDepSidsUptoMinisterialStr = String.join(",", spcCanViewDepSidsUptoMinisterial.stream().map(e -> e + "").collect(Collectors.toList()));
        this.spcCanViewDepsUptoMinisterial = WkOrgUtils.prepareDepsNameByTreeStyle(spcCanViewDepSidsUptoMinisterial, 40);

        // ====================================
        // 畫面更新
        // ====================================
        this.displayController.update(
                Lists.newArrayList(
                        "execSettingArea",
                        "pps6TaskArea",
                        "userBPMRolesInfoArea",
                        "userDepManagerInfoArea",
                        "selectUserInfoArea",
                        "userSignFlowArea",
                        "userRoleInfoArea",
                        "menuItemsInfoArea",
                        this.DEP_DLG_CONTENT));

        if (WkStringUtils.isEmpty(execSettingItems)
                && WkStringUtils.isEmpty(pps6TaskItems)
                && WkStringUtils.isEmpty(userBPMRolesInfo)
                && WkStringUtils.isEmpty(userDepManagerInfo)) {
            MessagesUtils.showInfo("[" + WkUserUtils.findNameBySid(selectUserSid) + "] 沒有關聯資料");
        }
    }
}
