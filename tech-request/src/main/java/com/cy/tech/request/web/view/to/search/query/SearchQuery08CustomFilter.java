/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.view.to.search.query;

import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.Search08QueryColumn;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.view.component.CategoryTreeComponent;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterArrayStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterDetailVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterVO;
import com.cy.tech.request.web.listener.CategoryTreeCallBack;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.listener.ReportCustomFilterCallback;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
public class SearchQuery08CustomFilter implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 8871940546237969919L;
    /** 登入者Sid */
	private Integer loginUserSid;
	@Getter
	private SearchQuery08 tempSearchQuery08;
	/** 該登入者在需求單,自訂查詢條件List */
	private List<ReportCustomFilterVO> reportCustomFilterVOs;
	/** 該登入者在需求單,挑選自訂查詢條件物件 */
	private ReportCustomFilterVO selReportCustomFilterVO;
	/** ReportCustomFilterLogicComponent */
	private ReportCustomFilterLogicComponent reportCustomFilterLogicComponent;
	/** 類別樹 Component */
	@Getter
	private CategoryTreeComponent categoryTreeComponent;
	/** MessageCallBack */
	private MessageCallBack messageCallBack;
	/** DisplayController */
	private DisplayController display;
	/** ReportCustomFilterCallback */
	private ReportCustomFilterCallback reportCustomFilterCallback;

	public SearchQuery08CustomFilter(Integer loginUserSid, Integer loginDepSid, Integer loginCompSid,
	        ReportCustomFilterLogicComponent reportCustomFilterLogicComponent, MessageCallBack messageCallBack,
	        DisplayController display,
	        ReportCustomFilterCallback reportCustomFilterCallback) {
		this.loginUserSid = loginUserSid;
		this.reportCustomFilterLogicComponent = reportCustomFilterLogicComponent;
		this.messageCallBack = messageCallBack;
		this.display = display;
		this.categoryTreeComponent = new CategoryTreeComponent(categoryTreeCallBack);
		this.reportCustomFilterCallback = reportCustomFilterCallback;
		this.tempSearchQuery08 = new SearchQuery08(ReportType.PROTOTYPE_CHECK);
	}

	public void loadDefaultSetting(SearchQuery08 searchQuery) {
		this.initDefault(searchQuery);
		reportCustomFilterVOs = reportCustomFilterLogicComponent.getReportCustomFilter(Search08QueryColumn.Search08Query, loginUserSid);
		if (reportCustomFilterVOs != null && !reportCustomFilterVOs.isEmpty()) {
			selReportCustomFilterVO = reportCustomFilterVOs.get(0);
			loadSettingData(searchQuery);
		} else {
			searchQuery.setSelectRequireStatusType(RequireStatusType.PROCESS);
			searchQuery.setStartDate(null);
			searchQuery.setEndDate(null);
		}
	}

	/** 載入挑選的自訂搜尋條件 */
	public void loadSettingData(SearchQuery08 searchQuery) {
		selReportCustomFilterVO.getReportCustomFilterDetailVOs().forEach(item -> {
			if (Search08QueryColumn.DemandType.equals(item.getSearchReportCustomEnum())
			        && item instanceof ReportCustomFilterStringVO) {
				settingDemandType(item, searchQuery);
			} else if (Search08QueryColumn.DemandProcess.equals(item.getSearchReportCustomEnum())
			        && item instanceof ReportCustomFilterStringVO) {
				settingDemandProcess(item, searchQuery);
			} else if (Search08QueryColumn.ReadStatus.equals(item.getSearchReportCustomEnum())
			        && item instanceof ReportCustomFilterStringVO) {
				settingReadStatus(item, searchQuery);
			} else if (Search08QueryColumn.DemandPerson.equals(item.getSearchReportCustomEnum())
			        && item instanceof ReportCustomFilterStringVO) {
				settingDemandPerson(item, searchQuery);
			} else if (Search08QueryColumn.CategoryCombo.equals(item.getSearchReportCustomEnum())
			        && item instanceof ReportCustomFilterArrayStringVO) {
				settingCategoryCombo(item, searchQuery);
			} else if (Search08QueryColumn.WaitReadReason.equals(item.getSearchReportCustomEnum())
			        && item instanceof ReportCustomFilterArrayStringVO) {
				settingReadReason(item, searchQuery);
			} else if (Search08QueryColumn.CheckStatus.equals(item.getSearchReportCustomEnum())
			        && item instanceof ReportCustomFilterArrayStringVO) {
				settingCheckStatus(item, searchQuery);
			} else if (Search08QueryColumn.DateIndex.equals(item.getSearchReportCustomEnum())
			        && item instanceof ReportCustomFilterStringVO) {
				settingDateIndex(item, searchQuery);
			} else if (Search08QueryColumn.SearchText.equals(item.getSearchReportCustomEnum())
			        && item instanceof ReportCustomFilterStringVO) {
				settingFuzzyText(item, searchQuery);
			} else if (Search08QueryColumn.VersonView.equals(item.getSearchReportCustomEnum())
			        && item instanceof ReportCustomFilterStringVO) {
				settingVersonView(item, searchQuery);
			} else if (Search08QueryColumn.SortType.equals(item.getSearchReportCustomEnum())
			        && item instanceof ReportCustomFilterStringVO) {
				settingSortType(item, searchQuery);
			}
		});
	}

	/**
	 * 塞入原型確認狀態
	 *
	 * @param reportCustomFilterDetailVO
	 */
	private void settingCheckStatus(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery08 searchQuery) {
		try {
			searchQuery.getPrototypeStatus().clear();
			ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
			if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
				searchQuery.getPrototypeStatus().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
			}
		} catch (Exception e) {
			log.error("settingCategoryCombo", e);
		}
	}

	/**
	 * 塞入待閱原因
	 *
	 * @param reportCustomFilterDetailVO
	 */
	private void settingReadReason(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery08 searchQuery) {
		try {
			searchQuery.getReadReason().clear();
			ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
			if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
				searchQuery.getReadReason().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
			}
		} catch (Exception e) {
			log.error("settingCategoryCombo", e);
		}
	}

	/**
	 * 塞入立單區間Type
	 *
	 * @param reportCustomFilterDetailVO
	 */
	private void settingDateIndex(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery08 searchQuery) {
		try {
			ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
			searchQuery.setDateTypeIndex(Integer.valueOf(reportCustomFilterStringVO.getValue()));

			switch (searchQuery.getDateTypeIndex()) {
			case 0:
				searchQuery.setStartDate(null);
				searchQuery.setEndDate(null);
				searchQuery.changeDateIntervalPreMonth();
				break;
			case 1:
				searchQuery.setStartDate(null);
				searchQuery.setEndDate(null);
				searchQuery.changeDateIntervalThisMonth();
				break;
			case 2:
				searchQuery.setStartDate(null);
				searchQuery.setEndDate(null);
				searchQuery.changeDateIntervalNextMonth();
				break;
			default:
				break;
			}
		} catch (Exception e) {
			log.error("settingFuzzyText", e);
		}
	}

	/**
	 * 塞入需求類別
	 *
	 * @param reportCustomFilterDetailVO
	 */
	private void settingCategoryCombo(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery08 searchQuery) {
		try {
			searchQuery.getSmallDataCateSids().clear();
			ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
			if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
				searchQuery.getSmallDataCateSids().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
			}
		} catch (Exception e) {
			log.error("settingCategoryCombo", e);
		}
	}

	/**
	 * 塞入排序方式
	 *
	 * @param reportCustomFilterDetailVO
	 */
	private void settingSortType(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery08 searchQuery) {
		try {
			ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
			searchQuery.setSortType(reportCustomFilterStringVO.getValue());
		} catch (Exception e) {
			log.error("settingSortType", e);
		}
	}

	/**
	 * 塞入版次顯示
	 *
	 * @param reportCustomFilterDetailVO
	 */
	private void settingVersonView(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery08 searchQuery) {
		try {
			ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
			if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
				searchQuery.setIsLatestEdition(Boolean.valueOf(reportCustomFilterStringVO.getValue()));
			} else {
				searchQuery.setIsLatestEdition(null);
			}
		} catch (Exception e) {
			log.error("settingFuzzyText", e);
		}
	}

	/**
	 * 塞入模糊搜尋
	 *
	 * @param reportCustomFilterDetailVO
	 */
	private void settingFuzzyText(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery08 searchQuery) {
		try {
			ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
			searchQuery.setFuzzyText(reportCustomFilterStringVO.getValue());
		} catch (Exception e) {
			log.error("settingFuzzyText", e);
		}
	}

	/**
	 * 塞入需求人員
	 *
	 * @param reportCustomFilterDetailVO
	 */
	private void settingDemandPerson(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery08 searchQuery) {
		try {
			ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
			searchQuery.setTrCreatedUserName(reportCustomFilterStringVO.getValue());
		} catch (Exception e) {
			log.error("trCreatedUserName", e);
		}
	}

	/**
	 * 塞入需求單位
	 *
	 * @param reportCustomFilterDetailVO
	 */
	private void settingReadStatus(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery08 searchQuery) {
		try {
			ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
			searchQuery.setSelectReadRecordType(reportCustomFilterStringVO.getValue());
		} catch (Exception e) {
			log.error("settingDepartment", e);
		}
	}

	/**
	 * 塞入需求製作進度
	 *
	 * @param reportCustomFilterDetailVO
	 */
	private void settingDemandProcess(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery08 searchQuery) {
		try {
			ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
			searchQuery.setSelectRequireStatusType(
			        RequireStatusType.safeValueOf(reportCustomFilterStringVO.getValue())
			);
		} catch (Exception e) {
			log.error("settingDemandType", e);
		}
	}

	/**
	 * 塞入需求類別
	 *
	 * @param reportCustomFilterDetailVO
	 */
	private void settingDemandType(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery08 searchQuery) {
		try {
			ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
			searchQuery.setSelectBigCategorySid(reportCustomFilterStringVO.getValue());
		} catch (Exception e) {
			log.error("settingDemandType", e);
		}
	}

	/** 儲存自訂搜尋條件 */
	public void saveReportCustomFilter() {
		try {
			ReportCustomFilterDetailVO demandType = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search08QueryColumn.DemandType,
			        (tempSearchQuery08.getSelectBigCategorySid() != null) ? tempSearchQuery08.getSelectBigCategorySid() : "");
			ReportCustomFilterDetailVO readStatus = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search08QueryColumn.ReadStatus,
			        tempSearchQuery08.getSelectReadRecordType());
			ReportCustomFilterDetailVO demandProcess = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(
			        Search08QueryColumn.DemandProcess,
			        tempSearchQuery08.getSelectRequireStatusType().name());
			ReportCustomFilterDetailVO waitReadReason = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search08QueryColumn.WaitReadReason,
			        tempSearchQuery08.getReadReason());
			ReportCustomFilterDetailVO checkStatus = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search08QueryColumn.CheckStatus,
			        tempSearchQuery08.getPrototypeStatus());
			ReportCustomFilterDetailVO searchText = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search08QueryColumn.SearchText,
			        tempSearchQuery08.getFuzzyText());
			ReportCustomFilterDetailVO versonView = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search08QueryColumn.VersonView,
			        (tempSearchQuery08.getIsLatestEdition() != null) ? String.valueOf(tempSearchQuery08.getIsLatestEdition()) : "");
			ReportCustomFilterDetailVO sortType = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search08QueryColumn.SortType,
			        tempSearchQuery08.getSortType());
			ReportCustomFilterDetailVO demandPerson = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search08QueryColumn.DemandPerson,
			        tempSearchQuery08.getTrCreatedUserName());
			ReportCustomFilterDetailVO categoryCombo = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search08QueryColumn.CategoryCombo,
			        tempSearchQuery08.getSmallDataCateSids());
			ReportCustomFilterDetailVO dateIndex = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search08QueryColumn.DateIndex,
			        (tempSearchQuery08.getDateTypeIndex() != null) ? String.valueOf(tempSearchQuery08.getDateTypeIndex()) : "5");
			List<ReportCustomFilterDetailVO> saveDetails = Lists.newArrayList();
			saveDetails.add(demandType);
			saveDetails.add(demandProcess);
			saveDetails.add(waitReadReason);
			saveDetails.add(checkStatus);
			saveDetails.add(readStatus);
			saveDetails.add(demandPerson);
			saveDetails.add(searchText);
			saveDetails.add(categoryCombo);
			saveDetails.add(dateIndex);
			saveDetails.add(versonView);
			saveDetails.add(sortType);
			reportCustomFilterLogicComponent.saveReportCustomFilter(Search08QueryColumn.Search08Query, loginUserSid,
			        (selReportCustomFilterVO != null) ? selReportCustomFilterVO.getIndex() : null, true, saveDetails);
			reportCustomFilterCallback.reloadDefault("");
			display.update("headerTitle");
			display.execute("doSearchData();");
			display.hidePfWidgetVar("dlgReportCustomFilter");
		} catch (Exception e) {
			this.messageCallBack.showMessage(e.getMessage());
			log.error("saveReportCustomFilter ERROR", e);
		}
	}

    public void initDefault(SearchQuery08 searchQuery) {
		searchQuery.clear();
	}

	/** 類別樹 Component CallBack */
	private final CategoryTreeCallBack categoryTreeCallBack = new CategoryTreeCallBack() {
		/**
         * 
         */
        private static final long serialVersionUID = 1513033337258136642L;

        @Override
		public void showMessage(String m) {
			messageCallBack.showMessage(m);
		}

		@Override
		public void confirmSelCate() {
			categoryTreeComponent.selCate();
			tempSearchQuery08.getBigDataCateSids().clear();
			tempSearchQuery08.getMiddleDataCateSids().clear();
			tempSearchQuery08.getSmallDataCateSids().clear();
			tempSearchQuery08.getBigDataCateSids().addAll(Lists.newArrayList(categoryTreeComponent.getBigDataCateSids()));
			tempSearchQuery08.getMiddleDataCateSids().addAll(Lists.newArrayList(categoryTreeComponent.getMiddleDataCateSids()));
			tempSearchQuery08.getSmallDataCateSids().addAll(Lists.newArrayList(categoryTreeComponent.getSmallDataCateSids()));
		}
	};

	/**
	 * 開啟 類別樹
	 */
	public void btnOpenCategoryTree() {
		try {
			categoryTreeComponent.init();
			categoryTreeComponent.selectedItem(tempSearchQuery08.getSmallDataCateSids());
			display.showPfWidgetVar("defaultDlgCate");
		} catch (Exception e) {
			log.error("btnOpenCategoryTree Error", e);
			messageCallBack.showMessage(e.getMessage());
		}
	}

	/** 清除立單區間Type */
	public void clearDateType() {
		if (3 == tempSearchQuery08.getDateTypeIndex()) {
			tempSearchQuery08.setDateTypeIndex(4);
		}
	}

}
