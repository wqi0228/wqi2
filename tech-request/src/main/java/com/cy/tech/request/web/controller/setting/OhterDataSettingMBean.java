package com.cy.tech.request.web.controller.setting;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;

import com.cy.tech.request.logic.service.othset.OthSetThemeService;
import com.cy.tech.request.vo.require.othset.OthSetTheme;
import com.cy.tech.request.web.pf.utils.MessagesUtils;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author aken_kao
 */
@Slf4j
@Controller
@Scope("view")
public class OhterDataSettingMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8764830098488117327L;

    @Autowired
    private OthSetThemeService othSetThemeService;
    
    @Getter
    private List<OthSetTheme> othSetThemeList;
    
    @PostConstruct
    public void init(){
        othSetThemeList = othSetThemeService.findAll();
    }
    
    public void onRowEdit(RowEditEvent event) {
        try {
            othSetThemeService.update((OthSetTheme)event.getObject());
        } catch(DataIntegrityViolationException e){
            MessagesUtils.showError("重複的theme，請重新設定！");
            init();
        } catch(Exception e){
            log.error(e.getMessage(), e);
        }
    }
    
    /**
     * 新增一筆空資料，待修改
     * 
     */
    public void addOthSetTheme() {
        try {
            if(othSetThemeService.findByTheme("") == null){
                othSetThemeService.addEmptyOthSetTheme();
                init();
            } else {
                MessagesUtils.showInfo("已新增一筆參數待設定！");
            }
        } catch(Exception e){
            log.error(e.getMessage(), e);
        }
    }
    
    /**
     * 刪除
     * @param sid
     */
    public void delete(String sid){
        othSetThemeService.delete(sid);
        init();
    }
}
