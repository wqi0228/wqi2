/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search.helper;

import com.cy.tech.request.logic.enumerate.ReportType;
import java.io.Serializable;
import java.util.Date;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.LocalDate;
import org.springframework.stereotype.Component;

/**
 * 支援xlsx匯出<BR/>
 * com.lapis.jsfexporter
 *
 * @author shaun
 */
@Component
public class Search26ExcelHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6198258702801182684L;

    /**
     * 匯出excel
     *
     * @param document
     * @param start
     * @param end
     * @param type
     */
    public void exportExcel(Object document, Date start, Date end, ReportType type) {
        exportExcel(document, start, end, type, Boolean.TRUE);
    }

    /**
     * 匯出excel
     *
     * @param document
     * @param start
     * @param end
     * @param type
     */
    public void exportExcel(Object document, Date start, Date end, ReportType type, Boolean showCreateDate) {
        XSSFWorkbook wb = (XSSFWorkbook) document;
        XSSFSheet sheet = wb.getSheetAt(0);
        int row = 5;
        int rowIndex = 0;
        if (!showCreateDate) {
            row = 4;
        }
        String[] strs = new String[row];
        strs[rowIndex++] = "報表產生日：" + new LocalDate().toString("yyyy/MM/dd");
        if (showCreateDate) {
            if (start != null) {
                strs[rowIndex++] = "立單日期：" + new LocalDate(start).toString("yyyy/MM/dd") + " ~ "
                          + new LocalDate(end).toString("yyyy/MM/dd");
            } else {
                strs[rowIndex++] = "立單日期： ∞" + " ~ "
                          + new LocalDate(end).toString("yyyy/MM/dd");
            }

        }
        strs[rowIndex++] = "報表類型：" + type.getReportName();

        for (int i = 0; i < row; i++) {
            int totalRows = sheet.getLastRowNum();
            sheet.shiftRows(i, totalRows, 1);
            XSSFRow headerRow = sheet.getRow(i);
            if (headerRow == null) {
                headerRow = sheet.createRow(i);
            }
            XSSFCell cell = headerRow.getCell(0);
            if (cell == null) {
                cell = headerRow.createCell(0);
            }
            cell.setCellValue(strs[i]);
        }
    }
}
