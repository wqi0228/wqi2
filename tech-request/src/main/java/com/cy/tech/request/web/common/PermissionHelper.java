/**
 * 
 */
package com.cy.tech.request.web.common;

import java.io.Serializable;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.vo.constants.ReqPermission;

/**
 * @author allen1214_wu
 */
@Service
public class PermissionHelper implements Serializable, InitializingBean {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -9046443162620866923L;

    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static PermissionHelper instance;

    /**
     * getInstance
     *
     * @return instance
     */
    public static PermissionHelper getInstance() { return instance; }

    /**
     * fix for fireBugs warning
     *
     * @param instance
     */
    private static void setInstance(PermissionHelper instance) { PermissionHelper.instance = instance; }

    /**
     * afterPropertiesSet
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * 是否有 QA 管理者角色
     * 
     * @return
     */
    public boolean isQAManager() { return SecurityFacade.hasPermission(ReqPermission.ROLE_QA_REVIEW_MGR); }
}
