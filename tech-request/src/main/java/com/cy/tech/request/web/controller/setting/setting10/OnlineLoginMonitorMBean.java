/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.setting.setting10;

import com.cy.tech.request.web.controller.to.OnlineUserInfoTo;
import com.cy.tech.request.web.controller.values.StorageLoginHelper;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 線上人員觀測
 *
 * @author shaun
 */
@NoArgsConstructor
@Controller
@Scope("view")
public class OnlineLoginMonitorMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5926437351091129709L;

    @Autowired
    transient private StorageLoginHelper userStorageHelper;

    @Getter
    @Setter
    private OnlineUserInfoTo sendTo;
    @Getter
    @Setter
    private String sendMsg;
    @Getter
    @Setter
    private String sendMsgMode;

    /** 線上成員資訊自動更新 */
    @Getter
    @Setter
    private Boolean autoUpdateSwtich = false;

    /**
     * 取得當前線上成員資訊
     *
     * @return
     */
    public List<OnlineUserInfoTo> showOnLineMemberInfo() {
        return userStorageHelper.findOnLineUser();
    }

}
