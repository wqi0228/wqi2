package com.cy.tech.request.web.controller.require;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.anew.manager.TrIssueMappTransManager;
import com.cy.tech.request.logic.service.ReqModifyService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.helper.RequireFlowHelper;
import com.cy.tech.request.logic.utils.DateUtils;
import com.cy.tech.request.logic.vo.UrlParamTo;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.helper.RequireHelper;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.ConfirmCallbackDialogController;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.customer.vo.WorkCustomer;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 需求單存檔控制
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class RequireSaveMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6154593808137354916L;
    @Autowired
    transient private TrIssueMappTransManager issueMappTransManager;
    @Autowired
    transient private RequireHelper requireHelper;
    @Autowired
    transient private RequireFlowHelper requireFlowHelper;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private ReqModifyService reqModifyService;
    @Autowired
    transient private URLService urlService;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private ConfirmCallbackDialogController confirmCallbackDialogController;

    @Getter
    private String updateFaildMsg;

    /**
     * 存檔時呼叫(新增)
     *
     * @param r01MBean
     * @param templateMBean
     * @throws IOException
     */
    public void save(Require01MBean r01MBean, RequireTemplateFieldMBean templateMBean) throws IOException {
        try {
            // ====================================
            // 檢查輸入欄位
            // ====================================
            this.requireHelper.checkInputInfo(
                    r01MBean.getRequire(),
                    r01MBean.getRequireCheckItemMBean(),
                    templateMBean.getTemplateItem(),
                    templateMBean.getComValueMap());

            // ====================================
            // 檢查trans物件是否已被建立相關資訊
            // ====================================
            // 是否為案件單轉需求單ON程式
            if (r01MBean.getIsIssueCreateMode()) {
                this.issueMappTransManager.checkInputInfo(r01MBean.getIssueCreateMappKeySid());
            }

            // ====================================
            // 特定小類需上傳附加檔案時且為新建檔模式或編輯模式時彈出提示，
            // 直到使用者點擊提示視窗【確定】後再進行存檔
            // ====================================
            if (requireService.isPopupAttachNotify(templateMBean.getMapping(), r01MBean.getRequire())) {
                confirmCallbackDialogController.showConfimDialog(
                        templateMBean.getMapping().getSmall().getName() + "類別，需上傳附檔！！",
                        Lists.newArrayList(
                                "require01_title_info_id",
                                "require_template_id"),
                        () -> this.checkNeedPopupOnpgCompleteNotify(r01MBean, templateMBean));
                return;
            }

            // 存檔
            this.checkNeedPopupOnpgCompleteNotify(r01MBean, templateMBean);

        } catch (UserMessageException ex) {
            MessagesUtils.show(ex);
            return;
        } catch (IllegalArgumentException e) {
            log.debug(e.getMessage());
            MessagesUtils.showInfo(e.getMessage());
            return;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            MessagesUtils.showError("系統錯誤, 請恰資訊人員!");
            return;
        }
    }

    public void checkNeedPopupOnpgCompleteNotify(Require01MBean r01MBean, RequireTemplateFieldMBean templateMBean) {
        if (r01MBean.getRequire().getMapping() == null) {
            r01MBean.getRequire().setMapping(templateMBean.getMapping());
        }
        // 取消原跳出提示訊息：當ON程式檢查完成後，是否將此需求單進度變更為已完成?
        // ps.現主單進度由需求進度表功能控制, 故取消此功能

        if (Strings.isNullOrEmpty(r01MBean.getRequire().getSid())) {
            this.saveInfo(r01MBean, templateMBean);
        } else {
            // 當外部流程有附加檔案時，會先呼叫update進行檢查是否需提示附加檔案dlg
            this.updateInfo(r01MBean, templateMBean);
        }
    }

    /**
     * 上傳附加檔案提示視窗【確定】鍵直接呼叫(新增)
     *
     * @param r01MBean
     * @param templateMBean
     * @throws IOException
     */

    public void saveInfo(
            Require01MBean r01MBean,
            RequireTemplateFieldMBean templateMBean) {

        Date execDate = new Date();
        try {
            // ====================================
            // 檢查
            // ====================================

            // 建立或編輯時檢查需求單輸入值
            this.requireHelper.checkInputInfo(
                    r01MBean.getRequire(),
                    r01MBean.getRequireCheckItemMBean(),
                    templateMBean.getTemplateItem(),
                    templateMBean.getComValueMap());

            // 為案件單轉ON時, 檢查trans物件是否已被建立相關資訊
            if (r01MBean.getIsIssueCreateMode()) {
                issueMappTransManager.checkInputInfo(r01MBean.getIssueCreateMappKeySid());
            }

            // ====================================
            // 欄位資料處理
            // ====================================
            Require editRequire = r01MBean.getRequire();

            // 重設定主題後綴 20170320
            templateMBean.resetThemeSuffixBySpecCom();

            // 案件單轉需求單
            String transSid = null;
            if (r01MBean.getIsIssueCreateMode()) {
                transSid = r01MBean.getIssueCreateMappKeySid();
            }

            // ====================================
            // 欄位資料處理
            // ====================================
            this.requireFlowHelper.processForCreateSave(
                    editRequire,
                    r01MBean.getRequireCheckItemMBean().getSelectedCheckItemTypes(),
                    templateMBean.getComValueMap(),
                    r01MBean.getReqAttachMBean().getSelectedAttachments(),
                    transSid,
                    null,
                    this.loginBean.getUser(),
                    execDate);

        } catch (UserMessageException ex) {
            MessagesUtils.show(ex);
            return;
        } catch (Exception e) {
            log.error(WkMessage.PROCESS_FAILED + e.getMessage(), e);
            this.saveErrorHandler(r01MBean, templateMBean, WkMessage.PROCESS_FAILED + e.getMessage());
            return;
        }

        // ====================================
        // 畫面控制
        // ====================================
        this.saveSucessHandler(r01MBean, templateMBean);
    }

    /**
     * 存檔時呼叫(更新)
     *
     * @param r01MBean
     * @param templateMBean
     * @throws IOException
     */
    public void update(Require01MBean r01MBean, RequireTemplateFieldMBean templateMBean) throws IOException {
        display.hidePfWidgetVar("reqEditUpdateFaildWv");
        updateFaildMsg = "";
        try {
            this.requireHelper.checkInputInfo(
                    r01MBean.getRequire(),
                    r01MBean.getRequireCheckItemMBean(),
                    templateMBean.getTemplateItem(),
                    templateMBean.getComValueMap());
        } catch (UserMessageException ex) {
            templateMBean.toggleDisabled(Boolean.FALSE);
            MessagesUtils.show(ex);
            return;
        }

        // 特定小類需上傳附加檔案時且為新建檔模式或編輯模式時彈出提示，
        // 直到使用者點擊提示視窗【確定】後再進行存檔
        if (requireService.isPopupAttachNotify(templateMBean.getMapping(), r01MBean.getRequire())) {

            confirmCallbackDialogController.showConfimDialog(
                    templateMBean.getMapping().getSmall().getName() + "類別，需上傳附檔！！",
                    Lists.newArrayList(
                            "require01_title_info_id",
                            "title_info_click_btn_id",
                            "require_template_id",
                            "viewPanelBottomInfoTabId"),
                    () -> this.updateInfo(r01MBean, templateMBean));
            return;
        }

        this.updateInfo(r01MBean, templateMBean);
    }

    /**
     * 上傳附加檔案提示視窗【確定】鍵直接呼叫(更新)
     *
     * @param r01MBean
     * @param templateMBean
     * @throws IOException
     */
    public void updateInfo(Require01MBean r01MBean, RequireTemplateFieldMBean templateMBean) {
        try {
            // 重設定主題後綴 20170320
            templateMBean.resetThemeSuffixBySpecCom();

            // 更新資料
            Require newRequrie = requireService.update(
                    loginBean.getUser(),
                    r01MBean.getRequire(),
                    r01MBean.getRequireCheckItemMBean().getSelectedCheckItemTypes(),
                    r01MBean.getBackupReq(),
                    templateMBean.getComValueMap());

            r01MBean.setRequire(newRequrie);

            r01MBean.getTitleBtnMBean().clear();
            r01MBean.getTraceMBean().clear();
        } catch (UserMessageException e) {
            log.error(e.getMessage() + "...userSId => " + loginBean.getUserSId(), e);
            updateFaildMsg = e.getMessage();
            display.update("@widgetVar(reqEditUpdateFaildWv)");
            templateMBean.toggleDisabled(Boolean.FALSE);
            r01MBean.reBuildeByUpdateFaild();
            r01MBean.startMaintenace(RequireStep.LOAD_IN_PAGE_EDIT.name(), r01MBean.isEditByAdmin());
            display.showPfWidgetVar("reqEditUpdateFaildWv");
            return;
        } catch (DataIntegrityViolationException e) {
            String errorMsg = "新增失敗，輸入圖檔符號可能導致資料異常！！";
            log.error(errorMsg + "...userSId => " + loginBean.getUserSId(), e);
            this.saveErrorHandler(r01MBean, templateMBean, errorMsg);
            return;
        } catch (Exception e) {
            String errorMsg = "系統異常導致存檔失敗！！";
            log.error(errorMsg + "...userSId => " + loginBean.getUserSId(), e);
            this.saveErrorHandler(r01MBean, templateMBean, errorMsg);
            return;
        }
        this.saveSucessHandler(r01MBean, templateMBean);
    }

    private void saveErrorHandler(Require01MBean r01MBean, RequireTemplateFieldMBean templateMBean, String errorMsg) {
        if (r01MBean.getSetp().equals(RequireStep.NEW_IN_PAGE)) {
            r01MBean.getRequire().setSid(null);
            r01MBean.getRequire().getCssContents().clear();
            r01MBean.getRequire().getIndex().clear();
            r01MBean.getRequire().setReqUnitSign(null);
        }
        if (r01MBean.getSetp().equals(RequireStep.LOAD_IN_PAGE_EDIT)) {
            r01MBean.recovery(templateMBean);
            templateMBean.toggleDisabled(Boolean.FALSE);
            r01MBean.reBuildeByUpdateFaild();
        }
        MessagesUtils.showError(errorMsg);
    }

    private void saveSucessHandler(Require01MBean r01MBean, RequireTemplateFieldMBean templateMBean) {
        // ====================================
        // RequireStep.NEW_IN_PAGE
        // ====================================
        if (r01MBean.getSetp().equals(RequireStep.NEW_IN_PAGE)) {
            // 因 iframe 操作時, update src 頁面有問題, 故改為直接導到新頁

            // 兜組 url
            UrlParamTo urlParamTo = urlService.createSimpleUrlTo(
                    SecurityFacade.getUserSid(),
                    r01MBean.getRequire().getRequireNo(),
                    1);

            String url = urlService.createLoacalURLLink(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlParamTo);

            this.display.execute("req01_replaceUrl('" + url + "')");
            return;
        }

        // ====================================
        // RequireStep.LOAD_IN_PAGE_EDIT
        // ====================================
        if (r01MBean.getSetp().equals(RequireStep.LOAD_IN_PAGE_EDIT)) {
            templateMBean.toggleDisabled(Boolean.TRUE);
            r01MBean.setSetp(RequireStep.LOAD_IN_PAGE);
            templateMBean.recoveryTemplate(r01MBean.getRequire());
            display.update(Lists.newArrayList(
                    "require01_title_info_id",
                    "title_info_click_btn_id",
                    "require_template_id",
                    "viewPanelBottomInfoTabId"));
        }
    }

    /**
     * 變更緊急度用
     *
     * @param r01MBean
     */
    public void updateByChangeUrgency(Require01MBean r01MBean) {
        try {
            reqModifyService.updateUrgency(r01MBean.getRequire(), r01MBean.getRequire().getUrgency(), loginBean.getUser());
            r01MBean.getTitleBtnMBean().clear();
        } catch (Exception e) {
            log.error(r01MBean.getRequire().getRequireNo() + ":" + loginBean.getUser().getName() + ":更新緊急度失敗..." + e.getMessage(), e);
            r01MBean.reBuildeByUpdateFaild();
            MessagesUtils.showError(e.getMessage());
        }
    }

    /**
     * 變更期望完成日
     *
     * @param r01MBean
     */
    public void updateByChangeHopeDate(Require01MBean r01MBean) {
        try {
            Preconditions.checkArgument(r01MBean.getRequire().getHopeDate() != null, "請輸入期望完成日期。");
        } catch (IllegalArgumentException e) {
            log.debug(e.getMessage(), e);
            MessagesUtils.showError(e.getMessage());
            return;
        }
        try {
            Require entity = requireService.findByReqObj(r01MBean.getRequire());
            // 若有異動才做修改
            if (!DateUtils.isSameDate(entity.getHopeDate(), r01MBean.getRequire().getHopeDate())) {
                Require newRequrie = requireService.saveByChangeHopeDate(loginBean.getUser(), r01MBean.getRequire());
                // 同步預計上線日至案件單
                this.issueMappTransManager.syncHopeDateToTechnicalCase(newRequrie.getSid(), SecurityFacade.getUserSid());
                r01MBean.setRequire(newRequrie);
                r01MBean.getTitleBtnMBean().clear();
                r01MBean.getTraceMBean().clear();
            }
        } catch (Exception e) {
            log.error(r01MBean.getRequire().getRequireNo() + ":" + loginBean.getUser().getName() + ":變更期望完成日失敗..." + e.getMessage(), e);
            r01MBean.reBuildeByUpdateFaild();
            MessagesUtils.showError(e.getMessage());
        }
    }

    /**
     * 修改廳主
     *
     * @param r01MBean
     */
    public void updateByChangeCustmoer(Require01MBean r01MBean) {
        try {
            Preconditions.checkNotNull(r01MBean.getRequire().getCustomer(), "請輸入廳主資料！！");
        } catch (NullPointerException | IllegalArgumentException e) {
            log.debug(e.getMessage(), e);
            WorkCustomer oldCustomer = requireService.findCustomerByRequire(r01MBean.getRequire());
            r01MBean.getRequire().setCustomer(oldCustomer);
            MessagesUtils.showError(e.getMessage());
            return;
        }
        try {
            if ("新包網資料提供".equals(r01MBean.getRequire().getMapping().getSmallName())) {
                r01MBean.getRequire().setAuthor(r01MBean.getRequire().getCustomer());
            }
            reqModifyService.updateChangeCustomer(r01MBean.getRequire(), r01MBean.getTemplateMBean().getComValueMap(), loginBean.getUser());
            r01MBean.getTitleBtnMBean().clear();
            r01MBean.getTraceMBean().clear();
            display.hidePfWidgetVar("change_custmoer_wv");
        } catch (Exception e) {
            log.error(r01MBean.getRequire().getRequireNo() + ":" + loginBean.getUser().getName() + ":修改廳主失敗..." + e.getMessage(), e);
            r01MBean.reBuildeByUpdateFaild();
            MessagesUtils.showError(e.getMessage());
        }
    }

    /**
     * 修改提出客戶
     *
     * @param r01MBean
     */
    public void updateByChangeAuthor(Require01MBean r01MBean) {
        try {
            reqModifyService.updateChangeAuthor(r01MBean.getRequire(), r01MBean.getTemplateMBean().getComValueMap(), loginBean.getUser());
            r01MBean.getTitleBtnMBean().clear();
            r01MBean.getTraceMBean().clear();
        } catch (Exception e) {
            log.error(r01MBean.getRequire().getRequireNo() + ":" + loginBean.getUser().getName() + ":修改提出客戶失敗..." + e.getMessage(), e);
            r01MBean.reBuildeByUpdateFaild();
            MessagesUtils.showError(e.getMessage());
        }
    }
}
