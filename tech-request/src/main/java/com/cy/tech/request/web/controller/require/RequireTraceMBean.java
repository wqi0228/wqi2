/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require;

import com.cy.tech.request.logic.service.RequireTraceService;
import com.cy.tech.request.logic.utils.DateUtils;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireTrace;
import com.cy.tech.request.web.anew.view.components.vo.MappCreateTransView;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.mapp.create.trans.logic.MappCreateTransManager;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.NoArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 需求單追蹤控制
 *
 * @author shaun
 */
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class RequireTraceMBean implements IRequireBottom, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7253625442040453001L;
    @Autowired
    transient private RequireTraceService service;
    @Autowired
    transient private DisplayController display;
    @Autowired
    private MappCreateTransManager mappCreateTransManager;
    @Getter
    private List<RequireTrace> traces;
    @Getter
    private List<MappCreateTransView> tracesByTransferFromTech;
    @Getter
    private String rollbackConfirmMsg;
    
    public void clear() {
        traces = null;
        tracesByTransferFromTech = null;
    }

    public List<RequireTrace> findRequireTrace(Require01MBean r01MBean) {
        if (traces == null) {
            this.initTabInfo(r01MBean);
        }
        return traces;
    }

    @Override
    public void initTabInfo(Require01MBean r01MBean) {
        Require require = r01MBean.getRequire();
        if (require == null || !require.getHasTrace()) {
            return;
        }
//        20160811 因鎖定此TAB造成新建檔時追蹤TAB出不來，所以暫時解除
//        初步判定應為載完單據後未執行bottomTabMBean.resetTab(r01MBean)造成
//        if (!r01MBean.getBottomTabMBean().currentTabByName(RequireBottomTabType.TRACE)) {
//            return;
//        }
        traces = service.findByRequire(require);
        if (traces != null) {
            service.sortTrace(traces);
        }
    }

    private void initTabInfo(Require require) {
        if (require == null || !require.getHasTrace()) {
            return;
        }
        traces = service.findByRequire(require);
        if (traces != null) {
            service.sortTrace(traces);
        }
    }

    public void openTab(Require01MBean r01MBean, RequireTrace trace) {
        if (trace == null) {
            return;
        }
        if (traces == null) {
            this.initTabInfo(trace.getRequire());
        }
        int idx = traces.indexOf(trace);
        if (idx != -1) {
            display.execute("PF('req_trace_tab_wv').select(" + idx + ");");
            for (int i = 0; i < traces.size(); i++) {
                if (i != idx) {
                    display.execute("PF('req_trace_tab_wv').unselect(" + i + ");");
                }
            }
        }
    }

    public int countRequireTraceByTransferFromTech(Require01MBean r01MBean) {
        return this.findRequireTraceByTransferFromTech(r01MBean).size();
    }
    
    public List<MappCreateTransView> findRequireTraceByTransferFromTech(Require01MBean r01MBean) {
        if (tracesByTransferFromTech == null) {
            if (r01MBean.getRequire() != null) {
                tracesByTransferFromTech = mappCreateTransManager.findMappOnpgByTargetSidAndTypeIn(
                        r01MBean.getRequire().getSid(), mappCreateTransManager.getTypesByTransReq()).stream()
                        .map(each -> {
                            return new MappCreateTransView(
                                    each.getSid(),
                                    DateUtils.YYYY_MM_DD_HH24_MI.print(each.getCreateDate().getTime()),
                                    WkOrgCache.getInstance().findNameBySid(each.getCreateDep()),
                                    WkUserUtils.findNameBySid(each.getCreateUser()),
                                    each.getOnpgTheme(),
                                    each.getOnpgNo()
                            );
                        })
                        .collect(Collectors.toList());
            }
        }
        return tracesByTransferFromTech;
    }
    
    
}
