/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.logic.component;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.TrAlertInboxService;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.ReadRecordType;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class ReadLogicComponent implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3521033773944541788L;
    private static ReadLogicComponent instance;
    @Autowired
    private RequireService requireService;
    @Autowired
    private TrAlertInboxService trAlertInboxService;

    public static ReadLogicComponent getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        ReadLogicComponent.instance = this;
    }

    public void readWorkInBox(String requestSid, Integer loginUserSid, Integer loginUesrDepSid) {
        try {
            trAlertInboxService.readWorkInbox(requestSid, loginUserSid, loginUesrDepSid);
        } catch (Exception e) {
            log.error("readWorkInBox", e);
        }
    }

    public List<User> getReadedUser(String requireSid) {
        Require ri = requireService.findByReqSid(requireSid);
        List<User> users = Lists.newArrayList();
        if (ri.getReadRecordGroup() != null && ri.getReadRecordGroup().getRecords() != null) {
            Set<String> keys = ri.getReadRecordGroup().getRecords().keySet();
            keys.forEach(item -> {
                try {
                    if (ri.getReadRecordGroup().getRecords().get(item) != null
                            && ReadRecordType.HAS_READ.equals(ri.getReadRecordGroup().getRecords().get(item).getType())) {
                        users.add(WkUserCache.getInstance().findBySid(Integer.valueOf(item)));
                    }
                } catch (Exception e) {
                    log.error("getReadedUser ERROR", e);
                }
            });
        }
        return users;
    }
}
