/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.component.searchquery;

import java.util.Date;
import java.util.List;

import com.cy.tech.request.logic.service.CategorySettingService;
import com.cy.tech.request.logic.utils.ToolsDate;
import com.cy.tech.request.logic.vo.SimpleDateFormatEnum;
import com.cy.tech.request.vo.category.BasicDataBigCategory;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.Search27QueryColumn;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.view.component.CategoryTreeComponent;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterArrayStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterDetailVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterVO;
import com.cy.tech.request.web.listener.CategoryTreeCallBack;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.listener.ReportCustomFilterCallback;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.enums.ReadRecordType;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 需求單查詢-自訂查詢條件物件
 *
 * @author kasim
 */
@Slf4j
public class SearchQuery27CustomFilter {

    /** 登入者Sid */
    private Integer loginUserSid;
    /** 該登入者在需求單,自訂查詢條件List */
    private List<ReportCustomFilterVO> reportCustomFilterVOs;
    /** 該登入者在需求單,挑選自訂查詢條件物件 */
    private ReportCustomFilterVO selReportCustomFilterVO;
    /** 需求類別 */
    @Setter
    @Getter
    private BasicDataBigCategory selectBigCategory;
    /** 單位挑選 */
    @Getter
    private List<String> requireDepts = Lists.newArrayList();
    /** 需求來源 立 | 分 | 轉 */
    @Setter
    @Getter
    private String reqSource;
    /** 需求製作進度 */
    @Setter
    @Getter
    private RequireStatusType selectRequireStatusType;
    /** 是否閱讀 */
    @Setter
    @Getter
    private ReadRecordType selectReadRecordType;
    /** 待閱原因 */
    @Setter
    @Getter
    private List<String> selectReqToBeReadType;
    /** 類別組合 */
    private List<String> bigDataCateSids = Lists.newArrayList();
    private List<String> middleDataCateSids = Lists.newArrayList();
    private List<String> smallDataCateSids = Lists.newArrayList();
    /** 立單區間(起) */
    @Setter
    @Getter
    private Date startDate;
    /** 立單區間(訖) */
    @Setter
    @Getter
    private Date endDate;
    /** 模糊搜尋 */
    @Setter
    @Getter
    private String fuzzyText;
    /** 需求人員 */
    @Setter
    @Getter
    private String trCreatedUserName;
    /** 時間切換的index */
    @Setter
    @Getter
    private String dateTypeIndex;
    /** ReportCustomFilterLogicComponent */
    private ReportCustomFilterLogicComponent reportCustomFilterLogicComponent;
    /** 類別樹 Component */
    @Getter
    private CategoryTreeComponent categoryTreeComponent;
    /** MessageCallBack */
    private MessageCallBack messageCallBack;
    /** DisplayController */
    private DisplayController display;
    /** CategorySettingService */
    private CategorySettingService categorySettingService;
    /** ReportCustomFilterCallback */
    private ReportCustomFilterCallback reportCustomFilterCallback;
    @Getter
    @Setter
    private Search27QueryColumn dateTimeType;
    
    public SearchQuery27CustomFilter(Integer loginUserSid, Integer loginDepSid,
            ReportCustomFilterLogicComponent reportCustomFilterLogicComponent, MessageCallBack messageCallBack,
            DisplayController display, CategorySettingService categorySettingService,ReportCustomFilterCallback reportCustomFilterCallback) {
        this.loginUserSid = loginUserSid;
        this.reportCustomFilterLogicComponent = reportCustomFilterLogicComponent;
        this.categorySettingService = categorySettingService;
        this.messageCallBack = messageCallBack;
        this.display = display;
        this.categoryTreeComponent = new CategoryTreeComponent(categoryTreeCallBack);
        this.reportCustomFilterCallback = reportCustomFilterCallback;
    }

    /** 初始化查詢條件預設值 */
    public void initDefault() {
        this.dateTypeIndex = "";
        this.bigDataCateSids = Lists.newArrayList();
        this.middleDataCateSids = Lists.newArrayList();
        this.smallDataCateSids = Lists.newArrayList();
        this.selectBigCategory = null;
        this.fuzzyText = "";
        this.trCreatedUserName = "";
        this.reqSource = "";
        this.selectRequireStatusType = null;
        this.selectReadRecordType = null;
        this.requireDepts = Lists.newArrayList();
        this.initToBeReadTypes();
    }

    /** 初始化待閱原因選單 */
    private void initToBeReadTypes() {
        selectReqToBeReadType = Lists.newArrayList();
        for (ReqToBeReadType each : ReqToBeReadType.values()) {
            selectReqToBeReadType.add(each.name());
        }
    }

    /** 載入自訂搜尋條件資料 */
    public void initData() {
        initDefault();
        reportCustomFilterVOs = reportCustomFilterLogicComponent.getReportCustomFilter(Search27QueryColumn.Search27Query, loginUserSid);
        if (reportCustomFilterVOs != null && !reportCustomFilterVOs.isEmpty()) {
            selReportCustomFilterVO = reportCustomFilterVOs.get(0);
            loadSettingData();
        }
    }

    /** 儲存自訂搜尋條件 */
    public void saveReportCustomFilter() {
        try {
            ReportCustomFilterDetailVO demandType = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search27QueryColumn.DemandType, (selectBigCategory != null) ? selectBigCategory.getSid() : "");
            //10/12與Anna討論不需要加入自訂義
            //ReportCustomFilterDetailVO department = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search27QueryColumn.Department, requireDepts);
            ReportCustomFilterDetailVO demandSource = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search27QueryColumn.DemandSource, reqSource);
            ReportCustomFilterDetailVO demandProcess = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search27QueryColumn.DemandProcess, (selectRequireStatusType != null) ? this.selectRequireStatusType.name() : "");
            ReportCustomFilterDetailVO readStatus = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search27QueryColumn.ReadStatus, (selectReadRecordType != null) ? selectReadRecordType.name() : "");
            ReportCustomFilterDetailVO waitReadReson = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search27QueryColumn.WaitReadReson, this.selectReqToBeReadType);
            ReportCustomFilterDetailVO demandPerson = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search27QueryColumn.DemandPerson, this.trCreatedUserName);
            ReportCustomFilterDetailVO categoryCombo = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search27QueryColumn.CategoryCombo, this.smallDataCateSids);
            ReportCustomFilterDetailVO dateIndex = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search27QueryColumn.DateIndex, this.dateTypeIndex);
            ReportCustomFilterDetailVO searchText = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search27QueryColumn.SearchText, this.fuzzyText);
            ReportCustomFilterDetailVO dateTimeType = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search27QueryColumn.DateTimeType, this.dateTimeType.name());
            
            List<ReportCustomFilterDetailVO> saveDetails = Lists.newArrayList();
            saveDetails.add(demandType);
            saveDetails.add(demandSource);
            saveDetails.add(demandProcess);
            saveDetails.add(readStatus);
            saveDetails.add(waitReadReson);
            saveDetails.add(demandPerson);
            saveDetails.add(categoryCombo);
            saveDetails.add(dateIndex);
            saveDetails.add(searchText);
            saveDetails.add(dateTimeType);
            
            reportCustomFilterLogicComponent.saveReportCustomFilter(Search27QueryColumn.Search27Query, loginUserSid, (selReportCustomFilterVO != null) ? selReportCustomFilterVO.getIndex() : null, true, saveDetails);
            reportCustomFilterCallback.reloadDefault("");
            display.update("headerTitle");
            display.execute("doSearchData()");
            display.hidePfWidgetVar("dlgReportCustomFilter");
        } catch (Exception e) {
            this.messageCallBack.showMessage(e.getMessage());
            log.error("saveReportCustomFilter ERROR", e);
        }
    }

    /** 載入挑選的自訂搜尋條件 */
    public void loadSettingData() {
        selReportCustomFilterVO.getReportCustomFilterDetailVOs().forEach(item -> {
            if (Search27QueryColumn.DemandType.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDemandType(item);
            } else if (Search27QueryColumn.Department.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingDepartment(item);
            } else if (Search27QueryColumn.DemandSource.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDemandSource(item);
            } else if (Search27QueryColumn.DemandProcess.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDemandProcess(item);
            } else if (Search27QueryColumn.ReadStatus.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingReadStatus(item);
            } else if (Search27QueryColumn.WaitReadReson.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingWaitReadReson(item);
            } else if (Search27QueryColumn.DemandPerson.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDemandPerson(item);
            } else if (Search27QueryColumn.CategoryCombo.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingCategoryCombo(item);
            } else if (Search27QueryColumn.StartDate.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingStartDate(item);
            } else if (Search27QueryColumn.EndDate.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingEndDate(item);
            } else if (Search27QueryColumn.DateIndex.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDateIndex(item);
            } else if (Search27QueryColumn.SearchText.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingFuzzyText(item);
            } else if(Search27QueryColumn.DateTimeType.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO){
                
            }
        });
    }

    /**
     * 塞入立單區間Type
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDateIndex(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            this.dateTypeIndex = reportCustomFilterStringVO.getValue();
        } catch (Exception e) {
            log.error("settingFuzzyText", e);
        }
    }

    /**
     * 塞入模糊搜尋
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingFuzzyText(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            this.fuzzyText = reportCustomFilterStringVO.getValue();
        } catch (Exception e) {
            log.error("settingFuzzyText", e);
        }
    }

    /**
     * 塞入立單區間-起始
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingEndDate(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            this.endDate = ToolsDate.transStringToDate(SimpleDateFormatEnum.SdfDate.getValue(), reportCustomFilterStringVO.getValue());
        } catch (Exception e) {
            log.error("settingEndDate", e);
        }
    }

    /**
     * 塞入立單區間-結束
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingStartDate(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            this.startDate = ToolsDate.transStringToDate(SimpleDateFormatEnum.SdfDate.getValue(), reportCustomFilterStringVO.getValue());
        } catch (Exception e) {
            log.error("settingStartDate", e);
        }
    }

    /**
     * 塞入需求類別
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingCategoryCombo(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            this.smallDataCateSids.clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                this.smallDataCateSids.addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingCategoryCombo", e);
        }
    }

    /**
     * 塞入需求人員
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandPerson(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            this.trCreatedUserName = reportCustomFilterStringVO.getValue();
        } catch (Exception e) {
            log.error("trCreatedUserName", e);
        }
    }

    /**
     * 塞入待閱原因
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingWaitReadReson(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            this.selectReqToBeReadType.clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                this.selectReqToBeReadType.addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingDepartment", e);
        }
    }

    /**
     * 塞入是否閱讀：
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingReadStatus(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                this.selectReadRecordType = ReadRecordType.valueOf(reportCustomFilterStringVO.getValue());
            }
        } catch (Exception e) {
            log.error("settingDemandSource", e);
        }
    }

    /**
     * 塞入需求製作進度
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandProcess(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                this.selectRequireStatusType = RequireStatusType.valueOf(reportCustomFilterStringVO.getValue());
            }
        } catch (Exception e) {
            log.error("settingDemandSource", e);
        }
    }

    /**
     * 塞入需求來源
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandSource(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            this.reqSource = reportCustomFilterStringVO.getValue();
        } catch (Exception e) {
            log.error("settingDemandSource", e);
        }
    }

    /**
     * 塞入單位挑選
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDepartment(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            this.requireDepts.clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                this.requireDepts.addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingDepartment", e);
        }
    }

    /**
     * 塞入需求類別
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandType(ReportCustomFilterDetailVO reportCustomFilterDetailVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                this.selectBigCategory = categorySettingService.findBigBySid(reportCustomFilterStringVO.getValue());
            }
        } catch (Exception e) {
            log.error("settingDemandType", e);
        }
    }

    /** 類別樹 Component CallBack */
    private final CategoryTreeCallBack categoryTreeCallBack = new CategoryTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 5762631741430533163L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelCate() {
            categoryTreeComponent.selCate();
            bigDataCateSids.clear();
            middleDataCateSids.clear();
            smallDataCateSids.clear();
            bigDataCateSids.addAll(Lists.newArrayList(categoryTreeComponent.getBigDataCateSids()));
            middleDataCateSids.addAll(Lists.newArrayList(categoryTreeComponent.getMiddleDataCateSids()));
            smallDataCateSids.addAll(Lists.newArrayList(categoryTreeComponent.getSmallDataCateSids()));
        }
    };

    /**
     * 開啟 類別樹
     */
    public void btnOpenCategoryTree() {
        try {
            categoryTreeComponent.init();
            categoryTreeComponent.selectedItem(this.smallDataCateSids);
            display.showPfWidgetVar("defaultDlgCate");
        } catch (Exception e) {
            log.error("btnOpenCategoryTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /** 修改需求來源 */
    public void changeReqSource() {
        requireDepts.clear();
    }

    /** 清除需求來源 */
    public void clearReqSource() {
        requireDepts.clear();
        reqSource = "";
    }

    /** 清除立單區間Type */
    public void clearDateType() {
        if ("4".equals(this.dateTypeIndex)) {
            this.dateTypeIndex = "";
        }
    }

}
