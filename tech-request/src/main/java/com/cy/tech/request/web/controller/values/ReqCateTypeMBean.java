package com.cy.tech.request.web.controller.values;

import com.cy.tech.request.logic.enumerate.BasicDataCategoryType;
import com.cy.tech.request.vo.enums.ReqCateType;
import com.cy.tech.request.web.pf.utils.CommonBean;
import java.io.Serializable;
import javax.faces.model.SelectItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 類別 - 提供頁面服務
 *
 * @author shaun
 */
@Controller
@Scope(WebApplicationContext.SCOPE_APPLICATION)
public class ReqCateTypeMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 231337693895372299L;
    @Autowired
    transient private CommonBean common;

    /**
     * 提供頁面 類別型態 SelectItem[]
     *
     * @return
     */
    public SelectItem[] getValues() {
        SelectItem[] items = new SelectItem[ReqCateType.values().length];
        int i = 0;
        for (BasicDataCategoryType each : BasicDataCategoryType.values()) {
            String label = common.get(each);
            items[i++] = new SelectItem(each, label);
        }
        return items;
    }

}
