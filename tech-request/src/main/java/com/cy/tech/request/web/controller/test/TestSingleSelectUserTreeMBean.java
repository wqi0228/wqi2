package com.cy.tech.request.web.controller.test;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.viewcomponent.ssutreepicker.SingleSelectUserTreeCallback;
import com.cy.work.viewcomponent.ssutreepicker.SingleSelectUserTreePicker;
import com.cy.work.viewcomponent.ssutreepicker.SingleSelectUserTreePickerConfig;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author allen1214_wu TestSingleSelectUserTree 測試頁 mbean
 */
@NoArgsConstructor
@Controller
@Scope("view")
public class TestSingleSelectUserTreeMBean implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -1703429450578041578L;
    @Getter
	private SingleSelectUserTreePicker singleSelectUserTreePicker;
	@Getter
	private List<SelectItem> compItems = Lists.newArrayList();
	@Getter
	@Setter
	private Integer selectedCompSid = 2;

	/**
	 * 
	 */
	@PostConstruct
	public void init() {

		this.prepareCompSelectItem();
		Org comp = WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId());
		if(comp!=null) {
			selectedCompSid = comp.getSid();
		}
		
		// ====================================
		// 初始化元件
		// ====================================
		SingleSelectUserTreePickerConfig config = new SingleSelectUserTreePickerConfig(
		        // Sets.newHashSet(1928, 1941, 2041, 2035),
		        WkOrgCache.getInstance().findAllChildSids(selectedCompSid));

		// 已選擇的人員所在的單位 orgSid
		config.setSelectedDepSid(2041);
		// 已選擇的人員 userSid
		config.setSelectedUserSid(3383);
		// call back 事件實做 (雙擊選項事件)
		config.setCallback(singleSelectUserTreeCallback);
		// 是否在單位、使用者前方顯示 sid
		config.setShowSid(true);

		this.singleSelectUserTreePicker = new SingleSelectUserTreePicker(config);
	}

	public void changeComp() {
		SingleSelectUserTreePickerConfig config = new SingleSelectUserTreePickerConfig(
		        // Sets.newHashSet(1928, 1941, 2041, 2035),
		        WkOrgCache.getInstance().findAllChildSids(selectedCompSid));
		// call back 事件實做 (雙擊選項事件)
		config.setCallback(singleSelectUserTreeCallback);
		// 是否在單位、使用者前方顯示 sid
		config.setShowSid(true);
		this.singleSelectUserTreePicker = new SingleSelectUserTreePicker(config);
	}

	public Set<Integer> getAllDeps() {
		return WkOrgCache.getInstance().findAllChildSids(SecurityFacade.getCompanySid());
	}

	public String getSelectedDepName() {
		Integer depSid = this.singleSelectUserTreePicker.getSelectedDepSid();
		if (depSid == null) {
			return "未選擇";
		}
		return WkOrgUtils.prepareBreadcrumbsByDepNameAndMakeup(depSid, OrgLevel.GROUPS, true, "-");
	}

	public String getSelectedUsrName() {
		Integer userSid = this.singleSelectUserTreePicker.getSelectedUserSid();
		if (userSid == null) {
			return "未選擇";
		}
		return WkUserUtils.prepareUserNameWithDep(userSid, OrgLevel.GROUPS, true, "-");
	}

	public void confirm() {
		MessagesUtils.showInfo("選取:" + getSelectedUsrName());
		//System.out.println("confirm");
	}

	private void prepareCompSelectItem() {
		// 取得所有公司業列表
		List<Org> allOrgs = WkOrgCache.getInstance().findCompanies();
		Set<String> secAuthCompIDs = WkUserCache.getInstance().findSecAuth().stream()
		        .map(secAuth -> secAuth.getCompId())
		        .collect(Collectors.toSet());

		List<CompInfo> compInfos = Lists.newArrayList();

		for (Org comp : allOrgs) {
			if (!Activation.ACTIVE.equals(comp.getStatus())) {
				continue;
			}

			CompInfo compInfo = new CompInfo();
			compInfo.sid = comp.getSid();
			compInfo.id = comp.getId();
			compInfo.name = comp.getName();
			compInfo.authComp = secAuthCompIDs.contains(comp.getId());
			// 人數
			compInfo.employeeCnt = WkUserUtils.findAllUserByDepSid(comp.getSid()).stream()
			        .filter(user -> Activation.ACTIVE.equals(user.getStatus()))
			        .count();

			compInfo.showName = "[" + compInfo.id + "] " + compInfo.name +  "  (" + compInfo.employeeCnt + "人) - " + + compInfo.sid ;

			compInfos.add(compInfo);
		}

		// 排序
		Comparator<CompInfo> comparator = Comparator.comparing(CompInfo::isAuthComp, Comparator.reverseOrder());
		comparator = comparator.thenComparing(CompInfo::getEmployeeCnt, Comparator.reverseOrder());
		comparator = comparator.thenComparing(CompInfo::getName);
		compInfos = compInfos.stream().sorted(comparator).collect(Collectors.toList());

		this.compItems = Lists.newArrayList();
		for (CompInfo compInfo : compInfos) {
			this.compItems.add(new SelectItem(compInfo.getSid(), compInfo.getShowName()));
		}

	}

	@Getter
	public class CompInfo {
		public Integer sid;
		public String id;
		public String name;
		public long employeeCnt;
		public boolean authComp;
		public String showName;
	}

	protected final SingleSelectUserTreeCallback singleSelectUserTreeCallback = new SingleSelectUserTreeCallback() {
		/**
         * 
         */
        private static final long serialVersionUID = -5807616322134947924L;

        /**
		 * 雙擊項目時的動作
		 * 
		 * @return
		 */
		@Override
		public void doubleClickItem() {
			MessagesUtils.showInfo("選取:" + getSelectedUsrName());
		}
	};

}
