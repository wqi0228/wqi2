package com.cy.tech.request.web.controller.test;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerCallback;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerComponent;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerConfig;
import com.cy.tech.request.web.controller.component.mipker.helper.MultItemPickerByOrgHelper;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.logic.SettingCustomGroupService;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Controller
@Scope("view")
@Slf4j
public class TestOrgMultiPickerMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1208583505119706016L;
    // ========================================================================
    // 服務區
    // ========================================================================
    @Autowired
    private MultItemPickerByOrgHelper multItemPickerByOrgHelper;
    @Autowired
    private SettingCustomGroupService settingCustomGroupService;

    // ========================================================================
    // 變數區
    // ========================================================================
    @Getter
    private MultItemPickerComponent orgPicker;

    /**
     * 
     */
    private List<Integer> selectedDeps;

    // ========================================================================
    // 方法區
    // ========================================================================
    @PostConstruct
    public void init() {
        this.selectedDeps = Lists.newArrayList(784);

        try {
            // 選擇器設定資料
            MultItemPickerConfig config = new MultItemPickerConfig();
            config.setTreeModePrefixName("單位");
            config.setContainFollowing(true);
            config.setEnableGroupMode(true);
            config.setItemComparator(MultItemPickerByOrgHelper.getInstance().parpareComparator());

            // 選擇器初始化
            this.orgPicker = new MultItemPickerComponent(config, orgPickerCallback);

        } catch (Exception e) {
            log.error("MultItemPickerComponent build error!", e);
            MessagesUtils.showError("建立單位選單失敗, 請恰系統人員!");
        }

        this.selectedDeps = Lists.newArrayList();
    }

    private final MultItemPickerCallback orgPickerCallback = new MultItemPickerCallback() {

        /**
         * 
         */
        private static final long serialVersionUID = 8285042035763503741L;

        /**
         * 準備所有的項目
         * 
         * @return
         * @throws Exception
         */
        @Override
        public List<WkItem> prepareAllItems() throws SystemDevelopException {
            // 取得所有單位
            return multItemPickerByOrgHelper.prepareAllOrgItems();
        }

        /**
         * 準備已選擇項目
         * 
         * @return
         */
        @Override
        public List<WkItem> prepareSelectedItems() throws SystemDevelopException {
            if (WkStringUtils.isEmpty(selectedDeps)) {
                return Lists.newArrayList();
            }

            List<WkItem> selectedItems = Lists.newArrayList();

            for (Integer depSid : selectedDeps) {
                Org dep = WkOrgCache.getInstance().findBySid(depSid);
                selectedItems.add(multItemPickerByOrgHelper.createDepItem(dep));
            }

            return selectedItems;
        }

        @Override
        public List<String> prepareDisableItemSids() throws SystemDevelopException {
            return Lists.newArrayList("784");
        }

        /**
         * @return
         * @throws SystemDevelopException
         */
        @Override
        public List<SelectItem> prepareGroupItems() throws SystemDevelopException {
            return settingCustomGroupService.findDepGroupForMultItemPicker(SecurityFacade.getUserSid());
        }

        /**
         * 依據傳入的群組值, 準備該群組的項目
         * 
         * @return 回傳群組項目
         * @throws SystemDevelopException 未實做時拋出
         */
        public List<String> prepareItemSidByGroupSid(String groupSid) throws SystemDevelopException {
            return settingCustomGroupService.findGroupDepsBySid(Long.parseLong(groupSid));
        }
    };
}
