/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.helper;

import com.cy.tech.request.vo.enums.SearchReportCustomEnum;
import com.cy.tech.request.vo.enums.SearchReportCustomType;
import com.cy.tech.request.vo.value.to.CustomFilterColumnTo;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterArrayIntegerVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterArrayStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterDetailVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterStringVO;
import java.io.Serializable;
import org.springframework.stereotype.Component;

/**
 *
 * @author brain0925_liao
 */
@Component
public class ReportCustomFilterHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1303287984280424112L;

    public ReportCustomFilterDetailVO transToReportCustomFilterDetailVO(CustomFilterColumnTo customFilterColumnTo, SearchReportCustomEnum selSearchReportCustomEnum) {
        SearchReportCustomEnum searchReportCustomEnum = selSearchReportCustomEnum.getSearchReportCustomEnumByName(customFilterColumnTo.getKey());
        if (searchReportCustomEnum == null) {
            return null;
        }
        if (SearchReportCustomType.STRING.name().equals(customFilterColumnTo.getSearchReportCustomType())) {
            return new ReportCustomFilterStringVO(searchReportCustomEnum, customFilterColumnTo.getValue());
        } else if (SearchReportCustomType.ARRAYINTEGER.name().equals(customFilterColumnTo.getSearchReportCustomType())) {
            return new ReportCustomFilterArrayIntegerVO(searchReportCustomEnum, customFilterColumnTo.getValue());
        } else if (SearchReportCustomType.ARRAYSTRING.name().equals(customFilterColumnTo.getSearchReportCustomType())) {
            return new ReportCustomFilterArrayStringVO(searchReportCustomEnum, customFilterColumnTo.getValue());
        }
        return null;
    }

}
