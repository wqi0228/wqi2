package com.cy.tech.request.web.controller.view.vo;

import javax.annotation.PostConstruct;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.vo.query.search.SearchQuery;
import com.cy.tech.request.vo.category.BasicDataBigCategory;
import com.cy.tech.request.web.controller.view.component.AdvancedSearchVO;
import com.cy.tech.request.web.controller.view.component.CategoryCombineVO;
import com.cy.tech.request.web.controller.view.component.DateIntervalVO;
import lombok.Getter;
import lombok.Setter;

@Scope("prototype")
@Component
public class SabaQueryVO extends SearchQuery {

    private static final long serialVersionUID = 5479516588541723907L;

    @Getter
    @Setter
    private BasicDataBigCategory catetory;
    @Getter
    @Setter
    private String searchText;
    @Getter
    @Setter
    private Boolean isNcsDone;

    @Getter
    @Setter
    /** 日期區間 */
    private DateIntervalVO dateIntervalVO = new DateIntervalVO();
    @Getter
    @Setter
    /** 類別組合 */
    private CategoryCombineVO categoryCombineVO = new CategoryCombineVO();

    @Autowired
    @Getter
    @Setter
    /** 進階搜尋 */
    private AdvancedSearchVO advancedVO;

    @PostConstruct
    public void init() {
        catetory = null;
        searchText = null;
        isNcsDone = false;
        dateIntervalVO.setDateTypeIndex(-1);
        dateIntervalVO.setStartDate(null);
        dateIntervalVO.setEndDate(new LocalDate().toDate());
        reportType = ReportType.SABA_LIST;

    }
}
