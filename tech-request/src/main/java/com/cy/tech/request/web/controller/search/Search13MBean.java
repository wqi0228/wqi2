/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.omnifaces.util.Faces;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.search.view.Search13View;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.send.test.SendTestService;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.tech.request.web.controller.component.mipker.helper.MultItemPickerByOrgHelper;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.search.helper.Search13Helper;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.view.component.searchquery.SearchQuery13;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkCommonCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.viewcomponent.treepker.TreePickerCallback;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 送測狀況一覽表
 *
 * @author jason_h
 */
@Controller
@Scope("view")
@Slf4j
public class Search13MBean extends BaseSearchMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2732857046489233350L;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private WkCommonCache wkCommonCache;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private SendTestService sendTestService;
    @Autowired
    transient private Require01MBean r01MBean;
    @Autowired
    transient private Search13Helper search13Helper;

    /** ReportCustomFilterLogicComponent */
    @Autowired
    transient private ReportCustomFilterLogicComponent reportCustomFilterLogicComponent;
    /** 所有的送測單 */
    @Getter
    @Setter
    private List<Search13View> queryItems;
    private List<Search13View> tempItems;

    /** 選擇的送測單 */
    @Getter
    @Setter
    private Search13View querySelection;

    /** 上下筆移動keeper */
    @Getter
    private Search13View queryKeeper;

    /** 切換模式 */
    @Getter
    private SwitchType switchType = SwitchType.CONTENT;

    /** 切換模式 - 全畫面狀態 */
    @Getter
    private SwitchType switchFullType = SwitchType.DETAIL;

    /** 在匯出的時候，某些內容需要隱藏 */
    @Getter
    private boolean hasDisplay = true;

    @Getter
    private final String dataTableId = "dtRequire";
    @Getter
    private SearchQuery13 queryComponent;
    @Setter
    @Getter
    /** dataTable filter */
    private String createDepName;
    @Getter
    /** dataTable filter items */
    private List<SelectItem> createDepNameItems;
    @Getter
    private boolean portalMode = false;
    @Getter
    @Setter
    /** 顯示自訂義按鈕裡的元件 */
    private boolean displayCustomBtn = true;

    @PostConstruct
    public void init() {
        portalMode = Faces.getRequestParameter("mode") != null;
        this.initComponent();
        if (portalMode) {
            displayCustomBtn = false;
            queryComponent.clear();
            queryComponent.getQueryVO().setTestStatus(Lists.newArrayList(WorkTestStatus.ROLL_BACK_TEST.name()));
            queryComponent.getQueryVO().setTrCreatedUserName(SecurityFacade.getUserId());
            queryComponent.getQueryVO().setStartDate(null);
            queryComponent.getQueryVO().setEndDate(null);
        } else {
            queryComponent.clear();
            queryComponent.loadSetting();
        }
        display.execute("doSearchData();");

    }

    private void initComponent() {
        queryComponent = new SearchQuery13(
                search13Helper,
                display,
                messageCallBack,
                reportCustomFilterLogicComponent,
                loginBean.getUserSId());
    }

    public void search() {

        tempItems = search13Helper.findWithQuery(
                loginBean.getCompanyId(),
                queryComponent.getQueryVO(),
                loginBean.getUser());

        this.createDepName = null;
        this.createDepNameItems = this.tempItems.stream()
                .map(each -> each.getCreateDepName())
                .collect(Collectors.toSet()).stream()
                .map(each -> new SelectItem(each, each))
                .collect(Collectors.toList());
        this.doChangeFilter();
    }

    /**
     * 進行dataTable filter
     */
    public void doChangeFilter() {
        this.queryItems = this.filterDataTable();
    }

    /**
     * filter
     *
     * @return
     */
    private List<Search13View> filterDataTable() {
        return tempItems.stream()
                .filter(each -> Strings.isNullOrEmpty(createDepName)
                        || createDepName.equals(each.getCreateDepName()))
                .collect(Collectors.toList());
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        queryComponent.clear();
        queryComponent.loadSetting();
        this.search();
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            this.moveScreenTab();
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (Search13View) event.getObject();
        this.changeRequireContent(this.queryKeeper);
        this.moveScreenTab();
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Search13View view) {
        Require require = requireService.findByReqNo(view.getRequireNo());
        view.setReadRecordType(ReadRecordType.HAS_READ);
        loadManager.reloadReqForm(require, loginBean.getUser(), RequireBottomTabType.SEND_TEST_INFO);
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Search13View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
        this.moveScreenTab();
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
        this.moveScreenTab();
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        helper.exportExcel(document, queryComponent.getQueryVO().getStartDate(), queryComponent.getQueryVO().getEndDate(), queryComponent.getReportType());
        hasDisplay = true;
    }

    /**
     * 隱藏部分column裡的內容
     */
    public void hideColumnContent() {
        hasDisplay = false;
    }

    /**
     * 送測單位
     * 
     * @param view
     * @return
     */
    public String getSendTestDepStr(Search13View view) {

        // cache 中取回
        String cacheName = "Search13View-getSendTestDepStr";
        List<String> keys = Lists.newArrayList(view.getTestinfoNo());
        String orgNames = wkCommonCache.getCache(cacheName, keys, 5);
        if (orgNames != null) {
            return orgNames;
        }

        // 組單位名稱
        List<String> orgSids = Optional.ofNullable(view.getSendTestDep())
                .map(send -> send.getValue())
                .orElse(Lists.newArrayList());

        orgNames = WkOrgUtils.findNameBySidStrs(orgSids, "、");

        // put 快取
        wkCommonCache.putCache(cacheName, keys, orgNames);

        return orgNames;
    }

    public void moveScreenTab() {
        this.r01MBean.getBottomTabMBean().resetTabIdx(this.r01MBean);
        this.r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.SEND_TEST_INFO);

        List<String> sids = sendTestService.findSidsBySourceSid(this.r01MBean.getRequire().getSid());
        String indicateSid = queryKeeper.getSid();
        if (sids.indexOf(indicateSid) != -1) {
            display.execute("PF('st_acc_panel_layer_zero').select(" + sids.indexOf(indicateSid) + ");");
            for (int i = 0; i < sids.size(); i++) {
                if (i != sids.indexOf(indicateSid)) {
                    display.execute("PF('st_acc_panel_layer_zero').unselect(" + i + ");");
                }
            }
        }
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search13View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.removeClassByTextBold(dtId, pageCount);
        this.transformHasRead();
        this.checkHelfScreen();
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            display.update("headerTitle");
            display.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Search13View to) {
        querySelection = to;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 去除粗體Class
     *
     * @param dtId
     * @param pageCount
     */
    private void removeClassByTextBold(String dtId, String pageCount) {
        display.execute("removeClassByTextBold('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
        display.execute("changeAlreadyRead('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 變更已閱讀
     */
    private void transformHasRead() {
        querySelection.setReadRecordType(ReadRecordType.HAS_READ);
        queryKeeper = querySelection;
        queryItems.set(queryItems.indexOf(querySelection), querySelection);
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
        upDownBean.resetTabInfo(RequireBottomTabType.SEND_TEST_INFO, queryKeeper.getSid());
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param mbean
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param mbean
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.transformHasRead();
        this.removeClassByTextBold(dtId, pageCount);
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    /** 訊息呼叫 */
    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 6779518995695281073L;

        @Override
        public void showMessage(String m) {
            MessagesUtils.showError(m);
        }
    };

    /**
     * 畫面用全部製作進度查詢
     *
     * @return
     */
    public SelectItem[] getReqStatusItems() { return search13Helper.getReqStatusItems(); }

    /**
     * @return
     */
    public SelectItem[] getWorkTestStatusItems() { return search13Helper.getWorkTestStatusItems(); }

    // ========================================================================
    // 樹狀選單
    // ========================================================================
    /** 操作類型 */
    private TreePickerType currTreePickerType = null;

    private enum TreePickerType {

        CREATE_DEP("填單單位"),
        CREATE_DEP_IN_CSTOM_CONDITION("填單單位-自訂預設搜尋條件"),
        NOTIFY_DEP("通知單位"),
        NOTIFY_DEP_IN_CSTOM_CONDITION("通知單位-自訂預設搜尋條件"),
        CATEGORY("類別組合"),
        CATEGORY_IN_CSTOM_CONDITION("類別組合-自訂預設搜尋條件"),
        ;

        /** 說明 */
        @Getter
        private final String descr;

        private TreePickerType(String descr) {
            this.descr = descr;
        }

        public static TreePickerType safeValueOf(String str) {
            if (WkStringUtils.notEmpty(str)) {
                for (TreePickerType item : TreePickerType.values()) {
                    if (item.name().equals(str)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * @param typeStr
     */
    public void searchTreepicker_openDialog(String typeStr) {
        // ====================================
        // 取得此次開啟的選單類別
        // ====================================
        this.currTreePickerType = TreePickerType.safeValueOf(typeStr);
        if (this.currTreePickerType == null) {
            MessagesUtils.showError("開發時期錯誤!未定義的選單類別:[" + typeStr + "]");
            return;
        }

        // ====================================
        // 初始化選單
        // ====================================
        this.treepicker_openDialog(this.currTreePickerType.getDescr(), treePickerCallback);
    }

    /**
     * 樹選單 - 確認
     */
    @Override
    public void treePicker_confirm() {

        if (this.currTreePickerType == null) {
            log.error("currTreePickerType 為空!");
            MessagesUtils.showError(WkMessage.NEED_RELOAD);
            return;
        }

        // 取得選單中選擇的項目
        List<String> selectedItemSids = Lists.newArrayList(this.treePicker.getSelectedItemSids());

        switch (currTreePickerType) {
        case CREATE_DEP: // 填單單位
            this.queryComponent.getQueryVO().setRequireDepts(selectedItemSids);
            break;
        case CREATE_DEP_IN_CSTOM_CONDITION: // 填單單位-自訂預設搜尋條件
            this.queryComponent.getSearch13QueryVOFilter().setRequireDepts(selectedItemSids);
            break;
        case NOTIFY_DEP: // 送測單位
            this.queryComponent.getQueryVO().setNoticeDepts(selectedItemSids);
            break;
        case NOTIFY_DEP_IN_CSTOM_CONDITION: // 送測單位-自訂預設搜尋條件
            this.queryComponent.getSearch13QueryVOFilter().setNoticeDepts(selectedItemSids);
            break;
        case CATEGORY: // 需求類別
            this.queryComponent.getQueryVO().setSmallDataCateSids(selectedItemSids);
            break;
        case CATEGORY_IN_CSTOM_CONDITION:
            this.queryComponent.getSearch13QueryVOFilter().setSmallDataCateSids(selectedItemSids);
            break;

        default:
            log.error("開發時期錯誤!未定義的選單類別:[" + currTreePickerType.name() + "]");
            MessagesUtils.showError(WkMessage.NEED_RELOAD);
            return;
        }

        // 關閉視窗
        this.displayController.hidePfWidgetVar(this.TREE_PICKER_DLG_NAME);
    }

    /**
     * 樹選單 - callback 事件
     */
    private final TreePickerCallback treePickerCallback = new TreePickerCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = -1230591973323428848L;

        @Override
        public List<WkItem> prepareAllItems() throws Exception {
            if (currTreePickerType == null) {
                throw new Exception("currTreePickerType 為空!");
            }

            switch (currTreePickerType) {
            case CREATE_DEP: // 填單單位
            case CREATE_DEP_IN_CSTOM_CONDITION: // 填單單位-自訂預設搜尋條件

            case NOTIFY_DEP: // 通知單位
            case NOTIFY_DEP_IN_CSTOM_CONDITION: // 通知單位-自訂預設搜尋條件

                // 1.可選項目為部門上升到部+可檢視單位
                // 2.GM 可選擇全部單位
                if (workBackendParamHelper.isGM(SecurityFacade.getUserSid())) {

                    return MultItemPickerByOrgHelper.getInstance().prepareAllOrgItems();
                } else {
                    return prepareAllCanViewDepItemsByBaseDep();
                }

            case CATEGORY: // 需求類別
            case CATEGORY_IN_CSTOM_CONDITION:
                return prepareAllCategoryItems();

            default:
                break;
            }

            throw new Exception("開發時期錯誤!未定義的選單類別:[" + currTreePickerType.name() + "]");
        }

        /**
         * 準備已選擇項目
         * 
         * @return
         */
        @Override
        public List<String> prepareSelectedItemSids() throws Exception {
            if (currTreePickerType == null) {
                throw new Exception("currTreePickerType 為空!");
            }

            switch (currTreePickerType) {
            case CREATE_DEP: // 填單單位
                return queryComponent.getQueryVO().getRequireDepts();

            case CREATE_DEP_IN_CSTOM_CONDITION: // 填單單位-自訂預設搜尋條件
                return queryComponent.getSearch13QueryVOFilter().getRequireDepts();

            case NOTIFY_DEP: // 通知單位
                return queryComponent.getQueryVO().getNoticeDepts();

            case NOTIFY_DEP_IN_CSTOM_CONDITION: // 通知單位-自訂預設搜尋條件
                return queryComponent.getSearch13QueryVOFilter().getNoticeDepts();

            case CATEGORY: // 需求類別
                // 不用考慮大、中類
                return queryComponent.getQueryVO().getSmallDataCateSids();

            case CATEGORY_IN_CSTOM_CONDITION:
                // 不用考慮大、中類
                return queryComponent.getSearch13QueryVOFilter().getSmallDataCateSids();

            default:
                break;
            }

            throw new Exception("開發時期錯誤!未定義的選單類別:[" + currTreePickerType.name() + "]");
        }

        /**
         * 準備鎖定項目
         * 
         * @return
         */
        @Override
        public List<String> prepareDisableItemSids() throws Exception {
            return Lists.newArrayList();
        }
    };

}
