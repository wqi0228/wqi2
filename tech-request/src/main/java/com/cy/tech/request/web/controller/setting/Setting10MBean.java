/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. 
 */
package com.cy.tech.request.web.controller.setting;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.util.StopWatch;

import com.cy.security.logic.SecurityCacheHelper;
import com.cy.security.utils.CacheConfigUtil;
import com.cy.security.utils.SecurityFacade;
import com.cy.system.rest.client.util.SystemCacheUtils;
import com.cy.tech.request.client.ReqRestClient;
import com.cy.tech.request.logic.service.bpm.ReqBpmUpdateService;
import com.cy.tech.request.logic.utils.ProjectCustomCacheClearUtil;
import com.cy.tech.request.web.controller.logic.component.WorkInboxLogicComponents;
import com.cy.tech.request.web.controller.setting.setting10.MemMonitorMBean;
import com.cy.tech.request.web.controller.setting.setting10.OnlineLoginMonitorMBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.client.syncgm.SyncGMClient;
import com.cy.work.common.cache.WkCommonCache;
import com.cy.work.common.utils.WkJsonUtils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 系統維護
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope("view")
public class Setting10MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6643055511812217865L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private MemMonitorMBean sysinfoMBean;
    @Autowired
    transient private OnlineLoginMonitorMBean onlineinfoMBean;
    @Autowired
    transient private ReqBpmUpdateService bpmUpdateService;
    @Autowired
    transient private WorkInboxLogicComponents workInboxLogicComponents;
    @Autowired
    transient private CacheConfigUtil cacheConfigUtil;
    @Autowired
    transient private com.cy.tech.request.logic.config.ReqEhCacheHelper reqEhCacheHelper;
    @Autowired
    transient private ReqRestClient reqRestClient;
    @Autowired
    transient private SyncGMClient syncGMClient;
    @Autowired
    transient private ProjectCustomCacheClearUtil projectCustomCacheClearUtil;

    /** 資訊幕訊息 */
    private final StringBuilder infoScreenMsg = new StringBuilder();
    /** 重置流程的 BPM ID */
    @Getter
    @Setter
    private String resetBpmId;

    public void addInfoMsg(String msg) {
        infoScreenMsg.append(msg);
    }

    public String getInfoScreenMsg() { return infoScreenMsg.toString(); }

    public void appendInfoScreenMsg(String message) {
        this.infoScreenMsg.append(message);
    }

    public void clearInfoScreenMsg() {
        infoScreenMsg.setLength(0);
    }

    /**
     * 清除系統所有快取
     */
    public void clearAllCache() {
        this.clearCacheBefor();
        StopWatch sw = new StopWatch();
        sw.start();
        // ====================================
        //
        // ====================================
        infoScreenMsg.append("開始執行清除快取...<br/>");
        infoScreenMsg.append("開始執行清除公用快取(system-rest security)...<br/>");
        SecurityCacheHelper.getInstance().doClearSecurityCache();

        // ====================================
        // 系統自定義 cache
        // ====================================
        projectCustomCacheClearUtil.doClear();
        for (String execResult : projectCustomCacheClearUtil.getLastExecResult()) {
            infoScreenMsg.append(execResult + "...<br/>");
        }

        log.info("所有快取清除完畢");
        sw.stop();
        infoScreenMsg.append(sw.prettyPrint()).append("<BR/>");
        infoScreenMsg.append("清除快取完成。<BR/>");
        infoScreenMsg.append("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░<BR/>");
    }

    /**
     * 清除REST所有快取
     */
    public void clearRestAllCache() {
        this.clearCacheBefor();
        StopWatch sw = new StopWatch();
        sw.start();
        infoScreenMsg.append("開始執行清除REST快取...<br/>");
        reqRestClient.clearAllCache();
        sw.stop();
        infoScreenMsg.append(sw.prettyPrint()).append("<BR/>");
        infoScreenMsg.append("清除快取完成。<BR/>");
        infoScreenMsg.append("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░<BR/>");
    }

    /**
     * 執行清除一般快取
     */
    @Deprecated // by REQ-1555
    public void clearNormalCache() {
        this.clearCacheBefor();
        infoScreenMsg.append("開始執行清除快取...<BR/>");
        infoScreenMsg.append("清除Ehcache...<BR/>");
        StopWatch sw = new StopWatch();
        sw.start();
        reqEhCacheHelper.clearCache();
        WkCommonCache.getInstance().clearAllCache();
        // 即時新增
        log.info(loginBean.getUser().getName() + "清除快取完成。");
        sw.stop();
        infoScreenMsg.append(sw.prettyPrint()).append("<BR/>");
        infoScreenMsg.append("清除快取完成。<BR/>");
        infoScreenMsg.append("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░<BR/>");
    }

    /**
     * 執行清除組織快取
     */
    @Deprecated // by REQ-1555
    public void clearUnitCache() {
        this.clearCacheBefor();
        infoScreenMsg.append("<BR/>");
        infoScreenMsg.append("執行清除組織快取...<BR/>");
        StopWatch sw = new StopWatch();
        sw.start();
        log.info(loginBean.getUser().getName() + " 清除組織快取開始。");
        try {
            log.info("開始清除 System-REST 快取");
            SystemCacheUtils.getInstance().clearAllCache();
            // userStorageHelper.callInit();
            // security 快取
            log.info("開始清除 Security 快取");
            cacheConfigUtil.clearAllCache();
        } catch (Exception ex) {
            log.error("清除組織快取失敗。 執行人：" + loginBean.getUser().getName(), ex);
            infoScreenMsg.append("清除組織快取失敗...<BR/>");
        }
        log.info(loginBean.getUser().getName() + " 執行清除組織快取完成。");
        sw.stop();
        infoScreenMsg.append(sw.prettyPrint()).append("<BR/>");
        infoScreenMsg.append("清除組織快取完成。<BR/>");
        infoScreenMsg.append("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░<BR/>");
    }

    private void clearCacheBefor() {
        onlineinfoMBean.setAutoUpdateSwtich(false);
        sysinfoMBean.setAutoUpdateSwtich(false);
    }

    public void resetBpmBySingle() {
        infoScreenMsg.append("<BR/>");
        infoScreenMsg.append("開始執行流程重置作業...<BR/>");
        infoScreenMsg.append("流程ID：").append(resetBpmId).append("<BR/>");
        try {
            bpmUpdateService.checkReqSignInfo(infoScreenMsg, resetBpmId);
            bpmUpdateService.checkPtSignInfo(infoScreenMsg, resetBpmId);
            bpmUpdateService.checkWkStSignInfo(infoScreenMsg, resetBpmId);
        } catch (Exception e) {
            log.error("執行流程重置失敗", e);
            infoScreenMsg.append("執行流程重置作業失敗。 流程ID：").append(resetBpmId).append("<BR/>");
            infoScreenMsg.append(e.getMessage()).append("<BR/>");
        }
        infoScreenMsg.append("流程重置作業結束...<BR/>");
        infoScreenMsg.append("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░<BR/>");
    }

    public void resetBpmByAll() {
        infoScreenMsg.append("<BR/>");
        bpmUpdateService.resetAllFlow();
        infoScreenMsg.append(bpmUpdateService.getUpdateInfo().toString());
        infoScreenMsg.append("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░<BR/>");
    }

    public void syncInBoxReadStatus() {
        try {
            workInboxLogicComponents.syncInBoxReadStatus();
        } catch (Exception e) {
            log.error("syncInBoxReadStatus error", e);
        }

    }

    /**
     * GM管理系統同步
     */
    public void syncGM() {

        // ====================================
        //
        // ====================================
        boolean isSuccess = true;
        String message = "";
        try {
            message = this.syncGMClient.process(SecurityFacade.getUserSid());
            message = WkJsonUtils.getInstance().toPettyJson(message);
            message = "<div class='WS1-1-3b'>同步GM系統, 執行成功!</div></br>回應訊息:<br/>" + message;
        } catch (Exception e) {
            message = "<div class='WS1-1-2b'>同步GM系統, 執行失敗!</div></br>" + e.getMessage();
            log.error(message);
            isSuccess = false;
        }

        // ====================================
        //
        // ====================================
        infoScreenMsg.append("<br/>");
        infoScreenMsg.append(message);
        infoScreenMsg.append("<br/>");
        infoScreenMsg.append("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░<BR/>");

        if (isSuccess) {
            MessagesUtils.showInfo("執行成功!");
        } else {
            MessagesUtils.showError("執行失敗!");
        }
    }
}
