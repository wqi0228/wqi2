/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.anew.manager.TrIssueMappTransManager;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.enums.ConditionInChargeMode;
import com.cy.tech.request.logic.search.view.Search01View;
import com.cy.tech.request.logic.service.CategorySettingService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.URLService.URLServiceAttr;
import com.cy.tech.request.logic.service.customer.ReqWorkCustomerHelper;
import com.cy.tech.request.logic.vo.CustomerTo;
import com.cy.tech.request.logic.vo.query.search.SearchQuery;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.require.RequireForwardDeAndPersonMBean;
import com.cy.tech.request.web.controller.search.helper.Search01Helper;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.view.component.CategoryTreeComponent;
import com.cy.tech.request.web.controller.view.component.ReportOrgTreeComponent;
import com.cy.tech.request.web.controller.view.component.searchquery.SearchQuery01;
import com.cy.tech.request.web.controller.view.component.searchquery.SearchQuery01CustomFilter;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.listener.CategoryTreeCallBack;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.listener.ReportCustomFilterCallback;
import com.cy.tech.request.web.listener.ReportOrgTreeCallBack;
import com.cy.tech.request.web.logic.helper.Search01LogicHelper;
import com.cy.tech.request.web.pf.utils.CommonBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkUserWithRolesCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.customer.vo.enums.EnableType;
import com.cy.work.group.vo.WorkLinkGroup;
import com.cy.work.logic.WkUserAndOrgLogic;
import com.cy.work.viewcomponent.treepker.TreePickerCallback;
import com.cy.work.viewcomponent.treepker.TreePickerComponent;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 需求單查詢
 *
 * @author jason_h
 */
@Controller
@Scope("view")
@Slf4j
public class Search01MBean extends BaseSearchMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3839747452861964808L;
    @Autowired
    transient private TrIssueMappTransManager issueMappTransManager;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private ReqWorkCustomerHelper reqWorkCustomerHelper;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private URLService urlService;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private Require01MBean r01MBean;
    @Autowired
    transient private Search01Helper search01Helper;
    @Autowired
    transient private ReportCustomFilterLogicComponent reportCustomFilterLogicComponent;
    @Autowired
    transient private CategorySettingService categorySettingService;
    @Autowired
    transient private Search01LogicHelper search01LogicHelper;
    @Autowired
    transient private RequireForwardDeAndPersonMBean requireForwardDeAndPersonMBean;
    @Autowired
    transient private WkUserAndOrgLogic wkUserAndOrgLogic;

    private List<Search01View> allQueryItems;
    /** 所有的需求單 */
    @Getter
    @Setter
    private List<Search01View> queryItems;
    /** 選擇的需求單 */
    @Getter
    @Setter
    private Search01View querySelection;
    /** 上下筆移動keeper */
    @Getter
    private Search01View queryKeeper;
    /** 切換模式 */
    @Getter
    private SwitchType switchType = SwitchType.CONTENT;
    /** 切換模式 - 全畫面狀態 */
    @Getter
    private SwitchType switchFullType = SwitchType.DETAIL;
    /** 在匯出的時候，某些內容需要隱藏 */
    @Getter
    private boolean hasDisplay = true;
    @Getter
    private final String dataTableId = "dtRequire";

    /** 追蹤快取 */
    private List<String> hasTraceCache;
    /** 分派快取 */
    private List<String> hasAssignCache;
    /** 有轉寄給自己快取 */
    private List<String> hasForwardSelfCache;
    /** 有轉入需求單的sid */
    private List<String> hasTransIssue;

    @Getter
    /** 類別樹 Component */
    private CategoryTreeComponent categoryTreeComponent;
    @Getter
    /** 報表 組織樹 Component */
    private ReportOrgTreeComponent orgTreeForwardComponent;
    @Getter
    /** 查詢物件 */
    private SearchQuery01 searchQuery;

    @Override
    public SearchQuery getQueryCondition() { return this.searchQuery; }

    @Getter
    /* 廳主選項 **/
    private List<CustomerTo> customerItems;
    @Getter
    /* 廳主分頁選項 **/
    private List<CustomerTo> customerPageItems;
    @Getter
    private SearchQuery01CustomFilter search01ReportCustomFilterComponent;

    /**
     * 登入者可檢視的單位 (權限單位+可閱單位)
     */
    private Set<Integer> canViewDepSids = Sets.newHashSet();

    /**
     * 登入者預設檢視的單位 (權限單位)
     */
    private Set<Integer> defaultViewDepSids = Sets.newHashSet();

    @PostConstruct
    @Override
    public void init() {
        super.init();
        this.canViewDepSids = this.wkUserAndOrgLogic.prepareCanViewDepSidsBaseOnPanel(
                SecurityFacade.getUserSid(), true);
        this.defaultViewDepSids = this.wkUserAndOrgLogic.prepareCanViewDepSidsBaseOnPanel(
                SecurityFacade.getUserSid(), false);

        this.initComponent();
        allQueryItems = Lists.newArrayList();
        display.execute("doSearchData()");
    }

    public void search() {
        try {
            allQueryItems = search01Helper.findWithQuery(loginBean.getCompanyId(), searchQuery, customerItems, loginBean.getUserSId());
            List<String> reqSids = allQueryItems.stream().map(Search01View::getSid).collect(Collectors.toList());
            this.hasAssignCache = search01Helper.createAssignCache(searchQuery, reqSids);
            this.hasForwardSelfCache = search01Helper.createForwardSelfCache(searchQuery, reqSids);
            this.hasTraceCache = search01Helper.createTraceCache(searchQuery, reqSids);
            this.hasTransIssue = issueMappTransManager.createTransFromIssue(reqSids);
            this.filterCustomerPage(Boolean.TRUE);
        } catch (Exception e) {
            log.error("search ERROR", e);
        }
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        this.clearQuery();
        this.searchQuery.loadDefaultSetting(this.defaultViewDepSids);
        this.search();
    }

    public String showTrace(Search01View view) {
        if (view == null || CollectionUtils.isEmpty(hasTraceCache)) {
            return "　";
        }
        return hasTraceCache.contains(view.getSid()) ? "★" : "　";
    }

    public String showAssign(Search01View view) {
        if (view == null || CollectionUtils.isEmpty(hasAssignCache)) {
            return "　";
        }
        return hasAssignCache.contains(view.getSid()) ? "Y" : "　";
    }

    public String showForwardSelf(Search01View view) {
        if (view == null || CollectionUtils.isEmpty(hasForwardSelfCache)) {
            return "　";
        }
        return hasForwardSelfCache.contains(view.getSid()) ? "Y" : "　";
    }

    /**
     * 判斷是否有轉入需求單資料
     *
     * @param view
     * @return
     */
    public String hasTransByIssue(Search01View view) {
        if (view == null || CollectionUtils.isEmpty(hasTransIssue)) {
            return "　";
        }
        return hasTransIssue.contains(view.getSid()) ? "Y" : "　";
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Search01View view) {       
        view.setReadRecordType(ReadRecordType.HAS_READ);
        Require r = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(r, loginBean.getUser());
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (Search01View) event.getObject();
        this.changeRequireContent(this.queryKeeper);
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Search01View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        helper.exportExcel(document, searchQuery.getStartDate(), searchQuery.getEndDate(), searchQuery.getReportType());
        hasDisplay = true;
    }

    /**
     * 隱藏部分column裡的內容
     */
    public void hideColumnContent() {
        hasDisplay = false;
    }

    public String getAttInfo(Search01View view) {
        Boolean checkAtt = view.getCheckAttachment();
        if (checkAtt) {
            if (view.getHasAttachment()) {
                return "檢核正常";
            } else {
                return "檢核異常";
            }
        } else {
            return "不需檢核";
        }
    }

    /**
     * 列表執行關聯動作
     *
     * @param view
     */
    public void initRelevance(Search01View view) {
        this.querySelection = view;
        this.queryKeeper = this.querySelection;
        Require r = requireService.findByReqNo(view.getRequireNo());
        this.r01MBean.setRequire(r);
        this.r01MBean.getRelevanceMBean().initRelevance();
    }

    public String getRelevanceViewUrl(Search01View view) {
        if (view == null) {
            return "";
        }
        Require r = requireService.findByReqNo(view.getRequireNo());
        if (r == null) {
            return "";
        }
        WorkLinkGroup link = r.getLinkGroup();
        if (link == null) {
            return "";
        }
        return "../require/require04.xhtml" + urlService.createSimpleURLLink(URLServiceAttr.URL_ATTR_L, link.getSid(), view.getRequireNo(), 1);
    }

    /**
     * 列表點選轉寄功能
     * 
     * @param view
     */
    public void openForwardDialog(Search01View view) {
        this.queryKeeper = this.querySelection = view;

        if (SwitchType.DETAIL.equals(switchType)) {
            this.changeRequireContent(this.queryKeeper);
            this.display.update(Lists.newArrayList(
                    "@(.reportUpdateClz)",
                    "title_info_click_btn_id",
                    "require01_title_info_id",
                    "require_template_id",
                    "viewPanelBottomInfoId",
                    "req03botmid",
                    this.dataTableId + "1"));
        }

        this.requireForwardDeAndPersonMBean.openDialog(view.getRequireNo(), r01MBean);
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search01View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.removeClassByTextBold(dtId, pageCount);
        this.transformHasRead();
        this.checkHelfScreen();
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            display.update("headerTitle");
            display.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Search01View to) {
        querySelection = to;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 去除粗體Class
     *
     * @param dtId
     * @param pageCount
     */
    private void removeClassByTextBold(String dtId, String pageCount) {
        display.execute("removeClassByTextBold('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
        display.execute("changeAlreadyRead('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 變更已閱讀
     */
    private void transformHasRead() {
        querySelection.setReadRecordType(ReadRecordType.HAS_READ);
        queryKeeper = querySelection;
        queryItems.set(queryItems.indexOf(querySelection), querySelection);
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.transformHasRead();
        this.removeClassByTextBold(dtId, pageCount);
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    /**
     * 列表執行追蹤動作
     *
     * @param view
     */
    public void btnAddTrack(Search01View view) {
        Require r = this.highlightAndReturnRequire(view);
        this.r01MBean.setRequire(r);
        this.r01MBean.getTraceActionMBean().initTrace(this.r01MBean);
    }

    /**
     * 列表標註及回傳需求單
     *
     * @param view
     * @return
     */
    private Require highlightAndReturnRequire(Search01View view) {
        this.querySelection = view;
        this.queryKeeper = this.querySelection;
        return requireService.findByReqNo(view.getRequireNo());
    }

    /**
     * 變更 廳主
     */
    public void chgCustomerPage() {
        this.filterCustomerPage(Boolean.FALSE);
    }

    /**
     * 過濾 廳主
     *
     * @param isSearch
     */
    public void filterCustomerPage(Boolean isSearch) {
        if (searchQuery.getIsCustomerPage()) {
            if (searchQuery.getCustomerPage() == null && !customerPageItems.isEmpty()) {
                searchQuery.setCustomerPage(customerPageItems.get(0));
            }
        }
        this.bulidQueryItemsAndCustomerPageItems(isSearch);
    }

    /**
     * 建立廳主分頁選項
     */
    private void bulidQueryItemsAndCustomerPageItems(Boolean isSearch) {
        queryItems = Lists.newArrayList();
        Set<Long> customerSet = Sets.newHashSet();
        allQueryItems.stream().forEach(v -> {
            if (isSearch && v.getCustomer() != null) {
                customerSet.add(v.getCustomer());
            }
            if (!(searchQuery.getIsCustomerPage()
                    && (searchQuery.getCustomerPage() != null && !searchQuery.getCustomerPage().getSid().equals(v.getCustomer())))) {
                queryItems.add(v);
            }
        });

        if (isSearch) {
            List<CustomerTo> customers = Lists.newArrayList();
            try {
                customers = reqWorkCustomerHelper.findTosBySids(customerSet);
            } catch (Exception e) {
                log.error("查詢失敗：" + e.getMessage(), e);
            }
            customerPageItems = Lists.newArrayList(customers);
            if (searchQuery.getIsCustomerPage()
                    && (customers.isEmpty() || !customers.contains(searchQuery.getCustomerPage()))) {
                searchQuery.setCustomerPage(null);
                this.filterCustomerPage(Boolean.FALSE);
            }
        }
    }

    /**
     * 異動需求來源
     */
    public void changeReqSource() {
        // ====================================
        // 點選時，初始化查詢區間
        // ====================================
        this.searchQuery.initDefCreateReqIntervalDate();

        // ====================================
        // 有選需求來源時，因為會過濾掉主責單位，過鎖定過濾選項『主責單位』
        // ====================================
        boolean disableInChargeMode = true;
        if (WkStringUtils.isEmpty(this.searchQuery.getReqSource())
                || "責".equals(this.searchQuery.getReqSource())) {
            disableInChargeMode = false;
        } else {
            this.searchQuery.setConditionInChargeMode(ConditionInChargeMode.NO_FILTER);
            this.searchQuery.setInChargeDepSids(Lists.newArrayList());
            this.searchQuery.setInChargeUserSids(Lists.newArrayList());
        }
        this.searchQuery.setDisableInChargeMode(disableInChargeMode);

        // ====================================
        // 查詢
        // ====================================
        this.search();
    }

    /**
     * 初始化元件組 1.需求類別元件 2.單位挑選元件 3.自訂預設搜尋條件
     */
    private void initComponent() {

        // 查詢登入者所有角色
        List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
                SecurityFacade.getUserSid(),
                SecurityFacade.getCompanyId());

        search01ReportCustomFilterComponent = new SearchQuery01CustomFilter(loginBean.getUserSId(), loginBean.getDep().getSid(),
                reportCustomFilterLogicComponent, messageCallBack, display, categorySettingService, reportCustomFilterCallback);
        categoryTreeComponent = new CategoryTreeComponent(categoryTreeCallBack);

        orgTreeForwardComponent = new ReportOrgTreeComponent(
                loginBean.getCompanyId(),
                loginBean.getDep(),
                roleSids,
                false,
                reportOrgTreeForwardCallBack);

        searchQuery = new SearchQuery01(loginBean.getDep(), loginBean.getUser(), ReportType.REQUIRE, categoryTreeCallBack,
                categorySettingService, search01LogicHelper);
        this.initCustomerItems();
        this.clearQuery();
        searchQuery.loadDefaultSetting(this.defaultViewDepSids);
    }

    /**
     * 開啟 類別樹
     */
    public void btnOpenCategoryTree() {
        try {
            categoryTreeComponent.init();
            display.showPfWidgetVar("dlgCate");
        } catch (Exception e) {
            log.error("btnOpenCategoryTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 開啟 轉發至 組織樹
     */
    public void btnOpenForwardOrgTree() {
        try {
            orgTreeForwardComponent.initOrgTree(loginBean.getComp(), searchQuery.getForwardDepts());
            display.showPfWidgetVar("dlgOrgTreeForward");
        } catch (Exception e) {
            log.error("btnOpenForwardOrgTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 清除進階查詢條件
     */
    public void btnClearAdvance() {
        try {
            searchQuery.clearAdvance();
        } catch (Exception e) {
            log.error("btnClearAdvance Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /** 訊息呼叫 */
    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -301711651617888384L;

        @Override
        public void showMessage(String m) {
            MessagesUtils.showError(m);
        }
    };

    private final ReportCustomFilterCallback reportCustomFilterCallback = new ReportCustomFilterCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = -5995818625237190463L;

        @Override
        public void reloadDefault(String index) {
            categoryTreeComponent.clearCate();
            searchQuery.loadDefaultSetting(defaultViewDepSids);
            search();
        }
    };

    /** 類別樹 Component CallBack */
    private final CategoryTreeCallBack categoryTreeCallBack = new CategoryTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 5374977864409717052L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelCate() {
            categoryTreeComponent.selCate();
            searchQuery.setBigDataCateSids(categoryTreeComponent.getBigDataCateSids());
            searchQuery.setMiddleDataCateSids(categoryTreeComponent.getMiddleDataCateSids());
            searchQuery.setSmallDataCateSids(categoryTreeComponent.getSmallDataCateSids());
            categoryTreeComponent.clearCateSids();
        }

        @Override
        public void loadSelCate(List<String> smallDataCateSids) {
            categoryTreeComponent.init();
            categoryTreeComponent.selectedItem(smallDataCateSids);
            this.confirmSelCate();
        }

        @Override
        public void actionSelectAll() {
        }

        @Override
        public void onNodeSelect() {
        }

        @Override
        public void onNodeUnSelect() {
        }
    };

    /** 報表 組織樹 Component CallBack */
    private final ReportOrgTreeCallBack reportOrgTreeForwardCallBack = new ReportOrgTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 7643883466768772508L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelOrg() {
            searchQuery.setForwardDepts(orgTreeForwardComponent.getSelOrgSids());
        }
    };

    /**
     * 取得 廳主選項
     */
    private void initCustomerItems() {
        customerItems = Lists.newArrayList();
        reqWorkCustomerHelper.findByEnable(EnableType.ENABLE).forEach(each -> {
            customerItems.add(new CustomerTo(each.getSid(), each.getAlias(), each.getName(), each.getLoginCode(),
                    CommonBean.getInstance().get(each.getCustomerType())));
        });
    }

    /**
     * 清除/還原選項
     */
    private void clearQuery() {
        searchQuery.init();
        categoryTreeComponent.clearCate();

        // 預設可閱部門，為相關部門，不含可閱部門
        Set<Integer> relationDepSids = this.wkUserAndOrgLogic.prepareCanViewDepSidsBaseOnPanel(
                SecurityFacade.getUserSid(), false);

        this.searchQuery.setRequireDepts(relationDepSids);
    }

    // ========================================================================
    // 單位挑選樹
    // ========================================================================
    /**
     * 單位挑選
     */
    @Getter
    @Setter
    private transient TreePickerComponent depTreePicker;

    @Getter
    private final String DEP_TREE_PICKER_DLG_WV = this.getClass().getSimpleName() + "DEP_TREE_PICKER_DLG_WV";
    @Getter
    private final String DEP_TREE_PICKER_CONTENT_CLASS = this.getClass().getSimpleName() + "DEP_TREE_PICKER_CONTENT_CLASS";
    @Getter
    private final String DEP_TREE_PICKER_SHOW_SELECTED_CLASS = this.getClass().getSimpleName() + "DEP_TREE_PICKER_SHOW_SELECTED_CLASS";

    /**
     * 顯示所選取的單位
     * 
     * @return
     */
    public String getSelectedDepsInfo() {
        Set<Integer> requireDepts = this.searchQuery.getRequireDepts();
        if (WkStringUtils.isEmpty(requireDepts)
                || WkCommonUtils.compare(requireDepts, this.canViewDepSids)) {
            return "全部顯示";
        }

        return WkOrgUtils.findNameBySid(WkOrgUtils.sortByOrgTree(requireDepts), "、");
    }

    /**
     * 單位樹選單 - 開啟選單
     */
    public void event_dialog_depTreePicker_open() {

        try {
            this.depTreePicker = new TreePickerComponent(depTreePickerCallback, "單位挑選");
            this.depTreePicker.rebuild();
        } catch (Exception e) {
            log.error("開啟【單位挑選】設定視窗失敗!" + e.getMessage(), e);
            MessagesUtils.showError("開啟【單位挑選】設定視窗失敗!");
            return;
        }

        displayController.showPfWidgetVar(this.DEP_TREE_PICKER_DLG_WV);
    }

    /**
     * 單位樹選單 - 關閉選單
     */
    public void event_dialog_depTreePicker_confirm() {

        // 取得元件中被選擇的項目
        this.searchQuery.setRequireDepts(this.depTreePicker.getSelectedItemIntegerSids());

        // 畫面控制
        displayController.hidePfWidgetVar(this.DEP_TREE_PICKER_DLG_WV);
    }

    /**
     * 分派通知單位選單 - callback 事件
     */
    private final TreePickerCallback depTreePickerCallback = new TreePickerCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = -2685194232972981152L;

        @Override
        public List<WkItem> prepareAllItems() throws Exception {
            // ====================================
            // 轉 WkItem
            // ====================================
            return treePickerDepHelper.prepareDepItems(
                    SecurityFacade.getCompanyId(),
                    Lists.newArrayList(canViewDepSids));

        }

        /**
         * 準備已選擇項目
         * 
         * @return
         */
        @Override
        public List<String> prepareSelectedItemSids() throws Exception {

            if (WkStringUtils.isEmpty(searchQuery.getRequireDepts())) {
                return Lists.newArrayList();
            }

            // 回傳
            return searchQuery.getRequireDepts().stream()
                    .map(sid -> sid + "")
                    .collect(Collectors.toList());
        }

        @Override
        public List<String> prepareDisableItemSids() throws Exception {
            // 沒有不可選擇的單位
            return Lists.newArrayList();
        }
    };

}
