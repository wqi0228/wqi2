/**
 * 
 */
package com.cy.tech.request.web.controller.helper;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import com.cy.tech.request.vo.template.component.ComBase;
import com.cy.tech.request.web.controller.require.RequireCheckItemMBean;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

/**
 * @author allen1214_wu
 *
 */
@Component
public class RequireHelper {

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 建立或編輯時檢查需求單輸入值
     * @param require
     * @param requireCheckItemMBean
     * @param templateItem
     * @param valueMap
     * @throws UserMessageException
     */
    public void checkInputInfo(
            Require require,
            RequireCheckItemMBean requireCheckItemMBean,
            List<FieldKeyMapping> templateItem,
            Map<String, ComBase> valueMap) throws UserMessageException {

        List<String> messages = Lists.newArrayList();

        // ====================================
        // 欄位檢核
        // ====================================
        // 期望日期
        if (WkStringUtils.isEmpty(require.getHopeDate())) {
            messages.add("請輸入【期望完成日期】！");
        }

        // 廳主資料
        if (require.getCustomer() == null) {
            messages.add("請輸入【廳主】資料！");
        }

        // 系統別
        if (!requireCheckItemMBean.checkMustInput()) {
            messages.add("請輸入【系統別】資料！");
        }

        // 組件欄位資料
        List<ComBase> comBases = templateItem.stream()
                .filter(each -> each.getRequiredInput())
                .map(each -> valueMap.get(each.getComId()))
                .collect(Collectors.toList());

        for (ComBase com : comBases) {
            if (com.checkRequireValueIsEmpty()) {
                messages.add("請輸入【" + com.getName() + "】資料！");
            }
        }

        // ====================================
        // 組警示訊息
        // ====================================
        if (WkStringUtils.notEmpty(messages)) {
            // 警示訊息
            String userWarnMessage = String.join("<br/>", messages);

            throw new UserMessageException(
                    userWarnMessage,
                    InfomationLevel.WARN);
        }
    }

}
