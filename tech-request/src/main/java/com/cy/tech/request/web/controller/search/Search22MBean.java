/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search22QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.Search22View;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.TraceService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.URLService.URLServiceAttr;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.require.RequireForwardDeAndPersonMBean;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.searchheader.CommonHeaderMBean;
import com.cy.tech.request.web.controller.searchheader.Search22HeaderMBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.group.vo.WorkLinkGroup;
import com.cy.work.trace.vo.WorkTrace;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 退件暫存單據查詢
 *
 * @author jason_h
 */
@Controller
@Scope("view")
@Slf4j
public class Search22MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3430846052220189692L;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private Search22HeaderMBean headerMBean;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private CategoryTreeMBean cateTreeMBean;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private URLService urlService;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private TraceService traceService;
    @Autowired
    transient private Require01MBean r01MBean;
    @Autowired
    transient private Search22QueryService search22QueryService;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchResultHelper searchResultHelper;
    @Autowired
    transient private SearchHelper searchHelper;

    /** 所有的需求單 */
    @Getter
    @Setter
    private List<Search22View> queryItems;

    /** 選擇的需求單 */
    @Getter
    @Setter
    private Search22View querySelection;

    /** 上下筆移動keeper */
    @Getter
    private Search22View queryKeeper;

    /** 切換模式 */
    @Getter
    private SwitchType switchType = SwitchType.CONTENT;

    /** 切換模式 - 全畫面狀態 */
    @Getter
    private SwitchType switchFullType = SwitchType.DETAIL;

    /** 在匯出的時候，某些內容需要隱藏 */
    @Getter
    private boolean hasDisplay = true;

    @Getter
    private final String dataTableId = "dtRequire";
    @Getter
    @Setter
    /** 列表是否全選 */
    private boolean dtSelAll = false;
    @Getter
    /** 欲完成的追蹤 sid */
    private List<String> finishTraceSids = Lists.newArrayList();

    @PostConstruct
    public void init() {
        this.search();
    }

    public void search() {
        queryItems = this.findWithQuery();
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        headerMBean.clear();
        this.search();
    }

    private List<Search22View> findWithQuery() {
        String requireNo = reqularUtils.getRequireNo(loginBean.getCompanyId(), commHeaderMBean.getFuzzyText());
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT "
                + "trace.tracesid,"
                + "tr.has_forward_dep,"
                + "tr.has_forward_member,"
                + "tr.has_link,"
                + "trace.trace_type,"
                + "ckm.small_category_name,"
                + "trace.trace_notice_date,"
                + "trace.create_usr,"
                + "trace.trace_status,"
                + "trace.trace_memo,"
                + "trace.trace_memo_css,"
                + "tid.field_content,"
                + "tr.require_status,"
                + "tr.update_dt,"
                + "trace.create_dt,"
                + "tr.require_no,"
                + "tr.urgency, "
                + "trace.Status, "
                // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
                + this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire()

                + " FROM ");
        this.buildTraceCondition(requireNo, builder, parameters);
        this.buildRequireCondition(requireNo, builder, parameters);
        this.buildCategoryKeyMappingCondition(requireNo, builder, parameters);
        this.buildRequireIndexDictionaryCondition(requireNo, builder, parameters);
        // 檢查項目 (系統別)
        // 後方需對 主單 sid 做 group by
        builder.append(this.searchConditionSqlHelper.prepareCommonJoin(SecurityFacade.getUserSid()));

        builder.append("WHERE trace.tracesid IS NOT NULL ");
        builder.append(" GROUP BY tr.require_sid ");
        builder.append("ORDER BY tr.update_dt DESC , trace.trace_notice_date ASC ");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.TRACE, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.TRACE, SecurityFacade.getUserSid());

        // 查詢
        List<Search22View> resultList = search22QueryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // ====================================
        // 後續處理
        // ====================================
        if (WkStringUtils.notEmpty(resultList)) {
            // 後續處理-開始
            usageRecord.afterProcessStart();
            // 查詢條件過濾：系統別
            resultList = resultList.stream()
                    .filter(each -> this.searchResultHelper.filterCheckItems(
                            each.getCheckItemTypes(),
                            commHeaderMBean.getCheckItemTypes(),
                            false,
                            null))
                    .collect(Collectors.toList());

            // 後續處理-結束
            usageRecord.afterProcessEnd();
        }

        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return resultList;
    }

    /**
     * 追蹤條件
     *
     * @param builder
     * @param parameters
     */
    private void buildTraceCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {

        builder.append("(SELECT * FROM work_trace_info trace WHERE 1=1");

        // 預設條件-符合部門追蹤或人員追蹤
        Set<Integer> traceDeps = Sets.newHashSet(SecurityFacade.getPrimaryOrgSid());
        Set<Integer> managerDepSids = WkOrgCache.getInstance().findManagerOrgSids(SecurityFacade.getUserSid());
        if (WkStringUtils.notEmpty(managerDepSids)) {
            traceDeps.addAll(managerDepSids);
        }

        builder.append(" AND (trace.trace_usr = " + SecurityFacade.getUserSid() + " ");
        if (WkStringUtils.notEmpty(traceDeps)) {
            builder.append("       OR trace.trace_dep IN (:trace_deps) ");
            parameters.put("trace_deps", traceDeps);
        }
        builder.append("     ) ");

        // 預設條件-有效單據
        builder.append(" AND trace.status = 0 ").append("\r\n");

        // 追蹤狀態
        if (Strings.isNullOrEmpty(requireNo) && headerMBean.getTraceStatus() != null) {
            builder.append(" AND trace.trace_status = :traceStatus");
            parameters.put("traceStatus", headerMBean.getTraceStatus().name());
        }
        // 追蹤區間
        if (Strings.isNullOrEmpty(requireNo) && headerMBean.getStartCreatedDate() != null && headerMBean.getEndCreatedDate() != null) {
            builder.append(" AND trace.create_dt BETWEEN :startCreatedDate AND :endCreatedDate ").append("\r\n");
            parameters.put("startCreatedDate", searchHelper.transStartDate(headerMBean.getStartCreatedDate()));
            parameters.put("endCreatedDate", searchHelper.transEndDate(headerMBean.getEndCreatedDate()));
        } else if (Strings.isNullOrEmpty(requireNo) && headerMBean.getStartCreatedDate() != null) {
            builder.append(" AND trace.create_dt >= :startCreatedDate ").append("\r\n");
            parameters.put("startCreatedDate", searchHelper.transStartDate(headerMBean.getStartCreatedDate()));
        } else if (Strings.isNullOrEmpty(requireNo) && headerMBean.getEndCreatedDate() != null) {
            builder.append(" AND trace.create_dt <= :endCreatedDate ").append("\r\n");
            parameters.put("endCreatedDate", searchHelper.transEndDate(headerMBean.getEndCreatedDate()));
        }
        // 追蹤提醒日
        if (Strings.isNullOrEmpty(requireNo)) {
            if (headerMBean.getStartNoticeDate() != null && headerMBean.getEndNoticeDate() != null) {
                builder.append(" AND trace.trace_notice_date BETWEEN :startNoticeDate AND :endNoticeDate ").append("\r\n");
                parameters.put("startNoticeDate", searchHelper.transStartDate(headerMBean.getStartNoticeDate()));
                parameters.put("endNoticeDate", searchHelper.transEndDate(headerMBean.getEndNoticeDate()));
            } else if (headerMBean.getStartNoticeDate() != null) {
                builder.append(" AND trace.trace_notice_date >= :startNoticeDate ").append("\r\n");
                parameters.put("startNoticeDate", searchHelper.transStartDate(headerMBean.getStartNoticeDate()));
            } else if (headerMBean.getEndNoticeDate() != null) {
                builder.append(" AND trace.trace_notice_date <= :endNoticeDate ").append("\r\n");
                parameters.put("endNoticeDate", searchHelper.transEndDate(headerMBean.getEndNoticeDate()));
            }
        }
        builder.append(") AS trace ");
    }

    /**
     * 需求單條件
     *
     * @param builder
     * @param parameters
     */
    private void buildRequireCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_require tr WHERE 1=1 ");
        // 需求單號
        if (!Strings.isNullOrEmpty(requireNo)) {
            builder.append(" AND tr.require_no = :requireNo");
            parameters.put("requireNo", requireNo);
        }
        // 需求類別製作進度
        builder.append(" AND tr.require_status IN (:requireStatus)");
        if (Strings.isNullOrEmpty(requireNo)) {
            parameters.put("requireStatus", headerMBean.createQueryReqStatus());
        } else {
            parameters.put("requireStatus", headerMBean.createQueryAllReqStatus());
        }
        builder.append(") AS tr ON tr.require_sid=trace.trace_source_sid ");
    }

    /**
     * 欄位條件
     *
     * @param builder
     * @param parameters
     */
    private void buildRequireIndexDictionaryCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT tid.require_sid,tid.field_content FROM tr_index_dictionary tid WHERE 1=1");
        // 模糊搜尋
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(commHeaderMBean.getFuzzyText())) {
            String text = "%" + reqularUtils.replaceIllegalSqlLikeStr(commHeaderMBean.getFuzzyText()) + "%";
            builder.append(" AND (tid.require_sid"
                    + " IN (SELECT DISTINCT tid1.require_sid FROM tr_index_dictionary tid1 WHERE tid1.field_content LIKE :text)"
                    + " OR tid.require_sid IN (SELECT DISTINCT trace.require_sid FROM tr_require_trace trace WHERE "
                    + "trace.require_trace_type = 'REQUIRE_INFO_MEMO' AND trace.require_trace_content LIKE :text))");
            parameters.put("text", text);
        }

        // only 主題
        if (Strings.isNullOrEmpty(requireNo) && Strings.isNullOrEmpty(commHeaderMBean.getFuzzyText())
                && Strings.isNullOrEmpty(headerMBean.getContent())
                && !Strings.isNullOrEmpty(headerMBean.getTheme())) {
            String themeText = "%" + reqularUtils.replaceIllegalSqlLikeStr(headerMBean.getTheme()) + "%";
            builder.append(" AND (tid.field_name = '主題' AND tid.field_content LIKE :themeText)");
            parameters.put("themeText", themeText);
        }

        // only 內容
        if (Strings.isNullOrEmpty(requireNo) && Strings.isNullOrEmpty(commHeaderMBean.getFuzzyText())
                && Strings.isNullOrEmpty(headerMBean.getTheme())
                && !Strings.isNullOrEmpty(headerMBean.getContent())) {
            String contentText = "%" + reqularUtils.replaceIllegalSqlLikeStr(headerMBean.getContent()) + "%";
            builder.append(" AND tid.require_sid IN (SELECT DISTINCT tid1.require_sid FROM tr_index_dictionary tid1 WHERE "
                    + "tid1.field_name!='主題' AND tid1.field_content LIKE :contentText)");
            parameters.put("contentText", contentText);
        }

        // 內容 & 主題
        if (Strings.isNullOrEmpty(requireNo) && Strings.isNullOrEmpty(commHeaderMBean.getFuzzyText())
                && !Strings.isNullOrEmpty(headerMBean.getTheme())
                && !Strings.isNullOrEmpty(headerMBean.getContent())) {
            String contentText = "%" + reqularUtils.replaceIllegalSqlLikeStr(headerMBean.getContent()) + "%";
            String themeText = "%" + reqularUtils.replaceIllegalSqlLikeStr(headerMBean.getTheme()) + "%";

            builder.append(" AND tid.require_sid IN (SELECT DISTINCT tid1.require_sid FROM tr_index_dictionary tid1 WHERE "
                    + "tid1.field_name!='主題' AND tid1.field_content LIKE :contentText)");
            parameters.put("contentText", contentText);

            builder.append(" AND (tid.field_name = '主題' AND tid.field_content LIKE :themeText)");
            parameters.put("themeText", themeText);
        }
        builder.append(" AND tid.field_name='主題') AS tid ON tr.require_sid=tid.require_sid ");
    }

    /**
     * 類別條件
     *
     * @param builder
     * @param parameters
     */
    private void buildCategoryKeyMappingCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_category_key_mapping ckm WHERE 1=1");
        // 類別組合
        if (Strings.isNullOrEmpty(requireNo) && (!cateTreeMBean.getBigDataCateSids().isEmpty() || !cateTreeMBean.getMiddleDataCateSids().isEmpty()
                || !cateTreeMBean.getSmallDataCateSids().isEmpty())) {
            builder.append(" AND ckm.big_category_sid IN (:bigs) OR ckm.middle_category_sid IN (:middles) OR ckm.small_category_sid IN "
                    + "(:smalls)");
            // 加入單獨選擇的大類
            if (commHeaderMBean.getSelectBigCategory() != null) {
                cateTreeMBean.getBigDataCateSids().add(commHeaderMBean.getSelectBigCategory().getSid());
            }
            parameters.put("bigs", cateTreeMBean.getBigDataCateSids().isEmpty() ? "" : cateTreeMBean.getBigDataCateSids());
            parameters.put("middles", cateTreeMBean.getMiddleDataCateSids().isEmpty() ? "" : cateTreeMBean.getMiddleDataCateSids());
            parameters.put("smalls", cateTreeMBean.getSmallDataCateSids().isEmpty() ? "" : cateTreeMBean.getSmallDataCateSids());
        }
        builder.append(") AS ckm ON tr.mapping_sid=ckm.key_sid ");
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Search22View view) {
        view.setReadRecordType(ReadRecordType.HAS_READ);
        Require r = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(r, loginBean.getUser());
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (Search22View) event.getObject();
        this.changeRequireContent(this.queryKeeper);
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Search22View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        searchHelper.exportExcel(document, commHeaderMBean.getStartDate(), commHeaderMBean.getEndDate(), commHeaderMBean.getReportType());
        hasDisplay = true;
    }

    /**
     * 隱藏部分column裡的內容
     */
    public void hideColumnContent() {
        hasDisplay = false;
    }

    /**
     * 列表執行關聯動作
     *
     * @param view
     */
    public void initRelevance(Search22View view) {
        this.querySelection = view;
        this.queryKeeper = this.querySelection;
        Require r = requireService.findByReqNo(view.getRequireNo());
        this.r01MBean.setRequire(r);
        this.r01MBean.getRelevanceMBean().initRelevance();
    }

    public String getRelevanceViewUrl(Search22View view) {
        if (view == null) {
            return "";
        }
        Require r = requireService.findByReqNo(view.getRequireNo());
        if (r == null) {
            return "";
        }
        WorkLinkGroup link = r.getLinkGroup();
        if (link == null) {
            return "";
        }
        return "../require/require04.xhtml" + urlService.createSimpleURLLink(URLServiceAttr.URL_ATTR_L, link.getSid(), view.getRequireNo(), 1);
    }

    /**
     * 列表點選轉寄功能
     * 
     * @param view
     */
    public void openForwardDialog(Search22View view) {
        this.queryKeeper = this.querySelection = view;

        if (SwitchType.DETAIL.equals(switchType)) {
            this.changeRequireContent(this.queryKeeper);
            this.display.update(Lists.newArrayList(
                    "@(.reportUpdateClz)",
                    "title_info_click_btn_id",
                    "require01_title_info_id",
                    "require_template_id",
                    "viewPanelBottomInfoId",
                    "req03botmid",
                    this.dataTableId + "1"));
        }

        RequireForwardDeAndPersonMBean mbean = WorkSpringContextHolder.getBean(RequireForwardDeAndPersonMBean.class);
        mbean.openDialog(view.getRequireNo(), r01MBean);
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search22View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            display.update("headerTitle");
            display.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Search22View to) {
        querySelection = to;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    /**
     * 取消追蹤
     */
    public void btnCancelTrace() {
        WorkTrace workTrace = traceService.findOne(new WorkTrace(querySelection.getSid()));
        traceService.updateByInactive(workTrace, loginBean.getUser());
        this.refreshList();
    }

    /**
     * 已完成追蹤
     */
    public void btnFinishTrace() {
        traceService.updateByFinish(finishTraceSids, loginBean.getUser());
        queryItems.removeIf(each -> {
            if (finishTraceSids.contains(each.getSid())) {
                return true;
            }
            return false;
        });
        finishTraceSids.clear();
        dtSelAll = false;
    }

    /**
     * 刷新列表
     */
    private void refreshList() {
        int index = queryItems.indexOf(querySelection);
        if (queryItems.size() > (index + 1)) {
            querySelection = queryItems.get(++index);
        } else if (index > 0) {
            querySelection = queryItems.get(--index);
        }
        this.search();
        if (queryItems.isEmpty()) {
            querySelection = null;
            if (switchType.equals(SwitchType.DETAIL)) {
                switchFullType = SwitchType.DETAIL;
                switchType = SwitchType.CONTENT;
            }
            return;
        } else if (!queryItems.contains(querySelection)) {
            querySelection = queryItems.get(0);
        }
        this.queryKeeper = this.querySelection;
        this.changeRequireContent(this.queryKeeper);
    }

    /**
     * 列表執行追蹤動作
     *
     * @param view
     */
    public void btnAddTrack(Search22View view) {
        Require r = this.highlightAndReturnRequire(view);
        this.r01MBean.setRequire(r);
        this.r01MBean.getTraceActionMBean().initTrace(this.r01MBean);
    }

    /**
     * 列表執行追蹤回覆動作
     *
     * @param view
     */
    public void btnTraceByMemo(Search22View view) {
        Require r = this.highlightAndReturnRequire(view);
        WorkTrace workTrace = traceService.findOne(new WorkTrace(querySelection.getSid()));
        this.r01MBean.setRequire(r);
        this.r01MBean.getTraceActionMBean().initTraceByMemo(r, workTrace);
    }

    /**
     * 列表標註及回傳需求單
     *
     * @param view
     * @return
     */
    private Require highlightAndReturnRequire(Search22View view) {
        this.querySelection = view;
        this.queryKeeper = this.querySelection;
        return requireService.findByReqNo(view.getRequireNo());
    }

    /**
     * 變更dataTable 全選
     */
    public void changeByDtSelAll() {
        try {
            finishTraceSids = Lists.newArrayList();
            queryItems.forEach(each -> {
                each.setHasSelCheckBox(dtSelAll);
                if (dtSelAll) {
                    finishTraceSids.add(each.getSid());
                }
            });
        } catch (Exception e) {
            log.error("changeByDtSelAll Error", e);
        }
    }

    /**
     * 變更dataTable value CheckBox
     *
     * @param to
     */
    public void changeByDtSelCheckBox(Search22View to) {
        try {
            if (!to.isHasSelCheckBox()) {
                finishTraceSids.remove(to.getSid());
                if (dtSelAll) {
                    dtSelAll = false;
                }
            } else {
                finishTraceSids.add(to.getSid());
                if (!dtSelAll) {
                    dtSelAll = queryItems.stream()
                            .allMatch(each -> each.isHasSelCheckBox());
                }
            }
        } catch (Exception e) {
            log.error("changeByDtSelCheckBox Error", e);
        }
    }
}
