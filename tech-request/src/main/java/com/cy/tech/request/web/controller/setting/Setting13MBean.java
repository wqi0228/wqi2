/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.setting;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.view.component.CategoryTreeComponent;
import com.cy.tech.request.web.controller.view.vo.TROtherCategoryTo;
import com.cy.tech.request.web.controller.view.vo.TROtherCategoryView;
import com.cy.tech.request.web.listener.CategoryTreeCallBack;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.view.logic.TROtherCategoryHelper;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 第二分類維護作業
 *
 * @author kasim
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope("view")
public class Setting13MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2130997853274602653L;
    @Autowired
    private LoginBean loginBean;
    @Autowired
    private TROtherCategoryHelper otherCategoryHelper;
    @Autowired
    private DisplayController displayController;

    @Getter
    /** 類別樹 Component */
    private CategoryTreeComponent categoryTreeComponent;
    @Getter
    @Setter
    /** 搜尋狀態 */
    private Activation filterStatus;

    @Getter
    private String msg;

    @Getter
    @Setter
    /** 分類名稱 LIKE %分類名稱% */
    private String filterName;

    @Getter
    @Setter
    /** 列表資料 */
    private List<TROtherCategoryView> allCategoryViews;
    @Getter
    @Setter
    /** 選取資料 */
    private TROtherCategoryView selCategoryView;
    @Getter
    /** 操作資料 */
    private TROtherCategoryTo categoryViewTo;

    @PostConstruct
    public void init() {
        this.categoryViewTo = new TROtherCategoryTo();
        this.initComponent();
        this.btnClear();
    }

    /**
     * 初始化元件
     */
    private void initComponent() {
        categoryTreeComponent = new CategoryTreeComponent(categoryTreeCallBack);
        categoryTreeComponent.init();
    }

    /**
     * 執行搜尋
     */
    public void doSearch() {
        try {
            this.search();
        } catch (Exception e) {
            log.error("btnSearch ERROR!!", e);
            this.showMsg("執行搜尋錯誤，" + e.getMessage());
        }
    }

    /**
     * 執行清除
     */
    public void btnClear() {
        try {
            filterStatus = null;
            filterName = "";
            this.search();
        } catch (Exception e) {
            log.error("btnClear ERROR!!", e);
            this.showMsg("執行清除錯誤，" + e.getMessage());
        }
    }

    /**
     * 執行新增
     */
    public void btnAdd() {
        try {
            categoryViewTo = new TROtherCategoryTo();
            displayController.showPfWidgetVar("dlg_category_wv");
        } catch (Exception e) {
            log.error("btnAdd ERROR!!", e);
            this.showMsg("執行新增錯誤，" + e.getMessage());
        }
    }

    /**
     * 進行編輯
     *
     * @param to
     */
    public void btnEdit(TROtherCategoryView to) {
        try {
            selCategoryView = to;
            categoryViewTo = otherCategoryHelper.findToBySid(selCategoryView.getSid());
            displayController.showPfWidgetVar("dlg_category_wv");
            displayController.execute("selectRow('wvDt'," + this.getRowIndex() + ");");
        } catch (Exception e) {
            log.error("btnEdit ERROR!!", e);
            this.showMsg("進行編輯錯誤，" + e.getMessage());
        }
    }

    /**
     * 取得索引位置
     *
     * @return
     */
    private int getRowIndex() {
        return allCategoryViews.indexOf(selCategoryView) % 50;
    }

    /**
     * 搜尋
     */
    private void search() {
        allCategoryViews = otherCategoryHelper.findByStatusAndName(filterStatus, filterName);
        if (selCategoryView != null && !allCategoryViews.contains(selCategoryView)) {
            selCategoryView = null;
        }
    }

    /**
     * 執行存檔
     */
    public void btnSave() {
        try {
            otherCategoryHelper.save(categoryViewTo, loginBean.getUserSId());
            displayController.hidePfWidgetVar("dlg_category_wv");
            this.search();
            displayController.update("screen_view_dt");
        } catch (IllegalStateException e) {
            this.showMsg(e.getMessage());
        } catch (Exception e) {
            log.error("btnSave ERROR!!", e);
            this.showMsg("執行存檔錯誤，" + e.getMessage());
        }
    }

    /**
     * 取消存檔
     */
    public void btnCancelSave() {
        try {
            categoryViewTo = new TROtherCategoryTo();
            categoryTreeComponent.clearCateSids();
        } catch (Exception e) {
            log.error("btnCancelSave ERROR!!", e);
            this.showMsg("取消存檔錯誤，" + e.getMessage());
        }
    }

    /**
     * 開啟 類別樹
     */
    public void btnOpenCategoryTree() {
        try {
            categoryTreeComponent.selectedItem(categoryViewTo.getCateSids());
            displayController.showPfWidgetVar("dlg_Cate");
        } catch (Exception e) {
            log.error("btnOpenCategoryTree Error", e);
            categoryTreeCallBack.showMessage(e.getMessage());
        }
    }

    private final CategoryTreeCallBack categoryTreeCallBack = new CategoryTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -3625478321958406294L;

        @Override
        public void showMessage(String m) {
            showMsg(m);
        }

        @Override
        public void confirmSelCate() {
            try {
                categoryTreeComponent.selCate();
                categoryViewTo.setCateSids(categoryTreeComponent.getSmallDataCateSids());
                categoryTreeComponent.clearCateSids();
            } catch (Exception e) {
                log.error("confirmSelCate Error", e);
                this.showMessage(e.getMessage());
            }
        }
    };

    /**
     * 開啟訊息視窗
     *
     * @param m
     */
    private void showMsg(String m) {
        msg = m;
        displayController.update("dlg_msg_view");
        displayController.showPfWidgetVar("dlg_msg_wv");
    }
}
