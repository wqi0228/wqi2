package com.cy.tech.request.web.controller.search.helper;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.OverdueUnclosedVO;
import com.cy.tech.request.logic.service.CategorySettingService;
import com.cy.tech.request.logic.service.RequireProcessCloseService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.RequireShowService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.vo.category.BasicDataBigCategory;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.enums.ReqStatusMBean;
import com.cy.tech.request.web.controller.view.vo.OverdueUnclosedQueryVO;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.logic.lib.manager.WorkCommonReadRecordManager;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class OverdueUnclosedHelper {
    @Autowired
    private RequireService requireService;
    @Autowired
    private WkJsonUtils jsonUtils;
    @Autowired
    private URLService urlService;
    @Autowired
    private ReqularPattenUtils reqularUtils;
    @Autowired
    private RequireShowService requireShowService;
    @Autowired
    private RequireProcessCloseService requireProcessCloseService;
    @Autowired
    private NativeSqlRepository nativeSqlRepository;
    @Autowired
    private SearchResultHelper searchResultHelper;
    @Autowired
    transient private WorkCommonReadRecordManager workCommonReadRecordManager;
    @Autowired
    transient private CategorySettingService categorySettingService;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchHelper searchHelper;

    private List<OverdueUnclosedVO> convert(List<OverdueUnclosedVO> resultList, Integer loginUserSid) {
        for (OverdueUnclosedVO vo : resultList) {
            try {

                // 公用欄位處理
                vo.prepareCommonColumn();

                vo.setCreateUserName(WkUserUtils.findNameBySid(vo.getCreateUserSid()));
                vo.setInteStaffUserName(WkUserUtils.findNameBySid(vo.getInteStaffUserSid()));
                vo.setDeptName(WkOrgCache.getInstance().findNameBySid(vo.getDepSid()));

                // theme
                if (!Strings.isNullOrEmpty(vo.getRequireTheme())) {
                    vo.setRequireTheme(String.join(" ", jsonUtils.fromJsonToList(vo.getRequireTheme(), String.class)));
                }
                // open windows link
                vo.setLocalUrlLink(urlService.createLoacalURLLink(
                        URLService.URLServiceAttr.URL_ATTR_M,
                        urlService.createSimpleUrlTo(loginUserSid, vo.getRequireNo(), 1)));

            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return resultList;
    }

    public String close(
            String requireSid,
            String closeMemo,
            User loginUser,
            List<OverdueUnclosedVO> allItems) throws UserMessageException {

        // ====================================
        // 取得資料
        // ====================================
        Optional<OverdueUnclosedVO> optional = allItems.stream()
                .filter(vo -> WkCommonUtils.compareByStr(requireSid, vo.getRequireSid()))
                .findFirst();

        if (!optional.isPresent()) {
            String message = "找不到選擇單據 requireSid:[" + requireSid + "]";
            log.warn(message);
            throw new UserMessageException(message, InfomationLevel.WARN);
        }

        OverdueUnclosedVO overdueUnclosedVO = optional.get();

        // ====================================
        // 查詢主檔
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);
        if (require == null) {
            String message = String.format(
                    "[%s] 找不到選擇單據，可能資料已被異動",
                    overdueUnclosedVO.getRequireNo());
            log.error(message + " ,requireSid:[{}]", requireSid);
            throw new UserMessageException(message, InfomationLevel.WARN);
        }
        // ====================================
        // 檢核是否可進行結案
        // ====================================
        boolean isCanClose = false;
        boolean isForceCloce = false;
        if (requireShowService.showCloseBtn(require, loginUser.getSid())) {
            isCanClose = true;
        } else if (requireShowService.showForceCloseBtn(require, loginUser.getSid())) {
            isCanClose = true;
            isForceCloce = true;
        }

        if (!isCanClose) {
            String message = String.format(
                    "[%s]單據狀態無法結案.單據狀態可能已被異動",
                    overdueUnclosedVO.getRequireNo(), overdueUnclosedVO.getRequireTheme());
            log.info(message);
            throw new UserMessageException(message, InfomationLevel.WARN);
        }

        // ====================================
        // 結案
        // ====================================
        try {
            this.requireProcessCloseService.executeByBatchClose(
                    require,
                    loginUser,
                    isForceCloce,
                    closeMemo);
        } catch (SystemOperationException e) {
            String message = String.format(
                    "[%s]" + e.getMessage(),
                    overdueUnclosedVO.getRequireNo());
            throw new UserMessageException(message, InfomationLevel.ERROR);
        } catch (Exception e) {
            String message = String.format(
                    "[%s] 結案失敗!" + e.getMessage(),
                    overdueUnclosedVO.getRequireNo());
            throw new UserMessageException(message, InfomationLevel.ERROR);
        }

        // ====================================
        // 執行訊息
        // ====================================
        String message = String.format(
                "[%s]已" + (isForceCloce ? "強制結案" : "結案"),
                overdueUnclosedVO.getRequireNo());

        log.info(message);

        return message;
    }

    public List<OverdueUnclosedVO> queryOverdueUnclosedByCondition(OverdueUnclosedQueryVO queryVO) throws UserMessageException {
        // ====================================
        // 檢查
        // ====================================
        if (queryVO == null) {
            log.warn("傳入 queryVO 為空");
            throw new UserMessageException(WkMessage.SESSION_TIMEOUT, InfomationLevel.WARN);
        }

        // ====================================
        // 取得以單號查詢
        // ====================================
        String targetRequireNo = reqularUtils.getRequireNo(SecurityFacade.getCompanyId(), queryVO.getSearchText());

        // ====================================
        // 組 SQL
        // ====================================

        Map<String, Object> parameters = Maps.newHashMap();

        StringBuffer sql = new StringBuffer();
        sql.append("SELECT tr.require_sid                    AS sid, ");
        sql.append("       tr.require_sid                    AS requireSid, ");
        sql.append("       tr.require_no                     AS requireNo, ");
        sql.append("       tr.create_dt                      AS createDate, ");
        sql.append("       tr.create_usr                     AS createUserSid, ");
        sql.append("       tr.update_dt                      AS updateDate, ");
        sql.append("       tr.dep_sid                        AS depSid, ");
        sql.append("       tr.require_status                 AS requireStatus, ");
        sql.append("       tr.hope_dt                        AS hopeDate, ");
        sql.append("       tr.urgency                        AS urgency, ");
        sql.append("       tr.inte_staff                     AS inteStaffUserSid, ");
        sql.append("       tr.read_reason                    AS readReason, ");
        sql.append("       keyMapping.big_category_name      AS bigCategoryName, ");
        sql.append("       keyMapping.middle_category_name   AS middleCategoryName, ");
        sql.append("       keyMapping.small_category_name    AS smallCategoryName, ");
        sql.append("       tid.field_content                 AS requireTheme, ");
        sql.append("       Datediff(Date_format(Sysdate(), '%Y%m%d'), Date_format(tr.hope_dt, '%Y%m%d'))              AS overdueDays, ");
        // select 公用欄位
        sql.append(searchConditionSqlHelper.prepareCommonSelectColumnByRequireType2());

        sql.append(" FROM ");

        // FROM tr_require
        this.preparePartSQL_FromTrRequire(queryVO, targetRequireNo, sql, parameters);

        // INNER JOIN CategoryKeyMapping
        this.preparePartSQL_InnerJoinCategoryKeyMapping(queryVO, targetRequireNo, sql, parameters);

        // LEFT JOIN 主題+模糊查詢
        sql.append(
                this.searchHelper.buildRequireIndexDictionaryCondition(
                        "",
                        queryVO.getSearchText(),
                        targetRequireNo));

        // LEFT JOIN 閱讀記錄
        sql.append(searchConditionSqlHelper.prepareSubFormReadRecordJoin(
                SecurityFacade.getUserSid(), FormType.REQUIRE, "tr"));

        //
        sql.append(" WHERE 1=1 ");

        // 查詢條件：是否閱讀
        if (WkStringUtils.isEmpty(targetRequireNo)) {
            sql.append(
                    this.workCommonReadRecordManager.prepareWhereConditionSQL(
                            queryVO.getReadRecordType(), "readRecord"));
        }

        sql.append(" GROUP BY requireSid ");
        sql.append(" ORDER BY overdueDays DESC");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.OVERDUE_UNCLOSED, sql.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.OVERDUE_UNCLOSED, SecurityFacade.getUserSid());

        // ====================================
        // 查詢
        // ====================================
        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        List<OverdueUnclosedVO> resultList = this.nativeSqlRepository.getResultList(
                sql.toString(), parameters, OverdueUnclosedVO.class);

        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((resultList == null) ? 0 : resultList.size());

        if (WkStringUtils.notEmpty(resultList)) {

            // ====================================
            // 封裝
            // ====================================
            // 解析資料-開始
            usageRecord.parserDataStart();
            this.convert(resultList, SecurityFacade.getUserSid());
            // 解析資料-結束
            usageRecord.parserDataEnd();

            // ====================================
            // 後續處理
            // ====================================
            // 後續處理-開始
            usageRecord.afterProcessStart();

            // 若查詢條件含沒有"無待閱原因", 將資料移除
            resultList.removeIf(vo -> {
                if (!queryVO.getReasons().contains(ReqToBeReadType.NO_TO_BE_READ.name())) {
                    return vo.getReadReason() == null;
                }
                return false;
            });

            // 查詢條件過濾：逾期天數
            if (queryVO.getOverdueDays() != null) {
                resultList = resultList.stream()
                        .filter(vo -> vo.getOverdueDays() >= queryVO.getOverdueDays())
                        .collect(Collectors.toList());
            }

            // 查詢條件過濾：系統別
            resultList = resultList.stream()
                    .filter(each -> this.searchResultHelper.filterCheckItems(
                            each.getCheckItemTypes(),
                            queryVO.getCheckItemTypes(),
                            false,
                            null))
                    .collect(Collectors.toList());

            // 後續處理-結束
            usageRecord.afterProcessEnd();
        }
        
        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return resultList;

    }

    /**
     * 兜組部分SQL Inner Join CategoryKeyMapping
     * 
     * @param queryVO         查詢條件VO
     * @param targetRequireNo 指定需求單號
     * @param sql             SQL StringBuffer
     * @param parameters      parameters map
     */
    private void preparePartSQL_FromTrRequire(
            OverdueUnclosedQueryVO queryVO,
            String targetRequireNo,
            StringBuffer sql,
            Map<String, Object> parameters) {

        sql.append("( ");
        sql.append(" SELECT * ");
        sql.append("   FROM tr_require ");

        // ====================================
        // 固定條件
        // ====================================
        sql.append("  WHERE comp_sid = " + SecurityFacade.getCompanySid() + " ");
        sql.append("    AND close_code = 0 ");
        sql.append("    AND close_dt IS NULL ");
        sql.append("    AND Date(Sysdate()) >= hope_dt ");

        // ====================================
        // 固定 + 查詢條件：製作進度
        // ====================================
        List<String> requireStatus = queryVO.getRequireStatusTypes();
        if (WkStringUtils.notEmpty(targetRequireNo)
                || WkStringUtils.isEmpty(requireStatus)) {
            requireStatus = ReqStatusMBean.STATUS1.stream().map(each -> each.name()).collect(Collectors.toList());
        }
        sql.append("  AND require_status IN (:requireStatus) ");
        parameters.put("requireStatus", requireStatus);

        // ====================================
        // 查詢條件：需求單位
        // ====================================
        // 單位未挑選時或有指定需求單號時 ，預設所有可閱部門
        Set<Integer> createDepSids = queryVO.getCanViewDepSids();
        if (WkStringUtils.isEmpty(targetRequireNo)
                && WkStringUtils.notEmpty(queryVO.getCreateDepSids())) {
            createDepSids = queryVO.getCreateDepSids();
        }
        // 取得全公司單位
        Set<Integer> allDepSids = WkCommonUtils.safeStream(WkOrgCache.getInstance().findAllDepSidByCompID(SecurityFacade.getCompanyId()))
                .collect(Collectors.toSet());
        // 查詢範圍為全公司時, 無需加入查詢條件
        if (!WkCommonUtils.compare(createDepSids, allDepSids)) {
            sql.append("               AND dep_sid IN ( :createDepSids ) ");
            parameters.put("createDepSids", createDepSids);
        }

        // ====================================
        // 查詢條件：待閱原因
        // ====================================
        if (WkStringUtils.isEmpty(targetRequireNo)
                && ReqToBeReadType.values().length != queryVO.getReasons().size()) {
            sql.append("               AND (  ");
            sql.append("                     read_reason IN ( :reasons ) ");
            sql.append("                     req.read_reason IS NULL )  ");
            parameters.put("reasons", queryVO.getReasons());
        }

        // ====================================
        // 查詢條件：需求人員
        // ====================================
        if (WkStringUtils.isEmpty(targetRequireNo)
                && WkStringUtils.notEmpty(queryVO.getCreator())) {
            List<Integer> userSids = WkUserUtils.findByNameLike(queryVO.getCreator(), SecurityFacade.getCompanyId());
            if (userSids.size() < 1) {
                userSids.add(-999);
            }
            sql.append("               AND create_usr IN ( :userSids ) ");
            parameters.put("userSids", userSids);
        }

        // ====================================
        // 查詢條件：立單區間
        // ====================================
        if (WkStringUtils.isEmpty(targetRequireNo)
                && queryVO.getDateIntervalVO().getStartDate() != null) {
            sql.append("                AND create_dt >= :startDate  ");
            parameters.put("startDate", WkDateUtils.convertToOriginOfDay(queryVO.getDateIntervalVO().getStartDate()));
        }
        if (WkStringUtils.isEmpty(targetRequireNo)
                && queryVO.getDateIntervalVO().getEndDate() != null) {
            sql.append("                AND create_dt <= :endDate  ");
            parameters.put("endDate", WkDateUtils.convertToEndOfDay(queryVO.getDateIntervalVO().getEndDate()));
        }

        sql.append(") tr ");
    }

    /**
     * 兜組部分SQL Inner Join CategoryKeyMapping
     * 
     * @param queryVO         查詢條件VO
     * @param targetRequireNo 指定需求單號
     * @param sql             SQL StringBuffer
     * @param parameters      parameters map
     */
    private void preparePartSQL_InnerJoinCategoryKeyMapping(
            OverdueUnclosedQueryVO queryVO,
            String targetRequireNo,
            StringBuffer sql,
            Map<String, Object> parameters) {

        sql.append("       INNER JOIN tr_category_key_mapping keyMapping ");
        sql.append("               ON keyMapping.key_sid = tr.mapping_sid ");

        // 有指定需求單號時，以下查詢條件忽略
        if (WkStringUtils.isEmpty(targetRequireNo)) {
            return;
        }

        // 查詢條件：大類
        Set<String> bigCategorySids = Sets.newHashSet();
        if (WkStringUtils.notEmpty(queryVO.getBigCategorySids())) {
            bigCategorySids.addAll(queryVO.getBigCategorySids());
        }
        if (WkStringUtils.notEmpty(queryVO.getCategoryCombineVO().getBigDataCateSids())) {
            bigCategorySids.addAll(queryVO.getCategoryCombineVO().getBigDataCateSids());
        }
        if (WkStringUtils.notEmpty(bigCategorySids)) {
            Set<String> allBigSids = WkCommonUtils.safeStream(this.categorySettingService.findAllBig())
                    .map(BasicDataBigCategory::getSid)
                    .collect(Collectors.toSet());

            if (!WkCommonUtils.compare(allBigSids, bigCategorySids)) {
                sql.append("           AND keyMapping.big_category_sid IN ( :bigCategorySids )  ");
                parameters.put("bigCategorySids", bigCategorySids);
            }
        }

        // 查詢條件：中類
        if (WkStringUtils.notEmpty(queryVO.getCategoryCombineVO().getMiddleDataCateSids())) {
            sql.append("           AND keyMapping.middle_category_sid IN ( :middleCategorySids )  ");
            parameters.put("middleCategorySids", queryVO.getCategoryCombineVO().getMiddleDataCateSids());
        }

        // 查詢條件：小類
        if (WkStringUtils.notEmpty(queryVO.getCategoryCombineVO().getSmallDataCateSids())) {
            sql.append("           AND keyMapping.small_category_sid IN ( :smallCategorySids )  ");
            parameters.put("smallCategorySids", queryVO.getCategoryCombineVO().getSmallDataCateSids());
        }
    }

}
