/**
 * 
 */
package com.cy.tech.request.web.controller.component.assigndep.vo;

import java.io.Serializable;
import java.util.List;

import com.cy.tech.request.logic.vo.PtCheckTo;
import com.cy.tech.request.logic.vo.WorkOnpgTo;
import com.cy.tech.request.logic.vo.WorkTestInfoTo;
import com.cy.tech.request.vo.enums.ReqConfirmDepCompleteType;
import com.cy.tech.request.vo.enums.ReqConfirmDepProgStatus;

import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 *
 */
public class AssignUnitVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7762249434385532045L;

    /**
     * 單位 SID
     */
    @Setter
    @Getter
    private Integer depSid;

    /**
     * 單位名稱
     */
    @Setter
    @Getter
    private String depName;
    
    /**
     * 單位名稱
     */
    @Setter
    @Getter
    private String depFullName;

    /**
     * 負責人名稱
     */
    @Setter
    @Getter
    private String ownerName;

    /**
     * 處理進度
     */
    @Setter
    @Getter
    private ReqConfirmDepProgStatus progressStatus;
    
    @Setter
    @Getter
    private ReqConfirmDepCompleteType completeType;
    
    /**
     * 原型確認單資訊
     */
    @Setter
    @Getter
    List<PtCheckTo> ptChecks;
    
    /**
     * 送測單資訊
     */
    @Setter
    @Getter
    List<WorkTestInfoTo> workTestInfos;
    
    /**
     * 
     */
    @Setter
    @Getter
    List<WorkOnpgTo> onpgInfos;

    /**
     * 進度條 CSS
     * 
     * @return
     */
    public String getProgressStyle() {
        if (this.progressStatus == null) {
            return "";
        }
        return "width:" + this.progressStatus.getProgressPercent() + "%;"
                + "background-color:" + this.progressStatus.getProgressColer() + ";";
    }
    
    public String getProgressHtml() {
        return "<div class='assignDepWorkPanel-progress'>"
                + "<div class='assignDepWorkPanel-progress-bar' style='" + this.getProgressStyle()+ "' /></div>";
    }
    
    public String getCompleteResultTooltip() {
        if(ReqConfirmDepProgStatus.COMPLETE.equals(this.getProgressStatus())) {
            return "2019/11/10 " + this.ownerName + " : balbalbalbalbal"; 
        }
        return "";
    }

    /**
     * 處理進度名稱
     * 
     * @return
     */
    public String getProgressStatusName() {
        if (this.progressStatus == null) {
            return "";
        }
        
        if(ReqConfirmDepProgStatus.COMPLETE.equals(this.getProgressStatus())) {
            if (this.completeType == null) {
                return this.progressStatus.getLabel();
            }
            return this.completeType.getLabel();
        }
        
        return this.progressStatus.getLabel();
    }
    
    public Integer getProgressPercent() {
        if (this.progressStatus == null) {
            return 0;
        }
        return this.progressStatus.getProgressPercent();
    }

    /**
     * 工作清單
     */
    @Setter
    @Getter
    private String workInfo;
    
    
    /**
     * 負責人名稱
     */
    @Setter
    @Getter
    private Integer sortSeq = 0;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((depSid == null) ? 0 : depSid.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AssignUnitVO other = (AssignUnitVO) obj;
        if (depSid == null) {
            if (other.depSid != null)
                return false;
        } else if (!depSid.equals(other.depSid))
            return false;
        return true;
    }
}
