package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.omnifaces.util.Faces;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.view.OverdueUnclosedVO;
import com.cy.tech.request.logic.service.CategorySettingService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.utils.RequireSkypeAlertHelper;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.search.helper.OverdueUnclosedHelper;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.view.component.searchquery.OverdueUnclosedCustomQueryVO;
import com.cy.tech.request.web.controller.view.vo.OverdueUnclosedQueryVO;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.pf.utils.ConfirmCallbackDialogController;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.logic.WkUserAndOrgLogic;
import com.cy.work.viewcomponent.treepker.TreePickerCallback;
import com.cy.work.viewcomponent.treepker.TreePickerComponent;
import com.cy.work.viewcomponent.treepker.helper.TreePickerDepHelper;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author aken_kao
 *             逾期未結案需求報表
 */
@Scope("view")
@Controller
@Slf4j
@ManagedBean
public class OverdueUnclosedMBean extends PaginationMBean<OverdueUnclosedVO> implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 7212885383037907446L;
    @Autowired
    private LoginBean loginBean;
    @Autowired
    private TableUpDownBean upDownBean;
    @Autowired
    private DisplayController display;
    @Autowired
    private OverdueUnclosedHelper overdueUnclosedHelper;
    @Autowired
    private RequireService requireService;
    @Autowired
    private ReqLoadBean loadManager;
    @Autowired
    private SearchHelper searchHelper;
    @Autowired
    private WorkBackendParamHelper workBackendParamHelper;
    @Autowired
    transient private CategorySettingService categorySettingService;
    @Autowired
    transient private TreePickerDepHelper treePickerDepHelper;
    @Autowired
    transient private DisplayController displayController;
    @Autowired
    transient private WkUserAndOrgLogic wkUserAndOrgLogic;
    @Autowired
    transient private ConfirmCallbackDialogController confirmCallbackDialogController;
    @Getter
    @Setter
    /** 顯示dataTable裡的元件 */
    private boolean displayButton = true;
    @Getter
    @Setter
    /** 顯示自訂義按鈕裡的元件 */
    private boolean displayCustomBtn = true;
    @Getter
    private boolean portalMode = false;
    @Getter
    /** 切換模式 */
    private SwitchType switchType = SwitchType.CONTENT;
    @Getter
    @Setter
    private OverdueUnclosedQueryVO queryVO;
    @Getter
    @Setter
    private OverdueUnclosedCustomQueryVO customQueryVO = new OverdueUnclosedCustomQueryVO();

    @Getter
    private final String dataTableId = "dtRequire";

    @Getter
    @Setter
    private String batchCloseSidsStr = "";

    @PostConstruct
    public void init() {
        queryVO = new OverdueUnclosedQueryVO(
                loginBean.getDep(),
                loginBean.getUser(),
                ReportType.OVERDUE_UNCLOSED,
                categorySettingService.findAllBig());
        // if from portal, init default depts
        portalMode = Faces.getRequestParameter("mode") != null;
        initQueryVO();
    }

    public void initQueryVO() {
        queryVO.init();
        customQueryVO.initCustomQueryVO();
        this.initConditionCreateDepSids();
        if (portalMode) {
            displayCustomBtn = false;
            this.queryVO.setOverdueDays(0);
        } else {
            loadDefaultSetting();
        }
    }

    /** 載入自訂搜尋條件設定 */
    public void loadDefaultSetting() {
        queryVO.publicConditionInit();
        queryVO.setBigCategorySids(customQueryVO.getBigCategorySids());
        queryVO.setRequireStatusTypes(customQueryVO.getRequireStatusTypes());
        queryVO.setReadRecordType(customQueryVO.getReadRecordType());
        queryVO.setReasons(customQueryVO.getReasons());
        queryVO.setCreator(customQueryVO.getCreator());
        queryVO.getCategoryCombineVO().getCategoryTreeComponent().selectedItem(customQueryVO.getCategoryCombineVO().getSmallDataCateSids());
        queryVO.getCategoryCombineVO().setSmallDataCateSids(customQueryVO.getCategoryCombineVO().getSmallDataCateSids());
        queryVO.getDateIntervalVO().init(Strings.isNullOrEmpty(customQueryVO.getDateTypeIndex()) ? -1
                : Integer.valueOf(customQueryVO.getDateTypeIndex()));
        queryVO.setSearchText(customQueryVO.getSearchText());

        // ====================================
        // 需求單位
        // ====================================
        final Set<Integer> finalCreateDepSids = queryVO.getCanViewDepSids().stream()
                .collect(Collectors.toSet());

        // 由定條件取回
        Set<Integer> createDepSids = WkCommonUtils.safeStream(customQueryVO.getOrgTreeVO().getRequireDepts())
                .filter(depSid -> WkStringUtils.isNumber(depSid))
                .map(depSid -> Integer.parseInt(depSid))
                .filter(depSid -> finalCreateDepSids.contains(depSid)) // 排除已經不可閱的自訂條件
                .collect(Collectors.toSet());

        queryVO.setCreateDepSids(createDepSids);

    }

    public void saveReportCustomFilter() {
        customQueryVO.saveReportCustomFilter();
        loadDefaultSetting();
        search();
    }

    public void search() {
        try {
            queryItems = overdueUnclosedHelper.queryOverdueUnclosedByCondition(queryVO);
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
        }
    }

    /**
     * 清除按鈕
     */
    public void clearBtn() {
        try {
            initQueryVO();
            search();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 匯出時，隱藏dataTable按鈕元件
     */
    public void disableButton() {
        displayButton = false;
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        Date startDate = queryVO.getDateIntervalVO().getStartDate();
        Date endDate = queryVO.getDateIntervalVO().getEndDate();
        ReportType reportType = queryVO.getReportType();
        searchHelper.exportExcel(document, startDate, endDate, reportType);
        displayButton = true;
    }

    /**
     * 點選dataTable 主題 link
     */
    public void showDetail(OverdueUnclosedVO selectedResult) {
        querySelection = selectedResult;
        queryKeeper = selectedResult;
        switchType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();

    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (!SwitchType.FULLCONTENT.equals(switchType)) {
            int status = switchType.ordinal();
            int xor = status ^= 1;
            switchType = SwitchType.values()[xor];
        }
        loadRequireMaster();
    }

    /**
     * 讀取需求單主檔
     */
    public void loadRequireMaster() {
        if (querySelection != null) {
            queryKeeper = querySelection;
        } else if (this.queryKeeper == null) {
            this.querySelection = this.queryKeeper = this.queryItems.get(0);
        }
        this.changeRequireContent(queryKeeper);
    }

    /**
     * 需求單明細，切換到data list require03_title_btn.xhtml, 共同元件
     */
    public void normalScreenReport() {
        querySelection = queryKeeper;
        switchType = SwitchType.CONTENT;
    }

    @Override
    protected void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
        display.update("searchBody");

    }

    @Override
    protected void highlightReportTo(String widgetVar, String pageCount, OverdueUnclosedVO selectedResult) {
        querySelection = selectedResult;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + super.getRowIndex(pageCount) + ");");

    }

    @Override
    protected void changeRequireContent(OverdueUnclosedVO vo) {
        Require r = requireService.findByReqNo(vo.getRequireNo());
        loadManager.reloadReqForm(r, loginBean.getUser());

    }

    /**
     * refresh dataTable
     */
    public void refreshDataTable() {
        search();
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (OverdueUnclosedVO) event.getObject();
        this.changeRequireContent(this.queryKeeper);
    }

    /**
     * 顯示批次結案相關功能
     * 
     * @return
     */
    public boolean isShowBatchClose() {
        // 無查詢結果不可使用
        if (WkStringUtils.isEmpty(this.queryItems)) {
            return false;
        }

        // 有任何一筆結果不是『已完成』就不可使用
        Optional<?> notCompleted = this.queryItems.stream()
                .filter(item -> !RequireStatusType.COMPLETED.equals(item.getRequireStatus()))
                .findFirst();

        if (notCompleted.isPresent()) {
            return false;
        }

        // GM管理者才可以使用
        if (!workBackendParamHelper.isGMAdmin(SecurityFacade.getUserSid())) {
            return false;
        }

        return true;
    }

    public void beforeBatchClose() {
        // ====================================
        // 取得畫面選擇的資料
        // ====================================
        List<String> batchCloseSids = Lists.newArrayList();
        if (WkStringUtils.notEmpty(this.batchCloseSidsStr)) {
            try {
                batchCloseSids = WkJsonUtils.getInstance().fromJsonToList(this.batchCloseSidsStr, String.class);
            } catch (Exception e) {
                String message = WkMessage.PROCESS_FAILED + "(解析選擇資料失敗!)";
                log.error("\r\nbatchCloseSidsStr[{}]", this.batchCloseSidsStr);
                log.error(message + e.getMessage(), e);
                MessagesUtils.showError(message);
                return;
            }
        }
        // ====================================
        // 尚未選擇
        // ====================================
        if (WkStringUtils.isEmpty(batchCloseSids)) {
            MessagesUtils.showWarn("請至少選擇一筆要結案的單據!");
            return;
        }

        // ====================================
        // 確認視窗
        // ====================================
        String confirmMessage = String.format("您已選擇[%s]筆單據，將進行結案<br/>是否確定?", batchCloseSids.size());
        confirmCallbackDialogController.showConfimDialog(
                confirmMessage,
                Lists.newArrayList("searchClauses", "searchBody"),
                () -> batchClose());

    }

    /**
     * 批次結案
     */
    public void batchClose() {

        User loginUser = WkUserCache.getInstance().findBySid(SecurityFacade.getUserSid());
        if (loginUser == null) {
            MessagesUtils.showWarn(WkMessage.SESSION_TIMEOUT);
            return;
        }
        if (this.queryItems == null) {
            MessagesUtils.showWarn(WkMessage.SESSION_TIMEOUT);
            return;
        }

        // ====================================
        // 取得畫面選擇的資料
        // ====================================
        List<String> batchCloseSids = Lists.newArrayList();
        if (WkStringUtils.notEmpty(this.batchCloseSidsStr)) {
            try {
                batchCloseSids = WkJsonUtils.getInstance().fromJsonToList(this.batchCloseSidsStr, String.class);
            } catch (Exception e) {
                String message = WkMessage.PROCESS_FAILED + "(解析選擇資料失敗!)";
                log.error("\r\nbatchCloseSidsStr[{}]", this.batchCloseSidsStr);
                log.error(message + e.getMessage(), e);
                MessagesUtils.showError(message);
                return;
            }
        }
        // ====================================
        // 尚未選擇
        // ====================================
        if (WkStringUtils.isEmpty(batchCloseSids)) {
            MessagesUtils.showWarn("請至少選擇一筆要結案的單據!");
            return;
        }

        // ====================================
        // 批次結案
        // ====================================
        List<String> successMessages = Lists.newArrayList();
        List<String> failMessages = Lists.newArrayList();
        for (String requireSid : batchCloseSids) {
            try {
                String successMessage = this.overdueUnclosedHelper.close(requireSid, "批次結案", loginUser, this.queryItems);
                successMessages.add(successMessage);
            } catch (UserMessageException e) {
                failMessages.add(e.getMessage());
            } catch (Exception e) {
                String message = String.format(
                        "[%s] 結案失敗!" + e.getMessage(),
                        requireSid);
                log.error(message, e);
                RequireSkypeAlertHelper.getInstance().sendSkypeAlert(message, SecurityFacade.getCompanyId());
                failMessages.add(e.getMessage());
            }
        }

        // ====================================
        // 重新查詢
        // ====================================
        this.search();

        // ====================================
        // 兜組回應訊息
        // ====================================

        List<String> resultMessages = Lists.newArrayList();
        resultMessages.add(WkHtmlUtils.addBlueBlodClass("執行完成，結果如下："));
        resultMessages.add("<br/>");

        if (WkStringUtils.notEmpty(successMessages)) {
            int nomorlClose = 0;
            int forceClose = 0;
            for (String message : successMessages) {
                if (message.contains("強制結案")) {
                    forceClose++;
                } else {
                    nomorlClose++;
                }
            }

            if (nomorlClose > 0) {
                resultMessages.add("完成一般結案:[" + nomorlClose + "]筆");
            }

            if (forceClose > 0) {
                resultMessages.add("完成強制結案:[" + forceClose + "]筆");
            }
        }

        if (WkStringUtils.notEmpty(failMessages)) {
            if (WkStringUtils.notEmpty(successMessages)) {
                resultMessages.add("<br/>");
                resultMessages.add("<br/>");
            }

            resultMessages.add(WkHtmlUtils.addRedBlodClass("執行失敗[" + failMessages.size() + "]筆"));
            resultMessages.add("<br/>");
            resultMessages.addAll(failMessages);
        }

        String resultMessage = resultMessages.stream()
                .collect(Collectors.joining("<br/>"));

        if (WkStringUtils.notEmpty(failMessages)) {
            MessagesUtils.showWarn(resultMessage);
        } else {
            MessagesUtils.showInfo(resultMessage);
        }
    }

    // ========================================================================
    // 單位挑選樹
    // ========================================================================
    /**
     * 單位挑選
     */
    @Getter
    @Setter
    private transient TreePickerComponent depTreePicker;

    @Getter
    private final String DEP_TREE_PICKER_DLG_WV = this.getClass().getSimpleName() + "DEP_TREE_PICKER_DLG_WV";
    @Getter
    private final String DEP_TREE_PICKER_CONTENT_CLASS = this.getClass().getSimpleName() + "DEP_TREE_PICKER_CONTENT_CLASS";
    @Getter
    private final String DEP_TREE_PICKER_SHOW_SELECTED_CLASS = this.getClass().getSimpleName() + "DEP_TREE_PICKER_SHOW_SELECTED_CLASS";

    /**
     * 
     */
    private void initConditionCreateDepSids() {

        Set<Integer> canViewDepSids = Sets.newHashSet();
        Set<Integer> createDepSids = Sets.newHashSet();

        if (workBackendParamHelper.isGM(loginBean.getUserSId())) {
            // GM可閱全部單位
            canViewDepSids = WkCommonUtils.safeStream(WkOrgCache.getInstance().findAllDepSidByCompID(SecurityFacade.getCompanyId()))
                    .collect(Collectors.toSet());
        } else {
            //
            canViewDepSids = this.wkUserAndOrgLogic.prepareCanViewDepSidBaseOnOrgLevelWithParallel(
                    SecurityFacade.getUserSid(),
                    OrgLevel.MINISTERIAL,
                    true);
        }

        if (portalMode
                && customQueryVO != null
                && customQueryVO.getOrgTreeVO() != null
                && WkStringUtils.notEmpty(customQueryVO.getOrgTreeVO().getRequireDepts())) {
            // 預設
            createDepSids = wkUserAndOrgLogic.prepareCanViewDepSidsBaseOnMinsterial(SecurityFacade.getUserSid(), false);
        } else {

            final Set<Integer> finalCreateDepSids = canViewDepSids.stream()
                    .collect(Collectors.toSet());

            // 由定條件取回
            createDepSids = WkCommonUtils.safeStream(customQueryVO.getOrgTreeVO().getRequireDepts())
                    .filter(depSid -> WkStringUtils.isNumber(depSid))
                    .map(depSid -> Integer.parseInt(depSid))
                    .filter(depSid -> finalCreateDepSids.contains(depSid)) // 排除已經不可閱的自訂條件
                    .collect(Collectors.toSet());

        }

        this.queryVO.setCanViewDepSids(canViewDepSids);
        this.queryVO.setCreateDepSids(createDepSids);
    }

    /**
     * 顯示所選取的單位
     * 
     * @return
     */
    public String getSelectedDepsInfo() {
        Set<Integer> requireDepts = this.queryVO.getCreateDepSids();
        if (WkStringUtils.isEmpty(requireDepts)
                || WkCommonUtils.compare(requireDepts, this.queryVO.getCanViewDepSids())) {
            return "全部顯示";
        }

        return WkOrgUtils.findNameBySid(WkOrgUtils.sortByOrgTree(requireDepts), "、");
    }

    /**
     * 單位樹選單 - 開啟選單
     */
    public void event_dialog_depTreePicker_open() {

        try {
            this.depTreePicker = new TreePickerComponent(depTreePickerCallback, "單位挑選");
            // GM為全單位，預設不展開
            if (workBackendParamHelper.isGM(loginBean.getUserSId())) {
                this.depTreePicker.setAlwaysCollapseNode(true);
            }
            this.depTreePicker.rebuild();
        } catch (Exception e) {
            log.error("開啟【單位挑選】設定視窗失敗!" + e.getMessage(), e);
            MessagesUtils.showError("開啟【單位挑選】設定視窗失敗!");
            return;
        }

        displayController.showPfWidgetVar(this.DEP_TREE_PICKER_DLG_WV);
    }

    /**
     * 單位樹選單 - 關閉選單
     */
    public void event_dialog_depTreePicker_confirm() {

        // 取得元件中被選擇的項目
        this.queryVO.setCreateDepSids(this.depTreePicker.getSelectedItemIntegerSids());

        // 畫面控制
        displayController.hidePfWidgetVar(this.DEP_TREE_PICKER_DLG_WV);
    }

    /**
     * 分派通知單位選單 - callback 事件
     */
    private final TreePickerCallback depTreePickerCallback = new TreePickerCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = -2685194232972981152L;

        @Override
        public List<WkItem> prepareAllItems() throws Exception {
            // ====================================
            // 轉 WkItem
            // ====================================
            return treePickerDepHelper.prepareDepItems(
                    SecurityFacade.getCompanyId(),
                    Lists.newArrayList(queryVO.getCanViewDepSids()));

        }

        /**
         * 準備已選擇項目
         * 
         * @return
         */
        @Override
        public List<String> prepareSelectedItemSids() throws Exception {

            if (WkStringUtils.isEmpty(queryVO.getCreateDepSids())) {
                return Lists.newArrayList();
            }

            // 回傳
            return queryVO.getCreateDepSids().stream().map(sid -> sid + "").collect(Collectors.toList());
        }

        @Override
        public List<String> prepareDisableItemSids() throws Exception {
            // 沒有不可選擇的單位
            return Lists.newArrayList();
        }
    };
}
