/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search.helper;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search04QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.Search04View;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.SpecificPermissionService;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.web.view.to.search.query.SearchQuery04;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 輔助 Search04MBean
 *
 * @author kasim
 */
@Component
public class Search04Helper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6251765627742590272L;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private Search04QueryService search04QueryService;
    @Autowired
    transient private OrganizationService orgService;
    @Autowired
    transient private SpecificPermissionService spService;
    @Autowired
    transient private WorkBackendParamHelper workBackendParamHelper;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchResultHelper searchResultHelper;

    /**
     * 取得預設查詢部門
     *
     * @param loginDepSid
     * @param roles
     * @param hasCateConfirmCheck
     * @return
     */
    public List<String> getDefaultDepSids(Integer loginDepSid, List<Long> roleSids) {
        // 若為GM, 可看到全公司的退件
        if (workBackendParamHelper.isGM(SecurityFacade.getUserSid())) {
            Org comp = WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId());
            if (comp != null) {
                loginDepSid = comp.getSid();
            }
        }
        Set<String> results = Sets.newHashSet(String.valueOf(loginDepSid));
        return Lists.newArrayList(
                this.addSpDepts(
                        roleSids,
                        this.getDefaultDepSids(loginDepSid, results)));
    }

    /**
     * 取得預設查詢部門
     *
     * @param depSid
     * @param results
     * @return
     */
    private Set<String> getDefaultDepSids(Integer depSid, Set<String> results) {
        orgService.findByParentSid(depSid).forEach(each -> {
            results.add(String.valueOf(each.getSid()));
            this.getDefaultDepSids(each.getSid(), results);
        });
        return results;
    }

    /**
     * 初始化特殊權限部門
     *
     * @param roles
     * @param results
     * @return
     */
    private Set<String> addSpDepts(List<Long> roleSids, Set<String> results) {
        List<Org> spDepts = spService.findSpecificDeptsByRoleSidIn(roleSids);
        spDepts.forEach(each -> {
            results.add(String.valueOf(each.getSid()));
        });
        return results;
    }

    /**
     * 取得 關聯檢視 網址
     *
     * @param view
     * @return
     */
    public String getRelevanceViewUrl(Search04View view) {
        if (view == null) {
            return "";
        }
        return helper.getRelevanceViewUrl(view.getRequireNo());
    }

    /**
     * 查詢
     *
     * @param loginDep
     * @param loginUser
     * @param hasCanCheckUserRight
     * @param query
     * @return
     */
    public List<Search04View> search(
            String compId,
            Org loginDep,
            User loginUser,
            SearchQuery04 query) {

        String requireNo = reqularUtils.getRequireNo(compId, query.getFuzzyText());
        StringBuilder builder = new StringBuilder();
        Map<String, Object> parameters = Maps.newHashMap();

        builder.append("SELECT ");
        builder.append("        tr.require_sid, ");
        builder.append("        tr.require_no, ");
        builder.append("        tid.field_content, ");
        builder.append("        tr.urgency, ");
        builder.append("        tr.has_forward_dep, ");
        builder.append("        tr.has_forward_member, ");
        builder.append("        tr.has_link, ");
        builder.append("        trt.create_dt AS createdDate, ");
        builder.append("        trt.create_usr AS createdUser, ");
        builder.append("        tr.create_dt, ");
        builder.append("        ckm.big_category_name, ");
        builder.append("        ckm.middle_category_name, ");
        builder.append("        ckm.small_category_name, ");
        builder.append("        tr.dep_sid, ");
        builder.append("        tr.create_usr, ");
        builder.append("        tr.require_status, ");
        builder.append("        trt.reason, ");
        builder.append("        trt.require_trace_content, ");
        this.builderHasTraceColumn(loginDep, loginUser, builder);
        // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
        builder.append(this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire());

        builder.append("FROM ( ").append("\n");
        this.buildRequireCondition(query, requireNo, builder);
        this.buildRequireIndexDictionaryCondition(query, requireNo, builder, parameters);
        this.buildRequireTraceCondition(query, requireNo, builder);
        this.buildCategoryKeyMappingCondition(query, requireNo, builder);
        // 檢查項目 (系統別)
        // 後方需對 tr.require_sid 做 group by
        builder.append(this.searchConditionSqlHelper.prepareCommonJoin(SecurityFacade.getUserSid()));

        builder.append("WHERE tr.require_sid IS NOT NULL ");
        builder.append(" GROUP BY tr.require_sid ");

        // ====================================
        // 查詢
        // ====================================
        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.REJECT, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.REJECT, SecurityFacade.getUserSid());

        // 查詢
        List<Search04View> resultList = search04QueryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // ====================================
        // 後續處理
        // ====================================
        if (WkStringUtils.notEmpty(resultList)) {
            // 後續處理-開始
            usageRecord.afterProcessStart();
            // 系統別查詢條件過濾
            resultList = resultList.stream()
                    .filter(each -> this.searchResultHelper.filterCheckItems(
                            each.getCheckItemTypes(),
                            query.getCheckItemTypes(),
                            false,
                            null))
                    .collect(Collectors.toList());
            // 後續處理-結束
            usageRecord.afterProcessEnd();
        }

        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return resultList;
    }

    /**
     * 是否有追蹤資料
     *
     * @param loginDep
     * @param loginUser
     * @param builder
     */
    private void builderHasTraceColumn(Org loginDep, User loginUser, StringBuilder builder) {
        builder.append("( ").append("\n");
        builder.append("    SELECT CASE WHEN (COUNT(trace.tracesid) > 0) THEN 'TRUE' ELSE 'FALSE' END ").append("\n");
        builder.append("    FROM work_trace_info trace ").append("\n");
        builder.append("        WHERE trace.trace_source_no = tr.require_no ").append("\n");
        builder.append("        AND trace.trace_source_type = 'TECH_REQUEST' ").append("\n");
        builder.append("        AND ( ").append("\n");
        builder.append("            trace.trace_dep = ").append(loginDep.getSid()).append("\n");
        builder.append("            OR ").append("\n");
        builder.append("            trace.trace_usr = ").append(loginUser.getSid()).append("\n");
        builder.append("        ) ").append("\n");
        builder.append("        AND trace.trace_status = 'UN_FINISHED' ").append("\n");
        builder.append("        AND trace.Status = '0' ").append("\n");
        builder.append(") AS hasTrace, ").append("\n");
    }

    /**
     * 組合需求單部分
     *
     * @param hasCanCheckUserRight
     * @param searchQuery
     * @param requireNo
     * @param builder
     */
    private void buildRequireCondition(
            SearchQuery04 searchQuery,
            String requireNo,
            StringBuilder builder) {
        builder.append("    SELECT * FROM tr_require tr ").append("\n");
        // 退件通知碼(退件暫存單據查詢條件)
        builder.append("        WHERE tr.back_code = 1 ").append("\n");
        // 需求類別製作進度 (鎖定退件)
        builder.append(" AND tr.require_status = 'ROLL_BACK_NOTIFY'");

        if (!Strings.isNullOrEmpty(requireNo)) {
            // 需求單號
            builder.append("        AND tr.require_no = '").append(requireNo).append("' ").append("\n");
        } else {

            // 需求單位
            if (WkStringUtils.notEmpty(searchQuery.getRequireDepts())) {
                String depSidsStr = searchQuery.getRequireDepts().stream()
                        .collect(Collectors.joining(","));
                builder.append("        AND tr.dep_sid IN (").append(depSidsStr).append(") ").append("\n");
            }

            // 立單區間
            if (searchQuery.getStartDate() != null && searchQuery.getEndDate() != null) {
                builder.append("        AND tr.create_dt BETWEEN '")
                        .append(helper.transStrByStartDate(searchQuery.getStartDate())).append("' AND '")
                        .append(helper.transStrByEndDate(searchQuery.getEndDate())).append("' ").append("\n");
            } else if (searchQuery.getStartDate() != null) {
                builder.append("        AND tr.create_dt >= '")
                        .append(helper.transStrByStartDate(searchQuery.getStartDate())).append("' ").append("\n");
            } else if (searchQuery.getEndDate() != null) {
                builder.append("        AND tr.create_dt <= '")
                        .append(helper.transStrByEndDate(searchQuery.getEndDate())).append("' ").append("\n");
            }

            // 需求人員
            if (!Strings.isNullOrEmpty(searchQuery.getTrCreatedUserName())) {
                // 模糊查詢符合 user
                List<Integer> userSids = WkUserUtils.findByNameLike(searchQuery.getTrCreatedUserName(), SecurityFacade.getCompanyId());
                if (WkStringUtils.isEmpty(userSids)) {
                    userSids.add(-999);
                }

                if (WkStringUtils.notEmpty(userSids)) {
                    String userSidsStr = userSids.stream()
                            .map(each -> String.valueOf(each))
                            .collect(Collectors.joining(","));
                    builder.append("        AND tr.create_usr IN (").append(userSidsStr).append(") ");
                } else {
                    // 沒有符合 user , 強制查不到
                    builder.append("        AND tr.create_usr = -999 ");
                }

                if (userSids != null && !userSids.isEmpty()) {

                }

            }
            // 異動區間
            if (searchQuery.getStartUpdatedDate() != null && searchQuery.getEndUpdatedDate() != null) {
                builder.append("        AND tr.update_dt BETWEEN '")
                        .append(helper.transStrByStartDate(searchQuery.getStartUpdatedDate())).append("' AND '")
                        .append(helper.transStrByEndDate(searchQuery.getEndUpdatedDate())).append("' ").append("\n");
            } else if (searchQuery.getStartUpdatedDate() != null) {
                builder.append("        AND tr.update_dt >= '")
                        .append(helper.transStrByStartDate(searchQuery.getStartUpdatedDate())).append("' ").append("\n");
            } else if (searchQuery.getEndUpdatedDate() != null) {
                builder.append("        AND tr.update_dt <= '")
                        .append(helper.transStrByEndDate(searchQuery.getEndUpdatedDate())).append("' ").append("\n");
            }
            // 緊急度
            if (searchQuery.getUrgencyList() != null && !searchQuery.getUrgencyList().isEmpty()) {
                String urgencyStr = searchQuery.getUrgencyList().stream()
                        .collect(Collectors.joining(","));
                builder.append("        AND tr.urgency IN (").append(urgencyStr).append(") ").append("\n");
            }
            // 需求單號
            if (!Strings.isNullOrEmpty(searchQuery.getRequireNo())) {
                String textNo = "'%" + reqularUtils.replaceIllegalSqlLikeStr(searchQuery.getRequireNo()) + "%'";
                builder.append("        AND tr.require_no LIKE ").append(textNo).append(" ").append("\n");
            }

        }
        builder.append(") AS tr ").append("\n");
    }

    /**
     * 組合模糊查詢語句
     *
     * @param query
     * @param requireNo
     * @param builder
     */
    private void buildRequireIndexDictionaryCondition(SearchQuery04 query, String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN ( ").append("\n");
        builder.append("    SELECT tid.require_sid,tid.field_content FROM tr_index_dictionary tid ").append("\n");
        builder.append("        WHERE tid.field_name='主題' ").append("\n");
        // 模糊搜尋
        if (Strings.isNullOrEmpty(requireNo)
                && !Strings.isNullOrEmpty(query.getFuzzyText())) {
            String text = "%" + reqularUtils.replaceIllegalSqlLikeStr(query.getFuzzyText()) + "%";
            builder.append("        AND ( ").append("\n");
            builder.append("            tid.require_sid IN ( ").append("\n");
            builder.append("                SELECT DISTINCT tid1.require_sid FROM tr_index_dictionary tid1 ").append("\n");
            builder.append("                    WHERE tid1.field_content LIKE :text ").append(" ").append("\n");
            builder.append("            ) ").append("\n");
            builder.append("            OR ").append("\n");
            builder.append("            tid.require_sid IN ( ").append("\n");
            builder.append("               SELECT DISTINCT trace.require_sid FROM tr_require_trace trace ").append("\n");
            builder.append("                  WHERE trace.require_trace_type = 'REQUIRE_INFO_MEMO' ").append("\n");
            builder.append("                   AND trace.require_trace_content LIKE :text ").append(" ").append("\n");
            builder.append("            ) ").append("\n");
            builder.append("        ) ").append("\n");

            parameters.put("text", text);
        }
        builder.append(") AS tid ON tr.require_sid=tid.require_sid ").append("\n");
    }

    /**
     * @param query
     * @param requireNo
     * @param builder
     */
    private void buildRequireTraceCondition(SearchQuery04 query, String requireNo, StringBuilder builder) {
        builder.append("INNER JOIN ( ").append("\n");
        builder.append(
                "    SELECT trt.require_sid,trt.create_dt,trt.create_usr,trt.reason,trt.require_trace_content,MAX(trt.create_dt) FROM tr_require_trace trt ")
                .append("\n");
        builder.append("        WHERE trt.require_trace_type = 'ROLL_BACK_NOTIFY' ").append("\n");
        if (Strings.isNullOrEmpty(requireNo)) {
            // 退件時間
            if (query.getStartRejectedDate() != null && query.getEndRejectedDate() != null) {
                builder.append("        AND trt.create_dt BETWEEN '")
                        .append(helper.transStrByStartDate(query.getStartRejectedDate())).append("' AND '")
                        .append(helper.transStrByEndDate(query.getEndRejectedDate())).append("' ").append("\n");
            } else if (query.getStartRejectedDate() != null) {
                builder.append("        AND trt.create_dt >= '")
                        .append(helper.transStrByStartDate(query.getStartRejectedDate())).append("' ").append("\n");
            } else if (query.getEndRejectedDate() != null) {
                builder.append("        AND trt.create_dt <= '")
                        .append(helper.transStrByEndDate(query.getEndRejectedDate())).append("' ").append("\n");
            }
            // 退件人員
            if (!Strings.isNullOrEmpty(query.getRejectUserName())) {
                String userSidsStr = "'isEmpty'";
                List<Integer> userSids = WkUserUtils.findByNameLike(query.getRejectUserName(), SecurityFacade.getCompanyId());
                if (WkStringUtils.isEmpty(userSids)) {
                    userSids.add(-999);
                }
                if (userSids != null && !userSids.isEmpty()) {
                    userSidsStr = userSids.stream()
                            .map(each -> String.valueOf(each))
                            .collect(Collectors.joining(","));
                }
                builder.append("        AND trt.create_usr IN (").append(userSidsStr).append(") ").append("\n");
            }
            // 退件原因
            if (!Strings.isNullOrEmpty(query.getRejectReason())) {
                builder.append(" AND trt.reason = '").append(query.getRejectReason()).append("' ").append("\n");
            }
        }
        builder.append("    GROUP BY trt.require_no ").append("\n");
        builder.append(") AS trt ON tr.require_sid = trt.require_sid ").append("\n");
    }

    /**
     * 組合模板查詢語句
     *
     * @param query
     * @param requireNo
     * @param builder
     */
    private void buildCategoryKeyMappingCondition(SearchQuery04 query, String requireNo, StringBuilder builder) {
        builder.append("INNER JOIN ( ").append("\n");
        builder.append("    SELECT * FROM tr_category_key_mapping ckm ").append("\n");
        builder.append("        WHERE 1=1 ").append("\n");
        if (Strings.isNullOrEmpty(requireNo)) {
            // 需求類別
            if (!Strings.isNullOrEmpty(query.getSelectBigCategorySid())
                    && query.getBigDataCateSids().isEmpty()) {
                builder.append("        AND ckm.big_category_sid = '")
                        .append(query.getSelectBigCategorySid()).append("' ").append("\n");
            }
            // 類別組合
            if (!query.getBigDataCateSids().isEmpty()
                    || !query.getMiddleDataCateSids().isEmpty()
                    || !query.getSmallDataCateSids().isEmpty()) {
                String bigs = "'isEmpty'";
                String middles = "'isEmpty'";
                String smalls = "'isEmpty'";
                if (!query.getBigDataCateSids().isEmpty()) {
                    bigs = query.getBigDataCateSids().stream()
                            .collect(Collectors.joining("','", "'", "'"));
                }
                if (!query.getMiddleDataCateSids().isEmpty()) {
                    middles = query.getMiddleDataCateSids().stream()
                            .collect(Collectors.joining("','", "'", "'"));
                }
                if (!query.getSmallDataCateSids().isEmpty()) {
                    smalls = query.getSmallDataCateSids().stream()
                            .collect(Collectors.joining("','", "'", "'"));
                }
                if (!Strings.isNullOrEmpty(query.getSelectBigCategorySid())) {
                    if ("'isEmpty'".equals(bigs)) {
                        bigs = "'" + query.getSelectBigCategorySid() + "'";
                    } else {
                        bigs = bigs + ",'" + query.getSelectBigCategorySid() + "'";
                    }
                }
                builder.append("        AND( ").append("\n");
                builder.append("            ckm.big_category_sid IN (").append(bigs).append(") ").append("\n");
                builder.append("            OR ").append("\n");
                builder.append("            ckm.middle_category_sid IN (").append(middles).append(") ").append("\n");
                builder.append("            OR ").append("\n");
                builder.append("            ckm.small_category_sid IN (").append(smalls).append(")").append("\n");
                builder.append("        ) ").append("\n");
            }
        }
        builder.append(") AS ckm ON tr.mapping_sid=ckm.key_sid ").append("\n");
    }
}
