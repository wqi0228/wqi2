/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search20QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.Search20View;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.URLService.URLServiceAttr;
import com.cy.tech.request.logic.service.setting.SettingCheckConfirmRightService;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.require.RequireForwardDeAndPersonMBean;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.searchheader.CommonHeaderMBean;
import com.cy.tech.request.web.controller.searchheader.Search20HeaderMBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.group.vo.WorkLinkGroup;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 退件資訊查詢(for portal代辦
 *
 * @author jason_h
 */
@Controller
@Scope("view")
@Slf4j
public class Search20MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7930931402683435251L;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private Search20HeaderMBean headerMBean;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private CategoryTreeMBean cateTreeMBean;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private URLService urlService;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private Require01MBean r01MBean;
    @Autowired
    transient private SettingCheckConfirmRightService settingCheckConfirmRightService;
    @Autowired
    transient private Search20QueryService search20QueryService;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchResultHelper searchResultHelper;

    /** 所有的需求單 */
    @Getter
    @Setter
    private List<Search20View> queryItems;

    /** 選擇的需求單 */
    @Getter
    @Setter
    private Search20View querySelection;

    /** 上下筆移動keeper */
    @Getter
    private Search20View queryKeeper;

    /** 切換模式 */
    @Getter
    private SwitchType switchType = SwitchType.CONTENT;

    /** 切換模式 - 全畫面狀態 */
    @Getter
    private SwitchType switchFullType = SwitchType.DETAIL;

    /** 在匯出的時候，某些內容需要隱藏 */
    @Getter
    private boolean hasDisplay = true;

    @Getter
    private final String dataTableId = "dtRequire";

    @PostConstruct
    public void init() {
        headerMBean.clear();
        this.search();
    }

    public void search() {
        queryItems = this.findWithQuery();
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        headerMBean.clear();
        this.search();
    }

    private List<Search20View> findWithQuery() {
        String requireNo = reqularUtils.getRequireNo(loginBean.getCompanyId(), commHeaderMBean.getFuzzyText());
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT "
                + "tr.require_sid,"
                + "tr.require_no,"
                + "tid.field_content,"
                + "tr.urgency,"
                + "tr.has_forward_dep,"
                + "tr.has_forward_member,"
                + "tr.has_link,"
                + "trt.create_dt AS createdDate,"
                + "trt.create_usr AS createdUser,"
                + "tr.create_dt,"
                + "ckm.big_category_name,"
                + "ckm.middle_category_name,"
                + "ckm.small_category_name,"
                + "tr.dep_sid,"
                + "tr.create_usr,"
                + "tr.require_status,"
                + "trt.reason,"
                + "trt.require_trace_content,"
                + this.builderHasTraceColumn() + ", "
                // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
                + this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire()
                + " FROM ");

        buildRequireCondition(requireNo, builder, parameters);
        buildRequireIndexDictionaryCondition(requireNo, builder, parameters);
        buildRequireTraceCondition(requireNo, builder, parameters);
        buildCategoryKeyMappingCondition(requireNo, builder, parameters);
        // 檢查項目 (系統別)
        // 後方需對 主單 sid 做 group by
        builder.append(this.searchConditionSqlHelper.prepareCommonJoin(SecurityFacade.getUserSid()));

        builder.append("WHERE tr.require_sid IS NOT NULL ");
        builder.append(" GROUP BY tr.require_sid ");
        builder.append("  ORDER BY tr.update_dt DESC");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.REJECT_DEP, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.REJECT_DEP, SecurityFacade.getUserSid());

        // 查詢
        List<Search20View> resultList = search20QueryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // ====================================
        // 後續處理
        // ====================================
        if (WkStringUtils.notEmpty(resultList)) {
            // 後續處理-開始
            usageRecord.afterProcessStart();
            // 查詢條件過濾：系統別
            resultList = resultList.stream()
                    .filter(each -> this.searchResultHelper.filterCheckItems(
                            each.getCheckItemTypes(),
                            commHeaderMBean.getCheckItemTypes(),
                            false,
                            null))
                    .collect(Collectors.toList());
            // 後續處理-結束
            usageRecord.afterProcessEnd();
        }

        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return resultList;
    }

    private void buildRequireCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("(SELECT * FROM tr_require tr WHERE 1=1");

        // 立單區間
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getStartDate() != null && commHeaderMBean.getEndDate() != null) {
            builder.append(" AND tr.create_dt BETWEEN :startDate AND :endDate");
            parameters.put("startDate", helper.transStartDate(commHeaderMBean.getStartDate()));
            parameters.put("endDate", helper.transEndDate(commHeaderMBean.getEndDate()));
        }

        // 退件通知碼(退件暫存單據查詢條件)
        builder.append(" AND tr.back_code = 1 ");

        // 需求單位 (同REST查詢條件
        if (!settingCheckConfirmRightService.hasCanCheckUserRight(SecurityFacade.getUserSid())) {
            builder.append(" AND tr.dep_sid IN (:depSids)");
            parameters.put("depSids", commHeaderMBean.getRequireDepts() == null || commHeaderMBean.getRequireDepts().isEmpty()
                    ? ""
                    : commHeaderMBean.getRequireDepts());
        }

        // 需求類別製作進度
        builder.append(" AND tr.require_status IN (:requireStatus)");
        if (Strings.isNullOrEmpty(requireNo)) {
            parameters.put("requireStatus", headerMBean.createQueryReqStatus());
        } else {
            parameters.put("requireStatus", headerMBean.createQueryAllReqStatus());
        }

        // 需求人員
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(commHeaderMBean.getTrCreatedUserName())) {
            List<Integer> userSids = WkUserUtils.findByNameLike(commHeaderMBean.getTrCreatedUserName(), SecurityFacade.getCompanyId());
            if (WkStringUtils.isEmpty(userSids)) {
                userSids.add(-999);
            }
            builder.append(" AND tr.create_usr IN (:userSids)");
            parameters.put("userSids", userSids.isEmpty() ? "" : userSids);
        }
        // 需求單號
        if (!Strings.isNullOrEmpty(requireNo)) {
            builder.append(" AND tr.require_no = :requireNo");
            parameters.put("requireNo", requireNo);
        }
        //////////////////// 以下為進階搜尋條件//////////////////////////////
        // 異動區間
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getStartUpdatedDate() != null && commHeaderMBean.getEndUpdatedDate() != null) {
            builder.append(" AND tr.update_dt BETWEEN :startUpdatedDate AND :endUpdatedDate");
            parameters.put("startUpdatedDate", helper.transStartDate(commHeaderMBean.getStartUpdatedDate()));
            parameters.put("endUpdatedDate", helper.transEndDate(commHeaderMBean.getEndUpdatedDate()));
        }
        // 緊急度
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getUrgencyTypeList() != null && !commHeaderMBean.getUrgencyTypeList().isEmpty()) {
            builder.append(" AND tr.urgency IN (:urgencyList)");
            parameters.put("urgencyList", commHeaderMBean.getUrgencyTypeList());
        }
        // 需求單號
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(commHeaderMBean.getRequireNo())) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(commHeaderMBean.getRequireNo()) + "%";
            builder.append(" AND tr.require_no LIKE :requireNo");
            parameters.put("requireNo", textNo);
        }
        builder.append(") AS tr ");
    }

    private void buildRequireIndexDictionaryCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        // 模糊搜尋
        builder.append("INNER JOIN (SELECT tid.require_sid,tid.field_content FROM tr_index_dictionary tid WHERE 1=1");
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.isFuzzyQuery()) {
            String text = "%" + reqularUtils.replaceIllegalSqlLikeStr(commHeaderMBean.getFuzzyText()) + "%";
            builder.append(" AND (tid.require_sid"
                    + " IN (SELECT DISTINCT tid1.require_sid FROM tr_index_dictionary tid1 WHERE tid1.field_content LIKE :text)"
                    + " OR tid.require_sid IN (SELECT DISTINCT trace.require_sid FROM tr_require_trace trace WHERE "
                    + "trace.require_trace_type = 'REQUIRE_INFO_MEMO' AND trace.require_trace_content LIKE :text))");
            parameters.put("text", text);
        }
        builder.append(" AND tid.field_name='主題') AS tid ON tr.require_sid=tid.require_sid ");
    }

    private void buildRequireTraceCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT trt.require_sid,trt.create_dt,trt.create_usr,trt.reason,trt.require_trace_content,"
                + "MAX(trt.create_dt) FROM tr_require_trace trt WHERE trt.require_trace_type = 'ROLL_BACK_NOTIFY'");
        // 退件時間
        if (Strings.isNullOrEmpty(requireNo) && headerMBean.getStartRejectedDate() != null && headerMBean.getEndRejectedDate() != null) {
            builder.append(" AND trt.create_dt BETWEEN :startRejectDate AND :endRejectDate");
            parameters.put("startRejectDate", helper.transStartDate(headerMBean.getStartRejectedDate()));
            parameters.put("endRejectDate", helper.transEndDate(headerMBean.getEndRejectedDate()));
        }
        // 退件人員
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(headerMBean.getRejectUserName())) {
            List<Integer> userSids = WkUserUtils.findByNameLike(headerMBean.getRejectUserName(), SecurityFacade.getCompanyId());
            if (WkStringUtils.isEmpty(userSids)) {
                userSids.add(-999);
            }
            builder.append(" AND trt.create_usr IN (:rejectUserSids)");
            parameters.put("rejectUserSids", userSids.isEmpty() ? "" : userSids);
        }
        // 退件原因
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(headerMBean.getRejectReason())) {
            builder.append(" AND trt.reason = :rejectReason");
            parameters.put("rejectReason", headerMBean.getRejectReason());
        }
        builder.append(" GROUP BY trt.require_no) AS trt ON tr.require_sid = trt.require_sid ");
    }

    private void buildCategoryKeyMappingCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_category_key_mapping ckm WHERE 1=1");
        // 類別組合
        if (Strings.isNullOrEmpty(requireNo) && (!cateTreeMBean.getBigDataCateSids().isEmpty() || !cateTreeMBean.getMiddleDataCateSids().isEmpty()
                || !cateTreeMBean.getSmallDataCateSids().isEmpty())) {
            builder.append(" AND ckm.big_category_sid IN (:bigs) OR ckm.middle_category_sid IN (:middles) OR ckm.small_category_sid IN "
                    + "(:smalls)");
            // 加入單獨選擇的大類
            if (commHeaderMBean.getSelectBigCategory() != null) {
                cateTreeMBean.getBigDataCateSids().add(commHeaderMBean.getSelectBigCategory().getSid());
            }
            parameters.put("bigs", cateTreeMBean.getBigDataCateSids().isEmpty() ? "" : cateTreeMBean.getBigDataCateSids());
            parameters.put("middles", cateTreeMBean.getMiddleDataCateSids().isEmpty() ? "" : cateTreeMBean.getMiddleDataCateSids());
            parameters.put("smalls", cateTreeMBean.getSmallDataCateSids().isEmpty() ? "" : cateTreeMBean.getSmallDataCateSids());
        }
        // 需求類別
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getSelectBigCategory() != null && cateTreeMBean.getBigDataCateSids().isEmpty()) {
            builder.append(" AND ckm.big_category_sid = :big");
            parameters.put("big", commHeaderMBean.getSelectBigCategory().getSid());
        }
        builder.append(") AS ckm ON tr.mapping_sid=ckm.key_sid ");
    }

    /**
     * 是否有追蹤資料
     *
     * @return
     */
    private String builderHasTraceColumn() {
        StringBuilder builder = new StringBuilder();
        builder.append(" (SELECT CASE WHEN (COUNT(trace.tracesid) > 0) THEN 'TRUE' ELSE 'FALSE' END ");
        builder.append(" FROM work_trace_info trace ");
        builder.append(" WHERE trace.trace_source_no = tr.require_no");
        builder.append(" AND trace.trace_source_type = 'TECH_REQUEST'");
        builder.append(" AND (trace.trace_dep = ").append(loginBean.getDep().getSid());
        builder.append(" OR trace.trace_usr = ").append(loginBean.getUser().getSid()).append(")");
        builder.append(" AND trace.trace_status = 'UN_FINISHED'");
        builder.append(" AND trace.Status = '0'");
        builder.append(") AS hasTrace ");
        return builder.toString();
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Search20View view) {
        view.setReadRecordType(ReadRecordType.HAS_READ);
        Require r = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(r, loginBean.getUser());
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (Search20View) event.getObject();
        this.changeRequireContent(this.queryKeeper);
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Search20View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        helper.exportExcel(document, commHeaderMBean.getStartDate(), commHeaderMBean.getEndDate(), commHeaderMBean.getReportType());
        hasDisplay = true;
    }

    /**
     * 隱藏部分column裡的內容
     */
    public void hideColumnContent() {
        hasDisplay = false;
    }

    /**
     * 列表執行關聯動作
     *
     * @param view
     */
    public void initRelevance(Search20View view) {
        this.querySelection = view;
        this.queryKeeper = this.querySelection;
        Require r = requireService.findByReqNo(view.getRequireNo());
        this.r01MBean.setRequire(r);
        this.r01MBean.getRelevanceMBean().initRelevance();
    }

    public String getRelevanceViewUrl(Search20View view) {
        if (view == null) {
            return "";
        }
        Require r = requireService.findByReqNo(view.getRequireNo());
        if (r == null) {
            return "";
        }
        WorkLinkGroup link = r.getLinkGroup();
        if (link == null) {
            return "";
        }
        return "../require/require04.xhtml" + urlService.createSimpleURLLink(URLServiceAttr.URL_ATTR_L, link.getSid(), view.getRequireNo(), 1);
    }

    /**
     * 列表點選轉寄功能
     * 
     * @param view
     */
    public void openForwardDialog(Search20View view) {
        this.queryKeeper = this.querySelection = view;

        if (SwitchType.DETAIL.equals(switchType)) {
            this.changeRequireContent(this.queryKeeper);
            this.display.update(Lists.newArrayList(
                    "@(.reportUpdateClz)",
                    "title_info_click_btn_id",
                    "require01_title_info_id",
                    "require_template_id",
                    "viewPanelBottomInfoId",
                    "req03botmid",
                    this.dataTableId + "1"));
        }

        RequireForwardDeAndPersonMBean mbean = WorkSpringContextHolder.getBean(RequireForwardDeAndPersonMBean.class);
        mbean.openDialog(view.getRequireNo(), r01MBean);
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search20View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            display.update("headerTitle");
            display.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Search20View to) {
        querySelection = to;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    /**
     * 列表執行追蹤動作
     *
     * @param view
     */
    public void btnAddTrack(Search20View view) {
        Require r = this.highlightAndReturnRequire(view);
        this.r01MBean.setRequire(r);
        this.r01MBean.getTraceActionMBean().initTrace(this.r01MBean);
    }

    /**
     * 列表標註及回傳需求單
     *
     * @param view
     * @return
     */
    private Require highlightAndReturnRequire(Search20View view) {
        this.querySelection = view;
        this.queryKeeper = this.querySelection;
        return requireService.findByReqNo(view.getRequireNo());
    }
}
