/**
 * 
 */
package com.cy.tech.request.web.controller.setting.setting10;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.enums.OrgLevel;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.setting.BatchAddNoticeDepService;
import com.cy.tech.request.web.controller.component.singleselecttree.SingleSelectTreeCallback;
import com.cy.tech.request.web.controller.component.singleselecttree.SingleSelectTreePicker;
import com.cy.tech.request.web.controller.component.singleselecttree.SingleSelectTreePickerConfig;
import com.cy.tech.request.web.controller.setting.Setting10MBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.viewcomponent.treepker.helper.TreePickerDepHelper;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Slf4j
@Controller
@Scope("view")
public class Set10BatchAddNoticeDepMBean implements Serializable {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 2922060143469947104L;

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    protected transient DisplayController displayController;
    @Autowired
    protected transient TreePickerDepHelper treePickerDepHelper;
    @Autowired
    protected transient Setting10MBean setting10MBean;
    @Autowired
    private BatchAddNoticeDepService batchAddNoticeDepService;

    // ========================================================================
    // 變數區
    // ========================================================================
    /**
     * 被通知單位選擇器
     */
    @Getter
    private SingleSelectTreePicker noticeDepTreePicker;

    /**
     * 目標通知單位
     */
    private Integer targetNoticeDepSid;

    @Getter
    @Setter
    private String transTargetRequireForm;

    // ========================================================================
    // 方法
    // ========================================================================
    @PostConstruct
    public void init() {
    }

    public void process() {

        // ====================================
        // 檢查
        // ====================================
        if (targetNoticeDepSid == null) {
            MessagesUtils.showWarn("尚未選擇被通知部門！");
            return;
        }
        if (WkStringUtils.isEmpty(this.transTargetRequireForm)) {
            MessagesUtils.showWarn("尚未輸入『新增通知單位單據』！");
            return;
        }

        // ====================================
        // 解析輸入資料
        // ====================================
        String tempTargetRequireFormStr = this.transTargetRequireForm;
        tempTargetRequireFormStr = tempTargetRequireFormStr.replaceAll("\r\n", ",");
        tempTargetRequireFormStr = tempTargetRequireFormStr.replaceAll("\n", ",");
        tempTargetRequireFormStr = tempTargetRequireFormStr.replaceAll(" ", "");

        Set<String> requireNos = Sets.newHashSet();
        Set<String> requireSids = Sets.newHashSet();
        List<String> errorDatas = Lists.newArrayList();

        // 需求單號格式
        Pattern requireNoPattern = Pattern.compile(SecurityFacade.getCompanyId() + "TR[\\d]{11}");
        log.debug("[" + "[" + SecurityFacade.getCompanyId() + "TR][\\d]{11}" + "]");
        // UUID格式
        Pattern uuidPattern = Pattern.compile("[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}");

        for (String str : tempTargetRequireFormStr.split(",")) {
            if (WkStringUtils.isEmpty(str)) {
                continue;
            }
            
            log.debug("[" + str + "]");

            // 比對為需求單號
            if (requireNoPattern.matcher(str).matches()) {
                requireNos.add(str);
            }
            // 比對為UUID
            else if (uuidPattern.matcher(str).matches()) {
                requireSids.add(str);
            }
            // 資料不正確
            else {
                errorDatas.add(str);
            }
        }

        if (WkStringUtils.notEmpty(errorDatas)) {
            String message = "<br/>通知單據資料輸入錯誤!請檢查以下資料：<br/>";
            message += errorDatas.stream()
                    .collect(Collectors.joining("』<br/>『", "『", "』"));

            this.setting10MBean.clearInfoScreenMsg();
            this.setting10MBean.appendInfoScreenMsg(message);

            MessagesUtils.showWarn("『新增通知單位單據』輸入資料有誤，請檢查！");
            return;
        }

        if (WkStringUtils.isEmpty(requireNos)
                && WkStringUtils.isEmpty(requireSids)) {
            MessagesUtils.showWarn("解析不到可以執行的單據，請檢查！");
            return;
        }

        // ====================================
        // 查詢資料
        // ====================================
        // 記錄執行資訊的容器
        List<String> execInfos = Lists.newArrayList();
        String processResultMessage = "";
        int count = 0;
        // 批次新增
        try {
            count = this.batchAddNoticeDepService.process(
                    requireSids,
                    requireNos,
                    this.targetNoticeDepSid,
                    execInfos);
        } catch (UserMessageException e) {
            processResultMessage = e.getMessage();
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, processResultMessage, 1);
        } catch (Exception e) {
            processResultMessage = WkMessage.PROCESS_FAILED + e.getMessage();
            log.error(processResultMessage, e);
        }

        if (WkStringUtils.isEmpty(processResultMessage)) {
            MessagesUtils.showInfo("執行完成，共[" + count + "]筆。");
        } else {
            MessagesUtils.showError(processResultMessage);
        }

        this.setting10MBean.clearInfoScreenMsg();
        this.setting10MBean.appendInfoScreenMsg(String.join("<br/>", execInfos));

    }

    /**
     * 顯示已選擇的通知單位資訊
     * 
     * @return
     */
    public String showSelectTargetNoticeDep() {
        if (targetNoticeDepSid == null) {
            return WkHtmlUtils.addRedBlodClass("未選擇");
        }

        return WkOrgUtils.prepareBreadcrumbsByDepNameAndMakeupAndDecorationStyle(
                this.targetNoticeDepSid, OrgLevel.GROUPS, true, "->");

    }

    // ========================================================================
    // 通知單位選單
    // ========================================================================
    /**
     * 開啟通知單位選擇視窗
     */
    public void event_dialog_noticeDepTree_open() {

        // ====================================
        // 初始化元件
        // ====================================
        try {
            // Picker Config
            SingleSelectTreePickerConfig noticeDepPickerConfig = new SingleSelectTreePickerConfig(noticeDepTreeCallback);
            this.noticeDepTreePicker = new SingleSelectTreePicker(noticeDepPickerConfig);
        } catch (Exception e) {
            String message = "開啟【被通知單位】設定視窗失敗！" + e.getMessage();
            log.error(message, e);
            MessagesUtils.showError(message);
            return;
        }

        this.displayController.showPfWidgetVar("wv_dlg_assignNoticeDep");
    }

    /**
     * 需求項目選單 callback
     */
    private final SingleSelectTreeCallback noticeDepTreeCallback = new SingleSelectTreeCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = 3217148495007956799L;

        /**
         * 準備所有的項目
         * 
         * @return
         */
        public List<WkItem> prepareAllItems() throws SystemDevelopException {

            return treePickerDepHelper.prepareDepItems(
                    SecurityFacade.getCompanyId(),
                    WkOrgCache.getInstance().findAllDepSidByCompID(SecurityFacade.getCompanyId()));
        }

        /**
         * 擊點項目時的動作
         * 
         * @return
         */
        public void clickItem() {
            // 取得選擇的單位sid
            String selectedSid = noticeDepTreePicker.getSelectedSid();
            // 轉Integer
            targetNoticeDepSid = WkStringUtils.isEmpty(selectedSid) ? null : Integer.parseInt(selectedSid);
            // 關閉選擇視窗
            displayController.hidePfWidgetVar("wv_dlg_assignNoticeDep");
        }
    };

}
