/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.logic.helper;

import com.cy.commons.vo.Org;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.TrRequireService;
import com.cy.tech.request.logic.service.onpg.TrWorkOnpgService;
import com.cy.tech.request.logic.service.othset.TrOsTransService;
import com.cy.tech.request.logic.service.pt.WorkPtCheckService;
import com.cy.tech.request.logic.service.send.test.TrWorkTestInfoService;
import com.cy.tech.request.logic.utils.SimpleDateFormatEnum;
import com.cy.tech.request.logic.utils.ToolsDate;
import com.cy.tech.request.vo.enums.RequireTransProgramType;
import com.cy.tech.request.vo.onpg.TrWorkOnpg;
import com.cy.tech.request.vo.pt.WorkPtCheck;
import com.cy.tech.request.vo.require.TrRequire;
import com.cy.tech.request.vo.require.tros.TrOs;
import com.cy.tech.request.vo.worktest.TrWorkTestInfo;
import com.cy.tech.request.web.condition.Setting10CreateDepModifyCondition;
import com.cy.tech.request.web.setting.RequireTransSetting;
import com.cy.work.common.utils.WkOrgUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 建立部門轉單Logic元件
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class TransCreateDepLogicHelper {

	/** TrRequireService */
	@Autowired
	private TrRequireService trRequireService;
	/** TrOsTransService */
	@Autowired
	private TrOsTransService trOsTransService;
	/** OrganizationService */
	@Autowired
	private OrganizationService organizationService;
	/** WorkPtCheckService */
	@Autowired
	private WorkPtCheckService workPtCheckService;
	/** TrWorkTestInfoService */
	@Autowired
	private TrWorkTestInfoService trWorkTestInfoService;
	/** TrWorkOnpgService */
	@Autowired
	private TrWorkOnpgService trWorkOnpgService;

	/**
	 * 執行On程式轉單動作
	 *
	 * @param selReqTypes        需求製作進度挑選Sids
	 * @param sourceMappOrg      來源對應部門
	 * @param targetMappOrg      目標新增部門
	 * @param transNO            轉換單號
	 * @param selSubProgramTypes 子程序狀態挑選Sids
	 * @param isJustView         是否僅預覽(true-> 預覽結果, false->執行並更新至DB)
	 * @return
	 */
	public List<Setting10CreateDepModifyCondition> viewWorkOnpgTransCreateDep(List<String> selReqTypes,
	        Org sourceMappOrg, Org targetMappOrg, String transNO, List<String> selSubProgramTypes, boolean isJustView) {
		List<Setting10CreateDepModifyCondition> result = Lists.newArrayList();
		String[] nos = transNO.split(",");
		Lists.newArrayList(nos).forEach(item -> {
			try {
				if (Strings.isNullOrEmpty(item)) {
					log.error("單號為空白,異常");
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, "單號為空白,異常"));
					return;
				}
				TrWorkOnpg tr = trWorkOnpgService.findByOnpgNo(item);
				if (tr == null) {
					log.error("ON程式單號不存在DB,異常,單號 : " + item);
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, "ON程式單號不存在DB,異常,單號 : " + item));
					return;
				}
				TrRequire trq = trRequireService.getTrRequireByRequireNo(tr.getSourceNo());
				if (trq == null) {
					log.error("ON程式內需求單單號不存在,異常送測單號 : " + item + ".需求單單號 : " + tr.getSourceNo());
					StringBuilder sbText = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.WORKONPG),
					        tr.getOnpgNo(), RequireTransSetting.getTransJobWorkOnpgStatusName(tr.getOnpgStatus()), tr.getCreate_dt());
					sbText.append(" 可否異動 : 不可異動,送測內需求單單號不存在,送測單號 : " + item + ".需求單單號 : " + tr.getSourceNo());
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, sbText.toString()));
					return;
				}
				if (!tr.getDep_sid().equals(sourceMappOrg.getSid())) {
					Org dep = organizationService.findBySid(tr.getDep_sid());
					StringBuilder sbText = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.WORKONPG),
					        tr.getOnpgNo(), RequireTransSetting.getTransJobWorkOnpgStatusName(tr.getOnpgStatus()), tr.getCreate_dt());
					sbText.append(" 可否異動 : 不可異動,"
					        + "需求單部門 [" + WkOrgUtils.getOrgName(dep) + "] "
					        + "挑選來源部門卻是 [" + WkOrgUtils.getOrgName(sourceMappOrg) + "]");
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, sbText.toString()));
					return;
				}
				String requireStatus = trq.getRequireStatus().name();
				if (!selReqTypes.contains(requireStatus)) {
					StringBuilder sbText = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.WORKONPG),
					        tr.getOnpgNo(), RequireTransSetting.getTransJobWorkOnpgStatusName(tr.getOnpgStatus()), tr.getCreate_dt());
					sbText.append(" 可否異動 : 不可異動,製作進度不在挑選內");
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, sbText.toString()));
					return;
				}
				String onpgStatus = tr.getOnpgStatus().name();
				if (!selSubProgramTypes.contains(onpgStatus)) {
					StringBuilder sbText = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.WORKONPG),
					        tr.getOnpgNo(), RequireTransSetting.getTransJobWorkOnpgStatusName(tr.getOnpgStatus()), tr.getCreate_dt());
					sbText.append(" 可否異動 : 不可異動,子程序狀態不在挑選內");
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, sbText.toString()));
					return;
				}
				if (isJustView) {
					StringBuilder resSb = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.WORKONPG),
					        tr.getOnpgNo(), RequireTransSetting.getTransJobWorkOnpgStatusName(tr.getOnpgStatus()), tr.getCreate_dt());
					resSb.append(" 可否異動 : 可");
					result.add(createSetting10CreateDepModifyCondition(result.size(), true, resSb.toString()));
				} else {
					try {
						trWorkOnpgService.updateDepsidByPtNo(targetMappOrg.getSid(), tr.getOnpgNo());
						StringBuilder resSb = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.WORKONPG),
						        tr.getOnpgNo(), RequireTransSetting.getTransJobWorkOnpgStatusName(tr.getOnpgStatus()), tr.getCreate_dt());
						resSb.append(" 異動成功 : true");
						result.add(createSetting10CreateDepModifyCondition(result.size(), true, resSb.toString()));
					} catch (Exception e) {
						log.error(" trWorkOnpgService.updateDepsidByPtNo", e);
						StringBuilder resSb = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.WORKONPG),
						        tr.getOnpgNo(), RequireTransSetting.getTransJobWorkOnpgStatusName(tr.getOnpgStatus()), tr.getCreate_dt());
						resSb.append(" 異動成功 : false");
						result.add(createSetting10CreateDepModifyCondition(result.size(), false, resSb.toString()));
					}
				}
			} catch (Exception e) {
				log.error("viewTransCreateDep ERROR", e);
				result.add(createSetting10CreateDepModifyCondition(result.size(), false, "單號 : " + item + "執行失敗"));
			}
		});
		return result;
	}

	/**
	 * 執行送測轉單動作
	 *
	 * @param selReqTypes        需求製作進度挑選Sids
	 * @param sourceMappOrg      來源對應部門
	 * @param targetMappOrg      目標新增部門
	 * @param transNO            轉換單號
	 * @param selSubProgramTypes 子程序狀態挑選Sids
	 * @param isJustView         是否僅預覽(true-> 預覽結果, false->執行並更新至DB)
	 * @return
	 */
	public List<Setting10CreateDepModifyCondition> viewWorkTestSignInfoTransCreateDep(List<String> selReqTypes,
	        Org sourceMappOrg, Org targetMappOrg, String transNO, List<String> selSubProgramTypes, boolean isJustView) {
		List<Setting10CreateDepModifyCondition> result = Lists.newArrayList();
		String[] nos = transNO.split(",");
		Lists.newArrayList(nos).forEach(item -> {
			try {
				if (Strings.isNullOrEmpty(item)) {
					log.error("單號為空白,異常");
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, "單號為空白,異常"));
					return;
				}
				TrWorkTestInfo tr = trWorkTestInfoService.findByTestinfoNo(item);
				if (tr == null) {
					log.error("送測單號不存在DB,異常,單號 : " + item);
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, "送測單號不存在DB,異常,單號 : " + item));
					return;
				}
				TrRequire trq = trRequireService.getTrRequireByRequireNo(tr.getSourceNo());
				if (trq == null) {
					log.error("送測內需求單單號不存在,異常送測單號 : " + item + ".需求單單號 : " + tr.getSourceNo());
					StringBuilder sbText = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.WORKTESTSIGNINFO),
					        tr.getTestinfoNo(), RequireTransSetting.getTransJobWorkTestStatusName(tr.getTestinfoStatus()), tr.getCreate_dt());
					sbText.append(" 可否異動 : 不可異動,送測內需求單單號不存在,送測單號 : " + item + ".需求單單號 : " + tr.getSourceNo());
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, sbText.toString()));
					return;
				}
				if (!tr.getDep_sid().equals(sourceMappOrg.getSid())) {
					Org dep = organizationService.findBySid(tr.getDep_sid());
					StringBuilder sbText = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.WORKTESTSIGNINFO),
					        tr.getTestinfoNo(), RequireTransSetting.getTransJobWorkTestStatusName(tr.getTestinfoStatus()), tr.getCreate_dt());
					
					sbText.append(" 可否異動 : 不可異動,"
					        + "需求單部門 [" + WkOrgUtils.getOrgName(dep) + "] "
					        + "挑選來源部門卻是 [" + WkOrgUtils.getOrgName(sourceMappOrg) + "]");

					result.add(createSetting10CreateDepModifyCondition(result.size(), false, sbText.toString()));
					return;
				}
				String requireStatus = trq.getRequireStatus().name();
				if (!selReqTypes.contains(requireStatus)) {
					StringBuilder sbText = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.WORKTESTSIGNINFO),
					        tr.getTestinfoNo(), RequireTransSetting.getTransJobWorkTestStatusName(tr.getTestinfoStatus()), tr.getCreate_dt());
					sbText.append(" 可否異動 : 不可異動,製作進度不在挑選內");
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, sbText.toString()));
					return;
				}
				String workTestStatus = tr.getTestinfoStatus().name();
				if (!selSubProgramTypes.contains(workTestStatus)) {
					StringBuilder sbText = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.WORKTESTSIGNINFO),
					        tr.getTestinfoNo(), RequireTransSetting.getTransJobWorkTestStatusName(tr.getTestinfoStatus()), tr.getCreate_dt());
					sbText.append(" 可否異動 : 不可異動,子程序狀態不在挑選內");
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, sbText.toString()));
					return;
				}
				if (isJustView) {
					StringBuilder resSb = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.WORKTESTSIGNINFO),
					        tr.getTestinfoNo(), RequireTransSetting.getTransJobWorkTestStatusName(tr.getTestinfoStatus()), tr.getCreate_dt());
					resSb.append(" 可否異動 : 可");
					result.add(createSetting10CreateDepModifyCondition(result.size(), true, resSb.toString()));
				} else {
					try {
						trWorkTestInfoService.updateDepsidByTestinfoNo(targetMappOrg.getSid(), tr.getTestinfoNo());
						StringBuilder resSb = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.WORKTESTSIGNINFO),
						        tr.getTestinfoNo(), RequireTransSetting.getTransJobWorkTestStatusName(tr.getTestinfoStatus()), tr.getCreate_dt());
						resSb.append(" 異動成功 : true");
						result.add(createSetting10CreateDepModifyCondition(result.size(), true, resSb.toString()));
					} catch (Exception e) {
						log.error("  trWorkTestInfoService.updateDepsidByTestinfoNo", e);
						StringBuilder resSb = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.WORKTESTSIGNINFO),
						        tr.getTestinfoNo(), RequireTransSetting.getTransJobWorkTestStatusName(tr.getTestinfoStatus()), tr.getCreate_dt());
						resSb.append(" 異動成功 : false");
						result.add(createSetting10CreateDepModifyCondition(result.size(), false, resSb.toString()));
					}
				}
			} catch (Exception e) {
				log.error("viewTransCreateDep ERROR", e);
				result.add(createSetting10CreateDepModifyCondition(result.size(), false, "單號 : " + item + "執行失敗"));
			}
		});
		return result;
	}

	/**
	 * 執行原型確認轉單動作
	 *
	 * @param selReqTypes        需求製作進度挑選Sids
	 * @param sourceMappOrg      來源對應部門
	 * @param targetMappOrg      目標新增部門
	 * @param transNO            轉換單號
	 * @param selSubProgramTypes 子程序狀態挑選Sids
	 * @param isJustView         是否僅預覽(true-> 預覽結果, false->執行並更新至DB)
	 * @return
	 */
	public List<Setting10CreateDepModifyCondition> viewPtCheckTransCreateDep(List<String> selReqTypes,
	        Org sourceMappOrg, Org targetMappOrg, String transNO, List<String> selSubProgramTypes, boolean isJustView) {
		List<Setting10CreateDepModifyCondition> result = Lists.newArrayList();
		String[] nos = transNO.split(",");
		Lists.newArrayList(nos).forEach(item -> {
			try {
				if (Strings.isNullOrEmpty(item)) {
					log.error("單號為空白,異常");
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, "單號為空白,異常"));
					return;
				}
				WorkPtCheck tr = workPtCheckService.findByPtNo(item);
				if (tr == null) {
					log.error("原型確認單號不存在DB,異常,單號 : " + item);
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, "原型確認單號不存在DB,異常,單號 : " + item));
					return;
				}
				TrRequire trq = trRequireService.getTrRequireByRequireNo(tr.getSourceNo());
				if (trq == null) {
					log.error("原型確認內需求單單號不存在,異常原型確認單號 : " + item + ".需求單單號 : " + tr.getSourceNo());
					StringBuilder sbText = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.PTCHECK),
					        tr.getPtNo(), RequireTransSetting.getTransJobPtStatusName(tr.getPtStatus()), tr.getCreate_dt());
					sbText.append(" 可否異動 : 不可異動,原型確認內需求單單號不存在,原型確認單號 : " + item + ".需求單單號 : " + tr.getSourceNo());
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, sbText.toString()));
					return;
				}
				if (!tr.getDep_sid().equals(sourceMappOrg.getSid())) {
					Org dep = organizationService.findBySid(tr.getDep_sid());
					StringBuilder sbText = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.PTCHECK),
					        tr.getPtNo(), RequireTransSetting.getTransJobPtStatusName(tr.getPtStatus()), tr.getCreate_dt());
					sbText.append(" 可否異動 : 不可異動,"
					        + "需求單部門 [" + WkOrgUtils.getOrgName(dep) + "] "
					        + "挑選來源部門卻是 [" + WkOrgUtils.getOrgName(sourceMappOrg) + "]");
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, sbText.toString()));
					return;
				}
				String requireStatus = trq.getRequireStatus().name();
				if (!selReqTypes.contains(requireStatus)) {
					StringBuilder sbText = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.PTCHECK),
					        tr.getPtNo(), RequireTransSetting.getTransJobPtStatusName(tr.getPtStatus()), tr.getCreate_dt());
					sbText.append(" 可否異動 : 不可異動,製作進度不在挑選內");
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, sbText.toString()));
					return;
				}
				String ptStatus = tr.getPtStatus().name();
				if (!selSubProgramTypes.contains(ptStatus)) {
					StringBuilder sbText = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.PTCHECK),
					        tr.getPtNo(), RequireTransSetting.getTransJobPtStatusName(tr.getPtStatus()), tr.getCreate_dt());
					sbText.append(" 可否異動 : 不可異動,子程序狀態不在挑選內");
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, sbText.toString()));
					return;
				}
				if (isJustView) {
					StringBuilder resSb = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.PTCHECK),
					        tr.getPtNo(), RequireTransSetting.getTransJobPtStatusName(tr.getPtStatus()), tr.getCreate_dt());
					resSb.append(" 可否異動 : 可");
					result.add(createSetting10CreateDepModifyCondition(result.size(), true, resSb.toString()));
				} else {
					try {
						workPtCheckService.updateDepsidByPtNo(targetMappOrg.getSid(), tr.getPtNo());
						StringBuilder resSb = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.PTCHECK),
						        tr.getPtNo(), RequireTransSetting.getTransJobPtStatusName(tr.getPtStatus()), tr.getCreate_dt());
						resSb.append(" 異動成功 : true");
						result.add(createSetting10CreateDepModifyCondition(result.size(), true, resSb.toString()));
					} catch (Exception e) {
						log.error("  workPtCheckService.updateDepsidByPtNo", e);
						StringBuilder resSb = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.PTCHECK),
						        tr.getPtNo(), RequireTransSetting.getTransJobPtStatusName(tr.getPtStatus()), tr.getCreate_dt());
						resSb.append(" 異動成功 : false");
						result.add(createSetting10CreateDepModifyCondition(result.size(), false, resSb.toString()));
					}
				}
			} catch (Exception e) {
				log.error("viewTransCreateDep ERROR", e);
				result.add(createSetting10CreateDepModifyCondition(result.size(), false, "單號 : " + item + "執行失敗"));
			}
		});
		return result;
	}

	/**
	 * 執行其他資料設定轉單動作
	 *
	 * @param selReqTypes        需求製作進度挑選Sids
	 * @param sourceMappOrg      來源對應部門
	 * @param targetMappOrg      目標新增部門
	 * @param transNO            轉換單號
	 * @param selSubProgramTypes 子程序狀態挑選Sids
	 * @param isJustView         是否僅預覽(true-> 預覽結果, false->執行並更新至DB)
	 * @return
	 */
	public List<Setting10CreateDepModifyCondition> viewOthSetTransCreateDep(
	        List<String> selReqTypes,
	        Org sourceMappOrg, Org targetMappOrg, String transNO, List<String> selSubProgramTypes, boolean isJustView) {
		List<Setting10CreateDepModifyCondition> result = Lists.newArrayList();
		String[] nos = transNO.split(",");
		Lists.newArrayList(nos).forEach(item -> {
			try {
				if (Strings.isNullOrEmpty(item)) {
					log.error("單號為空白,異常");
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, "單號為空白,異常"));
					return;
				}
				TrOs tr = trOsTransService.findByOsNo(item);
				if (tr == null) {
					log.error("其他資料設定單號不存在DB,異常,單號 : " + item);
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, "其他資料設定單號不存在DB,異常,單號 : " + item));
					return;
				}
				TrRequire trq = trRequireService.getTrRequireByRequireNo(tr.getRequire_no());
				if (trq == null) {
					log.error("其他資料設定內需求單單號不存在,異常,其他資料設定單號 : " + item + ".需求單單號 : " + tr.getRequire_no());
					StringBuilder sbText = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.OTHSET),
					        tr.getOs_no(), RequireTransSetting.getTransJobOthSetStatusName(tr.getOsStatus()), tr.getCreate_dt());
					sbText.append(" 可否異動 : 不可異動,其他資料設定內需求單單號不存在,其他資料設定單號 : " + item + ".需求單單號 : " + tr.getRequire_no());
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, sbText.toString()));
					return;
				}
				if (!tr.getDep_sid().equals(sourceMappOrg.getSid())) {
					Org dep = organizationService.findBySid(tr.getDep_sid());
					StringBuilder sbText = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.OTHSET),
					        tr.getOs_no(), RequireTransSetting.getTransJobOthSetStatusName(tr.getOsStatus()), tr.getCreate_dt());
					sbText.append(" 可否異動 : 不可異動,"
					        + "需求單部門 [" + WkOrgUtils.getOrgName(dep) + "] "
					        + "挑選來源部門卻是 [" + WkOrgUtils.getOrgName(sourceMappOrg) + "]");
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, sbText.toString()));
					return;
				}
				String requireStatus = trq.getRequireStatus().name();
				if (!selReqTypes.contains(requireStatus)) {
					StringBuilder sbText = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.OTHSET),
					        tr.getOs_no(), RequireTransSetting.getTransJobOthSetStatusName(tr.getOsStatus()), tr.getCreate_dt());
					sbText.append(" 可否異動 : 不可異動,製作進度不在挑選內");
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, sbText.toString()));
					return;
				}
				String osStatus = tr.getOsStatus().name();
				if (!selSubProgramTypes.contains(osStatus)) {
					StringBuilder sbText = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.OTHSET),
					        tr.getOs_no(), RequireTransSetting.getTransJobOthSetStatusName(tr.getOsStatus()), tr.getCreate_dt());
					sbText.append(" 可否異動 : 不可異動,子程序狀態不在挑選內");
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, sbText.toString()));
					return;
				}
				if (isJustView) {
					StringBuilder resSb = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.OTHSET),
					        tr.getOs_no(), RequireTransSetting.getTransJobOthSetStatusName(tr.getOsStatus()), tr.getCreate_dt());
					resSb.append(" 可否異動 : 可");
					result.add(createSetting10CreateDepModifyCondition(result.size(), true, resSb.toString()));
				} else {
					try {
						trOsTransService.updateDepsidByPtNo(targetMappOrg.getSid(), tr.getOs_no());
						StringBuilder resSb = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.OTHSET),
						        tr.getOs_no(), RequireTransSetting.getTransJobOthSetStatusName(tr.getOsStatus()), tr.getCreate_dt());
						resSb.append(" 異動成功 : true");
						result.add(createSetting10CreateDepModifyCondition(result.size(), true, resSb.toString()));
					} catch (Exception e) {
						log.error("   trOsTransService.updateDepsidByPtNo", e);
						StringBuilder resSb = createSubInfo(trq, RequireTransSetting.getTransProgramTypeName(RequireTransProgramType.OTHSET),
						        tr.getOs_no(), RequireTransSetting.getTransJobOthSetStatusName(tr.getOsStatus()), tr.getCreate_dt());
						resSb.append(" 異動成功 : false");
						result.add(createSetting10CreateDepModifyCondition(result.size(), false, resSb.toString()));
					}
				}
			} catch (Exception e) {
				log.error("viewTransCreateDep ERROR", e);
				result.add(createSetting10CreateDepModifyCondition(result.size(), false, "單號 : " + item + "執行失敗"));
			}
		});
		return result;
	}

	/**
	 * 執行需求單轉單動作
	 *
	 * @param selReqTypes   需求製作進度挑選Sids
	 * @param sourceMappOrg 來源對應部門
	 * @param targetMappOrg 目標新增部門
	 * @param transNO       轉換單號
	 * @param isJustView    子程序狀態挑選Sids
	 * @return
	 */
	public List<Setting10CreateDepModifyCondition> viewRequieTransCreateDep(List<String> selReqTypes,
	        Org sourceMappOrg, Org targetMappOrg, String transNO, boolean isJustView) {
		List<Setting10CreateDepModifyCondition> result = Lists.newArrayList();
		String[] nos = transNO.split(",");
		Lists.newArrayList(nos).forEach(item -> {
			try {
				if (Strings.isNullOrEmpty(item)) {
					log.error("單號為空白,異常");
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, "單號為空白,異常"));
					return;
				}
				TrRequire tr = trRequireService.getTrRequireByRequireNo(item);
				if (tr == null) {
					log.error("單號不存在DB,異常,單號 : " + item);
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, "單號不存在DB,異常,單號 : " + item));
					return;
				}
				if (!tr.getDep_sid().equals(sourceMappOrg.getSid())) {
					Org dep = organizationService.findBySid(tr.getDep_sid());
					StringBuilder sbText = createRequireInfo(tr);
					sbText.append(" 可否異動 : 不可異動,"
					        + "需求單部門 [" + WkOrgUtils.getOrgName(dep) + "] "
					        + "挑選來源部門卻是 [" + WkOrgUtils.getOrgName(sourceMappOrg) + "]");
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, sbText.toString()));
					return;
				}
				String requireStatus = tr.getRequireStatus().name();
				if (!selReqTypes.contains(requireStatus)) {
					StringBuilder sbText = createRequireInfo(tr);
					sbText.append(" 可否異動 : 不可異動,製作進度不在挑選內");
					result.add(createSetting10CreateDepModifyCondition(result.size(), false, sbText.toString()));
					return;
				}
				if (isJustView) {
					StringBuilder resSb = createRequireInfo(tr);
					resSb.append(" 可否異動 : 可");
					result.add(createSetting10CreateDepModifyCondition(result.size(), true, resSb.toString()));
				} else {
					try {
						trRequireService.updateTrRequireDepSidByRequireNo(targetMappOrg.getSid(), tr.getRequireNo());
						StringBuilder resSb = createRequireInfo(tr);
						resSb.append(" 異動成功 : true");
						Setting10CreateDepModifyCondition condition = createSetting10CreateDepModifyCondition(result.size(), true, resSb.toString());
						condition.setTrRequire(tr);
						result.add(condition);
					} catch (Exception e) {
						log.error("   trOsTransService.updateDepsidByPtNo", e);
						StringBuilder resSb = createRequireInfo(tr);
						resSb.append(" 異動成功 : false");
						result.add(createSetting10CreateDepModifyCondition(result.size(), false, resSb.toString()));
					}
				}

			} catch (Exception e) {
				log.error("viewTransCreateDep ERROR", e);
				result.add(createSetting10CreateDepModifyCondition(result.size(), false, "單號 : " + item + "執行失敗"));
			}
		});
		return result;
	}

	/**
	 * 建立需求單Info Log
	 *
	 * @param trRequire 需求單物件
	 * @return
	 */
	private StringBuilder createRequireInfo(TrRequire trRequire) {
		StringBuilder sbText = new StringBuilder();
		sbText.append(" 需求單ID : " + trRequire.getSid() + "...<BR/>");
		sbText.append(" 需求單號 : " + trRequire.getRequireNo() + "...<BR/>");
		sbText.append(" 製作進度 : "
		        + RequireTransSetting.getTransJobRequireStatusTypeName(trRequire.getRequireStatus()) + "...<BR/>");
		sbText.append(" 建單時間 : " + ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), trRequire.getCreatedDate()) + "...<BR/>");
		return sbText;
	}

	/**
	 * 建立子程序Info Log
	 *
	 * @param trRequire  需求單物件
	 * @param subName    子程序名稱
	 * @param subNo      子程式單號
	 * @param subStatus  子程序狀態
	 * @param createDate 子程序建立時間
	 * @return
	 */
	public StringBuilder createSubInfo(TrRequire trRequire, String subName, String subNo, String subStatus, Date createDate) {
		StringBuilder sbText = new StringBuilder();
		sbText.append(" 需求單ID : " + ((trRequire != null) ? trRequire.getSid() : "") + "...<BR/>");
		sbText.append(" 需求單號 : " + ((trRequire != null) ? trRequire.getRequireNo() : "") + "...<BR/>");
		sbText.append(" 製作進度 : "
		        + ((trRequire != null) ? RequireTransSetting.getTransJobRequireStatusTypeName(trRequire.getRequireStatus()) : "") + "...<BR/>");
		sbText.append(" 子程序 : " + subName + "...<BR/>");
		sbText.append(" 子程序單號 : " + subNo + "...<BR/>");
		sbText.append(" 子程序狀態 : " + subStatus + "...<BR/>");
		sbText.append(" 子程序建單時間 : " + ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), createDate) + "...<BR/>");
		return sbText;
	}

	/**
	 * 建立Log結果物件
	 *
	 * @param index
	 * @param result  執行結果
	 * @param message 執行LogMessage
	 * @return
	 */
	private Setting10CreateDepModifyCondition createSetting10CreateDepModifyCondition(int index, boolean result, String message) {
		Setting10CreateDepModifyCondition sm = new Setting10CreateDepModifyCondition();
		sm.setResult(result);
		String allMessage = "(" + (index + 1) + ")...<BR/>" + message;
		sm.setText(allMessage);
		return sm;
	}

}
