/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.values;

import com.cy.commons.vo.Org;
import com.cy.tech.request.logic.service.OrganizationService;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 組織
 *
 * @author shaun
 */
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class UnitBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4526010492340222134L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private OrganizationService service;

    /**
     * 取得型態為公司的資料
     *
     * @return
     */
    public List<Org> compValues() {
        return service.findAllCompany();
    }

    public List<Org> compValuesByLogin() {
        return Lists.newArrayList(loginBean.getComp());
    }
}
