/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.attach;

import com.cy.work.common.vo.Attachment;
import java.io.Serializable;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.FileUploadEvent;

/**
 * 所有附加檔案MBean都控制同一個上傳Dialog跟刪除訊息Dialog
 *
 * @author shaun
 */
@SuppressWarnings({ "rawtypes" })
public interface IAttachDlg<MBean, E, A extends Attachment> extends Serializable {

    public void onUploadDialogOpen(Integer fileSLimitize);

    public Integer getDialogWidth();

    public Integer getDialogHeight();

    public Integer getFileLimitSize();

    public void handleFileUpload(FileUploadEvent event);

    public StringBuilder getUploadMessages();

    public void clearFailMessages();

    public StringBuilder getFailMsg();

    public void onUploadDialogClose(CloseEvent event);

    public A getEditAttach();

    public void initRecycle(A attachment);

    public void whenDeleteAttachment(MBean r01MBean, E dependEntity);

    public int findAttachCnt(E dependEntity);

    public void endUploadAction(E dependEntity);

}
