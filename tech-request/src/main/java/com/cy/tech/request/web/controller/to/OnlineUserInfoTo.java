/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.to;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.utils.WkOrgUtils;

import java.io.Serializable;
import java.util.TreeSet;
import lombok.Getter;
import lombok.Setter;

/**
 * 線上人員資訊
 *
 * @author shaun
 */
public class OnlineUserInfoTo implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -7878215380241206038L;
    @Getter
	@Setter
	private int userSid;
	@Getter
	@Setter
	private String userId;
	@Getter
	@Setter
	private String userName;
	@Getter
	@Setter
	private int depSid;
	@Getter
	@Setter
	private String depId;
	@Getter
	@Setter
	private String depName;
	/** 登入時間 */
	@Getter
	@Setter
	private String startActivityTime;
	/** 最後活動畫面 */
	@Getter
	@Setter
	private String lastActiveViewId;
	/** 最後活動時間 */
	@Getter
	@Setter
	private String lastActiveTime;
	/** 物件關聯 */
	@Getter
	@Setter
	transient private Object loginBeanLink;
	/** 系統訊息 */
	@Getter
	@Setter
	private TreeSet<String> sysinfoMsgs;

	public OnlineUserInfoTo(User login, Org loginDept, String startTime, Object mBeanLink) {
		sysinfoMsgs = new TreeSet<String>();

		userSid = login.getSid();
		userId = login.getId();
		userName = login.getName();

		depSid = loginDept.getSid();
		depId = loginDept.getId();
		depName = WkOrgUtils.getOrgName(loginDept);

		startActivityTime = startTime;
		this.loginBeanLink = mBeanLink;
	}

}
