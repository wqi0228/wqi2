package com.cy.tech.request.web.controller.require;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.AssignSendInfoHistoryService;
import com.cy.tech.request.logic.service.AssignSendInfoService;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.require.AssignSendInfo;
import com.cy.tech.request.vo.require.AssignSendInfoHistoryVO;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author allen1214_wu
 *
 */
@Controller
@NoArgsConstructor
@Scope("view")
public class AssignSendInfoComponent implements IRequireBottom, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 156037512028660144L;
    // ====================================
    // 服務物件區
    // ====================================
    @Autowired
    private AssignSendInfoService assignSendInfoService;
    @Autowired
    private AssignSendInfoHistoryService assignSendInfoHistoryService;
    @Autowired
    private DisplayController displayController;

    // ====================================
    // 變數區
    // ====================================
    /**
     * 分派結果單位 (HTML)
     */
    @Getter
    private String resultAssignDepsInfo = "";

    /**
     * 分派結果單位 (TreeNode)
     */
    @Getter
    private TreeNode resultAssignDepTree = null;

    /**
     * 通知結果單位
     */
    @Getter
    private String resultSendDepsInfo = "";

    /**
     * 分派結果單位 (TreeNode)
     */
    @Getter
    private TreeNode resultSendDepTree = null;

    /**
     * 分派/通知歷史記錄
     */
    @Getter
    private List<AssignSendInfoHistoryVO> historys;

    // ====================================
    // 方法區
    // ====================================

    /**
     * 被動觸發初始化 若已被頁面引用, 但還未執行初始化方法時觸發
     */
    public String passiveInitForView() {
        if (historys != null) {
            return "";
        }
        Require01MBean r01mBean = com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder.getBean(Require01MBean.class);
        this.initTabInfo(r01mBean);
        return "";
    }

    /**
     *
     */
    @Override
    public void initTabInfo(Require01MBean r01mBean) {

        // ====================================
        // 初始化查詢結果參數
        // ====================================
        this.resultAssignDepsInfo = "尚未分派";
        this.resultSendDepsInfo = "尚無通知單位";
        this.historys = Lists.newArrayList();

        // ====================================
        // 檢核無須查詢
        // ====================================
        if (r01mBean == null || r01mBean.getRequire() == null || WkStringUtils.isEmpty(r01mBean.getRequire().getSid())) {
            return;
        }

        // ====================================
        // 查詢分派/通知單位
        // ====================================
        // 分派單位
        this.resultAssignDepsInfo = this.prepareInfo(r01mBean.getRequire().getSid(), AssignSendType.ASSIGN);
        this.resultAssignDepTree = this.prepareTreeNode(r01mBean.getRequire().getSid(), AssignSendType.ASSIGN);

        // 通知單位
        this.resultSendDepsInfo = this.prepareInfo(r01mBean.getRequire().getSid(), AssignSendType.SEND);
        this.resultSendDepTree = this.prepareTreeNode(r01mBean.getRequire().getSid(), AssignSendType.SEND);

        // ====================================
        // 查詢分派/通知歷史記錄
        // ====================================
        this.historys = this.assignSendInfoHistoryService.findAssignSendInfoHistory(r01mBean.getRequire().getSid());

        this.displayController.update("require_bottom_info_tab_assignsend");
    }

    public void onNodeSelect(NodeSelectEvent event) {
        if (!event.getTreeNode().isLeaf()) {
            event.getTreeNode().setExpanded(!event.getTreeNode().isExpanded());
        }
    }

    /**
     * @param requireSid
     */
    private String prepareInfo(String requireSid, AssignSendType qryType) {
        // ====================================
        // 查詢最新的分派/通知結果
        // ====================================
        // 查詢記錄
        AssignSendInfo assignSendInfo = this.assignSendInfoService.findByRequireSidAndType(
                requireSid,
                qryType);

        if (assignSendInfo == null) {
            return "";
        }

        // ====================================
        // 取得設定物件中的部門
        // ====================================
        List<Integer> depSids = this.assignSendInfoService.prepareDeps(assignSendInfo);

        if (WkStringUtils.isEmpty(depSids)) {
            return "";
        }

        // ====================================
        // 轉文字
        // ====================================
        return WkOrgUtils.prepareDepsNameByTreeStyle(SecurityFacade.getCompanyId(), depSids, 10);
    }

    /**
     * @param requireSid
     */
    private TreeNode prepareTreeNode(String requireSid, AssignSendType qryType) {

        // ====================================
        // 查詢最新的分派/通知結果
        // ====================================
        // 查詢記錄
        AssignSendInfo assignSendInfo = this.assignSendInfoService.findByRequireSidAndType(
                requireSid,
                qryType);

        if (assignSendInfo == null) {
            return null;
        }

        // ====================================
        // 取得設定物件中的部門
        // ====================================
        List<Integer> depSids = this.assignSendInfoService.prepareDeps(assignSendInfo);

        if (WkStringUtils.isEmpty(depSids)) {
            return null;
        }

        // ====================================
        // 建立樹狀結構
        // ====================================
        TreeNode root = new DefaultTreeNode(new WkItem("0", "root", true), null);
        this.createChildNode(root, depSids);

        return root;
    }

    /**
     * 建立子節點
     *
     * @param depSids
     * @return
     */
    private void createChildNode(TreeNode parentNode, List<Integer> depSids) {
        if (WkStringUtils.isEmpty(depSids)) {
            return;
        }

        // 避免影響外層，複製一份
        List<Integer> currDepSids = Lists.newArrayList(depSids);
        // 取得清單中最上層單位 『無父單位在清單中』
        List<Integer> topDepSids = this.findTopDep(depSids);
        if (WkStringUtils.isEmpty(topDepSids)) {
            return;
        }

        // 移除頂層組織僅餘下層組織
        currDepSids.removeAll(topDepSids);

        // 逐筆處理
        for (Integer topDepSid : topDepSids) {
            // 建立單位節點
            TreeNode topDepNode = this.createNewDepNode(topDepSid, parentNode);
            // 取得子單位
            List<Integer> childDepSids = this.findchildDepSidsInList(topDepSid, currDepSids);
            // 建立子單位節點
            if (WkStringUtils.notEmpty(childDepSids)) {
                this.createChildNode(topDepNode, childDepSids);
            }
        }
    }

    /**
     * 取得清單中最上層單位
     * 
     * @param depSids
     * @return
     */
    private List<Integer> findTopDep(List<Integer> depSids) {
        List<Integer> topDepSids = Lists.newArrayList();
        for (Integer depSid : depSids) {
            List<Org> parents = WkOrgCache.getInstance().findAllParent(depSid);
            // 自己就是最高層
            if (WkStringUtils.isEmpty(parents)) {
                topDepSids.add(depSid);
                continue;
            }
            // 檢查自己的上層是否存在於傳入單位(all)中
            boolean isParentsInList = parents.stream()
                    .anyMatch(parentDep -> depSids.contains(parentDep.getSid()));

            // 上層單位沒有在列表中，代表自己為最上層單位
            if (!isParentsInList) {
                topDepSids.add(depSid);
                continue;
            }
        }
        return WkOrgUtils.sortByOrgTree(topDepSids);
    }

    /**
     * 取得清單中, 屬於傳入單位的子單位者
     * 
     * @param parentDepSid
     * @param allDepSids
     * @return
     */
    private List<Integer> findchildDepSidsInList(Integer parentDepSid, List<Integer> allDepSids) {
        // 取得傳入父單位所有的子單位
        Set<Integer> childDepSids = WkOrgCache.getInstance().findAllChildSids(parentDepSid);
        // 過濾查出的子單位, 需在傳入清單中
        return childDepSids.stream()
                .filter(childDepSid -> allDepSids.contains(childDepSid))
                .collect(Collectors.toList());
    }

    /**
     * 建議單位節點資料
     * 
     * @param depSid
     * @param parentNode
     * @return
     */
    private TreeNode createNewDepNode(Integer depSid, TreeNode parentNode) {
        // 查詢 org 資料
        Org topDep = WkOrgCache.getInstance().findBySid(depSid);
        if (topDep == null) {
            return null;
        }

        // 建立節點資料
        WkItem wkItem = new WkItem(
                depSid + "",
                WkOrgUtils.getOrgName(topDep),
                Activation.ACTIVE.equals(topDep.getStatus()));

        // 顯示名稱裝飾
        String showName = WkOrgUtils.prepareBreadcrumbsByDepName(
                depSid,
                OrgLevel.MINISTERIAL,
                true,
                ">");

        showName = WkOrgUtils.makeupDepName(showName, ">", "font-weight:bold;");

        // 停用單位醒目
        if (!wkItem.isActive()) {
            showName = WkHtmlUtils.addStrikethroughStyle(showName, "停用");
        }
        wkItem.setShowName(showName);

        // 建立樹節點
        TreeNode node = new DefaultTreeNode(wkItem, parentNode);

        // 處以上節點展開
        boolean isExpend = WkOrgUtils.getOrgLevelOrder(topDep.getLevel()) >= WkOrgUtils.getOrgLevelOrder(OrgLevel.DIVISION_LEVEL);

        node.setExpanded(isExpend);

        return node;
    }
}
