/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.tech.request.logic.service.ForwardService;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;
import java.util.stream.Collectors;

/**
 *
 * @author jason_h
 */
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class ForwardDepInfoMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2116874681531768817L;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    transient private ForwardService forwardService;

    public String getForwardDepStr(Require01MBean r01MBean) {
        if (r01MBean == null || r01MBean.getRequire() == null) {
            return "";
        }
        String requireNo = r01MBean.getRequire().getRequireNo();
        if (Strings.isNullOrEmpty(requireNo)) {
            return "";
        }
        List<Org> depts = forwardService.findReceiveDepByRequireNoAndStatus(requireNo, Activation.ACTIVE);
        if (depts == null || depts.isEmpty()) {
            return "★無轉發任何部門★";
        }
        String str = "<span style=\"color: red;\">已轉寄部門</span><br/>"
                  + depts.stream()
                  .map(dept -> orgService.getOrgName(dept))
                  .filter(name -> !Strings.isNullOrEmpty(name))
                  .collect(Collectors.joining("、"));
        return str + "<br/>★共轉發：" + depts.size() + "個部門★";
    }

}
