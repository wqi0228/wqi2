/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.pf.utils;

import com.cy.commons.util.WebVersion;
import com.cy.tech.request.logic.service.CommonService;
import com.cy.tech.request.logic.service.URLService;
import com.google.common.base.Strings;

import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Date;

import org.omnifaces.util.Faces;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * common
 */
@Controller
@Scope(WebApplicationContext.SCOPE_APPLICATION)
@Slf4j
public class CommonBean implements InitializingBean, Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 720475723401292773L;
    private static CommonBean instance;

	public static CommonBean getInstance() {
		return instance;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		CommonBean.instance = this;
	}

	@Autowired
	transient private CommonService commonService;
	@Autowired
	private URLService urlService;

	public String get(Object type) {
		return commonService.get(type);
	}

	/**
	 * 開啟需求單
	 * 
	 * @param requireNo
	 * @return
	 */
	public String buildRequireURL(String requireNo) {
		return urlService.buildRequireURL(requireNo);
	}

	/**
	 * 開啟送測單
	 * 
	 * @param workTestInfoNo
	 * @return
	 */
	public String buildWorkTestInfoURL(String workTestInfoNo) {
		if (Strings.isNullOrEmpty(workTestInfoNo)) {
			return "";
		}
		String servletPath = urlService.createWorkTestInfoUrl(workTestInfoNo);
		String url = Faces.getRequest().getRequestURL().toString();
		url = url.substring(0, url.indexOf(Faces.getRequest().getServletPath()));
		url = String.format("%s%s", url, servletPath);

		return url;
	}

	/**
	 * for 清除 client 端 js 和 css 的快取用 (更換版本號即更新)
	 * 
	 * @return
	 */
	public String getVersion() {

		try {
			// return PomPropertiesLoader.getProperty("version");
			// 改吃 common-util
			return WebVersion.getInstance().getWebVersion();
		} catch (Exception e) {
			log.warn("取得系統版本別失敗![" + e.getMessage() + "]", e);
			return new Date().getTime() + "";
		}
	}

	/**
	 * for 清除 client 端 js 和 css 的快取用 (每次刷新頁面即更新)
	 * 
	 * @return
	 */
	public String getSysTime() {
		return System.currentTimeMillis() + "";
	}

	/**
	 * 需強制 post back ,但又不需做任何事時使用
	 */
	public void doNottingPostback() {
	}
}
