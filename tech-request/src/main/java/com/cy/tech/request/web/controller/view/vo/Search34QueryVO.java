package com.cy.tech.request.web.controller.view.vo;

import java.io.Serializable;
import java.util.Date;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.vo.query.search.SearchQuery;
import com.cy.tech.request.web.controller.view.component.WeekIntervalVO;
import lombok.Getter;
import lombok.Setter;

public class Search34QueryVO extends SearchQuery implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2470632654658574347L;

    public enum Search34Column{
        TEST_DATE("送測區間", "test_date"),
        EXPECT_ONLINE_DATE("預計上線日", "expect_online_date");
        
        @Getter
        private String name;
        @Getter
        private String value;
        
        private Search34Column(String name, String value) {
            this.name = name;
            this.value = value;
        }
    }
    
    /** 日期種類 */
    @Getter
    @Setter
    private String dateType;
    @Getter
    @Setter
    /** 日期區間 */
    private WeekIntervalVO weekIntervalVO = new WeekIntervalVO();


    public Search34QueryVO(){}
    
    public Search34QueryVO(Org org, User user, ReportType reportType){
        this.dep = org;
        this.user = user;
        this.reportType = reportType;
    }
    
    public void init(){
      dateType = Search34Column.TEST_DATE.getValue();
      Date currentDate = new Date();
      weekIntervalVO.setStartDate(currentDate);
      weekIntervalVO.setEndDate(currentDate);
      weekIntervalVO.setDateTypeIndex(-1);
    }
    
}
