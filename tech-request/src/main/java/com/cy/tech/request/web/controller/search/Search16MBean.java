/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.omnifaces.util.Faces;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search16QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.Search16View;
import com.cy.tech.request.logic.service.CategorySettingService;
import com.cy.tech.request.logic.service.URLService.URLServiceAttr;
import com.cy.tech.request.logic.service.customer.ReqWorkCustomerHelper;
import com.cy.tech.request.logic.service.onpg.OnpgService;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.logic.vo.CustomerTo;
import com.cy.tech.request.vo.constants.ReqPermission;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.component.mipker.helper.MultItemPickerByOrgHelper;
import com.cy.tech.request.web.controller.component.qkstree.impl.OrgUserTreeMBean;
import com.cy.tech.request.web.controller.enums.ReqStatusMBean;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.searchheader.CommonHeaderMBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.view.component.searchquery.SearchQuery16;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkCommonCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.logic.lib.manager.WorkCommonReadRecordManager;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkSqlUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.viewcomponent.treepker.TreePickerCallback;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * ON程式一覽表
 *
 * @author jason_h
 */
@Controller
@Scope("view")
@Slf4j
public class Search16MBean extends BaseSearchMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3221891855530556208L;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private WkCommonCache wkCommonCache;
    @Autowired
    transient private ReqWorkCustomerHelper reqWorkCustomerHelper;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private OnpgService opService;
    @Autowired
    transient private Require01MBean r01MBean;
    @Autowired
    transient private ReqStatusMBean reqStatusUtils;
    @Autowired
    transient private Search16QueryService search16QueryService;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchResultHelper searchResultHelper;
    @Autowired
    transient private WorkCommonReadRecordManager workCommonReadRecordManager;

    /** 人員選擇器 */
    @Autowired
    @Getter
    private transient OrgUserTreeMBean usersPicker;
    /** ReportCustomFilterLogicComponent */
    @Autowired
    private ReportCustomFilterLogicComponent reportCustomFilterLogicComponent;
    /** CategorySettingService */
    @Autowired
    private CategorySettingService categorySettingService;
    /** 所有的ON程式單 */
    private List<Search16View> allQueryItems;
    @Getter
    @Setter
    private List<Search16View> queryItems;
    /** 選擇的ON程式單 */
    @Getter
    @Setter
    private Search16View querySelection;
    /** 上下筆移動keeper */
    @Getter
    private Search16View queryKeeper;
    /** 切換模式 */
    @Getter
    private SwitchType switchType = SwitchType.CONTENT;
    /** 切換模式 - 全畫面狀態 */
    @Getter
    private SwitchType switchFullType = SwitchType.DETAIL;
    /** 在匯出的時候，某些內容需要隱藏 */
    @Getter
    private boolean hasDisplay = true;
    @Getter
    private final String dataTableId = "dtRequire";
    /** 查詢控制物件 */
    @Getter
    private SearchQuery16 searchQuery;
    @Setter
    @Getter
    /** dataTable filter */
    private String createDepName;
    @Getter
    /** dataTable filter items */
    private List<SelectItem> createDepNameItems;

    @Getter
    /** 訊息呼叫 */
    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -1774675260019904310L;

        @Override
        public void showMessage(String m) {
            MessagesUtils.showError(m);
        }
    };

    @PostConstruct
    public void init() {
        this.initComponent();
        this.displayController.execute("doSearchData();");
        // this.search();
    }

    private void initComponent() {

        searchQuery = new SearchQuery16(
                loginBean.getCompanyId(),
                messageCallBack,
                this.displayController,
                reqStatusUtils,
                reqWorkCustomerHelper,
                reportCustomFilterLogicComponent,
                categorySettingService,
                usersPicker,
                loginBean.getUserSId());
        searchQuery.initSetting();
        commHeaderMBean.clearAdvance();
        searchQuery.loadSetting();

        // ====================================
        // 由首頁待辦事項連過來的-預設帶查詢條件
        // ====================================
        this.prepareDefaultConditionByParam();
    }

    /**
     * 由 portal 待辦事項 - 連過來時的『預設查詢條件』
     */
    private void prepareDefaultConditionByParam() {
        // ====================================
        // 解析查詢類別 （首頁）
        // ====================================
        // 取得傳入參數
        String paramValue = Faces.getRequestParameterMap().get(URLServiceAttr.URL_ATTR_SEARCH16_FROM_PORTAL.getAttr());
        if (WkStringUtils.isEmpty(paramValue)) {
            return;
        }

        // ====================================
        // 設定預設查詢條件
        // ====================================
        // 查詢日期清空
        searchQuery.getQueryVo().setDateTypeIndexStr("4");

        // 製作進度進行中
        searchQuery.getQueryVo().setSelectRequireStatusType(RequireStatusType.PROCESS);

        // ON程式狀態 : 已ON上、檢查回覆
        searchQuery.getQueryVo().setOnPgStatus(Lists.newArrayList(
                WorkOnpgStatus.ALREADY_ON,
                WorkOnpgStatus.CHECK_REPLY).stream()
                .map(WorkOnpgStatus::name)
                .collect(Collectors.toList()));

        // 欲讀狀態：需閱讀
        searchQuery.getQueryVo().setSelectReadRecordType(ReadRecordType.NEED_READ);
    }

    public void search() {
        Long startTime = System.currentTimeMillis();
        allQueryItems = this.findWithQuery();
        this.createDepName = null;
        this.createDepNameItems = this.allQueryItems.stream()
                .map(each -> each.getCreateDepName())
                .collect(Collectors.toSet()).stream()
                .map(each -> new SelectItem(each, each))
                .collect(Collectors.toList());
        this.filterCustomerPage(Boolean.TRUE);
        log.debug(WkCommonUtils.prepareCostMessage(startTime, "ON程式一覽表查詢"));
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        searchQuery.initSetting();
        commHeaderMBean.clearAdvance();
        // 清除時, 改為不載入預設搜尋條件
        // searchQuery.loadSetting();
        searchQuery.loadSettingWithoutCust();
        this.search();
    }

    /**
     * 載入預設搜尋條件
     */
    public void loadCustSetting() {
        searchQuery.initSetting();
        commHeaderMBean.clearAdvance();
        searchQuery.loadSetting();
        this.search();
    }

    /**
     * 過濾 廳主
     *
     * @param isSearch
     */
    public void filterCustomerPage(Boolean isSearch) {
        if (searchQuery.getIsCustomerPage()) {
            if (searchQuery.getCustomerPage() == null && !searchQuery.getCustomerPageItems().isEmpty()) {
                searchQuery.setCustomerPage(searchQuery.getCustomerPageItems().get(0));
            }
        }
        this.bulidQueryItemsAndCustomerPageItems(isSearch);
    }

    /**
     * 建立廳主分頁選項
     */
    private void bulidQueryItemsAndCustomerPageItems(Boolean isSearch) {
        queryItems = Lists.newArrayList();
        Set<Long> customerSet = Sets.newHashSet();
        allQueryItems.stream().forEach(v -> {
            if (isSearch && v.getCustomer() != null) {
                customerSet.add(v.getCustomer());
            }
            if (!Strings.isNullOrEmpty(createDepName)
                    && !createDepName.equals(v.getCreateDepName())) {
                return;
            }
            if (searchQuery.getIsCustomerPage()
                    && (searchQuery.getCustomerPage() != null && !searchQuery.getCustomerPage().getSid().equals(v.getCustomer()))) {
                return;
            }
            queryItems.add(v);
        });

        if (isSearch) {

            List<CustomerTo> customers = Lists.newArrayList();
            try {
                customers = reqWorkCustomerHelper.findTosBySids(customerSet);
            } catch (Exception e) {
                log.error("查詢失敗：" + e.getMessage(), e);
            }

            searchQuery.setCustomerPageItems(Lists.newArrayList(customers));
            if (searchQuery.getIsCustomerPage()
                    && (customers.isEmpty() || !customers.contains(searchQuery.getCustomerPage()))) {
                searchQuery.setCustomerPage(null);
                this.filterCustomerPage(Boolean.FALSE);
            }
        }
    }

    /**
     * 變更 廳主
     */
    public void chgCustomerPage() {
        this.filterCustomerPage(Boolean.FALSE);
    }

    private List<Search16View> findWithQuery() {
        String requireNo = reqularUtils.getRequireNo(loginBean.getCompanyId(), searchQuery.getQueryVo().getFuzzyText());
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT "
                + "wop.onpg_sid,"
                + "tr.require_no,"
                + "tid.field_content,"
                + "tr.urgency,"
                + "wop.create_dt,"
                + "wop.dep_sid,"
                + "wop.create_usr,"
                + "wop.onpg_theme,"
                + "ckm.big_category_name,"
                + "ckm.middle_category_name,"
                + "ckm.small_category_name,"
                + "tr.dep_sid AS requireDep,"
                + "tr.create_usr AS requireUser,"
                + "wop.onpg_status,"
                + "wop.read_reason,"
                + "woh.reason,"
                + "wop.onpg_estimate_dt,"
                + "wop.onpg_finish_dt,"
                + "wop.update_dt,"
                + "wop.onpg_no,"
                + "wop.onpg_deps,"
                + "tr.create_dt AS requireCreatedDate,"
                + "tr.customer,"
                + "tr.author,"
                + "tr.require_status, "
                // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
                + this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire()

                + " FROM ");
        buildWorkOnpgCondition(requireNo, builder, parameters);
        buildRequireCondition(requireNo, builder, parameters);
        buildRequireIndexDictionaryCondition(builder, parameters);
        buildCategoryKeyMappingCondition(requireNo, builder, parameters);
        buildWorkOnpgHistoryCondition(builder, parameters);
        // 檢查項目 (系統別)
        // 後方需對 主單 sid 做 group by
        builder.append(this.searchConditionSqlHelper.prepareSubFormCommonJoin(
                SecurityFacade.getUserSid(),
                FormType.WORKONPG,
                "wop"));

        builder.append("WHERE wop.onpg_sid IS NOT NULL ");

        // 查詢條件：是否閱讀
        if (WkStringUtils.isEmpty(requireNo)) {
            builder.append(
                    this.workCommonReadRecordManager.prepareWhereConditionSQL(
                            searchQuery.getQueryVo().getSelectReadRecordType(), "readRecord"));
        }

        builder.append(" GROUP BY wop.onpg_sid ");
        builder.append("  ORDER BY wop.create_dt DESC");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.WORK_ON_PG, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.WORK_ON_PG, SecurityFacade.getUserSid());

        // 查詢
        List<Search16View> resultList = search16QueryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // ====================================
        // 後續處理
        // ====================================
        if (WkStringUtils.notEmpty(resultList)) {
            // 後續處理-開始
            usageRecord.afterProcessStart();
            // 查詢條件過濾：系統別
            resultList = resultList.stream()
                    .filter(each -> this.searchResultHelper.filterCheckItems(
                            each.getCheckItemTypes(),
                            this.searchQuery.getQueryVo().getCheckItemTypes(),
                            false,
                            null))
                    .collect(Collectors.toList());

            // 後續處理-結束
            usageRecord.afterProcessEnd();
        }

        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return resultList;
    }

    private void buildWorkOnpgCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("(SELECT * FROM work_onpg wop WHERE 1=1");

        // 填單區間、異動區間、預計完成區間
        if (WkStringUtils.isEmpty(requireNo)) {
            String dateTimeType = searchQuery.getDateTimeType().getVal();

            if (searchQuery.getQueryVo().getOnpgStartDt() != null) {
                builder.append(" AND " + dateTimeType + " >= '" + WkDateUtils.transStrByStartDate(searchQuery.getQueryVo().getOnpgStartDt()) + "' ");

                helper.transStartDate(searchQuery.getQueryVo().getOnpgStartDt());
            }

            if (searchQuery.getQueryVo().getOnpgEndDt() != null) {
                builder.append(" AND " + dateTimeType + " <= '" + WkDateUtils.transStrByEndDate(searchQuery.getQueryVo().getOnpgEndDt()) + "' ");
            }
        }

        // 技術需求單類型
        builder.append(" AND wop.onpg_source_type='TECH_REQUEST'");

        // 具有 ON程式管理員 角色
        boolean hasPermissionRole = WkUserUtils.isUserHasRole(
                SecurityFacade.getUserSid(),
                SecurityFacade.getCompanyId(),
                ReqPermission.OP_MGR);

        // 填ON程式單的單位 或 收到ON程式單的單位
        if (!hasPermissionRole) {

            String depSidStr = searchQuery.createNoticeRegexpParam();
            if (WkStringUtils.isEmpty(depSidStr)) {
                depSidStr = "nerverFind";
            }

            builder.append(" AND ("
                    + " wop.dep_sid IN (:depSids) "
                    + " OR wop.onpg_deps REGEXP '" + depSidStr + "' "
                    + ")");

            parameters.put("depSids", searchQuery.getQueryVo().getCreateOnpgDepts() == null || searchQuery.getQueryVo().getCreateOnpgDepts().isEmpty()
                    ? "nerverFind"
                    : searchQuery.getQueryVo().getCreateOnpgDepts());

        }
        // 填單人員
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(searchQuery.getQueryVo().getOpCreatedUsrName())) {

            List<Integer> userSids = WkUserUtils.findByNameLike(searchQuery.getQueryVo().getOpCreatedUsrName(), SecurityFacade.getCompanyId());
            if (WkStringUtils.isEmpty(userSids)) {
                userSids.add(-999);
            }

            builder.append(" AND wop.create_usr IN (:userSids)");
            parameters.put("userSids", userSids.isEmpty() ? "" : userSids);
        }

        // ====================================
        // 待閱原因
        // ====================================
        List<String> readReasons = WkCommonUtils.safeStream(searchQuery.getQueryVo().getReadReason())
                .map(WaitReadReasonType::name)
                .collect(Collectors.toList());

        builder.append(
                this.searchConditionSqlHelper.prepareWhereConditionForWaitReadReason(
                        requireNo,
                        WaitReadReasonType.valueNames(FormType.WORKONPG),
                        readReasons,
                        WaitReadReasonType.ONPG_NOT_WAITREASON.name(),
                        "wop.read_reason"));

        // ON程式狀態
        if (Strings.isNullOrEmpty(requireNo)
                && WkStringUtils.notEmpty(searchQuery.getQueryVo().getOnPgStatus()) // 全不選時,不過濾
                && WorkOnpgStatus.values().length != searchQuery.getQueryVo().getOnPgStatus().size() // 全選時,不過濾
        ) {
            builder.append(" AND wop.onpg_status IN (:onPgStatus)");
            parameters.put("onPgStatus", searchQuery.getQueryVo().getOnPgStatus());
        }

        // 主題
        String onpgTheme = searchQuery.getQueryVo().getOnpgTheme();
        if (Strings.isNullOrEmpty(requireNo) && WkStringUtils.notEmpty(onpgTheme)) {
            builder.append(" AND " + WkSqlUtils.prepareConditionSQLByMutiKeyword("wop.onpg_theme", onpgTheme));
        }

        // 模糊搜尋
        String fuzzyText = searchQuery.getQueryVo().getFuzzyText();
        if (Strings.isNullOrEmpty(requireNo) && WkStringUtils.notEmpty(fuzzyText)) {

            Map<String, String> bindColumns = Maps.newHashMap();
            bindColumns.put("onpg_sid", "wop.onpg_sid");

            String currSql = "";
            currSql += " AND ( ";

            // ON 程式單主題
            currSql += " " + WkSqlUtils.prepareConditionSQLByMutiKeyword("wop.onpg_theme", fuzzyText);
            // ON 程式單內容
            currSql += " OR " + WkSqlUtils.prepareConditionSQLByMutiKeyword("wop.onpg_content", fuzzyText);
            // ON 程式單備註
            currSql += " OR " + WkSqlUtils.prepareConditionSQLByMutiKeyword("wop.onpg_note", fuzzyText);
            // 回覆
            currSql += " OR " + WkSqlUtils.prepareExistsInOtherTableConditionSQLForMutiKeywordSearch(
                    "work_onpg_check_record",
                    bindColumns,
                    "record_content",
                    fuzzyText);
            // 回覆的回覆
            currSql += " OR " + WkSqlUtils.prepareExistsInOtherTableConditionSQLForMutiKeywordSearch(
                    "work_onpg_check_record_reply",
                    bindColumns,
                    "reply_content",
                    fuzzyText);

            // 檢查註記
            currSql += " OR " + WkSqlUtils.prepareExistsInOtherTableConditionSQLForMutiKeywordSearch(
                    "work_onpg_check_memo",
                    bindColumns,
                    "memo_content",
                    fuzzyText);

            currSql += "     ) ";

            builder.append(currSql);
        }
        // ====================================
        // 回覆人員
        // ====================================
        if (WkStringUtils.notEmpty(searchQuery.getQueryVo().getReplyUsers())) {
            builder.append(" AND ( ");
            builder.append(" EXISTS ("
                    + "SELECT 1 "
                    + "  FROM work_onpg_check_record ocr "
                    + " WHERE ocr.onpg_sid  = wop.onpg_sid "
                    + "   AND ocr.record_person_sid IN (:replyUsers) )");
            builder.append(" OR ");
            builder.append(" EXISTS ("
                    + "SELECT 1 "
                    + "  FROM work_onpg_check_record_reply ocrr "
                    + " WHERE ocrr.onpg_sid  = wop.onpg_sid "
                    + "   AND ocrr.reply_person_sid IN (:replyUsers) ) ");
            builder.append("     )");
            parameters.put("replyUsers", searchQuery.getQueryVo().getReplyUsers());
        }
        builder.append(") AS wop ");
    }

    private void buildRequireCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_require tr WHERE 1=1");
        // 需求單號
        if (!Strings.isNullOrEmpty(requireNo)) {
            builder.append(" AND tr.require_no = :requireNo");
            parameters.put("requireNo", requireNo);
        }
        //////////////////// 以下為進階搜尋條件//////////////////////////////
        // 需求單-立單日期區間
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getStartUpdatedDate() != null && commHeaderMBean.getEndUpdatedDate() != null) {
            builder.append(" AND tr.create_dt BETWEEN :startDateOfAdvance AND :endDateOfAdvance");
            parameters.put("startDateOfAdvance", helper.transStartDate(commHeaderMBean.getStartUpdatedDate()));
            parameters.put("endDateOfAdvance", helper.transEndDate(commHeaderMBean.getEndUpdatedDate()));
        }
        // 需求單號
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(commHeaderMBean.getRequireNo())) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(commHeaderMBean.getRequireNo()) + "%";
            builder.append(" AND tr.require_no LIKE :requireNo");
            parameters.put("requireNo", textNo);
        }
        // 廳主
        if (Strings.isNullOrEmpty(requireNo) && searchQuery.getCustomers() != null && !searchQuery.getCustomers().isEmpty()
                && searchQuery.getCustomers().size() != searchQuery.getCustomerItems().size()) {
            builder.append(" AND tr.customer IN (:customers)");
            parameters.put("customers", searchQuery.getCustomerSids());
        }

        // 需求類別製作進度
        builder.append(" AND tr.require_status IN (:requireStatus)");
        if (Strings.isNullOrEmpty(requireNo)) {
            parameters.put("requireStatus", searchQuery.createQueryReqStatus());
        } else {
            parameters.put("requireStatus", searchQuery.createQueryAllReqStatus());
        }

        builder.append(") AS tr ON tr.require_no = wop.onpg_source_no ");
    }

    private void buildRequireIndexDictionaryCondition(StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_index_dictionary tid WHERE "
                + "tid.field_name='主題') AS tid ON "
                + "tr.require_sid = tid.require_sid ");
    }

    private void buildCategoryKeyMappingCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_category_key_mapping ckm WHERE 1=1");
        // 類別組合 (REQ-1587後. 僅認定勾選的小類-不會有大中類勾選，但底下小類不勾選的狀況)
        if (WkStringUtils.isEmpty(requireNo)
                && WkStringUtils.notEmpty(searchQuery.getQueryVo().getSmallDataCateSids())) {
            builder.append(" AND  ckm.small_category_sid IN (:smalls)");
            parameters.put("smalls", searchQuery.getQueryVo().getSmallDataCateSids().isEmpty() ? "" : searchQuery.getQueryVo().getSmallDataCateSids());
        }
        // 需求類別
        if (Strings.isNullOrEmpty(requireNo)
                && WkStringUtils.notEmpty(searchQuery.getQueryVo().getSelectBigCategory())) {
            builder.append(" AND ckm.big_category_sid = :big ");
            parameters.put("big", searchQuery.getQueryVo().getSelectBigCategory());
        }

        builder.append(") AS ckm ON tr.mapping_sid=ckm.key_sid ");
    }

    private void buildWorkOnpgHistoryCondition(StringBuilder builder, Map<String, Object> parameters) {
        builder.append("LEFT JOIN (SELECT woh.onpg_history_sid,woh.reason,woh.onpg_sid,MAX(woh.create_dt) "
                + "FROM work_onpg_history woh "
                + "WHERE woh.onpg_source_type ='TECH_REQUEST' GROUP BY woh.onpg_sid) "
                + "AS woh ON woh.onpg_sid = wop.onpg_sid ");
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            this.moveScreenTab();
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (Search16View) event.getObject();
        this.changeRequireContent(this.queryKeeper);
        this.moveScreenTab();
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Search16View view) {
        view.setReadRecordType(ReadRecordType.HAS_READ);
        Require require = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(require, loginBean.getUser(), RequireBottomTabType.ONPG);
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Search16View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
        this.moveScreenTab();
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
        this.moveScreenTab();
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        helper.exportExcel(document, searchQuery.getQueryVo().getOnpgStartDt(), searchQuery.getQueryVo().getOnpgEndDt(), commHeaderMBean.getReportType());
        hasDisplay = true;
    }

    /**
     * 隱藏部分column裡的內容
     */
    public void hideColumnContent() {
        hasDisplay = false;
    }

    public String getNoticeDepsStr(Search16View view) {
        // cache 中取回
        String cacheName = "Search16View-getNoticeDepsStr";
        List<String> keys = Lists.newArrayList(view.getOnpgNo());
        String orgNames = wkCommonCache.getCache(cacheName, keys, 5);
        if (orgNames != null) {
            return orgNames;
        }

        // 組單位名稱
        List<String> orgSids = Optional.ofNullable(view.getNoticeDeps())
                .map(send -> send.getValue())
                .orElse(Lists.newArrayList());

        orgNames = WkOrgUtils.findNameBySidStrs(orgSids, "、");

        // put 快取
        wkCommonCache.putCache(cacheName, keys, orgNames);

        return orgNames;
    }

    public void moveScreenTab() {
        this.r01MBean.getBottomTabMBean().resetTabIdx(this.r01MBean);
        this.r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.ONPG);

        List<String> sids = opService.findSidsBySourceSid(this.r01MBean.getRequire().getSid());
        String indicateSid = queryKeeper.getSid();
        if (sids.indexOf(indicateSid) != -1) {
            this.displayController.execute("PF('op_acc_panel_layer_zero').select(" + sids.indexOf(indicateSid) + ");");
            for (int i = 0; i < sids.size(); i++) {
                if (i != sids.indexOf(indicateSid)) {
                    this.displayController.execute("PF('op_acc_panel_layer_zero').unselect(" + i + ");");
                }
            }
        }
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search16View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.removeClassByTextBold(dtId, pageCount);
        this.transformHasRead();
        this.checkHelfScreen();
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            this.displayController.update("headerTitle");
            this.displayController.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Search16View to) {
        querySelection = to;
        queryKeeper = querySelection;
        this.displayController.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 去除粗體Class
     *
     * @param dtId
     * @param pageCount
     */
    private void removeClassByTextBold(String dtId, String pageCount) {
        this.displayController.execute("removeClassByTextBold('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
        this.displayController.execute("changeAlreadyRead('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 變更已閱讀
     */
    private void transformHasRead() {
        querySelection.setReadRecordType(ReadRecordType.HAS_READ);
        queryKeeper = querySelection;
        queryItems.set(queryItems.indexOf(querySelection), querySelection);
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
        upDownBean.resetTabInfo(RequireBottomTabType.ONPG, queryKeeper.getSid());
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.transformHasRead();
        this.removeClassByTextBold(dtId, pageCount);
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    /**
     * 回覆人員選擇列示
     *
     * @return
     */
    public String showReplyUsers() {
        return WkUserUtils.findNameBySid(searchQuery.getQueryVo().getReplyUsers(), "、");
    }

    // ========================================================================
    // 樹狀選單
    // ========================================================================
    /** 操作類型 */
    private TreePickerType currTreePickerType = null;

    private enum TreePickerType {

        CREATE_DEP("填單單位"),
        CREATE_DEP_IN_CSTOM_CONDITION("填單單位-自訂預設搜尋條件"),
        NOTIFY_DEP("通知單位"),
        NOTIFY_DEP_IN_CSTOM_CONDITION("通知單位-自訂預設搜尋條件"),
        CATEGORY("類別組合"),
        CATEGORY_IN_CSTOM_CONDITION("類別組合-自訂預設搜尋條件"),
        ;

        /** 說明 */
        @Getter
        private final String descr;

        private TreePickerType(String descr) {
            this.descr = descr;
        }

        public static TreePickerType safeValueOf(String str) {
            if (WkStringUtils.notEmpty(str)) {
                for (TreePickerType item : TreePickerType.values()) {
                    if (item.name().equals(str)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * @param typeStr
     */
    public void searchTreepicker_openDialog(String typeStr) {
        // ====================================
        // 取得此次開啟的選單類別
        // ====================================
        this.currTreePickerType = TreePickerType.safeValueOf(typeStr);
        if (this.currTreePickerType == null) {
            MessagesUtils.showError("開發時期錯誤!未定義的選單類別:[" + typeStr + "]");
            return;
        }

        // ====================================
        // 初始化選單
        // ====================================
        this.treepicker_openDialog(this.currTreePickerType.getDescr(), treePickerCallback);
    }

    /**
     * 樹選單 - 確認
     */
    @Override
    public void treePicker_confirm() {

        if (this.currTreePickerType == null) {
            log.error("currTreePickerType 為空!");
            MessagesUtils.showError(WkMessage.NEED_RELOAD);
            return;
        }

        // 取得選單中選擇的項目
        List<String> selectedItemSids = Lists.newArrayList(this.treePicker.getSelectedItemSids());

        switch (currTreePickerType) {
        case CREATE_DEP: // 填單單位
            this.searchQuery.getQueryVo().setCreateOnpgDepts(selectedItemSids);
            break;
        case CREATE_DEP_IN_CSTOM_CONDITION: // 填單單位-自訂預設搜尋條件
            this.searchQuery.getCustomerQueryVo().setCreateOnpgDepts(selectedItemSids);
            break;
        case NOTIFY_DEP: // 通知單位
            this.searchQuery.getQueryVo().setOnpgNoticeDepts(selectedItemSids);
            break;
        case NOTIFY_DEP_IN_CSTOM_CONDITION: // 通知單位-自訂預設搜尋條件
            this.searchQuery.getCustomerQueryVo().setOnpgNoticeDepts(selectedItemSids);
            break;
        case CATEGORY: // 需求類別
            this.searchQuery.getQueryVo().setSmallDataCateSids(selectedItemSids);
            break;
        case CATEGORY_IN_CSTOM_CONDITION:
            this.searchQuery.getCustomerQueryVo().setSmallDataCateSids(selectedItemSids);
            break;

        default:
            log.error("開發時期錯誤!未定義的選單類別:[" + currTreePickerType.name() + "]");
            MessagesUtils.showError(WkMessage.NEED_RELOAD);
            return;
        }

        // 關閉視窗
        this.displayController.hidePfWidgetVar(this.TREE_PICKER_DLG_NAME);
    }

    /**
     * 樹選單 - callback 事件
     */
    private final TreePickerCallback treePickerCallback = new TreePickerCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = 8147298184881358342L;

        @Override
        public List<WkItem> prepareAllItems() throws Exception {
            if (currTreePickerType == null) {
                throw new Exception("currTreePickerType 為空!");
            }

            switch (currTreePickerType) {
            case CREATE_DEP: // 填單單位
            case CREATE_DEP_IN_CSTOM_CONDITION: // 填單單位-自訂預設搜尋條件

            case NOTIFY_DEP: // 通知單位
            case NOTIFY_DEP_IN_CSTOM_CONDITION: // 通知單位-自訂預設搜尋條件
                // 1.可選項目為部門上升到部+可檢視單位
                // 2.GM可選擇全部單位
                if (workBackendParamHelper.isGM(SecurityFacade.getUserSid())) {
                    return MultItemPickerByOrgHelper.getInstance().prepareAllOrgItems();
                } else {
                    return prepareAllCanViewDepItemsByBaseDep();
                }

            case CATEGORY: // 需求類別
            case CATEGORY_IN_CSTOM_CONDITION:
                return prepareAllCategoryItems();

            default:
                break;
            }

            throw new Exception("開發時期錯誤!未定義的選單類別:[" + currTreePickerType.name() + "]");
        }

        /**
         * 準備已選擇項目
         * 
         * @return
         */
        @Override
        public List<String> prepareSelectedItemSids() throws Exception {
            if (currTreePickerType == null) {
                throw new Exception("currTreePickerType 為空!");
            }

            switch (currTreePickerType) {
            case CREATE_DEP: // 填單單位
                return searchQuery.getQueryVo().getCreateOnpgDepts();

            case CREATE_DEP_IN_CSTOM_CONDITION: // 填單單位-自訂預設搜尋條件
                return searchQuery.getCustomerQueryVo().getCreateOnpgDepts();

            case NOTIFY_DEP: // 通知單位
                return searchQuery.getQueryVo().getOnpgNoticeDepts();

            case NOTIFY_DEP_IN_CSTOM_CONDITION: // 通知單位-自訂預設搜尋條件
                return searchQuery.getCustomerQueryVo().getOnpgNoticeDepts();

            case CATEGORY: // 需求類別
                // 不用考慮大、中類
                return searchQuery.getQueryVo().getSmallDataCateSids();

            case CATEGORY_IN_CSTOM_CONDITION:
                // 不用考慮大、中類
                return searchQuery.getCustomerQueryVo().getSmallDataCateSids();

            default:
                break;
            }

            throw new Exception("開發時期錯誤!未定義的選單類別:[" + currTreePickerType.name() + "]");
        }

        /**
         * 準備鎖定項目
         * 
         * @return
         */
        @Override
        public List<String> prepareDisableItemSids() throws Exception {
            return Lists.newArrayList();
        }
    };

}
