/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.ReqDraftService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.SimpleCategoryService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.helper.RequireFlowHelper;
import com.cy.tech.request.logic.vo.UrlParamTo;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.helper.RequireHelper;
import com.cy.tech.request.web.controller.search.TableUpDownBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.ConfirmCallbackDialogController;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 需求單存檔控制
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class ReqDraftMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4282184348236053768L;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private RequireService reqService;
    @Autowired
    transient private ReqDraftService draftService;
    @Autowired
    transient private URLService urlService;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private ConfirmCallbackDialogController confirmCallbackDialogController;
    @Autowired
    transient private RequireFlowHelper requireFlowHelper;
    @Autowired
    transient private RequireHelper requireHelper;
    @Autowired
    transient private SimpleCategoryService simpleCategoryService;

    /**
     * 儲存草稿前檢查
     * 
     * @param r01MBean
     * @param templateMBean
     */
    public void preSaveDraft(Require01MBean r01MBean, RequireTemplateFieldMBean templateMBean) {

        try {
            this.requireHelper.checkInputInfo(
                    r01MBean.getRequire(),
                    r01MBean.getRequireCheckItemMBean(),
                    templateMBean.getTemplateItem(),
                    templateMBean.getComValueMap());

        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        }

        // 特定小類需上傳附加檔案時且為新建檔模式或編輯模式時彈出提示，
        // 直到使用者點擊提示視窗【確定】後再進行存檔
        if (reqService.isPopupAttachNotify(templateMBean.getMapping(), r01MBean.getRequire())) {
            confirmCallbackDialogController.showConfimDialog(
                    templateMBean.getMapping().getSmall().getName() + "類別，需上傳附檔！！",
                    Lists.newArrayList(),
                    () -> this.saveDraft(r01MBean, templateMBean));
            return;
        }

        this.saveDraft(r01MBean, templateMBean);
    }

    /** 儲存草稿 */
    public void saveDraft(Require01MBean r01MBean, RequireTemplateFieldMBean templateMBean) {
        try {
            // 檢核必要輸入欄位
            this.requireHelper.checkInputInfo(
                    r01MBean.getRequire(),
                    r01MBean.getRequireCheckItemMBean(),
                    templateMBean.getTemplateItem(),
                    templateMBean.getComValueMap());

            reqService.firstSaveAttachHandler(r01MBean.getRequire(), r01MBean.getReqAttachMBean().getSelectedAttachments());
            // 重設定主題後綴 20170320
            templateMBean.resetThemeSuffixBySpecCom();

            Require newRequrie = draftService.saveDraft(
                    loginBean.getUser(),
                    r01MBean.getRequire(),
                    r01MBean.getRequireCheckItemMBean().getSelectedCheckItemTypes(),
                    templateMBean.getMapping(),
                    templateMBean.getComValueMap());

            log.info("{} 新增需求單(草稿):{}", SecurityFacade.getUserId(), newRequrie.getRequireNo());
            r01MBean.setRequire(newRequrie);
            r01MBean.getTitleBtnMBean().clear();
            r01MBean.getTraceMBean().clear();
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (NullPointerException | IllegalArgumentException e) {
            log.debug(e.getMessage(), e);
            MessagesUtils.showWarn(e.getMessage());
            return;
        } catch (DataIntegrityViolationException e) {
            String errorMsg = "新增失敗，輸入圖檔符號可能導致資料異常！！";
            log.error(errorMsg + "...userSId => " + loginBean.getUserSId(), e);
            this.saveErrorHandler(r01MBean, templateMBean, errorMsg);
            return;
        } catch (Exception e) {
            String errorMsg = "系統異常導致草稿存檔失敗！！";
            log.error(errorMsg + "...userSId => " + loginBean.getUserSId(), e);
            this.saveErrorHandler(r01MBean, templateMBean, errorMsg);
            return;
        }
        this.saveSucessHandler(r01MBean, templateMBean);
    }

    private void saveSucessHandler(Require01MBean r01MBean, RequireTemplateFieldMBean templateMBean) {
        // 新增
        if (r01MBean.getSetp().equals(RequireStep.NEW_IN_PAGE)) {
            // r01MBean.reloadPage(r01MBean.getRequire());
            // r01MBean.selectCategory(templateMBean, r01MBean.getReqAttachMBean());
            // display.execute("returnToCateSelectForDraft()");

            // 因 iframe 操作時, update src 頁面有問題, 故改為直接導到新頁

            // 兜組 url
            UrlParamTo urlParamTo = urlService.createSimpleUrlTo(
                    SecurityFacade.getUserSid(),
                    r01MBean.getRequire().getRequireNo(),
                    1);

            String url = urlService.createDraftURLLink(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlParamTo);

            this.display.execute("req01_replaceUrl('" + url + "')");
            return;
        }
        // 更新
        if (r01MBean.getSetp().equals(RequireStep.LOAD_IN_PAGE_EDIT)) {
            templateMBean.toggleDisabled(Boolean.TRUE);
            r01MBean.setSetp(RequireStep.LOAD_IN_PAGE);
            templateMBean.recoveryTemplate(r01MBean.getRequire());
            display.update(Lists.newArrayList(
                    "require01_title_info_id",
                    "title_info_click_btn_id",
                    "require_template_id",
                    "viewPanelBottomInfoTabId"));
            if (Faces.getViewId().endsWith("search23.xhtml")) {
                display.execute("reSearch();");
                return;
            }
        }
        // 提交
        if (r01MBean.getSetp().equals(RequireStep.LOAD_IN_PAGE) && this.isSucessBySubmit(r01MBean)) {
            this.doSucessSubmitHandler(r01MBean);
        }
    }

    private boolean isSucessBySubmit(Require01MBean r01MBean) {
        return !r01MBean.getRequire().getRequireStatus().equals(RequireStatusType.DRAFT);
    }

    private void doSucessSubmitHandler(Require01MBean r01MBean) {
        if (Faces.getViewId().endsWith("search23.xhtml")) {
            r01MBean.reloadPage(r01MBean.getRequire());
            display.execute("resetTableBySubmit()");
            return;
        }
        this.redirectReq02BySubmit(r01MBean);
    }

    private void redirectReq02BySubmit(Require01MBean r01MBean) {
        r01MBean.reloadPage(r01MBean.getRequire());
        display.execute("window.opener.handlerBySubmitFromReq06();");
        try {
            String requireNo = r01MBean.getRequire().getRequireNo();
            String url = urlService.createLoacalURLLink(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(loginBean.getUser().getSid(), requireNo, 1));
            Faces.getExternalContext().redirect(url + "&isDraftSubmitAfter=true");
            Faces.getContext().responseComplete();
        } catch (Exception e) {
            log.info("導向需求單失敗..." + e.getMessage(), e);
        }
    }

    private void saveErrorHandler(Require01MBean r01MBean, RequireTemplateFieldMBean templateMBean, String errorMsg) {
        if (r01MBean.getSetp().equals(RequireStep.NEW_IN_PAGE)) {
            r01MBean.getRequire().setSid(null);
            r01MBean.getRequire().getCssContents().clear();
            r01MBean.getRequire().getIndex().clear();
            r01MBean.getRequire().setReqUnitSign(null);
        }
        if (r01MBean.getSetp().equals(RequireStep.LOAD_IN_PAGE_EDIT)) {
            r01MBean.recovery(templateMBean);
            templateMBean.toggleDisabled(Boolean.FALSE);
            r01MBean.reBuildeByUpdateFaild();
        }
        display.execute("hideLoad();");
        MessagesUtils.showWarn(errorMsg);
    }

    /**
     * 儲存草稿前檢查
     * 
     * @param r01MBean
     * @param templateMBean
     */
    public void preUpdateDraft(Require01MBean r01MBean, RequireTemplateFieldMBean templateMBean) {

        try {
            this.requireHelper.checkInputInfo(
                    r01MBean.getRequire(),
                    r01MBean.getRequireCheckItemMBean(),
                    templateMBean.getTemplateItem(),
                    templateMBean.getComValueMap());
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        }

        // 特定小類需上傳附加檔案時且為新建檔模式或編輯模式時彈出提示，
        // 直到使用者點擊提示視窗【確定】後再進行存檔
        if (reqService.isPopupAttachNotify(templateMBean.getMapping(), r01MBean.getRequire())) {
            confirmCallbackDialogController.showConfimDialog(
                    templateMBean.getMapping().getSmall().getName() + "類別，需上傳附檔！！",
                    Lists.newArrayList(
                            "require01_title_info_id",
                            "title_info_click_btn_id",
                            "require_template_id",
                            "viewPanelBottomInfoTabId"),
                    () -> this.updateDraft(r01MBean, templateMBean));
            return;
        }

        this.updateDraft(r01MBean, templateMBean);
    }

    /**
     * 更新草稿
     *
     * @param r01MBean
     * @param templateMBean
     * @throws IOException
     */
    public void updateDraft(Require01MBean r01MBean, RequireTemplateFieldMBean templateMBean) {
        try {
            // 輸入檢核
            this.requireHelper.checkInputInfo(
                    r01MBean.getRequire(),
                    r01MBean.getRequireCheckItemMBean(),
                    templateMBean.getTemplateItem(),
                    templateMBean.getComValueMap());
            // 重設定主題後綴 20170320
            templateMBean.resetThemeSuffixBySpecCom();
            Require newRequrie = draftService.updateDraft(
                    loginBean.getUser(),
                    r01MBean.getRequire(),
                    r01MBean.getRequireCheckItemMBean().getSelectedCheckItemTypes(),
                    templateMBean.getComValueMap());
            r01MBean.setRequire(newRequrie);
            r01MBean.getTitleBtnMBean().clear();
            r01MBean.getTraceMBean().clear();

        } catch (UserMessageException ex) {
            MessagesUtils.show(ex);
            return;
        } catch (NullPointerException | IllegalArgumentException e) {
            log.debug("檢核失敗", e);
            templateMBean.toggleDisabled(Boolean.FALSE);
            MessagesUtils.showWarn(e.getMessage());
            return;
        } catch (DataIntegrityViolationException e) {
            String errorMsg = "新增失敗，輸入圖檔符號可能導致資料異常！！";
            log.error(errorMsg + "...userSId => " + loginBean.getUserSId(), e);
            this.saveErrorHandler(r01MBean, templateMBean, errorMsg);
            return;
        } catch (Exception e) {
            String errorMsg = "系統異常導致存檔失敗！！";
            log.error(errorMsg + "...userSId => " + loginBean.getUserSId(), e);
            this.saveErrorHandler(r01MBean, templateMBean, errorMsg);
            return;
        }
        this.saveSucessHandler(r01MBean, templateMBean);
    }

    /**
     * 檢視是否需彈出ON程式通知
     *
     * @param r01MBean
     * @return
     */
    public boolean popupOnpgNotify(Require01MBean r01MBean) {
        if (r01MBean.getRequire() == null) {
            return false;
        }

        Require require = r01MBean.getRequire();

        if (!reqService.isPopupConfirmOnpgNotify(r01MBean.getRequire())) {
            return false;
        }

        //====================================
        //需要簽核
        //====================================
        try {
            if (this.simpleCategoryService.isNeedReqUnitSign(require.getMapping().getSmall().getSid())) {
                return false;
            }
        } catch (SystemOperationException e) {
            MessagesUtils.showError(e.getMessage());
            return false;
        }

        return true;
    }

    public void submitDraftBefor(Require01MBean r01MBean, RequireTemplateFieldMBean templateMBean) throws IOException {

        // 特定小類需上傳附加檔案時且為新建檔模式或編輯模式時彈出提示，
        // 直到使用者點擊提示視窗【確定】後再進行存檔
        if (reqService.isPopupAttachNotify(templateMBean.getMapping(), r01MBean.getRequire())) {
            confirmCallbackDialogController.showConfimDialog(
                    templateMBean.getMapping().getSmall().getName() + "類別，需上傳附檔！！",
                    Lists.newArrayList(
                            "require01_title_info_id",
                            "title_info_click_btn_id",
                            "require_template_id",
                            "viewPanelBottomInfoTabId"),
                    () -> this.checkNeedPopupOnpgCompleteNotify(r01MBean, templateMBean));
            return;
        }

        this.checkNeedPopupOnpgCompleteNotify(r01MBean, templateMBean);
    }

    public void checkNeedPopupOnpgCompleteNotify(Require01MBean r01MBean, RequireTemplateFieldMBean templateMBean) {
        try {
            this.submitDraft(r01MBean, templateMBean);
        } catch (NullPointerException | IllegalArgumentException e) {
            log.debug("檢核失敗", e);
            templateMBean.toggleDisabled(Boolean.FALSE);
            display.execute("draftSubmitFaild('" + Faces.getViewId() + "');");
            MessagesUtils.showWarn(e.getMessage());
        }
    }

    /**
     * 提交草稿
     *
     * @param r01MBean
     * @param templateMBean
     * @throws IOException
     */
    public void submitDraft(Require01MBean r01MBean, RequireTemplateFieldMBean templateMBean) {
        try {
            // 檢查是否可提交草稿
            draftService.checkCanSubmit(r01MBean.getRequire());

            // 載入檢查項目
            r01MBean.getRequireCheckItemMBean().reload(r01MBean.getRequire().getSid());

            // 執行草稿提交
            Require require = this.requireFlowHelper.processForSubmitDraft(
                    r01MBean.getRequire().getSid(),
                    r01MBean.getRequireCheckItemMBean().getSelectedCheckItemTypes(),
                    templateMBean.getComValueMap(),
                    loginBean.getUser(),
                    new Date());

            r01MBean.setRequire(require);

        } catch (UserMessageException ex) {
            MessagesUtils.show(ex);
            templateMBean.toggleDisabled(Boolean.FALSE);
            display.execute("draftSubmitFaild('" + Faces.getViewId() + "');");
            return;
        } catch (NullPointerException | IllegalArgumentException e) {
            log.debug("檢核失敗", e);
            templateMBean.toggleDisabled(Boolean.FALSE);
            display.execute("draftSubmitFaild('" + Faces.getViewId() + "');");
            MessagesUtils.showWarn(e.getMessage());
            return;
        } catch (Exception e) {
            String errorMsg = "系統異常導致提交失敗！！";
            log.error(errorMsg + "...userSId => " + loginBean.getUserSId(), e);
            display.execute("draftSubmitFaild('" + Faces.getViewId() + "');");
            this.saveErrorHandler(r01MBean, templateMBean, errorMsg);
            return;
        }
        this.saveSucessHandler(r01MBean, templateMBean);
    }

    /**
     * 刪除草稿
     *
     * @param r01MBean
     */
    public void draftDelete(Require01MBean r01MBean, RequireTemplateFieldMBean templateMBean) {
        try {
            draftService.draftDelete(loginBean.getUser(), r01MBean.getRequire(), templateMBean.getComValueMap());
            this.draftDeleteSucess(r01MBean);
        } catch (Exception e) {
            String errorMsg = "系統異常導致刪除失敗！！";
            log.error(errorMsg + "...userSId => " + loginBean.getUserSId(), e);
            this.saveErrorHandler(r01MBean, templateMBean, errorMsg);
        } finally {
            display.hidePfWidgetVar("draft_del_noti_dlg_wv");
        }
    }

    /**
     * 草稿移除後動作
     *
     * @param r01MBean
     */
    private void draftDeleteSucess(Require01MBean r01MBean) {
        r01MBean.getRequire().setIsDraftDelete(true);

        String whenDelDraftActionScript = ""
                + "if(window.whenDelDraftAction){whenDelDraftAction();"
                + "}else if(window && window.opener && window.opener.whenDelDraftAction){"
                + "window.opener.whenDelDraftAction();"
                + "}";

        if (Faces.getViewId().endsWith("search23.xhtml")) {
            display.execute(whenDelDraftActionScript);
        }
        if (Faces.getViewId().endsWith("require06.xhtml")) {
            if (r01MBean.getIsDraftCreateAfter()) {// require01.xhtml
                upDownBean.disableAll();
            } else {// require06.xhtml
                // 以下為分頁處理
                display.execute("replaceUrlNotNavigation('./require01.xhtml')");// 清除URL避免重新load單
                display.execute(whenDelDraftActionScript);// 呼叫父頁面執行表格重置
            }
        }
        display.update(Lists.newArrayList(
                "title_info_click_btn_id",
                "require01_title_info_id",
                "require_template_id",
                "viewPanelBottomInfoTabId"));
    }

}
