package com.cy.tech.request.web.controller.view.component;

import java.util.Date;

import org.joda.time.LocalDate;

import lombok.Getter;
import lombok.Setter;

/**
 * @author aken_kao
 */
public class WeekIntervalVO {

    @Getter
    @Setter
    /** 起始時間 */
    private Date startDate;
    @Getter
    @Setter
    /** 結束時間 */
    private Date endDate;
    @Getter
    @Setter
    /** 時間切換的index */
    private int dateTypeIndex = -1;
    
    public void init(int dateTypeIndex){
        this.dateTypeIndex = dateTypeIndex;
        switch(dateTypeIndex){
        case 0:
            changeDateIntervalPreWeek();
            break;
        case 1:
            changeDateIntervalThisWeek();
            break;
        case 2:
            changeDateIntervalNextWeek();
            break;
        case 3:
            changeDateIntervalToDay();
            break;
        default:
            startDate = null;
            endDate = new Date();
            break;
                
        }
    }
    
    public void clear() {
        dateTypeIndex = -1;
        startDate = null;
        endDate = null;
    }
    
    /**
     * 上週
     */
    public void changeDateIntervalPreWeek() {
        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate lastDate = new LocalDate(date).minusWeeks(1);
        this.dateTypeIndex = 0;
        this.startDate = lastDate.dayOfWeek().withMinimumValue().toDate();
        this.endDate = lastDate.dayOfWeek().withMaximumValue().toDate();
    }

    /**
     * 本週
     */
    public void changeDateIntervalThisWeek() {
        this.dateTypeIndex = 1;
        this.startDate = new LocalDate().dayOfWeek().withMinimumValue().toDate();
        this.endDate = new LocalDate().dayOfWeek().withMaximumValue().toDate();
    }

    /**
     * 下週
     */
    public void changeDateIntervalNextWeek() {
        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate nextDate = new LocalDate(date).plusWeeks(1);
        this.dateTypeIndex = 2;
        this.startDate = nextDate.dayOfWeek().withMinimumValue().toDate();
        this.endDate = nextDate.dayOfWeek().withMaximumValue().toDate();
    }

    /**
     * 今日
     */
    public void changeDateIntervalToDay() {
        this.dateTypeIndex = 3;
        this.startDate = new Date();
        this.endDate = new Date();
    }
}
