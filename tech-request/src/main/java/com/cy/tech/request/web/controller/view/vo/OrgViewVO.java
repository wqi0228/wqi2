/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.vo;

import java.io.Serializable;
import lombok.Getter;

/**
 * 單位介面物件
 *
 * @author brain0925_liao
 */
public class OrgViewVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5849652186280821677L;
    public OrgViewVO(Integer orgSid, String orgName) {
        this.orgSid = orgSid;
        this.orgName = orgName;
    }

    @Getter
    private Integer orgSid;
    @Getter
    private String orgName;

}
