package com.cy.tech.request.web.controller.component.qkstree;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.primefaces.model.TreeNode;

import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen
 *
 */
@Slf4j
public abstract class AbstractQucikSelectTreeComponentMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 253814940429940201L;

    public static final String STYLE_CONTENT_TRUE_DATA = "STYLE_CONTENT_TRUE_DATA";

    /**
     * 
     */
    protected QuickSelectTreeCallback callback;

    @Getter
    private String memoMessage = "※&nbsp;<span style='font-weight: bold;text-decoration:underline;'>雙擊&nbsp;</span>清單項目選取 (雙擊項目後移至右邊或左邊)";

    /**
     * 樹節點結構資料
     */
    @Getter
    protected TreeNode rootTreeNode;

    /**
     * 選擇的節點
     */
    @Getter
    @Setter
    protected TreeNode selectedNode;

    /**
     * 
     */
    @Getter
    protected List<?> selectedList;

    /**
     * 是否顯示停用
     */
    @Getter
    @Setter
    protected Boolean queryShowInactiveNode = false;

    /**
     * 
     */
    protected Map<String, AbstractTreeComponentNodeData> allNodeDataMapByRowKey;
    protected Map<String, AbstractTreeComponentNodeData> allNodeDataMapBySid;

    /**
     * 
     */
    @Getter
    protected String nodeDataJsonString = "";

    /**
     * 
     */
    @Getter
    @Setter
    protected String selectedNodeDataJsonString = "";

    /**
     * 連動資料
     */
    @Getter
    @Setter
    protected String linkageMapJsonString = "";

    /**
     * 
     */
    @Getter
    @Setter
    protected boolean selectedListMode = false;

    /**
     * 訊息
     */
    @Getter
    public String message = "";

    /**
     * 切換顯示模式(正常/停用)
     */
    public abstract void switchShowActiveOrinactiveMode();

    /**
     * @return
     */
    public abstract List<?> getSelectedDataList();

    @Getter
    public boolean renderFlag = false;

    public void preOpenDlg() {
        // log.debug("preOpenDlg");
        this.renderFlag = true;
    }

    public void afterCloseDlg() {
        // log.debug("afterCloseDlg");
        this.renderFlag = false;
    }

    /**
     * 在初始化前執行
     */
    protected void brforeInitialize(
            QuickSelectTreeCallback callback) {

        // 記錄傳入參數
        this.callback = callback;

        // 預設不【顯示停用】
        this.queryShowInactiveNode = false;
        //
        this.nodeDataJsonString = "{}";
        this.selectedNodeDataJsonString = "[]";
        renderFlag = false;
    }

    /**
     * 初始化後執行
     */
    protected void afterInitialize() {
    }

    /**
     * 
     */
    protected void afterbuildTreeData() {
        // ====================================
        // 整理資料
        // ====================================
        // 將TreeNode 轉為 list
        List<TreeNode> allNodelist = Lists.newArrayList();
        this.treeNodeToList(allNodelist, this.rootTreeNode);

        // 轉為 map 形式
        this.allNodeDataMapByRowKey = Maps.newHashMap();
        this.allNodeDataMapBySid = Maps.newHashMap();

        for (TreeNode eachNode : allNodelist) {
            if (eachNode.getData() == null) {
                continue;
            }
            AbstractTreeComponentNodeData nodeData = (AbstractTreeComponentNodeData) eachNode.getData();
            nodeData.setRowKey(eachNode.getRowKey());

            if (this.allNodeDataMapByRowKey.containsKey(eachNode.getRowKey())) {
                log.info("節點重複[" + eachNode.getRowKey() + ""
                        + "原節點[" + this.allNodeDataMapByRowKey.get(eachNode.getRowKey()).getOuName() + "]"
                        + "新節點[" + nodeData.getOuName() + "]");
            }

            this.allNodeDataMapByRowKey.put(eachNode.getRowKey(), nodeData);

            this.allNodeDataMapBySid.put(this.getAllNodeDataSidMapKey(nodeData), nodeData);

        }
        // 轉JSON以供 client 端調用
        this.nodeDataJsonString = new Gson().toJson(this.allNodeDataMapByRowKey);

        // 連動項目資料（子項實做）
        this.prepareLinkageData();
    }

    /**
     * @param nodeData
     * @return
     */
    public String getAllNodeDataSidMapKey(AbstractTreeComponentNodeData nodeData) {
        return this.getAllNodeDataSidMapKey(nodeData.isDataNode(), nodeData.getSid());
    }

    public String getAllNodeDataSidMapKey(boolean isDataNode, String nodeSid) {
        return isDataNode + nodeSid;
    }

    /**
     * @param nodeData
     * @return
     */
    public String getNodeDisplayContext(AbstractTreeComponentNodeData nodeData) {
        if (nodeData == null) {
            return "";
        }

        // 取得顯示名稱字串
        String displayContext = this.getNodeName(nodeData);
        // 為user 節點時, 前方加入 icon
        if (nodeData.isDataNode()) {
            displayContext = ""
                    + "<span class=\"quick-select-tree-data-node-highlight\">"
                    + "<i class=\"fa\">"
                    + "</i>&nbsp;&nbsp;"
                    + displayContext
                    + "</span>";
        }

        return displayContext;
    }

    /**
     * 取得節點名稱
     * 
     * @param nodeData
     * @return
     */
    public String getNodeName(AbstractTreeComponentNodeData nodeData) {

        String displayName = "<span class='ui-treenode-name'></span>";

        // 取得 node data
        if (nodeData == null) {
            return displayName;
        }

        // 取得節點名稱
        displayName = WkStringUtils.removeCtrlChr(WkStringUtils.safeTrim(nodeData.getName()));

        // 加上 class 標示 (client script 用)
        return "<span class='ui-treenode-name'>" + displayName + "</span>";
    }

    /**
     * 將所有樹狀節點加入list中
     *
     * @param list
     * @param root
     */
    protected void treeNodeToList(List<TreeNode> list, TreeNode root) {
        if (root == null || root.getChildren() == null) {
            return;
        }
        root.getChildren().forEach(each -> {
            list.add(each);
            this.treeNodeToList(list, each);
        });
    }

    /**
     * 準備連動資料 (需 overwrite)
     */
    public void prepareLinkageData() {
        this.linkageMapJsonString = "{}";
    }

    /**
     * @author allen1214_wu
     *
     */
    @Data
    public abstract class AbstractTreeComponentNodeData implements Serializable {

        /**
         * 
         */
        private static final long serialVersionUID = 7711451727966382020L;

        /**
         * 標註僅進行一次連動
         */
        public static final String INFO_KEY_NOT_COMBO_LINKAGE = "NOT_COMBO_LINKAGE";

        /**
         * 名稱
         */
        protected String name;
        /**
         * SID
         */
        protected String sid;
        /**
         * 是否為 『可選擇』 節點 (非階層)
         */
        protected boolean dataNode = false;
        /**
         * tree node key
         */
        protected String rowKey;
        /**
         * 是否啟用
         */
        protected boolean active = false;
        /**
         * 顯示用名稱
         */
        protected String ouName;

        /**
         * 其他註記資料
         */
        protected Map<String, String> otherInfo = Maps.newHashMap();
    }
}
