/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.jsf.converter;

import com.cy.tech.request.logic.enumerate.RejectReason;
import com.cy.work.common.utils.WkStringUtils;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.springframework.stereotype.Component;

@Component("rejectReasonConverter")
public class RejectReasonConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        for (RejectReason rejectReason : RejectReason.values()) {
            if (rejectReason.name().equals(WkStringUtils.safeTrim(value))) {
                return rejectReason;
            }
        }

        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return null;
        }

        if (value instanceof RejectReason) {
            return ((RejectReason) value).name();

        }
        return null;
    }
}
