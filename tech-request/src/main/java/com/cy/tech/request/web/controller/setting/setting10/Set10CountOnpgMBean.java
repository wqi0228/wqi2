/**
 * 
 */
package com.cy.tech.request.web.controller.setting.setting10;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.primefaces.model.DefaultStreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.tech.request.logic.service.onpg.Set10CountOnpgService;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.rbt.util.exceloperate.ExcelExporter;
import com.rbt.util.exceloperate.bean.expt.ColumnDataSet;
import com.rbt.util.exceloperate.bean.expt.ExportDataSet;
import com.rbt.util.exceloperate.bean.expt.config.ExportConfigInfo;
import com.rbt.util.exceloperate.config.ExportConfigReader;
import com.rbt.util.exceloperate.exception.ExcelOperateException;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Slf4j
@Controller
@Scope("view")
public class Set10CountOnpgMBean implements Serializable {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -5058739207949920616L;

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private Set10CountOnpgService set10CountOnpgService;
    @Autowired
    private transient ServletContext servletContext;

    // ========================================================================
    // 變數區
    // ========================================================================
    /**
     * 查詢起始時間
     */
    @Getter
    @Setter
    private Date conditionStartDate;
    /**
     * 查詢結束時間
     */
    @Getter
    @Setter
    private Date conditionEndDate = WkDateUtils.convertToEndOfDay(new Date());

    /**
     * 執行狀況查詢起始時間
     */
    @Getter
    @Setter
    private Date conditionExecStatusStartDate;

    /**
     * 執行狀況查詢結束時間
     */
    @Getter
    @Setter
    private Date conditionExecStatusEndDate = WkDateUtils.convertToEndOfDay(new Date());

    /**
     * 
     */
    @Getter
    private DefaultStreamedContent download;

    // ========================================================================
    // 方法
    // ========================================================================
    @PostConstruct
    public void init() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MONTH, -3);
        this.conditionExecStatusStartDate = calendar.getTime();
    }

    public void process() {
        // conditionStartDate = new java.util.GregorianCalendar(2020, 01, 01).getTime();

        // ====================================
        // 查詢
        // ====================================
        ColumnDataSet columnDataSet = this.set10CountOnpgService.process(conditionStartDate, conditionEndDate);
        if (columnDataSet == null) {
            MessagesUtils.showInfo("無資料，請調整查詢區間!");
            return;
        }

        log.debug("組資料ok");

        // System.out.println(WkJsonUtils.getInstance().toPettyJson(columnDataSet));

        // ====================================
        // 組 ExportDataSet
        // ====================================

        ExportDataSet exportDataSet = new ExportDataSet();

        // title
        Map<String, Object> titleMap = Maps.newHashMap();
        String dateRange = "";
        if (this.conditionStartDate != null) {
            dateRange += WkDateUtils.formatDate(this.conditionStartDate, WkDateUtils.YYYY_MM_DD2);
        }
        dateRange += "～";
        if (this.conditionEndDate == null) {
            this.conditionEndDate = new Date();
        }
        dateRange += WkDateUtils.formatDate(this.conditionEndDate, WkDateUtils.YYYY_MM_DD2);
        titleMap.put("DATE_RANGE", dateRange);

        exportDataSet.setContext("title", titleMap);

        // header
        exportDataSet.setContext("header", new HashMap<>());

        List<ColumnDataSet> detailDataSetList = Lists.newArrayList();
        exportDataSet.setDetailDataSet("detail", detailDataSetList);

        detailDataSetList.add(columnDataSet);

        // ====================================
        // 取得EXCEL格式定義
        // ====================================
        File excelExportDefFile = new File(servletContext.getRealPath("/WEB-INF/excel/excel-export.xml"));
        if (!excelExportDefFile.exists()) {
            log.error("excel-export.xml 檔案不存在!");
            MessagesUtils.showInfo("excel-export.xml 檔案不存在!");
            return;
        }
        String path = servletContext.getRealPath("/WEB-INF/excel/excel-export.xml");
        ExportConfigInfo exportConfigInfo = null;
        try {
            exportConfigInfo = new ExportConfigReader().read(path, "CountOnpg");
        } catch (Exception e) {
            String errorMessage = "讀取 excel-export.xml 失敗![" + e.getMessage() + "]";
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);
            return;
        }

        log.debug("讀取設定檔ok");

        // ====================================
        // 組輸出檔名
        // ====================================
        String encodedFilename = "ON程式統計表(" + dateRange + ").xls";
        try {
            encodedFilename = URLEncoder.encode(encodedFilename, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error("檔案名稱編碼失敗! [" + e.getMessage() + "]", e);
        }

        // ====================================
        // 取得EXCEL格式定義
        // ====================================
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        try {
            new ExcelExporter().export(exportConfigInfo, exportDataSet, buffer);
        } catch (ExcelOperateException e) {
            log.error(e.getMessage(), e);
            return;
        }
        log.debug("ExcelExporter");

        byte[] bytes = buffer.toByteArray();
        InputStream inputStream = new ByteArrayInputStream(bytes);

        this.download = new DefaultStreamedContent(inputStream, "application/vnd.ms-excel", encodedFilename);
    }

    public void processExecStats() {

        // ====================================
        // 查詢
        // ====================================
        List<ColumnDataSet> columnDataSets = this.set10CountOnpgService.processExecStats(
                this.conditionExecStatusStartDate, this.conditionExecStatusEndDate);
        if (WkStringUtils.isEmpty(columnDataSets)) {
            MessagesUtils.showInfo("無資料，請調整查詢區間!");
            return;
        }

        log.debug("組資料ok");

        // ====================================
        // 組 ExportDataSet
        // ====================================
        ExportDataSet exportDataSet = new ExportDataSet();

        // title
        Map<String, Object> titleMap = Maps.newHashMap();
        String dateRange = "";
        if (this.conditionExecStatusEndDate != null) {
            dateRange += WkDateUtils.formatDate(this.conditionExecStatusStartDate, WkDateUtils.YYYY_MM_DD2);
        }
        dateRange += "～";
        if (this.conditionExecStatusEndDate == null) {
            this.conditionExecStatusEndDate = new Date();
        }
        dateRange += WkDateUtils.formatDate(this.conditionExecStatusEndDate, WkDateUtils.YYYY_MM_DD2);
        titleMap.put("DATE_RANGE", dateRange);

        List<String> descrs = Lists.newArrayList();
        descrs.add("新增：當日新增的單據");
        descrs.add("完工：當日所有派工單位全部完成");
        descrs.add("完工平均：當日完工的單據，從建單日到完工日的耗時 （總耗時/完工單據數）");
        descrs.add("結案：當日被結案的單據");
        descrs.add("結案平均：當日結案的單據，從建單日到結案日的耗時 （總耗時/結案單據數）");
        descrs.add("逾期未完工：當日有多少單據超過『希望完成日』還未完成");
        descrs.add("逾期未結案：當日有多少單據超過『希望完成日』還未結案");
        titleMap.put("descr", descrs.stream().collect(Collectors.joining("\r\n")));

        exportDataSet.setContext("title", titleMap);

        // header
        exportDataSet.setContext("header", new HashMap<>());

        exportDataSet.setDetailDataSet("detail", columnDataSets);

        // ====================================
        // 取得EXCEL格式定義
        // ====================================
        File excelExportDefFile = new File(servletContext.getRealPath("/WEB-INF/excel/excel-export.xml"));
        if (!excelExportDefFile.exists()) {
            log.error("excel-export.xml 檔案不存在!");
            MessagesUtils.showInfo("excel-export.xml 檔案不存在!");
            return;
        }
        String path = servletContext.getRealPath("/WEB-INF/excel/excel-export.xml");
        ExportConfigInfo exportConfigInfo = null;
        try {
            exportConfigInfo = new ExportConfigReader().read(path, "CountRequireExecStatus");
        } catch (Exception e) {
            String errorMessage = "讀取 excel-export.xml 失敗![" + e.getMessage() + "]";
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);
            return;
        }

        log.debug("讀取設定檔ok");

        // ====================================
        // 組輸出檔名
        // ====================================
        String encodedFilename = "需求單執行狀況統計(" + dateRange + ").xls";
        try {
            encodedFilename = URLEncoder.encode(encodedFilename, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error("檔案名稱編碼失敗! [" + e.getMessage() + "]", e);
        }

        // ====================================
        // 取得EXCEL格式定義
        // ====================================
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        try {
            new ExcelExporter().export(exportConfigInfo, exportDataSet, buffer);
        } catch (ExcelOperateException e) {
            log.error(e.getMessage(), e);
            return;
        }
        log.debug("ExcelExporter");

        byte[] bytes = buffer.toByteArray();
        InputStream inputStream = new ByteArrayInputStream(bytes);

        this.download = new DefaultStreamedContent(inputStream, "application/vnd.ms-excel", encodedFilename);

    }

    // ========================================================================
    //
    // ========================================================================

}
