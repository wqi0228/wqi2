/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search19QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.Search19View;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.othset.OthSetService;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.searchheader.CommonHeaderMBean;
import com.cy.tech.request.web.controller.searchheader.Search19HeaderMBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.cache.WkCommonCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 其它設定一覽表
 *
 * @author jason_h
 */
@Controller
@Scope("view")
@Slf4j
public class Search19MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1096133866380917266L;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private Search19HeaderMBean headerMBean;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private CategoryTreeMBean cateTreeMBean;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private OthSetService osService;
    @Autowired
    transient private Require01MBean r01MBean;
    @Autowired
    transient private WkCommonCache wkCommonCache;
    @Autowired
    transient private Search19QueryService search19QueryService;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchResultHelper searchResultHelper;

    /** 所有的其它設定單 */
    @Getter
    @Setter
    private List<Search19View> queryItems;
    private List<Search19View> tempItems;

    /** 選擇的其它設定單 */
    @Getter
    @Setter
    private Search19View querySelection;

    /** 上下筆移動keeper */
    @Getter
    private Search19View queryKeeper;

    /** 切換模式 */
    @Getter
    private SwitchType switchType = SwitchType.CONTENT;

    /** 切換模式 - 全畫面狀態 */
    @Getter
    private SwitchType switchFullType = SwitchType.DETAIL;

    /** 在匯出的時候，某些內容需要隱藏 */
    @Getter
    private boolean hasDisplay = true;

    @Getter
    private final String dataTableId = "dtRequire";
    @Setter
    @Getter
    /** dataTable filter */
    private String createDepName;
    @Getter
    /** dataTable filter items */
    private List<SelectItem> createDepNameItems;

    @PostConstruct
    public void init() {
        this.search();
    }

    public void search() {
        tempItems = this.findWithQuery();
        this.createDepName = null;
        this.createDepNameItems = this.tempItems.stream()
                .map(each -> each.getCreateDepName())
                .collect(Collectors.toSet()).stream()
                .map(each -> new SelectItem(each, each))
                .collect(Collectors.toList());
        this.doChangeFilter();
    }

    /**
     * 進行dataTable filter
     */
    public void doChangeFilter() {
        this.queryItems = this.filterDataTable();
    }

    /**
     * filter
     *
     * @return
     */
    private List<Search19View> filterDataTable() {
        return tempItems.stream()
                .filter(each -> Strings.isNullOrEmpty(createDepName)
                        || createDepName.equals(each.getCreateDepName()))
                .collect(Collectors.toList());
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        headerMBean.clear();
        this.search();
    }

    private List<Search19View> findWithQuery() {
        String requireNo = reqularUtils.getRequireNo(loginBean.getCompanyId(), commHeaderMBean.getFuzzyText());
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();

        builder.append("SELECT "
                + "os.os_sid,"
                + "tr.require_no,"
                + "tid.field_content,"
                + "os.create_dt AS osCreateDt,"
                + "os.os_theme,"
                + "os.os_notice_deps,"
                + "os.os_status,"
                + "os.dep_sid as osCreateDep,"
                + "os.create_usr AS osCreateUsr,"
                + "tr.create_dt AS reqCreateDt,"
                + "ckm.big_category_name,"
                + "ckm.middle_category_name,"
                + "ckm.small_category_name,"
                + "tr.dep_sid AS requireDep,"
                + "tr.create_usr AS requireUser,"
                + "tr.urgency,"
                + "tr.require_status, "
                + "os.os_no, "
                // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
                + this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire()

                + " FROM ");
        buildOsCondition(requireNo, builder, parameters);
        buildRequireCondition(requireNo, builder, parameters);
        buildRequireIndexDictionaryCondition(builder, parameters);
        buildCategoryKeyMappingCondition(requireNo, builder, parameters);
        // 檢查項目 (系統別)
        // 後方需對 主單 sid 做 group by
        builder.append(this.searchConditionSqlHelper.prepareCommonJoin(SecurityFacade.getUserSid()));

        builder.append("WHERE os.os_sid IS NOT NULL ");
        builder.append(" GROUP BY os.os_sid ");
        builder.append("  ORDER BY os.update_dt DESC");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.OTHER_SETUP_SETTING, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.OTHER_SETUP_SETTING, SecurityFacade.getUserSid());

        // 查詢
        List<Search19View> resultList = search19QueryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // ====================================
        // 後續處理
        // ====================================
        if (WkStringUtils.notEmpty(resultList)) {
            // 後續處理-開始
            usageRecord.afterProcessStart();
            // 查詢條件過濾：系統別
            resultList = resultList.stream()
                    .filter(each -> this.searchResultHelper.filterCheckItems(
                            each.getCheckItemTypes(),
                            commHeaderMBean.getCheckItemTypes(),
                            false,
                            null))
                    .collect(Collectors.toList());

            // 後續處理-結束
            usageRecord.afterProcessEnd();
        }

        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return resultList;

    }

    private void buildOsCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("(SELECT * FROM tr_os os WHERE 1=1");
        // 挑選主題
        if (Strings.isNullOrEmpty(requireNo) && headerMBean.getSelectOthSetTheme() != null && !headerMBean.getSelectOthSetTheme().isEmpty()) {
            builder.append(" AND os.os_theme = :os_theme");
            parameters.put("os_theme", headerMBean.getSelectOthSetTheme());
        }
        // 提出區間
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getStartDate() != null && commHeaderMBean.getEndDate() != null) {
            builder.append(" AND os.create_dt BETWEEN :startDate AND :endDate");
            parameters.put("startDate", helper.transStartDate(commHeaderMBean.getStartDate()));
            parameters.put("endDate", helper.transEndDate(commHeaderMBean.getEndDate()));
        }

        // 填寫其他設定資料的使用者與歸屬單位的成員們
        // 通知單位的成員 與主管 / 部級主管 與 處級主管 也要能看的到單據
        builder.append(" AND (os.dep_sid IN (:depSids) OR os.os_notice_deps REGEXP :depSid)");
        parameters.put("depSids", commHeaderMBean.getRequireDepts() == null || commHeaderMBean.getRequireDepts().isEmpty()
                ? "nerverFind"
                : commHeaderMBean.getRequireDepts());
        parameters.put("depSid", Strings.isNullOrEmpty(commHeaderMBean.createNoticeRegexpParam())
                ? "nerverFind"
                : commHeaderMBean.createNoticeRegexpParam());
        // 填單人員
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(commHeaderMBean.getTrCreatedUserName())) {
            List<Integer> userSids = WkUserUtils.findByNameLike(commHeaderMBean.getTrCreatedUserName(), SecurityFacade.getCompanyId());
            if (WkStringUtils.isEmpty(userSids)) {
                userSids.add(-999);
            }
            builder.append(" AND os.create_usr IN (:userSids)");
            parameters.put("userSids", userSids.isEmpty() ? "" : userSids);
        }
        // 其它資料設定狀態
        if (Strings.isNullOrEmpty(requireNo) && headerMBean.getOsStatus() != null && !headerMBean.getOsStatus().isEmpty()) {
            builder.append(" AND os.os_status IN (:osStatus)");
            parameters.put("osStatus", headerMBean.getOsStatus());
        }
        // 模糊搜尋
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.isFuzzyQuery()) {
            String text = "%" + reqularUtils.replaceIllegalSqlLikeStr(commHeaderMBean.getFuzzyText()) + "%";
            builder.append(" AND (os.os_content LIKE :text OR os.os_theme LIKE :text OR os_note LIKE :text OR ");
            builder.append("      os.os_sid IN (SELECT osr.os_sid FROM tr_os_reply osr WHERE osr.reply_content LIKE :text) OR ");
            builder.append("      os.os_sid IN (SELECT osrr.os_sid FROM tr_os_reply_and_reply osrr WHERE osrr.reply_content LIKE :text) ");
            builder.append("     )");
            parameters.put("text", text);
        }
        builder.append(") AS os ");
    }

    private void buildRequireCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_require tr WHERE tr.has_os=1");

        // 需求類別製作進度
        builder.append(" AND tr.require_status IN (:requireStatus)");
        if (Strings.isNullOrEmpty(requireNo)) {
            parameters.put("requireStatus", headerMBean.createQueryReqStatus());
        } else {
            parameters.put("requireStatus", headerMBean.createQueryAllReqStatus());
        }

        // 需求單號
        if (!Strings.isNullOrEmpty(requireNo)) {
            builder.append(" AND tr.require_no = :requireNo");
            parameters.put("requireNo", requireNo);
        }
        builder.append(") AS tr ON tr.require_no = os.require_no ");
    }

    private void buildRequireIndexDictionaryCondition(StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_index_dictionary tid WHERE "
                + "tid.field_name='主題') AS tid ON "
                + "tr.require_sid = tid.require_sid ");
    }

    private void buildCategoryKeyMappingCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_category_key_mapping ckm WHERE 1=1");
        // 類別組合
        if (Strings.isNullOrEmpty(requireNo) && (!cateTreeMBean.getBigDataCateSids().isEmpty() || !cateTreeMBean.getMiddleDataCateSids().isEmpty()
                || !cateTreeMBean.getSmallDataCateSids().isEmpty())) {
            builder.append(" AND ckm.big_category_sid IN (:bigs) OR ckm.middle_category_sid IN (:middles) OR ckm.small_category_sid IN "
                    + "(:smalls) ");
            // 加入單獨選擇的大類
            if (commHeaderMBean.getSelectBigCategory() != null) {
                cateTreeMBean.getBigDataCateSids().add(commHeaderMBean.getSelectBigCategory().getSid());
            }
            parameters.put("bigs", cateTreeMBean.getBigDataCateSids().isEmpty() ? "" : cateTreeMBean.getBigDataCateSids());
            parameters.put("middles", cateTreeMBean.getMiddleDataCateSids().isEmpty() ? "" : cateTreeMBean.getMiddleDataCateSids());
            parameters.put("smalls", cateTreeMBean.getSmallDataCateSids().isEmpty() ? "" : cateTreeMBean.getSmallDataCateSids());
        }
        // 需求類別
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getSelectBigCategory() != null && cateTreeMBean.getBigDataCateSids().isEmpty()) {
            builder.append(" AND ckm.big_category_sid = :big ");
            parameters.put("big", commHeaderMBean.getSelectBigCategory().getSid());
        }

        builder.append(") AS ckm ON tr.mapping_sid=ckm.key_sid ");
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            this.moveScreenTab();
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (Search19View) event.getObject();
        this.changeRequireContent(this.queryKeeper);
        this.moveScreenTab();
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Search19View view) {
        view.setReadRecordType(ReadRecordType.HAS_READ);
        Require require = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(require, loginBean.getUser(), RequireBottomTabType.OTH_SET);
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Search19View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
        this.moveScreenTab();
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
        this.moveScreenTab();
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        helper.exportExcel(document, commHeaderMBean.getStartDate(), commHeaderMBean.getEndDate(), commHeaderMBean.getReportType());
        hasDisplay = true;
    }

    /**
     * 隱藏部分column裡的內容
     */
    public void hideColumnContent() {
        hasDisplay = false;
    }

    public String getNoticeDepsStr(Search19View view) {

        // cache 中取回
        String cacheName = "Search19View-getNoticeDepsStr";
        List<String> keys = Lists.newArrayList(view.getOsno());
        String orgNames = wkCommonCache.getCache(cacheName, keys, 5);
        if (orgNames != null) {
            return orgNames;
        }

        // 組單位名稱
        List<String> orgSids = Optional.ofNullable(view.getOsNoticeDeps())
                .map(send -> send.getValue())
                .orElse(Lists.newArrayList());

        orgNames = WkOrgUtils.findNameBySidStrs(orgSids, "、");

        // put 快取
        wkCommonCache.putCache(cacheName, keys, orgNames);

        return orgNames;

    }

    public void moveScreenTab() {
        this.r01MBean.getBottomTabMBean().resetTabIdx(this.r01MBean);
        this.r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.OTH_SET);

        List<String> sids = osService.findSidsByRequire(this.r01MBean.getRequire());
        String indicateSid = queryKeeper.getSid();
        if (sids.indexOf(indicateSid) != -1) {
            display.execute("PF('os_acc_panel_layer_zero').select(" + sids.indexOf(indicateSid) + ");");
            for (int i = 0; i < sids.size(); i++) {
                if (i != sids.indexOf(indicateSid)) {
                    display.execute("PF('os_acc_panel_layer_zero').unselect(" + i + ");");
                }
            }
        }
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search19View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            display.update("headerTitle");
            display.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Search19View to) {
        querySelection = to;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
        upDownBean.resetTabInfo(RequireBottomTabType.OTH_SET, queryKeeper.getSid());
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }
}
