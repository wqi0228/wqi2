/**
 * 
 */
package com.cy.tech.request.web.controller.component.mutiuserqkpker;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.tech.request.web.controller.component.mutiuserqkpker.model.MultiUserQuickPickerUserGroupData;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.logic.SettingCustomGroupService;
import com.cy.work.vo.SettingCustomGroupVO;
import com.cy.work.vo.enums.CustomGroupType;
import com.google.common.collect.Lists;

import lombok.NoArgsConstructor;

/**
 * @author allen
 *
 */
@NoArgsConstructor
@Component
public class MultiUserQuickPickerHelper implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3112741057902112369L;
    // ====================================
    // for InitializingBean
    // ====================================
    private static MultiUserQuickPickerHelper instance;

    public static MultiUserQuickPickerHelper getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        MultiUserQuickPickerHelper.instance = this;
    }

    // ========================================================================
    // 服務區
    // ========================================================================
    @Autowired
    private transient SettingCustomGroupService settingCustomGroupService;

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * 查詢自定義人員群組
     * 
     * @param userSid user sid
     * @return
     */
    public List<MultiUserQuickPickerUserGroupData> findUserGroups(Integer userSid) {

        // ====================================
        // 查詢自定義人員群組
        // ====================================
        List<SettingCustomGroupVO> vos = this.settingCustomGroupService.findRelationGroupByUser(
                userSid, CustomGroupType.USER);

        if (WkStringUtils.isEmpty(vos)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 轉專用 model
        // ====================================
        List<MultiUserQuickPickerUserGroupData> groupDatas = Lists.newArrayList();
        for (SettingCustomGroupVO vo : vos) {
            groupDatas.add(
                    new MultiUserQuickPickerUserGroupData(
                            vo.getGroupName(),
                            vo.getGroupUserSids()));
        }

        return groupDatas;
    }
}
