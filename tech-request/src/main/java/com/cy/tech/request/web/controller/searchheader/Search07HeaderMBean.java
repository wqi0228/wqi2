/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.searchheader;

import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireFinishCodeType;
import com.cy.work.common.enums.ReadRecordType;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.google.common.collect.Lists;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

/**
 * 已分派單據查詢的搜尋表頭
 *
 * @author jason_h
 */
@Controller
@Scope("view")
public class Search07HeaderMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1966107683634130833L;

    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;

    @Getter
    @Setter
    private List<String> selectFinishCode;
    @Getter
    @Setter
    private List<Integer> selectedColseCode;
    /** 需求完成碼 */
    @Getter
    @Setter
    private List<String> reqFinishCode = Lists.newArrayList();
    /** 選擇的閱讀類型 */
    @Getter
    @Setter
    private ReadRecordType selectReadRecordType;
    /** 選擇的待閱原因 */
    @Getter
    @Setter
    private List<String> selectReqToBeReadType;

    @PostConstruct
    private void init() {
        this.clear();
    }

    /**
     * 清除/還原選項
     */
    public void clear() {
        this.initCommHeader();
        this.initHeader();
    }

    /**
     * 初始化commHeader
     */
    private void initCommHeader() {
        commHeaderMBean.clear();
        commHeaderMBean.clearAdvance();
        commHeaderMBean.initDefaultRequireDepts(true, false);
        commHeaderMBean.setStartDate(null);
        commHeaderMBean.setEndDate(new Date());
    }

    /**
     * 初始化Header
     */
    private void initHeader() {
        reqFinishCode = Lists.newArrayList();
        this.initFinishCode();
        selectedColseCode = Lists.newArrayList(0);
        selectReadRecordType = null;
        this.initToBeReadTypes();
    }

    private void initFinishCode() {
        reqFinishCode = Lists.newArrayList();
        reqFinishCode.add("未完成");
        reqFinishCode.add("已完成");
        reqFinishCode.add("終止");
    }

    private void initToBeReadTypes() {
        selectReqToBeReadType = Lists.newArrayList();
        for (ReqToBeReadType each : ReqToBeReadType.values()) {
            selectReqToBeReadType.add(each.name());
        }
    }

    public SelectItem[] getFinishCodeItems() {
        SelectItem[] items = new SelectItem[3];
        int idx = 0;
        items[idx++] = new SelectItem("未完成", "未完成");
        items[idx++] = new SelectItem("終止", "終止");
        items[idx++] = new SelectItem("已完成", "已完成");
        return items;
    }

    

    public List<String> getToBeReadTypeQuery() {
        if (selectReqToBeReadType == null || selectReqToBeReadType.isEmpty()) {
            return Lists.newArrayList();
        }
        List<String> queryResult = selectReqToBeReadType.stream()
                .collect(Collectors.toList());
        if (queryResult.contains(ReqToBeReadType.NO_TO_BE_READ.name())) {
            queryResult.set(queryResult.indexOf(ReqToBeReadType.NO_TO_BE_READ.name()), "");
        }
        return queryResult;
    }

    public List<String> findFilterAllFinishCodes() {
        List<String> filterResult = Lists.newArrayList();
        for (RequireFinishCodeType each : RequireFinishCodeType.values()) {
            if (Lists.newArrayList("未完成", "已完成", "終止").contains(each.getCodeName())) {
                filterResult.add(each.getCode());
            }
        }
        return filterResult;
    }
}
