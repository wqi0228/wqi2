package com.cy.tech.request.web.controller.test;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.enums.OrgLevel;
import com.cy.security.utils.SecurityFacade;
import com.cy.system.rest.client.vo.OrgTransMappingTo;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgTransMappingUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@NoArgsConstructor
@Controller
@Scope("view")
public class TestSpcMBean implements Serializable {

    // @Autowired
    // private transient TcMappingClient mappingClient;

    /**
     * 
     */
    private static final long serialVersionUID = 4896679435228865283L;

    @Getter
    private String orgTransHistory;

    /**
     * 
     */
    @PostConstruct
    public void init() {
        log.info("TestSpcMBean @PostConstruct");
        this.showOrgTransHistory();
    }

    public void testTcMappingClient() {
        // try {
        // mappingClient.updateLinkGroup("38137f85-9d49-4551-8ef5-4dc1fa638eb2", Sets.newHashSet("TGTECH20150313003"));
        // } catch (TransReqClientClientException e) {
        // log.error(e.getMessage(),e);
        // }
    }

    public void showOrgTransHistory() {

        List<Date> allDates = WkOrgTransMappingUtils.getInstance().findOrgTransMappingEffectiveDates(SecurityFacade.getCompanyId());

        List<OrgTransMappingTo> allMappings = Lists.newArrayList();

        for (Date effectiveDate : allDates) {
            List<OrgTransMappingTo> orgTransMappingTo = WkOrgTransMappingUtils.getInstance().findOrgTrnsMapping(
                    SecurityFacade.getCompanyId(), effectiveDate);

            if (WkStringUtils.notEmpty(orgTransMappingTo)) {
                allMappings.addAll(orgTransMappingTo);
            }
        }

        // 生效日排序
        allMappings = allMappings.stream()
                .sorted(Comparator.comparing(OrgTransMappingTo::getEffectiveDate))
                .collect(Collectors.toList());

        Map<Integer, Integer> orderSeqMap = WkOrgCache.getInstance().findAllOrgOrderSeqMap();

        Map<String, String> beforeDepSidMapByAfterDepSid = Maps.newHashMap();
        Map<String, String> afterDepSidMapByBeforeDepSid = Maps.newHashMap();
        for (OrgTransMappingTo orgTransMappingTo : allMappings) {
            if (beforeDepSidMapByAfterDepSid.containsKey(orgTransMappingTo.getBeforeOrgSid() + "")) {
                String currAfterDepSid = beforeDepSidMapByAfterDepSid.get(orgTransMappingTo.getBeforeOrgSid() + "");
                if (WkCommonUtils.compareByStr(orgTransMappingTo.getAfterOrgSid() + "", currAfterDepSid)) {
                    String message = "設定迴圈! sid:[%s] 轉換前單位:[%s],轉換後單位:[%s]";
                    message = String.format(message,
                            orgTransMappingTo.getSid() + "",
                            orgTransMappingTo.getBeforeOrgSid() + "-" + WkOrgUtils.findNameBySid(orgTransMappingTo.getBeforeOrgSid()),
                            orgTransMappingTo.getAfterOrgSid() + "-" + WkOrgUtils.findNameBySid(orgTransMappingTo.getAfterOrgSid()));
                    continue;
                }
            }

            beforeDepSidMapByAfterDepSid.put(orgTransMappingTo.getAfterOrgSid() + "", orgTransMappingTo.getBeforeOrgSid() + "");
            afterDepSidMapByBeforeDepSid.put(orgTransMappingTo.getBeforeOrgSid() + "", orgTransMappingTo.getAfterOrgSid() + "");
        }

        // System.out.println(WkJsonUtils.getInstance().toPettyJson(beforeDepSidMapByAfterDepSid));

        Map<String, List<String>> aaaMap = Maps.newHashMap();

        for (String afterDepSid : beforeDepSidMapByAfterDepSid.keySet()) {
            List<String> currDpeSids = Lists.newArrayList();

            String point = afterDepSid;

            // 先降到最底
            while (afterDepSidMapByBeforeDepSid.containsKey(point)) {
                point = afterDepSidMapByBeforeDepSid.get(point);
            }

            // 升到最高
            String topDpeSid = point;
            while (WkStringUtils.notEmpty(topDpeSid)) {
                currDpeSids.add(0, topDpeSid);
                topDpeSid = beforeDepSidMapByAfterDepSid.get(topDpeSid);
            }

            String finalPonit = currDpeSids.get(currDpeSids.size() - 1);

            aaaMap.put(finalPonit, currDpeSids);
        }

        String prefix = "<div style='display: flex; flex-direction:row; justify-content:flex-start; align-items:center; align-self:stretch;'>";

        this.orgTransHistory = aaaMap.values().stream()
                .sorted(Comparator.comparing(list -> orderSeqMap.get(Integer.parseInt(list.get(0)))))
                .map(list -> {
                    return list.stream()
                            .map(depSid -> Integer.parseInt(depSid))
                            .map(depSid -> this.prepareName(depSid))
                            .collect(Collectors.joining("&nbsp;&nbsp;->&nbsp;&nbsp;"));
                })
                .collect(Collectors.joining("</div>" + prefix, prefix, "</div>"));

        this.findTransDepMapping(SecurityFacade.getCompanyId());
    }
    
    
    private void findTransDepMapping(String companyId) {

        List<Date> allDates = WkOrgTransMappingUtils.getInstance().findOrgTransMappingEffectiveDates(companyId);

        List<OrgTransMappingTo> allMappings = Lists.newArrayList();

        for (Date effectiveDate : allDates) {
            List<OrgTransMappingTo> orgTransMappingTo = WkOrgTransMappingUtils.getInstance().findOrgTrnsMapping(
                    companyId, effectiveDate);
            if (WkStringUtils.notEmpty(orgTransMappingTo)) {
                allMappings.addAll(orgTransMappingTo);
            }
        }

        // 生效日排序
        allMappings = allMappings.stream()
                .sorted(Comparator.comparing(OrgTransMappingTo::getEffectiveDate))
                .collect(Collectors.toList());

        Map<Integer, Integer> beforeDepSidMapByAfterDepSid = Maps.newHashMap();
        Map<Integer, Integer> afterDepSidMapByBeforeDepSid = Maps.newHashMap();
        for (OrgTransMappingTo orgTransMappingTo : allMappings) {
            if (beforeDepSidMapByAfterDepSid.containsKey(orgTransMappingTo.getBeforeOrgSid())) {
                Integer currAfterDepSid = beforeDepSidMapByAfterDepSid.get(orgTransMappingTo.getBeforeOrgSid());
                if (WkCommonUtils.compareByStr(orgTransMappingTo.getAfterOrgSid(), currAfterDepSid)) {
                    String message = "設定迴圈! sid:[%s] 轉換前單位:[%s],轉換後單位:[%s]";
                    message = String.format(message,
                            orgTransMappingTo.getSid() + "",
                            orgTransMappingTo.getBeforeOrgSid() + "-" + WkOrgUtils.findNameBySid(orgTransMappingTo.getBeforeOrgSid()),
                            orgTransMappingTo.getAfterOrgSid() + "-" + WkOrgUtils.findNameBySid(orgTransMappingTo.getAfterOrgSid()));
                    continue;
                }
            }

            beforeDepSidMapByAfterDepSid.put(orgTransMappingTo.getAfterOrgSid(), orgTransMappingTo.getBeforeOrgSid());
            afterDepSidMapByBeforeDepSid.put(orgTransMappingTo.getBeforeOrgSid(), orgTransMappingTo.getAfterOrgSid());

        }

        Map<Integer, Integer> finalDepSidMapByAfterDepSid = Maps.newHashMap();

        for (Integer afterDepSid : beforeDepSidMapByAfterDepSid.keySet()) {
            Integer point = Integer.parseInt(afterDepSid + "");

            // 找到最新
            while (afterDepSidMapByBeforeDepSid.containsKey(point)) {
                point = afterDepSidMapByBeforeDepSid.get(point);
            }
            
            if(finalDepSidMapByAfterDepSid.containsKey(afterDepSid)) {
                System.out.println();
            }

            finalDepSidMapByAfterDepSid.put(afterDepSid, point);
        }
    }
    
    

    private String prepareName(Integer depSid) {
        return "(" + depSid + ")"
                + WkOrgUtils.prepareBreadcrumbsByDepNameAndMakeupAndDecorationStyle(depSid, OrgLevel.THE_PANEL, false, "－");
    }

}
