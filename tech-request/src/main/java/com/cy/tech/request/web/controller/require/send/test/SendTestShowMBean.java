/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require.send.test;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.send.test.SendTestService;
import com.cy.tech.request.logic.service.send.test.SendTestShowService;
import com.cy.tech.request.logic.vo.WorkTestInfoTo;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.vo.worktest.WorkTestInfoHistory;
import com.cy.tech.request.web.common.PermissionHelper;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 功能按鍵顯示處理
 *
 * @author shaun
 */
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class SendTestShowMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1990058590694857863L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private SendTestShowService stShowService;
    @Autowired
    transient private SendTestService sendTestService;
    @Autowired
    private transient WorkBackendParamHelper workBackendParamHelper;

    @Getter
    @Setter
    private Boolean isQAManager;

    // 儲存各按鈕是否顯示 cache
    private Map<String, Boolean> modifyStatusByEstablishDateCache = Maps.newHashMap();
    private Map<String, Boolean> modifyStatusByTestDateCache = Maps.newHashMap();
    private Map<String, Boolean> modifyStatusByOnlineDateCache = Maps.newHashMap();
    private Map<String, Boolean> modifyStatusByFinishDateCache = Maps.newHashMap();
    private Map<String, Boolean> isContainsQADeptsCache = Maps.newHashMap();

    @PostConstruct
    public void init() {
        // log.debug("SendTestShowMBean request scope construct..");
    }

    /**
     * 組合送測單位抬頭訊息
     *
     * @param testinfo
     * @return
     */
    public String findSendTestDepTitle(String testNo) {

        WorkTestInfoTo testinfo = this.sendTestService.findToByTestinfoNo(testNo);
        if (testinfo == null || testinfo.getSendTestDep() == null || WkStringUtils.isEmpty(testinfo)) {
            return "";
        }

        return WkOrgUtils.findNameBySidStrs(testinfo.getSendTestDep().getValue(), "、");
    }

    /**
     * 組合送測單位抬頭訊息 for overlay
     *
     * @param onpg
     * @return
     */
    public List<String> findSendTestDepTitleByOp(String testNo) {
        return stShowService.findSendTestDepTitleByOp(testNo);
    }

    /**
     * 關閉測試回覆
     *
     * @param req
     * @param testInfo
     * @return
     */
    public boolean disableTestReply(Require req, WorkTestInfo testInfo) {
        return stShowService.disableTestReply(req, testInfo, loginBean.getUser());
    }

    /**
     * 顯示退測按鈕
     */
    public boolean renderedRollbackTest(Require req, WorkTestInfo testInfo) {
        return stShowService.renderedRollbackTest(req, testInfo, loginBean.getUser());
    }

    /**
     * 顯示重測按鈕
     */
    public boolean renderedRetest(Require req, WorkTestInfo testInfo) {
        return stShowService.renderedRetest(req, testInfo, loginBean.getUser());
    }

    /**
     * 顯示測試報告
     *
     * @param req
     * @param testInfo
     * @return
     */
    public boolean renderedTestReportBtn(Require req, WorkTestInfo testInfo) {
        return stShowService.renderedTestReportBtn(req, testInfo, loginBean.getUser());
    }

    /**
     * 顯示取消測試
     *
     * @param req
     * @param testInfo
     * @return
     */
    public boolean renderedCancelTest(WorkTestInfo testInfo) {
        return stShowService.renderedCancelTest(testInfo, loginBean.getUser());
    }

    /**
     * 顯示測試完成
     *
     * @param req
     * @param testInfo
     * @return
     */
    public boolean renderedTestComplate(Require req, WorkTestInfo testInfo) {
        return stShowService.renderedTestComplate(req, testInfo, loginBean.getUser());
    }

    /**
     * 關閉送測歷程查詢
     *
     * @param req
     * @param testInfo
     * @return
     */
    public boolean disableTestHistory(Require req, WorkTestInfo testInfo) {
        return stShowService.disableTestHistory(req, testInfo);
    }

    /**
     * 顯示簽核資訊查詢(渲染)
     *
     * @param req
     * @param testInfo
     * @return
     */
    public boolean showSignInfo(Require req, WorkTestInfo testInfo) {
        return stShowService.showSignInfo(req, testInfo);
    }

    public boolean showQAReportEdit() {
        return workBackendParamHelper.isQAUser(SecurityFacade.getUserSid());
    }

    /**
     * disable BPM button when use form sign web
     * 
     * @return
     */
    public boolean disableBpmBtn() {
        return Faces.getRequestParameter("mode") != null;
    }

    /**
     * 是否顯示納入排程按鈕
     */
    public boolean renderedJoinScheduleBtn(WorkTestInfo testInfo) {
        return stShowService.renderedJoinScheduleBtn(testInfo, isQAManager());
    }

    /**
     * 主題、內容、備註 可修改時機
     */
    public boolean hasModifyPermission(WorkTestInfo testInfo) {
        return stShowService.hasModifyPermission(testInfo, loginBean.getDep());
    }

    /**
     * 是否顯示不納入排程按鈕
     */
    public boolean renderedUnjoinScheduleBtn(WorkTestInfo testInfo) {
        return stShowService.renderedUnjoinScheduleBtn(testInfo, isQAManager());
    }

    /**
     * 是否顯示測試人員按鈕
     * 
     * @return
     */
    public boolean renderedTestersBtn(WorkTestInfo testInfo) {
        return stShowService.renderedTestersBtn(testInfo, isQAManager());
    }

    /**
     * @param testInfo
     * @return
     */
    public boolean renderedSendTestUpdateTesterBtn(WorkTestInfo testInfo) {
        // 以是否為QA部門成員判斷
        return stShowService.renderedTestersBtn(testInfo, isQAManager());
    }

    /**
     * 是否顯示測試人員超連結, search34.xhtml
     * 
     * @return
     */
    public boolean renderedTestersLink(String testinfoNo) {
        WorkTestInfo testInfo = sendTestService.findByTestinfoNo(testinfoNo);
        return stShowService.renderedTestersBtn(testInfo, isQAManager());
    }

    /**
     * 是否顯示文件補齊按鈕
     * 
     * @return
     */
    public boolean renderedCommitPaper(WorkTestInfo testInfo) {
        return stShowService.renderedCommitPaper(testInfo, loginBean.getUser());
    }

    /**
     * 是否顯示FBNO 編輯
     */
    public boolean renderedFbNo(WorkTestInfo testInfo) {
        return stShowService.isCreatedAndAboveManager(testInfo.getCreatedUser().getSid(), loginBean.getUser());
    }

    /**
     * 是否顯示QA連結 編輯
     */
    public boolean renderedQALink(WorkTestInfo testInfo) {
        return stShowService.renderedQALink(testInfo, loginBean.getUser());
    }

    /**
     * 預計完成日 可修改時機
     */
    public boolean modifyStatusByEstablishDate(WorkTestInfo testInfo) {
        if (modifyStatusByEstablishDateCache.get(testInfo.getTestinfoNo()) == null) {
            modifyStatusByEstablishDateCache.put(testInfo.getTestinfoNo(), stShowService.modifyStatusByEstablishDate(testInfo, loginBean.getUser()));
        }
        return modifyStatusByEstablishDateCache.get(testInfo.getTestinfoNo());
    }

    /**
     * 送測日 可修改時機
     */
    public boolean modifyStatusByTestDate(WorkTestInfo testInfo) {
        if (modifyStatusByTestDateCache.get(testInfo.getTestinfoNo()) == null) {
            modifyStatusByTestDateCache.put(
                    testInfo.getTestinfoNo(),
                    stShowService.modifyStatusByTestDate(testInfo, loginBean.getUser()));
        }
        return modifyStatusByTestDateCache.get(testInfo.getTestinfoNo());

    }

    /**
     * 預計上線日 可修改時機
     */
    public boolean modifyStatusByOnlineDate(WorkTestInfo testInfo) {
        if (modifyStatusByOnlineDateCache.get(testInfo.getTestinfoNo()) == null) {
            modifyStatusByOnlineDateCache.put(testInfo.getTestinfoNo(), stShowService.modifyStatusByOnlineDate(testInfo, loginBean.getUser()));
        }
        return modifyStatusByOnlineDateCache.get(testInfo.getTestinfoNo());
    }

    /**
     * 排定完成日 可修改時機
     */
    public boolean modifyStatusByFinishDate(WorkTestInfo testInfo) {
        if (modifyStatusByFinishDateCache.get(testInfo.getTestinfoNo()) == null) {
            modifyStatusByFinishDateCache.put(testInfo.getTestinfoNo(), stShowService.modifyStatusByFinishDate(testInfo, loginBean.getUser()));
        }
        return modifyStatusByFinishDateCache.get(testInfo.getTestinfoNo());
    }

    /**
     * 判斷是否為QA人員(主管級
     * 
     * @return
     */
    public boolean isQAManager() {
        if (isQAManager == null) {
            isQAManager = PermissionHelper.getInstance().isQAManager();
        }
        return isQAManager;
    }

    /**
     * 是否包含QA單位
     */
    public boolean isContainsQADepts(WorkTestInfo testInfo) {
        if (isContainsQADeptsCache.get(testInfo.getTestinfoNo()) == null) {
            isContainsQADeptsCache.put(testInfo.getTestinfoNo(), sendTestService.isContainsQADepts(testInfo));
        }
        return isContainsQADeptsCache.get(testInfo.getTestinfoNo());
    }

    public boolean isEditor(WorkTestInfoHistory history) {
        return history.getCreatedUser().equals(loginBean.getUser());
    }
}
