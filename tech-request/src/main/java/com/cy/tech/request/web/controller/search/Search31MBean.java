/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.view.Search31View;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.othset.OthSetService;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.search.helper.Search31Helper;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.values.UsrBean;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.enums.ReadRecordType;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author saul_chen
 */
@Controller
@Slf4j
@Scope("view")
public class Search31MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8681959908034359745L;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private Search31Helper searchHelper;
    @Autowired
    transient private Require01MBean r01MBean;
    @Autowired
    transient private OthSetService osService;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private UsrBean usrBean;
    /** 切換模式 */
    @Getter
    private SwitchType switchType = SwitchType.CONTENT;
    /** 切換模式 - 全畫面狀態 */
    @Getter
    private SwitchType switchFullType = SwitchType.DETAIL;
    @Getter
    @Setter
    private List<Search31View> queryItems;
    /** 選擇的需求單 */
    @Getter
    @Setter
    private Search31View querySelection;
    /** 上下筆移動keeper */
    @Getter
    private Search31View queryKeeper;

    /** 在匯出的時候，某些內容需要隱藏 */
    @Getter
    private boolean hasDisplay = true;
    @Getter
    private final String dataTableId = "dtRequire";

    @PostConstruct
    public void init() {
        search();
    }

    public void search() {
        queryItems = searchHelper.search(loginBean.getDep(), loginBean.getUser());
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search31View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            display.update("headerTitle");
            display.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Search31View to) {
        querySelection = to;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
        upDownBean.resetTabInfo(RequireBottomTabType.OTH_SET, queryKeeper.getSid());
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            this.moveScreenTab();
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Search31View view) {
        view.setReadRecordType(ReadRecordType.HAS_READ);
        Require require = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(require, loginBean.getUser(), RequireBottomTabType.OTH_SET);
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Search31View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
        this.moveScreenTab();
    }

    private void moveScreenTab() {
        this.r01MBean.getBottomTabMBean().resetTabIdx(this.r01MBean);
        this.r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.OTH_SET);

        List<String> sids = osService.findSidsByRequire(this.r01MBean.getRequire());
        String indicateSid = queryKeeper.getSid();
        if (sids.indexOf(indicateSid) != -1) {
            display.execute("PF('os_acc_panel_layer_zero').select(" + sids.indexOf(indicateSid) + ");");
            for (int i = 0; i < sids.size(); i++) {
                if (i != sids.indexOf(indicateSid)) {
                    display.execute("PF('os_acc_panel_layer_zero').unselect(" + i + ");");
                }
            }
        }
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(querySelection);
        this.moveScreenTab();
    }

    /**
     * 隱藏部分column裡的內容
     */
    public void hideColumnContent() {
        hasDisplay = false;
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        helper.exportExcel(document, usrBean.name(loginBean.getUser()), ReportType.TODO_OTHER_SETUP_SETTING.getReportName());
        hasDisplay = true;
    }

}
