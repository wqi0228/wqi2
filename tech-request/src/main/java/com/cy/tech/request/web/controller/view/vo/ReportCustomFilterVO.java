/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.vo;

import java.util.List;
import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */
public class ReportCustomFilterVO {

    public ReportCustomFilterVO(boolean defaultSetting, int index, List<ReportCustomFilterDetailVO> reportCustomFilterDetailVOs) {
        this.defaultSetting = defaultSetting;
        this.index = index;
        this.reportCustomFilterDetailVOs = reportCustomFilterDetailVOs;
    }

    @Getter
    private boolean defaultSetting;
    @Getter
    private int index;
    @Getter
    private List<ReportCustomFilterDetailVO> reportCustomFilterDetailVOs;

}
