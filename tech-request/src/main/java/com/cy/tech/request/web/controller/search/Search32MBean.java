/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.omnifaces.util.Faces;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search32QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.view.Search32View;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.send.test.SendTestService;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.worktest.enums.WorkTestInfoStatus;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.searchheader.CommonHeaderMBean;
import com.cy.tech.request.web.controller.searchheader.Search14HeaderMBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.values.OrgBean;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.enums.ReadRecordType;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 送測簽核進度查詢
 *
 * @author marlow_chen
 */
@Controller
@Scope("view")
@Slf4j
public class Search32MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2162097665708121336L;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private Search14HeaderMBean headerMBean;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private OrganizationService orgService;
    @Autowired
    transient private Search32QueryService queryService;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private SendTestService sendTestService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private Require01MBean r01MBean;
    @Autowired
    transient private OrgBean orgBean;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;

    /**
     * 所有的送測單
     */
    @Getter
    @Setter
    private List<Search32View> queryItems;

    /**
     * 選擇的送測單
     */
    @Getter
    @Setter
    private Search32View querySelection;

    /**
     * 上下筆移動keeper
     */
    @Getter
    private Search32View queryKeeper;

    /**
     * 切換模式
     */
    @Getter
    private SwitchType switchType = SwitchType.CONTENT;

    /**
     * 切換模式 - 全畫面狀態
     */
    @Getter
    private SwitchType switchFullType = SwitchType.DETAIL;

    @Getter
    private final String dataTableId = "dtRequire";

    /**
     * 所有填單單位
     */
    @Getter
    private List<SelectItem> createDeps;

    /**
     * 提交狀態
     */
    @Getter
    @Setter
    private String commitStatus;

    /**
     * 登入者所包含的所有組織
     */
    private List<Org> allOrgs;

    private List<SelectItem> commitStatusItem;
    @Getter
    private boolean portalMode = false;
    @Getter
    private boolean bySelf = false;
    @Getter
    @Setter
    /** 是否顯示搜尋元件 */
    private boolean displaySearchComp = true;

    @PostConstruct
    public void init() {
        portalMode = Faces.getRequestParameter("mode") != null;
        bySelf = Faces.getRequestParameter("bySelf") != null;
        if (portalMode) {
            displaySearchComp = portalMode && bySelf;
        }
        this.findUserOrgs();
        commitStatus = "UNCOMMIT";
        //commHeaderMBean.getDateInterval("TODAY");

        display.execute("doSearchData();");
    }

    public void search() {
        queryItems = this.findWithQuery();
        this.initCreateDeps(queryItems);

        display.resetDataTable(dataTableId);
    }

    /**
     * 取得登入者所包含的所有組織
     */
    public void findUserOrgs() {
        int orgSid = SecurityFacade.getPrimaryOrgSid();
        Org userOrg = WkOrgCache.getInstance().findBySid(orgSid);
        allOrgs = orgService.findChildOrgsByBasicOrg(userOrg);
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        headerMBean.clear();
        portalMode = Faces.getRequestParameter("mode") != null;
        bySelf = Faces.getRequestParameter("bySelf") != null;
        if (portalMode) {
            displaySearchComp = portalMode && bySelf;
        }
        this.findUserOrgs();
        commitStatus = "UNCOMMIT";
        commHeaderMBean.getDateInterval("TODAY");
        this.search();
    }

    private List<Search32View> findWithQuery() {
        String requireNo = reqularUtils.getRequireNo(loginBean.getCompanyId(), commHeaderMBean.getFuzzyText());
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT "
                + "wti.testinfo_sid,"
                + "wti.test_date,"
                + "wti.dep_sid,"
                + "wti.create_usr,"
                + "wti.testinfo_theme,"
                + "wti.commit_status,"
                + "wti.testinfo_estimate_dt,"
                + "wti.create_dt,"
                + "wti.testinfo_source_no, "
                + this.searchConditionSqlHelper.prepareReadRecordSelectColumn()
                + " FROM ");
        buildWorkTestInfoCondition(requireNo, builder, parameters);

        builder.append(this.searchConditionSqlHelper.prepareSubFormReadRecordJoin(
                SecurityFacade.getUserSid(),
                FormType.WORKTESTSIGNINFO,
                "wti"));

        builder.append("WHERE wti.testinfo_sid IS NOT NULL ");
        builder.append("  ORDER BY wti.test_date ASC, wti.create_dt ASC");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.WORK_TEST_SEND, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.WORK_TEST_SEND, SecurityFacade.getUserSid());

        // 查詢
        List<Search32View> results = queryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return results;
    }

    private void buildWorkTestInfoCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("(SELECT * FROM work_test_info wti WHERE 1=1");

        // 送測區間
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getStartDate() != null && commHeaderMBean.getEndDate() != null) {
            builder.append(" AND wti.test_date BETWEEN :startDate AND :endDate");
            parameters.put("startDate", helper.transStartDate(commHeaderMBean.getStartDate()));
            parameters.put("endDate", helper.transEndDate(commHeaderMBean.getEndDate()));
        }
        // 技術需求單類型
        builder.append(" AND wti.testinfo_source_type='TECH_REQUEST'");

        // 提交狀態
        if (Strings.isNullOrEmpty(requireNo) && commitStatus != null) {
            if (commitStatus.equals("")) {
                builder.append(" AND ((wti.commit_status = 'UNCOMMIT' AND (wti.testinfo_status='SONGCE' or wti.testinfo_status='RETEST')) OR wti.commit_status = 'COMMITED')");
            } else if (commitStatus.equals("COMMITED")) {
                builder.append(" AND (wti.commit_status = 'COMMITED')");
            } else {
                builder.append(" AND (wti.commit_status = 'UNCOMMIT' AND (wti.testinfo_status='SONGCE' or wti.testinfo_status='RETEST'))");
            }
        }

        if (portalMode) {
            if (bySelf) {
                builder.append(" AND wti.create_usr = :userSid");
                parameters.put("userSid", SecurityFacade.getUserSid());
            }
        } else {
            String allOrgStr = "";

            for (int i = 0; i < allOrgs.size(); i++) {
                allOrgStr = allOrgStr + allOrgs.get(i).getSid();
                if (i != allOrgs.size() - 1) {
                    allOrgStr = allOrgStr + ",";
                }
            }
            builder.append(" AND wti.dep_sid IN (" + allOrgStr + " )");
        }

        builder.append(") AS wti ");
    }

    /**
     * 初始化填單單位
     */
    private void initCreateDeps(List<Search32View> queryItems) {

        Set<Object> allCreateDeps = new HashSet<Object>();

        queryItems.forEach(each -> {
            allCreateDeps.add(each.getCreateDep());
        });

        List<Object> setToList = allCreateDeps.stream().collect(Collectors.toList());
        this.createDeps = new ArrayList<>(setToList.size());
        setToList.forEach(each -> {
            SelectItem sel = new SelectItem();
            String depName = orgBean.name(each);
            sel.setLabel(depName);
            sel.setValue(depName);
            this.createDeps.add(sel);
        });
    }

    /**
     * 取得提交狀態
     *
     * @return
     */
    public List<SelectItem> getCommitStatusItem() {

        commitStatusItem = new ArrayList<>();
        SelectItem notCommit = new SelectItem();
        notCommit.setLabel("未提交");
        notCommit.setValue("UNCOMMIT");
        commitStatusItem.add(notCommit);
        SelectItem commit = new SelectItem();
        commit.setLabel("已提交");
        commit.setValue("COMMITED");
        commitStatusItem.add(commit);

        return commitStatusItem;
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            this.moveScreenTab();
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (Search32View) event.getObject();
        this.changeRequireContent(this.queryKeeper);
        this.moveScreenTab();
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Search32View view) {
        view.setReadRecordType(ReadRecordType.HAS_READ);
        Require require = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(require, loginBean.getUser(), RequireBottomTabType.SEND_TEST_INFO);
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Search32View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
        this.moveScreenTab();
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
        this.moveScreenTab();
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        helper.exportExcel(document, commHeaderMBean.getStartDate(), commHeaderMBean.getEndDate(), commHeaderMBean.getReportType());
    }

    public void moveScreenTab() {
        this.r01MBean.getBottomTabMBean().resetTabIdx(this.r01MBean);
        this.r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.SEND_TEST_INFO);

        List<String> sids = sendTestService.findSidsBySourceSid(this.r01MBean.getRequire().getSid());
        String indicateSid = queryKeeper.getSid();
        if (sids.indexOf(indicateSid) != -1) {
            display.execute("PF('st_acc_panel_layer_zero').select(" + sids.indexOf(indicateSid) + ");");
            for (int i = 0; i < sids.size(); i++) {
                if (i != sids.indexOf(indicateSid)) {
                    display.execute("PF('st_acc_panel_layer_zero').unselect(" + i + ");");
                }
            }
        }
    }

    /**
     * 取得提交狀態字串
     *
     * @param view
     * @return
     */
    public String getCommitStatusStr(Search32View view) {
        if (view == null || view.getCommitStatus() == null) {
            if (view == null) {
                log.error("view is null from search32");
            } else if (view.getCommitStatus() == null) {
                log.error("view read record group is null from search32");
            }
            return "";
        }
        return WorkTestInfoStatus.valueOf(view.getCommitStatus()).getValue();
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search32View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            display.update("headerTitle");
            display.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Search32View to) {
        querySelection = to;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
        upDownBean.resetTabInfo(RequireBottomTabType.SEND_TEST_INFO, queryKeeper.getSid());
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }
}
