/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.enums.Search18SubType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search18QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.Search18View;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.pt.PtService;
import com.cy.tech.request.logic.service.send.test.SendTestService;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.searchheader.CommonHeaderMBean;
import com.cy.tech.request.web.controller.searchheader.Search18HeaderMBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.backend.logic.WorkCalendarHelper;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 測試提醒清單
 *
 * @author jason_h
 */
@Slf4j
@Controller
@Scope("view")
public class Search18MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5536139134593348843L;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private Search18HeaderMBean headerMBean;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private PtService ptService;
    @Autowired
    transient private SendTestService stService;
    @Autowired
    transient private SearchService searchHelper;
    @Autowired
    transient private WorkCalendarHelper wcHelper;
    @Autowired
    transient private Require01MBean r01MBean;
    @Autowired
    transient private Search18QueryService search18QueryService;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchResultHelper searchResultHelper;

    /** 所有的測試提醒清單 原型確認 | 送測 */
    @Getter
    @Setter
    private List<Search18View> queryItems;
    /** 選擇的 原型確認 | 送測 單 */
    @Getter
    @Setter
    private Search18View querySelection;
    /** 上下筆移動keeper */
    @Getter
    private Search18View queryKeeper;
    /** 切換模式 */
    @Getter
    private SwitchType switchType = SwitchType.CONTENT;
    /** 切換模式 - 全畫面狀態 */
    @Getter
    private SwitchType switchFullType = SwitchType.DETAIL;
    @Getter
    private final String dataTableId = "dtRequire";

    /** 在匯出的時候，某些內容需要隱藏 */
    @Getter
    private boolean hasDisplay = true;

    private Search18SubType fullScreenFlagType;

    // 不清楚用途
    @SuppressWarnings("unused")
    private boolean isSignProcess;

    @PostConstruct
    public void init() {
        this.search();
    }

    public void search() {
        queryItems = this.findWithQuery();
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        headerMBean.clear();
        this.search();
    }

    private List<Search18View> findWithQuery() {
        String requireNo = reqularUtils.getRequireNo(loginBean.getCompanyId(), commHeaderMBean.getFuzzyText());
        Map<String, Object> ptParam = Maps.newHashMap();
        StringBuilder ptBuilder = new StringBuilder();
        Map<String, Object> stParam = Maps.newHashMap();
        StringBuilder stBuilder = new StringBuilder();
        switch (headerMBean.getConfirmFilterType()) {
        case PT:
            this.findWithQueryDetail(Search18SubType.PT, requireNo, ptBuilder, ptParam);
            break;
        case ST:
            this.findWithQueryDetail(Search18SubType.ST, requireNo, stBuilder, stParam);
            break;
        default:
            this.findWithQueryDetail(Search18SubType.PT, requireNo, ptBuilder, ptParam);
            this.findWithQueryDetail(Search18SubType.ST, requireNo, stBuilder, stParam);
            break;
        }

        // show SQL in debug log
        if (ptBuilder.length() > 0) {
            SearchCommonHelper.getInstance().showSQLDebugLog(
                    ReportType.TEST_NOTIFY_LIST, ptBuilder.toString(), ptParam);
        }
        if (stBuilder.length() > 0) {
            SearchCommonHelper.getInstance().showSQLDebugLog(
                    ReportType.TEST_NOTIFY_LIST, stBuilder.toString(), stParam);
        }

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.TEST_NOTIFY_LIST, SecurityFacade.getUserSid());

        // 查詢
        List<Search18View> resultList = search18QueryService.findWithQuery(
                headerMBean.getConfirmFilterType(),
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(ptBuilder.toString()), // 格式化 SQL
                ptParam,
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(stBuilder.toString()), // 格式化 SQL
                stParam,
                SecurityFacade.getUserSid(),
                usageRecord);

        // ====================================
        // 後續處理
        // ====================================
        if (WkStringUtils.notEmpty(resultList)) {
            // 後續處理-開始
            usageRecord.afterProcessStart();
            // 查詢條件過濾：系統別
            resultList = resultList.stream()
                    .filter(each -> this.searchResultHelper.filterCheckItems(
                            each.getCheckItemTypes(),
                            commHeaderMBean.getCheckItemTypes(),
                            false,
                            null))
                    .collect(Collectors.toList());
            usageRecord.afterProcessEnd();
        }

        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return resultList;
    }

    private void findWithQueryDetail(Search18SubType filterType, String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builderPrototypePrefix(filterType, builder);
        builderTestinfoPrefix(filterType, builder);
        buildPrototypeCondition(filterType, requireNo, builder, parameters);
        buildTestinfoCondition(filterType, requireNo, builder, parameters);
        buildRequireCondition(filterType, requireNo, builder);
        buildRequireIndexDictionaryCondition(builder, parameters);
        buildCategoryKeyMappingCondition(builder);

        FormType formType = null;
        String mainTableAliases = "";
        if (Search18SubType.PT.equals(filterType)) {
            formType = FormType.PTCHECK;
            mainTableAliases = "pc";
        }
        if (Search18SubType.ST.equals(filterType)) {
            formType = FormType.WORKTESTSIGNINFO;
            mainTableAliases = "wti";
        }

        // 檢查項目 (系統別)
        // 後方需對 主單 sid 做 group by
        builder.append(this.searchConditionSqlHelper.prepareSubFormCommonJoin(
                SecurityFacade.getUserSid(),
                formType,
                mainTableAliases));

        buildSubSuffixCondition(filterType, builder);
    }

    private void builderPrototypePrefix(Search18SubType filterType, StringBuilder builder) {
        if (filterType.equals(Search18SubType.ST)) {
            return;
        }
        builder.append("SELECT "
                + "tr.require_sid,"
                + "tr.create_dt as tr_create_dt,"
                + "ckm.big_category_name,"
                + "ckm.middle_category_name,"
                + "ckm.small_category_name,"
                + "tr.dep_sid as tr_dep_sid,"
                + "tr.create_usr as tr_create_usr,"
                + "tid.field_content,"
                + "tr.urgency,"
                + "tr.require_status,"
                + "tr.require_no,"
                + "tr.hope_dt, "
                + "pc.pt_check_sid,"
                + "pc.pt_check_status,"
                + "pc.create_dt,"
                + "pc.dep_sid,"
                + "pc.create_usr,"
                + "pc.pt_check_estimate_dt,"
                + "pc.pt_check_theme, "
                // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
                + this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire()
                + " FROM ");
    }

    private void builderTestinfoPrefix(Search18SubType filterType, StringBuilder builder) {
        if (filterType.equals(Search18SubType.PT)) {
            return;
        }
        builder.append("SELECT "
                + "tr.require_sid,"
                + "tr.create_dt as tr_create_dt,"
                + "ckm.big_category_name,"
                + "ckm.middle_category_name,"
                + "ckm.small_category_name,"
                + "tr.dep_sid as tr_dep_sid,"
                + "tr.create_usr as tr_create_usr,"
                + "tid.field_content,"
                + "tr.urgency,"
                + "tr.require_status,"
                + "tr.require_no,"
                + "tr.hope_dt, "
                + "wti.testinfo_sid,"
                + "wti.testinfo_status,"
                + "wti.create_dt,"
                + "wti.dep_sid,"
                + "wti.create_usr,"
                + "wti.testinfo_estimate_dt,"
                + "wti.testinfo_theme,"
                + "wti.testinfo_deps,"
                + builderHistoryCompleteColumn() + ", "
                + this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire()
                + " FROM ");
    }

    private String builderHistoryCompleteColumn() {
        StringBuilder builder = new StringBuilder();
        builder.append("(SELECT CASE WHEN (COUNT(*) > 0) THEN 'TRUE' ELSE 'FALSE' END ");
        builder.append("  FROM work_test_info_history ");
        builder.append("    WHERE wti.testinfo_sid = testinfo_sid ");
        builder.append("      AND behavior = 'QA_TEST_COMPLETE' ");
        builder.append(") AS qa_test_complete,");
        builder.append("(SELECT CASE WHEN (COUNT(*) > 0) THEN 'TRUE' ELSE 'FALSE' END ");
        builder.append("  FROM work_test_info_history ");
        builder.append("    WHERE wti.testinfo_sid = testinfo_sid ");
        builder.append("      AND behavior = 'TEST_COMPLETE' ");
        builder.append(") AS test_complete ");
        return builder.toString();
    }

    private void buildPrototypeCondition(Search18SubType filterType, String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        if (filterType.equals(Search18SubType.ST)) {
            return;
        }
        // 原型確認
        builder.append("(SELECT * FROM work_pt_check pc ");
        builder.append("  WHERE pc.pt_check_status NOT IN ('VERIFY_INVAILD','FUNCTION_CONFORM')");
        builder.append("    AND pc.pt_check_estimate_dt < :systemfinishDate ");
        builder.append("    AND (pc.pt_notice_member REGEXP :notifyMemberRegexp ");
        builder.append("     OR pc.dep_sid IN (:createPtUnitGroup) ");
        builder.append("    )");
        // 模糊搜尋
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.isFuzzyQuery()) {
            String text = "%" + reqularUtils.replaceIllegalSqlLikeStr(commHeaderMBean.getFuzzyText()) + "%";
            builder.append(" AND (pc.pt_check_content LIKE :pcFuzzText OR pc.pt_check_theme LIKE :pcFuzzText OR pc.pt_check_note LIKE :pcFuzzText OR ");
            builder.append("      pc.pt_check_sid IN (SELECT wpr.pt_check_sid FROM work_pt_reply wpr WHERE wpr.reply_content LIKE :pcFuzzText) OR ");
            builder.append("      pc.pt_check_sid IN (SELECT wpar.pt_check_sid FROM work_pt_already_reply wpar WHERE wpar.reply_content LIKE :pcFuzzText) ");
            builder.append("     )");
            parameters.put("pcFuzzText", text);
        }
        builder.append(") AS pc ");
        // 到期日
        parameters.put("systemfinishDate", this.findSystemFinishDate(headerMBean.getRemainderExpireDate()));
        // 通知對象可看到
        parameters.put("notifyMemberRegexp", "\"" + loginBean.getUser().getSid() + "\"");
        // 填單單位成員及向上主管
        parameters.put("createPtUnitGroup", headerMBean.createUnitGroupDep(loginBean.getDep()));
    }

    private Date findSystemFinishDate(int remainderExpireDate) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, remainderExpireDate);
        this.calcHoilday(c);
        return searchHelper.transEndDate(c.getTime());
    }

    /**
     * 如果剩餘日數當日為假日則往後再加1天
     *
     * @param c
     */
    private void calcHoilday(Calendar c) {
        c.add(Calendar.DATE, 1);
        if (wcHelper.isHoilday(c.getTime())) {
            this.calcHoilday(c);
        }
    }

    private void buildTestinfoCondition(Search18SubType filterType, String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        if (filterType.equals(Search18SubType.PT)) {
            return;
        }
        // 送測
        builder.append("(SELECT * FROM work_test_info wti ");
        builder.append("  WHERE wti.testinfo_status NOT IN ('VERIFY_INVAILD','CANCEL_TEST')");
        builder.append("    AND wti.testinfo_estimate_dt < :systemfinishDate ");
        builder.append("    AND (wti.testinfo_deps REGEXP :testDepRegexp");
        builder.append("     OR  wti.dep_sid IN (:createStUnitGroup) ");
        builder.append("    )");
        // 模糊搜尋
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.isFuzzyQuery()) {
            String text = "%" + reqularUtils.replaceIllegalSqlLikeStr(commHeaderMBean.getFuzzyText()) + "%";
            builder.append(
                    " AND (wti.testinfo_content LIKE :wtiFuzzyText OR wti.testinfo_theme LIKE :wtiFuzzyText OR wti.testinfo_note LIKE :wtiFuzzyText OR ");
            builder.append(" wti.testinfo_sid IN (SELECT wtr.testinfo_sid FROM work_test_reply wtr WHERE wtr.reply_content LIKE :wtiFuzzyText) OR ");
            builder.append(" wti.testinfo_sid IN (SELECT wtar.testinfo_sid FROM work_test_already_reply wtar WHERE wtar.reply_content LIKE :wtiFuzzyText) OR ");
            builder.append(" wti.testinfo_sid IN (SELECT wtqr.testinfo_sid FROM work_test_qa_report wtqr WHERE wtqr.report_content LIKE :wtiFuzzyText) ");
            builder.append("     )");
            parameters.put("wtiFuzzyText", text);
        }
        builder.append(") AS wti ");
        // 到期日
        parameters.put("systemfinishDate", this.findSystemFinishDate(headerMBean.getRemainderExpireDate()));
        // 送測的通知單位（通知單位的成員與通知單位的主管群）
        parameters.put("testDepRegexp", headerMBean.createOrgsRegexpParam(loginBean.getDep()));
        // 填單單位成員及向上主管
        parameters.put("createStUnitGroup", headerMBean.createUnitGroupDep(loginBean.getDep()));

    }

    private void buildRequireCondition(Search18SubType filterType, String requireNo, StringBuilder builder) {
        builder.append("INNER JOIN (SELECT * FROM tr_require tr ");
        if (Strings.isNullOrEmpty(requireNo) && headerMBean.getExcludeClose() != null && headerMBean.getExcludeClose()) {
            builder.append("WHERE tr.close_code = 0 ");
        }
        if (filterType.equals(Search18SubType.ST)) {
            builder.append(") AS tr ON tr.require_sid=wti.testinfo_source_sid ");
        } else {
            builder.append(") AS tr ON tr.require_sid=pc.pt_check_source_sid ");
        }
        // 需求單號
        if (!Strings.isNullOrEmpty(requireNo)) {
            builder.append(" AND tr.require_no = '" + requireNo + "' ");
        }
    }

    private void buildRequireIndexDictionaryCondition(StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_index_dictionary tid WHERE "
                + "tid.field_name='主題') AS tid ON "
                + "tr.require_sid = tid.require_sid ");
    }

    private void buildCategoryKeyMappingCondition(StringBuilder builder) {
        builder.append("INNER JOIN (SELECT * FROM tr_category_key_mapping ckm ) AS ckm ON tr.mapping_sid=ckm.key_sid ");
    }

    private void buildSubSuffixCondition(Search18SubType filterType, StringBuilder builder) {
        if (filterType.equals(Search18SubType.ST)) {
            builder.append("WHERE wti.testinfo_sid IS NOT NULL ");
            builder.append(" GROUP BY wti.testinfo_sid ");
            builder.append("  ORDER BY wti.testinfo_estimate_dt DESC");
        } else {
            builder.append("WHERE pc.pt_check_sid IS NOT NULL ");
            builder.append(" GROUP BY pc.pt_check_sid ");
            builder.append("  ORDER BY pc.pt_check_estimate_dt DESC");
        }
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            this.moveScreenTab();
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (Search18View) event.getObject();
        this.changeRequireContent(this.queryKeeper);
        this.moveScreenTab();
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Search18View view) {
        view.setReadRecordType(ReadRecordType.HAS_READ);
        Require require = requireService.findByReqNo(view.getRequireNo());
        RequireBottomTabType btt = this.findMoveTabType(view);
        if (btt == null) {
            loadManager.reloadReqForm(require, loginBean.getUser());
        } else {
            loadManager.reloadReqForm(require, loginBean.getUser(), btt);
        }
    }

    private RequireBottomTabType findMoveTabType(Search18View view) {
        if (fullScreenFlagType == null && view != null) {
            fullScreenFlagType = view.getSubConfirmStatus();
        }
        if (fullScreenFlagType.equals(Search18SubType.PT)) {
            return RequireBottomTabType.PT_CHECK_INFO;
        }
        if (fullScreenFlagType.equals(Search18SubType.ST)) {
            return RequireBottomTabType.SEND_TEST_INFO;
        }
        return null;
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Search18View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.resetScreenType(view);
        this.toggleSearchBody();
        this.moveScreenTab();
    }

    private void resetScreenType(Search18View view) {
        fullScreenFlagType = view.getSubConfirmStatus();
        isSignProcess = view.getSubStatus().contains("簽核中");
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.resetScreenType(querySelection);
        this.changeRequireContent(this.querySelection);
        this.moveScreenTab();
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        helper.exportExcel(document, commHeaderMBean.getStartDate(), commHeaderMBean.getEndDate(), commHeaderMBean.getReportType());
        hasDisplay = true;
    }

    /**
     * 隱藏部分column裡的內容
     */
    public void hideColumnContent() {
        hasDisplay = false;
    }

    /**
     * 跟其它報表不同，指向不同TAB位置
     *
     * @param r01MBean
     */
    public void moveScreenTab() {
        if (fullScreenFlagType.equals(Search18SubType.PT)) {
            this.toPrototypeTab();
        } else {
            this.toTestInfoTab();
        }
    }

    private void toTestInfoTab() {
        this.r01MBean.getBottomTabMBean().resetTabIdx(this.r01MBean);
        this.r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.SEND_TEST_INFO);
        List<String> sids = stService.findSidsBySourceSid(this.r01MBean.getRequire().getSid());
        String indicateSid = queryKeeper.getSubSid();
        if (sids.indexOf(indicateSid) != -1) {
            display.execute("PF('st_acc_panel_layer_zero').select(" + sids.indexOf(indicateSid) + ");");
            for (int i = 0; i < sids.size(); i++) {
                if (i != sids.indexOf(indicateSid)) {
                    display.execute("PF('st_acc_panel_layer_zero').unselect(" + i + ");");
                }
            }
        }
    }

    private void toPrototypeTab() {
        this.r01MBean.getBottomTabMBean().resetTabIdx(this.r01MBean);
        this.r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.PT_CHECK_INFO);
        List<String> sids = ptService.findSidsBySourceSid(this.r01MBean.getRequire().getSid());
        String indicateSid = queryKeeper.getSubSid();
        if (sids.indexOf(indicateSid) != -1) {
            display.execute("PF('pt_acc_panel_layer_zero').select(" + sids.indexOf(indicateSid) + ");");
            for (int i = 0; i < sids.size(); i++) {
                if (i != sids.indexOf(indicateSid)) {
                    display.execute("PF('pt_acc_panel_layer_zero').unselect(" + i + ");");
                }
            }
        }
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search18View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            display.update("headerTitle");
            display.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Search18View to) {
        querySelection = to;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
        RequireBottomTabType resetType = queryKeeper.getSubConfirmStatus().equals(Search18SubType.PT)
                ? RequireBottomTabType.PT_CHECK_INFO
                : RequireBottomTabType.SEND_TEST_INFO;
        upDownBean.resetTabInfo(resetType, queryKeeper.getSubSid());
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }
}
