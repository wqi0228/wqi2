/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.view.Search02View;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.vo.enums.Search02QueryColumn;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.search.helper.Search02Helper;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.view.component.CategoryTreeComponent;
import com.cy.tech.request.web.controller.view.component.ReportOrgTreeComponent;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterArrayStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterDetailVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterVO;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.listener.CategoryTreeCallBack;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.listener.ReportCustomFilterCallback;
import com.cy.tech.request.web.listener.ReportOrgTreeCallBack;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.tech.request.web.view.to.search.query.SearchQuery02;
import com.cy.tech.request.web.view.to.search.query.SearchQuery02CustomFilter;
import com.cy.work.common.cache.WkUserWithRolesCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.logic.WkUserAndOrgLogic;
import com.cy.work.viewcomponent.treepker.TreePickerCallback;
import com.cy.work.viewcomponent.treepker.TreePickerComponent;
import com.cy.work.viewcomponent.treepker.helper.TreePickerDepHelper;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 需求單位簽核進度查詢
 *
 * @author jason_h
 */
@Controller
@Scope("view")
@Slf4j
public class Search02MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6577062502226977643L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private Search02Helper searchHelper;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private ReportCustomFilterLogicComponent reportCustomFilterLogicComponent;

    @Getter
    /** 類別樹 Component */
    private CategoryTreeComponent categoryTreeComponent;
    @Getter
    /** 報表 組織樹 Component */
    private ReportOrgTreeComponent orgTreeForwardComponent;

    @Getter
    /** 切換模式 - 全畫面狀態 */
    private SwitchType switchFullType = SwitchType.DETAIL;
    @Getter
    /** 切換模式 */
    private SwitchType switchType = SwitchType.CONTENT;
    @Getter
    /** 列表 id */
    private final String dataTableId = "dtRequire";
    @Getter
    /** 查詢物件 */
    private SearchQuery02 searchQuery;

    @Getter
    @Setter
    /** 所有的需求單 */
    private List<Search02View> queryItems;
    @Getter
    @Setter
    /** 選擇的需求單 */
    private Search02View querySelection;
    /** 上下筆移動keeper */
    @Getter
    private Search02View queryKeeper;
    @Getter
    private SearchQuery02CustomFilter searchQuery02CustomFilter;
    @Autowired
    transient private WkUserAndOrgLogic wkUserAndOrgLogic;
    @Autowired
    transient private TreePickerDepHelper treePickerDepHelper;
    @Autowired
    transient private DisplayController displayController;

    /**
     * 可閱單位
     */
    private Set<Integer> canViewDepSids = Sets.newHashSet();

    /**
     * 預設檢視單位
     */
    private Set<Integer> defaultViewDepSids = Sets.newHashSet();

    @PostConstruct
    public void init() {
        this.initViewDep();
        this.initComponents();
        this.clear();
    }

    /**
     * 取得登入者的『可閱單位』、『預設檢視單位』
     */
    private void initViewDep() {

        // ====================================
        // 可閱單位
        // ====================================
        this.canViewDepSids = wkUserAndOrgLogic.prepareCanViewDepSidsBaseOnPanel(
                SecurityFacade.getUserSid(), true);

        // ====================================
        // 預設檢視單位
        // ====================================
        this.defaultViewDepSids = wkUserAndOrgLogic.prepareCanViewDepSids(SecurityFacade.getUserSid(), false);

    }

    /**
     * 初始化 元件
     */
    private void initComponents() {

        // 查詢登入者所有角色
        List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
                SecurityFacade.getUserSid(),
                SecurityFacade.getCompanyId());

        this.searchQuery = new SearchQuery02(ReportType.SIGN);
        this.categoryTreeComponent = new CategoryTreeComponent(categoryTreeCallBack);

        this.orgTreeForwardComponent = new ReportOrgTreeComponent(
                loginBean.getCompanyId(),
                loginBean.getDep(),
                roleSids,
                false,
                reportOrgTreeForwardCallBack);

        // 初始化報表自定義物件
        this.searchQuery02CustomFilter = new SearchQuery02CustomFilter(
                SecurityFacade.getCompanyId(), SecurityFacade.getUserSid(),
                reportCustomFilterLogicComponent, messageCallBack, display, reportCustomFilterCallback,
                roleSids,
                loginBean.getDep(),
                this.canViewDepSids);
    }

    public void openDefaultSetting() {
        try {
            this.searchQuery02CustomFilter.loadDefaultSetting(
                    this.defaultViewDepSids,
                    this.searchQuery02CustomFilter.getTempSearchQuery02());

        } catch (Exception e) {
            log.error("openDefaultSetting ERROR", e);
        }
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        this.clearQuery();
        this.search();
    }

    /**
     * 清除/還原選項
     */
    private void clearQuery() {
        try {
            searchQuery02CustomFilter.loadDefaultSetting(
                    this.defaultViewDepSids, searchQuery);
            categoryTreeComponent.clearCate();
            categoryTreeCallBack.loadSelCate(searchQuery.getSmallDataCateSids());
        } catch (Exception e) {
            log.error("clearQuery ERROR", e);
        }
    }

    /**
     * 查詢
     */
    public void search() {
        queryItems = searchHelper.search(loginBean.getCompanyId(), loginBean.getUser(), searchQuery);
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search02View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Search02View to) {
        querySelection = to;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            display.update("headerTitle");
            display.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 全畫面前準備
     *
     * @param view
     */
    public void fullScreenForm(Search02View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Search02View view) {
        view.setReadRecordType(ReadRecordType.HAS_READ);
        Require r = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(r, loginBean.getUser());
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
    }

    /**
     * 開啟 類別樹
     */
    public void btnOpenCategoryTree() {
        try {
            categoryTreeComponent.init();
            display.showPfWidgetVar("dlgCate");
        } catch (Exception e) {
            log.error("btnOpenCategoryTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 開啟 轉發至 組織樹
     */
    public void btnOpenForwardOrgTree() {
        try {
            orgTreeForwardComponent.initOrgTree(loginBean.getComp(), searchQuery.getForwardDepts());
            display.showPfWidgetVar("dlgOrgTreeForward");
        } catch (Exception e) {
            log.error("btnOpenForwardOrgTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /** 訊息呼叫 */
    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -8682268805931852908L;

        @Override
        public void showMessage(String m) {
            MessagesUtils.showError(m);
        }
    };

    /** 類別樹 Component CallBack */
    private final CategoryTreeCallBack categoryTreeCallBack = new CategoryTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -5433163255086308391L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelCate() {
            categoryTreeComponent.selCate();
            searchQuery.setBigDataCateSids(categoryTreeComponent.getBigDataCateSids());
            searchQuery.setMiddleDataCateSids(categoryTreeComponent.getMiddleDataCateSids());
            searchQuery.setSmallDataCateSids(categoryTreeComponent.getSmallDataCateSids());
            categoryTreeComponent.clearCateSids();
        }

        @Override
        public void loadSelCate(List<String> smallDataCateSids) {
            categoryTreeComponent.init();
            categoryTreeComponent.selectedItem(smallDataCateSids);
            this.confirmSelCate();
        }

    };

    /** 報表 組織樹 Component CallBack */
    private final ReportOrgTreeCallBack reportOrgTreeForwardCallBack = new ReportOrgTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 5173521095139153082L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelOrg() {
            searchQuery.setForwardDepts(orgTreeForwardComponent.getSelOrgSids());
        }
    };

    private final ReportCustomFilterCallback reportCustomFilterCallback = new ReportCustomFilterCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = -2918400113936816843L;

        @Override
        public void reloadDefault(String index) {
            searchQuery02CustomFilter.loadDefaultSetting(defaultViewDepSids, searchQuery);
            categoryTreeCallBack.loadSelCate(searchQuery.getSmallDataCateSids());
        }
    };

    // ========================================================================
    // 需求來源單位挑選樹
    // ========================================================================
    @Getter
    @Setter
    private transient TreePickerComponent depTreePicker;

    @Getter
    private final String DEP_TREE_PICKER_DLG_WV = this.getClass().getSimpleName() + "DEP_TREE_PICKER_DLG_WV";
    @Getter
    private final String DEP_TREE_PICKER_CONTENT_CLASS = this.getClass().getSimpleName() + "DEP_TREE_PICKER_CONTENT_CLASS";
    @Getter
    private final String DEP_TREE_PICKER_SHOW_SELECTED_CLASS = this.getClass().getSimpleName() + "DEP_TREE_PICKER_SHOW_SELECTED_CLASS";

    /**
     * 顯示所選取的單位
     * 
     * @return
     */
    public String getSelectedDepsInfo() {
        Set<Integer> requireDepts = this.searchQuery.getRequireDepts();
        if (WkStringUtils.isEmpty(requireDepts)
                || WkCommonUtils.compare(requireDepts, this.canViewDepSids)) {
            return "全部顯示";
        }

        return WkOrgUtils.findNameBySid(WkOrgUtils.sortByOrgTree(requireDepts), "、");
    }

    /**
     * 單位樹選單 - 開啟選單
     */
    public void event_dialog_depTreePicker_open() {

        try {
            this.depTreePicker = new TreePickerComponent(depTreePickerCallback, "單位挑選");
            this.depTreePicker.rebuild();
        } catch (Exception e) {
            log.error("開啟【單位挑選】設定視窗失敗!" + e.getMessage(), e);
            MessagesUtils.showError("開啟【單位挑選】設定視窗失敗!");
            return;
        }

        displayController.showPfWidgetVar(this.DEP_TREE_PICKER_DLG_WV);
    }

    /**
     * 單位樹選單 - 關閉選單
     */
    public void event_dialog_depTreePicker_confirm() {

        // 取得元件中被選擇的項目
        this.searchQuery.setRequireDepts(this.depTreePicker.getSelectedItemIntegerSids());

        // 畫面控制
        displayController.hidePfWidgetVar(this.DEP_TREE_PICKER_DLG_WV);
    }

    /**
     * 分派通知單位選單 - callback 事件
     */
    private final TreePickerCallback depTreePickerCallback = new TreePickerCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = -2685194232972981152L;

        @Override
        public List<WkItem> prepareAllItems() throws Exception {
            // ====================================
            // 轉 WkItem
            // ====================================
            return treePickerDepHelper.prepareDepItems(
                    SecurityFacade.getCompanyId(),
                    Lists.newArrayList(canViewDepSids));

        }

        /**
         * 準備已選擇項目
         * 
         * @return
         */
        @Override
        public List<String> prepareSelectedItemSids() throws Exception {

            if (WkStringUtils.isEmpty(searchQuery.getRequireDepts())) {
                return Lists.newArrayList();
            }

            // 回傳
            return searchQuery.getRequireDepts().stream()
                    .map(sid -> sid + "")
                    .collect(Collectors.toList());
        }

        @Override
        public List<String> prepareDisableItemSids() throws Exception {
            // 沒有不可選擇的單位
            return Lists.newArrayList();
        }
    };

    // ========================================================================
    // 自訂預設搜尋條件-需求來源單位挑選樹
    // ========================================================================
    @Getter
    @Setter
    private transient TreePickerComponent customDepTreePicker;

    @Getter
    private final String CUSTOM_DEP_TREE_PICKER_DLG_WV = this.getClass().getSimpleName() + "CUSTOM_DEP_TREE_PICKER_DLG_WV";
    @Getter
    private final String CUSTOM_DEP_TREE_PICKER_CONTENT_CLASS = this.getClass().getSimpleName() + "CUSTOM_DEP_TREE_PICKER_CONTENT_CLASS";
    @Getter
    private final String CUSTOM_DEP_TREE_PICKER_SHOW_SELECTED_CLASS = this.getClass().getSimpleName() + "CUSTOM_DEP_TREE_PICKER_SHOW_SELECTED_CLASS";

    /**
     * 顯示所選取的單位
     * 
     * @return
     */
    public String getCustomSelectedDepsInfo() {
        Set<Integer> requireDepts = this.searchQuery02CustomFilter.getTempSearchQuery02().getRequireDepts();
        if (WkStringUtils.isEmpty(requireDepts)
                || WkCommonUtils.compare(requireDepts, this.canViewDepSids)) {
            return "全部顯示";
        }

        return WkOrgUtils.findNameBySid(WkOrgUtils.sortByOrgTree(requireDepts), "、");
    }

    /**
     * 單位樹選單 - 開啟選單
     */
    public void event_dialog_customDepTreePicker_open() {

        try {
            this.customDepTreePicker = new TreePickerComponent(customDepTreePickerCallback, "單位挑選");
            this.customDepTreePicker.rebuild();
        } catch (Exception e) {
            log.error("開啟【單位挑選】設定視窗失敗!" + e.getMessage(), e);
            MessagesUtils.showError("開啟【單位挑選】設定視窗失敗!");
            return;
        }

        displayController.showPfWidgetVar(this.CUSTOM_DEP_TREE_PICKER_DLG_WV);
    }

    /**
     * 單位樹選單 - 關閉選單
     */
    public void event_dialog_customDepTreePicker_confirm() {

        // 取得元件中被選擇的項目
        this.searchQuery02CustomFilter.getTempSearchQuery02().setRequireDepts(this.customDepTreePicker.getSelectedItemIntegerSids());

        // 畫面控制
        this.displayController.hidePfWidgetVar(this.CUSTOM_DEP_TREE_PICKER_DLG_WV);
    }

    /**
     * 單位樹選單 - callback 事件
     */
    private final TreePickerCallback customDepTreePickerCallback = new TreePickerCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = -2685194232972981152L;

        @Override
        public List<WkItem> prepareAllItems() throws Exception {
            // ====================================
            // 轉 WkItem
            // ====================================
            return treePickerDepHelper.prepareDepItems(
                    SecurityFacade.getCompanyId(),
                    Lists.newArrayList(canViewDepSids));

        }

        /**
         * 準備已選擇項目
         * 
         * @return
         */
        @Override
        public List<String> prepareSelectedItemSids() throws Exception {

            // ====================================
            // 取得自定義查詢條件設定值
            // ====================================
            List<ReportCustomFilterVO> reportCustomFilterVOs = reportCustomFilterLogicComponent.getReportCustomFilter(
                    Search02QueryColumn.Search02Query,
                    SecurityFacade.getUserSid());

            if (WkStringUtils.isEmpty(reportCustomFilterVOs)) {
                return defaultViewDepSids.stream()
                        .map(depSid->depSid+"")
                        .collect(Collectors.toList());
            }

            ReportCustomFilterVO reportCustomFilterVO = reportCustomFilterVOs.get(0);

            // ====================================
            // 取得『需求來源』欄位值
            // ====================================
            Set<Integer> customrDepSids = Sets.newHashSet();

            for (ReportCustomFilterDetailVO detailVO : reportCustomFilterVO.getReportCustomFilterDetailVOs()) {
                if (Search02QueryColumn.Department.equals(detailVO.getSearchReportCustomEnum())
                        && detailVO instanceof ReportCustomFilterArrayStringVO) {

                    ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) detailVO;
                    if (WkStringUtils.notEmpty(reportCustomFilterArrayStringVO.getArrayStrings())) {
                        customrDepSids = reportCustomFilterArrayStringVO.getArrayStrings().stream()
                                .filter(depStr -> WkStringUtils.isNumber(depStr))
                                .map(depStr -> Integer.parseInt(depStr))
                                .filter(depSid -> canViewDepSids.contains(depSid)) // 自訂條件需存在於可閱單位清單
                                .collect(Collectors.toSet());
                    }
                }
            }

            if (WkStringUtils.isEmpty(customrDepSids)) {
                return defaultViewDepSids.stream()
                        .map(depSid->depSid+"")
                        .collect(Collectors.toList());
            }

            // 回傳
            return customrDepSids.stream()
                    .map(sid -> sid + "")
                    .collect(Collectors.toList());
        }

        @Override
        public List<String> prepareDisableItemSids() throws Exception {
            // 沒有不可選擇的單位
            return Lists.newArrayList();
        }
    };

}
