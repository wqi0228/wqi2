package com.cy.tech.request.web.controller.test;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.tech.request.logic.helper.component.ComponentHelper;
import com.cy.tech.request.logic.helper.component.treepker.TreePickerCategoryHelper;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.viewcomponent.treepker.TreePickerCallback;
import com.cy.work.viewcomponent.treepker.TreePickerComponent;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Controller
@Scope("view")
@Slf4j
public class TestTreePickerMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3063177885784086335L;
    @Autowired
    public TreePickerCategoryHelper helper;
    @Autowired
    public ComponentHelper componentHelper;

    /**
     * 項目樹
     */
    @Getter
    private TreePickerComponent categoryTreePicker;

    /**
     * 
     */
    private List<String> selectedCategorys;

    
    @PostConstruct
    public void init() {
        this.categoryTreePicker = new TreePickerComponent(categoryPickerCallback, "類別組合選單");
        try {
            this.categoryTreePicker.rebuild();
        } catch (Exception e) {
            MessagesUtils.showError("初始化錯誤!");
            log.error("初始化錯誤", e);
        }
    }

    private final TreePickerCallback categoryPickerCallback = new TreePickerCallback() {

        /**
         * 
         */
        private static final long serialVersionUID = 5149787257516101465L;

        /**
         * 準備所有的項目
         * 
         * @return
         * @throws Exception
         */
        @Override
        public List<WkItem> prepareAllItems() throws Exception {
            // 取得類別項目
            return componentHelper.prepareAllCategoryItems();
        }

        /**
         * 準備已選擇項目
         * 
         * @return
         */
        @Override
        public List<String> prepareSelectedItemSids() throws Exception {
            if (WkStringUtils.isEmpty(selectedCategorys)) {
                return Lists.newArrayList();
            }
            return selectedCategorys;
        }

        @Override
        public List<String> prepareDisableItemSids() throws Exception {
            return Lists.newArrayList();
        }
    };
}
