package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.view.Search08View;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.pt.PtService;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.search.helper.Search08Helper;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.view.component.CategoryTreeComponent;
import com.cy.tech.request.web.controller.view.component.ReportOrgTreeComponent;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.listener.CategoryTreeCallBack;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.listener.ReportCustomFilterCallback;
import com.cy.tech.request.web.listener.ReportOrgTreeCallBack;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.tech.request.web.view.to.search.query.SearchQuery08;
import com.cy.tech.request.web.view.to.search.query.SearchQuery08CustomFilter;
import com.cy.work.common.cache.WkCommonCache;
import com.cy.work.common.cache.WkUserWithRolesCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 原型確認一覽表
 *
 * @author jason_h
 */
@Controller
@Scope("view")
@Slf4j
public class Search08MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8934896183587741653L;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private Search08Helper searchHelper;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private PtService ptService;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private Require01MBean r01MBean;
    @Autowired
    transient private WkCommonCache wkCommonCache;
    @Autowired
    transient private ReportCustomFilterLogicComponent reportCustomFilterLogicComponent;

    @Getter
    /** 類別樹 Component */
    private CategoryTreeComponent categoryTreeComponent;
    @Getter
    /** 報表 組織樹 Component */
    private ReportOrgTreeComponent orgTreeForwardComponent;

    @Getter
    /** 切換模式 - 全畫面狀態 */
    private SwitchType switchFullType = SwitchType.DETAIL;
    @Getter
    /** 切換模式 */
    private SwitchType switchType = SwitchType.CONTENT;
    
    /** 列表 id */
    @Getter
    private final String dataTableId = "dtRequire";
    @Getter
    /** 查詢物件 */
    private SearchQuery08 searchQuery;

    @Getter
    @Setter
    /** 所有的原型確認單據 */
    private List<Search08View> queryItems;
    private List<Search08View> tempItems;
    @Getter
    @Setter
    /** 選擇的原型確認單據 */
    private Search08View querySelection;
    @Getter
    /** 上下筆移動keeper */
    private Search08View queryKeeper;
    @Getter
    private SearchQuery08CustomFilter searchQuery08CustomFilter;
    @Setter
    @Getter
    /** dataTable filter */
    private String prototypeCreateDepName;
    @Getter
    /** dataTable filter items */
    private List<SelectItem> prototypeCreateDepNameItems;

    @PostConstruct
    public void init() {
        this.initComponents();
        this.clear();
    }

    /**
     * 初始化 元件
     */
    private void initComponents() {
        this.searchQuery = new SearchQuery08(ReportType.PROTOTYPE_CHECK);
        this.categoryTreeComponent = new CategoryTreeComponent(categoryTreeCallBack);

        // 查詢登入者所有角色
        List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
                SecurityFacade.getUserSid(),
                SecurityFacade.getCompanyId());

        // 進階查詢條件: 轉發至
        this.orgTreeForwardComponent = new ReportOrgTreeComponent(
                loginBean.getCompanyId(),
                loginBean.getDep(),
                roleSids,
                false,
                reportOrgTreeForwardCallBack);

        this.searchQuery08CustomFilter = new SearchQuery08CustomFilter(
                loginBean.getUserSId(),
                loginBean.getDep().getSid(),
                loginBean.getComp().getSid(),
                reportCustomFilterLogicComponent, messageCallBack, display, reportCustomFilterCallback);
    }

    public void openDefaultSetting() {

        try {
            this.searchQuery08CustomFilter.loadDefaultSetting(
                    this.searchQuery08CustomFilter.getTempSearchQuery08());

        } catch (Exception e) {
            log.error("openDefaultSetting ERROR", e);
        }
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        this.clearQuery();
        this.search();
    }

    /**
     * 清除/還原選項
     */
    private void clearQuery() {

        try {
            searchQuery08CustomFilter.loadDefaultSetting(searchQuery);
            categoryTreeComponent.clearCate();
            categoryTreeCallBack.loadSelCate(searchQuery.getSmallDataCateSids());
        } catch (Exception e) {
            log.error("clearQuery ERROR", e);
        }
    }

    public void search() {
        tempItems = searchHelper.search(loginBean.getCompanyId(), loginBean.getDep(), loginBean.getUser(), searchQuery);
        this.prototypeCreateDepName = null;
        this.prototypeCreateDepNameItems = this.tempItems.stream()
                .map(each -> each.getPrototypeCreateDepName())
                .collect(Collectors.toSet()).stream()
                .map(each -> new SelectItem(each, each))
                .collect(Collectors.toList());
        this.doChangeFilter();
    }

    /**
     * 進行dataTable filter
     */
    public void doChangeFilter() {
        this.queryItems = this.filterDataTable();
    }

    /**
     * filter
     *
     * @return
     */
    private List<Search08View> filterDataTable() {
        return tempItems.stream()
                .filter(each -> Strings.isNullOrEmpty(prototypeCreateDepName)
                        || prototypeCreateDepName.equals(each.getPrototypeCreateDepName()))
                .collect(Collectors.toList());
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (Search08View) event.getObject();
        this.changeRequireContent(this.queryKeeper);
        this.moveScreenTab();
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search08View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.removeClassByTextBold(dtId, pageCount);
        this.transformHasRead();
        this.checkHelfScreen();
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Search08View to) {
        querySelection = to;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
        upDownBean.resetTabInfo(RequireBottomTabType.PT_CHECK_INFO, queryKeeper.getSid());
    }

    /**
     * 去除粗體Class
     *
     * @param dtId
     * @param pageCount
     */
    private void removeClassByTextBold(String dtId, String pageCount) {
        display.execute("removeClassByTextBold('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
        display.execute("changeAlreadyRead('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 變更已閱讀
     */
    private void transformHasRead() {
        querySelection.setReadRecordType(ReadRecordType.HAS_READ);
        queryKeeper = querySelection;
        queryItems.set(queryItems.indexOf(querySelection), querySelection);
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            display.update("headerTitle");
            display.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Search08View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
        this.moveScreenTab();
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            this.moveScreenTab();
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Search08View view) {
        view.setReadRecordType(ReadRecordType.HAS_READ);
        Require r = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(r, loginBean.getUser(), RequireBottomTabType.PT_CHECK_INFO);
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
        this.moveScreenTab();
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param mbean
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param mbean
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param mbean
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.transformHasRead();
        this.removeClassByTextBold(dtId, pageCount);
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    /**
     * 開啟 類別樹
     */
    public void btnOpenCategoryTree() {
        try {
            categoryTreeComponent.init();
            display.showPfWidgetVar("dlgCate");
        } catch (Exception e) {
            log.error("btnOpenCategoryTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 開啟 轉發至 組織樹
     */
    public void btnOpenForwardOrgTree() {
        try {
            orgTreeForwardComponent.initOrgTree(loginBean.getComp(), searchQuery.getForwardDepts());
            display.showPfWidgetVar("dlgOrgTreeForward");
        } catch (Exception e) {
            log.error("btnOpenForwardOrgTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /** 訊息呼叫 */
    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 2896239523943112468L;

        @Override
        public void showMessage(String m) {
            MessagesUtils.showError(m);
        }
    };

    /** 類別樹 Component CallBack */
    private final CategoryTreeCallBack categoryTreeCallBack = new CategoryTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 2841198012164258463L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelCate() {
            categoryTreeComponent.selCate();
            searchQuery.setBigDataCateSids(categoryTreeComponent.getBigDataCateSids());
            searchQuery.setMiddleDataCateSids(categoryTreeComponent.getMiddleDataCateSids());
            searchQuery.setSmallDataCateSids(categoryTreeComponent.getSmallDataCateSids());
            categoryTreeComponent.clearCateSids();
        }

        @Override
        public void loadSelCate(List<String> smallDataCateSids) {
            categoryTreeComponent.init();
            categoryTreeComponent.selectedItem(smallDataCateSids);
            this.confirmSelCate();
        }

    };

    /** 報表 組織樹 Component CallBack */
    private final ReportOrgTreeCallBack reportOrgTreeForwardCallBack = new ReportOrgTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -6396689333888467274L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelOrg() {
            searchQuery.setForwardDepts(orgTreeForwardComponent.getSelOrgSids());
        }
    };

    public String getPrototypeVersion(Search08View view) {
        String version = "V" + view.getVersion();
        if (PtStatus.REDO.equals(view.getPrototypeStatus())) {
            return version + "(重做)";
        }
        if (PtStatus.VERIFY_INVAILD.equals(view.getPrototypeStatus())) {
            return version + "(審核作廢)";
        }
        return version;
    }

    private void moveScreenTab() {
        this.r01MBean.getBottomTabMBean().resetTabIdx(this.r01MBean);
        this.r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.PT_CHECK_INFO);

        List<String> sids = ptService.findSidsBySourceSid(this.r01MBean.getRequire().getSid());
        String indicateSid = queryKeeper.getSid();
        if (sids.indexOf(indicateSid) != -1) {
            display.execute("PF('pt_acc_panel_layer_zero').select(" + sids.indexOf(indicateSid) + ");");
            for (int i = 0; i < sids.size(); i++) {
                if (i != sids.indexOf(indicateSid)) {
                    display.execute("PF('pt_acc_panel_layer_zero').unselect(" + i + ");");
                }
            }
        }
    }

    /**
     * 通知對象
     * 
     * @param view
     * @return
     */
    public String getNotifyStaffStr(Search08View view) {

        // cache 中取回
        String cacheName = "Search08View-getNotifyStaffStr";
        List<String> keys = Lists.newArrayList(view.getCheckNo());
        String names = wkCommonCache.getCache(cacheName, keys, 5);
        if (names != null) {
            return names;
        }

        // 組名稱
        List<String> userSids = Optional.ofNullable(view.getNotifyStaffTo())
                .map(notify -> notify.getValue())
                .orElse(Lists.newArrayList());

        names = WkUserUtils.findNameBySidStrs(userSids, "、");

        // put 快取
        wkCommonCache.putCache(cacheName, keys, names);

        return names;
    }

    /**
     * 通知單位
     * 
     * @param view
     * @return
     */
    public String getNotifyDepsStr(Search08View view) {

        // cache 中取回
        String cacheName = "Search08View-getNotifyDepsStr";
        List<String> keys = Lists.newArrayList(view.getCheckNo());
        String orgNames = wkCommonCache.getCache(cacheName, keys, 5);
        if (orgNames != null) {
            return orgNames;
        }

        // 組單位名稱
        List<String> orgSids = Optional.ofNullable(view.getNotifyDepsTo())
                .map(send -> send.getValue())
                .orElse(Lists.newArrayList());

        orgNames = WkOrgUtils.findNameBySidStrs(orgSids, "、");

        // put 快取
        wkCommonCache.putCache(cacheName, keys, orgNames);

        return orgNames;
    }

    private final ReportCustomFilterCallback reportCustomFilterCallback = new ReportCustomFilterCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = -1007996283505146662L;

        @Override
        public void reloadDefault(String index) {
            searchQuery08CustomFilter.loadDefaultSetting(searchQuery);
            categoryTreeCallBack.loadSelCate(searchQuery.getSmallDataCateSids());
        }
    };

}
