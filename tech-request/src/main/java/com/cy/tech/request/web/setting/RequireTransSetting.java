/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.setting;

import com.cy.tech.request.vo.enums.OthSetStatus;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.RequireTransProgramType;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.google.common.collect.Lists;
import java.util.List;
import javax.faces.model.SelectItem;

/**
 * 轉單程式共用選項
 *
 * @author brain0925_liao
 */
public class RequireTransSetting {

    /** 轉單程式-建立單位轉換,程序挑選選項 */
    private static List<SelectItem> transCreateDepProgramTypeItems;
    /** 轉單程式-通知單位轉換,程序挑選選項 */
    private static List<SelectItem> transNotifyDepProgramTypeItems;

    /** 需求製作進度 */
    private static List<SelectItem> requireStatusTypeItems;
    /** 原型確認狀態 */
    private static List<SelectItem> ptStatusItems;
    /** 送測狀態 */
    private static List<SelectItem> workTestStatusItems;
    /** on程式狀態 */
    private static List<SelectItem> workOnpgStatusItems;
    /** 其它設定資訊狀態 */
    private static List<SelectItem> othSetStatusItems;

    /** 轉單程式-通知單位轉換,程序挑選選項 */
    public static List<SelectItem> getTransNotifyDepProgramTypeItems() {
        if (transNotifyDepProgramTypeItems != null) {
            return transNotifyDepProgramTypeItems;
        }
        transNotifyDepProgramTypeItems = Lists.newArrayList();
        transNotifyDepProgramTypeItems.add(new SelectItem(RequireTransProgramType.PTCHECK.name(), "原型確認"));
        transNotifyDepProgramTypeItems.add(new SelectItem(RequireTransProgramType.WORKTESTSIGNINFO.name(), "送測"));
        transNotifyDepProgramTypeItems.add(new SelectItem(RequireTransProgramType.OTHSET.name(), "其他資料設定"));
        transNotifyDepProgramTypeItems.add(new SelectItem(RequireTransProgramType.WORKONPG.name(), "ON程式"));
        return transNotifyDepProgramTypeItems;
    }

    /** 轉單程式-建立單位轉換,程序挑選選項 */
    public static List<SelectItem> getTransCreateDepProgramTypeItems() {
        if (transCreateDepProgramTypeItems != null) {
            return transCreateDepProgramTypeItems;
        }
        transCreateDepProgramTypeItems = Lists.newArrayList();
        transCreateDepProgramTypeItems.add(new SelectItem(RequireTransProgramType.REQUIRE.name(), "需求單"));
        transCreateDepProgramTypeItems.add(new SelectItem(RequireTransProgramType.PTCHECK.name(), "原型確認"));
        transCreateDepProgramTypeItems.add(new SelectItem(RequireTransProgramType.WORKTESTSIGNINFO.name(), "送測"));
        transCreateDepProgramTypeItems.add(new SelectItem(RequireTransProgramType.OTHSET.name(), "其他資料設定"));
        transCreateDepProgramTypeItems.add(new SelectItem(RequireTransProgramType.WORKONPG.name(), "ON程式"));
        return transCreateDepProgramTypeItems;
    }

    public static String getTransProgramTypeName(RequireTransProgramType requireTransProgramType) {
        List<SelectItem> selectItems = getTransCreateDepProgramTypeItems();
        for (SelectItem si : selectItems) {
            if (String.valueOf(si.getValue()).equals(requireTransProgramType.name())) {
                return si.getLabel();
            }
        }
        return "";
    }

    public static String getTransJobOthSetStatusName(OthSetStatus othSetStatus) {
        List<SelectItem> selectItems = getTransJobOthSetStatusItems();
        for (SelectItem si : selectItems) {
            if (String.valueOf(si.getValue()).equals(othSetStatus.name())) {
                return si.getLabel();
            }
        }
        return "";
    }

    /** 其它設定資訊狀態 */
    public static List<SelectItem> getTransJobOthSetStatusItems() {
        if (othSetStatusItems != null) {
            return othSetStatusItems;
        }
        othSetStatusItems = Lists.newArrayList();
        othSetStatusItems.add(new SelectItem(OthSetStatus.PROCESSING.name(), "進行中"));
        othSetStatusItems.add(new SelectItem(OthSetStatus.FINISH.name(), "完成"));
        othSetStatusItems.add(new SelectItem(OthSetStatus.INVALID.name(), "作廢"));
        return othSetStatusItems;
    }

    /** on程式狀態 */
    public static List<SelectItem> getTransJobWorkOnpgStatusItems() {
        if (workOnpgStatusItems != null) {
            return workOnpgStatusItems;
        }
        workOnpgStatusItems = Lists.newArrayList();
        workOnpgStatusItems.add(new SelectItem(WorkOnpgStatus.ALREADY_ON.name(), "已ON上"));
        workOnpgStatusItems.add(new SelectItem(WorkOnpgStatus.CHECK_REPLY.name(), "檢查回覆"));
        workOnpgStatusItems.add(new SelectItem(WorkOnpgStatus.CANCEL_ONPG.name(), "取消ON程式"));
        workOnpgStatusItems.add(new SelectItem(WorkOnpgStatus.CHECK_COMPLETE.name(), "檢查完成"));
        return workOnpgStatusItems;
    }

    public static String getTransJobWorkOnpgStatusName(WorkOnpgStatus workOnpgStatus) {
        List<SelectItem> selectItems = getTransJobWorkOnpgStatusItems();
        for (SelectItem si : selectItems) {
            if (String.valueOf(si.getValue()).equals(workOnpgStatus.name())) {
                return si.getLabel();
            }
        }
        return "";
    }

    /** 送測狀態 */
    public static List<SelectItem> getTransJobWorkTestStatusItems() {
        if (workTestStatusItems != null) {
            return workTestStatusItems;
        }
        workTestStatusItems = Lists.newArrayList();
        workTestStatusItems.add(new SelectItem(WorkTestStatus.SONGCE.name(), "送測"));
        workTestStatusItems.add(new SelectItem(WorkTestStatus.TESTING.name(), "測試中"));
        workTestStatusItems.add(new SelectItem(WorkTestStatus.RETEST.name(), "重測"));
        workTestStatusItems.add(new SelectItem(WorkTestStatus.QA_TEST_COMPLETE.name(), "QA測試完成"));
        workTestStatusItems.add(new SelectItem(WorkTestStatus.TEST_COMPLETE.name(), "測試完成"));
        workTestStatusItems.add(new SelectItem(WorkTestStatus.ROLL_BACK_TEST.name(), "退測"));
        workTestStatusItems.add(new SelectItem(WorkTestStatus.CANCEL_TEST.name(), "取消測試"));
        return workTestStatusItems;
    }

    public static String getTransJobWorkTestStatusName(WorkTestStatus workTestStatus) {
        List<SelectItem> selectItems = getTransJobWorkTestStatusItems();
        for (SelectItem si : selectItems) {
            if (String.valueOf(si.getValue()).equals(workTestStatus.name())) {
                return si.getLabel();
            }
        }
        return "";
    }

    /** 原型確認狀態 */
    public static List<SelectItem> getTransJobPtStatusItems() {
        if (ptStatusItems != null) {
            return ptStatusItems;
        }
        ptStatusItems = Lists.newArrayList();
        ptStatusItems.add(new SelectItem(PtStatus.SIGN_PROCESS.name(), "原型確認簽核中"));
        ptStatusItems.add(new SelectItem(PtStatus.APPROVE.name(), "原型確認已核准"));
        ptStatusItems.add(new SelectItem(PtStatus.PROCESS.name(), "原型確認中"));
        ptStatusItems.add(new SelectItem(PtStatus.REDO.name(), "重新實作"));
        ptStatusItems.add(new SelectItem(PtStatus.VERIFY_INVAILD.name(), "審核作廢"));
        ptStatusItems.add(new SelectItem(PtStatus.FUNCTION_CONFORM.name(), "符合需求"));
        return ptStatusItems;
    }

    public static String getTransJobPtStatusName(PtStatus ptStatus) {
        List<SelectItem> selectItems = getTransJobPtStatusItems();
        for (SelectItem si : selectItems) {
            if (String.valueOf(si.getValue()).equals(ptStatus.name())) {
                return si.getLabel();
            }
        }
        return "";
    }

    /** 需求製作進度 */
    public static List<SelectItem> getTransJobRequireStatusTypeItems() {
        if (requireStatusTypeItems != null) {
            return requireStatusTypeItems;
        }

        requireStatusTypeItems = Lists.newArrayList();
        requireStatusTypeItems.add(new SelectItem(RequireStatusType.NEW_INSTANCE.name(), "新建檔"));
        requireStatusTypeItems.add(new SelectItem(RequireStatusType.WAIT_CHECK.name(), "待檢查確認"));
        requireStatusTypeItems.add(new SelectItem(RequireStatusType.ROLL_BACK_NOTIFY.name(), "退件通知"));
        requireStatusTypeItems.add(new SelectItem(RequireStatusType.SUSPENDED.name(), "需求暫緩"));
        requireStatusTypeItems.add(new SelectItem(RequireStatusType.PROCESS.name(), "進行中"));
        requireStatusTypeItems.add(new SelectItem(RequireStatusType.COMPLETED.name(), "已完成"));
        requireStatusTypeItems.add(new SelectItem(RequireStatusType.CLOSE.name(), "結案"));
        requireStatusTypeItems.add(new SelectItem(RequireStatusType.AUTO_CLOSED.name(), "自動結案"));
        requireStatusTypeItems.add(new SelectItem(RequireStatusType.INVALID.name(), "作廢"));
        return requireStatusTypeItems;
    }

    public static String getTransJobRequireStatusTypeName(RequireStatusType requireStatusType) {
        if (requireStatusType == null) {
            return "";
        }

        List<SelectItem> selectItems = getTransJobRequireStatusTypeItems();
        for (SelectItem si : selectItems) {
            if (String.valueOf(si.getValue()).equals(requireStatusType.name())) {
                return si.getLabel();
            }
        }
        return "";
    }
}
