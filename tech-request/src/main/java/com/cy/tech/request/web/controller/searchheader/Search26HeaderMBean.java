/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.searchheader;

import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.web.controller.enums.ReqStatusMBean;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.google.common.collect.Lists;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

/**
 * 已分派單據查詢的搜尋表頭
 *
 * @author jason_h
 */
@Controller
@Scope("view")
public class Search26HeaderMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4112433831154619484L;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private ReqStatusMBean reqStatusUtils;

    /** 選擇的需求製作進度 */
    @Getter
    @Setter
    private RequireStatusType selectRequireStatusType;

    @Getter
    private String queryUnitName;
    @Getter
    private Boolean isQueryAssignUnit;

    @PostConstruct
    private void init() {
        this.clear();
    }

    /**
     * 清除/還原選項
     */
    public void clear() {
        this.initCommHeader();
        this.initHeader();
    }

    /**
     * 初始化commHeader
     */
    private void initCommHeader() {
        commHeaderMBean.clear();
        commHeaderMBean.clearAdvance();
        commHeaderMBean.initDefaultRequireDepts(true, false);
        commHeaderMBean.setStartDate(null);
        commHeaderMBean.setEndDate(new Date());
    }

    /**
     * 初始化Header
     */
    private void initHeader() {
        selectRequireStatusType = RequireStatusType.PROCESS;
        this.initQueryUnitName();
    }

    /**
     * 建立需求狀態查詢 List
     *
     * @return
     */
    public List<String> createQueryReqStatus() {
        return reqStatusUtils.createQueryReqStatus(this.selectRequireStatusType, this.getAllReqStatus());
    }

    /**
     * 取得此報表全部製作進度查詢
     *
     * @return
     */
    private List<RequireStatusType> getAllReqStatus() {
        return reqStatusUtils.createExcludeStatus(
                Lists.newArrayList(RequireStatusType.DRAFT)
        );
    }

    /**
     * 畫面用全部製作進度查詢
     *
     * @return
     */
    public SelectItem[] getReqStatusItems() {
        return reqStatusUtils.buildItem(this.getAllReqStatus());
    }

    public void changeReqStatusEvent() {
        this.initQueryUnitName();
    }

    private void initQueryUnitName() {
        if (selectRequireStatusType.equals(RequireStatusType.NEW_INSTANCE)
                || selectRequireStatusType.equals(RequireStatusType.WAIT_CHECK)
                || selectRequireStatusType.equals(RequireStatusType.INVALID)
                || selectRequireStatusType.equals(RequireStatusType.ROLL_BACK_NOTIFY)) {
            queryUnitName = "建單";
            isQueryAssignUnit = false;
        } else {
            queryUnitName = "分派";
            isQueryAssignUnit = true;
        }
    }

    /**
     * 建立需求狀態查詢 List
     *
     * @return
     */
    public List<String> createQueryAllReqStatus() {
        return this.getAllReqStatus().stream()
                .map(each -> each.name())
                .collect(Collectors.toList());
    }
}
