/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.setting;

import com.cy.tech.request.logic.service.customer.ReqWorkCustomerHelper;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.customer.vo.WorkCustomer;
import com.cy.work.customer.vo.enums.CustomerSourceType;
import com.cy.work.customer.vo.enums.EnableType;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 客戶資料維護
 *
 * @author kasim
 */
@NoArgsConstructor
@Controller
@Scope("view")
public class Setting07MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2802119533948793851L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private ReqWorkCustomerHelper customerService;
    @Autowired
    transient private DisplayController displayController;

    /** 狀態 */
    @Getter
    @Setter
    private EnableType enableType;
    /** 客戶名稱 */
    @Getter
    @Setter
    private String name;
    /** 列表高度 */
    @Getter
    private String dtScrollHeight = "400";
    /** 列表 */
    @Getter
    private List<WorkCustomer> allCustomer;
    /** 列表選取 */
    @Getter
    @Setter
    private WorkCustomer selCustomer;
    /** 控制物件 */
    @Getter
    @Setter
    private WorkCustomer customerObj = new WorkCustomer();

    @PostConstruct
    public void init() {
        this.enableType = null;
        this.name = "";
        this.seach();
        selCustomer = new WorkCustomer();
    }

    /**
     * 查詢
     */
    public void seach() {
        boolean isEnableType = enableType != null;
        boolean hasName = !Strings.isNullOrEmpty(name);
        if (isEnableType && hasName) {
            allCustomer = customerService.findByEnableAndName(enableType, name);
        } else if (isEnableType) {
            allCustomer = customerService.findByEnable(enableType);
        } else if (hasName) {
            allCustomer = customerService.findByName(name);
        } else {
            allCustomer = customerService.findAll();
        }
        if (selCustomer != null && !allCustomer.contains(selCustomer)) {
            selCustomer = null;
        }
    }

    /**
     * 新增
     */
    public void btnAdd() {
        customerObj = new WorkCustomer();
        customerObj.setEnable(EnableType.ENABLE);
        customerObj.setCustomerSourceType(CustomerSourceType.MANUALLY);
    }

    /**
     * 編輯
     */
    public void btnEdit() {
        customerObj = customerService.findCopyOne(selCustomer.getSid());
    }

    /**
     * 更新
     */
    public void btnSave() {
        String errMsg = customerService.checkSave(customerObj);
        if (!Strings.isNullOrEmpty(errMsg)) {
            MessagesUtils.showError(errMsg);
            return;
        }
        if (customerService.isAnyWorkCustomerUseByWorkCustomer(customerObj)) {
            displayController.showPfWidgetVar("screens_dlgDblAlias");
            return;
        }
        this.save();
    }

    /**
     * 更新
     */
    public void save() {
        customerService.save(customerObj, loginBean.getUser());
        displayController.hidePfWidgetVar("screen_dlgSave");
        this.seach();
        displayController.update("screen_view_dt");
    }

    /**
     * 更新dtScrollHeight
     */
    public void updateDtScrollHeight() {
        String height = displayController.getRequestParameterMap("dtScrollHeight");
        if (!Strings.isNullOrEmpty(height)) {
            dtScrollHeight = height;
        }
    }
}
