/**
 * 
 */
package com.cy.tech.request.web.controller.setting.setting10;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.cy.tech.request.logic.service.Set10V70TrnsRecorveyService;
import com.cy.tech.request.web.controller.setting.Setting10MBean;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 *
 */
@Slf4j
@Controller
@Scope("view")
public class Set10V70TrnsRecorveyMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -9113612726062153001L;
    @Autowired
    private transient Set10V70TrnsRecorveyService set10V70TrnsRecorveyService;
    @Autowired
    transient private Setting10MBean set10Bean;

    // ========================================================================
    // view 變數
    // ========================================================================
    /**
     * 起日
     */
    @Getter
    @Setter
    public Date condition_startDate;
    /**
     * 迄日
     */
    @Getter
    @Setter
    public Date condition_endDate;
    /**
     * 
     */
    @Getter
    @Setter
    public String condition_requireNos;

    // ========================================================================
    // 方法區
    // ========================================================================
    @PostConstruct
    public void init() {
    }

    public void recorvey() {

        this.set10Bean.clearInfoScreenMsg();

        String logMessage = "開始處理復原!";
        this.set10Bean.addInfoMsg("\r\n" + logMessage);
        log.info(logMessage);
        // ====================================
        // 查詢此次要處理的單號
        // ====================================
        Map<String, String> requireNoSidMap = null;

        try {
            requireNoSidMap = this.prepareRequireNoSidMap();
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        }

        if (requireNoSidMap.size() == 0) {
            logMessage = "找不到需要復原的需求單!";
            this.set10Bean.addInfoMsg("\r\n" + logMessage);
            log.info(logMessage);
        } else {
            logMessage = "開始處理需要復原的需求單共[" + requireNoSidMap.size() + "]筆!";
            this.set10Bean.addInfoMsg("\r\n" + logMessage);
            log.info(logMessage);
        }

        // ====================================
        // 查詢此次要處理的單號
        // ====================================

        try {
            Iterator<String> reqNosIt = requireNoSidMap.keySet().stream().sorted().iterator();
            Map<String, String> currRequireNoSidMap = Maps.newLinkedHashMap();
            // 每 500 筆 commit 一次
            while (reqNosIt.hasNext()) {
                // 收集資料
                String requireNo = reqNosIt.next();
                String requireSid = requireNoSidMap.get(requireNo);
                currRequireNoSidMap.put(requireNo, requireSid);

                // 累積五百筆一次處理
                if (currRequireNoSidMap.size() >= 500) {
                    this.set10V70TrnsRecorveyService.recorvey(currRequireNoSidMap);
                    logMessage = "\r\n復原:" + String.join("\r\n復原:", currRequireNoSidMap.keySet());
                    this.set10Bean.addInfoMsg(logMessage);
                    log.info(logMessage);
                    currRequireNoSidMap.clear();
                }

            }
            // 執行 while loop 最後一次剩下的
            if (currRequireNoSidMap.size() > 0) {
                this.set10V70TrnsRecorveyService.recorvey(currRequireNoSidMap);
                logMessage = "\r\n復原:" + String.join("\r\n復原:", currRequireNoSidMap.keySet());
                this.set10Bean.addInfoMsg(logMessage);
                log.info(logMessage);
                currRequireNoSidMap.clear();
            }
        } catch (Exception e) {
            MessagesUtils.showError("復原失敗！" + e.getMessage());
            log.error("復原失敗！", e);
            this.set10Bean.addInfoMsg("\r\n復原失敗");
        }

        this.set10Bean.addInfoMsg("\r\n復原完成");
        MessagesUtils.showInfo("復原完成");
    }

    /**
     * @return
     * @throws UserMessageException
     */
    private Map<String, String> prepareRequireNoSidMap() throws UserMessageException {
        // ====================================
        // 解析輸入的條件
        // ====================================
        // 最小日期
        String greaterThanRequireNo = "";
        if (condition_startDate != null) {
            Calendar cal = Calendar.getInstance(TimeZone.getDefault());
            cal.setTime(condition_startDate);
            greaterThanRequireNo = "TGTR"
                    + WkStringUtils.padding(cal.get(Calendar.YEAR) + "", '0', 4, true)
                    + WkStringUtils.padding((cal.get(Calendar.MONTH) + 1) + "", '0', 2, true)
                    + WkStringUtils.padding(cal.get(Calendar.DAY_OF_MONTH) + "", '0', 2, true)
                    + "000";
        }
        // 最大日期
        String lessThanRequireNo = "";
        if (condition_endDate != null) {
            Calendar cal = Calendar.getInstance(TimeZone.getDefault());
            cal.setTime(condition_endDate);
            lessThanRequireNo = "TGTR"
                    + WkStringUtils.padding(cal.get(Calendar.YEAR) + "", '0', 4, true)
                    + WkStringUtils.padding((cal.get(Calendar.MONTH) + 1) + "", '0', 2, true)
                    + WkStringUtils.padding(cal.get(Calendar.DAY_OF_MONTH) + "", '0', 2, true)
                    + "999";
        }
        // 需求單號字串
        List<String> requireNos = null;
        condition_requireNos = WkStringUtils.safeTrim(condition_requireNos);
        // 去斷行
        condition_requireNos = condition_requireNos.replaceAll("\r\n", condition_requireNos);
        condition_requireNos = condition_requireNos.replaceAll("\n", condition_requireNos);
        // 去空白
        condition_requireNos = condition_requireNos.replaceAll(" ", condition_requireNos);
        if (WkStringUtils.notEmpty(condition_requireNos)) {
            requireNos = Lists.newArrayList(condition_requireNos.split(","));
        }
        // 去除空白資料
        if (WkStringUtils.notEmpty(requireNos)) {
            requireNos = requireNos.stream().filter(requireNo -> WkStringUtils.notEmpty(requireNo)).collect(Collectors.toList());
        }

        // ====================================
        // 查詢
        // ====================================
        return this.set10V70TrnsRecorveyService.queryRequireNoSidMap(
                requireNos,
                greaterThanRequireNo,
                lessThanRequireNo);
    }

}
