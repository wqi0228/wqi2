/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require.othset;

import com.cy.tech.request.logic.service.othset.OthSetShowService;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.othset.OthSet;
import com.cy.tech.request.web.controller.values.LoginBean;
import java.io.Serializable;
import java.util.List;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 功能按鍵顯示處理
 *
 * @author shaun
 */
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class OthSetShowMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8860732114869551769L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private OthSetShowService osShowService;

    /**
     * 組合通知單位抬頭訊息
     *
     * @param othset
     * @return
     */
    public String findNoticeDepTitle(OthSet othset) {
        return osShowService.findNoticeDepTitle(othset);
    }

    /**
     * 組合通知單位抬頭訊息 for overlay
     *
     * @param othset
     * @return
     */
    public List<String> findNoticeDepTitleByOp(OthSet othset) {
        return osShowService.findNoticeDepTitleByOp(othset);
    }

    /**
     * 關閉編輯
     *
     * @param othset
     * @return
     */
    public boolean disableEdit(Require require, OthSet othset) {
        return osShowService.disableEdit(require, othset, loginBean.getUser());
    }

    /**
     * 關閉檢查記錄
     *
     * @param othset
     * @return
     */
    public boolean disableReply(OthSet othset) {
        return osShowService.disableReply(othset, loginBean.getUser());
    }

    /**
     * 關閉取消測試
     *
     * @param othset
     * @return
     */
    public boolean disableCancel(Require req, OthSet othset) {
        return osShowService.disableCancel(req, othset, loginBean.getUser());
    }

    /**
     * 關閉完成
     *
     * @param othset
     * @return
     */
    public boolean disableComplate(Require require, OthSet othset) {
        return osShowService.disableComplate(require, othset, loginBean.getUser());
    }

}
