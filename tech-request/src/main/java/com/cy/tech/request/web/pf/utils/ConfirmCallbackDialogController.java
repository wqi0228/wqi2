package com.cy.tech.request.web.pf.utils;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.utils.WkStringUtils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 注意, 不可使用 InitializingBean , Scope 要 區分為view ，但是 InitializingBean 會取得同一個
 * 
 * @author allen1214_wu
 */
@NoArgsConstructor
@Controller
@Slf4j
@Scope("view")
public class ConfirmCallbackDialogController implements  Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7055144204860401900L;

    @Autowired
    @Getter
    private DisplayController displayController;

    /**
     * 顯示訊息
     */
    @Getter
    private String message;
    /**
     * process 參數
     */
    @Getter
    private String process;

    /**
     * update 參數
     */
    @Getter
    private String updateAreaStr = "";

    /**
     * showConfimDialog 點選確認後的 callback Action
     */
    @Getter
    private Runnable confirmCallbackAction;
    /**
     * showConfimDialog 點選取消後的 callback Action
     */
    private Runnable cancelCallbackAction;

    @PostConstruct
    public void initPostConstruct() {
        this.init();
    }

    public void init() {
        this.message = "";
        this.process = "";
        this.updateAreaStr = "";
    }

    /**
     * 顯示確認視窗
     * 
     * @param msg            確認訊息
     * @param updates        按下確定後更新的區域
     * @param callbackAction 按下確定後，執行的方法
     */
    public void showConfimDialog(
            String msg,
            List<String> updates,
            Runnable callbackAction) {

        this.showConfimDialog(msg, updates, callbackAction, null);
    }

    /**
     * 顯示確認視窗
     * 
     * @param msg                   確認訊息
     * @param updateAreaStr         按下確定後更新的區域
     * @param confirmCallbackAction 按下確定後，執行的方法
     */
    public void showConfimDialog(
            String msg,
            String updateAreaStr,
            Runnable confirmCallbackAction) {
        this.showConfimDialog(msg, updateAreaStr, confirmCallbackAction, null);
    }

    /**
     * @param msg
     * @param updateAreaStr
     * @param confirmCallbackAction
     * @param cancelCallbackAction
     */
    public void showConfimDialog(
            String msg,
            String updateAreaStr,
            Runnable confirmCallbackAction,
            Runnable cancelCallbackAction) {
        this.init();

        this.message = msg;
        this.updateAreaStr = updateAreaStr;
        this.confirmCallbackAction = confirmCallbackAction;
        this.cancelCallbackAction = cancelCallbackAction;

        displayController.showPfWidgetVar("wv_confimDlgTemplate_YN");
        displayController.update("confimDlgTemplate_YN");
    }

    /**
     * @param msg
     * @param event
     * @param process
     * @param updates
     * @param confirmCallbackAction
     * @param cancelCallbackAction
     */
    public void showConfimDialog(
            String msg,
            List<String> updates,
            Runnable confirmCallbackAction,
            Runnable cancelCallbackAction) {

        this.init();

        this.message = msg;
        if (WkStringUtils.notEmpty(updates)) {
            this.updateAreaStr = String.join(" ", updates);
        }
        this.confirmCallbackAction = confirmCallbackAction;
        this.cancelCallbackAction = cancelCallbackAction;

        displayController.showPfWidgetVar("wv_confimDlgTemplate_YN");
        displayController.update("confimDlgTemplate_YN");
    }

    /**
     * client 端按下確認後執行的方法
     */
    public void showConfimDialogCallback() {
        // 執行callback方法
        if (confirmCallbackAction != null) {
            try {
                confirmCallbackAction.run();
            } catch (Exception e) {
                log.error("callback 方法呼叫錯誤!", e);
                MessagesUtils.showError(WkMessage.EXECTION);
            }
        }
    }

    /**
     * client 端按下取消後執行的方法
     */
    public void showConfimDialogCancelCallback() {
        if (confirmCallbackAction != null) {
            try {
                this.cancelCallbackAction.run();
            } catch (Exception e) {
                log.error("callback 方法呼叫錯誤!", e);
                MessagesUtils.showError(WkMessage.EXECTION);
            }

        }
    }

    /**
     * 點選取消時, 需執行 callback function
     * 
     * @return
     */
    public boolean hasCallbackAction() {
        return this.cancelCallbackAction != null;
    }

}
