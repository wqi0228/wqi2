package com.cy.tech.request.web.controller.search.helper;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.OverdueUnfinishedVO;
import com.cy.tech.request.logic.service.CategorySettingService;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.utils.DateUtils;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.vo.category.BasicDataBigCategory;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.web.controller.enums.ReqStatusMBean;
import com.cy.tech.request.web.controller.view.vo.OverdueUnfinishedQueryVO;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.logic.lib.manager.WorkCommonReadRecordManager;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class OverdueUnfinishedHelper {
    @Autowired
    private WkJsonUtils jsonUtils;
    @Autowired
    private URLService urlService;
    @Autowired
    private ReqularPattenUtils reqularUtils;
    @Autowired
    private SearchService searchService;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchHelper searchHelper;
    @Autowired
    transient private SearchResultHelper searchResultHelper;
    @Autowired
    transient private NativeSqlRepository nativeSqlRepository;
    @Autowired
    transient private WorkCommonReadRecordManager workCommonReadRecordManager;
    @Autowired
    transient private CategorySettingService categorySettingService;

    public List<OverdueUnfinishedVO> queryOverdueUnfinishedByCondition(OverdueUnfinishedQueryVO queryVO) throws UserMessageException {
        // ====================================
        // 檢查
        // ====================================
        if (queryVO == null) {
            log.warn("傳入 queryVO 為空");
            throw new UserMessageException(WkMessage.SESSION_TIMEOUT, InfomationLevel.WARN);
        }

        if (queryVO.getOrgTreeVO() == null
                || WkStringUtils.isEmpty(queryVO.getOrgTreeVO().getRequireDepts())) {
            throw new UserMessageException("請至少選擇一個被分派單位", InfomationLevel.WARN);
        }

        // ====================================
        // 取得以單號查詢
        // ====================================
        String targetRequireNo = reqularUtils.getRequireNo(SecurityFacade.getCompanyId(), queryVO.getSearchText());

        // ====================================
        // 組 SQL
        // ====================================

        Map<String, Object> parameters = Maps.newHashMap();

        StringBuffer sql = new StringBuffer();
        sql.append("SELECT tr.require_sid                    AS sid, ");
        sql.append("       tr.require_sid                    AS requireSid, ");
        sql.append("       tr.require_no                     AS requireNo, ");
        sql.append("       tr.create_dt                      AS createDate, ");
        sql.append("       tr.create_usr                     AS createUserSid, ");
        sql.append("       tr.dep_sid                        AS depSid, ");
        sql.append("       tr.require_status                 AS requireStatus, ");
        sql.append("       tr.hope_dt                        AS hopeDate, ");
        sql.append("       tr.urgency                        AS urgency, ");
        sql.append("       tr.inte_staff                     AS inteStaffUserSid, ");
        sql.append("       tr.require_establish_dt           AS requireEstablishDate, ");
        sql.append("       tr.require_finish_dt              AS requireFinishDate, ");
        sql.append("       keyMapping.big_category_name      AS bigCategoryName, ");
        sql.append("       keyMapping.middle_category_name   AS middleCategoryName, ");
        sql.append("       keyMapping.small_category_name    AS smallCategoryName, ");
        sql.append("       assignDep.assignDate              AS assignDate, ");
        sql.append("       assignDep.depts                   AS deptSids, ");
        sql.append("       tid.field_content                 AS requireTheme, ");
        sql.append("       Datediff(Date_format(Sysdate(), '%Y%m%d'), Date_format(tr.require_establish_dt, '%Y%m%d')) AS executeDays, ");
        sql.append("       Datediff(Date_format(Sysdate(), '%Y%m%d'), Date_format(tr.hope_dt, '%Y%m%d'))              AS overdueDays, ");
        sql.append("       CASE ");
        sql.append("         WHEN ( Date(Sysdate()) > tr.hope_dt ) = 1 THEN '已逾期' ");
        sql.append("         ELSE '' ");
        sql.append("       END                               AS overdue, ");
        // select 公用欄位
        sql.append(searchConditionSqlHelper.prepareCommonSelectColumnByRequireType2());

        sql.append(" FROM ");

        // FROM tr_require
        this.preparePartSQL_FromTrRequire(queryVO, targetRequireNo, sql, parameters);

        // INNER JOIN CategoryKeyMapping
        this.preparePartSQL_InnerJoinCategoryKeyMapping(queryVO, targetRequireNo, sql, parameters);

        // INNER JOIN 分派單位
        this.preparePartSQL_InnerJoinAssignSendSearchInfo(queryVO, targetRequireNo, sql, parameters);

        // LEFT JOIN 主題+模糊查詢
        sql.append(
                this.searchHelper.buildRequireIndexDictionaryCondition(
                        "",
                        queryVO.getSearchText(),
                        targetRequireNo));

        // LEFT JOIN 閱讀記錄
        sql.append(searchConditionSqlHelper.prepareSubFormReadRecordJoin(
                SecurityFacade.getUserSid(), FormType.REQUIRE, "tr"));

        //
        sql.append(" WHERE 1=1 ");

        // 查詢條件：是否閱讀
        if (WkStringUtils.isEmpty(targetRequireNo)) {
            sql.append(
                    this.workCommonReadRecordManager.prepareWhereConditionSQL(
                            queryVO.getReadRecordType(), "readRecord"));
        }

        sql.append(" GROUP BY requireSid ");
        sql.append(" ORDER BY assignDate DESC");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.OVERDUE_UNFINISHED, sql.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.OVERDUE_UNFINISHED, SecurityFacade.getUserSid());

        // ====================================
        // 查詢
        // ====================================
        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        List<OverdueUnfinishedVO> resultList = this.nativeSqlRepository.getResultList(
                sql.toString(), parameters, OverdueUnfinishedVO.class);

        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((resultList == null) ? 0 : resultList.size());
        if (WkStringUtils.notEmpty(resultList)) {

            // ====================================
            // 封裝
            // ====================================
            // 解析資料-開始
            usageRecord.parserDataStart();
            this.convert(resultList, SecurityFacade.getUserSid());
            // 解析資料-結束
            usageRecord.parserDataEnd();

            // ====================================
            // 後續處理
            // ====================================
            // 後續處理-開始
            usageRecord.afterProcessStart();

            // 查詢條件過濾：系統別
            resultList = resultList.stream()
                    .filter(each -> this.searchResultHelper.filterCheckItems(
                            each.getCheckItemTypes(),
                            queryVO.getCheckItemTypes(),
                            false,
                            null))
                    .collect(Collectors.toList());

            // 後續處理-結束
            usageRecord.afterProcessEnd();
        }
        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return resultList;

    }

    /**
     * 兜組部分SQL Inner Join CategoryKeyMapping
     * 
     * @param queryVO         查詢條件VO
     * @param targetRequireNo 指定需求單號
     * @param sql             SQL StringBuffer
     * @param parameters      parameters map
     */
    private void preparePartSQL_FromTrRequire(
            OverdueUnfinishedQueryVO queryVO,
            String targetRequireNo,
            StringBuffer sql,
            Map<String, Object> parameters) {

        sql.append("( ");
        sql.append(" SELECT * ");
        sql.append("   FROM tr_require ");

        // 固定條件：未完成
        sql.append("  WHERE require_finish_code = 'N' ");

        // 固定 + 查詢條件：需求單製作進度
        List<String> requireStatus = queryVO.getRequireStatusTypes();
        if (WkStringUtils.notEmpty(targetRequireNo)
                || WkStringUtils.isEmpty(requireStatus)) {
            requireStatus = ReqStatusMBean.STATUS2.stream().map(each -> each.name()).collect(Collectors.toList());
        }
        sql.append("  AND require_status IN (:requireStatus) ");
        parameters.put("requireStatus", requireStatus);

        // 查詢條件：是否逾期
        if (WkStringUtils.isEmpty(targetRequireNo)) {
            if (queryVO.isOverdue()) {
                sql.append("   AND Date(Sysdate()) > hope_dt  ");
            } else {
                sql.append("   AND Date(Sysdate()) <= hope_dt  ");
            }
        }

        sql.append(") tr ");
    }

    /**
     * 兜組部分SQL Inner Join CategoryKeyMapping
     * 
     * @param queryVO         查詢條件VO
     * @param targetRequireNo 指定需求單號
     * @param sql             SQL StringBuffer
     * @param parameters      parameters map
     */
    private void preparePartSQL_InnerJoinCategoryKeyMapping(
            OverdueUnfinishedQueryVO queryVO,
            String targetRequireNo,
            StringBuffer sql,
            Map<String, Object> parameters) {

        sql.append("       INNER JOIN tr_category_key_mapping keyMapping ");
        sql.append("               ON keyMapping.key_sid = tr.mapping_sid ");

        // 有指定需求單號時，以下查詢條件忽略
        if (WkStringUtils.isEmpty(targetRequireNo)) {
            return;
        }

        // 查詢條件：大類
        Set<String> bigCategorySids = Sets.newHashSet();
        if (WkStringUtils.notEmpty(queryVO.getBigCategorySids())) {
            bigCategorySids.addAll(queryVO.getBigCategorySids());
        }
        if (WkStringUtils.notEmpty(queryVO.getCategoryCombineVO().getBigDataCateSids())) {
            bigCategorySids.addAll(queryVO.getCategoryCombineVO().getBigDataCateSids());
        }
        if (WkStringUtils.notEmpty(bigCategorySids)) {
            Set<String> allBigSids = WkCommonUtils.safeStream(this.categorySettingService.findAllBig())
                    .map(BasicDataBigCategory::getSid)
                    .collect(Collectors.toSet());

            if (!WkCommonUtils.compare(allBigSids, bigCategorySids)) {
                sql.append("           AND keyMapping.big_category_sid IN ( :bigCategorySids )  ");
                parameters.put("bigCategorySids", bigCategorySids);
            }
        }

        // 查詢條件：中類
        if (WkStringUtils.notEmpty(queryVO.getCategoryCombineVO().getMiddleDataCateSids())) {
            sql.append("           AND keyMapping.middle_category_sid IN ( :middleCategorySids )  ");
            parameters.put("middleCategorySids", queryVO.getCategoryCombineVO().getMiddleDataCateSids());
        }

        // 查詢條件：小類
        if (WkStringUtils.notEmpty(queryVO.getCategoryCombineVO().getSmallDataCateSids())) {
            sql.append("           AND keyMapping.small_category_sid IN ( :smallCategorySids )  ");
            parameters.put("smallCategorySids", queryVO.getCategoryCombineVO().getSmallDataCateSids());
        }
    }

    /**
     * 兜組部分SQL Inner Join AssignSendSearchInfo
     * 
     * @param queryVO         查詢條件VO
     * @param targetRequireNo 指定需求單號
     * @param sql             SQL StringBuffer
     * @param parameters      parameters map
     */
    private void preparePartSQL_InnerJoinAssignSendSearchInfo(
            OverdueUnfinishedQueryVO queryVO,
            String targetRequireNo,
            StringBuffer sql,
            Map<String, Object> parameters) {

        sql.append("       INNER JOIN (SELECT require_sid, ");
        sql.append("                          Max(create_dt)                      AS assignDate, ");
        sql.append("                          Group_concat(dep_sid SEPARATOR ',') AS depts ");
        sql.append("                   FROM   tr_assign_send_search_info ");
        sql.append("                   WHERE  type = 0 ");

        // ====================================
        // 查詢條件：分派日期
        // ====================================
        if (WkStringUtils.isEmpty(targetRequireNo)
                && queryVO.getDateIntervalVO() != null
                && queryVO.getDateIntervalVO().getStartDate() != null) {
            sql.append("                AND create_dt >= :startDate  ");
            parameters.put("startDate", queryVO.getDateIntervalVO().getStartDate());
        }
        if (WkStringUtils.isEmpty(targetRequireNo)
                && queryVO.getDateIntervalVO() != null
                && queryVO.getDateIntervalVO().getEndDate() != null) {
            sql.append("                AND create_dt <= :endDate  ");
            parameters.put("endDate", DateUtils.convertToEndOfDay(queryVO.getDateIntervalVO().getEndDate()));
        }

        // ====================================
        // 查詢條件：被分派單位
        // ====================================
        // 單位挑選, 若沒選擇, 回傳空集合
        Set<String> allDepSids = WkCommonUtils.safeStream(WkOrgCache.getInstance().findAllDepSidByCompID(SecurityFacade.getCompanyId()))
                .map(sid -> sid + "")
                .collect(Collectors.toSet());

        Set<String> conditionDepSids = Sets.newHashSet();
        // 為空時，或有指定需求單號時，預設全部可閱單位
        if (WkStringUtils.isEmpty(queryVO.getOrgTreeVO().getRequireDepts())
                || WkStringUtils.notEmpty(targetRequireNo)) {
            conditionDepSids = queryVO.getOrgTreeVO().getCanViewDepSids();
        } else {
            conditionDepSids.addAll(queryVO.getOrgTreeVO().getRequireDepts());
        }

        // 不為全單位時，才下過濾條件
        if (!WkCommonUtils.compare(allDepSids, conditionDepSids)) {
            sql.append("                    AND dep_sid IN ( :deptSids ) ");
            parameters.put("deptSids", conditionDepSids);
        }

        sql.append("                    GROUP  BY require_sid) assignDep ");
        sql.append("       ON tr.require_sid = assignDep.require_sid ");

    }

    private List<OverdueUnfinishedVO> convert(
            List<OverdueUnfinishedVO> resultList,
            Integer loginUserSid) {
        for (OverdueUnfinishedVO vo : resultList) {

            try {
                // 公用欄位處理
                vo.prepareCommonColumn();

                vo.setCreateUserName(WkUserUtils.findNameBySid(vo.getCreateUserSid()));
                vo.setInteStaffUserName(WkUserUtils.findNameBySid(vo.getInteStaffUserSid()));
                vo.setDeptName(WkOrgCache.getInstance().findNameBySid(vo.getDepSid()));

                // theme
                if (!Strings.isNullOrEmpty(vo.getRequireTheme())) {
                    vo.setRequireTheme(String.join(" ", jsonUtils.fromJsonToList(vo.getRequireTheme(), String.class)));
                }
                // open windows link
                vo.setLocalUrlLink(urlService.createLoacalURLLink(
                        URLService.URLServiceAttr.URL_ATTR_M,
                        urlService.createSimpleUrlTo(loginUserSid, vo.getRequireNo(), 1)));

                // 分派單位
                List<String> orgSids = Lists.newArrayList(WkStringUtils.safeTrim(vo.getDeptSids()).split(","));
                vo.setDeptNames(WkOrgUtils.findNameBySidStrs(orgSids, "、"));

                // ====================================
                // REQ-1031
                // ====================================
                vo.setExecuteDays(
                        this.searchService.calcExecDays(
                                vo.getRequireStatus(),
                                vo.getRequireEstablishDate(),
                                vo.getRequireFinishDate()));

            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return resultList;
    }

}
