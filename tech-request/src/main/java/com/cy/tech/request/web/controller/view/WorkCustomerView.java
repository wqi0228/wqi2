/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 客戶資料前端下拉式選項用
 *
 * @author shaun
 */
@AllArgsConstructor
@Data
public class WorkCustomerView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6113420996371953227L;
    private Long sid;
    /** 後置碼 */
    private String loginCode;
    /** 廳主名稱 */
    private String name;
    /** 廳主別名 */
    private String alias;
    /** 客戶資料分類 */
    private String customerType;
}
