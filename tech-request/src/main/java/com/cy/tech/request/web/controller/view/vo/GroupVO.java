/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */
@EqualsAndHashCode(of = { "group_sid" })
public class GroupVO implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 1397173915677430887L;
    public GroupVO(List<OrgViewVO> orgViewVOs, String group_sid, String group_name, String status, String statusID, String note, String info) {
		this.orgViewVOs = orgViewVOs;
		this.group_sid = group_sid;
		this.group_name = group_name;
		this.status = status;
		this.statusID = statusID;
		this.note = note;
		this.info = info;

		if (orgViewVOs == null) {
			orgViewVOs = Lists.newArrayList();
		}
		depSids = Sets.newHashSet();
		for (OrgViewVO orgViewVO : orgViewVOs) {
			depSids.add(orgViewVO.getOrgSid());
		}
	}

	@Getter
	private List<OrgViewVO> orgViewVOs;
	@Getter
	private Set<Integer> depSids;
	@Getter
	private String group_sid;
	@Getter
	private String group_name;
	@Getter
	private String status;
	@Getter
	private String statusID;
	@Getter
	private String note;
	@Getter
	private String info;
}
