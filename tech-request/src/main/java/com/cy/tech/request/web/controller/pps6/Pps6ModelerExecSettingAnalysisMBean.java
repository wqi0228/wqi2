package com.cy.tech.request.web.controller.pps6;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.pps6.Pps6ModelerService;
import com.cy.tech.request.vo.pps6.Pps6ModelerExecSettingVO;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@NoArgsConstructor
@Controller
@Scope("view")
@Slf4j
public class Pps6ModelerExecSettingAnalysisMBean implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 8199243380516682492L;
    // ========================================================================
	//
	// ========================================================================
	@Autowired
	private transient Pps6ModelerService pps6ModelerService;
	@Autowired
	private transient DisplayController displayController;
	// ========================================================================
	// 頁面變數區
	// ========================================================================
	/**
	 * 上傳檔案暫存區
	 */
	@Getter
	private List<UploadedFile> uploadedFiles;

	/**
	 * 設定資料
	 */
	@Getter
	List<Pps6ModelerExecSettingVO> execSettingItems;

	@Getter
	private String dialogID = "modelerUploadeDialog";
	@Getter
	private String dialogContent = "modelerUploadeDialogContent";

	// ========================================================================
	// 方法區
	// ========================================================================
	/**
	 * 
	 */
	@PostConstruct
	public void init() {
		this.search();
	}

	/**
	 * 
	 */
	public void search() {
		// 查詢
		try {
			this.execSettingItems = this.pps6ModelerService.prepareAllData();
		} catch (UserMessageException e) {
			MessagesUtils.show(e);
			return;
		} catch (Exception e) {
			log.error("取得資料失敗!" + e.getMessage(), e);
			MessagesUtils.showError("取得資料失敗!" + e.getMessage());
			return;
		}
		// 更新顯示區域
		this.displayController.update("dataArea");
	}

	public void openDialog() {
		this.uploadedFiles = Lists.newArrayList();
		this.displayController.update(dialogContent);
		this.displayController.showPfWidgetVar(dialogID);
	}

	/**
	 * 
	 */
	public void process() {

		// ====================================
		// 檢查
		// ====================================
		if (WkStringUtils.isEmpty(uploadedFiles)) {
			MessagesUtils.showWarn("尚未上傳檔案");
			return;
		}

		// ====================================
		// 逐筆處理
		// ====================================
		List<String> processInfo = Lists.newArrayList();

		for (UploadedFile uploadedFile : uploadedFiles) {
			try {
				// 處理單筆
				String flowName = this.pps6ModelerService.processImport(
				        uploadedFile.getInputstream(),
				        SecurityFacade.getUserSid());

				processInfo.add("更新流程:[" + flowName + "] 成功!");
			} catch (Exception e) {
				String message = "分析檔案:[" + uploadedFile.getFileName() + "] 失敗! (" + e.getMessage() + ")";
				log.error(message, e);
			}
		}

		// ====================================
		// 處理結果
		// ====================================
		MessagesUtils.showInfo("處理完畢! 結果如下:<br/><br/>" + String.join("<br/>", processInfo));
	}

	/**
	 * 檔案上傳事件 (單檔觸發一次)
	 * 
	 * @param event
	 */
	public void handleFileUpload(FileUploadEvent event) {
		log.info("update :" + event.getFile().getFileName());
		
		try {
			String flowName = this.pps6ModelerService.processImport(
					event.getFile().getInputstream(),
			        SecurityFacade.getUserSid());
			
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("更新流程:[" + flowName + "] 成功!"));

		} catch (Exception e) {
			String message = "分析檔案:[" + event.getFile().getFileName() + "] 失敗! (" + e.getMessage() + ")";
			log.error(message, e);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(message));
		}
	}

}
