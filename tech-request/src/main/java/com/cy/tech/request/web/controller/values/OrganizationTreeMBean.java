/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.values;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgType;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.logic.WkUserAndOrgLogic;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.NodeUnselectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 組織樹
 *
 * @author shaun
 */
@Controller
@Scope("view")
@Slf4j
public class OrganizationTreeMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2086935201229677179L;
    @Autowired
    transient private OrganizationService orgService;
    @Autowired
    transient WkUserAndOrgLogic userAndOrgLogic;

    /**
     * 組織樹 - 全選
     */
    @Getter
    @Setter
    private boolean selectAllNode;

    /**
     * 組織樹 - 不包含停用部門
     */
    @Getter
    @Setter
    private boolean selectJustActive = true;

    /**
     * 組織樹 - 樹狀容器
     */
    @Getter
    @Setter
    transient private TreeNode orgTreeRoot;

    /**
     * 組織樹 - 選擇的樹
     */
    @Getter
    @Setter
    transient private TreeNode[] orgTreeSelect;

    private List<Org> allOrgs;

    // 需要註記部門
    private List<Org> highlightOrgs;

    private Org root;

    /** 需disable 單位 */
    @Setter
    private List<Org> disableOrgList;
    transient private List<TreeNode> disableTreeNode;

    @PostConstruct
    public void init() {
        highlightOrgs = Lists.newArrayList();
        selectAllNode = false;
        selectJustActive = true;
    }

    /**
     * 依組織的層級來初始根節點
     *
     * @param dept
     */
    public void initRootByDeptLevel(Integer userSid, boolean isAllDeps) {

        initAllOrgs();

        // ====================================
        // 查詢 user 資料
        // ====================================
        User user = WkUserCache.getInstance().findBySid(userSid);
        if (user == null || user.getPrimaryOrg() == null) {
            return;
        }
        // 公司資料
        Org comp = WkOrgCache.getInstance().findBySid(user.getPrimaryOrg().getCompanySid());
        if (comp == null) {
            log.warn("傳入使用者公司別為空:[{}-{}]", userSid, user.getId());
            return;
        }

        // ====================================
        // 重新生成全單位 (by公司)
        // ====================================
        this.orgTreeRoot = getOtherRoot(comp);

        // ====================================
        // 顯示全公司時，無需繼續往下處理
        // ====================================
        if (isAllDeps) {
            return;
        }

        // ====================================
        // 依據可閱權限，過濾無法挑選的單位
        // ====================================
        // 查詢可閱部門 (基礎到組)
        Set<Org> canViewDeps = this.userAndOrgLogic.prepareCanViewOrgsBaseOnPanel(userSid, true);

        // 計算要disable 的上層組織
        List<Org> disableDeps = Lists.newArrayList();
        for (Org canViewDep : canViewDeps) {
            getDisableParentOrg(
                    canViewDep,
                    Lists.newArrayList(canViewDeps),
                    disableDeps);
        }

        // 處理 disable 的單位
        filterOrg(
                orgTreeRoot,
                Lists.newArrayList(canViewDeps),
                disableDeps);
    }

    /**
     * 取得要disable的上層組織
     *
     * @param parent
     * @param collectionOrgs
     * @param disableOrgs
     */
    private void getDisableParentOrg(Org parent, List<Org> collectionOrgs, List<Org> disableOrgs) {
        if (parent == null) {
            return;
        }
        if (parent.getParent() != null && OrgType.DEPARTMENT.equals(parent.getParent().getType())
                && !collectionOrgs.contains(orgService.findBySid(parent.getParent().getSid()))) {
            disableOrgs.add(orgService.findBySid(parent.getParent().getSid()));
        }
        if (parent.getParent() == null) {
            return;
        }
        getDisableParentOrg(orgService.findBySid(parent.getParent().getSid()), collectionOrgs, disableOrgs);
    }

    /**
     * 取得另一個組織樹
     *
     * @param org
     */
    private TreeNode getOtherRoot(Org org) {
        Activation status = selectJustActive ? Activation.ACTIVE : null;
        initAllOrgs();
        TreeNode otherRoot = new DefaultTreeNode("root", null);
        for (Org o : allOrgs) {
            Org parent = null;
            if (o.getParent() != null) {
                parent = orgService.findBySid(o.getParent().getSid());
            }
            if (status == null) {
                if (parent != null && parent.equals(org)) {
                    TreeNode node = new DefaultTreeNode(o, otherRoot);
                    buildTree(node, allOrgs, status);
                }
            } else if (status.equals(o.getStatus()) && parent != null && parent.equals(org)) {
                TreeNode node = new DefaultTreeNode(o, otherRoot);
                buildTree(node, allOrgs, status);
            }
        }
        return otherRoot;
    }

    /**
     * 篩選組織-根據有權限的部門及要disable的上層組織來過濾完整的組織樹
     *
     * @param root
     * @param collectionOrgs
     * @param disableOrgs
     */
    private void filterOrg(TreeNode root, List<Org> collectionOrgs, List<Org> disableOrgs) {
        if (root.getChildren() == null || root.getChildren().isEmpty()) {
            return;
        }
        List<TreeNode> nodes = Lists.newArrayList();
        for (TreeNode node : root.getChildren()) {
            Org o = (Org) node.getData();
            if (disableOrgs.contains(o)) {
                node.setSelectable(false);
            }
            if (collectionOrgs.contains(o) || disableOrgs.contains(o)) {
                nodes.add(node);
            }
        }

        List<TreeNode> children = root.getChildren();
        children.clear();
        children.addAll(nodes);
        for (TreeNode node : root.getChildren()) {
            filterOrg(node, collectionOrgs, disableOrgs);
        }
    }

    /**
     * 初始所有組織
     */
    private void initAllOrgs() {
        if (allOrgs == null) {
            allOrgs = orgService.findAll();
        }
    }

    public void rebuildRootByDeptLevel(boolean buildByDeptLevel) {
        if (buildByDeptLevel) {
            this.initRootByDeptLevel(SecurityFacade.getUserSid(), false);
        } else {
            this.rebuildRoot();
        }
    }

    /**
     * 初始根節點
     *
     * @param org
     */
    public void initRoot(Org org) {
        this.initRoot(org, null);
    }

    /**
     * 初始根節點
     *
     * @param org
     */
    public void initRoot(Org org, List<Org> disableOrgList) {
        this.disableOrgList = disableOrgList;
        root = org;
//        if (orgTreeRoot != null) {
//            return;
//        }
        Activation status = selectJustActive ? Activation.ACTIVE : null;
        initAllOrgs();
        orgTreeRoot = new DefaultTreeNode("root", null);
        for (Org o : allOrgs) {
            Org parent = null;
            if (o.getParent() != null) {
                parent = orgService.findBySid(o.getParent().getSid());
            }
            if (status == null) {
                if (parent != null && parent.equals(org)) {
                    TreeNode node = new DefaultTreeNode(o, orgTreeRoot);
                    buildTree(node, allOrgs, status);
                }
            } else if (status.equals(o.getStatus()) && parent != null && parent.equals(org)) {
                TreeNode node = new DefaultTreeNode(o, orgTreeRoot);
                buildTree(node, allOrgs, status);
            }

        }
    }

    public void resetSelectAll() {
        if (orgTreeRoot == null) {
            return;
        }
        boolean hasAnyUnSelect = false;
        if (orgTreeRoot.getChildren() != null && !orgTreeRoot.getChildren().isEmpty()) {
            for (TreeNode node : orgTreeRoot.getChildren()) {
                if (this.checkNodeUnSelect(node)) {
                    hasAnyUnSelect = true;
                    break;
                }
            }
        }
        this.selectAllNode = !hasAnyUnSelect;
    }

    private boolean checkNodeUnSelect(TreeNode source) {
        if (!source.isSelected()) {
            return true;
        }
        if (source.getChildren() != null && !source.getChildren().isEmpty()) {
            for (TreeNode node : source.getChildren()) {
                return this.checkNodeUnSelect(node);
            }
        }
        return false;
    }

    /**
     * 重建根節點
     */
    public void rebuildRoot() {
        this.rebuildRoot(root, disableOrgList);
        this.markSelectedNode(Lists.newArrayList(), orgTreeRoot);
    }

    /**
     * 重建根節點
     *
     * @param root
     */
    public void rebuildRoot(Org root) {
        this.rebuildRoot(root, null);
    }

    /**
     * 重建根節點
     *
     * @param root
     * @param disableOrgList
     */
    public void rebuildRoot(Org root, List<Org> disableOrgList) {
        orgTreeRoot = null;
        this.initRoot(root, disableOrgList);
    }

    /**
     * 建置樹
     *
     * @param parentNode
     * @param allOrgs
     * @param status
     */
    private void buildTree(TreeNode parentNode, List<Org> allOrgs, Activation status) {
        Org parent = (Org) parentNode.getData();
        for (Org org : allOrgs) {
            if (status == null) {
                if (org.getParent() != null && org.getParent().getSid().equals(parent.getSid())) {
                    TreeNode node = new DefaultTreeNode(org, parentNode);
                    buildTree(node, allOrgs, status);
                }
            } else if (status.equals(org.getStatus()) && org.getParent() != null && org.getParent().getSid().equals(parent.getSid())) {
                TreeNode node = new DefaultTreeNode(org, parentNode);
                buildTree(node, allOrgs, status);
            }
        }
    }

    /**
     * 依查詢字串來查詢節點
     *
     * @param orgName
     * @param parent
     */
    public void searchNodeByText(String orgName, TreeNode parent) {
        highlightOrgs = Lists.newArrayList();
        unselAllNode(parent);
        List<TreeNode> selNodes = Lists.newArrayList();
        selectedNodeByText(orgName, parent, selNodes, true);
        TreeNode[] nodes = new TreeNode[selNodes.size()];
        orgTreeSelect = selNodes.toArray(nodes);
    }

    public void searchNodeByTextNoSelect(String orgName, TreeNode parent) {
        highlightOrgs = Lists.newArrayList();
        unselAllNode(parent);
        List<TreeNode> selNodes = Lists.newArrayList();
        selectedNodeByText(orgName, parent, selNodes, false);
        TreeNode[] nodes = new TreeNode[selNodes.size()];
        orgTreeSelect = selNodes.toArray(nodes);
    }

    /**
     * 依照部門名稱進行搜尋後，將查詢到資料highlight
     *
     * @param o
     * @return
     */
    public String treeSyle(Org o) {
        if (o != null && highlightOrgs != null && !highlightOrgs.isEmpty()
                && highlightOrgs.contains(o)) {
            return "ui-state-highlight";
        }
        return "";
    }

    /**
     * 往上展開父節點
     *
     * @param orgName
     * @param parent
     * @param selNodes
     */
    private void selectedNodeByText(String orgName, TreeNode parent, List<TreeNode> selNodes, boolean select) {
        for (TreeNode node : parent.getChildren()) {
            Org dep = (Org) node.getData();
            if (WkOrgUtils.getOrgName(dep).toLowerCase().contains(orgName.toLowerCase())) {
                if (!Strings.isNullOrEmpty(orgName)) {
                    this.expandedAboveNode(node.getParent());
                }
                node.setSelected(select);
                if (select) {
                    selNodes.add(node);
                }
                highlightOrgs.add(dep);
            }
            this.disabledDep(node, dep);
            if (!selNodes.contains(node) && node.isSelected()) {
                selNodes.add(node);
            }
            selectedNodeByText(orgName, node, selNodes, select);
        }
    }

    private void expandedAboveNode(TreeNode node) {
        if (node == null) {
            return;
        }
        node.setExpanded(true);
        if (node.getParent() != null) {
            this.expandedAboveNode(node.getParent());
        }
    }

    /**
     * 不選取所有節點
     *
     * @param parent
     */
    private void unselAllNode(TreeNode parent) {
        for (TreeNode node : parent.getChildren()) {
            if (node.getParent() != null) {
                node.getParent().setExpanded(false);
            }
            node.setSelected(false);
            unselAllNode(node);
        }
    }

    /**
     * 標註已選取的節點(依已選擇的組織sid)
     *
     * @param selOrgSids
     * @param parent
     */
    public void markSelectedNode(List<String> selOrgSids, TreeNode parent) {
        highlightOrgs = Lists.newArrayList();
        unselAllNode(parent);
        selectedNodeBySelOrgSids(selOrgSids, parent);
    }

    private void selectedNodeBySelOrgSids(List<String> selOrgSids, TreeNode parent) {
        for (TreeNode node : parent.getChildren()) {
            String orgSid = String.valueOf(((Org) node.getData()).getSid());
            if (selOrgSids.contains(orgSid)) {
                // this.expandedAboveNode(node.getParent());
                node.setSelected(true);
            }
            this.disabledDep(node, (Org) node.getData());
            markSelectedNode(selOrgSids, node);
        }
    }

    /**
     * 樹狀圖上
     *
     * @return
     */
    public List<Activation> getStatusValue() {
        if (selectJustActive) {
            return Lists.newArrayList(Activation.ACTIVE);
        }
        return Lists.newArrayList(Activation.values());
    }

    /**
     * 全選 - AJAX
     */
    public void actionSelectALL() {
        List<TreeNode> selNodes = Lists.newArrayList();
        disableTreeNode = Lists.newArrayList();
        if (selectAllNode) {
            this.selectAllNodeStatus(orgTreeRoot.getChildren(), selNodes, true);
            TreeNode[] nodes = new TreeNode[selNodes.size()];
            orgTreeSelect = selNodes.toArray(nodes);
            return;
        }
        // 全關
        this.selectAllNodeStatus(orgTreeRoot.getChildren(), selNodes, false);
        if (disableTreeNode.isEmpty()) {
            orgTreeSelect = null;
        } else {
            TreeNode[] nodes = new TreeNode[disableTreeNode.size()];
            orgTreeSelect = disableTreeNode.toArray(nodes);
        }
    }

    /**
     * 擇選全部節點
     *
     * @param childsNode
     */
    private void selectAllNodeStatus(List<TreeNode> childsNode, List<TreeNode> selNodes, boolean status) {
        if (childsNode == null) {
            return;
        }
        for (TreeNode node : childsNode) {
            node.setSelected(status);
            selNodes.add(node);
            this.disabledDep(node, (Org) node.getData());
            if (!disableTreeNode.contains(node) && node.isSelected()) {
                disableTreeNode.add(node);
            }
            this.selectAllNodeStatus(node.getChildren(), selNodes, status);
        }
    }

    public void onNodeExpand(NodeExpandEvent event) {
        event.getTreeNode().setExpanded(true);
    }

    public void onNodeCollapse(NodeCollapseEvent event) {
        event.getTreeNode().setExpanded(false);
    }

    public void onNodeSelect(NodeSelectEvent event) {
        if (event.getTreeNode().getData() != null) {
            Org selectOrg = (Org) event.getTreeNode().getData();
            if (highlightOrgs != null && !highlightOrgs.contains(selectOrg)) {
                highlightOrgs.add(selectOrg);
            }
        }
    }

    public void onNodeUnselect(NodeUnselectEvent event) {
        if (event.getTreeNode().getData() != null) {
            Org unselectOrg = (Org) event.getTreeNode().getData();
            if (highlightOrgs != null && highlightOrgs.contains(unselectOrg)) {
                highlightOrgs.remove(unselectOrg);
            }
        }
    }

    public void disableNode(List<Org> org) {
        this.disable(orgTreeRoot, org);
    }

    private void disable(TreeNode node, List<Org> disableOrgs) {
        if (node.getData() != null && node.getData() instanceof Org) {
            if (disableOrgs.contains((Org) node.getData())) {
                node.setSelectable(false);
            }
        }
        if (node.getChildren() == null) {
            return;
        }
        for (TreeNode each : node.getChildren()) {
            this.disable(each, disableOrgs);
        }
    }

    /**
     * Disabled部門
     */
    private void disabledDep(TreeNode node, Org o) {
        if (this.checkContainsDisabledDep(o)) {
            node.setSelectable(false);
            node.setSelected(true);
        }
    }

    /**
     * 判斷是否有在預設Disabled部門中
     *
     * @param o
     */
    private boolean checkContainsDisabledDep(Org o) {
        if (disableOrgList == null) {
            disableOrgList = Lists.newArrayList();
        }
        return disableOrgList.contains(o);
    }

    /**
     * 選取預設部門(需調整，在建立組織樹時就可以先執行預設動作)
     *
     * @param defaultOrgs
     * @param disabledOrgs
     * @param parent
     */
    public void selectDefaultOrgs(List<Org> defaultOrgs, List<Org> disabledOrgs, TreeNode parent) {
        this.disableOrgList = disabledOrgs;
        highlightOrgs = Lists.newArrayList();
        this.unselAllNode(parent);
        List<TreeNode> selNodes = Lists.newArrayList();
        this.selectedNodeByDefaultOrgs(defaultOrgs, parent, selNodes, true);
        TreeNode[] nodes = new TreeNode[selNodes.size()];
        orgTreeSelect = selNodes.toArray(nodes);
    }

    /**
     * 往上展開父節點
     *
     * @param orgName
     * @param parent
     * @param selNodes
     */
    private void selectedNodeByDefaultOrgs(List<Org> defaultOrgs, TreeNode parent, List<TreeNode> selNodes, boolean select) {
        for (TreeNode node : parent.getChildren()) {
            Org o = (Org) node.getData();
            if (defaultOrgs.contains(o)) {
                this.expandedAboveNode(node.getParent());
                node.setSelected(select);
                if (select) {
                    selNodes.add(node);
                }
                highlightOrgs.add(o);
            }
            this.disabledDep(node, o);
            if (!selNodes.contains(node) && node.isSelected()) {
                selNodes.add(node);
            }
            this.selectedNodeByDefaultOrgs(defaultOrgs, node, selNodes, select);
        }
    }
}
