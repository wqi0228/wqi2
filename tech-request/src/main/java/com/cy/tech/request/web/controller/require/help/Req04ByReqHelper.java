package com.cy.tech.request.web.controller.require.help;

import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.group.vo.WorkLinkGroupDetail;
import com.cy.work.group.vo.to.GroupDetailListTo;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author kasim
 */
@Component
public class Req04ByReqHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3960133393436119000L;
    @Autowired
    transient private DisplayController displayController;

    /**
     * dataTable選擇事件
     *
     * @param dtId
     * @param listTo
     */
    public void onRowSelect(GroupDetailListTo listTo, String url) {
        this.hideIssue(listTo);
        displayController.execute("changeIframeUrl('" + url + "');");
    }

    /**
     * 開啟分頁
     *
     * @param to
     */
    public void btnOpenUrl(WorkLinkGroupDetail to, GroupDetailListTo listTo) {
        this.highlightReportTo(to, listTo);
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param to
     * @param listTo
     */
    private void highlightReportTo(WorkLinkGroupDetail to, GroupDetailListTo listTo) {
        listTo.setSelectedByReq(to);
        this.hideIssue(listTo);
        displayController.execute("selectRow('" + listTo.getWidgetVar_req() + "'," + this.getRowIndex(listTo) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param listTo
     * @return
     */
    private int getRowIndex(GroupDetailListTo listTo) {
        return listTo.getGroupDetailsByReq().indexOf(listTo.getSelectedByReq()) % 50;
    }

    private void hideIssue(GroupDetailListTo listTo) {
        listTo.setSelectedByIssue(null);
        displayController.execute(" PF('" + listTo.getWidgetVar_issue() + "').unselectAllRows();");
    }
}
