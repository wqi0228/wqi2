/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.component;

import com.cy.commons.enums.Activation;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.CategorySettingService;
import com.cy.tech.request.logic.vo.CateNodeTo;
import com.cy.tech.request.vo.category.BasicDataBigCategory;
import com.cy.tech.request.vo.category.BasicDataMiddleCategory;
import com.cy.tech.request.vo.category.BasicDataSmallCategory;
import com.cy.tech.request.vo.constants.ReqPermission;
import com.cy.tech.request.web.listener.CategoryTreeCallBack;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.collections.CollectionUtils;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.NodeUnselectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 * 類別樹 Component
 *
 * @author kasim
 */
@Slf4j
public class CategoryTreeComponent implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2785503839339938884L;

    private CategorySettingService cateSettingService;

    /** 類別樹 CallBack */
    private CategoryTreeCallBack categoryTreeCallBack;

    @Getter
    @Setter
    /** 類別樹 */
    private TreeNode cateRoot;
    @Getter
    @Setter
    /** 選擇的類別節點 */
    private TreeNode[] selCateNodes;
    @Getter
    @Setter
    /** 選擇的類別節點(單選) */
    private TreeNode singleSelectedNode;
    @Getter
    @Setter
    /** 類別樹搜尋字串 */
    private String cateText;
    @Getter
    @Setter
    /** 是否全選 */
    private Boolean isAllSel;
    /** 註記類別 */
    @Getter
    @Setter
    private List<String> highlightCategory = Lists.newArrayList();

    /** 已選擇大類 sid */
    private List<String> bigDataCateSids = Lists.newArrayList();
    /** 已選擇中類 sid */
    private List<String> middleDataCateSids = Lists.newArrayList();
    /** 已選擇大小類 sid */
    private List<String> smallDataCateSids = Lists.newArrayList();

    /**
     * 建構式
     *
     * @param categoryTreeCallBack
     */
    public CategoryTreeComponent(CategoryTreeCallBack categoryTreeCallBack) {
        this.cateSettingService = WorkSpringContextHolder.getBean(CategorySettingService.class);
        this.categoryTreeCallBack = categoryTreeCallBack;
    }

    /**
     * 初始根節點
     */
    public void init() {
        if (cateRoot != null) {
            return;
        }
        cateRoot = new DefaultTreeNode("root", null);

        Comparator<BasicDataBigCategory> comparator = Comparator.comparing(
                BasicDataBigCategory::getStatus, Comparator.nullsLast(Comparator.naturalOrder()));
        comparator = comparator.thenComparing(Comparator.comparing(
                BasicDataBigCategory::getId, Comparator.nullsLast(Comparator.naturalOrder())));

        Comparator<BasicDataMiddleCategory> comparatorForMiddle = Comparator.comparing(
                BasicDataMiddleCategory::getStatus, Comparator.nullsLast(Comparator.naturalOrder()));
        comparatorForMiddle = comparatorForMiddle.thenComparing(Comparator.comparing(
                BasicDataMiddleCategory::getId, Comparator.nullsLast(Comparator.naturalOrder())));

        Comparator<BasicDataSmallCategory> comparatorForSmall = Comparator.comparing(
                BasicDataSmallCategory::getStatus, Comparator.nullsLast(Comparator.naturalOrder()));
        comparatorForSmall = comparatorForSmall.thenComparing(Comparator.comparing(
                BasicDataSmallCategory::getId, Comparator.nullsLast(Comparator.naturalOrder())));

        // 存放所有的大類節點
        List<TreeNode> bigNodes = Lists.newArrayList();
        List<BasicDataBigCategory> bigCates = cateSettingService.findAllBigWithInactive().stream()
                .filter(big -> WkStringUtils.notEmpty(big.getChildren())) // 移除沒有中類的大類
                .sorted(comparator)
                .collect(Collectors.toList());

        // 為需求單系統管理員
        boolean isSysAdmin = WkUserUtils.isUserHasRole(
                SecurityFacade.getUserSid(),
                SecurityFacade.getCompanyId(),
                ReqPermission.ROLE_SYS_ADMIN);

        for (BasicDataBigCategory big : bigCates) {

            // ====================================
            // 排除無子類的項目
            // ====================================
            boolean isHasSmall = false;
            for (BasicDataMiddleCategory middle : big.getChildren()) {
                if (WkStringUtils.notEmpty(middle.getChildren())) {
                    isHasSmall = true;
                    break;
                }
            }

            if (!isHasSmall) {
                continue;
            }

            // ====================================
            // 大類
            // ====================================
            String name = big.getName();
            if (Activation.INACTIVE.equals(big.getStatus())) {
                name = WkHtmlUtils.addStrikethroughStyle(name, "停用");
            }

            TreeNode bigNode = new DefaultTreeNode(
                    new CateNodeTo(big.getSid(), name, big.getClass(), big.getId()), cateRoot);
            bigNode.setExpanded(true);
            bigNodes.add(bigNode);

            // ====================================
            // 中類
            // ====================================
            List<BasicDataMiddleCategory> middles = big.getChildren().stream()
                    .filter(middle -> WkStringUtils.notEmpty(middle.getChildren())) // 移除沒有小類的中類
                    .sorted(comparatorForMiddle) // 排序
                    .collect(Collectors.toList());

            for (BasicDataMiddleCategory middle : middles) {

                // 非系統管理員不顯示E化測試
                if ("E化測試".equals(middle.getName()) && !isSysAdmin) {
                    continue;
                }

                String middleName = middle.getName();
                if (Activation.INACTIVE.equals(middle.getStatus())) {
                    middleName = WkHtmlUtils.addStrikethroughStyle(middleName, "停用");
                }

                TreeNode middleNode = new DefaultTreeNode(
                        new CateNodeTo(middle.getSid(), middleName, middle.getClass(), middle.getId()), bigNode);

                List<BasicDataSmallCategory> smalls = middle.getChildren().stream()
                        .sorted(comparatorForSmall) // 排序
                        .collect(Collectors.toList());

                for (BasicDataSmallCategory small : smalls) {

                    String smallName = small.getName();
                    if (Activation.INACTIVE.equals(small.getStatus())) {
                        smallName = WkHtmlUtils.addStrikethroughStyle(smallName, "停用");
                    }

                    new DefaultTreeNode(
                            new CateNodeTo(small.getSid(), smallName, small.getClass(), small.getId()), middleNode);
                }

            }

        }

        // this.buildMiddleNode(bigCates, bigNodes);
    }

    /**
     * 依查詢字串搜尋節點
     */
    public void searchNodeByText() {
        try {
            if (Strings.isNullOrEmpty(cateText)) {
                this.unselAllNode(cateRoot);
                selCateNodes = null;
                this.expendToBig(cateRoot);
                return;
            }
            List<TreeNode> selNodes = Lists.newArrayList();
            this.markByText(cateText, cateRoot, selNodes);
            TreeNode[] nodes = new TreeNode[selNodes.size()];
            selCateNodes = selNodes.toArray(nodes);
        } catch (Exception e) {
            log.error("searchNodeByText Error", e);
            categoryTreeCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 類別查詢, focus 第一筆
     */
    public void searchSingleNodeByText() {
        try {
            this.unselAllNode(cateRoot);
            if (Strings.isNullOrEmpty(cateText)) {
                selCateNodes = null;
                this.expendToBig(cateRoot);
                return;
            }
            List<TreeNode> selNodes = Lists.newArrayList();
            this.markSmallCategoryByFuzzyText(cateText, cateRoot, selNodes);
            if (CollectionUtils.isNotEmpty(selNodes)) {
                selCateNodes = new TreeNode[] { selNodes.get(0) };
                singleSelectedNode = selNodes.get(0);
            }
        } catch (Exception e) {
            log.error("searchNodeByText Error", e);
            categoryTreeCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 未選取所有的節點
     *
     * @param parent
     */
    private void unselAllNode(TreeNode parent) {
        parent.getChildren().forEach(node -> {
            if (node.getParent() != null) {
                node.getParent().setExpanded(false);
            }
            node.setSelected(false);
            this.unselAllNode(node);
        });
    }

    /**
     * 展開類別樹
     *
     * @param parent
     */
    private void expendToBig(TreeNode parent) {
        parent.getChildren().forEach(big -> {
            big.setSelected(false);
            big.setExpanded(true);
            big.getChildren().forEach(middle -> {
                middle.setSelected(false);
                middle.setExpanded(false);
                middle.getChildren().forEach(small -> {
                    small.setSelected(false);
                    small.setExpanded(false);
                });
            });
        });
    }

    /**
     * 依查詢字串標註相關的類別
     *
     * @param text
     * @param parent
     * @param selNodes
     */
    private void markByText(String text, TreeNode parent, List<TreeNode> selNodes) {
        parent.getChildren().forEach(node -> {
            CateNodeTo cate = (CateNodeTo) node.getData();
            if (cate.getName().toLowerCase().contains(text.toLowerCase())) {
                this.expandedAboveNode(node.getParent());
                node.setSelected(true);
                selNodes.add(node);
            }
            this.markByText(text, node, selNodes);
        });
    }

    /**
     * 依查詢字串標註相關小類 (單選)
     * a.只標註第一筆小類
     * b.將搜尋符合項目展開
     * c.只搜尋小類
     * 
     * @param text
     * @param parent
     * @param selNodes
     */
    private void markSmallCategoryByFuzzyText(String text, TreeNode parent, List<TreeNode> selNodes) {
        for (TreeNode node : parent.getChildren()) {
            CateNodeTo cate = (CateNodeTo) node.getData();
            if (cate.getName().toLowerCase().contains(text.toLowerCase())) {
                this.expandedAboveNode(node.getParent());
                // 指選取第一筆 符合的小類
                if (BasicDataSmallCategory.class.equals(cate.getCateClz()) && CollectionUtils.isEmpty(selNodes)) {
                    node.setSelected(true);
                    selNodes.add(node);
                    break;
                }
            }
            this.markSmallCategoryByFuzzyText(text, node, selNodes);
        }
    }

    /**
     * 展開類別樹
     *
     * @param node
     */
    private void expandedAboveNode(TreeNode node) {
        if (node == null) {
            return;
        }
        node.setExpanded(true);
        if (node.getParent() != null) {
            this.expandedAboveNode(node.getParent());
        }
    }

    /**
     * 全選 - AJAX
     */
    public void actionSelectALL() {
        try {
            List<TreeNode> selNodes = Lists.newArrayList();
            if (isAllSel) {
                this.selectAllNodeStatus(cateRoot.getChildren(), selNodes, true);
                TreeNode[] nodes = new TreeNode[selNodes.size()];
                selCateNodes = selNodes.toArray(nodes);
            } else {
                // 全關
                this.selectAllNodeStatus(cateRoot.getChildren(), selNodes, false);
                selCateNodes = null;
            }
            this.categoryTreeCallBack.actionSelectAll();
        } catch (Exception e) {
            log.error("actionSelectALL Error", e);
            categoryTreeCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 選擇全部節點
     *
     * @param childsNode
     * @param selNodes
     * @param status
     */
    private void selectAllNodeStatus(List<TreeNode> childsNode, List<TreeNode> selNodes, boolean status) {
        if (childsNode == null) {
            return;
        }
        childsNode.forEach(node -> {
            node.setSelected(status);
            selNodes.add(node);
            this.selectAllNodeStatus(node.getChildren(), selNodes, status);
        });
    }

    /**
     * 確認選擇類別
     */
    public void confirmSelCate() {
        try {
            this.categoryTreeCallBack.confirmSelCate();
        } catch (Exception e) {
            log.error("confirmSelCate Error", e);
            categoryTreeCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 確認選擇類別
     */
    public void selCate() {
        try {
            this.clearCateSids();
            for (TreeNode n : selCateNodes) {
                CateNodeTo cate = (CateNodeTo) n.getData();
                if (cate.getCateClz().getName().equals(BasicDataBigCategory.class.getName())) {
                    bigDataCateSids.add(cate.getSid());
                } else if (cate.getCateClz().getName().equals(BasicDataMiddleCategory.class.getName())) {
                    middleDataCateSids.add(cate.getSid());
                } else if (cate.getCateClz().getName().equals(BasicDataSmallCategory.class.getName())) {
                    smallDataCateSids.add(cate.getSid());
                }
            }
        } catch (Exception e) {
            log.error("confirmSelCate Error", e);
            categoryTreeCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 清除
     */
    public void clearCateSids() {
        bigDataCateSids.clear();
        middleDataCateSids.clear();
        smallDataCateSids.clear();
    }

    /**
     * 清除類別組合
     */
    public void clearCate() {
        try {
            if (cateRoot == null) {
                return;
            }
            this.expendToBig(cateRoot);
            selCateNodes = null;
            isAllSel = Boolean.FALSE;
            cateText = "";
            this.clearCateSids();
        } catch (Exception e) {
            log.error("clearCate Error", e);
            categoryTreeCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 取得所有小類 選項 sid
     */
    public List<String> getItemSidsByAllSmall() {
        try {
            List<TreeNode> treeNode = Lists.newArrayList();
            this.allNode(treeNode, cateRoot);
            return treeNode.stream()
                    .map(each -> (CateNodeTo) each.getData())
                    .filter(cate -> BasicDataSmallCategory.class.getName().equals(cate.getCateClz().getName()))
                    .map(obj -> obj.getSid())
                    .collect(Collectors.toList());
        } catch (Exception e) {
            log.error("getItemSidsBySmall Error", e);
            categoryTreeCallBack.showMessage(e.getMessage());
        }
        return Lists.newArrayList();
    }

    /**
     * 將所有樹狀節點加入list中
     *
     * @param list
     * @param root
     */
    private void allNode(List<TreeNode> list, TreeNode root) {
        if (root == null || root.getChildren() == null) {
            return;
        }
        root.getChildren().forEach(each -> {
            list.add(each);
            this.allNode(list, each);
        });
    }

    /**
     * 預設選取選項
     *
     * @param cateSids
     */
    public void selectedItem(List<String> cateSids) {
        this.isAllSel = Boolean.FALSE;
        List<TreeNode> selNodes = Lists.newArrayList();
        cateRoot.getChildren().forEach(each -> {
            each.setSelected(false);
            this.selectedItem(each, cateSids, selNodes);
        });
        List<TreeNode> treeNode = Lists.newArrayList();
        this.allNode(treeNode, cateRoot);
        isAllSel = treeNode.stream()
                .allMatch(each -> each.isSelected());
        TreeNode[] nodes = new TreeNode[selNodes.size()];
        selCateNodes = selNodes.toArray(nodes);
    }

    /**
     * 預設選取選項
     *
     * @param root
     * @param cateSids
     * @param selNodes
     */
    private void selectedItem(TreeNode root, List<String> cateSids, List<TreeNode> selNodes) {
        if (root == null || root.getChildren() == null || root.getChildren().isEmpty()) {
            return;
        }
        int i = 0;
        for (TreeNode each : root.getChildren()) {
            CateNodeTo cate = (CateNodeTo) each.getData();
            if (cateSids.contains(cate.getSid())) {
                each.setSelected(true);
                selNodes.add(each);
            } else {
                each.setSelected(false);
            }
            this.selectedItem(each, cateSids, selNodes);
            if (each.isSelected()) {
                i++;
            }
        }
        if (i > 0 && root.getChildCount() == i) {
            root.setSelected(true);
            selNodes.add(root);
        }
    }

    public List<String> getBigDataCateSids() { return Lists.newArrayList(bigDataCateSids); }

    public List<String> getMiddleDataCateSids() { return Lists.newArrayList(middleDataCateSids); }

    public List<String> getSmallDataCateSids() { return Lists.newArrayList(smallDataCateSids); }

    /**
     * 移除特定選取資料
     *
     * @param cateName
     * @param isSelectable
     */
    public void removeSelByCategoryName(String cateName, Boolean isSelectable) {
        List<TreeNode> selNods = Lists.newArrayList();
        cateRoot.getChildren().forEach(each -> {
            String bigCateName = ((CateNodeTo) each.getData()).getName();
            if ((cateName.equals(bigCateName) && isSelectable)
                    || (!cateName.equals(bigCateName) && each.isSelected())) {
                selNods.add(each);
            }
            this.removeSelByCategoryName(cateName, isSelectable, selNods, each, bigCateName);
        });
        TreeNode[] nodes = new TreeNode[selNods.size()];
        selCateNodes = selNods.toArray(nodes);
    }

    /**
     * 移除特定選取資料
     *
     * @param cateName
     * @param isSelectable
     * @param selNods
     * @param node
     * @param bigCateName
     */
    private void removeSelByCategoryName(String cateName, Boolean isSelectable,
            List<TreeNode> selNods, TreeNode node, String bigCateName) {
        List<TreeNode> treeNode = Lists.newArrayList();
        this.allNode(treeNode, node);
        treeNode.forEach(each -> {
            if ((cateName.equals(bigCateName) && isSelectable)
                    || (!cateName.equals(bigCateName) && each.isSelected())) {
                selNods.add(each);
            }
        });
    }

    public void onNodeExpand(NodeExpandEvent event) {
        event.getTreeNode().setExpanded(true);
    }

    public void onNodeCollapse(NodeCollapseEvent event) {
        event.getTreeNode().setExpanded(false);
    }

    /**
     * 勾選
     *
     * @param event
     */
    public void onNodeSelect(NodeSelectEvent event) {
        try {
            List<TreeNode> treeNode = Lists.newArrayList();
            this.allNode(treeNode, cateRoot);
            isAllSel = treeNode.stream()
                    .allMatch(each -> each.isSelected());
            this.categoryTreeCallBack.onNodeSelect();
        } catch (Exception e) {
            log.error("onNodeSelect Error", e);
            categoryTreeCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 勾選
     *
     * @param event
     */
    public void onSingleNodeSelect(NodeSelectEvent event) {
        try {
            this.singleSelectedNode = event.getTreeNode();
        } catch (Exception e) {
            log.error("onNodeSelect Error", e);
            categoryTreeCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 取選勾選
     *
     * @param event
     */
    public void onNodeUnselect(NodeUnselectEvent event) {
        try {
            if (isAllSel) {
                isAllSel = Boolean.FALSE;
            }
            this.categoryTreeCallBack.onNodeUnSelect();
        } catch (Exception e) {
            log.error("onNodeUnselect Error", e);
            categoryTreeCallBack.showMessage(e.getMessage());
        }
    }

    public boolean checkSelByCategoryName(String cateName) {
        if (selCateNodes == null) {
            return false;
        }
        List<TreeNode> nodes = Arrays.asList(selCateNodes);
        return nodes.stream()
                .anyMatch(each -> this.checkSelByCategoryName(cateName, each));
    }

    private boolean checkSelByCategoryName(String cateName, TreeNode node) {
        if (!(node.getData() instanceof CateNodeTo)) {
            return false;
        }
        if (cateName.equals(((CateNodeTo) node.getData()).getName())) {
            return true;
        }
        if (node.getParent() != null) {
            return this.checkSelByCategoryName(cateName, node.getParent());
        }
        return false;
    }

    /**
     * 依照部門名稱進行搜尋後，將查詢到資料highlight
     *
     * @param o
     * @return
     */
    public boolean highlight(String sid) {
        return highlightCategory.contains(sid);
    }
}
