package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.Date;
import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.repository.result.SabaQueryResult;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.search.helper.SabaHelper;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.view.vo.SabaQueryVO;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.pf.utils.DisplayController;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Scope("view")
@Controller
@Slf4j
public class SabaMBean extends PaginationMBean<SabaQueryResult> implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4359483385538194485L;

    @Autowired
    transient private SabaHelper sabaHelper;

    @Autowired
    transient private TableUpDownBean upDownBean;

    @Autowired
    transient private DisplayController display;

    @Autowired
    transient private LoginBean loginBean;

    @Autowired
    transient private ReqLoadBean loadManager;

    @Autowired
    transient private RequireService requireService;

    @Autowired
    transient private SearchHelper searchHelper;

    @Autowired
    @Getter
    @Setter
    private SabaQueryVO queryVO;

    /** 切換模式 */
    @Getter
    private SwitchType switchFullType = SwitchType.CONTENT;

    /**
     * 顯示dataTable裡的元件
     */
    @Getter
    @Setter
    private boolean displayButton = true;

    @PostConstruct
    public void init() {
        queryVO.setUser(loginBean.getUser());
        this.query();
    }

    /**
     * 查詢
     */
    public void query() {
        queryItems = sabaHelper.findSabaByCondition(queryVO);
    }

    /**
     * 清除
     */
    public void clear() {
        queryVO.init();
        queryVO.getCategoryCombineVO().init();
        queryVO.getAdvancedVO().init();
        this.query();
    }

    /**
     * 點選NCS確認完成紐
     */
    public void ncsAction(String sid) {
        try {
            sabaHelper.saveRequieTrace(sid, queryVO.getUser());
            this.query();
            display.execute("doEditContentToUpdate('" + sid + "')");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * require_title_btn_inner.xhtml頁面點選NCS確認完成紐
     */
    public void contentNcsAction(Require require) {
        require.setIsNcsDone(true);
        this.ncsAction(require.getSid());
    }

    /**
     * 匯出時，隱藏dataTable按鈕元件
     */
    public void disableButton() {
        displayButton = false;
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        Date startDate = queryVO.getDateIntervalVO().getStartDate();
        Date endDate = queryVO.getDateIntervalVO().getEndDate();
        ReportType reportType = queryVO.getReportType();
        searchHelper.exportExcel(document, startDate, endDate, reportType);
        displayButton = true;
    }

    /**
     * 點選dataTable 主題 link
     */
    public void showDetail(SabaQueryResult selectedResult) {
        querySelection = selectedResult;
        queryKeeper = selectedResult;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();

    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        this.changeRequireContent(queryKeeper);
    }

    /**
     * 需求單明細，切換到data list require03_title_btn.xhtml, 共同元件
     */
    public void normalScreenReport() {
        querySelection = queryKeeper;
        switchFullType = SwitchType.CONTENT;
        this.toggleSearchBody();
    }

    @Override
    protected void changeRequireContent(SabaQueryResult view) {
        Require r = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(r, loginBean.getUser());
    }

    @Override
    protected void highlightReportTo(String widgetVar, String pageCount, SabaQueryResult selectedResult) {
        selectedResult.setReadRecord("已閱讀");
        querySelection = selectedResult;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + super.getRowIndex(pageCount) + ");");

    }

    @Override
    protected void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
        display.update("searchBody");
    }

    /**
     * refresh dataTable
     */
    public void refreshDataTable() {
        query();
        display.update("dtRequire");
    }
}
