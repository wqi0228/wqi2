/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.home;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.Home06QueryService;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.Home06View;
import com.cy.tech.request.logic.service.AlertInboxService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.URLService.URLServiceAttr;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.vo.enums.InboxType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.homeheader.Home06HeaderMBean;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.require.RequireForwardDeAndPersonMBean;
import com.cy.tech.request.web.controller.search.TableUpDownBean;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.searchheader.CommonHeaderMBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.logic.lib.manager.WorkCommonReadRecordManager;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.group.vo.WorkLinkGroup;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 收指示
 *
 * @author kasim
 */
@Controller
@Scope("view")
@Slf4j
public class Home06MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3898812808717279744L;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private Home06HeaderMBean headerMBean;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private AlertInboxService inboxService;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private URLService urlService;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private Require01MBean r01MBean;
    @Autowired
    transient private Home06QueryService home06QueryService;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchResultHelper searchResultHelper;
    @Autowired
    transient private WorkCommonReadRecordManager workCommonReadRecordManager;

    /** 所有的收件資訊 */
    @Getter
    @Setter
    private List<Home06View> queryItems;
    /** 選擇的收件資訊 */
    @Getter
    @Setter
    private Home06View querySelection;
    /** 上下筆移動keeper */
    @Getter
    private Home06View queryKeeper;
    /** 切換模式 */
    @Getter
    private SwitchType switchType = SwitchType.CONTENT;
    /** 切換模式 - 全畫面狀態 */
    @Getter
    private SwitchType switchFullType = SwitchType.DETAIL;
    /** 在匯出的時候，某些內容需要隱藏 */
    @Getter
    private boolean hasDisplay = true;
    @Getter
    private final String dataTableId = "dtRequire";

    @PostConstruct
    public void init() {
        headerMBean.clear();
        this.search();
    }

    public void search() {
        queryItems = this.findWithQuery();
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        this.init();
    }

    private List<Home06View> findWithQuery() {
        String requireNo = reqularUtils.getRequireNo(loginBean.getCompanyId(), commHeaderMBean.getFuzzyText());
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT "
                + "ai.alert_sid,"
                + "tr.require_no,"
                + "tid.field_content,"
                + "tr.urgency,"
                + "tr.has_forward_dep,"
                + "tr.has_forward_member,"
                + "tr.has_link,"
                + "ai.alert_group_sid,"
                + "ai.sender_dep,"
                + "ai.sender,"
                + "ai.create_dt,"
                + "ckm.big_category_name,"
                + "tr.update_dt AS requireUpdatedDate,"
                + this.builderHasTraceColumn() + ", "
                // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
                + this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire()
                + " FROM ");
        buildInboxCondition(requireNo, builder, parameters);
        buildRequireCondition(requireNo, builder, parameters);
        buildRequireIndexDictionaryCondition(requireNo, builder, parameters);
        buildCategoryKeyMappingCondition(requireNo, builder, parameters);
        // 檢查項目 (系統別)
        // 後方需對 主單 sid 做 group by
        builder.append(this.searchConditionSqlHelper.prepareCommonJoin(SecurityFacade.getUserSid()));

        builder.append("WHERE ai.require_sid IS NOT NULL ");

        // 查詢條件：是否閱讀
        if (WkStringUtils.isEmpty(requireNo)) {
            builder.append(
                    this.workCommonReadRecordManager.prepareWhereConditionSQL(
                            headerMBean.getSelectReadRecordType(), "readRecord"));
        }

        builder.append(" GROUP BY tr.require_sid ");
        builder.append("  ORDER BY tr.update_dt DESC");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.INBOX_INSTRUCTION, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.INBOX_INSTRUCTION, SecurityFacade.getUserSid());

        // 查詢
        List<Home06View> resultList = home06QueryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // ====================================
        // 後續處理
        // ====================================
        if (WkStringUtils.notEmpty(resultList)) {
            // 後續處理-開始
            usageRecord.afterProcessStart();
            // 查詢條件過濾：系統別
            resultList = resultList.stream()
                    .filter(each -> this.searchResultHelper.filterCheckItems(
                            each.getCheckItemTypes(),
                            commHeaderMBean.getCheckItemTypes(),
                            false,
                            null))
                    .collect(Collectors.toList());
            // 後續處理-結束
            usageRecord.afterProcessEnd();
        }

        // 使用記錄
        usageRecord.saveUsageRecord();

        return resultList;
    }

    private void buildInboxCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("(SELECT * FROM tr_alert_inbox ai  WHERE ai.status = 0 ");

        // 收件類型 - 收部門轉發
        builder.append(" AND ai.inbox_type = :inboxType");
        parameters.put("inboxType", InboxType.INCOME_INSTRUCTION.name());

        // 收件者
        builder.append(" AND ai.receive = :receiveSid");
        parameters.put("receiveSid", loginBean.getUser().getSid());

        // 寄發單位 - 寄件部門
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getForwardDepts() != null && !commHeaderMBean.getForwardDepts().isEmpty()) {
            builder.append(" AND ai.sender_dep IN (:senderDep)");
            parameters.put("senderDep", commHeaderMBean.getForwardDepts() == null || commHeaderMBean.getForwardDepts().isEmpty()
                    ? ""
                    : commHeaderMBean.getForwardDepts());
        }

        // 寄發者 - 寄件人
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(headerMBean.getSender())) {
            List<Integer> senders = WkUserUtils.findByNameLike(headerMBean.getSender(), SecurityFacade.getCompanyId());
            if (WkStringUtils.isEmpty(senders)) {
                senders.add(-999);
            }
            builder.append(" AND ai.sender IN (:senders)");
            parameters.put("senders", senders.isEmpty() ? "" : senders);
        }

        builder.append(") AS ai ");
    }

    private void buildRequireCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_require tr WHERE 1=1");
        // 異動區間
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getStartUpdatedDate() != null && commHeaderMBean.getEndUpdatedDate() != null) {
            builder.append(" AND tr.update_dt BETWEEN :startUpdatedDate AND :endUpdatedDate");
            parameters.put("startUpdatedDate", helper.transStartDate(commHeaderMBean.getStartUpdatedDate()));
            parameters.put("endUpdatedDate", helper.transEndDate(commHeaderMBean.getEndUpdatedDate()));
        }
        // 需求單號
        if (!Strings.isNullOrEmpty(requireNo)) {
            builder.append(" AND tr.require_no = :requireNo");
            parameters.put("requireNo", requireNo);
        }
        this.buildRequireAdvanceCondition(requireNo, builder, parameters);
        builder.append(") AS tr ON tr.require_sid=ai.require_sid ");
    }

    private void buildRequireAdvanceCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        //////////////////// 以下為進階搜尋條件//////////////////////////////
        // 立單區間
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getStartDate() != null && commHeaderMBean.getEndDate() != null) {
            builder.append(" AND tr.create_dt BETWEEN :startDate AND :endDate");
            parameters.put("startDate", helper.transStartDate(commHeaderMBean.getStartDate()));
            parameters.put("endDate", helper.transEndDate(commHeaderMBean.getEndDate()));
        }

        // 需求類別製作進度
        builder.append(" AND tr.require_status IN (:requireStatus)");
        if (Strings.isNullOrEmpty(requireNo)) {
            parameters.put("requireStatus", headerMBean.createQueryReqStatus());
        } else {
            parameters.put("requireStatus", headerMBean.createQueryAllReqStatus());
        }

        // 緊急度
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getUrgencyTypeList() != null && !commHeaderMBean.getUrgencyTypeList().isEmpty()) {
            builder.append(" AND tr.urgency IN (:urgencyList)");
            parameters.put("urgencyList", commHeaderMBean.getUrgencyTypeList());
        }
        // 需求單號
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(commHeaderMBean.getRequireNo())) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(commHeaderMBean.getRequireNo()) + "%";
            builder.append(" AND tr.require_no LIKE :requireNo");
            parameters.put("requireNo", textNo);
        }
    }

    private void buildRequireIndexDictionaryCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {

        // 主題 | 內容 | 模糊搜尋
        builder.append("INNER JOIN (SELECT tid.require_sid,tid.field_content FROM tr_index_dictionary tid WHERE 1=1");
        // 模糊搜尋
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(commHeaderMBean.getFuzzyText())) {
            String text = "%" + reqularUtils.replaceIllegalSqlLikeStr(commHeaderMBean.getFuzzyText()) + "%";
            builder.append(" AND (tid.require_sid"
                    + " IN (SELECT DISTINCT tid1.require_sid FROM tr_index_dictionary tid1 WHERE tid1.field_content LIKE :text)"
                    + " OR tid.require_sid IN (SELECT DISTINCT trace.require_sid FROM tr_require_trace trace WHERE "
                    + "trace.require_trace_type = 'REQUIRE_INFO_MEMO' AND trace.require_trace_content LIKE :text))");
            parameters.put("text", text);
        }

        // only 主題
        if (Strings.isNullOrEmpty(requireNo) && Strings.isNullOrEmpty(commHeaderMBean.getFuzzyText())
                && Strings.isNullOrEmpty(headerMBean.getContent())
                && !Strings.isNullOrEmpty(headerMBean.getTheme())) {
            String themeText = "%" + reqularUtils.replaceIllegalSqlLikeStr(headerMBean.getTheme()) + "%";
            builder.append(" AND (tid.field_name = '主題' AND tid.field_content LIKE :themeText)");
            parameters.put("themeText", themeText);
        }

        // only 內容
        if (Strings.isNullOrEmpty(requireNo) && Strings.isNullOrEmpty(commHeaderMBean.getFuzzyText())
                && Strings.isNullOrEmpty(headerMBean.getTheme())
                && !Strings.isNullOrEmpty(headerMBean.getContent())) {
            String contentText = "%" + reqularUtils.replaceIllegalSqlLikeStr(headerMBean.getContent()) + "%";
            builder.append(" AND tid.require_sid IN (SELECT DISTINCT tid1.require_sid FROM tr_index_dictionary tid1 WHERE "
                    + "tid1.field_name!='主題' AND tid1.field_content LIKE :contentText)");
            parameters.put("contentText", contentText);
        }

        // 內容 & 主題
        if (Strings.isNullOrEmpty(requireNo) && Strings.isNullOrEmpty(commHeaderMBean.getFuzzyText())
                && !Strings.isNullOrEmpty(headerMBean.getTheme())
                && !Strings.isNullOrEmpty(headerMBean.getContent())) {
            String contentText = "%" + reqularUtils.replaceIllegalSqlLikeStr(headerMBean.getContent()) + "%";
            String themeText = "%" + reqularUtils.replaceIllegalSqlLikeStr(headerMBean.getTheme()) + "%";

            builder.append(" AND tid.require_sid IN (SELECT DISTINCT tid1.require_sid FROM tr_index_dictionary tid1 WHERE "
                    + "tid1.field_name!='主題' AND tid1.field_content LIKE :contentText)");
            parameters.put("contentText", contentText);

            builder.append(" AND (tid.field_name = '主題' AND tid.field_content LIKE :themeText)");
            parameters.put("themeText", themeText);
        }

        builder.append(" AND tid.field_name='主題') AS tid ON tr.require_sid=tid.require_sid ");
    }

    private void buildCategoryKeyMappingCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_category_key_mapping ckm WHERE 1=1");
        // 需求類別
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getSelectBigCategory() != null) {
            builder.append(" AND ckm.big_category_sid = :big");
            parameters.put("big", commHeaderMBean.getSelectBigCategory().getSid());
        }
        builder.append(") AS ckm ON tr.mapping_sid=ckm.key_sid ");
    }

    /**
     * 是否有追蹤資料
     *
     * @return
     */
    private String builderHasTraceColumn() {
        StringBuilder builder = new StringBuilder();
        builder.append(" (SELECT CASE WHEN (COUNT(trace.tracesid) > 0) THEN 'TRUE' ELSE 'FALSE' END ");
        builder.append(" FROM work_trace_info trace ");
        builder.append(" WHERE trace.trace_source_no = tr.require_no");
        builder.append(" AND trace.trace_source_type = 'TECH_REQUEST'");
        builder.append(" AND (trace.trace_dep = ").append(loginBean.getDep().getSid());
        builder.append(" OR trace.trace_usr = ").append(loginBean.getUser().getSid()).append(")");
        builder.append(" AND trace.trace_status = 'UN_FINISHED'");
        builder.append(" AND trace.Status = '0'");
        builder.append(") AS hasTrace ");
        return builder.toString();
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            this.moveScreenTab();
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Home06View view) {
        view.setReadRecordType(ReadRecordType.HAS_READ);
        Require r = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(r, loginBean.getUser(), RequireBottomTabType.INBOX_INSTRUCTION);
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (Home06View) event.getObject();
        this.changeRequireContent(this.queryKeeper);
        this.moveScreenTab();
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Home06View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
        this.moveScreenTab();
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
        this.moveScreenTab();
    }

    public void moveScreenTab() {
        this.r01MBean.getBottomTabMBean().resetTabIdx(this.r01MBean);
        this.r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.INBOX_INSTRUCTION);

        List<String> sids = inboxService.findSidsByRequire(this.r01MBean.getRequire(), loginBean.getUser(), InboxType.INCOME_INSTRUCTION);
        String indicateSid = queryKeeper.getInboxSendGroup().getSid();
        if (sids.indexOf(indicateSid) != -1) {
            display.execute("PF('ix_instruction_acc_panel_layer_zero').select(" + sids.indexOf(indicateSid) + ");");
            for (int i = 0; i < sids.size(); i++) {
                if (i != sids.indexOf(indicateSid)) {
                    display.execute("PF('ix_instruction_acc_panel_layer_zero').unselect(" + i + ");");
                }
            }
        }
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        helper.exportExcel(document, commHeaderMBean.getStartDate(), commHeaderMBean.getEndDate(), commHeaderMBean.getReportType(), false);
        hasDisplay = true;
    }

    /**
     * 隱藏部分column裡的內容
     */
    public void hideColumnContent() {
        hasDisplay = false;
    }

    /**
     * 列表執行關聯動作
     *
     * @param view
     */
    public void initRelevance(Home06View view) {
        this.querySelection = view;
        this.queryKeeper = this.querySelection;
        Require r = requireService.findByReqNo(view.getRequireNo());
        this.r01MBean.setRequire(r);
        this.r01MBean.getRelevanceMBean().initRelevance();
    }

    public String getRelevanceViewUrl(Home06View view) {
        if (view == null) {
            return "";
        }
        Require r = requireService.findByReqNo(view.getRequireNo());
        if (r == null) {
            return "";
        }
        WorkLinkGroup link = r.getLinkGroup();
        if (link == null) {
            return "";
        }
        return "../require/require04.xhtml" + urlService.createSimpleURLLink(URLServiceAttr.URL_ATTR_L, link.getSid(), view.getRequireNo(), 1);
    }

    /**
     * 列表點選轉寄功能
     * 
     * @param view
     */
    public void openForwardDialog(Home06View view) {
        this.queryKeeper = this.querySelection = view;

        if (SwitchType.DETAIL.equals(switchType)) {
            this.changeRequireContent(this.queryKeeper);
            this.display.update(Lists.newArrayList(
                    "@(.reportUpdateClz)",
                    "title_info_click_btn_id",
                    "require01_title_info_id",
                    "require_template_id",
                    "viewPanelBottomInfoId",
                    "req03botmid",
                    this.dataTableId + "1"));
        }

        RequireForwardDeAndPersonMBean mbean = WorkSpringContextHolder.getBean(RequireForwardDeAndPersonMBean.class);
        mbean.openDialog(view.getRequireNo(), r01MBean);
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Home06View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.removeClassByTextBold(dtId, pageCount);
        this.transformHasRead();
        this.checkHelfScreen();
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            display.update("headerTitle");
            display.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Home06View to) {
        querySelection = to;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 去除粗體Class
     *
     * @param dtId
     * @param pageCount
     */
    private void removeClassByTextBold(String dtId, String pageCount) {
        display.execute("removeClassByTextBold('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
        display.execute("changeAlreadyRead('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 變更已閱讀
     */
    private void transformHasRead() {
        querySelection.setReadRecordType(ReadRecordType.HAS_READ);
        queryKeeper = querySelection;
        queryItems.set(queryItems.indexOf(querySelection), querySelection);
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
        upDownBean.resetTabInfo(RequireBottomTabType.INBOX_INSTRUCTION, queryKeeper.getInboxSendGroup().getSid());
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.transformHasRead();
        this.removeClassByTextBold(dtId, pageCount);
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    /**
     * 列表執行追蹤動作
     *
     * @param view
     */
    public void btnAddTrack(Home06View view) {
        Require r = this.highlightAndReturnRequire(view);
        this.r01MBean.setRequire(r);
        this.r01MBean.getTraceActionMBean().initTrace(this.r01MBean);
    }

    /**
     * 列表標註及回傳需求單
     *
     * @param view
     * @return
     */
    private Require highlightAndReturnRequire(Home06View view) {
        this.querySelection = view;
        this.queryKeeper = this.querySelection;
        return requireService.findByReqNo(view.getRequireNo());
    }
}
