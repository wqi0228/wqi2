package com.cy.tech.request.web.controller.view.vo;

import java.util.List;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.service.CategorySettingService;
import com.cy.tech.request.logic.vo.query.search.SearchQuery;
import com.cy.tech.request.vo.category.BasicDataBigCategory;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.web.controller.enums.ReqStatusMBean;
import com.cy.tech.request.web.controller.view.component.CategoryCombineVO;
import com.cy.tech.request.web.controller.view.component.DateIntervalVO;
import com.cy.tech.request.web.controller.view.component.OrgTreeVO;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;

public class OverdueUnfinishedQueryVO extends SearchQuery {

    /**
     * 
     */
    private static final long serialVersionUID = 2023908013368804761L;

    private CategorySettingService categorySettingService = WorkSpringContextHolder.getBean(CategorySettingService.class);

    @Getter
    @Setter
    /** 需求類別 */
    private List<String> bigCategorySids = Lists.newArrayList();
    @Getter
    @Setter
    /** 被分派單位 */
    private OrgTreeVO orgTreeVO = new OrgTreeVO();
    @Getter
    @Setter
    /** 需求製作進度 */
    private List<String> requireStatusTypes = Lists.newArrayList();
    @Getter
    @Setter
    /** 閱讀類型 */
    private ReadRecordType readRecordType;
    @Getter
    @Setter
    /** 類別組合 */
    private CategoryCombineVO categoryCombineVO = new CategoryCombineVO();
    @Getter
    @Setter
    /** 日期區間 */
    private DateIntervalVO dateIntervalVO = new DateIntervalVO();
    @Getter
    @Setter
    private String searchText;
    @Getter
    @Setter
    private boolean overdue = true;

    public OverdueUnfinishedQueryVO() {
    }

    public OverdueUnfinishedQueryVO(Org org, User user, ReportType reportType) {
        this.dep = org;
        this.user = user;
        this.reportType = reportType;
    }

    public void init() {
        this.publicConditionInit();
        // 需求類別
        bigCategorySids.clear();
        for (BasicDataBigCategory bigCategory : categorySettingService.findAllBig()) {
            this.bigCategorySids.add(bigCategory.getSid());
        }
        // 被分派單位
        this.orgTreeVO.init();
        // 需求製作進度
        requireStatusTypes.clear();
        for (RequireStatusType type : ReqStatusMBean.STATUS2) {
            requireStatusTypes.add(type.name());
        }
        // 是否閱讀
        this.readRecordType = null;
        // 模糊搜尋
        this.searchText = null;
        // 類別組合
        this.categoryCombineVO.init();
        // 分派區間
        this.dateIntervalVO.init(-1);
        // 逾期狀態
        this.overdue = true;
    }

}
