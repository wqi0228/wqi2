/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search09QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.Search09View;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.pt.PtService;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.searchheader.CommonHeaderMBean;
import com.cy.tech.request.web.controller.searchheader.Search09HeaderMBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 原型確認簽核進度查詢
 *
 * @author jason_h
 */
@Controller
@Scope("view")
@Slf4j
public class Search09MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1155816800890649703L;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private Search09HeaderMBean headerMBean;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private CategoryTreeMBean cateTreeMBean;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private PtService ptService;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private Require01MBean r01MBean;
    @Autowired
    transient private Search09QueryService search09QueryService;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchResultHelper searchResultHelper;

    /** 所有的原型確認單據 */
    @Getter
    @Setter
    private List<Search09View> queryItems;

    /** 選擇的原型確認單據 */
    @Getter
    @Setter
    private Search09View querySelection;

    /** 上下筆移動keeper */
    @Getter
    private Search09View queryKeeper;

    /** 切換模式 */
    @Getter
    private SwitchType switchType = SwitchType.CONTENT;

    /** 切換模式 - 全畫面狀態 */
    @Getter
    private SwitchType switchFullType = SwitchType.DETAIL;

    @Getter
    private final String dataTableId = "dtRequire";

    @PostConstruct
    public void init() {
        this.search();
    }

    public void search() {
        queryItems = this.findWithQuery();
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        headerMBean.clear();
        this.search();
    }

    private List<Search09View> findWithQuery() {
        String requireNo = reqularUtils.getRequireNo(loginBean.getCompanyId(), commHeaderMBean.getFuzzyText());
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT "
                + "tpc.pt_check_sid,"
                + "tr.require_no,"
                + "tid.field_content,"
                + "tr.urgency,"
                + "tpc.create_dt,"
                + "tpc.dep_sid AS pDep,"
                + "tpc.create_usr AS pUser,"
                + "rpsi.approval_dt,"
                + "tpc.pt_check_estimate_dt,"
                + "ckm.big_category_name,"
                + "ckm.middle_category_name,"
                + "ckm.small_category_name,"
                + "tr.dep_sid,"
                + "tr.create_usr,"
                + "rpsi.bpm_default_signed_name,"
                + "rpsi.paper_code,"
                + "tpc.currently_ver,"
                + "tpc.pt_check_status,"
                + "tpc.pt_check_theme, "
                // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
                + this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire()
                + "FROM ");
        buildRequirePrototypeCheckCondition(requireNo, builder, parameters);
        buildRequirePrototypeSignInfoCondition(requireNo, builder, parameters);
        buildRequireCondition(requireNo, builder, parameters);
        buildRequireIndexDictionaryCondition(builder, parameters);
        buildCategoryKeyMappingCondition(requireNo, builder, parameters);
        // 檢查項目 (系統別)
        // 後方需對 主單 sid 做 group by
        builder.append(this.searchConditionSqlHelper.prepareSubFormCommonJoin(
                SecurityFacade.getUserSid(),
                FormType.PTCHECK,
                "tpc"));

        builder.append("WHERE tpc.pt_check_sid IS NOT NULL ");

        builder.append(" GROUP BY tpc.pt_check_sid ");
        builder.append("  ORDER BY tpc.read_update_dt DESC");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.PROTOTYPE_SIGN, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.PROTOTYPE_SIGN, SecurityFacade.getUserSid());

        // 查詢
        List<Search09View> resultList = search09QueryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // ====================================
        // 後續處理
        // ====================================
        if (WkStringUtils.notEmpty(resultList)) {
            // 後續處理-開始
            usageRecord.afterProcessStart();

            // 查詢條件過濾：系統別
            resultList = resultList.stream()
                    .filter(each -> this.searchResultHelper.filterCheckItems(
                            each.getCheckItemTypes(),
                            commHeaderMBean.getCheckItemTypes(),
                            false,
                            null))
                    .collect(Collectors.toList());

            // 後續處理-結束
            usageRecord.afterProcessEnd();
        }

        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return resultList;
    }

    private void buildRequirePrototypeCheckCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("(SELECT * FROM work_pt_check tpc WHERE 1=1");
        // 填單區間
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getStartDate() != null && commHeaderMBean.getEndDate() != null) {
            builder.append(" AND tpc.create_dt BETWEEN :startDate AND :endDate");
            parameters.put("startDate", helper.transStartDate(commHeaderMBean.getStartDate()));
            parameters.put("endDate", helper.transEndDate(commHeaderMBean.getEndDate()));
        }
        // 填寫單位
        builder.append(" AND tpc.dep_sid IN (:depSids)");
        parameters.put("depSids", commHeaderMBean.getRequireDepts() == null || commHeaderMBean.getRequireDepts().isEmpty()
                ? ""
                : commHeaderMBean.getRequireDepts());
        // 填單人員
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(commHeaderMBean.getTrCreatedUserName())) {
            List<Integer> userSids = WkUserUtils.findByNameLike(commHeaderMBean.getTrCreatedUserName(), SecurityFacade.getCompanyId());
            if (WkStringUtils.isEmpty(userSids)) {
                userSids.add(-999);
            }
            builder.append(" AND tpc.create_usr IN (:userSids)");
            parameters.put("userSids", userSids.isEmpty() ? "" : userSids);
        }
        // 模糊搜尋
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.isFuzzyQuery()) {
            String text = "%" + reqularUtils.replaceIllegalSqlLikeStr(commHeaderMBean.getFuzzyText()) + "%";
            builder.append(" AND (tpc.pt_check_content LIKE :text OR tpc.pt_check_theme LIKE :text OR tpc.pt_check_note LIKE :text OR ");
            builder.append("      tpc.pt_check_sid IN (SELECT wpr.pt_check_sid FROM work_pt_reply wpr WHERE wpr.reply_content LIKE :text) OR ");
            builder.append("      tpc.pt_check_sid IN (SELECT wpar.pt_check_sid FROM work_pt_already_reply wpar WHERE wpar.reply_content LIKE :text) ");
            builder.append("     )");
            parameters.put("text", text);
        }
        builder.append(") AS tpc ");
    }

    private void buildRequireCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_require tr WHERE 1=1");
        //////////////////// 以下為進階搜尋條件//////////////////////////////
        // 異動區間
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getStartUpdatedDate() != null && commHeaderMBean.getEndUpdatedDate() != null) {
            builder.append(" AND tr.update_dt BETWEEN :startUpdatedDate AND :endUpdatedDate");
            parameters.put("startUpdatedDate", helper.transStartDate(commHeaderMBean.getStartUpdatedDate()));
            parameters.put("endUpdatedDate", helper.transEndDate(commHeaderMBean.getEndUpdatedDate()));
        }
        // 緊急度
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getUrgencyTypeList() != null && !commHeaderMBean.getUrgencyTypeList().isEmpty()) {
            builder.append(" AND tr.urgency IN (:urgencyList)");
            parameters.put("urgencyList", commHeaderMBean.getUrgencyTypeList());
        }
        // 需求單號
        if (!Strings.isNullOrEmpty(requireNo)) {
            builder.append(" AND tr.require_no = :requireNo");
            parameters.put("requireNo", requireNo);
        }
        // 需求單號
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(commHeaderMBean.getRequireNo())) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(commHeaderMBean.getRequireNo()) + "%";
            builder.append(" AND tr.require_no LIKE :requireNo");
            parameters.put("requireNo", textNo);
        }
        // 轉發部門
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getForwardDepts() != null && !commHeaderMBean.getForwardDepts().isEmpty()) {
            builder.append(" AND tr.require_sid IN (SELECT tai.require_sid FROM tr_alert_inbox tai WHERE tai.forward_type='FORWARD_DEPT'"
                    + " AND tai.receive_dep IN (:forwardDepts))");
            parameters.put("forwardDepts", commHeaderMBean.getForwardDepts());
        }
        builder.append(") AS tr ON tr.require_sid=tpc.pt_check_source_sid ");
    }

    private void buildRequireIndexDictionaryCondition(StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_index_dictionary tid WHERE "
                + "tid.field_name='主題') AS tid ON "
                + "tr.require_sid = tid.require_sid ");
    }

    private void buildCategoryKeyMappingCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_category_key_mapping ckm WHERE 1=1");
        // 類別組合
        if (Strings.isNullOrEmpty(requireNo) && (!cateTreeMBean.getBigDataCateSids().isEmpty() || !cateTreeMBean.getMiddleDataCateSids().isEmpty()
                || !cateTreeMBean.getSmallDataCateSids().isEmpty())) {
            builder.append(" AND ckm.big_category_sid IN (:bigs) OR ckm.middle_category_sid IN (:middles) OR ckm.small_category_sid IN "
                    + "(:smalls)");
            // 加入單獨選擇的大類
            if (commHeaderMBean.getSelectBigCategory() != null) {
                cateTreeMBean.getBigDataCateSids().add(commHeaderMBean.getSelectBigCategory().getSid());
            }
            parameters.put("bigs", cateTreeMBean.getBigDataCateSids().isEmpty() ? "" : cateTreeMBean.getBigDataCateSids());
            parameters.put("middles", cateTreeMBean.getMiddleDataCateSids().isEmpty() ? "" : cateTreeMBean.getMiddleDataCateSids());
            parameters.put("smalls", cateTreeMBean.getSmallDataCateSids().isEmpty() ? "" : cateTreeMBean.getSmallDataCateSids());
        }
        // 需求類別
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getSelectBigCategory() != null && cateTreeMBean.getBigDataCateSids().isEmpty()) {
            builder.append(" AND ckm.big_category_sid = :big");
            parameters.put("big", commHeaderMBean.getSelectBigCategory().getSid());
        }
        builder.append(") AS ckm ON tr.mapping_sid=ckm.key_sid ");
    }

    private void buildRequirePrototypeSignInfoCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM work_pt_sign_info rpsi WHERE 1=1");
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(headerMBean.getPendingSignOnUserName())) {
            String text = "%" + reqularUtils.replaceIllegalSqlLikeStr(headerMBean.getPendingSignOnUserName()) + "%";
            builder.append(" AND rpsi.bpm_default_signed_name LIKE :pendingSignOnUserName");
            parameters.put("pendingSignOnUserName", text);
        }
        if (Strings.isNullOrEmpty(requireNo) && headerMBean.getSelectStatuses() != null && !headerMBean.getSelectStatuses().isEmpty()) {
            builder.append(" AND rpsi.paper_code IN (:instanceStatus)");
            parameters.put("instanceStatus", headerMBean.getInstancePaperCodes());
        }
        builder.append(") AS rpsi ON rpsi.pt_check_sid=tpc.pt_check_sid ");
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            this.moveScreenTab();
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (Search09View) event.getObject();
        this.changeRequireContent(this.queryKeeper);
        this.moveScreenTab();
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Search09View view) {
        view.setReadRecordType(ReadRecordType.HAS_READ);
        Require r = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(r, loginBean.getUser(), RequireBottomTabType.PT_CHECK_INFO);
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Search09View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
        this.moveScreenTab();
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
        this.moveScreenTab();
    }

    public void moveScreenTab() {
        this.r01MBean.getBottomTabMBean().resetTabIdx(this.r01MBean);
        this.r01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.PT_CHECK_INFO);

        List<String> sids = ptService.findSidsBySourceSid(this.r01MBean.getRequire().getSid());
        String indicateSid = queryKeeper.getSid();
        if (sids.indexOf(indicateSid) != -1) {
            display.execute("PF('pt_acc_panel_layer_zero').select(" + sids.indexOf(indicateSid) + ");");
            for (int i = 0; i < sids.size(); i++) {
                if (i != sids.indexOf(indicateSid)) {
                    display.execute("PF('pt_acc_panel_layer_zero').unselect(" + i + ");");
                }
            }
        }
    }

    public String getPrototypeVersion(Search09View view) {
        String version = "V" + view.getVersion();
        if (PtStatus.REDO.equals(view.getPrototypeStatus())) {
            return version + "(重做)";
        }
        if (PtStatus.VERIFY_INVAILD.equals(view.getPrototypeStatus())) {
            return version + "(審核作廢)";
        }
        return version;
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search09View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            display.update("headerTitle");
            display.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Search09View to) {
        querySelection = to;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
        upDownBean.resetTabInfo(RequireBottomTabType.PT_CHECK_INFO, queryKeeper.getSid());
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }
}
