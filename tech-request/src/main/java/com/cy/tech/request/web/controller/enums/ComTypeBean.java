/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.enums;

import com.cy.tech.request.vo.enums.ComType;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.SelectItem;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author shaun
 */
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class ComTypeBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6349967254756903640L;

    public List<ComType> getValues() {
        return Lists.newArrayList(ComType.values());
    }

    public SelectItem[] getSelectItemValues() {
        SelectItem[] items = new SelectItem[ComType.values().length];
        int i = 0;
        for (ComType each : ComType.values()) {
            String label = each.name();
            items[i++] = new SelectItem(each, label);
        }
        return items;
    }
}
