package com.cy.tech.request.web.newsearch;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.work.common.utils.WkStringUtils;

import lombok.Getter;

@Controller
@Scope("view")
public class NewSearchDisplayComponent implements Serializable {

    // ========================================================================
    // 工具區
    // ========================================================================

    /**
     * 
     */
    private static final long serialVersionUID = 1998058621247871976L;

    @Autowired
    private NewSearchHelper serachHelper;

    // ========================================================================
    // 變數區
    // ========================================================================
    /**
     * 
     */
    @Getter
    private ReportType reportType;

    /** 切換模式 */
    @Getter
    protected SwitchType switchType = SwitchType.CONTENT;
    /** 切換模式 - 全畫面狀態 */
    @Getter
    protected SwitchType switchFullType = SwitchType.DETAIL;

    /**
     * 選項：需求類型
     */
    @Getter
    private List<SelectItem> bigCategorySelectItems;

    // ========================================================================
    // 方法
    // ========================================================================
    @PostConstruct
    public void init() {
        // ====================================
        // 取得需求類型
        // ====================================
        this.reportType = this.prepareReportType();

        // ====================================
        // 取得需求類型
        // ====================================
        this.bigCategorySelectItems = this.serachHelper.prepareSelectItemBigCategory();
    }

    private ReportType prepareReportType() {
        String viewId = Faces.getViewId();
        if (WkStringUtils.notEmpty(viewId) && viewId.split("/").length > 1) {
            String[] idAry = viewId.split("/");
            viewId = idAry[idAry.length - 1];
        }
        for (ReportType reportType : ReportType.values()) {
            if (reportType.getViewId().equals(viewId)) {
                return reportType;
            }
        }
        return ReportType.NOT_FIND;
    }

}
