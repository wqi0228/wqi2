package com.cy.tech.request.web.controller.component.mutiuserqkpker.model;

import java.io.Serializable;
import java.util.List;

import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 *
 */
@Getter
@Setter
public class MultiUserQuickPickerUserGroupData implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -9169475644534920899L;

    /**
     * 建構子
     */
    public MultiUserQuickPickerUserGroupData(String name, List<Integer> groupUserSids) {
        this.name = name;
        this.groupUserSids = groupUserSids;
    }
    
    /**
     * 群組名稱
     */
    private String name;

    /**
     * 群組的 user
     */
    private List<Integer> groupUserSids = Lists.newArrayList();
}
