/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search12QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.Search12View;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.customer.ReqWorkCustomerHelper;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.searchheader.CommonHeaderMBean;
import com.cy.tech.request.web.controller.searchheader.Search12HeaderMBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.customer.vo.WorkCustomer;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 收費金額一覽表查詢
 *
 * @author jason_h
 */
@Controller
@Scope("view")
@Slf4j
public class Search12MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6856243604153540798L;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private Search12HeaderMBean headerMBean;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private CategoryTreeMBean cateTreeMBean;
    @Autowired
    transient private ReqWorkCustomerHelper reqWorkCustomerHelper;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private Search12QueryService search12QueryService;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchResultHelper searchResultHelper;

    /** 所有的需求單 */
    private List<Search12View> allQueryItems;
    @Getter
    @Setter
    private List<Search12View> queryItems;

    /** 選擇的需求單 */
    @Getter
    @Setter
    private Search12View querySelection;

    /** 上下筆移動keeper */
    @Getter
    private Search12View queryKeeper;

    /** 切換模式 */
    @Getter
    private SwitchType switchType = SwitchType.CONTENT;

    /** 切換模式 - 全畫面狀態 */
    @Getter
    private SwitchType switchFullType = SwitchType.DETAIL;

    /** 在匯出的時候，某些內容需要隱藏 */
    @Getter
    private boolean hasDisplay = true;

    @Getter
    private final String dataTableId = "dtRequire";

    @PostConstruct
    public void init() {
        allQueryItems = Lists.newArrayList();
        this.search();
    }

    /**
     * 查詢
     */
    public void search() {
        allQueryItems = this.findWithQuery();
        this.filterCustomerPage(Boolean.TRUE);
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        headerMBean.clear();
        this.search();
    }

    /**
     * 過濾 廳主
     *
     * @param isSearch
     */
    public void filterCustomerPage(Boolean isSearch) {
        if (headerMBean.getIsCustomerPage()) {
            if (headerMBean.getCustomerPage() == null && !headerMBean.getCustomerPageItems().isEmpty()) {
                headerMBean.setCustomerPage(headerMBean.getCustomerPageItems().get(0));
            }
        }
        this.bulidQueryItemsAndCustomerPageItems(isSearch);
    }

    /**
     * 建立廳主分頁選項
     */
    private void bulidQueryItemsAndCustomerPageItems(Boolean isSearch) {
        queryItems = Lists.newArrayList();
        List<WorkCustomer> customerPageItems = Lists.newArrayList();
        for (Search12View v : allQueryItems) {
            if (isSearch && v.getCustomer() != null && !reqWorkCustomerHelper.containCustomer(customerPageItems, v.getCustomer())) {
                customerPageItems.add(reqWorkCustomerHelper.findCopyOne(v.getCustomer()));
            }
            if (!(headerMBean.getIsCustomerPage()
                    && (headerMBean.getCustomerPage() != null && !headerMBean.getCustomerPage().getSid().equals(v.getCustomer())))) {
                queryItems.add(v);
            }
        }
        if (isSearch) {
            headerMBean.setCustomerPageItems(customerPageItems);
            if (headerMBean.getIsCustomerPage()
                    && (customerPageItems.isEmpty() || !customerPageItems.contains(headerMBean.getCustomerPage()))) {
                headerMBean.setCustomerPage(null);
                this.filterCustomerPage(Boolean.FALSE);
            }
        }
    }

    /**
     * 變更 廳主
     */
    public void chgCustomerPage() {
        this.filterCustomerPage(Boolean.FALSE);
    }

    private List<Search12View> findWithQuery() {
        String requireNo = reqularUtils.getRequireNo(loginBean.getCompanyId(), commHeaderMBean.getFuzzyText());
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT "
                + "tr.require_sid,"
                + "tr.require_no,"
                + "tid.field_name,"
                + "tid.field_content,"
                + "tr.urgency,"
                + "tr.create_dt,"
                + "ckm.big_category_name,"
                + "ckm.middle_category_name,"
                + "ckm.small_category_name,"
                + "tr.dep_sid,"
                + "tr.create_usr,"
                + "tr.customer,"
                + "tr.author,"
                + this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire()

                + " FROM ");
        buildRequireCondition(requireNo, builder, parameters);
        buildRequireIndexDictionaryCondition(requireNo, builder, parameters);
        buildCategoryKeyMappingCondition(requireNo, builder, parameters);
        builder.append(this.searchConditionSqlHelper.prepareCommonJoin(SecurityFacade.getUserSid()));

        builder.append("WHERE tr.require_sid IS NOT NULL ");
        builder.append(" GROUP BY tr.require_sid, field_name,field_content  ");
        builder.append("  ORDER BY tr.create_dt ASC");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.AMOUNT_CASH, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.AMOUNT_CASH, SecurityFacade.getUserSid());

        // 查詢
        List<Search12View> resultList = search12QueryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // ====================================
        // 後續處理
        // ====================================
        if (WkStringUtils.notEmpty(resultList)) {
            // 後續處理-開始
            usageRecord.afterProcessStart();
            // 查詢條件過濾：系統別
            resultList = resultList.stream()
                    .filter(each -> this.searchResultHelper.filterCheckItems(
                            each.getCheckItemTypes(),
                            commHeaderMBean.getCheckItemTypes(),
                            false,
                            null))
                    .collect(Collectors.toList());
            // 後續處理-結束
            usageRecord.afterProcessEnd();
        }
        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return resultList;
    }

    private void buildRequireCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("(SELECT * FROM tr_require tr WHERE 1=1");
        // 立單區間
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getStartDate() != null && commHeaderMBean.getEndDate() != null) {
            builder.append(" AND tr.create_dt BETWEEN :startDate AND :endDate");
            parameters.put("startDate", helper.transStartDate(commHeaderMBean.getStartDate()));
            parameters.put("endDate", helper.transEndDate(commHeaderMBean.getEndDate()));
        }

        // 需求單位
        builder.append(" AND tr.dep_sid IN (:depSids)");
        parameters.put("depSids", commHeaderMBean.getRequireDepts() == null || commHeaderMBean.getRequireDepts().isEmpty()
                ? ""
                : commHeaderMBean.getRequireDepts());

        // 需求類別製作進度
        builder.append(" AND tr.require_status IN (:requireStatus)");
        if (Strings.isNullOrEmpty(requireNo)) {
            parameters.put("requireStatus", headerMBean.createQueryReqStatus());
        } else {
            parameters.put("requireStatus", headerMBean.createQueryAllReqStatus());
        }

        // 廳主
        if (Strings.isNullOrEmpty(requireNo) && headerMBean.getCustomers() != null && !headerMBean.getCustomers().isEmpty()) {
            builder.append(" AND tr.customer IN (:customers)");
            parameters.put("customers", headerMBean.getCustomerSids());
        }
        // 需求單號
        if (!Strings.isNullOrEmpty(requireNo)) {
            builder.append(" AND tr.require_no = :requireNo");
            parameters.put("requireNo", requireNo);
        }
        builder.append(") AS tr ");
    }

    private void buildRequireIndexDictionaryCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT tid.require_sid,tid.field_content,tid.field_name FROM tr_index_dictionary tid WHERE "
                + "tid.require_sid IN (SELECT DISTINCT tid1.require_sid FROM tr_index_dictionary tid1 WHERE tid1.field_name = :fieldName)");
        parameters.put("fieldName", Search12HeaderMBean.INDEX_KEY);
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.isFuzzyQuery()) {
            // 模糊搜尋
            String text = "%" + reqularUtils.replaceIllegalSqlLikeStr(commHeaderMBean.getFuzzyText()) + "%";
            builder.append(" AND (tid.require_sid"
                    + " IN (SELECT DISTINCT tid2.require_sid FROM tr_index_dictionary tid2 WHERE tid2.field_content LIKE :text)"
                    + " OR tid.require_sid IN (SELECT DISTINCT trace.require_sid FROM tr_require_trace trace WHERE "
                    + "trace.require_trace_type = 'REQUIRE_INFO_MEMO' AND trace.require_trace_content LIKE :text))");
            parameters.put("text", text);
        }
        builder.append(" AND (tid.field_name='主題' OR tid.field_name='是否收費')) AS tid ON tr.require_sid=tid.require_sid ");
    }

    private void buildCategoryKeyMappingCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_category_key_mapping ckm WHERE 1=1");
        // 類別組合
        if (Strings.isNullOrEmpty(requireNo) && (!cateTreeMBean.getBigDataCateSids().isEmpty() || !cateTreeMBean.getMiddleDataCateSids().isEmpty()
                || !cateTreeMBean.getSmallDataCateSids().isEmpty())) {
            builder.append(" AND ckm.big_category_sid IN (:bigs) OR ckm.middle_category_sid IN (:middles) OR ckm.small_category_sid IN "
                    + "(:smalls)");
            // 加入單獨選擇的大類
            if (commHeaderMBean.getSelectBigCategory() != null) {
                cateTreeMBean.getBigDataCateSids().add(commHeaderMBean.getSelectBigCategory().getSid());
            }
            parameters.put("bigs", cateTreeMBean.getBigDataCateSids().isEmpty() ? "" : cateTreeMBean.getBigDataCateSids());
            parameters.put("middles", cateTreeMBean.getMiddleDataCateSids().isEmpty() ? "" : cateTreeMBean.getMiddleDataCateSids());
            parameters.put("smalls", cateTreeMBean.getSmallDataCateSids().isEmpty() ? "" : cateTreeMBean.getSmallDataCateSids());
        }
        // 需求類別
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getSelectBigCategory() != null && cateTreeMBean.getBigDataCateSids().isEmpty()) {
            builder.append(" AND ckm.big_category_sid = :big");
            parameters.put("big", commHeaderMBean.getSelectBigCategory().getSid());
        }
        builder.append(") AS ckm ON tr.mapping_sid=ckm.key_sid ");
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Search12View view) {
        view.setReadRecordType(ReadRecordType.HAS_READ);
        Require r = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(r, loginBean.getUser());
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Search12View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        helper.exportExcel(document, commHeaderMBean.getStartDate(), commHeaderMBean.getEndDate(), commHeaderMBean.getReportType());

        HSSFWorkbook wb = (HSSFWorkbook) document;
        HSSFSheet sheet = wb.getSheetAt(0);
        HSSFRow headerRow = sheet.getRow(3);
        headerRow.getCell(0).setCellValue("參考幣別：RMB");
        hasDisplay = true;
    }

    /**
     * 隱藏部分column裡的內容
     */
    public void hideColumnContent() {
        hasDisplay = false;
    }

    public String getSum() {
        BigDecimal sum = queryItems.stream()
                .filter(view -> view.getCash() != null)
                .map(view -> view.getCash())
                .reduce(new BigDecimal("0"), (total, num) -> total.add(num));

        DecimalFormat df = new DecimalFormat();
        df.applyPattern("#,##0.00");
        return df.format(sum.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Search12View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            display.update("headerTitle");
            display.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Search12View to) {
        querySelection = to;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }
}
