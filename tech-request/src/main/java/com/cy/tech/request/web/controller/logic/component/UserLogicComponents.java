/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.logic.component;

import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

/**
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class UserLogicComponents implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8284730177901548617L;

    private static UserLogicComponents instance;

    public static UserLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        UserLogicComponents.instance = this;
    }

    public User findBySid(Integer userSid) {
        return WkUserCache.getInstance().findBySid(userSid);
    }

    public List<User> findUserByDepSid(Integer depSid) {
        try {
            List<User> users = WkUserCache.getInstance().getUsersByPrimaryOrgSid(depSid);
            return users;
        } catch (Exception e) {
            log.error("findUserByDepSid error :", e);
        }
        return Lists.newArrayList();
    }
}
