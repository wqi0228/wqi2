package com.cy.tech.request.web.controller.values;

import com.cy.work.common.utils.WkEntityUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.tech.request.logic.service.CategoryCreateService;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.BasicDataCategoryType;
import com.cy.tech.request.logic.service.CategorySettingService;
import com.cy.tech.request.logic.vo.BasicDataCategoryTo;
import com.cy.tech.request.vo.category.BasicDataBigCategory;
import com.cy.tech.request.web.pf.utils.CommonBean;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.SelectItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.google.common.collect.Lists;
import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;

/**
 * 類別 - 提供頁面服務
 *
 * @author shaun
 */
@Controller
@Scope(WebApplicationContext.SCOPE_APPLICATION)
public class CategoryMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5814177435882059836L;
    @Autowired
    transient private CommonBean common;
    @Autowired
    transient private CategoryCreateService ccService;
    @Autowired
    transient private CategorySettingService cssService;
    @Autowired
    transient private WkEntityUtils entityUtil;

    /**
     * 提供頁面 類別型態 SelectItem[]
     *
     * @return
     */
    public SelectItem[] getTypeValues() {
        SelectItem[] items = new SelectItem[BasicDataCategoryType.values().length];
        int i = 0;
        for (BasicDataCategoryType each : BasicDataCategoryType.values()) {
            String label = common.get(each);
            items[i++] = new SelectItem(each, label);
        }
        return items;
    }

    /**
     * 透過 類別型態 得到 類別To結果 並回傳 SelectItem[]
     *
     * @param type
     * @return
     */
    public List<BasicDataCategoryTo> getBasicDataCategoryToMappingAttr(BasicDataCategoryType type) {
        if (type == null) {
            return Lists.newArrayList();
        }
        @SuppressWarnings("rawtypes")
        List mappingEntity = ccService.findMappingEntity(type);
        List<BasicDataCategoryTo> result = Lists.newArrayList();
        for (Object each : mappingEntity) {
            BasicDataCategoryTo to = new BasicDataCategoryTo();
            entityUtil.copyProperties(each, to);
            to.setType(ccService.findType(each));
            to.setParent(ccService.findParent(each));
            result.add(to);
        }
        return result;
    }

    /**
     * 提供頁面 大類 List
     *
     * @return
     */
    public List<BasicDataBigCategory> getBigValues() {
        return cssService.findAllBig();
    }

    /**
     * 是否擁有功能清單權限 <br/>
     *
     * @param itemUrl
     * @param permission
     * @return
     */
    public Boolean hasCatePermission(LoginBean loginBean, String catePermissionRole) {
        //全空代表未設定權限
    	if (Strings.isNullOrEmpty(catePermissionRole)) {
            return Boolean.TRUE;
        }
        
        
        return WkUserUtils.isUserHasRole(
        		SecurityFacade.getUserSid(), 
        		SecurityFacade.getCompanyId(),
        		Lists.newArrayList(catePermissionRole.split(",")));
    }
}
