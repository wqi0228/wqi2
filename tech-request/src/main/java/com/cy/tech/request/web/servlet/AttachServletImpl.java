/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.servlet;

import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.vo.AttachmentService;
import com.cy.work.common.vo.AttachmentServlet;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 子系統用AttachmentServlet實作<BR/>
 * 用以判斷目前是由哪個附加檔案服務提供上傳
 */
@WebServlet("/AttachServletImpl")
public class AttachServletImpl extends HttpServlet implements AttachmentServlet {

    /**
     * 
     */
    private static final long serialVersionUID = 8506204825336582466L;
    //要覆寫
    public final static String SERVLET_NAME = AttachServletImpl.class.getSimpleName();

    @SuppressWarnings("rawtypes")
    @Override
    public AttachmentService getAttachService(HttpServletRequest request) {
        String selectService = this.getAttachmentServiceParameter(request);
        switch (selectService) {
            case "ReqAttachService":
                return WorkSpringContextHolder.getBean("require_attach", AttachmentService.class);
            case "ReqFbkAttachService":
                return WorkSpringContextHolder.getBean("require_fbk_attach", AttachmentService.class);
            case "ReqFbkReplyAttachService":
                return WorkSpringContextHolder.getBean("req_fbk_reply_attach", AttachmentService.class);
            case "PtAttachService":
                return WorkSpringContextHolder.getBean("pt_attach", AttachmentService.class);
            case "PtHistoryAttachService":
                return WorkSpringContextHolder.getBean("pt_history_attach", AttachmentService.class);
            case "SendTestAttachService":
                return WorkSpringContextHolder.getBean("sendTestAttachService", AttachmentService.class);
            case "SendTestHistoryAttachService":
                return WorkSpringContextHolder.getBean("send_test_history_attach", AttachmentService.class);
            case "OnpgAttachService":
                return WorkSpringContextHolder.getBean("onpg_attach", AttachmentService.class);
            case "OnpgHistoryAttachService":
                return WorkSpringContextHolder.getBean("onpg_history_attach", AttachmentService.class);
            case "OthSetAttachService":
                return WorkSpringContextHolder.getBean("othset_attach", AttachmentService.class);
            case "OthSetHistoryAttachService":
                return WorkSpringContextHolder.getBean("othset_history_attach", AttachmentService.class);
        }
        return null;
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
              throws ServletException, IOException {
        AttachmentServlet.super.doGet(request, response, super.getServletContext());
    }

}
