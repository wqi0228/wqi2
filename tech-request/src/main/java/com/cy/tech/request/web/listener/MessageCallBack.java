/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.listener;

import java.io.Serializable;

/**
 * 顯示訊息CallBack (任何子物件欲傳訊息使用)
 * @author brain0925_liao
 */
public class MessageCallBack implements  Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4430109778806670130L;

    public void showMessage(String message) {

    }
}
