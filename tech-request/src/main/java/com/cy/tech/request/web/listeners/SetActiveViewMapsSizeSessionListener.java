/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.listeners;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 *
 * @author kasim
 */
public class SetActiveViewMapsSizeSessionListener implements HttpSessionListener {

    String ACTIVE_VIEW_MAPS_SIZE = "com.sun.faces.application.view.activeViewMapsSize";
    
    @Override
    public void sessionCreated(HttpSessionEvent event) {
        event.getSession().setAttribute(ACTIVE_VIEW_MAPS_SIZE, 60);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
    }
}
