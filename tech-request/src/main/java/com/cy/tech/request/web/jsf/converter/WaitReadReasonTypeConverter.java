/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.jsf.converter;

import javax.faces.convert.EnumConverter;
import javax.faces.convert.FacesConverter;

import com.cy.tech.request.vo.enums.WaitReadReasonType;

/**
 *
 * @author kasim
 */
@FacesConverter(value = "waitReadReasonTypeConverter")
public class WaitReadReasonTypeConverter extends EnumConverter {

    public WaitReadReasonTypeConverter() {
        super(WaitReadReasonType.class);
    }
}
