/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search.helper;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.view.Search30View;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * @author saul_chen
 */
@Component
public class Search30Helper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1775220781458235672L;
    @Autowired
    transient private OrganizationService orgService;
    @Autowired
    transient private UserService userService;
    @Autowired
    private URLService urlService;
    @Autowired
    private SearchService searchHelper;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;

    @PersistenceContext
    transient private EntityManager em;

    public List<Search30View> search(Org loginDep, User loginUser) {

        StringBuilder builder = new StringBuilder();
        builder.append("SELECT tpc.pt_check_sid, ");
        builder.append("       tr.require_no, ");
        builder.append("       tid.field_content, ");
        builder.append("       tpc.create_dt, ");
        builder.append("       tpc.dep_sid                                           AS pDep, ");
        builder.append("       tpc.create_usr                                        AS pUser, ");
        builder.append("       ckm.big_category_name, ");
        builder.append("       ckm.middle_category_name, ");
        builder.append("       ckm.small_category_name, ");
        builder.append("       tr.create_usr, ");
        builder.append("       tpc.pt_check_estimate_dt, ");
        builder.append("       tpc.pt_check_theme, ");
        builder.append("       rpsi.approval_dt, ");
        builder.append("       tr.require_status, ");
        // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
        builder.append(this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire());
        builder.append("FROM ( ");

        this.buildRequirePrototypeCheckCondition(loginUser, builder);
        this.buildRequireCondition(builder, loginUser);
        this.buildRequireIndexDictionaryCondition(builder);
        this.buildCategoryKeyMappingCondition(builder);
        this.buildRequirePrototypeSignInfoCondition(builder);
        this.buildRequirePrototypeReplyCondition(builder);
        // 檢查項目 (系統別)
        // 後方需對 主單 sid 做 group by
        builder.append(this.searchConditionSqlHelper.prepareSubFormCommonJoin(
                SecurityFacade.getUserSid(),
                FormType.PTCHECK,
                "tpc"));

        builder.append("WHERE tpc.pt_check_sid IS NOT NULL ");
        builder.append(" GROUP BY tpc.pt_check_sid ");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.TODO_PROTOTYPE_CHECK, builder.toString(), null);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.TODO_PROTOTYPE_CHECK, SecurityFacade.getUserSid());

        // 查詢
        List<Search30View> resultList = this.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                Maps.newHashMap(),
                SecurityFacade.getUserSid(),
                usageRecord);

        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return resultList;
    }

    /**
     * 組合語句
     *
     * @param loginDep
     * @param loginUser
     * @param builder
     */
    private void buildRequirePrototypeCheckCondition(User loginUser, StringBuilder builder) {

        builder.append("    SELECT * FROM work_pt_check tpc ");
        builder.append("        WHERE tpc.pt_check_status in (");
        builder.append("        '").append(PtStatus.APPROVE.name()).append("', ");
        builder.append("        '").append(PtStatus.PROCESS.name()).append("'");
        builder.append("        )");

        builder.append(") AS tpc ");
    }

    /**
     * 組合語句
     *
     * @param query
     * @param builder
     */
    private void buildRequireCondition(StringBuilder builder, User loginUser) {
        builder.append("INNER JOIN ( ");
        builder.append("    SELECT * FROM tr_require tr ");
        builder.append("        WHERE 1=1 ");
        builder.append("        AND tr.require_status = '").append(RequireStatusType.PROCESS.name()).append("' ");
        builder.append("        AND tr.has_prototype = 1 ");

        // 加入自己
        Set<Integer> userSids = Sets.newHashSet(loginUser.getSid());
        // 管理單位以下所有成員
        Set<Integer> managerDepSids = WkOrgCache.getInstance().findManagerWithChildOrgSids(loginUser.getSid());
        if (WkStringUtils.notEmpty(managerDepSids)) {
            Set<Integer> depUserSids = WkUserCache.getInstance().findUserSidByOrgsWithManager(managerDepSids, Activation.ACTIVE);
            if (WkStringUtils.notEmpty(depUserSids)) {
                userSids.addAll(depUserSids);
            }
        }

        String userSidSql = userSids.stream()
                .map(each -> String.valueOf(each))
                .collect(Collectors.joining(",", " AND tr.create_usr in (", ") "));

        builder.append(userSidSql);

        builder.append(") AS tr ON tr.require_sid=tpc.pt_check_source_sid ");
    }

    /**
     * 組合語句
     *
     * @param builder
     */
    private void buildRequireIndexDictionaryCondition(StringBuilder builder) {
        builder.append("INNER JOIN ( ");
        builder.append("    SELECT tid.require_sid,tid.field_content FROM tr_index_dictionary tid ");
        builder.append("        WHERE tid.field_name='主題' ");
        builder.append(") AS tid ON tr.require_sid = tid.require_sid ");
    }

    /**
     * 組合語句
     *
     * @param builder
     */
    private void buildCategoryKeyMappingCondition(StringBuilder builder) {
        builder.append("INNER JOIN ( ");
        builder.append("    SELECT * FROM tr_category_key_mapping ckm ");
        builder.append("        WHERE 1=1 ");
        builder.append(") AS ckm ON tr.mapping_sid=ckm.key_sid ");
    }

    /**
     * 組合語句
     *
     * @param builder
     */
    private void buildRequirePrototypeSignInfoCondition(StringBuilder builder) {
        builder.append("INNER JOIN ( ");
        builder.append("    SELECT rpsi.pt_check_sid,rpsi.paper_code,rpsi.approval_dt FROM work_pt_sign_info rpsi ");
        builder.append(") AS rpsi ON rpsi.pt_check_sid=tpc.pt_check_sid ");
    }

    /**
     * 組合語句
     *
     * @param builder
     */
    private void buildRequirePrototypeReplyCondition(StringBuilder builder) {
        builder.append("LEFT JOIN ( ");
        builder.append("    SELECT rpr.pt_check_sid,rpr.reply_udt,MAX(rpr.reply_udt) FROM work_pt_reply rpr ");
        builder.append("    GROUP BY rpr.pt_check_sid ");
        builder.append(") AS rpr ON rpr.pt_check_sid=tpc.pt_check_sid ");
    }

    private List<Search30View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {

        // ====================================
        // 查詢
        // ====================================

        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        @SuppressWarnings("unchecked")
        List<Object[]> result = query.getResultList();
        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if (WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();
        List<Search30View> viewResult = Lists.newArrayList();

        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd");

        for (int i = 0; i < result.size(); i++) {
            Search30View v = new Search30View();

            Object[] record = (Object[]) result.get(i);
            int index = 0;
            String ptSid = (String) record[index++];
            String requireNo = (String) record[index++];
            String requireTheme = (String) record[index++];
            Date prototypeCreatedDate = (Date) record[index++];
            Integer prototypeCreateDep = (Integer) record[index++];
            Integer prototypeCreatedUser = (Integer) record[index++];
            String bigName = (String) record[index++];
            String middleName = (String) record[index++];
            String smallName = (String) record[index++];
            Integer requireCreatedUser = (Integer) record[index++];
            Date estimateFinishDate = (Date) record[index++];
            String ptTheme = (String) record[index++];
            Date approvalDate = (Date) record[index++];
            String requireStatus = (String) record[index++];
            // 處理共通欄位 (一定要擺在『取值』最後面, 否則 index 會不對)
            v.prepareCommonColumn(record, index);

            v.setPtSid(ptSid);
            v.setRequireNo(requireNo);
            v.setPrototypeCreateDep(orgService.getOrgName(prototypeCreateDep));
            v.setPrototypeCreatedDate(sdf1.format(prototypeCreatedDate));
            v.setPrototypeCreatedUser(userService.getUserName(prototypeCreatedUser));
            v.setBigName(bigName);
            v.setMiddleName(middleName);
            v.setSmallName(smallName);
            v.setRequireCreatedUser(userService.getUserName(requireCreatedUser));
            v.setEstimateFinishDate(sdf2.format(estimateFinishDate));
            v.setRequireStatus(RequireStatusType.safeValueOf(requireStatus));
            v.setPtTheme(ptTheme);
            v.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
            v.setApprovalDate(sdf1.format(approvalDate));
            v.setLocalUrlLink(urlService.createLocalUrlLinkParamForTab(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(execUserSid, v.getRequireNo(), 1),
                    URLService.URLServiceAttr.URL_ATTR_TAB_PT, v.getPtSid()));
            viewResult.add(v);
        }
        // 解析資料-結束
        usageRecord.parserDataEnd();

        return viewResult;
    }
}
