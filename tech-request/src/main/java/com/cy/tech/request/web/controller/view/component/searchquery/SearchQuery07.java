/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.component.searchquery;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.joda.time.LocalDate;

import com.cy.tech.request.logic.enumerate.DateType;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.service.CategorySettingService;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepService;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireFinishCodeType;
import com.cy.tech.request.vo.enums.Search07QueryColumn;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.view.component.CategoryTreeComponent;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterArrayStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterDetailVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterVO;
import com.cy.tech.request.web.controller.view.vo.Search07QueryVO;
import com.cy.tech.request.web.listener.CategoryTreeCallBack;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Slf4j
public class SearchQuery07 implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8391368120295587747L;
    @Getter
    private Search07QueryVO search07QueryVO;
    @Getter
    private Search07QueryVO search07QueryVOCustomerFilter;

    // @Getter
    /** 報表 組織樹 Component */
    // private ReportOrgTreeComponent orgTreeComponent;
    // @Getter
    /** 報表 組織樹 Component */
    // private ReportOrgTreeComponent defaultOrgTreeComponent;
    @Getter
    /** 類別樹 Component */
    private CategoryTreeComponent categoryTreeComponent;
    @Getter
    /** 類別樹 Component */
    private CategoryTreeComponent defaultCategoryTreeComponent;

    private MessageCallBack messageCallBack;

    /** 登入者Sid */
    private Integer loginUserSid;

    private DisplayController display;

    public Set<Integer> defaultDepSids;

    /** 該登入者在需求單,自訂查詢條件List */
    private List<ReportCustomFilterVO> reportCustomFilterVOs;
    /** 該登入者在需求單,挑選自訂查詢條件物件 */
    private ReportCustomFilterVO selReportCustomFilterVO;
    /** ReportCustomFilterLogicComponent */
    private ReportCustomFilterLogicComponent reportCustomFilterLogicComponent;
    /** CategorySettingService */
    private CategorySettingService categorySettingService;

    public SearchQuery07(String compId, SearchHelper helper,
            UserService userService,
            ReportCustomFilterLogicComponent reportCustomFilterLogicComponent,
            CategorySettingService categorySettingService,
            MessageCallBack messageCallBack,
            DisplayController display,
            Integer loginUserSid) {
        this.loginUserSid = loginUserSid;
        this.reportCustomFilterLogicComponent = reportCustomFilterLogicComponent;
        this.categorySettingService = categorySettingService;
        this.messageCallBack = messageCallBack;
        this.search07QueryVO = new Search07QueryVO();
        this.search07QueryVOCustomerFilter = new Search07QueryVO();
        this.display = display;
        // String compId,

        this.categoryTreeComponent = new CategoryTreeComponent(categoryTreeCallBack);
        this.defaultCategoryTreeComponent = new CategoryTreeComponent(defaultCategoryTreeCallBack);
    }

    private void initSearchQueryVODefautSetting(Search07QueryVO searchQueryVO) {
        searchQueryVO.publicConditionInit();
        searchQueryVO.setSelectBigCategory(null);
        searchQueryVO.getBigDataCateSids().clear();
        searchQueryVO.getMiddleDataCateSids().clear();
        searchQueryVO.getSmallDataCateSids().clear();
        searchQueryVO.setFuzzyText("");
        searchQueryVO.setDateTypeIndexStr("");
        searchQueryVO.setDateTypeIndex(ReportType.ASSIGN.getDateType().ordinal());

        // 預設查詢部門
        RequireConfirmDepService requireConfirmDepService = WorkSpringContextHolder.getBean(RequireConfirmDepService.class);
        // 計算通知單位相關部門
        searchQueryVO.setRequireDepts(Lists.newArrayList(requireConfirmDepService.prepareUserMatchDepSids(loginUserSid)));

        searchQueryVO.getReqFinishCode().clear();
        searchQueryVO.getReqFinishCode().add("未完成");
        searchQueryVO.getReqFinishCode().add("已完成");
        searchQueryVO.getReqFinishCode().add("終止");
        searchQueryVO.getSelectedColseCode().clear();
        searchQueryVO.getSelectedColseCode().add(String.valueOf("0"));
        searchQueryVO.setSelectReadRecordType(null);
        searchQueryVO.getSelectReqToBeReadType().clear();
        searchQueryVO.setDateTimeType(Search07QueryColumn.UPDATE_DATE);

        for (ReqToBeReadType each : ReqToBeReadType.values()) {
            searchQueryVO.getSelectReqToBeReadType().add(each.name());
        }

        searchQueryVO.setOwnerName(null);
        searchQueryVO.setReqConfirmDepProgStatus(null);
    }

    public void initSetting() {
        initSearchQueryVODefautSetting(search07QueryVO);
        getDateInterval(ReportType.ASSIGN.getDateType().name());
        search07QueryVO.setStartDate(null);
        search07QueryVO.setEndDate(null);
    }

    public void loadDefaultSettingDialog() {
        initSearchQueryVODefautSetting(search07QueryVOCustomerFilter);
        loadSettingData(search07QueryVOCustomerFilter);
    }

    public void loadSetting() {
        initSearchQueryVODefautSetting(search07QueryVO);
        loadSettingData(search07QueryVO);
        categoryTreeComponent.init();
        categoryTreeComponent.selectedItem(search07QueryVO.getSmallDataCateSids());
        categoryTreeCallBack.confirmSelCate();
    }

    public void saveDefaultSetting() {
        try {
            ReportCustomFilterDetailVO demandType = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(
                    Search07QueryColumn.DemandType,
                    (search07QueryVOCustomerFilter.getSelectBigCategory() != null) ? search07QueryVOCustomerFilter.getSelectBigCategory().getSid() : "");

            List<String> requireDepts = Lists.newArrayList();
            if (WkStringUtils.notEmpty(search07QueryVOCustomerFilter.getAssignDepSids())) {
                requireDepts = search07QueryVOCustomerFilter.getAssignDepSids().stream()
                        .map(depSid -> depSid + "")
                        .collect(Collectors.toList());
            }

            ReportCustomFilterDetailVO department = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(
                    Search07QueryColumn.Department,
                    requireDepts);

            ReportCustomFilterDetailVO finishCode = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(
                    Search07QueryColumn.FinishCode,
                    search07QueryVOCustomerFilter.getReqFinishCode());

            ReportCustomFilterDetailVO closeStatus = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(
                    Search07QueryColumn.CloseStatus,
                    search07QueryVOCustomerFilter.getSelectedColseCode());

            ReportCustomFilterDetailVO readStatus = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(
                    Search07QueryColumn.ReadStatus,
                    (search07QueryVOCustomerFilter.getSelectReadRecordType() != null) ? search07QueryVOCustomerFilter.getSelectReadRecordType().name() : "");

            ReportCustomFilterDetailVO waitReadReson = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(
                    Search07QueryColumn.WaitReadReson,
                    search07QueryVOCustomerFilter.getSelectReqToBeReadType());

            ReportCustomFilterDetailVO categoryCombo = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(
                    Search07QueryColumn.CategoryCombo,
                    search07QueryVOCustomerFilter.getSmallDataCateSids());

            ReportCustomFilterDetailVO dateIndex = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(
                    Search07QueryColumn.DateIndex,
                    search07QueryVOCustomerFilter.getDateTypeIndexStr());

            ReportCustomFilterDetailVO searchText = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(
                    Search07QueryColumn.SearchText,
                    search07QueryVOCustomerFilter.getFuzzyText());

            ReportCustomFilterDetailVO dateTimeType = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(
                    Search07QueryColumn.DateTimeType,
                    search07QueryVOCustomerFilter.getDateTimeType().name());

            List<ReportCustomFilterDetailVO> saveDetails = Lists.newArrayList();
            saveDetails.add(demandType);
            saveDetails.add(department);
            saveDetails.add(finishCode);
            saveDetails.add(closeStatus);
            saveDetails.add(readStatus);
            saveDetails.add(waitReadReson);
            saveDetails.add(categoryCombo);
            saveDetails.add(dateIndex);
            saveDetails.add(searchText);
            saveDetails.add(dateTimeType);
            reportCustomFilterLogicComponent.saveReportCustomFilter(Search07QueryColumn.Search07Query, loginUserSid,
                    (selReportCustomFilterVO != null) ? selReportCustomFilterVO.getIndex() : null, true, saveDetails);
            loadSetting();
            display.update("headerTitle");
            display.execute("doSearchData();");
            display.hidePfWidgetVar("dlgReportCustomFilter");
        } catch (Exception e) {
            this.messageCallBack.showMessage(e.getMessage());
            log.error("saveDefaultSetting ERROR", e);
        }
    }

    /** 載入挑選的自訂搜尋條件 */
    public void loadSettingData(Search07QueryVO searchQueryVO) {
        reportCustomFilterVOs = reportCustomFilterLogicComponent.getReportCustomFilter(Search07QueryColumn.Search07Query, loginUserSid);
        if (reportCustomFilterVOs == null || reportCustomFilterVOs.isEmpty()) {
            return;
        }
        selReportCustomFilterVO = reportCustomFilterVOs.get(0);
        selReportCustomFilterVO.getReportCustomFilterDetailVOs().forEach(item -> {
            if (Search07QueryColumn.DemandType.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDemandType(item, searchQueryVO);
            } else if (Search07QueryColumn.Department.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingDepartment(item, searchQueryVO);
            } else if (Search07QueryColumn.FinishCode.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingFinishCode(item, searchQueryVO);
            } else if (Search07QueryColumn.CloseStatus.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingCloseStatus(item, searchQueryVO);
            } else if (Search07QueryColumn.ReadStatus.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingReadStatus(item, searchQueryVO);
            } else if (Search07QueryColumn.WaitReadReson.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingWaitReadReson(item, searchQueryVO);
            } else if (Search07QueryColumn.CategoryCombo.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingCategoryCombo(item, searchQueryVO);
            } else if (Search07QueryColumn.DateIndex.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDateIndex(item, searchQueryVO);
            } else if (Search07QueryColumn.SearchText.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingFuzzyText(item, searchQueryVO);
            } else if (Search07QueryColumn.DateTimeType.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) item;
                searchQueryVO.setDateTimeType(Search07QueryColumn.valueOf(reportCustomFilterStringVO.getValue()));
            }
        });
    }

    /**
     * 塞入需求類別
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandType(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search07QueryVO searchQueryVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                searchQueryVO.setSelectBigCategory(categorySettingService.findBigBySid(reportCustomFilterStringVO.getValue()));
            }
        } catch (Exception e) {
            log.error("settingDemandType", e);
        }
    }

    /**
     * 塞入被分派單位
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDepartment(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search07QueryVO searchQueryVO) {
        try {

            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (WkStringUtils.notEmpty(reportCustomFilterArrayStringVO.getArrayStrings())) {
                searchQueryVO.getAssignDepSids().clear();
                for (String depSidStr : reportCustomFilterArrayStringVO.getArrayStrings()) {
                    if (WkStringUtils.isNumber(depSidStr)) {
                        searchQueryVO.getAssignDepSids().add(Integer.valueOf(depSidStr));
                    }
                }
            }
        } catch (Exception e) {
            log.error("settingDepartment", e);
        }
    }

    /**
     * 塞入完成碼
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingFinishCode(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search07QueryVO searchQueryVO) {
        try {
            searchQueryVO.getReqFinishCode().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQueryVO.getReqFinishCode().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingDepartment", e);
        }
    }

    /**
     * 塞入結案狀態
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingCloseStatus(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search07QueryVO searchQueryVO) {
        try {
            searchQueryVO.getSelectedColseCode().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQueryVO.getSelectedColseCode().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingDepartment", e);
        }
    }

    /**
     * 塞入是否閱讀：
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingReadStatus(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search07QueryVO searchQueryVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                searchQueryVO.setSelectReadRecordType(ReadRecordType.valueOf(reportCustomFilterStringVO.getValue()));
            }
        } catch (Exception e) {
            log.error("settingDemandSource", e);
        }
    }

    /**
     * 塞入待閱原因
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingWaitReadReson(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search07QueryVO searchQueryVO) {
        try {
            searchQueryVO.getSelectReqToBeReadType().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQueryVO.getSelectReqToBeReadType().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingDepartment", e);
        }
    }

    /**
     * 塞入需求類別
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingCategoryCombo(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search07QueryVO searchQueryVO) {
        try {
            searchQueryVO.getSmallDataCateSids().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQueryVO.getSmallDataCateSids().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingCategoryCombo", e);
        }
    }

    /**
     * 塞入立單區間Type
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDateIndex(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search07QueryVO searchQueryVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQueryVO.setDateTypeIndexStr(reportCustomFilterStringVO.getValue());
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                searchQueryVO.setDateTypeIndex(Integer.valueOf(reportCustomFilterStringVO.getValue()));
            }

            if (searchQueryVO.equals(search07QueryVO)) {
                if (DateType.PREVIOUS_MONTH.ordinal() == search07QueryVO.getDateTypeIndex()) {
                    getDateInterval(DateType.PREVIOUS_MONTH.name());
                } else if (DateType.THIS_MONTH.ordinal() == search07QueryVO.getDateTypeIndex()) {
                    getDateInterval(DateType.THIS_MONTH.name());
                } else if (DateType.NEXT_MONTH.ordinal() == search07QueryVO.getDateTypeIndex()) {
                    getDateInterval(DateType.NEXT_MONTH.name());
                } else if (DateType.TODAY.ordinal() == search07QueryVO.getDateTypeIndex()) {
                    getDateInterval(DateType.TODAY.name());
                }
            }
        } catch (Exception e) {
            log.error("settingFuzzyText", e);
        }
    }

    /**
     * 塞入模糊搜尋
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingFuzzyText(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search07QueryVO searchQueryVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQueryVO.setFuzzyText(reportCustomFilterStringVO.getValue());
        } catch (Exception e) {
            log.error("settingFuzzyText", e);
        }
    }

    public List<String> getToBeReadTypeQuery() {
        if (search07QueryVO.getSelectReqToBeReadType() == null || search07QueryVO.getSelectReqToBeReadType().isEmpty()) {
            return Lists.newArrayList();
        }
        List<String> queryResult = search07QueryVO.getSelectReqToBeReadType().stream()
                .collect(Collectors.toList());
        if (queryResult.contains(ReqToBeReadType.NO_TO_BE_READ.name())) {
            queryResult.set(queryResult.indexOf(ReqToBeReadType.NO_TO_BE_READ.name()), "");
        }
        return queryResult;
    }

    public List<String> findFilterFinishCodes() {
        List<String> filterResult = Lists.newArrayList();
        if (search07QueryVO.getReqFinishCode() == null || search07QueryVO.getReqFinishCode().isEmpty()) {
            filterResult.add("");
            return filterResult;
        }
        for (RequireFinishCodeType each : RequireFinishCodeType.values()) {
            if (search07QueryVO.getReqFinishCode().contains(each.getCodeName())) {
                filterResult.add(each.getCode());
            }
        }
        return filterResult;
    }

    public List<String> findFilterAllFinishCodes() {
        List<String> filterResult = Lists.newArrayList();
        for (RequireFinishCodeType each : RequireFinishCodeType.values()) {
            if (Lists.newArrayList("未完成", "已完成", "終止").contains(each.getCodeName())) {
                filterResult.add(each.getCode());
            }
        }
        return filterResult;
    }

    /**
     * 取得日期區間
     *
     * @param dateType
     */
    public void getDateInterval(String dateType) {
        if (DateType.PREVIOUS_MONTH.name().equals(dateType)) {
            if (search07QueryVO.getStartDate() == null) {
                search07QueryVO.setStartDate(new Date());
            }
            LocalDate lastDate = new LocalDate(search07QueryVO.getStartDate()).minusMonths(1);
            search07QueryVO.setStartDate(lastDate.dayOfMonth().withMinimumValue().toDate());
            search07QueryVO.setEndDate(lastDate.dayOfMonth().withMaximumValue().toDate());
            search07QueryVO.setDateTypeIndex(DateType.PREVIOUS_MONTH.ordinal());
        } else if (DateType.THIS_MONTH.name().equals(dateType)) {
            search07QueryVO.setStartDate(new LocalDate().dayOfMonth().withMinimumValue().toDate());
            search07QueryVO.setEndDate(new LocalDate().dayOfMonth().withMaximumValue().toDate());
            search07QueryVO.setDateTypeIndex(DateType.THIS_MONTH.ordinal());
        } else if (DateType.NEXT_MONTH.name().equals(dateType)) {
            if (search07QueryVO.getStartDate() == null) {
                search07QueryVO.setStartDate(new Date());
            }
            LocalDate nextDate = new LocalDate(search07QueryVO.getStartDate()).plusMonths(1);
            search07QueryVO.setStartDate(nextDate.dayOfMonth().withMinimumValue().toDate());
            search07QueryVO.setEndDate(nextDate.dayOfMonth().withMaximumValue().toDate());
            search07QueryVO.setDateTypeIndex(DateType.NEXT_MONTH.ordinal());
        } else if (DateType.TODAY.name().equals(dateType)) {
            search07QueryVO.setStartDate(new Date());
            search07QueryVO.setEndDate(new Date());
            search07QueryVO.setDateTypeIndex(DateType.TODAY.ordinal());
        } else if (DateType.NULL.name().equals(dateType)) {
            search07QueryVO.setStartDate(null);
            search07QueryVO.setEndDate(null);
            search07QueryVO.setDateTypeIndex(DateType.NULL.ordinal());
        }
    }

    /**
     * 開啟 類別樹
     */
    public void btnOpenCategoryTree() {
        try {
            categoryTreeComponent.init();
            categoryTreeComponent.selectedItem(search07QueryVO.getSmallDataCateSids());
            display.showPfWidgetVar("dlgCate");
        } catch (Exception e) {
            log.error("btnOpenCategoryTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 開啟 類別樹
     */
    public void btnOpenDefaultCategoryTree() {
        try {
            defaultCategoryTreeComponent.init();
            defaultCategoryTreeComponent.selectedItem(search07QueryVOCustomerFilter.getSmallDataCateSids());
            display.showPfWidgetVar("defaultDlgCate");
        } catch (Exception e) {
            log.error("btnOpenDefaultCategoryTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void clearDateType() {
        if ("4".equals(search07QueryVOCustomerFilter.getDateTypeIndexStr())) {
            search07QueryVOCustomerFilter.setDateTypeIndexStr("");
        }
    }

    /** 類別樹 Component CallBack */
    private final CategoryTreeCallBack defaultCategoryTreeCallBack = new CategoryTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 3729922573504890046L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelCate() {
            if (defaultCategoryTreeComponent.getSelCateNodes() == null
                    || defaultCategoryTreeComponent.getSelCateNodes().length == 0) {
                search07QueryVOCustomerFilter.setBigDataCateSids(Lists.newArrayList());
                search07QueryVOCustomerFilter.setMiddleDataCateSids(Lists.newArrayList());
                search07QueryVOCustomerFilter.setSmallDataCateSids(Lists.newArrayList());
                return;
            }
            defaultCategoryTreeComponent.selCate();
            search07QueryVOCustomerFilter.setBigDataCateSids(defaultCategoryTreeComponent.getBigDataCateSids());
            search07QueryVOCustomerFilter.setMiddleDataCateSids(defaultCategoryTreeComponent.getMiddleDataCateSids());
            search07QueryVOCustomerFilter.setSmallDataCateSids(defaultCategoryTreeComponent.getSmallDataCateSids());
            defaultCategoryTreeComponent.clearCateSids();
        }

        @Override
        public void loadSelCate(List<String> smallDataCateSids) {
        }

        @Override
        public void actionSelectAll() {
        }

        @Override
        public void onNodeSelect() {
        }

        @Override
        public void onNodeUnSelect() {
        }
    };

    /** 類別樹 Component CallBack */
    private final CategoryTreeCallBack categoryTreeCallBack = new CategoryTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 496762731758778357L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelCate() {
            if (categoryTreeComponent.getSelCateNodes() == null
                    || categoryTreeComponent.getSelCateNodes().length == 0) {
                search07QueryVO.setBigDataCateSids(Lists.newArrayList());
                search07QueryVO.setMiddleDataCateSids(Lists.newArrayList());
                search07QueryVO.setSmallDataCateSids(Lists.newArrayList());
                return;
            }
            categoryTreeComponent.selCate();
            search07QueryVO.setBigDataCateSids(categoryTreeComponent.getBigDataCateSids());
            search07QueryVO.setMiddleDataCateSids(categoryTreeComponent.getMiddleDataCateSids());
            search07QueryVO.setSmallDataCateSids(categoryTreeComponent.getSmallDataCateSids());
            categoryTreeComponent.clearCateSids();
        }

        @Override
        public void loadSelCate(List<String> smallDataCateSids) {
        }

        @Override
        public void actionSelectAll() {
        }

        @Override
        public void onNodeSelect() {
        }

        @Override
        public void onNodeUnSelect() {
        }
    };

}
