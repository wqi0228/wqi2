/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.view.to.search.query;

import java.io.Serializable;
import java.util.List;

import com.cy.commons.vo.Org;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.vo.enums.Search15QueryColumn;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.view.component.CategoryTreeComponent;
import com.cy.tech.request.web.controller.view.component.ReportOrgTreeComponent;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterArrayStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterDetailVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterVO;
import com.cy.tech.request.web.listener.CategoryTreeCallBack;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.listener.ReportCustomFilterCallback;
import com.cy.tech.request.web.listener.ReportOrgTreeCallBack;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.cache.WkOrgCache;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
public class SearchQuery15CustomFilter implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2433350192223252685L;
    /** 登入者Sid */
    private Integer loginUserSid;
    /** 登入者部門UserSid */
    private Integer loginDepSid;
    /** 登入者公司Sid */
    private Integer loginCompSid;
    @Getter
    private SearchQuery15 tempSearchQuery15;
    /** 該登入者在需求單,自訂查詢條件List */
    private List<ReportCustomFilterVO> reportCustomFilterVOs;
    /** 該登入者在需求單,挑選自訂查詢條件物件 */
    private ReportCustomFilterVO selReportCustomFilterVO;
    /** ReportCustomFilterLogicComponent */
    private ReportCustomFilterLogicComponent reportCustomFilterLogicComponent;
    /** 類別樹 Component */
    @Getter
    private CategoryTreeComponent categoryTreeComponent;
    /** MessageCallBack */
    private MessageCallBack messageCallBack;
    /** DisplayController */
    private DisplayController display;
    /** ReportCustomFilterCallback */
    private ReportCustomFilterCallback reportCustomFilterCallback;
    @Getter
    /** 報表 組織樹 Component */
    private ReportOrgTreeComponent orgTreeComponent;

    @Getter
    /** 報表 組織樹 Component */
    private ReportOrgTreeComponent sendTestorgTreeComponent;

    /** 是否有送測異動報表閱覽者角色 */
    private boolean hasSTHistoryViewer = false;

    public SearchQuery15CustomFilter(String compId, Integer loginUserSid, Integer loginDepSid, Integer loginCompSid,
            ReportCustomFilterLogicComponent reportCustomFilterLogicComponent, MessageCallBack messageCallBack,
            DisplayController display,
            ReportCustomFilterCallback reportCustomFilterCallback,
            boolean hasSTHistoryViewer, 
            List<Long> roleSids, 
            Org dep) {
        this.loginUserSid = loginUserSid;
        this.loginDepSid = loginDepSid;
        this.loginCompSid = loginCompSid;
        this.reportCustomFilterLogicComponent = reportCustomFilterLogicComponent;
        this.messageCallBack = messageCallBack;
        this.display = display;
        this.categoryTreeComponent = new CategoryTreeComponent(categoryTreeCallBack);
        this.reportCustomFilterCallback = reportCustomFilterCallback;
        this.tempSearchQuery15 = new SearchQuery15(ReportType.WORK_TEST_HISTORY);
        this.hasSTHistoryViewer = hasSTHistoryViewer;
        this.orgTreeComponent = new ReportOrgTreeComponent(
                compId, dep, roleSids, true, reportOrgTreeCallBack);
        this.sendTestorgTreeComponent = new ReportOrgTreeComponent(
                compId, dep, roleSids, false, reportOrgTreeNoticeCallBack);
    }

    public void loadDefaultSetting(List<String> noticeDepts, SearchQuery15 searchQuery) {
        this.initDefault(noticeDepts, searchQuery);
        reportCustomFilterVOs = reportCustomFilterLogicComponent.getReportCustomFilter(Search15QueryColumn.Search15Query, loginUserSid);
        if (reportCustomFilterVOs != null && !reportCustomFilterVOs.isEmpty()) {
            selReportCustomFilterVO = reportCustomFilterVOs.get(0);
            loadSettingData(searchQuery);
        }
    }

    /** 載入挑選的自訂搜尋條件 */
    public void loadSettingData(SearchQuery15 searchQuery) {
        selReportCustomFilterVO.getReportCustomFilterDetailVOs().forEach(item -> {
            if (Search15QueryColumn.DemandType.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDemandType(item, searchQuery);
            } else if (Search15QueryColumn.DemandDep.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingDemandDep(item, searchQuery);
            } else if (Search15QueryColumn.SendTestDep.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingSendTestDep(item, searchQuery);
            } else if (Search15QueryColumn.SearchText.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingFuzzyText(item, searchQuery);
            } else if (Search15QueryColumn.SendTestNo.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingSendTestNo(item, searchQuery);
            } else if (Search15QueryColumn.CategoryCombo.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingCategoryCombo(item, searchQuery);
            } else if (Search15QueryColumn.SendTestStatus.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingSendTestStatus(item, searchQuery);
            } else if (Search15QueryColumn.DateIndex.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDateIndex(item, searchQuery);
            }
        });
    }

    /**
     * 塞入送測狀態
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingSendTestStatus(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery15 searchQuery) {
        try {
            searchQuery.getTestStatus().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQuery.getTestStatus().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingCategoryCombo", e);
        }
    }

    /**
     * 塞入立單區間Type
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDateIndex(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery15 searchQuery) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQuery.setDateTypeIndex(Integer.valueOf(reportCustomFilterStringVO.getValue()));

            switch (searchQuery.getDateTypeIndex()) {
                case 0:
                    searchQuery.setStartDate(null);
                    searchQuery.setEndDate(null);
                    searchQuery.changeDateIntervalPreMonth();
                    break;
                case 1:
                    searchQuery.setStartDate(null);
                    searchQuery.setEndDate(null);
                    searchQuery.changeDateIntervalThisMonth();
                    break;
                case 2:
                    searchQuery.setStartDate(null);
                    searchQuery.setEndDate(null);
                    searchQuery.changeDateIntervalNextMonth();
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            log.error("settingFuzzyText", e);
        }
    }

    /**
     * 塞入需求類別
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingCategoryCombo(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery15 searchQuery) {
        try {
            searchQuery.getSmallDataCateSids().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQuery.getSmallDataCateSids().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingCategoryCombo", e);
        }
    }

    /**
     * 塞入送測單號
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingSendTestNo(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery15 searchQuery) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQuery.setTestinfoNo(reportCustomFilterStringVO.getValue());
        } catch (Exception e) {
            log.error("settingFuzzyText", e);
        }
    }

    /**
     * 塞入模糊搜尋
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingFuzzyText(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery15 searchQuery) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQuery.setFuzzyText(reportCustomFilterStringVO.getValue());
        } catch (Exception e) {
            log.error("settingFuzzyText", e);
        }
    }

    /**
     * 塞入送測單位
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingSendTestDep(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery15 searchQuery) {
        try {
            searchQuery.getNoticeDepts().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQuery.getNoticeDepts().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingSendTestDep", e);
        }
    }

    /**
     * 塞入填單單位
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandDep(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery15 searchQuery) {
        try {
            searchQuery.getRequireDepts().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQuery.getRequireDepts().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingDemandType", e);
        }
    }

    /**
     * 塞入需求類別
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandType(ReportCustomFilterDetailVO reportCustomFilterDetailVO, SearchQuery15 searchQuery) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQuery.setSelectBigCategorySid(reportCustomFilterStringVO.getValue());
        } catch (Exception e) {
            log.error("settingDemandType", e);
        }
    }

    /** 儲存自訂搜尋條件 */
    public void saveReportCustomFilter() {
        try {
            ReportCustomFilterDetailVO demandType = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search15QueryColumn.DemandType, (tempSearchQuery15.getSelectBigCategorySid() != null) ? tempSearchQuery15.getSelectBigCategorySid() : "");
            ReportCustomFilterDetailVO demandDep = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search15QueryColumn.DemandDep, tempSearchQuery15.getRequireDepts());
            ReportCustomFilterDetailVO sendTestDep = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search15QueryColumn.SendTestDep, tempSearchQuery15.getNoticeDepts());
            ReportCustomFilterDetailVO searchText = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search15QueryColumn.SearchText, (tempSearchQuery15.getFuzzyText() != null) ? tempSearchQuery15.getFuzzyText() : "");
            ReportCustomFilterDetailVO sendTestNo = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search15QueryColumn.SendTestNo, (tempSearchQuery15.getTestinfoNo() != null) ? tempSearchQuery15.getTestinfoNo() : "");
            ReportCustomFilterDetailVO categoryCombo = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search15QueryColumn.CategoryCombo, tempSearchQuery15.getSmallDataCateSids());
            ReportCustomFilterDetailVO sendTestStatus = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search15QueryColumn.SendTestStatus, tempSearchQuery15.getTestStatus());
            ReportCustomFilterDetailVO dateIndex = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search15QueryColumn.DateIndex, (tempSearchQuery15.getDateTypeIndex() != null) ? String.valueOf(tempSearchQuery15.getDateTypeIndex()) : "5");
            List<ReportCustomFilterDetailVO> saveDetails = Lists.newArrayList();
            saveDetails.add(demandType);
            saveDetails.add(demandDep);
            saveDetails.add(sendTestDep);
            saveDetails.add(searchText);
            saveDetails.add(sendTestNo);
            saveDetails.add(categoryCombo);
            saveDetails.add(sendTestStatus);
            saveDetails.add(dateIndex);
            reportCustomFilterLogicComponent.saveReportCustomFilter(Search15QueryColumn.Search15Query, loginUserSid, (selReportCustomFilterVO != null) ? selReportCustomFilterVO.getIndex() : null, true, saveDetails);
            reportCustomFilterCallback.reloadDefault("");
            display.update("headerTitle");
            display.execute("doSearchData();");
            display.hidePfWidgetVar("dlgReportCustomFilter");
        } catch (Exception e) {
            this.messageCallBack.showMessage(e.getMessage());
            log.error("saveReportCustomFilter ERROR", e);
        }
    }

    public void initDefault(List<String> noticeDepts, SearchQuery15 searchQuery) {
        searchQuery.clear(noticeDepts);
    }

    /** 類別樹 Component CallBack */
    private final CategoryTreeCallBack categoryTreeCallBack = new CategoryTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 5061247659654182280L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelCate() {
            categoryTreeComponent.selCate();
            tempSearchQuery15.getBigDataCateSids().clear();
            tempSearchQuery15.getMiddleDataCateSids().clear();
            tempSearchQuery15.getSmallDataCateSids().clear();
            tempSearchQuery15.getBigDataCateSids().addAll(Lists.newArrayList(categoryTreeComponent.getBigDataCateSids()));
            tempSearchQuery15.getMiddleDataCateSids().addAll(Lists.newArrayList(categoryTreeComponent.getMiddleDataCateSids()));
            tempSearchQuery15.getSmallDataCateSids().addAll(Lists.newArrayList(categoryTreeComponent.getSmallDataCateSids()));
        }
    };

    /**
     * 開啟 類別樹
     */
    public void btnOpenCategoryTree() {
        try {
            categoryTreeComponent.init();
            categoryTreeComponent.selectedItem(tempSearchQuery15.getSmallDataCateSids());
            display.showPfWidgetVar("defaultDlgCate");
        } catch (Exception e) {
            log.error("btnOpenCategoryTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /** 清除立單區間Type */
    public void clearDateType() {
        if (3 == tempSearchQuery15.getDateTypeIndex()) {
            tempSearchQuery15.setDateTypeIndex(4);
        }
    }

    /**
     * 開啟 單位挑選 組織樹
     */
    public void btnOpenOrgTree() {
        try {
            Org dep = WkOrgCache.getInstance().findBySid(loginDepSid);
            Org comp = WkOrgCache.getInstance().findBySid(loginCompSid);
            if (this.hasSTHistoryViewer) {
                orgTreeComponent.initOrgTree(comp, tempSearchQuery15.getRequireDepts(), false, false);
            } else {
                orgTreeComponent.initOrgTree(dep, tempSearchQuery15.getRequireDepts(), false, false);
            }
            display.showPfWidgetVar("defaultdlgOrgTree");
        } catch (Exception e) {
            log.error("btnOpenOrgTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void btnOpenNoticeOrgTree() {
        try {
            Org dep = WkOrgCache.getInstance().findBySid(loginDepSid);
            Org comp = WkOrgCache.getInstance().findBySid(loginCompSid);
            if (this.hasSTHistoryViewer) {
                sendTestorgTreeComponent.initOrgTree(comp, tempSearchQuery15.getNoticeDepts(), false, false);
            } else {
                sendTestorgTreeComponent.initOrgTree(dep, tempSearchQuery15.getNoticeDepts(), false, false);
            }
            display.showPfWidgetVar("defaultdlgOrgTreeNotice");
        } catch (Exception e) {
            log.error("btnOpenNoticeOrgTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /** 報表 組織樹 Component CallBack */
    private final ReportOrgTreeCallBack reportOrgTreeCallBack = new ReportOrgTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -8431146052293987564L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelOrg() {
            tempSearchQuery15.getRequireDepts().clear();
            tempSearchQuery15.getRequireDepts().addAll(orgTreeComponent.getSelOrgSids());
        }
    };

    /** 報表 組織樹 Component CallBack */
    private final ReportOrgTreeCallBack reportOrgTreeNoticeCallBack = new ReportOrgTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 8870050193215235970L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelOrg() {
            tempSearchQuery15.setNoticeDepts(sendTestorgTreeComponent.getSelOrgSids());
        }
    };

}
