/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.jsf.converter;

import com.cy.tech.request.logic.service.customer.ReqWorkCustomerHelper;
import com.cy.tech.request.web.controller.view.WorkCustomerView;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 客戶資料 轉換器
 *
 * @author kasim
 */
@Component("customerSimpleConverter")
public class CustomerSimpleConverter implements Converter {

    @Autowired
    transient private ReqWorkCustomerHelper service;
    @Autowired
    transient private WkStringUtils stringUtils;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (Strings.isNullOrEmpty(value) || !stringUtils.isNumeric(value)) {
            return null;
        }
        return service.findCopyOne(Long.valueOf(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof WorkCustomerView) {
            return String.valueOf(((WorkCustomerView) value).getSid());
        }
        return null;
    }
}
