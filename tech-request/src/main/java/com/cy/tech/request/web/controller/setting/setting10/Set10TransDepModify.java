/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.setting.setting10;

import com.cy.commons.vo.Org;
import com.cy.tech.request.logic.service.AlertInboxService;
import com.cy.tech.request.logic.vo.TransDepBacksageTo;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.web.controller.setting.Setting10MBean;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 需求單異動轉寄資訊後台
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope("view")
public class Set10TransDepModify implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 213072137246969052L;
    @Autowired
    transient private AlertInboxService aiSvc;
    @Autowired
    transient private Setting10MBean set10Bean;

    /** 需求製作進度 */
    @Getter
    @Setter
    private List<String> selReqTypes;
    /** 轉換單號 */
    @Getter
    @Setter
    private String transReqNos;
    /** 來源對應部門 */
    @Getter
    @Setter
    private Org sourceMappOrg;
    /** 目標新增部門 */
    @Getter
    @Setter
    private Org targetMappOrg;
    /** hide 執行轉寄數量查詢 */
    @Getter
    @Setter
    private Boolean showExecute;
    /** hide 執行轉換 */
    @Getter
    @Setter
    private Boolean showDownloadLog;
    /** 下載用記錄檔 */
    @Getter
    private StreamedContent file;
    /** 新增轉寄物件 */
    private List<TransDepBacksageTo> doTransItems;

    /** 還原部門 */
    @Getter
    @Setter
    private Org recoveryOrg;
    /** 還原對應收件群組ID */
    @Getter
    @Setter
    private String recoveryAlertGroupSids;
    /** 還原轉寄物件 */
    private List<TransDepBacksageTo> doRecoveryItems;

    @PostConstruct
    void init() {
        showDownloadLog = false;
        showExecute = false;
        selReqTypes = Lists.newArrayList(RequireStatusType.PROCESS.name());
    }

    /**
     * 計算執行數量
     */
    public void countExecute() {
        if (this.isNotDownloadRec()) {
            return;
        }
        set10Bean.clearInfoScreenMsg();
        if (this.hasProblemInput()) {
            return;
        }
        List<String> reqNos = Arrays.asList(transReqNos.replace("\r", "").replace("\n", "").replace(" ", "").split(","));
        doTransItems = aiSvc.findBackstageModifyItems(
                  sourceMappOrg.getSid(),
                  targetMappOrg.getSid(),
                  selReqTypes,
                  reqNos);
        String handlerMsg = aiSvc.htmlHandlerMsg(doTransItems, sourceMappOrg.getSid(), targetMappOrg.getSid(), true);
        set10Bean.addInfoMsg(handlerMsg);
        if (this.notHasModifyItems(doTransItems)) {
            return;
        }
        showExecute = true;
        showDownloadLog = false;
    }

    /**
     * 檢查輸入
     */
    private boolean hasProblemInput() {
        try {
            Preconditions.checkArgument(this.selReqTypes != null && !selReqTypes.isEmpty(), "未選擇需求製作進度");
            Preconditions.checkNotNull(this.sourceMappOrg, "未輸入來源對應部門");
            Preconditions.checkNotNull(this.targetMappOrg, "未輸入目標新增部門");
            Preconditions.checkArgument(!Strings.isNullOrEmpty(this.transReqNos), "未輸入轉換單號");
            Preconditions.checkArgument(!this.sourceMappOrg.equals(this.targetMappOrg), "來源部門不可等同新增部門");
            return false;
        } catch (NullPointerException | IllegalArgumentException e) {
            MessagesUtils.showError(e.getMessage());
            return true;
        }
    }

    /**
     * 檢查是否無處理對象
     *
     * @return
     */
    private boolean notHasModifyItems(List<TransDepBacksageTo> itmes) {
        if (itmes == null || itmes.isEmpty()) {
            MessagesUtils.showError("沒有可處理資料！！");
            return true;
        }
        return false;
    }

    public void downloadHandlerInfo() {
        try {
            String downLoadMsg = aiSvc.downloadHandlerMsg(doTransItems, sourceMappOrg.getSid(), targetMappOrg.getSid());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            String fileName = "BackstageTransDepLog" + sdf.format(new Date()) + ".txt";
            InputStream is = new ByteArrayInputStream(downLoadMsg.getBytes(Charset.forName("UTF-8")));
            is.close();
            file = new DefaultStreamedContent(is, "text/plain", fileName);
            showDownloadLog = false;
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    private boolean isNotDownloadRec() {
        try {
            Preconditions.checkArgument(!this.showDownloadLog, "尚未下載本次記錄！！");
            return false;
        } catch (NullPointerException | IllegalArgumentException e) {
            MessagesUtils.showError(e.getMessage());
            return true;
        }
    }

    /**
     * 執行
     */
    public void execute() {
        set10Bean.clearInfoScreenMsg();
        if (this.hasProblemInput() || this.notHasModifyItems(doTransItems)) {
            return;
        }
        //開始插入轉換資料
        aiSvc.modifyBackstageModifyItems(this.targetMappOrg.getSid(), doTransItems);
        String handlerMsg = aiSvc.htmlHandlerMsg(doTransItems, sourceMappOrg.getSid(), targetMappOrg.getSid(), false);
        set10Bean.addInfoMsg(handlerMsg);
        log.info("執行後台轉寄轉換完成\n" + aiSvc.downloadHandlerMsg(doTransItems, sourceMappOrg.getSid(), targetMappOrg.getSid()));
        showDownloadLog = true;
        showExecute = false;
    }

    public void dropThisExec() {
        showExecute = false;
        showDownloadLog = false;
    }

    /**
     * 計算執行數量
     */
    public void countRecovery() {
        if (this.isNotDownloadRec()) {
            return;
        }
        set10Bean.clearInfoScreenMsg();
        if (this.hasProblemInputOfRecovery()) {
            return;
        }
        List<String> reqNos = Arrays.asList(recoveryAlertGroupSids.replace("\r", "").replace("\n", "").replace(" ", "").split(","));
        doRecoveryItems = aiSvc.findBackstageModifyRecoveryItems(recoveryOrg.getSid(), reqNos);
        String handlerMsg = aiSvc.htmlHandlerMsgByRecovery(doRecoveryItems, recoveryOrg.getSid(), true);
        set10Bean.addInfoMsg(handlerMsg);
        if (this.notHasModifyItems(doRecoveryItems)) {
            return;
        }
        showExecute = true;
        showDownloadLog = false;
    }

    private boolean hasProblemInputOfRecovery() {
        try {
            Preconditions.checkNotNull(this.recoveryOrg, "未輸入還原部門");
            Preconditions.checkArgument(!Strings.isNullOrEmpty(this.recoveryAlertGroupSids), "未輸入還原收件群組ID");
            return false;
        } catch (NullPointerException | IllegalArgumentException e) {
            MessagesUtils.showError(e.getMessage());
            return true;
        }
    }

    /**
     * 執行還原
     */
    public void executeRecovery() {
        set10Bean.clearInfoScreenMsg();
        if (this.hasProblemInputOfRecovery() || this.notHasModifyItems(doRecoveryItems)) {
            return;
        }
        //開始還原轉換資料
        aiSvc.modifyBackstageRecoveryItems(this.recoveryOrg.getSid(), doRecoveryItems);
        String handlerMsg = aiSvc.htmlHandlerMsgByRecovery(doRecoveryItems, recoveryOrg.getSid(), false);
        set10Bean.addInfoMsg(handlerMsg);
        log.info("執行轉換轉寄還原完成\n" + aiSvc.downloadRecoveryMsg(doRecoveryItems, recoveryOrg.getSid()));
        showDownloadLog = true;
        showExecute = false;
    }

    public void downloadRecoveryInfo() {
        try {
            String downLoadMsg = aiSvc.downloadRecoveryMsg(doRecoveryItems, recoveryOrg.getSid());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            String fileName = "BackstageTransDepRecoveryLog" + sdf.format(new Date()) + ".txt";
            InputStream is = new ByteArrayInputStream(downLoadMsg.getBytes(Charset.forName("UTF-8")));
            is.close();
            file = new DefaultStreamedContent(is, "text/plain", fileName);
            showDownloadLog = false;
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }
    }

}
