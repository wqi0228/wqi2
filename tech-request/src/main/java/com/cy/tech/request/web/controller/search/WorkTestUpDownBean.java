/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search;

import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 送測表格上下筆控制
 *
 * @author brain0926_liao
 */
@Controller
@Scope(WebApplicationContext.SCOPE_SESSION)
@Slf4j
public class WorkTestUpDownBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6915731879993808984L;
    transient private Lock lock = new ReentrantLock();
    private boolean upDownWait = true;
    @Getter
    private Boolean disableUp = true;
    @Getter
    private Boolean disableDown = true;
    @Getter
    private Boolean showHome = false;
    @Getter
    @Setter
    private String currRow = "";
    @Getter
    @Setter
    private RequireBottomTabType currTab;
    @Getter
    @Setter
    private String currTabSid;

    @PostConstruct
    void init() {
        lock = new ReentrantLock();
        //log.info("WorkTestUpDownBean - 上下筆傳遞Session產生");
    }

    @PreDestroy
    void destory() {
        //log.info("WorkTestUpDownBean - 銷毀");
    }

    /**
     * 移除控制記錄
     */
    public void clearAllRec() {
        this.disableAll();
        this.clearCurrRow();
        this.clearTabInfo();
        this.resetToWait();
    }

    /**
     * 關閉上下筆及不顯示Home
     */
    public void disableAll() {
        disableUp = true;
        disableDown = true;
        showHome = false;
    }

    /**
     * 上下筆開啟關閉設定
     *
     * @param index
     * @param size
     */
    public void resetUpDown(int index, int size) {
        disableUp = index <= 0;
        disableDown = size <= index + 1;
        //log.debug("disableUp " + disableUp);
        //log.debug("disableDown " + disableDown);
        this.unlockWait();
    }

    /**
     * 上下筆等待解除鎖定
     */
    private void unlockWait() {
        lock.lock();// 得到锁
        try {
            upDownWait = false;
        } catch (Exception e) {
            log.error("setUpDownWaitOver Error", e);
        } finally {
            lock.unlock();// 释放锁
        }
    }

    /**
     * 上下筆是否解除鎖定
     *
     * @return
     */
    private boolean isContinueWait() {
        lock.lock();// 得到锁
        try {
            return this.upDownWait;
        } catch (Exception e) {
            log.error("isContinueWait Error", e);
        } finally {
            lock.unlock();// 释放锁
        }
        return false;
    }

    /**
     * 上下筆解除等待鎖定
     */
    private void finishWait() {
        lock.lock();// 得到锁
        try {
            if (this.upDownWait) {
                log.error("Session 上下筆設定尚未完成,便進行清除");
            }
            this.resetToWait();
        } catch (Exception e) {
            log.error("clearSessionSettingOver Error", e);
        } finally {
            lock.unlock();// 释放锁
        }
    }

    /**
     * 重新設定為等待狀態
     */
    private void resetToWait() {
        upDownWait = true;
    }

    /**
     * 執行上下筆鎖定等待
     */
    public void doWaitUpDown() {
        long startTime = System.currentTimeMillis();
        while (this.isContinueWait()) {
            try {
                TimeUnit.MICROSECONDS.sleep(50);
                if (System.currentTimeMillis() - startTime > 2000) {
                    this.finishWait();
                    log.error("執行上下筆,Session並未等到執行值,最多等待兩秒");
                    break;
                }
            } catch (Exception e) {
                log.error("updown sleep error !!", e);
            }
        }
        this.finishWait();
    }

    /**
     * 檢查當前上下筆Row資訊是否為空值
     *
     * @return
     */
    public boolean isEmptyCurrRow() {
        return Strings.isNullOrEmpty(this.currRow);
    }

    /**
     * 重設定頁籤設定
     *
     * @param type
     * @param tabSid
     */
    public void resetTabInfo(RequireBottomTabType type, String tabSid) {
        currTab = type;
        currTabSid = tabSid;
    }

    /**
     * 頁籤設定是否為空值
     *
     * @return
     */
    public boolean isEmptyTab() {
        return this.currTab == null || Strings.isNullOrEmpty(this.currTabSid);
    }

    /**
     * 建立當前頁籤所需資訊
     *
     * @return
     */
    public Map<String, Object> maybeCreateTabParam() {
        if (this.isEmptyTab()) {
            return null;
        }
        Map<String, Object> urlParam = Maps.newHashMap();
        urlParam.put(ReqLoadBean.TAB_TYPE, this.currTab);
        urlParam.put(ReqLoadBean.TAB_SID, this.currTabSid);
        this.clearTabInfo();
        return urlParam;
    }

    public void clearCurrRow() {
        this.currRow = null;
    }

    /**
     * 清空TAB資訊
     */
    private void clearTabInfo() {
        this.currTab = null;
        this.currTabSid = null;
    }

}
