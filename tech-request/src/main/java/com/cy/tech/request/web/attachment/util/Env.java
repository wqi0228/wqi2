/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.attachment.util;

import java.io.Serializable;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */
@Component
public class Env implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1812901061747342931L;

    @Value(value = "${tech-request.attachment.root}")
    @Getter
    private String attachRootPath;
    
    private static Env instance;

    @Override
    public void afterPropertiesSet() throws Exception {
        Env.instance = this;
    }

    public static Env getInstance() {
        return instance;
    }

    public String getAttachmentRoot() {
        return attachRootPath;
    }
}

