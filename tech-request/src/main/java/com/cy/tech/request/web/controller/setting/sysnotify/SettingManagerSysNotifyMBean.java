package com.cy.tech.request.web.controller.setting.sysnotify;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.helper.component.ComponentHelper;
import com.cy.tech.request.logic.service.customer.ReqWorkCustomerHelper;
import com.cy.tech.request.logic.service.setting.sysnotify.SettingSysNotifyService;
import com.cy.tech.request.vo.enums.CategoryType;
import com.cy.tech.request.vo.setting.sysnotify.vo.SettingManagetSysNotifyVO;
import com.cy.tech.request.web.controller.component.singleselectlist.SingleSelectListCallback;
import com.cy.tech.request.web.controller.component.singleselectlist.SingleSelectListPicker;
import com.cy.tech.request.web.controller.component.singleselectlist.SingleSelectListPickerConfig;
import com.cy.tech.request.web.controller.component.singleselecttree.SingleSelectTreeCallback;
import com.cy.tech.request.web.controller.component.singleselecttree.SingleSelectTreePicker;
import com.cy.tech.request.web.controller.component.singleselecttree.SingleSelectTreePickerConfig;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.customer.vo.WorkCustomer;
import com.cy.work.customer.vo.enums.EnableType;
import com.cy.work.notify.vo.enums.NotifyFilterType;
import com.cy.work.notify.vo.enums.NotifyType;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 *
 */
@Slf4j
@Controller
@Scope("view")
public class SettingManagerSysNotifyMBean implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -6115515478788438679L;
    // ========================================================================
	// 服務
	// ========================================================================
	@Autowired
	private transient SettingSysNotifyService settingSysNotifyService;
	@Autowired
	private transient ReqWorkCustomerHelper workCustomerService;
	@Autowired
	private transient ComponentHelper componentHelper;

	// ========================================================================
	// 變數
	// ========================================================================
	@Getter
	@Setter
	private NotifyFilterType notifyFilterType = NotifyFilterType.SMALL_CATEGORY;
	/**
	 * 需求項目樹選單
	 */
	@Getter
	private SingleSelectTreePicker categoryTreePicker;
	/**
	 * 所有需求項目
	 */
	private List<WkItem> allCategoryItems = Lists.newArrayList();
	/**
	 * 所有項目存放容器
	 */
	private Map<NotifyFilterType, Set<String>> allItemSidsMapByNotifyFilterType = Maps.newHashMap();

	/**
	 * 所有廳主資料
	 */
	List<WorkCustomer> allCustomers = Lists.newArrayList();

	@Getter
	private SingleSelectListPicker customerListPicker;

	/**
	 * 使用者設定資訊
	 */
	@Getter
	private List<SettingManagetSysNotifyVO> userSettingInfos;
	/**
	 * 備份的使用者設定資訊 (搜尋用)
	 */
	private List<SettingManagetSysNotifyVO> keepUserSettingInfos;

	/**
	 * 搜尋關鍵字
	 */
	@Getter
	@Setter
	private String serachUserNameKeyword;

	// ========================================================================
	// 方法區
	// ========================================================================
	/**
	 * 初始化
	 */
	@PostConstruct
	public void init() {
		// ====================================
		// 準備需求項目相關
		// ====================================
		this.prepareCategory();

		// ====================================
		// 準備廳主
		// ====================================
		this.prepareCustomer();

		// ====================================
		// 查詢login user 所有可設定的 user 的通知設定
		// ====================================
		try {
			this.prepareUserSettingData();
		} catch (Exception e) {
			log.error("", e);
		}

	}

	/**
	 * 準備需求項目相關
	 */
	private void prepareCategory() {
		// ====================================
		// 取得所有需求項目
		// ====================================
		// 查詢
		this.allCategoryItems = this.prepareAllCategoryItems();
		// 預設選擇第一筆小類
		String defaultSelectedCategorySid = "";
		// 收集小類
		Set<String> smallCategoryItemSids = Sets.newHashSet();

		if (WkStringUtils.notEmpty(this.allCategoryItems)) {
			for (WkItem wkItem : this.allCategoryItems) {
				// 取得項目類別
				CategoryType categoryType = componentHelper.getItemCategoryType(wkItem);
				if (CategoryType.SMALL.equals(categoryType)) {
					// 預設選項
					if (WkStringUtils.isEmpty(defaultSelectedCategorySid)) {
						defaultSelectedCategorySid = wkItem.getSid();
					}
					// 收集小類
					smallCategoryItemSids.add(wkItem.getSid());
				}
			}
		}

		this.allItemSidsMapByNotifyFilterType.put(NotifyFilterType.SMALL_CATEGORY, smallCategoryItemSids);

		// ====================================
		// 初始化需求項目樹
		// ====================================
		// Picker Config
		SingleSelectTreePickerConfig categoryPickerConfig = new SingleSelectTreePickerConfig(categoryTreeCallback);
		categoryPickerConfig.setDefaultSelectedItemSid(defaultSelectedCategorySid);
		// init SingleSelectTreePicker
		this.categoryTreePicker = new SingleSelectTreePicker(categoryPickerConfig);
	}

	/**
	 * 準備廳主相關資料
	 */
	private void prepareCustomer() {

		// ====================================
		// 取得所有非停用的廳主
		// ====================================
		this.allCustomers = workCustomerService.findByEnable(EnableType.ENABLE);

		this.allItemSidsMapByNotifyFilterType.put(
		        NotifyFilterType.COSTOMER,
		        this.allCustomers.stream()
		                .map(each -> (each.getSid() + ""))
		                .collect(Collectors.toSet()));

		// ====================================
		// 初始化廳主選單
		// ====================================
		// Picker Config
		SingleSelectListPickerConfig customerListPickerConfig = new SingleSelectListPickerConfig(customerListCallback);
		customerListPickerConfig.setDefaultSelectedItemSid(this.allCustomers.get(0).getSid() + "");

		// init SingleSelectListPicker
		this.customerListPicker = new SingleSelectListPicker(customerListPickerConfig);

	}

	/**
	 * 事件：更新設定項目
	 * 
	 * @param userSid
	 * @param notifyType
	 * @param itemSid
	 */
	public void event_update(
	        Integer userSid,
	        NotifyType notifyType,
	        String itemSid) {

		// ====================================
		// 取得設定資料容器
		// ====================================
		Optional<SettingManagetSysNotifyVO> optional = this.userSettingInfos.stream()
		        .filter(vo -> WkCommonUtils.compareByStr(vo.getUserSid(), userSid))
		        .findFirst();

		if (!optional.isPresent()) {
			String msg = WkMessage.SESSION_TIMEOUT;
			log.info(msg + " -> 找不到 SettingManagetSysNotifyVO : userSid:[" + userSid + "]");
			MessagesUtils.showWarn(msg);
			return;
		}

		SettingManagetSysNotifyVO userSettingInfo = optional.get();

		// ====================================
		// update
		// ====================================
		try {
			this.settingSysNotifyService.updateOpenSwitchForManager(
			        userSid,
			        SecurityFacade.getUserSid(),
			        notifyType,
			        this.notifyFilterType,
			        itemSid,
			        userSettingInfo.isOpen(notifyType),
			        this.allItemSidsMapByNotifyFilterType);

		} catch (UserMessageException e) {
			MessagesUtils.show(e);
			return;
		} catch (Exception e) {
			String message = WkMessage.PROCESS_FAILED + ":" + e.getMessage();
			MessagesUtils.showError(message);
			log.error(message, e);
			this.init();
		}
	}

	/**
	 * 事件：切換設定方式
	 */
	public void event_change_notifyFilterType() {

		if (NotifyFilterType.SMALL_CATEGORY.equals(notifyFilterType)) {
			this.prepareCategory();
		} else {
			this.prepareCustomer();
		}

		prepareUserSettingData();
	}

	/**
	 * 使用者關鍵字搜尋
	 */
	public void event_searchUserNameByKeyword() {
		// 重新轉換查詢關鍵字, 忽略大小寫差異
		String keyword = WkStringUtils.safeTrim(serachUserNameKeyword).toUpperCase();

		if (WkStringUtils.isEmpty(keyword)) {
			// 關鍵字為空時，顯示全部 (取回原本保存的那一份)
			this.userSettingInfos = Lists.newArrayList(this.keepUserSettingInfos);
		} else {
			// 重新由備份過濾出最新的
			this.userSettingInfos = this.keepUserSettingInfos.stream()
			        .filter(settingInfo -> {
			            // 依據輸入的關鍵字過濾
			            return WkStringUtils.safeTrim(settingInfo.getUserName()).toUpperCase().contains(keyword);
			        })
			        .collect(Collectors.toList());
		}
	}

	/**
	 * 查詢login user 所有可設定的 user 的通知設定
	 */
	private void prepareUserSettingData() {

		// ====================================
		// 選取的設定項目 SID (需求項目或廳主)
		// ====================================
		String itemSid = "";
		switch (this.notifyFilterType) {
		case SMALL_CATEGORY:
			itemSid = this.categoryTreePicker.getSelectedSid();
			break;

		case COSTOMER:
			itemSid = this.customerListPicker.getSelectedSid();
			break;
		default:
			String errorMessage = "未實做類別:[" + notifyFilterType + "]";
			log.error(errorMessage);
			MessagesUtils.showError(errorMessage);
			break;
		}

		// ====================================
		// 依據權限, 找出使用者可設定的使用者
		// ====================================
		Set<Integer> userSids = this.prepareRelationUserSids();

		// ====================================
		// 準備資料
		// ====================================
		this.userSettingInfos = this.settingSysNotifyService.prepareSettingInfoByUserSids(
		        Lists.newArrayList(userSids),
		        notifyFilterType,
		        itemSid);
		// 備份一份 (搜尋用)
		this.keepUserSettingInfos = Lists.newArrayList(this.userSettingInfos);
	}

	/**
	 * @return
	 */
	private Set<Integer> prepareRelationUserSids() {
		// ====================================
		// 1.登入者
		// ====================================
		Set<Integer> userSids = Sets.newHashSet(SecurityFacade.getUserSid());

		// ====================================
		// 2.登入者管理部門以下成員
		// ====================================
		// 登入者管理的部門
		Set<Integer> managerDepSids = WkOrgCache.getInstance().findManagerOrgSids(SecurityFacade.getUserSid());

		// 複製一份 for loop 使用
		Set<Integer> copyList = managerDepSids.stream().collect(Collectors.toSet());

		// 以下的子部門
		for (Integer managerDepSid : copyList) {
			Set<Integer> managerChildOrgSids = WkOrgCache.getInstance().findAllChildSids(managerDepSid);
			if (WkStringUtils.notEmpty(managerChildOrgSids)) {
				managerDepSids.addAll(managerChildOrgSids);
			}
		}

		List<Integer> userSidsByManagerDeps = WkUserCache.getInstance().findUserWithManagerByOrgSids(managerDepSids, Activation.ACTIVE)
		        .stream()
		        .map(User::getSid)
		        .collect(Collectors.toList());

		if (WkStringUtils.notEmpty(userSidsByManagerDeps)) {
			userSids.addAll(userSidsByManagerDeps);
		}

		return userSids;
	}

	/**
	 * @return
	 */
	private List<WkItem> prepareAllCategoryItems() {
		// ====================================
		// 準備需求項目樹所有項目
		// ====================================
		// 取得所有項目
		List<WkItem> allItems = componentHelper.prepareAllCategoryItems();

		// 過濾停用
		allItems = allItems.stream()
		        .filter(WkItem::isActive)
		        .collect(Collectors.toList());

		// ====================================
		// 客製化
		// ====================================
		for (WkItem wkItem : allItems) {
			// 取得項目類別
			CategoryType categoryType = componentHelper.getItemCategoryType(wkItem);

			// 不是小類時，不可選擇
			if (!CategoryType.SMALL.equals(categoryType)) {
				wkItem.setSelectable(false);
			}

			// 預設展開大類
			if (CategoryType.BIG.equals(categoryType)) {
				wkItem.setForceExpand(true);
			}
		}

		return allItems;
	}

	/**
	 * 需求項目選單 callback
	 */
	private final SingleSelectTreeCallback categoryTreeCallback = new SingleSelectTreeCallback() {
		/**
         * 
         */
        private static final long serialVersionUID = 3217148495007956799L;

        /**
		 * 準備所有的項目
		 * 
		 * @return
		 */
		public List<WkItem> prepareAllItems() throws SystemDevelopException {
			return allCategoryItems;
		}

		/**
		 * 擊點項目時的動作
		 * 
		 * @return
		 */
		public void clickItem() {
			// WkItem s = categoryTreePicker.getSelectedWkItem();
			// String name = (s == null) ? "null" : s.getName();
			// log.debug("selected:[" + name + "]");
			// ====================================
			// 查詢login user 所有可設定的 user 的通知設定
			// ====================================
			prepareUserSettingData();
		}
	};

	/**
	 * 廳主項目選單 callback
	 */
	private final SingleSelectListCallback customerListCallback = new SingleSelectListCallback() {
		/**
         * 
         */
        private static final long serialVersionUID = -7313183624397734982L;

        /**
		 * 準備所有的項目
		 * 
		 * @return
		 */
		public List<WkItem> prepareAllItems() throws SystemDevelopException {
			List<WkItem> allItem = Lists.newArrayList();
			for (WorkCustomer workCustomer : allCustomers) {
				WkItem wkItem = new WkItem(
				        workCustomer.getSid() + "",
				        workCustomer.getName() + "@" + workCustomer.getLoginCode(),
				        true);

				String showName = ""
				        + "<span style='font-weight: bold'>" + workCustomer.getName() + "</span>"
				        + "<span class='WS1-1-3b'>&nbsp@</span>"
				        + workCustomer.getLoginCode();
				wkItem.setShowName(showName);
				allItem.add(wkItem);
			}

			return allItem;
		}

		/**
		 * 擊點項目時的動作
		 * 
		 * @return
		 */
		public void clickItem() {
			// WkItem s = customerListPicker.getSelectedWkItem();
			// String name = (s == null) ? "null" : s.getName();
			// log.debug("selected:[" + name + "]");
			// ====================================
			// 查詢login user 所有可設定的 user 的通知設定
			// ====================================
			prepareUserSettingData();
		}
	};
}
