package com.cy.tech.request.web.controller.view.vo;

import com.cy.tech.request.web.controller.view.component.DateIntervalVO;

import lombok.Getter;
import lombok.Setter;

public class BatchAdjustDateQueryVO {
    @Getter
    @Setter
    private String requireNo;
    @Getter
    @Setter
    private String requireTheme;
    @Getter
    @Setter
    private QueryDateType queryDateType = QueryDateType.TEST_DATE;
    @Getter
    @Setter
    private DateIntervalVO dateIntervalVO = new DateIntervalVO();
    
    public enum QueryDateType{
        TEST_DATE("送測日期"),
        REQUIRE_DATE("需求日期");
        
        @Getter
        private String value;
        
        private QueryDateType(String value) {
            this.value = value;
        }
    }
    
    public void init() {
        this.requireNo = null;
        this.requireTheme = null;
        this.queryDateType = QueryDateType.TEST_DATE;
        this.dateIntervalVO.clear();
    }
    
    public QueryDateType[] buildQueryDateType() {
        return QueryDateType.values();
    }
}
