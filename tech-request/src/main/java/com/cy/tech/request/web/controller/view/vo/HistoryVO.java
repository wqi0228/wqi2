/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.vo;

import com.cy.tech.request.web.listener.ReplyCallBack;
import com.cy.tech.request.web.listener.ReplyCallBackCondition;
import java.util.List;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

/**
 * 回覆介面物件
 *
 * @author brain0925_liao
 */
public class HistoryVO {

    @Getter
    private boolean showExpandAndCompress = false;
    @Getter
    private boolean showReplyAndReplyBtn = false;

    public HistoryVO(String os_sid, String historySid, String os_reply_sid, boolean showModifyBtn,
            boolean showAttachmentBtn, boolean showExpandAndCompress, boolean showReplyAndReplyBtn,
            String behavior, Integer createUserSid, Integer createUserDepSid,
            String createUserName, String createUserDepName, String lastModifyTime,
            String content, List<SubReplyVO> subReplyVO, ReplyCallBack replyCallBack, int index) {
        this.historySid = historySid;
        this.os_sid = os_sid;
        this.os_reply_sid = os_reply_sid;
        this.showModifyBtn = showModifyBtn;
        this.showAttachmentBtn = showAttachmentBtn;
        this.showExpandAndCompress = showExpandAndCompress;
        this.showReplyAndReplyBtn = showReplyAndReplyBtn;
        this.behavior = behavior;
        this.createUserSid = createUserSid;
        this.createUserDepSid = createUserDepSid;
        this.createUserName = createUserName;
        this.createUserDepName = createUserDepName;
        this.lastModifyTime = lastModifyTime;
        this.content = content;
        this.subReplyVO = subReplyVO;
        this.replyCallBack = replyCallBack;
        this.index = index;
    }

    public HistoryVO(String historySid) {
        this.historySid = historySid;
    }

    public void modifyReply() {
        ReplyCallBackCondition rc = new ReplyCallBackCondition();
        rc.setOs_history_sid(historySid);
        rc.setOs_sid(os_sid);
        rc.setOs_reply_sid(os_reply_sid);
        replyCallBack.modifyReply(rc);
    }

    public void addReplyAndReply() {
        ReplyCallBackCondition rc = new ReplyCallBackCondition();
        rc.setOs_history_sid(historySid);
        rc.setOs_reply_sid(os_reply_sid);
        rc.setOs_sid(os_sid);
        replyCallBack.addReplyAndReply(rc);
    }

    public void maintainAtt() {
        ReplyCallBackCondition rc = new ReplyCallBackCondition();
        rc.setOs_sid(os_sid);
        rc.setOs_history_sid(historySid);
        replyCallBack.maintainAtt(rc);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.historySid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HistoryVO other = (HistoryVO) obj;
        if (!Objects.equals(this.historySid, other.historySid)) {
            return false;
        }
        return true;
    }

    private String os_sid;
    @Getter
    private int index;

    @Getter
    private String historySid;
    @Getter
    private String os_reply_sid;

    /** 是否顯示修改回覆按鈕 */
    @Getter
    private boolean showModifyBtn;
    /** 是否有附件 */
    @Getter
    @Setter
    private boolean showAttachmentBtn;
    @Getter
    private String behavior;
    /** 建立者Sid */
    @Getter
    private Integer createUserSid;
    /** 建立者部門Sid */
    @Getter
    private Integer createUserDepSid;
    /** 建立者名稱 */
    @Getter
    private String createUserName;
    /** 建立者部門名稱 */
    @Getter
    private String createUserDepName;
    /** 回覆最後修改時間 */
    @Getter
    private String lastModifyTime;
    /** 回覆內容 */
    @Getter
    private String content;
    /** 子回覆 */
    @Getter
    private List<SubReplyVO> subReplyVO;

    private ReplyCallBack replyCallBack;

}
