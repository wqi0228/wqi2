/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.component.searchquery;

import com.cy.commons.enums.OrgLevel;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.DateType;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.Search13QueryColumn;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.search.helper.Search13Helper;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterArrayStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterDetailVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterVO;
import com.cy.tech.request.web.controller.view.vo.Search13QueryVO;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.logic.WkUserAndOrgLogic;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.LocalDate;

/**
 * @author kasim
 */
@Slf4j
public class SearchQuery13 implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3477487613411371337L;

    private WkUserAndOrgLogic wkUserAndOrgLogic = WorkSpringContextHolder.getBean(WkUserAndOrgLogic.class);

    @Getter
    /** 報表類型 */
    private ReportType reportType = ReportType.WORK_TEST_INFO;

    private DisplayController display;
    private Search13Helper helper;

    private MessageCallBack messageCallBack;

    @Getter
    private Search13QueryVO queryVO;

    @Getter
    private Search13QueryVO search13QueryVOFilter;
    /** 該登入者在需求單,自訂查詢條件List */
    private List<ReportCustomFilterVO> reportCustomFilterVOs;
    /** 該登入者在需求單,挑選自訂查詢條件物件 */
    private ReportCustomFilterVO selReportCustomFilterVO;
    /** ReportCustomFilterLogicComponent */
    private ReportCustomFilterLogicComponent reportCustomFilterLogicComponent;

    private Integer loginUserSid;

    public SearchQuery13(Search13Helper helper, DisplayController display,
            MessageCallBack messageCallBack,
            ReportCustomFilterLogicComponent reportCustomFilterLogicComponent,
            Integer loginUserSid) {
        this.helper = helper;
        this.display = display;
        this.messageCallBack = messageCallBack;
        this.reportCustomFilterLogicComponent = reportCustomFilterLogicComponent;
        this.loginUserSid = loginUserSid;
        this.init();
    }

    /**
     * 初始化
     *
     * @param roleSids
     */
    private void init() {
        this.queryVO = new Search13QueryVO();
        this.search13QueryVOFilter = new Search13QueryVO();
    }

    public void saveDefaultSetting() {
        try {
            ReportCustomFilterDetailVO demandType = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search13QueryColumn.DemandType,
                    (search13QueryVOFilter.getSelectBigCategory() != null) ? search13QueryVOFilter.getSelectBigCategory() : "");
            ReportCustomFilterDetailVO department = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search13QueryColumn.Department,
                    search13QueryVOFilter.getRequireDepts());
            ReportCustomFilterDetailVO noticeDepartment = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search13QueryColumn.TestDepartment,
                    search13QueryVOFilter.getNoticeDepts());
            ReportCustomFilterDetailVO demandProcess = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search13QueryColumn.DemandProcess,
                    (search13QueryVOFilter.getSelectRequireStatusType() != null) ? search13QueryVOFilter.getSelectRequireStatusType().name() : "");
            ReportCustomFilterDetailVO readStatus = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search13QueryColumn.ReadStatus,
                    (search13QueryVOFilter.getSelectReadRecordType() != null) ? search13QueryVOFilter.getSelectReadRecordType().name() : "");
            ReportCustomFilterDetailVO demandPerson = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search13QueryColumn.DemandPerson,
                    search13QueryVOFilter.getTrCreatedUserName());
            List<String> waitReadResonStr = Lists.newArrayList();
            search13QueryVOFilter.getReadReason().forEach(item -> {
                waitReadResonStr.add(item.name());
            });
            ReportCustomFilterDetailVO waitReadResons = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search13QueryColumn.WaitReadReson,
                    waitReadResonStr);
            ReportCustomFilterDetailVO testStatuss = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search13QueryColumn.TestStatus,
                    search13QueryVOFilter.getTestStatus());
            ReportCustomFilterDetailVO orderType = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search13QueryColumn.OrderType,
                    search13QueryVOFilter.getSortType());
            ReportCustomFilterDetailVO categoryCombo = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search13QueryColumn.CategoryCombo,
                    search13QueryVOFilter.getSmallDataCateSids());
            ReportCustomFilterDetailVO dateIndex = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search13QueryColumn.DateIndex,
                    search13QueryVOFilter.getDateTypeIndexStr());
            ReportCustomFilterDetailVO searchText = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search13QueryColumn.SearchText,
                    search13QueryVOFilter.getFuzzyText());

            List<ReportCustomFilterDetailVO> saveDetails = Lists.newArrayList();
            saveDetails.add(demandType);
            saveDetails.add(department);
            saveDetails.add(noticeDepartment);
            saveDetails.add(demandProcess);
            saveDetails.add(readStatus);
            saveDetails.add(demandPerson);
            saveDetails.add(waitReadResons);
            saveDetails.add(testStatuss);
            saveDetails.add(orderType);
            saveDetails.add(categoryCombo);
            saveDetails.add(dateIndex);
            saveDetails.add(searchText);
            reportCustomFilterLogicComponent.saveReportCustomFilter(Search13QueryColumn.Search13Query, loginUserSid,
                    (selReportCustomFilterVO != null) ? selReportCustomFilterVO.getIndex() : null, true, saveDetails);
            loadSetting();
            display.update("headerTitle");
            display.execute("doSearchData();");
            display.hidePfWidgetVar("dlgReportCustomFilter");
        } catch (Exception e) {
            this.messageCallBack.showMessage(e.getMessage());
            log.error("saveDefaultSetting ERROR", e);
        }
    }

    public void loadSetting() {
        initSearchQueryVODefautSetting(queryVO);
        loadSettingData(queryVO);
    }

    public void loadDefaultSettingDialog() {
        initSearchQueryVODefautSetting(search13QueryVOFilter);
        loadSettingData(search13QueryVOFilter);
    }

    public void loadSettingData(Search13QueryVO searchQueryVO) {
        reportCustomFilterVOs = reportCustomFilterLogicComponent.getReportCustomFilter(Search13QueryColumn.Search13Query, loginUserSid);
        if (reportCustomFilterVOs == null || reportCustomFilterVOs.isEmpty()) {
            return;
        }
        selReportCustomFilterVO = reportCustomFilterVOs.get(0);
        selReportCustomFilterVO.getReportCustomFilterDetailVOs().forEach(item -> {
            if (Search13QueryColumn.DemandType.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDemandType(item, searchQueryVO);
            } else if (Search13QueryColumn.Department.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingDepartment(item, searchQueryVO);
            } else if (Search13QueryColumn.TestDepartment.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingTestDepartment(item, searchQueryVO);
            } else if (Search13QueryColumn.DemandProcess.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDemandProcess(item, searchQueryVO);
            } else if (Search13QueryColumn.ReadStatus.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingReadStatus(item, searchQueryVO);
            } else if (Search13QueryColumn.DemandPerson.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDemandPerson(item, searchQueryVO);
            } else if (Search13QueryColumn.WaitReadReson.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingWaitReadReson(item, searchQueryVO);
            } else if (Search13QueryColumn.TestStatus.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingTestStatus(item, searchQueryVO);
            } else if (Search13QueryColumn.CategoryCombo.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingCategoryCombo(item, searchQueryVO);
            } else if (Search13QueryColumn.OrderType.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingOrderType(item, searchQueryVO);
            } else if (Search13QueryColumn.DateIndex.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDateIndex(item, searchQueryVO);
            } else if (Search13QueryColumn.SearchText.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingFuzzyText(item, searchQueryVO);
            }
        });
    }

    /**
     * 塞入模糊搜尋
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingFuzzyText(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search13QueryVO searchQueryVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQueryVO.setFuzzyText(reportCustomFilterStringVO.getValue());
        } catch (Exception e) {
            log.error("settingFuzzyText", e);
        }
    }

    /**
     * 塞入立單區間Type
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDateIndex(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search13QueryVO searchQueryVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQueryVO.setDateTypeIndexStr(reportCustomFilterStringVO.getValue());
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                searchQueryVO.setDateTypeIndex(Integer.valueOf(reportCustomFilterStringVO.getValue()));
            }

            if (searchQueryVO.equals(queryVO)) {
                if (DateType.PREVIOUS_MONTH.ordinal() == queryVO.getDateTypeIndex()) {
                    getDateInterval(DateType.PREVIOUS_MONTH.name());
                } else if (DateType.THIS_MONTH.ordinal() == queryVO.getDateTypeIndex()) {
                    getDateInterval(DateType.THIS_MONTH.name());
                } else if (DateType.NEXT_MONTH.ordinal() == queryVO.getDateTypeIndex()) {
                    getDateInterval(DateType.NEXT_MONTH.name());
                } else if (DateType.TODAY.ordinal() == queryVO.getDateTypeIndex()) {
                    getDateInterval(DateType.TODAY.name());
                }
            }
        } catch (Exception e) {
            log.error("settingFuzzyText", e);
        }
    }

    /**
     * 塞入需求人員
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingOrderType(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search13QueryVO searchQueryVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQueryVO.setSortType(reportCustomFilterStringVO.getValue());
        } catch (Exception e) {
            log.error("trCreatedUserName", e);
        }
    }

    /**
     * 塞入需求類別
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingCategoryCombo(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search13QueryVO searchQueryVO) {
        try {
            searchQueryVO.getSmallDataCateSids().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQueryVO.getSmallDataCateSids().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingCategoryCombo", e);
        }
    }

    /**
     * 塞入送測狀態
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingTestStatus(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search13QueryVO searchQueryVO) {
        try {
            searchQueryVO.getTestStatus().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQueryVO.getTestStatus().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingDepartment", e);
        }
    }

    /**
     * 塞入待閱原因
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingWaitReadReson(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search13QueryVO searchQueryVO) {
        try {
            searchQueryVO.getReadReason().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                reportCustomFilterArrayStringVO.getArrayStrings().forEach(item -> {
                    searchQueryVO.getReadReason().add(WaitReadReasonType.valueOf(item));
                });
            }
        } catch (Exception e) {
            log.error("settingWaitReadReson", e);
        }
    }

    /**
     * 塞入需求人員
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandPerson(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search13QueryVO searchQueryVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQueryVO.setTrCreatedUserName(reportCustomFilterStringVO.getValue());
        } catch (Exception e) {
            log.error("trCreatedUserName", e);
        }
    }

    /**
     * 塞入是否閱讀：
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingReadStatus(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search13QueryVO searchQueryVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                searchQueryVO.setSelectReadRecordType(ReadRecordType.valueOf(reportCustomFilterStringVO.getValue()));
            }
        } catch (Exception e) {
            log.error("settingDemandSource", e);
        }
    }

    /**
     * 塞入需求類別
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandProcess(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search13QueryVO searchQueryVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                searchQueryVO.setSelectRequireStatusType(RequireStatusType.valueOf(reportCustomFilterStringVO.getValue()));
            }
        } catch (Exception e) {
            log.error("settingDemandType", e);
        }
    }

    /**
     * 塞入被分派單位
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingTestDepartment(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search13QueryVO searchQueryVO) {
        try {
            searchQueryVO.getNoticeDepts().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQueryVO.getNoticeDepts().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingTestDepartment", e);
        }
    }

    /**
     * 塞入被分派單位
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDepartment(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search13QueryVO searchQueryVO) {
        try {
            searchQueryVO.getRequireDepts().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQueryVO.getRequireDepts().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingDepartment", e);
        }
    }

    /**
     * 塞入需求類別
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandType(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search13QueryVO searchQueryVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                searchQueryVO.setSelectBigCategory(reportCustomFilterStringVO.getValue());
            }
        } catch (Exception e) {
            log.error("settingDemandType", e);
        }
    }

    private void initSearchQueryVODefautSetting(Search13QueryVO searchQueryVO) {
        // 共用查詢條件初始化
        searchQueryVO.publicConditionInit();
        searchQueryVO.setSelectBigCategory(null);
        searchQueryVO.setFuzzyText("");
        searchQueryVO.setTrCreatedUserName("");
        searchQueryVO.setDateTypeIndex(reportType.getDateType().ordinal());
        searchQueryVO.setStartDate(new LocalDate().toDate());
        searchQueryVO.setEndDate(new LocalDate().toDate());
        searchQueryVO.setSortType(Search13QueryVO.SortType.CREATE_DESC.name());
        searchQueryVO.setSelectRequireStatusType(null);
        if (searchQueryVO.getTestStatus() == null) {
            searchQueryVO.setTestStatus(Lists.newArrayList());
        }
        searchQueryVO.getTestStatus().clear();
        helper.workTestStatusItems().forEach(each -> searchQueryVO.getTestStatus().add(each.name()));
        searchQueryVO.setSelectReadRecordType(null);
        searchQueryVO.setReadReason(helper.getTestValues());
        searchQueryVO.setDateTypeIndexStr("");

        // 需求單位
        // 部級以下, 不包含特殊可閱
        searchQueryVO.setRequireDepts(
                Lists.newArrayList(
                        wkUserAndOrgLogic.prepareCanViewDepSidStrBaseOnOrgLevelWithParallel(
                                SecurityFacade.getUserSid(),
                                OrgLevel.MINISTERIAL, false)));

        // 送測單位
        // 部級以下, 包含特殊可閱
        searchQueryVO.setNoticeDepts(
                Lists.newArrayList(
                        wkUserAndOrgLogic.prepareCanViewDepSidStrBaseOnOrgLevelWithParallel(
                                SecurityFacade.getUserSid(),
                                OrgLevel.MINISTERIAL, true)));
    }

    public void clearDateType() {
        if ("3".equals(search13QueryVOFilter.getDateTypeIndexStr())) {
            search13QueryVOFilter.setDateTypeIndexStr("");
        }
    }

    /**
     * 清除/還原選項
     */
    public void clear() {
        this.initSearchQueryVODefautSetting(queryVO);
        this.clearAdvance();
    }

    /**
     * 取得日期區間
     *
     * @param dateType
     */
    public void getDateInterval(String dateType) {
        if (DateType.PREVIOUS_MONTH.name().equals(dateType)) {
            if (queryVO.getStartDate() == null) {
                queryVO.setStartDate(new Date());
            }
            LocalDate lastDate = new LocalDate(queryVO.getStartDate()).minusMonths(1);
            queryVO.setStartDate(lastDate.dayOfMonth().withMinimumValue().toDate());
            queryVO.setEndDate(lastDate.dayOfMonth().withMaximumValue().toDate());
            queryVO.setDateTypeIndex(DateType.PREVIOUS_MONTH.ordinal());
        } else if (DateType.THIS_MONTH.name().equals(dateType)) {
            queryVO.setStartDate(new LocalDate().dayOfMonth().withMinimumValue().toDate());
            queryVO.setEndDate(new LocalDate().dayOfMonth().withMaximumValue().toDate());
            queryVO.setDateTypeIndex(DateType.THIS_MONTH.ordinal());
        } else if (DateType.NEXT_MONTH.name().equals(dateType)) {
            if (queryVO.getStartDate() == null) {
                queryVO.setStartDate(new Date());
            }
            LocalDate nextDate = new LocalDate(queryVO.getStartDate()).plusMonths(1);
            queryVO.setStartDate(nextDate.dayOfMonth().withMinimumValue().toDate());
            queryVO.setEndDate(nextDate.dayOfMonth().withMaximumValue().toDate());
            queryVO.setDateTypeIndex(DateType.NEXT_MONTH.ordinal());
        } else if (DateType.TODAY.name().equals(dateType)) {
            queryVO.setStartDate(new Date());
            queryVO.setEndDate(new Date());
            queryVO.setDateTypeIndex(DateType.TODAY.ordinal());
        } else if (DateType.NULL.name().equals(dateType)) {
            queryVO.setStartDate(null);
            queryVO.setEndDate(null);
            queryVO.setDateTypeIndex(DateType.NULL.ordinal());
        }
    }

    /**
     * 清除進階選項
     */
    public void clearAdvance() {
        queryVO.setStartUpdatedDate(null);
        queryVO.setEndUpdatedDate(null);
        queryVO.setRequireNo("");
    }
}
