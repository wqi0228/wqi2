package com.cy.tech.request.web.controller.search;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.logic.service.CategorySettingService;
import com.cy.tech.request.logic.service.TemplateService;
import com.cy.tech.request.vo.category.BasicDataBigCategory;
import com.cy.tech.request.vo.category.BasicDataMiddleCategory;
import com.cy.tech.request.vo.category.BasicDataSmallCategory;
import com.cy.tech.request.vo.template.CategoryKeyMapping;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import java.util.Set;

/**
 *
 * @author jason_h
 */
@Controller
@Scope("view")
public class CategoryTreeMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 193954287582296439L;
    /**
     * 類別根節點
     */
    @Autowired
    transient private CategorySettingService cateSettingService;
    @Autowired
    transient private TemplateService tmpService;

    @Getter
    @Setter
    transient private TreeNode cateRoot;

    /**
     * 選擇的類別節點
     */
    @Getter
    @Setter
    transient private TreeNode[] selCateNodes;

    /**
     * 類別樹搜尋字串
     */
    @Getter
    @Setter
    private String cateText;

    /**
     * 是否全選
     */
    @Getter
    @Setter
    private Boolean isAllSel;

    @Getter
    @Setter
    private List<String> bigDataCateSids = Lists.newArrayList();

    @Getter
    @Setter
    private List<String> middleDataCateSids = Lists.newArrayList();

    @Getter
    @Setter
    private List<String> smallDataCateSids = Lists.newArrayList();

    /**
     * Creates a new instance of CategoryTreeMBean
     */
    public CategoryTreeMBean() {
    }

    /**
     * 類別節點-頁面顯示用
     */
    @EqualsAndHashCode(of = { "sid", "name", "cateClz" })
    @Data
    @SuppressWarnings("rawtypes")
    public class CateNode implements Serializable {

        /**
         * 
         */
        private static final long serialVersionUID = 8660583282478611741L;

        private String sid;

        private String name;

        private Class cateClz;

        public CateNode(String sid, String name, Class cateClz) {
            this.sid = sid;
            this.name = name;
            this.cateClz = cateClz;
        }

    }

    /**
     * 初始根節點
     */
    public void initRoot() {
        if (cateRoot != null) {
            return;
        }
        List<BasicDataBigCategory> bigCates = cateSettingService.findAllBig();

        cateRoot = new DefaultTreeNode("root", null);
        // 存放所有的大類節點
        List<TreeNode> bigNodes = Lists.newArrayList();
        // 建立大類節點
        bigCates.forEach(big -> {
            TreeNode bigNode = new DefaultTreeNode(new CateNode(big.getSid(), big.getName(), big.getClass()), cateRoot);
            bigNode.setExpanded(true);
            bigNodes.add(bigNode);
        });
        buildMiddleNode(bigCates, bigNodes);
    }

    /**
     * 建立中類節點
     *
     * @param bigCates
     * @param bigNodes
     */
    private void buildMiddleNode(List<BasicDataBigCategory> bigCates, List<TreeNode> bigNodes) {
        List<BasicDataMiddleCategory> middleCates = Lists.newArrayList();
        // 存放所有的中類節點
        List<TreeNode> middleNodes = Lists.newArrayList();
        for (BasicDataBigCategory big : bigCates) {
            middleCates.addAll(big.getChildren());
            for (TreeNode bigNode : bigNodes) {
                CateNode cate = (CateNode) bigNode.getData();
                if (big.getSid().equals(cate.getSid())) {
                    big.getChildren().forEach(middle -> {
                        if (middle.getStatus().equals(Activation.ACTIVE)) {
                            TreeNode middleNode = new DefaultTreeNode(new CateNode(middle.getSid(), middle.getName(), middle.getClass()), bigNode);
                            middleNodes.add(middleNode);
                        }
                    });
                    break;
                }
            }
        }
        buildSmallNode(middleCates, middleNodes);
    }

    /**
     * 建立小類節點
     *
     * @param middleCates
     * @param middleNodes
     */
    private void buildSmallNode(List<BasicDataMiddleCategory> middleCates, List<TreeNode> middleNodes) {
        for (BasicDataMiddleCategory middle : middleCates) {
            for (TreeNode middleNode : middleNodes) {
                CateNode cate = (CateNode) middleNode.getData();
                if (middle.getSid().equals(cate.getSid())) {
                    middle.getChildren().forEach(small -> {
                        if (small.getStatus().equals(Activation.ACTIVE)) {
                            new DefaultTreeNode(new CateNode(small.getSid(), small.getName(), small.getClass()), middleNode);
                        }
                    });
                    break;
                }
            }
        }
    }

    /**
     * 全選 - AJAX
     */
    public void actionSelectALL() {
        List<TreeNode> selNodes = Lists.newArrayList();
        if (isAllSel) {
            this.selectAllNodeStatus(cateRoot.getChildren(), selNodes, true);
            TreeNode[] nodes = new TreeNode[selNodes.size()];
            selCateNodes = selNodes.toArray(nodes);
            return;
        }
        // 全關
        this.selectAllNodeStatus(cateRoot.getChildren(), selNodes, false);
        selCateNodes = null;
    }

    /**
     * 選擇全部節點
     *
     * @param childsNode
     */
    private void selectAllNodeStatus(List<TreeNode> childsNode, List<TreeNode> selNodes, boolean status) {
        if (childsNode == null) {
            return;
        }
        childsNode.forEach(node -> {
            node.setSelected(status);
            selNodes.add(node);
            this.selectAllNodeStatus(node.getChildren(), selNodes, status);
        });
    }

    /**
     * 依查詢字串搜尋節點
     *
     * @param parent
     */
    public void searchNodeByText(TreeNode parent) {

        if (Strings.isNullOrEmpty(cateText)) {
            unselAllNode(parent);
            selCateNodes = null;
            this.expendToBig(parent);
            return;
        }

        List<TreeNode> selNodes = Lists.newArrayList();
        markByText(cateText, parent, selNodes);

        TreeNode[] nodes = new TreeNode[selNodes.size()];
        selCateNodes = selNodes.toArray(nodes);
    }

    /**
     * 未選取所有的節點
     *
     * @param parent
     */
    private void unselAllNode(TreeNode parent) {
        parent.getChildren().forEach(node -> {
            if (node.getParent() != null) {
                node.getParent().setExpanded(false);
            }
            node.setSelected(false);
            unselAllNode(node);
        });
    }

    /**
     * 依查詢字串標註相關的類別
     *
     * @param text
     * @param parent
     * @param selNodes
     */
    private void markByText(String text, TreeNode parent, List<TreeNode> selNodes) {
        parent.getChildren().forEach(node -> {
            CateNode cate = (CateNode) node.getData();
            if (cate.getName().toLowerCase().contains(text.toLowerCase())) {
                expandedAboveNode(node.getParent());
                node.setSelected(true);
                selNodes.add(node);
            }
            markByText(text, node, selNodes);
        });
    }

    private void expandedAboveNode(TreeNode node) {
        if (node == null) {
            return;
        }
        node.setExpanded(true);
        if (node.getParent() != null) {
            this.expandedAboveNode(node.getParent());
        }
    }

    /**
     * 清除類別組合
     */
    public void clearCate() {
        if (cateRoot == null) {
            return;
        }
        expendToBig(cateRoot);
        selCateNodes = null;
        isAllSel = false;
        cateText = "";
        clearCateSids();
    }

    private void expendToBig(TreeNode parent) {
        parent.getChildren().forEach(big -> {
            big.setSelected(false);
            big.setExpanded(true);
            big.getChildren().forEach(middle -> {
                middle.setSelected(false);
                middle.setExpanded(false);
                middle.getChildren().forEach(small -> {
                    small.setSelected(false);
                    small.setExpanded(false);
                });
            });
        });
    }

    private void clearCateSids() {
        bigDataCateSids.clear();
        middleDataCateSids.clear();
        smallDataCateSids.clear();
    }

    /**
     * 確認選擇類別
     */
    public void confirmSelCate() {
        clearCateSids();
        if (selCateNodes == null || selCateNodes.length == 0) {
            return;
        }
        for (TreeNode n : selCateNodes) {
            CateNode cate = (CateNode) n.getData();
            if (cate.getCateClz().getName().equals(BasicDataBigCategory.class.getName())) {
                bigDataCateSids.add(cate.getSid());
            } else if (cate.getCateClz().getName().equals(BasicDataMiddleCategory.class.getName())) {
                middleDataCateSids.add(cate.getSid());
            } else if (cate.getCateClz().getName().equals(BasicDataSmallCategory.class.getName())) {
                smallDataCateSids.add(cate.getSid());
            }
        }
    }

    /**
     * 是否為CategoryKeyMapping entity本身的欄位查詢
     *
     * @return
     */
    public boolean isCategoryKeyMappingCondition() {
        return !bigDataCateSids.isEmpty() || !middleDataCateSids.isEmpty() || !smallDataCateSids.isEmpty();
    }

    public void initRootWithAmountCash() {
        List<CategoryKeyMapping> tmps = tmpService.findMappingByFieldName("是否收費");
        Set<BasicDataBigCategory> bigs = Sets.newTreeSet((BasicDataBigCategory o1, BasicDataBigCategory o2) -> o1.getId().compareTo(o2.getId()));
        Set<BasicDataMiddleCategory> middles = Sets.newTreeSet((BasicDataMiddleCategory o1, BasicDataMiddleCategory o2) -> o1.getId().compareTo(o2.getId()));
        Set<BasicDataSmallCategory> smalls = Sets.newTreeSet((BasicDataSmallCategory o1, BasicDataSmallCategory o2) -> o1.getId().compareTo(o2.getId()));
        tmps.forEach(tmp -> {
            bigs.add(tmp.getBig());
            middles.add(tmp.getMiddle());
            smalls.add(tmp.getSmall());
        });

        cateRoot = new DefaultTreeNode("root", null);
        // 建立大類
        bigs.forEach(big -> {
            TreeNode bigNode = new DefaultTreeNode(new CateNode(big.getSid(), big.getName(), big.getClass()));
            bigNode.setExpanded(true);
            cateRoot.getChildren().add(bigNode);
        });
        // 建立中類
        List<TreeNode> middleNodes = Lists.newArrayList();
        for (BasicDataMiddleCategory middle : middles) {
            for (TreeNode node : cateRoot.getChildren()) {
                CateNode cate = (CateNode) node.getData();
                if (middle.getParentBigCategory().getSid().equals(cate.getSid())) {
                    TreeNode middleNode = new DefaultTreeNode(new CateNode(middle.getSid(), middle.getName(), middle.getClass()));
                    middleNodes.add(middleNode);
                    node.getChildren().add(middleNode);
                    break;
                }
            }
        }
        // 建立小類
        smalls.stream().forEach(small -> {
            middleNodes.stream().forEach(node -> {
                CateNode cate = (CateNode) node.getData();
                if (small.getParentMiddleCategory().getSid().equals(cate.getSid())) {
                    node.getChildren().add(new DefaultTreeNode(new CateNode(small.getSid(), small.getName(), small.getClass())));
                }
            });
        });

        // 重新指派選取節點
        List<TreeNode> nodeList = Lists.newArrayList();
        if (selCateNodes != null && selCateNodes.length > 0) {
            markBySelNode(cateRoot, Lists.newArrayList(selCateNodes), nodeList);
        }
        TreeNode[] nodes = new TreeNode[nodeList.size()];
        selCateNodes = nodeList.toArray(nodes);
    }

    private void markBySelNode(TreeNode parent, List<TreeNode> hasSelNode, List<TreeNode> nodeList) {
        parent.getChildren().forEach(node -> {
            if (hasSelNode.contains(node)) {
                expandedAboveNode(node.getParent());
                node.setSelected(true);
                nodeList.add(node);
            }
            markBySelNode(node, hasSelNode, nodeList);
        });
    }

}
