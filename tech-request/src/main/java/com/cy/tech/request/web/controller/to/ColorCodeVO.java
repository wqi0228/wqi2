package com.cy.tech.request.web.controller.to;

import lombok.Getter;
import lombok.Setter;

public class ColorCodeVO {

    @Getter
    @Setter
    private String colorCode;
    @Getter
    @Setter
    private String description;
}
