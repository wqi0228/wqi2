package com.cy.tech.request.web.controller.component.mutiuserqkpker.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu 傳到client端的node 資料封裝容器, 為了減少傳輸容量, 變數進行簡式命名
 */
@Getter
@Setter
public class MultiUserQuickPickerClientNodeData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5477775839940612622L;
    /**
     * 建構子
     * 
     * @param nodeData MultiUserQuickPickerNodeData
     */
    public MultiUserQuickPickerClientNodeData(MultiUserQuickPickerNodeData nodeData) {
        this.sid = nodeData.getSid();
        this.na = nodeData.getName();
        this.isUr = nodeData.isUserNode();
        if(this.isUr) {
            this.lna = nodeData.getSelectedListShowName();
        }
        this.rk = nodeData.getRowKey();
        this.act = nodeData.isActive();
    }

    /**
     * 是否為User節點 (非部門) isUserNode
     */
    private boolean isUr = false;

    /**
     * SID
     */
    private String sid;
    /**
     * 名稱 name
     */
    private String na;
    /**
     * 列表用名稱 list name
     */
    private String lna;

    /**
     * tree node key (rowKey)
     */
    private String rk;
    /**
     * 是否啟用 isActive
     */
    private boolean act = false;
}
