package com.cy.tech.request.web.controller.test;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.vo.User;
import com.cy.tech.request.web.controller.component.qkstree.QuickSelectTreeCallback;
import com.cy.tech.request.web.controller.component.qkstree.impl.OrgUserTreeMBean;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@NoArgsConstructor
@Controller
@Scope("view")
public class TestOrgUserTreeMBean implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -1192617550423168185L;

    /**
	 * 使用者資料快取
	 */
	@Autowired
	private WkUserCache wkUserCache;

	/**
	 * 
	 */
	@Autowired
	private OrgUserTreeMBean orgUserTreeMBean;

	@Getter
	private List<User> selectUsers;

	/**
	 * 
	 */
	@PostConstruct
	public void init() {
		log.info("TestOrgUserTreeMBean @PostConstruct");
		selectUsers = Lists.newArrayList();
	}

	/**
	 * 點選開啟組織人員選擇樹
	 */
	public void openOrgUserTree() {

		//Org comp = WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId());
		//if (comp == null) {
			//MessagesUtils.showError("公司ID:[" + SecurityFacade.getCompanyId() + "]找不到對應資料!");
			
		//}

//		this.orgUserTreeMBean.init(
//		        comp.getSid(),
//		        SecurityFacade.getUserSid(),
//		        quickSelectTreeCallback);
		
		
		this.orgUserTreeMBean.init(
		        2,
		        3383,
		        quickSelectTreeCallback);

		this.orgUserTreeMBean.preOpenDlg();
	}

	/**
	 * 
	 */
	private final QuickSelectTreeCallback quickSelectTreeCallback = new QuickSelectTreeCallback() {
		/**
         * 
         */
        private static final long serialVersionUID = -1198931886106084135L;

        @SuppressWarnings("unchecked")
		@Override
		public List<Integer> getSelectedDataList() {

			// 將現行已選擇的 user 轉為元件需要的資料型態 (sid list)
			if (WkStringUtils.notEmpty(selectUsers)) {
				return selectUsers.stream()
				        .map(o -> o.getSid())
				        .collect(Collectors.toList());
			}

			return Lists.newArrayList();
		}
	};

	/**
	 * 更新畫面的已選擇人員清單
	 */
	public void reNewSelectedUsers() {

		// 取得元件中選取的 user 清單
		List<Integer> userSids = this.orgUserTreeMBean.getSelectedDataList();

		if (WkStringUtils.isEmpty(userSids)) {
			this.selectUsers = Lists.newArrayList();
			return;
		}

		this.selectUsers = Lists.newArrayList();
		for (Integer userSid : userSids) {
			selectUsers.add(wkUserCache.findBySid(userSid));
		}

		this.orgUserTreeMBean.afterCloseDlg();

	}

	/**
	 * 實做供元件呼叫取得已選清單的方法, 會將此份名單帶入右邊的【已選擇人員】清單
	 */
	Function<Map<String, Object>, List<?>> getSelectedDataList = (condition) -> {

		// 將現行已選擇的 user 轉為元件需要的資料型態 (sid list)
		if (WkStringUtils.notEmpty(this.selectUsers)) {
			return this.selectUsers.stream()
			        .map(o -> o.getSid())
			        .collect(Collectors.toList());
		}

		return Lists.newArrayList();
	};

}
