/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

import com.cy.tech.request.logic.service.customer.ReqWorkCustomerHelper;
import com.cy.tech.request.vo.enums.ReqCateType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.template.CategoryKeyMapping;
import com.cy.tech.request.web.controller.view.WorkCustomerView;
import com.cy.work.customer.vo.WorkCustomer;
import com.cy.work.customer.vo.enums.CustomerType;
import com.cy.work.customer.vo.enums.EnableType;
import com.google.common.collect.Lists;

import lombok.NoArgsConstructor;

/**
 * 廳主控制
 *
 * @author shaun
 */
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class CustomerMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1283445795480556041L;
    @Autowired
    transient private ReqWorkCustomerHelper reqWorkCustomerHelper;

    /**
     * 當小類為 新包網資料提供 ，客戶資料自動帶 新包網 <BR/>
     * 當大類為 內部流程時，客戶資料自動帶內部需求流程
     *
     * @param selectMapping
     */
    public void updateCustomer(Require01MBean r01MBean, CategoryKeyMapping selectMapping) {
        WorkCustomer workCustomer = null;
        if (selectMapping != null && "新包網資料提供".equals(selectMapping.getSmallName())) {
            workCustomer = reqWorkCustomerHelper.findByEnableAndCustomerTypeAndAlias(EnableType.ENABLE,
                    CustomerType.VIRTUAL_CUSTOMER, "新包網");
        }
        if (selectMapping != null && selectMapping.getBig().getReqCateType().equals(ReqCateType.INTERNAL)) {
            workCustomer = reqWorkCustomerHelper.findByEnableAndCustomerTypeAndAlias(EnableType.ENABLE,
                    CustomerType.INNER_VIRTUAL_CUSTOMER, "內部需求流程");
        }
        r01MBean.getRequire().setCustomer(workCustomer);
        r01MBean.getRequire().setAuthor(workCustomer);
    }

    /**
     * 取得 變更廳主
     */
    public void changeCustomerItems(Require01MBean r01MBean) {
        if (RequireStep.NEW_IN_PAGE.equals(r01MBean.getSetp())
                || (r01MBean.getRequire().getCustomer() == null
                        || !CustomerType.VIRTUAL_CUSTOMER.equals(r01MBean.getRequire().getCustomer().getCustomerType()))) {
            this.sameCustomer(r01MBean.getRequire());
        }

        r01MBean.getTemplateMBean().changeThemeValueByCustomer(r01MBean.getRequire().getCustomer());
    }

    /**
     * 取得 變更廳主
     */
    public void changeCustomerItemsByIcon(Require01MBean r01MBean) {
        if (RequireStep.NEW_IN_PAGE.equals(r01MBean.getSetp())
                || (r01MBean.getRequire().getCustomer() == null
                        || !CustomerType.VIRTUAL_CUSTOMER.equals(r01MBean.getRequire().getCustomer().getCustomerType()))) {
            this.sameCustomer(r01MBean.getRequire());
        }
        r01MBean.getTemplateMBean().changeThemeValueByCustomer(r01MBean.getRequire().getCustomer());
    }

    /**
     * 客戶與廳主相同
     */
    private void sameCustomer(Require require) {
        require.setAuthor(require.getCustomer());
    }

    /**
     * 取得 廳主選項（新增須為啟用；編輯時 小類為 新包網資料提供 只能顯示實體廳主）
     */
    public List<WorkCustomer> getCustomerItems(Require01MBean r01MBean) {
        if (RequireStep.NEW_IN_PAGE.equals(r01MBean.getSetp())) {
            return this.findByEnableAndCustomerType(EnableType.ENABLE, null);
        } else {
            if ("新包網資料提供".equals(r01MBean.getRequire().getMapping().getSmallName())) {
                return this.findByEnableAndCustomerType(null, CustomerType.REAL_CUSTOMER, r01MBean.getRequire().getCustomer());
            }
            return this.findByEnableAndCustomerType(null, null);
        }
    }

    /**
     * 廳主下拉式選項 getCustomerItems 替代方法<BR/>
     * 配合customerSimpleConverter進行PF元件間轉換<BR/>
     * 但ajax 觸發 changeCustomerItems() 失效導致相關作業都未動作
     *
     * @param r01MBean
     * @return
     */
    public List<WorkCustomerView> getCustomerViews(Require01MBean r01MBean) {
        List<WorkCustomer> items = this.getCustomerItems(r01MBean);
        return this.entityToView(items);
    }

    /**
     * 廳主下拉式選項 getAuthorItems 替代方法<BR/>
     * 配合customerSimpleConverter進行PF元件間轉換<BR/>
     * 但ajax 觸發 changeCustomerItems() 失效導致相關作業都未動作
     *
     * @param r01MBean
     * @return
     */
    public List<WorkCustomerView> getAuthorViews(Require01MBean r01MBean) {
        List<WorkCustomer> items = this.getAuthorItems(r01MBean);
        return this.entityToView(items);
    }

    private List<WorkCustomerView> entityToView(List<WorkCustomer> items) {
        return items.stream().map(each -> new WorkCustomerView(
                each.getSid(),
                each.getLoginCode(),
                each.getName(),
                each.getAlias(),
                each.getCustomerType() == null ? "" : each.getCustomerType().getName()
        )).collect(Collectors.toList());
    }

    /**
     * 取得 客戶選項（新增須為啟用）
     */
    public List<WorkCustomer> getAuthorItems(Require01MBean r01MBean) {
        if (RequireStep.NEW_IN_PAGE.equals(r01MBean.getSetp())) {
            return this.findByEnableAndCustomerType(EnableType.ENABLE, null);
        } else {
            return this.findByEnableAndCustomerType(null, null);
        }
    }

    /**
     * 是否disabled提出客戶選項
     */
    public boolean disabledAuthorItems(Require01MBean r01MBean) {
        if (r01MBean.getRequire().getCustomer() != null
                && CustomerType.REAL_CUSTOMER.equals(r01MBean.getRequire().getCustomer().getCustomerType())) {
            return true;
        }
        if (r01MBean.getRequire().getMapping() != null
                && "新包網資料提供".equals(r01MBean.getRequire().getMapping().getSmallName())) {
            return true;
        }
        return false;
    }

    /**
     * 依據EnableType OR CustomerType取得客戶資料
     *
     * @param enable
     * @param customerType
     * @return
     */
    private List<WorkCustomer> findByEnableAndCustomerType(EnableType enable, CustomerType customerType) {
        return this.findByEnableAndCustomerType(enable, customerType, null);
    }

    /**
     * 依據EnableType OR CustomerType取得客戶資料
     *
     * @param enable
     * @param customerType
     * @param addWorkCustomer
     * @return
     */
    private List<WorkCustomer> findByEnableAndCustomerType(EnableType enable,
            CustomerType customerType, WorkCustomer addWorkCustomer) {
        List<WorkCustomer> workCustomers = reqWorkCustomerHelper.findAll();
        List<WorkCustomer> customers = Lists.newArrayList();
        for (WorkCustomer obj : workCustomers) {
            if (addWorkCustomer != null && addWorkCustomer.equals(obj)) {
                customers.add(obj);
                continue;
            }
            if (enable != null && !enable.equals(obj.getEnable())) {
                continue;
            }
            if (customerType != null && !customerType.equals(obj.getCustomerType())) {
                continue;
            }
            customers.add(obj);
        }
        return customers;
    }

    public String alias(Long sid) {
        WorkCustomer customer = reqWorkCustomerHelper.findCopyOne(sid);
        if (customer == null) {
            return "";
        }
        return customer.getAlias();
    }

}
