package com.cy.tech.request.web.logic.helper;

import com.cy.commons.vo.Org;
import com.cy.tech.request.logic.utils.SimpleDateFormatEnum;
import com.cy.tech.request.logic.utils.ToolsDate;
import com.cy.work.common.utils.WkOrgUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Data;

/**
 * 轉單 自訂物件
 *
 * @author kasim
 */
@Data
public class TransNotifyTo implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 6816416607064666748L;

    /** 程序挑選 */
	private String selTransProgramType;

	/** 需求製作進度 */
	private List<String> selReqTypes;

	/** 子程序狀態 */
	private List<String> selSubProgramTypes;

	/** 轉換單號 */
	private String transNos;

	/** 來源對應部門 */
	private Org sourceMappOrg;

	/** 目標新增部門 */
	private Org targetMappOrg;

	private Boolean isTrans = Boolean.FALSE;
	/** 訊息 */
	private StringBuilder result;
	private Integer count;
	private Integer errCount;

	/**
	 * 註記轉檔
	 */
	public void startTrans() {
		isTrans = Boolean.TRUE;
	}

	/**
	 * 清除log
	 */
	public void clearMsg() {
		isTrans = Boolean.FALSE;
		result = new StringBuilder();
		count = 0;
		errCount = 0;
	}

	/**
	 * add log
	 */
	public void addCount() {
		count++;
		this.addLog("----------------------------------------");
		this.addLog("第" + count + "筆");
	}

	/**
	 * add log
	 */
	public void addErrCount(String message) {
		errCount++;
		this.addLog(message);
	}

	/**
	 * add log
	 */
	public void addLog(String message) {
		this.result.append(message).append("\r\n");
	}

	/**
	 * 取得log
	 */
	public String getMsg() {
		if (result == null) {
			return "";
		}
		if (isTrans) {
			String msg = "增加通知單位作業";
			msg += "\r\n原單單位：" + WkOrgUtils.getOrgName(sourceMappOrg);
			msg += "\r\n轉換單位新增部門：" + WkOrgUtils.getOrgName(targetMappOrg);
			msg += "\r\n處理筆數：" + count;
			msg += "\r\n失敗筆數：" + errCount;
			msg += "\r\n記錄日期：" + ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), new Date());
			return msg + "\r\n" + result.toString();
		}
		return result.toString();
	}

	public String getRecoveryMsg() {
		if (result == null) {
			return "";
		}
		if (isTrans) {
			String msg = "還原通知單位作業";
			msg += "\r\n還原單位：" + WkOrgUtils.getOrgName(targetMappOrg);
			msg += "\r\n處理筆數：" + count;
			msg += "\r\n失敗筆數：" + errCount;
			msg += "\r\n記錄日期：" + ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), new Date());
			return msg + "\r\n" + result.toString();
		}
		return result.toString();
	}
}
