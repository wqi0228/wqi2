/**
 * 
 */
package com.cy.tech.request.web.controller.setting.setting10;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.syncmms.SyncMmsDominLogic;
import com.cy.tech.request.logic.service.syncmms.SyncMmsRequireDataLogic;
import com.cy.tech.request.logic.service.syncmms.SyncMmsSyncFormLogic;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.utils.WkCommonUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Slf4j
@Controller
@Scope("view")
public class Set10SyncMmsMBean {

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private SyncMmsDominLogic syncMmsDominLogic;
    @Autowired
    private SyncMmsRequireDataLogic syncMmsRequireDataLogic;
    @Autowired
    private SyncMmsSyncFormLogic syncMmsSyncFormLogic;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 同步廳主
     */
    public void syncMmsDomin() {
        try {
            this.syncMmsDominLogic.process(SecurityFacade.getCompanyId());
        } catch (SystemOperationException e) {
            MessagesUtils.show(e);
            log.error(e.getMessage());
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, e.getMessage(), 2);
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + e.getMessage();
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);;
        }
    }

    /**
     * 同步需求單列表
     */
    public void syncMmsRequireData() {
        try {
            this.syncMmsRequireDataLogic.process(false, SecurityFacade.getCompanyId());
        } catch (SystemOperationException e) {
            MessagesUtils.show(e);
            log.error(e.getMessage());
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, e.getMessage(), 2);
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + e.getMessage();
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);;
        }
    }

    public void syncMmsRequireDataAllSend() {
        try {
            this.syncMmsRequireDataLogic.process(true, SecurityFacade.getCompanyId());
        } catch (SystemOperationException e) {
            MessagesUtils.show(e);
            log.error(e.getMessage());
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, e.getMessage(), 2);
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + e.getMessage();
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);;
        }
    }

    /**
     * 同步維護單
     */
    public void syncMmsSyncForm() {
        try {
            this.syncMmsSyncFormLogic.process(SecurityFacade.getCompanyId());
        } catch (SystemOperationException e) {
            MessagesUtils.show(e);
            log.error(e.getMessage());
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, e.getMessage(), 2);
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + e.getMessage();
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);;
        }
    }
}
