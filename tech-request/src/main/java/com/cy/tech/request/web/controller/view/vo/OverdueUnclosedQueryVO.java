package com.cy.tech.request.web.controller.view.vo;

import java.util.List;
import java.util.Set;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.service.CategorySettingService;
import com.cy.tech.request.logic.vo.query.search.SearchQuery;
import com.cy.tech.request.vo.category.BasicDataBigCategory;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.web.controller.enums.ReqStatusMBean;
import com.cy.tech.request.web.controller.view.component.CategoryCombineVO;
import com.cy.tech.request.web.controller.view.component.DateIntervalVO;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;

public class OverdueUnclosedQueryVO extends SearchQuery {

    /**
     * 
     */
    private static final long serialVersionUID = 3170311670326526014L;

    private CategorySettingService categorySettingService = WorkSpringContextHolder.getBean(CategorySettingService.class);

    @Getter
    @Setter
    /** 需求類別 */
    private List<String> bigCategorySids = Lists.newArrayList();
    @Getter
    @Setter
    private Set<Integer> createDepSids = Sets.newHashSet();
    @Getter
    @Setter
    /** 需求製作進度 */
    private List<String> requireStatusTypes = Lists.newArrayList();
    @Getter
    @Setter
    /** 閱讀類型 */
    private ReadRecordType readRecordType;
    @Getter
    @Setter
    /** 待閱原因 */
    private List<String> reasons = Lists.newArrayList();
    @Getter
    @Setter
    /** 需求人員 */
    private String creator;
    @Getter
    @Setter
    /** 逾期天數 */
    private Integer overdueDays = 1;
    @Getter
    @Setter
    /** 類別組合 */
    private CategoryCombineVO categoryCombineVO = new CategoryCombineVO();
    @Getter
    @Setter
    /** 日期區間 */
    private DateIntervalVO dateIntervalVO = new DateIntervalVO();
    @Getter
    @Setter
    private String searchText;
    @Getter
    private List<BasicDataBigCategory> allBigCategory = Lists.newArrayList();
    
    
    /**
     * 登入者可檢視的單位 (權限單位+可閱單位)
     */
    @Getter
    @Setter
    private Set<Integer> canViewDepSids = Sets.newHashSet();

    public OverdueUnclosedQueryVO() {
    }

    public OverdueUnclosedQueryVO(
            Org org,
            User user,
            ReportType reportType,
            List<BasicDataBigCategory> allBigCategory) {

        this.dep = org;
        this.user = user;
        this.reportType = reportType;
        this.allBigCategory = allBigCategory;
    }

    public void init() {

        // 共用欄位初始化
        this.publicConditionInit();

        // 需求類別
        bigCategorySids.clear();
        for (BasicDataBigCategory bigCategory : categorySettingService.findAllBig()) {
            this.bigCategorySids.add(bigCategory.getSid());
        }
        // 需求製作進度
        requireStatusTypes.clear();
        for (RequireStatusType type : ReqStatusMBean.STATUS1) {
            requireStatusTypes.add(type.name());
        }
        // 是否閱讀
        this.readRecordType = null;
        // 待閱原因
        reasons.clear();
        for (ReqToBeReadType each : ReqToBeReadType.values()) {
            this.reasons.add(each.name());
        }
        // 需求人員
        this.creator = null;
        // 類別組合
        this.categoryCombineVO.init();
        // 立單區間
        this.dateIntervalVO.init(-1);
        // 模糊搜尋
        this.searchText = null;
        // 逾期天數
        this.overdueDays = 1;
    }

}
