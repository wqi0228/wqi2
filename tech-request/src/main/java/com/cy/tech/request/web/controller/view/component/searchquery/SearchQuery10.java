/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.component.searchquery;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.enumerate.DateType;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.service.CategorySettingService;
import com.cy.tech.request.logic.vo.query.search.SearchQuery;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.Search10QueryColumn;
import com.cy.tech.request.web.controller.enums.ReqStatusMBean;
import com.cy.tech.request.web.controller.logic.component.ReportCustomFilterLogicComponent;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.view.component.CategoryTreeComponent;
import com.cy.tech.request.web.controller.view.component.ReportOrgTreeComponent;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterArrayStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterDetailVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterStringVO;
import com.cy.tech.request.web.controller.view.vo.ReportCustomFilterVO;
import com.cy.tech.request.web.controller.view.vo.Search10QueryVO;
import com.cy.tech.request.web.listener.CategoryTreeCallBack;
import com.cy.tech.request.web.listener.MessageCallBack;
import com.cy.tech.request.web.listener.ReportOrgTreeCallBack;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.logic.WkUserAndOrgLogic;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.LocalDate;

/**
 * @author brain0925_liao
 */
@Slf4j
public class SearchQuery10 extends SearchQuery implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1020457316142850803L;
    @Getter
    private Search10QueryVO search10QueryVO;
    @Getter
    private Search10QueryVO search10QueryVOCustomerFilter;

    private ReqStatusMBean reqStatusUtils;

    private User user;

    private Org dep;

    @Getter
    /** 類別樹 Component */
    private CategoryTreeComponent categoryTreeComponent;
    @Getter
    /** 類別樹 Component */
    private CategoryTreeComponent defaultCategoryTreeComponent;
    @Getter
    /** 報表 組織樹 Component */
    private ReportOrgTreeComponent orgTreeComponent;
    @Getter
    /** 報表 組織樹 Component */
    private ReportOrgTreeComponent defaultOrgTreeComponent;

    private DisplayController display;
    /** 該登入者在需求單,自訂查詢條件List */
    private List<ReportCustomFilterVO> reportCustomFilterVOs;
    /** 該登入者在需求單,挑選自訂查詢條件物件 */
    private ReportCustomFilterVO selReportCustomFilterVO;
    /** ReportCustomFilterLogicComponent */
    private ReportCustomFilterLogicComponent reportCustomFilterLogicComponent;
    /** CategorySettingService */
    private CategorySettingService categorySettingService;

    private MessageCallBack messageCallBack;

    public SearchQuery10(String compId, SearchHelper helper,
            ReportCustomFilterLogicComponent reportCustomFilterLogicComponent,
            CategorySettingService categorySettingService,
            MessageCallBack messageCallBack,
            DisplayController display,
            List<Long> roleSids,
            Org dep,
            User user,
            ReqStatusMBean reqStatusUtils) {
        this.reportCustomFilterLogicComponent = reportCustomFilterLogicComponent;
        this.categorySettingService = categorySettingService;
        this.messageCallBack = messageCallBack;
        this.display = display;
        this.dep = dep;
        this.user = user;
        this.reqStatusUtils = reqStatusUtils;
        this.categoryTreeComponent = new CategoryTreeComponent(categoryTreeCallBack);
        this.defaultCategoryTreeComponent = new CategoryTreeComponent(defaultCategoryTreeCallBack);
        this.orgTreeComponent = new ReportOrgTreeComponent(
                compId, dep, roleSids, true, reportOrgTreeCallBack);
        this.defaultOrgTreeComponent = new ReportOrgTreeComponent(
                compId, dep, roleSids, true, defaultReportOrgTreeCallBack);
        this.search10QueryVO = new Search10QueryVO();
        this.search10QueryVOCustomerFilter = new Search10QueryVO();
    }

    public void saveDefaultSetting() {
        try {
            ReportCustomFilterDetailVO demandType = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search10QueryColumn.DemandType,
                    (search10QueryVOCustomerFilter.getSelectBigCategory() != null) ? search10QueryVOCustomerFilter.getSelectBigCategory().getSid() : "");
            ReportCustomFilterDetailVO department = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search10QueryColumn.Department,
                    search10QueryVOCustomerFilter.getRequireDepts());
            ReportCustomFilterDetailVO demandProcess = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search10QueryColumn.DemandProcess,
                    (search10QueryVOCustomerFilter.getSelectRequireStatusType() != null) ? search10QueryVOCustomerFilter.getSelectRequireStatusType().name() : "");
            ReportCustomFilterDetailVO readStatus = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search10QueryColumn.ReadStatus,
                    (search10QueryVOCustomerFilter.getSelectReadRecordType() != null) ? search10QueryVOCustomerFilter.getSelectReadRecordType().name() : "");
            ReportCustomFilterDetailVO demandPerson = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search10QueryColumn.DemandPerson,
                    search10QueryVOCustomerFilter.getTrCreatedUserName());
            ReportCustomFilterDetailVO categoryCombo = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search10QueryColumn.CategoryCombo,
                    search10QueryVOCustomerFilter.getSmallDataCateSids());
            ReportCustomFilterDetailVO dateIndex = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search10QueryColumn.DateIndex,
                    search10QueryVOCustomerFilter.getDateTypeIndexStr());
            ReportCustomFilterDetailVO searchText = reportCustomFilterLogicComponent.createReportCustomFilterDetailVO(Search10QueryColumn.SearchText,
                    search10QueryVOCustomerFilter.getFuzzyText());

            List<ReportCustomFilterDetailVO> saveDetails = Lists.newArrayList();
            saveDetails.add(demandType);
            saveDetails.add(department);
            saveDetails.add(demandProcess);
            saveDetails.add(readStatus);
            saveDetails.add(demandPerson);
            saveDetails.add(categoryCombo);
            saveDetails.add(dateIndex);
            saveDetails.add(searchText);
            reportCustomFilterLogicComponent.saveReportCustomFilter(Search10QueryColumn.Search10Query, user.getSid(), (selReportCustomFilterVO != null) ? selReportCustomFilterVO.getIndex() : null,
                    true, saveDetails);
            loadSetting();
            display.update("headerTitle");
            display.execute("doSearchData();");
            display.hidePfWidgetVar("dlgReportCustomFilter");
        } catch (Exception e) {
            this.messageCallBack.showMessage(e.getMessage());
            log.error("saveDefaultSetting ERROR", e);
        }
    }

    public void loadSetting() {
        initSearchQueryVODefautSetting(search10QueryVO);
        loadSettingData(search10QueryVO);
        categoryTreeComponent.init();
        categoryTreeComponent.selectedItem(search10QueryVO.getSmallDataCateSids());
        categoryTreeCallBack.confirmSelCate();
    }

    public void initSearchQueryVODefautSetting(Search10QueryVO searchQuery) {
        // 共用查詢條件初始化
        searchQuery.publicConditionInit();
        searchQuery.setSelectBigCategory(null);
        searchQuery.setFuzzyText("");
        searchQuery.setTrCreatedUserName("");
        searchQuery.setDateTypeIndex(ReportType.NOT_CLOSE.getDateType().ordinal());
        searchQuery.setDateTypeIndexStr("");
        // 查詢可閱部門
        WkUserAndOrgLogic userAndOrgLogic = WorkSpringContextHolder.getBean(WkUserAndOrgLogic.class);
        Set<Integer> canViewDepSids = userAndOrgLogic.prepareCanViewDepSids(this.user.getSid(), true);

        searchQuery.setRequireDepts(canViewDepSids.stream().map(depsid -> depsid + "").collect(Collectors.toList()));
        searchQuery.setSelectRequireStatusType(null);
        searchQuery.setSelectReadRecordType(null);
        searchQuery.getBigDataCateSids().clear();
        searchQuery.getMiddleDataCateSids().clear();
        searchQuery.getSmallDataCateSids().clear();
    }

    public void loadDefaultSettingDialog() {
        initSearchQueryVODefautSetting(search10QueryVOCustomerFilter);
        loadSettingData(search10QueryVOCustomerFilter);
    }

    public void loadSettingData(Search10QueryVO searchQueryVO) {
        reportCustomFilterVOs = reportCustomFilterLogicComponent.getReportCustomFilter(Search10QueryColumn.Search10Query, user.getSid());
        if (reportCustomFilterVOs == null || reportCustomFilterVOs.isEmpty()) {
            return;
        }
        selReportCustomFilterVO = reportCustomFilterVOs.get(0);
        selReportCustomFilterVO.getReportCustomFilterDetailVOs().forEach(item -> {
            if (Search10QueryColumn.DemandType.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDemandType(item, searchQueryVO);
            } else if (Search10QueryColumn.Department.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingDepartment(item, searchQueryVO);
            } else if (Search10QueryColumn.DemandProcess.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDemandProcess(item, searchQueryVO);
            } else if (Search10QueryColumn.ReadStatus.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingReadStatus(item, searchQueryVO);
            } else if (Search10QueryColumn.DemandPerson.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDemandPerson(item, searchQueryVO);
            } else if (Search10QueryColumn.CategoryCombo.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterArrayStringVO) {
                settingCategoryCombo(item, searchQueryVO);
            } else if (Search10QueryColumn.DateIndex.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingDateIndex(item, searchQueryVO);
            } else if (Search10QueryColumn.SearchText.equals(item.getSearchReportCustomEnum())
                    && item instanceof ReportCustomFilterStringVO) {
                settingFuzzyText(item, searchQueryVO);
            }
        });
    }

    /**
     * 塞入模糊搜尋
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingFuzzyText(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search10QueryVO searchQueryVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQueryVO.setFuzzyText(reportCustomFilterStringVO.getValue());
        } catch (Exception e) {
            log.error("settingFuzzyText", e);
        }
    }

    /**
     * 塞入立單區間Type
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDateIndex(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search10QueryVO searchQueryVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQueryVO.setDateTypeIndexStr(reportCustomFilterStringVO.getValue());
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                searchQueryVO.setDateTypeIndex(Integer.valueOf(reportCustomFilterStringVO.getValue()));
            }

            if (searchQueryVO.equals(search10QueryVO)) {
                if (DateType.PREVIOUS_MONTH.ordinal() == search10QueryVO.getDateTypeIndex()) {
                    getDateInterval(DateType.PREVIOUS_MONTH.name());
                } else if (DateType.THIS_MONTH.ordinal() == search10QueryVO.getDateTypeIndex()) {
                    getDateInterval(DateType.THIS_MONTH.name());
                } else if (DateType.NEXT_MONTH.ordinal() == search10QueryVO.getDateTypeIndex()) {
                    getDateInterval(DateType.NEXT_MONTH.name());
                } else if (DateType.TODAY.ordinal() == search10QueryVO.getDateTypeIndex()) {
                    getDateInterval(DateType.TODAY.name());
                }
            }
        } catch (Exception e) {
            log.error("settingFuzzyText", e);
        }
    }

    /**
     * 塞入需求類別
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingCategoryCombo(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search10QueryVO searchQueryVO) {
        try {
            searchQueryVO.getSmallDataCateSids().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQueryVO.getSmallDataCateSids().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingCategoryCombo", e);
        }
    }

    /**
     * 塞入需求人員
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandPerson(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search10QueryVO searchQueryVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            searchQueryVO.setTrCreatedUserName(reportCustomFilterStringVO.getValue());
        } catch (Exception e) {
            log.error("trCreatedUserName", e);
        }
    }

    /**
     * 塞入是否閱讀：
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingReadStatus(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search10QueryVO searchQueryVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                searchQueryVO.setSelectReadRecordType(ReadRecordType.valueOf(reportCustomFilterStringVO.getValue()));
            }
        } catch (Exception e) {
            log.error("settingDemandSource", e);
        }
    }

    /**
     * 塞入需求類別
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandProcess(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search10QueryVO searchQueryVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                searchQueryVO.setSelectRequireStatusType(RequireStatusType.valueOf(reportCustomFilterStringVO.getValue()));
            }
        } catch (Exception e) {
            log.error("settingDemandType", e);
        }
    }

    /**
     * 塞入被分派單位
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDepartment(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search10QueryVO searchQueryVO) {
        try {
            searchQueryVO.getRequireDepts().clear();
            ReportCustomFilterArrayStringVO reportCustomFilterArrayStringVO = (ReportCustomFilterArrayStringVO) reportCustomFilterDetailVO;
            if (reportCustomFilterArrayStringVO.getArrayStrings() != null) {
                searchQueryVO.getRequireDepts().addAll(reportCustomFilterArrayStringVO.getArrayStrings());
            }
        } catch (Exception e) {
            log.error("settingDepartment", e);
        }
    }

    /**
     * 塞入需求類別
     *
     * @param reportCustomFilterDetailVO
     */
    private void settingDemandType(ReportCustomFilterDetailVO reportCustomFilterDetailVO, Search10QueryVO searchQueryVO) {
        try {
            ReportCustomFilterStringVO reportCustomFilterStringVO = (ReportCustomFilterStringVO) reportCustomFilterDetailVO;
            if (!Strings.isNullOrEmpty(reportCustomFilterStringVO.getValue())) {
                searchQueryVO.setSelectBigCategory(categorySettingService.findBigBySid(reportCustomFilterStringVO.getValue()));
            }
        } catch (Exception e) {
            log.error("settingDemandType", e);
        }
    }

    /**
     * 開啟 類別樹
     */
    public void btnOpenCategoryTree() {
        try {
            categoryTreeComponent.init();
            categoryTreeComponent.selectedItem(search10QueryVO.getSmallDataCateSids());
            display.showPfWidgetVar("dlgCate");
        } catch (Exception e) {
            log.error("btnOpenCategoryTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 開啟 單位挑選 組織樹
     */
    public void btnDefaultOpenOrgTree() {
        try {
            defaultOrgTreeComponent.initOrgTree(dep, search10QueryVOCustomerFilter.getRequireDepts(), false, false);
            display.showPfWidgetVar("defaultdlgOrgTree");
        } catch (Exception e) {
            log.error("btnDefaultOpenOrgTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 開啟 類別樹
     */
    public void btnOpenDefaultCategoryTree() {
        try {
            defaultCategoryTreeComponent.init();
            defaultCategoryTreeComponent.selectedItem(search10QueryVOCustomerFilter.getSmallDataCateSids());
            display.showPfWidgetVar("defaultDlgCate");
        } catch (Exception e) {
            log.error("btnOpenDefaultCategoryTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 開啟 單位挑選 組織樹
     */
    public void btnOpenOrgTree() {
        try {
            orgTreeComponent.initOrgTree(dep, search10QueryVO.getRequireDepts(), false, false);
            display.showPfWidgetVar("dlgOrgTree");
        } catch (Exception e) {
            log.error("btnOpenOrgTree Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void initSetting() {
        initSearchQueryVODefautSetting(search10QueryVO);
        getDateInterval(ReportType.NOT_CLOSE.getDateType().name());
        search10QueryVO.setStartDate(null);
        search10QueryVO.setEndDate(new Date());
    }

    /**
     * 建立需求狀態查詢 List
     *
     * @return
     */
    public List<String> createQueryReqStatus() {
        return reqStatusUtils.createQueryReqStatus(this.search10QueryVO.getSelectRequireStatusType(), this.getAllReqStatus());
    }

    /**
     * 建立需求狀態查詢 List
     *
     * @return
     */
    public List<String> createQueryAllReqStatus() {
        return this.getAllReqStatus().stream()
                .map(each -> each.name())
                .collect(Collectors.toList());
    }

    /**
     * 取得此報表全部製作進度查詢
     *
     * @return
     */
    private List<RequireStatusType> getAllReqStatus() {
        return reqStatusUtils.createExcludeStatus(
                Lists.newArrayList(
                        RequireStatusType.INVALID,
                        RequireStatusType.CLOSE,
                        RequireStatusType.AUTO_CLOSED,
                        RequireStatusType.DRAFT));
    }

    public void clearDateType() {
        if ("4".equals(search10QueryVOCustomerFilter.getDateTypeIndexStr())) {
            search10QueryVOCustomerFilter.setDateTypeIndexStr("");
        }
    }

    /**
     * 取得日期區間
     *
     * @param dateType
     */
    public void getDateInterval(String dateType) {
        if (DateType.PREVIOUS_MONTH.name().equals(dateType)) {
            if (search10QueryVO.getStartDate() == null) {
                search10QueryVO.setStartDate(new Date());
            }
            LocalDate lastDate = new LocalDate(search10QueryVO.getStartDate()).minusMonths(1);
            this.search10QueryVO.setStartDate(lastDate.dayOfMonth().withMinimumValue().toDate());
            this.search10QueryVO.setEndDate(lastDate.dayOfMonth().withMaximumValue().toDate());
            this.search10QueryVO.setDateTypeIndex(DateType.PREVIOUS_MONTH.ordinal());
        } else if (DateType.THIS_MONTH.name().equals(dateType)) {
            this.search10QueryVO.setStartDate(new LocalDate().dayOfMonth().withMinimumValue().toDate());
            this.search10QueryVO.setEndDate(new LocalDate().dayOfMonth().withMaximumValue().toDate());
            this.search10QueryVO.setDateTypeIndex(DateType.THIS_MONTH.ordinal());
        } else if (DateType.NEXT_MONTH.name().equals(dateType)) {
            if (search10QueryVO.getStartDate() == null) {
                search10QueryVO.setStartDate(new Date());
            }
            LocalDate nextDate = new LocalDate(search10QueryVO.getStartDate()).plusMonths(1);
            this.search10QueryVO.setStartDate(nextDate.dayOfMonth().withMinimumValue().toDate());
            this.search10QueryVO.setEndDate(nextDate.dayOfMonth().withMaximumValue().toDate());
            this.search10QueryVO.setDateTypeIndex(DateType.NEXT_MONTH.ordinal());
        } else if (DateType.TODAY.name().equals(dateType)) {
            this.search10QueryVO.setStartDate(new Date());
            this.search10QueryVO.setEndDate(new Date());
            this.search10QueryVO.setDateTypeIndex(DateType.TODAY.ordinal());
        } else if (DateType.NULL.name().equals(dateType)) {
            this.search10QueryVO.setStartDate(null);
            this.search10QueryVO.setEndDate(null);
            this.search10QueryVO.setDateTypeIndex(DateType.NULL.ordinal());
        }
    }

    /** 類別樹 Component CallBack */
    private final CategoryTreeCallBack categoryTreeCallBack = new CategoryTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -304765404328975263L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelCate() {
            if (categoryTreeComponent.getSelCateNodes() == null
                    || categoryTreeComponent.getSelCateNodes().length == 0) {
                search10QueryVO.setBigDataCateSids(Lists.newArrayList());
                search10QueryVO.setMiddleDataCateSids(Lists.newArrayList());
                search10QueryVO.setSmallDataCateSids(Lists.newArrayList());
                return;
            }
            categoryTreeComponent.selCate();
            search10QueryVO.setBigDataCateSids(categoryTreeComponent.getBigDataCateSids());
            search10QueryVO.setMiddleDataCateSids(categoryTreeComponent.getMiddleDataCateSids());
            search10QueryVO.setSmallDataCateSids(categoryTreeComponent.getSmallDataCateSids());
            categoryTreeComponent.clearCateSids();
        }

        @Override
        public void loadSelCate(List<String> smallDataCateSids) {
        }

        @Override
        public void actionSelectAll() {
        }

        @Override
        public void onNodeSelect() {
        }

        @Override
        public void onNodeUnSelect() {
        }
    };

    /** 報表 組織樹 Component CallBack */
    private final ReportOrgTreeCallBack reportOrgTreeCallBack = new ReportOrgTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -4620098826622339614L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelOrg() {
            search10QueryVO.setRequireDepts(orgTreeComponent.getSelOrgSids());
        }
    };

    /** 報表 組織樹 Component CallBack */
    private final ReportOrgTreeCallBack defaultReportOrgTreeCallBack = new ReportOrgTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -1843870752441198250L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelOrg() {
            search10QueryVOCustomerFilter.setRequireDepts(defaultOrgTreeComponent.getSelOrgSids());
        }
    };

    /** 類別樹 Component CallBack */
    private final CategoryTreeCallBack defaultCategoryTreeCallBack = new CategoryTreeCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 3630034819112268720L;

        @Override
        public void showMessage(String m) {
            messageCallBack.showMessage(m);
        }

        @Override
        public void confirmSelCate() {
            if (defaultCategoryTreeComponent.getSelCateNodes() == null
                    || defaultCategoryTreeComponent.getSelCateNodes().length == 0) {
                search10QueryVOCustomerFilter.setBigDataCateSids(Lists.newArrayList());
                search10QueryVOCustomerFilter.setMiddleDataCateSids(Lists.newArrayList());
                search10QueryVOCustomerFilter.setSmallDataCateSids(Lists.newArrayList());
                return;
            }
            defaultCategoryTreeComponent.selCate();
            search10QueryVOCustomerFilter.setBigDataCateSids(defaultCategoryTreeComponent.getBigDataCateSids());
            search10QueryVOCustomerFilter.setMiddleDataCateSids(defaultCategoryTreeComponent.getMiddleDataCateSids());
            search10QueryVOCustomerFilter.setSmallDataCateSids(defaultCategoryTreeComponent.getSmallDataCateSids());
            defaultCategoryTreeComponent.clearCateSids();
        }

        @Override
        public void loadSelCate(List<String> smallDataCateSids) {
        }

        @Override
        public void actionSelectAll() {
        }

        @Override
        public void onNodeSelect() {
        }

        @Override
        public void onNodeUnSelect() {
        }
    };
}
