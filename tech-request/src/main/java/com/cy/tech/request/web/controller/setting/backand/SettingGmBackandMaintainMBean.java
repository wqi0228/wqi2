/**
 * 
 */
package com.cy.tech.request.web.controller.setting.backand;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.setting.SettingGMBackandMaintainService;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerCallback;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerComponent;
import com.cy.tech.request.web.controller.component.mipker.MultItemPickerConfig;
import com.cy.tech.request.web.controller.component.mipker.helper.MultItemPickerByOrgHelper;
import com.cy.tech.request.web.controller.component.mipker.vo.MultItemPickerShowMode;
import com.cy.tech.request.web.controller.component.qkstree.QuickSelectTreeCallback;
import com.cy.tech.request.web.controller.component.qkstree.impl.OrgUserTreeMBean;
import com.cy.tech.request.web.controller.require.RequireCheckItemMBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.backend.logic.WorkBackendParamService;
import com.cy.work.backend.vo.enums.WkBackendParam;
import com.cy.work.backend.vo.enums.WkBackendParamValueType;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.common.vo.WkItem;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 後台維護 MBean
 * 
 * @author allen1214_wu
 */
@Slf4j
@Controller
@Scope("view")
public class SettingGmBackandMaintainMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8678215667062018272L;
    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private transient SettingGMBackandMaintainService settingGMBackandMaintainService;
    @Autowired
    private transient WorkBackendParamService workBackendParamService;
    @Autowired
    private transient MultItemPickerByOrgHelper multItemPickerByOrgHelper;
    @Autowired
    private transient DisplayController displayController;

    // ========================================================================
    // 元件
    // ========================================================================
    /**
     * 部門選擇器
     */
    @Getter
    private transient MultItemPickerComponent depsPicker;
    /**
     * 人員選擇器
     */
    @Autowired
    @Getter
    private transient OrgUserTreeMBean usersPicker;

    // ========================================================================
    // view 變數
    // ========================================================================
    /**
     * 目前的維護項目
     */
    private WkBackendParam maintainParam;

    /**
     * 功能1：需求單號
     */
    @Getter
    @Setter
    private String f01_RequireNo;

    /**
     * 功能1：檢查項目
     */
    @Getter
    @Setter
    private List<RequireCheckItemType> f01_selectedCheckItemTypes; // f01_WebCode

    /**
     * 功能1：檢查項目 mbean
     */
    @Getter
    @Autowired
    private transient RequireCheckItemMBean f01_requireCheckItemMBean;

    /**
     * 功能3：GM設定-部門
     */
    @Getter
    private String f03_depNames;

    /**
     * 功能3：GM設定-人員
     */
    @Getter
    private String f03_userNames;

    /**
     * 功能4：GM管理者設定-部門
     */
    @Getter
    private String f04_depNames;

    /**
     * 功能4：GM管理者設定-人員
     */
    @Getter
    private String f04_userNames;

    /**
     * 功能5：需求管理者-人員
     */
    @Getter
    private String f05_userNames;

    // ========================================================================
    // 初始化 + 重整畫面資料
    // ========================================================================
    @PostConstruct
    public void init() {
        this.reloadData();
    }

    /**
     * 重新取得畫面資料
     */
    private void reloadData() {
        // GM名單
        this.f03_depNames = WkOrgUtils.prepareDepsNameByTreeStyle(
                this.workBackendParamService.findIntsByKeywordWithOutCache(
                        WkBackendParam.GM_LIST_LV1_DEPSIDS),
                10);
        this.f03_userNames = WkUserUtils.prepareUserNameWithDep(
                this.workBackendParamService.findIntsByKeywordWithOutCache(WkBackendParam.GM_LIST_LV1_USERSIDS),
                OrgLevel.MINISTERIAL,
                true,
                "-",
                "<br/>",
                false);

        // GM管理者
        this.f04_depNames = WkOrgUtils.prepareDepsNameByTreeStyle(
                this.workBackendParamService.findIntsByKeywordWithOutCache(
                        WkBackendParam.GM_LIST_LV2_DEPSIDS),
                10);
        this.f04_userNames = WkUserUtils.prepareUserNameWithDep(
                this.workBackendParamService.findIntsByKeywordWithOutCache(WkBackendParam.GM_LIST_LV2_USERSIDS),
                OrgLevel.MINISTERIAL,
                true,
                "-",
                "<br/>",
                false);

        // 需求管理者 REQ_REQUIRE_ADMIN_USERSIDS
        this.f05_userNames = WkUserUtils.prepareUserNameWithDep(
                this.workBackendParamService.findIntsByKeywordWithOutCache(WkBackendParam.REQ_REQUIRE_ADMIN_USERSIDS),
                OrgLevel.MINISTERIAL,
                true,
                "-",
                "<br/>",
                false);
    }

    // ========================================================================
    // F01
    // ========================================================================
    /**
     * 
     */
    public void f01_Process() {
        // ====================================
        // 輸入檢查
        // ====================================
        if (WkStringUtils.isEmpty(f01_RequireNo)) {
            MessagesUtils.showWarn("未輸入需求單號！");
            return;
        }
        if (WkStringUtils.isEmpty(this.f01_selectedCheckItemTypes)) {
            MessagesUtils.showWarn("請選擇系統別！");
            return;
        }

        // ====================================
        // 執行
        // ====================================

        try {
            this.settingGMBackandMaintainService.f01_process(
                    this.f01_RequireNo,
                    this.f01_selectedCheckItemTypes,
                    SecurityFacade.getUserSid());

        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            MessagesUtils.showError(WkMessage.EXECTION);
            log.error(WkMessage.EXECTION, e);
            return;
        }

        MessagesUtils.showInfo("異動成功");
    }

    // ========================================================================
    // F03、F04
    // ========================================================================
    /**
     * 開啟部門選單
     * 
     * @param maintainTypeStr
     */
    public void openDepsPicker(String maintainPramStr) {

        // ====================================
        // 取得本次維護項目
        // ====================================
        this.maintainParam = WkBackendParam.safeValueOf(maintainPramStr);
        if (this.maintainParam == null) {
            log.error("傳入的維護參數資訊無法解析 maintainPramStr:[{}]!", maintainPramStr);
            MessagesUtils.showError(WkMessage.EXECTION);
            return;
        }

        // ====================================
        // 初始化單位選擇元件
        // ====================================
        try {
            // 選擇器設定資料
            MultItemPickerConfig config = new MultItemPickerConfig();
            config.setDefaultShowMode(MultItemPickerShowMode.TREE);
            config.setTreeModePrefixName("單位");
            config.setContainFollowing(true);
            config.setItemComparator(this.multItemPickerByOrgHelper.parpareComparator());

            // 選擇器初始化
            this.depsPicker = new MultItemPickerComponent(config, depsPickerCallback);

        } catch (Exception e) {
            String message = "單位選單初始化失敗!" + e.getMessage();
            MessagesUtils.showError(message);
            log.error(message, e);
            return;
        }

        // 開啟 dialog
        this.displayController.showPfWidgetVar("settingGmBackandMaintain_depsPicker_dlg");
    }

    /**
     * 單位選擇器 callback
     */
    private final MultItemPickerCallback depsPickerCallback = new MultItemPickerCallback() {

        /**
         * 
         */
        private static final long serialVersionUID = 6555721137806733299L;

        /**
         * 取得所有可選擇單位
         */
        @Override
        public List<WkItem> prepareAllItems() throws SystemDevelopException {
            // 取得所有單位
            return multItemPickerByOrgHelper.prepareAllOrgItems();
        }

        /**
         * 取得已選擇部門
         */
        @Override
        public List<WkItem> prepareSelectedItems() throws SystemDevelopException {
            // 取得部門
            List<Integer> depSids = workBackendParamService.findIntsByKeywordWithOutCache(maintainParam);
            // 轉為 WkItem 物件
            return multItemPickerByOrgHelper.prepareWkItemByDepSids(Sets.newHashSet(depSids));
        }

        /**
         * 準備 disable 的單位
         */
        @Override
        public List<String> prepareDisableItemSids() throws SystemDevelopException {
            // 不需要 Disable
            return Lists.newArrayList();
        }
    };

    /**
     * 開啟人員選單
     * 
     * @param maintainTypeStr
     */
    public void openUsersPicker(String maintainPramStr) {

        // ====================================
        // 取得本次維護項目
        // ====================================
        this.maintainParam = WkBackendParam.safeValueOf(maintainPramStr);
        if (this.maintainParam == null) {
            log.error("傳入的維護參數資訊無法解析 maintainPramStr:[{}]!", maintainPramStr);
            MessagesUtils.showError(WkMessage.EXECTION);
            return;
        }

        // ====================================
        // 初始化單位選擇元件
        // ====================================
        try {

            Org comp = WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId());
            if (comp == null) {
                MessagesUtils.showError("公司ID:[" + SecurityFacade.getCompanyId() + "]找不到對應資料!");
                return;

            }

            this.usersPicker.init(
                    comp.getSid(),
                    SecurityFacade.getUserSid(),
                    usersPickerCallback);

            this.usersPicker.preOpenDlg();

        } catch (Exception e) {
            String message = "人員選單初始化失敗!" + e.getMessage();
            MessagesUtils.showError(message);
            log.error(message, e);
            return;
        }

        // 開啟 dialog
        this.displayController.showPfWidgetVar("settingGmBackandMaintain_usersPicker_dlg");
    }

    /**
     * 可執行人員設定選單 callback
     */
    private final QuickSelectTreeCallback usersPickerCallback = new QuickSelectTreeCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = -3020740308602833418L;

        @SuppressWarnings("unchecked")
        @Override
        public List<Integer> getSelectedDataList() {

            // 取得已設定的人員
            List<Integer> depSids = workBackendParamService.findIntsByKeywordWithOutCache(maintainParam);

            return Lists.newArrayList(depSids);
        }
    };

    /**
     * 
     */
    public void confirmPicker() {

        // ====================================
        // 檢查維護項目是否還存在
        // ====================================
        if (this.maintainParam == null) {
            MessagesUtils.showError(WkMessage.NEED_RELOAD);
            return;
        }

        try {

            // ====================================
            // 取得選擇內容
            // ====================================
            List<Integer> sids = Lists.newArrayList();
            // 維護類型為部門時，由部門選擇器取得資訊
            if (WkBackendParamValueType.DEP_SID.equals(this.maintainParam.getWkBackendParamValueType())) {
                sids = this.multItemPickerByOrgHelper.itemsToSids(this.depsPicker.getSelectedItems());
            }
            // 維護類型為部門(含以下)時
            if (WkBackendParamValueType.DEP_SID_INCLUDE.equals(this.maintainParam.getWkBackendParamValueType())) {
                // 由部門選擇器取得資訊
                sids = this.multItemPickerByOrgHelper.itemsToSids(this.depsPicker.getSelectedItems());
                // 僅取最上層單位
                sids = WkOrgUtils.collectToTopmost(sids).stream().collect(Collectors.toList());
            }

            // 維護類型為人員時，由人員選擇器取得資訊
            else if (WkBackendParamValueType.USER_SID.equals(this.maintainParam.getWkBackendParamValueType())) {
                sids = this.usersPicker.getSelectedDataList();
            }

            // 將 sid 組為以『,』分隔的字串
            String paramContent = String.join(
                    ",",
                    sids.stream()
                            .map(sid -> sid + "")
                            .collect(Collectors.toList()));

            // ====================================
            // save
            // ====================================
            String message = this.workBackendParamService.save(
                    this.maintainParam,
                    paramContent,
                    SecurityFacade.getUserSid());

            MessagesUtils.showInfo(message);

        } catch (Exception e) {
            String message = WkMessage.PROCESS_FAILED + ":" + e.getMessage();
            MessagesUtils.showError(message);
            log.error(message, e);
            return;
        }

        // ====================================
        // 更新畫面資料
        // ====================================
        this.reloadData();

        // ====================================
        // 關閉視窗
        // ====================================
        if (WkBackendParamValueType.DEP_SID.equals(this.maintainParam.getWkBackendParamValueType())) {
            this.displayController.hidePfWidgetVar("settingGmBackandMaintain_depsPicker_dlg");
        } else if (WkBackendParamValueType.USER_SID.equals(this.maintainParam.getWkBackendParamValueType())) {
            this.displayController.hidePfWidgetVar("settingGmBackandMaintain_usersPicker_dlg");
        }
    }
}
