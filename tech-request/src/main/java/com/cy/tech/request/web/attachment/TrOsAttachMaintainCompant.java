/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.attachment;

import com.cy.tech.request.logic.vo.AttachmentVO;
import com.cy.tech.request.web.attachment.util.AttachMaintainCompant;
import com.cy.tech.request.web.controller.logic.component.OthSetSettingAttachmentLogicComponents;
import com.cy.tech.request.web.listener.AttachMaintainCallBack;
import com.cy.tech.request.web.listener.TabLoadCallBack;
import com.cy.tech.request.web.listener.UploadAttCallBack;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.google.common.collect.Lists;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import lombok.extern.slf4j.Slf4j;

/**
 * 其他設定資訊附件維護元件
 *
 * @author brain0925_liao
 */
@Slf4j
public class TrOsAttachMaintainCompant extends AttachMaintainCompant {

    /**
     * 
     */
    private static final long serialVersionUID = 4405554967941548978L;
    /** 其他設定資訊Sid */
    private String os_Sid;
    /** UploadAttCallBack */
    private UploadAttCallBack uploadAttCallBack;
    /** TabLoadCallBack */
    private TabLoadCallBack tabLoadCallBack;

    public TrOsAttachMaintainCompant(Integer userSid, String os_Sid, AttachMaintainCallBack attachMaintainCallBack) {
        super(userSid, "tech-request", attachMaintainCallBack);
        this.os_Sid = os_Sid;
    }

    /**
     * 載入資料
     *
     * @param attachmentVOs 附件List
     * @param uploadAttCallBack UploadAttCallBack
     * @param tabLoadCallBack TabLoadCallBack
     */
    public void loadData(List<AttachmentVO> attachmentVOs, UploadAttCallBack uploadAttCallBack, TabLoadCallBack tabLoadCallBack) {
        super.attachmentVOs = attachmentVOs;
        this.uploadAttCallBack = uploadAttCallBack;
        this.tabLoadCallBack = tabLoadCallBack;
    }

    /** 重新載入資料 */
    public void reloadData() {
        try {
            List<AttachmentVO> tempAttachmentVOs = OthSetSettingAttachmentLogicComponents.getInstance().getAttachmentVOByOSSid(os_Sid);
            tempAttachmentVOs.forEach(item -> {
                if (!attachmentVOs.contains(item)) {
                    item.setChecked(false);
                    if (String.valueOf(userSid).equals(item.getCreateUserSId())) {
                        item.setShowEditBtn(true);
                    } else {
                        item.setShowEditBtn(false);
                    }
                    item.setEditMode(false);
                    attachmentVOs.add(item);
                }
            });
            List<AttachmentVO> tempReal = Lists.newArrayList();
            attachmentVOs.forEach(item -> {
                if (tempAttachmentVOs.contains(item)) {
                    tempReal.add(item);
                }
            });

            Collections.sort(tempReal, attachmentVOComparator);
            attachmentVOs.clear();
            attachmentVOs.addAll(tempReal);
            //DisplayController.getInstance().update("attachmentMaintain_othSet_" + os_Sid);
            DisplayController.getInstance().update("viewPanelBottomInfoTabId:os_acc_panel_layer_zero");

        } catch (Exception e) {
            log.error("loadData", e);
        }

    }

    /** 介面點選上傳附件 */
    public void toUpload() {
        uploadAttCallBack.doUploadAtt(os_Sid);
    }

    @Override
    protected void doDeleteAtt(AttachmentVO selAttachmentVO) {
        OthSetSettingAttachmentLogicComponents.getInstance().deleteAtt(selAttachmentVO.getAttSid(), userSid);
        attachmentVOs.remove(selAttachmentVO);

        tabLoadCallBack.reloadTraceTab();
    }

    @Override
    protected void doSaveAttDesc(AttachmentVO selAttachmentVO) {
        OthSetSettingAttachmentLogicComponents.getInstance().updateAttDesc(selAttachmentVO.getAttSid(), selAttachmentVO.getAttDesc(), userSid);
    }

    transient private Comparator<AttachmentVO> attachmentVOComparator = new Comparator<AttachmentVO>() {
        @Override
        public int compare(AttachmentVO obj1, AttachmentVO obj2) {
            final String seq1 = obj1.getCreateTime();
            final String seq2 = obj2.getCreateTime();
            return seq2.compareTo(seq1);
        }
    };

}
