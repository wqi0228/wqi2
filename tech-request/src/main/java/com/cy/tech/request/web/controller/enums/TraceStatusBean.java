package com.cy.tech.request.web.controller.enums;

import com.cy.tech.request.web.pf.utils.CommonBean;
import com.cy.work.trace.vo.enums.TraceStatus;
import java.io.Serializable;
import javax.faces.model.SelectItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kasim
 */
@Controller
public class TraceStatusBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -774852654318762907L;
    @Autowired
    transient private CommonBean common;

    public SelectItem[] getValues() {
        SelectItem[] items = new SelectItem[TraceStatus.values().length];
        int i = 0;
        for (TraceStatus each : TraceStatus.values()) {
            String label = common.get(each);
            items[i++] = new SelectItem(each, label);
        }
        return items;
    }
}
