/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.home;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.omnifaces.util.Faces;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.Home03QueryService;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.Home03View;
import com.cy.tech.request.logic.service.AlertInboxService;
import com.cy.tech.request.logic.service.RequireReadRecordHelper;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.URLService.URLServiceAttr;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.ForwardType;
import com.cy.tech.request.vo.require.AlertInbox;
import com.cy.tech.request.vo.require.AlertInboxSendGroup;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.value.to.ReadRecordGroupAdvanceTo;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.homeheader.Home03HeaderMBean;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.require.RequireForwardDeAndPersonMBean;
import com.cy.tech.request.web.controller.search.TableUpDownBean;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.searchheader.CommonHeaderMBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.logic.lib.manager.WorkCommonReadRecordManager;
import com.cy.work.common.logic.lib.manager.to.WorkCommonReadRecordTo;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.group.vo.WorkLinkGroup;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 寄件備份
 *
 * @author jason_h
 */
@Controller
@Scope("view")
@Slf4j
public class Home03MBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6897177572836839627L;
    @Autowired
    transient private TableUpDownBean upDownBean;
    @Autowired
    transient private Home03HeaderMBean headerMBean;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private AlertInboxService inboxService;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private ReqLoadBean loadManager;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private URLService urlService;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private Require01MBean r01MBean;
    @Autowired
    transient private Home03QueryService home03QueryService;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchResultHelper searchResultHelper;
    @Autowired
    transient private RequireReadRecordHelper requireReadRecordHelper;
    @Autowired
    transient private WorkCommonReadRecordManager workCommonReadRecordManager;

    /** 所有的收件資訊 */
    @Getter
    @Setter
    private List<Home03View> queryItems;
    /** 選擇的收件資訊 */
    @Getter
    @Setter
    private Home03View querySelection;
    /** 上下筆移動keeper */
    @Getter
    private Home03View queryKeeper;
    /** 切換模式 */
    @Getter
    private SwitchType switchType = SwitchType.CONTENT;
    /** 切換模式 - 全畫面狀態 */
    @Getter
    private SwitchType switchFullType = SwitchType.DETAIL;
    /** 在匯出的時候，某些內容需要隱藏 */
    @Getter
    private boolean hasDisplay = true;
    @Getter
    private final String dataTableId = "dtRequire";

    private Map<AlertInboxSendGroup, List<AlertInbox>> alertCache = Maps.newHashMap();

    @PostConstruct
    public void init() {
        headerMBean.clear();
        this.search();
    }

    public void search() {
        queryItems = this.findWithQuery();
        alertCache = inboxService.getCacheByForwardType(ForwardType.FORWARD_DEPT);
    }

    /**
     * 還原預設值並查詢
     */
    public void clear() {
        this.init();
    }

    private List<Home03View> findWithQuery() {
        String requireNo = reqularUtils.getRequireNo(loginBean.getCompanyId(), commHeaderMBean.getFuzzyText());
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT "
                + "aig.alert_group_sid,"
                + "tr.require_sid,"
                + "tr.require_no,"
                + "tid.field_content,"
                + "tr.urgency,"
                + "tr.has_forward_dep,"
                + "tr.has_forward_member,"
                + "tr.has_link,"
                + "aig.forward_type,"
                + "aig.send_to,"
                + "aig.create_dt,"
                + "ckm.big_category_name,"
                + "tr.update_dt,"
                + this.builderHasTraceColumn() + ", "
                // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
                + this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire()
                + " FROM ");
        buildSendGroupInboxCondition(requireNo, builder, parameters);
        buildInboxCondition(requireNo, builder, parameters);
        buildRequireCondition(requireNo, builder, parameters);
        buildRequireIndexDictionaryCondition(requireNo, builder, parameters);
        buildCategoryKeyMappingCondition(requireNo, builder, parameters);
        // 檢查項目 (系統別)
        // 後方需對 主單 sid 做 group by
        builder.append(this.searchConditionSqlHelper.prepareCommonJoin(SecurityFacade.getUserSid()));

        builder.append("WHERE aig.alert_group_sid IS NOT NULL ");
        // 查詢條件：是否閱讀
        if (WkStringUtils.isEmpty(requireNo)) {
            builder.append(
                    this.workCommonReadRecordManager.prepareWhereConditionSQL(
                            headerMBean.getSelectReadRecordType(), "readRecord"));
        }

        builder.append(" GROUP BY tr.require_sid ");
        builder.append("  ORDER BY aig.create_dt DESC");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.INBOX_SEND_BACKUP, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.INBOX_SEND_BACKUP, SecurityFacade.getUserSid());

        // 查詢
        List<Home03View> resultList = home03QueryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // ====================================
        // 後續處理
        // ====================================
        if (WkStringUtils.notEmpty(resultList)) {
            // 後續處理-開始
            usageRecord.afterProcessStart();

            // ====================================
            // 查詢條件過濾：系統別
            // ====================================
            resultList = resultList.stream()
                    .filter(each -> this.searchResultHelper.filterCheckItems(
                            each.getCheckItemTypes(),
                            commHeaderMBean.getCheckItemTypes(),
                            false,
                            null))
                    .collect(Collectors.toList());

            // ====================================
            // 組裝閱讀記錄
            // ====================================
            if (WkStringUtils.notEmpty(resultList)) {

                List<String> requireSids = resultList.stream()
                        .map(Home03View::getRequireSid)
                        .collect(Collectors.toList());

                // 查詢所有閱讀記錄
                Map<String, List<WorkCommonReadRecordTo>> readRecordsMapByRequireSid = this.requireReadRecordHelper.findByFormSids(
                        FormType.REQUIRE, requireSids);

                // 逐筆組裝
                for (Home03View result : resultList) {
                    if (readRecordsMapByRequireSid.containsKey(result.getRequireSid())) {
                        result.setReadRecordTos(readRecordsMapByRequireSid.get(result.getRequireSid()));
                    }
                }
            }

            // 後續處理-結束
            usageRecord.afterProcessEnd();
        }

        // 使用記錄
        usageRecord.saveUsageRecord();

        return resultList;
    }

    private void buildSendGroupInboxCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("(SELECT * FROM tr_alert_inbox_send_group aig  WHERE 1=1");

        builder.append(" AND aig.create_usr = :createdUser");
        parameters.put("createdUser", loginBean.getUser().getSid());

        // 寄發類型
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(headerMBean.getSelectForwardType())) {
            builder.append(" AND aig.forward_type = :aigForwardType");
            parameters.put("aigForwardType", headerMBean.getSelectForwardType());
        }
        builder.append(") AS aig ");
    }

    private void buildInboxCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT DISTINCT ai.alert_group_sid FROM tr_alert_inbox ai  WHERE 1=1");
        // 寄發者 - 寄件人
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(headerMBean.getReceive())) {
            List<Integer> receives = WkUserUtils.findByNameLike(headerMBean.getReceive(), SecurityFacade.getCompanyId());
            if (WkStringUtils.isEmpty(receives)) {
                receives.add(-999);
            }

            builder.append(" AND ai.receive IN (:receive)");
            parameters.put("receive", receives.isEmpty() ? "" : receives);
        }
        // 寄發至 - 收件部門
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getRequireDepts() != null && !commHeaderMBean.getRequireDepts().isEmpty()) {
            builder.append(" AND ai.receive_dep IN (:receiveDep)");
            parameters.put("receiveDep", commHeaderMBean.getRequireDepts() == null || commHeaderMBean.getRequireDepts().isEmpty()
                    ? ""
                    : commHeaderMBean.getRequireDepts());
        }
        builder.append(") AS ai ON ai.alert_group_sid=aig.alert_group_sid ");
    }

    private void buildRequireCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_require tr WHERE 1=1");
        // 異動區間
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getStartUpdatedDate() != null && commHeaderMBean.getEndUpdatedDate() != null) {
            builder.append(" AND tr.update_dt BETWEEN :startUpdatedDate AND :endUpdatedDate");
            parameters.put("startUpdatedDate", helper.transStartDate(commHeaderMBean.getStartUpdatedDate()));
            parameters.put("endUpdatedDate", helper.transEndDate(commHeaderMBean.getEndUpdatedDate()));
        }
        // 需求單號
        if (!Strings.isNullOrEmpty(requireNo)) {
            builder.append(" AND tr.require_no = :requireNo");
            parameters.put("requireNo", requireNo);
        }
        if (Strings.isNullOrEmpty(requireNo) && headerMBean.isAdvanceCondition()) {
            this.buildRequireAdvanceCondition(requireNo, builder, parameters);
        }
        builder.append(") AS tr ON tr.require_sid=aig.require_sid ");
    }

    private void buildRequireAdvanceCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        //////////////////// 以下為進階搜尋條件//////////////////////////////
        // 立單區間
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getStartDate() != null && commHeaderMBean.getEndDate() != null) {
            builder.append(" AND tr.create_dt BETWEEN :startDate AND :endDate");
            parameters.put("startDate", helper.transStartDate(commHeaderMBean.getStartDate()));
            parameters.put("endDate", helper.transEndDate(commHeaderMBean.getEndDate()));
        }
        // 需求類別製作進度
        builder.append(" AND tr.require_status IN (:requireStatus)");
        if (Strings.isNullOrEmpty(requireNo)) {
            parameters.put("requireStatus", headerMBean.createQueryReqStatus());
        } else {
            parameters.put("requireStatus", headerMBean.createQueryAllReqStatus());
        }

        // 緊急度
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getUrgencyTypeList() != null && !commHeaderMBean.getUrgencyTypeList().isEmpty()) {
            builder.append(" AND tr.urgency IN (:urgencyList)");
            parameters.put("urgencyList", commHeaderMBean.getUrgencyTypeList());
        }
        // 需求單號
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(commHeaderMBean.getRequireNo())) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(commHeaderMBean.getRequireNo()) + "%";
            builder.append(" AND tr.require_no LIKE :requireNo");
            parameters.put("requireNo", textNo);
        }
    }

    private void buildRequireIndexDictionaryCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {

        // 主題 | 內容 | 模糊搜尋
        builder.append("INNER JOIN (SELECT tid.require_sid,tid.field_content FROM tr_index_dictionary tid WHERE 1=1");
        // 模糊搜尋
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(commHeaderMBean.getFuzzyText())) {
            String text = "%" + reqularUtils.replaceIllegalSqlLikeStr(commHeaderMBean.getFuzzyText()) + "%";
            builder.append(" AND (tid.require_sid"
                    + " IN (SELECT DISTINCT tid1.require_sid FROM tr_index_dictionary tid1 WHERE tid1.field_content LIKE :text)"
                    + " OR tid.require_sid IN (SELECT DISTINCT trace.require_sid FROM tr_require_trace trace WHERE "
                    + "trace.require_trace_type = 'REQUIRE_INFO_MEMO' AND trace.require_trace_content LIKE :text))");
            parameters.put("text", text);
        }

        // only 主題
        if (Strings.isNullOrEmpty(requireNo) && Strings.isNullOrEmpty(commHeaderMBean.getFuzzyText())
                && Strings.isNullOrEmpty(headerMBean.getContent())
                && !Strings.isNullOrEmpty(headerMBean.getTheme())) {
            String themeText = "%" + reqularUtils.replaceIllegalSqlLikeStr(headerMBean.getTheme()) + "%";
            builder.append(" AND (tid.field_name = '主題' AND tid.field_content LIKE :themeText)");
            parameters.put("themeText", themeText);
        }

        // only 內容
        if (Strings.isNullOrEmpty(requireNo) && Strings.isNullOrEmpty(commHeaderMBean.getFuzzyText())
                && Strings.isNullOrEmpty(headerMBean.getTheme())
                && !Strings.isNullOrEmpty(headerMBean.getContent())) {
            String contentText = "%" + reqularUtils.replaceIllegalSqlLikeStr(headerMBean.getContent()) + "%";
            builder.append(" AND tid.require_sid IN (SELECT DISTINCT tid1.require_sid FROM tr_index_dictionary tid1 WHERE "
                    + "tid1.field_name!='主題' AND tid1.field_content LIKE :contentText)");
            parameters.put("contentText", contentText);
        }

        // 內容 & 主題
        if (Strings.isNullOrEmpty(requireNo) && Strings.isNullOrEmpty(commHeaderMBean.getFuzzyText())
                && !Strings.isNullOrEmpty(headerMBean.getTheme())
                && !Strings.isNullOrEmpty(headerMBean.getContent())) {
            String contentText = "%" + reqularUtils.replaceIllegalSqlLikeStr(headerMBean.getContent()) + "%";
            String themeText = "%" + reqularUtils.replaceIllegalSqlLikeStr(headerMBean.getTheme()) + "%";

            builder.append(" AND tid.require_sid IN (SELECT DISTINCT tid1.require_sid FROM tr_index_dictionary tid1 WHERE "
                    + "tid1.field_name!='主題' AND tid1.field_content LIKE :contentText)");
            parameters.put("contentText", contentText);

            builder.append(" AND (tid.field_name = '主題' AND tid.field_content LIKE :themeText)");
            parameters.put("themeText", themeText);
        }

        builder.append(" AND tid.field_name='主題') AS tid ON tr.require_sid=tid.require_sid ");
    }

    private void buildCategoryKeyMappingCondition(String requireNo, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_category_key_mapping ckm WHERE 1=1");
        // 需求類別
        if (Strings.isNullOrEmpty(requireNo) && commHeaderMBean.getSelectBigCategory() != null) {
            builder.append(" AND ckm.big_category_sid = :big");
            parameters.put("big", commHeaderMBean.getSelectBigCategory().getSid());
        }
        builder.append(") AS ckm ON tr.mapping_sid=ckm.key_sid ");
    }

    /**
     * 是否有追蹤資料
     *
     * @return
     */
    private String builderHasTraceColumn() {
        StringBuilder builder = new StringBuilder();
        builder.append(" (SELECT CASE WHEN (COUNT(trace.tracesid) > 0) THEN 'TRUE' ELSE 'FALSE' END ");
        builder.append(" FROM work_trace_info trace ");
        builder.append(" WHERE trace.trace_source_no = tr.require_no");
        builder.append(" AND trace.trace_source_type = 'TECH_REQUEST'");
        builder.append(" AND (trace.trace_dep = ").append(loginBean.getDep().getSid());
        builder.append(" OR trace.trace_usr = ").append(loginBean.getUser().getSid()).append(")");
        builder.append(" AND trace.trace_status = 'UN_FINISHED'");
        builder.append(" AND trace.Status = '0'");
        builder.append(") AS hasTrace ");
        return builder.toString();
    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (switchType.equals(SwitchType.CONTENT)) {
            switchType = SwitchType.DETAIL;
            if (querySelection != null) {
                queryKeeper = querySelection;
            } else if (this.queryKeeper == null) {
                this.querySelection = this.queryKeeper = this.queryItems.get(0);
            }
            this.changeRequireContent(queryKeeper);
            return;
        }
        if (switchType.equals(SwitchType.DETAIL)) {
            switchFullType = SwitchType.DETAIL;
            switchType = SwitchType.CONTENT;
        }
    }

    /**
     * 變更需求單內容
     *
     * @param view
     */
    private void changeRequireContent(Home03View view) {
        // 開啟檢視頁面後，會由檢視頁面的程式記錄，故在此僅異動頁面資料
        this.requireReadRecordHelper.changeToAlreadyReadAndNotSave(
                this.querySelection.getReadRecordTos(), SecurityFacade.getUserSid());

        Require r = requireService.findByReqNo(view.getRequireNo());
        loadManager.reloadReqForm(r, loginBean.getUser(), RequireBottomTabType.INBOX_SEND);
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (Home03View) event.getObject();
        this.changeRequireContent(this.queryKeeper);
    }

    /**
     * 切換 - 全畫面需求單
     *
     * @param view
     */
    public void fullScreenForm(Home03View view) {
        this.queryKeeper = this.querySelection = view;
        switchFullType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();
    }

    /**
     * 切換 - 返回報表
     */
    public void normalScreenReport() {
        this.querySelection = this.queryKeeper;
        switchFullType = SwitchType.DETAIL;
        this.toggleSearchBody();
    }

    /**
     * 上下筆移動
     *
     * @param action
     */
    public void moveRequireTemplateSelect(int action) {
        int index = this.queryItems.indexOf(this.queryKeeper);
        index += action;
        if (index < 0 || index >= this.queryItems.size()) {
            return;
        }
        this.querySelection = this.queryKeeper = this.queryItems.get(index);
        this.changeRequireContent(this.querySelection);
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        helper.exportExcel(document, commHeaderMBean.getStartDate(), commHeaderMBean.getEndDate(), commHeaderMBean.getReportType());
        hasDisplay = true;
    }

    /**
     * 隱藏部分column裡的內容
     */
    public void hideColumnContent() {
        hasDisplay = false;
    }

    /**
     * 列表執行關聯動作
     *
     * @param view
     */
    public void initRelevance(Home03View view) {
        this.querySelection = view;
        this.queryKeeper = this.querySelection;
        Require r = requireService.findByReqNo(view.getRequireNo());
        this.r01MBean.setRequire(r);
        this.r01MBean.getRelevanceMBean().initRelevance();
    }

    public String getRelevanceViewUrl(Home03View view) {
        if (view == null) {
            return "";
        }
        Require r = requireService.findByReqNo(view.getRequireNo());
        if (r == null) {
            return "";
        }
        WorkLinkGroup link = r.getLinkGroup();
        if (link == null) {
            return "";
        }
        return "../require/require04.xhtml" + urlService.createSimpleURLLink(URLServiceAttr.URL_ATTR_L, link.getSid(), view.getRequireNo(), 1);
    }

    /**
     * 列表點選轉寄功能
     * 
     * @param view
     */
    public void openForwardDialog(Home03View view) {
        this.queryKeeper = this.querySelection = view;

        if (SwitchType.DETAIL.equals(switchType)) {
            this.changeRequireContent(this.queryKeeper);
            this.display.update(Lists.newArrayList(
                    "@(.reportUpdateClz)",
                    "title_info_click_btn_id",
                    "require01_title_info_id",
                    "require_template_id",
                    "viewPanelBottomInfoId",
                    "req03botmid",
                    this.dataTableId + "1"));
        }

        RequireForwardDeAndPersonMBean mbean = WorkSpringContextHolder.getBean(RequireForwardDeAndPersonMBean.class);
        mbean.openDialog(view.getRequireNo(), r01MBean);
    }

    public String findReadMsgCnt(Home03View view) {
        ReadRecordGroupAdvanceTo to = inboxService.createInboxReadRecord(
                view.getReadRecordTos(),
                alertCache.get(view.getInboxSendGroup()));
        return to.findReadSizeStr().trim() + " / " + to.findTotalSizeStr().trim();
    }

    public ReadRecordGroupAdvanceTo createReadMsg() {
        if (querySelection == null) {
            return null;
        }
        return inboxService.createInboxReadRecord(
                querySelection.getReadRecordTos(),
                alertCache.get(querySelection.getInboxSendGroup()));
    }

    public void handlerReadMsgDetail() {
        if (!Faces.getRequestParameterMap().containsKey("readInboxIdx")) {
            return;
        }
        int readInboxIdx = Integer.valueOf(Faces.getRequestParameterMap().get("readInboxIdx"));
        this.querySelection = this.queryKeeper = this.queryItems.get(readInboxIdx);
    }

    /**
     * 開啟分頁
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    public void btnOpenUrl(String dtId, String widgetVar, String pageCount, Home03View to) {
        this.highlightReportTo(widgetVar, pageCount, to);
        this.resetUpdownInfo();
        this.removeClassByTextBold(dtId, pageCount);
        this.transformHasRead();
        this.checkHelfScreen();
    }

    private boolean checkHelfScreen() {
        if (switchFullType.equals(SwitchType.DETAIL) && !switchType.equals(SwitchType.CONTENT)
                || switchFullType.equals(SwitchType.FULLCONTENT) && !switchType.equals(SwitchType.CONTENT)) {
            this.normalScreenReport();
            display.update("headerTitle");
            display.update("searchBody");
            return true;
        }
        return false;
    }

    /**
     * highlight列表位置
     *
     * @param widgetVar
     * @param pageCount
     * @param to
     */
    private void highlightReportTo(String widgetVar, String pageCount, Home03View to) {
        querySelection = to;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 取得索引位置
     *
     * @param pageCountStr
     * @return
     */
    private int getRowIndex(String pageCountStr) {
        Integer pageCount = 50;
        if (!Strings.isNullOrEmpty(pageCountStr)) {
            try {
                pageCount = Integer.valueOf(pageCountStr);
            } catch (Exception e) {
                log.error("dataTable pageCount 轉型失敗 ：" + pageCountStr, e);
            }
        }
        return queryItems.indexOf(querySelection) % pageCount;
    }

    /**
     * 去除粗體Class
     *
     * @param dtId
     * @param pageCount
     */
    private void removeClassByTextBold(String dtId, String pageCount) {
        display.execute("removeClassByTextBold('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
        display.execute("changeAlreadyRead('" + dtId + "'," + this.getRowIndex(pageCount) + ");");
    }

    /**
     * 變更已閱讀
     */
    private void transformHasRead() {

        // 開啟檢視頁面後，會由檢視頁面的程式記錄，故在此僅異動頁面資料
        this.requireReadRecordHelper.changeToAlreadyReadAndNotSave(
                this.querySelection.getReadRecordTos(), SecurityFacade.getUserSid());

        queryKeeper = querySelection;
        queryItems.set(queryItems.indexOf(querySelection), querySelection);
        DisplayController.getInstance().update(this.dataTableId);
    }

    /**
     * 重設定上下筆資訊
     */
    private void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
        upDownBean.resetTabInfo(RequireBottomTabType.INBOX_SEND, queryKeeper.getInboxSendGroup().getSid());
    }

    /**
     * 上一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnUp(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (index > 0) {
            index--;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 下一筆（分頁）
     *
     * @param dtId
     * @param widgetVar
     */
    public void openerByBtnDown(String dtId, String widgetVar, String pageCount) {
        int index = queryItems.indexOf(querySelection != null ? querySelection : queryKeeper);
        if (queryItems.size() > index + 1) {
            index++;
            querySelection = queryItems.get(index);
        }
        this.refreshViewByOpener(dtId, widgetVar, pageCount);
    }

    /**
     * 刷新列表（分頁）
     *
     * @param dtId
     * @param widgetVar
     * @param pageCount
     */
    private void refreshViewByOpener(String dtId, String widgetVar, String pageCount) {
        queryKeeper = querySelection;
        this.transformHasRead();
        this.removeClassByTextBold(dtId, pageCount);
        this.highlightReportTo(widgetVar, pageCount, queryKeeper);
        this.resetUpdownInfo();
        this.checkHelfScreen();
    }

    /**
     * 列表執行追蹤動作
     *
     * @param view
     */
    public void btnAddTrack(Home03View view) {
        Require r = this.highlightAndReturnRequire(view);
        this.r01MBean.setRequire(r);
        this.r01MBean.getTraceActionMBean().initTrace(this.r01MBean);
    }

    /**
     * 列表標註及回傳需求單
     *
     * @param view
     * @return
     */
    private Require highlightAndReturnRequire(Home03View view) {
        this.querySelection = view;
        this.queryKeeper = this.querySelection;
        return requireService.findByReqNo(view.getRequireNo());
    }
}
