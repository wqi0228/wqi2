package com.cy.tech.request.web.newsearch.newsearch02;

import com.cy.tech.request.web.newsearch.NewSearchConditionVO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class NewSearch02ConditionVO extends NewSearchConditionVO {
    /**
     * 
     */
    private static final long serialVersionUID = 4561917390524547906L;
    /** 需求完成確認單位負責人 */
    @Getter
    @Setter
    private String reqConcirmDepOwnerName;
}
