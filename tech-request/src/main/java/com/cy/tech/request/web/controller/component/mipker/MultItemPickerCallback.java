package com.cy.tech.request.web.controller.component.mipker;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.SelectItem;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.web.controller.component.mipker.helper.MultItemPickerByOrgHelper;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.vo.WkItem;
import com.cy.work.logic.SettingCustomGroupService;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class MultItemPickerCallback implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6010485240897131030L;

    // ========================================================================
    // 服務
    // ========================================================================
    private MultItemPickerByOrgHelper multItemPickerByOrgHelper = WorkSpringContextHolder.getBean(MultItemPickerByOrgHelper.class);

    private SettingCustomGroupService settingCustomGroupService = WorkSpringContextHolder.getBean(SettingCustomGroupService.class);

    // ========================================================================
    // var
    // ========================================================================
    /**
     * 預設提供的實作方法
     */
    @Getter
    @Setter
    private MultItemPickerCallbackDefaultImplType defaultImplType;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 傳入類型, 可呼叫預設實作方法內容
     * 
     * @param defaultImplType
     */
    public MultItemPickerCallback(MultItemPickerCallbackDefaultImplType defaultImplType) {
        this.defaultImplType = defaultImplType;
    }

    /**
     * 準備所有的項目
     * 
     * @return
     */
    public List<WkItem> prepareAllItems() throws SystemDevelopException {

        // 組件提供的預設方法
        if (defaultImplType != null) {
            switch (defaultImplType) {
            case DEP:
                // 取得所有單位
                return this.multItemPickerByOrgHelper.prepareAllOrgItems();
            default:
                break;
            }
        }

        throw new SystemDevelopException("MultItemPickerCallback: 未實做 prepareAllItem");
    }

    /**
     * 準備已選擇項目
     * 
     * @return
     */
    public List<WkItem> prepareSelectedItems() throws SystemDevelopException {
        throw new SystemDevelopException("MultItemPickerCallback: 未實做 prepareSelectedItems");
    }

    /**
     * 準備鎖定項目
     * 
     * @return
     */
    public List<String> prepareDisableItemSids() throws SystemDevelopException {
        throw new SystemDevelopException("SingleSelectTreeCallback: 未實做 prepareDisableItemSids");
    }

    /**
     * @return 群組項目
     * @throws SystemDevelopException
     */
    public List<SelectItem> prepareGroupItems() throws SystemDevelopException {

        // 組件提供的預設方法
        if (defaultImplType != null) {
            switch (defaultImplType) {
            case DEP:
                // 取得所有單位
                return this.settingCustomGroupService.findDepGroupForMultItemPicker(SecurityFacade.getUserSid());
            default:
                break;
            }
        }

        throw new SystemDevelopException("MultItemPickerCallback: 有開啟群組功能，但未實做 prepareGroupItems");

    }

    /**
     * 依據傳入的群組值, 準備該群組的項目
     * 
     * @return 回傳群組項目
     * @throws SystemDevelopException 未實做時拋出
     */
    public List<String> prepareItemSidByGroupSid(String groupSid) throws SystemDevelopException {
        

        // 組件提供的預設方法
        if (defaultImplType != null) {
            switch (defaultImplType) {
            case DEP:
                // 取得所有單位
                return this.settingCustomGroupService.findGroupDepsBySid(Long.parseLong(groupSid));
            default:
                break;
            }
        }
        
        throw new SystemDevelopException("MultItemPickerCallback: 有開啟群組功能，但未實做 prepareItemSidByGroupSid");
    }

    /**
     * 已選擇項目, 顯示前整理
     * 
     * @param targetItems
     * @return
     */
    public List<WkItem> beforTargetShow(List<WkItem> targetItems) {
        return targetItems;
    }
}
