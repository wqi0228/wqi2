/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search.helper;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.view.Search31View;
import com.cy.tech.request.logic.service.CommonService;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.vo.enums.OthSetStatus;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.web.controller.logic.component.WorkCalendarLogicComponent;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

/**
 * @author saul_chen
 */
@Component
public class Search31Helper implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -2103943551405703595L;
    @Autowired
    private SearchService searchHelper;
    @Autowired
    private URLService urlService;
    @Autowired
    transient private CommonService commonService;
    @Autowired
    transient private UserService userService;
    @Autowired
    private WorkCalendarLogicComponent workCalendarLogicComponent;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;

    @PersistenceContext
    transient private EntityManager em;

    public List<Search31View> search(Org loginDep, User loginUser) {

        StringBuilder builder = new StringBuilder();
        builder.append("SELECT "
                + "os.os_sid,"
                + "tr.require_no,"
                + "tid.field_content,"
                + "os.create_dt AS osCreateDt,"
                + "os.os_theme,"
                + "os.os_status,"
                + "os.dep_sid as osCreateDep,"
                + "os.create_usr AS osCreateUsr,"
                + "tr.create_dt AS reqCreateDt,"
                + "ckm.big_category_name,"
                + "ckm.middle_category_name,"
                + "ckm.small_category_name,"
                + "tr.dep_sid AS requireDep,"
                + "tr.create_usr AS requireUser,"
                + "tr.urgency, "
                // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
                + this.searchConditionSqlHelper.prepareCommonSelectColumnByRequire()
                + " FROM ");
        buildOsCondition(builder, loginDep.getSid().toString());
        buildRequireCondition(builder, loginDep.getSid().toString());
        buildRequireIndexDictionaryCondition(builder);
        builder.append("INNER JOIN tr_category_key_mapping ckm ON tr.mapping_sid=ckm.key_sid ");

        // 檢查項目 (系統別)
        // 後方需對 主單 sid 做 group by
        builder.append(this.searchConditionSqlHelper.prepareCommonJoin(SecurityFacade.getUserSid()));

        builder.append("WHERE os.os_sid IS NOT NULL ");
        builder.append(" GROUP BY os.os_sid ");
        builder.append("ORDER BY os.update_dt DESC ");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.TODO_OTHER_SETUP_SETTING, builder.toString(), null);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.TODO_OTHER_SETUP_SETTING, SecurityFacade.getUserSid());

        // 查詢
        List<Search31View> resultList = this.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                SecurityFacade.getUserSid(),
                usageRecord);

        // 儲存使用記錄
        usageRecord.saveUsageRecord();

        return resultList;
    }

    private void buildOsCondition(StringBuilder builder, String depId) {
        /** [tr_os].[create_dt] < systemdate -3天的工作日筆數 */
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String checkDate = sdf.format(workCalendarLogicComponent.getSpecialWorkDay(new Date(), 3));

        builder.append("(   SELECT * FROM tr_os os WHERE 1=1");
        builder.append("        AND os_status = '").append(OthSetStatus.PROCESSING.name()).append("' ");
        builder.append("        AND os_notice_deps like '").append("%\"").append(depId).append("\"%").append("' ");
        builder.append("        AND os.create_dt <= '").append(checkDate).append(" 23:59:59' ");
        builder.append(") AS os ");
    }

    private void buildRequireCondition(StringBuilder builder, String depId) {
        builder.append("INNER JOIN (SELECT * FROM tr_require tr WHERE tr.has_os=1");
        builder.append("        AND tr.require_status = '").append(RequireStatusType.PROCESS.name()).append("' ");
        builder.append("        AND tr.dep_sid <> '").append(depId).append("' ");
        builder.append(") AS tr ON tr.require_no = os.require_no ");
    }

    private void buildRequireIndexDictionaryCondition(StringBuilder builder) {
        builder.append("INNER JOIN (SELECT * FROM tr_index_dictionary tid WHERE "
                + "tid.field_name='主題') AS tid ON "
                + "tr.require_sid = tid.require_sid ");
    }

    private List<Search31View> findWithQuery(
            String sql,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {

        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        // ====================================
        // 查詢
        // ====================================
        Query query = em.createNativeQuery(sql);

        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        @SuppressWarnings("unchecked")
        List<Object[]> result = query.getResultList();
        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if (WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();

        List<Search31View> viewResult = Lists.newArrayList();
        for (int i = 0; i < result.size(); i++) {

            Search31View v = new Search31View();
            int index = 0;
            Object[] record = (Object[]) result.get(i);
            String sid = (String) record[index++];
            String requireNo = (String) record[index++];
            String requireTheme = (String) record[index++];
            Date osCreateDt = (Date) record[index++];
            String osTheme = (String) record[index++];
            String osStatus = (String) record[index++];
            Integer createDep = (Integer) record[index++];
            Integer createdUser = (Integer) record[index++];
            Date reqCreateDt = (Date) record[index++];
            String bigName = (String) record[index++];
            String middleName = (String) record[index++];
            String smallName = (String) record[index++];
            Integer requireDep = (Integer) record[index++];
            Integer requireUser = (Integer) record[index++];
            Integer urgency = (Integer) record[index++];
            // 處理共通欄位 (一定要擺在『取值』最後面, 否則 index 會不對)
            v.prepareCommonColumn(record, index);

            v.setSid(sid);
            v.setRequireNo(requireNo);
            v.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
            v.setOsCreateDt(sdf1.format(osCreateDt));
            v.setOsTheme(osTheme);
            v.setOsStatus(commonService.get(OthSetStatus.valueOf(osStatus)));
            v.setCreateDep(WkOrgCache.getInstance().findNameBySid(createDep));
            v.setCreatedUser(userService.getUserName(createdUser));
            v.setReqCreateDt(sdf1.format(reqCreateDt));
            v.setBigName(bigName);
            v.setMiddleName(middleName);
            v.setSmallName(smallName);
            v.setRequireDep(WkOrgCache.getInstance().findNameBySid(requireDep));
            v.setRequireUser(userService.getUserName(requireUser));
            v.setUrgency(commonService.get(UrgencyType.values()[urgency]));

            v.setLocalUrlLink(urlService.createLocalUrlLinkParamForTab(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(execUserSid, v.getRequireNo(), 1),
                    URLService.URLServiceAttr.URL_ATTR_TAB_OS, v.getSid()));

            viewResult.add(v);
        }
        // 解析資料-結束
        usageRecord.parserDataEnd();
        return viewResult;
    }
}
