package com.cy.tech.request.web.controller.searchheader;

import com.cy.work.common.enums.InstanceStatus;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 送測簽核進度查詢表頭
 *
 * @author jason_h
 */
@Controller
@Scope("view")
public class Search14HeaderMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5439105098515346544L;

    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;

    /** 待審核人員 */
    @Getter
    @Setter
    private String pendingSignOnUserName;

    /** 送測簽核審核狀態 */
    @Getter
    @Setter
    private List<String> selectStatuses;

    @PostConstruct
    private void init() {
        this.clear();
    }

    public void clear() {
        this.initCommHeader();
        this.initHeader();
    }

    /**
     * 初始化commHeader
     */
    private void initCommHeader() {
        commHeaderMBean.clear();
        commHeaderMBean.clearAdvanceWithoutForwardAndUrgency();
        commHeaderMBean.initDefaultRequireDepts(true);
    }

    /**
     * 初始化Header
     */
    private void initHeader() {
        pendingSignOnUserName = null;
        this.initInstanceStatuses();
    }

    private void initInstanceStatuses() {
        selectStatuses = Lists.newArrayList();
        selectStatuses.add(InstanceStatus.NEW_INSTANCE.name());
        selectStatuses.add(InstanceStatus.WAITING_FOR_APPROVE.name());
        selectStatuses.add(InstanceStatus.RECONSIDERATION.name());
        selectStatuses.add(InstanceStatus.APPROVING.name());
    }

    public SelectItem[] getInstanceStatusItems() {
        InstanceStatus[] types = new InstanceStatus[8];
        types[0] = InstanceStatus.NEW_INSTANCE;
        types[1] = InstanceStatus.WAITING_FOR_APPROVE;
        types[2] = InstanceStatus.INVALID;
        types[3] = InstanceStatus.RECONSIDERATION;
        types[4] = InstanceStatus.APPROVING;
        types[5] = InstanceStatus.APPROVED;
        types[6] = InstanceStatus.CLOSED;
        types[7] = InstanceStatus.REQUIRE_SUSPENDED_CANCLE;

        SelectItem[] items = new SelectItem[types.length];
        for (int i = 0; i < types.length; i++) {
            items[i] = new SelectItem(types[i], types[i].getLabel());
        }
        return items;
    }

    public List<String> getInstancePaperCodes() {
        List<String> codes = Lists.newArrayList();
        selectStatuses.forEach(status -> codes.add(InstanceStatus.valueOf(status).getValue()));
        return codes;
    }
}
