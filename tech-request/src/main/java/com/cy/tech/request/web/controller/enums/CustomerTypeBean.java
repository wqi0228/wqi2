/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.enums;

import com.cy.tech.request.web.pf.utils.CommonBean;
import com.cy.work.customer.vo.enums.CustomerType;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.SelectItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kasim
 */
@Controller
public class CustomerTypeBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1217769193097872677L;
    @Autowired
    transient private CommonBean common;

    public SelectItem[] getValues() {
        SelectItem[] items = new SelectItem[CustomerType.values().length];
        int i = 0;
        for (CustomerType each : CustomerType.values()) {
            String label = common.get(each);
            items[i++] = new SelectItem(each, label);
        }
        return items;
    }

    public SelectItem[] getValuesExcludReal() {
        List<CustomerType> types = Lists.newArrayList(
                CustomerType.VIRTUAL_CUSTOMER,
                CustomerType.INNER_VIRTUAL_CUSTOMER);
        SelectItem[] items = new SelectItem[types.size()];
        int i = 0;
        for (CustomerType each : types) {
            String label = common.get(each);
            items[i++] = new SelectItem(each, label);
        }
        return items;
    }
}
