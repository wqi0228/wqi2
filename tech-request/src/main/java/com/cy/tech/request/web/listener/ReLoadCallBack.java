/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.listener;

import java.io.Serializable;

/**
 * 重新更新資料CallBack
 *
 * @author brain0925_liao
 */
public class ReLoadCallBack implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1329442169203595053L;

    public void onReload() {
    }
}
