/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.vo;

import com.cy.tech.request.logic.vo.query.search.SearchQuery;
import com.cy.tech.request.vo.category.BasicDataBigCategory;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.work.common.enums.ReadRecordType;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author brain0925_liao
 */
public class Search10QueryVO extends SearchQuery implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -1811394084673427676L;
    /** 選擇的大類 */
	@Getter
	@Setter
	private BasicDataBigCategory selectBigCategory;
	@Getter
	@Setter
	private Date startDate;
	@Getter
	@Setter
	private Date endDate;
	/** 需求單位 */
	@Getter
	@Setter
	private List<String> requireDepts = Lists.newArrayList();
	/** 選擇的需求製作進度(未結案單據查詢條件) */
	@Getter
	@Setter
	private RequireStatusType selectRequireStatusType;
	/** 選擇的閱讀類型 */
	@Getter
	@Setter
	private ReadRecordType selectReadRecordType;
	/** 單據建立者 */
	@Getter
	@Setter
	private String trCreatedUserName;
	/** 要模糊查詢的字串 */
	@Getter
	@Setter
	private String fuzzyText;
	@Getter
	@Setter
	private List<String> bigDataCateSids = Lists.newArrayList();

	@Getter
	@Setter
	private List<String> middleDataCateSids = Lists.newArrayList();

	@Getter
	@Setter
	private List<String> smallDataCateSids = Lists.newArrayList();

	/** 時間切換的index */
	@Getter
	@Setter
	private int dateTypeIndex;
	@Getter
	@Setter
	private String dateTypeIndexStr;
}
