/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require.othset;

import com.cy.tech.request.logic.service.othset.OthSetHistoryService;
import com.cy.tech.request.logic.service.othset.OthSetReplyService;
import com.cy.tech.request.logic.service.othset.OthSetService;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.othset.OthSet;
import com.cy.tech.request.vo.require.othset.OthSetAlreadyReply;
import com.cy.tech.request.vo.require.othset.OthSetHistory;
import com.cy.tech.request.vo.require.othset.OthSetReply;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.require.IRequireBottom;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.utils.WkEntityUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class OthSetBottomMBean implements IRequireBottom, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3362314594723725561L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private OthSetService osService;
    @Autowired
    transient private OthSetReplyService osrService;
    @Autowired
    transient private OthSetHistoryService oshService;
    @Autowired
    transient private WkEntityUtils entityUtils;
    @Autowired
    transient private WkJsoupUtils jsoupUtils;

    @Getter
    private List<OthSet> othsets;
    public void initResult() {
    	this.othsets = null;
    }
    
    @Getter
    private OthSetReply editReply;
    @Getter
    private OthSetAlreadyReply editAlreadyReply;
    /** 歷程用 */
    @Getter
    private OthSetHistory editHistory;

    private OthSetReply backupReply;
    private OthSetAlreadyReply backupAlreadyReply;

    private Boolean isEditReply;
    private Boolean isEditAlreadyReply;

    @PostConstruct
    public void init() {
        othsets = null;

        editReply = new OthSetReply();
        editAlreadyReply = new OthSetAlreadyReply();
        editHistory = new OthSetHistory();

        backupReply = new OthSetReply();
        backupAlreadyReply = new OthSetAlreadyReply();

        isEditReply = Boolean.FALSE;
        isEditAlreadyReply = Boolean.FALSE;
    }

    @Override
    public void initTabInfo(Require01MBean r01MBean) {
        Require require = r01MBean.getRequire();
        if (require == null || !require.getHasOthSet()) {
            return;
        }
        if (!r01MBean.getBottomTabMBean().currentTabByName(RequireBottomTabType.OTH_SET)) {
            return;
        }
        othsets = osService.initTabInfo(require);
    }

    public List<OthSet> findOthsets(Require01MBean r01MBean) {
        if (othsets == null) {
            this.initTabInfo(r01MBean);
        }
        return othsets;
    }

    public void updateTabInfo(Require require) {
        othsets = osService.initTabInfo(require);
    }

    public List<OthSetHistory> findHistorys(OthSet othset) {
        return oshService.findHistorys(othset);
    }

    public List<OthSetAlreadyReply> findAlreadyReplys(OthSetHistory history) {
        if (history == null || history.getReply() == null) {
            return Lists.newArrayList();
        }
        return osrService.findAlreadyReplys(history.getReply());
    }

    public void initReply(OthSet othset) {
        isEditReply = Boolean.FALSE;
        editReply = osrService.createEmptyReply(othset, loginBean.getUser());
    }

    public void clickSaveReply() {
        try {
            Preconditions.checkArgument(!Strings.isNullOrEmpty(jsoupUtils.clearCssTag(editReply.getContentCss())), "回覆內容不可為空白，請重新輸入！！");
            display.hidePfWidgetVar("othset_reply_dlg_wv");
            if (isEditReply) {
                osrService.saveByEditReply(editReply, loginBean.getUser());
                isEditReply = Boolean.FALSE;
            } else {
                osrService.saveByNewReply(editReply);
            }
        } catch (IllegalArgumentException e) {
            log.debug("檢核失敗", e);
            MessagesUtils.showError(e.getMessage());
            
        } catch (IllegalAccessException e) {
            log.error(e.getMessage(), e);
            this.recoveryView(null, editReply.getOthset());
            MessagesUtils.showError(e.getMessage());
        }
    }

    public void initReplyEdit(OthSetReply reply) {
        this.editReply = reply;
        isEditReply = Boolean.TRUE;
        entityUtils.copyProperties(editReply, backupReply);
    }

    public void cancelEditReply() {
        if (isEditReply) {
            entityUtils.copyProperties(backupReply, editReply);
            isEditReply = Boolean.FALSE;
        }
    }

    public void initAlreadyReply(OthSetReply reply) {
        isEditAlreadyReply = Boolean.FALSE;
        editAlreadyReply = osrService.createEmptyAlreadyReply(reply, loginBean.getUser());
    }

    public void clickSaveAlreadyReply() {
        try {
            Preconditions.checkArgument(!Strings.isNullOrEmpty(jsoupUtils.clearCssTag(editAlreadyReply.getContentCss())), "回覆不可為空白，請重新輸入！！");
            if (isEditAlreadyReply) {
                osrService.saveByEditAlreadyReply(editAlreadyReply, loginBean.getUser());
                isEditAlreadyReply = Boolean.FALSE;
            } else {
                osrService.saveByNewAlreadyReply(editAlreadyReply);
            }
            display.hidePfWidgetVar("othset_already_reply_dlg_wv");
            this.whenSaveAlreadyReplyOpenTab(editAlreadyReply);
        } catch (IllegalArgumentException e) {
            log.debug(e.getMessage(), e);
            MessagesUtils.showError(e.getMessage());
        }
    }

    private void whenSaveAlreadyReplyOpenTab(OthSetAlreadyReply aReply) {
        OthSet othset = aReply.getOthset();
        OthSetReply reply = aReply.getReply();
        int idx = othset.getHistorys().indexOf(reply.getHistory());
        display.execute("accPanelSelectTab('os_acc_panel_layer_one_" + othset.getSid() + "'," + idx + ")");
    }

    public void initAlreadyReplyEdit(OthSetAlreadyReply aReply) {
        this.editAlreadyReply = aReply;
        isEditAlreadyReply = Boolean.TRUE;
        entityUtils.copyProperties(editAlreadyReply, backupAlreadyReply);
    }

    public void cancelEditAlreadyReply() {
        if (isEditAlreadyReply) {
            entityUtils.copyProperties(backupAlreadyReply, editAlreadyReply);
            isEditAlreadyReply = Boolean.FALSE;
            this.whenSaveAlreadyReplyOpenTab(editAlreadyReply);
        }
    }

    public void createEmptyHistory(OthSet othset) {
        editHistory = oshService.createEmptyHistory(othset, loginBean.getUser());
    }

    public void openAttachByHistory(OthSetHistory history, OthSetHistoryAttachMBean sthaMBean) {
        sthaMBean.init(history);
        this.editHistory = history;
    }

    public void clickSaveCancelHistory() {
        try {
            Preconditions.checkArgument(!Strings.isNullOrEmpty(jsoupUtils.clearCssTag(editHistory.getInputInfoCss())), "請輸入取消原因！！");
            oshService.saveCancelHistory(editHistory);
            display.hidePfWidgetVar("othset_cancel_dlg_wv");
        } catch (IllegalArgumentException e) {
            log.debug("檢核失敗", e);
            MessagesUtils.showError(e.getMessage());
        } catch (IllegalAccessException e) {
            log.error(e.getMessage(), e);
            this.recoveryView(null, editHistory.getOthset());
            MessagesUtils.showError(e.getMessage());
        }
    }

    /**
     * 點選：完成
     * @param r01MBean
     */
    public void clickSaveCompleteHistory(Require01MBean r01MBean) {
        try {
            oshService.saveCompleteHistory(r01MBean.getRequire(), editHistory);
            display.hidePfWidgetVar("othset_complete_dlg_wv");
        } catch (IllegalAccessException e) {
            log.error(e.getMessage(), e);
            this.recoveryView(null, editHistory.getOthset());
            MessagesUtils.showError(e.getMessage());
        }
        r01MBean.getTitleBtnMBean().clear();
    }

    public void reBuildOthset(OthSet othset) {
        if (othset == null || Strings.isNullOrEmpty(othset.getOsNo())) {
            return;
        }
        OthSet nOthset = osService.findByOsNo(othset.getOsNo());
        int idx = this.othsets.indexOf(nOthset);
        othsets.set(idx, nOthset);
    }

    public void recoveryView(Require01MBean r01MBean, OthSet othset) {
        if (r01MBean == null) {
            r01MBean = (Require01MBean) Faces.getApplication().getELResolver().getValue(Faces.getELContext(), null, "require01MBean");
        }
        r01MBean.reBuildeRequire();
        //this.reBuildOthset(othset);
        r01MBean.getTraceMBean().clear();
        r01MBean.getTitleBtnMBean().clear();
    }

}
