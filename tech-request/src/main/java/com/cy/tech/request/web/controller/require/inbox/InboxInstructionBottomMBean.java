/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require.inbox;

import com.cy.tech.request.logic.service.AlertInboxService;
import com.cy.tech.request.vo.enums.InboxType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.logic.search.view.InboxView;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.require.IRequireBottom;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import java.io.Serializable;
import java.util.List;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 收指示
 *
 * @author shaun
 */
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class InboxInstructionBottomMBean implements IRequireBottom, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7254047159635672197L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private AlertInboxService inboxService;

    private static final InboxType INBOX_TYPE = InboxType.INCOME_INSTRUCTION;
    private Boolean showTab;
    private List<InboxView> views;

    public void clear() {
        showTab = null;
        views = null;
    }

    public List<InboxView> findInboxViews(Require01MBean r01MBean) {
        if (views == null) {
            this.initTabInfo(r01MBean);
        }
        return views;
    }

    @Override
    public void initTabInfo(Require01MBean r01MBean) {
        Require require = r01MBean.getRequire();
        if (!this.showTab(require)) {
            return;
        }
        if (!r01MBean.getBottomTabMBean().currentTabByName(RequireBottomTabType.INBOX_INSTRUCTION)) {
            return;
        }
        views = inboxService.findReqInboxView(require, loginBean.getUser(), INBOX_TYPE);
    }

    public boolean showTab(Require require) {
        if (showTab == null) {
            showTab = inboxService.hasAnyInbox(require, loginBean.getUser(), INBOX_TYPE);
        }
        return showTab;
    }
}
