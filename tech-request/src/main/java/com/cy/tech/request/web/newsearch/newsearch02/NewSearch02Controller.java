package com.cy.tech.request.web.newsearch.newsearch02;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.URLService.URLServiceAttr;
import com.cy.tech.request.web.newsearch.BaseNewSearchController;
import com.cy.tech.request.web.newsearch.INewSearchController;
import com.cy.tech.request.web.newsearch.NewSearchService;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.viewcomponent.treepker.TreePickerComponent;
import lombok.extern.slf4j.Slf4j;

@Controller
@Scope("view")
@Slf4j
public class NewSearch02Controller extends BaseNewSearchController implements INewSearchController, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8138748707406988528L;

    // ========================================================================
    // 工具區
    // ========================================================================
    @Autowired
    private transient NewSearchService newSearchService;

    // ========================================================================
    // 變數區
    // ========================================================================

    // ========================================================================
    // 初始化
    // ========================================================================
    @PostConstruct
    public void init() {
        // ====================================
        // 初始化查詢條件
        // ====================================
        this.initQueryCondition();

        // ====================================
        // 初始化元件
        // ====================================
        // 初始化類別組合樹
        this.categoryTreePicker = new TreePickerComponent(categoryPickerCallback, "類別組合");

        // ====================================
        // 初始化畫面後 , 觸發查詢
        // ====================================
        this.displayController.execute("doSearchData()");
    }

    public void initQueryCondition() {

        // ====================================
        // 初始化查詢條件 VO
        // ====================================
        this.conditionVO = new NewSearch02ConditionVO();
        // ====================================
        // 取得傳入參數
        // ====================================
        // 負責人 = for 個人未領單
        String ownerSid = Faces.getRequestParameterMap().get(URLServiceAttr.URL_ATTR_SEARCH_OWNERSID.getAttr());
        ownerSid = WkStringUtils.safeTrim(ownerSid);
        if (WkStringUtils.notEmpty(ownerSid) && WkStringUtils.isNumber(ownerSid)) {
            User user = this.wkUserCache.findBySid(Integer.valueOf(ownerSid));
            if (user != null) {
                ((NewSearch02ConditionVO) this.conditionVO).setReqConcirmDepOwnerName(user.getName());
            }
        }
    }

    // ========================================================================
    // 頁面功能
    // ========================================================================
    public void search() {
        try {
            this.queryItems = this.newSearchService.searchForNewSearch02((NewSearch02ConditionVO) conditionVO);
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            log.error("查詢失敗，請洽系統人員! [" + e.getMessage() + "]", e);
            MessagesUtils.showError("查詢失敗，請洽系統人員! [" + e.getMessage() + "]");
            return;
        }
    }

    public void clear() {
        this.initQueryCondition();
        try {
            this.categoryTreePicker.rebuild();
        } catch (Exception e) {
            String message = WkMessage.EXECTION + "[" + e.getMessage() + "]";
            log.error(message, e);
            MessagesUtils.showError(message);
            return;
        }
    }
}
