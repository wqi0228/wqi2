/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.searchheader;

import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.web.controller.enums.ReqStatusMBean;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.google.common.collect.Lists;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

/**
 * 執行狀況查詢搜尋表頭
 *
 * @author jason_h
 */
@Controller
@Scope("view")
public class Search25HeaderMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8231203564586050289L;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private ReqStatusMBean reqStatusUtils;

    /** 期望啟始時間 */
    @Getter
    @Setter
    private Date hopeStartDate;
    /** 期望結束時間 */
    @Getter
    @Setter
    private Date hopeEndDate;

    /** 選擇的需求製作進度 */
    @Getter
    @Setter
    private RequireStatusType selectRequireStatusType;

    @PostConstruct
    public void init() {
        this.clear();
    }

    /**
     * 清除/還原選項
     */
    public void clear() {
        this.initCommHeader();
        this.initHeader();
    }

    /**
     * 初始化commHeader
     */
    private void initCommHeader() {
        commHeaderMBean.clear();
        commHeaderMBean.setEndDate(new Date());
    }

    /**
     * 初始化Header
     */
    private void initHeader() {
        selectRequireStatusType = RequireStatusType.PROCESS;
        hopeStartDate = null;
        hopeEndDate = this.findNearWednesDayDate();
    }

    /**
     * 若今天是星期二,則顯示明天（週三）<BR/>
     * 若今天是星期三,則顯示（下週三）<BR/>
     * 若今天是星期四,則顯示（下週三)
     *
     * @return
     */
    private Date findNearWednesDayDate() {
        Calendar sysCal = Calendar.getInstance();
        int dayWeek;
        do {
            sysCal.add(Calendar.DAY_OF_MONTH, 1);
            dayWeek = sysCal.get(Calendar.DAY_OF_WEEK);
        } while (dayWeek != Calendar.WEDNESDAY);
        return sysCal.getTime();
    }

    /**
     * 建立需求狀態查詢 List
     *
     * @return
     */
    public List<String> createQueryReqStatus() {
        return reqStatusUtils.createQueryReqStatus(this.selectRequireStatusType, this.getAllReqStatus());
    }

    /**
     * 取得此報表全部製作進度查詢
     *
     * @return
     */
    private List<RequireStatusType> getAllReqStatus() {
        return reqStatusUtils.createExcludeStatus(
                Lists.newArrayList(RequireStatusType.DRAFT)
        );
    }

    /**
     * 畫面用全部製作進度查詢
     *
     * @return
     */
    public SelectItem[] getReqStatusItems() {
        return reqStatusUtils.buildItem(this.getAllReqStatus());
    }

    /**
     * 建立需求狀態查詢 List
     *
     * @return
     */
    public List<String> createQueryAllReqStatus() {
        return this.getAllReqStatus().stream()
                .map(each -> each.name())
                .collect(Collectors.toList());
    }
}
