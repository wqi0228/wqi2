/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.anew.components;

import com.cy.tech.request.web.anew.components.vo.DialogOnpgSettingTo;
import java.io.Serializable;
import org.springframework.beans.factory.InitializingBean;

/**
 * DialogOnpgSettingComponent logic
 *
 * @author kasim
 */
public class DialogOnpgSettingLogicComponent implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2582022608039197755L;
    private static DialogOnpgSettingLogicComponent instance;

    public static DialogOnpgSettingLogicComponent getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        DialogOnpgSettingLogicComponent.instance = this;
    }

    /**
     * 建立自訂物件
     *
     * @return
     */
    public DialogOnpgSettingTo createTo() {
        return new DialogOnpgSettingTo();
    }

}
