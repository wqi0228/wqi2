package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.utils.DateUtils;
import com.cy.tech.request.repository.result.BatchAdjustDateVO;
import com.cy.tech.request.vo.constants.ReqPermission;
import com.cy.tech.request.vo.worktest.enums.WorkTestInfoHistoryBehavior;
import com.cy.tech.request.web.controller.search.helper.BatchAdjustDateHelper;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.view.vo.BatchAdjustDateQueryVO;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author aken_kao
 */
@Slf4j
@Controller
@Scope("view")
public class BatchAdjustDateMBean implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 7241806124050148941L;
    @Autowired
	private BatchAdjustDateHelper batchAdjustDateHelper;
	@Autowired
	private LoginBean loginBean;

	@Getter
	private List<BatchAdjustDateVO> resultList = Lists.newArrayList();
	@Getter
	private List<BatchAdjustDateVO> selectedResultList = Lists.newArrayList();
	@Getter
	@Setter
	private BatchAdjustDateQueryVO queryVO = new BatchAdjustDateQueryVO();
	@Getter
	@Setter
	private boolean selectedAll = false;
	@Getter
	@Setter
	private Date adjustDate;
	@Getter
	private List<BatchAdjustDateVO> workTestInfoList = Lists.newArrayList();
	@Getter
	@Setter
	private String requireNo;
	@Getter
	@Setter
	private String requireTheme;
	@Getter
	@Setter
	private WorkTestInfoHistoryBehavior behavior;
	@Getter
	private boolean isQAManager;

	@PostConstruct
	public void init() {
		
		isQAManager = WkUserUtils.isUserHasRole(
		        SecurityFacade.getUserSid(),
		        SecurityFacade.getCompanyId(),
		        ReqPermission.ROLE_QA_MANAGER);

		search();
	}

	/**
	 * 搜尋按鈕
	 */
	public void search() {
		this.resultList = batchAdjustDateHelper.findBatchAdjustDateList(
		        queryVO,
		        loginBean.getUser(),
		        isQAManager);
	}

	/**
	 * 清除按鈕
	 */
	public void clear() {
		queryVO.init();
		selectedResultList.clear();
		selectedAll = false;
		search();
	}

	/**
	 * 批次調整日期
	 */
	public void batchAdjustDate() {
		try {
			String message = validate();
			if (StringUtils.isNotBlank(message)) {
				MessagesUtils.showError(message);
				return;
			}

			batchAdjustDateHelper.batchAdjustDate(selectedResultList, behavior, adjustDate, loginBean.getUser());

			findWorkTestInfoByBehavior();
			MessagesUtils.showInfo("更新完成！");
		} catch (IllegalArgumentException e) {
			MessagesUtils.showError("批次更新失敗，" + e.getMessage());
		} catch (Exception e) {
			MessagesUtils.showError("批次更新失敗，" + e.getMessage());
			log.warn(e.getMessage(), e);
		}

	}

	private String validate() {
		Preconditions.checkArgument(behavior != null, "請選擇更新日期類型！");
		if (WorkTestInfoHistoryBehavior.ADJUST_TEST_DATE.equals(behavior) || WorkTestInfoHistoryBehavior.MODIFY_ESTABLISH_DATE.equals(behavior)) {
			Preconditions.checkArgument(adjustDate != null, "請選擇日期！");
		}

		String message = null;
		for (BatchAdjustDateVO vo : selectedResultList) {
			switch (behavior) {
			case ADJUST_ONLINE_DATE:
				if (adjustDate != null) {
					Preconditions.checkArgument(!adjustDate.before(DateUtils.convertToOriginOfDay(new Date())), "不能小於系統日！");
					if (!(adjustDate.after(vo.getWtEstablishDate())
					        && adjustDate.after(vo.getTestDate())
					        && (vo.getWtScheduleFinishDate() == null || adjustDate.after(vo.getWtScheduleFinishDate())))) {
						message = "調整預計上線日失敗，單號:";
					}
				}
				break;
			case ADJUST_TEST_DATE:
				Preconditions.checkArgument(!adjustDate.before(DateUtils.convertToOriginOfDay(new Date())), "不能小於系統日！");
				if (!((vo.getWtExpectOnlineDate() == null || adjustDate.before(vo.getWtExpectOnlineDate()))
				        && (vo.getWtScheduleFinishDate() == null || adjustDate.before(vo.getWtScheduleFinishDate()))
				        && adjustDate.before(vo.getWtEstablishDate()))) {
					message = "調整送測日失敗，單號:";
				}
				break;
			case MODIFY_ESTABLISH_DATE:
				if (!((vo.getWtExpectOnlineDate() == null || adjustDate.before(vo.getWtExpectOnlineDate()))
				        && (vo.getWtScheduleFinishDate() == null || !adjustDate.before(vo.getWtScheduleFinishDate()))
				        && adjustDate.after(vo.getTestDate()))) {
					message = "調整預計完成日失敗，單號:";
				}
				break;
			case ADJUST_SCHEDULE_FINISH_DATE:
				if (adjustDate != null) {
					if (!((vo.getWtExpectOnlineDate() == null || adjustDate.before(vo.getWtExpectOnlineDate()))
					        && !adjustDate.after(vo.getWtEstablishDate())
					        && adjustDate.after(vo.getTestDate()))) {
						message = "調整排定完成日失敗，單號:";
					}
				}
				break;
			default:
				break;
			}
			if (StringUtils.isNotBlank(message)) {
				return String.format("%s%s<br><br>調整範圍需為 <br>預計上線日>預計完成日>=排定完成日>送測日", message, vo.getTestInfoNo());
			}
		}
		return message;
	}

	/**
	 * 開啟 送測單列表 by 批次調整日期類型
	 * 
	 * @param requireNo
	 */
	public void initWorkTestInfoList(String requireNo, String requireTheme) {
		this.requireNo = requireNo;
		this.requireTheme = requireTheme;
		this.behavior = null;
		this.adjustDate = null;
		this.workTestInfoList.clear();

		if (!isQAManager) {
			behavior = WorkTestInfoHistoryBehavior.ADJUST_ONLINE_DATE;
		}
		findWorkTestInfoByBehavior();
	}

	/**
	 * 初始化checkbox
	 */
	public void initCheckBox() {
		this.selectedResultList.clear();
		this.selectedAll = false;
	}

	/**
	 * 開啟 送測單列表 by 批次調整日期類型
	 * 
	 * @param requireNo
	 */
	public void findWorkTestInfoByBehavior() {
		initCheckBox();
		workTestInfoList = batchAdjustDateHelper.findWorkTestInfoByBehavior(
		        requireNo,
		        behavior,
		        loginBean.getUser(),
		        isQAManager);
	}

	/**
	 * 變更dataTable 全選
	 */
	public void checkBySelectedAll() {
		try {
			selectedResultList.clear();
			for (BatchAdjustDateVO vo : workTestInfoList) {
				vo.setChecked(selectedAll);
				if (selectedAll) {
					selectedResultList.add(vo);
				}
			}
		} catch (Exception e) {
			log.warn(e.getMessage(), e);
		}
	}

	/**
	 * 點擊check box
	 * 
	 * @param vo
	 */
	public void checkBySingle(BatchAdjustDateVO vo) {
		try {
			if (vo.isChecked()) {
				selectedResultList.add(vo);
				selectedAll = workTestInfoList.stream().allMatch(each -> each.isChecked());
			} else {
				selectedResultList.remove(vo);
				selectedAll = false;
			}
		} catch (Exception e) {
			log.warn(e.getMessage(), e);
		}
	}

}
