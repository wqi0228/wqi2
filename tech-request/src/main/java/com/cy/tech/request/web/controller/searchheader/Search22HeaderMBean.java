/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.searchheader;

import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.web.controller.enums.ReqStatusMBean;
import com.cy.work.trace.vo.enums.TraceStatus;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author kasim
 */
@Controller
@Scope("view")
@Data
public class Search22HeaderMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4033369986789892031L;
    @Autowired
    transient private CommonHeaderMBean commHeaderMBean;
    @Autowired
    transient private ReqStatusMBean reqStatusUtils;

    /** 選擇的需求製作進度 */
    private RequireStatusType selectRequireStatusType;
    /** 追蹤狀態 */
    private TraceStatus traceStatus;
    /** 主題 */
    private String theme;
    /** 內容 */
    private String content;
    /** 追蹤區間(起) */
    private Date startCreatedDate;
    /** 追蹤區間(訖) */
    private Date endCreatedDate;
    /** 追蹤提醒日(起) */
    private Date startNoticeDate;
    /** 追蹤提醒日(訖) */
    private Date endNoticeDate;

    @PostConstruct
    private void init() {
        this.clear();
    }

    /**
     * 清除/還原選項
     */
    public void clear() {
        this.initCommHeader();
        selectRequireStatusType = null;
        traceStatus = TraceStatus.UN_FINISHED;
        theme = null;
        content = null;
        startCreatedDate = null;
        endCreatedDate = null;
        startNoticeDate = null;
        endNoticeDate = null;
    }

    /**
     * 初始化commHeader
     */
    private void initCommHeader() {
        commHeaderMBean.clear();
        commHeaderMBean.clearAdvance();
        commHeaderMBean.initDefaultRequireDepts(false);
    }

    /**
     * 建立需求狀態查詢 List
     *
     * @return
     */
    public List<String> createQueryReqStatus() {
        return reqStatusUtils.createQueryReqStatus(this.selectRequireStatusType, this.getAllReqStatus());
    }

    /**
     * 取得此報表全部製作進度查詢
     *
     * @return
     */
    private List<RequireStatusType> getAllReqStatus() {
        return reqStatusUtils.createExcludeStatus(
                Lists.newArrayList(RequireStatusType.DRAFT)
        );
    }

    /**
     * 畫面用全部製作進度查詢
     *
     * @return
     */
    public SelectItem[] getReqStatusItems() {
        return reqStatusUtils.buildItem(this.getAllReqStatus());
    }

    /**
     * 建立需求狀態查詢 List
     *
     * @return
     */
    public List<String> createQueryAllReqStatus() {
        return this.getAllReqStatus().stream()
                .map(each -> each.name())
                .collect(Collectors.toList());
    }
}
