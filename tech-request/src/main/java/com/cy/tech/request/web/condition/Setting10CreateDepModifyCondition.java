/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.condition;

import java.io.Serializable;

import com.cy.tech.request.vo.require.TrRequire;

import lombok.Getter;
import lombok.Setter;

/**
 * 需求單轉單程式 - 建立部門轉單結果物件
 * @author brain0925_liao
 */
public class Setting10CreateDepModifyCondition implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5273294143765019037L;
    @Getter
    @Setter
    private boolean result;
    @Getter
    @Setter
    private String text;
    @Getter
    @Setter
    private TrRequire trRequire;

}
