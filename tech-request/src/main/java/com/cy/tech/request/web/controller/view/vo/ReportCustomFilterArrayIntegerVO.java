/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.view.vo;

import com.cy.tech.request.vo.enums.SearchReportCustomEnum;
import com.cy.tech.request.vo.enums.SearchReportCustomType;
import com.cy.work.common.utils.WkJsonUtils;
import java.util.List;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
public class ReportCustomFilterArrayIntegerVO implements ReportCustomFilterDetailVO {

    @Getter
    private SearchReportCustomEnum searchReportCustomEnum;
    @Getter
    private List<Integer> arrayIntegers;

    public ReportCustomFilterArrayIntegerVO(SearchReportCustomEnum searchReportCustomEnum, List<Integer> arrayIntegers) {
        this.searchReportCustomEnum = searchReportCustomEnum;
        this.arrayIntegers = arrayIntegers;
    }

    public ReportCustomFilterArrayIntegerVO(SearchReportCustomEnum searchReportCustomEnum, String value) {
        this.searchReportCustomEnum = searchReportCustomEnum;
        try {
            arrayIntegers = WkJsonUtils.getInstance().fromJsonToList(value, Integer.class);
        } catch (Exception e) {
            log.error("WkJsonUtils.getInstance().fromJsonToList(value, Integer.class); ERROR", e);
        }
    }

    public String getJsonValue() {
        try {
            return WkJsonUtils.getInstance().toJson(arrayIntegers);
        } catch (Exception e) {
            log.error("WkJsonUtils.getInstance().toJson(arrayIntegers); ERROR", e);
        }
        return "";
    }

    public SearchReportCustomType getSearchReportCustomType() {
        return SearchReportCustomType.ARRAYINTEGER;
    }
}
