package com.cy.tech.request.web.controller.view.component;

import java.util.Date;
import org.joda.time.LocalDate;
import lombok.Getter;
import lombok.Setter;

/**
 * @author aken_kao
 */
public class DateIntervalVO {

    @Getter
    @Setter
    /** 起始時間 */
    private Date startDate;
    @Getter
    @Setter
    /** 結束時間 */
    private Date endDate;
    @Getter
    @Setter
    /** 時間切換的index */
    private int dateTypeIndex = -1;
    
    public void init(int dateTypeIndex){
        this.dateTypeIndex = dateTypeIndex;
        switch(dateTypeIndex){
        case 0:
            changeDateIntervalPreMonth();
            break;
        case 1:
            changeDateIntervalThisMonth();
            break;
        case 2:
            changeDateIntervalNextMonth();
            break;
        case 3:
            changeDateIntervalToDay();
            break;
        default:
            startDate = null;
            endDate = new Date();
            break;
                
        }
    }
    
    public void clear() {
        dateTypeIndex = -1;
        startDate = null;
        endDate = null;
    }
    
    /**
     * 上個月
     */
    public void changeDateIntervalPreMonth() {
        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate lastDate = new LocalDate(date).minusMonths(1);
        this.dateTypeIndex = 0;
        this.startDate = lastDate.dayOfMonth().withMinimumValue().toDate();
        this.endDate = lastDate.dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 本月份
     */
    public void changeDateIntervalThisMonth() {
        this.dateTypeIndex = 1;
        this.startDate = new LocalDate().dayOfMonth().withMinimumValue().toDate();
        this.endDate = new LocalDate().dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 下個月
     */
    public void changeDateIntervalNextMonth() {
        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate nextDate = new LocalDate(date).plusMonths(1);
        this.dateTypeIndex = 2;
        this.startDate = nextDate.dayOfMonth().withMinimumValue().toDate();
        this.endDate = nextDate.dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 今日
     */
    public void changeDateIntervalToDay() {
        this.dateTypeIndex = 3;
        this.startDate = new Date();
        this.endDate = new Date();
    }
    
}
