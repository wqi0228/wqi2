package com.cy.tech.request.web.controller.search;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.omnifaces.util.Faces;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.view.OverdueUnfinishedVO;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.web.controller.require.ReqLoadBean;
import com.cy.tech.request.web.controller.search.helper.OverdueUnfinishedHelper;
import com.cy.tech.request.web.controller.search.helper.SearchHelper;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.controller.view.component.searchquery.OverdueUnfinishedCustomQueryVO;
import com.cy.tech.request.web.controller.view.vo.OverdueUnfinishedQueryVO;
import com.cy.tech.request.web.enums.SwitchType;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.exception.UserMessageException;
import com.google.common.base.Strings;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author aken_kao
 *             逾期未完工需求報表
 */
@Scope("view")
@Controller
@Slf4j
public class OverdueUnfinishedMBean extends PaginationMBean<OverdueUnfinishedVO> implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 7478099810855370449L;
    @Autowired
    private LoginBean loginBean;
    @Autowired
    private TableUpDownBean upDownBean;
    @Autowired
    private DisplayController display;
    @Autowired
    private OverdueUnfinishedHelper overdueUnfinishedHelper;
    @Autowired
    private RequireService requireService;
    @Autowired
    private ReqLoadBean loadManager;
    @Autowired
    private SearchHelper searchHelper;
    @Getter
    @Setter
    /** 顯示dataTable裡的元件 */
    private boolean displayButton = true;
    @Getter
    @Setter
    /** 顯示自訂義按鈕裡的元件 */
    private boolean displayCustomBtn = true;
    @Getter
    private boolean portalMode = false;
    /** 切換模式 */
    @Getter
    private SwitchType switchType = SwitchType.CONTENT;
    @Getter
    @Setter
    private OverdueUnfinishedQueryVO queryVO;

    @Getter
    @Setter
    private OverdueUnfinishedCustomQueryVO customQueryVO = new OverdueUnfinishedCustomQueryVO();
    @Getter
    private final String dataTableId = "dtRequire";

    @PostConstruct
    public void init() {
        queryVO = new OverdueUnfinishedQueryVO(loginBean.getDep(), loginBean.getUser(), ReportType.OVERDUE_UNFINISHED);
        portalMode = Faces.getRequestParameter("mode") != null;
        initQueryVO();
    }

    public void initQueryVO() {
        queryVO.init();
        if (portalMode) {
            displayCustomBtn = false;
            queryVO.getOrgTreeVO().initDefaultDepts(loginBean.getDep());
        } else {
            customQueryVO.initCustomQueryVO();
            loadDefaultSetting();
        }
    }

    /** 載入自訂搜尋條件設定 */
    public void loadDefaultSetting() {
        queryVO.setBigCategorySids(customQueryVO.getBigCategorySids());
        queryVO.getOrgTreeVO().setRequireDepts(customQueryVO.getOrgTreeVO().getRequireDepts());
        queryVO.setReadRecordType(customQueryVO.getReadRecordType());
        queryVO.getCategoryCombineVO().getCategoryTreeComponent().selectedItem(customQueryVO.getCategoryCombineVO().getSmallDataCateSids());
        queryVO.getCategoryCombineVO().setSmallDataCateSids(customQueryVO.getCategoryCombineVO().getSmallDataCateSids());
        queryVO.getDateIntervalVO().init(Strings.isNullOrEmpty(customQueryVO.getDateTypeIndex()) ? -1
                : Integer.valueOf(customQueryVO.getDateTypeIndex()));
        queryVO.setSearchText(customQueryVO.getSearchText());
    }

    public void saveReportCustomFilter() {
        customQueryVO.saveReportCustomFilter();
        loadDefaultSetting();
        search();
    }

    public void search() {
        try {
            queryItems = overdueUnfinishedHelper.queryOverdueUnfinishedByCondition(queryVO);
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        }
    }

    /**
     * 清除按鈕
     */
    public void clearBtn() {
        try {
            initQueryVO();
            search();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 匯出時，隱藏dataTable按鈕元件
     */
    public void disableButton() {
        displayButton = false;
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        Date startDate = queryVO.getDateIntervalVO().getStartDate();
        Date endDate = queryVO.getDateIntervalVO().getEndDate();
        ReportType reportType = queryVO.getReportType();
        searchHelper.exportExcel(document, startDate, endDate, reportType);
        displayButton = true;
    }

    /**
     * 點選dataTable 主題 link
     */
    public void showDetail(OverdueUnfinishedVO selectedResult) {
        querySelection = selectedResult;
        queryKeeper = selectedResult;
        switchType = SwitchType.FULLCONTENT;
        this.toggleSearchBody();

    }

    /**
     * 切換查詢表身
     */
    public void toggleSearchBody() {
        if (!SwitchType.FULLCONTENT.equals(switchType)) {
            int status = switchType.ordinal();
            int xor = status ^= 1;
            switchType = SwitchType.values()[xor];
        }
        loadRequireMaster();
    }

    /**
     * 讀取需求單主檔
     */
    public void loadRequireMaster() {
        if (querySelection != null) {
            queryKeeper = querySelection;
        } else if (this.queryKeeper == null) {
            this.querySelection = this.queryKeeper = this.queryItems.get(0);
        }
        this.changeRequireContent(queryKeeper);
    }

    /**
     * 需求單明細，切換到data list require03_title_btn.xhtml, 共同元件
     */
    public void normalScreenReport() {
        querySelection = queryKeeper;
        switchType = SwitchType.CONTENT;
    }

    @Override
    protected void resetUpdownInfo() {
        upDownBean.setCurrRow(queryKeeper.getRequireNo());
        upDownBean.resetUpDown(queryItems.indexOf(queryKeeper), queryItems.size());
        display.update("searchBody");

    }

    @Override
    protected void highlightReportTo(String widgetVar, String pageCount, OverdueUnfinishedVO selectedResult) {
        selectedResult.setReadRecordType(ReadRecordType.HAS_READ);
        querySelection = selectedResult;
        queryKeeper = querySelection;
        display.execute("selectRow('" + widgetVar + "'," + super.getRowIndex(pageCount) + ");");

    }

    @Override
    protected void changeRequireContent(OverdueUnfinishedVO vo) {
        Require r = requireService.findByReqNo(vo.getRequireNo());
        loadManager.reloadReqForm(r, loginBean.getUser());

    }

    /**
     * refresh dataTable
     */
    public void refreshDataTable() {
        search();
    }

    /**
     * 半版row選擇
     *
     * @param event
     */
    public void onRowSelect(SelectEvent event) {
        this.queryKeeper = this.querySelection = (OverdueUnfinishedVO) event.getObject();
        this.changeRequireContent(this.queryKeeper);
    }

}
