/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require.send.test;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.FogbugzService;
import com.cy.tech.request.logic.service.FormViewUpdateService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.send.test.QaAliasService;
import com.cy.tech.request.logic.service.send.test.SendTestHistoryService;
import com.cy.tech.request.logic.service.send.test.SendTestQAReportService;
import com.cy.tech.request.logic.service.send.test.SendTestReplyService;
import com.cy.tech.request.logic.service.send.test.SendTestService;
import com.cy.tech.request.logic.service.send.test.SendTestShowService;
import com.cy.tech.request.vo.constants.ReqPermission;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.value.to.WorkTestQaAliasVO;
import com.cy.tech.request.vo.worktest.InplaceControl;
import com.cy.tech.request.vo.worktest.WorkTestAlreadyReply;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.vo.worktest.WorkTestInfoHistory;
import com.cy.tech.request.vo.worktest.WorkTestQAReport;
import com.cy.tech.request.vo.worktest.WorkTestReply;
import com.cy.tech.request.vo.worktest.enums.WorkTestInfoStatus;
import com.cy.tech.request.web.common.PermissionHelper;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.logic.component.UsersMultiSelectComponent;
import com.cy.tech.request.web.controller.require.IRequireBottom;
import com.cy.tech.request.web.controller.require.Require01MBean;
import com.cy.tech.request.web.controller.search.WorkTestUpDownBean;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.utils.WkEntityUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.omnifaces.util.Faces;
import org.primefaces.event.TabChangeEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class SendTestBottomMBean implements IRequireBottom, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6463874699121901329L;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private FormViewUpdateService formViewUpdateService;
    @Autowired
    transient private SendTestService stService;
    @Autowired
    transient private SendTestShowService stsService;
    @Autowired
    transient private SendTestReplyService strService;
    @Autowired
    transient private SendTestHistoryService sthService;
    @Autowired
    transient private SendTestQAReportService stqrService;
    @Autowired
    transient private WkEntityUtils entityUtils;
    @Autowired
    transient private WkJsoupUtils jsoupUtils;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private WorkTestUpDownBean workTestUpDownBean;
    @Autowired
    transient private URLService urlService;
    @Autowired
    private FogbugzService fbService;

    @Getter
    private List<WorkTestInfo> testinfos = Lists.newArrayList();
    @Getter
    private WorkTestReply editReply;
    @Getter
    private WorkTestAlreadyReply editAlreadyReply;
    /** 歷程用 */
    @Getter
    private WorkTestInfoHistory editInfoHistory;
    @Getter
    private WorkTestQAReport editQAReport;

    private WorkTestReply backupReply;
    private WorkTestAlreadyReply backupAlreadyReply;
    private WorkTestQAReport backupQAReport;

    @Getter
    private Boolean isEditQAReport;
    @Getter
    private Boolean isEditReply;
    @Getter
    private Boolean isEditAlreadyReply;
    @Getter
    @Setter
    private WorkTestInfo testInfo = new WorkTestInfo();
    @Getter
    private UsersMultiSelectComponent<WorkTestQaAliasVO> usersMultiSelectCompt = new UsersMultiSelectComponent<WorkTestQaAliasVO>();
    @Getter
    @Setter
    private boolean loadWorkTestInfoByUrlParam = false; // 是否為 單張送測單
    @Autowired
    private QaAliasService qaAliasService;
    @Autowired
    private transient WorkBackendParamHelper workBackendParamHelper;

    /** 是否修改人員為主測 */
    private boolean isMaster;

    @PostConstruct
    public void init() {
        testinfos = null;

        editReply = new WorkTestReply();
        editAlreadyReply = new WorkTestAlreadyReply();
        editInfoHistory = new WorkTestInfoHistory();
        editQAReport = new WorkTestQAReport();

        backupReply = new WorkTestReply();
        backupAlreadyReply = new WorkTestAlreadyReply();
        backupQAReport = new WorkTestQAReport();

        isEditReply = Boolean.FALSE;
        isEditAlreadyReply = Boolean.FALSE;
        isEditQAReport = Boolean.FALSE;
    }

    @Override
    public void initTabInfo(Require01MBean r01MBean) {
        Require require = r01MBean.getRequire();
        if (require == null || !require.getHasTestInfo()) {
            return;
        }
        if (!r01MBean.getBottomTabMBean().currentTabByName(RequireBottomTabType.SEND_TEST_INFO)) {
            return;
        }
        testinfos = stService.initTabInfo(require);
    }

    public List<WorkTestInfo> findTestInfos(Require01MBean r01MBean) {
        if (testinfos == null) {
            this.initTabInfo(r01MBean);
        }
        return testinfos;
    }

    /**
     * 更新送測單List (update accordionPanel 如果是單張送測單則不需update
     * 
     * @param require
     */
    public void updateTabInfo(Require require) {
        if (!loadWorkTestInfoByUrlParam) {
            testinfos = stService.initTabInfo(require);
        }
    }

    public WorkTestInfo findRefreByIdx() {
        if (!Faces.getRequestParameterMap().containsKey("testinfoIdx")) {
            return null;
        }
        int readInboxIdx = Integer.valueOf(Faces.getRequestParameterMap().get("testinfoIdx"));
        return this.testinfos.get(readInboxIdx);
    }

    public List<WorkTestInfoHistory> findHistorys(WorkTestInfo testInfo) {
        return sthService.findInfoHistorysByLazy(testInfo);
    }

    public List<WorkTestAlreadyReply> findAlreadyReplys(WorkTestInfoHistory history) {
        if (history == null || history.getReply() == null) {
            return Lists.newArrayList();
        }
        return strService.findAlreadyReplys(history.getReply());
    }

    public void initReply(WorkTestInfo testinfo) {
        isEditReply = Boolean.FALSE;
        editReply = strService.createEmptyReply(testinfo, loginBean.getUser());
    }

    /**
     * 送測單回覆
     */
    public void clickSaveReply() {
        try {
            Preconditions.checkArgument(!Strings.isNullOrEmpty(jsoupUtils.clearCssTag(editReply.getContentCss())), "回覆不可為空白，請重新輸入！！");
            display.hidePfWidgetVar("send_test_reply_dlg_wv");
            if (isEditReply) {
                testInfo = strService.saveByEditReply(editReply, loginBean.getUser());
                isEditReply = Boolean.FALSE;
            } else {
                testInfo = strService.saveByNewReply(editReply);
            }
            // update tab
            updateTabInfo(requireService.findByReqSid(testInfo.getSourceSid()));
        } catch (IllegalArgumentException e) {
            log.debug("檢核失敗", e);
            MessagesUtils.showError(e.getMessage());
        } catch (IllegalAccessException e) {
            log.error(e.getMessage(), e);
            this.recoveryView(null, editReply.getTestInfo());
            MessagesUtils.showError(e.getMessage());
        }
    }

    public void initEditReply(WorkTestReply reply) {
        this.editReply = reply;
        isEditReply = Boolean.TRUE;
        entityUtils.copyProperties(editReply, backupReply);
    }

    public void cancelEditReply() {
        if (isEditReply) {
            entityUtils.copyProperties(backupReply, editReply);
            isEditReply = Boolean.FALSE;
        }
    }

    public void initAlreadyReply(WorkTestReply reply) {
        isEditAlreadyReply = Boolean.FALSE;
        editAlreadyReply = strService.createEmptyAlreadyReply(reply, loginBean.getUser());
    }

    public void clickSaveAlreadyReply() {
        try {
            Preconditions.checkArgument(!Strings.isNullOrEmpty(jsoupUtils.clearCssTag(editAlreadyReply.getContentCss())), "回覆不可為空白，請重新輸入！！");
            if (isEditAlreadyReply) {
                strService.saveByEditAlreadyReply(editAlreadyReply, loginBean.getUser());
                isEditAlreadyReply = Boolean.FALSE;
            } else {
                strService.saveByNewAlreadyReply(editAlreadyReply);
            }
            display.hidePfWidgetVar("send_test_reply_already_dlg_wv");
            this.whenSaveAReplyOpenTab(editAlreadyReply);
        } catch (IllegalArgumentException e) {
            log.debug(e.getMessage(), e);
            MessagesUtils.showError(e.getMessage());
        }
    }

    private void whenSaveAReplyOpenTab(WorkTestAlreadyReply aReply) {
        WorkTestInfo testInfo = stService.findByTestinfoNo(aReply.getTestInfo().getTestinfoNo());
        WorkTestReply reply = aReply.getReply();
        int idx = testInfo.getInfoHistorys().indexOf(reply.getHistory());
        display.execute("accPanelSelectTab('st_acc_panel_layer_one_" + testInfo.getSid() + "'," + idx + ")");
    }

    public void initAlreadyReplyEdit(WorkTestAlreadyReply aReply) {
        this.editAlreadyReply = aReply;
        isEditAlreadyReply = Boolean.TRUE;
        entityUtils.copyProperties(editAlreadyReply, backupAlreadyReply);
    }

    public void cancelEditAlreadyReply() {
        if (isEditAlreadyReply) {
            entityUtils.copyProperties(backupAlreadyReply, editAlreadyReply);
            isEditAlreadyReply = Boolean.FALSE;
            this.whenSaveAReplyOpenTab(editAlreadyReply);
        }
    }

    public void createEmptyHistory(WorkTestInfo testInfo) {
        editInfoHistory = sthService.createEmptyHistory(testInfo, loginBean.getUser());
    }

    public void createEmptyHistoryByReason(String testInfoNo, String reason) {
        createEmptyHistory(stService.findByTestinfoNo(testInfoNo));
        editInfoHistory.setReasonCss(reason);
        editInfoHistory.setUpdatedDate(null);
        editInfoHistory.setUpdatedUser(null);

    }

    public void openAttachByHistory(WorkTestInfoHistory history, SendTestHistoryAttachMBean sthaMBean) {
        sthaMBean.init(history);
        this.editInfoHistory = history;
    }

    /**
     * 退測
     */
    public void clickSaveRollbackHistory() {
        // ====================================
        // 檢核
        // ====================================
        try {
            Preconditions.checkArgument(
                    !Strings.isNullOrEmpty(
                            jsoupUtils.clearCssTag(
                                    editInfoHistory.getReasonCss())),
                    "請輸入退測原因！！");
        } catch (IllegalArgumentException e) {
            MessagesUtils.showError(e.getMessage());
            return;
        }

        try {
            sthService.saveRollbackHistory(
                    editInfoHistory,
                    loginBean.getUser());

            display.hidePfWidgetVar("send_test_rollback_dlg_wv");
        } catch (IllegalAccessException e) {
            this.recoveryView(null, editInfoHistory.getTestInfo());
            MessagesUtils.showError(e.getMessage());
        } finally {
            updateTabInfo(requireService.findByReqSid(editInfoHistory.getSourceSid()));
            this.reloadWorkTestInfo(editInfoHistory.getTestinfoNo());
        }
    }

    /**
     * 開啟測試報告Dialog
     * 
     * @param testInfo
     */
    public void initQAReport(WorkTestInfo testInfo) {
        editQAReport = stqrService.createEmptyQAReport(testInfo, loginBean.getUser());
    }

    /**
     * 測試報告
     * 
     * @param r01MBean
     */
    public void clickSaveQAReport(Require01MBean r01MBean) {
        try {
            stqrService.saveByNewQAReport(editQAReport, loginBean.getUser());
            display.hidePfWidgetVar("send_test_qa_report_dlg_wv");
            r01MBean.getTitleBtnMBean().clear();
        } catch (IllegalAccessException e) {
            log.error(e.getMessage(), e);
            this.recoveryView(null, editQAReport.getTestInfo());
            MessagesUtils.showError(e.getMessage());
        } finally {
            // update tab
            updateTabInfo(requireService.findByReqSid(editQAReport.getSourceSid()));
            this.reloadWorkTestInfo(editQAReport.getTestinfoNo());
        }
    }

    public void initQAReportView(WorkTestQAReport qaReport) {
        editQAReport = qaReport;
    }

    public void initQAReportEdit() {
        isEditQAReport = Boolean.TRUE;
        entityUtils.copyProperties(editQAReport, backupQAReport);
    }

    public void clickSaveQAReportEdit() {
        try {
            stqrService.saveByEditQAReport(editQAReport, loginBean.getUser());
            isEditQAReport = Boolean.FALSE;
            display.hidePfWidgetVar("send_test_qa_report_view_dlg_wv");
        } catch (IllegalAccessException e) {
            log.error(e.getMessage(), e);
            this.recoveryView(null, editQAReport.getTestInfo());
            MessagesUtils.showError(e.getMessage());
        }
    }

    public void cancelQAReportEdit() {
        isEditQAReport = Boolean.FALSE;
        if (!Strings.isNullOrEmpty(editQAReport.getSid())) {
            entityUtils.copyProperties(backupQAReport, editQAReport);
        }
    }

    /**
     * 取消測試
     */
    public void clickSaveCancelTestHistory() {

        if (WkStringUtils.isEmpty(jsoupUtils.clearCssTag(editInfoHistory.getReasonCss()))) {
            MessagesUtils.showWarn("請輸入取消原因！！");
            return;
        }

        try {
            sthService.saveCancelTestHistory(
                    editInfoHistory,
                    loginBean.getUser());
            display.hidePfWidgetVar("send_test_cancel_test_dlg_wv");
        } catch (IllegalAccessException e) {
            log.error(e.getMessage(), e);
            this.recoveryView(null, editQAReport.getTestInfo());
            MessagesUtils.showError(e.getMessage());
        } finally {
            // update tab
            updateTabInfo(requireService.findByReqSid(editInfoHistory.getSourceSid()));
            this.reloadWorkTestInfo(editInfoHistory.getTestinfoNo());
        }
    }

    /**
     * 測試完成
     */
    public void clickSaveTestCompleteHistory(Require01MBean r01MBean) {
        try {
            sthService.saveTestCompleteHistory(editInfoHistory, loginBean.getUser());
            display.hidePfWidgetVar("send_test_test_complete_dlg_wv");
            r01MBean.getTitleBtnMBean().clear();
        } catch (IllegalAccessException e) {
            this.recoveryView(null, editQAReport.getTestInfo());
            MessagesUtils.showError(e.getMessage());
        } finally {
            // update tab
            updateTabInfo(requireService.findByReqSid(editInfoHistory.getSourceSid()));
            reloadWorkTestInfo(editInfoHistory.getTestinfoNo());
        }
    }

    public String createSearch15ViewUrlLink(WorkTestInfo testInfo) {
        return stService.createSearch15ViewUrlLink(testInfo);
    }

    public void updateReadRecord(TabChangeEvent event) {
        try {
            WorkTestInfo testinfo = (WorkTestInfo) event.getData();
            formViewUpdateService.updateSendTestRecord(testinfo, loginBean.getUser());
        } catch (Exception e) {
            log.error("更新送測閱讀記錄失敗。" + e.getMessage(), e);
        }
    }

    public void reBuildTestinfo(WorkTestInfo testinfo) {
        if (testinfo == null || Strings.isNullOrEmpty(testinfo.getTestinfoNo())) {
            return;
        }
        this.reloadWorkTestInfo(testinfo.getTestinfoNo());
        if (CollectionUtils.isNotEmpty(testinfos)) {
            WorkTestInfo nTestinfo = stService.findByTestinfoNo(testinfo.getTestinfoNo());
            int idx = this.testinfos.indexOf(nTestinfo);
            testinfos.set(idx, nTestinfo);
        }
    }

    public void recoveryView(Require01MBean r01MBean, WorkTestInfo testInfo) {
        if (r01MBean == null) {
            r01MBean = (Require01MBean) Faces.getApplication().getELResolver().getValue(Faces.getELContext(), null, "require01MBean");
        }
        r01MBean.reBuildeRequire();
        this.reBuildTestinfo(testInfo);
        r01MBean.getTraceMBean().clear();
        r01MBean.getTitleBtnMBean().clear();
    }

    /**
     * 打開日期, QA連結 按鈕
     */
    public void prepareUpdateDate(WorkTestInfo testInfo) {
        this.testInfo = stService.findByTestinfoNo(testInfo.getTestinfoNo());
        this.testInfo.getInplaceControl().cancel();
    }

    /**
     * 打開主測協測按鈕
     */
    public void prepareUpdateTester(WorkTestInfo testInfo, boolean isMaster) {
        this.prepareUpdateDate(testInfo);
        this.isMaster = isMaster;
        this.usersMultiSelectCompt = new UsersMultiSelectComponent<WorkTestQaAliasVO>();
        display.resetDataTable("testerSource");

        // 初始 未選取清單
        usersMultiSelectCompt.setSourceUsers(
                qaAliasService.convertToQaAliasList(
                        this.workBackendParamHelper.getQAUsers()));

        String[] selectedMasterTarget = null;
        String[] selectedSlaveTarget = null;
        if (StringUtils.isNotBlank(this.testInfo.getMasterTesters())) {
            selectedMasterTarget = this.testInfo.getMasterTesters().split(",");
        }
        if (StringUtils.isNotBlank(this.testInfo.getSlaveTesters())) {
            selectedSlaveTarget = this.testInfo.getSlaveTesters().split(",");
        }

        List<WorkTestQaAliasVO> selected = null;
        String[] selectedTarget = null;
        if (isMaster) {
            // 已選取的協測使用者
            selected = qaAliasService.convertToQaAliasList(selectedSlaveTarget);
            selectedTarget = selectedMasterTarget;
        } else {
            // 已選取的主測使用者
            selected = qaAliasService.convertToQaAliasList(selectedMasterTarget);
            selectedTarget = selectedSlaveTarget;
        }
        if (WkStringUtils.notEmpty(selected)) {
            usersMultiSelectCompt.getSourceUsers().removeAll(selected);
        }

        if (selectedTarget != null && selectedTarget.length != 0) {
            List<WorkTestQaAliasVO> qaAliasList = qaAliasService.convertToQaAliasList(selectedTarget);
            usersMultiSelectCompt.setTargetUsers(qaAliasList);
            usersMultiSelectCompt.getSourceUsers().removeAll(qaAliasList);
        }
    }

    /**
     * 納入排程
     * 
     * @param testInfo
     */
    public void joinSchedule(String testInfoNo, boolean updateTab) {
        try {
            WorkTestInfo entity = stService.findByTestinfoNo(testInfoNo);
            boolean isQAManager = SecurityFacade.hasPermission(ReqPermission.ROLE_QA_REVIEW_MGR);
            if (stsService.renderedJoinScheduleBtn(entity, isQAManager)) {
                stService.joinSchedule(entity, loginBean.getUser());
            } else {
                MessagesUtils.showInfo("無操作權限，送測單已被更新！");
            }
            // update tab
            if (updateTab) {
                updateTabInfo(requireService.findByReqSid(entity.getSourceSid()));
            }
            this.reloadWorkTestInfo(entity.getTestinfoNo());
        } catch (Exception e) {
            MessagesUtils.showError(e.getMessage());
        }

    }

    /**
     * 不納入排程
     * 
     * @param testInfo
     */
    public void unjoinSchedule(boolean updateTab) {
        try {
            WorkTestInfo entity = stService.findByTestinfoNo(editInfoHistory.getTestinfoNo());
            boolean isQAManager = SecurityFacade.hasPermission(ReqPermission.ROLE_QA_REVIEW_MGR);
            if (stsService.renderedUnjoinScheduleBtn(entity, isQAManager)) {
                String reason = jsoupUtils.clearCssTag(editInfoHistory.getReasonCss());
                Preconditions.checkArgument(StringUtils.isNotBlank(reason), "請輸入原因！");
                stService.unjoinSchedule(editInfoHistory, loginBean.getUser());
            } else {
                MessagesUtils.showInfo("無操作權限，送測單已被更新！");
            }
            // update tab
            if (updateTab) {
                updateTabInfo(requireService.findByReqSid(editInfoHistory.getSourceSid()));
                display.hidePfWidgetVar("unjoinScheduleReason_wv");
            }
            this.reloadWorkTestInfo(entity.getTestinfoNo());
        } catch (Exception e) {
            MessagesUtils.showError(e.getMessage());
        }

    }

    /**
     * 文件補齊
     * 
     * @param testInfo
     */
    public void commitPaper(WorkTestInfo testInfo) {
        try {
            WorkTestInfo entity = stService.findOne(testInfo.getSid());
            Preconditions.checkArgument(!WorkTestInfoStatus.COMMITED.equals(entity.getCommitStatus()), "文件已補齊！");
            stService.commitPaper(testInfo, loginBean.getUser());

        } catch (IllegalAccessException e) {
            MessagesUtils.showError(e.getMessage());
        } catch (IllegalArgumentException e) {
            MessagesUtils.showError(e.getMessage());
        } finally {
            // update tab
            updateTabInfo(requireService.findByReqSid(testInfo.getSourceSid()));
            this.reloadWorkTestInfo(testInfo.getTestinfoNo());
        }
    }

    public void openWorkTestInfoHistory(WorkTestInfoHistory history) {
        editInfoHistory = sthService.findOne(history.getSid());
    }

    /**
     * 送測單, 修改資料 (inplace)
     * 
     * @param type
     */
    public void prepareEditData(String testNo, String type) {
        WorkTestInfo testInfo = stService.findByTestinfoNo(testNo);

        InplaceControl inplaceControl = testInfo.getInplaceControl();
        inplaceControl.cancel();
        switch (type) {
        case "theme":
            inplaceControl.setThemeEditable(true);
            break;
        case "content":
            inplaceControl.setContentEditable(true);
            break;
        case "comment":
            inplaceControl.setCommentEditable(true);
            break;
        case "fbNo":
            inplaceControl.setFbNoEditable(true);
            break;
        default:
            log.info("invalid type : {}", type);
            break;
        }

        if (loadWorkTestInfoByUrlParam) {
            this.reloadWorkTestInfo(testInfo.getTestinfoNo());
            display.update("wt_detail");
        } else {
            int index = testinfos.indexOf(testInfo);
            testinfos.set(index, testInfo);
            updateAccordionPanelByIndex(index, false, true, false);
        }
    }

    /**
     * 新增/修改測試人員
     */
    public void updateTesters() {
        try {
            String userSids = usersMultiSelectCompt.getTargetUsers().stream().map(user -> user.getUserSid().toString()).collect(Collectors.joining(","));
            String column = null;
            if (isMaster) {
                testInfo.setMasterTesters(StringUtils.isBlank(userSids) ? null : userSids);
                column = "masterTesters";
            } else {
                testInfo.setSlaveTesters(StringUtils.isBlank(userSids) ? null : userSids);
                column = "slaveTesters";
            }

            log.info("is QA User:{}", workBackendParamHelper.isQAUser(SecurityFacade.getUserSid()));
            stService.saveOrUpdate(testInfo, loginBean.getUser(), column, PermissionHelper.getInstance().isQAManager());

        } catch (IllegalAccessException e) {
            MessagesUtils.showError(e.getMessage());
            log.warn(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            // update tab
            updateTabInfo(requireService.findByReqSid(testInfo.getSourceSid()));
            reloadWorkTestInfo(testInfo.getTestinfoNo());
            display.hidePfWidgetVar("testersButtonDlg_wv");
        }
    }

    /**
     * 修改資料實作
     * 
     * @param column
     */
    public void editDataImpl(WorkTestInfo testInfo, String column) {
        try {
            stService.saveOrUpdate(
                    testInfo,
                    loginBean.getUser(),
                    column,
                    PermissionHelper.getInstance().isQAManager());

            InplaceControl inplaceControl = testInfo.getInplaceControl();
            inplaceControl.cancel();

            if (loadWorkTestInfoByUrlParam) {
                reloadWorkTestInfo(testInfo.getTestinfoNo());
                display.update("wt_detail");
            } else {
                // update tab
                updateTabInfo(requireService.findByReqSid(testInfo.getSourceSid()));
                updateAccordionPanelByIndex(testinfos.indexOf(testInfo), false, true, false);
            }

            display.hidePfWidgetVar("qaLinkDlg_wv");
        } catch (IllegalArgumentException e) {
            MessagesUtils.showError(e.getMessage());
        } catch (IllegalAccessException e) {
            MessagesUtils.showError(e.getMessage());
            updateTabInfo(requireService.findByReqSid(testInfo.getSourceSid()));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }

    /**
     * 修改日期實作
     * 
     * @param column
     */
    public void editDateImpl(WorkTestInfo testInfo) {
        try {
            stService.editDateImpl(
                    testInfo,
                    loginBean.getUser());

            if (loadWorkTestInfoByUrlParam) {
                this.reloadWorkTestInfo(testInfo.getTestinfoNo());
                display.update("workTestInfo_all");
            } else {
                int index = testinfos.indexOf(testInfo);
                updateAccordionPanelByIndex(index, true, false, true);
            }
            display.hidePfWidgetVar("dateButtonDlg_wv");
        } catch (IllegalArgumentException e) {
            MessagesUtils.showError(e.getMessage());
        } catch (IllegalAccessException e) {
            // 送測單若已被異動, 重新刷新
            MessagesUtils.showError(e.getMessage());
            updateTabInfo(requireService.findByReqSid(testInfo.getSourceSid()));
            this.testInfo = testinfos.get(testinfos.indexOf(testInfo));
            display.update("viewPanelBottomInfoTabId:st_acc_panel_layer_zero");
            display.hidePfWidgetVar("dateButtonDlg_wv");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 主題、內容、備註、FBNO、QA連結inplace元件，取消動作
     */
    public void cancelReolad(WorkTestInfo testInfo) {
        InplaceControl inplaceControl = testInfo.getInplaceControl();
        inplaceControl.cancel();
        if (!loadWorkTestInfoByUrlParam) {
            // update tab
            updateTabInfo(requireService.findByReqSid(testInfo.getSourceSid()));

            this.testInfo = testinfos.get(testinfos.indexOf(testInfo));

            updateAccordionPanelByIndex(testinfos.indexOf(testInfo), false, true, false);
        } else {
            reloadWorkTestInfo(testInfo.getTestinfoNo());
            display.update("wt_detail");
        }
    }

    /**
     * 不全部刷新，只更新單一送測單(accordionPanel by index)
     * 
     * @param index
     * @param title   更新accordionPanel title
     * @param detail  更新accordionPanel detail
     * @param history 更新accordionPanel history
     */
    private void updateAccordionPanelByIndex(int index, boolean title, boolean detail, boolean history) {
        List<String> updateIds = Lists.newArrayList();
        if (title) {
            updateIds.add(String.format("viewPanelBottomInfoTabId:st_acc_panel_layer_zero:%s:wt_info_title", index));
        }
        if (detail) {
            updateIds.add(String.format("viewPanelBottomInfoTabId:st_acc_panel_layer_zero:%s:wt_detail", index));
        }
        if (history) {
            // update history list by WorkTestInfo
            WorkTestInfo workTestInfo = testinfos.get(index);
            List<WorkTestInfoHistory> historyList = sthService.findInfoHistorys(workTestInfo);
            workTestInfo.setInfoHistorys(historyList);

            updateIds.add(String.format("viewPanelBottomInfoTabId:st_acc_panel_layer_zero:%s:st_acc_panel_layer_one", index));
        }
        display.update(updateIds);
    }

    /**
     * 開啟/重載單張送測單時使用
     * 
     * @param testInfoNo
     */
    public void reloadWorkTestInfo(String testInfoNo) {
        if (loadWorkTestInfoByUrlParam) {
            this.testInfo = stService.findByTestinfoNo(testInfoNo);
            testInfo.setForwardToFbUrl(fbService.createFbUrl(testInfo.getFbId()));
        }
    }

    /**
     * 載入送測單
     */
    public void loadWorkTestInfoByOpener() {
        workTestUpDownBean.doWaitUpDown();
        if (!workTestUpDownBean.isEmptyCurrRow()) {
            String testInfoNo = workTestUpDownBean.getCurrRow();

            this.reloadWorkTestInfo(testInfoNo);

            workTestUpDownBean.clearCurrRow();
            String jsMethod = String.format("replaceUrlNotNavigation('%s');", urlService.createWorkTestInfoUrl(testInfoNo));
            display.execute(jsMethod);
            display.update(Lists.newArrayList("up_down_btn", "workTestInfo_all"));
            display.execute("hideLoad();");
        }
    }
}
