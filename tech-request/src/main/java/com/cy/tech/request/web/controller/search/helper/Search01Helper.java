/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.search.helper;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.enums.ConditionInChargeMode;
import com.cy.tech.request.logic.search.service.RequireReportUsageRecord;
import com.cy.tech.request.logic.search.service.Search01QueryService;
import com.cy.tech.request.logic.search.service.SearchCommonHelper;
import com.cy.tech.request.logic.search.service.SearchConditionSqlHelper;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.search.view.Search01View;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.logic.vo.CustomerTo;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.web.controller.view.component.searchquery.SearchQuery01;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.logic.lib.manager.WorkCommonReadRecordManager;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.logic.WkUserAndOrgLogic;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 輔助 Search01MBean
 *
 * @author kasim
 */
@Component
public class Search01Helper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3460341579637758121L;
    @Autowired
    @Qualifier("s01Query")
    transient private Search01QueryService queryService;
    @Autowired
    transient private SearchHelper helper;
    @Autowired
    transient private OrganizationService orgSerivce;
    @Autowired
    transient private ReqularPattenUtils reqularUtils;
    @Autowired
    transient private SearchConditionSqlHelper searchConditionSqlHelper;
    @Autowired
    transient private SearchResultHelper searchResultHelper;
    @Autowired
    transient private SearchHelper searchHelper;

    @Autowired
    transient private WkUserAndOrgLogic wkUserAndOrgLogic;
    @Autowired
    transient private WorkCommonReadRecordManager workCommonReadRecordManager;

    /**
     * 查詢列表資料
     *
     * @return
     */
    public List<Search01View> findWithQuery(
            String compId,
            SearchQuery01 searchQuery,
            List<CustomerTo> customerItems,
            Integer loginUserSid) {
        String requireNo = reqularUtils.getRequireNo(compId, searchQuery.getFuzzyText());
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT "
                + "tr.require_sid,"
                + "tr.require_no,"
                + "tid.field_content,"
                + "tr.urgency,"
                + "tr.has_forward_dep,"
                + "tr.has_forward_member,"
                + "tr.has_link,"
                + "tr.create_dt,"
                + "ckm.big_category_name,"
                + "ckm.middle_category_name,"
                + "ckm.small_category_name,"
                + "tr.dep_sid,"
                + "tr.create_usr,"
                + "tr.require_status,"
                + "sc.check_attachment,"
                + "tr.has_attachment,"
                + "tr.hope_dt,"
                + "tr.read_reason, "
                + "tr.customer,"
                + "tr.inte_staff,"
                + "tr.update_dt, "
                // 組共通 select 欄位 (一定要放在 select 最後, 否則後方取值 index 會錯誤)
                + this.searchConditionSqlHelper.prepareCommonSelectColumnByRequireType2()
                + " FROM ");
        this.buildRequireCondition(requireNo, searchQuery, customerItems, builder, parameters, loginUserSid);
        builder.append(this.searchHelper.buildRequireIndexDictionaryCondition(
                searchQuery.getTheme(),
                searchQuery.getFuzzyText(),
                requireNo));
        this.buildCategoryKeyMappingCondition(requireNo, searchQuery, builder, parameters);
        this.buildBasicDataSmallCategoryCondition(builder, parameters);

        // 檢查項目 (系統別)
        // 後方需對 主單 sid 做 group by
        builder.append(this.searchConditionSqlHelper.prepareCommonJoin(
                SecurityFacade.getUserSid()));

        builder.append(" WHERE 1=1 ");
        
        //查詢條件：是否閱讀
        if (WkStringUtils.isEmpty(requireNo)) {
            builder.append(
                    this.workCommonReadRecordManager.prepareWhereConditionSQL(
                            searchQuery.getSelectReadRecordType(), "readRecord"));
        }

        builder.append(" GROUP BY tr.require_sid ");
        builder.append(" ORDER BY tr.update_dt DESC");

        // show SQL in debug log
        SearchCommonHelper.getInstance().showSQLDebugLog(
                ReportType.REQUIRE, builder.toString(), parameters);

        // 建立報表使用記錄物件
        RequireReportUsageRecord usageRecord = new RequireReportUsageRecord(
                ReportType.REQUIRE, SecurityFacade.getUserSid());

        List<Search01View> resultList = queryService.findWithQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()), // 格式化 SQL
                parameters,
                SecurityFacade.getUserSid(),
                usageRecord);

        // ====================================
        // 後續處理
        // ====================================
        if (WkStringUtils.notEmpty(resultList)) {
            // 後續處理-開始
            usageRecord.afterProcessStart();
            // 系統別
            resultList = resultList.stream()
                    .filter(each -> this.searchResultHelper.filterCheckItems(
                            each.getCheckItemTypes(),
                            searchQuery.getCheckItemTypes(),
                            false,
                            null))
                    .collect(Collectors.toList());

            // REQ-1440 主責單位-已停用異常檢查
            if (ConditionInChargeMode.EXCEPTION_INACTIVE.equals(searchQuery.getConditionInChargeMode())) {
                resultList = resultList.stream()
                        .filter(each -> this.searchResultHelper.filterInactiveByInCharge(
                                each.getInChargeDepSid(),
                                each.getInChargeUsrSid()))
                        .collect(Collectors.toList());
            }

            // 若待閱原因無勾選"無待閱原因", 只留待閱讀
            if (!searchQuery.getSelectReqToBeReadType().contains(ReqToBeReadType.NO_TO_BE_READ.name())) {
                resultList = resultList.stream()
                        .filter(vo -> ReadRecordType.WAIT_READ.equals(vo.getReadRecordType()))
                        .collect(Collectors.toList());
            }

            // 後續處理-結束
            usageRecord.afterProcessEnd();
        }
        
        // 儲存使用記錄
        usageRecord.saveUsageRecord();
        
        return resultList;
    }

    /**
     * 組合需求單語法
     *
     * @param requireNo
     * @param searchQuery
     * @param customerItems
     * @param builder
     * @param parameters
     */
    private void buildRequireCondition(
            String requireNo,
            SearchQuery01 searchQuery,
            List<CustomerTo> customerItems,
            StringBuilder builder,
            Map<String, Object> parameters,
            Integer userSid) {

        builder.append("(   SELECT tr.*  ");
        builder.append("    FROM   tr_require tr ");

        builder.append("    WHERE 1=1 ");

        // 日期區間
        builder.append(
                this.searchConditionSqlHelper.prepareWhereConditionForDateRange(
                        requireNo,
                        searchQuery.getDateTimeType().getVal(),
                        searchQuery.getStartDate(),
                        searchQuery.getEndDate()));

        // 依據單位限制
        this.createReqDefQuery(searchQuery, builder, parameters, userSid);

        // 需求類別製作進度
        builder.append(this.searchConditionSqlHelper.prepareWhereConditionForRequireStatus(
                requireNo,
                searchQuery.getSelectRequireStatusType()));

        // 廳主
        if (Strings.isNullOrEmpty(requireNo)
                && WkStringUtils.notEmpty(searchQuery.getCustomers())
                && searchQuery.getCustomers().size() != customerItems.size()) {
            builder.append(" AND tr.customer IN (:customers)");
            parameters.put("customers", this.getCustomerSids(searchQuery));
        }

        // 待閱原因
        builder.append(
                this.searchConditionSqlHelper.prepareWhereConditionForWaitReadReason(
                        requireNo,
                        ReqToBeReadType.valueNames(),
                        searchQuery.getSelectReqToBeReadType(),
                        ReqToBeReadType.NO_TO_BE_READ.name(),
                        "tr.read_reason"));

        // 需求人員
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(searchQuery.getTrCreatedUserName())) {
            List<Integer> userSids = WkUserUtils.findByNameLike(searchQuery.getTrCreatedUserName(), SecurityFacade.getCompanyId());
            if (WkStringUtils.isEmpty(userSids)) {
                userSids.add(-999);
            }
            builder.append(" AND tr.create_usr IN (:userSids)");
            parameters.put("userSids", userSids.isEmpty() ? "" : userSids);
        }

        // 主責單位
        builder.append(
                this.searchConditionSqlHelper.prepareWhereConditionForInCharge(
                        requireNo,
                        searchQuery.getConditionInChargeMode(),
                        searchQuery.getInChargeDepSids(),
                        searchQuery.getInChargeUserSids()));

        //////////////////// 以下為進階搜尋條件//////////////////////////////
        // 異動區間
        if (Strings.isNullOrEmpty(requireNo) && searchQuery.getStartUpdatedDate() != null && searchQuery.getEndUpdatedDate() != null) {
            builder.append(" AND tr.update_dt BETWEEN :startUpdatedDate AND :endUpdatedDate");
            parameters.put("startUpdatedDate", helper.transStartDate(searchQuery.getStartUpdatedDate()));
            parameters.put("endUpdatedDate", helper.transEndDate(searchQuery.getEndUpdatedDate()));
        }
        // 緊急度
        if (Strings.isNullOrEmpty(requireNo)) {
            List<Integer> urgencyList = this.getUrgencyTypeList(searchQuery);
            if (urgencyList != null && !urgencyList.isEmpty()) {
                builder.append(" AND tr.urgency IN (:urgencyList)");
                parameters.put("urgencyList", urgencyList);
            }
        }
        // 需求單號
        if (!Strings.isNullOrEmpty(requireNo)) {
            builder.append(" AND tr.require_no = :requireNo");
            parameters.put("requireNo", requireNo);
        }
        // 需求單號
        if (Strings.isNullOrEmpty(requireNo) && !Strings.isNullOrEmpty(searchQuery.getRequireNo())) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(searchQuery.getRequireNo()) + "%";
            builder.append(" AND tr.require_no LIKE :requireNo");
            parameters.put("requireNo", textNo);
        }
        // 轉發部門
        if (Strings.isNullOrEmpty(requireNo) && searchQuery.getForwardDepts() != null && !searchQuery.getForwardDepts().isEmpty()) {
            builder.append(" AND tr.require_sid IN (SELECT tai.require_sid FROM tr_alert_inbox tai WHERE tai.forward_type='FORWARD_DEPT'"
                    + " AND tai.receive_dep IN (:forwardDepts))");
            parameters.put("forwardDepts", searchQuery.getForwardDepts());
        }

        builder.append(") AS tr ");
    }

    /**
     * @param searchQuery
     * @param builder
     * @param parameters
     */
    private void createReqDefQuery(
            SearchQuery01 searchQuery,
            StringBuilder builder,
            Map<String, Object> parameters,
            Integer userSid) {

        // 是否未選擇需求來源
        boolean isEmptyReqSource = Strings.isNullOrEmpty(searchQuery.getReqSource());

        List<String> sqlConditions = Lists.newArrayList();
        // ====================================
        // 挑選單位 String
        // ====================================
        String canViewDepSidStr = "";
        if (WkStringUtils.notEmpty(searchQuery.getRequireDepts())) {
            canViewDepSidStr = searchQuery.getRequireDepts().stream()
                    .map(depSid -> depSid + "")
                    .collect(Collectors.joining(","));
        } else {
            // 未選擇時，塞入全部可閱單位
            Set<Integer> canViewDepSidsBaseOnMinisterial = wkUserAndOrgLogic.prepareCanViewDepSidsBaseOnPanel(userSid, true);
            canViewDepSidStr = canViewDepSidsBaseOnMinisterial.stream()
                    .map(depSid -> depSid + "")
                    .collect(Collectors.joining(","));
        }

        // ====================================
        // 被設定為主責單位或負責人時可查閱
        // ====================================
        if (isEmptyReqSource || "責".equals(searchQuery.getReqSource())) {
            // 規則需同步 URLHelper.checkPermissionsByInCharge

            // 主要負責人
            sqlConditions.add(" tr.in_charge_usr = " + SecurityFacade.getUserSid() + " ");

            // 主責單位
            if (WkStringUtils.notEmpty(canViewDepSidStr)) {
                sqlConditions.add(" tr.in_charge_dep IN (" + canViewDepSidStr + ") ");
            }
        }

        // ====================================
        // 立
        // ====================================
        if (isEmptyReqSource || "立".equals(searchQuery.getReqSource())) {
            // 搜尋立案單位
            sqlConditions.add(" tr.dep_sid IN (" + canViewDepSidStr + ") ");
        }

        // ====================================
        // 分/通
        // ====================================
        if (isEmptyReqSource
                || "分".equals(searchQuery.getReqSource())
                || "通".equals(searchQuery.getReqSource())) {

            // 不分分派或通知
            String sql = ""
                    + "     EXISTS ( "
                    + "         SELECT search_info.require_sid "
                    + "           FROM tr_assign_send_search_info search_info "
                    + "          WHERE search_info.require_sid = tr.require_sid "
                    + "            AND search_info.dep_sid IN (" + canViewDepSidStr + ") ";

            // 有選擇時
            if (!isEmptyReqSource) {
                Integer currType = "分".equals(searchQuery.getReqSource()) ? 0 : 1;
                sql += " AND search_info.type = " + currType + " ";
            }

            sql += ") ";

            sqlConditions.add(sql);
        }

        // ====================================
        // 轉
        // ====================================
        if (isEmptyReqSource || "轉".equals(searchQuery.getReqSource())) {
            String sql = ""
                    + "     EXISTS ( "
                    + "         SELECT tai.require_sid "
                    + "           FROM tr_alert_inbox tai "
                    + "          WHERE tai.require_sid = tr.require_sid "
                    + "            AND ( "
                    + "                  (tai.forward_type = 'FORWARD_MEMBER'   AND tai.receive = " + userSid + ") " // 寄給登入者
                    + "                   OR (tai.forward_type = 'FORWARD_DEPT' AND tai.receive_dep IN (" + canViewDepSidStr + ") ) " // 寄給登入者可閱部門
                    + "                )) ";

            sqlConditions.add(sql);
        }

        builder.append(" AND (  ");
        builder.append(String.join(" OR ", sqlConditions));
        builder.append(" ) ");
    }

    /**
     * 依照單位查詢需求單
     * 
     * @param searchQuery
     * @param builder
     * @param parameters
     */
    @SuppressWarnings("unused")
    private void createByReqOrQuery(SearchQuery01 searchQuery, StringBuilder builder, Map<String, Object> parameters) {
        if (Strings.isNullOrEmpty(searchQuery.getReqSource())) {
            // 1.原邏輯 OR
            // 2.被分派單位的同仁們要能夠看的到（要能看到自己含底下單位所有的被分派的單據）OR
            // 3.被轉發部門與被轉發個人 要能夠看的到

            // 原邏輯【需求單位及其特殊權限可閱部門】
            builder.append(" AND (tr.dep_sid IN (:depSids)");
            parameters.put("depSids", searchQuery.getRequireDepts() == null || searchQuery.getRequireDepts().isEmpty()
                    ? "nerverFind"
                    : searchQuery.getRequireDepts());

            // 被分派單位的同仁們要能夠看的到（要能看到自己含底下單位所有的被分派/通知的單據）
            builder.append("  OR tr.require_sid IN ");
            builder.append("      (SELECT assi.require_sid FROM tr_assign_send_search_info assi ");
            builder.append("         WHERE assi.dep_sid IN (:assignDep)");
            builder.append("      )");
            parameters.put("assignDep", this.createOrgsInParam(orgSerivce.findChildOrgs(searchQuery.getDep())));

            // 被轉發部門與被轉發個人 要能夠看的到
            builder.append("  OR tr.require_sid IN ");
            builder.append("      (SELECT tai.require_sid FROM tr_alert_inbox tai ");
            builder.append("         WHERE 1=1  ");
            builder.append("           AND ((tai.forward_type = 'FORWARD_MEMBER' AND tai.receive     = :inboxReceive) ");
            builder.append("            OR ( tai.forward_type = 'FORWARD_DEPT'   AND tai.receive_dep = :inboxReceiveDep)) ");
            builder.append("         GROUP BY tai.require_sid ");
            builder.append("      )");
            parameters.put("inboxReceive", searchQuery.getUser().getSid());
            parameters.put("inboxReceiveDep", searchQuery.getDep().getSid());
            builder.append(" )");
        }
    }

    /**
     * @param searchQuery
     * @param builder
     * @param parameters
     */
    @SuppressWarnings("unused")
    private void createByReqCreateQuery(SearchQuery01 searchQuery, StringBuilder builder, Map<String, Object> parameters) {
        if (!Strings.isNullOrEmpty(searchQuery.getReqSource()) && searchQuery.getReqSource().equals("立")) {
            // 立單單位需求單位及其特殊權限可閱部門
            builder.append(" AND (tr.dep_sid IN (:depSids)");
            parameters.put("depSids", searchQuery.getRequireDepts() == null || searchQuery.getRequireDepts().isEmpty()
                    ? "nerverFind"
                    : searchQuery.getRequireDepts());
            builder.append(" )");
        }
    }

    /**
     * @param searchQuery
     * @param builder
     * @param parameters
     */
    @SuppressWarnings("unused")
    private void createByReqAssignQuery(SearchQuery01 searchQuery, StringBuilder builder, Map<String, Object> parameters) {
        if (!Strings.isNullOrEmpty(searchQuery.getReqSource()) && searchQuery.getReqSource().equals("分")) {
            // 所勾選的立案單位 分派到自己單位的需求單
            builder.append(" AND (tr.dep_sid IN (:depSids)");
            parameters.put("depSids", searchQuery.getRequireDepts() == null || searchQuery.getRequireDepts().isEmpty()
                    ? "nerverFind"
                    : searchQuery.getRequireDepts());
            // 被分派單位的同仁們要能夠看的到（要能看到自己含底下單位所有的被分派的單據）
            builder.append("     AND tr.require_sid IN ");
            builder.append("      (SELECT assi.require_sid FROM tr_assign_send_search_info assi ");
            builder.append("         WHERE assi.type = 0");
            builder.append("           AND assi.dep_sid IN (:assignDep) ");
            builder.append("      )");
            builder.append(" )");
            parameters.put("assignDep", this.createOrgsInParam(orgSerivce.findChildOrgs(searchQuery.getDep())));
        }
    }

    /**
     * @param searchQuery
     * @param builder
     * @param parameters
     */
    @SuppressWarnings("unused")
    private void createByReqForwardQuery(SearchQuery01 searchQuery, StringBuilder builder, Map<String, Object> parameters) {
        if (!Strings.isNullOrEmpty(searchQuery.getReqSource()) && searchQuery.getReqSource().equals("轉")) {
            // 被轉發部門與被轉發個人 要能夠看的到
            builder.append(" AND (tr.dep_sid IN (:depSids)");
            parameters.put("depSids", searchQuery.getRequireDepts() == null || searchQuery.getRequireDepts().isEmpty()
                    ? "nerverFind"
                    : searchQuery.getRequireDepts());

            // 被轉發部門與被轉發個人 要能夠看的到
            builder.append("  AND tr.require_sid IN ");
            builder.append("      (SELECT tai.require_sid FROM tr_alert_inbox tai ");
            builder.append("         WHERE 1=1  ");
            builder.append("           AND ((tai.forward_type = 'FORWARD_MEMBER' AND tai.receive     = :inboxReceive) ");
            builder.append("            OR ( tai.forward_type = 'FORWARD_DEPT'   AND tai.receive_dep = :inboxReceiveDep)) ");
            builder.append("         GROUP BY tai.require_sid ");
            builder.append("      )");
            parameters.put("inboxReceive", searchQuery.getUser().getSid());
            parameters.put("inboxReceiveDep", searchQuery.getDep().getSid());
            builder.append(" )");
        }
    }

    /**
     * @param requireNo
     * @param searchQuery
     * @param builder
     * @param parameters
     */
    private void buildCategoryKeyMappingCondition(String requireNo, SearchQuery01 searchQuery, StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT * FROM tr_category_key_mapping ckm WHERE 1=1");
        // 類別組合
        if (Strings.isNullOrEmpty(requireNo) && searchQuery.isSelCateSids()) {
            builder.append(" AND ckm.big_category_sid IN (:bigs) OR ckm.middle_category_sid IN (:middles) OR ckm.small_category_sid IN (:smalls)");
            // 加入單獨選擇的大類
            if (searchQuery.getSelectBigCategory() != null) {
                searchQuery.getBigDataCateSids().add(searchQuery.getSelectBigCategory().getSid());
            }
            parameters.put("bigs", searchQuery.getBigDataCateSids().isEmpty() ? "" : searchQuery.getBigDataCateSids());
            parameters.put("middles", searchQuery.getMiddleDataCateSids().isEmpty() ? "" : searchQuery.getMiddleDataCateSids());
            parameters.put("smalls", searchQuery.getSmallDataCateSids().isEmpty() ? "" : searchQuery.getSmallDataCateSids());
        }
        // 需求類別
        if (Strings.isNullOrEmpty(requireNo) && searchQuery.getSelectBigCategory() != null && searchQuery.getBigDataCateSids().isEmpty()) {
            builder.append(" AND ckm.big_category_sid = :big");
            parameters.put("big", searchQuery.getSelectBigCategory().getSid());
        }
        builder.append(") AS ckm ON tr.mapping_sid=ckm.key_sid ");
    }

    /**
     * @param builder
     * @param parameters
     */
    private void buildBasicDataSmallCategoryCondition(StringBuilder builder, Map<String, Object> parameters) {
        builder.append("INNER JOIN (SELECT sc.basic_data_small_category_sid,sc.check_attachment FROM "
                + "tr_basic_data_small_category sc) AS sc ON sc.basic_data_small_category_sid = ckm.small_category_sid ");
    }

    /**
     * 建立是否有分派快取
     *
     * @param searchQuery
     * @param reqSids
     * @return
     */
    public List<String> createAssignCache(SearchQuery01 searchQuery, List<String> reqSids) {
        if (CollectionUtils.isEmpty(reqSids)) {
            return Lists.newArrayList();
        }
        // ====================================
        // 兜組查詢
        // ====================================
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT DISTINCT assi.require_sid FROM tr_assign_send_search_info assi ");
        builder.append("  WHERE assi.type = 0 ");
        builder.append("    AND assi.dep_sid IN (:hasAssignDep) ");
        builder.append("    AND assi.require_sid IN (:reqSids) ");
        Map<String, Object> param = Maps.newHashMap();
        param.put("hasAssignDep", searchQuery.getRequireDepts() == null || searchQuery.getRequireDepts().isEmpty()
                ? "nerverFind"
                : searchQuery.getRequireDepts());

        // ====================================
        // 分次查詢 (避免參數過多, 出現 java.nio.BufferOverflowException)
        // ====================================
        return this.queryService.querySingleResultByBatch(
                builder.toString(),
                "reqSids",
                reqSids,
                param);
    }

    /**
     * 建立是否有分派給自己快取
     *
     * @param searchQuery
     * @param reqSids
     * @return
     */
    public List<String> createForwardSelfCache(SearchQuery01 searchQuery, List<String> reqSids) {
        if (CollectionUtils.isEmpty(reqSids)) {
            return Lists.newArrayList();
        }
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT DISTINCT tai.require_sid FROM tr_alert_inbox  tai ");
        builder.append("  WHERE ((tai.forward_type = 'FORWARD_MEMBER' AND tai.receive     = :hasForwardInboxReceive) OR ");
        builder.append("        ( tai.forward_type = 'FORWARD_DEPT'   AND tai.receive_dep = :hasForwardInboxReceiveDep)) ");
        builder.append("    AND tai.require_sid IN (:reqSids) ");
        Map<String, Object> param = Maps.newHashMap();
        param.put("hasForwardInboxReceive", searchQuery.getUser().getSid());
        param.put("hasForwardInboxReceiveDep", searchQuery.getDep().getSid());

        // ====================================
        // 分次查詢 (避免參數過多, 出現 java.nio.BufferOverflowException)
        // ====================================
        return this.queryService.querySingleResultByBatch(
                builder.toString(),
                "reqSids",
                reqSids,
                param);
    }

    /**
     * 建立是否有追蹤資料快取
     *
     * @param searchQuery
     * @param reqSids
     * @return
     */
    public List<String> createTraceCache(SearchQuery01 searchQuery, List<String> reqSids) {
        if (CollectionUtils.isEmpty(reqSids)) {
            return Lists.newArrayList();
        }
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT DISTINCT trace.trace_source_sid FROM work_trace_info trace ");
        builder.append(" WHERE trace.trace_source_type = 'TECH_REQUEST'");
        builder.append("   AND trace.trace_source_sid IN (:reqSids)");
        builder.append("   AND (trace.trace_dep = ").append(searchQuery.getDep().getSid());
        builder.append("         OR trace.trace_usr = ").append(searchQuery.getUser().getSid());
        builder.append("       ) ");
        builder.append("   AND trace.trace_status = 'UN_FINISHED' ");
        builder.append("   AND trace.Status = '0' ");
        Map<String, Object> param = Maps.newHashMap();

        // ====================================
        // 分次查詢 (避免參數過多, 出現 java.nio.BufferOverflowException)
        // ====================================
        return this.queryService.querySingleResultByBatch(
                builder.toString(),
                "reqSids",
                reqSids,
                param);
    }

    /**
     * 轉換List<Org>為報表查詢用參數
     *
     * @param orgs
     * @return
     */
    private List<Integer> createOrgsInParam(List<Org> orgs) {
        return orgs.stream().map(Org::getSid).collect(Collectors.toList());
    }

    /**
     * 取得customers中sid
     *
     * @param searchQuery
     * @return
     */
    private List<Long> getCustomerSids(SearchQuery01 searchQuery) {
        return searchQuery.getCustomers().stream().map(CustomerTo::getSid).collect(Collectors.toList());
    }

    /**
     * @param searchQuery
     * @return
     */
    private List<Integer> getUrgencyTypeList(SearchQuery01 searchQuery) {
        if (searchQuery.getUrgencyList() == null) {
            return Lists.newArrayList();
        }
        return searchQuery.getUrgencyList().stream()
                .map(UrgencyType::valueOf)
                .map(UrgencyType::ordinal)
                .collect(Collectors.toList());
    }
}
