package com.cy.tech.request.web.controller.home;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.MenuService;
import com.cy.tech.request.logic.service.report.favorite.HomePageFavoriteService;
import com.cy.tech.request.logic.vo.FunctionNodeTo;
import com.cy.tech.request.vo.enums.MenuBaseType;
import com.cy.tech.request.vo.menu.TrMenuGroupBase;
import com.cy.tech.request.vo.menu.TrMenuItem;
import com.cy.tech.request.vo.menu.TrMenuItemGroup;
import com.cy.tech.request.vo.report.favorite.HomePageFavorite;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.work.common.cache.WkUserWithRolesCache;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.collections.CollectionUtils;
import org.omnifaces.util.Faces;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 入口程式<BR/>
 * 功能:<BR/>
 * 建構成員可使用程式列表<BR/>
 *
 * @author shaun
 */
@Slf4j
@Controller
@Scope("view")
@ManagedBean
public class PortalMBean implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -7946257646299870249L;
    @Autowired
	transient private LoginBean loginBean;
	@Autowired
	transient private MenuService menuService;
	@Autowired
	transient private HomePageFavoriteService homePageService;

	/** 新增預設Url */
	@Getter
	@Setter
	@Value("${config.portal.newRequireUrl}")
	private String newRequireUrl;
	/** High Light ID */
	@Getter
	@Setter
	private String menuitemId;
	@Getter
	private boolean showSetting;
	private String keepType = "HOME";
	@Getter
	private MenuModel menuModel = new DefaultMenuModel();
	// @Getter
	// private List<String> userPermission;

	@PostConstruct
	void init() {
		this.checkParamMap();
		this.createMenuNode(keepType);
		this.showSetting = showSetting();
	}

	private void checkParamMap() {
		if (Faces.getRequestParameterMap().containsKey("type")) {
			keepType = Faces.getRequestParameterMap().get("type");
		}
		if (Faces.getRequestParameterMap().containsKey("menuitemId")) {
			menuitemId = Faces.getRequestParameterMap().get("menuitemId");
		}
	}

	// 重新載入功能清單
	public void reloadMenu(String type) throws IOException {
		if (Faces.getRequestParameterMap().containsKey("type")) {
			keepType = Faces.getRequestParameterMap().get("type");
		}
		if (Faces.getRequestParameterMap().containsKey("menuitemId")) {
			menuitemId = Faces.getRequestParameterMap().get("menuitemId");
		}
		this.createMenuNode(type);
	}

	/**
	 * 建立功能清單
	 * 
	 * @param menuType
	 */
	private void createMenuNode(String menuType) {
		this.menuModel = new DefaultMenuModel();
		MenuBaseType type = MenuBaseType.valueOf(menuType);
		TrMenuGroupBase groupBase = menuService.findMenuBaseGroup(type);

		// 取得登入者所有角色
		List<String> userRoleNames = WkUserWithRolesCache.getInstance().findRoleNamesByUserAndLoginCompID(
		        SecurityFacade.getUserSid(),
		        SecurityFacade.getCompanyId());

		for (TrMenuItemGroup groupItem : groupBase.getMenuItemGroup()) {
			if (groupItem.getItems().isEmpty()) {
				continue;
			}
			DefaultSubMenu subMenu = new DefaultSubMenu(groupItem.getName());
			for (TrMenuItem item : groupItem.getItems()) {
				boolean hasPermission = menuService.hasPermission(
				        item.getPermissionRole(),
				        userRoleNames,
				        SecurityFacade.getUserSid());

				if (hasPermission) {

					subMenu.addElement(this.createMenuitem(item));
				}
			}
			if (CollectionUtils.isNotEmpty(subMenu.getElements())) {
				menuModel.addElement(subMenu);
			}
		}

		// 首頁, 加入報表快選區
		if (MenuBaseType.HOME.equals(type)) {
			DefaultSubMenu quickMenu = new DefaultSubMenu("報表快選區");
			HomePageFavorite homePage = homePageService.findByCreatedUser(loginBean.getUser());

			if (homePage != null && CollectionUtils.isNotEmpty(homePage.getHomePages().getValue())) {
				TrMenuGroupBase reportBase = menuService.findMenuBaseGroup(MenuBaseType.REPORT);
				TrMenuGroupBase settingBase = menuService.findMenuBaseGroup(MenuBaseType.SETTING);
				List<TrMenuGroupBase> groupBaseList = Lists.newArrayList(reportBase, settingBase);
				for (TrMenuGroupBase baseGroup : groupBaseList) {
					if (baseGroup == null || baseGroup.getMenuItemGroup() == null) {
						menuModel.addElement(quickMenu);
						continue;
					}
					for (TrMenuItemGroup groupItem : baseGroup.getMenuItemGroup()) {
						if (groupItem.getItems().isEmpty()) {
							continue;
						}
						for (TrMenuItem item : groupItem.getItems()) {

							boolean hasPermission = menuService.hasPermission(
							        item.getPermissionRole(),
							        userRoleNames,
							        SecurityFacade.getUserSid());

							if (hasPermission
							        && homePage.getHomePages().getValue().contains(item.getUrl())) {
								quickMenu.addElement(this.createMenuitem(item));
							}
						}
					}
				}
			}
			menuModel.addElement(quickMenu);
		}
	}

	/**
	 * 建立選單
	 * 
	 * @param item
	 * @return
	 */
	private DefaultMenuItem createMenuitem(TrMenuItem item) {
		DefaultMenuItem menuitem = new DefaultMenuItem();
		menuitem.setStyleClass(item.getComponentID());
		menuitem.setId(item.getComponentID());
		menuitem.setIcon(item.getIcon());
		menuitem.setOutcome(item.getUrl());
		menuitem.setTarget("frameCenter");
		menuitem.setProcess("@this");
		menuitem.setOnclick(String.format("highlightSingle('%s');toggleLayoutByBlock('block');", item.getComponentID()));

		String countStr = null;
		String color = null;
		try {
			// 查詢登入者所有角色
			List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
			        SecurityFacade.getUserSid(),
			        SecurityFacade.getCompanyId());
			
			List<String> userRoleNames = WkUserWithRolesCache.getInstance().findRoleNamesByUserAndLoginCompID(
			        SecurityFacade.getUserSid(),
			        SecurityFacade.getCompanyId());

			countStr = menuService.findTitleCount(
			        item.getCountSql(),
			        userRoleNames,
			        roleSids,
			        loginBean.getUser());
			color = menuService.findTitleColor(countStr);
		} catch (Exception e) {
			countStr = "0";
			color = "gray";
			log.error(e.getMessage(), e);
		}
		menuitem.setValue(menuService.findTittle(countStr, item.getTitle()));
		menuitem.setStyle(String.format("color:%s;", color));

		return menuitem;
	}

	/**
	 * 判斷是否顯示維護主選項
	 *
	 * @return
	 */
	public Boolean showSetting() {

		// 查詢登入者所有角色
		List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
		        SecurityFacade.getUserSid(),
		        SecurityFacade.getCompanyId());
		
		List<String> roleNames = WkUserWithRolesCache.getInstance().findRoleNamesByUserAndLoginCompID(
		        SecurityFacade.getUserSid(),
		        SecurityFacade.getCompanyId());

		FunctionNodeTo node = menuService.createFunctionMenu(
		        MenuBaseType.SETTING,
		        roleNames,
		        roleSids,
		        loginBean.getUser());
		if (node != null && node.getSubs() != null && !node.getSubs().isEmpty()) {
			return true;
		}
		return false;
	}
}
