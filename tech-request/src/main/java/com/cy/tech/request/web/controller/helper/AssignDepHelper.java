/**
 * 
 */
package com.cy.tech.request.web.controller.helper;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.AssignNoticeService;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.google.common.collect.Lists;

import lombok.NoArgsConstructor;

/**
 * @author allen1214_wu
 *
 */
@Component
@NoArgsConstructor
public class AssignDepHelper {

    // ========================================================================
    // 服務元件區
    // ========================================================================
    @Autowired
    private transient AssignNoticeService assignNoticeService;
    @Autowired
    private transient WkOrgCache wkOrgCache;

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * @param requireSid
     * @param ownerDepSelectItems
     * @return
     * @throws SystemOperationException
     * @throws SystemDevelopException
     */
    public List<WkItem> prepareLoginUserCaseOwnerDepsByAssignDep(
            String requireSid) throws SystemOperationException, SystemDevelopException {

        List<WkItem> ownerDepItems = Lists.newArrayList();

        // ====================================
        // 以『已分派單位』 計算登入者開單時, 該單據的歸屬單位
        // ====================================
        List<Integer> relationOwnerDeps = this.assignNoticeService.prepareCaseOwnerDepsByAssignDep(requireSid, SecurityFacade.getUserSid());

        if (WkStringUtils.isEmpty(relationOwnerDeps)) {
            throw new SystemDevelopException("assignNoticeService.prepareCaseOwnerDepsByAssignDep 不應回傳空值！");
        }

        // ====================================
        // 僅回傳一筆時不可選 （ownerDepSelectItems）留空
        // ====================================
        if (relationOwnerDeps.size() == 1) {
            ownerDepItems.add(this.trnsOrgSidToWkItem(relationOwnerDeps.get(0)));
            return ownerDepItems;
        }

        // ====================================
        // 多筆可選單位
        // ====================================
        // 排序
        relationOwnerDeps = WkOrgUtils.sortByOrgTree(relationOwnerDeps);
        // 轉WkItem
        for (Integer depSid : relationOwnerDeps) {
            // 轉WkItem
            WkItem orgDataItem = this.trnsOrgSidToWkItem(depSid);
            // 加入選項
            ownerDepItems.add(orgDataItem);
        }
        return ownerDepItems;
    }

    /**
     * 單位sid 轉 WkItem
     * 
     * @param depSid
     * @return
     * @throws SystemOperationException 找不到單位資料時拋出
     */
    public WkItem trnsOrgSidToWkItem(Integer depSid) throws SystemOperationException {
        // 查詢單位資料
        Org dep = this.wkOrgCache.findBySid(depSid);
        if (dep == null) {
            throw new SystemOperationException("找不到單位資料！", InfomationLevel.ERROR);
        }
        // 轉 WkItem
        return new WkItem(
                dep.getSid() + "",
                WkOrgUtils.getOrgName(dep),
                Activation.ACTIVE.equals(dep.getStatus()));
    }

}
