package com.cy.tech.request.web.controller.require;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.exception.LockRecordException;
import com.cy.tech.request.logic.enumerate.RejectReason;
import com.cy.tech.request.logic.service.LockService;
import com.cy.tech.request.logic.service.ReqUnitBpmService;
import com.cy.tech.request.logic.service.RequireCheckItemService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.RequireTraceService;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireTrace;
import com.cy.tech.request.web.controller.enums.RequireBottomTabType;
import com.cy.tech.request.web.controller.values.LoginBean;
import com.cy.tech.request.web.pf.utils.ConfirmCallbackDialogController;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.event.CloseEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 退件通知
 *
 * @author jason_h
 */
@Slf4j
@Controller
@Scope("view")
public class RequireRejectMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1446056762493477551L;
    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    transient private Require01MBean require01MBean;
    @Autowired
    transient private LoginBean loginBean;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private RequireTraceService requireTraceService;
    @Autowired
    transient private LockService lockService;
    @Autowired
    transient private DisplayController display;
    @Autowired
    transient private ReqUnitBpmService reqUnitBpmService;
    @Autowired
    transient private ConfirmCallbackDialogController confirmCallbackDialogController;
    @Autowired
    transient private RequireCheckItemService requireCheckItemService;

    // ========================================================================
    // 服務
    // ========================================================================
    /**
     * 退件通知追蹤物件
     */
    @Getter
    @Setter
    private RequireTrace rejectTrace = new RequireTrace();
    @Getter
    @Setter
    private SelectItem[] rollbackItems;// 簽核人員節點
    @Getter
    @Setter
    private int selectedHistoryIndex;// 選擇的節點index

    public void preOpenDialog(Require require) {

        // ====================================
        // 防呆
        // ====================================
        if (require == null) {
            log.warn("傳入 require 為空!");
            MessagesUtils.showWarn(WkMessage.NEED_RELOAD);
        }

        // ====================================
        // 提示已有完成檢查的項目
        // ====================================
        // 查詢已檢查項目
        List<RequireCheckItemType> checkedItems = requireCheckItemService.findCheckedItems(require.getSid());

        // 有已檢查項目時，顯示確認訊息
        if (WkStringUtils.notEmpty(checkedItems)) {
            String confirmMessage = "<br/>"
                    + "其他項目：["
                    + checkedItems.stream().map(RequireCheckItemType::getDescr).collect(Collectors.joining("]、["))
                    + "]&nbsp;已完成檢查，退件後需重新檢查。"
                    + "<br/><br/>"
                    + "是否確定執行退件？";

            confirmCallbackDialogController.showConfimDialog(
                    confirmMessage,
                    Lists.newArrayList("title_info_click_btn_id", "rejectPanel"),
                    () -> initRejectNotify());

            return;
        }

        // ====================================
        // 無需彈跳確認窗, 直接進行原流程
        // ====================================
        this.initRejectNotify();

    }

    /**
     * 點擊退件通知
     */
    public void initRejectNotify() {
        Require require = require01MBean.getRequire();
        this.bulidRollbackItems(require);
        try {
            lockService.lock(require, loginBean.getUser());

            // 初始化追蹤資料
            this.rejectTrace = requireTraceService.initRequireRejectTrace(
                    require.getSid(),
                    loginBean.getUser(),
                    RejectReason.CATEGORY_MISTAKE.getLabel());

            this.display.update(Lists.newArrayList("title_info_click_btn_id", "rejectPanel"));

            display.showPfWidgetVar("rejectDlgWv");
            require01MBean.getBottomTabMBean().resetTabIdx(require01MBean);
            require01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.TRACE);
            require01MBean.getTitleBtnMBean().clear();
        } catch (LockRecordException ex) {
            log.info(ex.getMessage(), ex);
            MessagesUtils.showError(ex.getMessage());
            String lockTimeStr = lockService.getLockedTimeString(require);
            display.execute("countDown('" + lockTimeStr + "');");
        }
    }

    /**
     * 變更退件原因
     */
    public void changeRejectReason() {
        if (RejectReason.CATEGORY_MISTAKE.getLabel().equals(rejectTrace.getReason())) {
            initRejectNotify();
            return;
        }
        if (RejectReason.OTHER.getLabel().equals(rejectTrace.getReason())) {
            rejectTrace.setRequireTraceContent("");
        }
    }

    /**
     * 儲存
     */
    public void save() {
        Require require = require01MBean.getRequire();
        try {
            // 執行退件通知
            requireService.executeRejectNotity(rejectTrace, loginBean.getUser(), require, selectedHistoryIndex);

            require01MBean.getReqBpmMBean().init();
            require01MBean.getTraceMBean().clear();
            require01MBean.getBottomTabMBean().resetTabIdx(require01MBean);
            require01MBean.getBottomTabMBean().changeTabByTabType(RequireBottomTabType.TRACE);
            require01MBean.getTitleBtnMBean().clear();
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            require01MBean.reBuildeByUpdateFaild();
            return;

        } catch (Exception e) {
            if (e instanceof UserMessageException) {
                MessagesUtils.show((UserMessageException) e);
            } else if (e.getCause() instanceof UserMessageException) {
                MessagesUtils.show((UserMessageException) e.getCause());
            } else {
                log.error(require01MBean.getRequire().getRequireNo() + ":" + loginBean.getUser().getName() + ":執行退件通知失敗..." + e.getMessage(), e);
                MessagesUtils.showError(e.getMessage());
            }

            require01MBean.reBuildeByUpdateFaild();

        }
        display.hidePfWidgetVar("rejectDlgWv");
    }

    /**
     * 取消鎖定
     *
     * @param event
     */
    public void cancelLock(CloseEvent event) {
        Require require = require01MBean.getRequire();
        try {
            lockService.unlock(require);
        } catch (Exception e) {
            log.error("取消鎖定失敗..." + e.getMessage(), e);
            MessagesUtils.showError(e.getMessage());
        }
    }

    /**
     * 建立退回人員
     */
    private void bulidRollbackItems(Require require) {
        try {
            String instanceId = require.getReqUnitSign().getBpmInstanceId();
            List<ProcessTaskBase> tasks = reqUnitBpmService.findFlowChartByInstanceId(instanceId, SecurityFacade.getUserId());
            this.rollbackItems = new SelectItem[tasks.size()];
            int index = 0;
            for (ProcessTaskBase each : tasks) {
                String label = each.getTaskName().contains("申請人") ? "申請人" : each.getRoleName();
                SelectItem item = new SelectItem(tasks.indexOf(each), label);
                rollbackItems[index++] = item;
            }
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
        }
    }
}
