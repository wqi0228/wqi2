package com.cy.tech.request.web.controller.setting.backand.enums;

import com.cy.work.common.utils.WkStringUtils;

import lombok.Getter;

/**
 * GM後台參數維護類別
 *
 * @author Allen
 */
public enum GmBackandMaintainType {

	GM_USER("一般GM"),
	GM_ADMIN("GM管理者"),
	;

	/**
	 * 類別說明
	 */
	@Getter
	private String descr;

	/**
	 * @param descr 類別說明
	 */
	private GmBackandMaintainType(
	        String descr) {
		this.descr = descr;
	}

	/**
	 * @param str
	 * @return
	 */
	public static GmBackandMaintainType safeValueOf(String str) {
		if (WkStringUtils.notEmpty(str)) {
			for (GmBackandMaintainType gmBackandMaintainType : GmBackandMaintainType.values()) {
				if (gmBackandMaintainType.name().equals(str)) {
					return gmBackandMaintainType;
				}
			}
		}
		return null;
	}

	/**
	 * @param WebCodeStr
	 * @return
	 */
	public static String safeGetDescr(String webCodeStr) {
		GmBackandMaintainType gmBackandMaintainType = safeValueOf(webCodeStr);
		if (gmBackandMaintainType != null) {
			return gmBackandMaintainType.getDescr();
		}
		return "";
	}
}
