/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.web.controller.require;

import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.TemplateService;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.template.CategoryKeyMapping;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import com.cy.tech.request.vo.template.component.ComBase;
import com.cy.tech.request.vo.template.component.ComTextTypeOne;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.work.customer.vo.WorkCustomer;
import com.cy.work.mapp.create.trans.vo.MappCreateTrans;
import com.cy.work.mapp.create.trans.vo.enums.MappTransType;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 需求單模版處理<BR/>
 * 取得同畫面需求單模版方法 <BR/>
 * RequireTemplateFieldMBean templateMBean = (RequireTemplateFieldMBean)
 * Faces.getApplication().getELResolver().getValue(Faces.getELContext(), null,
 * "requireTemplateFieldMBean");
 *
 * @author shaun
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope("view")
public class RequireTemplateFieldMBean implements IRequireLoad, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5499333651031880635L;
    @Autowired
    transient private TemplateService tempService;
    @Autowired
    transient private RequireService requireService;
    /** 初始化控制 */
    @Autowired
    transient private ReqLoadBean loadManager;

    /** 模版 */
    @Getter
    @Setter
    private CategoryKeyMapping mapping;
    /** 模版前端頁面容器 */
    @Getter
    @Setter
    private List<FieldKeyMapping> templateItem = Lists.newArrayList();
    /** 編輯開關 */
    @Getter
    private Boolean disabled;
    /** 前端頁面控制資料物件 */
    @Getter
    @Setter
    transient private Map<String, ComBase> comValueMap = Maps.newHashMap();

    // revise in tech-request.properties
    @Value("#{'${tech-request.interaction.category.mapping_id}'.split(',')}")
    private List<String> categoryList;

    public boolean getTriggerOnChangeEvent() {
        if (mapping != null) {
            return categoryList.contains(mapping.getId());
        }

        return false;
    }

    @PostConstruct
    public void init() {
        disabled = Boolean.FALSE;
        try {
            this.loadManager.checkLoad(this);
        } catch (Exception e) {
            log.error("初始化失敗!" + e.getMessage(), e);
            MessagesUtils.showError("初始化失敗!");
            return;
        }
    }

    @Override
    public void initByNewRequire() {
    }

    @Override
    public void initByCopy(Require source) {
        this.mapping = source.getMapping();
        this.templateItem = tempService.loadTemplateFieldByMapping(source.getMapping());
        this.comValueMap = tempService.loadTemplateFieldValue(source);
        this.toggleDisabled(false);
    }

    @Override
    public void initByIssueCreate(MappCreateTrans trans) {
        String smallSid = trans.getMessageTo().getSmallCategorySid();
        String smallName = trans.getMessageTo().getSmallCategoryName();
        this.mapping = tempService.findMappBySmallSidAndName(smallSid, smallName).get();
        this.templateItem = tempService.loadTemplateFieldByMapping(this.mapping);
        this.comValueMap = tempService.createNewDocValueMap(templateItem);

        if (MappTransType.ISSUE_TO_REQ_PROGRAM_REQ.equals(trans.getType())) {
            tempService.resetTheme(comValueMap, "");
            tempService.resetEditorByFieldName(comValueMap, TemplateService.FIELD_NAME_CONTEXT, "");
            tempService.resetEditorByFieldName(comValueMap, TemplateService.FIELD_NAME_NOTE, "");
        }
        this.toggleDisabled(false);
    }

    @Override
    public void initByIssueCreateOnpg(MappCreateTrans trans) {
        Require r = requireService.findByReqSid(trans.getTargetSid());
        if (r == null) {
            return;
        }
        this.initByUrl(r.getRequireNo());
    }

    @Override
    public void initByUrl(String sourceNo) {
        if (!requireService.isExistNo(sourceNo)) {
            return;
        }
        Require source = requireService.findByReqNo(sourceNo);
        this.initByTable(source);
    }

    @Override
    public void initByTable(Require source) {
        this.disabled = Boolean.TRUE;
        this.mapping = source.getMapping();
        this.templateItem = tempService.loadTemplateFieldByMapping(this.mapping);
        this.comValueMap = tempService.loadTemplateFieldValue(source);
    }

    /**
     * 初始化新模版
     *
     * @param selectMapping
     */
    public void initBySelectMapping(CategoryKeyMapping selectMapping) {

        // 取得模版欄位
        List<FieldKeyMapping> selectTemplateItem = tempService.loadTemplateFieldByMapping(selectMapping);
        if (selectTemplateItem.isEmpty()) {
            MessagesUtils.showInfo("選擇類別需求單模版未建立，請冾E化發展部。");
            return;
        }

        // 設定對應模版
        this.templateItem = selectTemplateItem;
        // 設定對應值
        this.mapping = selectMapping;
        // 設定模版內容值
        this.comValueMap = tempService.createNewDocValueMap(selectTemplateItem);
    }

    /**
     * 傳說中的復原
     *
     * @param require
     */
    public void recoveryTemplate(Require require) {
        this.initByTable(require);
    }

    /**
     * 清除全部資料
     */
    public void clearAllValue() {
        this.templateItem.clear();
    }

    /**
     * 編輯切換
     *
     * @param var
     */
    public void toggleDisabled(Boolean var) {
        this.disabled = var;
    }

    public ComBase findThemeCom() {
        return tempService.findFieldByMap("主題", comValueMap);
    }

    /**
     * 修改廳主(單獨Dialog連動)
     *
     * @param customer
     * @param comModi
     */
    public void changeThemeValueByCustomer(WorkCustomer customer, ComTextTypeOne comModi) {
        if (comModi != null) {
            comModi.setValue02(customer.getName() + " @" + customer.getLoginCode() + " " + comModi.getValue02());
            ComBase comBase = this.findThemeCom();
            if (comBase != null && comBase instanceof ComTextTypeOne) {
                ComTextTypeOne comTextOne = (ComTextTypeOne) comBase;
                comTextOne.setValue02(comModi.getValue02());
            }
        }
    }

    /**
     * 修改廳主(一般編輯時連動)
     *
     * @param customer
     */
    public void changeThemeValueByCustomer(WorkCustomer customer) {
        this.tempService.changeThemeValueByCustomer(customer, this.comValueMap);
    }

    /**
     * 取回需求單主題
     *
     * @param requireNo
     * @return
     */
    public String findTheme(String requireNo) {
        if (!Strings.isNullOrEmpty(requireNo)) {
            Require req = requireService.findByReqNo(requireNo);
            Map<String, ComBase> comMap = tempService.loadTemplateFieldValue(req);
            return tempService.findTheme(req, comMap);
        }
        if (mapping != null) {
            log.error("需求單號：" + requireNo + " 模板SID:　" + mapping.getSid() + " 查無主題欄位，請檢查。");
        }
        return "";
    }

    /**
     * 重設定主題後綴 20170320
     */
    public void resetThemeSuffixBySpecCom() {
        try {
            tempService.resetThemeSuffixBySpecCom(templateItem, comValueMap);
        } catch (Exception e) {
            log.error("處理主題後綴失敗(ComRadioTypeThree)!!", e);
        }
    }

    @Override
    public boolean loadSuccessWorkTestInfoByUrlParam(WorkTestInfo workTestInfo) {
        return true;
    }

    @Override
    public void reloadToIllegalPage(String errorCode) {
    }

}
