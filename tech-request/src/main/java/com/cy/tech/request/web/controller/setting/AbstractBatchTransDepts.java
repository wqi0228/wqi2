package com.cy.tech.request.web.controller.setting;

import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.cy.tech.request.logic.utils.DateUtils;
import com.cy.tech.request.repository.result.TransRequireVO;
import com.cy.tech.request.web.controller.report.AbstractReporter;
import com.cy.tech.request.web.controller.report.ExcelWriter;
import com.cy.tech.request.web.controller.view.component.OrgTreeVO;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 批次轉單程式
 * @author aken_kao
 *
 */
@Slf4j
public abstract class AbstractBatchTransDepts extends AbstractReporter  {
    /**
     * 
     */
    private static final long serialVersionUID = -4480641058604044743L;
    @Getter
    protected OrgTreeVO originTreeVO = new OrgTreeVO();
    @Getter
    protected OrgTreeVO transOrgTreeVO = new OrgTreeVO();
    @Getter
    protected List<TransRequireVO> resultList = Lists.newArrayList();
    @Getter
    protected List<TransRequireVO> selectedResultList = Lists.newArrayList();
    @Getter
    @Setter
    protected List<TransRequireVO> filterList = Lists.newArrayList();
    @Getter
    protected Set<String> updatedSet = Sets.newHashSet();
    @Getter
    @Setter
    protected boolean selectedAll = false;
    
    /**
     * 填單單位
     */
    public void openOrgTree(){
        originTreeVO.initOrgTree(true, false);
    }
    
    /**
     * 轉換單位
     */
    public void openTransOrgTreeVO(){
        transOrgTreeVO.initOrgTree(true, false);
    }
    
    /**
     * 變更dataTable 全選
     */
    public void checkBySelectedAll() {
        try {
            selectedResultList.clear();
            if(CollectionUtils.isNotEmpty(filterList)) {
                for(TransRequireVO vo : filterList){
                    vo.setChecked(selectedAll);
                    if(selectedAll) {
                        selectedResultList.add(vo);
                    }
                }
            } else {
                for(TransRequireVO vo : resultList){
                    vo.setChecked(selectedAll);
                    if(selectedAll) {
                        selectedResultList.add(vo);
                    }
                }
            }
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
        }
    }
    
    /**
     * 點擊check box
     * @param vo
     */
    public void checkBySingle(TransRequireVO vo) {
        try {
            if (vo.isChecked()) {
                selectedResultList.add(vo);
                selectedAll = resultList.stream().allMatch(each -> each.isChecked());
            } else {
                selectedResultList.remove(vo);
                selectedAll = false;
            }
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
        }
    }

    @Override
    public ExcelWriter buildExcelWriter() {
        ExcelWriter writer = new ExcelWriter();
        String[]title = new String[]{"建立日期:15", "填單單位:20", "填單人員:15", "主題:80", "單號:20", "狀態:20"};
        writer.setTitleAndWidth(title, 0);
        writer.getNextRowIndex();
        for(TransRequireVO vo : resultList){
            int currentIndex = 0;
            writer.setCellValue(currentIndex++, vo.getCreateDate() == null ? "" : DateUtils.YYYY_MM_DD.print(vo.getCreateDate().getTime()));
            writer.setCellValue(currentIndex++, vo.getCreateDept());
            writer.setCellValue(currentIndex++, vo.getCreateUserName());
            writer.setLeftCellValue(currentIndex++, vo.getTheme());
            writer.setCellValue(currentIndex++, vo.getOrderNo());
            writer.setCellValue(currentIndex++, vo.getStatus());
            writer.getNextRowIndex();
        }
        
        return writer;
    }
    
    public void resetWhileOnChange() {
        updatedSet.clear();
    }
    
    /**
     * 轉單成功後顯示check icon
     */
    public void assignSuccessIcon() {
        for (TransRequireVO vo : resultList) {
            if (updatedSet.contains(vo.getOrderNo())) {
                vo.setAlreadyChecked(true);
            }
        }
    }
    
    protected abstract void validate();
}
