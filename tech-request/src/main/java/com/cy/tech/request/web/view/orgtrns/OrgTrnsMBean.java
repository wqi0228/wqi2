package com.cy.tech.request.web.view.orgtrns;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.tech.request.logic.service.orgtrns.OrgTrnsService;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsPageVO;
import com.cy.tech.request.web.pf.utils.DisplayController;
import com.cy.tech.request.web.pf.utils.MessagesUtils;
import com.cy.tech.request.web.view.orgtrns.components.OrgTrnsAlertInboxMBean;
import com.cy.tech.request.web.view.orgtrns.components.OrgTrnsCreateDepMBean;
import com.cy.tech.request.web.view.orgtrns.components.OrgTrnsNoticeDepMBean;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgTransMappingUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

/**
 * 組織異動轉檔功能 MBean
 */
@NoArgsConstructor
@Controller
@Scope("view")
@Slf4j
public class OrgTrnsMBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2766853096695200121L;
    // ========================================================================
    // 服務元件區
    // ========================================================================
    @Autowired
    private transient DisplayController displayController;
    @Autowired
    private transient OrgTrnsService orgTrnsService;
    @Autowired
    private transient OrgTrnsCreateDepMBean orgTrnsCreateDepMBean;
    @Autowired
    private transient OrgTrnsNoticeDepMBean orgTrnsNoticeDepMBean;
    @Autowired
    private transient OrgTrnsAlertInboxMBean orgTrnsAlertInboxMBean;

    // ========================================================================
    // 變數區
    // ========================================================================
    /**
     * 
     */
    @Getter
    @Setter
    private OrgTrnsPageVO pageVO;

    /**
     * 生效日下拉選項
     */
    @Getter
    private List<SelectItem> allEffectiveDateItems;

    @PostConstruct
    public void init() {
        // ====================================
        // 本頁VO初始化
        // ====================================
        this.pageVO = new OrgTrnsPageVO();
        // 準備生效日列表生效日
        this.prepareSelectItem();
        // 查詢設定資料中，最大的生效日
        if (WkStringUtils.notEmpty(this.allEffectiveDateItems)) {
            // 取日期最近的一筆
            this.pageVO.setQryEffectiveDate((Long) this.allEffectiveDateItems.get(0).getValue());
        }

        // ====================================
        // 查詢
        // ====================================
        this.btnSearchWorkVerify();
    }

    /**
     * 查詢轉換單位設定資料
     */
    public void btnSearchWorkVerify() {

        try {
            // ====================================
            // 查詢轉換單位設定資料
            // ====================================
            this.orgTrnsService.queryWorkVerify(this.pageVO, SecurityFacade.getCompanyId());

            // ====================================
            // 計算待轉筆數
            // ====================================
            this.prepareWaitTransCountData();

            // ====================================
            // 可閱單位待檢查資料
            // ====================================
            this.orgTrnsService.prepareCanViewCheckData(this.pageVO.getVerifyDtVOList(), SecurityFacade.getCompanySid());

            // ====================================
            // 畫面處理
            // ====================================
            // 清除 Filters
            this.displayController.clearFilter("screen_view_wv");
            // 顯示畫面格線
            this.displayController.execute("removeElmClass('spec_RemoveCss', 'ui-panelgrid')");

        } catch (Exception e) {
            String errMsg = "查詢時發生錯誤：" + e.getMessage();
            log.error(errMsg, e);
            MessagesUtils.showError(errMsg);
        }
    }

    /**
     * 計算待轉檔筆數
     */
    public void prepareWaitTransCountData() {
        try {
            Long startTime = System.currentTimeMillis();
            this.orgTrnsCreateDepMBean.executeCountAllWaitTrans(this.pageVO.getVerifyDtVOList());
            log.debug(WkCommonUtils.prepareCostMessage(startTime, "查詢待轉筆數:立案單位"));
            startTime = System.currentTimeMillis();
            this.orgTrnsNoticeDepMBean.executeCountAllWaitTrans(this.pageVO.getVerifyDtVOList());
            log.debug(WkCommonUtils.prepareCostMessage(startTime, "查詢待轉筆數:通知單位"));
            startTime = System.currentTimeMillis();
            this.orgTrnsAlertInboxMBean.executeCountAllWaitTrans(this.pageVO.getVerifyDtVOList());
            log.debug(WkCommonUtils.prepareCostMessage(startTime, "查詢待轉筆數:轉寄單位單位"));
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            String errorMessage = "計算筆數失敗!(" + e.getMessage() + ")";
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);
            return;
        }
    }


    /**
     * 生效日下拉選項
     */
    public void prepareSelectItem() {
        this.allEffectiveDateItems = Lists.newArrayList();

        // 查詢所有的生效日
        List<Date> allEffectiveDate = WkOrgTransMappingUtils.getInstance().findOrgTransMappingEffectiveDates(SecurityFacade.getCompanyId());
        if (WkStringUtils.isEmpty(allEffectiveDate)) {
            return;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        for (Date date : allEffectiveDate) {
            this.allEffectiveDateItems.add(new SelectItem(date.getTime(), sdf.format(date)));
        }
    }

}
