/**
 * 
 */
$(document).ready(function() {
	//初始化日期選擇元件
	if ($(".queryCondition_DateRange_dateBtn").length > 0) {
		init_queryCondition_DateRange();
	}
});

/**
 * 初始化查詢條件, 日期區間組件
 */
function init_queryCondition_DateRange() {
	// 動態載入 JS 檔案
	if (typeof moment != 'function') {
		$.getScriptCached("../js/moment.min.js", function() {
			PF_tabMenu_SetClientClickEffect('queryCondition_DateRange_dateBtn', queryCondition_DateRange);
		});
	} else {
		PF_tabMenu_SetClientClickEffect('queryCondition_DateRange_dateBtn', queryCondition_DateRange);
	}
}

function queryCondition_DateRange(index) {

	var startDate;
	var endDate;

	switch (index) {
	case "0":
		// 上個月
		var prevMonth = moment().subtract(1, 'months');
		startDate = prevMonth.startOf('month').toDate();
		endDate = prevMonth.endOf('month').toDate();
		break;
	case "1":
		// 本月份
		var thisMonth = moment();
		startDate = thisMonth.startOf('month').toDate();
		endDate = thisMonth.endOf('month').toDate();
		break;

	case "2":
		// 下個月
		var nextMonth = moment().add(1, 'months');
		startDate = nextMonth.startOf('month').toDate();
		endDate = nextMonth.endOf('month').toDate();
		break;

	case "3":
		// 今日
		startDate = new Date();
		endDate = new Date();
		break;

	case "4":
		// 清除
		startDate = null;
		endDate = null;
		break;

	default:
		console.log("選項未實做");
		break;
	}

	PF('startDateByType').setDate(startDate);
	PF('endDateByType').setDate(endDate);
}

// 切換全畫面時隱藏
function hideHeaderTitle() {
	updateDisplay('headerTitle', 'none');
	updateDisplay('dtRequire', 'none');
}
function showHeaderTitle() {
	updateDisplay('headerTitle', '');
	updateDisplay('dtRequire', '');
}
// 重載目前的報表
reSearch = function() {
	if ($('#dtRequire').length || $('#dtRequire1').length) {
		doSearchData();
	}
};
// 如為iframe格式則重新刷新MenuItem
function reMenuItemByReport() {
	if (checkIframe()) {
		parent.reMenuItemCount();
	}
}

var winMap = {};
// 開啟視窗(以防連續開啟)
function openWinByStore(key) {
	var tabWin = window.open('', '_blank');
	winMap[key] = tabWin;
}
// 替換URl(以防連續開啟)
function replaceUrlByStore(key, url) {
	var tabWin = winMap[key];
	tabWin.location = url;
	delete winMap[key];
}
// 選取dataTable row
function selectRow(obj, rowIndex) {
	if (checkWidgets(obj)) {
		PF(obj).unselectAllRows();
		PF(obj).selectRow(rowIndex, false);
	}
}
// 是否閱讀強制換成已閱讀
function changeAlreadyRead(dtId, indexRow) {
	if ($("#" + dtId + "_data").length > 0) {
		$("#" + dtId + "_data").children("tr").eq(indexRow).each(function() {
			$(this).find(".isReadClz").each(function() {
				$(this).html("已閱讀");
				$(this).text("已閱讀");
				if ($(this).hasClass("redColorAndBoldFont")) {
					$(this).removeClass("redColorAndBoldFont");
				}
			});
		});
	}
}
// 去除粗體Class
function removeClassByTextBold(dtId, indexRow) {
	if ($("#" + dtId + "_data").length > 0) {
		$("#" + dtId + "_data").children("tr").eq(indexRow).each(function() {
			if ($(this).hasClass("urgencyRowAndUnRead")) {
				$(this).removeClass("urgencyRowAndUnRead");
				$(this).addClass("urgencyRow")
			} else if ($(this).hasClass("unRead")) {
				$(this).removeClass("unRead");
			} else if ($(this).hasClass("textBold")) {
				$(this).removeClass("textBold");
			}
		});
	}
	if ($("#" + dtId + "1_data").length > 0) {
		$("#" + dtId + "1_data").children("tr").eq(indexRow).each(function() {
			if ($(this).hasClass("urgencyRowAndUnRead")) {
				$(this).removeClass("urgencyRowAndUnRead");
				$(this).addClass("urgencyRow")
			} else if ($(this).hasClass("unRead")) {
				$(this).removeClass("unRead");
			} else if ($(this).hasClass("textBold")) {
				$(this).removeClass("textBold");
			}
		});
	}
}
