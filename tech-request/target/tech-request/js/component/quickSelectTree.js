/**
 * 快選樹
 */
var QUICK_SELECT_TREE_TREE_CONTAINER_CLASS = "ui-tree-container";
var QUICK_SELECT_TREE_TREE_COMPONENT_ROOT_CLASS = "ui-tree";
var QUICK_SELECT_TREE_TREE_NODE_CLASS = "ui-treenode";
var QUICK_SELECT_TREE_TREE_TOGGLER_CLASS = "ui-tree-toggler";
var QUICK_SELECT_TREE_TREE_CONTENT_CLASS = "ui-treenode-content";
var QUICK_SELECT_TREE_TREE_NAME_CLASS = "ui-treenode-name";
var QUICK_SELECT_TREE_TREE_NODE_ROW_KEY_ATTR = "data-rowkey";
var QUICK_SELECT_TREE_TREE_NODE_SELECTED_ATTR = "quickselecttree_selected_hide";
var QUICK_SELECT_TREE_TREE_NODE_SEARCH_HIDE_ATTR = "quickselecttree_search_hide";
var QUICK_SELECT_TREE_IS_DATA_CLASS = "ui-treenode-is-data";
var QUICK_SELECT_TREE_EC_ICON_CLASS = "quick-select-tree-treeECIconContainer";

var QUICK_SELECT_TREE_DATALIST_CONTENT_CLASS = ".ui-datalist-content";
var QUICK_SELECT_TREE_DATALIST_DATA_CLASS = ".ui-datalist-data";
var QUICK_SELECT_TREE_DATALIST_ITEM_CLASS = ".ui-datalist-item";
var QUICK_SELECT_TREE_DATALIST_ITEM_NAME_CLASS = "quick-select-tree-selected-area-hover-highlight";
var QUICK_SELECT_TREE_DATALIST_ITEM_INDEX_ATTR = "quickselecttree_itemIndex";
var QUICK_SELECT_TREE_DOUBLE_CLICK_TIME = 400;
var QUICK_SELECT_TREE_TREE_FILTER_WAIT_TIME = 1000;
var QUICK_SELECT_TREE_IS_SHOW_DEBUG = false;
var QUICK_SELECT_TREE_SEARCH_KEYWORD_HIGHLIGHT_CLASS = "WS1-1-3";

/**
 * 快速選擇樹
 * 
 * @param componentID
 *            元件 id
 * @param treeWidget
 *            待選樹 primefaces widget
 * @param selectedAreaEmptyMessage
 * @param dataNodeIcon
 *            資料節點前置 icon class
 * @returns
 */
function QuickSelectTree(componentID, treeWidget, selectedAreaEmptyMessage, dataNodeIcon) {

	// 因外部元件 id 會被 primefaces 亂加前綴, 所以 id 都改為以 class 的方式傳入

	var self = this;
	this.componentID = componentID;
	var allNodeDataMapByRowKeyId = componentID + "_id_allNodeDataMapByRowKey";
	var selectedNodeDataId = componentID + "_id_selectedNodeData";
	// 連動資料
	var linkageMapId = componentID + "_id_linkageMap";

	var treeAreaId = componentID + "_id_tree";
	var treeFilterTextId = componentID + "_id_treeFilterText";
	var selectedNodeAreaId = componentID + "_id_selectedNodeArea";

	var selectedFilterTextId = componentID + "_id_selectedFilterText";

	// 已選擇樹
	var selectedTreeAreaId = componentID + "_id_selected_tree";
	var selectedTreeFilterTextId = componentID + "_id_selected_treeFilterText";

	// =================================
	// 待選樹
	// =================================
	// 待選樹區域
	this.treeArea = $("." + treeAreaId).find("." + QUICK_SELECT_TREE_TREE_COMPONENT_ROOT_CLASS);
	if (this.treeArea.length == 0) {
		console.log("錯誤,元件找不到(待選樹區域 treeArea):[" + treeAreaId + "]");
	}
	// 待選樹過濾文字區
	this.treeFilterText = $("." + treeFilterTextId);
	if (this.treeFilterText.length == 0) {
		console.log("錯誤,元件找不到(待選樹過濾文字區 treeFilterText):[" + treeFilterTextId + "]");
	}
	// 待選樹 Widget
	this.treeWidget = treeWidget;
	if (this.treeWidget == null) {
		console.log("錯誤,元件找不到(待選樹 Widget)");
	}
	// 待選樹 Container
	this.treeContainer = self.treeArea.find("." + QUICK_SELECT_TREE_TREE_CONTAINER_CLASS);
	// 待選樹開收合 ICON Container
	this.treeECIconContainer = $("." + componentID + "_id_TreeECIconContainer");
	if (this.treeECIconContainer.length == 0) {
		console.log("錯誤,元件找不到(待選樹開收合 ICON Container):[" + componentID + "_id_TreeECIconContainer" + "]");
	}

	// =================================
	// 已選樹
	// =================================
	// 已選樹區域
	this.selectedTreeArea = $("." + selectedTreeAreaId).find("." + QUICK_SELECT_TREE_TREE_COMPONENT_ROOT_CLASS);
	if (this.selectedTreeArea.length == 0) {
		console.log("錯誤,元件找不到(已選樹區域 selectedTreeArea):[" + selectedTreeAreaId + "]");
	}
	// 已選樹過濾文字區
	this.selectedTreeFilterText = $("." + selectedTreeFilterTextId);
	if (this.selectedTreeFilterText.length == 0) {
		console.log("錯誤,元件找不到(已選樹過濾文字區 selectedTreeFilterText):[" + selectedTreeFilterTextId + "]");
	}

	// 已選樹開收合 ICON Container
	this.selectedTreeECIconContainer = $("." + componentID + "_id_selected_TreeECIconContainer");
	if (this.selectedTreeECIconContainer.length == 0) {
		console.log("錯誤,元件找不到(待選樹開收合 ICON Container):[" + componentID + "selectedTreeECIconContainer" + "]");
	}

	// =================================
	// 已選列表清單
	// =================================
	// 已選擇物件區
	this.selectedNodeArea = $("." + selectedNodeAreaId).find(QUICK_SELECT_TREE_DATALIST_CONTENT_CLASS);
	if (this.selectedNodeArea.length == 0) {
		console.log("錯誤,元件找不到(已選擇物件區 selectedNodeArea):[" + selectedNodeAreaId + "]");
	}

	// 已選擇節點過濾輸入框
	this.selectedFilterText = $("." + selectedFilterTextId);
	if (this.selectedNodeArea.length == 0) {
		console.log("錯誤,元件找不到(已選擇物件過濾輸入框 selectedFilterText):[" + selectedFilterTextId + "]");
	}
	// 已選擇節點的資料區 (回存畫面選擇資料用)
	this.selectedNodeData = $("." + selectedNodeDataId);
	if (this.selectedNodeData.length == 0) {
		console.log("錯誤,元件找不到(已選擇物件的資料區 selectedNodeData):[" + selectedNodeDataId + "]");
	}

	// 所有節點資料儲存區 （JSON String）
	this.allNodeDataMapByRowKeyElm = $("." + allNodeDataMapByRowKeyId);
	if (this.allNodeDataMapByRowKeyElm.length == 0) {
		console.log("錯誤,元件找不到(所有節點資料儲存區 allNodeDataMapByRowKeyElm):[" + allNodeDataMapByRowKeyId + "]");
	}

	// =================================
	// 其他
	// =================================
	// 連動資料
	var linkAgeMapElm = $("." + linkageMapId);
	this.linkAgeMap = {};
	if (linkAgeMapElm.length == 0) {
		console.log("錯誤,找不到連動資料區");
	} else {
		this.linkAgeMap = JSON.parse(linkAgeMapElm.val());
	}

	// =================================
	// 
	// =================================
	// 所有節點資料 (Map[rowKey]) - 後端帶入
	this.allNodeDataMapByRowKey = [];
	// 所有節點資料 (Map[node.dataNode + node.sid])
	this.allNodeDataSidMap = {};
	// 已選擇節點資料 (List) - 後端帶入
	this.selectedNodeList = [];

	// 尚未選擇資料訊息文字
	this.selectedAreaEmptyMessage = selectedAreaEmptyMessage;

	this.dataNodeIcon = (!dataNodeIcon) ? "fa-asterisk" : dataNodeIcon;

	// 初始化
	this.init = function() {
		// ====================================
		// 標記資料節點
		// ====================================
		// 1.前方加入 icon
		// 2.注入標記 class (非顯示用途, 僅用於識別資料節點)
		self.treeArea.find(".quick-select-tree-data-node-highlight").find(".fa").addClass(self.dataNodeIcon);
		self.selectedTreeArea.find(".quick-select-tree-data-node-highlight").find(".fa").addClass(self.dataNodeIcon);
		// .addClass(QUICK_SELECT_TREE_IS_DATA_CLASS)

		// ====================================
		// 取得所有節點資料
		// ====================================
		if (self.allNodeDataMapByRowKeyElm.length == 0 || self.allNodeDataMapByRowKeyElm.text().isEmpty()) {
			console.log("取不到節點資料! allNodeDataMapByRowKeyId = [" + self.allNodeDataMapByRowKeyId + "]");
			console.log("取不到節點資料! allNodeDataMapByRowKeyElm = [" + self.allNodeDataMapByRowKeyElm.text() + "]");
			return;
		}

		// 解析資料字串 JSON to object
		self.allNodeDataMapByRowKey = JSON.parse(self.allNodeDataMapByRowKeyElm.text());
		// 轉換資料結構
		$.each(self.allNodeDataMapByRowKey, function(index, node) {
			self.allNodeDataSidMap[self.getNodeKey(node)] = node;
		});

		// 標註資料節點
		self.treeArea.find("." + QUICK_SELECT_TREE_TREE_NODE_CLASS).each(function() {
			var eachNode = $(this);
			var nodeData = self.allNodeDataMapByRowKey[eachNode.attr("data-rowkey")];
			if (nodeData != null && nodeData.dataNode) {
				eachNode.addClass(QUICK_SELECT_TREE_IS_DATA_CLASS);
			}
		});
		self.selectedTreeArea.find("." + QUICK_SELECT_TREE_TREE_NODE_CLASS).each(function() {
			var eachNode = $(this);
			var nodeData = self.allNodeDataMapByRowKey[eachNode.attr("data-rowkey")];
			if (nodeData != null && nodeData.dataNode) {
				eachNode.addClass(QUICK_SELECT_TREE_IS_DATA_CLASS);
			}
		});

		// ====================================
		// 將已選擇的項目從待選清單中隱藏
		// ====================================
		if (self.selectedNodeData.length == 0 || self.selectedNodeData.val().isEmpty()) {
			self.debug("quickSelectTree: 取不到已選擇節點資料! selectedNodeDataId = [" + selectedNodeDataId + "]");
		} else {
			// 取得已選擇 資料 (由後端提供)
			self.selectedNodeList = JSON.parse(self.selectedNodeData.val());

			// 將已選擇的 資料節點 從待選清單中隱藏
			for (var i = 0; i < self.selectedNodeList.length; i++) {
				var node = self.selectedNodeList[i];
				// 依據row key 從待選樹隱藏已選擇的資料
				var selector = "." + QUICK_SELECT_TREE_TREE_NODE_CLASS + "[" + QUICK_SELECT_TREE_TREE_NODE_ROW_KEY_ATTR + "='" + node.rowKey + "']";
				var treeNode = self.treeArea.find(selector);
				if (treeNode.length == 0) {
					// 排除已選 資料節點 為停用, 所以畫面上沒有
					if (!(node.rowKey).startsWith("_")) {
						self.debug("畫面上找不到已選資料的節點 rowKey:[" + node.rowKey + "]");
					}
				} else {
					// 隱藏節點 (因被選擇而隱藏)
					self.hideTreeNode(treeNode, QUICK_SELECT_TREE_TREE_NODE_SELECTED_ATTR);
				}
			}
		}
		// ====================================
		// 標記已選清單樹(selectedTreeArea)顯示狀態
		// ====================================
		var selectedNodeRowKeys = [];
		for (var i = 0; i < self.selectedNodeList.length; i++) {
			selectedNodeRowKeys.push(self.selectedNodeList[i].rowKey + "");
		}

		// 撈出已選樹中所有的節點
		var allTreeNode = self.selectedTreeArea.find("." + QUICK_SELECT_TREE_TREE_NODE_CLASS);

		// 標註 節點 QUICK_SELECT_TREE_TREE_NODE_SELECTED_ATTR 屬性 (顯示為N, 隱藏為 Y)
		allTreeNode.each(function() {
			var treeNode = $(this);
			var noderowKey = treeNode.attr(QUICK_SELECT_TREE_TREE_NODE_ROW_KEY_ATTR);
			var isSelectedNode = selectedNodeRowKeys.includes(noderowKey);

			// 標註節點隱藏屬性
			treeNode.attr(QUICK_SELECT_TREE_TREE_NODE_SELECTED_ATTR, isSelectedNode ? "N" : "Y")
			// 有被選擇節點時, 展開上層
			if (isSelectedNode) {
				self.expandTreeNodeParentUntilRoot(treeNode);
			}
		});

		// ====================================
		// 建立 tree 顯示訊息區
		// ====================================
		var emptyMessageHtmlContent = "<div class='ui-tree-message WS1-1-2'>無符合搜尋結果</div>";
		self.waitSelectTreeEmptyMessage = $(emptyMessageHtmlContent);
		self.waitSelectTreeEmptyMessage.hide();
		self.treeArea.append(self.waitSelectTreeEmptyMessage);

		self.selectedTreeEmptyMessage = $(emptyMessageHtmlContent);
		self.selectedTreeEmptyMessage.hide();
		self.selectedTreeArea.append(self.selectedTreeEmptyMessage);

		// ====================================
		// 重置待選清單樹顯示狀態
		// ====================================
		self.resetTreeNodeDisplayStatus();

		// ====================================
		// 重建已選擇顯示區
		// ====================================
		self.rebuildselectedNodeArea();

		// ====================================
		// 重置已選清單樹顯示狀態
		// ====================================
		self.resetSelectedTreeNodeDisplayStatus();
	}

	this.getNodeKey = function(node) {
		return node.dataNode + "" + node.sid;
	}

	this.getShowNodeSelector = function() {
		// 組選取條件
		var selector = "";
		// 限定為 node 層
		selector += "." + QUICK_SELECT_TREE_IS_DATA_CLASS;
		// 排除已選擇隱藏
		selector += "[" + QUICK_SELECT_TREE_TREE_NODE_SELECTED_ATTR + " != 'Y']";
		// 排除關鍵字過濾隱藏
		selector += "[" + QUICK_SELECT_TREE_TREE_NODE_SEARCH_HIDE_ATTR + " != 'Y']";

		return selector;
	}
	// 取得樹節點選擇器
	this.getTreeNodeSelectorByRowKey = function(rowkey) {
		var selector = "." + QUICK_SELECT_TREE_TREE_NODE_CLASS;
		selector += "[" + QUICK_SELECT_TREE_TREE_NODE_ROW_KEY_ATTR + "='" + rowkey + "']";
		return selector;
	}

	this.getTargetTreeAreaByType = function(areaType) {
		return (areaType == "A") ? self.treeArea : self.selectedTreeArea;
	}

	// ====================================
	// 重置tree node 的顯示狀態
	// ====================================
	this.resetTreeNodeDisplayStatus = function() {

		var hasDisplayNode = self.resetTreeNodeDisplayStatus_core(self.treeArea);

		if (hasDisplayNode) {
			// 有顯示節點時顯示待選樹和展開收合 icon
			self.treeContainer.show();
			self.treeECIconContainer.show();
			self.waitSelectTreeEmptyMessage.hide();
		} else {
			// 無顯示節點時顯示訊息
			self.treeContainer.hide();
			self.treeECIconContainer.hide();
			self.showTreeEmptyMessage("A");
		}
	}

	this.resetSelectedTreeNodeDisplayStatus = function() {
		var hasDisplayNode = self.resetTreeNodeDisplayStatus_core(self.selectedTreeArea);

		if (hasDisplayNode) {
			// 預設展開所有節點
			self.expandAllTreeNode("B");
			self.selectedTreeECIconContainer.show();
			self.selectedTreeEmptyMessage.hide();
		} else {
			self.selectedTreeECIconContainer.hide();
			self.showTreeEmptyMessage("B");
		}

		//初次處理完畢後, 顯示預設隱藏的樹
		$("." + treeAreaId).find(".quick-select-tree").show();
		$("." + selectedTreeAreaId).find(".quick-select-tree").show();
	}

	this.showTreeEmptyMessage = function(treeAreaType) {
		var targetMessageArea = ("A" == treeAreaType) ? self.waitSelectTreeEmptyMessage : self.selectedTreeEmptyMessage;
		var targetTreeArea = self.getTargetTreeAreaByType(treeAreaType);

		// 取得所有的資料節點
		var dataNodeList = targetTreeArea.find("." + QUICK_SELECT_TREE_TREE_NODE_CLASS + " ." + QUICK_SELECT_TREE_IS_DATA_CLASS);

		var hasDataNode = false;
		dataNodeList.each(function() {
			var eachNode = $(this);
			if (eachNode.attr(QUICK_SELECT_TREE_TREE_NODE_SELECTED_ATTR) != "Y") {
				hasDataNode = true;
				return false;
			}
		});

		targetMessageArea.text(hasDataNode ? "無符合搜尋結果，請清除搜尋關鍵字" : self.selectedAreaEmptyMessage);
		if (hasDataNode) {
			targetMessageArea.addClass("WS1-1-2");
			targetMessageArea.removeClass("WS1-1-1");
		} else {
			targetMessageArea.addClass("WS1-1-1");
			targetMessageArea.removeClass("WS1-1-2");

		}
		targetMessageArea.show();
	}

	this.resetTreeNodeDisplayStatus_core = function(targetTreeArea) {

		// 取得所有的 treeNode
		var treeNodeList = targetTreeArea.find("." + QUICK_SELECT_TREE_TREE_NODE_CLASS);

		// 若父節點下無可顯示的節點, 則隱藏父節點
		var hasDisplayNode = false;
		treeNodeList.each(function() {
			var eachNode = $(this);
			var isDataNode = eachNode.hasClass(QUICK_SELECT_TREE_IS_DATA_CLASS);
			//本節點需被隱藏(已被選擇（永遠不該出現）或因非搜尋結果而需要被隱藏)
			var isNeedHide = (eachNode.attr(QUICK_SELECT_TREE_TREE_NODE_SELECTED_ATTR) == "Y" || eachNode.attr(QUICK_SELECT_TREE_TREE_NODE_SEARCH_HIDE_ATTR) == "Y");

			if (isDataNode && isNeedHide) {
				// 判斷本節點需被隱藏
				// 1.已被選取標記
				// 2.關鍵字過濾標記
				eachNode.hide();

			} else if (!isDataNode && eachNode.find(self.getShowNodeSelector()).length == 0) {
				// 1.非資料節點(父節點)
				// 3.子項目沒有一項需顯示
				eachNode.hide();

			} else {
				eachNode.show();
				hasDisplayNode = true;
			}
		});
		return hasDisplayNode;
	}

	// ====================================
	// 模擬判斷是否 double click
	// ====================================
	// 前次擊點節點
	var prevClickRowkey = "";
	// 前次擊點時間
	var prevClickTime = 0;
	// 模擬判斷是否 double click
	this.isNodeDoubleClick = function(rowkey) {
		// 取得目前時間
		var nowTime = Date.now();
		// 比較上次擊點時間
		var intervalTime = nowTime - prevClickTime;
		// 判斷須為上次擊點的 node , 且間隔時間不大於定值
		var result = (rowkey == prevClickRowkey && intervalTime <= QUICK_SELECT_TREE_DOUBLE_CLICK_TIME);

		// 更新本次擊點狀態
		prevClickRowkey = rowkey;
		prevClickTime = nowTime;

		return result;
	}

	// ====================================
	// 過濾已經選擇區域的資料
	// ====================================
	this.filterSelectedList = function(keyword) {
		keyword = (keyword + "").trim();
		// 取得顯示列表
		var itemList = self.selectedNodeArea.find(QUICK_SELECT_TREE_DATALIST_ITEM_CLASS);
		if (itemList.length == 0) {
			self.debug("畫面尚無可選擇資料");
			return;
		}

		// 清除前次Highlight結果
		self.removeHighlight(self.selectedNodeArea);

		if (keyword.isEmpty()) {
			itemList.each(function() {
				$(this).show();
			});
		} else {
			var compareKeyword = keyword.toUpperCase();
			itemList.each(function() {
				//取得項目名稱區塊
				var itemNameElm = $(this).children("." + QUICK_SELECT_TREE_DATALIST_ITEM_NAME_CLASS);
				if (itemNameElm.length == 0) {
					return;
				}

				//若有被處理(美化)過, 取得真實項目名稱區塊
				var styleItemNameElm = itemNameElm.find(".STYLE_CONTENT_TRUE_DATA");

				if (styleItemNameElm.length > 0) {
					//最深的一個
					itemNameElm = $(styleItemNameElm[styleItemNameElm.length - 1]);
				}
				// 忽略大小寫差異
				var context = (itemNameElm.text() + "").trim().toUpperCase();
				// 比對關鍵字
				if (context.includes(compareKeyword)) {
					//
					var highlightContent = self.highlightKeyWord(itemNameElm.text(), keyword);
					itemNameElm.html(highlightContent);

					$(this).show();
				} else {
					$(this).hide();
				}
			});
		}
	}
	// ====================================
	// tree Node 展開、收合
	// ====================================
	// 節點展開
	this.expandTreeNode = function(targetNode) {
		if (self.isTreeNodeMustDoExpandOrCollapse(targetNode, true)) {
			self.treeWidget.expandNode(targetNode);
		}
	}
	// 節點收合
	this.collapseTreeNode = function(targetNode) {
		if (self.isTreeNodeMustDoExpandOrCollapse(targetNode, false)) {
			self.treeWidget.collapseNode(targetNode);
		}
	}
	// 為可開合的節點
	this.isTreeNodeMustDoExpandOrCollapse = function(targetNode, isDoExpand) {
		var treeToggler = self.getTreeNodeToggler(targetNode);
		if (treeToggler == null) {
			return false;
		}
		var isInExpand = treeToggler.hasClass("ui-icon-triangle-1-s");
		// 要做展開, 但已經展開了
		if (isDoExpand && isInExpand) {
			return false;
		}
		// 要做收合, 但已經收合了
		if (!isDoExpand && !isInExpand) {
			return false;
		}
		return true;
	}
	// 展開樹節點的上層, 直到頂端
	this.expandTreeNodeParentUntilRoot = function(targetNode) {
		// 取得父節點
		var parentNode = self.getParentNode(targetNode);
		// 無則終止
		if (parentNode == null) {
			return;
		}
		// 遞迴呼叫展開上層
		self.expandTreeNodeParentUntilRoot(parentNode);
		// 展開父節點
		self.expandTreeNode(parentNode);
	}
	// 取得收合elm
	this.getTreeNodeToggler = function(targetNode) {
		if (targetNode == null) {
			return null;
		}
		var togglerElm = targetNode.children("." + QUICK_SELECT_TREE_TREE_CONTENT_CLASS).children("." + QUICK_SELECT_TREE_TREE_TOGGLER_CLASS);
		if (togglerElm.length != 0) {
			return togglerElm;
		}
		return null;
	}

	// ====================================
	// 待選樹全部展開、收合
	// ====================================
	this.expandAllTreeNode = function(treeAreaType) {
		// 依據類型取得要處理的樹
		var targetTreeArea = self.getTargetTreeAreaByType(treeAreaType);

		// 取得所有節點
		var treeNodeList = targetTreeArea.find("." + QUICK_SELECT_TREE_TREE_NODE_CLASS);
		treeNodeList.each(function() {
			self.expandTreeNode($(this));
		});
	}
	this.collapseAllTreeNode = function(treeAreaType) {
		// 依據類型取得要處理的樹
		var targetTreeArea = self.getTargetTreeAreaByType(treeAreaType);
		// 取得所有節點
		var treeNodeList = targetTreeArea.find("." + QUICK_SELECT_TREE_TREE_NODE_CLASS);
		treeNodeList.each(function() {
			self.collapseTreeNode($(this));
		});
	}
	// ====================================
	// 樹狀清單搜尋
	// ====================================
	this.isRunTreeNodeFilter = false;
	this.isNewFilterRequest = false;
	this.treeNodeFilter = function(treeAreaType) {
		// 延遲控制 （避免快速輸入時, 執行很多次）
		if (self.isRunTreeNodeFilter) {
			self.isNewFilterRequest = true;
			return;
		}
		self.isRunTreeNodeFilter = true;
		setTimeout(function() {
			// try {
			self.isNewFilterRequest = false;
			var nowTime = Date.now();
			var keyWord = self.treeFilterText.val();

			// 重置清單樹顯示狀態
			if ("A" == treeAreaType) {
				keyWord = self.treeFilterText.val();
			} else {
				keyWord = self.selectedTreeFilterText.val();
			}

			self.treeNodeFilter_Run(keyWord, treeAreaType);

			self.debug("treeNodeFilter [" + keyWord + "] cost time:" + (Date.now() - nowTime));

			self.isRunTreeNodeFilter = false;
			if (self.isNewFilterRequest) {
				self.isNewFilterRequest = false;
				self.treeNodeFilter(0);
			}
		}, QUICK_SELECT_TREE_TREE_FILTER_WAIT_TIME);
	}
	this.treeNodeFilter_Run = function(filterKeyword, treeAreaType) {

		// 依據類型取得要處理的樹
		var targetTreeArea = self.getTargetTreeAreaByType(treeAreaType);

		// 取得本次輸入文字
		filterKeyword = (filterKeyword + "").toUpperCase().trim();
		// 取得所有的 treeNode
		var treeNodeList = targetTreeArea.find("." + QUICK_SELECT_TREE_TREE_NODE_CLASS);

		if (treeNodeList.length == 0) {
			return;
		}
		// 清除前次Highlight結果
		self.removeHighlight(targetTreeArea);
		// 初始化前次搜尋結果
		$(treeNodeList).attr(QUICK_SELECT_TREE_TREE_NODE_SEARCH_HIDE_ATTR, "N");

		if (!filterKeyword.isEmpty()) {
			// disable 節點展開動畫 (避免處理過程過慢)
			self.treeWidget.cfg.animate = false;

			this.treeNodeFilter_B(treeNodeList, filterKeyword);

			// enable 節點展開動畫
			self.treeWidget.cfg.animate = true;
		}

		// 重置清單樹顯示狀態
		if ("A" == treeAreaType) {
			self.resetTreeNodeDisplayStatus();
		} else {
			self.resetSelectedTreeNodeDisplayStatus();
		}
	}
	this.treeNodeFilter_A = function(treeNodeList, filterKeyword) {
		// 逐筆處理
		treeNodeList.each(function() {

			var eachNode = $(this);
			var eachNodeName = self.getTreeNodeName(eachNode);

			// 本節點名稱包含關鍵字
			if (eachNodeName.includes(filterKeyword)) {
				// 標亮node 中的關鍵字
				self.highlightKeyWordForNode(eachNode, filterKeyword);
				// 註記此節點不需被隱藏
				eachNode.attr(QUICK_SELECT_TREE_TREE_NODE_SEARCH_HIDE_ATTR, "N");
				// 展開上層節點
				self.expandTreeNodeParentUntilRoot(eachNode);
				return;
			}

			//雖然沒有關鍵字,但上層有關鍵字, 則此節點需保留, 但不自動展開
			if (self.isParentNodeHasKeyword(eachNode, filterKeyword)) {
				// 註記此節點不需被隱藏
				eachNode.attr(QUICK_SELECT_TREE_TREE_NODE_SEARCH_HIDE_ATTR, "N");
				return;
			}

			//未通過判斷者隱藏
			eachNode.attr(QUICK_SELECT_TREE_TREE_NODE_SEARCH_HIDE_ATTR, "Y");

		});
	}

	this.treeNodeFilter_B = function(treeNodeList, filterKeyword) {
		// 逐筆處理
		treeNodeList.each(function() {
			var eachNode = $(this);
			var attrVal = "Y"
			var eachNodeName = self.getTreeNodeName(eachNode);
			var isExpand = false;

			if (eachNodeName.includes(filterKeyword)) {
				// 本節點名稱包含關鍵字
				attrVal = "N";
				isExpand = true;
				// 標亮node 中的關鍵字
				self.highlightKeyWordForNode(eachNode, filterKeyword);

			} else if (self.isDirectNodeHasKeyWord(eachNode, filterKeyword)) {
				// 本節點直系 node 包含關鍵字
				attrVal = "N";
			}

			// 設定是否因搜尋而隱藏標記
			eachNode.attr(QUICK_SELECT_TREE_TREE_NODE_SEARCH_HIDE_ATTR, attrVal);

			// 需展開
			if (isExpand) {
				self.expandTreeNodeParentUntilRoot(eachNode);
			} else {
				// self.collapseTreeNode(eachNode);
			}
		});
	}

	// 判斷直系節點是否含有關鍵字
	this.isDirectNodeHasKeyWord = function(targetNode, filterKeyword) {
		if (targetNode == null) {
			return false;
		}

		// 判斷所有子節點
		var childNodeList = targetNode.find("." + QUICK_SELECT_TREE_TREE_NODE_CLASS);
		for (var i = 0; i < childNodeList.length; i++) {
			var eachChildNode = $(childNodeList[i]);
			// 排除已選擇
			if ("Y" == eachChildNode.attr(QUICK_SELECT_TREE_TREE_NODE_SELECTED_ATTR)) {
				continue;
			}
			// 取得節點名稱
			var eachChildNodeName = self.getTreeNodeName(eachChildNode);
			if (eachChildNodeName.includes(filterKeyword)) {
				return true;
			}
		}
		// 判斷直系往上父節點
		return self.isParentNodeHasKeyword(targetNode, filterKeyword);

	}
	// 判斷node往上(含自己)是否有關鍵字
	this.isParentNodeHasKeyword = function(targetNode, filterKeyword) {

		// 往上判斷父節點
		// 取得父節點
		var parentNode = self.getParentNode(targetNode);
		if (parentNode == null) {
			return false;
		}

		// 取得節點名稱
		var parentNodeName = self.getTreeNodeName(parentNode);
		// 判斷節點名稱
		if (parentNodeName.includes(filterKeyword)) {
			return true;
		}

		// 遞迴判斷父節點
		return self.isParentNodeHasKeyword(parentNode, filterKeyword);
	}

	// 取得節點名稱
	this.getTreeNodeName = function(targetNode, isKeepCase) {
		if (targetNode == null) {
			return "";
		}
		var nodeContentElm = targetNode.children("." + QUICK_SELECT_TREE_TREE_CONTENT_CLASS).find("." + QUICK_SELECT_TREE_TREE_NAME_CLASS);
		if (nodeContentElm.length == 0) {
			return "";
		}

		//被風格化的資料, 需再從其中取出正確資料段
		var styleNodeContentElm = nodeContentElm.find(".STYLE_CONTENT_TRUE_DATA");
		if (styleNodeContentElm.length > 0) {
			nodeContentElm = styleNodeContentElm;
		}

		if (isKeepCase) {
			return (nodeContentElm.text() + "").trim()
		}

		return (nodeContentElm.text() + "").toUpperCase().trim();
	}
	this.setTreeNodeName = function(targetNode, newContent) {
		if (targetNode == null) {
			return "";
		}
		var nodeContentElm = targetNode.children("." + QUICK_SELECT_TREE_TREE_CONTENT_CLASS).find("." + QUICK_SELECT_TREE_TREE_NAME_CLASS);
		if (nodeContentElm.length == 0) {
			return "";
		}
		nodeContentElm.html(newContent);
	}
	// ====================================
	// 關鍵字搜尋 Highlight
	// ====================================
	this.removeHighlight = function(areaElm) {
		areaElm.find("." + QUICK_SELECT_TREE_SEARCH_KEYWORD_HIGHLIGHT_CLASS).each(function() {
			var parent = $(this).parent();
			if (parent.length == 0) {
				return;
			}
			parent.html(parent.text());
		});
	}
	this.highlightKeyWord = function(displayName, queryKeyword) {

		var displayName = (displayName + "").trim();
		queryKeyword = (queryKeyword + "").trim();
		if (queryKeyword.isEmpty()) {
			return displayName;
		}
		// 相等時直接標亮全部
		if (displayName == queryKeyword) {
			return self.highlightStr(displayName);
		}

		var displayNameForCompare = displayName.toUpperCase();
		var queryKeywordForCompare = queryKeyword.toUpperCase();

		var highlightContext = "";
		var queryKeywordLength = queryKeyword.length;

		// 逐字比對
		for (var i = 0; i < displayName.length; i++) {
			var currContext = displayNameForCompare.substr(i);
			if (currContext.startsWith(queryKeywordForCompare)) {
				highlightContext += self.highlightStr(displayName.substr(i, queryKeywordLength));
				i += queryKeywordLength - 1;
			} else {
				highlightContext += displayName.substr(i, 1);
			}
		}
		return highlightContext;
	}

	this.highlightKeyWordForNode = function(targetNode, queryKeyword) {

		if (targetNode == null) {
			return "";
		}
		var nodeContentElm = targetNode.children("." + QUICK_SELECT_TREE_TREE_CONTENT_CLASS).find("." + QUICK_SELECT_TREE_TREE_NAME_CLASS);
		if (nodeContentElm.length == 0) {
			return "";
		}

		//被風格化的資料, 需再從其中取出正確資料段
		var styleNodeContentElm = nodeContentElm.find(".STYLE_CONTENT_TRUE_DATA");
		if (styleNodeContentElm.length > 0) {
			nodeContentElm = styleNodeContentElm;
		}

		var highlightContext = self.highlightKeyWord(nodeContentElm.text(), queryKeyword);

		if (styleNodeContentElm.length > 0) {
			styleNodeContentElm.html(highlightContext);
		} else {
			nodeContentElm.html(highlightContext);
		}
	}
	this.highlightStr = function(str) {
		return "<span class='" + QUICK_SELECT_TREE_SEARCH_KEYWORD_HIGHLIGHT_CLASS + "'>" + str + "</span>";
	}

	// ====================================
	// click 待選清單樹節點
	// ====================================
	this.onTreeNodeClick = function(targetNode, event) {

		try {

			// 取得選擇節點的 row key
			var rowkey = $(targetNode).attr(QUICK_SELECT_TREE_TREE_NODE_ROW_KEY_ATTR);
			if (rowkey.isEmpty()) {
				return;
			}

			// 判斷是否為雙擊
			if (!this.isNodeDoubleClick("treeNode_" + rowkey)) {
				return;
			}

			// 取得選擇節點的 資料
			var targetNodeData = self.allNodeDataMapByRowKey[rowkey];
			if (targetNodeData == null) {
				return;
			}

			// 為資料節點
			if (targetNodeData.dataNode) {
				// 加入已選擇資料
				this.addSelectedData(rowkey, true);
			}

			// 若選擇為 資料夾 節點, 加入下面所有未選擇的資料節點資料
			if (!targetNodeData.dataNode) {
				// 取得下層所有節點
				var childNodeList = $(targetNode).find("." + QUICK_SELECT_TREE_TREE_NODE_CLASS);
				var hasAddDataNode = false;

				for (var i = 0; i < childNodeList.length; i++) {
					var childNode = $(childNodeList[i]);

					// 取得row key
					var childRowkey = $(childNode).attr(QUICK_SELECT_TREE_TREE_NODE_ROW_KEY_ATTR);
					if (childRowkey.isEmpty()) {
						console.log("childRowkey is empty");
						continue;
					}
					// 新增資料到已選清單
					if (this.addSelectedData(childRowkey, true)) {
						hasAddDataNode = true;
					}
				}

				// 若節點下沒有可以新增的資料節點 , 則節點不消失
				if (!hasAddDataNode) {
					alert("沒有可以新增的項目");
				}
			}

			// 重建已選 資料 清單區
			self.rebuildselectedNodeArea();

			// 重置待選清單樹顯示狀態
			self.resetTreeNodeDisplayStatus();

			// 重置已選清單樹顯示狀態
			self.resetSelectedTreeNodeDisplayStatus();

		} catch (e) {
			console.log("選擇 node 處理失敗:" + e)
			console.log(e.stack);
		}
	}

	// ====================================
	// click 已選清單樹節點
	// ====================================
	this.onSelectedTreeNodeClick = function(targetNode, event) {

		try {

			// 取得選擇節點的 row key
			var rowkey = $(targetNode).attr(QUICK_SELECT_TREE_TREE_NODE_ROW_KEY_ATTR);
			if (rowkey.isEmpty()) {
				return;
			}

			// 判斷是否為雙擊
			if (!this.isNodeDoubleClick("selectTreeNode_" + rowkey)) {
				return;
			}

			// 取得選擇節點的 資料
			var targetNodeData = self.allNodeDataMapByRowKey[rowkey];
			if (targetNodeData == null) {
				return;
			}

			// 為資料節點
			if (targetNodeData.dataNode) {
				this.removeSelectedData(rowkey);
			}

			// 若選擇為 部門 節點, 加入下面所有未選擇的資料節點資料
			if (!targetNodeData.dataNode) {
				// 取得下層所有節點
				var childNodeList = $(targetNode).find("." + QUICK_SELECT_TREE_TREE_NODE_CLASS);

				for (var i = 0; i < childNodeList.length; i++) {
					var childNode = $(childNodeList[i]);

					// 取得row key
					var childRowkey = $(childNode).attr(QUICK_SELECT_TREE_TREE_NODE_ROW_KEY_ATTR);
					if (childRowkey.isEmpty()) {
						console.log("childRowkey is empty");
						continue;
					}
					this.removeSelectedData(childRowkey);
				}
			}

			// 重建已選 資料 清單區
			self.rebuildselectedNodeArea();

			// 重置待選清單樹顯示狀態
			self.resetTreeNodeDisplayStatus();

			// 重置已選清單樹顯示狀態
			self.resetSelectedTreeNodeDisplayStatus();

		} catch (e) {
			console.log("選擇 node 處理失敗:" + e)
			console.log(e.stack);
		}
	}

	// ====================================
	// 移除已選擇資料
	// ====================================
	this.removeSelectedData = function(rowkey, isInactive, sid) {

		// 從已選擇人員列表移除停用者時的特別處理
		if (isInactive) {
			for (var i = 0; i < self.selectedNodeList.length; i++) {
				if (self.selectedNodeList[i].sid == sid) {
					self.selectedNodeList.splice(i, 1);
				}
			}
			return;
		}

		// 取得選擇節點的 資料
		var targetNodeData = self.allNodeDataMapByRowKey[rowkey];
		if (targetNodeData == null) {
			console.log("removeSelectedData -> 找不到選擇節點資料 ")
			return;
		}

		// --------------------------
		// 從已選擇列表資料中移除
		// --------------------------
		for (var i = 0; i < self.selectedNodeList.length; i++) {
			if (self.selectedNodeList[i].sid == targetNodeData.sid) {
				self.selectedNodeList.splice(i, 1);
			}
		}

		// --------------------------
		// 處理待選清單樹
		// --------------------------
		// 顯示待選樹節點
		var waitSelectTreeNode = self.treeArea.find(self.getTreeNodeSelectorByRowKey(rowkey));
		if (waitSelectTreeNode.length != 0) {
			// 註記顯示
			waitSelectTreeNode.attr(QUICK_SELECT_TREE_TREE_NODE_SELECTED_ATTR, "N");
		}

		// --------------------------
		// 處理已選清單樹
		// --------------------------
		// 隱藏已選樹節點
		var selectedTreeNode = self.selectedTreeArea.find(self.getTreeNodeSelectorByRowKey(rowkey));
		if (selectedTreeNode.length != 0) {
			// 註記隱藏
			selectedTreeNode.attr(QUICK_SELECT_TREE_TREE_NODE_SELECTED_ATTR, "Y");
		}
	}

	// ====================================
	// 將 資料節點 加入已選清單
	// ====================================
	this.addSelectedData = function(rowkey, isLinkage) {
		// 判斷無資料
		if (!(rowkey in self.allNodeDataMapByRowKey)) {
			return false;
		}

		// 要新增的節點資料
		var addNodeData = self.allNodeDataMapByRowKey[rowkey];

		// 非 資料節點 則略過
		if (!addNodeData.dataNode) {
			return false;
		}

		// --------------------------
		// 加入已選擇列表資料
		// --------------------------
		// 判斷是否已存在
		for (var i = 0; i < self.selectedNodeList.length; i++) {
			if (self.selectedNodeList[i].sid == addNodeData.sid) {
				// 跳出前處理連動
				self.addLinkAge(addNodeData, isLinkage);
				// 此節點已存在, 略過
				return false;
			}
		}

		// 加入
		self.selectedNodeList.push(addNodeData);
		// 排序 - 以樹狀清單順序
		self.selectedNodeList.sort(function(a, b) {
			if (a.rowKey > b.rowKey)
				return 1;
			if (a.rowKey < b.rowKey)
				return -1;
			return 0;
		});

		// --------------------------
		// 處理待選清單樹
		// --------------------------
		// 隱藏待選樹節點
		var waitSelectTreeNode = self.treeArea.find(self.getTreeNodeSelectorByRowKey(rowkey));
		if (waitSelectTreeNode.length != 0) {
			// 註記隱藏
			waitSelectTreeNode.attr(QUICK_SELECT_TREE_TREE_NODE_SELECTED_ATTR, "Y");
		}

		// --------------------------
		// 處理已選清單樹
		// --------------------------
		// 顯示已選樹節點
		var selectedTreeNode = self.selectedTreeArea.find(self.getTreeNodeSelectorByRowKey(rowkey));
		if (selectedTreeNode.length != 0) {
			// 註記顯示
			selectedTreeNode.attr(QUICK_SELECT_TREE_TREE_NODE_SELECTED_ATTR, "N");
		}

		// --------------------------
		// 連動
		// --------------------------
		self.addLinkAge(addNodeData, isLinkage);

		return true;
	}

	// 連動
	this.addLinkAge = function(nodeData, isLinkage) {
		if (!isLinkage) {
			return;
		}

		var linkageNodeRowkeys = self.getLinkageRowkeys(nodeData.rowKey);
		for (var i = 0; i < linkageNodeRowkeys.length; i++) {
			try {
				// 被連動節點 rowKey
				var linkageNodeRowkey = linkageNodeRowkeys[i];
				// 被連動節點資料
				var linkageNode = self.allNodeDataMapByRowKey[linkageNodeRowkey];
				// 連動之後再連動 (被連動節點若有連動是設定時，一併取出)
				var isCombolinkage = self.isCombolinkage(linkageNode);
				self.debug("連動：" + nodeData.ouName + "->" + linkageNode.ouName + "," + isCombolinkage);
				self.addSelectedData(linkageNodeRowkey, isCombolinkage);
			} catch (e) {
				console.log("處理連動設定失敗");
				console.log(e);
			}
		}

	}

	//『被連動節點』是否開啟再連動
	// 說明:組織節點僅往上連動一層
	this.isCombolinkage = function(nodeData) {
		var nodeOtherInfo = nodeData.otherInfo;
		if (commonUtil.isNull(nodeOtherInfo)) {
			return true;
		}

		if (commonUtil.isNull(nodeOtherInfo['INFO_KEY_NOT_COMBO_LINKAGE'])) {
			return false;
		}

		return true;
	}

	// ====================================
	// 重建已選清單區 , 且更新回傳後端資料區
	// ====================================
	this.rebuildselectedNodeArea = function() {

		// 更新回傳後端資料區
		self.selectedNodeData.val(JSON.stringify(self.selectedNodeList));

		// 清空區域
		self.selectedNodeArea.html("");

		// 無任何一筆資料時
		if (self.selectedNodeList == null || self.selectedNodeList.length == 0) {
			// 顯示訊息
			self.selectedNodeArea.append("<div class='ui-datalist-empty-message'>" + self.selectedAreaEmptyMessage + "</div>");
			// 過濾框控制
			self.selectedFilterText.attr("placeholder", "請先選擇一筆資料");
			self.selectedFilterText.prop('disabled', true);
			return;
		}

		var dataListDataElm = $("<ol id='" + selectedNodeAreaId + "_list' class='" + QUICK_SELECT_TREE_DATALIST_DATA_CLASS + "'>");

		// 逐筆新增
		for (var i = 0; i < self.selectedNodeList.length; i++) {
			var dataNode = self.selectedNodeList[i];
			// dataListItem
			var dataListItemElm = $("<li class='ui-datalist-item'></li>");
			// 停用標記
			// self.debug("dataNode.avtive：" + dataNode.avtive);
			if (!dataNode.active) {
				dataListItemElm.append("<span class='WS1-1-2'>(停用) - </span>");
			}
			// 節點名稱
			var nodeNameElm = $("<label class='" + QUICK_SELECT_TREE_DATALIST_ITEM_NAME_CLASS + "' " + QUICK_SELECT_TREE_DATALIST_ITEM_INDEX_ATTR + "=" + i + ">" + dataNode.ouName + "</label>");
			// 綁定 click 事件
			nodeNameElm.on("click", function() {
				self.onSelectedAreaRowClick($(this).attr(QUICK_SELECT_TREE_DATALIST_ITEM_INDEX_ATTR));
			})
			dataListItemElm.append(nodeNameElm);
			dataListItemElm.append("<hr style='border-style: ridge'/>");
			dataListDataElm.append(dataListItemElm);
		}

		self.selectedNodeArea.append(dataListDataElm);

		// 過濾框控制
		self.selectedFilterText.attr("placeholder", "Search");
		self.selectedFilterText.prop('disabled', false);

		//若有關鍵字時, 重新過濾
		if (!self.selectedFilterText.val().isEmpty()) {
			this.filterSelectedList(self.selectedFilterText.val());
		}
	}

	// ====================================
	// 檢選已選擇清單 （移除 資料節點）
	// ====================================
	this.onSelectedAreaRowClick = function(rowIndex) {

		rowIndex = parseInt(rowIndex + "");

		if ((rowIndex + 1) > self.selectedNodeList.length) {
			console.log("移除使用者失敗！remove index:" + rowIndex);
			console.log("selectedNodeList (" + self.selectedNodeList.length + "): \r\n" + JSON.stringify(self.selectedNodeList));
			alert("移除使用者失敗！");
			return;
		}
		// 節點資料
		var targetNodeData = self.selectedNodeList[rowIndex];

		// 判斷是否為雙擊
		if (!this.isNodeDoubleClick(targetNodeData.rowKey)) {
			return;
		}
		// 為停用節點
		var isInactive = (targetNodeData != null && !targetNodeData.active);

		this.removeSelectedData(targetNodeData.rowKey, isInactive, isInactive ? targetNodeData.sid : null);

		// 重建已選 資料 清單區
		self.rebuildselectedNodeArea();

		// 重置待選清單樹顯示狀態
		self.resetTreeNodeDisplayStatus();

		// 重置已選清單樹顯示狀態
		self.resetSelectedTreeNodeDisplayStatus();

	}

	// ====================================
	// 由樹狀待選清單中隱藏節點
	// ====================================
	this.hideTreeNode = function(targetNode, hideAttr) {
		// jQuery 化
		targetNode = $(targetNode);
		// 隱藏傳入節點
		targetNode.hide();
		// 加入自訂隱藏標記
		targetNode.attr(hideAttr, "Y");
	}

	// ====================================
	// 取得連動資料
	// ====================================
	this.getLinkageRowkeys = function(rowkey) {
		// 判斷無資料
		if (!(rowkey in self.allNodeDataMapByRowKey)) {
			return [];
		}

		// 取得連動設定
		var linkAgeNodeRowkeys = self.linkAgeMap[rowkey];
		if (commonUtil.isNull(linkAgeNodeRowkeys)) {
			return [];
		}

		return linkAgeNodeRowkeys;
	}

	// ====================================
	// 取得父節點
	// ====================================
	this.getParentNode = function(targetElm) {
		var parentElm = $(targetElm).parent();
		//已達 doc 最頂端
		if (parentElm == null) {
			return null;
		}

		// 已達最上層- 終止
		if (parentElm.hasClass(QUICK_SELECT_TREE_TREE_CONTAINER_CLASS)) {
			return null;
		}

		// 判斷父元素是否為 tree node
		if (parentElm.is("." + QUICK_SELECT_TREE_TREE_NODE_CLASS)) {
			return parentElm;
		}
		//遞迴往上爬
		return self.getParentNode(parentElm);
	}
	// ====================================
	// 清除過濾器
	// ====================================
	this.clearFilter4Tree = function() {
		self.treeFilterText.val("");
		self.treeFilterText.keyup();
	}
	this.clearFilter4SelectedArea = function() {
		self.selectedFilterText.val("");
		self.selectedFilterText.keyup();
	}

	// ====================================
	// 切換
	// ====================================
	this.switchSelectedShowMode_check = false;
	this.switchSelectedShowMode = function(switchElm, type, event) {

		switchElm = $(switchElm);
		var isChecked = $(switchElm).is(":checked");
		var wvName = self.componentID + "_inputSwitch_";
		wvName += ("A" == type) ? "list" : "tree";

		if (self.switchSelectedShowMode_check) {
			// console.log(wvName + "離開");
			self.switchSelectedShowMode_check = false;
			return;
		}
		// console.log(wvName + "註記");
		self.switchSelectedShowMode_check = true;

		$("." + wvName).click();

		if (isChecked) {
			$(".quick-select-tree-selected-tree").animate({
				opacity: '0'
			}, 0, function() {
				$(".quick-select-tree-selected-tree").hide();
			});
			$(".quick-select-tree-selected-list").css("opacity", 1);
			$(".quick-select-tree-selected-list").show();

		} else {
			$(".quick-select-tree-selected-list").animate({
				opacity: '0'
			}, 0, function() {
				$(".quick-select-tree-selected-list").hide();
			});
			$(".quick-select-tree-selected-tree").css("opacity", 1);
			$(".quick-select-tree-selected-tree").show();
		}
	}

	// ====================================
	// debug 訊息控制
	// ====================================
	this.debug = function(message) {
		if (QUICK_SELECT_TREE_IS_SHOW_DEBUG) {
			console.log(message);
		}
	}

	// ====================================
	// 初始化元件
	// ====================================
	try {
		$(".quick-select-tree-selected-list").hide();
		self.init();
		console.log("quickSelectTree init :[" + componentID + "]");
	} catch (e) {
		console.log("初始化失敗:" + e)
		console.log(e);
	}
}

QuickSelectTree.prototype = {
	constructor: QuickSelectTree
}