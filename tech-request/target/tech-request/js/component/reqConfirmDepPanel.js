/**
 * 
 */
if (window._reqConfirmDepPanel == null || typeof _reqConfirmDepPanel === 'undefined') {
	window._reqConfirmDepPanel = {
		isShow: false,
		isInit: false,
		widget: null,
		tabContent: function() {
			var width = screen.width;
			var height = screen.height;
			if (width > 1439) {
				if (height > 899) {
					return "<br />&nbsp;&nbsp;製作&nbsp;&nbsp;<br />&nbsp;&nbsp;進度&nbsp;&nbsp;<br />&nbsp;";
				}
			}
			return "&nbsp;<br />製作<br />進度<br />&nbsp;"
		},
		init: function() {

			//已存在時, 清掉重置
			deleteReqConfirmDepPanel();

			// 不顯示
			if (!_reqConfirmDepPanel.isShow) {
				return;
			}

			// ===========================================
			// slider
			// ===========================================
			try {
				_reqConfirmDepPanel.widget = $(".reqConfirmDepPanel").slidein({
					dock: "right", // 置右
					breadth: $(".reqConfirmDepPanel").outerWidth(), // 內容寬度
					open: false, // 預設收合
					peek: 12,
					speed: 100,
					position: 30,
					opacity: 0.95, // 透明度
					handleOpacity: 0.5, // 透明度
					prompt: _reqConfirmDepPanel.tabContent(),
					afterOpen: function() {
						// 點選頁簽(展開)後, 觸發資料重新讀取事件
						reqConfirmDepPanel_load();
					}
				});
				//console.log("_reqConfirmDepPanel init ok");

			} catch (e) {
				console.log(e);
				alert("製作進度面板初始化失敗, 請洽相關人員!");
			}
		},
		// 觸發關閉
		close: function() {
			if (_reqConfirmDepPanel.widget != null) {
				try {
					_reqConfirmDepPanel.widget.slidein("open", false);
				}
				catch (e) {
					console.log(e);
				}
			}
		}
	};
}

function deleteReqConfirmDepPanel() {
	if (window._reqConfirmDepPanel == null || window._reqConfirmDepPanel.widget == null) {
		return;
	}
	//已存在時, 清掉重置
	try {
		_reqConfirmDepPanel.widget.slidein("destroy", false);
		//console.log("deleteReqConfirmDepPanel destroy");
	}
	catch (e) { //console.log(e);
	}
}
