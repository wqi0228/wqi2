/**
 * 
 */
// 判斷是否按下Enter按鈕
function checkEnter(event) {
	var charCode = (event.which) ? event.which : event.keyCode;
	if (charCode === 13) {
		return true;
	}
	return false;
}

/**
 * 檢核是否為 function
 * 
 * @param func
 * @returns
 */
function common_isFunc(func) {
	try {
		return $.isFunction(func);
	} catch (e) { }
	return false;
}

/**
 * 以 class 搜尋, 並移除搜尋結果元素的指定 class
 * 
 * @param targetClassName
 *            目標的class
 * @param removeClassName
 *            要移除的 class 名稱
 * @returns
 */
function removeElmClass(targetClassName, removeClassName) {
	$("." + targetClassName).each(function() {
		var self = $(this);
		if (self.hasClass(removeClassName)) {
			self.removeClass(removeClassName);
		}
	});
}

function common_changeIframeSrc(iframeid, srcUrl) {
	var iframe = $("iframe #" + iframeid);
	if (iframe.length == 0) {
		return;
	}

	iframe.attr("src", srcUrl);
}

//以動態方式, 載入CSS file
function appedCSSFileByUnqID(unqID, path) {
	// 動態載入 CSS 檔案
	if (!document.getElementById(unqID)) {
		var head = document.getElementsByTagName('head')[0];
		var link = document.createElement('link');
		// link.id = "quick-select-tree-css-file";
		link.id = unqID;
		link.rel = 'stylesheet';
		link.type = 'text/css';
		// link.href = '../resources/css/quickSelectTree.css?ver=${commonBean.getVersion()}';
		link.href = path;
		link.media = 'all';
		head.appendChild(link);
	}
}


//複製傳入值到剪貼簿
function common_copyToClickBoard(val) {
	const el = document.createElement('textarea');
	el.value = val;
	document.body.appendChild(el);
	el.select();
	document.execCommand('copy');
	document.body.removeChild(el);
}