/**
 * startsWith (for safari)
 */
if (!String.prototype.startsWith) {
	Object.defineProperty(String.prototype, 'startsWith', {
		value : function(search, pos) {
			pos = !pos || pos < 0 ? 0 : +pos;
			return this.substring(pos, pos + search.length) === search;
		}
	});
}

/**
 * includes (for safari)
 */
if (!String.prototype.includes) {
	Object.defineProperty(String.prototype, 'includes', {
		value : function(search, start) {
			if (typeof start !== 'number') {
				start = 0
			}
			search = search + "";
			if (start + search.length > this.length) {
				return false
			} else {
				return this.indexOf(search, start) !== -1
			}
		}
	})
}

// String 擴充方法 ,判斷是否為空
if (typeof String.prototype.isEmpty != 'function') {
	String.prototype.isEmpty = function() {
		return (this == null || this.length === 0 || !this.trim());
	};
}

if (!String.prototype.trim) {
  String.prototype.trim = function () {
    return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
  };
}

if (!String.prototype.isNumeric) {
    String.prototype.isNumeric = function(str) {
        if (typeof str != "string") return false; // we only process strings!
        return !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
            !isNaN(parseFloat(str)); // ...and ensure strings of whitespace fail
    };
}

// cssValue
(function($) {
	$.fn.cssValue = function(p) {
		var result;
		return isNaN(result = parseFloat(this.css(p))) ? 0 : result;
	};
})(jQuery);


// 提供【不快取】的取得 script 版本
(function($) {
	$.getScriptCached = function(url, callback) {
		return $.ajax({
			url : url,
			dataType : "script",
			cache : true
		}).done(callback)
	}
})(jQuery);
