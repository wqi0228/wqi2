/**
 * 
 */
var commonUtil = {}

commonUtil.isNull = function(obj) {
    return obj == undefined || obj == null;
}

var commonPFUtil = {}

// 依據傳入區域中有的 checkbox 異動【全選 checkbox】的選擇狀態
commonPFUtil.changeAllSelectedCheckboxByItemStatus = function(itemAreaElm, allSelectedCheckboxElm) {
    if (itemAreaElm == null || itemAreaElm == undefined || itemAreaElm.length == 0) {
        log.console("autoChangeAllSelectedCheckboxByItemStatus:傳入的 itemAreaElm 有誤!");
        return;
    }
    if (commonUtil.isNull(allSelectedCheckboxElm) || allSelectedCheckboxElm.length == 0) {
        log.console("autoChangeAllSelectedCheckboxByItemStatus:傳入的 全選checkbox 有誤!");
        return;
    }
    commonPFUtil.changeCheckBoxStatus(allSelectedCheckboxElm, (itemAreaElm.find(".ui-icon-blank").length == 0));
}
// 異動 PF checkbox 的選擇狀態
commonPFUtil.changeCheckBoxStatus = function(elm, isSelected) {
    if (elm == null || elm == undefined || elm.length == 0) {
        log.console("changePFCheckBoxStatus:傳入全選checkbox有誤！");
        return;
    }

    var elmBox = $(elm).find(".ui-chkbox-box");
    var elmIcon = $(elm).find(".ui-chkbox-icon");
    if (elmBox.length == 0 || elmIcon.length == 0) {
        log.console("傳入物件不是 PF Checkbox");
        return;
    }

    var cssAvtice = "ui-state-active";
    var cssIconCheck = "ui-icon-check";
    var cssIconBlank = "ui-icon-blank";

    if (isSelected) {
        if (!elmBox.hasClass(cssAvtice)) {
            elmBox.addClass(cssAvtice);
        }
        elmIcon.removeClass(cssIconBlank);
        if (!elmIcon.hasClass(cssIconCheck)) {
            elmIcon.addClass(cssIconCheck);
        }

    } else {
        elmBox.removeClass(cssAvtice);
        elmIcon.removeClass(cssIconCheck);
        if (!elmIcon.hasClass(cssIconBlank)) {
            elmIcon.addClass(cssIconBlank);
        }
    }
}
