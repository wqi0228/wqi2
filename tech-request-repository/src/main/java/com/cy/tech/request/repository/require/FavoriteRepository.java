/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.tech.request.vo.require.Favorite;
import com.cy.tech.request.vo.require.Require;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * 收藏夾 REPO
 *
 * @author shaun
 */
@Repository("tr_favoriteRepository")
public interface FavoriteRepository extends JpaRepository<Favorite, String>, Serializable {

    public Favorite findByRequireAndCreatedUser(Require require, User createdUser);

    public List<Favorite> findByCreatedUserOrderByCreatedDateDesc(User createdUser);

    @Query("SELECT f FROM #{#entityName} f JOIN f.require tr JOIN tr.index idx WHERE f.createdUser = :user "
              + "AND f.createdDate BETWEEN :start AND :end AND f.status IN :status "
              + "AND (idx.fieldName = '主題' AND idx.fieldContent LIKE :searchText) ")
    public List<Favorite> findBySearchPage(@Param("user") User user, @Param("start") Date date, @Param("end") Date end, @Param("searchText") String searchText, @Param("status") List<Activation> status);

}
