package com.cy.tech.request.repository.pps6;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cy.tech.request.vo.pps6.Pps6ModelerExecSetting;

/**
 * PPS6 Modeler 執行設定資料檔 Repository
 * @author allen
 */
public interface Pps6ModelerExecSettingRepository extends JpaRepository<Pps6ModelerExecSetting, String>, Serializable {

	/**
	 * 刪除所流程相關的執行設定資料
	 * 
	 * @param flowId flow ID
	 * @return 返回刪除筆數
	 */
	@Modifying
	@Query("delete "
			+ "FROM #{#entityName} e "
			+ "WHERE e.flowSid = :flowSid")
	public void deleteByFlowSid(@Param("flowSid") Integer flowSid);
	
	/**
	 * 以流程SID查詢
	 * @param flowSid
	 * @return
	 */
	public List<Pps6ModelerExecSetting> findByFlowSid(Integer flowSid);
}
