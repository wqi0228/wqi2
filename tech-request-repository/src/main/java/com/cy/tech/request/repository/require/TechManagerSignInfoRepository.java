/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require;

import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.TechManagerSignInfo;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author shaun
 */
public interface TechManagerSignInfoRepository extends JpaRepository<TechManagerSignInfo, String>, Serializable {

    public TechManagerSignInfo findByRequire(Require require);

    public TechManagerSignInfo findByRequireNo(String requireNo);

    public TechManagerSignInfo findByBpmInstanceId(String bpmInstanceId);

    @Query(value = "SELECT * FROM tr_tech_manager_sign_info tmsi "
              + "     WHERE tmsi.paper_code IN ('PAPER000','PAPER001','PAPER002','PAPER004');", nativeQuery = true)
    public List<TechManagerSignInfo> findRunningProcess();

}
