/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require.othset;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.require.othset.OthSet;
import com.cy.tech.request.vo.require.othset.OthSetAttachment;
import com.cy.tech.request.vo.require.othset.OthSetHistory;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface OthSetAttachmentRepo extends JpaRepository<OthSetAttachment, String>, Serializable {

    public List<OthSetAttachment> findByOthsetAndStatusAndHistoryIsNullOrderByCreatedDateDesc(OthSet othset, Activation status);

    public List<OthSetAttachment> findByHistoryAndStatusOrderByCreatedDateDesc(OthSetHistory history, Activation status);

    @Query(value = "SELECT * FROM tr_os_attachment a JOIN tr_os_history h ON a.os_history_sid = h.os_history_sid "
              + "     WHERE h.os_history_sid IN (:historySids) "
              + "       AND a.status = 0"
              + "     ORDER BY a.create_dt DESC ", nativeQuery = true)
    public List<OthSetAttachment> findAttachByHistoryInOrderByUpdateDateDesc(@Param("historySids") List<String> historySids);
}
