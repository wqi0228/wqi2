
package com.cy.tech.request.repository.require;

import com.cy.tech.request.vo.require.AssignSendInfoHistory;
import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author allen1214_wu
 */
public interface AssignSendInfoHistoryRepository extends JpaRepository<AssignSendInfoHistory, String>, Serializable {
    
    
    /**
     * 以需求單 SID 查詢
     * @param requireSid
     * @return
     */
    public List<AssignSendInfoHistory> findByRequireSidOrderByCreatedDateDesc(String requireSid);
    
    /**
     * 以需求單 SID 查詢
     * @param requireSid
     * @return
     */
    public List<AssignSendInfoHistory> findByRequireSidOrderByCreatedDateDescSidDesc(String requireSid);
}
