package com.cy.tech.request.repository.setting.sysnotify;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.cy.tech.request.vo.setting.sysnotify.SettingSysNotify;

/**
 * @author allen
 * 設定：系統通知設定
 */
public interface SettingSysNotifyRepository extends JpaRepository<SettingSysNotify, String>, Serializable {
	
	/**
	 * 以使用者 sid 查詢
	 * @param userSid 使用者 sid
	 * @return SettingSysNotify 設定資料
	 */
	public SettingSysNotify findByUserSid(Integer userSid);
	
	/**
	 * 以使用者 sid 查詢 (多筆)
	 * @param userSids 使用者 sid
	 * @return SettingSysNotify 設定資料
	 */
	public List<SettingSysNotify> findByUserSidIn(List<Integer> userSids);
	
}
