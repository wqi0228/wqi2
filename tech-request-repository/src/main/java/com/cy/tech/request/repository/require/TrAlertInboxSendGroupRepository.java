/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.enums.ForwardType;
import com.cy.tech.request.vo.require.TrAlertInboxSendGroup;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author brain0925_liao
 */
public interface TrAlertInboxSendGroupRepository extends JpaRepository<TrAlertInboxSendGroup, String>, Serializable {

    @Query("SELECT r FROM #{#entityName} r WHERE r.requireSid = :requireSid  AND r.createdUser = :createdUser  AND r.status = 0 ORDER BY r.createdDate DESC ")
    public List<TrAlertInboxSendGroup> findByUserSidAndRequireSid(@Param("requireSid") String requireSid, @Param("createdUser") Integer createdUser);

    /**
     * 以下列條件查詢
     * 
     * @param requireNo   需求單號
     * @param forwardType 轉寄類別
     * @param createdUser 建立者
     * @param status      狀態
     * @return
     */
    public List<TrAlertInboxSendGroup> findByRequireNoAndForwardTypeAndCreatedUserAndStatus(
            String requireNo,
            ForwardType forwardType,
            Integer createdUser,
            Activation status);

}
