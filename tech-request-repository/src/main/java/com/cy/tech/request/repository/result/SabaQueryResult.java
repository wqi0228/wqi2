package com.cy.tech.request.repository.result;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;

public class SabaQueryResult {

    @Getter
    @Setter
    private String requirdSid;
    @Getter
    @Setter
    private Date createDate;
    @Getter
    @Setter
    private String deptName;
    @Getter
    @Setter
    private String userName;
    @Getter
    @Setter
    private Integer depSid;
    @Getter
    @Setter
    private Integer userSid;
    @Getter
    @Setter
    private String theme;
    @Getter
    @Setter
    private String requireNo;
    @Getter
    @Setter
    private Date expectDate;
    @Getter
    @Setter
    private String status;
    @Getter
    @Setter
    private String finishCode;
    @Getter
    @Setter
    private String isNcsDone;
    @Getter
    @Setter
    /** 本地端連結網址 */
    private String localUrlLink;
    @Getter
    @Setter
    /** 緊急度 */
    private Integer urgency;
    @Getter
    @Setter
    /** 閱讀狀態 */
    private String readRecord;
    @Getter
    @Setter
    /** 閱讀紀錄JSON */
    private String readRecordJson;

    public SabaQueryResult() {

    }

    public SabaQueryResult(String requirdSid, Date createDate, Integer depSid, Integer userSid, String theme,
              String requireNo, Date expectDate, String status, String finishCode, String isNcsDone, Integer urgency, String readRecordJson) {
        super();
        this.requirdSid = requirdSid;
        this.createDate = createDate;
        this.depSid = depSid;
        this.userSid = userSid;
        this.theme = theme;
        this.requireNo = requireNo;
        this.expectDate = expectDate;
        this.status = status;
        this.finishCode = finishCode;
        this.isNcsDone = isNcsDone;
        this.urgency = urgency;
        this.readRecordJson = readRecordJson;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((requirdSid == null) ? 0 : requirdSid.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SabaQueryResult other = (SabaQueryResult) obj;
        if (requirdSid == null) {
            if (other.requirdSid != null) {
                return false;
            }
        } else if (!requirdSid.equals(other.requirdSid)) {
            return false;
        }
        return true;
    }

}
