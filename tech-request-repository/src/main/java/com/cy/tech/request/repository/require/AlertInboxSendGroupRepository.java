/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require;

import com.cy.commons.vo.User;
import com.cy.tech.request.vo.enums.ForwardType;
import com.cy.tech.request.vo.require.AlertInboxSendGroup;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface AlertInboxSendGroupRepository extends JpaRepository<AlertInboxSendGroup, String>, Serializable {

    @Query("SELECT aig FROM #{#entityName} aig WHERE aig.requireNo = :requireNo AND aig.forwardType = :forwardType AND aig.createdUser = :sender")
    public List<AlertInboxSendGroup> findByRequireNoAndForwardTypeAndSend(
              @Param("requireNo") String requireNo, @Param("forwardType") ForwardType forwardType, @Param("sender") User sender);
    
    @Query("SELECT aig FROM #{#entityName} aig WHERE aig.require.sid = :requireSid AND aig.forwardType = :forwardType")
    public List<AlertInboxSendGroup> findByRequireSidAndForwardType(
              @Param("requireSid") String requireSid, @Param("forwardType") ForwardType forwardType);

    @Modifying
    @Query("UPDATE #{#entityName} g SET g.sendTo = :sendTo ,"
              + "                       g.updatedUser = :upUsr ,"
              + "                       g.updatedDate = :upDt "
              + " WHERE g = :group")
    public int updateSendToStr(@Param("group") AlertInboxSendGroup group,
              @Param("sendTo") String sendTo,
              @Param("upUsr") User upUsr, @Param("upDt") Date upDt);

    @Modifying
    @Query("UPDATE #{#entityName} g SET g.sendTo = :sendTo "
              + " WHERE g = :group")
    public int updateSendToStr(@Param("group") AlertInboxSendGroup group,
              @Param("sendTo") String sendTo);
}
