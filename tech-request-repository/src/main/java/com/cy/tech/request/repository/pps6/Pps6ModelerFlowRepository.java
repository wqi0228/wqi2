package com.cy.tech.request.repository.pps6;

import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cy.tech.request.vo.pps6.Pps6ModelerFlow;

/**
 * PPS6 Modeler 流程定義資料檔 Repository 
 * @author allen
 */
public interface Pps6ModelerFlowRepository extends JpaRepository<Pps6ModelerFlow, String>, Serializable {

	/**
	 * 以流程代號查詢
	 * 
	 * @param flowId 流程代號
	 * @return Pps6ModelerFlow
	 */
	public Pps6ModelerFlow findByFlowId(String flowId);

	/**
	 * 查詢流程代號資料是否存在
	 * 
	 * @param flowId
	 * @return
	 */
	@Query(""
			+ "SELECT COUNT(e.sid) > 0  as AA "
			+ "FROM #{#entityName} e "
			+ "WHERE e.flowId = :flowId")
	public boolean existsByFlowId(@Param("flowId") String flowId);

}
