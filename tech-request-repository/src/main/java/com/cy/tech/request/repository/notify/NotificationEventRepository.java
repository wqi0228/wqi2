/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.notify;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cy.tech.request.vo.require.notify.NotificationEvent;

/**
 *
 * @author aken_kao
 */
public interface NotificationEventRepository extends JpaRepository<NotificationEvent, String>, Serializable {
    
    @Query("SELECT t FROM #{#entityName} t WHERE t.receiver = :userSid ORDER BY t.sourceNo DESC, t.createdDate DESC")
    public List<NotificationEvent> findAllByUserSid(
            @Param("userSid") Integer userSid);
    
    @Query("SELECT t FROM #{#entityName} t WHERE t.receiver = :userSid and t.status = 0")
    public List<NotificationEvent> findByReceiver(
            @Param("userSid") Integer userSid);
    
    @Query("SELECT DISTINCT t.receiver FROM #{#entityName} t")
    public List<Integer> findDistinctReceiver();
    
}
