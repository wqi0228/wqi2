package com.cy.tech.request.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.cy.tech.request.repository.result.TransRequireVO;

/**
 *
 *
 */
@Repository
public class TrNamedQueryRepository {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * 批次轉單程式-建立單位轉發(需求單)
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<TransRequireVO> findRequireByCreateDeptAndUser(List<Integer> deptSids, List<Integer> userSids) {
        Query query = entityManager.createNamedQuery("findRequireByCreateDeptAndUser");
        query.setParameter("deptSids", deptSids);
        query.setParameter("userSids", userSids);
        return query.getResultList();
    }

    /**
     * 批次轉單程式-建立單位轉發(原型確認)
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<TransRequireVO> findPtByCreateDeptAndUser(List<Integer> deptSids, List<Integer> userSids) {
        Query query = entityManager.createNamedQuery("findPtByCreateDeptAndUser");
        query.setParameter("deptSids", deptSids);
        query.setParameter("userSids", userSids);
        return query.getResultList();
    }

    /**
     * 批次轉單程式-建立單位轉發(送測)
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<TransRequireVO> findSendTestByCreateDeptAndUser(List<Integer> deptSids, List<Integer> userSids) {
        Query query = entityManager.createNamedQuery("findSendTestByCreateDeptAndUser");
        query.setParameter("deptSids", deptSids);
        query.setParameter("userSids", userSids);
        return query.getResultList();
    }

    /**
     * 批次轉單程式-建立單位轉發(其他設定)
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<TransRequireVO> findOthSetByCreateDeptAndUser(List<Integer> deptSids, List<Integer> userSids) {
        Query query = entityManager.createNamedQuery("findOthSetByCreateDeptAndUser");
        query.setParameter("deptSids", deptSids);
        query.setParameter("userSids", userSids);
        return query.getResultList();
    }

    /**
     * 批次轉單程式-建立單位轉發(ON程式)
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<TransRequireVO> findOnpgByCreateDeptAndUser(List<Integer> deptSids, List<Integer> userSids) {
        Query query = entityManager.createNamedQuery("findOnpgByCreateDeptAndUser");
        query.setParameter("deptSids", deptSids);
        query.setParameter("userSids", userSids);
        return query.getResultList();
    }

    /**
     * 批次轉單程式-轉寄單位轉發(需求單)
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<TransRequireVO> findAlertBoxByDepts(List<Integer> deptSids) {
        Query query = entityManager.createNamedQuery("findAlertBoxByDepts");
        query.setParameter("deptSids", deptSids);
        return query.getResultList();
    }

    /**
     * 批次轉單程式-通知單位轉發(原型確認)
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<TransRequireVO> findPtByNotifiedDept(String regexpDepts) {
        Query query = entityManager.createNamedQuery("findPtByNotifiedDept");
        query.setParameter("regexpDepts", regexpDepts);
        return query.getResultList();
    }

    /**
     * 批次轉單程式-通知單位轉發(送測)
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<TransRequireVO> findSendTestByNotifiedDept(String regexpDepts) {
        Query query = entityManager.createNamedQuery("findSendTestByNotifiedDept");
        query.setParameter("regexpDepts", regexpDepts);
        return query.getResultList();
    }

    /**
     * 批次轉單程式-通知單位轉發(其他設定)
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<TransRequireVO> findOthSetByNotifiedDept(String regexpDepts) {
        Query query = entityManager.createNamedQuery("findOthSetByNotifiedDept");
        query.setParameter("regexpDepts", regexpDepts);
        return query.getResultList();
    }

    /**
     * 批次轉單程式-通知單位轉發(ON程式)
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<TransRequireVO> findOnpgByNotifiedDept(String regexpDepts) {
        Query query = entityManager.createNamedQuery("findOnpgByNotifiedDept");
        query.setParameter("regexpDepts", regexpDepts);
        return query.getResultList();
    }
}
