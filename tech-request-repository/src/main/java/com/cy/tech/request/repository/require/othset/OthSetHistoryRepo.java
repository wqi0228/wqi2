/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require.othset;

import com.cy.tech.request.vo.require.othset.OthSet;
import com.cy.tech.request.vo.require.othset.OthSetHistory;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface OthSetHistoryRepo extends JpaRepository<OthSetHistory, String>, Serializable {

    @Query("SELECT h FROM #{#entityName} h WHERE h.othset = :othset AND h.behavior != 'REPLY_AND_REPLY' ORDER BY updatedDate DESC ")
    public List<OthSetHistory> findByOthset(@Param("othset") OthSet othset);

    @Query("SELECT h FROM #{#entityName} h WHERE h.osNo IN :osNos ORDER BY updatedDate DESC ")
    public List<OthSetHistory> findByOsNoIn(@Param("osNos") List<String> osNos);
}
