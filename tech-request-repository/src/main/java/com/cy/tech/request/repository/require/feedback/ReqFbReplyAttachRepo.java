/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require.feedback;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.require.feedback.ReqFbkReply;
import com.cy.tech.request.vo.require.feedback.ReqFbkReplyAttach;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 需求資訊補充回覆附加檔案
 *
 * @author shaun
 */
public interface ReqFbReplyAttachRepo extends JpaRepository<ReqFbkReplyAttach, String>, Serializable {

    public List<ReqFbkReplyAttach> findByReplyAndStatusOrderByCreatedDateDesc(ReqFbkReply reply, Activation status);

}
