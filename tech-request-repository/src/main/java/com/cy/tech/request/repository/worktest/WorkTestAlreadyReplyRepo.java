/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.worktest;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cy.tech.request.vo.worktest.WorkTestAlreadyReply;
import com.cy.tech.request.vo.worktest.WorkTestReply;

/**
 *
 * @author shaun
 */
public interface WorkTestAlreadyReplyRepo extends JpaRepository<WorkTestAlreadyReply, String>, Serializable {

    public List<WorkTestAlreadyReply> findByReplyOrderByUpdateDateDesc(WorkTestReply reply);

    /**
     * 查找回覆by history sid <BR/> 無法接受空集合做參數值
     *
     * @param historySids
     * @return
     */
    @Query(value = "SELECT * FROM work_test_already_reply r "
              + "    JOIN work_test_info_history h ON r.already_reply_sid = h.already_reply_sid "
              + "    WHERE h.testinfo_history_sid IN (:historySids) ORDER BY r.reply_udt DESC ", nativeQuery = true)
    public List<WorkTestAlreadyReply> findReplyByHistoryInOrderByUpdateDateDesc(@Param("historySids") List<String> historySids);

}
