/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.worktest;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.vo.worktest.WorkTestReply;

/**
 *
 * @author shaun
 */
public interface WorkTestReplyRepo extends JpaRepository<WorkTestReply, String>, Serializable {

    public List<WorkTestReply> findByTestInfo(WorkTestInfo testInfo);

}
