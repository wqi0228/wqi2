/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.sp;

import com.cy.commons.vo.Role;
import com.cy.tech.request.vo.sp.SpecificRoleInquireDep;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author jason_h
 */
public interface SpecificRoleInquireDepRepository extends JpaRepository<SpecificRoleInquireDep, Long>, Serializable {

    public List<SpecificRoleInquireDep> findByRoleIn(List<Role> roles);
}
