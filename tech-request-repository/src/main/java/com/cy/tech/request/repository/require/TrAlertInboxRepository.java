/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.enums.ForwardType;
import com.cy.tech.request.vo.enums.ReadRecordStatus;
import com.cy.tech.request.vo.require.TrAlertInbox;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author brain0925_liao
 */
public interface TrAlertInboxRepository extends JpaRepository<TrAlertInbox, String>, Serializable {

    @Query("SELECT r FROM #{#entityName} r WHERE r.sid in :sids  ")
    public List<TrAlertInbox> findByInboxBySids(@Param("sids") List<String> sids);

    @Query("SELECT r FROM #{#entityName} r WHERE r.status = 0  ")
    public List<TrAlertInbox> findActiveAll();

    /** 搜尋轉寄細項 By 需求單Sid及轉寄類型 */
    @Query("SELECT r FROM #{#entityName} r WHERE r.requireSid = :requireSid AND r.forwardType = :forwardType ORDER BY r.status ASC,r.updatedDate DESC  ")
    public List<TrAlertInbox> findByInboxByRequireSidAndForwardType(@Param("requireSid") String requireSid, @Param("forwardType") ForwardType forwardType);

    /**
     * 依下列條件查詢
     * 
     * @param requireNo 需求單號
     * @return TrAlertInbox list
     */
    public List<TrAlertInbox> findByRequireNo(String requireNo);

    /**
     * 以下列條件查詢
     * 
     * @param requireNo   需求單號
     * @param forwardType 轉寄類型
     * @param status      狀態
     * @return TrAlertInbox list
     */
    public List<TrAlertInbox> findByRequireNoAndForwardTypeAndStatusOrderByCreatedDate(
            String requireNo,
            ForwardType forwardType,
            Activation status);

    /**
     * 以下列條件查詢
     * 
     * @param requireSid 需求單 Sid
     * @param status     資料狀態
     * @return
     */
    public List<TrAlertInbox> findByRequireSidAndStatus(
            String requireSid,
            Activation status);

    /**
     * 查詢轉寄個人的收件者
     * 
     * @param requireNo 需求單號
     * @param sender    寄件者
     * @return
     */
    @Query("SELECT e.receive "
            + "FROM  #{#entityName} e "
            + "WHERE e.requireNo = :requireNo "
            + "  AND e.sender = :sender "
            + "  AND e.forwardType = com.cy.tech.request.vo.enums.ForwardType.FORWARD_MEMBER "
            + "  AND e.status =  com.cy.commons.enums.Activation.ACTIVE ")
    public List<Integer> findReceiverByRequireNoAndSender(
            @Param("requireNo") String requireNo,
            @Param("sender") Integer sender);

    @Query("SELECT r FROM #{#entityName} r WHERE r.alert_group_sid = :alert_group_sid  ORDER BY r.status ASC,r.updatedDate DESC,r.read_dt ASC")
    public List<TrAlertInbox> findByInboxByInboxGroupSid(@Param("alert_group_sid") String alert_group_sid);

    @Modifying
    @Transactional(rollbackFor = Exception.class)
    @Query("UPDATE #{#entityName} r SET r.read_record  = :readed,r.read_dt = :read_dt "
            + " WHERE r.status = 0 AND r.requireSid = :requireSid AND r.read_record = :read_record AND ((r.receiveDep = :receiveDep AND r.forwardType = :depforwardType) OR (r.receive = :receive AND r.forwardType = :personforwardType)) ")
    public int updateReadRecordStatus(@Param("readed") ReadRecordStatus readed, @Param("read_dt") Date read_dt, @Param("requireSid") String requireSid,
            @Param("read_record") ReadRecordStatus read_record, @Param("receive") Integer receive, @Param("receiveDep") Integer receiveDep, @Param("depforwardType") ForwardType depforwardType,
            @Param("personforwardType") ForwardType personforwardType);

    @Query("SELECT t FROM #{#entityName} t WHERE t.requireSid = :requireSid and t.receiveDep in :receiveDeptSids and t.forwardType = 'FORWARD_DEPT' and t.status = 'ACTIVE' and t.inboxType = 'INCOME_DEPT'")
    public List<TrAlertInbox> findByInboxByRequireSidAndReceiveDepts(
            @Param("requireSid") String requireSid,
            @Param("receiveDeptSids") List<Integer> receiveDeptSids);

    @Query("SELECT t FROM #{#entityName} t WHERE t.requireSid = :requireSid and t.receiveDep = :receiveDeptSid and t.forwardType = 'FORWARD_DEPT' and t.status = 'ACTIVE' and t.inboxType = 'INCOME_DEPT'")
    public List<TrAlertInbox> findByInboxByRequireSidAndReceiveDep(
            @Param("requireSid") String requireSid,
            @Param("receiveDeptSid") Integer receiveDeptSid);

}
