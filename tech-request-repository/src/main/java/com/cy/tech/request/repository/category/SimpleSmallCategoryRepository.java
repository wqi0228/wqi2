package com.cy.tech.request.repository.category;

import com.cy.tech.request.vo.anew.enums.SignLevelType;
import com.cy.tech.request.vo.category.SimpleSmallCategory;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author allen
 */
public interface SimpleSmallCategoryRepository extends JpaRepository<SimpleSmallCategory, String>, Serializable {

    /**
     * @param status
     * @return
     */
    public List<SimpleSmallCategory> findAllByOrderByIdAsc();
    
    /**
     * @param sid 以中分類 sid 查詢
     * @return
     */
    public List<SimpleSmallCategory> findByParentMiddleCategorySidOrderByIdAsc(String parentMiddleCategorySid);
    
    /**
     * @return
     */
    public SimpleSmallCategory findBySid(String sid);
    
    @Query("SELECT signLevel FROM #{#entityName} e WHERE e.sid = :sid")
    public SignLevelType findSignLevelBySid(@Param("sid") String sid);
    
}
