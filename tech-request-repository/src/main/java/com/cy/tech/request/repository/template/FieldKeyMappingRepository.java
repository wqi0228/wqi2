/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.template;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 鍵值欄位對照表
 *
 * @author shaun
 */
public interface FieldKeyMappingRepository extends JpaRepository<FieldKeyMapping, String>, Serializable {

    @Query("SELECT fk FROM #{#entityName} fk WHERE fk.mappingId = :mappingId AND fk.mappingVersion = :ver AND fk.status = :status order by fk.seq")
    public List<FieldKeyMapping> findByMappingIdAndVersion(@Param("mappingId") String mappingId, @Param("ver") Integer ver, @Param("status") Activation status);

}
