package com.cy.tech.request.repository;

import java.io.BufferedReader;
import java.io.Reader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Clob;
import java.util.Arrays;
import java.util.Map;

import javax.persistence.AttributeConverter;
import javax.persistence.Convert;

import org.hibernate.HibernateException;
import org.hibernate.property.ChainedPropertyAccessor;
import org.hibernate.property.PropertyAccessor;
import org.hibernate.property.PropertyAccessorFactory;
import org.hibernate.property.Setter;
import org.hibernate.transform.AliasedTupleSubsetResultTransformer;
import org.springframework.util.ReflectionUtils;

import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * Alias to Bean 轉換器 <br/>
 * 修改自 org.hibernate.transform.AliasToBeanResultTransformer
 * 
 * @author allen1214_wu
 */
@Slf4j
public class CustomAliasToBeanResultTransformer extends AliasedTupleSubsetResultTransformer {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 687252215956268661L;

    private final Class<?> resultClass;
    private boolean isInitialized;
    private String[] aliases;
    private Setter[] setters;

    private Map<String, Object> converterMapByAlias = Maps.newHashMap();

    public CustomAliasToBeanResultTransformer(Class<?> resultClass) {
        if (resultClass == null) {
            throw new IllegalArgumentException("resultClass cannot be null");
        }
        isInitialized = false;
        this.resultClass = resultClass;
    }

    @Override
    public boolean isTransformedValueATupleElement(String[] aliases, int tupleLength) {
        return false;
    }

    @Override
    public Object transformTuple(Object[] tuple, String[] aliases) {
        Object result;

        try {
            if (!isInitialized) {
                initialize(aliases);
            } else {
                check(aliases);
            }

            result = resultClass.newInstance();

            for (int i = 0; i < aliases.length; i++) {

                if (setters[i] != null && tuple[i] != null) {

                    // 有 converter 先使用
                    if (this.converterMapByAlias.containsKey(aliases[i])) {
                        setters[i].set(result, this.prepareFieldValue(aliases[i], tuple[i]), null);
                        continue;
                    }

                    Class<?> paramClass = setters[i].getMethod().getParameterTypes()[0];
                    Class<?> fieldValueClass = tuple[i].getClass();

                    // 若目標參數類型和當前參數類型不匹配 ,嘗試進行轉換
                    if (!paramClass.equals(fieldValueClass)) {
                        if (Number.class.isAssignableFrom((fieldValueClass))) {
                            Number num = (Number) tuple[i];
                            if (Long.class.equals(paramClass) || long.class.equals(paramClass)) {
                                setters[i].set(result, num.longValue(), null);
                            } else if (Integer.class.equals(paramClass) || int.class.equals(paramClass)) {
                                setters[i].set(result, num.intValue(), null);
                            } else if (Boolean.class.equals(paramClass) || boolean.class.equals(paramClass)) {
                                setters[i].set(result, num.intValue() == 1, null);
                            } else if (Float.class.equals(paramClass) || float.class.equals(paramClass)) {
                                setters[i].set(result, num.floatValue(), null);
                            } else if (Double.class.equals(paramClass) || double.class.equals(paramClass)) {
                                setters[i].set(result, num.doubleValue(), null);
                            } else if (Short.class.equals(paramClass) || short.class.equals(paramClass)) {
                                setters[i].set(result, num.shortValue(), null);
                            } else if (BigDecimal.class.equals(paramClass)) {
                                setters[i].set(result, num, null);
                            }
                            // 列舉類型轉換
                            else if (paramClass.isEnum()) {
                                if (paramClass.getEnumConstants().length - 1 < num.intValue()) {
                                    log.warn("Enum 值無法轉換! field name:[{}], field type:[{}], value:[{}] ",
                                            aliases[i],
                                            paramClass,
                                            num.intValue());
                                } else {
                                    setters[i].set(result, paramClass.getEnumConstants()[num.intValue()], null);
                                }
                            }
                        }
                        // 如果tuple為參數的子類,直接設定
                        // 如java.util.Date; java.sql.Date;
                        else if (paramClass.isAssignableFrom(fieldValueClass)) {
                            setters[i].set(result, tuple[i], null);
                        }
                        // 處理資料庫類型定義為Clob的資料，將其轉換成字串類型
                        else if (tuple[i] instanceof Clob) {
                            Clob clob = (Clob) tuple[i];
                            setters[i].set(result, clobToString(clob), null);

                        } else {
                            log.error("不支援轉換的類型! field name:[{}], field type:[{}], value type:[{}]",
                                    aliases[i],
                                    paramClass,
                                    tuple[i].getClass());
                        }
                    } else {
                        setters[i].set(result, tuple[i], null);
                    }
                }
            }
        } catch (InstantiationException e) {
            throw new HibernateException("Could not instantiate resultclass: " + resultClass.getName());
        } catch (IllegalAccessException e) {
            throw new HibernateException("Could not instantiate resultclass: " + resultClass.getName());
        }

        return result;
    }

    private void initialize(String[] aliases) {
        PropertyAccessor propertyAccessor = new ChainedPropertyAccessor(
                new PropertyAccessor[] {
                        PropertyAccessorFactory.getPropertyAccessor(resultClass, null),
                        PropertyAccessorFactory.getPropertyAccessor("field")
                });

        this.aliases = new String[aliases.length];
        setters = new Setter[aliases.length];

        for (int i = 0; i < aliases.length; i++) {
            String aliasName = aliases[i];
            if (WkStringUtils.notEmpty(aliasName)) {
                // 欄位名稱
                this.aliases[i] = aliasName;

                // class setter (hibernate 原生方法)
                setters[i] = propertyAccessor.getSetter(this.resultClass, aliasName);

                // 準備欄位 Convert
                this.prepareFieldConvert(aliasName);
            }
        }
        isInitialized = true;
    }

    /**
     * 準備欄位 Convert
     * 
     * @param aliasName
     */
    private void prepareFieldConvert(String aliasName) {
        // ====================================
        // 取得 field
        // ====================================
        Field field = null;
        try {
            field = ReflectionUtils.findField(resultClass, aliasName);
        } catch (Exception e) {
            // 前方 propertyAccessor.getSetter 應該已經報錯, 理論上不會發生
            log.error("class field 找不到! class:[{}], aliasName:[{}]", this.resultClass, aliasName);
            log.error(e.getMessage(), e);
            return;
        }

        // ====================================
        // 取得 annotation (Convert)
        // ====================================
        // 取得 annotation
        Annotation[] annotations = field.getAnnotationsByType(Convert.class);
        // 未定義時跳出
        if (annotations == null || annotations.length == 0) {
            return;
        }

        // 防呆, 被定義了多個 convert
        if (annotations.length > 1) {
            throw new SystemDevelopException(
                    String.format("class field 被定義了多個 Convert! Class:[%s], field:[%s], ", this.resultClass, aliasName));
        }

        Convert convert = (Convert) annotations[0];
        // ====================================
        //
        // ====================================
        if (convert.converter() == null) {
            throw new SystemDevelopException(
                    String.format("class field 未設定 Converter! Class:[%s], field:[%s], ", this.resultClass, aliasName));
        }

        if (AttributeConverter.class.isAssignableFrom(convert.converter())) {
            try {
                this.converterMapByAlias.put(aliasName, convert.converter().newInstance());
            } catch (Exception e) {
                String message = String.format("class field 的 Convert 實例化失敗! Class:[%s], field:[%s], converter:[%s]",
                        this.resultClass,
                        aliasName,
                        convert.converter());
                log.error(message + "\r\n" + e.getMessage(), e);
                throw new SystemDevelopException(message);
            }
        }

    }

    /**
     * 準備欄位值
     * 
     * @param alias 欄位名稱
     * @param tuple 欄位值
     * @return 欄位值
     */
    @SuppressWarnings("unchecked")
    private Object prepareFieldValue(String alias, Object tuple) {

        // 沒有定義 convert 時, 直接回傳
        if (!this.converterMapByAlias.containsKey(alias)) {
            return tuple;
        }

        @SuppressWarnings("rawtypes")
        AttributeConverter attributeConverter = (AttributeConverter) this.converterMapByAlias.get(alias);
        return attributeConverter.convertToEntityAttribute(tuple);
    }

    private void check(String[] aliases) {
        if (!Arrays.equals(aliases, this.aliases)) {
            throw new IllegalStateException(
                    "aliases are different from what is cached; aliases=" + Arrays.asList(aliases) +
                            " cached=" + Arrays.asList(this.aliases));
        }
    }

    /**
     * 將clob類型資料轉換成字串
     * 
     * @param clob
     * @return
     */
    public static String clobToString(Clob clob) {
        String reString = "";
        try {
            Reader is = null;
            is = clob.getCharacterStream();

            // 得到流
            BufferedReader br = new BufferedReader(is);
            String s = null;
            s = br.readLine();

            StringBuffer sb = new StringBuffer();
            while (s != null) {
                // 將字串全部取出付值給StringBuffer由StringBuffer轉成STRING
                sb.append(s);
                s = br.readLine();
            }
            reString = sb.toString();
        } catch (Exception e) {
            log.error("取得clob內容失敗:" + e.getMessage(), e);
        }
        return reString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CustomAliasToBeanResultTransformer that = (CustomAliasToBeanResultTransformer) o;

        if (!resultClass.equals(that.resultClass)) {
            return false;
        }
        if (!Arrays.equals(aliases, that.aliases)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = resultClass.hashCode();
        result = 31 * result + (aliases != null ? Arrays.hashCode(aliases) : 0);
        return result;
    }

}
