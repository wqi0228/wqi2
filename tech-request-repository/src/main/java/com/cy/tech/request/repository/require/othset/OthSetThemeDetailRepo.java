/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require.othset;

import com.cy.tech.request.vo.require.othset.OthSetTheme;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author shaun
 */
public interface OthSetThemeDetailRepo extends JpaRepository<OthSetTheme, String>, Serializable {

}
