/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require.tros;

import com.cy.tech.request.vo.require.tros.TrOsReply;
import java.io.Serializable;
import java.util.Date;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author brain0925_liao
 */
public interface TrOsReplyRepository extends JpaRepository<TrOsReply, String>, Serializable {

    @Modifying
    @Transactional(rollbackFor = Exception.class)
    @Query("UPDATE #{#entityName} r SET r.reply_udt  = :reply_udt , r.reply_content = :reply_content ,r.reply_content_css = :reply_content_css"
            + " WHERE r.sid = :sid")
    public int updateTrOsReplyContent(@Param("reply_udt") Date reply_udt, @Param("reply_content") String reply_content,
            @Param("reply_content_css") String reply_content_css, @Param("sid") String sid);
}
