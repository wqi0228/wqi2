/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require;

import com.cy.tech.request.vo.require.TrRequire;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author brain0925_liao
 */
public interface TrRequireRepository extends JpaRepository<TrRequire, String>, Serializable {

    @Query("SELECT r FROM #{#entityName} r WHERE r.requireNo = :requireNo")
    public TrRequire findByRequireNo(@Param("requireNo") String requireNo);

    @Modifying
    @Transactional(rollbackFor = Exception.class)
    @Query("UPDATE #{#entityName} r SET r.dep_sid  = :dep_sid  WHERE r.requireNo = :requireNo")
    public int updateDepsid(@Param("requireNo") String requireNo, @Param("dep_sid") Integer dep_sid);
}
