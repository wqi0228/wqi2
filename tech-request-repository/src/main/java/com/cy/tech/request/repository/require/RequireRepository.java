/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require;

import com.cy.commons.vo.Org;
import com.cy.tech.request.repository.result.SabaQueryResult;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.customer.vo.WorkCustomer;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author shaun
 */
public interface RequireRepository extends JpaRepository<Require, String>, Serializable {

    @Query("SELECT CASE WHEN (COUNT(r.sid) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} r "
            + "WHERE r.requireNo = :requireNo "
            + "AND r.requireStatus = 'COMPLETED' ")
    public Boolean checkReqFinish(
            @Param("requireNo") String requireNo);

    @Query("SELECT COUNT(r) FROM #{#entityName} r WHERE r.requireStatus = :status")
    public Integer countByRequireStatus(@Param("status") RequireStatusType status);

    @Query("SELECT COUNT(r) FROM #{#entityName} r WHERE r.categoryConfirmCode = 'Y' AND r.hasAssign = 0 AND r.requireSuspendedCode = 0")
    public Integer countUnassign();

    @Query("SELECT author FROM #{#entityName} r WHERE r = :require ")
    public WorkCustomer findAuthorByRequire(@Param("require") Require require);

    /**
     * 以立案單位查詢
     * 
     * @param createDep
     * @return
     */
    public List<Require> findByCreateDep(Org createDep);

    @Query("SELECT r FROM #{#entityName} r WHERE r.requireNo = :requireNo")
    public Require findByRequireNo(@Param("requireNo") String requireNo);

    public List<Require> findByRequireNoIn(List<String> requireNoes);

    public List<Require> findByRequireNoIn(List<String> requireNoes, Sort sort);

    /**
     * 依據傳入條件查詢
     * 
     * @param requireStatus   製作進度 （多筆）
     * @param startCreateDate 立案起日 (條件為大於> , 無等於)
     * @param endCreateDate   立案迄日 (條件為大於< , 無等於)
     * @return
     */
    public List<Require> findByRequireStatusInAndCreatedDateGreaterThanAndCreatedDateLessThan(
            List<RequireStatusType> requireStatus,
            Date startCreateDate,
            Date endCreateDate);

    public Require findBySid(String sid);

    @Query("SELECT r.closeCode FROM #{#entityName} r WHERE r.sid = :sid ")
    public List<Boolean> findCloseCodeBySid(@Param("sid") String sid);

    @Query("SELECT r.createDep FROM #{#entityName} r WHERE r.sid = :sid ")
    public List<Org> findCreateDepBySid(@Param("sid") String sid);

    @Query("SELECT customer FROM #{#entityName} r WHERE r = :require ")
    public WorkCustomer findCustomerByRequire(@Param("require") Require require);

    @Query("SELECT hopeDate FROM #{#entityName} r WHERE r = :require ")
    public Date findHopeDateByRequire(@Param("require") Require require);

    @Query("SELECT tr FROM #{#entityName} tr JOIN tr.index idx WHERE tr.hasLink = :hasLink AND tr != :excludeRequire "
            + "AND((tr.updatedDate BETWEEN :startRelevanceDate AND :endRelevanceDate )"
            + "OR (tr.updatedDate IS NULL AND tr.createdDate BETWEEN :startRelevanceDate AND :endRelevanceDate ))"
            + "AND ((idx.fieldName = '主題' AND idx.fieldContent LIKE :text)"
            + "OR (idx.fieldName = '內容' AND idx.fieldContent LIKE :text))")
    public List<Require> findRelevanceByDateAndTextAndExcludeRequire(
            @Param("hasLink") Boolean hasLink,
            @Param("excludeRequire") Require excludeRequire,
            @Param("startRelevanceDate") Date startRelevanceDate,
            @Param("endRelevanceDate") Date endRelevanceDate,
            @Param("text") String text);

    @Query("SELECT r FROM #{#entityName} r WHERE r in :requires ")
    public List<Require> findRelevanceByRequire(@Param("requires") List<Require> requires);

    @Query(name = "findSabaByCondition", nativeQuery = true)
    public List<SabaQueryResult> findSabaByCondition(
            @Param("startDate") Date startDate,
            @Param("endDate") Date endDate,
            @Param("searchText") String searchText,
            @Param("categorySid") String categorySid,
            @Param("ncsDone") String ncsDone,
            @Param("hasL1Sids") Boolean hasL1Sids,
            @Param("hasL2Sids") Boolean hasL2Sids,
            @Param("hasL3Sids") Boolean hasL3Sids,
            @Param("bCategorySids") List<String> bCategorySids,
            @Param("mCategorySids") List<String> mCategorySids,
            @Param("sCategorySids") List<String> sCategorySids,
            @Param("startUpdatedDate") Date startUpdatedDate,
            @Param("endUpdatedDate") Date endUpdatedDate,
            @Param("hasUrgency") Boolean hasUrgency,
            @Param("urgencyList") List<Integer> urgencyList,
            @Param("requireNo") String requireNo,
            @Param("hasDepts") Boolean hasDepts,
            @Param("forwardDepts") List<String> forwardDepts);

    @Query("SELECT COUNT(r) AS size FROM #{#entityName} r WHERE r.createdDate BETWEEN :start AND :end AND r.requireStatus = :reqStatus")
    public Integer findTodayDraftTotalSize(@Param("start") Date start, @Param("end") Date end, @Param("reqStatus") RequireStatusType reqStatus);

    @Query("SELECT requireNo FROM #{#entityName} r WHERE r.createdDate BETWEEN :start AND :end AND r.requireStatus = :reqStatus ORDER BY r.createdDate DESC")
    public List<String> findTodayLastDraft(@Param("start") Date start, @Param("end") Date end, @Param("reqStatus") RequireStatusType reqStatus);

    @Query("SELECT requireNo FROM #{#entityName} r WHERE r.createdDate BETWEEN :start AND :end order by r.createdDate DESC")
    public List<String> findTodayLastRequire(@Param("start") Date start, @Param("end") Date end);

    @Query("SELECT COUNT(r) AS size FROM #{#entityName} r WHERE r.createdDate BETWEEN :start AND :end")
    public Integer findTodayRequireTotalSize(@Param("start") Date start, @Param("end") Date end);

    @Query("SELECT CASE WHEN (COUNT(r) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} r WHERE r.requireNo = :requireNo")
    public Boolean isExistNo(@Param("requireNo") String requireNo);

    /**
     * 查詢待檢查項目數量
     * 
     * @param checkItemTypes 檢查項目類別
     * @return 待檢查項目數量
     */
    @Query(value = ""
            + "SELECT Count(*) AS cnt "
            + "FROM  (SELECT req.require_sid "
            + "       FROM   tr_require req "
            + "              INNER JOIN tr_require_check_item ckitem "
            + "                      ON ckitem.require_sid = req.require_sid "
            + "       WHERE  1 = 1 "
            + "              AND req.require_status = 'WAIT_CHECK' "
            + "              AND req.status = 0 "
            + "              AND ckitem.status = 0 "
            + "              AND ckitem.check_date IS NULL "
            + "              AND ckitem.check_item_type IN ( :checkItemTypes ) "
            + "       GROUP  BY req.require_sid) a ", nativeQuery = true)
    public Integer queryWaitCheckCountByRequireCheckItemTypeIn(
            @Param("checkItemTypes") List<String> checkItemTypes);

    /**
     * 查詢未轉檔單據
     * 
     * @param minNO 最小案件單號
     * @param maxNO 最大案件單號
     * @return
     */
    @Query(value = ""
            + "SELECT require_no "
            + "  FROM tr_require "
            + " WHERE trns_flag_for_v70 = 0 "
            + "   AND require_no >= :minNO "
            + "   AND require_no <= :maxNO ", nativeQuery = true)
    public List<String> queryRequireNoByV70TrnsFlag(
            @Param("minNO") String minNO,
            @Param("maxNO") String maxNO);

}
