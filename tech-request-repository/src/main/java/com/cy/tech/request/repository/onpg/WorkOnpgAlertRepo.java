/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.onpg;

import com.cy.commons.vo.User;
import com.cy.tech.request.vo.onpg.WorkOnpgAlert;
import com.cy.tech.request.vo.onpg.WorkOnpgCheckRecord;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.enums.WorkSourceType;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface WorkOnpgAlertRepo extends JpaRepository<WorkOnpgAlert, String>, Serializable {

    @Query("SELECT a.sender FROM #{#entityName} a WHERE a.sourceType = :type AND a.checkRecord = :checkRecord")
    public List<User> findSenderBySourceTypeAndCheckRecord(@Param("type") WorkSourceType sourceType, @Param("checkRecord") WorkOnpgCheckRecord checkRecord);

    /**
     * tech-request-rest
     *
     * @param sourceType
     * @param readStatus
     * @param receiver
     * @param limit
     * @return
     */
    public List<WorkOnpgAlert> findBySourceTypeAndReadStatusAndReceiverOrderBySendDateDesc(WorkSourceType sourceType,
            ReadRecordType readStatus, User receiver, Pageable limit);

    /**
     * tech-request-rest
     *
     * @param sourceType
     * @param readStatus
     * @param receiver
     * @return
     */
    public List<WorkOnpgAlert> findBySourceTypeAndReadStatusAndReceiverOrderBySendDateDesc(WorkSourceType sourceType,
            ReadRecordType readStatus, User receiver);

    @Query("SELECT a FROM #{#entityName} a WHERE a.sourceType = :sourceType AND a.readStatus = :readStatus AND receiver = :receiver AND (ifnull(:startDate, null) is null or a.sendDate >= :startDate) AND (ifnull(:endDate, null) is null or a.sendDate <= :endDate)")
    public List<WorkOnpgAlert> findByReadStatusAndReceiverAndSendDate(
            @Param("sourceType") WorkSourceType sourceType,
            @Param("readStatus") ReadRecordType readStatus,
            @Param("receiver")  User receiver,
            @Param("startDate")  Date startDate,
            @Param("endDate")  Date endDate);
    
    /**
     * tech-request-rest
     *
     * @param alertSid
     * @param clickTime
     * @param readStatus
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} a SET a.clickTime = :clickTime,a.readStatus = :readStatus WHERE a.sid = :alertSid")
    public int updateClickTime(@Param("alertSid") String alertSid, @Param("clickTime") Date clickTime, @Param("readStatus") ReadRecordType readStatus);
}
