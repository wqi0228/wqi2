/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.pt;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.cy.work.common.vo.value.to.ReadRecordGroupTo;
import com.cy.work.common.enums.WorkSourceType;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface PtCheckRepo extends JpaRepository<PtCheck, String>, Serializable {

    /**
     * 原型確認流程數量
     *
     * @param require
     * @return
     */
    @Query("SELECT COUNT(r) AS size FROM #{#entityName} r WHERE r.sourceType = :sourceType AND r.sourceSid = :sourceSid")
    public Integer findVersionSizeBySource(@Param("sourceType") WorkSourceType sourceType, @Param("sourceSid") String sourceSid);

    @Query("SELECT COUNT(w) AS size FROM #{#entityName} w WHERE w.createdDate BETWEEN :start AND :end")
    public Integer findTodayTotalSize(@Param("start") Date start, @Param("end") Date end);

    @Query("SELECT ptNo FROM #{#entityName} w WHERE w.createdDate BETWEEN :start AND :end ORDER BY w.createdDate DESC")
    public List<String> findTodayLastNo(@Param("start") Date start, @Param("end") Date end);

    @Query("SELECT CASE WHEN (COUNT(w) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} w WHERE w.ptNo = :ptNo")
    public Boolean isExistNo(@Param("ptNo") String ptNo);

    @Query("SELECT COUNT(r) AS size FROM #{#entityName} r WHERE r.sourceType = :sourceType AND r.sourceNo = :sourceNo AND r.status = 1")
    public Integer findHistoryVersionSizeBySource(
            @Param("sourceType") WorkSourceType sourceType,
            @Param("sourceNo") String sourceNo);

    public List<PtCheck> findBySourceSidOrderByCreatedDateDesc(@Param("sourceSid") String sourceSid);

    @Query("SELECT w FROM #{#entityName} w WHERE w.sourceType = :sourceType AND w.sourceNo = :sourceNo AND w.status = :status ORDER BY w.version DESC")
    public List<PtCheck> findBySourceAndStatusDescVer(
            @Param("sourceType") WorkSourceType sourceType,
            @Param("sourceNo") String sourceNo,
            @Param("status") Activation status);

    @Modifying
    @Query("UPDATE #{#entityName} w SET w.readRecord = :readRecordGroup , w.readUpdateDate = :readUpdateDate WHERE w = :ptCheck")
    public int updateReadRecord(@Param("ptCheck") PtCheck ptCheck,
            @Param("readRecordGroup") ReadRecordGroupTo readRecordGroup, @Param("readUpdateDate") Date readUpdateDate);

    @Modifying
    @Query("UPDATE #{#entityName} w SET w.readRecord = :readRecordGroup , w.readUpdateDate = :readUpdateDate WHERE w.sid = :sid")
    public int updateReadRecord(@Param("sid") String sid,
            @Param("readRecordGroup") ReadRecordGroupTo readRecordGroup, @Param("readUpdateDate") Date readUpdateDate);

    @Query("SELECT w.sid FROM #{#entityName} w WHERE w.sourceType = :sourceType AND w.sourceSid = :sourceSid ORDER BY w.createdDate DESC")
    public List<String> findSidsBySourceTypeAndSourceSid(@Param("sourceType") WorkSourceType sourceType,
            @Param("sourceSid") String sourceSid);

    @Query(value = "SELECT w.pt_check_status FROM work_pt_check w "
            + "    WHERE w.pt_check_source_type = :sourceType AND w.pt_check_source_no = :sourceNo "
            + "ORDER BY w.pt_check_no DESC LIMIT 1", nativeQuery = true)
    public String findStatusBySourceDescPtNo(
            @Param("sourceType") String sourceType,
            @Param("sourceNo") String sourceNo);

    @Query("SELECT ptStatus FROM #{#entityName} w WHERE w = :ptCheck")
    public PtStatus findPtStatus(@Param("ptCheck") PtCheck ptCheck);

    public List<PtCheck> findByPtNoIn(List<String> ptNoes);

    @Query("SELECT COUNT(w) AS size FROM #{#entityName} w "
            + "WHERE w.sourceType = :sourceType AND w.sourceNo = :sourceNo AND  w.ptStatus IN :status")
    public Integer findCountBySourceTypeAndSourceNoAndStatus(
            @Param("sourceType") WorkSourceType sourceType,
            @Param("sourceNo") String sourceNo,
            @Param("status") List<PtStatus> status);

    public PtCheck findByPtNo(String ptNo);

    /**
     * 以下列欄位查詢
     * 
     * @param sourceType WorkSourceType
     * @param sourceNo   sourceNo
     * @param createDeps createDeps
     * @param inPtStatus PtStatus
     * @return 筆數
     */
    @Query("SELECT COUNT(r) AS size "
            + "FROM #{#entityName} r "
            + "WHERE r.sourceType = :sourceType "
            + "AND r.sourceNo = :sourceNo "
            + "AND r.status = 0 "
            + "AND r.createDep in (:createDeps) "
            + "AND r.ptStatus in (:inPtStatus) ")
    public Integer queryCountByCreateDepAndPtStatus(
            @Param("sourceType") WorkSourceType sourceType,
            @Param("sourceNo") String sourceNo,
            @Param("createDeps") List<Org> createDeps,
            @Param("inPtStatus") List<PtStatus> inPtStatus);

    /**
     * 以下列欄位查詢
     * 
     * @param sourceType WorkSourceType
     * @param sourceNo   sourceNo
     * @param createDeps createDeps
     * @param inPtStatus PtStatus
     * @return 符合的 PtCheck
     */
    @Query("SELECT r "
            + "FROM #{#entityName} r "
            + "WHERE r.sourceType = :sourceType "
            + "AND r.sourceNo = :sourceNo "
            + "AND r.status = 0 "
            + "AND r.createDep in (:createDeps) "
            + "AND r.ptStatus in (:inPtStatus) ")
    public List<PtCheck> queryByCreateDepAndPtStatus(
            @Param("sourceType") WorkSourceType sourceType,
            @Param("sourceNo") String sourceNo,
            @Param("createDeps") List<Org> createDeps,
            @Param("inPtStatus") List<PtStatus> inPtStatus);

    /**
     * 以下列欄位查詢
     * 
     * @param sourceType
     * @param sourceNo
     * @param inPtStatus
     * @return 筆數
     */
    @Query("SELECT COUNT(r) AS size "
            + "FROM #{#entityName} r "
            + "WHERE r.sourceType = :sourceType "
            + "AND r.sourceNo = :sourceNo "
            + "AND r.status = 0 "
            + "AND r.ptStatus in (:inPtStatus) ")
    public Integer queryCountByPtStatus(
            @Param("sourceType") WorkSourceType sourceType,
            @Param("sourceNo") String sourceNo,
            @Param("inPtStatus") List<PtStatus> inPtStatus);

    /**
     * 以下列欄位查詢
     * 
     * @param sourceType 來源
     * @param status     資料狀態
     * @param sourceNo   單號
     * @param inPtStatus 原型確認狀態
     * @return List PtCheck
     */
    public List<PtCheck> findBySourceTypeAndStatusAndSourceNoAndPtStatusIn(
            WorkSourceType sourceType,
            Activation status,
            String sourceNo,
            List<PtStatus> inPtStatus);

    /**
     * 以下列欄位查詢
     * 
     * @param sourceType 來源
     * @param status     資料狀態
     * @param sourceNo   單號
     * @param inPtStatus 原型確認狀態
     * @param createDeps 立案單位
     * @return List PtCheck
     */
    public List<PtCheck> findBySourceTypeAndStatusAndSourceNoAndPtStatusInAndCreateDepIn(
            WorkSourceType sourceType,
            Activation status,
            String sourceNo,
            List<PtStatus> inPtStatus,
            List<Org> createDeps);

    /**
     * 以下列欄位查詢
     * 
     * @param sourceType 來源
     * @param status     資料狀態
     * @param sourceNo   單號
     * @param createDeps 立案部門
     * @return
     */
    public List<PtCheck> findBySourceTypeAndStatusAndSourceNoAndCreateDepIn(
            WorkSourceType sourceType,
            Activation status,
            String sourceNo,
            List<Org> createDeps);

}
