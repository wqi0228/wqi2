/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require;

import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.AssignSendSearchInfo;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 分派通知搜尋用
 *
 * @author shaun
 */
public interface AssignSendSearchInfoRepo extends JpaRepository<AssignSendSearchInfo, String>, Serializable {

    @Query(value = "SELECT t FROM #{#entityName} t WHERE t.requireNo = :requireNo AND t.type = :type")
    public List<AssignSendSearchInfo> findByRequireNoAndType(
            @Param("requireNo") String requireNo, 
            @Param("type") AssignSendType type);

    public List<AssignSendSearchInfo> findByRequire(Require require);

    @Modifying
    @Query(value = "DELETE FROM tr_assign_send_search_info WHERE require_sid = :reqSid AND dep_sid = :depSid AND type = 0 ", nativeQuery = true)
    public int deleteAssignByReqSidAndDepSid(@Param("reqSid") String reqSid, @Param("depSid") Integer depSid);
    
    /**
     * @param reqSid
     * @return
     */
    @Modifying
    @Query(value = "DELETE FROM tr_assign_send_search_info WHERE require_sid = :requireSid", nativeQuery = true)
    public int deleteAssignByRequireSid(@Param("requireSid") String reqSid);
    
    
}
