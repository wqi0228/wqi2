/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.worktest;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.common.vo.value.to.ReadRecordGroupTo;
import com.cy.work.common.enums.WorkSourceType;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface WorkTestInfoRepo extends JpaRepository<WorkTestInfo, String>, Serializable {

    @Query("SELECT COUNT(w) AS size FROM #{#entityName} w WHERE w.createdDate BETWEEN :start AND :end")
    public Integer findTodayTotalSize(@Param("start") Date start, @Param("end") Date end);

    @Query("SELECT testinfoNo FROM #{#entityName} w WHERE w.createdDate BETWEEN :start AND :end ORDER BY w.createdDate DESC")
    public List<String> findTodayLastNo(@Param("start") Date start, @Param("end") Date end);

    @Query("SELECT CASE WHEN (COUNT(w) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} w WHERE w.testinfoNo = :testinfoNo")
    public Boolean isExistNo(@Param("testinfoNo") String testinfoNo);

    public List<WorkTestInfo> findBySourceSidOrderByCreatedDateDesc(@Param("sourceSid") String sourceSid);

    @Query("SELECT w FROM #{#entityName} w WHERE w.sourceNo = :requireNo ORDER BY w.testDate DESC, w.testinfoNo DESC")
    public List<WorkTestInfo> findByRequireNo(@Param("requireNo") String requireNo);

    @Modifying
    @Query("UPDATE #{#entityName} w SET w.readRecord = :readRecordGroup , w.readUpdateDate = :readUpdateDate WHERE w = :testInfo")
    public int updateReadRecord(@Param("testInfo") WorkTestInfo testInfo,
            @Param("readRecordGroup") ReadRecordGroupTo readRecordGroup, @Param("readUpdateDate") Date readUpdateDate);

    @Modifying
    @Query("UPDATE #{#entityName} w SET w.readRecord = :readRecordGroup , w.readUpdateDate = :readUpdateDate WHERE w.sid = :sid")
    public int updateReadRecord(@Param("sid") String sid,
            @Param("readRecordGroup") ReadRecordGroupTo readRecordGroup, @Param("readUpdateDate") Date readUpdateDate);

    @Modifying
    @Query("UPDATE #{#entityName} w SET w.fbId = :fbId , w.updatedUser = :updatedUser, w.updatedDate = :updatedDate WHERE w = :testInfo")
    public int updateFbId(@Param("testInfo") WorkTestInfo testInfo,
            @Param("fbId") Integer fbId, @Param("updatedUser") User updatedUser, @Param("updatedDate") Date updatedDate);

    @Query("SELECT w.sid FROM #{#entityName} w WHERE w.sourceType = :sourceType AND w.sourceSid = :sourceSid ORDER BY w.createdDate DESC")
    public List<String> findSidsBySourceTypeAndSourceSid(@Param("sourceType") WorkSourceType sourceType,
            @Param("sourceSid") String sourceSid);

    @Query("SELECT testinfoStatus FROM #{#entityName} w WHERE w = :testinfo")
    public WorkTestStatus findInfoStatus(@Param("testinfo") WorkTestInfo testinfo);

    public List<WorkTestInfo> findByTestinfoNoIn(List<String> testinfoNoes);

    @Query("SELECT COUNT(w) AS size FROM #{#entityName} w "
            + "WHERE w.sourceType = :sourceType AND w.sourceNo = :sourceNo AND  w.testinfoStatus IN :status")
    public Integer findCountBySourceTypeAndSourceNoAndStatus(
            @Param("sourceType") WorkSourceType sourceType,
            @Param("sourceNo") String sourceNo,
            @Param("status") List<WorkTestStatus> status);

    public WorkTestInfo findByTestinfoNo(String testInfoNo);

    @Query("SELECT w FROM #{#entityName} w WHERE w.testDate = date(sysdate()) and w.commitStatus = 'COMMITED' and w.testinfoStatus = :status")
    public List<WorkTestInfo> findByWorkTestStatus(@Param("status") WorkTestStatus status);

    /**
     * @param sourceType
     * @param sourceNo
     * @param createDep
     * @param inPtStatus
     * @return
     */
    @Query("SELECT COUNT(r) AS size "
            + "FROM #{#entityName} r "
            + "WHERE r.sourceType = :sourceType "
            + "AND r.sourceNo = :sourceNo "
            + "AND r.status = 0 "
            + "AND r.createDep in (:createDeps) "
            + "AND r.testinfoStatus in (:inTestinfoStatus) ")
    public Integer queryCountByCreateDepAndTestinfoStatus(
            @Param("sourceType") WorkSourceType sourceType,
            @Param("sourceNo") String sourceNo,
            @Param("createDeps") List<Org> createDeps,
            @Param("inTestinfoStatus") List<WorkTestStatus> inTestinfoStatus);
    
    /**
     * @param sourceType
     * @param sourceNo
     * @param createDep
     * @param inPtStatus
     * @return
     */
    @Query("SELECT r AS size "
            + "FROM #{#entityName} r "
            + "WHERE r.sourceType = :sourceType "
            + "AND r.sourceNo = :sourceNo "
            + "AND r.status = 0 "
            + "AND r.createDep in (:createDeps) "
            + "AND r.testinfoStatus in (:inTestinfoStatus) ")
    public List<WorkTestInfo> queryByCreateDepAndTestinfoStatus(
            @Param("sourceType") WorkSourceType sourceType,
            @Param("sourceNo") String sourceNo,
            @Param("createDeps") List<Org> createDeps,
            @Param("inTestinfoStatus") List<WorkTestStatus> inTestinfoStatus);


    /**
     * @param sourceType
     * @param sourceNo
     * @param createDep
     * @param inPtStatus
     * @return
     */
    @Query("SELECT COUNT(r) AS size "
            + "FROM #{#entityName} r "
            + "WHERE r.sourceType = :sourceType "
            + "AND r.sourceNo = :sourceNo "
            + "AND r.status = 0 "
            + "AND r.testinfoStatus in (:inTestinfoStatus) ")
    public Integer queryCountByTestinfoStatus(
            @Param("sourceType") WorkSourceType sourceType,
            @Param("sourceNo") String sourceNo,
            @Param("inTestinfoStatus") List<WorkTestStatus> inTestinfoStatus);

    /**
     * 以下列欄位查詢
     * 
     * @param sourceType 來源
     * @param status     資料狀態
     * @param sourceNo   單號
     * @param createDeps 立案部門
     * @return
     */
    public List<WorkTestInfo> findBySourceTypeAndStatusAndSourceNoAndCreateDepIn(
            WorkSourceType sourceType,
            Activation ststus,
            String sourceNo,
            List<Org> createDeps);
}
