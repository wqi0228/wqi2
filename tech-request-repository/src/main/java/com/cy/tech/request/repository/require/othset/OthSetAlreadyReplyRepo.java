/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require.othset;

import com.cy.tech.request.vo.require.othset.OthSetAlreadyReply;
import com.cy.tech.request.vo.require.othset.OthSetReply;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface OthSetAlreadyReplyRepo extends JpaRepository<OthSetAlreadyReply, String>, Serializable {

    public List<OthSetAlreadyReply> findByReplyOrderByUpdateDateDesc(OthSetReply reply);

    /**
     * 查找回覆by history sid <BR/> 無法接受空集合做參數值
     *
     * @param historySids
     * @return
     */
    @Query(value = "SELECT * FROM tr_os_reply_and_reply r "
              + "    JOIN tr_os_history h ON r.os_reply_and_reply_sid = h.os_reply_and_reply_sid "
              + "    WHERE h.os_history_sid IN (:historySids) ORDER BY r.reply_udt DESC ", nativeQuery = true)
    public List<OthSetAlreadyReply> findReplyByHistoryInOrderByUpdateDateDesc(@Param("historySids") List<String> historySids);
}
