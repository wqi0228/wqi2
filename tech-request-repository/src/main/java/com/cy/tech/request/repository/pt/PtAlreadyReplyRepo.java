/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.pt;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cy.tech.request.vo.pt.PtAlreadyReply;
import com.cy.tech.request.vo.pt.PtReply;

/**
 *
 * @author shaun
 */
public interface PtAlreadyReplyRepo extends JpaRepository<PtAlreadyReply, String>, Serializable {

    public List<PtAlreadyReply> findByReplyOrderByUpdateDateDesc(PtReply reply);

    /**
     * 查找回覆by history sid <BR/> 無法接受空集合做參數值
     *
     * @param historySids
     * @return
     */
    @Query(value = "SELECT * FROM work_pt_already_reply r "
              + "    JOIN work_pt_check_history h ON r.already_reply_sid = h.already_reply_sid "
              + "    WHERE h.pt_check_history_sid IN (:historySids) ORDER BY r.reply_udt DESC ", nativeQuery = true)
    public List<PtAlreadyReply> findReplyByHistoryInOrderByUpdateDateDesc(@Param("historySids") List<String> historySids);

}
