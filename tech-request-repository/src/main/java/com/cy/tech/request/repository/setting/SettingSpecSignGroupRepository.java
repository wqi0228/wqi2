package com.cy.tech.request.repository.setting;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.setting.SettingSpecSignGroup;

/**
 * 設定：『需求單位申請流程』特殊簽核群組
 * @author allen 
 */
public interface SettingSpecSignGroupRepository extends JpaRepository<SettingSpecSignGroup, String>, Serializable {

    /**
     * 以下列條件查詢
     * 
     * @param sid sid
     * @return SettingSpecSignGroup SettingSpecSignGroup
     */
    public SettingSpecSignGroup findBySid(String sid);
    
    /**
     * 以下列條件查詢
     * @param compID 公司ID
     * @param status 資料狀態
     * @return SettingSpecSignGroup List
     */
    public List<SettingSpecSignGroup> findByCompIDAndStatus(String compID, Activation status);
}
