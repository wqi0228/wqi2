/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require.othset;

import com.cy.commons.vo.User;
import com.cy.tech.request.vo.enums.OthSetAlertType;
import com.cy.tech.request.vo.require.othset.OthSet;
import com.cy.tech.request.vo.require.othset.OthSetAlert;
import com.cy.tech.request.vo.require.othset.OthSetReply;
import com.cy.work.common.enums.ReadRecordType;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface OthSetAlertRepo extends JpaRepository<OthSetAlert, String>, Serializable {

    @Query("SELECT a.sender FROM #{#entityName} a WHERE a.reply = :reply")
    public List<User> findSenderByReply(@Param("reply") OthSetReply reply);

    @Modifying
    @Query("UPDATE #{#entityName} a SET a.readStatus = :readStatus WHERE a.othset = :othset AND a.type = :type ")
    public int updateReadStatus(@Param("othset") OthSet othset, @Param("type") OthSetAlertType type, @Param("readStatus") ReadRecordType readStatus);

    /**
     * tech-request-rest
     *
     * @param readStatus
     * @param receiver
     * @param limit
     * @return
     */
    public List<OthSetAlert> findByReadStatusAndReceiverOrderBySendDateDesc(ReadRecordType readStatus, User receiver, Pageable limit);

    /**
     * tech-request-rest
     *
     * @param readStatus
     * @param receiver
     * @return
     */
    public List<OthSetAlert> findByReadStatusAndReceiverOrderBySendDateDesc(ReadRecordType readStatus, User receiver);

    @Query("SELECT a FROM #{#entityName} a WHERE a.readStatus = :readStatus AND receiver = :receiver AND (ifnull(:startDate, null) is null or a.sendDate >= :startDate) AND (ifnull(:endDate, null) is null or a.sendDate <= :endDate)")
    public List<OthSetAlert> findByReadStatusAndReceiverAndSendDate(
            @Param("readStatus") ReadRecordType readStatus,
            @Param("receiver")  User receiver,
            @Param("startDate")  Date startDate,
            @Param("endDate")  Date endDate);
    
    /**
     * tech-request-rest
     *
     * @param alertSid
     * @param clickTime
     * @param readStatus
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} a SET a.clickTime = :clickTime,a.readStatus = :readStatus WHERE a.sid = :alertSid")
    public int updateClickTime(@Param("alertSid") String alertSid, @Param("clickTime") Date clickTime, @Param("readStatus") ReadRecordType readStatus);
}
