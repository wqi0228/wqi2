/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require;

import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireUnitSignInfo;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface RequireUnitSignInfoRepository extends JpaRepository<RequireUnitSignInfo, String>, Serializable {

    public RequireUnitSignInfo findByRequire(Require require);

    @Query(value = "SELECT * FROM tr_require_manager_sign_info rmsi "
              + "     WHERE rmsi.bpm_can_signed_id_list like '%\":userId\"%' "
              + "       AND rmsi.paper_code IN ('PAPER000','PAPER001','PAPER002','PAPER004');", nativeQuery = true)
    public List<RequireUnitSignInfo> findUnSignByUserId(@Param("user") String userId);

    public RequireUnitSignInfo findByRequireNo(String requireNo);

    public RequireUnitSignInfo findByBpmInstanceId(String bpmInstanceId);

    @Query(value = "SELECT * FROM tr_require_manager_sign_info rmsi "
              + "     WHERE rmsi.paper_code IN ('PAPER000','PAPER001','PAPER002','PAPER004');", nativeQuery = true)
    public List<RequireUnitSignInfo> findRunningProcess();

}
