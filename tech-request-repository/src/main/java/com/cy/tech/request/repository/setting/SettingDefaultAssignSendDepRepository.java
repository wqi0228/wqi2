package com.cy.tech.request.repository.setting;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.setting.asdep.SettingDefaultAssignSendDep;

/**
 * @author allen 設定:檢查確認預設分派/通知單位
 */
public interface SettingDefaultAssignSendDepRepository extends JpaRepository<SettingDefaultAssignSendDep, String>, Serializable {

    /**
     * 以下列條件查詢
     * 
     * @param sid sid
     * @return SettingDefaultAssignSendDep
     */
    public SettingDefaultAssignSendDep findBySid(String sid);

    /**
     * 以下列條件查詢
     * 
     * @param smallCategorySids 小類 SID
     * @return SettingDefaultAssignSendDep
     */
    public List<SettingDefaultAssignSendDep> findBySmallCategorySidIn(List<String> smallCategorySids);

    /**
     * 以下列條件查詢
     * 
     * @param smallCategorySid 小類 SID
     * @param checkItemType    檢查項目
     * @param assignSendType   分派/通知類別
     * @return
     */
    public SettingDefaultAssignSendDep findBySmallCategorySidAndCheckItemTypeAndAssignSendType(
            String smallCategorySid,
            RequireCheckItemType checkItemType,
            AssignSendType assignSendType);

}
