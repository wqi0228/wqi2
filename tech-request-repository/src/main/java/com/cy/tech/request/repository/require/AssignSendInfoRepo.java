/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.require.AssignSendInfo;
import com.cy.tech.request.vo.value.to.SetupInfoTo;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 分派通知
 *
 * @author shaun
 */
public interface AssignSendInfoRepo extends JpaRepository<AssignSendInfo, String>, Serializable {

    @Query(value = "SELECT ai.* FROM tr_assign_send_info ai "
            + "   JOIN (SELECT MAX(ai.create_dt) as maxTime FROM tr_assign_send_info ai GROUP BY ai.require_sid) "
            + "    AS temp ON ai.create_dt IN (temp.maxTime) "
            + "    WHERE ai.require_sid IN (:requireSids) "
            + "      AND ai.type = 0 ", nativeQuery = true)
    public List<AssignSendInfo> findBackstageModifyItems(@Param("requireSids") List<String> requireSids);

    public List<AssignSendInfo> findByRequireSid(String requireSid);

    /**
     * @param requireSid
     * @param status
     * @return
     */
    public List<AssignSendInfo> findByRequireSidAndStatusOrderByCreatedDateDesc(String requireSid, Activation status);

    /**
     * 以複數需求單 SID + 通知類別查詢
     * 
     * @param requireSids
     * @param status
     * @return
     */
    public List<AssignSendInfo> findByRequireSidInAndTypeAndStatus(List<String> requireSids, AssignSendType type, Activation status);

    public List<AssignSendInfo> findByRequireSidAndTypeAndStatus(String requireSid, AssignSendType type, Activation status);

    public List<AssignSendInfo> findByRequireSidAndTypeAndStatusOrderByCreatedDateDesc(String requireSid, AssignSendType type, Activation status);

    public List<AssignSendInfo> findByRequireSidAndTypeOrderByCreatedDateDesc(String requireSid, AssignSendType type);

    /**
     * @param sids
     * @return
     */
    public List<AssignSendInfo> findBySidIn(List<String> sids);

    /**
     * @param requireSid
     * @param type
     * @param status
     * @return
     */
    public AssignSendInfo findTopByRequireSidAndTypeAndStatusOrderByCreatedDateDesc(String requireSid, AssignSendType type, Activation status);

    @Modifying
    @Query("UPDATE #{#entityName} ai SET ai.info  = :info "
            + " WHERE ai = :ai")
    public int updateSetupInfo(@Param("ai") AssignSendInfo ai,
            @Param("info") SetupInfoTo info);

    /**
     * 刪除需求單相關的分派/通知檔
     * 
     * @param requireSid
     */
    @Modifying
    @Query("   DELETE "
            + "  FROM #{#entityName} e "
            + " WHERE e.requireSid = :requireSid ")
    public void deleteByRequire(
            @Param("requireSid") String requireSid);

    @Modifying
    @Query("   DELETE "
            + "  FROM #{#entityName} e "
            + " WHERE e.requireSid = :requireSid "
            + "   AND e.type = :type ")
    public void deleteByRequireAndType(
            @Param("requireSid") String requireSid, @Param("type") AssignSendType type);

}
