/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.category;

import com.cy.tech.request.vo.category.TROtherCategory;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author kasim
 */
public interface TROtherCategoryRepository extends JpaRepository<TROtherCategory, String>, Serializable {

    @Query("SELECT CASE WHEN (COUNT(o.sid) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} o "
            + "WHERE o.name = :name "
            + "AND o.sid != :excludeSid")
    public Boolean isAnyUseByNameAndExcludeSid(
            @Param("name") String name,
            @Param("excludeSid") String excludeSid);

    @Query("SELECT CASE WHEN (COUNT(o.sid) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} o "
            + "WHERE o.name = :name ")
    public Boolean isAnyUseByName(
            @Param("name") String name);
}
