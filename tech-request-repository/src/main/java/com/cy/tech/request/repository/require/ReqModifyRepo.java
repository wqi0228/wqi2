/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.tech.request.vo.enums.CateConfirmType;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireFinishCodeType;
import com.cy.tech.request.vo.enums.RequireFinishMethodType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireUnitSignInfo;
import com.cy.tech.request.vo.value.to.RequireContentTo;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.customer.vo.WorkCustomer;
import java.io.Serializable;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * 負責需求單特定欄位更新
 *
 * @author Shaun
 */
public interface ReqModifyRepo extends JpaRepository<Require, String>, Serializable {

    @Modifying
    @Query("UPDATE #{#entityName} r SET "
            + "                       r.requireFinishUsr = null   ,"
            + "                       r.updatedUser = :updateUsr ,  "
            + "                       r.updatedDate = :upDt "
            + " WHERE r = :require")
    public int clearReqFinishUsr(@Param("require") Require require,
            @Param("updateUsr") User updateUsr, @Param("upDt") Date upDt);

    @Modifying
    @Query("UPDATE #{#entityName} r SET r.hasTrace  = :hasTrace ,"
            + "                       r.author = :author ,"
            + "                       r.updatedUser = :upUsr ,"
            + "                       r.updatedDate = :upDt "
            + " WHERE r = :require")
    public int updateAuthor(@Param("require") Require require,
            @Param("hasTrace") Boolean hasTrace,
            @Param("author") WorkCustomer author,
            @Param("upUsr") User upUsr, @Param("upDt") Date upDt);

    /**
     * 其它設定資訊更新
     *
     * @param require
     * @param hasOthSet
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} r SET r.hasOthSet = :hasOthSet "
            + "WHERE r = :require")
    public int updateByHasOthset(@Param("require") Require require,
            @Param("hasOthSet") Boolean hasOthSet);

    /**
     * 原型確認更新
     *
     * @param require
     * @param hasTestInfo
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} r SET r.hasPrototype = :hasPrototype ,"
            + "                       r.redoCode = :redoCode "
            + "WHERE r = :require")
    public int updateByHasPrototype(@Param("require") Require require,
            @Param("hasPrototype") Boolean hasPrototype,
            @Param("redoCode") Boolean redoCode);

    /**
     * 送測更新
     *
     * @param require
     * @param hasTestInfo
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} r SET r.hasTestInfo = :hasTestInfo "
            + "WHERE r = :require")
    public int updateByHasSendTest(@Param("require") Require require,
            @Param("hasTestInfo") Boolean hasTestInfo);

    /**
     * 原型確認更新(功能確認)
     *
     * @param require
     * @param hasTrace
     * @param functionOkCode
     * @param functionOkDate
     * @param upUsr
     * @param upDt
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} r SET r.hasTrace = :hasTrace ,"
            + "                       r.functionOkCode = :functionOkCode ,"
            + "                       r.functionOkDate = :functionOkDate ,"
            + "                       r.updatedUser = :upUsr ,"
            + "                       r.updatedDate = :upDt "
            + " WHERE r = :require")
    public int updateByPrototypeFunOk(@Param("require") Require require,
            @Param("hasTrace") Boolean hasTrace,
            @Param("functionOkCode") Boolean functionOkCode,
            @Param("functionOkDate") Date functionOkDate,
            @Param("upUsr") User upUsr, @Param("upDt") Date upDt);

    @Modifying
    @Query("UPDATE #{#entityName} r SET r.hasTrace  = :hasTrace ,"
            + "                       r.customer = :customer ,"
            + "                       r.author = :author ,"
            + "                       r.content = :content ,"
            + "                       r.updatedUser = :upUsr ,"
            + "                       r.updatedDate = :upDt "
            + " WHERE r = :require")
    public int updateCustomer(
            @Param("require") Require require,
            @Param("hasTrace") Boolean hasTrace,
            @Param("customer") WorkCustomer customer,
            @Param("author") WorkCustomer author,
            @Param("content") RequireContentTo content,
            @Param("upUsr") User upUsr, @Param("upDt") Date upDt);

    /**
     * 更新FB
     *
     * @param require
     * @param urgency
     * @param upUsr
     * @param upDt
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} r SET r.hasFbRecord = :hasFbRecord ,"
            + "                       r.updatedUser = :upUsr   ,"
            + "                       r.updatedDate = :upDt "
            + " WHERE r = :require")
    public int updateFbRecord(
            @Param("require") Require require,
            @Param("hasFbRecord") Boolean hasFbRecord,
            @Param("upUsr") User upUsr,
            @Param("upDt") Date upDt);

    /**
     * 需求單位流程 - 核准
     *
     * @param require
     * @param hasTrace
     * @param requireStatus
     * @param readReason
     * @param requireEstablishDate
     * @param upUsr
     * @param upDt
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} r SET r.hasTrace = :hasTrace ,"
            + "                       r.requireStatus = :requireStatus ,"
            + "                       r.readReason = :readReason ,"
            + "                       r.requireEstablishDate = :requireEstablishDate ,"
            + "                       r.updatedUser = :upUsr ,"
            + "                       r.updatedDate = :upDt "
            + " WHERE r = :require")
    public int updateFlowApproveByReqUnit(
            @Param("require") Require require,
            @Param("hasTrace") Boolean hasTrace,
            @Param("requireStatus") RequireStatusType requireStatus,
            @Param("readReason") ReqToBeReadType readReason,
            @Param("requireEstablishDate") Date requireEstablishDate,
            @Param("upUsr") User upUsr,
            @Param("upDt") Date upDt);

    /**
     * 需求單位流程 - 作廢
     *
     * @param require
     * @param hasTrace
     * @param status
     * @param requireStatus
     * @param readReason
     * @param invalidCode
     * @param upUsr
     * @param upDt
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} r SET r.hasTrace = :hasTrace ,"
            + "                       r.status = :status ,"
            + "                       r.requireStatus = :requireStatus ,"
            + "                       r.readReason = :readReason ,"
            + "                       r.invalidCode = :invalidCode ,"
            + "                       r.updatedUser = :upUsr ,"
            + "                       r.updatedDate = :upDt "
            + " WHERE r = :require")
    public int updateFlowInvaildByReqUnit(
            @Param("require") Require require,
            @Param("hasTrace") Boolean hasTrace,
            @Param("status") Activation status,
            @Param("requireStatus") RequireStatusType requireStatus,
            @Param("readReason") ReqToBeReadType readReason,
            @Param("invalidCode") Boolean invalidCode,
            @Param("upUsr") User upUsr,
            @Param("upDt") Date upDt);

    /**
     * 更新轉寄部門
     * 
     * @param require
     * @param hasForwardDep
     * @param upUsr
     * @param upDt
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} r "
            + "SET r.hasForwardDep = :hasForwardDep ,"
            + "    r.updatedUser = :upUsr ,"
            + "    r.updatedDate = :upDt "
            + " WHERE r = :require")
    public int updateForwardDep(
            @Param("require") Require require,
            @Param("hasForwardDep") Boolean hasForwardDep,
            @Param("upUsr") User upUsr,
            @Param("upDt") Date upDt);

    /**
     * 更新轉寄個人
     * 
     * @param require
     * @param hasForwardMember
     * @param upUsr
     * @param upDt
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} r "
            + "SET r.hasForwardMember = :hasForwardMember ,"
            + "    r.updatedUser = :upUsr ,"
            + "    r.updatedDate = :upDt "
            + " WHERE r = :require")
    public int updateForwardMember(
            @Param("require") Require require,
            @Param("hasForwardMember") Boolean hasForwardMember,
            @Param("upUsr") User upUsr,
            @Param("upDt") Date upDt);

    @Modifying
    @Query("UPDATE #{#entityName} r SET r.hasAssign  = :hasAssign ,"
            + "                       r.updatedUser = :upUsr ,"
            + "                       r.updatedDate = :upDt "
            + " WHERE r.sid = :requireSid")
    @Transactional(rollbackFor = Exception.class)
    public int updateHasAssign(
            @Param("requireSid") String requireSid,
            @Param("hasAssign") Boolean hasAssign,
            @Param("upUsr") User upUsr,
            @Param("upDt") Date upDt);

    /**
     * 更新是否有追蹤
     *
     * @param require
     * @param hasTrace
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} r SET r.hasTrace = :hasTrace "
            + "WHERE r = :require")
    public int updateHasTrace(
            @Param("require") Require require,
            @Param("hasTrace") Boolean hasTrace);

    /**
     * 期望完成日更新
     *
     * @param require
     * @param readReason
     * @param user
     * @param upDt
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} r SET r.hasTrace = :hasTrace ,"
            + "                       r.readReason  = :readReason ,"
            + "                       r.hopeDate    = :hopeDate ,"
            + "                       r.updatedUser = :upUsr ,"
            + "                       r.updatedDate = :upDt "
            + " WHERE r = :require")
    public int updateHopeDate(@Param("require") Require require,
            @Param("hasTrace") Boolean hasTrace,
            @Param("readReason") ReqToBeReadType readReason,
            @Param("hopeDate") Date hopeDate,
            @Param("upUsr") User upUsr, @Param("upDt") Date upDt);

    /**
     * 需求單最後執行成員、執行時間狀態更新
     *
     * @param require
     * @param requireStatus
     * @param upUsr
     * @param upDt
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} r SET r.updatedUser = :upUsr ,"
            + "                       r.updatedDate = :upDt "
            + " WHERE r = :require")
    public int updateLastExecutor(@Param("require") Require require,
            @Param("upUsr") User upUsr, @Param("upDt") Date upDt);

    /**
     * 更新待閱原因
     *
     * @param require
     * @param readReason
     * @param upUsr
     * @param upDt
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} r SET r.readReason = :readReason,"
            + "                       r.updatedUser = :upUsr,"
            + "                       r.updatedDate = :upDt "
            + "WHERE r = :require")
    public int updateReadReasonAndRecord(
            @Param("require") Require require,
            @Param("readReason") ReqToBeReadType readReason,
            @Param("upUsr") User upUsr,
            @Param("upDt") Date upDt);

    /**
     * 退件通知更新
     *
     * @param require
     * @param hasTrace
     * @param requireStatus
     * @param readReason
     * @param backCode
     * @param upUsr
     * @param upDt
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} r SET r.hasTrace = :hasTrace ,"
            + "                       r.requireStatus = :requireStatus ,"
            + "                       r.readReason = :readReason ,"
            + "                       r.backCode = :backCode ,"
            + "                       r.updatedUser = :upUsr ,"
            + "                       r.updatedDate = :upDt "
            + " WHERE r = :require")
    public int updateRejectNotify(@Param("require") Require require,
            @Param("hasTrace") Boolean hasTrace,
            @Param("requireStatus") RequireStatusType requireStatus,
            @Param("readReason") ReqToBeReadType readReason,
            @Param("backCode") Boolean backCode,
            @Param("upUsr") User upUsr, @Param("upDt") Date upDt);

    /**
     * 結案更新
     *
     * @param require
     * @param hasTrace
     * @param closeCode
     * @param closeDate
     * @param closeUser
     * @param requireStatus
     * @param readReason
     * @param upUsr
     * @param upDt
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} r SET r.hasTrace = :hasTrace ,"
            + "                       r.closeCode = :closeCode ,"
            + "                       r.closeDate = :closeDate ,"
            + "                       r.closeUser = :closeUser ,"
            + "                       r.requireStatus = :requireStatus ,"
            + "                       r.readReason = :readReason ,"
            + "                       r.updatedUser = :upUsr ,"
            + "                       r.updatedDate = :upDt "
            + " WHERE r = :require")
    public int updateReqClose(@Param("require") Require require,
            @Param("hasTrace") Boolean hasTrace,
            @Param("closeCode") Boolean closeCode,
            @Param("closeDate") Date closeDate,
            @Param("closeUser") User closeUser,
            @Param("requireStatus") RequireStatusType requireStatus,
            @Param("readReason") ReqToBeReadType readReason,
            @Param("upUsr") User upUsr, @Param("upDt") Date upDt);

    /**
     * 需求完成狀態更新
     *
     * @param require
     * @param hasTrace
     * @param finishCode
     * @param finishMethod
     * @param finishDate
     * @param requireStatus
     * @param readReason
     * @param upUsr
     * @param upDt
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} r SET r.hasTrace = :hasTrace, "
            + "                       r.finishCode = :finishCode, "
            + "                       r.finishMethod = :finishMethod, "
            + "                       r.finishDate = :finishDate, "
            + "                       r.requireStatus = :requireStatus, "
            + "                       r.readReason = :readReason, "
            + "                       r.updatedUser = :upUsr, "
            + "                       r.updatedDate = :upDt, "
            + "                       r.requireFinishUsr = :requireFinishUsr "
            + " WHERE r = :require")
    @Transactional(rollbackFor = Exception.class)
    public int updateReqComplete(
            @Param("require") Require require,
            @Param("hasTrace") Boolean hasTrace,
            @Param("finishCode") RequireFinishCodeType finishCode,
            @Param("finishMethod") RequireFinishMethodType finishMethod,
            @Param("finishDate") Date finishDate,
            @Param("requireFinishUsr") User requireFinishUsr,
            @Param("requireStatus") RequireStatusType requireStatus,
            @Param("readReason") ReqToBeReadType readReason,
            @Param("upUsr") User upUsr,
            @Param("upDt") Date upDt);

    /**
     * 更新需求完成人員的資訊
     * 
     * @param require
     * @param finishUsr
     * @param upDt
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} r SET "
            + "                       r.requireFinishUsr = :finishUsr   ,"
            + "                       r.updatedUser = :finishUsr ,  "
            + "                       r.updatedDate = :upDt "
            + " WHERE r = :require")
    public int updateReqFinishUsr(@Param("require") Require require,
            @Param("finishUsr") User finishUsr, @Param("upDt") Date upDt);

    /**
     * 反需求完成更新
     * 
     * @param require
     * @param finishCode
     * @param finishMethod
     * @param finishDate
     * @param requireStatus
     * @param upUsr
     * @param upDt
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} r SET "
            + "                       r.finishCode = :finishCode ,"
            + "                       r.finishMethod = :finishMethod ,"
            + "                       r.finishDate = :finishDate ,"
            + "                       r.requireStatus = :requireStatus ,"
            + "                       r.updatedUser = :upUsr ,"
            + "                       r.updatedDate = :upDt "
            + " WHERE r = :require")
    public int updateReqRollbackFinish(@Param("require") Require require,
            @Param("finishCode") RequireFinishCodeType finishCode,
            @Param("finishMethod") RequireFinishMethodType finishMethod,
            @Param("finishDate") Date finishDate,
            @Param("requireStatus") RequireStatusType requireStatus,
            @Param("upUsr") User upUsr, @Param("upDt") Date upDt);

    /**
     * 需求單狀態更新
     *
     * @param require
     * @param requireStatus
     * @param upUsr
     * @param upDt
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} r SET r.requireStatus = :requireStatus ,"
            + "                       r.updatedUser = :upUsr ,"
            + "                       r.updatedDate = :upDt "
            + " WHERE r = :require")
    public int updateReqStatus(@Param("require") Require require,
            @Param("requireStatus") RequireStatusType requireStatus,
            @Param("upUsr") User upUsr, @Param("upDt") Date upDt);

    /**
     * 緊急度更新
     *
     * @param require
     * @param urgency
     * @param user
     * @param upDt
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} r SET r.urgency     = :urgency ,"
            + "                       r.updatedUser = :upUsr   ,"
            + "                       r.updatedDate = :upDt "
            + " WHERE r = :require")
    public int updateUrgency(
            @Param("require") Require require,
            @Param("urgency") UrgencyType urgency,
            @Param("upUsr") User upUsr, @Param("upDt") Date upDt);

    /**
     * 更新 <br/>
     * 1.需求單位是否簽核註記 <br/>
     * 2.需求製作進度
     * 
     * @param requireSid     需求單 sid
     * @param hasReqUnitSign 需求單位是否簽核
     * @param requireStatus  製作進度
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} e "
            + "   SET e.hasReqUnitSign = :hasReqUnitSign, "
            + "       e.reqUnitSign = :reqUnitSign, "
            + "       e.requireStatus = :requireStatus "
            + " WHERE e.sid = :requireSid")
    public int updateHasReqUnitSignAndRequireStatus(
            @Param("requireSid") String requireSid,
            @Param("hasReqUnitSign") boolean hasReqUnitSign,
            @Param("reqUnitSign") RequireUnitSignInfo reqUnitSign,
            @Param("requireStatus") RequireStatusType requireStatus);

    /**
     * 需求單位流程 - 核准 <BR/>
     * 內部類別
     *
     * @param require
     * @param hasTrace
     * @param hasOnpg
     * @param hasAssign
     * @param requireStatus
     * @param requireEstablishDate
     * @param categoryConfirmCode
     * @param upUsr
     * @param upDt
     * @return
     */
    @Modifying
    @Query("UPDATE #{#entityName} r SET r.hasTrace = :hasTrace ,"
            + "                       r.hasOnpg = :hasOnpg ,"
            + "                       r.hasAssign = :hasAssign ,"
            + "                       r.requireStatus = :requireStatus ,"
            + "                       r.requireEstablishDate = :requireEstablishDate ,"
            + "                       r.categoryConfirmCode = :categoryConfirmCode ,"
            + "                       r.updatedUser = :upUsr ,"
            + "                       r.updatedDate = :upDt "
            + " WHERE r = :require")
    public int updateFlowApproveByReqUnitForInternal(
            @Param("require") Require require,
            @Param("hasTrace") Boolean hasTrace,
            @Param("hasOnpg") Boolean hasOnpg,
            @Param("hasAssign") Boolean hasAssign,
            @Param("requireStatus") RequireStatusType requireStatus,
            @Param("requireEstablishDate") Date requireEstablishDate,
            @Param("categoryConfirmCode") CateConfirmType categoryConfirmCode,
            @Param("upUsr") User upUsr,
            @Param("upDt") Date upDt);

}
