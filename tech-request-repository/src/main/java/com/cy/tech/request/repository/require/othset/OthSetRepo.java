/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require.othset;

import com.cy.commons.vo.Org;
import com.cy.tech.request.vo.enums.OthSetStatus;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.othset.OthSet;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface OthSetRepo extends JpaRepository<OthSet, String>, Serializable {

	@Query("SELECT COUNT(w) AS size FROM #{#entityName} w WHERE w.createdDate BETWEEN :start AND :end")
	public Integer findTodayTotalSize(@Param("start") Date start, @Param("end") Date end);

	@Query("SELECT osNo FROM #{#entityName} w WHERE w.createdDate BETWEEN :start AND :end ORDER BY w.createdDate DESC")
	public List<String> findTodayLastNo(@Param("start") Date start, @Param("end") Date end);

	@Query("SELECT CASE WHEN (COUNT(w) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} w WHERE w.osNo = :osNo")
	public Boolean isExistNo(@Param("osNo") String osNo);

	public List<OthSet> findByRequireOrderByCreatedDateDesc(@Param("require") Require require);

	@Query("SELECT os.sid FROM #{#entityName} os WHERE os.require = :require ORDER BY os.createdDate DESC")
	public List<String> findSidsByRequire(@Param("require") Require require);

	@Query("SELECT osStatus FROM #{#entityName} w WHERE w = :othset")
	public OthSetStatus findOthsetStatus(@Param("othset") OthSet othset);

	public OthSet findByOsNo(String osNo);

	/**
	 * 以部門和單據狀態查詢筆數
	 * 
	 * @param requireNo
	 * @param createDeps
	 * @param inOthSetStatus
	 * @return
	 */
	@Query("SELECT COUNT(r) AS size "
	        + "FROM #{#entityName} r "
	        + "WHERE r.requireNo = :requireNo "
	        + "  AND r.status = 0 "
	        + "  AND r.createDep in (:createDeps) "
	        + "  AND r.osStatus in (:inOthSetStatus) ")
	public Integer queryCountByCreateDepAndInOthSetStatus(
	        @Param("requireNo") String requireNo,
	        @Param("createDeps") List<Org> createDeps,
	        @Param("inOthSetStatus") List<OthSetStatus> inOthSetStatus);

	/**
	 * 以部門和單據狀態查詢筆數
	 * 
	 * @param requireNo
	 * @param createDeps
	 * @param inOthSetStatus
	 * @return
	 */
	@Query("SELECT r "
	        + "FROM #{#entityName} r "
	        + "WHERE r.requireNo = :requireNo "
	        + "  AND r.status = 0 "
	        + "  AND r.createDep in (:createDeps) "
	        + "  AND r.osStatus in (:inOthSetStatus) ")
	public List<OthSet> queryByCreateDepAndInOthSetStatus(
	        @Param("requireNo") String requireNo,
	        @Param("createDeps") List<Org> createDeps,
	        @Param("inOthSetStatus") List<OthSetStatus> inOthSetStatus);

	/**
	 * 以單據狀態查詢筆數
	 * 
	 * @param requireNo
	 * @param inOthSetStatus
	 * @return
	 */
	@Query("SELECT COUNT(r) AS size "
	        + "FROM #{#entityName} r "
	        + "WHERE r.requireNo = :requireNo "
	        + "  AND r.status = 0 "
	        + "  AND r.osStatus in (:inOthSetStatus) ")
	public Integer queryCountByOthSetStatus(
	        @Param("requireNo") String requireNo,
	        @Param("inOthSetStatus") List<OthSetStatus> inOthSetStatus);

	/**
	 * 以下列欄位查詢
	 * 
	 * @param requireNo
	 * @param inStatus
	 * @return
	 */
	public List<OthSet> findByRequireNoAndOsStatusIn(
	        String requireNo,
	        List<OthSetStatus> inStatus);

	/**
	 * 以下列欄位查詢
	 * 
	 * @param requireNo
	 * @param inStatus
	 * @param createCompanys
	 * @return
	 */
	public List<OthSet> findByRequireNoAndOsStatusInAndCreateDepIn(
	        String requireNo,
	        List<OthSetStatus> inStatus,
	        List<Org> createDeps);

}
