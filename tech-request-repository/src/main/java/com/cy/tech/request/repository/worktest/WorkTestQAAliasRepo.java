/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.worktest;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cy.tech.request.vo.worktest.WorkTestQAAlias;

/**
 *
 * @author aken_kao
 */
public interface WorkTestQAAliasRepo extends JpaRepository<WorkTestQAAlias, String>, Serializable {

}
