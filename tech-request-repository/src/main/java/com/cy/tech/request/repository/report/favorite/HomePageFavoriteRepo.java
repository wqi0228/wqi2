package com.cy.tech.request.repository.report.favorite;

import com.cy.commons.vo.User;
import com.cy.tech.request.vo.report.favorite.HomePageFavorite;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kasim
 */
@Repository
public interface HomePageFavoriteRepo extends JpaRepository<HomePageFavorite, String>, Serializable {

    public HomePageFavorite findByCreatedUser(User createdUser);

}
