/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require.tros;

import com.cy.tech.request.vo.require.tros.TrOs;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author brain0925_liao
 */
public interface TrOsRepository extends JpaRepository<TrOs, String>, Serializable {

    @Query("SELECT o FROM #{#entityName} o "
            + " WHERE o.require_sid = :require_sid "
            + " ORDER BY o.create_dt DESC")
    public List<TrOs> getTrOsByRequireSid(@Param("require_sid") String require_sid);

    /**
     * 以下列欄位查詢
     * 
     * @param requireNo  單號
     * @param createDeps 立案部門
     * @return
     */
    @Query("SELECT o FROM #{#entityName} o "
            + " WHERE o.status = 0 "
            + "   AND o.require_no = :require_no "
            + "   AND o.dep_sid in :createDeps ")
    public List<TrOs> queryByRequirenoAndDepsidIn(
            @Param("require_no") String requireNo,
            @Param("createDeps") List<Integer> createDeps);
}
