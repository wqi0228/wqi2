
package com.cy.tech.request.repository.log;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.log.LogSyncMms;

/**
 * MMS維護單同步記錄 Repository
 * 
 * @author allen1214_wu
 */
public interface LogSyncMmsRepository extends JpaRepository<LogSyncMms, String>, Serializable {

    /**
     * 取得最近的 MMS 資料時間
     * 
     * @return
     */
    @Query(value = "SELECT Max(mms_data_update_dt) "
            + "       FROM tr_log_sync_mms "
            + "      WHERE is_success = 'Y' ", nativeQuery = true)
    public Date queryLatestMmsDate();
    
    /**
     * 查詢需要重新執行的單據(單號不重複)
     * @return
     */
    @Query(value = "SELECT DISTINCT mms_id "
            + "       FROM tr_log_sync_mms "
            + "      WHERE is_sync_again = 0 "
            + "        AND is_success = 'N' ", nativeQuery = true)
    public List<String> queryMmsIdsByWaitDoAgain();

    /**
     * 依據 MMSID, 資料時間 判斷是否已成功執行
     * 
     * @param mmsID       mmsID
     * @param mmsDateTime 資料時間
     * @param success     是否成功
     * @param status      資料狀態
     * @return TRUE/FALSE
     */
    @Query(""
            + "SELECT "
            + "  CASE WHEN (COUNT(e) > 0) THEN 'TRUE' ELSE 'FALSE' END "
            + "  FROM #{#entityName} e "
            + " WHERE e.mmsID = :mmsID "
            + "   AND e.mmsDateTime = :mmsDateTime"
            + "   AND e.success = :success "
            + "   AND e.status = :status ")
    public Boolean isProcessedAndSuccess(
            @Param("mmsID") String mmsID,
            @Param("mmsDateTime") Date mmsDateTime,
            @Param("success") boolean success,
            @Param("status") Activation status);

}
