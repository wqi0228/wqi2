/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.template;

import com.cy.tech.request.vo.template.TemplateBaseDataField;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface TemplateBaseDataFieldRepository extends JpaRepository<TemplateBaseDataField, String>, Serializable {

    @Query("SELECT b FROM #{#entityName} b WHERE b.fieldName like :searchText ")
    public List<TemplateBaseDataField> findByFuzzyText(@Param("searchText") String searchText);
}
