/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require;

import com.cy.tech.request.vo.require.ReportCustomFilter;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author aken_kao
 */
public interface ReportCustomFilterRepository extends JpaRepository<ReportCustomFilter, String>, Serializable {
    
    @Query("SELECT t FROM #{#entityName} t WHERE t.url = :url AND t.create_usr = :createUserSid")
    public ReportCustomFilter findByUrlAndUserSid(
            @Param("url") String url,
            @Param("createUserSid") Integer createUserSid);
}
