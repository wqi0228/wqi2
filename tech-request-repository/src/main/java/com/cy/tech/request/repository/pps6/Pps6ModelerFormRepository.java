package com.cy.tech.request.repository.pps6;

import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import com.cy.tech.request.vo.pps6.Pps6ModelerForm;

/**
 * PPS6 Modeler 單據名稱定義資料檔 Repository
 * 
 * @author allen
 */
public interface Pps6ModelerFormRepository extends JpaRepository<Pps6ModelerForm, String>, Serializable {
}
