/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require.tros;

import com.cy.tech.request.vo.enums.OthSetAlertType;
import com.cy.tech.request.vo.require.tros.TrOsAlert;
import com.cy.work.common.enums.ReadRecordType;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author brain0925_liao
 */
public interface TrOsAlertRepository extends JpaRepository<TrOsAlert, String>, Serializable {

    @Query("SELECT a.send_usr FROM #{#entityName} a WHERE a.os_reply_sid = :os_reply_sid")
    public List<Integer> findSenderSidByReplySid(@Param("os_reply_sid") String os_reply_sid);

    @Modifying
    @Query("UPDATE #{#entityName} a SET a.readStatus = :readStatus WHERE a.os_sid = :os_sid AND a.type = :type ")
    public int updateReadStatus(@Param("os_sid") String os_sid, @Param("type") OthSetAlertType type, @Param("readStatus") ReadRecordType readStatus);
}
