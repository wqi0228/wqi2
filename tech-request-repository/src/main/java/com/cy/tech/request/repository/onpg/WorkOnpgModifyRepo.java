/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.onpg;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cy.commons.vo.User;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.work.common.vo.value.to.JsonStringListTo;

/**
 *
 * @author kasim
 */
public interface WorkOnpgModifyRepo extends JpaRepository<WorkOnpg, String>, Serializable {

    @Modifying
    @Query("UPDATE #{#entityName} r SET r.noticeDeps  = :noticeDeps ,"
            + "                       r.establishDate   = :establishDate ,"
            + "                       r.theme  = :theme ,"
            + "                       r.content    = :content ,"
            + "                       r.contentCss = :contentCss ,"
            + "                       r.note = :note ,"
            + "                       r.noteCss = :noteCss ,"
            + "                       r.readReason = :readReason ,"
            + "                       r.readUpdateDate = :readUpdateDate ,"
            + "                       r.updatedDate = :updatedDate ,"
            + "                       r.updatedUser = :updatedUser "
            + " WHERE r.sid = :sid")
    public int modifyByEdit(
            @Param("noticeDeps") JsonStringListTo noticeDeps,
            @Param("establishDate") Date establishDate,
            @Param("theme") String theme,
            @Param("content") String content,
            @Param("contentCss") String contentCss,
            @Param("note") String note,
            @Param("noteCss") String noteCss,
            @Param("readReason") WaitReadReasonType readReason,
            @Param("readUpdateDate") Date readUpdateDate,
            @Param("updatedDate") Date updatedDate,
            @Param("updatedUser") User updatedUser,
            @Param("sid") String sid
    );

}
