/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require;

import com.cy.commons.vo.Org;
import com.cy.tech.request.vo.require.FogbugzProjectMapping;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * FB 對應部門專案名稱
 *
 * @author shaun
 */
public interface FogbugzProjectMappingRepository extends JpaRepository<FogbugzProjectMapping, Long>, Serializable {

    @Query("SELECT fm.projectName FROM #{#entityName} fm WHERE fm.dept = :dept")
    public String findProjectNameByDept(@Param("dept") Org dept);

    public FogbugzProjectMapping findByDept(Org dept);

    public List<FogbugzProjectMapping> findByProjectName(String projectName);

}
