/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.pt;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.pt.PtReply;

/**
 *
 * @author shaun
 */
public interface PtReplyRepo extends JpaRepository<PtReply, String>, Serializable {

    public List<PtReply> findByPtCheck(PtCheck ptCheck);

}
