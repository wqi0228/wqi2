package com.cy.tech.request.repository.menu;

import com.cy.tech.request.vo.menu.TrMenuItemGroup;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author shaun
 */
public interface TrMenuItemGroupRepository extends JpaRepository<TrMenuItemGroup, String> , Serializable{
    
}
