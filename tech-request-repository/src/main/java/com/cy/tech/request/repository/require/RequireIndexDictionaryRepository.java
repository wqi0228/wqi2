/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require;

import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireIndexDictionary;
import com.cy.tech.request.vo.template.CategoryKeyMapping;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface RequireIndexDictionaryRepository extends JpaRepository<RequireIndexDictionary, String>, Serializable {

    public List<RequireIndexDictionary> findByRequire(Require require);

    /**
     * 欄位名稱尋找需求單索引
     *
     * @param requires
     * @param fieldName
     * @return
     */
    @Query("SELECT idx FROM #{#entityName} idx WHERE idx.require IN :requires AND idx.fieldName = :fieldName")
    public List<RequireIndexDictionary> findIndexByRequiresAndFieldName(@Param("requires") List<Require> requires, @Param("fieldName") String fieldName);

    public List<RequireIndexDictionary> findByFieldName(String fieldName);

    @Query("SELECT idx.mapping FROM #{#entityName} idx "
            + "WHERE idx.fieldName = :fieldName "
            + "GROUP BY idx.mapping.sid")
    public List<CategoryKeyMapping> findMappingByFieldName(
            @Param("fieldName") String fieldName);
    
	/**
	 * @param requireSid
	 */
    @Modifying
	@Query("delete from #{#entityName} e where e.require = :require")
	public void deleteByRequire(
	        @Param("require") Require require);

}
