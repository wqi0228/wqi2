/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.anew.onpg;

import com.cy.tech.request.vo.anew.onpg.ReqWorkOnpg;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * ON程式主檔 Dao
 *
 * @author kasim
 */
public interface ReqWorkOnpgRepo extends JpaRepository<ReqWorkOnpg, String>, Serializable {

    @Query("SELECT e FROM #{#entityName} e "
            + "WHERE e.sourceSid IN :sourceSids ")
    public List<ReqWorkOnpg> findBySourceSidIn(
            @Param("sourceSids") List<String> sourceSids
    );

    @Query("SELECT e FROM #{#entityName} e "
            + "WHERE e.onpgNo IN :onpgNos ")
    public List<ReqWorkOnpg> findByOnpgNoIn(
            @Param("onpgNos") List<String> onpgNos
    );
}
