/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.pt;

import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.pt.PtHistory;
import com.cy.tech.request.vo.pt.enums.PtStatus;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface PtHistoryRepo extends JpaRepository<PtHistory, String>, Serializable {

    @Query("SELECT h FROM #{#entityName} h WHERE h.ptCheck = :ptCheck AND h.behavior != 'REPLY_AND_REPLY' ORDER BY updatedDate DESC ")
    public List<PtHistory> findByPtCheck(@Param("ptCheck") PtCheck ptCheck);

    @Query("SELECT CASE WHEN (COUNT(r) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} r "
              + " WHERE r.ptNo = :ptNo AND r.behaviorStatus IN :behaviorStatus")
    public Boolean isContainBehaviorStatusByPtNo(@Param("ptNo") String ptNo, @Param("behaviorStatus") List<PtStatus> behaviorStatus);

    @Query("SELECT h FROM #{#entityName} h WHERE h.ptNo IN :ptNos ORDER BY updatedDate DESC ")
    public List<PtHistory> findByPtNoIn(@Param("ptNos") List<String> ptNos);
}
