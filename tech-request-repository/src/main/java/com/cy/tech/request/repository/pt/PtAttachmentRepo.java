/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.pt;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.pt.PtAttachment;
import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.pt.PtHistory;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface PtAttachmentRepo extends JpaRepository<PtAttachment, String>, Serializable {

    public List<PtAttachment> findByPtCheckAndStatusAndHistoryIsNullOrderByCreatedDateDesc(PtCheck ptCheck, Activation status);

    public List<PtAttachment> findByHistoryAndStatusOrderByCreatedDateDesc(PtHistory history, Activation status);

    @Query(value = "SELECT * FROM work_pt_attachment a JOIN work_pt_check_history h ON a.pt_check_history_sid = h.pt_check_history_sid "
              + "     WHERE h.pt_check_history_sid IN (:historySids) "
              + "       AND a.status = 0"
              + "     ORDER BY a.create_dt DESC ", nativeQuery = true)
    public List<PtAttachment> findAttachByHistoryInOrderByUpdateDateDesc(@Param("historySids") List<String> historySids);

}
