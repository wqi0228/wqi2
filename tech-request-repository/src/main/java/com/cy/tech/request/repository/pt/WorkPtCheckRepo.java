/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.pt;

import com.cy.tech.request.vo.pt.WorkPtCheck;
import com.cy.work.common.vo.value.to.JsonStringListTo;

import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author brain0925_liao
 */
public interface WorkPtCheckRepo extends JpaRepository<WorkPtCheck, String>, Serializable {

    @Query("SELECT r FROM #{#entityName} r WHERE r.ptNo = :ptNo")
    public WorkPtCheck findByPtNo(@Param("ptNo") String ptNo);

    @Modifying
    @Transactional
    @Query("UPDATE #{#entityName} r SET r.dep_sid  = :dep_sid  WHERE r.ptNo = :ptNo")
    public int updateDepsid(@Param("ptNo") String ptNo, @Param("dep_sid") Integer dep_sid);

    @Modifying
    @Transactional
    @Query("UPDATE #{#entityName} r SET r.noticeDeps  = :noticeDeps  WHERE r.sid = :sid")
    public int updateNoticeDeps(@Param("sid") String sid, @Param("noticeDeps") JsonStringListTo noticeDeps);
}
