/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.vo.enums.ForwardType;
import com.cy.tech.request.vo.require.AlertInbox;
import com.cy.tech.request.vo.require.AlertInboxSendGroup;
import com.cy.tech.request.vo.require.Require;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author shaun
 */
@Repository("tr_alertInboxRepository")
public interface AlertInboxRepository extends JpaRepository<AlertInbox, String>, Serializable {

    /**
     * 依下列條件查詢
     * @param requireNo 需求單號
     * @return AlertInbox list
     */
    public List<AlertInbox> findByRequireNoOrderByCreatedDateDesc(String requireNo);
    
    public List<AlertInbox> findByRequireAndForwardTypeOrderByCreatedDateDesc(Require require, ForwardType forwardType);
    
    /**
     * 依下列條件查詢
     * @param requireNo 需求單號
     * @param forwardType 轉寄類型
     * @return AlertInbox list
     */
    public List<AlertInbox> findByRequireNoAndForwardTypeOrderByCreatedDateDesc(String requireNo, ForwardType forwardType);

    @Query("SELECT ai FROM #{#entityName} ai WHERE ai.require = :require AND ai.status = :status AND ai.receiveDep IN :receiveDeps ")
    public List<AlertInbox> findByRequireAndStatusAndReceiveDeps(
              @Param("require") Require require,
              @Param("status") Activation status,
              @Param("receiveDeps") List<Org> receiveDeps);

    @Query("SELECT ai FROM #{#entityName} ai WHERE ai.require = :require AND ai.status = :status AND ai.receive IN :receives ")
    public List<AlertInbox> findByRequireAndStatusAndReceive(
              @Param("require") Require require,
              @Param("status") Activation status,
              @Param("receives") List<User> receives);

    @Query("SELECT ai.receive FROM #{#entityName} ai WHERE "
              + "ai.require = :require AND ai.status = :status AND ai.receive IN :recipients AND ai.forwardType = :forwardType ")
    public List<User> findReceiveByRequireAndStatusAndRecipients(
              @Param("require") Require require,
              @Param("status") Activation status,
              @Param("recipients") List<User> recipients,
              @Param("forwardType") ForwardType forwardType);

    @Query("SELECT ai.receiveDep FROM #{#entityName} ai WHERE "
              + "ai.require = :require AND ai.status = :status AND ai.forwardType = :forwardType ")
    public List<Org> findReceiveDepByRequireAndStatus(
              @Param("require") Require require,
              @Param("status") Activation status,
              @Param("forwardType") ForwardType forwardType);

    @Query("SELECT ai.receiveDep FROM #{#entityName} ai WHERE "
              + "ai.require = :require AND ai.status = :status AND ai.forwardType = :forwardType AND ai.sender IN :senders ")
    public List<Org> findReceiveDepByRequireAndStatusAndSenderIn(
              @Param("require") Require require,
              @Param("status") Activation status,
              @Param("forwardType") ForwardType forwardType,
              @Param("senders") List<User> senders);

    @Query("SELECT ai.receiveDep FROM #{#entityName} ai WHERE "
              + "ai.require = :require AND ai.status = :status AND ai.forwardType = :forwardType AND ai.senderDep IN :senderDeps ")
    public List<Org> findReceiveDepByRequireAndStatusAndSenderDepIn(
              @Param("require") Require require,
              @Param("status") Activation status,
              @Param("forwardType") ForwardType forwardType,
              @Param("senderDeps") List<Org> senderDeps);

    @Query("SELECT ai FROM #{#entityName} ai WHERE ai.require = :require AND ai.status = :status AND ai.forwardType = :forwardType ")
    public List<AlertInbox> findByRequireAndStatus(
              @Param("require") Require require,
              @Param("status") Activation status,
              @Param("forwardType") ForwardType forwardType);

    @Query("SELECT ai.sendInboxGroup FROM #{#entityName} ai WHERE ai.require = :require AND ai.sender IN :senders GROUP BY ai.sendInboxGroup ")
    public List<AlertInboxSendGroup> findSendGroupByRequireAndSenderIn(@Param("require") Require require,
              @Param("senders") List<User> senders);

    public List<AlertInbox> findBySendInboxGroupOrderByCreatedDateDesc(
              @Param("sendInboxGroup") AlertInboxSendGroup sendInboxGroup);

    public List<AlertInbox> findByRequireNoAndStatusAndForwardType(String requireNo, Activation status, ForwardType forwardType);

    @Query(value = "SELECT "
              + "ai.alert_sid,"
              + "ai.create_dt,"
              + "ai.receive_dep,"
              + "ai.forward_type,"
              + "ai.status,"
              + "ai.update_dt,"
              + "ai.alert_group_sid "
              + "FROM tr_alert_inbox ai WHERE ai.forward_type = :forwardType", nativeQuery = true)
    public List<Object[]> findByForwardTypeWithNative(@Param("forwardType") String forwardType);

    @Query(value = "  SELECT CASE WHEN (COUNT(*) > 0) THEN 'TRUE' ELSE 'FALSE' END "
              + "  FROM tr_alert_inbox a "
              + "    WHERE a.receive = :receiveSid "
              + "      AND a.inbox_type = :inboxType "
              + "      AND a.require_sid = :requireSid ", nativeQuery = true)
    public Boolean hasInbox(@Param("requireSid") String requireSid,
              @Param("receiveSid") Integer receiveSid,
              @Param("inboxType") String inboxTypeStr);

    @Query(value = "SELECT "
              + "a.require_sid, "
              + "a.require_no, "
              + "a.alert_sid, "
              + "a.alert_group_sid, "
              + "a.inbox_type, "
              + "a.sender, "
              + "a.receive, "
              + "a.create_dt, "
              + "g.message_css "
              + "  FROM tr_alert_inbox a "
              + "    JOIN tr_alert_inbox_send_group g ON a.alert_group_sid = g.alert_group_sid "
              + "  WHERE a.receive = :receiveSid "
              + "    AND a.inbox_type = :inboxType "
              + "    AND a.require_sid = :requireSid "
              + "  ORDER BY a.create_dt DESC ", nativeQuery = true)
    public List<Object[]> findReqInboxWithNative(@Param("requireSid") String requireSid,
              @Param("receiveSid") Integer receiveSid,
              @Param("inboxType") String inboxTypeStr);

    @Query(value = "  SELECT a.alert_group_sid "
              + "  FROM tr_alert_inbox a "
              + "    WHERE a.receive = :receiveSid "
              + "      AND a.inbox_type = :inboxType "
              + "      AND a.require_sid = :requireSid ", nativeQuery = true)
    public List<String> findSidsByRequire(@Param("requireSid") String requireSid,
              @Param("receiveSid") Integer receiveSid,
              @Param("inboxType") String inboxTypeStr);

    @Modifying
    @Query(value = "DELETE FROM tr_alert_inbox "
              + " WHERE alert_sid = :alertInboxSid", nativeQuery = true)
    public void deleteBySid(@Param("alertInboxSid") String alertInboxSid);

}
