/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require.tros;

import com.cy.tech.request.vo.require.tros.TrOsAttachment;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author brain0925_liao
 */
public interface TrOsAttachmentRepository extends JpaRepository<TrOsAttachment, String>, Serializable {

    @Query("SELECT o FROM #{#entityName} o WHERE o.status=0 AND o.os_sid = :os_sid AND ( o.os_history_sid is null OR o.os_history_sid = '') ")
    public List<TrOsAttachment> getTrOsAttachmentByOSSid(
            @Param("os_sid") String os_sid);

    @Query("SELECT o FROM #{#entityName} o WHERE o.status=0 "
            + "AND o.os_sid = :os_sid AND o.os_history_sid = :os_history_sid ")
    public List<TrOsAttachment> getTrOsAttachmentByOSSidAndOsHistorySid(
            @Param("os_sid") String os_sid, @Param("os_history_sid") String os_history_sid);

    /** 更新附件資訊 By 附件Sid */
    @Modifying
    @Transactional(rollbackFor = Exception.class)
    @Query("UPDATE #{#entityName} r SET r.os_sid  = :os_sid , r.os_no = :os_no ,r.require_sid = :require_sid,"
            + "r.require_no =:require_no,r.os_history_sid = :os_history_sid"
            + ",r.description = :description ,r.department = :department, r.update_usr = :update_usr , r.update_dt = :update_dt"
            + " WHERE r.sid = :sid")
    public int updateTrOsAttachmentMappingInfo(@Param("os_sid") String os_sid, @Param("os_no") String os_no,
            @Param("require_sid") String require_sid, @Param("require_no") String require_no, @Param("os_history_sid") String os_history_sid, @Param("description") String description, @Param("department") Integer department,
            @Param("update_usr") Integer update_usr, @Param("update_dt") Date update_dt, @Param("sid") String sid);
}
