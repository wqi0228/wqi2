/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require.tros;

import com.cy.tech.request.vo.require.tros.TrOsHistory;
import java.io.Serializable;
import java.util.Date;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author brain0925_liao
 */
public interface TrOsHistoryRepository extends JpaRepository<TrOsHistory, String>, Serializable {

    @Modifying
    @Transactional(rollbackFor = Exception.class)
    @Query("UPDATE #{#entityName} r SET r.update_usr  = :update_usr , r.update_dt = :update_dt "
            + " WHERE r.sid = :sid")
    public int updateTrOsReplyContent(@Param("update_usr") Integer update_usr, @Param("update_dt") Date update_dt,
            @Param("sid") String sid);
}
