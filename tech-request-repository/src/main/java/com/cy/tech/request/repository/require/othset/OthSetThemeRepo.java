/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require.othset;

import com.cy.tech.request.vo.require.othset.OthSetTheme;
import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface OthSetThemeRepo extends JpaRepository<OthSetTheme, String>, Serializable {

    @Query("SELECT o FROM #{#entityName} o WHERE o.theme = :theme")
    public OthSetTheme findByTheme(@Param("theme") String theme);

    @Query("SELECT o FROM #{#entityName} o ORDER BY o.seq")
    public List<OthSetTheme> findAll();
    
    @Query("SELECT max(t.seq) FROM #{#entityName} t")
    public Integer maxSeq();
}
