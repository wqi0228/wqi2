
package com.cy.tech.request.repository.require;

import com.cy.tech.request.vo.require.TrnsBackup;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 轉檔資料備份
 * @author allen1214_wu
 */
public interface TrnsBackupRepository extends JpaRepository<TrnsBackup, Long>, Serializable {
    
    /**
     * @param trnsType
     * @param custKey
     * @return
     */
    public List<TrnsBackup> findByTrnsTypeAndCustKeyIn(String trnsType, List<String> custKeys);
}
