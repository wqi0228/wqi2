/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.template;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.constants.CacheConstants;
import com.cy.tech.request.vo.category.BasicDataBigCategory;
import com.cy.tech.request.vo.category.BasicDataMiddleCategory;
import com.cy.tech.request.vo.category.BasicDataSmallCategory;
import com.cy.tech.request.vo.template.CategoryKeyMapping;
import java.io.Serializable;
import java.util.List;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * 欄位基本資料模版
 *
 * @author shaun
 */
@Repository
public interface CategoryKeyMappingRepository extends JpaRepository<CategoryKeyMapping, String>, Serializable {

    @Query("SELECT CASE WHEN (COUNT(b) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} b WHERE b.id = :id")
    public Boolean isExistId(@Param("id") String id);

    @Query("SELECT CASE WHEN (COUNT(b) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} b WHERE b.id = :id AND b.version = :version")
    public Boolean isExistIdAndVersion(@Param("id") String id, @Param("version") Integer version);

    public List<CategoryKeyMapping> findById(String id);

    public List<CategoryKeyMapping> findByBig(BasicDataBigCategory big);

    public List<CategoryKeyMapping> findByMiddle(BasicDataMiddleCategory middle);

    public List<CategoryKeyMapping> findBySmall(BasicDataSmallCategory small);

    @Query("SELECT c.version FROM #{#entityName} c WHERE c.id = :id ORDER by c.version DESC")
    public List<Integer> findByIdAllVersionList(@Param("id") String id);

    @Query("SELECT c FROM #{#entityName} c WHERE c.version = :version AND (c.bigName like :searchText OR c.middleName like :searchText OR c.smallName like :searchText) ORDER by c.id")
    public List<CategoryKeyMapping> findByFuzzyTextAndVersion(@Param("searchText") String searchText, @Param("version") Integer version);

    @Cacheable(value = CacheConstants.CACHE_CATE_KEY_MAPPING, key = "#p0", condition = "#p0 == '%%' ")
    @Query("SELECT a FROM #{#entityName} a WHERE  "
              + "(a.id,a.version) IN (SELECT b.id,MAX(b.version) FROM #{#entityName} b GROUP BY b.id) "
              + "AND (a.bigName LIKE :searchText OR a.middleName LIKE :searchText OR a.smallName LIKE :searchText) "
              + "AND a.status IN :status ORDER BY a.big.id,a.middle.id,a.small.id")
    public List<CategoryKeyMapping> findByFuzzyTextAndMaxVersion(@Param("searchText") String searchText, @Param("status") List<Activation> status);

    /**
     * 是否相關聯的類別已建立需求單據
     *
     * @param cateSid 類別 | 中類 | 小類 sid
     * @return
     */
    @Query(value = "SELECT CASE WHEN (COUNT(*) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM tr_require tr "
              + "    WHERE tr.mapping_sid IN ( "
              + "      SELECT tckm.key_sid FROM tr_category_key_mapping tckm WHERE "
              + "        tckm.big_category_sid = :cateSid OR "
              + "        tckm.middle_category_sid = :cateSid OR "
              + "        tckm.small_category_sid = :cateSid "
              + "      ) ", nativeQuery = true)
    public Boolean isAnyCategoryRelatedDocBeCreated(@Param("cateSid") String cateSid);

}
