/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require.trace;

import com.cy.tech.request.vo.require.trace.TrRequireTrace;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author brain0925_liao
 */
public interface TrRequireTraceRepository extends JpaRepository<TrRequireTrace, String>, Serializable {

}
