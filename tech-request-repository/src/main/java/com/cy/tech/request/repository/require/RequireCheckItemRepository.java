package com.cy.tech.request.repository.require;

import com.cy.tech.request.vo.require.RequireCheckItem;
import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 需求單-檢查項檔 Repository
 * 
 * @author allen1214_wu
 */
public interface RequireCheckItemRepository extends JpaRepository<RequireCheckItem, String>, Serializable {

    /**
     * 以下列條件查詢
     * 
     * @param requireSid 需求單 sid
     * @return RequireCheckItem list
     */
    public List<RequireCheckItem> findByRequireSid(String requireSid);

    /**
     * 以下列條件查詢
     * 
     * @param requireSid 需求單 sid
     * @return RequireCheckItem list
     */
    public List<RequireCheckItem> findByRequireSidIn(List<String> requireSids);
    
    /**
     * 取回已檢核的項目
     * @param requireSid
     * @return
     */
    public List<RequireCheckItem> findByRequireSidInAndCheckDateNotNull(List<String> requireSids);

}
