package com.cy.tech.request.repository.category;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.category.BasicDataMiddleCategory;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface BasicDataMiddleCategoryRepository extends JpaRepository<BasicDataMiddleCategory, String>, Serializable {

    public List<BasicDataMiddleCategory> findByStatusOrderByIdAsc(Activation status);

    @Query("SELECT b FROM #{#entityName} b WHERE b.status in :status AND (b.id LIKE :searchText OR b.name LIKE :searchText) ORDER BY b.id")
    public List<BasicDataMiddleCategory> findByStatusAndFuzzyText(@Param("status") List<Activation> status, @Param("searchText") String searchText);

    public BasicDataMiddleCategory findById(String id);

    public BasicDataMiddleCategory findByName(String name);

    @Query("SELECT CASE WHEN (COUNT(b) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} b WHERE b.id = :id")
    public Boolean isExistId(@Param("id") String id);

    @Query("SELECT CASE WHEN (COUNT(b) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} b WHERE b.name = :name")
    public Boolean isExistName(@Param("name") String name);

    @Query("SELECT CASE WHEN (COUNT(b) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} b WHERE b.sid != :sid AND b.id = :id")
    public Boolean isExistIdByEdit(@Param("sid") String sid, @Param("id") String id);

    @Query("SELECT CASE WHEN (COUNT(b) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} b WHERE b.sid != :sid AND b.name = :name")
    public Boolean isExistNameByEdit(@Param("sid") String sid, @Param("name") String name);

    @Query(
              value = "SELECT CASE WHEN (COUNT(*) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM tr_basic_data_middle_category middle "
              + "    LEFT JOIN tr_basic_data_small_category small ON small.parent_middle_category = :sid "
              + "    WHERE small.status = 0", nativeQuery = true)
    public Boolean checkChildIsActive(@Param("sid") String sid);

    @Query(
              value = "SELECT CASE WHEN (COUNT(*) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} m "
              + "    LEFT JOIN m.children smalls ON smalls.parentMiddleCategory.sid = :sid "
              + "    WHERE smalls.status = :status")
    public Boolean checkChildsByStatus(@Param("sid") String sid, @Param("status") Activation status);

}
