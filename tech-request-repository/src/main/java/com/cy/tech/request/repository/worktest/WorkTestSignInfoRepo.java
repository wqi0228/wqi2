/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.worktest;

import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.vo.worktest.WorkTestSignInfo;
import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.enums.WorkSourceType;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface WorkTestSignInfoRepo extends JpaRepository<WorkTestSignInfo, String>, Serializable {

    public WorkTestSignInfo findByTestInfo(WorkTestInfo testInfo);

    public WorkTestSignInfo findByTestinfoNo(String testinfoNo);

    @Query("SELECT CASE WHEN (COUNT(wtsi) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} wtsi "
              + " WHERE wtsi.sourceType = :sourceType AND wtsi.sourceNo = :sourceNo AND wtsi.instanceStatus IN :status")
    public Boolean hasAnyProcessNotCompleteBySourceTypeAndSoruceNoAndStatusIn(
              @Param("sourceType") WorkSourceType sourceType,
              @Param("sourceNo") String sourceNo,
              @Param("status") List<InstanceStatus> status);

    @Query(""
    		+ "SELECT wtsi FROM #{#entityName} wtsi "
            + " WHERE wtsi.sourceType = :sourceType "
            + "   AND wtsi.sourceNo = :sourceNo "
            + "   AND wtsi.instanceStatus IN :status")
    public List<WorkTestSignInfo> findProcessNotCompleteBySourceTypeAndSoruceNoAndStatusIn(
              @Param("sourceType") WorkSourceType sourceType,
              @Param("sourceNo") String sourceNo,
              @Param("status") List<InstanceStatus> status);

    public WorkTestSignInfo findByBpmInstanceId(String bpmInstanceId);

    @Query(value = "SELECT * FROM work_test_sign_info wtsi "
              + "     WHERE wtsi.paper_code IN ('PAPER000','PAPER001','PAPER002','PAPER004');", nativeQuery = true)
    public List<WorkTestSignInfo> findRunningProcess();
    
    
	/**
	 * @param sourceType
	 * @param sourceNo
	 * @param paperCodes
	 * @param depSids
	 * @return
	 */
	@Query(value = ""
			+ "SELECT Count(*) "
			+ "FROM   work_test_sign_info signInfo "
			+ "       INNER JOIN work_test_info tesInfo "
			+ "               ON tesInfo.testinfo_sid = signInfo.testinfo_sid "
			+ "WHERE  signInfo.testinfo_source_type = :sourceType "
			+ "       AND signInfo.testinfo_source_no = :sourceNo "
			+ "       AND signInfo.paper_code IN ( :paperCodes ) "
			+ "       AND tesInfo.dep_sid IN ( :depSids )",
	        nativeQuery = true)
	public Integer queryCountByPaperCodeAndDepSid(
	        @Param("sourceType") String sourceType,
	        @Param("sourceNo") String sourceNo,
	        @Param("paperCodes") List<String> paperCodes,
	        @Param("depSids") List<Integer> depSids);
    
}
