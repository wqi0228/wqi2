package com.cy.tech.request.repository.result;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * 批次調整上線日
 * @author aken_kao
 *
 */
public class BatchAdjustDateVO implements Serializable{
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -2007834883064499183L;
    @Getter
    @Setter
    private Date trCreatedDate;
    @Getter
    @Setter
    private String requireNo;
    @Getter
    @Setter
    private String requireTheme;
    @Getter
    @Setter
    private Integer wtCreateDeptSid;
    @Getter
    @Setter
    private String wtCreateDept;
    @Getter
    @Setter
    private Integer wtCreateUserSid;
    @Getter
    @Setter
    private String wtCreateUser;
    @Getter
    @Setter
    private String wtTheme;
    @Getter
    @Setter
    private String wtStatus;
    @Getter
    @Setter
    private Date wtEstablishDate;
    @Getter
    @Setter
    private Date wtExpectOnlineDate;
    @Getter
    @Setter
    private Date wtScheduleFinishDate;
    @Getter
    @Setter
    private String wtQaScheduleStatus;
    @Getter
    @Setter
    private Date testDate;
    @Getter
    @Setter
    private String testInfoNo;
    @Getter
    @Setter
    private String testInfoSid;
    @Getter
    @Setter
    private String qaAuditStatus;
    @Getter
    @Setter
    private String commitStatus;
    @Getter
    @Setter
    private boolean checked;
}
