package com.cy.tech.request.repository.result;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * @author aken_kao
 * 組織異動 轉單程式VO
 */
public class TransRequireVO implements Serializable{
    
    /**
     * 
     */
    private static final long serialVersionUID = 6793962472112937308L;
    @Getter
    @Setter
    private String sid;
    @Getter
    @Setter
    private String type;
    @Getter
    @Setter
    private Date createDate;
    @Getter
    @Setter
    private Integer deptSid;
    @Getter
    @Setter
    private Integer userSid;
    @Getter
    @Setter
    private String theme;
    @Getter
    @Setter
    private String orderNo;
    @Getter
    @Setter
    private String status;
    @Getter
    @Setter
    private String createDept;
    @Getter
    @Setter
    private String createUserName;
    @Getter
    @Setter
    private boolean checked;
    @Getter
    @Setter
    private boolean alreadyChecked;
    
    public TransRequireVO(String sid, Date createDate, Integer deptSid, Integer userSid, String theme, String orderNo,
            String status) {
        super();
        this.sid = sid;
        this.createDate = createDate;
        this.deptSid = deptSid;
        this.userSid = userSid;
        this.theme = theme;
        this.orderNo = orderNo;
        this.status = status;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((sid == null) ? 0 : sid.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TransRequireVO other = (TransRequireVO) obj;
        if (sid == null) {
            if (other.sid != null)
                return false;
        } else if (!sid.equals(other.sid))
            return false;
        return true;
    }
    
    
}
