package com.cy.tech.request.repository.menu;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cy.tech.request.vo.menu.TrMenuItem;

/**
 *
 * @author shaun
 */
public interface TrMenuItemRepository extends JpaRepository<TrMenuItem, String> , Serializable{

    @Query("SELECT t FROM #{#entityName} t WHERE t.componentID = :componentId")
    public TrMenuItem findyByComponentId(@Param("componentId") String componentId);
}
