package com.cy.tech.request.repository.category;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.category.SimpleBigCategory;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 */
public interface SimpleBigCategoryRepository extends JpaRepository<SimpleBigCategory, String>, Serializable {

    /**
     * @param status
     * @return
     */
    public List<SimpleBigCategory> findByStatusOrderByIdAsc(Activation status);
    
    /**
     * @param status
     * @return
     */
    public List<SimpleBigCategory> findAllByOrderByIdAsc();
}
