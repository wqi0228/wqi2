/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.column;

import com.cy.commons.vo.User;
import com.cy.tech.request.vo.column.CustomColumn;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author shaun
 */
public interface CustomColumnRepository extends JpaRepository<CustomColumn, String>, Serializable {

    public CustomColumn findByUser(User user);
}
