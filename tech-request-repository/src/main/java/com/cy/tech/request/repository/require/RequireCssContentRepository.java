/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require;

import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireCssContent;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface RequireCssContentRepository extends JpaRepository<RequireCssContent, String>, Serializable {

	public List<RequireCssContent> findByRequireSid(String requireSid);

	/**
	 * @param sids
	 */
	@Modifying
	@Query("delete from #{#entityName} e where e.sid in :sids")
	public void deleteBySids(
	        @Param("sids") List<String> sids);

	@Modifying
	@Query("delete from #{#entityName} e where e.require = :require")
	public void deleteByRequire(
	        @Param("require") Require require);

}
