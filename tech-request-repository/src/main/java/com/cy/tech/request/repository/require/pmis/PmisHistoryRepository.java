package com.cy.tech.request.repository.require.pmis;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cy.tech.request.vo.require.pmis.PmisHistory;

/**
 * @author aken_kao
 */
public interface PmisHistoryRepository extends JpaRepository<PmisHistory, String> {

    @Query("SELECT t FROM #{#entityName} t WHERE t.noteWerpId = :requireNo")
    public List<PmisHistory> findByRequireNo(@Param("requireNo") String requireNo);

    /**
     * 以需求單號單號查詢是否存在
     * 
     * @param requireNo
     * @return
     */
    @Query("SELECT count(t)>0 FROM #{#entityName} t WHERE t.noteWerpId = :requireNo")
    public boolean isExistByRequireNo(@Param("requireNo") String requireNo);

    /**
     * 組織異動用: 傳入 require_sid ，判斷是否需要同步PMIS 
     * 1. tr_to_pmis_history 需存在
     * 2. 結案者不同步
     * @param requireSids
     * @return
     */
    @Query(value = ""
            + "SELECT req.require_sid "
            + "FROM   tr_require req "
            + "       INNER JOIN tr_to_pmis_history pmis "
            + "               ON req.require_no = pmis.note_werp_id "
            + "WHERE  req.status = 0 "
            + "       AND req.require_status NOT IN ( 'SUSPENDED', 'CLOSE', 'INVALID', 'AUTO_CLOSED' ) "
            + "       AND req.require_sid IN ( :requireSids )", nativeQuery = true)
    public List<String> findNeedTrnasForOrgTrnas(@Param("requireSids") Set<String> requireSids);

}
