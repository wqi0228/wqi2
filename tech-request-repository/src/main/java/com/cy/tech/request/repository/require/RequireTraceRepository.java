/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireTrace;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface RequireTraceRepository extends JpaRepository<RequireTrace, String>, Serializable {

    public List<RequireTrace> findByRequireOrderByCreatedDateDesc(Require require);

    @Query("SELECT r FROM #{#entityName} r "
              + " WHERE r.require = :require"
              + "   AND r.status = :status"
              + "   AND r.requireTraceType IN :traceTypes "
              + " ORDER BY r.createdDate DESC, r.requireTraceType DESC")
    public List<RequireTrace> findTraceByRequireFilterTraceStatus(
              @Param("require") Require require,
              @Param("traceTypes") List<RequireTraceType> traceTypes,
              @Param("status") Activation status);
    
    @Query("SELECT r FROM #{#entityName} r WHERE r.require = :require AND r.status = :status AND r.requireTraceType = :traceType ")
    public List<RequireTrace> findTraceByTraceType(
            @Param("require") Require require,
            @Param("traceType") RequireTraceType traceTypes, 
            @Param("status") Activation status);
    
    @Query(""
            + "SELECT count(r) AS size "
            + "  FROM #{#entityName} r "
            + " WHERE r.require = :require "
            + "   AND r.status = :status "
            + "   AND r.requireTraceType = :traceType ")
    public Integer countTraceByRequireAndTraceTypeAndStatus(
            @Param("require") Require require,
            @Param("traceType") RequireTraceType traceTypes, 
            @Param("status") Activation status);
    
    
    /**
     * @param require
     * @param requireTraceType
     * @param status
     * @return
     */
    public List<RequireTrace> findByRequireAndRequireTraceTypeAndStatusOrderByCreatedDateDesc(
            Require require,
            RequireTraceType requireTraceType, 
            Activation status);
    
    
}
