/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.onpg;

import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.onpg.WorkOnpgCheckMemo;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author shaun
 */
public interface WorkOnpgCheckMemoRepo extends JpaRepository<WorkOnpgCheckMemo, String>, Serializable {

    public List<WorkOnpgCheckMemo> findByOnpgOrderByUpdatedDateDesc(WorkOnpg onpg);
}
