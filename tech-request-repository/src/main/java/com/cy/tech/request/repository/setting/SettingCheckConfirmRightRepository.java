package com.cy.tech.request.repository.setting;

import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.setting.SettingCheckConfirmRight;

/**
 * @author allen 設定：可檢查確認權限
 */
public interface SettingCheckConfirmRightRepository extends JpaRepository<SettingCheckConfirmRight, String>, Serializable {

    /**
     * 以下列條件查詢
     * 
     * @param checkItemType 檢查項目
     * @return SettingCheckConfirmRight
     */
    public SettingCheckConfirmRight findByCheckItemType(RequireCheckItemType checkItemType);

}
