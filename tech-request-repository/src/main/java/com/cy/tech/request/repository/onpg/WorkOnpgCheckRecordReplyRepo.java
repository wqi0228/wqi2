/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.onpg;

import com.cy.tech.request.vo.onpg.WorkOnpgCheckRecord;
import com.cy.tech.request.vo.onpg.WorkOnpgCheckRecordReply;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface WorkOnpgCheckRecordReplyRepo extends JpaRepository<WorkOnpgCheckRecordReply, String>, Serializable {

    /**
     * @param sid
     * @return
     */
    public WorkOnpgCheckRecordReply findBySid(String sid);
	
    public List<WorkOnpgCheckRecordReply> findByCheckRecordOrderByUpdateDateDesc(WorkOnpgCheckRecord checkRecord);

    /**
     * 查找回覆by history sid <BR/> 無法接受空集合做參數值
     *
     * @param historySids
     * @return
     */
    @Query(value = "SELECT * FROM work_onpg_check_record_reply r "
            + "    JOIN work_onpg_history h ON r.onpg_check_record_sid = h.onpg_check_record_sid "
            + "    WHERE h.onpg_history_sid IN (:historySids) ORDER BY r.reply_udt DESC ", nativeQuery = true)
    public List<WorkOnpgCheckRecordReply> findReplyByHistoryInOrderByUpdateDateDesc(@Param("historySids") List<String> historySids);

}
