package com.cy.tech.request.repository.category;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.category.SimpleMiddleCategory;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 */
public interface SimpleMiddleCategoryRepository extends JpaRepository<SimpleMiddleCategory, String>, Serializable {

    /**
     * @param status
     * @return
     */
    public List<SimpleMiddleCategory> findByStatusOrderByIdAsc(Activation status);

    /**
     * @param status
     * @return
     */
    public List<SimpleMiddleCategory> findAllByOrderByIdAsc();

    /**
     * @return
     */
    public SimpleMiddleCategory findBySid(String sid);
}
