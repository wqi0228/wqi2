/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require;

import com.cy.tech.request.vo.require.RequireTempNotify;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 需求單 暫存訊息
 *
 * @author kasim
 */
public interface RequireTempNotifyRepository extends JpaRepository<RequireTempNotify, String>, Serializable {

    @Query("SELECT e.sid FROM #{#entityName} e "
            + "WHERE e.createdUser = :createdUser "
            + "AND e.requireNo = :requireNo ")
    public List<String> findSidByCreatedUserAndRequireNo(
            @Param("createdUser") Integer createdUser,
            @Param("requireNo") String requireNo
    );

    @Query("SELECT e.sid FROM #{#entityName} e "
            + "WHERE e.requireNo = :requireNo ")
    public List<String> findSidByRequireNo(
            @Param("requireNo") String requireNo
    );
}
