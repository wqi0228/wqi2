/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.enums.SubNoticeType;
import com.cy.tech.request.vo.require.TrSubNoticeInfo;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 子程序異動記錄 Dao
 *
 * @author kasim
 */
public interface TrSubNoticeInfoRepository extends JpaRepository<TrSubNoticeInfo, String>, Serializable {

    @Query("SELECT e FROM #{#entityName} e "
            + "WHERE e.status = :status "
            + "AND e.requireSid = :requireSid "
            + "AND e.subProcessSid = :subProcessSid "
            + "AND e.type IN :types "
            + "ORDER BY e.createdDate ASC "
    )
    public List<TrSubNoticeInfo> findByStatusAndRequireSidAndSubProcessSidAndTypeIn(
            @Param("status") Activation status,
            @Param("requireSid") String requireSid,
            @Param("subProcessSid") String subProcessSid,
            @Param("types") List<SubNoticeType> types);
    
    @Query("SELECT e FROM #{#entityName} e WHERE e.type = :type and e.subProcessNo = :testInfoNo ORDER BY e.createdDate DESC")
    public List<TrSubNoticeInfo> findNoticeInfoByTypeAndNo(
            @Param("type") SubNoticeType type, 
            @Param("testInfoNo")String testInfoNo);

}
