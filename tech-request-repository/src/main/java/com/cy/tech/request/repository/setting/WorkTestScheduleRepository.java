package com.cy.tech.request.repository.setting;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cy.tech.request.vo.setting.WorkTestSchedule;

/**
 * @author aken_kao
 */
public interface WorkTestScheduleRepository extends JpaRepository<WorkTestSchedule, String>, Serializable {
    
    @Query("SELECT w FROM #{#entityName} w WHERE w.status = 0 and (ifnull(:startDate, null) is null or w.startDate >= :startDate) and (ifnull(:endDate, null) is null or w.endDate >= :endDate) order by w.createdDate")
    public List<WorkTestSchedule> findByDateRange(
            @Param("startDate") Date startDate, 
            @Param("endDate") Date endDate);
    
    @Query("SELECT w FROM #{#entityName} w WHERE w.status = 0 and w.eventId = :eventId")
    public WorkTestSchedule findByEventId(@Param("eventId") String eventId);
    
    @Query("SELECT count(*) FROM #{#entityName} w WHERE w.status = 0 and w.full = true and w.startDate <= :checkDate and w.endDate >= :checkDate")
    public Integer countHasFullByDate(@Param("checkDate") Date checkDate);
}
