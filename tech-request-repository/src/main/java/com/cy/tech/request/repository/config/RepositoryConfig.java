/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.config;

import com.cy.tech.request.vo.config.VoConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 *
 * @author shaun
 */
@Configuration("com.cy.tech.request.repository.config")
@ComponentScan("com.cy.tech.request.repository")
@Import({VoConfig.class})
public class RepositoryConfig {
}
