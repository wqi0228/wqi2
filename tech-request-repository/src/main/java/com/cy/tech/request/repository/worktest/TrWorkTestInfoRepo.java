/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.worktest;

import com.cy.tech.request.vo.worktest.TrWorkTestInfo;
import com.cy.work.common.vo.value.to.JsonStringListTo;

import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author brain0925_liao
 */
public interface TrWorkTestInfoRepo extends JpaRepository<TrWorkTestInfo, String>, Serializable {

    @Query("SELECT r FROM #{#entityName} r WHERE r.testinfoNo = :testinfoNo")
    public TrWorkTestInfo findByTestinfoNo(@Param("testinfoNo") String testinfoNo);

    @Modifying
    @Transactional
    @Query("UPDATE #{#entityName} r SET r.dep_sid  = :dep_sid  WHERE r.testinfoNo = :testinfoNo")
    public int updateDepsid(@Param("testinfoNo") String testinfoNo, @Param("dep_sid") Integer dep_sid);

    @Modifying
    @Transactional
    @Query("UPDATE #{#entityName} r SET r.sendTestDep  = :sendTestDep  WHERE r.sid = :sid")
    public int updateSendTestDep(@Param("sid") String sid, @Param("sendTestDep") JsonStringListTo sendTestDep);
}
