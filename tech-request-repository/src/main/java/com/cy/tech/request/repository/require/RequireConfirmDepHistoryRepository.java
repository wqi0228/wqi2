
package com.cy.tech.request.repository.require;

import com.cy.tech.request.vo.require.RequireConfirmDepHistory;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 需求完成確認單位-異動歷程檔
 * @author allen1214_wu
 */
public interface RequireConfirmDepHistoryRepository extends JpaRepository<RequireConfirmDepHistory, Long>, Serializable {
    
	/**
	 * @param sid
	 * @return
	 */
	public RequireConfirmDepHistory findBySid(Long sid);
	
    /**
     * 以需求單號查詢
     * @param requireSid
     * @return
     */
    public List<RequireConfirmDepHistory> findByRequireSid(String requireSid);
    
    /**
     * 以確認檔 sid 查詢
     * @param requireConfirmSids
     * @return
     */
    public List<RequireConfirmDepHistory> findByRequireConfirmSidIn(List<Long> requireConfirmSids);
    
    
}
