/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.onpg;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.onpg.WorkOnpgAttachment;
import com.cy.tech.request.vo.onpg.WorkOnpgHistory;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface WorkOnpgAttachmentRepo extends JpaRepository<WorkOnpgAttachment, String>, Serializable {

    public List<WorkOnpgAttachment> findByOnpgAndStatusAndHistoryIsNullOrderByCreatedDateDesc(WorkOnpg onpg, Activation status);

    public List<WorkOnpgAttachment> findByHistoryAndStatusOrderByCreatedDateDesc(WorkOnpgHistory history, Activation status);

    @Query(value = "SELECT * FROM work_onpg_attachment a JOIN work_onpg_history h ON a.onpg_history_sid = h.onpg_history_sid "
            + "     WHERE h.onpg_history_sid IN (:historySids) "
            + "       AND a.status = 0"
            + "     ORDER BY a.create_dt DESC ", nativeQuery = true)
    public List<WorkOnpgAttachment> findAttachByHistoryInOrderByUpdateDateDesc(@Param("historySids") List<String> historySids);

}
