/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.worktest;

import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.vo.worktest.WorkTestInfoHistory;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface WorkTestInfoHistoryRepo extends JpaRepository<WorkTestInfoHistory, String>, Serializable {

    @Query("SELECT h FROM #{#entityName} h WHERE h.testInfo = :testInfo AND h.behavior != 'REPLY_AND_REPLY' ORDER BY updatedDate DESC ")
    public List<WorkTestInfoHistory> findByTestInfo(@Param("testInfo") WorkTestInfo testInfo);

    @Query("SELECT CASE WHEN (COUNT(r) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} r "
              + " WHERE r.testinfoNo = :testinfoNo AND r.behaviorStatus IN :behaviorStatus")
    public Boolean isContainBehaviorStatusByTestInfoNo(@Param("testinfoNo") String testinfoNo, @Param("behaviorStatus") List<WorkTestStatus> behaviorStatus);

    @Query("SELECT h FROM #{#entityName} h WHERE h.testinfoNo IN :testinfoNos ORDER BY updatedDate DESC ")
    public List<WorkTestInfoHistory> findByTestInfoNoIn(@Param("testinfoNos") List<String> testinfoNos);
}
