/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.pt;

import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.pt.PtSignInfo;
import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.enums.WorkSourceType;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface PtSignInfoRepo extends JpaRepository<PtSignInfo, String>, Serializable {

	public PtSignInfo findByPtCheck(PtCheck ptCheck);

	public PtSignInfo findByPtNo(String ptNo);

	@Query("SELECT CASE WHEN (COUNT(ptsi) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} ptsi "
	        + " WHERE ptsi.sourceType = :sourceType AND ptsi.sourceNo = :sourceNo AND ptsi.instanceStatus IN :status")
	public Boolean hasAnyProcessNotCompleteBySourceTypeAndSoruceNoAndStatusIn(
	        @Param("sourceType") WorkSourceType sourceType,
	        @Param("sourceNo") String sourceNo,
	        @Param("status") List<InstanceStatus> status);

	@Query("SELECT ptsi FROM #{#entityName} ptsi "
	        + " WHERE ptsi.sourceType = :sourceType AND ptsi.sourceNo = :sourceNo AND ptsi.instanceStatus IN :status")
	public List<PtSignInfo> findProcessNotCompleteBySourceTypeAndSoruceNoAndStatusIn(
	        @Param("sourceType") WorkSourceType sourceType,
	        @Param("sourceNo") String sourceNo,
	        @Param("status") List<InstanceStatus> status);

	@Query(value = ""
	        + "SELECT r.paper_code FROM work_pt_sign_info r "
	        + " WHERE r.pt_check_source_type = :sourceType "
	        + "  AND r.pt_check_source_no = :sourceNo "
	        + "ORDER BY r.pt_check_no DESC LIMIT 1", nativeQuery = true)
	public String findPaperCodeBySourceNoDescPtNo(
	        @Param("sourceType") String sourceType,
	        @Param("sourceNo") String sourceNo);

	/**
	 * 查詢最新一筆單據的簽核狀態
	 * 
	 * @param sourceType
	 * @param sourceNo
	 * @param depSids
	 * @return
	 */
	@Query(value = ""
			+ "SELECT r.paper_code "
			+ "  FROM work_pt_sign_info r "
			+ " INNER JOIN work_pt_check c "
			+ "    ON c.pt_check_sid = r.pt_check_sid "
	        + " WHERE r.pt_check_source_type = :sourceType "
	        + "   AND r.pt_check_source_no = :sourceNo "
	        + "   AND c.dep_sid in (:depSids) "
	        + " ORDER BY r.pt_check_no DESC "
	        + " LIMIT 1 ", //原型確認單作用中的僅會有一筆 
	        nativeQuery = true)
	public String queryLastPaperCodeBySourceNoAndDepSid(
	        @Param("sourceType") String sourceType,
	        @Param("sourceNo") String sourceNo,
	        @Param("depSids") List<Integer> depSids);

	@Query(value = "SELECT r.* FROM work_pt_sign_info r "
	        + "    WHERE r.pt_check_source_type = :sourceType AND r.pt_check_source_no = :sourceNo "
	        + "ORDER BY r.pt_check_no DESC LIMIT 1", nativeQuery = true)
	public PtSignInfo findSigninfoBySourceNoDescPtNo(@Param("sourceType") String sourceType, @Param("sourceNo") String sourceNo);

	@Query(value = "SELECT r.* FROM work_pt_sign_info r "
	        + "    WHERE r.pt_check_source_type = :sourceType AND r.pt_check_source_no = :sourceNo "
	        + "ORDER BY r.pt_check_no DESC", nativeQuery = true)
	public List<PtSignInfo> findBySourceNoDescPtNo(@Param("sourceType") String sourceType, @Param("sourceNo") String sourceNo);

	public PtSignInfo findByBpmInstanceId(String bpmInstanceId);

	@Query(value = "SELECT * FROM work_pt_sign_info wpsi "
	        + "     WHERE wpsi.paper_code IN ('PAPER000','PAPER001','PAPER002','PAPER004');", nativeQuery = true)
	public List<PtSignInfo> findRunningProcess();

}
