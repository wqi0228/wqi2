/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.onpg;

import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.onpg.WorkOnpgHistory;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface WorkOnpgHistoryRepo extends JpaRepository<WorkOnpgHistory, String>, Serializable {

    @Query("SELECT h FROM #{#entityName} h WHERE h.onpg = :onpg AND h.behavior != 'CHECK_RECORD_REPLY' ORDER BY updatedDate DESC ")
    public List<WorkOnpgHistory> findByOnpg(@Param("onpg") WorkOnpg onpg);

    @Query("SELECT h FROM #{#entityName} h WHERE h.onpgNo IN :onpgNos ORDER BY updatedDate DESC ")
    public List<WorkOnpgHistory> findByOnpgNoIn(@Param("onpgNos") List<String> onpgNos);

}
