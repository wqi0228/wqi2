package com.cy.tech.request.repository.require;

import com.cy.tech.request.vo.require.SimpleRequireIndexDictionary;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Allen
 */
public interface SimpleRequireIndexDictionaryRepository extends JpaRepository<SimpleRequireIndexDictionary, String>, Serializable {

	/**
	 * @param requireSid
	 * @return
	 */
	public List<SimpleRequireIndexDictionary> findByRequireSid(String requireSid);

	/**
	 * @param requireSid
	 */
	@Modifying
	@Query("delete from #{#entityName} e where e.requireSid = :requireSid")
	public void deleteByRequireSid(
	        @Param("requireSid") String requireSid);

}
