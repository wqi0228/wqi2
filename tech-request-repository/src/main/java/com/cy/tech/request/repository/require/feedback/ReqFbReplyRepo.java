/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require.feedback;

import com.cy.commons.vo.User;
import com.cy.tech.request.vo.require.RequireTrace;
import com.cy.tech.request.vo.require.feedback.ReqFbkReply;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 需求資訊補充回覆
 *
 * @author shaun
 */
public interface ReqFbReplyRepo extends JpaRepository<ReqFbkReply, String>, Serializable {

    public List<ReqFbkReply> findByTraceOrderByUpdateDateDesc(RequireTrace trace);

    @Query("SELECT DISTINCT r.person FROM #{#entityName} r "
              + "WHERE r.trace = :trace ")
    public List<User> findPersonByTrace(@Param("trace") RequireTrace trace);

}
