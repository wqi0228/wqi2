
package com.cy.tech.request.repository.require;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.enums.ReqConfirmDepProgStatus;
import com.cy.tech.request.vo.require.RequireConfirmDep;
import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author allen1214_wu
 */
public interface RequireConfirmDepRepository extends JpaRepository<RequireConfirmDep, Long>, Serializable {

    /**
     * 以需求單 sid 查詢
     * 
     * @param requireSid
     * @return RequireConfirmDep list (多筆)
     */
    public List<RequireConfirmDep> findByRequireSid(String requireSid);

    /**
     * 以下列條件查詢
     * 
     * @param requireSid 需求單 sid
     * @param depSid     部門 sid
     * @param progStatus 進度
     * @return RequireConfirmDep （單筆）
     */
    public RequireConfirmDep findByRequireSidAndDepSidAndProgStatus(
            String requireSid,
            Integer depSid,
            ReqConfirmDepProgStatus progStatus);

    /**
     * 以下列條件查詢
     * @param requireSid 需求單 sid
     * @param depSid     部門 sid
     * @param progStatus 進度
     * @return RequireConfirmDep （單筆）
     */
    public RequireConfirmDep findByRequireSidAndDepSidAndProgStatusIn(
            String requireSid,
            Integer depSid,
            List<ReqConfirmDepProgStatus> progStatusList);

    /**
     * 以下列條件查詢
     * 
     * @param requireSid 需求單 sid
     * @param depSid     部門 sid
     * @param status     資料狀態
     * @return 單筆
     */
    public RequireConfirmDep findByRequireSidAndDepSidAndStatus(
            String requireSid,
            Integer depSid,
            Activation status);

    /**
     * 以需求單 SID 查詢
     * 
     * @param requireSid 需求單 sid
     * @param status     資料狀態
     * @return RequireConfirmDep list (多筆)
     */
    public List<RequireConfirmDep> findByRequireSidAndStatus(String requireSid, Activation status);

    /**
     * 以下列條件查詢
     * 
     * @param requireSid 需求單 sid
     * @param depSids    部門 sid （多筆）
     * @return RequireConfirmDep （單筆）
     */
    public List<RequireConfirmDep> findByRequireSidInAndDepSidIn(
            List<String> requireSids,
            List<Integer> depSids);

    /**
     * 以下列條件查詢
     * 
     * @param requireSids 需求單 sid (多筆)
     * @param status      部門 sid （多筆）
     * @return RequireConfirmDep list (多筆)
     */
    public List<RequireConfirmDep> findByRequireSidInAndStatus(
            List<String> requireSids,
            Activation status);

    /**
     * 以 SID 查詢
     * 
     * @return RequireConfirmDep
     */
    public RequireConfirmDep findBySid(Long sid);

    /**
     * 查詢SID最大值
     * 
     * @return
     */
    @Query("SELECT MAX(r.sid)  "
            + "FROM #{#entityName} r ")
    public Long findMaxSid();

    /**
     * @param requireSid
     * @return
     */
    @Query("SELECT count(r) > 0 "
            + "FROM #{#entityName} r "
            + "WHERE r.requireSid = :requireSid "
            + "  AND r.status = 0 ")
    public Boolean isExsitByRequireSid(
            @Param("requireSid") String requireSid);

    /**
     * 以下條件查詢
     * 
     * @param inDepSids
     * @param inProgStatus
     * @return 回傳 <br/>
     *             需求單 sid <br/>
     *             單位 sid <br/>
     *             負責人 sid
     */
    @Query(value = ""
            + "SELECT require_sid, "
            + "       dep_sid, "
            + "       owner_sid "
            + "FROM tr_require_confirm_dep r "
            + "WHERE r.status = 0 "
            + "  AND r.dep_sid in (:inDepSids) "
            + "  AND r.prog_status in (:inProgStatus) ", nativeQuery = true)
    public List<Object[]> queryByDepSidInAndProgStatusIn(
            @Param("inDepSids") List<Integer> inDepSids,
            @Param("inProgStatus") List<String> inProgStatus);

    /**
     * 以以下條件查詢
     * 
     * @param requireSid
     * @param inProgStatus
     * @return
     */
    @Query("SELECT r "
            + "FROM #{#entityName} r "
            + "WHERE r.requireSid = :requireSid "
            + "  AND r.status = 0 "
            + "  AND r.progStatus in (:inProgStatus) ")
    public List<RequireConfirmDep> queryByRequireSidAndProgStatus(
            @Param("requireSid") String requireSid,
            @Param("inProgStatus") List<ReqConfirmDepProgStatus> inProgStatus);

    /**
     * 以下列參數為條件統計數量
     * 
     * @param requireSid
     * @return
     */
    @Query("SELECT COUNT(r) AS size "
            + "FROM #{#entityName} r "
            + "WHERE r.requireSid = :requireSid "
            + "  AND r.status = 0 ")
    public Integer queryCountByRequireSid(
            @Param("requireSid") String requireSid);

    /**
     * 以下列參數為條件統計數量
     * 
     * @param requireSid
     * @param inProgStatus
     * @return
     */
    @Query("SELECT COUNT(r) AS size "
            + "FROM #{#entityName} r "
            + "WHERE r.requireSid = :requireSid "
            + "  AND r.status = 0 "
            + "  AND r.progStatus in (:inProgStatus) ")
    public Integer queryCountByRequireSidAndProgStatus(
            @Param("requireSid") String requireSid,
            @Param("inProgStatus") List<ReqConfirmDepProgStatus> inProgStatus);

    /**
     * @param requireSid
     * @return
     */
    @Query("SELECT r.depSid "
            + "FROM #{#entityName} r "
            + "WHERE r.requireSid = :requireSid "
            + "  AND r.status = 0 ")
    public List<Integer> querydepSidByRequireSid(
            @Param("requireSid") String requireSid);

    /**
     * 更新負責人 (ownerSid)
     * 
     * @param sid
     * @param ownerSid
     * @return
     */
    // @Transactional(rollbackFor = Exception.class)
    // @Modifying
    // @Query("update RequireConfirmDep dep set dep.ownerSid = ?2 where dep.sid =
    // ?1")
    // @Deprecated //@Modifying 方法禁用
    // public int setOwner(Long sid, Integer ownerSid);

    /**
     * 更新進度
     * 
     * @param sid
     * @param progStatus
     * @param updateUserSid
     * @param updateDate
     * @return
     */
    // @Transactional(rollbackFor = Exception.class)
    // @Modifying
    // @Query("update RequireConfirmDep dep "
    // + "set dep.progStatus = ?2, "
    // + " dep.updatedUser = ?3, "
    // + " dep.updatedDate = ?4 "
    // + "where dep.sid = ?1")
    // @Deprecated //@Modifying 方法禁用
    // public int setProgStatus(Long sid, ReqConfirmDepProgStatus progStatus,
    // Integer updatedUser, Date updatedDate);

}
