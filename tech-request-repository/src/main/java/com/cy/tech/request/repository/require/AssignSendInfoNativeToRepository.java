package com.cy.tech.request.repository.require;

import com.cy.tech.request.vo.require.AssignSendInfoNativeTo;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 分派通知
 */
public interface AssignSendInfoNativeToRepository extends JpaRepository<AssignSendInfoNativeTo, String>, Serializable {    
    /**
     * @param sids
     * @return
     */
    public List<AssignSendInfoNativeTo> findBySidIn(List<String> sids);
}
