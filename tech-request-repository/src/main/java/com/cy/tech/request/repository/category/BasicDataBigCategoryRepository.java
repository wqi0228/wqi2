package com.cy.tech.request.repository.category;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.category.BasicDataBigCategory;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface BasicDataBigCategoryRepository extends JpaRepository<BasicDataBigCategory, String>, Serializable {

    public List<BasicDataBigCategory> findByStatusOrderByIdAsc(Activation status);

    @Query("SELECT b FROM #{#entityName} b WHERE b.status in :status AND (b.id LIKE :searchText OR b.name LIKE :searchText) ORDER BY b.id")
    public List<BasicDataBigCategory> findByStatusAndFuzzyText(@Param("status") List<Activation> status, @Param("searchText") String searchText);

    public BasicDataBigCategory findById(String id);

    public BasicDataBigCategory findByName(String name);

    @Query("SELECT CASE WHEN (COUNT(b) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} b WHERE b.id = :id")
    public Boolean isExistId(@Param("id") String id);

    @Query("SELECT CASE WHEN (COUNT(b) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} b WHERE b.name = :name")
    public Boolean isExistName(@Param("name") String name);

    @Query("SELECT CASE WHEN (COUNT(b) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} b WHERE b.sid != :sid AND b.id = :id")
    public Boolean isExistIdByEdit(@Param("sid") String sid, @Param("id") String id);

    @Query("SELECT CASE WHEN (COUNT(b) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} b WHERE b.sid != :sid AND b.name = :name")
    public Boolean isExistNameByEdit(@Param("sid") String sid, @Param("name") String name);

    @Query(
              value = "SELECT CASE WHEN (COUNT(*) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM tr_basic_data_big_category big "
              + "    LEFT JOIN tr_basic_data_middle_category middle ON middle.parent_big_category = :sid "
              + "    LEFT JOIN tr_basic_data_small_category small ON middle.basic_data_middle_category_sid = small.parent_middle_category "
              + "      WHERE middle.status = 0 OR small.status = 0", nativeQuery = true)
    public Boolean checkChildIsActive(@Param("sid") String sid);

    @Query(
              value = "SELECT CASE WHEN (COUNT(*) > 0) THEN 'TRUE' ELSE 'FALSE' END "
              		+ "  FROM #{#entityName} b "
              + "   LEFT JOIN b.children middles ON middles.parentBigCategory.sid = :sid "
              + "   LEFT JOIN middles.children smalls"
              + "       WHERE middles.status = :status OR smalls.status = :status ")
    public Boolean checkChildsByStatus(
    		@Param("sid") String sid, 
    		@Param("status") Activation status);
}
