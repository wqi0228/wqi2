/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.worktest;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.worktest.WorkTestAttachment;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.vo.worktest.WorkTestInfoHistory;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface WorkTestAttachmentRepo extends JpaRepository<WorkTestAttachment, String>, Serializable {

    public List<WorkTestAttachment> findByTestInfoAndStatusAndHistoryIsNullOrderByCreatedDateDesc(WorkTestInfo testinfo, Activation status);

    public List<WorkTestAttachment> findByHistoryAndStatusOrderByCreatedDateDesc(WorkTestInfoHistory history, Activation status);

    @Query(value = "SELECT * FROM work_test_attachment a JOIN work_test_info_history h ON a.testinfo_history_sid = h.testinfo_history_sid "
              + "     WHERE h.testinfo_history_sid IN (:historySids) "
              + "       AND a.status = 0"
              + "     ORDER BY a.create_dt DESC ", nativeQuery = true)
    public List<WorkTestAttachment> findAttachByHistoryInOrderByUpdateDateDesc(@Param("historySids") List<String> historySids);

}
