/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.require.RequireFbkAttachment;
import com.cy.tech.request.vo.require.RequireTrace;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 需求資訊回覆補充附加檔案
 *
 * @author shaun
 */
public interface RequireFbkAttachmentRepo extends JpaRepository<RequireFbkAttachment, String>, Serializable {

    public List<RequireFbkAttachment> findByTraceAndStatus(RequireTrace trace, Activation status);
}
