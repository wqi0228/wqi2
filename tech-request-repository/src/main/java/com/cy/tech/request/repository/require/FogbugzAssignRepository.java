/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.require;

import com.cy.tech.request.vo.require.FogbugzAssign;
import com.cy.tech.request.vo.require.Require;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * FB 轉入記錄 DAO
 *
 * @author shaun
 */
public interface FogbugzAssignRepository extends JpaRepository<FogbugzAssign, String>, Serializable {

    public List<FogbugzAssign> findByRequireOrderByUpdatedDateDesc(Require require);

}
