package com.cy.tech.request.repository.menu;

import com.cy.tech.request.vo.constants.CacheConstants;
import com.cy.tech.request.vo.enums.MenuBaseType;
import com.cy.tech.request.vo.menu.TrMenuGroupBase;
import java.io.Serializable;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * 需建立另一個findByType方法
 *
 * @author shaun
 */
@Repository
public interface TrMenuGroupBaseRepository extends JpaRepository<TrMenuGroupBase, String>, Serializable {

    @Cacheable(value = CacheConstants.CACHE_PORTAL_MENU, key = "T(java.util.Objects).hash(#p0)")
    public TrMenuGroupBase findByType(MenuBaseType type);

}
