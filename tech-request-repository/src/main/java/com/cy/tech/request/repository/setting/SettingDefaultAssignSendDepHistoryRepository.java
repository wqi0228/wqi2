package com.cy.tech.request.repository.setting;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.setting.asdep.SettingDefaultAssignSendDepHistory;

/**
 * @author allen 異動記錄:檢查確認預設分派/通知單位設定
 */
public interface SettingDefaultAssignSendDepHistoryRepository extends JpaRepository<SettingDefaultAssignSendDepHistory, String>, Serializable {

    /**
     * 以下列條件查詢
     * 
     * @param smallCategorySid 小類 SID
     * @param checkItemType    檢查項目
     * @param assignSendType   分派/通知類別
     * @return
     */
    public List<SettingDefaultAssignSendDepHistory> findBySmallCategorySidAndCheckItemTypeOrderByCreatedDateDesc(
            String smallCategorySid,
            RequireCheckItemType checkItemType
            );

}
