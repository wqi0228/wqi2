/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.onpg;

import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.onpg.WorkOnpgCheckRecord;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author shaun
 */
public interface WorkOnpgCheckRecordRepo extends JpaRepository<WorkOnpgCheckRecord, String>, Serializable {

    /**
     * @param onpg
     * @return
     */
    public List<WorkOnpgCheckRecord> findByOnpg(WorkOnpg onpg);
    
    /**
     * @param sid
     * @return
     */
    public WorkOnpgCheckRecord findBySid(String sid);

}
