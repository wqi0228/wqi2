/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.onpg;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.cy.work.common.vo.value.to.ReadRecordGroupTo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author shaun
 */
public interface WorkOnpgRepo extends JpaRepository<WorkOnpg, String>, Serializable {

    @Query("SELECT COUNT(w) AS size FROM #{#entityName} w WHERE w.createdDate BETWEEN :start AND :end")
    public Integer findTodayTotalSize(@Param("start") Date start, @Param("end") Date end);

    /**
     * 以需求單號查詢
     * 
     * @param requireNo
     * @return
     */
    @Query(value = ""
            + "SELECT COUNT(*) AS size "
            + "  FROM work_onpg  "
            + " WHERE status = 0 "
            + "   AND onpg_source_no = :requireNo ", nativeQuery = true)
    public Integer countActiveByRequireNo(@Param("requireNo") String requireNo);

    @Query("SELECT onpgNo FROM #{#entityName} w WHERE w.createdDate BETWEEN :start AND :end ORDER BY w.createdDate DESC")
    public List<String> findTodayLastNo(@Param("start") Date start, @Param("end") Date end);

    @Query("SELECT CASE WHEN (COUNT(w) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} w WHERE w.onpgNo = :onpgNo")
    public Boolean isExistNo(@Param("onpgNo") String onpgNo);

    public List<WorkOnpg> findBySourceSidOrderByCreatedDateDesc(@Param("sourceSid") String sourceSid);

    @Modifying
    @Query("UPDATE #{#entityName} w SET w.readRecord = :readRecordGroup , w.readUpdateDate = :readUpdateDate WHERE w = :onpg")
    public int updateReadRecord(@Param("onpg") WorkOnpg onpg,
            @Param("readRecordGroup") ReadRecordGroupTo readRecordGroup, @Param("readUpdateDate") Date readUpdateDate);

    @Modifying
    @Query("UPDATE #{#entityName} w SET w.readRecord = :readRecordGroup , w.readUpdateDate = :readUpdateDate WHERE w.sid = :sid")
    public int updateReadRecord(
            @Param("sid") String sid,
            @Param("readRecordGroup") ReadRecordGroupTo readRecordGroup,
            @Param("readUpdateDate") Date readUpdateDate);

    /**
     * update 通知單位
     * 
     * @param sid        sid
     * @param noticeDeps 通知單位
     * @return result
     */
    @Modifying
    @Query("UPDATE #{#entityName} w SET w.noticeDeps = :noticeDeps WHERE w.sid = :sid")
    public int updateNoticeDeps(
            @Param("sid") String sid,
            @Param("noticeDeps") JsonStringListTo noticeDeps);

    @Query("SELECT w.sid FROM #{#entityName} w WHERE w.sourceType = :sourceType AND w.sourceSid = :sourceSid ORDER BY w.createdDate DESC")
    public List<String> findSidsBySourceTypeAndSourceSid(@Param("sourceType") WorkSourceType sourceType,
            @Param("sourceSid") String sourceSid);

    @Query("SELECT onpgStatus FROM #{#entityName} w WHERE w = :onpg")
    public WorkOnpgStatus findOnpgStatus(@Param("onpg") WorkOnpg testinfo);

    @Query("SELECT COUNT(w) AS size FROM #{#entityName} w "
            + "WHERE w.sourceType = :sourceType AND w.sourceNo = :sourceNo AND  w.onpgStatus IN :status")
    public Integer findCountBySourceTypeAndSourceNoAndStatus(
            @Param("sourceType") WorkSourceType sourceType,
            @Param("sourceNo") String sourceNo,
            @Param("status") List<WorkOnpgStatus> status);

    public WorkOnpg findByOnpgNo(String onpgNo);

    /**
     * 以【單據流程狀態】和【開單部門】查詢主檔筆數
     * 
     * @param sourceType
     * @param sourceNo
     * @param createDeps
     * @param inOnpgStatus
     * @return
     */
    @Query("SELECT COUNT(r) AS size "
            + "FROM #{#entityName} r "
            + "WHERE r.sourceType = :sourceType "
            + "AND r.sourceNo = :sourceNo "
            + "AND r.status = 0 "
            + "AND r.createDep in (:createDeps) "
            + "AND r.onpgStatus in (:inOnpgStatus) ")
    public Integer queryCountByCreateDepAndInOnpgStatus(
            @Param("sourceType") WorkSourceType sourceType,
            @Param("sourceNo") String sourceNo,
            @Param("createDeps") List<Org> createDeps,
            @Param("inOnpgStatus") List<WorkOnpgStatus> inOnpgStatus);

    /**
     * 以【單據流程狀態】和【開單部門】查詢主檔筆數
     * 
     * @param sourceType
     * @param sourceNo
     * @param createDeps
     * @param inOnpgStatus
     * @return
     */
    @Query("SELECT r "
            + "FROM #{#entityName} r "
            + "WHERE r.sourceType = :sourceType "
            + "AND r.sourceNo = :sourceNo "
            + "AND r.status = 0 "
            + "AND r.createDep in (:createDeps) "
            + "AND r.onpgStatus in (:inOnpgStatus) ")
    public List<WorkOnpg> queryByCreateDepAndInOnpgStatus(
            @Param("sourceType") WorkSourceType sourceType,
            @Param("sourceNo") String sourceNo,
            @Param("createDeps") List<Org> createDeps,
            @Param("inOnpgStatus") List<WorkOnpgStatus> inOnpgStatus);

    /**
     * 以【單據流程狀態】查詢主檔筆數
     * 
     * @param sourceType
     * @param sourceNo
     * @param createDeps
     * @param inOnpgStatus
     * @return
     */
    @Query("SELECT COUNT(r) AS size "
            + "FROM #{#entityName} r "
            + "WHERE r.sourceType = :sourceType "
            + "AND r.sourceNo = :sourceNo "
            + "AND r.status = 0 "
            + "AND r.onpgStatus in (:inOnpgStatus) ")
    public Integer queryCountByOnpgStatus(
            @Param("sourceType") WorkSourceType sourceType,
            @Param("sourceNo") String sourceNo,
            @Param("inOnpgStatus") List<WorkOnpgStatus> inOnpgStatus);

    /**
     * 依據【單據流程狀態】查詢
     * 
     * @param sourceType   送測來源
     * @param sourceSid    sid
     * @param inOnpgStatus 單據狀態
     * @return list WorkOnpg
     */
    public List<WorkOnpg> findBySourceTypeAndSourceSidAndOnpgStatusIn(
            WorkSourceType sourceType,
            String sourceSid,
            List<WorkOnpgStatus> inOnpgStatus);

    /**
     * 依據【單據流程狀態】查詢
     * 
     * @param sourceType   送測來源
     * @param sourceSid    sid
     * @param inOnpgStatus 單據狀態
     * @param createDeps   歸屬單位
     * @return list WorkOnpg
     */
    public List<WorkOnpg> findBySourceTypeAndSourceSidAndOnpgStatusInAndCreateDepIn(
            WorkSourceType sourceType,
            String sourceSid,
            List<WorkOnpgStatus> inOnpgStatus,
            List<Org> createDeps);

    /**
     * 以下列欄位查詢
     * 
     * @param sourceType 來源
     * @param status     資料狀態
     * @param sourceNo   單號
     * @param createDeps 立案部門
     * @return
     */
    public List<WorkOnpg> findBySourceTypeAndStatusAndSourceNoAndCreateDepIn(
            WorkSourceType sourceType,
            Activation ststus,
            String sourceNo,
            List<Org> createDeps);

}
