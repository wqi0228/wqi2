package com.cy.tech.request.repository.category;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.category.BasicDataMiddleCategory;
import com.cy.tech.request.vo.category.BasicDataSmallCategory;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author shaun
 */
public interface BasicDataSmallCategoryRepository extends JpaRepository<BasicDataSmallCategory, String>, Serializable {
    
    public BasicDataSmallCategory findBySid(String sid);

    public List<BasicDataSmallCategory> findByStatusOrderByIdAsc(Activation status);

    @Query("SELECT b FROM #{#entityName} b WHERE b.status in :status AND (b.id LIKE :searchText OR b.name LIKE :searchText) ORDER BY b.id")
    public List<BasicDataSmallCategory> findByStatusAndFuzzyText(@Param("status") List<Activation> status, @Param("searchText") String searchText);

    public BasicDataSmallCategory findById(String id);

    public BasicDataSmallCategory findByName(String name);

    @Query("SELECT CASE WHEN (COUNT(b) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} b WHERE b.id = :id")
    public Boolean isExistId(@Param("id") String id);

    @Query("SELECT CASE WHEN (COUNT(b) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} b WHERE b.name = :name")
    public Boolean isExistName(@Param("name") String name);

    @Query("SELECT CASE WHEN (COUNT(b) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} b WHERE b.sid != :sid AND b.id = :id")
    public Boolean isExistIdByEdit(@Param("sid") String sid, @Param("id") String id);

    @Query("SELECT CASE WHEN (COUNT(b) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} b WHERE b.sid != :sid AND b.name = :name")
    public Boolean isExistNameByEdit(@Param("sid") String sid, @Param("name") String name);

    @Query("SELECT b FROM #{#entityName} b WHERE b.parentMiddleCategory = :parent ORDER BY b.id ")
    public List<BasicDataSmallCategory> findByParentMiddle(@Param("parent") BasicDataMiddleCategory parent);

    @Query("SELECT s FROM #{#entityName} s "
            + "WHERE s.status = :status "
            + "AND s.showItemInTc = :showItemInTc "
            + "ORDER BY s.id ")
    public List<BasicDataSmallCategory> findByStatusAndShowItemInTc(
            @Param("status") Activation status,
            @Param("showItemInTc") Boolean showItemInTc
    );

}
