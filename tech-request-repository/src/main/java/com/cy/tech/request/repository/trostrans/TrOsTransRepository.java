/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.repository.trostrans;

import com.cy.tech.request.vo.require.tros.TrOs;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author brain0925_liao
 */
public interface TrOsTransRepository extends JpaRepository<TrOs, String>, Serializable {

    @Query("SELECT r FROM #{#entityName} r WHERE r.os_no = :os_no")
    public TrOs findByOsNo(@Param("os_no") String os_no);

    @Modifying
    @Transactional(rollbackFor = Exception.class)
    @Query("UPDATE #{#entityName} r SET r.dep_sid  = :dep_sid  WHERE r.os_no = :os_no")
    public int updateDepsid(@Param("os_no") String os_no, @Param("dep_sid") Integer dep_sid);

    @Modifying
    @Transactional(rollbackFor = Exception.class)
    @Query("UPDATE #{#entityName} r SET r.noticeDeps  = :noticeDeps  WHERE r.sid = :sid")
    public int updateNoticeDeps(@Param("sid") String sid, @Param("noticeDeps") JsonStringListTo noticeDeps);
}
