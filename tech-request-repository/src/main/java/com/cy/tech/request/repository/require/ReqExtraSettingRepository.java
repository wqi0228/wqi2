package com.cy.tech.request.repository.require;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.setting.RequireExtraSetting;

/**
 * 
 * @author aken_kao
 *
 */
public interface ReqExtraSettingRepository extends JpaRepository<RequireExtraSetting, String>, Serializable {

	/**
	 * @param sid
	 * @param code
	 * @return
	 */
	@Query("SELECT r FROM #{#entityName} r WHERE r.sid = :sid AND r.code = :code")
	public RequireExtraSetting findValueBySidAndCode(@Param("sid") String sid, @Param("code") String code);

	/**
	 * @param require
	 * @return
	 */
	public List<RequireExtraSetting> findByRequire(Require require);
}
