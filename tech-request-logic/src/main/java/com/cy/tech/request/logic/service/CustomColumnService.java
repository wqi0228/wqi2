/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import com.cy.commons.vo.User;
import com.cy.tech.request.repository.column.CustomColumnRepository;
import com.cy.tech.request.vo.column.CustomColumn;
import com.cy.tech.request.vo.value.to.CustomColumnDetailTo;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kasim
 */
@Component
public class CustomColumnService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2353787446422273994L;
    @Autowired
    private CustomColumnRepository dao;

    /**
     * 依據登入者查詢客製化欄位資料
     *
     * @param login
     * @return CustomColumn
     */
    @Transactional(readOnly = true)
    public CustomColumn findByUser(User login) {
        return dao.findByUser(login);
    }

    /**
     * 依據登入者查詢客製化欄位資料
     *
     * @param login
     * @return CustomColumn
     */
    @Transactional(rollbackFor = Exception.class)
    public CustomColumn save(CustomColumn obj) {
        obj.setUpdatedDate(new Date());
        return dao.save(obj);
    }

    /**
     * 取得欄位所對應的CustomColumnDetailTo
     *
     * @param headerText
     * @param customColumnDetailTos
     * @return CustomColumnDetailTo
     */
    public CustomColumnDetailTo filterCustomColumnDetailTo(String headerText, List<CustomColumnDetailTo> customColumnDetailTos) {
        if (customColumnDetailTos == null || customColumnDetailTos.isEmpty()) {
            return null;
        }
        for (CustomColumnDetailTo obj : customColumnDetailTos) {
            if (obj.getName().equals(headerText)) {
                return obj;
            }
        }
        return null;
    }

    /**
     * 更新columnFields
     *
     * @param columnFields
     * @param value
     */
    public void changeColumnFields(LinkedHashMap<String, Boolean> columnFields, boolean value) {
        String key;
        Iterator<String> iter = columnFields.keySet().iterator();
        while (iter.hasNext()) {
            key = iter.next();
            columnFields.put(key, value);
        }
    }

    /**
     * dataTable顯示欄位判斷
     *
     * @param headerText
     * @param customColumnDetailTos
     * @return boolean
     */
    public boolean chekcDataTableColumn(String headerText, List<CustomColumnDetailTo> customColumnDetailTos) {
        if (customColumnDetailTos == null) {
            return true;
        }
        CustomColumnDetailTo obj = filterCustomColumnDetailTo(headerText, customColumnDetailTos);
        if (obj == null) {
            return false;
        }
        return true;
    }

    /**
     * dataTable顯示欄位寬度
     *
     * @param headerText
     * @param customColumnDetailTos
     * @return boolean
     */
    public String getColumnWidth(String headerText, List<CustomColumnDetailTo> customColumnDetailTos) {
        if (customColumnDetailTos == null) {
            return "";
        }
        CustomColumnDetailTo obj = filterCustomColumnDetailTo(headerText, customColumnDetailTos);
        if (obj == null) {
            return "";
        }
        return obj.getWidth();
    }

    /**
     * 更新customColumnDetailTos
     *
     * @param pathUrl
     * @param columnFields
     * @param customColumnDetailTos
     */
    public List<CustomColumnDetailTo> updateCustomColumnDetailTos(
            String pathUrl, LinkedHashMap<String, Boolean> columnFields, List<CustomColumnDetailTo> customColumnDetailTos) {
        List<CustomColumnDetailTo> customCols = Lists.newArrayList();
        String key;
        Iterator<String> iter = columnFields.keySet().iterator();
        CustomColumnDetailTo obj;
        while (iter.hasNext()) {
            key = iter.next();
            if (columnFields.get(key)) {
                obj = filterCustomColumnDetailTo(key, customColumnDetailTos);
                if (obj == null) {
                    obj = new CustomColumnDetailTo();
                    obj.setName(key);
                }
                customCols.add(obj);
            }
        }
        return customCols;
    }

}
