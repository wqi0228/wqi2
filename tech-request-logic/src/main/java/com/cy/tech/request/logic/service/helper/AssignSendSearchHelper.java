/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.helper;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.logic.service.AssignNoticeService;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepService;
import com.cy.tech.request.logic.vo.GroupItemTo;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.tech.request.vo.require.AssignSendSearchInfo;
import com.cy.tech.request.vo.require.AssignSendSearchInfoVO;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.cy.tech.request.repository.require.AssignSendSearchInfoRepo;
import com.cy.tech.request.vo.require.AssignSendInfo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 分派通知搜尋輔助
 *
 * @author shaun
 */
@Component
@Slf4j
public class AssignSendSearchHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3751125862707227026L;
    @Autowired
    private AssignSendSearchInfoRepo dao;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private AssignNoticeService assignNoticeService;
    @Autowired
    private RequireConfirmDepService requireConfirmDepService;
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private JdbcTemplate jdbcTemplate;
    @PersistenceContext
    transient private EntityManager em;
    @Autowired
    transient private com.cy.tech.request.logic.service.Set10V70TrneBatchHelper set10V70TrneBatchHelper;

    /**
     * 建立分派/通知單位查詢資訊
     * 
     * @param require
     * @param executor
     * @param createDt
     * @param assignDepSids
     * @param noticeDepSids
     */
    @Transactional(rollbackFor = Exception.class)
    public void buildAssignNoticeDepSearchInfo(
            String requireSid,
            String requireNo,
            User executor,
            Date execDate,
            List<Integer> assignDepSids,
            List<Integer> noticeDepSids,
            boolean isShowDebug) {

        Long startTime = System.currentTimeMillis();
        String procTitle = this.assignNoticeService.prepareLogTitle(requireNo, "建立分派/通知單位查詢資訊");
        if (isShowDebug) {
            log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));
        }
        // 移除前一次的設定值
        this.removeSearchInfo(requireSid, isShowDebug);
        // 建立分派單位
        this.createDepSearchInfo(
                requireSid,
                requireNo,
                executor,
                execDate,
                AssignSendType.ASSIGN,
                assignDepSids);

        // 建立通知單位
        this.createDepSearchInfo(
                requireSid,
                requireNo,
                executor,
                execDate,
                AssignSendType.SEND,
                noticeDepSids);

        if (isShowDebug) {
            log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
        }
    }

    /**
     * 建立分派搜尋資訊<BR/>
     *
     * @param require
     * @param executor
     * @param createDt
     * @param assignGroups
     */
    @Transactional(rollbackFor = Exception.class)
    public void buildAssignSearchInfo(Require require, User executor, Date createDt, List<GroupItemTo> assignGroups) {
        this.removeSearchInfo(require.getSid(), true);
        this.createSearchInfo(require, executor, createDt, AssignSendType.ASSIGN, assignGroups);
    }

    /**
     * 解除關聯並刪除內容
     *
     * @param require
     */
    @Transactional(rollbackFor = Exception.class)
    private int removeSearchInfo(String requireSid, boolean isShowDebug) {
        int removeCount = dao.deleteAssignByRequireSid(requireSid);
        if (isShowDebug) {
            log.debug("delete tr_assign_send_search_info : [{}]筆 by {} ",
                    removeCount,
                    requireSid);
        }
        return removeCount;
    }

    /**
     * @param require
     * @param executor
     * @param createDt
     * @param type
     * @param groupsItem
     */
    private void createSearchInfo(Require require, User executor, Date createDt, AssignSendType type, List<GroupItemTo> groupsItem) {

        List<AssignSendSearchInfoVO> insertAssignSendSearchInfoVOs = Lists.newArrayList();

        for (GroupItemTo each : groupsItem) {
            AssignSendSearchInfoVO search = new AssignSendSearchInfoVO();
            insertAssignSendSearchInfoVOs.add(search);

            search.setRequireSid(require.getSid());
            search.setRequireNo(require.getRequireNo());
            search.setType(type);
            if (each.getType().equals(GroupItemTo.GroupItemToType.DEPT) || each.getType().equals(GroupItemTo.GroupItemToType.GROUP_DEPT)) {
                search.setDepSid(Integer.parseInt(each.getKey()));
            } else {
                search.setMemberSid(Integer.parseInt(each.getKey()));
            }

            search.setCreatedDate(createDt);
            search.setCreatedUserSid(executor.getSid());

        }

        set10V70TrneBatchHelper.batchInsertAssignSendSearchInfos(insertAssignSendSearchInfoVOs);
    }

    /**
     * 建立分派/通知部門查詢資訊 (包含需求完成確認單位) (tr_assign_send_search_info)
     * 
     * @param requireSid
     * @param requireNo
     * @param executor
     * @param createDt
     * @param type
     * @param depSids
     */
    public void createDepSearchInfo(
            String requireSid,
            String requireNo,
            User executor,
            Date execDate,
            AssignSendType type,
            List<Integer> depSids) {

        if (WkStringUtils.isEmpty(depSids)) {
            return;
        }

        Set<Integer> depInfo = Sets.newHashSet(depSids);

        // ====================================
        // 加入需求完成確認單位
        // ====================================
        if (AssignSendType.ASSIGN.equals(type)) {
            List<Integer> requireConfirmDeps = this.requireConfirmDepService.prepareRequireConfirmDeps(depSids).stream()
                    .map(Org::getSid)
                    .collect(Collectors.toList());

            depInfo.addAll(requireConfirmDeps);
        }

        // ====================================
        // 準備資料
        // ====================================
        List<AssignSendSearchInfoVO> insertAssignSendSearchInfoVOs = Lists.newArrayList();

        for (Integer depSid : depInfo) {
            Org dep = WkOrgCache.getInstance().findBySid(depSid);
            if (dep == null) {
                log.warn("找不到 depSid:[" + depSid + "]");
                continue;
            }

            AssignSendSearchInfoVO search = new AssignSendSearchInfoVO();
            insertAssignSendSearchInfoVOs.add(search);

            search.setRequireSid(requireSid);
            search.setRequireNo(requireNo);
            search.setType(type);
            search.setDepSid(depSid);
            search.setCreatedDate(execDate);
            search.setCreatedUserSid(executor.getSid());
        }

        // ====================================
        // 批次 insert
        // ====================================
        set10V70TrneBatchHelper.batchInsertAssignSendSearchInfos(insertAssignSendSearchInfoVOs);

        // String message = String.format("【%s-insert
        // [tr_assign_send_search_info].[%s]:%s筆 】：\r\n%s",
        // requireNo,
        // type.name(),
        // depInfo.size(),
        // WkOrgUtils.findNameBySid(Lists.newArrayList(depInfo), "、", true));

        // log.debug(message);
    }

    @Transactional(readOnly = true)
    public List<AssignSendSearchInfo> findByRequire(Require require) {
        return dao.findByRequire(require);
    }

    @Transactional(readOnly = true)
    public Set<Integer> findAssignedDepSidsByRequire(Require require) {
        return this.findByRequire(require).stream()
                .filter(info -> info.getDepartment() != null)
                .map(info -> info.getDepartment().getSid())
                .collect(Collectors.toSet());
    }

    @Transactional(readOnly = true)
    public Set<Integer> findAssignedMemberSidsByRequire(Require require) {
        return this.findByRequire(require).stream()
                .filter(info -> info.getMember() != null)
                .map(info -> info.getMember().getSid())
                .collect(Collectors.toSet());
    }

    /**
     * 查詢所有送測單位(含向下單位)
     *
     * @param require
     * @return
     */
    @Transactional(readOnly = true)
    public Set<Integer> findAssignedChildDepSidsByRequire(Require require) {
        Set<Integer> depSids = Sets.newHashSet();
        List<AssignSendSearchInfo> assignSendSearchInfo = this.findByRequire(require).stream()
                .filter(info -> info.getDepartment() != null).collect(Collectors.toList());
        List<Org> orgs = Lists.newArrayList();
        assignSendSearchInfo.forEach(item -> {
            orgs.add(item.getDepartment());
        });
        for (Org o : orgs) {
            depSids.add(o.getSid());
            List<Org> childs = orgService.findChildOrgs(o);
            for (Org child : childs) {
                depSids.add(child.getSid());
            }
        }
        return depSids;
        // forEach(o -> {
        //
        // //orgService.findChildOrgs(o).forEach(child ->);
        // });
        // return depSids;
    }

    @Transactional(rollbackFor = Exception.class)
    public void createSearchInfoByBackstage(AssignSendInfo info, Integer orgSid) {
        Org createDep = new Org(orgSid);
        Require createReq = new Require();
        createReq.setSid(info.getRequireSid());

        AssignSendSearchInfo search = new AssignSendSearchInfo();
        search.setCreatedDate(info.getCreatedDate());
        search.setCreatedUser(info.getCreatedUser());
        search.setDepartment(createDep);
        search.setRequire(createReq);
        search.setRequireNo(info.getRequireNo());
        search.setType(info.getType());
        dao.save(search);
    }

    @Transactional(rollbackFor = Exception.class)
    public void removeSearchInfoByBackstage(String reqSid, Integer orgSid) {
        dao.deleteAssignByReqSidAndDepSid(reqSid, orgSid);
    }

    public List<AssignSendSearchInfo> findByRequireNoAndType(String requireNo, AssignSendType type) {
        return dao.findByRequireNoAndType(requireNo, type);
    }

    /**
     * 以需求單號進行整批刪除
     * 
     * @param requireNo
     */
    @Transactional(rollbackFor = Exception.class)
    public void deleteByReqNo(String requireNo) {
        String sql = "delete from tr_assign_send_search_info where require_no = ? ";
        this.jdbcTemplate.execute(sql, new PreparedStatementCallback<Boolean>() {
            @Override
            public Boolean doInPreparedStatement(PreparedStatement ps)
                    throws SQLException, DataAccessException {
                ps.setString(1, requireNo);
                return ps.execute();
            }
        });
        // this.em.flush();
    }

    /**
     * 以需求單 sid 查詢對應分派部門
     * 
     * @param requireSids 需求單 sid (多筆)
     * @return Map[requireSid, Set[depSid] ]
     */
    public Map<String, Set<Integer>> findAssignDepSidsMapByRequireSids(Set<String> requireSids) {

        if (WkStringUtils.isEmpty(requireSids)) {
            return Maps.newHashMap();
        }

        // ====================================
        // 組 SQL
        // ====================================
        // 需求單 sid where in 字串
        String requireSidsStr = requireSids.stream().map(sid -> sid + "").collect(Collectors.joining("', '"));

        StringBuffer sql = new StringBuffer();
        sql.append("SELECT require_sid, ");
        sql.append("       dep_sid ");
        sql.append("FROM   tr_assign_send_search_info ");
        sql.append("WHERE  type = 0 "); // 指定為『分派』資料
        sql.append("  AND  require_sid IN ( '" + requireSidsStr + "' )");

        // ====================================
        // 查詢
        // ====================================
        List<Map<String, Object>> dbRowDataMaps = this.jdbcTemplate.queryForList(sql.toString());

        if (WkStringUtils.isEmpty(dbRowDataMaps)) {
            return Maps.newHashMap();
        }

        // ====================================
        // 收集資料
        // ====================================
        Map<String, Set<Integer>> assignDepSidsMapByRequireSid = Maps.newHashMap();

        for (Map<String, Object> rowDataMap : dbRowDataMaps) {
            // 需求單 sid
            String requireSid = rowDataMap.get("require_sid") + "";
            // 部門 sid
            Integer depSid = (Integer) rowDataMap.get("dep_sid");

            // 取得對應需求容器
            Set<Integer> assignDepSids = assignDepSidsMapByRequireSid.get(requireSid);
            // 不存在時建立
            if (assignDepSids == null) {
                assignDepSids = Sets.newHashSet();
                assignDepSidsMapByRequireSid.put(requireSid, assignDepSids);
            }
            // 加入部門sid
            assignDepSids.add(depSid);
        }

        return assignDepSidsMapByRequireSid;

    }
}
