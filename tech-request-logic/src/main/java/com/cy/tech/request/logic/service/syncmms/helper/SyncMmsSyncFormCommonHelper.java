/**
 * 
 */
package com.cy.tech.request.logic.service.syncmms.helper;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.RequireTraceService;
import com.cy.tech.request.logic.service.syncmms.to.SyncFormTo;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

/**
 * @author allen1214_wu
 */
@Service
public class SyncMmsSyncFormCommonHelper {

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    transient private RequireService requireService;
    @Autowired
    private transient RequireTraceService requireTraceService;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 延後主單的『期望完成日』
     * @param syncFormTo  syncFormTo
     * @param onpgNo on程式單號
     * @return
     */
    public boolean postponeRequireHopeDate(
            SyncFormTo syncFormTo,
            String onpgNo) {

        // 判斷維護日期是否 > 期望日期
        if (WkDateUtils.isAfter(syncFormTo.getMaintenanceDate(), syncFormTo.getRequire().getHopeDate(), false)) {

            Date oldHopeDate = syncFormTo.getRequire().getHopeDate();

            // 異動期望完成日
            syncFormTo.getRequire().setHopeDate(syncFormTo.getMaintenanceDate());
            this.requireService.save(syncFormTo.getRequire());

            // 寫期望完成日異動追蹤
            this.requireTraceService.createSyncMmsTraceForChangeHopeDate(syncFormTo, oldHopeDate, onpgNo);

            return true;
        }

        return false;
    }

    private String prepareNativeContent(String htmlContent, boolean withoutFirstLine) {
        if (WkStringUtils.isEmpty(htmlContent)) {
            return "";
        }

        // HTML 轉純文字
        htmlContent = WkJsoupUtils.getInstance().htmlToText(htmlContent);

        String breakLine = "#8255210#";
        htmlContent = htmlContent.replaceAll("\r\n", breakLine);
        htmlContent = htmlContent.replaceAll("\n", breakLine);

        // 去空行、去空白
        List<String> lines = Lists.newArrayList(htmlContent.split(breakLine))
                .stream()
                .map(line -> WkStringUtils.safeTrim(line))
                .filter(line -> WkStringUtils.notEmpty(line))
                .collect(Collectors.toList());

        if (WkStringUtils.isEmpty(lines)) {
            return "";
        }

        if (withoutFirstLine) {
            lines.remove(0);
        }

        return lines.stream().collect(Collectors.joining());

    }

    public boolean compareHtmlContent(String sourceHtmlContent, String targetHtmlContent) {
        return this.prepareNativeContent(sourceHtmlContent, false).equals(
                this.prepareNativeContent(targetHtmlContent, false));
    }

    public boolean compareHtmlContentWithoutFirstLine(String sourceHtmlContent, String targetHtmlContent) {
        // this.showProcessContent(sourceHtmlContent, targetHtmlContent);
        return this.prepareNativeContent(sourceHtmlContent, true).equals(
                this.prepareNativeContent(targetHtmlContent, true));
    }

    @SuppressWarnings("unused")
    private void showProcessContent(String sourceHtmlContent, String targetHtmlContent) {
        com.cy.work.common.utils.WkFileUtils.getInstance().writeFileByText("c:/temp", "compareResult",
                "sourceHtmlContent0.txt",
                sourceHtmlContent);
        com.cy.work.common.utils.WkFileUtils.getInstance().writeFileByText("c:/temp", "compareResult",
                "targetHtmlContent0.txt",
                sourceHtmlContent);

        com.cy.work.common.utils.WkFileUtils.getInstance().writeFileByText("c:/temp", "compareResult",
                "sourceHtmlContent1.txt",
                this.prepareNativeContent(sourceHtmlContent, false));
        com.cy.work.common.utils.WkFileUtils.getInstance().writeFileByText("c:/temp", "compareResult",
                "targetHtmlContent1.txt",
                this.prepareNativeContent(targetHtmlContent, false));

        com.cy.work.common.utils.WkFileUtils.getInstance().writeFileByText("c:/temp", "compareResult",
                "sourceHtmlContent2.txt",
                this.prepareNativeContent(sourceHtmlContent, true));
        com.cy.work.common.utils.WkFileUtils.getInstance().writeFileByText("c:/temp", "compareResult",
                "targetHtmlContent2.txt",
                this.prepareNativeContent(targetHtmlContent, true));
    }

}
