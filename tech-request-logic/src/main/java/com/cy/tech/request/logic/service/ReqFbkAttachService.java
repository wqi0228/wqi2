/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.repository.require.RequireFbkAttachmentRepo;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.require.RequireFbkAttachment;
import com.cy.tech.request.vo.require.RequireTrace;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkAttachUtils;
import com.cy.work.common.vo.AttachmentService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;
import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * 需求資訊回覆補充附加檔案
 *
 * @author shaun
 */
@Component("require_fbk_attach")
public class ReqFbkAttachService implements AttachmentService<RequireFbkAttachment, RequireTrace, String>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1191661799697611909L;
    @Value("${erp.attachment.root}")
    private String attachPath;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private UserService userService;
    @Autowired
    private RequireTraceService traceService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private RequireFbkAttachmentRepo attachDao;
    @Autowired
    private WkAttachUtils attachUtils;

    @Transactional(readOnly = true)
    @Override
    public RequireFbkAttachment findBySid(String sid) {
        RequireFbkAttachment a = attachDao.findOne(sid);
        return a;
    }

    @Transactional(readOnly = true)
    @Override
    public List<RequireFbkAttachment> findAttachsByLazy(RequireTrace trace) {
        try {
            trace.getAttachments().size();
        } catch (LazyInitializationException e) {
            //log.debug("findAttachsByLazy lazy init error :" + e.getMessage());
            trace.setAttachments(this.findAttachs(trace));
        }
        return trace.getAttachments();
    }

    @Transactional(readOnly = true)
    @Override
    public List<RequireFbkAttachment> findAttachs(RequireTrace trace) {
        if (Strings.isNullOrEmpty(trace.getSid())) {
            if (trace.getAttachments() == null) {
                trace.setAttachments(Lists.newArrayList());
            }
            return trace.getAttachments();
        }
        return attachDao.findByTraceAndStatus(trace, Activation.ACTIVE);
    }

    @Override
    public String getAttachPath() {
        return attachPath;
    }

    @Override
    public String getDirectoryName() {
        return "tech-request";
    }

    @Override
    public RequireFbkAttachment createEmptyAttachment(String fileName, User executor, Org dep) {
        return (RequireFbkAttachment) attachUtils.createEmptyAttachment(new RequireFbkAttachment(), fileName, executor, dep);
    }

    /**
     * 存檔
     *
     * @param attach
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public RequireFbkAttachment updateAttach(RequireFbkAttachment attach) {
        RequireFbkAttachment a = attachDao.save(attach);
        return a;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveAttach(RequireFbkAttachment attach) {
        attachDao.save(attach);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveAttach(List<RequireFbkAttachment> attachs) {
        attachDao.save(attachs);
    }

    /**
     * 讀取附件上傳者及部門
     *
     * @param attachment
     * @return
     */
    @Override
    public String depAndUserName(RequireFbkAttachment attachment) {
        if (attachment == null) {
            return "";
        }
        User usr = WkUserCache.getInstance().findBySid(attachment.getCreatedUser().getSid());
           Org dep = orgService.findBySid(usr.getPrimaryOrg().getSid());
        String parentDepStr = orgService.showParentDep(dep);
        return parentDepStr + "-" + usr.getName();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void linkRelation(List<RequireFbkAttachment> attachments, RequireTrace trace, User login) {
        attachments.forEach(each -> this.setRelation(each, trace));
        this.saveAttach(attachments);
        trace.setAttachments(this.findAttachs(trace));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void linkRelation(RequireFbkAttachment ra, RequireTrace trace, User login) {
        this.setRelation(ra, trace);
        this.updateAttach(ra);
    }

    private void setRelation(RequireFbkAttachment ra, RequireTrace trace) {
        ra.setTrace(trace);
        ra.setRequire(trace.getRequire());
        ra.setRequireNo(trace.getRequireNo());
        if (!ra.getKeyChecked()) {
            ra.setStatus(Activation.INACTIVE);
        }
        ra.setKeyChecked(Boolean.FALSE);
    }

    /**
     * 刪除
     *
     * @param attachments
     * @param attachment
     * @param require
     * @param login
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void changeStatusToInActive(List<RequireFbkAttachment> attachments, RequireFbkAttachment attachment, RequireTrace trace, User login) {
        RequireFbkAttachment ra = (RequireFbkAttachment) attachment;
        ra.setStatus(Activation.INACTIVE);
        this.updateAttach(attachment);
        trace.setAttachments(this.findAttachs(trace));
        this.createDeleteTrace(attachment, trace, login);
    }

    private void createDeleteTrace(RequireFbkAttachment attachment, RequireTrace trace, User login) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String content = ""
                  + "檔案名稱：【" + attachment.getFileName() + "】\n"
                  + "上傳成員：【" + userService.getUserName(attachment.getCreatedUser()) + "】\n"
                  + "\n"
                  + "刪除來源：【追蹤 － " + commonService.get(trace.getRequireTraceType()) + " － " + sdf.format(trace.getCreatedDate()) + "】\n"
                  + "刪除成員：【" + login.getName() + "】\n"
                  + "刪除註記：" + attachment.getSid();
        traceService.createRequireTrace(trace.getRequire().getSid(), login, RequireTraceType.DELETE_ATTACHMENT, content);
    }

}
