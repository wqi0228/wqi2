/**
 * 
 */
package com.cy.tech.request.logic.service.setting.onetimetrns;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.vo.Org;
import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.logic.service.AssignNoticeService;
import com.cy.tech.request.logic.service.setting.onetimetrns.to.RequireDepRebuildTo;
import com.cy.tech.request.logic.service.setting.onetimetrns.to.RequireReAssignTo;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkConstants;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.converter.SplitListIntConverter;
import com.cy.work.common.vo.converter.SplitListStrConverter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Service
@Slf4j
public class OneTimeTrnsForRequireDepRestToDefaultService implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 9081062890321908827L;

    // ========================================================================
    //
    // ========================================================================
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private OneTimeTrnsForRequireDepCommonService requireDepCommonService;

    @Autowired
    private AssignNoticeService assignNoticeService;

    // ========================================================================
    //
    // ========================================================================
    /**
     * 查詢未處理
     * 
     * @param targetRequireNo
     * @return
     */
    public List<RequireReAssignTo> findWaitTransRequireSids(
            String targetRequireNo) {

        // ====================================
        // 查詢待轉檔
        // ====================================
        StringBuffer varname1 = new StringBuffer();
        varname1.append("SELECT tr.require_sid, ");
        varname1.append("       tr.require_no, ");
        varname1.append("       mapping.small_category_sid, ");
        varname1.append("       (SELECT Group_concat (DISTINCT checkitem.check_item_type SEPARATOR ',') ");
        varname1.append("        FROM   tr_require_check_item checkitem ");
        varname1.append("        WHERE  checkitem.require_sid = tr.require_sid ");
        varname1.append("               AND checkitem.status = 0) AS checkitemnames ");
        varname1.append("FROM   tr_require tr ");
        varname1.append("       INNER JOIN tr_category_key_mapping mapping ");
        varname1.append("               ON mapping.key_sid = tr.mapping_sid ");
        varname1.append("WHERE  tr.require_status = 'PROCESS' ");

        if (WkStringUtils.notEmpty(targetRequireNo)) {
            varname1.append("       AND tr.require_no = '" + targetRequireNo + "'");
        }

        varname1.append("ORDER  BY tr.create_dt DESC ");
        // varname1.append("LIMIT 100;");

        log.error(""
                + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(varname1.toString()) + "\r\n】"
                + "\r\n");

        List<Map<String, Object>> dataMaps = this.jdbcTemplate.queryForList(varname1.toString());
        if (WkStringUtils.isEmpty(dataMaps)) {
            return Lists.newArrayList();
        }

        log.debug("未轉主檔查詢:[{}]筆", dataMaps.size());

        Set<String> requireSids = dataMaps.stream()
                .map(dataMap -> dataMap.get("require_sid") + "")
                .collect(Collectors.toSet());

        // ====================================
        // 查詢預設資訊
        // ====================================
        Map<String, Set<Integer>> defaultAsdepMapBySmallCategorySid = this.findDefaultAsdep();

        // ====================================
        // 查詢分派、通知單位
        // ====================================
        Map<String, Set<Integer>> assignDepSidMapByRequireSid = this.requireDepCommonService.findAssignNoticeDepSids(
                requireSids, AssignSendType.ASSIGN);

        Map<String, Set<Integer>> noticeDepSidMapByRequireSid = this.requireDepCommonService.findAssignNoticeDepSids(
                requireSids, AssignSendType.SEND);

        // ====================================
        // 查詢確認單位檔
        // ====================================
        Map<String, List<RequireDepRebuildTo>> requireDepRebuildTosMapByRequireSid = this.requireDepCommonService.findRequireConfirmDeps(
                requireSids);

        // ====================================
        // 取得舊技術事業群單位
        // ====================================
        Org oldTech = WkOrgCache.getInstance().findById("TEBG");
        Set<Integer> oldTechDepSids = WkOrgCache.getInstance().findAllChildSids(oldTech.getSid());

        // ====================================
        // 組裝資料
        // ====================================
        List<RequireReAssignTo> requireReAssignTos = Lists.newArrayList();

        SplitListStrConverter splitListStrConverter = new SplitListStrConverter();

        for (Map<String, Object> dataMap : dataMaps) {
            RequireReAssignTo requireReAssignTo = new RequireReAssignTo();
            requireReAssignTos.add(requireReAssignTo);

            String requireSid = dataMap.get("require_sid") + "";
            String requireNo = dataMap.get("require_no") + "";
            String smallCategorySid = dataMap.get("small_category_sid") + "";

            List<String> checkItemTypes = splitListStrConverter.convertToEntityAttribute(dataMap.get("checkitemnames") + "");
            if (WkStringUtils.isEmpty(checkItemTypes)) {
                log.warn("\r\n [{}]沒有檢查項目！", dataMap.get("require_no") + "");
                continue;
            }

            requireReAssignTo.setRequireSid(requireSid);
            requireReAssignTo.setRequireNo(requireNo);

            // 分派單位
            Set<Integer> beforAssignDepSids = assignDepSidMapByRequireSid.get(requireSid);
            requireReAssignTo.setBeforAssignDepSids(beforAssignDepSids);

            Set<Integer> afterAssignDepSids = this.prepareAfterDepSids(
                    targetRequireNo,
                    smallCategorySid,
                    checkItemTypes,
                    beforAssignDepSids,
                    requireDepRebuildTosMapByRequireSid.get(requireSid),
                    defaultAsdepMapBySmallCategorySid,
                    AssignSendType.ASSIGN,
                    oldTechDepSids);
            requireReAssignTo.setAfterAssignDepSids(afterAssignDepSids);

            // 通知單位
            Set<Integer> beforNoticeDepSids = noticeDepSidMapByRequireSid.get(requireSid);
            requireReAssignTo.setBeforNoticeDepSids(beforNoticeDepSids);

            Set<Integer> afterNoticeDepSids = this.prepareAfterDepSids(
                    targetRequireNo,
                    smallCategorySid,
                    checkItemTypes,
                    beforNoticeDepSids,
                    requireDepRebuildTosMapByRequireSid.get(requireSid),
                    defaultAsdepMapBySmallCategorySid,
                    AssignSendType.SEND,
                    oldTechDepSids);

            // 若分派/通知單位同時存在同一個單位, 則移除通知
            afterNoticeDepSids = afterNoticeDepSids.stream()
                    .filter(afterNoticeDepSid -> !afterAssignDepSids.contains(afterNoticeDepSid))
                    .collect(Collectors.toSet());

            requireReAssignTo.setAfterNoticeDepSids(afterNoticeDepSids);
        }

        return requireReAssignTos;
    }

    private Set<Integer> prepareAfterDepSids(
            String requireNo,
            String smallCategorySid,
            List<String> checkItemTypes,
            Set<Integer> beforeDepSids,
            List<RequireDepRebuildTo> requireDepConfirmInfos,
            Map<String, Set<Integer>> defaultAsdepMapBySmallCategorySid,
            AssignSendType assignSendType,
            Set<Integer> oldTechDepSids) {

        Set<Integer> afterDepSids = Sets.newHashSet();

        if (WkStringUtils.isEmpty(beforeDepSids)) {
            beforeDepSids = Sets.newHashSet();
        }

        // ====================================
        // 收集預設單位
        // ====================================
        Set<Integer> defaultDepSids = Sets.newHashSet();
        for (String checkItemType : checkItemTypes) {
            String defaultAsDepkey = this.prepareDefaultAsDepkey(
                    smallCategorySid,
                    checkItemType,
                    assignSendType + "");
            Set<Integer> currDefaultAsDepSids = defaultAsdepMapBySmallCategorySid.get(defaultAsDepkey);
            if (WkStringUtils.notEmpty(currDefaultAsDepSids)) {
                defaultDepSids.addAll(currDefaultAsDepSids);
            }
        }

        // 如果沒有任何一筆預設單位，則此單不做異動
        if (WkStringUtils.isEmpty(defaultDepSids)) {
            log.info("沒有預設單位，不做異動!requireNo:[{}],checkItemTypes:[{}]", requireNo, checkItemTypes.stream().collect(Collectors.joining(",")));
            return beforeDepSids;
        }

        // ====================================
        // 保留非技術事業群單位
        // ===================================
        // 濾掉已設定單位中，的舊技術單位
        Set<Integer> beforeDepSidsWithoutTechDep = beforeDepSids.stream()
                .filter(beforeDepSid -> !oldTechDepSids.contains(beforeDepSid))
                .collect(Collectors.toSet());

        if (WkStringUtils.notEmpty(beforeDepSidsWithoutTechDep)) {
            afterDepSids.addAll(beforeDepSidsWithoutTechDep);
        }

        // ====================================
        // 加入預設分派單位
        // ===================================
        afterDepSids.addAll(defaultDepSids);

        // ====================================
        // 保留已確認單位
        // ====================================
        if (AssignSendType.ASSIGN.equals(assignSendType)) {
            for (RequireDepRebuildTo to : requireDepConfirmInfos) {
                if (to.getProgStatus() != null && to.getProgStatus().isInFinish()) {
                    // 執行完成，無條件保留此單位
                    afterDepSids.add(to.getDepSid());
                }
            }
        }

        return afterDepSids;
    }

    private Map<String, Set<Integer>> findDefaultAsdep() {

        StringBuffer varname1 = new StringBuffer();
        varname1.append("SELECT tsda.basic_data_small_category_sid, ");
        varname1.append("       tsda.check_item_type, ");
        varname1.append("       tsda.assign_send_type, ");
        varname1.append("       tsda.deps_info ");
        varname1.append("FROM   tr_setting_default_asdep tsda ");
        varname1.append("WHERE  status = 0 ");

        List<Map<String, Object>> dataMaps = this.jdbcTemplate.queryForList(varname1.toString());
        if (WkStringUtils.isEmpty(dataMaps)) {
            return Maps.newHashMap();
        }

        SplitListIntConverter splitListIntConverter = new SplitListIntConverter();

        Map<String, Set<Integer>> result = Maps.newHashMap();

        for (Map<String, Object> map : dataMaps) {
            List<Integer> depInfos = splitListIntConverter.convertToEntityAttribute(map.get("deps_info") + "");
            if (WkStringUtils.isEmpty(depInfos)) {
                continue;
            }

            String defaultAsDepkey = this.prepareDefaultAsDepkey(
                    (map.get("basic_data_small_category_sid") + ""),
                    (map.get("check_item_type") + ""),
                    (map.get("assign_send_type") + ""));

            result.put(defaultAsDepkey, Sets.newHashSet(depInfos));
        }

        return result;

    }

    public String prepareDefaultAsDepkey(
            String categorySid,
            String checkItemType,
            String assignSendType) {
        return categorySid + ":" + checkItemType + ":" + assignSendType;
    }

    @Transactional(rollbackFor = Exception.class)
    public void process(
            RequireReAssignTo requireReAssignTo) throws UserMessageException {

        List<Integer> beforeAssignDepSids = Lists.newArrayList();
        if (WkStringUtils.notEmpty(requireReAssignTo.getBeforAssignDepSids())) {
            beforeAssignDepSids.addAll(requireReAssignTo.getBeforAssignDepSids());
        }
        List<Integer> beforeNoticeDepSids = Lists.newArrayList();
        if (WkStringUtils.notEmpty(requireReAssignTo.getBeforNoticeDepSids())) {
            beforeNoticeDepSids.addAll(requireReAssignTo.getBeforNoticeDepSids());
        }
        List<Integer> afterAssignDepSids = Lists.newArrayList();
        if (WkStringUtils.notEmpty(requireReAssignTo.getAfterAssignDepSids())) {
            afterAssignDepSids.addAll(requireReAssignTo.getAfterAssignDepSids());
        }
        List<Integer> afterNoticeDepSids = Lists.newArrayList();
        if (WkStringUtils.notEmpty(requireReAssignTo.getAfterNoticeDepSids())) {
            afterNoticeDepSids.addAll(requireReAssignTo.getAfterNoticeDepSids());
        }

        this.assignNoticeService.processForRebuild(
                requireReAssignTo.getRequireSid(),
                requireReAssignTo.getRequireNo(),
                Lists.newArrayList(beforeAssignDepSids),
                Lists.newArrayList(beforeNoticeDepSids),
                Lists.newArrayList(afterAssignDepSids),
                Lists.newArrayList(afterNoticeDepSids),
                WkUserCache.getInstance().findBySid(WkConstants.ADMIN_USER_SID));

    }
}
