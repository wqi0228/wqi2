/**
 * 
 */
package com.cy.tech.request.logic.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 *
 */
@AllArgsConstructor
@Getter
@Setter
public class ProceeAddAssignDepForInternalTo  implements Serializable{
    
	
    /**
     * 
     */
    private static final long serialVersionUID = -2350866078065776148L;
    /**
     * 建構子
     */
    public ProceeAddAssignDepForInternalTo() {
    }
    
    /**
     * 需求單sid
     */
    String requireSid;
    /**
     * 需求單 no
     */
    String requireNo;
    /**
     * 立案單位
     */
    Integer depSid;

    /**
     * 需求單sid
     */
    String infoSid;
    /**
     * 立案單位
     */
    String info;
}
