package com.cy.tech.request.logic.search.view;

import java.io.Serializable;
import java.util.Date;

import com.cy.work.common.enums.UrgencyType;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 需求單查詢頁面顯示vo (search24.xhtml)
 *
 * @author kasim
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Search24View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7196541445789351062L;
    /** 廳主 */
    private Long customer;
    /** 客戶 */
    private Long author;
    /** 需求日期 - 資料來自於[tr_require].[create_dt] */
    private Date createdDate;
    /** 需求單位 - 資料來自於[tr_require].[dep_sid] */
    private Integer createDep;
    /** 需求類別 */
    private String bigName;
    /** 中類 */
    private String middleName;
    /** 小類 */
    private String smallName;
    /** 需求人員 - 資料來自於[tr_require].[create_usr] */
    private Integer createdUser;
    /** 統整人員 - 資料來自於[tr_require].[inte_staff] */
    private Integer inteStaffUser;
    /** 期望完成日 - 資料來自於[tr_require].[ hope_dt] */
    private Date hopeDate;
    /** 異動日期 - 資料來自於[tr_require].[ update_dt] */
    private Date updatedDate;
    /** 案件單 - 立案日期 */
    private Date issueCreatedDate;
    /** 案件單 - 主題 */
    private String issueTheme;
    /** 案件單 - 單號 */
    private String issueNo;
    /** 案件單 - 派工進度 */
    private String issueDispatchStatus;
    /** 案件單 - 進度 */
    private String issueStatus;
    /** 案件單 - 預計完成日 */
    private Date issueExpectedDate;
    /** 案件單 - 負責人員 */
    private Integer issueResponsible;

    /** 緊急度 */
    private UrgencyType urgency;
    /** 本地端連結網址 */
    private String localUrlLink;
}
