package com.cy.tech.request.logic.service.helper;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.anew.manager.TrIssueMappTransManager;
import com.cy.tech.request.logic.service.AssignNoticeService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.RequireTraceService;
import com.cy.tech.request.logic.service.TemplateService;
import com.cy.tech.request.logic.service.onpg.OnpgService;
import com.cy.tech.request.logic.service.syncmms.to.SyncFormTo;
import com.cy.tech.request.vo.enums.CateConfirmType;
import com.cy.tech.request.vo.enums.ReqCateType;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireAttachment;
import com.cy.tech.request.vo.template.component.ComBase;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Service
@Slf4j
public class RequireFlowHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2079118766151187384L;
    // ========================================================================
    // 服務元件區
    // ========================================================================
    @Autowired
    private transient AssignNoticeService assignNoticeService;
    @Autowired
    private transient TrIssueMappTransManager issueMappTransManager;
    @Autowired
    private transient OnpgService opService;
    @Autowired
    private transient RequireService requireService;
    @Autowired
    private transient TemplateService tempService;
    @Autowired
    private transient RequireTraceService traceService;
    @Autowired
    private transient RequireCreateUnitSignFlowHelper requireCreateUnitSignFlowHelper;

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * 執行提交流程 (內部方法, 不可直接呼叫)
     * 
     * @param requireSid            需求單 SID
     * @param requireNo             需求單號
     * @param issueCreateMappKeySid 為案件單轉 ONPG 時 wk_mapp_create_trans 的 Key
     * @param syncFormTo            取回的MMS表單資料
     * @param execUser              執行者
     * @param execDate              執行日
     * @throws UserMessageException     使用者訊息 exception
     * @throws SystemOperationException 系統執行錯誤時拋出
     */
    @Transactional(rollbackFor = Exception.class)
    private void doProcess(
            String requireSid,
            String requireNo,
            String issueCreateMappKeySid,
            SyncFormTo syncFormTo,
            User execUser,
            Date execDate) throws UserMessageException, SystemOperationException {

        // ====================================
        // 查詢主檔資料
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);
        if (require == null) {
            throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }

        // ====================================
        // 狀態阻擋
        // ====================================
        List<RequireStatusType> status = Lists.newArrayList(
                RequireStatusType.DRAFT,
                RequireStatusType.NEW_INSTANCE,
                RequireStatusType.ROLL_BACK_NOTIFY);

        if (require.getRequireStatus() != null &&
                !status.contains(require.getRequireStatus())) {
            String errorMessage = WkMessage.NEED_RELOAD + "製作進度狀態不應為：[" + require.getRequireStatus() + "]";
            log.warn(errorMessage);
            throw new UserMessageException(errorMessage, InfomationLevel.WARN);
        }

        // ====================================
        // 建立層簽流程
        // 進入點
        // 1.一般提交
        // 2.草稿提交
        // ====================================
        this.requireCreateUnitSignFlowHelper.process(requireSid, execDate);

        // ====================================
        // 判斷層簽是否結束 （最後一個人已經簽完）
        // 進入點
        // 1.簽名
        // ====================================
        // 檢核是否為『等待』單位簽核完成的狀態
        // 未結束時不往下執行
        if (this.doProcess_IsWaitReqUnitSign(require)) {
            return;
        }

        // ====================================
        // 『需求成立』追蹤
        // ====================================
        // insert『需求成立』追蹤
        this.traceService.createRequireEstablishTrace(
                requireSid,
                execUser,
                execDate);

        log.info("[{}] 需求成立!", require.getRequireNo());

        // ====================================
        // 為外部需求
        // ====================================
        if (ReqCateType.EXTERNAL.equals(require.getMapping().getBig().getReqCateType())) {
            log.info("[{}] 為外部需求，待GM檢查確認!", require.getRequireNo());

            // 更新主檔資料 update tr_require
            require = this.requireService.updateForFlowApporve(
                    requireSid,
                    true, // hasTrace
                    RequireStatusType.WAIT_CHECK, // requireStatus
                    ReqToBeReadType.GM_REVIEW, // readReason
                    execDate, // requireEstablishDate
                    CateConfirmType.N, // categoryConfirmCode
                    execUser.getSid());

        }

        // ====================================
        // 為內部需求
        // ====================================
        else if (ReqCateType.INTERNAL.equals(require.getMapping().getBig().getReqCateType())) {
            log.info("[{}] 為內部需求!", require.getRequireNo());

            // 更新主檔資料 update tr_require
            require = this.requireService.updateForFlowApporve(
                    requireSid,
                    true, // hasTrace
                    RequireStatusType.PROCESS, // requireStatus
                    require.getReadReason(), // readReason : 不異動
                    execDate, // requireEstablishDate
                    CateConfirmType.n, // categoryConfirmCode (內部需求)
                    execUser.getSid());

            // 自動分派給登入者單位
            require = this.assignNoticeService.processForInternalAdd(
                    requireSid,
                    requireNo,
                    execUser,
                    execDate);

            log.info("[{}] 自動分派給登入者單位!", requireNo);
        }

        // ====================================
        // 自動新增ON程式單
        // ====================================
        WorkOnpg workOnpg = null;
        if (require.getMapping().getSmall().getAutoCreateOnpg()) {

            if (syncFormTo != null) {
                // for MMS 維護單介接
                syncFormTo.setRequire(require);
                workOnpg = this.opService.createForMmsSync(
                        syncFormTo,
                        execDate);
            } else {
                workOnpg = doProcess_AutoCreateOnpg(
                        require,
                        issueCreateMappKeySid,
                        require.getCreatedUser(), // 自動on程式時,單申請人應為單據申請人 (原邏輯當有簽核時，會變成最後簽核者)
                        execDate);
            }

        }

        // ====================================
        // 為案件單轉需求單時的處理
        // ====================================
        if (WkStringUtils.notEmpty(issueCreateMappKeySid)) {
            //
            this.issueMappTransManager.processIssueRequireMapping(
                    issueCreateMappKeySid,
                    requireSid,
                    requireNo,
                    workOnpg,
                    execUser,
                    execDate);
        }
    }

    /**
     * 自動建立onpg單
     * 
     * @param require               需求單主檔
     * @param issueCreateMappKeySid 為案件單轉入時（onpg, 關連）， wk_mapp_create_trans 的 Key
     * @param execUser              執行者
     * @param execDate              執行日
     * @throws UserMessageException     使用者訊息 exception
     * @throws SystemOperationException 系統執行錯誤時拋出
     */
    private WorkOnpg doProcess_AutoCreateOnpg(
            Require require,
            String issueCreateMappKeySid,
            User execUser,
            Date execDate) throws UserMessageException, SystemOperationException {

        // ====================================
        // 取得主檔欄位資料
        // ====================================
        Map<String, ComBase> valueMap = tempService.loadTemplateFieldValue(require);

        // ====================================
        // 自動建立ON程式單
        // ====================================
        WorkOnpg workOnpg = this.opService.createInternalOnpg(
                require.getSid(),
                require.getRequireNo(),
                this.tempService.findTheme(require, valueMap),
                this.tempService.findEditorByFieldName(require, valueMap, TemplateService.FIELD_NAME_CONTEXT),
                this.tempService.findEditorByFieldName(require, valueMap, TemplateService.FIELD_NAME_NOTE),
                execUser,
                execDate);

        log.info("[{}] 自動建立ON程式單-[{}] !", require.getRequireNo(), workOnpg.getOnpgNo());

        return workOnpg;
    }

    /**
     * 檢核是否為『等待』單位簽核完成的狀態
     * 
     * @param require
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    private boolean doProcess_IsWaitReqUnitSign(Require require) {

        require = this.requireService.findByReqSid(require.getSid());

        // 簽核中的狀態一定是『新建檔』、『退件通知』
        List<RequireStatusType> status = Lists.newArrayList(
                RequireStatusType.NEW_INSTANCE,
                RequireStatusType.ROLL_BACK_NOTIFY);

        if (!status.contains(require.getRequireStatus())) {
            return false;
        }

        // 判斷無需執行層簽
        if (require.getReqUnitSign() == null) {
            return false;
        }

        // 簽完狀態不為核准，則為簽核中狀態
        return !InstanceStatus.APPROVED.equals(require.getReqUnitSign().getInstanceStatus());
    }

    // ========================================================================
    // 外部呼叫方法實做區
    // ========================================================================
    @Transactional(rollbackFor = Exception.class)
    public void processForCreateSave(
            Require editRequire,
            List<RequireCheckItemType> selectedCheckItemTypes,
            Map<String, ComBase> valueMap,
            List<RequireAttachment> attachments,
            String issueCreateMappKeySid,
            SyncFormTo syncFormTo,
            User execUser,
            Date execDate) throws UserMessageException, SystemOperationException {
        this.processForCreateSaveInNoTransactional(editRequire, selectedCheckItemTypes, valueMap, attachments, issueCreateMappKeySid, syncFormTo, execUser, execDate);
    }

    /**
     * 執行流程提交 : 新增-儲存
     * 
     * @param editRequire            編輯資料檔
     * @param selectedCheckItemTypes 被選擇的檢查項目（對應畫面系統別）
     * @param valueMap               單據欄位資料
     * @param attachments            附加檔案
     * @param issueCreateMappKeySid  為案件單轉需求單時的 mapping 檔 key
     * @param syncFormTo             取回的MMS 表單資料
     * @param execUser               執行者
     * @param execDate               執行日
     * @throws UserMessageException
     * @throws SystemOperationException
     */
    public void processForCreateSaveInNoTransactional(
            Require editRequire,
            List<RequireCheckItemType> selectedCheckItemTypes,
            Map<String, ComBase> valueMap,
            List<RequireAttachment> attachments,
            String issueCreateMappKeySid,
            SyncFormTo syncFormTo,
            User execUser,
            Date execDate) throws UserMessageException, SystemOperationException {

        // ====================================
        // 防呆
        // ====================================
        if (editRequire == null) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.ERROR, "傳入editRequire為空!", 30);
            throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }

        if (editRequire.getSid() != null) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.ERROR, "傳入editRequire 非新增 sid != null ", 30);
            throw new SystemDevelopException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }

        // ====================================
        // 主檔
        // ====================================
        // 新建前初始化欄位
        this.requireService.initRequire(
                editRequire,
                execUser.getSid(),
                execDate,
                syncFormTo);

        // 將附加檔案放到到 主檔（require）物件
        this.requireService.firstSaveAttachHandler(
                editRequire,
                attachments);

        // 儲存主檔資料, 且異動相關資料 (儲存後會產生 SID)
        this.requireService.saveContentAndRelationData(
                editRequire,
                selectedCheckItemTypes,
                valueMap,
                execUser,
                execDate,
                true);

        // ====================================
        // 執行提交流程
        // ====================================
        this.doProcess(
                editRequire.getSid(),
                editRequire.getRequireNo(),
                issueCreateMappKeySid,
                syncFormTo,
                execUser,
                execDate);
    }

    /**
     * 執行流程提交 : for 簽名後觸發後續流程使用
     * 
     * @throws UserMessageException     使用者訊息 exception
     * @throws SystemOperationException 系統執行錯誤時拋出
     */
    @Transactional(rollbackFor = Exception.class)
    public void processForDoSign(
            String requireSid,
            String requireNo,
            User execUser,
            Date execDate) throws UserMessageException, SystemOperationException {

        this.doProcess(requireSid, requireNo, null, null, execUser, execDate);

    }

    /**
     * 執行草稿提交
     * 
     * @param requireSid             需求項目
     * @param selectedCheckItemTypes 檢查項目
     * @param valueMap               單據欄位資料
     * @param execUser               執行者
     * @param execDate               執行日期
     * @return
     * @throws UserMessageException
     * @throws SystemOperationException
     */
    @Transactional(rollbackFor = Exception.class)
    public Require processForSubmitDraft(
            String requireSid,
            List<RequireCheckItemType> selectedCheckItemTypes,
            Map<String, ComBase> valueMap,
            User execUser,
            Date execDate) throws UserMessageException, SystemOperationException {

        // ====================================
        // 取得主檔內容
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);
        if (require == null) {
            throw new SystemDevelopException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }

        // ====================================
        // 異動主檔內容
        // ====================================
        // 初始化送出的欄位值
        // 初始化需求單欄位 (更改為為正式單據值)
        this.requireService.initRequire(
                require,
                execUser.getSid(),
                execDate,
                null // 非 MMS 介接, 不用傳
        );
        // 儲存主檔資料, 且異動相關資料
        this.requireService.saveContentAndRelationData(
                require,
                selectedCheckItemTypes,
                valueMap,
                execUser,
                execDate,
                true);

        // ====================================
        // 草稿提交
        // ====================================
        this.doProcess(
                requireSid,
                require.getRequireNo(),
                null,
                null,
                execUser,
                execDate);

        return this.requireService.findByReqSid(requireSid);
    }

}
