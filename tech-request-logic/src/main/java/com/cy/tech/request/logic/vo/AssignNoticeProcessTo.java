/**
 * 
 */
package com.cy.tech.request.logic.vo;

import java.io.Serializable;
import java.util.Set;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 執行分派/通知 資料容器
 * @author allen1214_wu
 */
@EqualsAndHashCode(of = {"requireSid"})
@NoArgsConstructor
public class AssignNoticeProcessTo implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -5896742550913685988L;

    /**
	 * 需求單 SID
	 */
	@Getter
	@Setter
	private String requireSid;
	
	/**
	 * 需求單號
	 */
	@Getter
	@Setter
	private String requireNo;
	
	
	/**
	 * 需求小類 SID
	 */
	@Getter
	@Setter
	private String basicDataSmallCategorySid;

	/**
	 * 分派單位 JSON String
	 */
	@Getter
	@Setter
	private String assignInfoJsonStr;

	/**
	 * 分派單位 檔 SID
	 */
	@Getter
	@Setter
	private String assignInfoSid;

	/**
	 * 分派單位檔
	 */
	// @Getter
	// @Setter
	// private AssignSendInfo assignInfo;

	/**
	 * 轉檔前的分派單位
	 */
	@Getter
	@Setter
	private Set<Integer> originalAssignDepSids;

	/**
	 * 轉檔後的分派單位
	 */
	@Getter
	@Setter
	private Set<Integer> newAssignDepSids;

	/**
	 * 通知單位 JSON String
	 */
	@Getter
	@Setter
	private String noticeInfoJsonStr;

	/**
	 * 通知單位 檔 SID
	 */
	@Getter
	@Setter
	private String noticeInfoSid;
	/**
	 * 通知單位檔
	 */
	// @Getter
	// @Setter
	// private AssignSendInfo noticeInfo;

	/**
	 * 轉檔前的分派單位
	 */
	@Getter
	@Setter
	private Set<Integer> originalNoticeDepSids;

	/**
	 * 轉檔後的分派單位
	 */
	@Getter
	@Setter
	private Set<Integer> newNoticeDepSids;

}
