/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import java.io.Serializable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * common 轉中文
 *
 * @author shaun
 */
@Slf4j
@Component
public class CommonService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2139061465148973800L;
    @Autowired
    transient private Environment env;

    public String get(Object type) {
        if (type == null) {
            return "";
        }
        if (!(type instanceof Enum)) {
            log.error("commonBean傳入非Enum型態物件..." + type.getClass().getSimpleName());
            return "";
        }
        return env.getProperty("common." + type.getClass().getSimpleName() + "." + ((Enum<?>) type).name());
    }
}
