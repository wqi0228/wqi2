package com.cy.tech.request.logic.service;

import java.io.Serializable;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.helper.MenuHelper;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepService;
import com.cy.tech.request.logic.service.setting.SettingCheckConfirmRightService;
import com.cy.tech.request.logic.utils.ToolsDate;
import com.cy.tech.request.logic.vo.SimpleDateFormatEnum;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.ForwardType;
import com.cy.tech.request.vo.enums.InboxType;
import com.cy.tech.request.vo.enums.OthSetStatus;
import com.cy.tech.request.vo.enums.ReqCateType;
import com.cy.tech.request.vo.enums.ReqConfirmDepProgStatus;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.backend.logic.WorkCalendarHelper;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkConstants;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.logic.lib.manager.WorkCommonReadRecordManager;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.logic.WkUserAndOrgLogic;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * 需求單數量服務
 *
 * @author shaun
 */
@Slf4j
@Component
public class ReqWorkCountService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1183192285639769772L;
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private transient WorkBackendParamHelper workBackendParamHelper;
    @Autowired
    private WorkCalendarHelper workCalendarHelper;
    @Autowired
    private MenuHelper menuHelper;
    @Autowired
    private transient RequireConfirmDepService requireConfirmDepService;
    @Autowired
    private transient WkUserCache wkUserCache;
    @Autowired
    private transient SettingCheckConfirmRightService settingCheckConfirmRightService;
    @Autowired
    private transient WkUserAndOrgLogic wkUserAndOrgLogic;
    @Autowired
    private transient WorkCommonReadRecordManager workCommonReadRecordManager;

    /**
     * 查詢通知/分派筆數
     * 
     * @param deptSid 部門 sid
     * @param roleStr 登入者角色
     * @param type    查詢類別 (分派/通知)
     */
    public Integer countAssignNotice(Integer userSid, AssignSendType type) {

        List<String> depSids = wkUserAndOrgLogic.prepareCanViewDepSidsBaseOnMinsterial(userSid, false).stream()
                .map(sid -> sid + "")
                .collect(Collectors.toList());

        // ====================================
        // 兜組查詢SQL
        // ====================================
        // 需閱讀 （不在閱讀記錄 + 閱讀狀態 = UN_READ ）
        String readRecordSQL = this.workCommonReadRecordManager.prepareExsitConditionSQL(
                FormType.REQUIRE.getReadRecordTableName(),
                FormType.REQUIRE.getReadRecordFormSidColumnName(),
                "tr.require_sid",
                userSid + "",
                ReadRecordType.UN_READ);

        StringBuffer builder = new StringBuffer();
        builder.append("SELECT Count(*) ");
        builder.append("FROM   (SELECT tr.require_sid ");
        builder.append("        FROM   tr_require tr ");
        builder.append("               LEFT JOIN tr_assign_send_search_info assi ");
        builder.append("                       ON tr.require_sid = assi.require_sid ");
        builder.append("        WHERE  tr.close_code = '0' ");
        builder.append("               AND tr.read_reason != 'NO_TO_BE_READ' ");
        builder.append("               AND assi.dep_sid IN ( " + String.join(",", depSids) + " )");
        builder.append("               AND assi.type = :type ");
        builder.append("               AND " + readRecordSQL + " ");
        builder.append("        GROUP  BY tr.require_sid) aa");

        // ====================================
        // 準備查詢參數
        // ====================================
        Map<String, Object> parameters = Maps.newHashMap();
        // 相關部門
        // parameters.put("depSids", Lists.newArrayList(
        // wkUserAndOrgLogic.prepareCanViewDepSidsBaseOnMinisterial(userSid)));
        // 查詢類別
        parameters.put("type", type.ordinal());

        // ====================================
        // 查詢
        // ====================================
        BigInteger count = (BigInteger) this.getSingleResult(builder.toString(), parameters);

        return count.intValue();
    }

    /**
     * 計算退件暫存單據的數量 search04.xhtml
     * 
     * @param deptSid 部門 sid
     * @param userSid 使用者 sid
     * @return 退件單據數量
     */
    public Integer countReject(Integer deptSid, Integer userSid) {

        // ====================================
        //
        // ====================================
        StringBuffer builder = new StringBuffer();
        Map<String, Object> parameters = Maps.newHashMap();

        builder.append("SELECT req.require_sid  ");
        builder.append("FROM   tr_require req ");
        builder.append("       LEFT JOIN tr_require_check_item ckitem ");
        builder.append("             ON ckitem.require_sid = req.require_sid ");

        // ====================================
        // 此功能限制查詢條件
        // ====================================
        builder.append("WHERE  1 = 1 ");
        builder.append("       AND req.back_code = TRUE "); // 退件通知碼(退件暫存單據查詢條件)
        builder.append("       AND req.require_status = '" + RequireStatusType.ROLL_BACK_NOTIFY + "' "); // 需求類別製作進度
        builder.append("       AND req.status = 0 "); // 單據狀態

        // 需閱讀 （WAIT_READ ）
        String readRecordSQL = this.workCommonReadRecordManager.prepareExsitConditionSQL(
                FormType.REQUIRE.getReadRecordTableName(),
                FormType.REQUIRE.getReadRecordFormSidColumnName(),
                "req.require_sid",
                userSid + "",
                ReadRecordType.WAIT_READ);
        builder.append("       AND " + readRecordSQL + " ");

        // ====================================
        // 權限判斷
        // ====================================
        // 不是檢查確認人員
        if (!settingCheckConfirmRightService.hasCanCheckUserRight(userSid)) {
            // 只能看自己單位的單
            builder.append("       AND req.dep_sid IN ( :depSids ) ");
            // 取得可閱單位 (上升到部級)
            Set<Integer> depSids = this.wkUserAndOrgLogic.prepareCanViewDepSidsBaseOnMinsterial(userSid, false);
            if (WkStringUtils.isEmpty(depSids)) {
                // 為空時強制查不到
                depSids = Sets.newHashSet(-99);
            }
            parameters.put("depSids", depSids);
        }

        // ====================================
        // 查詢
        // ====================================

        String sql = "SELECT count(*) FROM (" + builder.toString() + " GROUP BY req.require_sid ) aa ";

        BigInteger count = (BigInteger) this.getSingleResult(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql),
                parameters);
        return count.intValue();
    }

    /**
     * 計算ON程式的數量 search16.xhtml
     * 
     * @param userSid
     * @param isOpMgr
     * @return
     */
    public Integer countHasOn(Integer userSid, Boolean isOpMgr) {

        String readRecordSQL = this.workCommonReadRecordManager.prepareExsitConditionSQL(
                FormType.WORKONPG.getReadRecordTableName(),
                FormType.WORKONPG.getReadRecordFormSidColumnName(),
                "wo.onpg_sid",
                userSid + "",
                ReadRecordType.NEED_READ);

        StringBuilder builder = new StringBuilder();
        Map<String, Object> parameters = Maps.newHashMap();
        builder.append("  SELECT COUNT(DISTINCT wo.onpg_sid) FROM work_onpg wo ");
        builder.append("  INNER JOIN (SELECT tr.require_sid FROM tr_require tr ");
        builder.append("                 WHERE tr.require_status = 'PROCESS'");
        builder.append("             ) AS tr ON tr.require_sid = wo.onpg_source_sid ");

        builder.append("       WHERE wo.onpg_source_type='TECH_REQUEST' ");
        builder.append("         AND wo.onpg_status IN ('ALREADY_ON', 'CHECK_REPLY') ");
        builder.append("         AND " + readRecordSQL);

        // 填ON程式單的單位 或 收到ON程式單的單位
        if (!isOpMgr) {
            builder.append("     AND (wo.dep_sid IN (:depSids) OR wo.onpg_deps REGEXP :regexoDepSids) ");

            // 取得預設查詢單位
            Set<Integer> depSids = this.wkUserAndOrgLogic.prepareCanViewDepSidsBaseOnMinsterial(userSid, false);
            if (WkStringUtils.isEmpty(depSids)) {
                // 為空時強制查不到
                depSids = Sets.newHashSet(-99);
            }

            // 填單單位
            parameters.put("depSids", depSids);

            // 通知單位
            String notifyDepSid = depSids.stream().map(each -> "\"" + each + "\"").collect(Collectors.joining("|"));
            parameters.put("regexoDepSids", notifyDepSid);
        }

        BigInteger count = (BigInteger) this.getSingleResult(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()),
                parameters);
        return count.intValue();
    }

    /**
     * ON程式 需檢查確認數量
     *
     * @param userSid
     * @return
     */
    public Integer conuntOnpgCheck(Integer userSid) {

        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();

        try {
            User user = this.wkUserCache.findBySid(userSid);

            Set<Integer> depSids = Sets.newHashSet(user.getPrimaryOrg().getSid());

            Set<Integer> managerOrgSids = WkOrgCache.getInstance().findManagerOrgSids(userSid);

            // 管理單位 sid
            String managerOrgSidsStr = "";
            if (WkStringUtils.notEmpty(managerOrgSids)) {
                depSids.addAll(managerOrgSids);
                managerOrgSidsStr = managerOrgSids.stream().map(sid -> sid + "").collect(Collectors.joining(","));
            }

            Set<Integer> parentOrgSids = WkOrgCache.getInstance().findAllParentSid(user.getPrimaryOrg().getSid());
            if (WkStringUtils.notEmpty(parentOrgSids)) {
                depSids.addAll(parentOrgSids);
            }

            builder.append("SELECT COUNT(wop.onpg_sid)"
                    + " FROM ");
            builder.append(" work_onpg wop ");
            builder.append(" INNER JOIN  tr_require tr ON tr.require_no = wop.onpg_source_no ");
            // 需求類別製作進度(先決條件：[tr_require].[ require_status]= PROCESS)
            builder.append(" AND tr.require_status = '" + RequireStatusType.PROCESS.name() + "'");
            builder.append(" INNER JOIN  tr_index_dictionary tid ON "
                    + "tr.require_sid = tid.require_sid AND tid.field_name='主題' ");
            builder.append(" INNER JOIN tr_category_key_mapping ckm ON tr.mapping_sid=ckm.key_sid ");
            builder.append(" INNER JOIN tr_basic_data_big_category tbckm ON tbckm.basic_data_big_category_sid=ckm.big_category_sid ");
            StringBuilder deSb = new StringBuilder();
            depSids.forEach(item -> {
                if (!Strings.isNullOrEmpty(deSb.toString())) {
                    deSb.append(",");
                }
                deSb.append(item);
            });

            builder.append("WHERE wop.onpg_sid IS NOT NULL ");
            // 技術需求單類型
            builder.append(" AND wop.onpg_source_type='TECH_REQUEST'");
            // [work_onpg].[onpg_status] in (CHECK_REPLY , ALREADY_ON)
            builder.append(" AND wop.onpg_status in ('" + WorkOnpgStatus.CHECK_REPLY.name() + "','" + WorkOnpgStatus.ALREADY_ON.name() + "') ");

            String dateStr = ToolsDate.transDateToString(
                    SimpleDateFormatEnum.SdfDate.getValue(),
                    this.getSpecialWorkDay(new Date(), 3));

            dateStr = dateStr + " 23:59:59";
            builder.append(" AND wop.create_dt <= '" + dateStr + "' ");

            builder.append(" AND (");
            // 外部需求- 當LoginUser=[tr_require].[dep] 的同仁(含底下單位的同仁們)or
            // [tr_require].[dep]的主管(包含部級單位主管與處級單位主管時）
            builder.append(" ( tbckm.req_category = '" + ReqCateType.EXTERNAL + "' AND  tr.dep_sid in (" + deSb.toString() + ") )");
            // 內部需求單的需求單的create_usr或需求單create_usr的主管們時
            builder.append(" OR ( tbckm.req_category = '" + ReqCateType.INTERNAL + "' AND (tr.create_usr = '" + userSid + "'  ");

            if (!Strings.isNullOrEmpty(managerOrgSidsStr)) {
                builder.append(" OR  tr.dep_sid in (" + managerOrgSidsStr + ")");
            }
            builder.append("))");
            // 當LoginUser=企業服務處（含底下單位的同仁 或 含底下單位主管） 且 當ON程式的通知單位有企業服務處（含底下單位）
            // OR (當ON程式的通知單位 同時包含企業服務處（含底下單位）與
            // 會員服務處（含底下單位）)
            Org marketDep = this.workBackendParamHelper.getMarketDep();
            if (marketDep != null) {
                List<Org> marketDeps = WkOrgCache.getInstance().findAllChild(marketDep.getSid());
                marketDeps.add(marketDep);

                List<Org> flitermarketDeps = marketDeps.stream()
                        .filter(each -> each != null && each.getSid() != null && depSids.contains(each.getSid()))
                        .collect(Collectors.toList());
                if (flitermarketDeps != null && !flitermarketDeps.isEmpty()) {
                    StringBuilder deMarketSb = new StringBuilder();
                    marketDeps.forEach(item -> {
                        if (!Strings.isNullOrEmpty(deMarketSb.toString())) {
                            deMarketSb.append(" OR ");
                        }
                        deMarketSb.append(" wop.onpg_deps like '%\"" + item.getSid() + "\"%'");
                    });
                    builder.append(" OR ( tbckm.req_category = '" + ReqCateType.INTERNAL + "' AND  (" + deMarketSb.toString() + ") ) ");
                }
            }
            builder.append(" ) ");

            BigInteger count = (BigInteger) this.getSingleResult(
                    new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()),
                    parameters);
            return count.intValue();
        } catch (Exception e) {

            log.error("conuntOnpgCheck 查詢失敗!"
                    + "\r\n" + e.getMessage()
                    + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()) + "\r\n】"
                    + "\r\nPARAMs:【\r\n" + com.cy.work.common.utils.WkJsonUtils.getInstance().toPettyJson(parameters) + "\r\n】"
                    + "\r\n", e);

        }
        return 0;
    }

    /**
     * 計算待結案數量
     * 
     * @param userSid user sid
     * @return 待結案數量
     */
    public Integer conuntWaitClose(Integer userSid) {

        Map<String, Object> parameters = Maps.newHashMap();

        String managerDepConditionSql = wkUserAndOrgLogic.prepareCanViewDepSidsBaseOnMinsterial(userSid, false).stream()
                .map(sid -> sid + "")
                .collect(Collectors.joining(", ", " OR  tr.dep_sid IN (", ")"));

        StringBuffer varname1 = new StringBuffer();
        varname1.append("SELECT count(tr.require_sid) ");
        varname1.append("FROM   tr_require tr ");
        varname1.append("WHERE  tr.require_status = 'COMPLETED' ");
        varname1.append("       AND ( tr.create_usr = " + userSid + " ");
        varname1.append("       " + managerDepConditionSql + "); ");

        try {
            BigInteger count = (BigInteger) this.getSingleResult(varname1.toString(), parameters);
            return count.intValue();
        } catch (Exception e) {

            log.error("conuntWaitClose 查詢失敗!"
                    + "\r\n" + e.getMessage()
                    + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(varname1.toString()) + "\r\n】"
                    + "\r\nPARAMs:【\r\n" + com.cy.work.common.utils.WkJsonUtils.getInstance().toPettyJson(parameters) + "\r\n】"
                    + "\r\n",
                    e);

        }
        return 0;
    }

    /**
     * 將存放角色sid字串換List型態
     *
     * @param roleStr
     * @return
     */
    public List<Long> transRoleSids(String roleStr) {
        List<Long> roleList = Lists.newArrayList();
        if (roleStr.contains("、")) {
            String[] arrRoleStr = roleStr.split("、");
            for (String roleSid : arrRoleStr) {
                if (WkStringUtils.isNumber(roleSid)) {
                    roleList.add(Long.parseLong(roleSid));
                }
            }
        }
        return roleList;
    }

    /**
     * 查詢
     *
     * @param sql
     * @param parameters
     * @return
     */
    private Object getSingleResult(String sql, Map<String, Object> parameters) {
        Query query = em.createNativeQuery(new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql.toString()));
        if (parameters != null) {
            parameters.keySet().forEach(each -> {
                query.setParameter(each, parameters.get(each));
            });
        }

        try {
            return query.getSingleResult();
        } catch (Exception e) {
            log.error("查詢失敗!"
                    + "\r\n" + e.getMessage()
                    + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql.toString()) + "\r\n】"
                    + "\r\nPARAMs:【\r\n" + com.cy.work.common.utils.WkJsonUtils.getInstance().toPettyJson(parameters) + "\r\n】"
                    + "\r\n");

            throw e;
        }
    }

    /**
     * sssss
     *
     * @param userSid
     * @return
     */
    public Integer workPtCheck(Integer userSid, Integer userDepSid) {

        // ====================================
        // 主單建立者
        // ====================================
        // 加入自己
        Set<Integer> userSids = Sets.newHashSet(userSid);
        // 管理單位以下所有成員
        Set<Integer> managerDepSids = WkOrgCache.getInstance().findManagerWithChildOrgSids(userSid);
        if (WkStringUtils.notEmpty(managerDepSids)) {
            Set<Integer> depUserSids = WkUserCache.getInstance().findUserSidByOrgsWithManager(managerDepSids, Activation.ACTIVE);
            if (WkStringUtils.notEmpty(depUserSids)) {
                userSids.addAll(depUserSids);
            }
        }

        String userSidSql = userSids.stream()
                .map(each -> String.valueOf(each))
                .collect(Collectors.joining(","));

        // ====================================
        // 兜組 SQL
        // ====================================
        StringBuffer varname1 = new StringBuffer();
        varname1.append("SELECT IFNULL(COUNT(*), 0) ");
        varname1.append("FROM   (SELECT * ");
        varname1.append("        FROM   work_pt_check tpc ");
        varname1.append("        WHERE  tpc.pt_check_status IN ( '" + PtStatus.APPROVE.name() + "', '" + PtStatus.PROCESS.name() + "' )) AS tpc ");
        varname1.append("       INNER JOIN (SELECT * ");
        varname1.append("                   FROM   tr_require tr ");
        varname1.append("                   WHERE  1 = 1 ");
        varname1.append("                          AND tr.require_status = '" + RequireStatusType.PROCESS.name() + "' ");
        varname1.append("                          AND tr.has_prototype = 1 ");
        varname1.append("                          AND tr.create_usr IN ( " + userSidSql + " )) AS tr ");
        varname1.append("               ON tr.require_sid = tpc.pt_check_source_sid ");
        varname1.append("       LEFT JOIN (SELECT rpr.pt_check_sid, ");
        varname1.append("                         rpr.reply_udt, ");
        varname1.append("                         MAX(rpr.reply_udt) ");
        varname1.append("                  FROM   work_pt_reply rpr ");
        varname1.append("                  GROUP  BY rpr.pt_check_sid) AS rpr ");
        varname1.append("              ON rpr.pt_check_sid = tpc.pt_check_sid ");
        varname1.append("WHERE  tpc.pt_check_sid IS NOT NULL");

        Query query = em.createNativeQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(varname1.toString()));
        BigInteger count = (BigInteger) query.getSingleResult();
        return count.intValue();
    }

    public Integer trOsCheck(Integer orgSid) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String checkDate = sdf.format(this.getSpecialWorkDay(new Date(), 3));

        StringBuilder builder = new StringBuilder();
        builder.append("SELECT ifnull(count(*),0) FROM  ");
        builder.append("(   SELECT * FROM tr_os os WHERE 1=1");
        builder.append("        AND os_notice_deps like '").append("%\"").append(orgSid).append("\"%").append("' ").append("\n");
        builder.append("        AND os_status = '").append(OthSetStatus.PROCESSING.name()).append("' ").append("\n");
        builder.append("        AND os.create_dt <= '").append(checkDate).append(" 23:59:59' ").append("\n");
        builder.append(") AS os ");
        builder.append("INNER JOIN (SELECT * FROM tr_require tr WHERE tr.has_os=1");
        builder.append("        AND tr.require_status = '").append(RequireStatusType.PROCESS.name()).append("' ").append("\n");
        builder.append("        AND tr.dep_sid <> '").append(orgSid).append("' ").append("\n");
        builder.append(") AS tr ON tr.require_no = os.require_no ");
        builder.append("INNER JOIN tr_category_key_mapping ckm ON tr.mapping_sid=ckm.key_sid ");
        builder.append("WHERE os.os_sid IS NOT NULL ");

        Query query = em.createNativeQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()));
        BigInteger count = (BigInteger) query.getSingleResult();
        return count.intValue();
    }

    private Integer countRecord(String namedQuery, Map<String, Object> paramMap) {
        Query query = em.createNamedQuery(namedQuery);
        if (paramMap != null) {
            for (String key : paramMap.keySet()) {
                query.setParameter(key, paramMap.get(key));
            }
        }
        BigInteger count = (BigInteger) query.getSingleResult();
        return count.intValue();
    }

    /**
     * 轉寄個人
     * 
     * @param userSid   user sid
     * @param inboxType 轉寄類型
     * @return 數量
     */
    public Integer countUnreadInboxForForwardMember(Integer userSid, InboxType inboxType) {
        if (inboxType == null || !inboxType.isForwardMember()) {
            throw new SystemDevelopException("非轉寄個人不可使用此方法");
        }

        Map<String, Object> parameters = Maps.newHashMap();

        String readRecordSQL = this.workCommonReadRecordManager.prepareExsitConditionSQL(
                FormType.REQUIRE.getReadRecordTableName(),
                FormType.REQUIRE.getReadRecordFormSidColumnName(),
                "inbox.require_sid",
                "inbox.receive",
                ReadRecordType.NEED_READ);

        StringBuffer builder = new StringBuffer();
        builder.append("SELECT Count(*) FROM ( SELECT b.require_sid ");
        builder.append("FROM   (SELECT * ");
        builder.append("        FROM   tr_alert_inbox inbox ");
        builder.append("        WHERE  inbox.forward_type = '" + ForwardType.FORWARD_MEMBER.name() + "' ");
        builder.append("               AND inbox.status = 0 ");
        builder.append("               AND inbox.inbox_type = '" + inboxType.name() + "' ");
        builder.append("               AND inbox.receive = " + userSid + " ");
        builder.append("               AND " + readRecordSQL + " ");
        builder.append("       ) a ");
        builder.append("       INNER JOIN tr_require b ");
        builder.append("               ON a.require_sid = b.require_sid ");
        builder.append("       GROUP BY b.require_sid)  c ");

        Query query = em.createNativeQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()));

        menuHelper.setQueryParameter(query, parameters);
        BigInteger count = (BigInteger) query.getSingleResult();
        return count.intValue();
    }

    public Integer countUnreadInboxForForwardDep(Integer userSid, Set<Integer> receiveDepSids) {

        Map<String, Object> parameters = Maps.newHashMap();

        String readRecordSQL = this.workCommonReadRecordManager.prepareExsitConditionSQL(
                FormType.REQUIRE.getReadRecordTableName(),
                FormType.REQUIRE.getReadRecordFormSidColumnName(),
                "inbox.require_sid",
                userSid + "",
                ReadRecordType.NEED_READ);

        String depSidStr = "-987";
        if (WkStringUtils.notEmpty(receiveDepSids)) {
            depSidStr = receiveDepSids.stream()
                    .map(sid -> sid + "")
                    .collect(Collectors.joining(","));
        }

        StringBuffer builder = new StringBuffer();
        builder.append("SELECT Count(*) FROM ( SELECT b.require_sid ");
        builder.append("FROM   (SELECT * ");
        builder.append("        FROM   tr_alert_inbox inbox ");
        builder.append("        WHERE  inbox.forward_type = '" + ForwardType.FORWARD_DEPT + "' ");
        builder.append("               AND inbox.status = 0 ");
        builder.append("               AND inbox.receive_dep IN (" + depSidStr + ") ");
        builder.append("               AND " + readRecordSQL + " ");
        builder.append("       ) a ");
        builder.append("       INNER JOIN tr_require b ");
        builder.append("               ON a.require_sid = b.require_sid ");
        builder.append("       GROUP BY b.require_sid) c ");

        Query query = em.createNativeQuery(
                new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()));

        menuHelper.setQueryParameter(query, parameters);
        BigInteger count = (BigInteger) query.getSingleResult();
        return count.intValue();
    }

    /**
     * 逾期未結案
     * 
     * @param userSid
     * @return
     */
    public Integer countOverdueUnclosed(Integer userSid) {
        // 可閱單位
        Set<Integer> depSids = this.wkUserAndOrgLogic.prepareCanViewDepSidsBaseOnMinsterial(userSid, false);

        Query query = em.createNamedQuery("countOverdueUnclosed");
        query.setParameter("depSids", depSids);
        BigInteger count = (BigInteger) query.getSingleResult();
        return count.intValue();
    }

    public Integer countOverdueUnfinished(Integer userSid) {
        // 可閱單位
        Set<Integer> depSids = this.wkUserAndOrgLogic.prepareCanViewDepSidsBaseOnMinsterial(userSid, false);

        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("depSids", depSids);

        return countRecord("countOverdueUnfinished", paramMap);
    }

    /**
     * 送測-QA待審核單據
     */
    public Integer countStQaWaitApprove() {
        return countRecord("countStQaWaitApprove", null);
    }

    /**
     * 送測－送測文件待提交
     */
    public Integer countStUncommitByUser(Integer userSid) {
        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("userSid", userSid);
        return countRecord("countStUncommitByUser", paramMap);
    }

    /**
     * 送測－退測
     */
    public Integer countStRollbackByUser(Integer userSid) {
        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("userSid", userSid);
        return countRecord("countStRollbackByUser", paramMap);
    }

    /**
     * 送測－今日送測文件待補齊數(for QA)
     */
    public Integer countStTodayUncommit(Integer userSid) {
        if (this.workBackendParamHelper.isQAUser(userSid)) {
            return countRecord("countStTodayUncommit", null);
        }
        return 0;
    }

    /**
     * 需求單首頁最新通知
     */
    public Integer countNotification(Integer userSid) {
        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("userSid", userSid);
        return countRecord("countNotification", paramMap);
    }

    /**
     * 部門未領單
     * 
     * @param userSid
     * @return
     */
    public int countWaitReceiveByDep(Integer userSid, ReqConfirmDepProgStatus reqConfirmDepProgStatus) {

        User user = WkUserCache.getInstance().findBySid(userSid);
        if (user == null || user.getPrimaryOrg() == null || user.getPrimaryOrg().getSid() == null) {
            return 0;
        }

        // ====================================
        // 取得 登入者 所有可操作的單位
        // ====================================
        List<Integer> userMatchDepSids = this.requireConfirmDepService.prepareUserMatchDepSids(userSid);
        if (WkStringUtils.isEmpty(userMatchDepSids)) {
            log.warn("USER:[" + userSid + "] ,沒有符合的單位!");
            return 0;
        }

        // ====================================
        // 兜組查詢SQL
        // ====================================

        String querySql = requireConfirmDepService.prepareSqlForQueryByDepSidInAndProgStatus(
                userMatchDepSids,
                reqConfirmDepProgStatus,
                false,
                false);

        StringBuffer builder = new StringBuffer();
        builder.append("SELECT Count(*) FROM ( SELECT aa.require_sid  ");
        builder.append("FROM   ( " + querySql + ") aa GROUP BY aa.require_sid ) BB ");

        if (this.workBackendParamHelper.isShowRequireSearch07DebugLog()) {
            log.info(""
                    + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()) + "\r\n】"
                    + "\r\n");
        }

        // ====================================
        // 查詢
        // ====================================
        BigInteger count = (BigInteger) this.getSingleResult(builder.toString(), null);

        return count == null ? 0 : count.intValue();

    }

    /**
     * 個人未領單
     * 
     * @param userSid
     * @return
     */
    @Deprecated // (為了和後面報表結果一致，不再使用此方法查詢)
    public int countWaitReceiveByPerson(Integer userSid, ReqConfirmDepProgStatus reqConfirmDepProgStatus) {

        User user = WkUserCache.getInstance().findBySid(userSid);
        if (user == null || user.getPrimaryOrg() == null || user.getPrimaryOrg().getSid() == null) {
            return 0;
        }

        // ====================================
        // 查詢使用者管理單位
        // ====================================
        // 管理單位
        Set<Integer> managerDepSids = WkOrgCache.getInstance().findManagerOrgSids(userSid);
        String managerDepSidsStr = managerDepSids.stream().map(sid -> sid + "").collect(Collectors.joining(", "));

        // 管理單位的收束單位
        Set<Integer> confirmManagerDepSids = Sets.newHashSet();
        for (Integer managerDepSid : managerDepSids) {
            Org org = this.requireConfirmDepService.prepareRequireConfirmDep(managerDepSid);
            confirmManagerDepSids.add(org.getSid());
        }
        String confirmManagerDepSidsStr = confirmManagerDepSids.stream().map(sid -> sid + "").collect(Collectors.joining(", "));

        // ====================================
        // 兜組查詢SQL
        // ====================================
        StringBuffer builder = new StringBuffer();
        builder.append("SELECT Count(*) ");
        builder.append("FROM   (SELECT rcd.require_sid ");
        builder.append("        FROM   tr_require_confirm_dep rcd ");
        builder.append("               INNER JOIN tr_require tr ");
        builder.append("                       ON tr.require_sid = rcd.require_sid ");
        builder.append("                          AND tr.require_status = 'PROCESS' "); // REQ-1502 單據狀態需為『進行中』
        builder.append("                          AND tr.status = 0 ");
        builder.append("                          AND tr.close_code = 0 ");

        if (WkStringUtils.notEmpty(managerDepSids)) {
            builder.append("               LEFT JOIN tr_assign_send_search_info search_info ");
            builder.append("                       ON search_info.require_sid = rcd.require_sid ");
            builder.append("                      AND search_info.type = 0  ");
        }

        builder.append("        WHERE  ( rcd.owner_sid = " + userSid + " ");

        // 若為單位管理者, 增加尋找 owner_sid(單位主管) 的邏輯
        if (WkStringUtils.notEmpty(managerDepSids)) {
            builder.append("                  OR ( rcd.owner_sid = " + WkConstants.MANAGER_VIRTAUL_USER_SID + " ");
            builder.append("                       AND ( ");
            // 為部級單位主管 （收束到部, 找 tr_require_confirm_dep -> 組級分派，部長要看的到）
            builder.append("                             rcd.dep_sid IN ( " + confirmManagerDepSidsStr + " )   ");
            // 或為組級分派單位主管 (找 tr_assign_send_search_info, 為真實分派單位 for 組級主管)
            builder.append("                             OR  search_info.dep_sid IN ( " + managerDepSidsStr + " ) ) ) ");
        }

        builder.append("               )  ");
        builder.append("               AND rcd.prog_status = '" + reqConfirmDepProgStatus.name() + "' ");
        builder.append("        GROUP  BY rcd.require_sid) aa");

        if (this.workBackendParamHelper.isShowRequireSearch07DebugLog()) {
            log.info(""
                    + "\r\n個人未領單筆數SQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(builder.toString()) + "\r\n】"
                    + "\r\n");
        }

        // ====================================
        // 查詢
        // ====================================
        BigInteger count = (BigInteger) this.getSingleResult(builder.toString(), Maps.newHashMap());

        return count == null ? 0 : count.intValue();

    }

    /**
     * 取得該日期 - X 個工作天日期
     *
     * @param date
     * @param totalCount
     * @return
     */
    public Date getSpecialWorkDay(Date date, int totalCount) {
        try {
            int count = 0;
            Date finalDate = date;
            do {
                count = count + 1;
                finalDate = getWorkDay(new LocalDate(finalDate).minusDays(1).toDate());
            } while (count < totalCount);
            return finalDate;
        } catch (Exception e) {
            log.error("getSpecialWorkDay ERROR", e);
        }
        return date;
    }

    private Date getWorkDay(Date date) {
        if (workCalendarHelper.isHoilday(date)) {
            LocalDate preDate = new LocalDate(date).minusDays(1);
            return getWorkDay(preDate.toDate());
        }
        return date;
    }
}
