/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.enumerate;

import lombok.Getter;

/**
 * 報表菜單數量查詢語句
 * <P/>
 * fun_item 裡url 型式必需為 PRCQ_ 開頭
 *
 * @author shaun
 */
public enum ReportCntQueryType {
    /** ON程式一覽表 */
    WORK_ON_PG(-30, 0);

    @Getter
    private final int startDateRange;
    @Getter
    private final int endDateRange;

    private ReportCntQueryType(int startDateRange, int endDateRange) {
        this.startDateRange = startDateRange;
        this.endDateRange = endDateRange;
    }
}
