/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import java.io.Serializable;

/**
 * 計算tree table index用
 *
 * @author shaun
 */
public class Search26ViewIndex implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -365145573263252029L;
    private Integer currentCnt = -1;

    /** 啟始初為-1 ，所以第一次取值為0 */
    public Integer getIndex() {
        currentCnt++;
        return currentCnt;
    }
}
