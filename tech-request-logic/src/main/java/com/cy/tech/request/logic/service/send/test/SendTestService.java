package com.cy.tech.request.logic.service.send.test;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.logic.config.exception.TestInfoEditExceptions;
import com.cy.tech.request.logic.helper.systemnotify.SysNotifyHelper;
import com.cy.tech.request.logic.service.AssignNoticeService;
import com.cy.tech.request.logic.service.CommonService;
import com.cy.tech.request.logic.service.FogbugzService;
import com.cy.tech.request.logic.service.FormNumberService;
import com.cy.tech.request.logic.service.NotificationService;
import com.cy.tech.request.logic.service.RequireProcessCompleteRollbackService;
import com.cy.tech.request.logic.service.RequireReadRecordHelper;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.RequireShowService;
import com.cy.tech.request.logic.service.TrSubNoticeInfoService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.URLService.URLServiceAttr;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepService;
import com.cy.tech.request.logic.utils.DateUtils;
import com.cy.tech.request.logic.vo.WorkTestInfoTo;
import com.cy.tech.request.repository.TrNamedQueryRepository;
import com.cy.tech.request.repository.require.ReqModifyRepo;
import com.cy.tech.request.repository.result.TransRequireVO;
import com.cy.tech.request.repository.worktest.WorkTestAlreadyReplyRepo;
import com.cy.tech.request.repository.worktest.WorkTestAttachmentRepo;
import com.cy.tech.request.repository.worktest.WorkTestInfoHistoryRepo;
import com.cy.tech.request.repository.worktest.WorkTestInfoRepo;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.NotificationEventType;
import com.cy.tech.request.vo.enums.RequireTransProgramType;
import com.cy.tech.request.vo.enums.SubNoticeType;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.template.component.ComBase;
import com.cy.tech.request.vo.template.component.ComTextTypeOne;
import com.cy.tech.request.vo.worktest.WorkTestAlert;
import com.cy.tech.request.vo.worktest.WorkTestAlreadyReply;
import com.cy.tech.request.vo.worktest.WorkTestAttachment;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.vo.worktest.WorkTestInfoHistory;
import com.cy.tech.request.vo.worktest.WorkTestSignInfo;
import com.cy.tech.request.vo.worktest.enums.WorkTestInfoHistoryBehavior;
import com.cy.tech.request.vo.worktest.enums.WorkTestInfoStatus;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.backend.logic.WorkBackendParamService;
import com.cy.work.backend.vo.enums.WkBackendParam;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.common.vo.Attachment;
import com.cy.work.common.vo.AttachmentService;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.cy.work.notify.vo.enums.NotifyType;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * 送測服務
 *
 * @author shaun
 */
@Slf4j
@Component
public class SendTestService implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8759105266263052240L;
    private static SendTestService instance;

    public static SendTestService getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        SendTestService.instance = this;
    }

    @Autowired
    private transient WorkBackendParamService workBackendParamService;
    @Autowired
    private transient WorkBackendParamHelper workBackendParamHelper;
    @Autowired
    private RequireShowService reqShowService;
    @Autowired
    private RequireService reqService;
    @Autowired
    private SendTestShowService stsService;
    @Autowired
    private SendTestBpmService stBpmService;
    @Autowired
    private SendTestHistoryService sthService;
    @Autowired
    @Qualifier("sendTestAttachService")
    private AttachmentService<WorkTestAttachment, WorkTestInfo, String> attachService;
    @Autowired
    private FogbugzService fbService;
    @Autowired
    private URLService urlService;
    @Autowired
    private FormNumberService formNumberService;
    @Autowired
    private transient RequireReadRecordHelper requireReadRecordHelper;
    @Autowired
    private WkJsoupUtils jsoupUtils;
    @Autowired
    private WorkTestInfoRepo testinfoDao;
    @Autowired
    private WorkTestInfoHistoryRepo testHistoryDao;
    @Autowired
    private WorkTestAlreadyReplyRepo aReplyDao;
    @Autowired
    private WorkTestAttachmentRepo attachDao;
    @Autowired
    private ReqModifyRepo reqModifyDao;
    @Autowired
    private TrSubNoticeInfoService subNoticeInfoService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private SendTestAlertService sendTestAlertService;
    @Autowired
    private QaAliasService qaAliasService;
    @Autowired
    private NotificationService notificationService;

    @Autowired
    private SendTestStatusManager sendTestStatusManager;
    @Autowired
    private RequireProcessCompleteRollbackService requireProcessCompleteRollbackService;
    @Autowired
    private RequireConfirmDepService requireConfirmDepService;
    @Autowired
    private AssignNoticeService assignNoticeService;
    @Autowired
    private SysNotifyHelper sysNotifyHelper;
    @Autowired
    private transient TrNamedQueryRepository trNamedQueryRepository;
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private JdbcTemplate jdbcTemplate;

    /**
     * 建立空送測資訊
     *
     * @param executor
     * @param require
     * @return
     */
    public WorkTestInfo createEmptyTestInfo(User executor, Require require) {
        WorkTestInfo info = new WorkTestInfo();
        info.setSourceType(WorkSourceType.TECH_REQUEST);
        info.setSourceSid(require.getSid());
        info.setSourceNo(require.getRequireNo());
        // 建立公司別
        info.setCreateCompany(WkOrgCache.getInstance().findBySid(executor.getPrimaryOrg().getCompanySid()));
        // 建立單位
        info.setCreateDep(WkOrgCache.getInstance().findBySid(executor.getPrimaryOrg().getSid()));
        info.setStatus(Activation.ACTIVE);
        info.setCreatedUser(executor);
        info.setCreatedDate(new Date());
        info.setTestDate(new Date());

        // ====================================
        // 準備預設主題、內容
        // ====================================
        // 預設主題
        String theme = this.workBackendParamService.findTextByKeyword(WkBackendParam.WT_DF_THEME);
        for (String key : require.getContent().getComValueMap().keySet()) {
            ComBase combase = require.getContent().getComValueMap().get(key);
            if (combase instanceof ComTextTypeOne && "主題".equals(combase.getName())) {
                ComTextTypeOne comTextTypeOne = (ComTextTypeOne) combase;
                theme += comTextTypeOne.getValue02();
            }
        }
        info.setTheme(theme);

        // 預設內容
        info.setContent(this.workBackendParamService.findTextByKeyword(WkBackendParam.WT_DF_CONTENT));
        info.setContentCss(info.getContent());

        // ====================================
        // 準備預設送測單位
        // ====================================

        Set<String> sendTestDepSids = Sets.newHashSet();
        if (info.getSendTestDep() != null) {
            info.setSendTestDep(new JsonStringListTo());
        }
        if (WkStringUtils.notEmpty(info.getSendTestDep().getValue())) {
            sendTestDepSids.addAll(info.getSendTestDep().getValue());
        }

        // 1.取得預設送測單位 : 參數後台設定 config.send.test.df.select.deps
        List<Integer> defaultSendTestDepSids = this.workBackendParamService.findIntsByKeyword(WkBackendParam.WT_DF_SEND_TEST_DEPS);
        if (WkStringUtils.notEmpty(defaultSendTestDepSids)) {
            sendTestDepSids.addAll(defaultSendTestDepSids.stream().map(each -> each + "").collect(Collectors.toList()));
        }

        // 2.加入需求單位
        sendTestDepSids.add(require.getCreateDep().getSid() + "");

        // 3.加入需求單位上層組織(到處級)的sid
        List<Org> createDepParents = WkOrgCache.getInstance().findAllParent(
                require.getCreateDep().getSid(), OrgLevel.DIVISION_LEVEL);
        if (WkStringUtils.notEmpty(createDepParents)) {
            for (Org createDepParent : createDepParents) {
                sendTestDepSids.add(createDepParent.getSid() + "");
            }
        }

        info.getSendTestDep().setValue(Lists.newArrayList(sendTestDepSids));

        return info;
    }

    /**
     * 新增送測單 Org selectCompany 參數暫無使用
     * 
     * @throws SystemDevelopException
     * @throws UserMessageException
     */
    @Transactional(noRollbackFor = Exception.class)
    public WorkTestInfo saveByNewTestInfo(
            Require require,
            WorkTestInfo testInfo,
            User executor)
            throws ProcessRestException, IllegalAccessException, UserMessageException, SystemDevelopException {

        Date sysDate = new Date();

        // ====================================
        // 查詢主檔資料
        // ====================================
        Require entity = reqService.findByReqObj(require);

        // ====================================
        // 送測權限檢核
        // ====================================
        if (!reqShowService.showSendTestBtn(entity, reqShowService.isContainAssign(entity, executor))) {
            throw new IllegalAccessException("無新增送測權限！！");
        }

        // ====================================
        // 準備送測主檔欄位 並新增
        // ====================================
        // 內容
        testInfo.setContent(jsoupUtils.clearCssTag(testInfo.getContentCss()));
        // 備註
        testInfo.setNote(jsoupUtils.clearCssTag(testInfo.getNoteCss()));
        // 準備狀態欄位【送測-新建檔】
        this.sendTestStatusManager.changeStatusByNewCreate(testInfo);
        // 送測單號
        testInfo.setTestinfoNo(this.formNumberService.generateWtNum());
        // 存檔
        testInfo = this.save(testInfo, executor, sysDate);
        // 附加檔案
        attachService.linkRelation(testInfo.getAttachments(), testInfo, executor);

        // ====================================
        // 建立讀記錄
        // 1.執行者 -> 已讀
        // 2.填單單位+送測單位人員 -> 待閱讀
        // ====================================
        this.requireReadRecordHelper.saveExcutorReadAndOtherUserWaitRead(
                FormType.WORKTESTSIGNINFO,
                testInfo.getSid(),
                this.prepareReadRecordUserSids(testInfo),
                executor.getSid());

        // ====================================
        // 若設定需送簽時, 進行簽核流程
        // ====================================
        if (require.getMapping().getSmall().getTestSign()) {
            this.sendTestSign(testInfo, executor);
        }

        // ====================================
        // 非分派單位成員開單時, 進行自動分派 (如果需要的話)
        // ====================================
        this.assignNoticeService.processForAutoAddAssignDep(
                require.getSid(),
                require.getRequireNo(),
                testInfo.getCreateDep().getSid(),
                "開立送測單",
                executor,
                sysDate);

        // ====================================
        // 若需求製作進度為已完成, 將進度倒回進行中
        // ====================================
        this.requireProcessCompleteRollbackService.executeProcess(
                require.getSid(),
                executor,
                sysDate,
                "開立送測單");

        // ====================================
        // 再次開啟執行單位的【需求完成-確認流程】 (若需要開啟的話)
        // ====================================
        this.requireConfirmDepService.changeProgStatusCompleteByAddNewSubCase(
                require.getRequireNo(),
                require.getSid(),
                executor.getSid(),
                testInfo.getCreateDep().getSid(),
                "開立送測單",
                sysDate);

        // ====================================
        // 建立通知單位 異動紀錄 【送測】
        // ====================================
        subNoticeInfoService.createFirstSubChangeRecord(
                SubNoticeType.TEST_INFO_DEP,
                require.getSid(),
                require.getRequireNo(),
                testInfo.getSid(),
                testInfo.getTestinfoNo(),
                testInfo.getSendTestDep(),
                executor.getSid());

        // ====================================
        // 【通知】-【送測單已成立】- config.st.notify.user
        // ====================================
        // 若送測單位包含 QA單位才執行
        if (this.isContainsQADepts(testInfo)) {
            this.sendAlertToSpecificUser(testInfo, executor, WorkTestInfoHistoryBehavior.ST_ESTABLISH);
        }

        // ====================================
        // 異動需求單主檔狀態
        // ====================================
        require.setHasTestInfo(Boolean.TRUE);
        reqModifyDao.updateByHasSendTest(require, require.getHasTestInfo());

        // ====================================
        // 處理系統通知 (新增送測單)
        // ====================================
        try {
            this.sysNotifyHelper.processForWorkTest(
                    require.getSid(),
                    testInfo.getTheme(),
                    null, // 新增時，無異動前的通知單位資料
                    (testInfo.getSendTestDep() != null) ? testInfo.getSendTestDep().getValue() : Lists.newArrayList(),
                    executor.getSid());
        } catch (Exception e) {
            log.error("執行系統通知失敗!" + e.getMessage(), e);
        }

        return testInfo;
    }

    /**
     * 送簽
     * 
     * @throws ProcessRestException
     */
    @Transactional(rollbackFor = Exception.class)
    private void sendTestSign(WorkTestInfo testInfo, User executor) throws ProcessRestException {

        // ====================================
        // 建立 BPM 簽核流程
        // ====================================
        WorkTestSignInfo workTestSignInfo = this.stBpmService.createSignInfo(testInfo, executor);

        // ====================================
        // 異動測試主檔
        // ====================================
        // 準備狀態欄位【送簽】
        this.sendTestStatusManager.changeStatusBySignProcess(testInfo);
        // BPM 水管圖
        testInfo.setSignInfo(workTestSignInfo);
        // 存檔
        this.save(testInfo);
    }

    @Transactional(rollbackFor = Exception.class)
    public WorkTestInfo save(WorkTestInfo testinfo) {
        return testinfoDao.save(testinfo);
    }

    @Transactional(rollbackFor = Exception.class)
    public WorkTestInfo save(WorkTestInfo testinfo, User executor) {
        testinfo.setUpdatedDate(new Date());
        testinfo.setUpdatedUser(executor);
        return testinfoDao.save(testinfo);
    }

    @Transactional(rollbackFor = Exception.class)
    public WorkTestInfo save(WorkTestInfo testinfo, User executor, Date sysDate) {
        testinfo.setUpdatedDate(sysDate);
        testinfo.setUpdatedUser(executor);
        return testinfoDao.save(testinfo);
    }

    @Transactional(rollbackFor = Exception.class)
    public void systemAutoUpdateTestingStatus() {
        List<WorkTestInfo> resultList = findByWorkTestStatus(WorkTestStatus.SONGCE);
        User admin = WkUserCache.getInstance().findById("admin");
        String reason = String.format("系統於%s 00:00 將送測狀態變更為測試中", DateUtils.YYYY_MM_DD.print(new Date().getTime()));
        for (WorkTestInfo testInfo : resultList) {
            testInfo.setTestinfoStatus(WorkTestStatus.TESTING);
            this.save(testInfo, admin);
            this.addHistoryRecord(testInfo, admin, WorkTestInfoHistoryBehavior.TESTING, reason);

        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveByEditTestInfo(
            Require require,
            WorkTestInfo editInfo,
            User executor,
            WorkTestInfoTo tempTestInfoTo)

            throws IllegalAccessException, TestInfoEditExceptions {
        if (stsService.disableTestDep(
                reqService.findByReqObj(require),
                this.findByTestinfoNo(editInfo.getTestinfoNo()),
                executor)) {
            throw new IllegalAccessException("無編輯送測權限！！");
        }
        WorkTestInfo backupInfo = this.findByTestinfoNo(editInfo.getTestinfoNo());
        JsonStringListTo sendTestDep = backupInfo.getSendTestDep();
        this.checkWorkTestInfoByBeforeSave(tempTestInfoTo, backupInfo);
        editInfo.setContent(jsoupUtils.clearCssTag(editInfo.getContentCss()));
        editInfo.setNote(jsoupUtils.clearCssTag(editInfo.getNoteCss()));

        this.checkChangeDstablishDate(editInfo, backupInfo, executor);
        attachService.findAttachsByLazy(editInfo).forEach(each -> ((Attachment<String>) each).setKeyChecked(Boolean.TRUE));
        attachService.linkRelation(editInfo.getAttachments(), editInfo, executor);
        subNoticeInfoService.createSubChangeRecord(SubNoticeType.TEST_INFO_DEP,
                require.getSid(),
                require.getRequireNo(), editInfo.getSid(), editInfo.getTestinfoNo(), sendTestDep, editInfo.getSendTestDep(), executor.getSid());
        this.modifyByEditTestInfo(editInfo, executor);

        // ====================================
        // 更新讀記錄 (已包含異動送測單位需新增的人員)
        // 1.執行者 -> 已讀
        // 2.填單單位+送測單位人員 -> 待閱讀
        // 3.刪除被移除的『送測單位人員』
        // ====================================
        this.requireReadRecordHelper.resetUserList(
                FormType.WORKTESTSIGNINFO,
                editInfo.getSid(),
                this.prepareReadRecordUserSids(editInfo),
                executor.getSid());
    }

    /**
     * edit by retest
     */
    @Transactional(noRollbackFor = Exception.class)
    public void saveByRetestTestInfo(
            Require require,
            WorkTestInfo editInfo,
            User executor,
            WorkTestInfoTo tempTestInfoTo)
            throws IllegalAccessException, TestInfoEditExceptions {
        if (!stsService.renderedRetest(
                reqService.findByReqObj(require),
                this.findByTestinfoNo(editInfo.getTestinfoNo()),
                executor)) {
            throw new IllegalAccessException("無操作權限，送測單已被更新！");
        }
        WorkTestInfo backupInfo = this.findByTestinfoNo(editInfo.getTestinfoNo());
        this.checkWorkTestInfoByBeforeSave(tempTestInfoTo, backupInfo);
        editInfo.setContent(jsoupUtils.clearCssTag(editInfo.getContentCss()));
        editInfo.setNote(jsoupUtils.clearCssTag(editInfo.getNoteCss()));
        editInfo.setTestinfoStatus(WorkTestStatus.RETEST);
        editInfo.setReadReason(WaitReadReasonType.TEST_RETEST);
        editInfo.setReadUpdateDate(new Date());
        String reason = "執行重測";
        if (!DateUtils.isSameDate(editInfo.getEstablishDate(), backupInfo.getEstablishDate())) {
            String oldDate = DateUtils.YYYY_MM_DD.print(backupInfo.getEstablishDate().getTime());
            String newDate = DateUtils.YYYY_MM_DD.print(editInfo.getEstablishDate().getTime());
            reason += String.format("<br/>預計完成日由%s調整為%s", oldDate, newDate);
        }
        if (!DateUtils.isSameDate(editInfo.getExpectOnlineDate(), backupInfo.getExpectOnlineDate())) {
            if (backupInfo.getExpectOnlineDate() == null && editInfo.getExpectOnlineDate() != null) {
                String newDate = DateUtils.YYYY_MM_DD.print(editInfo.getExpectOnlineDate().getTime());
                reason += String.format("<br/>預計上線日調整為%s", newDate);
            } else if (backupInfo.getExpectOnlineDate() != null && editInfo.getExpectOnlineDate() != null) {
                String oldDate = DateUtils.YYYY_MM_DD.print(backupInfo.getExpectOnlineDate().getTime());
                String newDate = DateUtils.YYYY_MM_DD.print(editInfo.getExpectOnlineDate().getTime());
                reason += String.format("<br/>預計上線日由%s調整為%s", oldDate, newDate);
            } else if (backupInfo.getExpectOnlineDate() != null && editInfo.getExpectOnlineDate() == null) {
                reason += "<br/>預計上線日調整為空值";
            }
        }
        sthService.createRetestHistory(editInfo, executor, reason);
        attachService.findAttachsByLazy(editInfo).forEach(each -> ((Attachment<String>) each).setKeyChecked(Boolean.TRUE));
        attachService.linkRelation(editInfo.getAttachments(), editInfo, executor);
        this.modifyByEditTestInfo(editInfo, executor);

        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.WORKTESTSIGNINFO,
                editInfo.getSid(),
                executor.getSid());
    }

    /**
     * 重新編輯後，需檢查是否有異動預計完成日
     *
     * @param editInfo
     * @param backupInfo
     * @param executor
     */
    private void checkChangeDstablishDate(WorkTestInfo editInfo, WorkTestInfo backupInfo, User executor) {
        if (!editInfo.getEstablishDate().equals(backupInfo.getEstablishDate())) {
            sthService.createEsDtChangeHistory(editInfo, backupInfo, executor);
            editInfo.setReadReason(WaitReadReasonType.TEST_UPDATE_ESTABLISHDATE);
            editInfo.setReadUpdateDate(new Date());
        }
    }

    /**
     * 準備閱讀記錄 (填單單位+送測單位)
     *
     * @param onpg
     * @return
     */
    private Set<Integer> prepareReadRecordUserSids(WorkTestInfo testInfo) {

        // ====================================
        // 要被通知的單位
        // ====================================
        Set<Integer> allDepSids = Sets.newHashSet();

        // 建單單位
        if (testInfo.getCreateDep() != null) {
            allDepSids.add(testInfo.getCreateDep().getSid());
        }

        // 送測單位
        Set<Integer> sendTestDepSids = testInfo.getSendTestDepSids();
        if (WkStringUtils.notEmpty(sendTestDepSids)) {
            allDepSids.addAll(sendTestDepSids);
        }

        // ====================================
        // 取得部門下所有 user
        // ====================================
        Set<Integer> allUserSids = WkUserCache.getInstance().findUserSidByOrgsWithManager(
                allDepSids, Activation.ACTIVE);

        // ====================================
        // 為組時，把部長拉進來 (組織扁平化調整 原最高為處, 降為部)
        // ====================================
        if (testInfo.getCreateDep() != null
                && testInfo.getCreateDep().getParent() != null
                && OrgLevel.THE_PANEL.equals(testInfo.getCreateDep().getLevel())) {

            Set<Integer> managerUserSids = WkOrgUtils.findOrgManagerUserSidByOrgSid(
                    testInfo.getCreateDep().getParent().getSid());
            if (WkStringUtils.notEmpty(managerUserSids)) {
                allUserSids.addAll(managerUserSids);
            }
        }

        // 移除停用人員
        return allUserSids.stream()
                .filter(userSid -> WkUserUtils.isActive(userSid))
                .collect(Collectors.toSet());
    }

    @Transactional(rollbackFor = Exception.class)
    public void createFBCase(Require require, WorkTestInfo testInfo, User executor) throws IllegalArgumentException, UnsupportedEncodingException {
        log.info("建立送測FB資訊 Start -> 執行人:" + executor.getId() + " 執行部門:" + executor.getPrimaryOrg().getId());
        int fbId = fbService.createSendTestFB(require, testInfo, executor);
        log.info("建立送測FB資訊 End   -> 執行人:" + executor.getId() + " 執行部門:" + executor.getPrimaryOrg().getId() + " fbId:" + fbId);
        testInfo.setFbId(fbId);
        this.save(testInfo, executor);
    }

    public Integer verifyFbId(String editFbId) {
        if (Strings.isNullOrEmpty(editFbId) || editFbId.trim().equals("")) {
            return null;
        }
        // 情況1 - 從FB貼完整網址
        if (editFbId.contains("#") && editFbId.contains(fbService.findFbApiUrl())) {
            String fbIdArray = editFbId.replace(fbService.findFbApiUrl(), "").split("#")[0];
            if (fbIdArray.matches("\\d+")) {
                return Integer.valueOf(fbIdArray);
            }
        }
        // 情況2 只有#字號無網址
        if (editFbId.contains("#") && !editFbId.contains(fbService.findFbApiUrl())) {
            String fbIdArray = editFbId.split("#")[0];
            if (fbIdArray.matches("\\d+")) {
                return Integer.valueOf(fbIdArray);
            }
        }
        // 情況3 只有網址無#字號
        if (editFbId.contains(fbService.findFbApiUrl()) && !editFbId.contains("#")) {
            String fbIdArray = editFbId.replace(fbService.findFbApiUrl(), "");
            if (fbIdArray.matches("\\d+")) {
                return Integer.valueOf(fbIdArray);
            }
        }
        // 情況4 無網址無#字號
        if (editFbId.matches("\\d+")) {
            return Integer.valueOf(editFbId);
        }
        throw new IllegalArgumentException("FB No錯誤，請重新輸入！");
    }

    @Transactional(rollbackFor = Exception.class)
    public void updateFbId(WorkTestInfo testInfo, Integer editFbId, User executor) {
        testInfo.setFbId(editFbId);
        testInfo.setForwardToFbUrl(fbService.createFbUrl(testInfo.getFbId()));
        testinfoDao.updateFbId(testInfo, editFbId, executor, new Date());
    }

    @Transactional(readOnly = true)
    public List<WorkTestInfo> findByRequire(Require require) {
        if (require == null || Strings.isNullOrEmpty(require.getSid())) {
            return Lists.newArrayList();
        }
        return testinfoDao.findBySourceSidOrderByCreatedDateDesc(require.getSid());
    }

    public String createSearch15ViewUrlLink(WorkTestInfo testInfo) {
        return "../search/search15.xhtml" + urlService.createSimpleURLLink(URLServiceAttr.URL_ATTR_S, testInfo.getTestinfoNo(), testInfo.getSourceNo(), 1);
    }

    @Transactional(readOnly = true)
    public List<String> findSidsBySourceSid(String sourceSid) {
        return testinfoDao.findSidsBySourceTypeAndSourceSid(WorkSourceType.TECH_REQUEST, sourceSid);
    }

    @Transactional(readOnly = true)
    public List<WorkTestInfo> findByTestinfoNoIn(List<String> testNoes) {
        return testinfoDao.findByTestinfoNoIn(testNoes);
    }

    @Transactional(readOnly = true)
    public WorkTestInfo findByTestinfoNo(String testNo) {
        return testinfoDao.findByTestinfoNo(testNo);
    }

    @Transactional(readOnly = true)
    public Integer findConutByStatus(String requireNo, WorkTestStatus status) {
        return testinfoDao.findCountBySourceTypeAndSourceNoAndStatus(WorkSourceType.TECH_REQUEST, requireNo, Lists.newArrayList(status));
    }

    @Transactional(readOnly = true)
    public WorkTestStatus findInfoStatus(WorkTestInfo testinfo) {
        return testinfoDao.findInfoStatus(testinfo);
    }

    @Transactional(readOnly = true)
    public List<WorkTestInfo> initTabInfo(Require require) {
        if (require == null || Strings.isNullOrEmpty(require.getSid())) {
            return Lists.newArrayList();
        }
        List<WorkTestInfo> testinfos = testinfoDao.findBySourceSidOrderByCreatedDateDesc(require.getSid());
        List<WorkTestInfoHistory> historys = testHistoryDao
                .findByTestInfoNoIn(testinfos.stream().map(each -> each.getTestinfoNo()).collect(Collectors.toList()));
        if (historys == null) {
            historys = Lists.newArrayList();
        }
        List<WorkTestAlreadyReply> replys = Lists.newArrayList();
        List<WorkTestAttachment> attachs = Lists.newArrayList();
        if (!historys.isEmpty()) {
            List<String> historySids = historys.stream().map(each -> each.getSid()).collect(Collectors.toList());
            replys = aReplyDao.findReplyByHistoryInOrderByUpdateDateDesc(historySids);
            attachs = attachDao.findAttachByHistoryInOrderByUpdateDateDesc(historySids);
        }
        Map<String, List<WorkTestAlreadyReply>> replysMapKeyIsReplySid = replys.stream().collect(Collectors.groupingBy(each -> each.getReply().getSid()));
        historys.stream()
                .filter(each -> each.getReply() != null)
                .filter(each -> replysMapKeyIsReplySid.containsKey(each.getReply().getSid()))
                .forEach(each -> each.getReply().setAlreadyReplys(replysMapKeyIsReplySid.get(each.getReply().getSid())));
        Map<String, List<WorkTestAttachment>> attachMapKeyIsHistorySid = attachs.stream().collect(Collectors.groupingBy(each -> each.getHistory().getSid()));
        historys.stream()
                .filter(each -> attachMapKeyIsHistorySid.containsKey(each.getSid()))
                .forEach(each -> each.setAttachments(attachMapKeyIsHistorySid.get(each.getSid())));
        Map<String, List<WorkTestInfoHistory>> historysMapKeyIsTestinfoNo = historys.stream()
                .collect(Collectors.groupingBy(WorkTestInfoHistory::getTestinfoNo));

        testinfos.stream()
                .map(st -> {
                    st.setForwardToFbUrl(fbService.createFbUrl(st.getFbId()));
                    return st;
                })
                .filter(st -> historysMapKeyIsTestinfoNo.containsKey(st.getTestinfoNo()))
                .forEach(st -> {
                    st.setInfoHistorys(
                            historysMapKeyIsTestinfoNo.get(st.getTestinfoNo()).stream()
                                    .filter(history -> !history.getBehavior().equals(WorkTestInfoHistoryBehavior.REPLY_AND_REPLY))
                                    .collect(Collectors.toList()));
                });
        return testinfos;
    }

    /**
     * 轉換為自訂物件
     *
     * @param testNo
     * @return
     */
    public WorkTestInfoTo findToByTestinfoNo(String testNo) {
        WorkTestInfo testInfo = this.findByTestinfoNo(testNo);
        if (testInfo == null) {
            log.error("查無該testInfo資料，testInfo單號：" + testNo);
            return null;
        }
        return this.convert2To(testInfo);
    }

    public WorkTestInfoTo convert2To(WorkTestInfo testInfo) {
        WorkTestInfoTo to = new WorkTestInfoTo();
        to.setSid(testInfo.getSid());
        to.setTestinfoNo(testInfo.getTestinfoNo());
        to.setSourceNo(testInfo.getSourceNo());
        to.setSendTestDep(testInfo.getSendTestDep());
        to.setHasSign(testInfo.getHasSign());
        to.setTheme(testInfo.getTheme());
        to.setContent(testInfo.getContent());
        to.setContentCss(testInfo.getContentCss());
        to.setNote(testInfo.getNote());
        to.setNoteCss(testInfo.getNoteCss());
        to.setEstablishDate(testInfo.getEstablishDate());
        to.setFinishDate(testInfo.getFinishDate());
        to.setCancelDate(testInfo.getCancelDate());
        to.setTestinfoStatus(testInfo.getTestinfoStatus());
        to.setFbId(testInfo.getFbId());
        to.setUpdatedDate(testInfo.getUpdatedDate());

        if (testInfo.getCreateDep() != null) {
            to.setCreateDepSid(testInfo.getCreateDep().getSid());
        }

        if (testInfo.getCreatedUser() != null) {
            to.setCreateUsr(testInfo.getCreatedUser().getSid());
        }

        return to;
    }

    /**
     * 在存檔前進行檢核
     *
     * @param tempTo   副本
     * @param dbSource DB即時資料
     * @throws TestInfoEditExceptions
     */
    private void checkWorkTestInfoByBeforeSave(WorkTestInfoTo tempTo, WorkTestInfo dbSource) throws TestInfoEditExceptions {
        if (tempTo.getUpdatedDate().compareTo(dbSource.getUpdatedDate()) != 0) {
            StringBuilder errorMsg = new StringBuilder();
            if (!this.checkSameBySendTestDep(tempTo.getSendTestDep().getValue(), dbSource.getSendTestDep().getValue())) {
                errorMsg.append("送測單位、");
            }
            if (!tempTo.getTheme().equals(dbSource.getTheme())) {
                errorMsg.append("主題、");
            }
            if (!tempTo.getContentCss().equals(dbSource.getContentCss())) {
                errorMsg.append("內容、");
            }
            if (!tempTo.getNoteCss().equals(dbSource.getNoteCss())) {
                errorMsg.append("備註、");
            }
            if (dbSource.getEstablishDate() != null
                    && tempTo.getEstablishDate().compareTo(dbSource.getEstablishDate()) != 0) {
                errorMsg.append("預計完成日、");
            }
            if (dbSource.getFinishDate() != null
                    && tempTo.getFinishDate().compareTo(dbSource.getFinishDate()) != 0) {
                errorMsg.append("完成日、");
            }
            if (dbSource.getCancelDate() != null
                    && tempTo.getCancelDate().compareTo(dbSource.getCancelDate()) != 0) {
                errorMsg.append("取消日、");
            }
            if (!tempTo.getTestinfoStatus().equals(dbSource.getTestinfoStatus())) {
                errorMsg.append("狀態、");
            }
            if (dbSource.getHasSign() != null
                    && !tempTo.getHasSign().equals(dbSource.getHasSign())) {
                errorMsg.append("是否有流程狀態、");
            }
            if (dbSource.getFbId() != null
                    && !tempTo.getFbId().equals(dbSource.getFbId())) {
                errorMsg.append("fb Id、");
            }
            if (errorMsg.length() != 0) {
                String errMsg = "送測單號：" + tempTo.getTestinfoNo() + " ，來源單號：" + tempTo.getSourceNo();
                errMsg += "<BR/>異動欄位：" + errorMsg.toString().substring(0, errorMsg.length() - 1);
                throw new TestInfoEditExceptions(errMsg);
            }
        }
    }

    /**
     * 判斷送測單位是否相同
     *
     * @param depsByTemp
     * @param depsByDb
     * @return
     */
    private boolean checkSameBySendTestDep(List<String> depsByTemp, List<String> depsByDb) {
        if (depsByTemp.size() != depsByDb.size()) {
            return false;
        }
        return depsByTemp.stream()
                .allMatch(each -> depsByDb.contains(each));
    }

    /**
     * 存檔只部份更新
     *
     * @param obj
     * @param executor
     */
    @Transactional(rollbackFor = Exception.class)
    private void modifyByEditTestInfo(WorkTestInfo obj, User executor) {
        WorkTestInfo backupInfo = this.findByTestinfoNo(obj.getTestinfoNo());
        backupInfo.setSendTestDep(obj.getSendTestDep());
        backupInfo.setTestinfoStatus(obj.getTestinfoStatus());
        backupInfo.setTheme(obj.getTheme());
        backupInfo.setContent(obj.getContent());
        backupInfo.setContentCss(obj.getContentCss());
        backupInfo.setNote(obj.getNote());
        backupInfo.setNoteCss(obj.getNoteCss());
        backupInfo.setEstablishDate(obj.getEstablishDate());
        backupInfo.setExpectOnlineDate(obj.getExpectOnlineDate());
        backupInfo.setReadUpdateDate(obj.getReadUpdateDate());
        backupInfo.setReadReason(obj.getReadReason());
        backupInfo.setReadRecord(obj.getReadRecord());
        backupInfo.setFbId(obj.getFbId());
        backupInfo.setUpdatedDate(new Date());
        backupInfo.setUpdatedUser(executor);
        testinfoDao.save(backupInfo);
    }

    private void checkOperatePermission(boolean hasPermission) throws IllegalAccessException {
        if (!hasPermission) {
            throw new IllegalAccessException("無操作權限，送測單已被更新！");
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveOrUpdate(
            WorkTestInfo testInfo,
            User loginUser,
            String column,
            boolean isQaManager) throws Exception {

        WorkTestInfo entity = findByTestinfoNo(testInfo.getTestinfoNo());
        NotificationEventType event = null;
        Org loginOrg = WkOrgCache.getInstance().findBySid(loginUser.getPrimaryOrg().getSid());

        // 是否需將閱讀記錄更新為待閱
        boolean updateWaitRead = false;

        switch (column) {
        case "sendTestDepts":
            Require req = reqService.findByReqSid(entity.getSourceSid());

            subNoticeInfoService.createSubChangeRecord(
                    SubNoticeType.TEST_INFO_DEP,
                    req.getSid(),
                    req.getRequireNo(),
                    entity.getSid(), entity.getTestinfoNo(),
                    entity.getSendTestDep(), testInfo.getSendTestDep(), loginUser.getSid());
            boolean isSongce = WorkTestStatus.SONGCE.equals(testInfo.getTestinfoStatus());
            boolean isApproved = WorkTestInfoStatus.APPROVED.equals(testInfo.getQaAuditStatus());
            // 如果尚未審核, 依照有無QA單位更改狀態
            if (isSongce && !isApproved) {
                if (isContainsQADepts(testInfo)) {
                    entity.setQaAuditStatus(WorkTestInfoStatus.WAIT_APPROVE);
                    entity.setQaScheduleStatus(WorkTestInfoStatus.UNDEFINED);
                    entity.setCommitStatus(WorkTestInfoStatus.UNDEFINED);
                } else {
                    entity.setQaAuditStatus(WorkTestInfoStatus.UNNEEDED_APPROVE);
                    entity.setQaScheduleStatus(WorkTestInfoStatus.UNNEEDED_SCHEDULE);
                    entity.setCommitStatus(WorkTestInfoStatus.UNNEEDED_COMMIT);
                }
            }
            entity.setSendTestDep(testInfo.getSendTestDep());
            break;
        case "theme":
            this.checkOperatePermission(stsService.hasModifyPermission(entity, loginOrg));
            Preconditions.checkArgument(StringUtils.isNotBlank(testInfo.getTheme()), "送測主題不可為空白，請重新輸入！！");
            entity.setTheme(testInfo.getTheme());
            event = NotificationEventType.UPDATE_THTEM;
            break;
        case "content":
            this.checkOperatePermission(stsService.hasModifyPermission(entity, loginOrg));
            Preconditions.checkArgument(StringUtils.isNotBlank(jsoupUtils.clearCssTag(testInfo.getContentCss())),
                    " 送測內容不可為空白，請重新輸入！！");
            entity.setContent(jsoupUtils.clearCssTag(testInfo.getContentCss()));
            entity.setContentCss(testInfo.getContentCss());
            event = NotificationEventType.UPDATE_CONTENT;
            break;
        case "note":
            this.checkOperatePermission(stsService.hasModifyPermission(entity, loginOrg));
            entity.setNote(jsoupUtils.clearCssTag(testInfo.getNoteCss()));
            entity.setNoteCss(testInfo.getNoteCss());
            event = NotificationEventType.UPDATE_NOTE;
            break;
        case "fbNo":
            this.checkOperatePermission(stsService.isCreatedAndAboveManager(entity.getCreatedUser().getSid(), loginUser));
            if (testInfo.getFbId() != null) {
                int fbId = verifyFbId(testInfo.getFbId().toString());
                entity.setFbId(fbId);
                entity.setForwardToFbUrl(fbService.createFbUrl(testInfo.getFbId()));
            } else {
                entity.setFbId(null);
                entity.setForwardToFbUrl(null);
            }
            event = NotificationEventType.UPDATE_FB_NO;
            break;
        case "qaLink":
            this.checkOperatePermission(stsService.renderedQALink(entity, loginUser));
            if (StringUtils.isNotBlank(testInfo.getQaLinkName())) {
                Preconditions.checkArgument(testInfo.getQaLinkName().length() <= 255, "輸入的名稱過長!");
                Preconditions.checkArgument(StringUtils.isNotBlank(testInfo.getQaSearchLink()), "請輸入網址列");
            }
            if (StringUtils.isNotBlank(testInfo.getQaSearchLink())) {
                Preconditions.checkArgument(testInfo.getQaSearchLink().length() <= 500, "輸入的網址列過長!");
                Preconditions.checkArgument(testInfo.getQaSearchLink().matches("http(s)?://.*"), "錯誤的網址列，請輸入http(s)://");
                Preconditions.checkArgument(StringUtils.isNotBlank(testInfo.getQaLinkName()), "請輸入名稱");
            }

            entity.setQaLinkName(testInfo.getQaLinkName().trim());
            entity.setQaSearchLink(testInfo.getQaSearchLink().trim());
            break;
        case "masterTesters":
            this.checkOperatePermission(stsService.renderedTestersBtn(entity, isQaManager));
            // 若都無測試人員，不做任何事
            if (StringUtils.isBlank(entity.getMasterTesters()) && StringUtils.isBlank(testInfo.getMasterTesters())) {
                return;
            }
            if (StringUtils.isBlank(entity.getMasterTesters())) {
                String reason = String.format("主測人員：%s", qaAliasService.convertToQaAliasNames(testInfo.getMasterTesters()));
                addHistoryRecord(entity, loginUser, WorkTestInfoHistoryBehavior.ASSIGN_MASTER_TESTER, reason);
            } else {
                String afterAdjustQa = qaAliasService.convertToQaAliasNames(testInfo.getMasterTesters());
                String reason = null;
                if (StringUtils.isBlank(afterAdjustQa)) {
                    reason = "清空主測人員";
                } else {
                    reason = String.format("原主測人員：%s<br/>調整後主測人員：%s",
                            qaAliasService.convertToQaAliasNames(entity.getMasterTesters()), afterAdjustQa);
                }

                addHistoryRecord(entity, loginUser, WorkTestInfoHistoryBehavior.ADJUST_MASTER_TESTER, reason);
            }
            entity.setReadReason(WaitReadReasonType.ADJUST_MASTER_TESTER);
            entity.setMasterTesters(testInfo.getMasterTesters());

            // 閱讀記錄更新為待閱
            updateWaitRead = true;

            break;
        case "slaveTesters":
            this.checkOperatePermission(stsService.renderedTestersBtn(entity, isQaManager));
            // 若都無測試人員，不做任何事
            if (StringUtils.isBlank(entity.getSlaveTesters()) && StringUtils.isBlank(testInfo.getSlaveTesters())) {
                return;
            }
            if (StringUtils.isBlank(entity.getSlaveTesters())) {
                String reason = String.format("協測人員：%s", qaAliasService.convertToQaAliasNames(testInfo.getSlaveTesters()));
                addHistoryRecord(entity, loginUser, WorkTestInfoHistoryBehavior.ASSIGN_SLAVE_TESTER, reason);
            } else {
                String afterAdjustQa = qaAliasService.convertToQaAliasNames(testInfo.getSlaveTesters());
                String reason = null;
                if (StringUtils.isBlank(afterAdjustQa)) {
                    reason = "清空協測人員";
                } else {
                    reason = String.format("原協測人員：%s<br/>調整後協測人員：%s",
                            qaAliasService.convertToQaAliasNames(entity.getSlaveTesters()), afterAdjustQa);
                }

                addHistoryRecord(entity, loginUser, WorkTestInfoHistoryBehavior.ADJUST_SLAVE_TESTER, reason);
            }
            entity.setReadReason(WaitReadReasonType.ADJUST_SLAVE_TESTER);
            entity.setSlaveTesters(testInfo.getSlaveTesters());

            // 閱讀記錄更新為待閱
            updateWaitRead = true;

            break;
        default:
            break;
        }
        this.save(entity, loginUser);

        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        if (updateWaitRead) {
            this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                    FormType.WORKTESTSIGNINFO,
                    entity.getSid(),
                    loginUser.getSid());
        }

        // 通知QA人員事件
        if (event != null) {
            notifyQaUser(entity, loginUser.getSid(), Lists.newArrayList(event));
        }
    }

    /**
     * 納入排程
     * 
     * @param workTestInfo
     */
    @Transactional(rollbackFor = Exception.class)
    public void joinSchedule(WorkTestInfo workTestInfo, User loginUser) {
        try {
            Date currentTime = new Date();
            workTestInfo = testinfoDao.findOne(workTestInfo.getSid());
            workTestInfo.setQaAuditStatus(WorkTestInfoStatus.APPROVED);
            workTestInfo.setQaAuditDate(currentTime);
            workTestInfo.setQaScheduleStatus(WorkTestInfoStatus.JOIN_SCHEDULE);
            workTestInfo.setCommitStatus(WorkTestInfoStatus.UNCOMMIT);
            workTestInfo.setUpdatedDate(currentTime);
            workTestInfo.setUpdatedUser(loginUser);
            workTestInfo.setReadReason(WaitReadReasonType.QA_JOIN_SCHEDULE);
            testinfoDao.save(workTestInfo);

            WorkTestInfoHistory history = sthService.createEmptyHistory(workTestInfo, loginUser);
            history.setBehavior(WorkTestInfoHistoryBehavior.QA_JOIN_SCHEDULE);
            history.setReason(WorkTestInfoStatus.JOIN_SCHEDULE.getValue());
            history.setReasonCss(WorkTestInfoStatus.JOIN_SCHEDULE.getValue());
            sthService.update(history, loginUser);

            sendTestAlertService.save(createUnReadAlert(workTestInfo, loginUser, WorkTestInfoHistoryBehavior.QA_JOIN_SCHEDULE));
            this.sendAlertToSpecificUser(workTestInfo, loginUser, WorkTestInfoHistoryBehavior.QA_JOIN_SCHEDULE);

            // ====================================
            // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
            // ====================================
            this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                    FormType.WORKTESTSIGNINFO,
                    workTestInfo.getSid(),
                    loginUser.getSid());

            // ====================================
            // 處理系統通知 (QA納入排程)
            // ====================================
            try {
                this.sysNotifyHelper.processForWorkTestEventForRelationUser(
                        workTestInfo,
                        NotifyType.QA_JOIN_SCHEDULE,
                        loginUser.getSid());
            } catch (Exception e) {
                log.error("執行系統通知失敗!" + e.getMessage(), e);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 不納入排程
     * 
     * @param testInfo
     */
    @Transactional(rollbackFor = Exception.class)
    public void unjoinSchedule(WorkTestInfoHistory history, User loginUser) {
        try {
            Date currentTime = new Date();
            // for QA white list update, 不納排程原因 修改
            if (history.getSid() != null) {
                WorkTestInfoHistory entity = sthService.findOne(history.getSid());
                entity.setReason(jsoupUtils.clearCssTag(history.getReasonCss()));
                entity.setReasonCss(history.getReasonCss());
                sthService.update(entity, loginUser);
                return;
            }

            WorkTestInfo workTestInfo = testinfoDao.findOne(history.getTestInfo().getSid());
            workTestInfo.setQaAuditStatus(WorkTestInfoStatus.APPROVED);
            workTestInfo.setQaScheduleStatus(WorkTestInfoStatus.UNJOIN_SCHEDULE);
            workTestInfo.setQaAuditDate(currentTime);
            workTestInfo.setCommitStatus(WorkTestInfoStatus.UNNEEDED_COMMIT);
            workTestInfo.setUpdatedDate(currentTime);
            workTestInfo.setUpdatedUser(loginUser);
            workTestInfo.setReadReason(WaitReadReasonType.QA_UNJOIN_SCHEDULE);
            testinfoDao.save(workTestInfo);

            // add trace record
            history.setBehavior(WorkTestInfoHistoryBehavior.QA_UNJOIN_SCHEDULE);
            history.setReason(jsoupUtils.clearCssTag(history.getReasonCss()));
            history.setUpdatedDate(currentTime);
            history.setUpdatedUser(loginUser);
            sthService.update(history, loginUser);

            sendTestAlertService.save(createUnReadAlert(workTestInfo, loginUser, WorkTestInfoHistoryBehavior.QA_UNJOIN_SCHEDULE));

            // ====================================
            // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
            // ====================================
            this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                    FormType.WORKTESTSIGNINFO,
                    workTestInfo.getSid(),
                    loginUser.getSid());

            // ====================================
            // 處理系統通知 (QA不納入排程)
            // ====================================
            try {
                this.sysNotifyHelper.processForWorkTestEventForRelationUser(
                        workTestInfo,
                        NotifyType.QA_UNJOIN_SCHEDULE,
                        loginUser.getSid());
            } catch (Exception e) {
                log.error("執行系統通知失敗!" + e.getMessage(), e);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }

    /**
     * 文件補齊
     * 
     * @param testInfo
     */
    @Transactional(rollbackFor = Exception.class)
    public void commitPaper(WorkTestInfo testInfo, User loginUser) throws IllegalAccessException {
        if (!stsService.renderedCommitPaper(testInfo, loginUser)) {
            throw new IllegalAccessException("無操作權限，送測單已被更新！");
        }
        try {
            testInfo = testinfoDao.findOne(testInfo.getSid());
            testInfo.setCommitStatus(WorkTestInfoStatus.COMMITED);
            testInfo.setReadReason(WaitReadReasonType.TEST_PAPER_READY);
            testInfo.setReadUpdateDate(new Date());
            if (DateUtils.isSystemDate(testInfo.getTestDate())) {
                testInfo.setTestinfoStatus(WorkTestStatus.TESTING);
            }
            testinfoDao.save(testInfo);

            WorkTestInfoHistory history = sthService.createEmptyHistory(testInfo, loginUser);
            history.setBehavior(WorkTestInfoHistoryBehavior.PAPER_READY);
            if (DateUtils.isSystemDate(testInfo.getTestDate())) {
                history.setReason("文件補齊，送測狀態變更為測試中");
                history.setReasonCss("文件補齊，送測狀態變更為測試中");
            } else {
                history.setReason(commonService.get(WorkTestInfoHistoryBehavior.PAPER_READY));
                history.setReasonCss(commonService.get(WorkTestInfoHistoryBehavior.PAPER_READY));
            }

            history.setVisiable(true);
            sthService.update(history, loginUser);

            // ====================================
            // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
            // ====================================
            this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                    FormType.WORKTESTSIGNINFO,
                    testInfo.getSid(),
                    loginUser.getSid());

            // ====================================
            // add alert
            // ====================================
            List<Integer> userSids = Lists.newArrayList();
            // 主測
            if (StringUtils.isNotBlank(testInfo.getMasterTesters())) {
                String[] arr = testInfo.getMasterTesters().split(",");
                for (String userSid : arr) {
                    userSids.add(Integer.valueOf(userSid));
                }
            }
            // 協測
            if (StringUtils.isNotBlank(testInfo.getSlaveTesters())) {
                String[] arr = testInfo.getSlaveTesters().split(",");
                for (String userSid : arr) {
                    userSids.add(Integer.valueOf(userSid));
                }
            }

            if (userSids.isEmpty()) {
                // 取得 QA 部門設定
                List<Integer> qaDepSids = this.workBackendParamHelper.getQADepSids();
                // 取得所有人員
                List<User> qaUsers = WkUserCache.getInstance().findUserWithManagerByOrgSids(qaDepSids, Activation.ACTIVE);
                if (WkStringUtils.notEmpty(qaUsers)) {
                    for (User user : qaUsers) {
                        userSids.add(user.getSid());
                    }
                }
            }

            for (Integer sid : userSids) {
                WorkTestAlert alert = createUnReadAlert(testInfo, loginUser, WorkTestInfoHistoryBehavior.PAPER_READY);
                alert.setReceiver(WkUserCache.getInstance().findBySid(sid));
                sendTestAlertService.save(alert);
            }

        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    public WorkTestInfo findOne(String sid) {
        return testinfoDao.findOne(sid);
    }

    public List<WorkTestInfo> findByWorkTestStatus(WorkTestStatus status) {
        return testinfoDao.findByWorkTestStatus(status);
    }

    /**
     * 建立最新消息
     */
    public WorkTestAlert createUnReadAlert(WorkTestInfo testInfo, User loginUser, WorkTestInfoHistoryBehavior behavior) {
        WorkTestAlert alert = new WorkTestAlert();
        alert.setTestInfo(testInfo);
        alert.setTestinfoNo(testInfo.getTestinfoNo());
        alert.setSourceType(WorkSourceType.TECH_REQUEST);
        alert.setSourceSid(testInfo.getSourceSid());
        alert.setSourceNo(testInfo.getSourceNo());

        alert.setTestinfoTheme(commonService.get(behavior) + "-" + testInfo.getTheme());
        alert.setSender(loginUser);
        alert.setSendDate(new Date());
        alert.setReceiver(testInfo.getCreatedUser());
        alert.setReadStatus(ReadRecordType.UN_READ);
        return alert;
    }

    /**
     * 額外發送最新消息給特定人員
     */
    public void sendAlertToSpecificUser(WorkTestInfo testInfo, User loginUser, WorkTestInfoHistoryBehavior behavior) {

        // 查詢送測通知人員
        List<Integer> userSids = this.workBackendParamService.findIntsByKeyword(WkBackendParam.ST_NOTIFY_USER);

        for (Integer userSid : userSids) {
            // 檢查
            User user = WkUserCache.getInstance().findBySid(userSid);
            if (user == null) {
                log.warn("請檢查後台參數[{}]是否有設定錯誤，userSid[{}]找不到!",
                        WkBackendParam.ST_NOTIFY_USER.name(),
                        userSid);
                continue;
            }
            if (Activation.INACTIVE.equals(user.getStatus())) {
                log.warn("請檢查後台參數[{}]，user:[{}-{}], 已停用!",
                        WkBackendParam.ST_NOTIFY_USER.name(),
                        userSid,
                        user.getId());
                continue;
            }

            // 寫通知檔
            WorkTestAlert alert = createUnReadAlert(testInfo, loginUser, behavior);
            alert.setReceiver(user);
            sendTestAlertService.save(alert);
        }
    }

    public void addHistoryRecord(WorkTestInfo testInfo, User loginUser, WorkTestInfoHistoryBehavior behavior,
            String reason) {
        WorkTestInfoHistory history = sthService.createEmptyHistory(testInfo, loginUser);
        history.setBehavior(behavior);
        history.setReason(reason);
        history.setReasonCss(reason);
        sthService.update(history, loginUser);
    }

    /**
     * 是否包含QA單位
     */
    public boolean isContainsQADepts(WorkTestInfo testInfo) {
        // 防呆
        if (testInfo == null || testInfo.getSendTestDep() == null || WkStringUtils.isEmpty(testInfo.getSendTestDep().getValue())) {
            return false;
        }

        // 由參數中取得 QA 單位
        List<String> qaDepSids = this.workBackendParamHelper.getQADepSids().stream()
                .map(each -> each + "")
                .collect(Collectors.toList());

        // 逐一筆對
        for (String deptSid : testInfo.getSendTestDep().getValue()) {
            if (qaDepSids.contains(deptSid)) {
                return true;
            }
        }

        return false;
    }

    /**
     * 批次轉單程式-建立單位轉發(送測)
     * 
     * @return
     */
    public List<TransRequireVO> findSendTestByCreateDeptAndUser(List<Integer> deptSids, List<Integer> userSids) {
        List<TransRequireVO> resultList = trNamedQueryRepository.findSendTestByCreateDeptAndUser(deptSids, userSids);
        return convert(resultList);
    }

    /**
     * 批次轉單程式-通知單位轉發(送測)
     * 
     * @return
     */
    public List<TransRequireVO> findSendTestByNotifiedDept(String depts) {
        List<TransRequireVO> resultList = trNamedQueryRepository.findSendTestByNotifiedDept(depts);
        return convert(resultList);
    }

    private List<TransRequireVO> convert(List<TransRequireVO> resultList) {
        for (TransRequireVO vo : resultList) {
            vo.setType(RequireTransProgramType.WORKTESTSIGNINFO.name());
            vo.setCreateDept(WkOrgCache.getInstance().findNameBySid(vo.getDeptSid()));
            vo.setCreateUserName(WkUserCache.getInstance().findBySid(vo.getUserSid()).getId());
            vo.setStatus(WorkTestStatus.valueOf(vo.getStatus()).getValue());
        }
        return resultList;
    }

    /**
     * 修改日期實作
     * 
     * @param column
     */
    public void editDateImpl(WorkTestInfo testInfo, User loginUser) throws Exception {
        WorkTestInfo entity = findByTestinfoNo(testInfo.getTestinfoNo());
        StringBuffer reason = new StringBuffer();

        // ====================================
        // 檢查
        // ====================================
        Preconditions.checkArgument(testInfo.getTestDate() != null, "送測日不可為空白，請重新輸入！！");
        Preconditions.checkArgument(testInfo.getEstablishDate() != null, "預計完成日不可為空白，請重新輸入！！");

        // ====================================
        // 調整送測日
        // ====================================
        List<NotificationEventType> notifyEvents = Lists.newArrayList();
        if (!testInfo.getTestDate().equals(entity.getTestDate())) {
            this.checkOperatePermission(stsService.modifyStatusByTestDate(entity, loginUser));
            Preconditions.checkArgument(!testInfo.getTestDate().before(DateUtils.convertToOriginOfDay(new Date())), "送測日不得小於系統日，請重新調整！");
            // 發最新消息給建單者 and mickey
            if (workBackendParamHelper.isQAUser(loginUser)) {
                // 給立單者
                sendTestAlertService.save(createUnReadAlert(testInfo, loginUser, WorkTestInfoHistoryBehavior.ADJUST_TEST_DATE));
                // 給後台參數『送測 - 送測通知人員』
                this.sendAlertToSpecificUser(testInfo, loginUser, WorkTestInfoHistoryBehavior.ADJUST_TEST_DATE);
            }
            // add history
            String oldDate = DateUtils.YYYY_MM_DD.print(entity.getTestDate().getTime());
            String newDate = DateUtils.YYYY_MM_DD.print(testInfo.getTestDate().getTime());
            String message = String.format("送測日由%s調整為%s<br/>", oldDate, newDate);
            reason.append(message);
            // 自動變更測試中
            if (WorkTestStatus.SONGCE.equals(entity.getTestinfoStatus())
                    && WorkTestInfoStatus.COMMITED.equals(entity.getCommitStatus())
                    && DateUtils.isSystemDate(testInfo.getTestDate())) {
                entity.setTestinfoStatus(WorkTestStatus.TESTING);
                reason.append(String.format("送測日由%s調整為%s，送測狀態變更為測試中<br/>", oldDate, newDate));
            }
            entity.setTestDate(testInfo.getTestDate());

            // ====================================
            // 處理系統通知 (送測日異動通知)
            // ====================================
            try {
                this.sysNotifyHelper.processForWorkTestEventForRelationUser(
                        testInfo,
                        NotifyType.MODIFY_SEND_TEST_DATE,
                        loginUser.getSid());
            } catch (Exception e) {
                log.error("執行系統通知失敗!" + e.getMessage(), e);
            }

        }
        // ====================================
        // 調整預計完成日
        // ====================================
        if (!testInfo.getEstablishDate().equals(entity.getEstablishDate())) {
            this.checkOperatePermission(stsService.modifyStatusByEstablishDate(entity, loginUser));
            Preconditions.checkArgument(!testInfo.getEstablishDate().before(DateUtils.convertToOriginOfDay(new Date())), "預計完成日不得小於系統日，請重新調整！");

            String oldDate = DateUtils.YYYY_MM_DD.print(entity.getEstablishDate().getTime());
            String newDate = DateUtils.YYYY_MM_DD.print(testInfo.getEstablishDate().getTime());
            reason.append(String.format("預計完成日由%s調整為%s<br/>", oldDate, newDate));

            entity.setEstablishDate(testInfo.getEstablishDate());
            notifyEvents.add(NotificationEventType.UPDATE_ESTABLISH_DATE);

            // ====================================
            // 處理系統通知 (送測日異動通知)
            // ====================================
            try {
                this.sysNotifyHelper.processForWorkTestEventForRelationUser(
                        testInfo,
                        NotifyType.MODIFY_SEND_TEST_ESTIMATED_DATE,
                        loginUser.getSid());
            } catch (Exception e) {
                log.error("執行系統通知失敗!" + e.getMessage(), e);
            }
        }

        // ====================================
        // 調整排定完成日
        // ====================================
        if (testInfo.getScheduleFinishDate() == null && entity.getScheduleFinishDate() != null) {
            this.checkOperatePermission(stsService.modifyStatusByFinishDate(entity, loginUser));

            reason.append(String.format("排定完成日調整為空值<br/>"));
            entity.setScheduleFinishDate(testInfo.getScheduleFinishDate());
        } else if (testInfo.getScheduleFinishDate() != null && !testInfo.getScheduleFinishDate().equals(entity.getScheduleFinishDate())) {
            this.checkOperatePermission(stsService.modifyStatusByFinishDate(entity, loginUser));

            if (entity.getScheduleFinishDate() != null) {
                String oldDate = DateUtils.YYYY_MM_DD.print(entity.getScheduleFinishDate().getTime());
                String newDate = DateUtils.YYYY_MM_DD.print(testInfo.getScheduleFinishDate().getTime());
                reason.append(String.format("排定完成日由%s調整為%s<br/>", oldDate, newDate));
            } else {
                String newDate = DateUtils.YYYY_MM_DD.print(testInfo.getScheduleFinishDate().getTime());
                reason.append(String.format("排定完成日調整為%s<br/>", newDate));
            }
            entity.setScheduleFinishDate(testInfo.getScheduleFinishDate());
        }

        // ====================================
        // 調整預計上線日
        // ====================================
        if (testInfo.getExpectOnlineDate() == null && entity.getExpectOnlineDate() != null) {
            this.checkOperatePermission(stsService.modifyStatusByOnlineDate(entity, loginUser));

            reason.append(String.format("預計上線日調整為空值<br/>"));
            entity.setExpectOnlineDate(testInfo.getExpectOnlineDate());
            notifyEvents.add(NotificationEventType.UPDATE_ONLINE_DATE);
        } else if (testInfo.getExpectOnlineDate() != null && !testInfo.getExpectOnlineDate().equals(entity.getExpectOnlineDate())) {
            this.checkOperatePermission(stsService.modifyStatusByOnlineDate(entity, loginUser));

            if (entity.getExpectOnlineDate() != null) {
                String oldDate = DateUtils.YYYY_MM_DD.print(entity.getExpectOnlineDate().getTime());
                String newDate = DateUtils.YYYY_MM_DD.print(testInfo.getExpectOnlineDate().getTime());
                reason.append(String.format("預計上線日由%s調整為%s<br/>", oldDate, newDate));
            } else {
                String newDate = DateUtils.YYYY_MM_DD.print(testInfo.getExpectOnlineDate().getTime());
                reason.append(String.format("預計上線日調整為%s<br/>", newDate));
            }
            entity.setExpectOnlineDate(testInfo.getExpectOnlineDate());
            notifyEvents.add(NotificationEventType.UPDATE_ONLINE_DATE);
        }

        if (StringUtils.isNotBlank(reason.toString())) {
            entity.setReadReason(WaitReadReasonType.ADJUST_DATE);
            entity.setReadUpdateDate(new Date());
            addHistoryRecord(testInfo, loginUser, WorkTestInfoHistoryBehavior.ADJUST_DATE, reason.toString());
            save(entity, loginUser);

            // ====================================
            // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
            // ====================================
            this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                    FormType.WORKTESTSIGNINFO,
                    testInfo.getSid(),
                    loginUser.getSid());

            // 通知QA人員事件
            if (CollectionUtils.isNotEmpty(notifyEvents)) {
                notifyQaUser(entity, loginUser.getSid(), notifyEvents);
            }
        }
    }

    /**
     * @param requireNo
     * @return
     */
    public List<WorkTestInfoTo> findByRequireNo(String requireNo) {
        List<WorkTestInfo> workTestInfos = this.testinfoDao.findByRequireNo(requireNo);

        List<WorkTestInfoTo> result = Lists.newArrayList();
        for (WorkTestInfo workTestInfo : workTestInfos) {
            if (Activation.INACTIVE.equals(workTestInfo.getStatus())) {
                continue;
            }
            result.add(this.convert2To(workTestInfo));
        }

        return result;
    }

    /**
     * 依據需求單號查詢
     * 
     * @param sourceNo
     */
    public List<WorkTestInfoTo> findByRequireNoAndCreateDepSids(String requireNo, List<Integer> depSids) {

        // ====================================
        // 檢查傳入參數為空
        // ====================================
        if (WkStringUtils.isEmpty(requireNo) || WkStringUtils.isEmpty(depSids)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 轉 org
        // ====================================
        List<Org> deps = Lists.newArrayList();
        for (Integer depSid : depSids) {
            Org dep = WkOrgCache.getInstance().findBySid(depSid);
            if (dep != null) {
                deps.add(dep);
            }
        }

        // ====================================
        // 查詢
        // ====================================
        List<WorkTestInfo> items = this.testinfoDao.findBySourceTypeAndStatusAndSourceNoAndCreateDepIn(
                WorkSourceType.TECH_REQUEST,
                Activation.ACTIVE,
                requireNo,
                deps);

        // ====================================
        // 轉To
        // ====================================
        List<WorkTestInfoTo> results = Lists.newArrayList();
        if (results != null) {
            for (WorkTestInfo item : items) {
                results.add(this.convert2To(item));
            }
        }
        return results;
    }

    /**
     * 計算完成或未完成的單據
     * 
     * @param sourceNo   需求單號
     * @param createDeps 過濾建立部門 (為空時不過濾)
     * @param isComplete true:完成 / false:未完成
     * @return
     */
    public Integer countByCompleteStatus(
            String sourceNo,
            List<Org> createDeps,
            boolean isComplete) {

        // 取得本次所有需要過濾的狀態
        List<WorkTestStatus> filterStatus = Lists.newArrayList(WorkTestStatus.values()).stream()
                .filter(ptStatus -> isComplete == ptStatus.isInFinish())
                .collect(Collectors.toList());

        // 需過濾建立部門
        if (WkStringUtils.notEmpty(createDeps)) {
            return this.testinfoDao.queryCountByCreateDepAndTestinfoStatus(
                    WorkSourceType.TECH_REQUEST,
                    sourceNo,
                    createDeps,
                    filterStatus);
        }

        // 僅檢查狀態
        return this.testinfoDao.queryCountByTestinfoStatus(
                WorkSourceType.TECH_REQUEST,
                sourceNo,
                filterStatus);
    }

    /**
     * 計算完成或未完成的單據
     * 
     * @param sourceNo   需求單號
     * @param createDeps 過濾建立部門 (為空時不過濾)
     * @param isComplete true:完成 / false:未完成
     * @return
     */
    public List<WorkTestInfo> queryByCompleteStatus(
            String sourceNo,
            List<Org> createDeps,
            boolean isComplete) {

        // 取得本次所有需要過濾的狀態
        List<WorkTestStatus> filterStatus = Lists.newArrayList(WorkTestStatus.values()).stream()
                .filter(ptStatus -> isComplete == ptStatus.isInFinish())
                .collect(Collectors.toList());

        // 需過濾建立部門
        return this.testinfoDao.queryByCreateDepAndTestinfoStatus(
                WorkSourceType.TECH_REQUEST,
                sourceNo,
                createDeps,
                filterStatus);
    }

    /**
     * 通知QA人員
     * 
     * @param workTestInfo (送測單
     * @param loginUserSid (登入者
     * @param events       (事件
     */
    private void notifyQaUser(WorkTestInfo workTestInfo, Integer loginUserSid, List<NotificationEventType> events) {
        for (NotificationEventType event : events) {
            notificationService.notifyQaUsersByStEvent(Lists.newArrayList(workTestInfo), event, loginUserSid);
        }
    }

    /**
     * 查詢所有曾經開過送測單的單位
     * 
     * @param compSid 歸屬公司
     * @return org sid list
     */
    public Set<Integer> findAllCreateDepSids(Integer compSid) {

        StringBuffer varname1 = new StringBuffer();
        varname1.append("SELECT DISTINCT dep_sid ");
        varname1.append("FROM   work_test_info wt ");
        varname1.append("WHERE  wt.status = 0 ");
        varname1.append("       AND wt.comp_sid = " + compSid + "  ");

        List<Integer> depSids = this.jdbcTemplate.queryForList(varname1.toString(), Integer.class);

        if (WkStringUtils.isEmpty(depSids)) {
            return Sets.newHashSet();
        }

        return depSids.stream()
                .collect(Collectors.toSet());
    }
}
