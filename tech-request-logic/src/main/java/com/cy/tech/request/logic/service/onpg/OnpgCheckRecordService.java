/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.onpg;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.helper.systemnotify.SysNotifyHelper;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.RequireReadRecordHelper;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.repository.onpg.WorkOnpgCheckRecordReplyRepo;
import com.cy.tech.request.repository.onpg.WorkOnpgCheckRecordRepo;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.onpg.WorkOnpgCheckRecord;
import com.cy.tech.request.vo.onpg.WorkOnpgCheckRecordReply;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgCheckRecordReplyType;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgHistoryBehavior;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.work.common.utils.WkJsoupUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.primitives.Longs;

import lombok.extern.slf4j.Slf4j;

/**
 * @author shaun
 */
@Component
@Slf4j
public class OnpgCheckRecordService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3189042516785793777L;
    @Autowired
    private RequireService reqService;
    @Autowired
    private OnpgService opService;
    @Autowired
    private OnpgAlertService opaService;
    @Autowired
    private OnpgShowService opsService;
    @Autowired
    private OnpgHistoryService ophService;
    @Autowired
    private WkJsoupUtils jsoupUtils;
    @Autowired
    private WorkOnpgCheckRecordRepo workOnpgCheckRecordRepo;
    @Autowired
    private WorkOnpgCheckRecordReplyRepo workOnpgCheckRecordReplyRepo;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private SysNotifyHelper sysNotifyHelper;
    @Autowired
    private transient RequireReadRecordHelper requireReadRecordHelper;

    /**
     * 單一查詢
     *
     * @param obj
     * @return
     */
    @Transactional(readOnly = true)
    public WorkOnpgCheckRecord findBySid(WorkOnpgCheckRecord obj) {
        return workOnpgCheckRecordRepo.findBySid(obj.getSid());
    }

    /**
     * 單一查詢
     *
     * @param obj
     * @return
     */
    @Transactional(readOnly = true)
    public WorkOnpgCheckRecordReply findBySid(WorkOnpgCheckRecordReply obj) {
        return workOnpgCheckRecordReplyRepo.findBySid(obj.getSid());
    }

    @Transactional(readOnly = true)
    public List<WorkOnpgCheckRecord> findCheckRecord(WorkOnpg onpg) {
        try {
            onpg.getCheckRecords().size();
        } catch (LazyInitializationException e) {
            // log.debug("findCheckRecord lazy init error :" + e.getMessage(), e);
            onpg.setCheckRecords(workOnpgCheckRecordRepo.findByOnpg(onpg));
        }
        return onpg.getCheckRecords();
    }

    /**
     * 尋找原型確認對應的回覆內容
     *
     * @param checkRecord
     * @return
     */
    @Transactional(readOnly = true)
    public List<WorkOnpgCheckRecordReply> findCheckRecordReplys(WorkOnpgCheckRecord checkRecord) {
        if (checkRecord == null || Strings.isNullOrEmpty(checkRecord.getSid())) {
            return Lists.newArrayList();
        }
        try {
            checkRecord.getCheckRecordReplys().size();
        } catch (LazyInitializationException e) {
            // log.debug("findCheckRecordReplys lazy init error :" + e.getMessage(), e);
            checkRecord.setCheckRecordReplys(workOnpgCheckRecordReplyRepo.findByCheckRecordOrderByUpdateDateDesc(checkRecord));
        }
        return checkRecord.getCheckRecordReplys();
    }

    public WorkOnpgCheckRecord createEmptyCheckRecord(WorkOnpg onpg, WorkOnpgCheckRecordReplyType replyType, User executor) {
        WorkOnpgCheckRecord ck = new WorkOnpgCheckRecord();
        ck.setOnpg(onpg);
        ck.setOnpgNo(onpg.getOnpgNo());
        ck.setSourceType(onpg.getSourceType());
        ck.setSourceSid(onpg.getSourceSid());
        ck.setSourceNo(onpg.getSourceNo());
        ck.setDep(orgService.findBySid(executor.getPrimaryOrg().getSid()));
        ck.setPerson(executor);
        ck.setDate(new Date());
        ck.setUpdateDate(new Date());
        ck.setHistory(ophService.createEmptyHistory(onpg, executor));
        ck.getHistory().setBehavior(WorkOnpgHistoryBehavior.CHECK_RECORD);
        ck.setContentCss("本次檢查完成事項如下：<P/>" + onpg.getContentCss());
        ck.setReplyType(replyType);
        return ck;
    }

    /**
     * 檢查記錄(一般回覆、GM回覆、QA回覆) - 確認存檔 （新增）
     * 
     * @param editCheckRecord
     * @param executor
     * @param cateSmallSid
     * @throws IllegalAccessException
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveByNewCheckRecord(WorkOnpgCheckRecord editCheckRecord, User executor, String cateSmallSid) throws IllegalAccessException {
        // ====================================
        // 權限檢核
        // ====================================
        if (this.checkSaveCheckRecordPermission(editCheckRecord, executor, false)) {
            throw new IllegalAccessException("無權限進行操作");
        }

        // ====================================
        // 儲存回覆
        // ====================================
        editCheckRecord.setContent(jsoupUtils.clearCssTag(editCheckRecord.getContentCss()));
        WorkOnpgCheckRecord workOnpgCheckRecord = workOnpgCheckRecordRepo.save(editCheckRecord);

        // ====================================
        // 異動主檔
        // ====================================
        WorkOnpg onpg = opService.copyOnpg(editCheckRecord.getOnpg());
        this.setOnpgStatusByNewCR(onpg, editCheckRecord);
        onpg.setReadReason(WaitReadReasonType.ONPG_CHECK_RECORD);
        onpg.setReadUpdateDate(new Date());
        ophService.createCheckRecordHistory(workOnpgCheckRecord, editCheckRecord.getPerson());
        opService.save(onpg, editCheckRecord.getPerson());
        ophService.sortHistory(onpg);

        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.WORKONPG,
                onpg.getSid(),
                executor.getSid());

        // ====================================
        // 處理系統通知 for ON程式單 - 新增/編輯檢查訊息 (GM回覆、QA回覆、一般回覆)
        // ====================================
        try {
            this.sysNotifyHelper.processForOnpgAddCheckRecord(
                    onpg.getSourceSid(),
                    onpg.getTheme(),
                    editCheckRecord,
                    executor.getSid());
        } catch (Exception e) {
            log.error("執行系統通知失敗!" + e.getMessage(), e);
        }

        // notifyService.createNotifyByOnpgCheck(editCheckRecord, executor.getSid(), cateSmallSid);
    }

    private void setOnpgStatusByNewCR(WorkOnpg onpg, WorkOnpgCheckRecord checkRecord) {
        WorkOnpgStatus opStatus = onpg.getOnpgStatus();
        switch (checkRecord.getReplyType()) {
        case CHECK_RECORD:
            if (opStatus.equals(WorkOnpgStatus.ALREADY_ON)
                    || opStatus.equals(WorkOnpgStatus.CHECK_COMPLETE)) {
                onpg.setOnpgStatus(WorkOnpgStatus.CHECK_REPLY);
            }
            break;
        case GM_REPLY:
        case QA_REPLY:
        case GENERAL_REPLY:
            if (opStatus.equals(WorkOnpgStatus.ALREADY_ON)) {
                onpg.setOnpgStatus(WorkOnpgStatus.CHECK_REPLY);
            }
        }
    }

    private boolean checkSaveCheckRecordPermission(WorkOnpgCheckRecord checkRecord, User executor, boolean isEdit) {
        boolean isEmptyReply = true;
        if (isEdit) {
            isEmptyReply = workOnpgCheckRecordReplyRepo.findByCheckRecordOrderByUpdateDateDesc(checkRecord).isEmpty();
        }
        switch (checkRecord.getReplyType()) {
        case CHECK_RECORD:
            return opsService.disableCheckRecord(
                    reqService.findByReqSid(checkRecord.getSourceSid()),
                    opService.findByOnpgNo(checkRecord.getOnpgNo()),
                    executor)
                    || !isEmptyReply;
        case GM_REPLY:
            return opsService.disableGMReply(
                    reqService.findByReqSid(checkRecord.getSourceSid()),
                    opService.findByOnpgNo(checkRecord.getOnpgNo()),
                    executor)
                    || !isEmptyReply;
        case QA_REPLY:
            return opsService.disableQAReply(
                    reqService.findByReqSid(checkRecord.getSourceSid()),
                    opService.findByOnpgNo(checkRecord.getOnpgNo()),
                    executor)
                    || !isEmptyReply;
        case GENERAL_REPLY:
            return opsService.disableGeneralReply(
                    reqService.findByReqSid(checkRecord.getSourceSid()),
                    opService.findByOnpgNo(checkRecord.getOnpgNo()),
                    executor)
                    || !isEmptyReply;
        default:
            return false;
        }
    }

    /**
     * 檢查記錄(一般回覆、GM回覆、QA回覆) - 確認存檔 （編輯）
     * 
     * @param editCheckRecord
     * @param executor
     * @param isAddNotify
     * @param cateSmallSid
     * @throws IllegalAccessException
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveByEditCheckRecord(
            WorkOnpgCheckRecord editCheckRecord,
            User executor,
            Boolean isAddNotify,
            String cateSmallSid) throws IllegalAccessException {

        // ====================================
        // 權限檢核
        // ====================================
        if (this.checkSaveCheckRecordPermission(editCheckRecord, executor, true)) {
            throw new IllegalAccessException("無權限進行操作");
        }

        // ====================================
        // UPDATE
        // ====================================
        editCheckRecord.setContent(jsoupUtils.clearCssTag(editCheckRecord.getContentCss()));
        editCheckRecord.setUpdateDate(new Date());
        ophService.update(editCheckRecord.getHistory(), executor);

        WorkOnpg onpg = opService.copyOnpg(editCheckRecord.getOnpg());
        onpg.setReadUpdateDate(new Date());
        workOnpgCheckRecordRepo.save(editCheckRecord);
        opService.save(onpg, executor);
        ophService.sortHistory(onpg);

        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.WORKONPG,
                onpg.getSid(),
                executor.getSid());

        // ====================================
        // 處理系統通知 for ON程式單 - 新增/編輯檢查訊息 (GM回覆、QA回覆、一般回覆)
        // ====================================
        if (isAddNotify) {
            try {
                this.sysNotifyHelper.processForOnpgAddCheckRecord(
                        onpg.getSourceSid(),
                        onpg.getTheme(),
                        editCheckRecord,
                        executor.getSid());
            } catch (Exception e) {
                log.error("執行系統通知失敗!" + e.getMessage(), e);
            }
        }
    }

    public WorkOnpgCheckRecordReply createEmptyCheckRecordReply(WorkOnpgCheckRecord ck, User executor) {
        WorkOnpgCheckRecordReply ckrReply = new WorkOnpgCheckRecordReply();
        ckrReply.setSourceType(ck.getSourceType());
        ckrReply.setOnpg(ck.getOnpg());
        ckrReply.setOnpgNo(ck.getOnpgNo());
        ckrReply.setSourceSid(ck.getSourceSid());
        ckrReply.setSourceNo(ck.getSourceNo());
        ckrReply.setCheckRecord(ck);
        ckrReply.setDep(orgService.findBySid(executor.getPrimaryOrg().getSid()));
        ckrReply.setPerson(executor);
        ckrReply.setDate(new Date());
        ckrReply.setUpdateDate(new Date());
        ckrReply.setHistory(ophService.createEmptyHistory(ck.getOnpg(), executor));
        ckrReply.getHistory().setBehavior(WorkOnpgHistoryBehavior.CHECK_RECORD_REPLY);
        return ckrReply;
    }

    /**
     * 檢查記錄-回覆 (新增)
     *
     * @param ckrReply
     * @param execUserSid
     * @param cateSmallSid
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveByNewCheckRecordReply(WorkOnpgCheckRecordReply ckrReply, Integer execUserSid, String cateSmallSid) {
        ckrReply.setContent(jsoupUtils.clearCssTag(ckrReply.getContentCss()));
        ckrReply.setSid(workOnpgCheckRecordReplyRepo.save(ckrReply).getSid());
        ophService.createCheckRecordReplyHistory(ckrReply, ckrReply.getPerson());
        opaService.carateReplyAlert(ckrReply, execUserSid, cateSmallSid);

        WorkOnpgCheckRecord ckr = ckrReply.getCheckRecord();
        this.findCheckRecordReplys(ckr).add(ckrReply);
        this.sortCheckRecordReplys(ckrReply.getCheckRecord());

        WorkOnpg onpg = ckrReply.getOnpg();
        onpg.setReadReason(WaitReadReasonType.ONPG_CHECK_REPLY);
        onpg.setReadUpdateDate(new Date());
        opService.save(onpg, ckrReply.getPerson());
        ophService.sortHistory(onpg);

        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.WORKONPG,
                onpg.getSid(),
                execUserSid);

        // ====================================
        // 處理系統通知 for ON程式單 - 新增/編輯回覆檢查訊息 (GM回覆的回覆、QA回覆的回覆)
        // ====================================
        try {
            this.sysNotifyHelper.processForOnpgAddCheckRecordReply(
                    onpg.getSourceSid(),
                    onpg.getTheme(),
                    ckrReply,
                    execUserSid);
        } catch (Exception e) {
            log.error("執行系統通知失敗!" + e.getMessage(), e);
        }

    }

    /**
     * 檢查記錄-回覆 (編輯)
     *
     * @param editCkrReply
     * @param executor
     * @param isAddNotify
     * @param cateSmallSid
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveByEditCheckRecordReply(WorkOnpgCheckRecordReply editCkrReply, User executor,
            Boolean isAddNotify, String cateSmallSid) {
        editCkrReply.setContent(jsoupUtils.clearCssTag(editCkrReply.getContentCss()));
        editCkrReply.setUpdateDate(new Date());
        ophService.update(editCkrReply.getHistory(), executor);
        workOnpgCheckRecordReplyRepo.save(editCkrReply);
        WorkOnpg onpg = editCkrReply.getOnpg();
        opService.save(onpg, executor);

        // 重新排序回覆
        this.sortCheckRecordReplys(editCkrReply.getCheckRecord());

        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.WORKONPG,
                onpg.getSid(),
                executor.getSid());

        // ====================================
        // 處理系統通知 for ON程式單 - 新增/編輯回覆檢查訊息 (GM回覆的回覆、QA回覆的回覆)
        // ====================================
        if (isAddNotify) {
            try {
                this.sysNotifyHelper.processForOnpgAddCheckRecordReply(
                        onpg.getSourceSid(),
                        onpg.getTheme(),
                        editCkrReply,
                        executor.getSid());
            } catch (Exception e) {
                log.error("執行系統通知失敗!" + e.getMessage(), e);
            }
        }

    }

    /**
     * 重新排序回覆
     *
     * @param pc
     */
    private void sortCheckRecordReplys(WorkOnpgCheckRecord checkRecord) {
        List<WorkOnpgCheckRecordReply> replys = this.findCheckRecordReplys(checkRecord);
        Ordering<WorkOnpgCheckRecordReply> byCkrReplyUpdateOrdering = new Ordering<WorkOnpgCheckRecordReply>() {
            @Override
            public int compare(WorkOnpgCheckRecordReply left, WorkOnpgCheckRecordReply right) {
                return Longs.compare(right.getUpdateDate().getTime(), left.getUpdateDate().getTime());
            }
        };
        Collections.sort(replys, byCkrReplyUpdateOrdering);
    }

}
