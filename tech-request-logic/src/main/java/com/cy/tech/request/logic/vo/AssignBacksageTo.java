/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.vo;

import com.cy.tech.request.vo.enums.RequireStatusType;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 後台分派查詢物件
 *
 * @author shaun
 */
@EqualsAndHashCode(of = "requireSid")
@AllArgsConstructor
@ToString
@Data
public class AssignBacksageTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5184057821778577894L;
    private String requireSid;
    private String requireNo;
    private RequireStatusType reqType;
    private Date createDt;
    private Integer sourceOrgSid;
    /** 後台異動是否成功處理 */
    private boolean sucess;

}
