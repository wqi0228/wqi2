/**
 * 
 */
package com.cy.tech.request.logic.service.syncmms;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.syncmms.helper.SyncMmsParamHelper;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.work.client.syncmms.WorkSyncMmsClient;
import com.cy.work.client.syncmms.to.requiredata.SyncMmsSyncRequireDataModel;
import com.cy.work.client.syncmms.to.token.SyncMmsTokenModel;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.alert.TechRequestAlertException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Service
@Slf4j
public class SyncMmsRequireDataLogic {

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private SearchService searchService;
    @Autowired
    private WorkSyncMmsClient workSyncMmsClient;
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private JdbcTemplate jdbcTemplate;

    // ========================================================================
    // 變數
    // ========================================================================
    /**
     * 
     */
    private final String processName = "【MMS 系統介接-同步需求單資料】";

    /**
     * 最大傳送筆數
     */
    private final Integer MAX_SEND_COUNT = 1000;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 執行
     * 
     * @param allSend     是否全部重傳 (為 false 時，會和MMS要最後資料時間)
     * @param loginCompID 登入公司別 (若有的話)
     * @throws SystemOperationException 錯誤時拋出
     */
    public int process(boolean allSend, String loginCompID) throws SystemOperationException {

        // ====================================
        // 取得 token
        // ====================================
        SyncMmsTokenModel syncMmsTokenModel = null;
        try {
            syncMmsTokenModel = this.workSyncMmsClient.getToken(
                    SyncMmsParamHelper.getInstance().getAPIHostDomain(),
                    SyncMmsParamHelper.getInstance().getAPIGetTokenClientID(),
                    SyncMmsParamHelper.getInstance().getAPIGetTokenClientSecret(),
                    loginCompID);
        } catch (SystemOperationException e) {
            throw e;
        } catch (Exception e) {
            String errorMessage = processName + "取得 token 失敗! [" + e.getMessage() + "]";
            log.error(errorMessage, e);
            throw new TechRequestAlertException(errorMessage, loginCompID);
        }

        log.info(processName + "：取得 token 成功!");

        // ====================================
        // 取得最後異動時間
        // ====================================
        Date leastUpdateTime = null;
        // 為 all send 時，模擬第一次同步狀況 全送 [進行中、已完成]
        if (!allSend) {
            try {
                leastUpdateTime = this.workSyncMmsClient.findRequrieDataLeastUpdateTime(
                        SyncMmsParamHelper.getInstance().getAPIHostDomain(),
                        SyncMmsParamHelper.getInstance().getAPIPortal(),
                        syncMmsTokenModel);

                String leastUpdateTimeStr = "";
                if (leastUpdateTime != null) {
                    leastUpdateTimeStr = WkDateUtils.formatDate(leastUpdateTime, WkDateUtils.YYYY_MM_DD_HH24_mm_ss);
                }

                log.info(processName + "：取得最後異動時間成功 leastUpdateTime ->[" + leastUpdateTimeStr + "]");

            } catch (SystemOperationException e) {
                throw e;
            } catch (Exception e) {
                String errorMessage = processName + "取得最後異動時間失敗! [" + e.getMessage() + "]";
                log.error(errorMessage, e);
                throw new TechRequestAlertException(errorMessage, loginCompID);
            }
        }

        // ====================================
        // 取得需要傳送的資料
        // ====================================
        // 查詢
        List<SyncMmsSyncRequireDataModel> requireDatas = this.prepareData(leastUpdateTime);
        if (WkStringUtils.isEmpty(requireDatas)) {
            log.info(processName + "：目前沒有需要同步的資料!");
            return 0;
        }

        // 將結果均分，避免每次傳送筆數過多
        List<List<SyncMmsSyncRequireDataModel>> sendLists = WkCommonUtils.averageAssign(
                requireDatas,
                MAX_SEND_COUNT);

        // ====================================
        //
        // ====================================
        int sendSeq = 0;
        for (List<SyncMmsSyncRequireDataModel> currSendRequireDatas : sendLists) {
            sendSeq += 1;
            long startTime = System.currentTimeMillis();

            this.workSyncMmsClient.syncRequireData(
                    SyncMmsParamHelper.getInstance().getAPIHostDomain(),
                    SyncMmsParamHelper.getInstance().getAPIPortal(),
                    syncMmsTokenModel, 
                    currSendRequireDatas);

            // WkFileUtils.getInstance().writeFileByText("c:/temp", "SyncMmsRequireData", startTime + "", requestDataJsonStr);

            String info = processName + (sendLists.size() > 1 ? ("[第" + sendSeq + "次]") : "") + "[" + currSendRequireDatas.size() + "筆]";
            log.info(WkCommonUtils.prepareCostMessage(startTime, info));

            if (SyncMmsParamHelper.getInstance().isShowMmsSyncDebugLog()) {
                if (WkStringUtils.notEmpty(currSendRequireDatas.size())) {
                    String requestDataJsonStr = WkJsonUtils.getInstance().toPettyJson(currSendRequireDatas);
                    log.debug("發送資料：\r\n" + requestDataJsonStr);
                }
            }
        }

        return requireDatas.size();
    }

    /**
     * @param leastUpdateTime
     * @return
     */
    private List<SyncMmsSyncRequireDataModel> prepareData(Date leastUpdateTime) {

        List<Object> paramaters = Lists.newArrayList();

        // ====================================
        // 兜組 SQL
        // ====================================
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT tr.require_no                               as requireNo, ");
        sql.append("       tid.field_content                           as requireTheme_src, ");
        sql.append("       tr.status                                   as status, ");
        sql.append("       tr.require_status                           as requireStatus, ");
        sql.append("       DATE_FORMAT(tr.create_dt, \"%Y-%m-%d %T\")  as createDate, ");
        sql.append("       DATE_FORMAT(tr.update_dt, \"%Y-%m-%d %T\")  as updateDate ");
        sql.append("FROM   tr_require tr ");

        sql.append("       INNER JOIN (SELECT tid.require_sid, ");
        sql.append("                          tid.field_content ");
        sql.append("                   FROM   tr_index_dictionary tid ");
        sql.append("                   WHERE  1 = 1 ");
        sql.append("                          AND tid.field_name = '主題') AS tid ");
        sql.append("               ON tr.require_sid = tid.require_sid ");

        // 排除草稿
        sql.append("WHERE  tr.require_status != '" + RequireStatusType.DRAFT + "' ");

        // 第一次資料同步邏輯
        // 僅同步進行中、已完成但未結案的單據
        if (leastUpdateTime == null) {
            String typeString = Lists.newArrayList(
                    RequireStatusType.PROCESS,
                    RequireStatusType.COMPLETED).stream()
                    .map(type -> type.name())
                    .collect(Collectors.joining("','", "'", "'"));

            sql.append(" AND  tr.status = 0 ");
            sql.append(" AND  tr.require_status IN (" + typeString + ")");
        }

        // 取得上次異動時間以後的資料
        if (leastUpdateTime != null) {
            // 減1秒
            leastUpdateTime = new Date(leastUpdateTime.getTime() - 1000);

            sql.append(" AND  (  ");
            sql.append("         (tr.update_dt is null AND tr.create_dt > ?) ");
            sql.append("            OR tr.update_dt > ? ");
            sql.append("      )");

            paramaters.add(leastUpdateTime);
            paramaters.add(leastUpdateTime);

        }

        // 以防萬一
        sql.append("GROUP BY tr.require_no ");

        if (SyncMmsParamHelper.getInstance().isShowMmsSyncDebugLog()) {
            log.debug("MMS需求單列表同步："
                    + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql.toString()) + "\r\n】"
                    + "\r\nPARAMs:【\r\n" + com.cy.work.common.utils.WkJsonUtils.getInstance().toPettyJson(paramaters) + "\r\n】"
                    + "\r\n");
        }

        // ====================================
        // 查詢
        // ====================================
        long startTime = System.currentTimeMillis();
        List<Map<String, Object>> dbRowDataMaps = this.jdbcTemplate.queryForList(sql.toString(), paramaters.toArray());

        log.debug(WkCommonUtils.prepareCostMessage(startTime, "查詢異動需求單[" + dbRowDataMaps.size() + "]筆"));
        if (WkStringUtils.isEmpty(dbRowDataMaps)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 依據傳送格式準備資料
        // ====================================
        startTime = System.currentTimeMillis();
        List<SyncMmsSyncRequireDataModel> requireDatas = Lists.newArrayList();
        for (Map<String, Object> rowDataMap : dbRowDataMaps) {
            // 初始化資料容器
            SyncMmsSyncRequireDataModel requireData = new SyncMmsSyncRequireDataModel();
            requireDatas.add(requireData);

            // 單號
            requireData.setRequireNo(rowDataMap.get("requireNo") + "");
            if (WkStringUtils.isEmpty(requireData.getRequireNo())) {
                continue;
            }
            // 主題
            requireData.setTheme(this.searchService.combineFromJsonStr(rowDataMap.get("requireTheme_src") + ""));
            if (WkStringUtils.isEmpty(requireData.getTheme())) {
                continue;
            }
            // 時間
            String werpDateTimeStr = rowDataMap.get("updateDate") == null ? rowDataMap.get("createDate") + ""
                    : rowDataMap.get("updateDate") + "";
            requireData.setChangeDate(werpDateTimeStr);
            if (WkStringUtils.isEmpty(werpDateTimeStr)) {
                continue;
            }

            // 狀態
            // 主單已停用（tr.status=1)，-> 狀態為 0 -> 無法繼續附掛
            // 其餘依據需求單處理進度狀態判斷 (tr.require_status)
            Integer status = 0;
            if (WkCommonUtils.compareByStr("0", rowDataMap.get("status"))) {
                RequireStatusType requireStatusType = RequireStatusType.safeValueOf(rowDataMap.get("requireStatus") + "");
                if (requireStatusType != null) {
                    status = requireStatusType.getSyncMMSStatus();
                }
            }
            requireData.setStatus(status);
        }
        // log.debug(WkCommonUtils.prepareCostMessage(startTime, "組裝異動需求單"));

        return requireDatas;

    }

}
