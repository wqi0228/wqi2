/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.cy.tech.request.logic.vo.RequireConfirmDepVO;
import com.cy.tech.request.vo.enums.RequireFinishCodeType;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkOrgUtils;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 已分派單據查詢頁面顯示vo (search07.xhtml)
 *
 * @author jason_h
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Search07View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 440535842632133443L;

    private int resultIndex;

    /** 緊急度 */
    private UrgencyType urgency;
    /** 立單日期 */
    private Date createdDate;
    /** 異動日期 */
    private Date updatedDate;
    /** 需求類別 */
    private String bigName;
    /** 中類 */
    private String middleName;
    /** 小類" */
    private String smallName;
    /** 需求單位 */
    private Integer createDep;
    /** 需求人員 */
    private Integer createdUser;
    /** 技術事業群主管是否審核 */
    private Boolean hasTechManagerSign;
    /** 需求暫緩碼 */
    private Boolean requireSuspendedCode;
    /** 完成碼 */
    private RequireFinishCodeType finishCode;
    /** 期望完成日 */
    private Date hopeDate;

    /** 被分派單位 */
    List<Integer> assignDepSids;
    // private String assignedDeptStr;
    // private String assignedDeptTooltip;

    public String getAssignedDeptStr() {
        //
        return WkOrgUtils.findNameBySid(assignDepSids, "、", false, true);
    }

    public String getAssignedDeptTooltip() {
        //
        return WkOrgUtils.prepareDepsNameByTreeStyle(
                assignDepSids,
                20);
    }

    private boolean assignDep;
    /** 分派日 */
    private Date assignDate;
    /** 被分派人員 */
    private String assignedUserStr;
    /** 執行天數 */
    private int execDays;
    /** 本地端連結網址 */
    private String localUrlLink;
    /** 統整人員 */
    private Integer inteStaffUser;

    /**
     * 需求確認部門資訊
     */
    private List<RequireConfirmDepVO> requireConfirmDepVOs;
    /**
     * 執行進度
     */
    private String reqConfirmProgStatus;

    private String readType;
}
