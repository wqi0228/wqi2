/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.repository.template.TemplateBaseDataFieldRepository;
import com.cy.tech.request.vo.template.TemplateBaseDataField;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 基礎欄位模版
 *
 * @author shaun
 */
@Component
public class TemplateBaseFieldService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 337942032700795447L;

    @Autowired
    private ReqularPattenUtils reqularUtils;

    @Autowired
    private TemplateBaseDataFieldRepository bdfDao;

    /**
     * 搜尋欄位名稱(LIKE)
     *
     * @param searchText
     * @return
     */
    @Transactional(readOnly = true)
    public List<TemplateBaseDataField> findByFazzyText(String searchText) {
        if (Strings.isNullOrEmpty(searchText)) {
            return findAll();
        }
        String realSearchText = "%" + reqularUtils.replaceIllegalSqlLikeStr(searchText) + "%";
        return bdfDao.findByFuzzyText(realSearchText);
    }

    /**
     * 搜尋全部
     *
     * @return
     */
    private List<TemplateBaseDataField> findAll() {
        return bdfDao.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public void save(TemplateBaseDataField bdf) {
        bdfDao.save(bdf);
    }

}
