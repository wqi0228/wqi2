/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.othset;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.repository.require.othset.OthSetAlreadyReplyRepo;
import com.cy.tech.request.repository.require.othset.OthSetReplyRepo;
import com.cy.tech.request.vo.enums.OthSetHistoryBehavior;
import com.cy.tech.request.vo.require.othset.OthSet;
import com.cy.tech.request.vo.require.othset.OthSetAlreadyReply;
import com.cy.tech.request.vo.require.othset.OthSetReply;
import com.cy.work.common.utils.WkJsoupUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.primitives.Longs;
import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shaun
 */
@Component
public class OthSetReplyService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3542531455871797099L;
    @Autowired
    private OthSetService osService;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private OthSetShowService ossService;
    @Autowired
    private OthSetAlertService osaService;
    @Autowired
    private OthSetHistoryService oshService;
    @Autowired
    private WkJsoupUtils jsoupUtils;
    @Autowired
    private OthSetReplyRepo replyDao;
    @Autowired
    private OthSetAlreadyReplyRepo aReplyDao;

    @Transactional(readOnly = true)
    public List<OthSetReply> findReplys(OthSet othset) {
        try {
            othset.getReplys().size();
        } catch (LazyInitializationException e) {
            //log.debug("findReplys lazy init error :" + e.getMessage(), e);
            othset.setReplys(replyDao.findByOthset(othset));
        }
        return othset.getReplys();
    }

    /**
     * 尋找原型確認對應的回覆內容
     *
     * @param reply
     * @return
     */
    @Transactional(readOnly = true)
    public List<OthSetAlreadyReply> findAlreadyReplys(OthSetReply reply) {
        if (reply == null || Strings.isNullOrEmpty(reply.getSid())) {
            return Lists.newArrayList();
        }
        try {
            reply.getAlreadyReplys().size();
        } catch (LazyInitializationException e) {
            //log.debug("findAlreadyReplys lazy init error :" + e.getMessage(), e);
            reply.setAlreadyReplys(aReplyDao.findByReplyOrderByUpdateDateDesc(reply));
        }
        return reply.getAlreadyReplys();
    }

    public OthSetReply createEmptyReply(OthSet othset, User executor) {
        OthSetReply reply = new OthSetReply();
        reply.setOthset(othset);
        reply.setOsNo(othset.getOsNo());
        reply.setRequire(othset.getRequire());
        reply.setRequireNo(othset.getRequireNo());
        reply.setDep(orgService.findBySid(executor.getPrimaryOrg().getSid()));
        reply.setPerson(executor);
        reply.setDate(new Date());
        reply.setUpdateDate(new Date());
        reply.setHistory(oshService.createEmptyHistory(othset, executor));
        reply.getHistory().setBehavior(OthSetHistoryBehavior.REPLY);
        return reply;
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveByNewReply(OthSetReply editReply) throws IllegalAccessException {
        if (ossService.disableReply(osService.findByOsNo(editReply.getOsNo()), editReply.getPerson())) {
            throw new IllegalAccessException("無權限進行操作");
        }
        editReply.setContent(jsoupUtils.clearCssTag(editReply.getContentCss()));
        OthSetReply nR = replyDao.save(editReply);
        OthSet othset = editReply.getOthset();
        oshService.createReplyHistory(nR, editReply.getPerson());
        osService.save(othset, editReply.getPerson());
        oshService.sortHistory(othset);
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveByEditReply(OthSetReply editReply, User executor) throws IllegalAccessException {
        if (ossService.disableReply(osService.findByOsNo(editReply.getOsNo()), executor)
                || aReplyDao.findByReplyOrderByUpdateDateDesc(editReply).size() > 0) {
            throw new IllegalAccessException("無權限進行操作");
        }
        editReply.setContent(jsoupUtils.clearCssTag(editReply.getContentCss()));
        editReply.setUpdateDate(new Date());
        oshService.update(editReply.getHistory(), executor);
        OthSet onpg = editReply.getOthset();
        replyDao.save(editReply);
        osService.save(onpg, executor);
        oshService.sortHistory(onpg);
    }

    public OthSetAlreadyReply createEmptyAlreadyReply(OthSetReply reply, User executor) {
        OthSetAlreadyReply alreadyReply = new OthSetAlreadyReply();
        alreadyReply.setOthset(reply.getOthset());
        alreadyReply.setOsNo(reply.getOsNo());
        alreadyReply.setRequire(reply.getRequire());
        alreadyReply.setRequireNo(reply.getRequireNo());
        alreadyReply.setReply(reply);
        alreadyReply.setDep(orgService.findBySid(executor.getPrimaryOrg().getSid()));
        alreadyReply.setPerson(executor);
        alreadyReply.setDate(new Date());
        alreadyReply.setUpdateDate(new Date());
        alreadyReply.setHistory(oshService.createEmptyHistory(reply.getOthset(), executor));
        alreadyReply.getHistory().setBehavior(OthSetHistoryBehavior.REPLY_AND_REPLY);
        return alreadyReply;
    }

    /**
     * 儲存檢查記錄回覆
     *
     * @param alreadyReply
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveByNewAlreadyReply(OthSetAlreadyReply alreadyReply) {
        alreadyReply.setContent(jsoupUtils.clearCssTag(alreadyReply.getContentCss()));
        alreadyReply.setSid(aReplyDao.save(alreadyReply).getSid());
        oshService.createAlreadyHistory(alreadyReply, alreadyReply.getPerson());
        osaService.carateAlreadyReplyAlert(alreadyReply);

        OthSetReply reply = alreadyReply.getReply();
        this.findAlreadyReplys(reply).add(alreadyReply);
        this.sortAlreadyReplys(alreadyReply.getReply());

        OthSet othset = alreadyReply.getOthset();
        oshService.sortHistory(othset);
    }

    /**
     * 儲存編輯回覆
     *
     * @param editAlreadyReply
     * @param executor
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveByEditAlreadyReply(OthSetAlreadyReply editAlreadyReply, User executor) {
        editAlreadyReply.setContent(jsoupUtils.clearCssTag(editAlreadyReply.getContentCss()));
        editAlreadyReply.setUpdateDate(new Date());
        oshService.update(editAlreadyReply.getHistory(), executor);
        aReplyDao.save(editAlreadyReply);
        this.sortAlreadyReplys(editAlreadyReply.getReply());
    }

    /**
     * 重新排序回覆
     *
     * @param reply
     */
    private void sortAlreadyReplys(OthSetReply reply) {
        List<OthSetAlreadyReply> replys = this.findAlreadyReplys(reply);
        Ordering<OthSetAlreadyReply> byAlreadyReplyUpdateOrdering = new Ordering<OthSetAlreadyReply>() {
            @Override
            public int compare(OthSetAlreadyReply left, OthSetAlreadyReply right) {
                return Longs.compare(right.getUpdateDate().getTime(), left.getUpdateDate().getTime());
            }
        };
        Collections.sort(replys, byAlreadyReplyUpdateOrdering);
    }

}
