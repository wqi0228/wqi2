/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import com.cy.tech.request.vo.enums.ForwardType;
import com.cy.tech.request.vo.require.AlertInboxSendGroup;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.logic.lib.manager.to.WorkCommonReadRecordTo;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 寄件備份頁面顯示vo (home03.xhtml)
 *
 * @author jason_h
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Home03View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8026839751971305883L;
    @Getter
    @Setter
    private String requireSid;
    /** 寄件備份 */
    private AlertInboxSendGroup inboxSendGroup;
    /** 緊急度 */
    private UrgencyType urgency;
    /** 部 */
    private Boolean hasForwardDep;
    /** 個 */
    private Boolean hasForwardMember;
    /** 關 */
    private Boolean hasLink;
    /** 寄發類型 */
    private ForwardType forwardType;
    /** 寄發至 */
    private String sendTo;
    /** 寄發時間 */
    private Date createdDate;
    /** 類型 */
    private String bigName;
    /** 需求單-異動日期 */
    private Date requireUpdatedDate;
    /** 本地端連結網址 */
    private String localUrlLink;
    /** 閱讀記錄清單 */
    @Getter
    @Setter
    private List<WorkCommonReadRecordTo> readRecordTos = Lists.newArrayList();
}
