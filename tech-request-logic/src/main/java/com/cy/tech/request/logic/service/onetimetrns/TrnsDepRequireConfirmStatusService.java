/**
 * 
 */
package com.cy.tech.request.logic.service.onetimetrns;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepPanelActionService;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepService;
import com.cy.tech.request.logic.vo.onetimetrns.TrnsDepRequireConfirmStatusVO;
import com.cy.tech.request.vo.enums.ReqConfirmDepProgStatus;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireConfirmDep;
import com.cy.work.common.constant.WkConstants;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu 特殊需求轉檔 - 依據清單, 異動『需求確認單位』的『確認狀態』
 */
@Slf4j
@Service
public class TrnsDepRequireConfirmStatusService {

	// ========================================================================
	// 服務元件區
	// ========================================================================
	@Autowired
	private transient RequireService requireService;
	@Autowired
	private transient RequireConfirmDepService requireConfirmDepService;
	@Autowired
	private transient RequireConfirmDepPanelActionService requireConfirmDepPanelActionService;

	// ========================================================================
	// 方法區
	// ========================================================================

	public String process(
	        List<TrnsDepRequireConfirmStatusVO> trnsDataVO) throws UserMessageException {

		// ====================================
		// 檢核
		// ====================================
		if (WkStringUtils.isEmpty(trnsDataVO)) {
			throw new UserMessageException("未傳入轉檔資料!");
		}

		// ====================================
		// 檢核
		// ====================================
		// 記錄執行結果
		List<String> processResults = Lists.newArrayList();

		// 成功
		int seccuce = 0;
		// 無需處理
		int notNeedProcess = 0;
		// 輸入資料問題
		int dataProblem = 0;
		// 失敗
		int fail = 0;

		// 逐筆執行
		for (TrnsDepRequireConfirmStatusVO trnsDepRequireConfirmStatusVO : trnsDataVO) {

			String voInfo = this.prepareVoiNnfo(trnsDepRequireConfirmStatusVO);
			String processResultMessage = "";

			log.info("======>" + voInfo);

			try {
				// 執行
				this.doProcess(trnsDepRequireConfirmStatusVO);
				// 執行記錄
				processResultMessage = "轉檔成功!";
				// 成功計數
				seccuce++;

			} catch (SystemOperationException e) {
				dataProblem++;
				processResultMessage = "輸入資料異常 - [" + e.getMessage() + "]";
				log.warn(processResultMessage);
			} catch (UserMessageException e) {
				notNeedProcess++;
				processResultMessage = "無需處理 - [" + e.getMessage() + "]";
				log.info(processResultMessage);
			} catch (Throwable e) {
				fail++;
				processResultMessage = "執行失敗 - [" + e.getMessage() + "]";
				log.error(processResultMessage, e);
			}

			processResults.add(voInfo + "：" + processResultMessage);
			trnsDepRequireConfirmStatusVO.setProcessResut(processResultMessage);
		}

		String summary = String.format(
		        "執行結果如下\r\n  "
		                + "%s"
		                + "\r\n"
		                + "----------------------------------------------"
		                + "\r\n"
		                + "成功：%s筆, "
		                + "無需處理：%s筆, "
		                + "資料問題：%s筆, "
		                + "失敗：%s筆",
		        String.join("\r\n  ", processResults),
		        seccuce,
		        notNeedProcess,
		        dataProblem,
		        fail);

		log.info(summary);

		return summary;
	}

	@Transactional(rollbackFor = Exception.class)
	private void doProcess(
	        TrnsDepRequireConfirmStatusVO vo) throws SystemOperationException, UserMessageException {

		Long startTime = System.currentTimeMillis();

		// ====================================
		// 檢查無需轉檔
		// ====================================
		if (vo.getTrnsToCompleteType() == null) {
			throw new UserMessageException("單位標註『自行領單』(或調整狀態為空)");
		}

		// ====================================
		// 取得主檔
		// ====================================
		Require require = this.requireService.findByReqNo(vo.getRequireNo());
		if (require == null) {
			throw new SystemOperationException("找不到需求單資料檔！", InfomationLevel.ERROR);
		}

		if (require.getCloseCode()) {
			throw new UserMessageException("單據已結案");
		}

		if (!RequireStatusType.PROCESS.equals(require.getRequireStatus())) {
			throw new UserMessageException("製作進度不為進行中 (目前：" + require.getRequireStatus().getValue() + ")");
		}

		// ====================================
		// 取得傳入部門的需求確認檔
		// ====================================
		// 以單據、需求確認部門查詢確認檔
		RequireConfirmDep requireConfirmDep = this.requireConfirmDepService.findByRequireSidAndDepSid(
		        require.getSid(),
		        vo.getDepSid());

		// 檢查：無確認檔
		if (requireConfirmDep == null) {
			throw new UserMessageException("找不到確認檔，可能已減派！");
		}

		// 該單位已完成流程
		if (requireConfirmDep.getProgStatus().isInFinish()) {
			String msg = String.format(
			        "流程已完成! 目前狀態：[%s] -> 標的狀態：[%s]",
			        requireConfirmDep.getProgStatus().getLabel(),
			        vo.getTrnsToCompleteType().getLabel());

			throw new UserMessageException(msg);
		}

		// ====================================
		// 處理
		// ====================================
		while (!requireConfirmDep.getProgStatus().isInFinish()) {

			// 需求確認進度
			ReqConfirmDepProgStatus progStatus = requireConfirmDep.getProgStatus();

			// ====================================
			// 為待領單時, 自動領單
			// ====================================
			if (ReqConfirmDepProgStatus.WAIT_CONFIRM.equals(progStatus)) {
				// 執行領單
				this.requireConfirmDepPanelActionService.actionReceive(
				        require.getRequireNo(),
				        requireConfirmDep.getSid(),
				        "單位確認狀態-批次轉置-自動領單",
				        WkConstants.ADMIN_USER_SID);

				// 重新查詢
				requireConfirmDep = this.requireConfirmDepService.findBySid(requireConfirmDep.getSid());

			}

			// ====================================
			// 為待進行忠實時, 轉置狀態
			// ====================================
			else if (ReqConfirmDepProgStatus.IN_PROCESS.equals(progStatus)) {

				boolean isExecRequireComplete = this.requireConfirmDepPanelActionService.actionComplete(
				        require.getRequireNo(),
				        requireConfirmDep.getSid(),
				        WkConstants.ADMIN_USER_SID,
				        vo.getTrnsToCompleteType(),
				        "單位確認狀態-批次轉置-轉置為:" + vo.getTrnsToCompleteType().getLabel());

				if (isExecRequireComplete) {
					String msg = String.format(
					        "[%s] 狀態變更為已完成",
					        require.getRequireNo());

					log.info(msg);
				}
			}
		}

		log.debug(WkCommonUtils.prepareCostMessage(startTime, this.prepareVoiNnfo(vo)));

	}

	private String prepareVoiNnfo(TrnsDepRequireConfirmStatusVO vo) {
		return String.format(
		        "[%s]-[(%s)%s]",
		        vo.getRequireNo(),
		        vo.getDepSid(),
		        vo.getImportDepName());
	}

}
