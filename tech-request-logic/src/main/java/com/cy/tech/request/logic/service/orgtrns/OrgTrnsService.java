package com.cy.tech.request.logic.service.orgtrns;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.commons.enums.OrgLevel;
import com.cy.system.rest.client.vo.OrgTransMappingTo;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsDtVO;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsPageVO;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsWorkVerifyVO;
import com.cy.tech.request.vo.enums.OthSetStatus;
import com.cy.tech.request.vo.enums.RequireTransProgramType;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkOrgTransMappingUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.group.vo.enums.RequireStatusType;
import com.cy.work.sp.logic.manager.RoleInquireDepManager;
import com.cy.work.sp.vo.RoleInquireDepVO;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Slf4j
@NoArgsConstructor
@Service
public class OrgTrnsService {

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private RoleInquireDepManager roleInquireDepManager;

    // ========================================================================
    //
    // ========================================================================
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * 查詢組織異動設定檔
     * 
     * @param pageVO
     */
    public void queryWorkVerify(OrgTrnsPageVO pageVO, String compId) {

        // ====================================
        // 清空舊的查詢資料
        // ====================================
        pageVO.setVerifyDtVOList(Lists.newArrayList());

        // ====================================
        // 查詢組織異動設定檔
        // ====================================
        // 查詢起始時間
        Long startTime = System.currentTimeMillis();

        // 查詢
        List<OrgTransMappingTo> orgTransMappingTos = WkOrgTransMappingUtils.getInstance().findOrgTrnsMapping(
                compId,
                new Date(pageVO.getQryEffectiveDate() == null ? 0 : pageVO.getQryEffectiveDate()));

        if (WkStringUtils.isEmpty(orgTransMappingTos)) {
            log.debug("查詢組織異動設定檔：0筆");
            return;
        }

        // ====================================
        // 排序器
        // ====================================
        // 取得所有單位排序序號
        Map<Integer, Integer> sortSeqMapByOrgSid = WkOrgCache.getInstance().findAllOrgOrderSeqMap();
        // 以轉換前單位排序
        Comparator<OrgTransMappingTo> comparator = Comparator.comparing(
                vo -> WkOrgUtils.prepareOrgSortSeq(sortSeqMapByOrgSid, vo.getBeforeOrgSid()));

        // ====================================
        // 排序並轉 VO
        // ====================================
        List<OrgTrnsWorkVerifyVO> verifyDtVOList = orgTransMappingTos.stream()
                .sorted(comparator)
                .map(orgTransMapping -> new OrgTrnsWorkVerifyVO(orgTransMapping))
                .collect(Collectors.toList());

        pageVO.setVerifyDtVOList(verifyDtVOList);

        log.debug(WkCommonUtils.prepareCostMessage(
                startTime,
                "查詢組織異動設定檔：[" + verifyDtVOList.size() + "]筆"));
    }

    /**
     * 準備顯示欄位
     * 
     * @param results
     * @param transProgramType
     */
    public void prepareShowInfo(List<OrgTrnsDtVO> results, RequireTransProgramType transProgramType) {

        for (OrgTrnsDtVO resultVO : results) {
            // user名稱
            String createUserName = WkUserUtils.prepareUserNameWithDep(resultVO.getCreateUserSid(), OrgLevel.THE_PANEL, true, "");
            if (WkStringUtils.isEmpty(createUserName)) {
                createUserName = "sid:" + resultVO.getCreateUserSid();
            }
            resultVO.setCreateUserName(createUserName);

            // 立案日期
            if (resultVO.getCreateDate_src() != null) {
                resultVO.setCreateDate(this.sdf.format(resultVO.getCreateDate_src()));
            }

            // 案件狀態
            if (WkStringUtils.notEmpty(resultVO.getCaseStatus_Src())) {
                String caseStatusDesc = "";
                String statusStr = WkStringUtils.safeTrim(resultVO.getCaseStatus_Src());
                try {

                    switch (transProgramType) {
                    case REQUIRE:
                        /** 需求單 */
                        caseStatusDesc = RequireStatusType.valueOf(statusStr).getValue();
                        break;
                    case PTCHECK:
                        /** 原型確認 */
                        caseStatusDesc = PtStatus.valueOf(statusStr).getLabel();
                        break;
                    case WORKTESTSIGNINFO:
                        /** 送測 */
                        caseStatusDesc = WorkTestStatus.valueOf(statusStr).getValue();
                        break;
                    case OTHSET:
                        /** 其他資料設定 */
                        caseStatusDesc = OthSetStatus.valueOf(statusStr).getVal();
                        break;
                    case WORKONPG:
                        /** ON程式 */
                        caseStatusDesc = WorkOnpgStatus.valueOf(statusStr).getLabel();
                        break;
                    default:
                        throw new SystemDevelopException("未定義的類型:" + transProgramType);
                    }
                } catch (Exception e) {
                    log.warn("解析 " + transProgramType + " 錯誤:" + resultVO.getCaseStatus_Src(), e);
                    caseStatusDesc = "unknown status:" + resultVO.getCaseStatus_Src();
                }

                resultVO.setCaseStatusDesc(caseStatusDesc);
            }
        }
    }

    /**
     * 取得待轉筆數字串
     * 
     * @param verifyVO
     * @param programTypeStr
     * @return
     */
    public String prepareWaitTransCountInfo(
            Map<String, Integer> waitTransCountMapByDataKey,
            OrgTrnsWorkVerifyVO verifyVO,
            String programTypeStr) {

        if (verifyVO == null) {
            log.error("傳入前後對應資料為空");
            return "";
        }

        if (waitTransCountMapByDataKey == null) {
            log.error("尚未計算筆數");
            return "";
        }

        // ====================================
        // 檢查參數
        // ====================================
        String dataKey = this.prepareWaitCountDataKey(programTypeStr, verifyVO.getBeforeOrgSid());

        if (!waitTransCountMapByDataKey.containsKey(dataKey)) {
            return WkHtmlUtils.addBlueClass("無");
        }

        Integer cnt = waitTransCountMapByDataKey.get(dataKey);
        if (cnt == 0) {
            return WkHtmlUtils.addBlueClass("無");
        }

        return WkHtmlUtils.addRedBlodClass(cnt + "筆");
    }

    /**
     * @param requireTransProgramType
     * @param orgTransMappingTo
     * @return
     */
    public String prepareWaitCountDataKey(
            String programTypeStr,
            Integer depSid) {
        return programTypeStr + "-" + depSid;
    }

    /**
     * 準備可閱設定檢查資料
     * 
     * @param verifyDtVOList
     * @param compSid
     */
    public void prepareCanViewCheckData(List<OrgTrnsWorkVerifyVO> verifyDtVOList, Integer compSid) {

        // 角色的可閱單位
        // 包含『轉換前單位』，但不包含『轉換後單位』者 -> 代表設定資料還沒轉過去
        // 包含『轉換前單位』，也包含『轉換後單位』者 -> 同時可看新舊單位->

        // ====================================
        // 查詢特殊可閱資料
        // ====================================
        List<RoleInquireDepVO> roleInquireDepVOs = this.roleInquireDepManager.findActiveVOByCompSid(compSid);
        if (WkStringUtils.isEmpty(roleInquireDepVOs)) {
            return;
        }

        // ====================================
        // 收集 部門 - 可閱角色資料
        // ====================================
        // Map<DepSid, List<可閱設定資料>>
        Map<Integer, List<RoleInquireDepVO>> settingsMapByDepSid = Maps.newHashMap();

        for (RoleInquireDepVO roleInquireDepVO : roleInquireDepVOs) {
            // 角色已停用時略過
            if (!roleInquireDepVO.isRoleActive()) {
                continue;
            }

            // 可閱全集團時略過
            if (roleInquireDepVO.getReadAllDep()) {
                continue;
            }

            // 未設定可閱部門時略過
            if (WkStringUtils.isEmpty(roleInquireDepVO.getCanViewDepSids())) {
                continue;
            }

            for (Integer canViewDepSid : roleInquireDepVO.getCanViewDepSids()) {

                // 取得對應 list
                List<RoleInquireDepVO> settingVOs = settingsMapByDepSid.get(canViewDepSid);

                // 還不存在時，初始化
                if (settingVOs == null) {
                    settingVOs = Lists.newArrayList();
                    settingsMapByDepSid.put(canViewDepSid, settingVOs);
                }
                // 加入
                settingVOs.add(roleInquireDepVO);
            }
        }

        // ====================================
        //
        // ====================================
        for (OrgTrnsWorkVerifyVO orgTrnsWorkVerifyVO : verifyDtVOList) {

            // 比對轉換前單位, 和轉換前單位有關的才需要處理
            if (!settingsMapByDepSid.containsKey(orgTrnsWorkVerifyVO.getBeforeOrgSid())) {
                continue;
            }

            // 取得和轉換前單位有關的可閱設定
            List<RoleInquireDepVO> settingVOs = settingsMapByDepSid.get(orgTrnsWorkVerifyVO.getBeforeOrgSid());

            // 取得不包含轉換後單位的設定資料
            settingVOs = settingVOs.stream()
                    .filter(settingVO -> !settingVO.getCanViewDepSids().contains(orgTrnsWorkVerifyVO.getAfterOrgSid()))
                    .collect(Collectors.toList());

            orgTrnsWorkVerifyVO.setCanViewSettingVOs(settingVOs);
        }
    }
}
