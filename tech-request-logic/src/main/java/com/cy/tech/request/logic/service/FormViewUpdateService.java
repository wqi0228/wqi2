/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.onpg.OnpgService;
import com.cy.tech.request.logic.service.pt.PtService;
import com.cy.tech.request.logic.service.send.test.SendTestService;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.work.common.utils.WkCommonUtils;

/**
 * 檢視需求單時需更新內容(非同步更新)
 *
 * @author shaun
 */
@Service
public class FormViewUpdateService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1868650269731295806L;
    @Autowired
    private PtService ptService;
    @Autowired
    private SendTestService stService;
    @Autowired
    private OnpgService opService;
    @Autowired
    private transient RequireReadRecordHelper requireReadRecordHelper;

    /**
     * 非同步進行更新閱讀記錄 - 全部 <BR/>
     * 暫時依開啟的TAB進行更新 20160129 shaun
     *
     * @param require
     * @param user
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateAllReadRecord(Require require, User user) {

        // ====================================
        // 需求單閱讀記錄
        // ====================================
        this.updateReqReadRecord(require, user);

        // ====================================
        // 原型確認單閱讀記錄
        // ====================================
        if (require.getHasPrototype()) {
            List<PtCheck> ptChecks = WkCommonUtils.safeStream(this.ptService.findByRequire(require))
                    .filter(ptCheck -> Activation.ACTIVE.equals(ptCheck.getStatus()))
                    .collect(Collectors.toList());
            for (PtCheck ptCheck : ptChecks) {
                this.updatePtRecord(ptCheck, user);
            }
        }

        // ====================================
        // 送測單閱讀記錄
        // ====================================
        if (require.getHasTestInfo()) {
            List<WorkTestInfo> testInfos = WkCommonUtils.safeStream(this.stService.findByRequire(require))
                    .filter(testInfo -> Activation.ACTIVE.equals(testInfo.getStatus()))
                    .collect(Collectors.toList());
            for (WorkTestInfo workTestInfo : testInfos) {
                this.updateSendTestRecord(workTestInfo, user);
            }
        }

        // ====================================
        // ON程式單閱讀記錄
        // ====================================
        if (require.getHasOnpg()) {
            List<WorkOnpg> workOnpgs = WkCommonUtils.safeStream(this.opService.findByRequire(require))
                    .filter(workOnpg -> Activation.ACTIVE.equals(workOnpg.getStatus()))
                    .collect(Collectors.toList());
            for (WorkOnpg workOnpg : workOnpgs) {
                this.updateOnpgRecord(workOnpg, user);
            }
        }
    }

    /**
     * 非同步進行更新閱讀記錄 - 需求單
     *
     * @param require
     * @param user
     */
    public void updateReqReadRecord(Require require, User user) {
        this.requireReadRecordHelper.saveAlreadyRead(FormType.REQUIRE, require.getSid(), user.getSid());
    }

    /**
     * 非同步進行更新閱讀記錄 - 原型確認
     *
     * @param require
     * @param user
     */
    public void updatePtRecord(PtCheck ptCheck, User executor) {
        this.requireReadRecordHelper.saveAlreadyRead(FormType.PTCHECK, ptCheck.getSid(), executor.getSid());
    }

    /**
     * 非同步進行更新閱讀記錄 - 送測
     *
     * @param require
     * @param user
     */
    public void updateSendTestRecord(WorkTestInfo testInfo, User executor) {
        this.requireReadRecordHelper.saveAlreadyRead(FormType.WORKTESTSIGNINFO, testInfo.getSid(), executor.getSid());
    }

    /**
     * 非同步進行更新閱讀記錄- ON程式
     *
     * @param require
     * @param user
     */
    public void updateOnpgRecord(WorkOnpg onpg, User executor) {
        this.requireReadRecordHelper.saveAlreadyRead(FormType.WORKONPG, onpg.getSid(), executor.getSid());
    }

}
