/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import com.cy.tech.request.repository.require.TrRequireRepository;
import com.cy.tech.request.vo.require.TrRequire;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 需求單Service
 *
 * @author brain0925_liao
 */
@Component
public class TrRequireService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4735667291332567058L;
    /** TrRequireRepository */
    @Autowired
    private TrRequireRepository trRequireRepository;

    /**
     * 取得需求單 By requireNo
     *
     * @param requireNo
     * @return
     */
    public TrRequire getTrRequireByRequireNo(String requireNo) {
        return trRequireRepository.findByRequireNo(requireNo);
    }

    /**
     * 更新需求單建立部門 By requireNo
     *
     * @param depSid 建立部門Sid
     * @param requireNo
     */
    public void updateTrRequireDepSidByRequireNo(Integer depSid, String requireNo) {
        trRequireRepository.updateDepsid(requireNo, depSid);
    }

}
