/**
 * 
 */
package com.cy.tech.request.logic.service.onpg.to;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Convert;

import com.cy.tech.request.vo.converter.RequireStatusTypeConverter;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.onpg.converter.WorkOnpgStatusConverter;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;

import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 */
public class Set10CountOnpgTo implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -8784439652972423622L;

    /**
     * 需求單號
     */
    @Getter
    @Setter
    private String requireNo;

    /**
     * 需求單 Sid
     */
    @Getter
    @Setter
    private String requireSid;
    
    /**
     * 需求單 製作進度
     */
    @Getter
    @Setter
    @Convert(converter = RequireStatusTypeConverter.class)
    private RequireStatusType requireStatus;

    /**
     * ONPG 單號
     */
    @Getter
    @Setter
    private String onpgNo;

    /**
     * ONPG Sid
     */
    @Getter
    @Setter
    private String onpgSid;

    /**
     * ONPG 主題
     */
    @Getter
    @Setter
    private String onpgTheme;

    /**
     * ONPG 單據狀態
     */
    @Getter
    @Setter
    @Convert(converter = WorkOnpgStatusConverter.class)
    private WorkOnpgStatus onpgStatus;

    /**
     * 歸屬部門 sid
     */
    @Getter
    @Setter
    private Integer depSid;

    /**
     * 建單者 sid
     */
    @Getter
    @Setter
    private Integer createUserSid;

    /**
     * 預計完成日
     */
    @Getter
    @Setter
    private Date estimateDate;

}
