package com.cy.tech.request.logic.service.pps6.vo;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class Pps6SignFlowTask implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1585855788241619695L;
    /**
     * 
     */
    private String nodeInfo;
    /**
     * 節點 User ID (BPM)
     */
    private String nodeUserID;
    /**
     * 節點 User 資訊 (對應WERP USER 資訊)
     */
    private String nodeUserInfo;
    /**
     * 
     */
    private String nodeRoleID;
    /**
     * 
     */
    private String nodeRoleName;

    /**
     * 是否已簽
     */
    private boolean completed;

    /**
     * 是否為代理簽核
     */
    private boolean agent;

    /**
     * 實際簽核者 User ID (BPM)
     */
    private String executorUserID;

    /**
     * 實際簽核者 User 資訊 (對應WERP USER 資訊)
     */
    private String executorUserInfo;

    /**
     * 簽核時間
     */
    private Date signTime;
}
