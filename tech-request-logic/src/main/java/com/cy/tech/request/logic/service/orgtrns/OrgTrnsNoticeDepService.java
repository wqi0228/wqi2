package com.cy.tech.request.logic.service.orgtrns;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.system.rest.client.vo.OrgTransMappingTo;
import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.orgtrns.emun.OrgTransSqlType;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsDtVO;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsWorkVerifyVO;
import com.cy.tech.request.repository.require.TrnsBackupRepository;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.RequireTransProgramType;
import com.cy.tech.request.vo.enums.SubNoticeType;
import com.cy.tech.request.vo.enums.TrnsType;
import com.cy.tech.request.vo.require.TrnsBackup;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.converter.JsonStringListToConverter;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.Gson;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Slf4j
@NoArgsConstructor
@Service
public class OrgTrnsNoticeDepService {

    // ========================================================================
    // 私有容器
    // ========================================================================
    @Data
    private class OrgTrnsNoticeDepBackupTo implements Serializable {
        /**
         * 
         */
        private static final long serialVersionUID = 6038247745062669820L;
        private List<String> sids;
        private List<String> caseNos;
    }

    // ========================================================================
    // 服務元件區
    // ========================================================================
    @Autowired
    private transient OrgTrnsService orgTrnsService;
    @Autowired
    private transient SearchService searchService;
    @Autowired
    private transient OrgTrnsBatchHelper orgTrnsBatchHelper;
    @Autowired
    private transient NativeSqlRepository nativeSqlRepository;
    @Autowired
    private transient TrnsBackupRepository trnsBackupRepository;
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private transient JdbcTemplate jdbcTemplate;

    // ========================================================================
    // 查詢方法區
    // ========================================================================
    /**
     * @param createDepSid
     * @param trnsType
     * @return
     * @throws UserMessageException
     * @throws SystemDevelopException
     */
    public List<OrgTrnsDtVO> queryNoticeDep(
            Integer createDepSid,
            RequireTransProgramType transProgramType) throws UserMessageException, SystemDevelopException {

        // ====================================
        // 取得部門
        // ====================================
        Org createDep = WkOrgCache.getInstance().findBySid(createDepSid);
        if (createDep == null) {
            throw new UserMessageException("查不到傳入的單位資料 sid:[" + createDepSid + "]", InfomationLevel.WARN);
        }

        // ====================================
        // 依據類別查詢
        // ====================================
        switch (transProgramType) {
        case REQUIRE:
            return this.queryForRequire(createDepSid);
        default:
            return this.queryForSubFrom(createDepSid, transProgramType);
        }
    }

    /**
     * @param noticeDepSid 通知單位 SID
     * @return
     */
    private List<OrgTrnsDtVO> queryForRequire(Integer noticeDepSid) {

        // ====================================
        // 查詢
        // ====================================
        // SQL
        String sql = this.prepareRequireQuerySQL(noticeDepSid, OrgTransSqlType.DETAIL);

        // 查詢
        List<OrgTrnsDtVO> results = this.nativeSqlRepository.getResultList(
                sql.toString(), null, OrgTrnsDtVO.class);

        if (WkStringUtils.isEmpty(results)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 準備顯示欄位
        // ====================================
        // 轉共同欄位
        this.orgTrnsService.prepareShowInfo(results, RequireTransProgramType.REQUIRE);

        // 轉私有欄位
        for (OrgTrnsDtVO resultVO : results) {
            // 主題
            if (WkStringUtils.notEmpty(resultVO.getTheme())) {
                resultVO.setTheme(searchService.combineFromJsonStr(resultVO.getTheme()));
            }
        }

        return results;
    }

    /**
     * 查詢子單
     * 
     * @param noticeDepSid
     * @param transProgramType
     * @return
     * @throws UserMessageException
     */
    private List<OrgTrnsDtVO> queryForSubFrom(
            Integer noticeDepSid,
            RequireTransProgramType transProgramType) throws UserMessageException {

        // ====================================
        // 查詢
        // ====================================
        // SQL
        String sql = this.prepareSubFormQuerySQL(noticeDepSid, transProgramType, OrgTransSqlType.DETAIL);

        // 查詢
        List<OrgTrnsDtVO> results = nativeSqlRepository.getResultList(
                sql.toString(), Maps.newHashMap(), OrgTrnsDtVO.class);

        // ====================================
        // 準備顯示欄位
        // ====================================
        this.orgTrnsService.prepareShowInfo(results, transProgramType);

        return results;
    }

    private String prepareRequireQuerySQL(
            Integer noticeDepSid,
            OrgTransSqlType sqlType) {

        // SQL
        StringBuffer sql = new StringBuffer();

        if (OrgTransSqlType.COUNT.equals(sqlType)) {
            sql.append("SELECT tr.require_sid  ");
        } else {
            sql.append("SELECT tr.require_sid            as sid, ");
            sql.append("       tr.require_no             as caseNo, ");
            sql.append("       tid.field_content         as theme, ");
            sql.append("       tr.create_dt              as createDate_src, ");
            sql.append("       tr.create_usr             as createUserSid, ");
            sql.append("       tr.require_status         as caseStatus_Src, ");
            sql.append("       info.info                 as noticeInfo_src ");
        }

        sql.append("FROM   tr_require tr ");

        // join 分派/通知檔 （取得日期最大的）
        sql.append("INNER JOIN tr_assign_send_info info ");
        sql.append("        ON tr.require_sid = info.require_sid ");
        sql.append("       AND info.type = " + AssignSendType.SEND.ordinal() + " ");
        sql.append("       AND info.create_dt = (SELECT Max(ai.create_dt) AS maxTime ");
        sql.append("                               FROM tr_assign_send_info ai ");
        sql.append("                              WHERE info.require_sid = ai.require_sid ");
        sql.append("                                AND info.type = ai.type ");
        sql.append("                              GROUP BY ai.require_sid) ");

        // join 取得主題
        if (OrgTransSqlType.DETAIL.equals(sqlType)) {
            sql.append("INNER JOIN (SELECT tid.require_sid, ");
            sql.append("                   tid.field_content ");
            sql.append("            FROM   tr_index_dictionary tid ");
            sql.append("            WHERE  1 = 1 ");
            sql.append("                   AND tid.field_name = '主題') AS tid ");
            sql.append("        ON tr.require_sid = tid.require_sid ");
        }

        // 以 tr_assign_send_search_info 為快速過濾條件
        sql.append("WHERE  EXISTS (SELECT dep_sid ");
        sql.append("               FROM   tr_assign_send_search_info si ");
        sql.append("               WHERE  tr.require_sid = si.require_sid ");
        sql.append("                      AND si.type = " + AssignSendType.SEND.ordinal() + " ");
        sql.append("                      AND si.dep_sid =  " + noticeDepSid + " ) ");

        sql.append("GROUP BY tr.require_sid ");

        if (OrgTransSqlType.DETAIL.equals(sqlType)) {
            sql.append("ORDER BY tr.require_no ASC ");
        }

        if (OrgTransSqlType.COUNT.equals(sqlType)) {
            return "SELECT COUNT(*) FROM (" + sql.toString() + ") aa ;";
        }

        return sql.toString();
    }

    /**
     * 準備子單查詢的 SQL
     * 
     * @param noticeDepSid
     * @param transProgramType
     * @param isCount
     * @return
     * @throws UserMessageException
     */
    private String prepareSubFormQuerySQL(
            Integer noticeDepSid,
            RequireTransProgramType transProgramType,
            OrgTransSqlType sqlType) throws UserMessageException {

        if (RequireTransProgramType.REQUIRE.equals(transProgramType)) {
            throw new UserMessageException("本方法不支援需求單", InfomationLevel.ERROR);
        }

        // ====================================
        // 兜組筆數查詢 SQL
        // ====================================
        if (OrgTransSqlType.COUNT.equals(sqlType)) {
            return ""
                    + "SELECT COUNT(*) "
                    + "  FROM " + transProgramType.getTableName() + " "
                    + "WHERE JSON_CONTAINS(" + transProgramType.getNoticeDepsColName() + ", '[\"" + noticeDepSid + "\"]' ,'$.value') ";
        }

        // ====================================
        // 兜組明細查詢 SQL
        // ====================================
        if (OrgTransSqlType.DETAIL.equals(sqlType)) {

            StringBuffer sql = new StringBuffer();
            sql.append("SELECT " + transProgramType.getSidColName() + "            as sid, ");
            sql.append("       " + transProgramType.getMasterCaseSidColName() + "  as requireSid, ");
            sql.append("       " + transProgramType.getCaseNoColName() + "         as caseNo, ");
            sql.append("       " + transProgramType.getThemeColName() + "          as theme, ");
            sql.append("       create_dt                                           as createDate_src, ");
            sql.append("       create_usr                                          as createUserSid, ");
            sql.append("       " + transProgramType.getStatusColName() + "         as caseStatus_Src, ");
            sql.append("       " + transProgramType.getNoticeDepsColName() + "     as noticeInfo_src, ");
            sql.append("       " + transProgramType.getMasterCaseNoColName() + "   as masterNo ");

            sql.append("FROM   " + transProgramType.getTableName() + "  ");
            sql.append("WHERE JSON_CONTAINS(" + transProgramType.getNoticeDepsColName() + ", '[\"" + noticeDepSid + "\"]' ,'$.value') ");
            sql.append("ORDER BY " + transProgramType.getCaseNoColName() + " ASC ");
            return sql.toString();
        }

        throw new SystemDevelopException("沒有定義的 SQL 類別:[" + sqlType + "]", InfomationLevel.ERROR);

    }

    // ========================================================================
    // 轉檔方法區
    // ========================================================================
    /**
     * 執行轉檔
     * 
     * @param orgTransMappingTo
     * @param execUserSid
     * @param transProgramType
     * @param selectedDtVOList
     * @throws SystemDevelopException
     */
    @Transactional(rollbackFor = Exception.class)
    public void processTrns(
            OrgTransMappingTo orgTransMappingTo,
            Integer execUserSid,
            RequireTransProgramType transProgramType,
            List<OrgTrnsDtVO> selectedDtVOList) {

        if (WkStringUtils.isEmpty(selectedDtVOList)) {
            return;
        }

        Date sysDate = new Date();

        log.info("\r\n開始進行 [" + transProgramType.getDescr() + "] 通知單位 轉檔 \r\n"
                + "來源單位: " + WkOrgUtils.prepareBreadcrumbsByDepName(orgTransMappingTo.getBeforeOrgSid(), OrgLevel.DIVISION_LEVEL, false, "-") + "\r\n"
                + "轉入單位: " + WkOrgUtils.prepareBreadcrumbsByDepName(orgTransMappingTo.getAfterOrgSid(), OrgLevel.DIVISION_LEVEL, false, "-") + "\r\n"
                + "共" + selectedDtVOList.size() + "筆");

        // ====================================
        // 建立轉檔前後對應關係
        // ====================================
        Map<Integer, Integer> trnsDepMapping = Maps.newHashMap();
        trnsDepMapping.put(
                orgTransMappingTo.getBeforeOrgSid(),
                orgTransMappingTo.getAfterOrgSid());

        // ====================================
        // 準備通知單位轉置資料
        // ====================================
        JsonStringListToConverter converter = new JsonStringListToConverter();
        for (OrgTrnsDtVO orgTrnsDtVO : selectedDtVOList) {

            // 取得原有的通知單位
            JsonStringListTo noticeDepInfo = converter.convertToEntityAttribute(orgTrnsDtVO.getNoticeInfo_src());

            // 原有的通知單位
            Set<Integer> oldNoticeDepSids = Sets.newHashSet();
            // 轉置後的通知單位
            Set<Integer> newNoticeDepSids = Sets.newHashSet();

            for (String depSidStr : noticeDepInfo.getValue()) {
                depSidStr = WkStringUtils.safeTrim(depSidStr);
                if (WkStringUtils.isNumber(depSidStr)) {
                    Integer depSid = Integer.parseInt(depSidStr);

                    // 舊分派部門
                    oldNoticeDepSids.add(depSid);

                    // 新分派部門
                    // 為需轉檔單位時, 換為新單位
                    if (trnsDepMapping.containsKey(depSid)) {
                        newNoticeDepSids.add(trnsDepMapping.get(depSid));
                    } else {
                        newNoticeDepSids.add(depSid);
                    }

                }
            }

            orgTrnsDtVO.setOldNoticeDepSids(oldNoticeDepSids);
            orgTrnsDtVO.setNewNoticeDepSids(newNoticeDepSids);

        }

        // ====================================
        // update create department
        // ====================================
        this.updateNoticeDepInfo(
                transProgramType.getTableName(),
                transProgramType.getNoticeDepsColName(),
                transProgramType.getSidColName(),
                transProgramType.getSubNoticeType(),
                selectedDtVOList,
                sysDate);

        log.info("轉檔已完成!");
    }

    /**
     * 異動子單
     * 
     * @param tableName
     * @param setColumnNameNoticeInfo
     * @param keyColumnNameSid
     * @param selectedDtVOList
     * @param sysDate
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    private void updateNoticeDepInfo(
            String tableName,
            String setColumnNameNoticeInfo,
            String keyColumnNameSid,
            SubNoticeType subNoticeType,
            List<OrgTrnsDtVO> selectedDtVOList,
            Date sysDate) {
        // ====================================
        // 更新子單通知單位資料
        // ====================================
        this.orgTrnsBatchHelper.batchUpdateNoticeDepInfo(
                tableName,
                setColumnNameNoticeInfo,
                keyColumnNameSid,
                selectedDtVOList,
                sysDate);

        // ====================================
        // 寫異動記錄
        // ====================================
        this.orgTrnsBatchHelper.batchInsertSubNoticeInfo(subNoticeType, selectedDtVOList, sysDate);
    }

    /**
     * save tr_trns_backup
     * 
     * @param orgTransMappingTo
     * @param trnsType
     * @param selectedDtVOList
     * @param execUserSid
     * @param sysdate
     */
    @Transactional(rollbackFor = Exception.class)
    private void processCreateBackup(
            OrgTransMappingTo orgTransMappingTo,
            TrnsType trnsType,
            List<OrgTrnsDtVO> selectedDtVOList,
            Integer execUserSid,
            Date sysdate) {

        // ====================================
        // 準備記錄資料
        // ====================================
        OrgTrnsNoticeDepBackupTo backupTo = new OrgTrnsNoticeDepBackupTo();
        backupTo.setSids(selectedDtVOList.stream().map(OrgTrnsDtVO::getSid).collect(Collectors.toList()));
        backupTo.setCaseNos(selectedDtVOList.stream().map(OrgTrnsDtVO::getCaseNo).collect(Collectors.toList()));

        // ====================================
        // 準備備份資料檔
        // ====================================
        TrnsBackup trnsBackup = new TrnsBackup();
        trnsBackup.setCreatedUser(execUserSid);
        trnsBackup.setCreatedDate(sysdate);
        trnsBackup.setTrnsType(trnsType.getValue());
        trnsBackup.setCustKey(this.prepareCustKey(orgTransMappingTo));
        trnsBackup.setBackData(new Gson().toJson(backupTo));

        // ====================================
        // save tr_trns_backup
        // ====================================
        trnsBackupRepository.save(trnsBackup);

    }

    /**
     * @param workVerify
     * @return
     */
    private String prepareCustKey(OrgTransMappingTo orgTransMappingTo) {
        return orgTransMappingTo.getBeforeOrgSid() + "-" + orgTransMappingTo.getAfterOrgSid();
    }

    // ========================================================================
    // 計算筆數
    // ========================================================================
    /**
     * 計算待轉筆數
     * 
     * @param orgTransMappingTos OrgTrnsWorkVerifyVO list
     * @return
     * @throws UserMessageException
     */
    public Map<String, Integer> countAllWaitTrans(List<OrgTrnsWorkVerifyVO> orgTransMappingTos) throws UserMessageException {
        if (WkStringUtils.isEmpty(orgTransMappingTos)) {
            return Maps.newHashMap();
        }

        // ====================================
        // 依據單據類別, 逐項查詢
        // ====================================
        Map<String, Integer> waitTransCountMapByDataKey = Maps.newHashMap();
        // 每一個單位
        for (OrgTrnsWorkVerifyVO orgTrnsWorkVerifyVO : orgTransMappingTos) {
            // 每一種轉檔型
            for (RequireTransProgramType requireTransProgramType : RequireTransProgramType.values()) {

                // 兜組查詢SQL
                String sql = "";
                if (RequireTransProgramType.REQUIRE.equals(requireTransProgramType)) {
                    sql = this.prepareRequireQuerySQL(
                            orgTrnsWorkVerifyVO.getBeforeOrgSid(), OrgTransSqlType.COUNT);

                } else {
                    sql = this.prepareSubFormQuerySQL(
                            orgTrnsWorkVerifyVO.getBeforeOrgSid(), requireTransProgramType, OrgTransSqlType.COUNT);
                }

                // 查詢
                try {
                    Number number = this.jdbcTemplate.queryForObject(sql, Integer.class);
                    // 不為 0 筆時才放資料
                    if (number != null) {
                        // 兜組 dataKey
                        String waitCountDataKey = this.orgTrnsService.prepareWaitCountDataKey(
                                requireTransProgramType.name(),
                                orgTrnsWorkVerifyVO.getBeforeOrgSid());

                        waitTransCountMapByDataKey.put(waitCountDataKey, number.intValue());
                    }
                } catch (Exception e) {
                    log.error(""
                            + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql) + "\r\n】"
                            + "\r\n");
                    throw e;
                }

            }

        }

        return waitTransCountMapByDataKey;
    }
}
