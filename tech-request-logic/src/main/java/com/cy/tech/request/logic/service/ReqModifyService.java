/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.customer.ReqWorkCustomerHelper;
import com.cy.tech.request.logic.service.helper.RequireIndexHelper;
import com.cy.tech.request.repository.require.ReqModifyRepo;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireIndexDictionary;
import com.cy.tech.request.vo.template.component.ComBase;
import com.cy.tech.request.vo.template.component.ComTextTypeOne;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.customer.vo.WorkCustomer;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 負責需求單更新(局部更新)<BR/>
 *
 * @author Shaun
 */
@Component
public class ReqModifyService implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 1003758555146977850L;
    @Autowired
	private RequireService reqService;
	@Autowired
	private RequireShowService reqShowService;
	@Autowired
	private ReqWorkCustomerHelper customerService;
	@Autowired
	private RequireTraceService traceService;
	@Autowired
	private SearchService searchService;
	@Autowired
	private TemplateService tempService;
	@Autowired
	private RequireIndexHelper indexHelper;

	@Autowired
	private ReqModifyRepo reqModifyDao;

	/**
	 * 更新緊急度(勿加Transactional)
	 *
	 * @param require
	 * @param urgency
	 * @param executor
	 */
	@Transactional(readOnly = true)
	public void updateUrgency(Require require, UrgencyType urgency, User executor) {
		Require nR = reqService.findByReqObj(require);
		if (nR.getUrgency().equals(urgency)) {
			return;
		}
		Preconditions.checkArgument(reqShowService.showUrgencyBtn(nR, executor.getSid()), "更新緊急度失敗！！");
		reqModifyDao.updateUrgency(require, urgency, executor, new Date());
	}

	@Transactional(rollbackFor = Exception.class)
	public void updateChangeCustomer(Require require, Map<String, ComBase> valueMap, User executor) {
		Require nR = reqService.findByReqObj(require);
		Preconditions.checkArgument(reqShowService.showCustmoerBtn(nR, executor, true), "修改廳主失敗！！");
		require.setContent(reqService.createContentTo(valueMap));
		this.changeCustomerAction(require, executor, valueMap);

		reqService.removeAndBuildCssAndIndex(
		        require.getSid(),
		        require.getRequireNo(),
		        valueMap,
		        new Date());

		reqModifyDao.updateCustomer(
		        require,
		        require.getHasTrace(),
		        require.getCustomer(),
		        require.getAuthor(),
		        require.getContent(),
		        executor, new Date());
	}

	@Transactional(rollbackFor = Exception.class)
	public void updateChangeAuthor(Require require, Map<String, ComBase> valueMap, User executor) {
		Require nR = reqService.findByReqObj(require);
		Preconditions.checkArgument(reqShowService.showAuthorBtn(nR, executor, true), "修改提出客戶失敗！！");
		if (this.changeCustomerAction(require, executor, valueMap)) {
			reqModifyDao.updateAuthor(
			        require,
			        require.getHasTrace(),
			        require.getAuthor(),
			        executor, new Date());
		}
	}

	/**
	 * 檢查是否變更廳主或客戶資料
	 *
	 * @param require
	 * @param executor
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean changeCustomerAction(Require require, User executor, Map<String, ComBase> valueMap) {
		WorkCustomer oldCustomer = reqService.findCustomerByRequire(require);
		WorkCustomer oldAuthor = reqService.findAuthorByRequire(require);
		boolean ischgCustomer = (oldCustomer != null && !oldCustomer.equals(require.getCustomer()))
		        || (require.getCustomer() != null && !require.getCustomer().equals(oldCustomer));
		boolean ischgAuthor = (oldAuthor != null && !oldAuthor.equals(require.getAuthor()))
		        || (require.getAuthor() != null && !require.getAuthor().equals(oldAuthor));
		String traceContent = "";
		RequireTraceType traceType = null;
		if (ischgCustomer && ischgAuthor) {
			traceType = RequireTraceType.MODIFY_CUSTOMER_AUTHOR;
			traceContent += customerService.buildChgCustomerText("廳主名稱", require.getCustomer(), oldCustomer);
			traceContent += "<BR/>";
			traceContent += customerService.buildChgCustomerText("提出客戶", require.getAuthor(), oldAuthor);
		} else if (ischgCustomer) {
			traceType = RequireTraceType.MODIFY_CUSTMOER;
			traceContent += customerService.buildChgCustomerText("廳主名稱", require.getCustomer(), oldCustomer);
		} else if (ischgAuthor) {
			traceType = RequireTraceType.MODIFY_AUTHOR;
			traceContent += customerService.buildChgCustomerText("提出客戶", require.getAuthor(), oldAuthor);
		}
		String themeChangeText = this.buildChangeThemeText(require, valueMap);
		if (!Strings.isNullOrEmpty(traceContent) && !Strings.isNullOrEmpty(themeChangeText)) {
			traceContent += "<BR/>";
			traceContent += themeChangeText;
		}

		if (!Strings.isNullOrEmpty(traceContent)) {
			traceService.createRequireTrace(require.getSid(), executor, traceType, traceContent);
			return true;
		}

		return false;
	}

	private String buildChangeThemeText(Require require, Map<String, ComBase> valueMap) {
		List<RequireIndexDictionary> reqThemeIdxs = indexHelper.findIndexByRequiresAndFieldName(Lists.newArrayList(require), "主題");
		if (reqThemeIdxs == null || reqThemeIdxs.isEmpty()) {
			return "";
		}
		String oldTheme = searchService.combineFromJsonStr(reqThemeIdxs.get(0).getFieldContent());
		ComBase comBase = tempService.findFieldByMap("主題", valueMap);
		if (comBase == null || !(comBase instanceof ComTextTypeOne)) {
			return "";
		}
		ComTextTypeOne comTextOne = (ComTextTypeOne) comBase;
		String newTheme = searchService.combineFromJsonStr(comTextOne.getIndexContent());
		if (oldTheme.equals(newTheme)) {
			return "";
		}
		return "主題 由【" + oldTheme + "】變更為【" + newTheme + "】";
	}

}
