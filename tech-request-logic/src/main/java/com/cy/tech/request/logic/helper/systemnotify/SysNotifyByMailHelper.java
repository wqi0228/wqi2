package com.cy.tech.request.logic.helper.systemnotify;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.TrMailService;
import com.cy.tech.request.logic.service.setting.sysnotify.SettingSysNotifyService;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.setting.sysnotify.to.SettingSysNotifyMainInfo;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.notify.vo.enums.NotifyMode;
import com.cy.work.notify.vo.enums.NotifyType;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Service
@Slf4j
public class SysNotifyByMailHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4669379521076915198L;
    // ========================================================================
    // 服務區
    // ========================================================================
    @Autowired
    private transient SettingSysNotifyService settingSysNotifyService;
    @Autowired
    private transient TrMailService trMailService;

    // ========================================================================
    // 核心方法區
    // ========================================================================
    /**
     * 依據部門發送通知mail (執行者不會收到)
     * 
     * @param require     需求單主檔
     * @param theme       單據主題
     * @param notifyType  通知類別
     * @param depSids     通知部門 sid
     * @param execUserSid 執行者 sid
     */
    protected void processNotifyByDeps(
            Require require,
            String theme,
            NotifyType notifyType,
            List<Integer> depSids,
            Integer execUserSid,
            boolean isShowDebug) {

        // ====================================
        // 取得推撥部門成員
        // ====================================
        List<Integer> userSids = WkUserCache.getInstance().findUserWithManagerByOrgSids(depSids, null).stream()
                .map(User::getSid)
                .collect(Collectors.toList());

        // ====================================
        // 過濾出有開啟通知的 user
        // ====================================
        Map<Integer, SettingSysNotifyMainInfo> notifySettingInfoMapByUserSid = this.settingSysNotifyService.filterNeedNoticeUser(
                notifyType,
                require.getMapping().getSmall().getSid(),
                require.getCustomer().getSid(),
                NotifyMode.MAIL,
                userSids);

        // ====================================
        // 執行程序
        // ====================================
        // 執行
        this.process(
                notifyType,
                notifySettingInfoMapByUserSid,
                require.getRequireNo(),
                theme,
                execUserSid,
                isShowDebug);
    }

    /**
     * 依據使用者發送通知mail (執行者不會收到)
     * 
     * @param require     需求單主檔
     * @param theme       單據主題
     * @param notifyType  通知類別
     * @param userSids    通知使用者 sid
     * @param execUserSid 執行者 sid
     */
    protected void processNotifyByUser(
            Require require,
            String theme,
            NotifyType notifyType,
            Collection<Integer> userSids,
            Integer execUserSid,
            boolean isShowDebug) {

        // ====================================
        // 過濾出有開啟通知的 user
        // ====================================
        Map<Integer, SettingSysNotifyMainInfo> notifySettingInfoMapByUserSid = this.settingSysNotifyService.filterNeedNoticeUser(
                notifyType,
                require.getMapping().getSmall().getSid(),
                require.getCustomer().getSid(),
                NotifyMode.MAIL,
                userSids);

        // ====================================
        // 執行程序
        // ====================================
        // 執行
        this.process(
                notifyType,
                notifySettingInfoMapByUserSid,
                require.getRequireNo(),
                theme,
                execUserSid,
                isShowDebug);
    }

    /**
     * 執行程序
     * 
     * @param notifyType     通知類別
     * @param notifyUserSids 通知使用者 sid
     * @param requireNo      需求單號
     * @param theme          單據主題 （主單 or 子單）
     * @param execUserSid    執行者 sid
     */
    private void process(
            NotifyType notifyType,
            Map<Integer, SettingSysNotifyMainInfo> notifySettingInfoMapByUserSid,
            String requireNo,
            String theme,
            Integer execUserSid,
            boolean isShowDebug) {

        Set<Integer> notifyUserSids = notifySettingInfoMapByUserSid.keySet();

        // ====================================
        // 扣掉自己不需要通知
        // ====================================
        notifyUserSids = notifyUserSids.stream()
                .filter(userSid -> !WkCommonUtils.compareByStr(userSid, execUserSid)) // 自己不需要通知
                .collect(Collectors.toSet());

        // ====================================
        // 不需要通知
        // ====================================
        if (WkStringUtils.isEmpty(notifyUserSids)) {
            if (isShowDebug) {
                log.info("需求單號：[{}], 方式：[{}], 類型:[{}]  => 沒有需要通知的 user",
                        requireNo,
                        NotifyMode.MAIL.getDescr(),
                        notifyType);
            }
            return;
        }

        // ====================================
        // 收集 mail address
        // ====================================
        Set<String> mailAddress = Sets.newHashSet();
        for (Integer notifyUserSid : notifyUserSids) {
            SettingSysNotifyMainInfo settingInfo = notifySettingInfoMapByUserSid.get(notifyUserSid);
            // 防呆, 應該不可能發生
            if (settingInfo == null || WkStringUtils.isEmpty(settingInfo.getNotifyMailAddrs())) {
                continue;
            }
            String notifyMailAddrStr = WkStringUtils.safeTrim(settingInfo.getNotifyMailAddrs());
            for (String mailAddr : notifyMailAddrStr.split(";")) {
                if (WkStringUtils.notEmpty(mailAddr)) {
                    mailAddress.add(mailAddr);
                }

                // bonnie@bb-in.com 檢核會不通過，暫不檢核
                // if (WkStringUtils.isEMail(mailAddr)) {
                // mailAddress.add(mailAddr);
                // } else {
                // log.warn("mail:[{}], 不符合郵件格式", mailAddr);
                // }
            }
        }

        // ====================================
        // 發送 mail
        // ====================================
        try {
            trMailService.sendSysNotify(
                    requireNo,
                    theme,
                    mailAddress,
                    notifyType);

            if (isShowDebug) {
                log.info("需求單號：[{}], 方式：[{}], 類型:[{}]  => 已建立：[{}]",
                        requireNo,
                        NotifyMode.MAIL.getDescr(),
                        notifyType,
                        WkUserUtils.findNameBySid(Lists.newArrayList(notifyUserSids), "、"));
            }

        } catch (Exception e) {
            log.error("發送分派 mail 通知失敗", e);
        }
    }
}
