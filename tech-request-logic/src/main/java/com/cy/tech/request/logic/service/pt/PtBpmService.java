/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.pt;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.BpmService;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.RequireReadRecordHelper;
import com.cy.tech.request.logic.service.RequireTraceService;
import com.cy.tech.request.repository.pt.PtCheckRepo;
import com.cy.tech.request.repository.pt.PtSignInfoRepo;
import com.cy.tech.request.repository.require.ReqModifyRepo;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.pt.PtSignInfo;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.value.to.BpmTaskTo;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * 原型確認 - 流程
 *
 * @author shaun
 */
@Slf4j
@Component
public class PtBpmService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5853555147845615588L;
    @Autowired
    private BpmService bpmService;
    @Autowired
    private RequireTraceService traceService;
    @Autowired
    private PtService ptService;
    @Autowired
    private OrganizationService orgSerivce;
    @Autowired
    private PtCheckRepo ptDao;
    @Autowired
    private PtSignInfoRepo signDao;
    @Autowired
    private ReqModifyRepo reqModifyDao;

    @Autowired
    private transient RequireReadRecordHelper requireReadRecordHelper;

    /**
     * 建立原型確認簽核流程
     *
     * @param ptCheck
     * @param executor
     * @return
     * @throws ProcessRestException
     */
    @Transactional(rollbackFor = Exception.class)
    public PtSignInfo createSignInfo(PtCheck ptCheck, User executor) throws ProcessRestException {
        PtSignInfo ptsi = new PtSignInfo();
        ptsi.setInstanceStatus(InstanceStatus.NEW_INSTANCE);
        ptsi.setSourceType(WorkSourceType.TECH_REQUEST);
        ptsi.setSourceSid(ptCheck.getSourceSid());
        ptsi.setSourceNo(ptCheck.getSourceNo());
        ptsi.setPtCheck(ptCheck);
        ptsi.setPtNo(ptCheck.getPtNo());
        ptsi.setBpmInstanceId(bpmService.createPtSignFlow(ptCheck, executor));
        this.setupPtSignInfo(ptsi);
        return signDao.save(ptsi);
    }

    /**
     * 設定簽核資訊經常性異動內容
     *
     * @param ptsi
     * @throws ProcessRestException
     */
    public void setupPtSignInfo(PtSignInfo ptsi) throws ProcessRestException {
        List<ProcessTaskBase> tasks = bpmService.findSimulationById(ptsi.getBpmInstanceId());
        ptsi.getTaskTo().setTasks(tasks);
        ptsi.setDefaultSignedName("");
        if (!tasks.isEmpty()) {
            ptsi.setDefaultSignedName(bpmService.findTaskDefaultUserName(tasks.get(tasks.size() - 1)));
            ptsi.getCanSignedIdsTo().setValue(bpmService.findTaskCanSignedUserIds(ptsi.getBpmInstanceId()));
        }
    }

    public boolean showSignBtn(PtCheck ptCheck, User login) {
        if (this.checkInfoStatus(ptCheck)) {
            return false;
        }
        PtSignInfo signInfo = ptCheck.getSignInfo();
        return signInfo.getCanSignedIdsTo().getValue().contains(login.getId());
    }

    private boolean checkInfoStatus(PtCheck ptCheck) {
        return ptCheck == null || !ptCheck.getHasSign() || !ptCheck.getPtStatus().equals(PtStatus.SIGN_PROCESS);
    }

    public boolean showRollBackBtn(PtCheck ptCheck, User login) {
        boolean canShow = this.showSignBtn(ptCheck, login);
        return canShow && ptCheck.getSignInfo().getTaskTo().getTasks().size() > 1;
    }

    public boolean showRecoveryBtn(PtCheck ptCheck, User login) {
        if (this.checkInfoStatus(ptCheck)) {
            return false;
        }
        PtSignInfo signInfo = ptCheck.getSignInfo();
        List<ProcessTaskBase> tasks = signInfo.getTaskTo().getTasks();
        InstanceStatus iStatus = signInfo.getInstanceStatus();
        // 任務模擬圖需超過兩位才能進行復原，結案也無法復原
        if (tasks.size() <= 1 || iStatus.equals(InstanceStatus.CLOSED)) {
            return false;
        }
        // 如果為核准..
        int recoverySub = iStatus.equals(InstanceStatus.APPROVED) ? 1 : 2;
        // 前一個簽核者為當前執行者才可復原
        ProcessTaskBase rTask = tasks.get(tasks.size() - recoverySub);
        if (rTask instanceof ProcessTaskHistory) {
            if (login.getId().equals(((ProcessTaskHistory) rTask).getExecutorID())) {
                return true;
            }
        }
        return false;
    }

    public boolean showInvaildBtn(PtCheck ptCheck, User login) {
        if (this.checkInfoStatus(ptCheck)) {
            return false;
        }
        List<ProcessTaskBase> tasks = ptCheck.getSignInfo().getTaskTo().getTasks();
        InstanceStatus iStatus = ptCheck.getSignInfo().getInstanceStatus();
        if (iStatus.equals(InstanceStatus.APPROVED)) {
            if (tasks.size() > 0) {
                ProcessTaskHistory lastTask = (ProcessTaskHistory) tasks.get(tasks.size() - 1);
                return lastTask.getExecutorID().equals(login.getId());
            }
        }
        List<User> canDoUsers = orgSerivce.findOrgManagers(WkOrgCache.getInstance().findBySid(ptCheck.getCreateDep().getSid()));
        canDoUsers.add(WkUserCache.getInstance().findBySid(ptCheck.getCreatedUser().getSid()));
        return canDoUsers.contains(login) && (iStatus.equals(InstanceStatus.NEW_INSTANCE)
                || iStatus.equals(InstanceStatus.WAITING_FOR_APPROVE)
                || iStatus.equals(InstanceStatus.APPROVING)
                || iStatus.equals(InstanceStatus.RECONSIDERATION));
    }

    @Transactional(rollbackFor = Exception.class)
    public void doSign(PtCheck ptCheck, User executor, String comment) throws ProcessRestException {
        Preconditions.checkArgument(this.showSignBtn(ptDao.findOne(ptCheck.getSid()), executor), "執行簽核失敗！！");
        bpmService.checkCanDoExceptRecovery(executor, ptCheck.getSignInfo().getBpmInstanceId());
        bpmService.doPtSign(ptCheck, executor, comment);
        this.updateSignInfo(ptCheck);
        this.handlerApproveSave(ptCheck, executor);
        log.info("{} BPM原型確認單簽核:{}", executor.getId(), ptCheck.getPtNo());
    }

    private void updateSignInfo(PtCheck ptCheck) throws ProcessRestException {
        this.setupPtSignInfo(ptCheck.getSignInfo());
        InstanceStatus is = bpmService.createInstanceStatus(ptCheck.getSignInfo().getBpmInstanceId(),
                ptCheck.getSignInfo().getTaskTo().getTasks());
        ptCheck.getSignInfo().setInstanceStatus(is);
        bpmService.updatePtSignInfo(ptCheck.getSignInfo());
    }

    private void handlerApproveSave(PtCheck ptCheck, User executor) {
        if (!ptCheck.getSignInfo().getInstanceStatus().equals(InstanceStatus.APPROVED)) {
            return;
        }
        ptCheck.getSignInfo().setApprovalDate(new Date());
        bpmService.updatePtSignInfo(ptCheck.getSignInfo());
        ptCheck.setPtStatus(PtStatus.APPROVE);
        ptCheck.setReadReason(WaitReadReasonType.PROTOTYPE_APPROVE);
        ptCheck.setReadUpdateDate(new Date());
        ptService.save(ptCheck, executor);

        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.PTCHECK,
                ptCheck.getSid(),
                executor.getSid());
    }

    @Transactional(rollbackFor = Exception.class)
    public void doRecovery(PtCheck ptCheck, User executor) throws ProcessRestException {
        Preconditions.checkArgument(this.showRecoveryBtn(ptDao.findOne(ptCheck.getSid()), executor), "執行復原失敗！！");
        bpmService.checkCanRecovery(executor, ptCheck.getSignInfo().getBpmInstanceId());
        BpmTaskTo taskTo = ptCheck.getSignInfo().getTaskTo();
        int recoverySub = ptCheck.getSignInfo().getInstanceStatus().equals(InstanceStatus.APPROVED) ? 1 : 2;
        bpmService.doRecovery(executor, (ProcessTaskHistory) taskTo.getTasks().get(taskTo.getTasks().size() - recoverySub));
        this.updateSignInfo(ptCheck);
        log.info("{} BPM原型確認單復原:{}", executor.getId(), ptCheck.getPtNo());
    }

    @Transactional(rollbackFor = Exception.class)
    public void doRollBack(PtCheck ptCheck, User executor, ProcessTaskHistory rollbackTask, String rollbackComment) throws ProcessRestException {
        Preconditions.checkArgument(this.showRollBackBtn(ptDao.findOne(ptCheck.getSid()), executor), "執行退回失敗！！");
        bpmService.checkCanDoExceptRecovery(executor, ptCheck.getSignInfo().getBpmInstanceId());
        bpmService.doRollBack(executor, rollbackTask, rollbackComment);
        this.updateSignInfo(ptCheck);
        log.info("{} BPM原型確認單退回:{}", executor.getId(), ptCheck.getPtNo());
    }

    @Transactional(rollbackFor = Exception.class)
    public void doInvaild(Require require, PtCheck ptCheck, User executor, String invaildTraceReason) throws ProcessRestException {
        Preconditions.checkArgument(this.showInvaildBtn(ptDao.findOne(ptCheck.getSid()), executor), "執行作廢失敗！！");
        bpmService.checkCanDoInvaild(executor, ptCheck.getSignInfo().getBpmInstanceId());
        bpmService.invaildProcess(executor, ptCheck.getSignInfo().getBpmInstanceId());
        this.setupPtSignInfo(ptCheck.getSignInfo());
        this.handlerInvaildSave(require, ptCheck, executor, invaildTraceReason);
        log.info("{} BPM原型確認單作廢:{}", executor.getId(), ptCheck.getPtNo());
    }

    private void handlerInvaildSave(Require require, PtCheck ptCheck, User executor, String invaildTraceReason) {
        PtSignInfo signInfo = ptCheck.getSignInfo();
        signInfo.setInstanceStatus(InstanceStatus.INVALID);
        signInfo.setCanSignedIdsTo(new JsonStringListTo());
        signInfo.setDefaultSignedName("");
        bpmService.updatePtSignInfo(signInfo);

        ptCheck.setStatus(Activation.INACTIVE);
        ptCheck.setPtStatus(PtStatus.VERIFY_INVAILD);
        ptCheck.setReadReason(WaitReadReasonType.PROTOTYPE_VERIFY_INVAILD);
        ptCheck.setReadUpdateDate(new Date());
        ptService.save(ptCheck, executor);

        traceService.createPtProcessInvaildTrace(require.getSid(), ptCheck, executor, invaildTraceReason);
        require.setHasTrace(Boolean.TRUE);
        reqModifyDao.updateHasTrace(require, require.getHasTrace());
        
        
        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.PTCHECK,
                ptCheck.getSid(),
                executor.getSid());
        
    }

    @Transactional(readOnly = true)
    public Boolean hasAnyProcessNotComplete(String requireNo) {
        List<InstanceStatus> status = Lists.newArrayList(
                InstanceStatus.NEW_INSTANCE,
                InstanceStatus.WAITING_FOR_APPROVE,
                InstanceStatus.APPROVING,
                InstanceStatus.RECONSIDERATION);
        return signDao.hasAnyProcessNotCompleteBySourceTypeAndSoruceNoAndStatusIn(WorkSourceType.TECH_REQUEST, requireNo, status);
    }

    @Transactional(readOnly = true)
    public List<PtSignInfo> findProcessNotComplete(String requireNo) {
        List<InstanceStatus> status = Lists.newArrayList(
                InstanceStatus.NEW_INSTANCE,
                InstanceStatus.WAITING_FOR_APPROVE,
                InstanceStatus.APPROVING,
                InstanceStatus.RECONSIDERATION);
        return signDao.findProcessNotCompleteBySourceTypeAndSoruceNoAndStatusIn(WorkSourceType.TECH_REQUEST, requireNo, status);
    }

    @Transactional(readOnly = true)
    public Integer findProcessNotCompleteCount(String requireNo) {
        List<PtSignInfo> result = this.findProcessNotComplete(requireNo);
        if (result == null) {
            return 0;
        }
        return result.size();
    }

    /**
     * 原型確認 - 重作 - 流程處理
     *
     * @param pc
     * @param executor
     * @throws ProcessRestException
     */
    @Transactional(rollbackFor = Exception.class)
    public void handlerRedo(PtCheck ptCheck, User executor) throws ProcessRestException {
        if (ptCheck.getHasSign()) {
            PtSignInfo signInfo = ptCheck.getSignInfo();
            signInfo.setInstanceStatus(InstanceStatus.CLOSED);
            bpmService.invaildProcess(executor, signInfo.getBpmInstanceId());
            this.setupPtSignInfo(signInfo);
            signDao.save(signInfo);
            ptCheck.setSignInfo(signInfo);
        }
    }

    /**
     * 原型確認 - 功能符合需求 - 流程處理
     *
     * @param pc
     * @param executor
     */
    @Transactional(rollbackFor = Exception.class)
    public void handlerFunctionConform(PtCheck ptCheck, User executor) {
        PtSignInfo signInfo = ptCheck.getSignInfo();
        if (signInfo == null) {
            return;
        }
        signInfo.setInstanceStatus(InstanceStatus.CLOSED);
        signDao.save(signInfo);
    }

    @Transactional(readOnly = true)
    public InstanceStatus findSigninfoStatusByRequire(Require require) {
        String paperCode = this.signDao.findPaperCodeBySourceNoDescPtNo(
                WorkSourceType.TECH_REQUEST.name(),
                require.getRequireNo());

        if (WkStringUtils.notEmpty(paperCode)) {
            return InstanceStatus.fromPaperCode(paperCode);
        }
        return null;
    }

    /**
     * 查詢最新一筆單據的簽核狀態
     * 
     * @param requireNo 需求單號
     * @param depSids   單位 sids
     * @return
     */
    @Transactional(readOnly = true)
    public InstanceStatus queryLastPaperCodeBySourceNoAndDepSid(String requireNo, List<Integer> depSids) {
        // ====================================
        // 檢核
        // ====================================
        if (WkStringUtils.isEmpty(requireNo)) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "傳入需求單號為空!");
            return null;
        }

        if (WkStringUtils.isEmpty(depSids)) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "傳入查詢單位為空!");
            return null;
        }

        // ====================================
        // 查詢
        // ====================================
        String paperCode = this.signDao.queryLastPaperCodeBySourceNoAndDepSid(
                WorkSourceType.TECH_REQUEST.name(),
                requireNo,
                depSids);

        if (WkStringUtils.notEmpty(paperCode)) {
            return InstanceStatus.fromPaperCode(paperCode);
        }
        return null;
    }

    /**
     * 最後一版流程是否作廢
     *
     * @param require
     * @return
     */
    @Transactional(readOnly = true)
    public Boolean isLastProcessInvaild(Require require) {
        if (!require.getHasPrototype()) {
            log.error("尚未進行過任何原型確認處理 ！！" + require.getRequireNo());
            return true;
        }
        List<PtSignInfo> processList = signDao.findBySourceNoDescPtNo(WorkSourceType.TECH_REQUEST.name(), require.getRequireNo());
        if (processList.isEmpty()) {
            log.warn("沒有任何原型確認流程 ！！" + require.getRequireNo());
            return true;
        }
        return processList.get(0).getInstanceStatus().equals(InstanceStatus.INVALID);
    }

}
