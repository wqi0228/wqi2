/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.logic.service.helper.RequireCssHelper;
import com.cy.tech.request.logic.service.helper.TemplateDefaultValueHelper;
import com.cy.tech.request.logic.service.helper.TemplateKeyMappingHelper;
import com.cy.tech.request.logic.service.helper.TemplateComponentHelper;
import com.cy.tech.request.logic.service.helper.TemplateFieldKeyMappingHelper;
import com.cy.tech.request.logic.vo.BasicDataCategoryTo;
import com.cy.tech.request.repository.require.RequireIndexDictionaryRepository;
import com.cy.tech.request.vo.enums.ComType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireCssContent;
import com.cy.tech.request.vo.require.RequireIndexDictionary;
import com.cy.tech.request.vo.template.CategoryKeyMapping;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import com.cy.tech.request.vo.template.component.ComBase;
import com.cy.tech.request.vo.template.component.ComBaseCss;
import com.cy.tech.request.vo.template.component.ComEditorTypeOne;
import com.cy.tech.request.vo.template.component.ComRadioTypeThree;
import com.cy.tech.request.vo.template.component.ComTextTypeOne;
import com.cy.tech.request.vo.value.to.TemplateComMapParserTo;
import com.cy.tech.request.vo.value.to.TemplateDefaultValueTo;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.customer.vo.WorkCustomer;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

/**
 * 模版
 *
 * @author shaun
 */
@Slf4j
@Component
public class TemplateService implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -6467653223879620528L;
    @Autowired
	private RequireIndexDictionaryRepository indexRepository;
	@Autowired
	private TemplateComponentHelper tmpFctoryHelper;
	@Autowired
	private TemplateKeyMappingHelper tmpKeyMappingHelper;
	@Autowired
	private TemplateFieldKeyMappingHelper tmpFieldKeyMappingHelper;
	@Autowired
	private TemplateDefaultValueHelper defaultValueHelper;
	@Autowired
	private RequireCssHelper cssHelper;
	/** 模版欄位名稱：主題 */
	public final static String FIELD_NAME_THEME = "主題";
	/** 模版欄位名稱：內容 */
	public final static String FIELD_NAME_CONTEXT = "內容";
	/** 模版欄位名稱：備註 */
	public final static String FIELD_NAME_NOTE = "備註";

	/**
	 * 搜尋全部活動中模版的最後一個版本
	 *
	 * @param searchText
	 * @return
	 */
	@Transactional(readOnly = true)
	public List<CategoryKeyMapping> findByFuzzyTextAndMaxVersion(String searchText) {
		return tmpKeyMappingHelper.findByFuzzyTextAndMaxVersion(searchText, Lists.newArrayList(Activation.ACTIVE));
	}

	/**
	 * 搜尋全部模版的最後一個版本
	 *
	 * @param searchText
	 * @return
	 */
	@Transactional(readOnly = true)
	public List<CategoryKeyMapping> findAllByFuzzyTextAndMaxVersion(String searchText) {
		return tmpKeyMappingHelper.findByFuzzyTextAndMaxVersion(searchText, Lists.newArrayList(Activation.ACTIVE, Activation.INACTIVE));
	}

	/**
	 * 查找模版鍵值
	 *
	 * @param smallName
	 * @param smallSid
	 * @return
	 */
	public Optional<CategoryKeyMapping> findMappBySmallSidAndName(String smallSid, String smallName) {
		List<CategoryKeyMapping> mappings = this.findByFuzzyTextAndMaxVersion(smallName);
		if (mappings == null) {
			return null;
		}
		return mappings.stream()
		        .filter(each -> each.getSmall().getName().equals(smallName))
		        .filter(each -> each.getSmall().getSid().equals(smallSid)).findAny();
	}

	/**
	 * 搜尋該MappingId所擁有的全部版本
	 *
	 * @param mappingId
	 * @return
	 */
	@Transactional(readOnly = true)
	public List<Integer> findByMappingIdAllVersionList(String mappingId) {
		return tmpKeyMappingHelper.findByIdAllVersionList(mappingId);
	}

	/**
	 * 檢查是否已存在相同的mappingId
	 *
	 * @param to
	 * @return
	 */
	@Transactional(readOnly = true)
	public Boolean isExistMappingId(BasicDataCategoryTo to) {
		return tmpKeyMappingHelper.isExistMappingId(to);
	}

	/**
	 * 儲存Mapping
	 *
	 * @param to
	 * @param isAdd
	 */
	@Transactional(rollbackFor = Exception.class)
	public void saveMapping(BasicDataCategoryTo to, Boolean isAdd) {
		tmpKeyMappingHelper.saveMapping(to, isAdd);
	}

	/**
	 * 建立空的模版欄位物件
	 *
	 * @param selectMapping
	 * @param comType
	 * @return
	 */
	public FieldKeyMapping createEmptyFieldKeyMapping(CategoryKeyMapping selectMapping, ComType comType) {
		return tmpFieldKeyMappingHelper.createEmptyFieldKeyMapping(selectMapping, comType);
	}

	/**
	 * 載入Field Mapping(模版欄位)
	 *
	 * @param mapping
	 * @return
	 */
	//@Transactional(readOnly = true)
	public List<FieldKeyMapping> loadTemplateFieldByMapping(CategoryKeyMapping mapping) {
		return tmpFieldKeyMappingHelper.findFieldByMapping(mapping);
	}

	/**
	 * 載入Map value
	 *
	 * @param require
	 * @return
	 */
	@Transactional(readOnly = true)
	public Map<String, ComBase> loadTemplateFieldValue(Require require) {
		// 請勿改為一般的Maps.newHashMap(...)避免在操作需求單內容資料時勿刪。
		ImmutableMap<String, ComBase> comValue = new ImmutableMap.Builder<String, ComBase>().putAll(require.getContent().getComValueMap()).build();
		List<RequireCssContent> cssValues = cssHelper.findCssContentByRequire(require.getSid());
		cssValues.stream()
		        .filter(each -> comValue.containsKey(each.getComId()))
		        .forEach(each -> {
			        ComBaseCss comCss = (ComBaseCss) comValue.get(each.getComId());
			        comCss.resetCssText(each.getContentCss());
		        });
		return comValue;
	}

	/**
	 * 儲存Field Mapping(模版欄位)
	 *
	 * @param mapping
	 * @param fields
	 */
	@Transactional(rollbackFor = Exception.class)
	public void saveFieldMapping(CategoryKeyMapping mapping, List<FieldKeyMapping> fields) {
		tmpFieldKeyMappingHelper.save(mapping, fields);
	}

	/**
	 * 儲存Field Mapping(模版欄位) 建立新版號
	 *
	 * @param mapping
	 * @param fields
	 */
	@Transactional(rollbackFor = Exception.class)
	public void saveByNewVersion(CategoryKeyMapping mapping, List<FieldKeyMapping> fields) {
		tmpFieldKeyMappingHelper.saveNewVersion(mapping, fields);
	}

	/**
	 * 建立模版元件預設值列表
	 *
	 * @param mappField
	 * @return
	 */
	public List<TemplateComMapParserTo> createComMapParserDefaultValueTos(FieldKeyMapping mappField) {
		return defaultValueHelper.createTemplateComMapParserDefaultValueTos(mappField);
	}

	/**
	 * 建立模版元件互動預設值列表
	 *
	 * @param mappField
	 * @return
	 */
	public List<TemplateComMapParserTo> createComMapParserInteractValueTos(FieldKeyMapping mappField) {
		return defaultValueHelper.createTemplateComMapParserInteractValueTos(mappField);
	}

	/**
	 * 建立模版欄位預設值物件
	 *
	 * @param tos
	 * @return
	 */
	public TemplateDefaultValueTo createTemplateDefaultValue(List<TemplateComMapParserTo> tos) {
		return defaultValueHelper.createTemplateDefaultValue(tos);
	}

	/**
	 * 建立新單的資料控制Map
	 *
	 * @param template
	 * @return
	 */
	public Map<String, ComBase> createNewDocValueMap(List<FieldKeyMapping> template) {
		return tmpFctoryHelper.createNewDocValueMap(template);
	}

	/**
	 * 建立空的元件對應物件
	 *
	 * @param type
	 * @return
	 */
	public ComBase createEmptyComBaseRefByComType(ComType type) {
		return tmpFctoryHelper.createEmptyComBaseRefByComType(type);
	}

	/**
	 * 載入模版欄位預設資料
	 *
	 * @param fields
	 * @return
	 */
	public Map<String, ComBase> loadTemplateFieldComBaseMap(List<FieldKeyMapping> fields, boolean isTriggerOnChangeEvent) {
		return tmpFctoryHelper.loadTemplateFieldComBaseMap(fields, isTriggerOnChangeEvent);
	}

	/**
	 * 依索引欄位名稱來查詢模版
	 *
	 * @param fieldName
	 * @return
	 */
	@Transactional(readOnly = true)
	public Set<CategoryKeyMapping> findTemplateByIndexFieldName(String fieldName) {
		return indexRepository.findByFieldName(fieldName).stream()
		        .map(RequireIndexDictionary::getMapping)
		        .collect(Collectors.toSet());
	}

	/**
	 * 依索引欄位名稱來查詢模版
	 *
	 * @param fieldName
	 * @return
	 */
	@Transactional(readOnly = true)
	public List<CategoryKeyMapping> findMappingByFieldName(String fieldName) {
		return indexRepository.findMappingByFieldName(fieldName);
	}

	public ComBase findFieldByMap(String fieldNmae, Map<String, ComBase> comValueMap) {
		Optional<ComBase> com = comValueMap.entrySet().stream()
		        .filter(each -> each.getValue().getName().equals(fieldNmae))
		        .map(Map.Entry::getValue).findFirst();
		return com.isPresent() ? com.get() : null;
	}

	/**
	 * 取回需求單主題
	 *
	 * @param requireNo
	 * @return
	 */
	public String findTheme(Require require, Map<String, ComBase> valueMap) {
		ComBase comBase = this.findFieldByMap(TemplateService.FIELD_NAME_THEME, valueMap);
		if (comBase != null && comBase instanceof ComTextTypeOne) {
			ComTextTypeOne themeCom = (ComTextTypeOne) comBase;
			return themeCom.getFinalValue() + " " + themeCom.getValue02() + " " + themeCom.getValue03();
		}
		if (require != null) {
			log.error("需求單號：" + require.getRequireNo() + " 模板SID:　" + require.getMapping().getSid() + " 查無主題欄位，請檢查。");
		} else {
			log.error("can't log find require theme error. because require is null !!");
		}
		return "";
	}

	/**
	 * 取得Editor元件內 css文字<BR/>
	 * 限備註及內容
	 *
	 * @param requireNo
	 * @param fieldName
	 * @return
	 */
	public String findEditorByFieldName(Require require, Map<String, ComBase> valueMap, String fieldName) {
		ComBase comBase = this.findFieldByMap(fieldName, valueMap);
		if (comBase != null && comBase instanceof ComEditorTypeOne) {
			ComEditorTypeOne editorCom = (ComEditorTypeOne) comBase;
			return editorCom.getValue01();
		}
		log.error("需求單號：" + require.getRequireNo() + " 模板SID:　" + require.getMapping().getSid() + " 查無" + fieldName + "欄位，請檢查。");
		return "";
	}

	/**
	 * 主題更新
	 *
	 * @param valueMap
	 * @param theme
	 */
	public void resetTheme(Map<String, ComBase> valueMap, String theme) {
		ComBase comBase = this.findFieldByMap(TemplateService.FIELD_NAME_THEME, valueMap);
		if (comBase != null && comBase instanceof ComTextTypeOne) {
			ComTextTypeOne themeCom = (ComTextTypeOne) comBase;
			themeCom.setValue02(theme);
		}
	}

	/**
	 * Editor元件內 css文字<BR/>
	 * 限備註及內容
	 *
	 * @param valueMap
	 * @param fieldName
	 * @param context
	 */
	public void resetEditorByFieldName(Map<String, ComBase> valueMap, String fieldName, String context) {
		ComBase comBase = this.findFieldByMap(fieldName, valueMap);
		if (comBase != null && comBase instanceof ComEditorTypeOne) {
			ComEditorTypeOne editorCom = (ComEditorTypeOne) comBase;
			editorCom.setValue01(context);
		}
	}

	/**
	 * 檢查該模版是否為最後版本
	 *
	 * @param mappingId
	 * @param version
	 * @return
	 */
	public boolean isLastVerByMapping(CategoryKeyMapping mapping) {
		List<Integer> versions = this.findByMappingIdAllVersionList(mapping.getId());
		Integer mattchVer = mapping.getVersion();
		return !versions.stream().anyMatch(ver -> ver > mattchVer);
	}

	/**
	 * 重設定主題後綴 (ComRadioTypeThree) 20170320
	 *
	 * @param templateItem
	 * @param comValueMap
	 */
	public void resetThemeSuffixBySpecCom(List<FieldKeyMapping> templateItem, Map<String, ComBase> comValueMap) {

		if (WkStringUtils.isEmpty(templateItem) || comValueMap == null) {
			return;
		}

		for (FieldKeyMapping fieldKeyMapping : templateItem) {
			ComBase comBase = comValueMap.get(fieldKeyMapping.getComId());
			if (comBase == null) {
				continue;
			}

			if (!comValueMap.containsKey(fieldKeyMapping.getInteractComId())) {
				continue;
			}

			if (!(comBase instanceof ComRadioTypeThree)) {
				continue;
			}

			ComRadioTypeThree radioT3 = (ComRadioTypeThree) comBase;

			radioT3.change(
			        comValueMap.get(
			                fieldKeyMapping.getInteractComId()),
			        fieldKeyMapping.getInteractComValue());

		}

//		templateItem.stream()
//		        .filter(each -> comValueMap.containsKey(each.getComId()))
//		        .forEach(each -> {
//			        ComBase com = comValueMap.get(each.getComId());
//			        if (com instanceof ComRadioTypeThree) {
//				        ComRadioTypeThree radioT3 = (ComRadioTypeThree) com;
//				        if (comValueMap.containsKey(each.getInteractComId())) {
//					        radioT3.change(comValueMap.get(each.getInteractComId()), each.getInteractComValue());
//				        }
//			        }
//		        });
	}
	
	
    /**
     * 修改廳主(一般編輯時連動)
     *
     * @param customer
     */
    public void changeThemeValueByCustomer(WorkCustomer customer, Map<String, ComBase> comValueMap) {
        ComBase comBase = this.findFieldByMap(FIELD_NAME_THEME, comValueMap);
        if (comBase != null && comBase instanceof ComTextTypeOne) {
            ComTextTypeOne comTextOne = (ComTextTypeOne) comBase;
            if (customer == null) {
                comTextOne.setValue02("");
            } else {
                // 取得上一次選擇的廳主@code
                String oldCustomerNameCode = comTextOne.getCustomerNameCode() == null ? "" : comTextOne.getCustomerNameCode();
                // 本次選擇的廳主@code
                String customerNameCode = String.format("%s @%s", customer.getName(), customer.getLoginCode());
                comTextOne.setCustomerNameCode(customerNameCode);
                // 還原value02
                comTextOne.setValue02(StringUtils.replace(comTextOne.getValue02(), oldCustomerNameCode, "").trim());
                // 重新組合 -> 廳主@code + value02
                comTextOne.setValue02(customerNameCode + " " + comTextOne.getValue02());
            }
        }
    }

}
