/**
 * 
 */
package com.cy.tech.request.logic.vo;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.setting.SettingCheckConfirmRight;
import com.cy.work.common.vo.AbstractEntityVO;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 設定：可檢查確認權限
 * 
 * @author allen1214_wu
 */
@EqualsAndHashCode(of = { "sid" }, callSuper = false)
@NoArgsConstructor
public class SettingCheckConfirmRightVO extends AbstractEntityVO implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 1385752732533543962L;

    /**
	 * @param assignDep
	 */
	public SettingCheckConfirmRightVO(SettingCheckConfirmRight settingCheckConfirmRight) {
		// ====================================
		// copy Properties
		// ====================================
		BeanUtils.copyProperties(settingCheckConfirmRight, this);
	}

	/**
	 * SID
	 */
	@Getter
	@Setter
	private String sid;

	/**
	 * 檢查類別
	 */
	@Getter
	@Setter
	private RequireCheckItemType checkItemType;

	/**
	 * 可檢查部門 sid
	 */
	@Getter
	@Setter
	private List<Integer> canCheckDepts;

	/**
	 * 可檢查人員 sid
	 */
	@Getter
	@Setter
	private List<Integer> canCheckUsers;

	/**
	 * 顯示:可檢查部門
	 */
	@Getter
	@Setter
	private String show_canCheckDepts;

	/**
	 * 顯示:可檢查人員
	 */
	@Getter
	@Setter
	private String show_canCheckUsers;
}
