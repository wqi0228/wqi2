package com.cy.tech.request.logic.service.setting.onetimetrns;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.logic.service.setting.onetimetrns.to.GroupModel;
import com.cy.tech.request.logic.service.setting.onetimetrns.to.TrnsForSetupInfoTo;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.repository.SettingCustomGroupRepository;
import com.cy.work.vo.SettingCustomGroup;
import com.cy.work.vo.enums.CustomGroupType;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;

/**
 * 一次性轉檔 - 預設分派/通知單位
 * 
 * @author allen1214_wu
 *
 */
@Slf4j
@Service
public class Setting10OneTimeTrnsforPrivateGroupService {

    // ========================================================================
    // 服務
    // ========================================================================

    @Autowired
    private transient NativeSqlRepository nativeSqlRepository;
    @Autowired
    private transient SettingCustomGroupRepository settingCustomGroupRepository;
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private transient JdbcTemplate jdbcTemplate;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 
     */
    @Transactional(rollbackFor = Exception.class)
    public void process() {
        this.process("tr_depgroup_setup_private", CustomGroupType.DEP);
        this.process("tr_group_setup_private", CustomGroupType.USER);
    }

    private void process(String tableName, CustomGroupType customGroupType) {
        // ====================================
        // 準備 SQL
        // ====================================

        StringBuffer varname1 = new StringBuffer();
        varname1.append("SELECT group_sid  AS groupSid, ");
        varname1.append("       create_usr AS createdUser, ");
        varname1.append("       create_dt  AS createdDate, ");
        varname1.append("       group_name AS groupName, ");
        varname1.append("       note       AS groupNote, ");
        varname1.append("       group_info AS groupInfoStr ");
        varname1.append("FROM   " + tableName + " ");
        varname1.append("WHERE  trns_flag = 0 ");
        varname1.append("ORDER BY create_usr ASC,  create_dt ASC ");

        // ====================================
        // 查詢
        // ====================================
        List<GroupModel> vos = nativeSqlRepository.getResultList(
                varname1.toString(),
                null,
                GroupModel.class);

        if (WkStringUtils.isEmpty(vos)) {
            log.info(tableName + " 已沒有需要轉檔的資料");
            return;
        }

        // ====================================
        // 組裝 entity
        // ====================================
        List<SettingCustomGroup> entities = Lists.newArrayList();
        Set<String> waitDeleteGroupSids = Sets.newHashSet();
        Map<String, Integer> userSeqMap = Maps.newHashMap();

        for (GroupModel groupModel : vos) {
            SettingCustomGroup entity = new SettingCustomGroup();

            entity.setCreatedUser(groupModel.getCreatedUser());
            entity.setCreatedDate(groupModel.getCreatedDate());
            entity.setGroupName(groupModel.getGroupName());
            entity.setGroupDescr(groupModel.getGroupNote());
            entity.setGroupType(customGroupType);

            // 排序
            Integer groupSeqByCreaterUser = 0;
            if (userSeqMap.containsKey(groupModel.getCreatedUser() + "")) {
                groupSeqByCreaterUser = userSeqMap.get(groupModel.getCreatedUser() + "");
                groupSeqByCreaterUser += 1;
            }
            userSeqMap.put(groupModel.getCreatedUser() + "", groupSeqByCreaterUser);
            entity.setSortSeq(groupSeqByCreaterUser);

            try {
                TrnsForSetupInfoTo info = WkJsonUtils.getInstance().fromJson(
                        groupModel.getGroupInfoStr(), TrnsForSetupInfoTo.class);

                List<String> sids = Lists.newArrayList();
                if (CustomGroupType.DEP.equals(customGroupType)) {
                    if (info != null && info.getDepartment() != null) {
                        sids = info.getDepartment();
                    }
                } else {
                    if (info != null && info.getUsers() != null) {
                        sids = info.getUsers();
                    }
                }

                List<Integer> intSids = sids.stream()
                        .filter(sid -> WkStringUtils.isNumber(sid))
                        .map(sid -> Integer.parseInt(sid))
                        .collect(Collectors.toList());

                if (CustomGroupType.DEP.equals(customGroupType)) {
                    entity.setGroupDepSids(intSids);
                } else {
                    entity.setGroupUserSids(intSids);
                }

            } catch (IOException e) {
                log.error(tableName + " GroupInfo 解析失敗! sid [{}], data:[{}]",
                        groupModel.getGroupSid(),
                        groupModel.getGroupInfoStr());
                continue;
            }

            waitDeleteGroupSids.add(groupModel.getGroupSid());
            entities.add(entity);
        }

        if (WkStringUtils.isEmpty(entities)) {
            log.info(tableName + ", 沒有可已成功轉檔的資料");
            return;
        }

        // ====================================
        // save
        // ====================================
        settingCustomGroupRepository.save(entities);

        // ====================================
        // 註記已轉檔
        // ====================================
        // 避免參數過多發生錯誤, 直接將SID組入字串
        String sql = ""
                + "UPDATE " + tableName + " "
                + "   SET trns_flag = 1 "
                + " WHERE group_sid IN ( '" + String.join("' ,'", waitDeleteGroupSids) + "'); ";
        // 執行 SQL
        jdbcTemplate.execute(sql);
    }
}
