/**
 * 
 */
package com.cy.tech.request.logic.service.setting.onetimetrns.to;

import com.cy.tech.request.vo.enums.ReqConfirmDepProgStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 */
@AllArgsConstructor
public class RequireDepRebuildTo {

    /**
     * sid
     */
    @Getter
    @Setter
    private String sid;

    /**
     * 主單sid
     */
    @Getter
    @Setter
    private String requireSid;

    /**
     * 部門Sid
     */
    @Getter
    @Setter
    private Integer depSid;

    /**
     * 負責人
     */
    @Getter
    @Setter
    private Integer ownerSid;

    @Getter
    @Setter
    private ReqConfirmDepProgStatus progStatus;
}
