/**
 * 
 */
package com.cy.tech.request.logic.helper.systemnotify;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.AssignSendInfoService;
import com.cy.tech.request.logic.service.BpmService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.TraceService;
import com.cy.tech.request.logic.service.setting.SettingCheckConfirmRightService;
import com.cy.tech.request.repository.require.feedback.ReqFbReplyRepo;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.onpg.WorkOnpgCheckRecord;
import com.cy.tech.request.vo.onpg.WorkOnpgCheckRecordReply;
import com.cy.tech.request.vo.require.AssignSendInfo;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.feedback.ReqFbkReply;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.value.to.ItemsCollectionDiffTo;
import com.cy.work.notify.logic.manager.WorkNotifyManager;
import com.cy.work.notify.vo.enums.NotifyType;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Service
@Slf4j
public class SysNotifyHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7722765963356150891L;
    // ========================================================================
    // 服務區
    // ========================================================================
    @Autowired
    private transient RequireService requireService;
    @Autowired
    private transient WorkNotifyManager workNotifyManager;
    @Autowired
    private transient AssignSendInfoService assignSendInfoService;
    @Autowired
    private transient BpmService bpmService;
    @Autowired
    private transient SysNotifyByPushHelper sysNotifyByPushHelper;
    @Autowired
    private transient SysNotifyByMailHelper sysNotifyByMailHelper;
    @Autowired
    private transient ReqFbReplyRepo reqFbReplyRepo;
    @Autowired
    private transient TraceService traceService;
    @Autowired
    private transient SettingCheckConfirmRightService settingCheckConfirmRightService;

    // ========================================================================
    // 客製方法區 （對外呼叫接口）- 需求單
    // ========================================================================
    /**
     * 執行通知 for 分派/通知
     * 
     * @param requireSid       需求單 sid
     * @param newAssignDepSids 異動後新分派結果
     * @param oldAssignDepSids 異動前的分派結果
     * @param newNoticeDepSids 異動後新通知結果
     * @param oldNoticeDepSids 異動前新通知結果
     * @param execUserSid      執行者
     */
    public void processForAssignDepAndNoticeDep(
            String requireSid,
            List<Integer> newAssignDepSids,
            List<Integer> oldAssignDepSids,
            List<Integer> newNoticeDepSids,
            List<Integer> oldNoticeDepSids,
            Integer execUserSid,
            boolean isShowDebug
            ) {

        // ====================================
        // 查詢需求單主檔
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);
        if (require == null) {
            log.warn("找不到需求單主檔資料! requireSid:[{}]", requireSid);
            return;
        }

        // 取出主題
        String theme = this.requireService.getReqTheme(require);

        // ====================================
        // 分派單位
        // ====================================
        // 比對新舊分派單位
        ItemsCollectionDiffTo<Integer> assignDepDiffTo = this.assignSendInfoService.prepareModifyDepsInfo(
                oldAssignDepSids,
                newAssignDepSids);

        // ------------------
        // 執行 mail + push
        // ------------------
        this.processByDep(
                require,
                theme,
                "",
                NotifyType.REQUEST_DISPATCH_NOTIFY,
                Lists.newArrayList(assignDepDiffTo.getPlusItems()),
                execUserSid,
                isShowDebug
                );

        // ------------------
        // 移除減派單位推撥
        // ------------------
        this.workNotifyManager.deleteByDeps(
                require.getRequireNo(),
                NotifyType.REQUEST_DISPATCH_NOTIFY,
                Lists.newArrayList(assignDepDiffTo.getReduceItems()),
                "單位被減派");

        // ====================================
        // 通知單位
        // ====================================
        // 比對新舊通知單位
        ItemsCollectionDiffTo<Integer> noticeDepDiffTo = this.assignSendInfoService.prepareModifyDepsInfo(
                oldNoticeDepSids,
                newNoticeDepSids);

        // ------------------
        // 執行 mail + push
        // ------------------
        this.processByDep(
                require,
                theme,
                "",
                NotifyType.REQUEST_UNIT_GOT_NOTIFY,
                Lists.newArrayList(noticeDepDiffTo.getPlusItems()),
                execUserSid,
                isShowDebug
                );

        // ------------------
        // 移除反通知單位推撥
        // ------------------
        this.workNotifyManager.deleteByDeps(
                require.getRequireNo(),
                NotifyType.REQUEST_UNIT_GOT_NOTIFY,
                Lists.newArrayList(noticeDepDiffTo.getReduceItems()),
                "通知單位被移除");

    }

    /**
     * 執行通知 for 結案
     * 
     * @param requireSid  需求單 sid
     * @param execUserSid 執行者
     */
    public void processForClose(
            String requireSid,
            Integer execUserSid) {

        // ====================================
        // 查詢需求單主檔
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);
        if (require == null) {
            log.warn("找不到需求單主檔資料! requireSid:[{}]", requireSid);
            return;
        }

        // 取出主題
        String theme = this.requireService.getReqTheme(require);

        // ====================================
        // 於案件結案時, 自動已讀推撥 -分派通知 -待結案通知
        // ====================================
        // 將類型通知已讀
        try {
            this.workNotifyManager.readNotify(
                    Lists.newArrayList(
                            NotifyType.REQUEST_DISPATCH_NOTIFY,
                            NotifyType.REQUEST_WAIT_CLOSE),
                    require.getRequireNo());
        } catch (Exception e) {
            log.error("將推撥已讀失敗!" + e.getMessage(), e);
        }

        // ====================================
        // 通知對象：申請者+該單據所有簽名者
        // ====================================
        List<Integer> flowUserSids = this.bpmService.findReqUnitSignFlowUser(require, true);

        // ====================================
        // 執行
        // ====================================
        this.processByUser(
                require,
                theme,
                " 已被結案",
                NotifyType.REQUEST_CLOSE,
                flowUserSids,
                execUserSid,
                true
                );
    }

    /**
     * 執行通知 for 結案
     * 
     * @param requireSid  需求單 sid
     * @param execUserSid 執行者
     */
    public void processForWaitClose(
            String requireSid,
            Integer execUserSid) {

        // ====================================
        // 查詢需求單主檔
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);
        if (require == null) {
            log.warn("找不到需求單主檔資料! requireSid:[{}]", requireSid);
            return;
        }

        // 取出主題
        String theme = this.requireService.getReqTheme(require);

        // ====================================
        // 移除推撥 - 結案通知
        // ====================================
        try {
            this.workNotifyManager.deleteByTypeAndSourceNo(
                    Lists.newArrayList(NotifyType.REQUEST_CLOSE),
                    require.getRequireNo());
        } catch (Exception e) {
            log.error("移除推撥 - 結案通知!" + e.getMessage(), e);
        }

        // ====================================
        // 通知對象：申請者+該單據所有簽名者
        // ====================================
        List<Integer> flowUserSids = this.bpmService.findReqUnitSignFlowUser(require, true);

        // ====================================
        // 執行
        // ====================================
        this.processByUser(
                require,
                theme,
                " 已需求完成，請確認是否可結案？",
                NotifyType.REQUEST_WAIT_CLOSE,
                flowUserSids,
                execUserSid,
                true);
    }

    /**
     * 執行通知 for 需求回覆補充
     * 
     * @param requireSid  需求單 sid
     * @param execUserSid 執行者
     */
    public void processForAddInfo(
            String requireSid,
            Integer execUserSid) {

        // ====================================
        // 查詢需求單主檔
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);
        if (require == null) {
            log.warn("找不到需求單主檔資料! requireSid:[{}]", requireSid);
            return;
        }

        // 取出主題
        String theme = this.requireService.getReqTheme(require);

        // ====================================
        // 通知對象
        // ====================================
        // 1.申請者+該單據所有簽名者
        List<Integer> flowUserSids = this.bpmService.findReqUnitSignFlowUser(require, false);
        // 2.所有分派單位成員
        List<Integer> assignDepUserSids = this.findAssignDepMemberByRequire(requireSid);

        // 收集
        Set<Integer> userSids = Sets.newHashSet();
        if (WkStringUtils.notEmpty(flowUserSids)) {
            userSids.addAll(flowUserSids);
        }
        if (WkStringUtils.notEmpty(assignDepUserSids)) {
            userSids.addAll(assignDepUserSids);
        }

        // ====================================
        // 執行
        // ====================================
        this.processByUser(
                require,
                theme,
                "",
                NotifyType.REQUEST_REPLY,
                Lists.newArrayList(userSids),
                execUserSid,
                true);
    }

    /**
     * 執行通知 for 需求回覆補充的回覆
     * 
     * @param requireSid  需求單 sid
     * @param fbkReply    回覆資料檔
     * @param execUserSid 執行者
     */
    public void processForAddInfoReply(
            String requireSid,
            ReqFbkReply fbkReply,
            Integer execUserSid) {

        // ====================================
        // 查詢需求單主檔
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);
        if (require == null) {
            log.warn("找不到需求單主檔資料! requireSid:[{}]", requireSid);
            return;
        }

        // 取出主題
        String theme = this.requireService.getReqTheme(require);

        // ====================================
        // 通知對象
        // ====================================
        // 1.該則補充資訊相關人員(建立者+回覆者)
        List<Integer> relationUserSids = this.findNeedNotifyUserByAgainReply(fbkReply);
        // 2.所有分派單位成員
        List<Integer> assignDepUserSids = this.findAssignDepMemberByRequire(requireSid);

        // 收集
        Set<Integer> userSids = Sets.newHashSet();
        if (WkStringUtils.notEmpty(relationUserSids)) {
            userSids.addAll(relationUserSids);
        }
        if (WkStringUtils.notEmpty(assignDepUserSids)) {
            userSids.addAll(assignDepUserSids);
        }

        // ====================================
        // 執行
        // ====================================
        this.processByUser(
                require,
                theme,
                "",
                NotifyType.REQUEST_AGAIN_REPLY,
                Lists.newArrayList(userSids),
                execUserSid,
                true);
    }

    /**
     * 執行通知 for 異動系統別
     * 
     * @param requireSid  需求單 sid
     * @param execUserSid 執行者
     */
    public void processForConfirmItemChange(
            String requireSid,
            Collection<RequireCheckItemType> beforeCheckItemTypes,
            Collection<RequireCheckItemType> afterCheckItemTypes,
            Integer execUserSid) {

        // ====================================
        // 查詢需求單主檔
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);
        if (require == null) {
            log.warn("找不到需求單主檔資料! requireSid:[{}]", requireSid);
            return;
        }

        // ====================================
        // 比對狀態
        // ====================================
        // 為進行中才發送
        if (!RequireStatusType.PROCESS.equals(require.getRequireStatus())) {
            log.debug("異動系統別通知：單據狀態非『進行中』無需通知 [{}]", require.getRequireStatus());
            return;
        }

        // ====================================
        // 比對新舊系統別差異
        // ====================================
        // 比對
        ItemsCollectionDiffTo<RequireCheckItemType> diffTo = WkCommonUtils.itemCollectionDiff(
                beforeCheckItemTypes,
                afterCheckItemTypes);

        // 沒有異動系統別時，不用處理
        if (WkStringUtils.isEmpty(diffTo.getPlusItems())
                && WkStringUtils.isEmpty(diffTo.getReduceItems())) {
            log.debug("系統通知模組-異動系統別:本次未異動系統別，無需通知");
            return;
        }

        // 收集有異動的系統別
        Set<RequireCheckItemType> modifyCheckItemType = Sets.newHashSet();
        // 1.新增
        if (WkStringUtils.notEmpty(diffTo.getPlusItems())) {
            modifyCheckItemType.addAll(diffTo.getPlusItems());
        }
        // 2.被移除
        if (WkStringUtils.notEmpty(diffTo.getReduceItems())) {
            modifyCheckItemType.addAll(diffTo.getReduceItems());
        }

        // ====================================
        // 收集通知對象 (『異動前後系統別』的對應『檢查人員』)
        // ====================================
        // 收集需通知人員
        Set<Integer> notifyUserSids = Sets.newHashSet();

        for (RequireCheckItemType requireCheckItemType : modifyCheckItemType) {
            // 查詢該類別的可檢查人員
            Set<Integer> canCheckUserSids = this.settingCheckConfirmRightService.findCanCheckUserSids(requireCheckItemType);
            if (WkStringUtils.notEmpty(canCheckUserSids)) {
                notifyUserSids.addAll(canCheckUserSids);
            }
        }

        if (WkStringUtils.isEmpty(notifyUserSids)) {
            log.debug("系統通知模組-異動系統別:找不到檢查人員，無需通知 [" + modifyCheckItemType.stream().map(RequireCheckItemType::getDescr).collect(Collectors.joining("、")));
            return;
        }

        // ====================================
        // 執行通知
        // ====================================
        // 取出主題
        String theme = this.requireService.getReqTheme(require);

        List<String> before = WkCommonUtils.safeStream(beforeCheckItemTypes)
                .map(RequireCheckItemType::getDescr)
                .collect(Collectors.toList());

        List<String> after = WkCommonUtils.safeStream(afterCheckItemTypes)
                .map(RequireCheckItemType::getDescr)
                .collect(Collectors.toList());

        String traceContent = String.format("異動檢查項目:[%s]->[%s]",
                String.join("、", before),
                String.join("、", after));

        this.processByUser(
                require,
                theme + " " + traceContent,
                "",
                NotifyType.REQUEST_CONFIRM_ITEM_CHANGE,
                notifyUserSids,
                execUserSid,
                true);
    }

    // ========================================================================
    // 客製方法區 （對外呼叫接口）- ON 程式
    // ========================================================================
    /**
     * 執行通知 for 新增 ON 程式單
     * 
     * @param requireSid    需求單 sid
     * @param onpgTheme     on 程式單主題
     * @param noticeDepSids 新增的通知單位
     * @param execUserSid   執行者
     */
    public void processForAddOnpg(
            String requireSid,
            String onpgTheme,
            List<String> beforeNoticeDepSids,
            List<String> afterNoticeDepSids,
            Integer execUserSid) {

        // ====================================
        // 查詢需求單主檔
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);
        if (require == null) {
            log.warn("找不到需求單主檔資料! requireSid:[{}]", requireSid);
            return;
        }

        // ====================================
        // 計算通知單位, 新增或減少
        // ====================================
        ItemsCollectionDiffTo<Integer> diffInfo = this.prepareNoticeDepsDiffInfo(
                beforeNoticeDepSids,
                afterNoticeDepSids);

        // ====================================
        // 移除被移除單位推撥
        // ====================================
        this.workNotifyManager.deleteByDeps(
                require.getRequireNo(),
                NotifyType.ON_PG_NOTIFY,
                Lists.newArrayList(diffInfo.getReduceItems()),
                "移除ON程式通知單位");

        // ====================================
        // 執行
        // ====================================
        this.processByDep(
                require,
                onpgTheme,
                "",
                NotifyType.ON_PG_NOTIFY,
                Lists.newArrayList(diffInfo.getPlusItems()),
                execUserSid,
                true);
    }

    /**
     * 執行通知 for ON程式單 - 新增/編輯檢查訊息 (GM回覆、QA回覆、一般回覆)
     * 
     * @param requireSid
     * @param onpgTheme
     * @param onpgCheckRecord
     * @param execUserSid
     */
    public void processForOnpgAddCheckRecord(
            String requireSid,
            String onpgTheme,
            WorkOnpgCheckRecord onpgCheckRecord,
            Integer execUserSid) {

        // ====================================
        // 取得通知類別
        // ====================================
        NotifyType notifyType = onpgCheckRecord.getReplyType().getNotifyTypeForCheckRecord();
        if (notifyType == null) {
            log.error("轉換 onpg 回覆類別失敗，無法發送系統通知");
            return;
        }

        // ====================================
        // 查詢需求單主檔
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);
        if (require == null) {
            log.warn("找不到需求單主檔資料! requireSid:[{}]", requireSid);
            return;
        }

        // ====================================
        // 通知人員
        // ====================================
        // 1.ON程式單建立者
        Set<Integer> userSids = Sets.newHashSet(onpgCheckRecord.getOnpg().getCreatedUser().getSid());

        // // 2.檢查訊息 (GM回覆、QA回覆、一般回覆) 建立者
        // // 不會發生 - 因為建立者即為建單觸發者
        // if (onpgCheckRecord.getPerson() != null) {
        // userSids.add(onpgCheckRecord.getPerson().getSid());
        // }
        //
        // // 3.檢查訊息 (GM回覆、QA回覆、一般回覆) 回覆者
        // // 不會發生 - 有回覆後，該則回覆不可編輯
        // if (WkStringUtils.notEmpty(onpgCheckRecord.getCheckRecordReplys())) {
        // for (WorkOnpgCheckRecordReply workOnpgCheckRecordReply : onpgCheckRecord.getCheckRecordReplys()) {
        // userSids.add(workOnpgCheckRecordReply.getPerson().getSid());
        // }
        // }

        // ====================================
        // 執行
        // ====================================
        this.processByUser(
                require,
                onpgTheme,
                "",
                notifyType,
                Lists.newArrayList(userSids),
                execUserSid,
                true);

    }

    /**
     * 執行通知 for ON程式單 - 新增/編輯檢查訊息 (GM回覆、QA回覆) 回覆
     * 
     * @param requireSid
     * @param onpgTheme
     * @param onpgCheckRecord
     * @param execUserSid
     */
    public void processForOnpgAddCheckRecordReply(
            String requireSid,
            String onpgTheme,
            WorkOnpgCheckRecordReply workOnpgCheckRecordReply,
            Integer execUserSid) {

        // ====================================
        // 檢查回覆檔
        // ====================================
        WorkOnpgCheckRecord onpgCheckRecord = workOnpgCheckRecordReply.getCheckRecord();
        if (onpgCheckRecord == null) {
            log.error("傳入檢查檔為空");
            return;
        }

        // ====================================
        // 取得通知類別
        // ====================================
        NotifyType notifyType = workOnpgCheckRecordReply.getCheckRecord().getReplyType().getNotifyTypeForCheckRecordReply();
        if (notifyType == null) {
            log.error("轉換 onpg 回覆類別失敗，無法發送系統通知");
            return;
        }

        // ====================================
        // 查詢需求單主檔
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);
        if (require == null) {
            log.warn("找不到需求單主檔資料! requireSid:[{}]", requireSid);
            return;
        }

        // ====================================
        // 通知人員
        // ====================================
        // 1.ON程式單建立者
        Set<Integer> userSids = Sets.newHashSet(onpgCheckRecord.getOnpg().getCreatedUser().getSid());
        // 2.檢查訊息 (GM回覆、QA回覆) 建立者
        if (onpgCheckRecord.getPerson() != null) {
            userSids.add(onpgCheckRecord.getPerson().getSid());
        }
        // 3.檢查訊息 (GM回覆、QA回覆) 回覆者
        if (WkStringUtils.notEmpty(onpgCheckRecord.getCheckRecordReplys())) {
            for (WorkOnpgCheckRecordReply currReply : onpgCheckRecord.getCheckRecordReplys()) {
                userSids.add(currReply.getPerson().getSid());
            }
        }

        // ====================================
        // 執行
        // ====================================
        this.processByUser(
                require,
                onpgTheme,
                "",
                notifyType,
                Lists.newArrayList(userSids),
                execUserSid,
                true);

    }

    // ========================================================================
    // 客製方法區 （對外呼叫接口）- 送測單
    // ========================================================================
    /**
     * 執行通知 for 新增 送測單
     * 
     * @param requireSid          需求單 sid
     * @param workTestTheme       送測單主題
     * @param beforeNoticeDepSids 新增的通知單位
     * @param execUserSid         執行者
     */
    public void processForWorkTest(
            String requireSid,
            String workTestTheme,
            List<String> beforeNoticeDepSids,
            List<String> afterNoticeDepSids,
            Integer execUserSid) {

        // ====================================
        // 查詢需求單主檔
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);
        if (require == null) {
            log.warn("找不到需求單主檔資料! requireSid:[{}]", requireSid);
            return;
        }

        // ====================================
        // 計算通知單位, 新增或減少
        // ====================================
        ItemsCollectionDiffTo<Integer> diffInfo = this.prepareNoticeDepsDiffInfo(
                beforeNoticeDepSids,
                afterNoticeDepSids);

        // ====================================
        // 移除被移除單位推撥
        // ====================================
        this.workNotifyManager.deleteByDeps(
                require.getRequireNo(),
                NotifyType.SEND_TEST_NOTIFY,
                Lists.newArrayList(diffInfo.getReduceItems()),
                "移除送測通知單位");

        // ====================================
        // 執行
        // ====================================
        this.processByDep(
                require,
                workTestTheme,
                "",
                NotifyType.SEND_TEST_NOTIFY,
                Lists.newArrayList(diffInfo.getPlusItems()),
                execUserSid,
                true);
    }

    /**
     * @param workTestTheme 送測單主檔
     * @param notifyType    僅限
     *                          QA_JOIN_SCHEDULE、QA_UNJOIN_SCHEDULE、MODIFY_SEND_TEST_DATE
     * @param execUserSid   執行者 SID
     */
    public void processForWorkTestEventForRelationUser(
            WorkTestInfo workTestInfo,
            NotifyType notifyType,
            Integer execUserSid) {

        // ====================================
        // 查詢需求單主檔
        // ====================================
        Require require = this.requireService.findByReqSid(workTestInfo.getSourceSid());
        if (require == null) {
            log.warn("找不到需求單主檔資料! requireSid:[{}]", workTestInfo.getSourceSid());
            return;
        }

        // ====================================
        // 收集
        // ====================================
        Set<Integer> notifyUserSids = Sets.newHashSet();
        // 1.主單申請者
        notifyUserSids.add(require.getCreatedUser().getSid());
        // 2.送測單申請者
        notifyUserSids.add(workTestInfo.getCreatedUser().getSid());
        // 3.送測單單位主管
        if (workTestInfo.getCreateDep() != null && workTestInfo.getCreateDep().getManager() != null) {
            notifyUserSids.add(workTestInfo.getCreateDep().getManager().getSid());
        }
        // 4.追蹤中的使用者
        Set<Integer> traceUserSids = this.traceService.findTraceUserSids(require.getRequireNo());
        if (WkStringUtils.notEmpty(traceUserSids)) {
            notifyUserSids.addAll(traceUserSids);
        }

        // ====================================
        // 執行
        // ====================================
        this.processByUser(
                require,
                workTestInfo.getTheme(),
                "&nbsp;送測單號[" + workTestInfo.getSourceNo() + "]&nbsp;" + notifyType.getMailContent(),
                notifyType,
                Lists.newArrayList(notifyUserSids),
                execUserSid,
                true);

    }

    // ========================================================================
    // 客製方法區 （對外呼叫接口）- 原型確認單
    // ========================================================================
    /**
     * 執行通知 for 新增 原型確認單
     * 
     * @param requireSid           需求單 sid
     * @param ptCheckTheme         原型確認單主題
     * @param beforeNoticeUserSids 異動前通知對象
     * @param beforeNoticeDepSids  異動前通知部門
     * @param afterNoticeUserSids  異動後通知對象
     * @param afterNoticeDepSids   異動後通知部門
     * @param execUserSid
     */
    public void processForAddPtCheck(
            String requireSid,
            String ptCheckTheme,
            List<String> beforeNoticeUserSids,
            List<String> beforeNoticeDepSids,
            List<String> afterNoticeUserSids,
            List<String> afterNoticeDepSids,
            Integer execUserSid) {

        // ====================================
        // 查詢需求單主檔
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);
        if (require == null) {
            log.warn("找不到需求單主檔資料! requireSid:[{}]", requireSid);
            return;
        }

        // ====================================
        // 防呆防呆
        // ====================================
        if (beforeNoticeUserSids == null) {
            beforeNoticeUserSids = Lists.newArrayList();
        }
        if (beforeNoticeDepSids == null) {
            beforeNoticeDepSids = Lists.newArrayList();
        }
        if (afterNoticeUserSids == null) {
            afterNoticeUserSids = Lists.newArrayList();
        }
        if (afterNoticeDepSids == null) {
            afterNoticeDepSids = Lists.newArrayList();
        }
        if (afterNoticeDepSids == null) {
            afterNoticeDepSids = Lists.newArrayList();
        }

        // ====================================
        // 收集容器
        // ====================================
        // 需新增新增通知的 user
        Set<Integer> addUserSids = Sets.newHashSet();
        // 需新增移除通知的 user
        Set<Integer> reduceUserSids = Sets.newHashSet();

        // 取得異動後通知單位所有人員
        Set<Integer> noticeDepSids = afterNoticeDepSids.stream()
                .map(sid -> Integer.parseInt(sid))
                .collect(Collectors.toSet());
        Set<Integer> noticeDepUserSids = WkUserCache.getInstance().findUserSidByOrgsWithManager(
                noticeDepSids,
                null);

        // ====================================
        // 處理通知user名單
        // ====================================
        // 計算通知使用者, 新增或減少
        ItemsCollectionDiffTo<Integer> diffUserInfo = this.prepareNoticeDepsDiffInfo(
                beforeNoticeUserSids,
                afterNoticeUserSids);

        // 收集新增的 user
        if (WkStringUtils.notEmpty(diffUserInfo.getPlusItems())) {
            addUserSids.addAll(diffUserInfo.getPlusItems());
        }

        // 收集移除的 user
        if (WkStringUtils.notEmpty(diffUserInfo.getReduceItems())) {
            // 不存在於通知單位中的 user 才移除(推撥)
            for (Integer reduceUserSid : diffUserInfo.getReduceItems()) {
                if (!noticeDepUserSids.contains(reduceUserSid)) {
                    reduceUserSids.add(reduceUserSid);
                }
            }
        }

        // ====================================
        // 處理通知單位名單
        // ====================================
        // 計算通知單位, 新增或減少
        ItemsCollectionDiffTo<Integer> diffDepInfo = this.prepareNoticeDepsDiffInfo(
                beforeNoticeDepSids,
                afterNoticeDepSids);

        // ----------------------
        // 處理增加的單位人員
        // ----------------------
        if (WkStringUtils.notEmpty(diffDepInfo.getPlusItems())) {
            // 查詢新增單位的使用者
            Set<Integer> depUserSids = WkUserCache.getInstance().findUserSidByOrgsWithManager(
                    Lists.newArrayList(diffDepInfo.getPlusItems()), null);

            if (WkStringUtils.notEmpty(depUserSids)) {
                addUserSids.addAll(depUserSids);
            }
        }
        // ----------------------
        // 處理移除的單位人員
        // ----------------------
        // 說明:不在通知單位&通知人員的列表中，才可移除
        if (WkStringUtils.notEmpty(diffDepInfo.getReduceItems())) {

            // 查詢本次移除單位的所有使用者
            Set<Integer> reduceDepUserSids = WkUserCache.getInstance().findUserSidByOrgsWithManager(
                    Lists.newArrayList(diffDepInfo.getReduceItems()), null);

            // 不存在於通知user列表中的 user 才移除(推撥)
            for (Integer reduceDepUserSid : reduceDepUserSids) {
                if (!afterNoticeUserSids.contains(reduceDepUserSid + "")) {
                    reduceUserSids.add(reduceDepUserSid);
                }
            }
        }

        // ====================================
        // 取消被通知單位、成員的推撥
        // ====================================
        this.workNotifyManager.delete(
                require.getRequireNo(),
                NotifyType.PROTOTPYE_NOTIFY,
                Lists.newArrayList(reduceUserSids),
                "被通知單位、成員移除");

        // ====================================
        // 執行
        // ====================================
        this.processByUser(
                require,
                ptCheckTheme,
                "",
                NotifyType.PROTOTPYE_NOTIFY,
                Lists.newArrayList(addUserSids),
                execUserSid,
                true);
    }

    // ========================================================================
    // 客製方法區 （對外呼叫接口）- 其他資訊
    // ========================================================================
    /**
     * 執行通知 for 新增 其他資訊
     * 
     * @param requireSid    需求單 sid
     * @param otherSetTheme 其他資訊主題
     * @param noticeDepSids 新增的通知單位
     * @param execUserSid   執行者
     */
    public void processForAddOtherSet(
            String requireSid,
            String otherSetTheme,
            List<String> beforeNoticeDepSids,
            List<String> afterNoticeDepSids,
            Integer execUserSid) {

        // ====================================
        // 查詢需求單主檔
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);
        if (require == null) {
            log.warn("找不到需求單主檔資料! requireSid:[{}]", requireSid);
            return;
        }

        // ====================================
        // 計算通知單位, 新增或減少
        // ====================================
        ItemsCollectionDiffTo<Integer> diffInfo = this.prepareNoticeDepsDiffInfo(
                beforeNoticeDepSids,
                afterNoticeDepSids);

        // ====================================
        // 移除被移除單位推撥
        // ====================================
        this.workNotifyManager.deleteByDeps(
                require.getRequireNo(),
                NotifyType.OTHER_NOTIFY,
                Lists.newArrayList(diffInfo.getReduceItems()),
                "移除其他資訊單通知單位");

        // ====================================
        // 執行
        // ====================================
        this.processByDep(
                require,
                otherSetTheme,
                "",
                NotifyType.OTHER_NOTIFY,
                Lists.newArrayList(diffInfo.getPlusItems()),
                execUserSid,
                true);
    }

    // ========================================================================
    // 附加方法
    // ========================================================================
    /**
     * 取得需求單分派單位的成員
     * 
     * @param requireSid 需求單 sid
     * @return
     */
    private List<Integer> findAssignDepMemberByRequire(String requireSid) {
        // ====================================
        // 查詢分派單位
        // ====================================
        Map<AssignSendType, AssignSendInfo> settingMap = this.assignSendInfoService.findByRequireSidAndStatus(requireSid);

        // ====================================
        // 取得分派單位設定
        // ====================================
        List<Integer> assignDepSids = Lists.newArrayList();
        if (settingMap.containsKey(AssignSendType.ASSIGN)) {
            assignDepSids = this.assignSendInfoService.prepareDeps(
                    settingMap.get(AssignSendType.ASSIGN));
        }
        if (WkStringUtils.isEmpty(assignDepSids)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 取得所有分派單位成員
        // ====================================
        return WkUserCache.getInstance().findUserWithManagerByOrgSids(assignDepSids, null).stream()
                .map(User::getSid)
                .collect(Collectors.toList());
    }

    /**
     * 取得【需求回覆補充】相關人員
     * 
     * @param fbkReply
     * @return
     */
    public List<Integer> findNeedNotifyUserByAgainReply(ReqFbkReply fbkReply) {
        // 加入【需求回覆補充】建立者
        Set<Integer> userSids = Sets.newHashSet(fbkReply.getTrace().getCreatedUser().getSid());

        // 加入所有回覆者
        List<User> users = reqFbReplyRepo.findPersonByTrace(fbkReply.getTrace());
        if (WkStringUtils.notEmpty(users)) {
            for (User user : users) {
                userSids.add(user.getSid());
            }
        }
        return Lists.newArrayList(userSids);
    }

    /**
     * 比對
     * 
     * @param beforeNoticeDepSids
     * @param afterNoticeDepSids
     * @return
     */
    private ItemsCollectionDiffTo<Integer> prepareNoticeDepsDiffInfo(
            List<String> beforeNoticeDepSids,
            List<String> afterNoticeDepSids) {
        // ====================================
        // 1.排除錯誤資料 (非數字)
        // 2.複製一份, 避免影響原本資料
        // ====================================
        if (beforeNoticeDepSids == null) {
            beforeNoticeDepSids = Lists.newArrayList();
        }
        List<Integer> currBeforeNoticeDepSids = Lists.newArrayList(beforeNoticeDepSids).stream()
                .filter(sid -> WkStringUtils.isNumber(sid))
                .map(sid -> Integer.parseInt(sid))
                .collect(Collectors.toList());

        if (afterNoticeDepSids == null) {
            afterNoticeDepSids = Lists.newArrayList();
        }
        List<Integer> currAfterNoticeDepSids = Lists.newArrayList(afterNoticeDepSids).stream()
                .filter(sid -> WkStringUtils.isNumber(sid))
                .map(sid -> Integer.parseInt(sid))
                .collect(Collectors.toList());

        // ====================================
        // 比對
        // ====================================
        return WkCommonUtils.itemCollectionDiff(currBeforeNoticeDepSids, currAfterNoticeDepSids);
    }

    // ========================================================================
    // 核心方法區
    // ========================================================================
    /**
     * 依據使用者建立通知 mail + push (執行者不會收到)
     * 
     * @param require                需求單主檔
     * @param theme                  主題
     * @param themeAppendTextForPush 推撥時發送主題後面的添加文字
     * @param notifyType             通知類別
     * @param userSids               通知使用者 sid
     * @param execUserSid            執行者 sid
     */
    private void processByUser(
            Require require,
            String theme,
            String themeAppendTextForPush,
            NotifyType notifyType,
            Collection<Integer> userSids,
            Integer execUserSid,
            boolean isShowDebug
            ) {

        if (WkStringUtils.isEmpty(userSids)) {
            log.debug("系統通知：沒有傳入需要通知的user");
            return;
        }

        // ====================================
        // 推撥
        // ====================================
        this.sysNotifyByPushHelper.processNotifyByUser(
                require,
                theme + themeAppendTextForPush,
                notifyType,
                userSids,
                execUserSid,
                isShowDebug
                );

        // ====================================
        // Mail
        // ====================================
        this.sysNotifyByMailHelper.processNotifyByUser(
                require,
                theme,
                notifyType,
                userSids,
                execUserSid,
                isShowDebug);
    }

    /**
     * 依據部門建立通知 mail + push (執行者不會收到)
     * 
     * @param require                需求單主檔
     * @param theme                  主題
     * @param themeAppendTextForPush 推撥時發送主題後面的添加文字
     * @param notifyType             通知類別
     * @param userSids               通知使用者 sid
     * @param execUserSid            執行者 sid
     */
    private void processByDep(
            Require require,
            String theme,
            String themeAppendTextForPush,
            NotifyType notifyType,
            List<Integer> depSids,
            Integer execUserSid,
            boolean isShowDebug
            ) {

        if (WkStringUtils.isEmpty(depSids)) {
            //log.debug("系統通知：沒有傳入需要通知的部門");
            return;
        }

        // ====================================
        // 推撥
        // ====================================
        this.sysNotifyByPushHelper.processNotifyByDeps(
                require,
                theme + themeAppendTextForPush,
                notifyType,
                depSids,
                execUserSid,
                isShowDebug);

        // ====================================
        // Mail
        // ====================================
        this.sysNotifyByMailHelper.processNotifyByDeps(
                require,
                theme,
                notifyType,
                depSids,
                execUserSid,
                isShowDebug);
    }

}
