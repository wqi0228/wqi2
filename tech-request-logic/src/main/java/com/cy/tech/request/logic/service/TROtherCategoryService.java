/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.repository.category.TROtherCategoryRepository;
import com.cy.tech.request.vo.category.TROtherCategory;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 第二分類 服務
 *
 * @author kasim
 */
@Component
public class TROtherCategoryService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5424593906088715956L;
    @Autowired
    private TROtherCategoryRepository otherCategoryDao;

    /**
     * 過濾資料
     *
     * @param filterStatus
     * @param filterName LIKE %分類名稱%
     * @return
     */
    public List<TROtherCategory> findByStatusAndName(Activation filterStatus, String filterName) {
        return otherCategoryDao.findAll().stream()
                .filter(each -> (filterStatus != null && filterStatus.equals(each.getStatus()))
                        || filterStatus == null)
                .filter(each -> (!Strings.isNullOrEmpty(filterName) && each.getName().toLowerCase().contains(filterName.toLowerCase()))
                        || Strings.isNullOrEmpty(filterName))
                .collect(Collectors.toList());
    }

    /**
     * 抓取最即時資料
     *
     * @param sid
     * @return
     */
    public TROtherCategory findBySid(String sid) {
        return otherCategoryDao.findOne(sid);
    }

    /**
     * 進行更新
     *
     * @param obj
     * @param userSid
     */
    public TROtherCategory save(TROtherCategory obj, Integer userSid) {
        if (obj.getSid() != null) {
            obj.setUpdatedUser(userSid);
            obj.setUpdatedDate(new Date());
        } else {
            obj.setStatus(Activation.ACTIVE);
            obj.setCreatedUser(userSid);
            obj.setCreatedDate(new Date());
            obj.setUpdatedDate(obj.getCreatedDate());
        }
        return otherCategoryDao.save(obj);
    }

    /**
     * 判斷是否使用
     *
     * @param name
     * @param excludeSid
     * @return
     */
    public Boolean isAnyUseByNameAndExcludeSid(String name, String excludeSid) {
        return otherCategoryDao.isAnyUseByNameAndExcludeSid(name, excludeSid);
    }

    /**
     * 判斷是否使用
     *
     * @param name
     * @param excludeSid
     * @return
     */
    public Boolean isAnyUseByName(String name) {
        return otherCategoryDao.isAnyUseByName(name);
    }

    /**
     * 過濾資料
     *
     * @param filterStatus
     * @return
     */
    public List<TROtherCategory> findByStatus(Activation filterStatus) {
        return otherCategoryDao.findAll().stream()
                .filter(each -> (filterStatus != null && filterStatus.equals(each.getStatus()))
                        || filterStatus == null)
                .collect(Collectors.toList());
    }
}
