package com.cy.tech.request.logic.service.customer;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.CommonService;
import com.cy.tech.request.logic.vo.CustomerTo;
import com.cy.work.client.WorkCustomerSyncClient;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkEntityUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.customer.logic.WorkCustomerService;
import com.cy.work.customer.logic.WorkCustomerSyncService;
import com.cy.work.customer.repository.customer.WorkCustomerRepository;
import com.cy.work.customer.vo.WorkCustomer;
import com.cy.work.customer.vo.converter.CustomerTypeConverter;
import com.cy.work.customer.vo.enums.CustomerType;
import com.cy.work.customer.vo.enums.EnableType;
import com.cy.work.customer.vo.to.RetTo;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author kasim
 */
@Component
@Slf4j
public class ReqWorkCustomerHelper implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -9019316164916330492L;

    private static ReqWorkCustomerHelper instance;

    public static ReqWorkCustomerHelper getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        ReqWorkCustomerHelper.instance = this;
    }

    @Autowired
    private WorkCustomerService workCustomerService;

    @Autowired
    private WkStringUtils stringUtils;

    @Autowired
    private WkEntityUtils entityUtils;
    @Autowired
    private CommonService commonService;
    @Autowired
    private WorkCustomerSyncService workCustomerSyncService;
    @Autowired
    private transient WorkCustomerSyncClient workCustomerSyncClient;

    @Autowired
    transient private NativeSqlRepository nativeSqlRepository;
    @Autowired
    private WorkCustomerRepository workCustomerRepository;

    /**
     * 查詢全部
     *
     * @return
     */
    @Transactional(readOnly = true)
    public List<WorkCustomer> findAll() {
        return workCustomerService.findAll();
    }

    /**
     * 單一查詢
     *
     * @param obj
     * @return
     */
    @Transactional(readOnly = true)
    public WorkCustomer findCopyOne(Long sid) {
        WorkCustomer workCustomer = this.workCustomerService.findBySid(sid);
        return this.returnByCopy(workCustomer);
    }

    /**
     * 複製新的WorkCustomer物件
     *
     * @param source
     * @return
     */
    private WorkCustomer returnByCopy(WorkCustomer source) {
        if (source == null) {
            return null;
        }
        WorkCustomer target = new WorkCustomer();
        entityUtils.copyProperties(source, target);
        return target;
    }

    /**
     * 依據paramt查詢資料
     *
     * @param enable
     * @param name
     * @return
     */
    @Transactional(readOnly = true)
    public List<WorkCustomer> findByEnableAndName(EnableType enable, String name) {
        return workCustomerRepository.findByEnableAndName(enable,
                stringUtils.replaceIllegalSqlLikeStrAndAddLikeStr(name));
    }

    /**
     * 依據paramt查詢資料
     *
     * @param enable
     * @return
     */
    @Transactional(readOnly = true)
    public List<WorkCustomer> findByEnable(EnableType enable) {
        return workCustomerRepository.findByEnable(enable);
    }

    /**
     * @param enable
     * @return
     */
    @Transactional(readOnly = true)
    public WorkCustomer findByDomain(String domain) {
        return workCustomerRepository.findByDomain(domain);
    }

    /**
     * 依據paramt查詢資料
     *
     * @param name
     * @return
     */
    @Transactional(readOnly = true)
    public List<WorkCustomer> findByName(String name) {
        return workCustomerRepository.findByName(stringUtils.replaceIllegalSqlLikeStrAndAddLikeStr(name));
    }

    /**
     * 檢核
     *
     * @param obj
     * @return
     */
    public String checkSave(WorkCustomer obj) {
        try {
            if (obj.getSid() == null) {
                Preconditions.checkNotNull(obj.getCustomerType(), "類型尚未選擇，請確認！！");
                Preconditions.checkState(!Strings.isNullOrEmpty(obj.getLoginCode()), "後置碼為必要輸入欄位，請確認！！");
                Preconditions.checkState(
                        !this.isAnyWorkCustomerUseByCustomerTypeAndLoginCode(obj.getCustomerType(), obj.getLoginCode()),
                        "後置碼已存在，請確認！！");
            }
            Preconditions.checkState(!Strings.isNullOrEmpty(obj.getName()), "客戶名稱為必要輸入欄位，請確認！！");
            Preconditions.checkState(!Strings.isNullOrEmpty(obj.getAlias()), "客戶別名為必要輸入欄位，請確認！！");
        } catch (NullPointerException | IllegalStateException e) {
            log.error("客戶檢核失敗！！", e);
            return e.getMessage();
        }
        return "";
    }

    /**
     * 存檔
     *
     * @param obj
     * @param user
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public WorkCustomer save(WorkCustomer obj, User user) {
        if (obj.getSid() != null) {
            obj.setUpdatedDate(new Date());
            obj.setUpdatedUser(user);
        } else {
            obj.setCreatedDate(new Date());
            obj.setCreatedUser(user);
        }
        if (EnableType.DISABLED.equals(obj.getEnable()) && obj.getStopUser() == null) {
            obj.setStopUser(user);
            obj.setStopDate(new Date());
        }
        WorkCustomer result = workCustomerRepository.save(obj);
        return result;
    }

    /**
     * 判斷是否使用
     *
     * @param customerType
     * @param loginCode
     * @return
     */
    private Boolean isAnyWorkCustomerUseByCustomerTypeAndLoginCode(CustomerType customerType, String loginCode) {
        return workCustomerRepository.isAnyWorkCustomerUseByCustomerTypeAndLoginCode(customerType, loginCode);
    }

    /**
     * 判斷是否使用
     *
     * @param obj
     * @return
     */
    @Transactional(readOnly = true)
    public Boolean isAnyWorkCustomerUseByWorkCustomer(WorkCustomer obj) {
        if (obj.getSid() != null) {
            return workCustomerRepository.isAnyWorkCustomerUseByAliasAndExcludCustomer(obj.getAlias(), obj);
        }
        return workCustomerRepository.isAnyWorkCustomerUseByAlias(obj.getAlias());
    }

    /**
     * 依據paramt查詢資料
     *
     * @param alias
     * @return
     */
    @Transactional(readOnly = true)
    public WorkCustomer findByEnableAndCustomerTypeAndAlias(EnableType enable, CustomerType customerType,
            String alias) {
        List<WorkCustomer> workCustomers = this.findAll();
        for (WorkCustomer customer : workCustomers) {
            if (enable.equals(customer.getEnable()) && customerType.equals(customer.getCustomerType())
                    && alias.equals(customer.getAlias())) {
                return this.returnByCopy(customer);
            }
        }
        return null;
    }

    /**
     * 建立變更客戶資料文字
     *
     * @param msg
     * @param customer
     * @param oldCustomer
     * @return
     */
    public String buildChgCustomerText(String msg, WorkCustomer customer, WorkCustomer oldCustomer) {
        msg += "由【";
        if (oldCustomer != null) {
            msg += oldCustomer.getName();
        }
        msg += "】 變更為【";
        if (customer != null) {
            msg += customer.getName();
        }
        msg += "】";
        return msg;
    }

    /**
     * 轉入客戶資料
     *
     * @param createdUser
     * @param days
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public String transferCustomerSource(User createdUser, int days) {

        try {
            return this.workCustomerSyncService.syncWorkCustomer();
        } catch (Exception e) {
            return "執行失敗:" + e.getMessage();
        }
    }

    /**
     * 檢查連線
     */
    public String checkConnection() {

        try {
            List<RetTo> retTos = this.workCustomerSyncClient.process();
            return new com.google.gson.GsonBuilder().setPrettyPrinting().create().toJson(retTos);
        } catch (Exception e) {
            return "執行失敗:" + e.getMessage();
        }

    }

    public boolean containCustomer(List<WorkCustomer> customerPageItems, Long customerSid) {
        return customerPageItems.stream().anyMatch(each -> each.getSid().equals(customerSid));
    }

    /**
     * 轉換為自訂物件
     *
     * @param sids
     * @return
     * @throws Exception
     */
    public List<CustomerTo> findTosBySids(Set<Long> sids) throws Exception {

        // ====================================
        // 查詢
        // ====================================
        Map<Long, Map<String, Object>> results = this.nativeSqlRepository.selectMapBySingleKeyField(
                "work_customer_data",
                Lists.newArrayList("cus_id", "alias", "name", "login_code", "data_category"),
                "cus_id",
                sids);

        if (WkStringUtils.isEmpty(results)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 轉 CustomerTo
        // ====================================
        List<CustomerTo> toResults = Lists.newArrayList();
        CustomerTypeConverter converter = new CustomerTypeConverter();

        for (Map<String, Object> rowData : results.values()) {
            // 轉 CustomerType
            String customerTypeStr = String.valueOf(rowData.get("data_category"));
            CustomerType customerType = converter.convertToEntityAttribute(customerTypeStr);
            String customerTypeName = commonService.get(customerType);

            // 建立 CustomerTo
            toResults.add(new CustomerTo(
                    Long.valueOf(rowData.get("cus_id") + ""),
                    String.valueOf(rowData.get("alias")),
                    String.valueOf(rowData.get("name")),
                    String.valueOf(rowData.get("login_code")),
                    customerTypeName));
        }

        return toResults;
    }

    /**
     * 查詢廳主名稱Map
     * 
     * @param custIds
     * @return
     * @throws Exception
     */
    public Map<Integer, String> findNameMapByCustId(Set<Integer> custIds) throws Exception {

        Map<Integer, Map<String, Object>> results = this.nativeSqlRepository.selectMapBySingleKeyField(
                "work_customer_data",
                Lists.newArrayList("name"),
                "cus_id",
                custIds);

        Map<Integer, String> custResults = Maps.newHashMap();
        for (Entry<Integer, Map<String, Object>> entry : results.entrySet()) {
            custResults.put(entry.getKey(), String.valueOf(entry.getValue().get("name")));
        }

        return custResults;
    }
}
