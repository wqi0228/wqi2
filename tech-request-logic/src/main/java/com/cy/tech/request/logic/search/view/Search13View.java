/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import java.io.Serializable;
import java.util.Date;

import com.cy.tech.request.vo.worktest.enums.WorkTestInfoStatus;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.vo.value.to.JsonStringListTo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 送測狀況一覽表頁面顯示vo (search13.xhtml)
 *
 * @author jason_h
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Search13View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7224631781015697569L;
    /** 需求單-緊急度 */
    private UrgencyType urgency;
    /** 建立日期 */
    private Date createdDate;
    /** 填單人所歸屬的部門 */
    private Integer createDep;
    private String createDepName;
    /** 建立者 */
    private Integer createdUser;
    /** 送測主題 */
    private String testTheme;
    /** 需求類別 */
    private String bigName;
    /** 中類 */
    private String middleName;
    /** 小類 */
    private String smallName;
    /** 需求單位 */
    private Integer requireDep;
    /** 需求人員 */
    private Integer requireUser;
    /** 送測單狀態 */
    private WorkTestStatus testinfoStatus;
    /** 送測-QA審核狀態 */
    private WorkTestInfoStatus qaAuditStatus;
    /** 說明 (退測 或是 取消測試時 或重測時) */
    private String testDescription;
    /** 預計完成日 */
    private Date establishDate;
    /** 送測完成日 */
    private Date finishDate;
    /** 異動日 */
    private Date readUpdateDate;
    /** 送測單號 */
    private String testinfoNo;
    /** 送測單位 */
    private JsonStringListTo sendTestDep;
    /** 本地端連結網址 */
    private String localUrlLink;
    /** 送測日 */
    private Date testDate;
    /**預計上線日 */
    private Date expectOnlineDate;
}
