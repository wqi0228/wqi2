/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.search.view.Search29View;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.utils.SimpleDateFormatEnum;
import com.cy.tech.request.logic.utils.ToolsDate;
import com.cy.tech.request.vo.enums.RequireFinishCodeType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * @author brain
 */
@Service
@Slf4j
public class Search29QueryService implements QueryService<Search29View>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1535283416507377444L;
    @Autowired
    private URLService urlService;
    @Autowired
    private SearchService searchHelper;

    @PersistenceContext
    transient private EntityManager em;

    private Map<String, RequireFinishCodeType> finishCodeMap;

    @PostConstruct
    public void init() {
        finishCodeMap = Maps.newHashMap();
        for (RequireFinishCodeType each : RequireFinishCodeType.values()) {
            finishCodeMap.put(each.getCode(), each);
        }
    }

    @Override
    public List<Search29View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {
        // ====================================
        // 查詢
        // ====================================
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        @SuppressWarnings("unchecked")
        List<Object[]> result = query.getResultList();
        //資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if(WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }

        List<Search29View> viewResult = Lists.newArrayList();

        for (int i = 0; i < result.size(); i++) {

            Search29View v = new Search29View();
            Object[] record = result.get(i);

            int index = 0;

            String sid = (String) record[index++];
            String requireNo = (String) record[index++];
            String requireTheme = (String) record[index++];
            Integer urgency = (Integer) record[index++];
            Boolean hasForwardDep = (Boolean) record[index++];
            Boolean hasForwardMember = (Boolean) record[index++];
            Boolean hasLink = (Boolean) record[index++];
            Date createdDate = (Date) record[index++];
            String bigName = (String) record[index++];
            String middleName = (String) record[index++];
            String smallName = (String) record[index++];
            Integer createDep = (Integer) record[index++];
            Integer createdUser = (Integer) record[index++];
            String requireStatus = (String) record[index++];
            Boolean checkAttachment = (Boolean) record[index++];
            Boolean hasAttachment = (Boolean) record[index++];
            Date hopeDate = (Date) record[index++];
            Integer inteStaffUser = (Integer) record[index++];

            Date requireFinishDate = (Date) record[index++];
            String requireFinishDateStr = "";
            try {
                requireFinishDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), requireFinishDate);
            } catch (Exception e) {
                log.error("requireFinishDate ERROR", e);
            }

            String finishCode = (String) record[index++];

            // REQ-1031
            Date requireEstablishDate = (Date) record[index++];

            // 處理共通欄位 (一定要擺在『取值』最後面, 否則 index 會不對)
            v.prepareCommonColumn(record, index);

            v.setSid(sid);
            v.setRequireNo(requireNo);
            v.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
            v.setUrgency(UrgencyType.values()[urgency]);
            v.setHasForwardDep(hasForwardDep);
            v.setHasForwardMember(hasForwardMember);
            v.setHasLink(hasLink);
            v.setCreatedDate(createdDate);
            v.setBigName(bigName);
            v.setMiddleName(middleName);
            v.setSmallName(smallName);
            v.setCreateDep(createDep);
            v.setCreatedUser(createdUser);
            v.setRequireStatus(RequireStatusType.valueOf(requireStatus));
            v.setCheckAttachment(checkAttachment);
            v.setHasAttachment(hasAttachment);
            v.setHopeDate(hopeDate);

            // REQ-1031
            v.setExecDays(
                    this.searchHelper.calcExecDays(
                            v.getRequireStatus(),
                            requireEstablishDate,
                            requireFinishDate));

            v.setLocalUrlLink(urlService.createLoacalURLLink(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(execUserSid, v.getRequireNo(), 1)));
            v.setInteStaffUser(inteStaffUser);
            v.setFinishCode(finishCodeMap.get(finishCode));
            v.setRequireFinishDate(requireFinishDateStr);
            viewResult.add(v);
        }
        return viewResult;
    }
}
