/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.vo;

import com.cy.bpm.rest.vo.client.ISign;
import com.cy.commons.vo.User;
import java.util.Map;
import lombok.Getter;

/**
 *
 * @author shaun
 */
public class BpmSignTo implements ISign {

    /**
     * 
     */
    private static final long serialVersionUID = 7376187378669975630L;
    @Getter
    private final String instanceId;
    @Getter
    private final String executorId;
    @Getter
    private final String comment;
    @Getter
    transient private final Map<String, Object> parameter;

    public BpmSignTo(String instanceId, User executor, String comment, Map<String, Object> parameter) {
        this.instanceId = instanceId;
        this.executorId = executor.getId();
        this.comment = comment;
        this.parameter = parameter;
    }
}
