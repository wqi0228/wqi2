/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.pt;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.InstanceStatus;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 送測顯示處理
 *
 * @author shaun
 */
@Component
public class PtShowService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2692378178624724518L;
    @Autowired
    private PtReplyService replyService;
    @Autowired
    private UserService userService;
    @Autowired
    private OrganizationService orgService;

    /**
     * 組合送測單位抬頭訊息
     *
     * @param ptCheck
     * @return
     */
    public String findNoticeDepTitle(PtCheck ptCheck) {
        if (ptCheck == null || ptCheck.getNoticeDeps() == null) {
            return "";
        }
        return ptCheck.getNoticeDeps().getValue().stream()
                .map(each -> orgService.getOrgName(each))
                .sorted()
                .collect(Collectors.joining("、"));
    }

    /**
     * 組合送測單位抬頭訊息
     *
     * @param ptCheck
     * @return
     */
    public List<String> findNoticeDepTitleByOp(PtCheck ptCheck) {
        if (ptCheck == null || ptCheck.getNoticeDeps() == null) {
            return Lists.newArrayList();
        }
        return ptCheck.getNoticeDeps().getValue().stream()
                .map(each -> orgService.getOrgName(each))
                .sorted()
                .collect(Collectors.toList());
    }

    /**
     * 組合送測單位抬頭訊息
     *
     * @param ptCheck
     * @return
     */
    public String findNoticeMemberTitle(PtCheck ptCheck) {
        if (ptCheck == null || ptCheck.getNoticeMember() == null) {
            return "";
        }
        return ptCheck.getNoticeMember().getValue().stream()
                .map(each -> userService.getUserName(each))
                .sorted()
                .collect(Collectors.joining("、"));
    }

    /**
     * 組合送測單位抬頭訊息
     *
     * @param ptCheck
     * @return
     */
    public List<String> findNoticeMemberTitleByOp(PtCheck ptCheck) {
        if (ptCheck == null || ptCheck.getNoticeMember() == null) {
            return Lists.newArrayList();
        }
        return ptCheck.getNoticeMember().getValue().stream()
                .map(each -> userService.getUserName(each))
                .sorted()
                .collect(Collectors.toList());
    }

    /**
     * 關閉編輯
     *
     * @param req
     * @param ptCheck
     * @param login
     * @return
     */
    public boolean disableEdit(Require req, PtCheck ptCheck, User login) {
        if (ptCheck == null || ptCheck.getPtStatus() == null
                || req == null || req.getCloseCode()
                || req.getRequireStatus().equals(RequireStatusType.SUSPENDED)
                || req.getRequireStatus().equals(RequireStatusType.COMPLETED)) {
            return Boolean.TRUE;
        }
        PtStatus rpStatus = ptCheck.getPtStatus();
        if (!rpStatus.equals(PtStatus.SIGN_PROCESS) && !rpStatus.equals(PtStatus.APPROVE) && !rpStatus.equals(PtStatus.PROCESS)) {
            return Boolean.TRUE;
        }
        User createUser = WkUserCache.getInstance().findBySid(ptCheck.getCreatedUser().getSid());
        List<User> canClickDisableList = orgService.findOrgManagers(orgService.findBySid(createUser.getPrimaryOrg().getSid()));
        if (!canClickDisableList.contains(createUser)) {
            canClickDisableList.add(createUser);
        }
        //被通知的成員
        boolean isNoticeMember = ptCheck.getNoticeMember().getValue().contains(String.valueOf(login.getSid()));
        //填寫此原型確認工程師　以及工程師主管或被通知的成員可以進行編輯 
        Boolean userCanEdit = canClickDisableList.contains(login) || isNoticeMember;
        //尚未有任何　原型確認回覆資訊時
        Boolean isEmptyReplys = replyService.findReply(ptCheck).isEmpty();
        return !(userCanEdit && isEmptyReplys);
    }

    /**
     * 檢查登入者是否為填單者及其上層主管
     *
     * @param ptCheck
     * @param login
     * @return
     */
    public boolean isCreatedAndAboveManager(PtCheck ptCheck, User login) {
        User createUser = WkUserCache.getInstance().findBySid(ptCheck.getCreatedUser().getSid());
        List<User> canClickDisableList = orgService.findOrgManagers(orgService.findBySid(createUser.getPrimaryOrg().getSid()));
        if (!canClickDisableList.contains(createUser)) {
            canClickDisableList.add(createUser);
        }
        return canClickDisableList.contains(login);
    }

    /**
     * 關閉測試回覆
     *
     * @param req
     * @param ptCheck
     * @param login
     * @return
     */
    public boolean disableReply(Require req, PtCheck ptCheck, User login) {
        Boolean isApproved = ptCheck.getSignInfo() == null ? Boolean.TRUE
                : ptCheck.getSignInfo().getInstanceStatus().equals(InstanceStatus.APPROVED);
        Boolean isFunctionOk = req.getFunctionOkCode();
        return !isApproved && !isFunctionOk;
    }

    /**
     * 關閉重做<BR/>
     * 僅開放原型確認的填單人員與其主管能使用此功能
     *
     * @param req
     * @param ptCheck
     * @param login
     * @return
     */
    public boolean disableRedo(Require req, PtCheck ptCheck, User login) {
        if (ptCheck == null || ptCheck.getPtStatus() == null
                || !ptCheck.getPtStatus().equals(PtStatus.PROCESS)
                || req == null || req.getCloseCode()
                || req.getRequireStatus().equals(RequireStatusType.SUSPENDED)
                || req.getRequireStatus().equals(RequireStatusType.COMPLETED)) {
            return Boolean.TRUE;
        }
        User createUser = WkUserCache.getInstance().findBySid(ptCheck.getCreatedUser().getSid());
        List<User> canClickRedoList = orgService.findOrgManagers(orgService.findBySid(createUser.getPrimaryOrg().getSid()));
        if (!canClickRedoList.contains(createUser)) {
            canClickRedoList.add(createUser);
        }
        Boolean userCanRedo = canClickRedoList.contains(login);
        return !userCanRedo;
    }

    /**
     * 關閉功能符合需求鍵
     *
     * @param req
     * @param ptCheck
     * @param login
     * @return
     */
    public boolean disableFunctionConform(Require req, PtCheck ptCheck, User login) {
        if (ptCheck == null || ptCheck.getPtStatus() == null
                || !ptCheck.getPtStatus().equals(PtStatus.PROCESS)
                || req == null || req.getCloseCode()
                || req.getRequireStatus().equals(RequireStatusType.SUSPENDED)
                || req.getRequireStatus().equals(RequireStatusType.COMPLETED)) {
            return Boolean.TRUE;
        }
        User reqCreateUser = WkUserCache.getInstance().findBySid(req.getCreatedUser().getSid());
        List<User> canClickDisableList = orgService.findOrgManagers(orgService.findBySid(reqCreateUser.getPrimaryOrg().getSid()));
        //2016-06-24 調整包含開單人也需可以使用符合需求功能
        Boolean isRequireUnitManager = canClickDisableList.contains(login) || (req.getCreatedUser().getSid().equals(login.getSid()));//僅開放需求單位主管使用
        Boolean isFunctionOk = req.getFunctionOkCode(); //符合後關閉
        return !isRequireUnitManager || isFunctionOk;
    }

    /**
     * 顯示簽核資訊查詢(渲染)
     *
     * @param req
     * @param ptCheck
     * @return
     */
    public boolean showSignInfo(Require req, PtCheck ptCheck) {
        if (ptCheck == null) {
            return false;
        }
        return ptCheck.getHasSign();
    }

    /**
     * 關閉原型確認附加檔案上傳功能
     *
     * @param ptCheck
     * @return
     */
    public boolean disableUploadFun(Require req, PtCheck ptCheck) {
        if (ptCheck == null || req == null || req.getCloseCode()
                || req.getRequireStatus().equals(RequireStatusType.SUSPENDED)
                || req.getRequireStatus().equals(RequireStatusType.COMPLETED)) {
            return Boolean.TRUE;
        }
        PtStatus status = ptCheck.getPtStatus();
        return status.equals(PtStatus.APPROVE) || status.equals(PtStatus.VERIFY_INVAILD) || status.equals(PtStatus.REDO);
    }

    public boolean disableEditNotify(Require req, PtCheck ptCheck, User viewer) {
        //20160831調整含通知單位成員也可編輯
        Integer viewerPrimaryOrgSid = userService.findPrimaryOrgSid(viewer);
        return !(ptCheck.getNoticeMember().getValue().contains(viewer.getSid().toString())
                || (ptCheck.getNoticeDeps() != null && ptCheck.getNoticeDeps().getValue().contains(viewerPrimaryOrgSid.toString()))
                || ptCheck.getCreatedUser().getSid().equals(viewer.getSid()));
    }
}
