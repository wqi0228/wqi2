/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 送測文件提交區頁面顯示vo (search32.xhtml)
 *
 * @author marlow_chen
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Search32View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7920770101905150907L;
    /** 送測日 */
    private Date testDate;
    /** 填單人所歸屬的部門 */
    private Integer createDep;
    /** 建立者 */
    private Integer createdUser;    
    /** 送測主題 */
    private String testTheme;    
    /** 提交狀態 */
    private String commitStatus;    
    /** 建立日期 */
    private Date createdDate;
    /** 預計完成日 */
    private Date establishDate;
    /** 本地端連結網址 */
    private String localUrlLink;
}
