/**
 * 
 */
package com.cy.tech.request.logic.service.setting.onetimetrns;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.logic.service.setting.onetimetrns.to.RequireDepRebuildTo;
import com.cy.tech.request.vo.converter.ReqConfirmDepProgStatusConverter;
import com.cy.tech.request.vo.enums.AssignSendType;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Slf4j
@Service
public class OneTimeTrnsForRequireDepCommonService implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 5450761924347933829L;

    // ========================================================================
    // 服務區
    // ========================================================================
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private JdbcTemplate jdbcTemplate;

    // ========================================================================
    // 方法區
    // ========================================================================

    /**
     * @param requireSids
     * @return
     */
    public Map<String, List<RequireDepRebuildTo>> findRequireConfirmDeps(Set<String> requireSids) {

        String requireSidsSql = requireSids.stream()
                .collect(Collectors.joining("', '", "AND require_sid IN ('", "')"));

        // ====================================
        // 查詢
        // ====================================
        StringBuffer varname1 = new StringBuffer();
        varname1.append("SELECT "
                + "require_confirm_sid, "
                + "require_sid, "
                + "dep_sid, "
                + "owner_sid,"
                + "prog_status ");
        varname1.append("FROM   tr_require_confirm_dep  ");
        varname1.append("WHERE  status = 0 ");
        varname1.append(" " + requireSidsSql);

        List<Map<String, Object>> dataMaps = this.jdbcTemplate.queryForList(varname1.toString());

        log.debug("需求單位檔:[{}]筆", dataMaps.size());

        // ====================================
        // 組裝
        // ====================================
        ReqConfirmDepProgStatusConverter reqConfirmDepProgStatusConverter = new ReqConfirmDepProgStatusConverter();
        List<RequireDepRebuildTo> requireDepRebuildTos = dataMaps.stream()
                .map(dataMap -> {
                    return new RequireDepRebuildTo(
                            dataMap.get("require_confirm_sid") + "",
                            dataMap.get("require_sid") + "",
                            Integer.parseInt(dataMap.get("dep_sid") + ""),
                            Integer.parseInt(dataMap.get("owner_sid") + ""),
                            reqConfirmDepProgStatusConverter.convertToEntityAttribute(dataMap.get("prog_status") + ""));
                })
                .collect(Collectors.toList());

        // ====================================
        // 分組
        // ====================================
        Map<String, List<RequireDepRebuildTo>> requireDepRebuildTosMapByRequireSid = requireDepRebuildTos.stream()
                .collect(Collectors.groupingBy(
                        RequireDepRebuildTo::getRequireSid,
                        Collectors.mapping(
                                each -> each,
                                Collectors.toList())));

        return requireDepRebuildTosMapByRequireSid;
    }

    public Map<String, Set<Integer>> findAssignNoticeDepSids(Set<String> requireSids, AssignSendType assignSendType) {

        // ====================================
        // 查詢
        // ====================================
        String requireSidsSql = requireSids.stream()
                .collect(Collectors.joining("', '", "AND require_sid IN ('", "')"));

        StringBuffer varname1 = new StringBuffer();
        varname1.append("SELECT require_sid, dep_sid ");
        varname1.append("FROM   tr_assign_send_search_info  ");
        varname1.append("WHERE  type = " + assignSendType.ordinal() + " ");
        varname1.append(" " + requireSidsSql + "  ");
        List<Map<String, Object>> dataMaps = this.jdbcTemplate.queryForList(varname1.toString());

        log.debug("{}檔:[{}]筆", assignSendType.getLabel(), dataMaps.size());

        // ====================================
        // 以 需求單號分組
        // ====================================
        return dataMaps.stream()
                .collect(Collectors.groupingBy(
                        dataMap -> dataMap.get("require_sid") + "",
                        Collectors.mapping(
                                dataMap -> Integer.parseInt(dataMap.get("dep_sid") + ""),
                                Collectors.toSet())));

    }

    /**
     * @param requireDepRebuildTo
     */
    public void updateDepSid(RequireDepRebuildTo requireDepRebuildTo) {
        String sql = "UPDATE tr_require_confirm_dep SET dep_sid=%s WHERE require_confirm_sid='%s';";
        sql = String.format(
                sql,
                requireDepRebuildTo.getDepSid(),
                requireDepRebuildTo.getSid());
        this.jdbcTemplate.execute(sql);
    }

}
