/**
 * 
 */
package com.cy.tech.request.logic.service.pps6;

import java.io.InputStream;
import java.io.Serializable;
import java.io.StringReader;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.vo.pps6.Pps6ModelerExecSettingVO;
import com.cy.tech.request.vo.pps6.Pps6ModelerFlowVO;
import com.cy.tech.request.vo.pps6.Pps6ModelerFormVO;
import com.cy.tech.request.vo.pps6.enums.Pps6ModelerExecType;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 *
 */
@Service
@Slf4j
public class Pps6ModelerService implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 4971858719041827551L;
    // ========================================================================
	// 服務區
	// ========================================================================
	@Autowired
	private transient Pps6ModelerFormService pps6ModelerFormService;
	@Autowired
	private transient Pps6ModelerFlowService pps6ModelerFlowService;
	@Autowired
	private transient Pps6ModelerExecSettingService pps6ModelerExecSettingService;
	@Autowired
	private transient Pps6ModelerBPMService pps6ModelerBPMService;
	@Autowired
	private transient WkOrgCache wkOrgCache;

	// ========================================================================
	// 變數區
	// ========================================================================
	private final String targetNodeName = "RelevantDataValue";
	private final String keyWordValue = "com.aboveE.powerProcess.organization.element.RoleAssignBean";
	private final List<String> PASS_DATA = Lists.newArrayList("<流程起始者>");
	private final List<String> PASS_USER = Lists.newArrayList("admin");
	private DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

	// ========================================================================
	// 方法區
	// ========================================================================
	/**
	 * 準備所有資料
	 * 
	 * @return
	 * @throws UserMessageException
	 */
	public List<Pps6ModelerExecSettingVO> prepareAllData() throws UserMessageException {

		// ====================================
		// 查詢單據定義檔
		// ====================================
		// 查詢全部
		List<Pps6ModelerFormVO> formVOs = this.pps6ModelerFormService.findAll();
		// 建立索引
		Map<Integer, Pps6ModelerFormVO> formVOMapBySid = formVOs.stream()
		        .collect(
		                Collectors.toMap(
		                        Pps6ModelerFormVO::getSid,
		                        each -> each));

		// ====================================
		// 查詢流程檔
		// ====================================
		// 查詢全部
		List<Pps6ModelerFlowVO> flowVOs = this.pps6ModelerFlowService.findAll();
		// 沒有流程主檔 無需往下處理
		if (WkStringUtils.isEmpty(flowVOs)) {
			return Lists.newArrayList();
		}
		// 組裝單據名稱定義資料
		for (Pps6ModelerFlowVO flowVO : flowVOs) {
			if (formVOMapBySid.containsKey(flowVO.getFormSid())) {
				flowVO.setFormVO(formVOMapBySid.get(flowVO.getFormSid()));
			}
		}

		// 依據單據名稱排序
		flowVOs = flowVOs.stream()
		        .sorted(Comparator.comparing(Pps6ModelerFlowVO::getFormName))
		        .collect(Collectors.toList());

		// ====================================
		// 查詢執行設定檔
		// ====================================
		// 查詢
		List<Pps6ModelerExecSettingVO> execSettings = this.pps6ModelerExecSettingService.findAll();

		// 建立索引 （groupingBy）
		Map<Integer, List<Pps6ModelerExecSettingVO>> execSettingsMapByFlowSid = execSettings.stream()
		        .collect(Collectors.groupingBy(
		                Pps6ModelerExecSettingVO::getFlowSid,
		                Collectors.mapping(
		                        each -> each,
		                        Collectors.toList())));

		// ====================================
		// 查詢執行角色的使用者
		// ====================================
		// 收集執行角色ID (不重複)
		Set<String> execRoleIDs = execSettings.stream()
		        .filter(vo -> !vo.isVariable()) // 不為變數
		        .filter(vo -> Pps6ModelerExecType.ROLE.equals(vo.getExecType())) // 執行類型為角色
		        .map(Pps6ModelerExecSettingVO::getExecId)
		        .collect(Collectors.toSet());

		// 由 bpm 查回角色下的 user
		Map<String, List<String>> roleUsersSetByRoleID = Maps.newHashMap();
		try {
			roleUsersSetByRoleID = this.pps6ModelerBPMService.findRoleUser(execRoleIDs);
		} catch (ProcessRestException e) {
			String message = "由 bpm 取回角色的使用者失敗!" + e.getMessage();
			log.error(message, e);
			throw new UserMessageException(message, InfomationLevel.ERROR);
		}

		// ====================================
		// 整裡輸出資料
		// ====================================
		List<Pps6ModelerExecSettingVO> results = Lists.newArrayList();

		// 依據流程資料逐項處理
		for (Pps6ModelerFlowVO flowVO : flowVOs) {

			if (execSettingsMapByFlowSid.containsKey(flowVO.getSid())) {

				List<Pps6ModelerExecSettingVO> resultsByFlow = execSettingsMapByFlowSid.get(flowVO.getSid());
				
				for (Pps6ModelerExecSettingVO pps6ModelerExecSettingVO : resultsByFlow) {
					//設定流程資訊物件
					pps6ModelerExecSettingVO.setFlowVO(flowVO);

					//werp 對應資訊
					if (!pps6ModelerExecSettingVO.isVariable()) {
						String werpMappingInfo = "";
						
						List<String> userIDs = Lists.newArrayList();
						
						//執行類型為『角色』
						if (Pps6ModelerExecType.ROLE.equals(pps6ModelerExecSettingVO.getExecType())) {
							userIDs = roleUsersSetByRoleID.get(pps6ModelerExecSettingVO.getExecId());
							if (WkStringUtils.notEmpty(userIDs)) {
								werpMappingInfo = this.prepareUserInfo(userIDs);
							} else {
								werpMappingInfo = "<span class='WS1-1-2'>此角色已無對應使用者</span>";
							}
						}
						//執行類型為『使用者』
						if (Pps6ModelerExecType.USER.equals(pps6ModelerExecSettingVO.getExecType())) {
							userIDs.add(pps6ModelerExecSettingVO.getExecId());
							werpMappingInfo = this.prepareUserInfo(Lists.newArrayList(pps6ModelerExecSettingVO.getExecId()));
						}
						
						pps6ModelerExecSettingVO.setWerpMappingUserIDs(userIDs);
						pps6ModelerExecSettingVO.setWerpMappingInfo(werpMappingInfo);
					}
				}

				// 加入該項目所有流程
				results.addAll(resultsByFlow);
			} else {
				// 流程沒有特殊角色時，也要建一筆 (用於顯示)
				Pps6ModelerExecSettingVO execSettingVO = new Pps6ModelerExecSettingVO();
				execSettingVO.setFlowVO(flowVO);
				execSettingVO.setName("此流程沒有執行定義");
			}
		}

		// ====================================
		// 回傳
		// ====================================
		return results;

	}

	private String prepareUserInfo(List<String> userIDs) {
		List<String> userInfos = Lists.newArrayList();

		for (String userID : userIDs) {
			User user = WkUserCache.getInstance().findByIdAndNotCheckEmpty(userID);
			if (userID == null) {
				userInfos.add("BPM回傳 UserID 對應不到 werp 資料:[" + userID + "]");
				continue;
			}

			String userName = user.getName() + " (" + user.getId() + ")";
			if (!WkUserUtils.isActive(user)) {
				userName = WkHtmlUtils.addStrikethroughStyle(userName, "停用");
			}

			Org org = this.wkOrgCache.findBySid(user.getPrimaryOrg().getSid());

			String depName = WkOrgUtils.prepareBreadcrumbsByDepNameAndMakeup(
			        org.getSid(),
			        OrgLevel.DIVISION_LEVEL,
			        false,
			        "-");

			if (!WkOrgUtils.isActive(org)) {
				depName = WkHtmlUtils.addStrikethroughStyle(depName, "停用");
			}

			userInfos.add("部門：" + depName + ", 使用者：" + userName);
		}

		return String.join("<br/>", userInfos);
	}

	/**
	 * 執行檔案分析
	 * 
	 * @param zipFileInputStream
	 * @param execUserSid
	 * @return
	 * @throws SystemOperationException
	 */
	@Transactional(rollbackFor = Exception.class)
	public String processImport(
	        InputStream zipFileInputStream,
	        Integer execUserSid) throws SystemOperationException {

		// ====================================
		// 解析 *.pf (XML) 文件
		// ====================================
		// root
		Element root = null;
		try {
			root = this.findPfFile(zipFileInputStream);
			if (root == null) {
				throw new SystemOperationException("解析檔案失敗 （讀不到資料!）", InfomationLevel.WARN);
			}
		} catch (Exception e) {
			log.error("解析檔案失敗", e);
			throw new SystemOperationException("解析檔案失敗:" + e.getMessage(), InfomationLevel.WARN);
		}

		NodeList nodeList = root.getChildNodes();
		if (nodeList == null || nodeList.getLength() == 0) {
			throw new SystemOperationException("解析檔案失敗 （找不到任何子節點資料!）", InfomationLevel.WARN);
		}

		// ====================================
		// 流程資料
		// ====================================
		// 取得 xml 資料
		String flowId = this.getAttrVal(root, "name");
		String flowName = this.getAttrVal(root, "display");

		// 建立或取得已存在資料
		Pps6ModelerFlowVO pps6ModelerFlowVO = this.pps6ModelerFlowService.findAndSave(
		        flowId,
		        flowName,
		        execUserSid);

		String infoMsg = "\r\n"
		        + "＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝\r\n"
		        + "流程代號:" + pps6ModelerFlowVO.getFlowId() + "\r\n"
		        + "流程名稱:" + pps6ModelerFlowVO.getFlowName() + "\r\n"
		        + "＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝\r\n";

		log.info(infoMsg);

		// ====================================
		// 遞迴節點, 解析資料內容
		// ====================================
		List<Pps6ModelerExecSettingVO> execSettingVOs = Lists.newArrayList();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			this.processNode(node, execSettingVOs);
		}

		// ====================================
		// 更新執行設定檔
		// ====================================
		this.pps6ModelerExecSettingService.deleteInsert(flowId, execSettingVOs, execUserSid);

		// ====================================
		// 回傳流程名稱
		// ====================================
		return flowName;
	}

	/**
	 * @param zipFileInputStream
	 * @return
	 * @throws SystemOperationException
	 */
	private Element findPfFile(InputStream zipFileInputStream) throws SystemOperationException {

		Element xmlRoot = null;
		ZipInputStream zipInputStream = null;
		try {
			zipInputStream = new ZipInputStream(zipFileInputStream);
			ZipEntry zipEntry = null;

			while ((zipEntry = zipInputStream.getNextEntry()) != null) {

				if (!zipEntry.isDirectory()) {
					String fileName = zipEntry.getName();
					// .pf 才處理
					if (!fileName.endsWith(".pf")) {
						continue;
					}
					
					log.debug("找到設定檔:" + fileName);

					// 解析 XML
					DocumentBuilder builder = factory.newDocumentBuilder();
					Document document = builder.parse(zipInputStream);
					// root
					xmlRoot = document.getDocumentElement();
					log.debug("[" + fileName + "] 解析 xml ok! ");
					// 找到第一筆即停止
					break;
				}
			}
		} catch (Exception e) {
			log.error("解析上傳檔案失敗! [" + e.getMessage() + "]", e);
			throw new SystemOperationException("解析上傳檔案失敗! [" + e.getMessage() + "]", InfomationLevel.ERROR);
		} finally {
			if (zipInputStream != null) {
				try {
					zipInputStream.closeEntry();
				} catch (Exception e2) {
				}
				try {
					zipInputStream.close();
				} catch (Exception e2) {
					System.err.println(e2);
				}
			}
		}

		if (xmlRoot == null) {
		    log.debug("找不到 *.pf 的檔案");
		}

		return xmlRoot;
	}

	/**
	 * @param node
	 * @param execSettingVOs
	 * @throws SystemOperationException
	 */
	private void processNode(Node node, List<Pps6ModelerExecSettingVO> execSettingVOs) throws SystemOperationException {

		// ====================================
		// 判斷是否為為需要判斷節點
		// ====================================
		// 判斷節點名稱
		if (this.targetNodeName.equals(node.getNodeName())) {
			// 取得值
			String value = this.getAttrVal(node, "value");

			// 取得節點名稱
			String modelerNodeName = this.findFlowNodeName(node);

			// 解析內容
			List<Pps6ModelerExecSettingVO> nodeParseInfos = this.parseData(value, modelerNodeName);
			if (WkStringUtils.notEmpty(nodeParseInfos)) {
				execSettingVOs.addAll(nodeParseInfos);
			}
		}

		// ====================================
		// 遞迴處理子節點
		// ====================================
		NodeList childNodeList = node.getChildNodes();
		if (childNodeList == null || childNodeList.getLength() == 0) {
			return;
		}

		for (int i = 0; i < childNodeList.getLength(); i++) {
			Node childNode = childNodeList.item(i);
			this.processNode(childNode, execSettingVOs);
		}

	}

	/**
	 * 解析 XML 節點資料
	 * 
	 * @param value
	 * @param modelerNodeName
	 * @return
	 * @throws SystemOperationException
	 */
	private List<Pps6ModelerExecSettingVO> parseData(
	        String value,
	        String modelerNodeName) throws SystemOperationException {

		// ====================================
		// 判斷值裡面是否有關鍵字
		// ====================================
		if (value.indexOf(keyWordValue) < 0) {
			return null;
		}

		// ====================================
		// 解析XML
		// ====================================
		Element root = null;
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			// Parse the content to Document object
			Document document = builder.parse(new InputSource(new StringReader(value)));

			root = document.getDocumentElement();

		} catch (Exception e) {
			log.error("解析內部 XML 失敗! [" + e.getMessage() + "]");
			log.error("value:[" + value + "]");
			throw new SystemOperationException("解析內部 XML 失敗! [" + e.getMessage() + "]", InfomationLevel.ERROR);
		}
		// ====================================
		// 解析 node
		// ====================================
		if (!"list".equals(root.getNodeName())) {
			log.error("不是 list 節點! name:[" + root.getNodeName() + "]");
			return null;
		}

		NodeList nodeList = root.getChildNodes();

		if (nodeList == null || nodeList.getLength() == 0) {
			log.error("list 節點下沒有內容");
			return null;
		}

		List<Pps6ModelerExecSettingVO> results = Lists.newArrayList();

		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			// 不是 RoleAssignBean 節點, 略過
			if (!keyWordValue.equals(node.getNodeName())) {
				continue;
			}

			NodeList childNodeList = node.getChildNodes();
			if (childNodeList == null || childNodeList.getLength() == 0) {
				log.warn("RoleAssignBean 節點下沒有內容");
				continue;
			}

			Pps6ModelerExecSettingVO execInfo = new Pps6ModelerExecSettingVO();
			execInfo.setModelerNodeName(modelerNodeName);

			// =================
			// 解析 RoleAssignBean 節點資訊
			// =================
			for (int j = 0; j < childNodeList.getLength(); j++) {

				Node childNode = childNodeList.item(j);
				String childNodeName = childNode.getNodeName();
				String childNodeValue = new String(childNode.getTextContent());

				// System.out.println(childNodeName + ":" + childNodeValue);

				if ("starterID".equals(childNodeName)) {
					execInfo.setExecId(childNodeValue);
				}

				if ("description".equals(childNodeName)) {
					execInfo.setUserDescr(childNodeValue);
				}

				if ("starterDescription".equals(childNodeName)) {
					this.parseStarterDescription(execInfo, childNodeValue);
				}
			}
			// =================
			// 排除無法、不需解析的節點
			// =================
			if (execInfo.getExecType() == null) {
				// 跳過名單
				if (PASS_DATA.contains(execInfo.getName())) {
					continue;
				} else {
					throw new SystemDevelopException("無法解析的執行資訊:[" + execInfo.getName() + "]");
				}
			}

			// 系統使用者, 無需解析
			if (Pps6ModelerExecType.USER.equals(execInfo.getExecType())
			        && PASS_USER.contains(execInfo.getName())) {
				continue;
			}

			// =================
			// 加入
			// =================
			results.add(execInfo);
		}

		return results;
	}

	private void parseStarterDescription(Pps6ModelerExecSettingVO execInfo, String childNodeValue) {

		String variableName = "";
		// 變數:角色
		if (childNodeValue.startsWith("角色變數<")) {
			execInfo.setExecType(Pps6ModelerExecType.ROLE);
			execInfo.setVariable(true);

			variableName = childNodeValue.substring(5);
			variableName = variableName.substring(0, variableName.length() - 1);
			execInfo.setName(variableName);
		}
		// 變數:部門
		else if (childNodeValue.startsWith("部門變數<")) {
			execInfo.setExecType(Pps6ModelerExecType.DEP);
			execInfo.setVariable(true);

			variableName = childNodeValue.substring(5);
			variableName = variableName.substring(0, variableName.length() - 1);
			execInfo.setName(variableName);
		}
		// 變數:使用者
		else if (childNodeValue.startsWith("使用者變數<")) {
			execInfo.setExecType(Pps6ModelerExecType.USER);
			execInfo.setVariable(true);
			variableName = childNodeValue.substring(6);
			variableName = variableName.substring(0, variableName.length() - 1);
			execInfo.setName(variableName);
		}
		// 例:<角色>款項問題覆核者
		else if (childNodeValue.indexOf("<") == 0 && !PASS_DATA.contains(childNodeValue)) {
			execInfo.setVariable(false);

			String[] strAry = childNodeValue.split(">");
			if (strAry.length < 2) {
				throw new SystemDevelopException("無法解析的執行資訊:[" + childNodeValue + "]");
			}

			Pps6ModelerExecType execType = Pps6ModelerExecType.safeDescrOf(strAry[0].substring(1));
			if (execType == null) {
				throw new SystemDevelopException("未定義的執行設定類別:[" + strAry[0].substring(1) + "]");
			}
			execInfo.setExecType(execType);
			execInfo.setName(strAry[1]);

		} else {
			execInfo.setName(childNodeValue);
		}
	}

	private String findFlowNodeName(Node node) {
		if (node.getParentNode() == null || node.getParentNode().getParentNode() == null) {
		    log.debug("找不到 FlowNode");
			return "";
		}

		Node flowNode = node.getParentNode().getParentNode();
		if (!"Activity".equals(flowNode.getNodeName())) {
		    log.debug("FlowNode 不是 Activity:" + flowNode.getNodeName());
			return "";
		}

		return getAttrVal(flowNode, "text");
	}

	private String getAttrVal(Node node, String attrName) {
		Node attrNode = node.getAttributes().getNamedItem(attrName);
		if (attrNode == null) {
		    log.debug("找不到屬性:[" + attrName + "]");
			return "";
		}

		return attrNode.getNodeValue();
	}
}
