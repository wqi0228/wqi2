/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.tros;

import com.cy.tech.request.logic.service.query.LogicQueryService;
import com.cy.tech.request.logic.vo.TrOsAttachmentVO;
import com.cy.tech.request.logic.vo.TrOsHistoryVO;
import com.cy.tech.request.logic.vo.TrOsReplyAndReplyVO;
import com.cy.tech.request.logic.vo.TrOsReplyVO;
import com.cy.tech.request.logic.vo.TrOsVO;
import com.cy.tech.request.repository.require.tros.TrOsRepository;
import com.cy.tech.request.vo.enums.OthSetHistoryBehavior;
import com.cy.tech.request.vo.enums.OthSetStatus;
import com.cy.tech.request.vo.enums.tros.TrOsType;
import com.cy.tech.request.vo.require.tros.TrOs;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class TrOsService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 114654531884125645L;
    /** TrOsRepository */
    @Autowired
    private TrOsRepository trOsRepository;
    /** LogicQueryService */
    @Autowired
    @Qualifier("logicQueryService")
    private LogicQueryService logicQueryService;

    /**
     * 取得TrOsVO List By request_Sid
     *
     * @param request_Sid 需求單Sid
     * @return
     */
    public List<TrOsVO> getTrOsByRequestSid(String request_Sid) {

        List<TrOsVO> trOsVOs = Lists.newArrayList();
        List<TrOs> trOss = trOsRepository.getTrOsByRequireSid(request_Sid);
        if (trOss == null || trOss.isEmpty()) {
            return trOsVOs;
        }
        try {
            Map<String, Object> parameters = Maps.newHashMap();
            StringBuilder builder = new StringBuilder();
            // 其它設定資訊 回覆
            builder.append("SELECT ");
            builder.append("'" + TrOsType.tr_os_reply.name() + "' as typeName,"
                    + " tor.os_reply_sid as sid,"
                    + " tor.os_reply_sid as os_reply_sid,"
                    + " '' as os_reply_and_reply_sid,"
                    + " '' as os_history_sid,"
                    + " tor.os_sid as os_sid,"
                    + " tor.os_no as os_no,"
                    + " tor.require_sid as require_sid,"
                    + " tor.require_no as require_no,"
                    + " tor.reply_person_dep as reply_person_dep,"
                    + " tor.reply_person_sid as reply_person_sid,"
                    + " tor.reply_dt as reply_dt,"
                    + " tor.reply_udt as reply_udt,"
                    + " tor.reply_content as content,"
                    + " tor.reply_content_css as content_css,"
                    + " '' as behavior,"
                    + " '' as behavior_status,"
                    + " null as create_usr,"
                    + " null as create_dt,"
                    + " null as update_usr,"
                    + " null as update_dt,"
                    + "'' as file_name,"
                    + "'' as description"
                    + " FROM tr_os_reply tor "
                    + " WHERE tor.require_sid = '" + request_Sid + "' ");
            builder.append(" UNION ALL ");
            // 其它設定資訊 回覆 的回覆
            builder.append("SELECT "
                    + "'" + TrOsType.tr_os_reply_and_reply.name() + "' as typeName,"
                    + "torr.os_reply_and_reply_sid as sid,"
                    + "torr.os_reply_sid as os_reply_sid,"
                    + "torr.os_reply_and_reply_sid as os_reply_and_reply_sid,"
                    + "'' as os_history_sid,"
                    + "torr.os_sid as os_sid,"
                    + "torr.os_no as os_no,"
                    + "torr.require_sid as require_sid,"
                    + "torr.require_no as require_no,"
                    + "torr.reply_person_dep as reply_person_dep,"
                    + "torr.reply_person_sid as reply_person_sid,"
                    + "torr.reply_dt as reply_dt,"
                    + "torr.reply_udt as reply_udt,"
                    + "torr.reply_content as content,"
                    + "torr.reply_content_css as content_css,"
                    + " '' as behavior,"
                    + " '' as behavior_status,"
                    + " null as create_usr,"
                    + " null as create_dt,"
                    + " null as update_usr,"
                    + " null as update_dt,"
                    + "'' as file_name,"
                    + "'' as description "
                    + " FROM tr_os_reply_and_reply torr "
                    + " WHERE torr.require_sid = '" + request_Sid + "' ");
            builder.append(" UNION ALL ");
            // 其它設定資訊 歷程
            builder.append(" SELECT "
                    + "'" + TrOsType.tr_os_history.name() + "' as typeName,"
                    + "toh.os_history_sid as sid,"
                    + "toh.os_reply_sid as os_reply_sid,"
                    + "toh.os_reply_and_reply_sid as os_reply_and_reply_sid,"
                    + "toh.os_history_sid as os_history_sid,"
                    + "toh.os_sid as os_sid,"
                    + "toh.os_no as os_no,"
                    + "toh.require_sid as require_sid,"
                    + "toh.require_no as require_no,"
                    + "null as reply_person_dep,"
                    + "null as reply_person_sid,"
                    + "null as reply_dt,"
                    + "null as reply_udt,"
                    + "toh.input_info as content,"
                    + "toh.input_info_css as content_css,"
                    + "toh.behavior as behavior,"
                    + "toh.behavior_status as behavior_status,"
                    + "toh.create_usr as create_usr,"
                    + "toh.create_dt as create_dt,"
                    + "toh.update_usr as update_usr,"
                    + "toh.update_dt as update_dt,"
                    + "'' as file_name,"
                    + "'' as description"
                    + " FROM tr_os_history toh"
                    + " WHERE toh.require_sid = '" + request_Sid + "'");
            builder.append(" UNION ALL ");
            // 其它設定資訊 歷程附件
            builder.append(" SELECT "
                    + "'" + TrOsType.tr_os_attachment.name() + "' as typeName,"
                    + "toa.os_attachment_sid as sid,"
                    + "'' as os_reply_sid,"
                    + "'' as os_reply_and_reply_sid,"
                    + "toa.os_history_sid as os_history_sid,"
                    + "toa.os_sid as os_sid,"
                    + "toa.os_no as os_no,"
                    + "toa.require_sid as require_sid,"
                    + "toa.require_no as require_no,"
                    + "toa.department as reply_person_dep,"
                    + "null as reply_person_sid,"
                    + "null as reply_dt,"
                    + "null as reply_udt,"
                    + "null as content,"
                    + "null as content_css,"
                    + "toa.behavior as behavior,"
                    + " '' as behavior_status,"
                    + "toa.create_usr as create_usr,"
                    + "toa.create_dt as create_dt,"
                    + "toa.update_usr as update_usr,"
                    + "toa.update_dt as update_dt,"
                    + "toa.file_name as file_name,"
                    + "toa.description as description"
                    + " FROM tr_os_attachment toa "
                    + " WHERE toa.require_sid = '" + request_Sid + "' and toa.status = 0 ");
            @SuppressWarnings("unchecked")
            List<Object[]> result = logicQueryService.findWithQuery(builder.toString(), parameters);
            List<TrOsAttachmentVO> trOsAttachmentVOs = Lists.newArrayList();
            List<TrOsReplyAndReplyVO> trOsReplyAndReplyVOs = Lists.newArrayList();
            List<TrOsReplyVO> trOsReplyVOs = Lists.newArrayList();
            List<TrOsHistoryVO> trOsHistoryVOs = Lists.newArrayList();

            for (int i = 0; i < result.size(); i++) {
                try {
                    Object[] record = (Object[]) result.get(i);
                    String typeNameStr = (String) record[0];
                    TrOsType typeName = TrOsType.valueOf(typeNameStr);
                    String sid = (String) record[1];
                    String os_reply_sid = "";
                    if (record[2] != null) {
                        os_reply_sid = (String) record[2];
                    }
                    String os_reply_and_reply_sid = "";
                    if (record[3] != null) {
                        os_reply_and_reply_sid = (String) record[3];
                    }
                    String os_history_sid = "";
                    if (record[4] != null) {
                        os_history_sid = (String) record[4];
                    }
                    String os_sid = (String) record[5];
                    String os_no = (String) record[6];
                    String require_sid = (String) record[7];
                    String require_no = (String) record[8];
                    Integer reply_person_dep = null;
                    if (record[9] != null) {
                        reply_person_dep = (Integer) record[9];
                    }
                    Integer reply_person_sid = null;
                    if (record[10] != null) {
                        reply_person_sid = (Integer) record[10];
                    }
                    Date reply_dt = null;
                    if (record[11] != null) {
                        reply_dt = (Date) record[11];
                    }
                    Date reply_udt = null;
                    if (record[12] != null) {
                        reply_udt = (Date) record[12];
                    }
                    String content = "";
                    if (record[13] != null) {
                        content = (String) record[13];
                    }
                    String content_css = "";
                    if (record[14] != null) {
                        byte[] msageBytpe = (byte[]) record[14];
                        content_css = msageBytpe == null ? "" : new String(msageBytpe, "UTF-8");
                    }
                    String behaviorStr = (String) record[15];
                    OthSetHistoryBehavior othSetHistoryBehavior = null;
                    if (!Strings.isNullOrEmpty(behaviorStr) && !typeName.equals(TrOsType.tr_os_attachment)) {
                        try {
                            othSetHistoryBehavior = OthSetHistoryBehavior.valueOf(behaviorStr);
                        } catch (Exception e) {
                            log.error("主檔上傳的附件", e);
                        }
                    }
                    String behavior_statusStr = (String) record[16];
                    OthSetStatus othSetStatus = null;
                    if (!Strings.isNullOrEmpty(behavior_statusStr)) {
                        othSetStatus = OthSetStatus.valueOf(behavior_statusStr);
                    }
                    Integer create_usr = null;
                    if (record[17] != null) {
                        create_usr = (Integer) record[17];
                    }
                    Date create_dt = null;
                    if (record[18] != null) {
                        create_dt = (Date) record[18];
                    }
                    Integer update_usr = null;
                    if (record[19] != null) {
                        update_usr = (Integer) record[19];
                    }
                    Date update_dt = null;
                    if (record[20] != null) {
                        update_dt = (Date) record[20];
                    }
                    String file_name = (String) record[21];
                    String description = (String) record[22];
                    if (typeName.equals(TrOsType.tr_os_attachment)) {
                        TrOsAttachmentVO tv = new TrOsAttachmentVO(sid, os_history_sid, os_sid, os_no, require_sid, require_no, reply_person_dep,
                                othSetHistoryBehavior, create_usr, create_dt, update_usr, update_dt, file_name, description);
                        trOsAttachmentVOs.add(tv);
                    } else if (typeName.equals(TrOsType.tr_os_reply_and_reply)) {
                        TrOsReplyAndReplyVO trv = new TrOsReplyAndReplyVO(sid, os_reply_sid, os_history_sid, os_sid, os_no,
                                require_sid, require_no, reply_person_dep, reply_person_sid, reply_dt, reply_udt, content, content_css, Lists.newArrayList());
                        trOsReplyAndReplyVOs.add(trv);
                    } else if (typeName.equals(TrOsType.tr_os_reply)) {
                        TrOsReplyVO tor = new TrOsReplyVO(sid, os_history_sid, os_sid, os_no, require_sid, require_no, reply_person_dep, reply_person_sid,
                                reply_dt, reply_udt,
                                content, content_css, Lists.newArrayList(), Lists.newArrayList());
                        trOsReplyVOs.add(tor);
                    } else if (typeName.equals(TrOsType.tr_os_history)) {
                        TrOsHistoryVO thv = new TrOsHistoryVO(sid, os_reply_sid, os_reply_and_reply_sid, null, os_sid, os_no, require_sid, require_no, content,
                                content_css, othSetHistoryBehavior,
                                othSetStatus, create_usr, create_dt, update_usr, update_dt);
                        trOsHistoryVOs.add(thv);
                    }
                } catch (Exception e) {
                    log.error("getTrOsByRequestSid Error", e);
                }
            }
            Collections.sort(trOsAttachmentVOs, trOsAttachmentVOComparator);
            Collections.sort(trOsReplyAndReplyVOs, trOsReplyAndReplyVOComparator);
            Collections.sort(trOsReplyVOs, trOsReplyVOComparator);

            trOss.forEach(item -> {
                // 其它設定資訊 - 歷程(包含回覆)
                List<TrOsHistoryVO> replyHistory = trOsHistoryVOs.stream()
                        .filter(each -> item.getSid().equals(each.getOs_sid()) && (OthSetHistoryBehavior.REPLY.equals(each.getBehavior())))
                        .collect(Collectors.toList());

                List<TrOsHistoryVO> realReplyHistory = Lists.newArrayList();
                if (replyHistory != null && !replyHistory.isEmpty()) {
                    replyHistory.forEach(replyItem -> {
                        if (Strings.isNullOrEmpty(replyItem.getOs_reply_sid())) {
                            log.error(
                                    "問題資料,歷程類型-回覆,卻無os_reply_sid. os_no:[" + replyItem.getOs_no() + "] os_history_sid:[" + replyItem.getOs_history_sid() + "]");
                            return;
                        }
                        List<TrOsReplyVO> filterTrOsReplyVOs = trOsReplyVOs.stream()
                                .filter(each -> replyItem.getOs_reply_sid().equals(each.getOs_reply_sid()) && item.getSid().equals(each.getOs_sid()))
                                .collect(Collectors.toList());
                        if (filterTrOsReplyVOs == null || filterTrOsReplyVOs.isEmpty()) {
                            log.error("問題資料,歷程類型-回覆的回覆,卻無os_reply_sid. os_no:[" + replyItem.getOs_no() + "] "
                                    + "os_reply_sid:[" + replyItem.getOs_reply_sid() + "]");
                            return;
                        }
                        TrOsReplyVO selTrOsReplyVO = filterTrOsReplyVOs.get(0);
                        selTrOsReplyVO.setOs_history_sid(replyItem.getOs_history_sid());

                        // 回覆附件
                        List<TrOsAttachmentVO> replyAtt = trOsAttachmentVOs.stream()
                                .filter(each -> replyItem.getOs_history_sid().equals(each.getOs_history_sid()) && item.getSid().equals(each.getOs_sid()))
                                .collect(Collectors.toList());
                        selTrOsReplyVO.setTrOsAttachmentVOs(replyAtt);
                        List<TrOsReplyAndReplyVO> replyAndReplys = trOsReplyAndReplyVOs.stream()
                                .filter(each -> item.getSid().equals(each.getOs_sid()) && each.getOs_reply_sid().equals(replyItem.getOs_reply_sid()))
                                .collect(Collectors.toList());
                        List<TrOsReplyAndReplyVO> realReplyAndReplys = Lists.newArrayList();
                        // 取得回覆的回覆附件
                        if (replyAndReplys != null && !replyAndReplys.isEmpty()) {
                            replyAndReplys.forEach(replyAndReplyItem -> {
                                List<TrOsHistoryVO> replyAndReplyHistorys = trOsHistoryVOs.stream()
                                        .filter(each -> item.getSid().equals(each.getOs_sid())
                                                && each.getOs_reply_and_reply_sid().equals(replyAndReplyItem.getOs_reply_and_reply_sid())
                                                && (OthSetHistoryBehavior.REPLY_AND_REPLY.equals(each.getBehavior())))
                                        .collect(Collectors.toList());
                                if (replyAndReplyHistorys == null || replyAndReplyHistorys.isEmpty()) {
                                    log.error("問題資料,歷程類型-回覆的回覆,卻無os_reply_and_reply_sid. os_no:[" + replyItem.getOs_no() + "] "
                                            + "os_reply_and_reply_sid:[" + replyAndReplyItem.getOs_reply_and_reply_sid() + "]");

                                } else {
                                    TrOsHistoryVO replyAndReplyHistory = replyAndReplyHistorys.get(0);
                                    List<TrOsAttachmentVO> replyAndReplyAtt = trOsAttachmentVOs.stream()
                                            .filter(each -> replyAndReplyHistory.getOs_history_sid().equals(each.getOs_history_sid())
                                                    && item.getSid().equals(each.getOs_sid()))
                                            .collect(Collectors.toList());
                                    replyAndReplyItem.setOs_history_sid(replyAndReplyHistory.getOs_history_sid());
                                    replyAndReplyItem.getTrOsAttachmentVOs().addAll(replyAndReplyAtt);
                                    realReplyAndReplys.add(replyAndReplyItem);
                                }
                            });
                        }
                        selTrOsReplyVO.setTrOsReplyAndReplyVO(realReplyAndReplys);
                        replyItem.setTrOsReplyVO(selTrOsReplyVO);
                        realReplyHistory.add(replyItem);
                    });
                }

                // 其它設定資訊 - 歷程(包含完成,取消)
                List<TrOsHistoryVO> processHistory = trOsHistoryVOs.stream()
                        .filter(each -> item.getSid().equals(each.getOs_sid()) && (OthSetHistoryBehavior.CANCEL_OS.equals(each.getBehavior())
                                || OthSetHistoryBehavior.FINISH_OS.equals(each.getBehavior())))
                        .collect(Collectors.toList());
                processHistory.forEach(processItem -> {
                    // 回覆附件
                    List<TrOsAttachmentVO> processAtts = trOsAttachmentVOs.stream()
                            .filter(each -> processItem.getOs_history_sid().equals(each.getOs_history_sid())
                                    && processItem.getOs_sid().equals(each.getOs_sid()))
                            .collect(Collectors.toList());

                    TrOsReplyVO trOsReplyVO = new TrOsReplyVO(
                            processItem.getOs_reply_sid(),
                            processItem.getOs_history_sid(),
                            processItem.getOs_sid(),
                            processItem.getOs_no(),
                            processItem.getRequire_sid(),
                            processItem.getRequire_no(),
                            null,
                            processItem.getCreate_usr(),
                            processItem.getCreate_dt(),
                            processItem.getUpdate_dt(),
                            processItem.getContent(),
                            processItem.getContent_css(),
                            processAtts,
                            Lists.newArrayList());
                    processItem.setTrOsReplyVO(trOsReplyVO);

                });

                // 其它設定資訊 - 附件
                List<TrOsAttachmentVO> trosAtt = trOsAttachmentVOs.stream()
                        .filter(each -> Strings.isNullOrEmpty(each.getOs_history_sid()) && item.getSid().equals(each.getOs_sid()))
                        .collect(Collectors.toList());
                List<TrOsHistoryVO> allHistorys = Lists.newArrayList();
                allHistorys.addAll(realReplyHistory);
                allHistorys.addAll(processHistory);
                Collections.sort(allHistorys, trOsHistoryVOComparator);
                TrOsVO osVO = new TrOsVO(item.getSid(), item.getOs_no(), item.getRequire_sid(), item.getRequire_no(), item.getTheme(), item.getContent(),
                        item.getContentCss(), item.getNote(), item.getNoteCss(), item.getFinishDate(), item.getCancelDate(), item.getOsStatus(),
                        item.getNoticeDeps().getValue(), allHistorys, trosAtt, item.getCreate_usr(), item.getCreate_dt(), item.getUpdate_usr(),
                        item.getUpdatedDate(), item.getDep_sid());
                trOsVOs.add(osVO);
            });
        } catch (Exception e) {
            log.error("getTrOsByRequestSid", e);
        }
        return trOsVOs;
    }

    transient private Comparator<TrOsAttachmentVO> trOsAttachmentVOComparator = new Comparator<TrOsAttachmentVO>() {
        @Override
        public int compare(TrOsAttachmentVO obj1, TrOsAttachmentVO obj2) {
            final Date seq1 = obj1.getCreate_dt();
            final Date seq2 = obj2.getCreate_dt();
            return seq2.compareTo(seq1);
        }
    };

    transient private Comparator<TrOsReplyAndReplyVO> trOsReplyAndReplyVOComparator = new Comparator<TrOsReplyAndReplyVO>() {
        @Override
        public int compare(TrOsReplyAndReplyVO obj1, TrOsReplyAndReplyVO obj2) {
            final Date seq1 = obj1.getReply_udt();
            final Date seq2 = obj2.getReply_udt();
            return seq2.compareTo(seq1);
        }
    };

    transient private Comparator<TrOsReplyVO> trOsReplyVOComparator = new Comparator<TrOsReplyVO>() {
        @Override
        public int compare(TrOsReplyVO obj1, TrOsReplyVO obj2) {
            final Date seq1 = obj1.getReply_udt();
            final Date seq2 = obj2.getReply_udt();
            return seq2.compareTo(seq1);
        }
    };

    transient private Comparator<TrOsHistoryVO> trOsHistoryVOComparator = new Comparator<TrOsHistoryVO>() {
        @Override
        public int compare(TrOsHistoryVO obj1, TrOsHistoryVO obj2) {
            final Date seq1 = obj1.getUpdate_dt();
            final Date seq2 = obj2.getUpdate_dt();
            return seq2.compareTo(seq1);
        }
    };

    /**
     * @param item
     * @return
     */
    public TrOsVO convert2To(TrOs item) {
        return new TrOsVO(
                item.getSid(),
                item.getOs_no(),
                item.getRequire_sid(),
                item.getRequire_no(),
                item.getTheme(),
                item.getContent(),
                item.getContentCss(),
                item.getNote(),
                item.getNoteCss(),
                item.getFinishDate(),
                item.getCancelDate(),
                item.getOsStatus(),
                item.getNoticeDeps().getValue(),
                null,
                null,
                item.getCreate_usr(),
                item.getCreate_dt(),
                item.getUpdate_usr(),
                item.getUpdatedDate(),
                item.getDep_sid());
    }

    /**
     * 依據需求單號查詢
     * 
     * @param sourceNo
     */
    public List<TrOsVO> findByRequireNoAndCreateDepSids(String requireNo, List<Integer> depSids) {

        // ====================================
        // 檢查傳入參數為空
        // ====================================
        if (WkStringUtils.isEmpty(requireNo) || WkStringUtils.isEmpty(depSids)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 查詢
        // ====================================
        List<TrOs> items = this.trOsRepository.queryByRequirenoAndDepsidIn(
                requireNo,
                depSids);

        // ====================================
        // 轉To
        // ====================================
        List<TrOsVO> results = Lists.newArrayList();
        if (items != null) {
            for (TrOs item : items) {
                results.add(this.convert2To(item));
            }
        }

        return results;
    }

}
