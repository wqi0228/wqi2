/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.repository.category.BasicDataBigCategoryRepository;
import com.cy.tech.request.repository.category.BasicDataSmallCategoryRepository;
import com.cy.tech.request.vo.category.BasicDataBigCategory;
import com.cy.tech.request.vo.category.BasicDataMiddleCategory;
import com.cy.tech.request.vo.category.BasicDataSmallCategory;

/**
 * 類別 設定<BR/>
 * 大類 | 中類 | 小類
 *
 * @author shaun
 */
@Component
public class CategorySettingService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8410714073451103651L;
    @Autowired
    private BasicDataBigCategoryRepository bigDao;
    @Autowired
    private BasicDataSmallCategoryRepository smallDao;

    @Transactional(readOnly = true)
    public List<BasicDataBigCategory> findAllBig() {
        return bigDao.findByStatusOrderByIdAsc(Activation.ACTIVE);
    }

    public List<BasicDataBigCategory> findAllBigWithInactive() {
        return this.bigDao.findAll()
                .stream()
                .sorted(Comparator.comparing(BasicDataBigCategory::getId))
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public BasicDataBigCategory findBigBySid(String sid) {
        return bigDao.findOne(sid);
    }

    @Transactional(readOnly = true)
    public List<BasicDataMiddleCategory> findMiddleByBig(BasicDataBigCategory big) {
        BasicDataBigCategory newBig = bigDao.findOne(big.getSid());
        return newBig.getChildren();
    }

    @Transactional(readOnly = true)
    public BasicDataSmallCategory findSmallBySid(String sid) {
        return smallDao.findOne(sid);
    }
}
