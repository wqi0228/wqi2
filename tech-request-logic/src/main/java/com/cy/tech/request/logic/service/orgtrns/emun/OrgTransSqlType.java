/**
 * 
 */
package com.cy.tech.request.logic.service.orgtrns.emun;

import lombok.Getter;

/**
 * @author allen1214_wu
 */
public enum OrgTransSqlType {
    DETAIL("查詢明細用(開啟列表視窗)"),
    COUNT("查詢筆數用"),
    ALL_COUNT("查詢全部筆數用"),
    ALLTRANS("全部轉檔用"),
    ;

    /**
     * 說明
     */
    @Getter
    private String descr;

    OrgTransSqlType(String descr) {
        this.descr = descr;
    }

}
