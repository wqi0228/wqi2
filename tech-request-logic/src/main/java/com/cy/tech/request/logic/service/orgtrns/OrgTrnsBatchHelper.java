/**
 * 
 */
package com.cy.tech.request.logic.service.orgtrns;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsAssignSendInfoForTrnsTO;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsDtVO;
import com.cy.tech.request.logic.vo.RequireConfirmDepVO;
import com.cy.work.common.constant.WkConstants;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.SubNoticeType;
import com.cy.tech.request.vo.require.AssignSendSearchInfoVO;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.converter.JsonStringListToConverter;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 *
 */
@Slf4j
@Service
public class OrgTrnsBatchHelper implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -5512998052451328651L;

    @Autowired
	@Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
	private transient JdbcTemplate jdbcTemplate;

	/**
	 * 每次批次執行筆數
	 */
	private final Integer BATCH_UPDATE_SIZE = 200;

	private final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:sss");

	/**
	 * 批次刪除 tr_assign_send_search_info
	 * 
	 * @param requireSids 需求單號
	 * @param type        分派/通知類型
	 */
	public void batchDeleteAssignSendSearchInfo(
	        Set<String> requireSids,
	        AssignSendType type) {

		Long startTime = System.currentTimeMillis();
		String procTitle = "DELETE FROM tr_assign_send_search_info （type:" + type.name() + "）";
		// log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

		if (WkStringUtils.isEmpty(requireSids)) {
			log.warn(procTitle);
			log.warn("傳入的 requireSids 條件為空，不執行");
			return;
		}

		String deleteSQL = ""
		        + "DELETE FROM tr_assign_send_search_info "
		        + " WHERE type = " + type.ordinal()
		        + "   AND require_sid IN ('"
		        + String.join("', '", requireSids)
		        + "');";

		int rows = this.jdbcTemplate.update(deleteSQL);
		log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle + " 共刪除[" + rows + "]筆"));
	}

	/**
	 * 批次新增 tr_require_confirm_dep
	 * 
	 * @param insertRequireConfirmDeps
	 * @throws SystemDevelopException
	 */
	@Transactional(rollbackFor = Exception.class)
	public void batchInsertRequireConfirmDeps(
	        List<RequireConfirmDepVO> insertRequireConfirmDeps) throws SystemDevelopException {

		if (WkStringUtils.isEmpty(insertRequireConfirmDeps)) {
			log.info("傳入的 tr_require_confirm_dep 為空，不執行");
			return;
		}

		Set<String> checkSet = Sets.newHashSet();
		for (RequireConfirmDepVO requireConfirmDep : insertRequireConfirmDeps) {
			String key = requireConfirmDep.getRequireSid() + "--" + requireConfirmDep.getDepSid();
			if (checkSet.contains(key)) {
				throw new SystemDevelopException("重複：[" + key + "]", InfomationLevel.ERROR);
			}
		}

		Long startTime = System.currentTimeMillis();
		String procTitle = "INSERT tr_require_confirm_dep [" + insertRequireConfirmDeps.size() + "]筆";
		// log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

		StringBuffer insertSQL = new StringBuffer();
		insertSQL.append("INSERT ");
		insertSQL.append("INTO ");
		insertSQL.append("    tr_require_confirm_dep ");
		insertSQL.append("    ( ");
		insertSQL.append("        require_confirm_sid, ");
		insertSQL.append("        status, ");
		insertSQL.append("        create_usr, ");
		insertSQL.append("        create_dt, ");
		insertSQL.append("        require_sid, ");
		insertSQL.append("        dep_sid, ");
		insertSQL.append("        owner_sid, ");
		insertSQL.append("        prog_status ");
		insertSQL.append("    ) ");
		insertSQL.append("    VALUES ");
		insertSQL.append("    ( ");
		insertSQL.append("        ?, "); // tr_require_confirm_dep
		insertSQL.append("        0, "); // status
		insertSQL.append("        ?, "); // create_usr
		insertSQL.append("        ?, "); // create_dt
		insertSQL.append("        ?, "); // require_sid
		insertSQL.append("        ?, "); // dep_sid
		insertSQL.append("        " + WkConstants.MANAGER_VIRTAUL_USER_SID + ", ");// owner_sid
		insertSQL.append("        ? "); // prog_status
		insertSQL.append("    );");

		int totalSize = insertRequireConfirmDeps.size();
		for (int i = 0; i < totalSize; i += BATCH_UPDATE_SIZE) {
			int index = (i + BATCH_UPDATE_SIZE) > totalSize ? totalSize : i + BATCH_UPDATE_SIZE;
			// 取得本次批次執行筆數
			final List<RequireConfirmDepVO> batchList = insertRequireConfirmDeps.subList(i, index);

			this.jdbcTemplate.batchUpdate(insertSQL.toString(), new BatchPreparedStatementSetter() {
				@Override
				public int getBatchSize() {
					return batchList.size();
				}

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					RequireConfirmDepVO item = batchList.get(i);
					int index = 1;
					ps.setLong(index++, item.getSid());
					// create_usr
					ps.setInt(index++, item.getCreatedUser());
					// create_dt
					ps.setString(index++, df.format(item.getCreatedDate()));
					// require_sid
					ps.setString(index++, item.getRequireSid());
					// dep_sid
					ps.setLong(index++, item.getDepSid());
					// prog_status
					ps.setString(index++, item.getProgStatus().name());
				}
			});
		}
		log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
	}

	/**
	 * 批次 update tr_assign_send_info
	 * 
	 * @param assignSendInfoForTrnsVOs
	 * @param sysdate
	 */
	@Transactional(rollbackFor = Exception.class)
	public void batchUpdateAssignSendInfo(
	        List<OrgTrnsAssignSendInfoForTrnsTO> assignSendInfoForTrnsVOs,
	        AssignSendType type,
	        Date sysdate) {

		if (WkStringUtils.isEmpty(assignSendInfoForTrnsVOs)) {
			log.warn("傳入的 assignSendInfoForTrnsVOs 為空，不執行");
			return;
		}

		Long startTime = System.currentTimeMillis();
		String procTitle = "UPDATE [tr_assign_send_info].[info] where type = "
		        + type.name()
		        + "[" + assignSendInfoForTrnsVOs.size() + "]筆";

		StringBuffer updateSQL = new StringBuffer();
		updateSQL.append("UPDATE tr_assign_send_info ");
		updateSQL.append("SET    info = ? ");
		updateSQL.append("WHERE  info_sid = ? ");
		updateSQL.append("  AND  type = " + type.ordinal() + " ; ");

		int totalSize = assignSendInfoForTrnsVOs.size();
		for (int i = 0; i < totalSize; i += BATCH_UPDATE_SIZE) {
			int index = (i + BATCH_UPDATE_SIZE) > totalSize ? totalSize : i + BATCH_UPDATE_SIZE;
			// 取得本次批次執行筆數
			final List<OrgTrnsAssignSendInfoForTrnsTO> batchList = assignSendInfoForTrnsVOs.subList(i, index);

			this.jdbcTemplate.batchUpdate(updateSQL.toString(), new BatchPreparedStatementSetter() {

				@Override
				public int getBatchSize() {
					return batchList.size();
				}

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {

					OrgTrnsAssignSendInfoForTrnsTO item = batchList.get(i);
					int index = 1;
					// info
					ps.setString(index++, item.getNewAssignInfoJsonStr());
					// info_sid
					ps.setString(index++, item.getSid());
				}
			});
		}
		log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
	}

	/**
	 * @param tableName
	 * @param setColumnNameNoticeInfo
	 * @param keyColumnNameSid
	 * @param selectedDtVOList
	 * @param sysDate
	 */
	public void batchUpdateNoticeDepInfo(
	        String tableName,
	        String setColumnNameNoticeInfo,
	        String keyColumnNameSid,
	        List<OrgTrnsDtVO> selectedDtVOList,
	        Date sysDate) {

		Long startTime = System.currentTimeMillis();
		String procTitle = "UPDATE [" + tableName + "].[" + setColumnNameNoticeInfo + "] :" + selectedDtVOList.size() + "筆";

		if (WkStringUtils.isEmpty(selectedDtVOList)) {
			log.warn(procTitle);
			log.warn("傳入為空，不執行");
			return;
		}

		StringBuffer updateSQL = new StringBuffer();
		updateSQL.append("UPDATE " + tableName + " ");
		updateSQL.append("SET    " + setColumnNameNoticeInfo + " = ?, ");
		updateSQL.append("       update_usr = 1, ");
		updateSQL.append("       update_dt = ? ");
		updateSQL.append("WHERE  " + keyColumnNameSid + " = ?  ");

		JsonStringListToConverter converter = new JsonStringListToConverter();

		int totalSize = selectedDtVOList.size();

		for (int i = 0; i < totalSize; i += BATCH_UPDATE_SIZE) {
			int index = (i + BATCH_UPDATE_SIZE) > totalSize ? totalSize : i + BATCH_UPDATE_SIZE;
			// 取得本次批次執行筆數
			final List<OrgTrnsDtVO> batchList = selectedDtVOList.subList(i, index);

			this.jdbcTemplate.batchUpdate(updateSQL.toString(), new BatchPreparedStatementSetter() {

				@Override
				public int getBatchSize() {
					return batchList.size();
				}

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {

					OrgTrnsDtVO orgTrnsDtVO = batchList.get(i);

					int index = 1;
					// setColumnNameNoticeInfo (set)
					ps.setString(index++, converter.convertToDatabaseColumn(orgTrnsDtVO.getNewNoticeDepSids()));
					// update_dt (set)
					ps.setString(index++, df.format(sysDate.getTime()));
					// table SID (where)
					ps.setString(index++, orgTrnsDtVO.getSid());

				}
			});
		}
		log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));

	}

	/**
	 * 批次 update tr_assign_send_info
	 * 
	 * @param assignSendInfoForTrnsVOs
	 * @param sysdate
	 */
	@Transactional(rollbackFor = Exception.class)
	public void batchUpdateRequireConfirmDep(
	        Map<Long, Integer> updateDepSidMapByConfirmSid,
	        Date sysDate) {

		Long startTime = System.currentTimeMillis();
		String procTitle = "UPDATE [tr_require_confirm_dep].[dep_sid]:" + updateDepSidMapByConfirmSid.size() + "筆";
		// log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

		if (WkStringUtils.isEmpty(updateDepSidMapByConfirmSid)) {
			log.warn("【" + procTitle + "】: 傳入空，不執行");
			return;
		}

		StringBuffer updateSQL = new StringBuffer();
		updateSQL.append("UPDATE tr_require_confirm_dep ");
		updateSQL.append("SET    dep_sid = ?, ");
		updateSQL.append("       update_usr = 1, ");
		updateSQL.append("       update_dt = ? ");
		updateSQL.append("WHERE  require_confirm_sid = ?; ");

		int totalSize = updateDepSidMapByConfirmSid.size();
		List<Entry<Long, Integer>> allData = Lists.newArrayList(updateDepSidMapByConfirmSid.entrySet());

		for (int i = 0; i < totalSize; i += BATCH_UPDATE_SIZE) {
			int index = (i + BATCH_UPDATE_SIZE) > totalSize ? totalSize : i + BATCH_UPDATE_SIZE;
			// 取得本次批次執行筆數
			final List<Entry<Long, Integer>> batchList = allData.subList(i, index);

			this.jdbcTemplate.batchUpdate(updateSQL.toString(), new BatchPreparedStatementSetter() {

				@Override
				public int getBatchSize() {
					return batchList.size();
				}

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {

					Entry<Long, Integer> item = batchList.get(i);
					int index = 1;
					// dep_sid
					ps.setInt(index++, item.getValue());
					// update_dt
					ps.setString(index++, df.format(sysDate.getTime()));
					// require_confirm_sid
					ps.setLong(index++, item.getKey());
				}
			});
		}
		log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
	}

	/**
	 * @param targetUpdateRequireConfirmDepHistorys
	 * @param sysDate
	 */
	@Transactional(rollbackFor = Exception.class)
	public void batchUpdateRequireConfirmDepHistory(
	        Set<String> targetUpdateRequireConfirmDepHistorys,
	        Date sysDate) {


		Long startTime = System.currentTimeMillis();
		String procTitle = "UPDATE [tr_require_confirm_dep_history].[create_dep]：" + targetUpdateRequireConfirmDepHistorys.size() + "筆";

		if (WkStringUtils.isEmpty(targetUpdateRequireConfirmDepHistorys)) {
			log.warn(procTitle);
			log.warn("傳入的資料為空，不執行");
			return;
		}

		List<String> historys = Lists.newArrayList(targetUpdateRequireConfirmDepHistorys);

		StringBuffer updateSQL = new StringBuffer();
		updateSQL.append("UPDATE tr_require_confirm_dep_history ");
		updateSQL.append("SET    create_dep = ?, ");
		updateSQL.append("       update_usr = 1, ");
		updateSQL.append("       update_dt = ? ");
		updateSQL.append("WHERE  require_confirm_sid = ?  ");
		updateSQL.append("  AND  create_dep = ? ;");

		int totalSize = historys.size();

		for (int i = 0; i < totalSize; i += BATCH_UPDATE_SIZE) {
			int index = (i + BATCH_UPDATE_SIZE) > totalSize ? totalSize : i + BATCH_UPDATE_SIZE;
			// 取得本次批次執行筆數
			final List<String> batchList = historys.subList(i, index);

			this.jdbcTemplate.batchUpdate(updateSQL.toString(), new BatchPreparedStatementSetter() {

				@Override
				public int getBatchSize() {
					return batchList.size();
				}

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {

					String[] dataKeys = batchList.get(i).split("@");
					String requireCnfirmSid = dataKeys[0];
					String beforeDepSid = dataKeys[1];
					String afterDepSid = dataKeys[2];

					int index = 1;
					// create_dep (set)
					ps.setInt(index++, Integer.parseInt(afterDepSid));
					// update_dt (set)
					ps.setString(index++, df.format(sysDate.getTime()));
					// require_confirm_sid (where)
					ps.setLong(index++, Long.parseLong(requireCnfirmSid));
					// create_dep (where)
					ps.setInt(index++, Integer.parseInt(beforeDepSid));
				}
			});
		}
		log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
	}

	/**
	 * 建立部門查詢資訊
	 * 
	 * @param requireSid
	 * @param requireNo
	 * @param executorSid
	 * @param createDt
	 * @param type
	 * @param depSids
	 * @return
	 */
	public List<AssignSendSearchInfoVO> createDepSearchInfo(
	        String requireSid,
	        String requireNo,
	        Integer executorSid,
	        Date createDt,
	        AssignSendType type,
	        List<Integer> depSids) {

		if (WkStringUtils.isEmpty(depSids)) {
			return Lists.newArrayList();
		}

		List<AssignSendSearchInfoVO> assignSendSearchInfoVOs = Lists.newArrayList();

		// ====================================
		// 整理新增單位
		// ====================================
		// 去重複
		// 不過濾停用, searchInfo 資料應和主檔資料一致
		List<Integer> depInfo = Sets.newHashSet(depSids).stream()
		        // .filter(depSid -> WkOrgUtils.isActive(depSid))
		        .collect(Collectors.toList());

		// ====================================
		// 建立 vo
		// ====================================
		for (Integer depSid : depInfo) {
			AssignSendSearchInfoVO search = new AssignSendSearchInfoVO();
			search.setCreatedDate(createDt);
			search.setCreatedUserSid(executorSid);
			search.setRequireSid(requireSid);
			search.setRequireNo(requireNo);
			search.setType(type);
			search.setTypeInt(AssignSendType.ASSIGN.equals(type) ? 0 : 1);
			search.setDepSid(depSid);
			assignSendSearchInfoVOs.add(search);
		}

		return assignSendSearchInfoVOs;
	}

	/**
	 * 批次新增 tr_sub_notice_info
	 * 
	 * @param insertHistorys
	 */
	@Transactional(rollbackFor = Exception.class)
	public void batchInsertSubNoticeInfo(
	        SubNoticeType subNoticeType,
	        List<OrgTrnsDtVO> selectedDtVOList,
	        Date sysDate) {

		if (WkStringUtils.isEmpty(selectedDtVOList)) {
			log.warn("傳入的 selectedDtVOList 為空，不執行");
			return;
		}
		Long startTime = System.currentTimeMillis();
		String procTitle = "INSERT [tr_sub_notice_info] :" + selectedDtVOList.size() + "筆";
		// log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

		StringBuffer insertSQL = new StringBuffer();
		insertSQL.append("INSERT ");
		insertSQL.append("INTO ");
		insertSQL.append("    tr_sub_notice_info ");
		insertSQL.append("    ( ");
		insertSQL.append("        notice_info_sid, ");
		insertSQL.append("        notice_type, ");
		insertSQL.append("        require_sid, ");
		insertSQL.append("        require_no, ");
		insertSQL.append("        sub_process_sid, ");
		insertSQL.append("        sub_process_no, ");
		insertSQL.append("        old_notice_info, ");
		insertSQL.append("        new_notice_info, ");
		insertSQL.append("        status, ");
		insertSQL.append("        create_usr, ");
		insertSQL.append("        create_dt, ");
		insertSQL.append("        update_usr, ");
		insertSQL.append("        update_dt ");
		insertSQL.append("    ) ");
		insertSQL.append("    VALUES ");
		insertSQL.append("    ( ");
		insertSQL.append("        ?, "); // notice_info_sid
		insertSQL.append("        ?, "); // notice_type
		insertSQL.append("        ?, "); // require_sid
		insertSQL.append("        ?, "); // require_no
		insertSQL.append("        ?, "); // sub_process_sid
		insertSQL.append("        ?, "); // sub_process_no
		insertSQL.append("        ?, "); // old_notice_info
		insertSQL.append("        ?, "); // new_notice_info
		insertSQL.append("        0, ");
		insertSQL.append("        1, ");
		insertSQL.append("        ?, "); // create_dt
		insertSQL.append("        1, ");
		insertSQL.append("        ? "); // update_dt
		insertSQL.append("    );");

		JsonStringListToConverter converter = new JsonStringListToConverter();

		int totalSize = selectedDtVOList.size();
		for (int i = 0; i < totalSize; i += BATCH_UPDATE_SIZE) {
			int index = (i + BATCH_UPDATE_SIZE) > totalSize ? totalSize : i + BATCH_UPDATE_SIZE;
			// 取得本次批次執行筆數
			final List<OrgTrnsDtVO> batchList = selectedDtVOList.subList(i, index);

			this.jdbcTemplate.batchUpdate(insertSQL.toString(), new BatchPreparedStatementSetter() {

				@Override
				public int getBatchSize() {
					return batchList.size();
				}

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {

					OrgTrnsDtVO item = batchList.get(i);
					int index = 1;

					// notice_info_sid
					ps.setString(index++, UUID.randomUUID().toString());
					// notice_type
					ps.setString(index++, subNoticeType.name());
					// require_sid
					ps.setString(index++, item.getRequireSid());
					// require_no
					ps.setString(index++, item.getMasterNo());
					// sub_process_sid
					ps.setString(index++, item.getSid());
					// sub_process_no
					ps.setString(index++, item.getCaseNo());
					// old_notice_info
					ps.setString(index++, converter.convertToDatabaseColumn(item.getOldNoticeDepSids()));
					// new_notice_info
					ps.setString(index++, converter.convertToDatabaseColumn(item.getNewNoticeDepSids()));
					// create_dt
					ps.setString(index++, df.format(sysDate.getTime()));
					// update_dt
					ps.setString(index++, df.format(sysDate.getTime()));

				}
			});
		}
		log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
	}
	
	
	   /**
     * 批次 update tr_assign_send_info
     * 
     * @param assignSendInfoForTrnsVOs
     * @param sysdate
     */
    @Transactional(rollbackFor = Exception.class)
    public void batchUpdateAlertInbox(
            List<OrgTrnsAssignSendInfoForTrnsTO> assignSendInfoForTrnsVOs,
            AssignSendType type,
            Date sysdate) {

        if (WkStringUtils.isEmpty(assignSendInfoForTrnsVOs)) {
            log.warn("傳入的 assignSendInfoForTrnsVOs 為空，不執行");
            return;
        }

        Long startTime = System.currentTimeMillis();
        String procTitle = "UPDATE [tr_alert_inbox] SET [receive_dep = ] ->" 
                + "[" + assignSendInfoForTrnsVOs.size() + "]筆";

        StringBuffer updateSQL = new StringBuffer();
        updateSQL.append("UPDATE tr_alert_inbox ");
        updateSQL.append("SET    receive_dep = ? ");
        updateSQL.append("WHERE  info_sid = ? ");
        updateSQL.append("  AND  type = " + type.ordinal() + " ; ");

        int totalSize = assignSendInfoForTrnsVOs.size();
        for (int i = 0; i < totalSize; i += BATCH_UPDATE_SIZE) {
            int index = (i + BATCH_UPDATE_SIZE) > totalSize ? totalSize : i + BATCH_UPDATE_SIZE;
            // 取得本次批次執行筆數
            final List<OrgTrnsAssignSendInfoForTrnsTO> batchList = assignSendInfoForTrnsVOs.subList(i, index);

            this.jdbcTemplate.batchUpdate(updateSQL.toString(), new BatchPreparedStatementSetter() {

                @Override
                public int getBatchSize() {
                    return batchList.size();
                }

                @Override
                public void setValues(PreparedStatement ps, int i) throws SQLException {

                    OrgTrnsAssignSendInfoForTrnsTO item = batchList.get(i);
                    int index = 1;
                    // info
                    ps.setString(index++, item.getNewAssignInfoJsonStr());
                    // info_sid
                    ps.setString(index++, item.getSid());
                }
            });
        }
        log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
    }

}
