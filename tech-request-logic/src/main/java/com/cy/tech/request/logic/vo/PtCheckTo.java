/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.vo;

import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author kasim
 */
@Data
@NoArgsConstructor
public class PtCheckTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3568966057070051353L;

    /** 原型確認sid */
    private String sid;

    /** 原型確認單號 */
    private String ptNo;

    /** 原型確認來源 單號 */
    private String sourceNo;

    /** 原型確認通知對象 */
    private JsonStringListTo noticeMember;

    /** 原型確認通知部門 */
    private JsonStringListTo noticeDeps;

    /** 是否有流程 */
    private Boolean hasSign;

    /** 原型確認主題 */
    private String theme;

    /** 原型確認內容 */
    private String content;

    /** 原型確認內容 含css */
    private String contentCss;

    /** 備註說明 */
    private String note;

    /** 備註說明 含css */
    private String noteCss;

    /** 預計完成日 */
    private Date establishDate;

    /** 實際完成日 */
    private Date finishDate;

    /** 原型確認重做日期 */
    private Date redoDate;

    /** 此次原型確認版本 */
    private Integer version;

    /** 原型確認狀態 */
    private PtStatus ptStatus;

    /** 異動日期 */
    private Date updatedDate;
    
    /**
     * 需求(填單)部門
     */
    private Integer createDepSid;
    /**
     * 需(填單)求者
     */
    private Integer createUsr;
    
    /**
     * 流程已完成
     * @return
     */
    public boolean isFinish() {
        return !Lists.newArrayList(PtStatus.SIGN_PROCESS,
                PtStatus.APPROVE,
                PtStatus.PROCESS).contains(this.ptStatus);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.getSid());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PtCheckTo other = (PtCheckTo) obj;
        if (!Objects.equals(this.getSid(), other.getSid())) {
            return false;
        }
        return true;
    }
}
