/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.callable.Search16ViewCallable;
import com.cy.tech.request.logic.search.view.Search16View;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.customer.ReqWorkCustomerHelper;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * @author jason_h
 */
@Service
@Slf4j
public class Search16QueryService implements QueryService<Search16View>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 322603766896032457L;
    @Autowired
    private URLService urlService;
    @Autowired
    private WkJsonUtils jsonUtils;
    @Autowired
    private SearchService searchHelper;
    @PersistenceContext
    transient private EntityManager em;

    @Autowired
    transient private ReqWorkCustomerHelper customerService;

    @SuppressWarnings("unchecked")
    @Override
    public List<Search16View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {

        // ====================================
        // 查詢
        // ====================================
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        List<Object[]> result = query.getResultList();
        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if (WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();
        // 解析欄位前準備
        Map<Integer, String> customerNameMapByCustId = this.prepareCustomerName(result);

        // 解析欄位

        List<Search16View> viewResult = Lists.newArrayList();
        List<Search16ViewCallable> search16ViewCallables = Lists.newArrayList();
        for (int i = 0; i < result.size(); i++) {
            Object[] record = (Object[]) result.get(i);

            Search16ViewCallable search16ViewCallable = new Search16ViewCallable(record, execUserSid, urlService,
                    jsonUtils, searchHelper, customerNameMapByCustId, i);
            search16ViewCallables.add(search16ViewCallable);
        }
        if (search16ViewCallables.isEmpty()) {
            return viewResult;
        }
        int poolNum = 50;
        if (search16ViewCallables.size() < poolNum) {
            poolNum = search16ViewCallables.size();
        }

        ExecutorService pool = Executors.newFixedThreadPool(poolNum);
        try {
            @SuppressWarnings("rawtypes")
            CompletionService completionPool = new ExecutorCompletionService(pool);
            search16ViewCallables.forEach(item -> {
                completionPool.submit(item);
            });
            IntStream.range(0, search16ViewCallables.size()).forEach(i -> {
                try {
                    Object obj = completionPool.take().get();
                    if (obj == null) {
                        return;
                    }
                    Search16View search16View = (Search16View) obj;
                    viewResult.add(search16View);
                } catch (Exception e) {
                    log.error("search16ViewCallables", e);
                }
            });
        } catch (Exception e) {
            log.error("search16ViewCallables", e);
        } finally {
            pool.shutdown();
        }

        // sort by age
        Collections.sort(viewResult, new Comparator<Search16View>() {
            @Override
            public int compare(Search16View o1, Search16View o2) {
                return o1.getIndex() - o2.getIndex();
            }
        });
        // 解析資料-結束
        usageRecord.parserDataEnd();

        return viewResult;
    }

    /**
     * 準備廳主名稱
     * 
     * @param result
     * @return
     */
    private Map<Integer, String> prepareCustomerName(List<Object[]> result) {

        if (WkStringUtils.isEmpty(result)) {
            return Maps.newHashMap();
        }

        // ====================================
        // 收集 cus_id
        // ====================================
        Set<Integer> custIDs = Sets.newHashSet();
        for (Object[] fileds : result) {
            if (fileds.length < 24) {
                continue;
            }

            // Integer customer = (Integer) record[23];
            // Integer author = (Integer) record[24];

            if (fileds[22] != null) {
                custIDs.add((Integer) fileds[22]);
            }
            if (fileds[23] != null) {
                custIDs.add((Integer) fileds[23]);
            }

        }

        // ====================================
        // 查詢
        // ====================================
        try {
            return this.customerService.findNameMapByCustId(custIDs);
        } catch (Exception e) {
            log.error("查詢廳主名稱失敗!" + e.getMessage(), e);
        }

        return Maps.newHashMap();
    }

}
