/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.config;

/**
 * 系統常數
 *
 * @author kasim
 */
public class ReqConstants {

	/** 呼叫及宣告JDBC TEMPLATE 用 */
	public final static String REQ_JDBC_TEMPLATE = "REQ_JDBC_TEMPLATE";

	public final static String REQ_CONFIRM = "製作進度確認";
}
