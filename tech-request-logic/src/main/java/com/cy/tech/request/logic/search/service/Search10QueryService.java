/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.anew.manager.TrIssueMappTransManager;
import com.cy.tech.request.logic.search.view.Search10View;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

/**
 * @author jason_h
 */
@Service
public class Search10QueryService implements QueryService<Search10View>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5233462210253281115L;
    @Autowired
    transient private TrIssueMappTransManager issueMappTransManager;
    @Autowired
    private URLService urlService;
    @Autowired
    private SearchService searchHelper;

    @PersistenceContext
    transient private EntityManager em;

    @Override
    public List<Search10View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {

        // ====================================
        // 查詢
        // ====================================

        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        @SuppressWarnings("unchecked")
        List<Object[]> result = query.getResultList();
        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if (WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }


        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();
        List<Search10View> viewResult = Lists.newArrayList();

        /** 轉入需求 20170427 add */
        List<String> reqSids = (List<String>) result.stream().map(each -> {
            Object[] temp = (Object[]) each;
            return (String) temp[0];
        }).collect(Collectors.toList());
        List<String> mappReqSids = issueMappTransManager.createTransFromIssue(reqSids);

        for (int i = 0; i < result.size(); i++) {

            Search10View v = new Search10View();

            Object[] record = (Object[]) result.get(i);
            int index = 0;
            String sid = (String) record[index++];
            String requireNo = (String) record[index++];
            String requireTheme = (String) record[index++];
            Integer urgency = (Integer) record[index++];
            Boolean hasForwardDep = (Boolean) record[index++];
            Boolean hasForwardMember = (Boolean) record[index++];
            Boolean hasLink = (Boolean) record[index++];
            Date createdDate = (Date) record[index++];
            String bigName = (String) record[index++];
            String middleName = (String) record[index++];
            String smallName = (String) record[index++];
            Integer createDep = (Integer) record[index++];
            Integer createdUser = (Integer) record[index++];
            String requireStatus = (String) record[index++];
            Boolean checkAttachment = (Boolean) record[index++];
            Boolean hasAttachment = (Boolean) record[index++];
            Date hopeDate = (Date) record[index++];
            Integer inteStaffUser = (Integer) record[index++];
            // REQ-1031
            Date requireEstablishDate = (Date) record[index++];
            Date requireFinishDate = (Date) record[index++];
            String hasTrace = (String) record[index++];
            // 處理共通欄位 (一定要擺在『取值』最後面, 否則 index 會不對)
            v.prepareCommonColumn(record, index);

            v.setSid(sid);
            v.setRequireNo(requireNo);
            v.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
            v.setUrgency(UrgencyType.values()[urgency]);
            v.setHasForwardDep(hasForwardDep);
            v.setHasForwardMember(hasForwardMember);
            v.setHasLink(hasLink);
            v.setCreatedDate(createdDate);
            v.setBigName(bigName);
            v.setMiddleName(middleName);
            v.setSmallName(smallName);
            v.setCreateDep(createDep);
            v.setCreatedUser(createdUser);
            v.setRequireStatus(RequireStatusType.valueOf(requireStatus));

            v.setIsTranFromIssue(mappReqSids.contains(sid) ? "Y" : "");

            v.setCheckAttachment(checkAttachment);
            v.setHasAttachment(hasAttachment);
            v.setHopeDate(hopeDate);

            // REQ-1031
            v.setExecDays(
                    this.searchHelper.calcExecDays(
                            v.getRequireStatus(),
                            requireEstablishDate,
                            requireFinishDate));

            v.setLocalUrlLink(urlService.createLoacalURLLink(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(execUserSid, v.getRequireNo(), 1)));
            v.setHasTrace(Boolean.valueOf(hasTrace));
            v.setInteStaffUser(inteStaffUser);
            viewResult.add(v);
        }
        
        // 解析資料-結束
        usageRecord.parserDataEnd();
        return viewResult;
    }
}
