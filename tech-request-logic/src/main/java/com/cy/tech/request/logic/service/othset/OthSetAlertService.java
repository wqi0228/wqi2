/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.othset;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.repository.require.othset.OthSetAlertRepo;
import com.cy.tech.request.vo.enums.OthSetAlertType;
import com.cy.tech.request.vo.require.othset.OthSet;
import com.cy.tech.request.vo.require.othset.OthSetAlert;
import com.cy.tech.request.vo.require.othset.OthSetAlreadyReply;
import com.cy.tech.request.vo.require.othset.OthSetHistory;
import com.cy.tech.request.vo.require.othset.OthSetReply;
import com.google.common.collect.Sets;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 最新消息
 *
 * @author shaun
 */
@Component
public class OthSetAlertService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3314249078984412347L;
    @Autowired
    private OthSetAlertRepo alertDao;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private UserService userService;
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
    

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void createOthSetAlert(OthSet othset, User sender) {
        Executors.newCachedThreadPool().execute(() -> {
            List<Org> noticeOrgs = orgService.findBySids(othset.getNoticeDeps().getValue());
            List<User> receiversUsers = userService.findByStatusAndPrimaryOrgIn(Activation.ACTIVE, noticeOrgs);
            receiversUsers.forEach(receiver
                    -> alertDao.save(this.createEmptyAlert(othset, sender, receiver, OthSetAlertType.NOTICE_WORKING)));
        });
    }

    private OthSetAlert createEmptyAlert(OthSet othset, User sender, User receiver, OthSetAlertType type) {
        OthSetAlert alert = new OthSetAlert();
        alert.setOthset(othset);
        alert.setOsNo(othset.getOsNo());
        alert.setRequire(othset.getRequire());
        alert.setRequireNo(othset.getRequireNo());
        alert.setOsTheme(othset.getTheme());
        alert.setSender(sender);
        alert.setSendDate(new Date());
        alert.setReceiver(receiver);
        alert.setReadStatus(ReadRecordType.UN_READ);
        alert.setType(type);
        return alert;
    }
    
     

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void carateAlreadyReplyAlert(OthSetAlreadyReply alreadyReply) {
        Executors.newCachedThreadPool().execute(() -> {
            Set<User> receivers = this.findSenderByReply(alreadyReply.getReply());
            //只取sid，以免not equals 
            User replyPerson = new User();
            replyPerson.setSid(alreadyReply.getReply().getPerson().getSid());
            receivers.add(replyPerson);
            receivers.forEach(receiver -> {
                OthSetAlert alert = this.createEmptyAlert(
                        alreadyReply.getOthset(),
                        alreadyReply.getPerson(),
                        receiver, OthSetAlertType.REPLY_AND_REPLY
                );
                alert.setOsTheme(sdf.format(new Date()) + " " + alert.getOsTheme() + " - 回覆通知");
                alert.setReply(alreadyReply.getReply());
                alert.setAlreadyReply(alreadyReply);
                alertDao.save(alert);
            });
        });
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void createCancelAlert(OthSetHistory history) {
        //開始作業訊息更新成已閱讀
        alertDao.updateReadStatus(history.getOthset(), OthSetAlertType.NOTICE_WORKING, ReadRecordType.SYSTEM_UPDATE);
        Executors.newCachedThreadPool().execute(() -> {
            List<Org> noticeOrgs = orgService.findBySids(history.getOthset().getNoticeDeps().getValue());
            List<User> receiversUsers = userService.findByStatusAndPrimaryOrgIn(Activation.ACTIVE, noticeOrgs);
            receiversUsers.forEach(receiver -> {
                OthSetAlert alert = this.createEmptyAlert(
                        history.getOthset(),
                        WkUserCache.getInstance().findBySid(history.getCreatedUser().getSid()),
                        receiver, 
                        OthSetAlertType.CANCEL
                );
                alert.setOsTheme(sdf.format(new Date()) + " " + alert.getOsTheme() + " - 已作廢");
                alertDao.save(alert);
            });
        });
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void createCancelAlert(OthSet othSet, User createUser) {
        //開始作業訊息更新成已閱讀
        alertDao.updateReadStatus(othSet, OthSetAlertType.NOTICE_WORKING, ReadRecordType.SYSTEM_UPDATE);
        Executors.newCachedThreadPool().execute(() -> {
            List<Org> noticeOrgs = orgService.findBySids(othSet.getNoticeDeps().getValue());
            List<User> receiversUsers = userService.findByStatusAndPrimaryOrgIn(Activation.ACTIVE, noticeOrgs);
            receiversUsers.forEach(receiver -> {
                OthSetAlert alert = this.createEmptyAlert(
                        othSet,
                        WkUserCache.getInstance().findBySid(createUser.getSid()),
                        receiver, OthSetAlertType.CANCEL
                );
                alert.setOsTheme(sdf.format(new Date()) + " " + alert.getOsTheme() + " - 已作廢");
                alertDao.save(alert);
            });
        });
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void createCompleteAlert(OthSetHistory history) {
        //開始作業訊息更新成已閱讀
        alertDao.updateReadStatus(history.getOthset(), OthSetAlertType.NOTICE_WORKING, ReadRecordType.SYSTEM_UPDATE);
        Executors.newCachedThreadPool().execute(() -> {
            List<Org> noticeOrgs = orgService.findBySids(history.getOthset().getNoticeDeps().getValue());
            List<User> receiversUsers = userService.findByStatusAndPrimaryOrgIn(Activation.ACTIVE, noticeOrgs);
            receiversUsers.forEach(receiver -> {
                OthSetAlert alert = this.createEmptyAlert(
                        history.getOthset(),
                        WkUserCache.getInstance().findBySid(history.getCreatedUser().getSid()),
                        receiver, OthSetAlertType.FINISH
                );
                alert.setOsTheme(sdf.format(new Date()) + " " + alert.getOsTheme() + " - 已完成");
                alertDao.save(alert);
            });
        });
    }

    private Set<User> findSenderByReply(OthSetReply reply) {
        return Sets.newHashSet(alertDao.findSenderByReply(reply));
    }

    @Transactional(readOnly = true)
    public List<OthSetAlert> findByReadStatusAndReceiverLimit(ReadRecordType readStatus, User receiver, Integer limit) {
        return alertDao.findByReadStatusAndReceiverOrderBySendDateDesc(
                readStatus, receiver, new PageRequest(0, limit));
    }

    @Transactional(readOnly = true)
    public List<OthSetAlert> findByReadStatusAndReceiver(ReadRecordType readStatus, User receiver) {
        return alertDao.findByReadStatusAndReceiverOrderBySendDateDesc(readStatus, receiver);
    }

    @Transactional(readOnly = true)
    public OthSetAlert findBySid(String sid) {
        return alertDao.findOne(sid);
    }

    @Transactional(rollbackFor = Exception.class)
    public void save(OthSetAlert alert) {
        alertDao.save(alert);
    }
}
