/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import com.cy.tech.request.vo.require.AlertInboxSendGroup;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.logic.lib.manager.to.WorkCommonReadRecordTo;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

/**
 * 收部門轉發頁面顯示vo (home02.xhtml)
 *
 * @author jason_h
 */
public class Home02View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7233634214334066424L;
    /** 需求單sid */
    @Getter
    @Setter
    private String requireSid;
    
    /** 緊急度 */
    @Getter
    @Setter
    private UrgencyType urgency;
    /** 部 */
    @Getter
    @Setter
    private Boolean hasForwardDep;
    /** 個 */
    @Getter
    @Setter
    private Boolean hasForwardMember;
    /** 關 */
    @Getter
    @Setter
    private Boolean hasLink;
    /** 寄件備份 */
    @Getter
    @Setter
    private AlertInboxSendGroup inboxSendGroup;
    /** 寄發單位 */
    @Getter
    @Setter
    private Integer senderDep;
    /** 寄發者 */
    @Getter
    @Setter
    private Integer sender;
    /** 寄發時間 */
    @Getter
    @Setter
    private Date createdDate;
    /** 寄發至 */
    @Getter
    @Setter
    private List<Integer> receiveDeps;
    /** 寄發至 */
    @Getter
    @Setter
    private String receiveDepName;
    /** 類型 */
    @Getter
    @Setter
    private String bigName;
    /** 需求單-異動日期 */
    @Getter
    @Setter
    private Date requireUpdatedDate;
    /** 本地端連結網址 */
    @Getter
    @Setter
    private String localUrlLink;
    /** 閱讀記錄清單 */
    @Getter
    @Setter
    private List<WorkCommonReadRecordTo> readRecordTos = Lists.newArrayList();

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(super.getSid());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Home02View other = (Home02View) obj;
        if (!Objects.equals(super.getSid(), other.getSid())) {
            return false;
        }
        return true;
    }
}
