/**
 * 
 */
package com.cy.tech.request.logic.service.setting.onetimetrns;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author allen1214_wu
 *
 */
@Getter
@Setter
@NoArgsConstructor
public class TrnsCheckItemsTo implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -127408672361733081L;
    // 需求單 sid
    private String requireSid;
    // 製作進度
    private String requireStatus;
    // 舊的系統別資料
    private String webCode;
    // 需求分類 EXTERNAL=外部 INTERNAL=內部
    private String requireCategory;
    // 單據立案人員
    private Integer requireCreateUserSid;
    // 單據立案時間
    private Date requireCreateDate;
    // 草稿建立時間
    private Date requireDraftDate;
    // 檢查人員
    private Integer checkUserSid;
    // 檢查時間
    private Date checkDate;
    //
    private Integer passUserSid;
    //
    private Date passDate;
}
