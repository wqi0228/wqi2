package com.cy.tech.request.logic.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepService;
import com.cy.tech.request.repository.require.ReqModifyRepo;
import com.cy.tech.request.repository.require.RequireTempNotifyRepository;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireFinishCodeType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireConfirmDep;
import com.cy.tech.request.vo.require.RequireTrace;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.notify.logic.manager.WorkNotifyManager;
import com.cy.work.notify.vo.enums.NotifyType;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * 需求製作進度 - 反需求完成
 * 
 * @author allen1214_wu
 */
@Slf4j
@Component
public class RequireProcessCompleteRollbackService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6715119759118276988L;
    // =======================================================================================
    // 服務物件區
    // =======================================================================================
    @Autowired
    private transient RequireTraceService traceService;
    @Autowired
    private transient RequireService requireService;
    @Autowired
    private transient RequireConfirmDepService requireConfirmDepService;
    @Autowired
    private WorkNotifyManager workNotifyManager;
    @Autowired
    private transient ReqModifyRepo reqModifyDao;
    @Autowired
    private RequireTempNotifyRepository reqTempNotifyRepository;
    @Autowired
    private transient RequireReadRecordHelper requireReadRecordHelper;

    // =======================================================================================
    // 執行完成方法區
    // =======================================================================================
    /**
     * 使用者執行反需求完成
     * 
     * @param requireSid
     * @param execUser
     * @param sysDate
     * @param traceMessage
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean executeByUser(
            String requireSid,
            User execUser,
            Date sysDate,
            String traceMessage) {

        return this.process(
                requireSid,
                execUser,
                sysDate,
                RequireTraceType.UNREQUIRE_FINISH,
                traceMessage);

    }

    /**
     * 因部分操作, 需復原單據狀態 (已完成 -> 進行中)
     * 
     * @param require
     * @param execUser
     * @param sysDate
     * @param operationName 新增原型確認單 | 開立送測單 | 開立ON程式單 | MMS同步ON程式單 | 再次新增其他設定資訊單
     */
    public void executeProcess(
            String requireSid,
            User execUser,
            Date sysDate,
            String operationName) {

        // ====================================
        // 組追蹤訊息
        // ====================================
        String traceMessage = execUser.getName()
                + " 於 "
                + WkDateUtils.formatDate(sysDate, WkDateUtils.YYYY_MM_DD_HH24_mm_ss)
                + " 因【"
                + operationName
                + "】，取消需求完成。";

        // ====================================
        // 執行
        // ====================================
        this.process(
                requireSid,
                execUser,
                sysDate,
                RequireTraceType.CANCEL_REQUIRE_FINISH,
                traceMessage);

    }

    /**
     * 因需求完成確認單位進行復原, 而復原需求進度
     * 
     * @param require
     * @param requireConfirmDep
     * @param execUser
     * @param sysDate
     * @return
     */
    public boolean executeByConfirmDepRecovery(
            String requireSid,
            RequireConfirmDep requireConfirmDep,
            User execUser,
            Date sysDate) {

        // ====================================
        // 組追蹤訊息
        // ====================================
        String traceMessage = "因【"
                + WkOrgCache.getInstance().findNameBySid(requireConfirmDep.getDepSid())
                + "-"
                + execUser.getName()
                + "】復原單位確認進度，故取消需求完成";

        // ====================================
        // 執行
        // ====================================
        return this.process(
                requireSid,
                execUser,
                sysDate,
                RequireTraceType.CANCEL_REQUIRE_FINISH,
                traceMessage);

    }

    /**
     * 進行分派單位異動, 而復原需求進度<br/>
     * 
     * @param require
     * @param requireConfirmDep
     * @param execUser
     * @param sysDate
     * @return
     */
    public boolean executeByModifyAssignDep(
            String requireSid,
            User execUser,
            Date sysDate) {

        // ====================================
        // 查詢現有的需求完成確認單位檔
        // ====================================
        List<RequireConfirmDep> requireConfirmDeps = this.requireConfirmDepService.findByRequireSid(requireSid);
        if (requireConfirmDeps == null) {
            requireConfirmDeps = Lists.newArrayList();
        }

        if (requireConfirmDeps.stream().allMatch(each -> each.getProgStatus() != null && each.getProgStatus().isInFinish())) {
            return false;
        }

        // ====================================
        // 組追蹤訊息
        // ====================================
        String traceMessage = " 執行【異動分派單位】作業後，因有分派單位未完成需求確認，因此取消需求完成。";

        // ====================================
        // 執行
        // ====================================
        return this.process(
                requireSid,
                execUser,
                sysDate,
                RequireTraceType.CANCEL_REQUIRE_FINISH,
                traceMessage);

    }

    // =======================================================================================
    // 核心流程
    // =======================================================================================
    /**
     * 執行反需求完成 (已完成 -> 進行中) (核心流程)
     * 
     * @param require
     * @param execUser
     * @param sysDate
     * @param requireTraceType
     * @param traceMessage
     */
    @Transactional(rollbackFor = Exception.class)
    private boolean process(
            String requireSid,
            User execUser,
            Date sysDate,
            RequireTraceType requireTraceType,
            String traceMessage) {

        // ====================================
        // 非完成狀態無需處理
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);
        // 依據進度的狀態, 判斷是否需執行
        if (require == null || require.getRequireStatus() == null || !require.getRequireStatus().isCanUseRollBackFinish()) {
            return false;
        }

        // ====================================
        // 異動需求主檔
        // ====================================
        // 完成代碼：未完成
        require.setFinishCode(RequireFinishCodeType.INCOMPLETE);
        // 清空完成資訊
        require.setFinishMethod(null);
        require.setFinishDate(null);
        require.setRequireFinishUsr(null);
        // 製作進度改為 進行中
        require.setRequireStatus(RequireStatusType.PROCESS);
        // 倒回未結案
        require.setCloseDate(null);
        require.setCloseUser(null);
        require.setCloseCode(false);
        // 更新待閱讀記錄
        require.setReadReason(ReqToBeReadType.COMPLETED_TO_PROCESS);


        // 因新增清除 close 相關欄位, 改為save entity
        this.reqModifyDao.save(require);
        
        // ====================================
        // 將『已經讀取』過單據的人，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.REQUIRE,
                require.getSid(),
                execUser.getSid());

        // ====================================
        // 新增追蹤
        // ====================================
        RequireTrace trace = this.traceService.createNewTrace(requireSid, execUser, sysDate);
        trace.setRequireTraceType(requireTraceType);
        trace.setRequireTraceContent(WkJsoupUtils.getInstance().clearCssTag(traceMessage));
        trace.setRequireTraceContentCss(traceMessage);
        this.traceService.save(trace);

        // ====================================
        // 再次開啟狀態為『無須確認』的需求完成確認單位流程
        // ====================================
        this.requireConfirmDepService.changeProgStatusPassByRequireCompleteRollback(
                require.getRequireNo(),
                requireSid,
                execUser.getSid(),
                sysDate,
                traceMessage);

        // ====================================
        // 移除暫存檔 - （功能未知?）
        // ====================================
        List<String> sids = reqTempNotifyRepository.findSidByRequireNo(require.getRequireNo());
        this.requireService.deleteReqTempNotify(sids);

        // ====================================
        // 移除推撥 - 待結案通知、結案通知
        // ====================================
        this.workNotifyManager.deleteByTypeAndSourceNo(
                Lists.newArrayList(NotifyType.REQUEST_WAIT_CLOSE, NotifyType.REQUEST_CLOSE),
                require.getRequireNo());

        log.info(String.format("【%s-執行反需求完成】：%s", require.getRequireNo(), traceMessage));
        return true;
    }

}
