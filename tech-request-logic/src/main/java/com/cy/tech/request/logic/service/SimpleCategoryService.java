package com.cy.tech.request.logic.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import com.cy.tech.request.repository.category.SimpleBigCategoryRepository;
import com.cy.tech.request.repository.category.SimpleMiddleCategoryRepository;
import com.cy.tech.request.repository.category.SimpleSmallCategoryRepository;
import com.cy.tech.request.vo.anew.enums.SignLevelType;
import com.cy.tech.request.vo.category.SimpleBigCategory;
import com.cy.tech.request.vo.category.SimpleBigCategoryVO;
import com.cy.tech.request.vo.category.SimpleMiddleCategory;
import com.cy.tech.request.vo.category.SimpleMiddleCategoryVO;
import com.cy.tech.request.vo.category.SimpleSmallCategory;
import com.cy.tech.request.vo.category.SimpleSmallCategoryVO;
import com.cy.tech.request.vo.constants.CacheConstants;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Service
@Slf4j
public class SimpleCategoryService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6818960174386590840L;

    /**
     * 
     */
    @Autowired
    private SimpleBigCategoryRepository bigCategoryDAO;

    /**
     * 
     */
    @Autowired
    private SimpleMiddleCategoryRepository middleCategoryDAO;

    /**
     * 
     */
    @Autowired
    private SimpleSmallCategoryRepository smallCategoryDAO;

    /**
     * 查詢所有大類
     */
    public List<SimpleBigCategoryVO> findAllBigCategory() {

        // ====================================
        // 查詢
        // ====================================
        List<SimpleBigCategory> results = this.bigCategoryDAO.findAllByOrderByIdAsc();

        if (WkStringUtils.isEmpty(results)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 轉型
        // ====================================
        return results.stream()
                .map(entity -> new SimpleBigCategoryVO(entity))
                .collect(Collectors.toList());
    }

    /**
     * 查詢所有中類
     */
    public List<SimpleMiddleCategoryVO> findAllMiddleCategory() {

        // ====================================
        // 查詢
        // ====================================
        List<SimpleMiddleCategory> results = this.middleCategoryDAO.findAllByOrderByIdAsc();

        if (WkStringUtils.isEmpty(results)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 轉型
        // ====================================
        return results.stream()
                .map(entity -> new SimpleMiddleCategoryVO(entity))
                .collect(Collectors.toList());
    }

    /**
     * 查詢所有中類
     */
    public List<SimpleSmallCategoryVO> findAllSmallCategory() {

        // ====================================
        // 查詢
        // ====================================
        List<SimpleSmallCategory> results = this.smallCategoryDAO.findAllByOrderByIdAsc();

        if (WkStringUtils.isEmpty(results)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 轉型
        // ====================================
        return results.stream()
                .map(entity -> new SimpleSmallCategoryVO(entity))
                .collect(Collectors.toList());
    }

    /**
     * 以中類查詢下層所有的小類
     */
    public List<SimpleSmallCategoryVO> findSmallCategoryByMiddleCategorySid(String middleCategorySid) {

        // ====================================
        // 查詢
        // ====================================
        List<SimpleSmallCategory> results = this.smallCategoryDAO.findByParentMiddleCategorySidOrderByIdAsc(middleCategorySid);

        if (WkStringUtils.isEmpty(results)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 轉型
        // ====================================
        return results.stream()
                .map(entity -> new SimpleSmallCategoryVO(entity))
                .collect(Collectors.toList());
    }

    /**
     * 以 sid 查詢中類
     * 
     * @param middleCategorySid 中類 sid
     * @return SimpleMiddleCategoryVO
     */
    public SimpleMiddleCategoryVO findMiddleCategoryBySid(String middleCategorySid) {

        // ====================================
        // 查詢
        // ====================================
        SimpleMiddleCategory entity = this.middleCategoryDAO.findBySid(middleCategorySid);

        if (entity == null) {
            return null;
        }
        // ====================================
        // 轉型
        // ====================================
        return new SimpleMiddleCategoryVO(entity);
    }

    /**
     * 更新是否簽核項目
     * 
     * @param smallCategorySid 小類 sid
     * @param signType         要異動的簽核設定欄位
     * @param isSign           是否需簽核
     * @param execUserSid      異動人員
     * @throws UserMessageException 找不到資料時拋出
     */
    @Caching(evict = {
            @CacheEvict(value = CacheConstants.CACHE_TEMP_FIELD, allEntries = true),
            @CacheEvict(value = CacheConstants.CACHE_CATE_KEY_MAPPING, allEntries = true),
            @CacheEvict(value = CacheConstants.CACHE_ALL_CATEGORY_PICKER, allEntries = true),
            @CacheEvict(value = CacheConstants.CACHE_findActiveBigCategory, allEntries = true)
    })
    public void updateIsSign(
            String smallCategorySid,
            String signType,
            boolean isSign,
            Integer execUserSid) throws UserMessageException {
        // ====================================
        // 查詢
        // ====================================
        SimpleSmallCategory entity = this.smallCategoryDAO.findBySid(smallCategorySid);
        if (entity == null) {
            String message = "找不到指定的小類:[" + smallCategorySid + "]";
            log.warn(message);
            throw new UserMessageException(message, InfomationLevel.WARN);
        }

        // ====================================
        // 判斷
        // ====================================
        switch (signType) {
        case "testSign":
            if (entity.getTestSign().equals(isSign)) {
                log.debug("無需異動資料");
                return;
            }
            entity.setTestSign(isSign);
            break;

        case "ptSign":
            if (entity.getPtSign().equals(isSign)) {
                log.debug("無需異動資料");
                return;
            }
            entity.setPtSign(isSign);
            break;
        default:
            throw new SystemDevelopException("開發時期錯誤!未定義的簽核類型:[" + signType + "]");
        }

        // ====================================
        // 更新資料
        // ====================================
        entity.setUpdatedUser(execUserSid);
        entity.setUpdatedDate(new Date());
        this.smallCategoryDAO.save(entity);
    }

    /**
     * 查詢簽核層級
     * 
     * @param smallSid
     * @return
     * @throws SystemOperationException
     */
    public SignLevelType findSignLevelBySid(String smallSid) throws SystemOperationException {

        // 查詢
        SignLevelType signLevelType = this.smallCategoryDAO.findSignLevelBySid(smallSid);

        // 判斷資料錯誤
        if (signLevelType == null) {
            String errorMessage = WkMessage.EXECTION + "SignLevelType 為空! basic_data_small_category_sid:[" + smallSid + "]";
            log.error(errorMessage);
            throw new SystemOperationException(errorMessage, InfomationLevel.ERROR);
        }

        return signLevelType;
    }

    /**
     * 是否需要需求單位簽核流程
     * 
     * @param smallSid 小類 sid
     * @return
     * @throws SystemOperationException
     */
    public boolean isNeedReqUnitSign(String smallSid) throws SystemOperationException {
        SignLevelType signLevelType = this.findSignLevelBySid(smallSid);
        return !SignLevelType.NO_SIGN.equals(signLevelType);
    }

}
