package com.cy.tech.request.logic.service.setting;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.onpg.OnpgService;
import com.cy.tech.request.logic.service.othset.OthSetService;
import com.cy.tech.request.logic.service.pt.PtService;
import com.cy.tech.request.logic.service.send.test.SendTestService;
import com.cy.tech.request.repository.result.TransRequireVO;
import com.cy.tech.request.vo.enums.RequireTransProgramType;
import com.google.common.collect.Lists;

@Service
public class BatchDeptDispatchService {
    @Autowired
    private RequireService requireService;
    @Autowired
    private PtService ptService;
    @Autowired
    private SendTestService sendTestService;
    @Autowired
    private OthSetService othSetService;
    @Autowired
    private OnpgService onpgService;

    /**
     * 建立單位轉發查詢
     */
    public List<TransRequireVO> searchByCreatedDepts(RequireTransProgramType type, List<Integer> deptSids, List<Integer> userSids) {
        List<TransRequireVO> resultList = Lists.newArrayList();
        // 查詢全部
        if (type == null) {
            resultList.addAll(requireService.findRequireByCreateDeptAndUser(deptSids, userSids));
            resultList.addAll(ptService.findPtByCreateDeptAndUser(deptSids, userSids));
            resultList.addAll(sendTestService.findSendTestByCreateDeptAndUser(deptSids, userSids));
            resultList.addAll(othSetService.findOthSetByCreateDeptAndUser(deptSids, userSids));
            resultList.addAll(onpgService.findOnpgByCreateDeptAndUser(deptSids, userSids));
            return resultList;
        }

        switch (type) {
        case REQUIRE:
            resultList = requireService.findRequireByCreateDeptAndUser(deptSids, userSids);
            break;
        case PTCHECK:
            resultList = ptService.findPtByCreateDeptAndUser(deptSids, userSids);
            break;
        case WORKTESTSIGNINFO:
            resultList = sendTestService.findSendTestByCreateDeptAndUser(deptSids, userSids);
            break;
        case OTHSET:
            resultList = othSetService.findOthSetByCreateDeptAndUser(deptSids, userSids);
            break;
        case WORKONPG:
            resultList = onpgService.findOnpgByCreateDeptAndUser(deptSids, userSids);
            break;
        default:
            break;
        }

        return resultList;
    }

    /**
     * 建立單位轉發查詢
     */
    public List<TransRequireVO> searchByNotifiedDepts(RequireTransProgramType type, List<Integer> deptSids) {
        String regexpDepts = deptSids.stream().map(each -> String.format("\"%s\"", each)).collect(Collectors.joining("|"));

        List<TransRequireVO> resultList = Lists.newArrayList();
        // 查詢全部
        if (type == null) {
            resultList.addAll(ptService.findPtByNotifiedDept(regexpDepts));
            resultList.addAll(sendTestService.findSendTestByNotifiedDept(regexpDepts));
            resultList.addAll(othSetService.findOthSetByNotifiedDept(regexpDepts));
            resultList.addAll(onpgService.findOnpgByNotifiedDept(regexpDepts));
            return resultList;
        }
        switch (type) {
        case PTCHECK:
            resultList = ptService.findPtByNotifiedDept(regexpDepts);
            break;
        case WORKTESTSIGNINFO:
            resultList = sendTestService.findSendTestByNotifiedDept(regexpDepts);
            break;
        case OTHSET:
            resultList = othSetService.findOthSetByNotifiedDept(regexpDepts);
            break;
        case WORKONPG:
            resultList = onpgService.findOnpgByNotifiedDept(regexpDepts);
            break;
        default:
            resultList = Lists.newArrayList();
            break;
        }

        return resultList;
    }

}
