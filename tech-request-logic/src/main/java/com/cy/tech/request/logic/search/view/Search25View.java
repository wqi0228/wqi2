/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import com.cy.work.common.enums.UrgencyType;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 執行狀況查詢 view (search25.xhtml)
 *
 * @author jason_h
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Search25View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6430650658795522934L;
    /** vo sid */
    private String sid;
    /** 需求單號 */
    private String requireNo;
    /** 需求單主題 */
    private String requireTheme;
    /** 緊急度 */
    private UrgencyType urgency;
    /** 期望完成日 */
    private Date hopeDate;
    /** 立單日期 */
    private Date createdDate;
    /** 需求類別 */
    private String bigName;
    /** 中類 */
    private String middleName;
    /** 小類 */
    private String smallName;
    /** 需求單位 */
    private Integer createDep;
    /** 需求人員 */
    private Integer createdUser;
    /** 週三維護 */
    private String wednesdayMaintain;
    /** 本地端連結網址 */
    private String localUrlLink;
}
