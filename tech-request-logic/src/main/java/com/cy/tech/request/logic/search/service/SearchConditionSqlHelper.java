/**
 * 
 */
package com.cy.tech.request.logic.search.service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.cy.tech.request.logic.search.enums.ConditionInChargeMode;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

/**
 * 兜組特定 SQL
 * 
 * @author allen1214_wu
 */
@Component
public class SearchConditionSqlHelper {

    /**
     * 共通的需求單主檔 select column
     * 
     * @return
     */
    public String prepareCommonSelectColumnByRequire() {
        return "GROUP_CONCAT(checkitem.check_item_type SEPARATOR ',') AS checkItemNames, "
                + "tr.in_charge_dep                                   AS inChargeDepSid, "
                + "tr.in_charge_usr                                   AS inChargeUsrSid,  "
                + "readRecord.wait_read                               AS waitRead,  "
                + "readRecord.read_dt                                 AS readDate  ";

    }

    public String prepareCommonSelectColumnByRequireType2() {

        // 無指定負責人時，才需要撈分派單位
        String sql = " ";
        sql += "(SELECT GROUP_CONCAT (DISTINCT checkitem.check_item_type SEPARATOR ',') ";
        sql += "  FROM tr_require_check_item checkitem ";
        sql += " WHERE checkitem.require_sid = tr.require_sid ";
        sql += "       AND checkitem.status = 0 )                                    AS checkItemNames,";
        sql += "tr.in_charge_dep                                                     AS inChargeDepSid, ";
        sql += "tr.in_charge_usr                                                     AS inChargeUsrSid, ";
        sql += "readRecord.wait_read                                                 AS waitRead,  ";
        sql += "readRecord.read_dt                                                   AS readDate  ";

        return sql;
    }

    public String prepareReadRecordSelectColumn() {
        String sql = " ";
        sql += "readRecord.wait_read                                                 AS waitRead,  ";
        sql += "readRecord.read_dt                                                   AS readDate  ";
        return sql;
    }

    /**
     * 過濾檢查項目 (系統別)
     * 
     * @return
     */
    public String prepareCommonJoin(Integer userSid) {

        // ====================================
        // tr_require_check_item
        // ====================================
        String sql = "";
        sql += " LEFT JOIN tr_require_check_item checkitem ";
        sql += "     ON checkitem.require_sid = tr.require_sid  ";
        sql += "    AND checkitem.status = 0  ";

        // 查詢條件由後續 filter 過濾
        // ====================================
        // 閱讀記錄
        // ====================================
        sql += " LEFT JOIN tr_require_read_record readRecord ";
        sql += "       ON readRecord.require_sid = tr.require_sid  ";
        sql += "      AND readRecord.user_sid = " + userSid + "  ";

        return sql;
    }

    public String prepareSubFormCommonJoin(Integer userSid, FormType formType, String mainTableAliases) {

        // ====================================
        // tr_require_check_item
        // ====================================
        String sql = " ";
        sql += "   LEFT JOIN tr_require_check_item checkitem ";
        sql += "     ON checkitem.require_sid = tr.require_sid  ";
        sql += "    AND checkitem.status = 0  ";

        // 查詢條件由後續 filter 過濾
        // ====================================
        // 閱讀記錄
        // ====================================
        sql += "LEFT JOIN " + formType.getReadRecordTableName() + " readRecord ";
        sql += "       ON readRecord." + formType.getReadRecordFormSidColumnName() + " = " + mainTableAliases + "." + formType.getReadRecordFormSidColumnName() + " ";
        sql += "      AND readRecord.user_sid = " + userSid + "  ";

        return sql;
    }

    public String prepareSubFormReadRecordJoin(Integer userSid, FormType formType, String mainTableAliases) {
        String sql = " ";
        sql += "LEFT JOIN " + formType.getReadRecordTableName() + " readRecord ";
        sql += "       ON readRecord." + formType.getReadRecordFormSidColumnName() + " = " + mainTableAliases + "." + formType.getReadRecordFormSidColumnName() + " ";
        sql += "      AND readRecord.user_sid = " + userSid + "  ";

        return sql;
    }

    /**
     * JOIN tr_require_read_record
     * 
     * @param userSid
     * @return
     */
    public String prepareJoinRequireReadRecord(Integer userSid) {
        String sql = "";
        sql += "LEFT JOIN tr_require_read_record reqReadRecord ";
        sql += "       ON reqReadRecord.require_sid = tr.require_sid  ";
        sql += "      AND reqReadRecord.user_sid = " + userSid + "  ";
        return sql;
    }

    /**
     * 準備過濾『起迄日』的SQL
     * 
     * @param requireNo
     * @param dateColumn
     * @param startDate
     * @param endDate
     * @return
     */
    public String prepareWhereConditionForDateRange(
            String requireNo, String dateColumn, Date startDate, Date endDate) {

        // ====================================
        // 查詢需求單號不為空時，選擇查詢條件無效 (等同全部)
        // ====================================
        if (WkStringUtils.notEmpty(requireNo)) {
            return "";
        }

        // ====================================
        // 起迄日
        // ====================================
        String sql = "";

        // 沒有指定前綴的, 加上 tr
        if (dateColumn.indexOf(".") < 0) {
            dateColumn = "tr." + dateColumn;
        }

        if (startDate != null) {
            sql += " AND " + dateColumn + " >= '" + WkDateUtils.transStrByStartDate(startDate) + "' ";
        }

        if (endDate != null) {
            sql += " AND " + dateColumn + " <= '" + WkDateUtils.transStrByEndDate(endDate) + "' ";
        }

        return sql;
    }

    /**
     * 兜組主責單位相關查詢條件
     * 
     * @param targetRequireNo          查詢條件有限定需求單號 有值時固定回傳空字串
     * @param selectedInChargeDepsMode 查詢模式
     * @param inChargeDepSids          主責單位
     * @param inChargeUserSids         主責單位負責人
     * @return
     */
    public String prepareWhereConditionForInCharge(
            String targetRequireNo,
            ConditionInChargeMode conditionInChargeMode,
            List<Integer> inChargeDepSids,
            List<Integer> inChargeUserSids) {

        // 限定需求單號時，此查詢條件不生效
        if (WkStringUtils.notEmpty(targetRequireNo)) {
            return "";
        }

        if (conditionInChargeMode == null) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.ERROR, "主責單位查詢模式傳入 null");
            return "";
        }

        // NO_FILTER("未過濾"),
        // IN_CHARGE_DEP("已設定主責單位"),
        // IN_CHARGE_USR("已設定負責人"),
        // WAIT_SETTING("未設定"),
        // EXCEPTION_INACTIVE("已停用異常檢查"),

        // 主責單位
        switch (conditionInChargeMode) {
        case NO_FILTER:
            // 全部-不過濾
            break;

        case IN_CHARGE_DEP: // 已設定主責單位
            if (WkStringUtils.notEmpty(inChargeDepSids)) {
                String depSidsStr = String.join(
                        ",",
                        inChargeDepSids.stream()
                                .map(sid -> sid + "")
                                .collect(Collectors.toList()));
                return " AND tr.in_charge_dep IN (" + depSidsStr + ") ";

            } else {
                // 選擇為空時，查詢所有有設定的單據
                return " AND tr.in_charge_dep is not NULL ";
            }

        case IN_CHARGE_USR: // 已設定負責人
            if (WkStringUtils.notEmpty(inChargeUserSids)) {
                String userSidsStr = String.join(
                        ",",
                        inChargeUserSids.stream()
                                .map(sid -> sid + "")
                                .collect(Collectors.toList()));
                return " AND tr.in_charge_usr IN (" + userSidsStr + ") ";
            } else {
                // 選擇為空時，查詢所有有設定的單據
                return " AND tr.in_charge_usr is not NULL ";
            }

        case WAIT_SETTING: // 未設定主責單位
            return " AND tr.in_charge_dep is NULL ";

        case EXCEPTION_INACTIVE: // 已停用異常檢查
            // 異常檢查, 取出單位或負責人不回空，待回傳資料後，逐筆過濾
            return " AND (tr.in_charge_dep IS NOT NULL OR  tr.in_charge_usr IS NOT NULL) ";

        default:
            WkCommonUtils.logWithStackTrace(InfomationLevel.ERROR, "未實做的主責單位查詢模式");
            break;
        }

        return "";
    }

    /**
     * 準備過濾『待閱原因』的SQL
     * 
     * @param formNo                        單號
     * @param allWaitReadReasonTypeNames    所有的待閱原因類型名稱
     * @param selectWaitReadReasonTypeNames 選擇的待閱原因類型名稱
     * @param noWaitReadReasonTypeName      『無待閱原因』選項的名稱
     * @param conditionColumnName           過濾的欄位名稱
     * @return
     */
    public String prepareWhereConditionForWaitReadReason(
            String formNo,
            List<String> allWaitReadReasonTypeNames,
            List<String> selectWaitReadReasonTypeNames,
            String noWaitReadReasonTypeName,
            String conditionColumnName) {

        // ====================================
        // 查詢需求單號不為空時，選擇查詢條件無效 (等同全部)
        // ====================================
        if (WkStringUtils.notEmpty(formNo)) {
            return "";
        }

        // ====================================
        // 未勾選任何一個項目時，不過濾
        // ====================================
        if (WkStringUtils.notEmpty(selectWaitReadReasonTypeNames)) {
            return "";
        }
        // ====================================
        // 比對是否為『全部』
        // ====================================
        // 為全部時，略過此查詢條件
        if (WkCommonUtils.compare(selectWaitReadReasonTypeNames, allWaitReadReasonTypeNames)) {
            return "";
        }

        // ====================================
        // 兜組查詢條件
        // ====================================
        // 無待閱原因時, DB 欄位為空
        if (selectWaitReadReasonTypeNames.contains(noWaitReadReasonTypeName)) {
            selectWaitReadReasonTypeNames.set(
                    selectWaitReadReasonTypeNames.indexOf(noWaitReadReasonTypeName), "");
        }

        String sql = "";

        if (selectWaitReadReasonTypeNames.size() == 1) {
            sql = " " + conditionColumnName + " = '" + selectWaitReadReasonTypeNames.get(0) + "' ";
        }
        // 有複數不可用狀態時
        if (selectWaitReadReasonTypeNames.size() > 1) {
            sql = " " + conditionColumnName + " IN('" + String.join("','", selectWaitReadReasonTypeNames) + "') ";
        }

        if (selectWaitReadReasonTypeNames.contains("")) {
            sql = " (" + sql + " OR " + conditionColumnName + " IS NULL) ";
        }

        return " AND " + sql;
    }

    public String prepareWhereConditionForRequireStatus(
            String requireNo,
            List<RequireStatusType> queryConditionStatusTypes) {

        // builder.append(" AND tr.require_status IN (:requireStatus)");

        // ====================================
        // 有輸入單號時，不過濾
        // ====================================
        if (WkStringUtils.notEmpty(requireNo)) {
            return "";
        }

        // ====================================
        // 取得全部的可選擇項目
        // ====================================
        List<RequireStatusType> allStatusTypes = Lists.newArrayList(RequireStatusType.values()).stream()
                .filter(each -> each.isReportQueryCondition())
                .collect(Collectors.toList());

        // ====================================
        // 全部不選擇時，強制查不到
        // ====================================
        if (WkStringUtils.isEmpty(queryConditionStatusTypes)) {
            return "AND tr.require_status = 'XXFILTER_EMPTYXX' ";
        }

        // ====================================
        // 選擇全部時
        // ====================================
        if (WkCommonUtils.compare(allStatusTypes, queryConditionStatusTypes)) {
            // 過濾固定顯示項目
            return "AND tr.require_status NOT IN (" +
                    Lists.newArrayList(RequireStatusType.values()).stream()
                            .filter(each -> !each.isReportQueryCondition())
                            .map(RequireStatusType::name)
                            .collect(Collectors.joining("','", "'", "'"))
                    + ")";
        }

        // ====================================
        // 兜組條件
        // ====================================
        return "AND tr.require_status IN (" +
                queryConditionStatusTypes.stream()
                        .map(RequireStatusType::name)
                        .collect(Collectors.joining("','", "'", "'"))
                + ")";
    }

    /**
     * 準備過濾『製作狀態』的SQL
     * 
     * @param selectRequireStatusType
     * @return
     */
    public String prepareWhereConditionForRequireStatus(String requireNo, RequireStatusType selectRequireStatusType) {

        // ====================================
        // 指定狀態
        // ====================================
        // 查詢需求單號不為空時，選擇查詢條件無效 (等同全部)
        if (WkStringUtils.isEmpty(requireNo)) {
            if (selectRequireStatusType != null) {
                return " AND tr.require_status = '" + selectRequireStatusType.name() + "' ";
            }
        }

        // ====================================
        // 條件為『全部』, 需排除不可用狀態
        // ====================================
        // 取得查詢報表的固定排除狀態 (ex:草稿)
        List<String> removeStats = Lists.newArrayList(RequireStatusType.values()).stream()
                .filter(each -> !each.isReportQueryCondition())
                .map(RequireStatusType::name)
                .collect(Collectors.toList());

        // 僅有一種不可用狀態時
        if (removeStats.size() == 1) {
            return " AND tr.require_status != '" + removeStats.get(0) + "' ";
        }
        // 有複數不可用狀態時
        if (removeStats.size() > 1) {
            return " AND tr.require_status NOT IN ('" + String.join("','", removeStats) + "' ";
        }

        // 沒有固定排除狀態時(應該不可能發生)
        return "";
    }

}
