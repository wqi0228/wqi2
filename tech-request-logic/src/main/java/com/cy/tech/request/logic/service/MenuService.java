package com.cy.tech.request.logic.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.enumerate.CustCntType;
import com.cy.tech.request.logic.enumerate.MenuPermissionRule;
import com.cy.tech.request.logic.enumerate.ReportCntQueryType;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.service.helper.MenuHelper;
import com.cy.tech.request.logic.service.setting.SettingCheckConfirmRightService;
import com.cy.tech.request.logic.vo.FunctionNodeTo;
import com.cy.tech.request.repository.menu.TrMenuGroupBaseRepository;
import com.cy.tech.request.repository.menu.TrMenuItemRepository;
import com.cy.tech.request.vo.constants.ReqPermission;
import com.cy.tech.request.vo.enums.InboxType;
import com.cy.tech.request.vo.enums.MenuBaseType;
import com.cy.tech.request.vo.enums.ReqConfirmDepProgStatus;
import com.cy.tech.request.vo.menu.TrMenuGroupBase;
import com.cy.tech.request.vo.menu.TrMenuItem;
import com.cy.tech.request.vo.menu.TrMenuItemGroup;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.logic.WkUserAndOrgLogic;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * 程式清單
 *
 * @author shaun
 */
@Slf4j
@Component
public class MenuService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8330806017033995282L;
    @Autowired
    transient private TrMenuGroupBaseRepository groupBaseDao;
    @Autowired
    transient private MenuHelper menuHelper;
    @Autowired
    transient private TrMenuItemRepository trMenuItemRepository;

    @Autowired
    transient private WorkBackendParamHelper workBackendParamHelper;
    @Autowired
    transient private SettingCheckConfirmRightService settingCheckConfirmRightService;
    @Autowired
    private transient RequireService requireService;
    @Autowired
    private transient ReqWorkCountService reqWorkCountService;

    @Autowired
    private transient WkUserAndOrgLogic wkUserAndOrgLogic;

    @PersistenceContext
    transient private EntityManager em;

    /**
     * 建立前端功能清單
     *
     * @param type
     * @param userRoleNames
     * @param userRoles
     * @param user
     * @return
     */
    public FunctionNodeTo createFunctionMenu(
            MenuBaseType type,
            List<String> userRoleNames,
            List<Long> userRoles,
            User user) {

        FunctionNodeTo root = new FunctionNodeTo();
        TrMenuGroupBase baseGroup = this.findMenuBaseGroup(type);
        if (baseGroup == null) {
            log.error("群組菜單建立失敗！！型態：" + type);
            return root;
        }

        List<TrMenuItemGroup> menuItemGroups = baseGroup.getMenuItemGroup();
        if (WkStringUtils.isEmpty(menuItemGroups)) {
            return root;
        }

        for (TrMenuItemGroup groupItem : baseGroup.getMenuItemGroup()) {
            if (groupItem.getItems().isEmpty()) {
                continue;
            }
            FunctionNodeTo groupNode = this.createGroupNode(groupItem);
            for (TrMenuItem item : groupItem.getItems()) {

                boolean hasPermission = this.hasPermission(
                        item.getPermissionRole(),
                        userRoleNames,
                        user.getSid());

                if (hasPermission) {
                    FunctionNodeTo itemNode = this.createItemNode(item, userRoleNames, userRoles, user);
                    groupNode.getSubs().add(itemNode);
                }
            }
            if (!groupNode.getSubs().isEmpty()) {
                root.getSubs().add(groupNode);
            }
        }
        return root;
    }

    @Transactional(readOnly = true)
    public TrMenuGroupBase findMenuBaseGroup(MenuBaseType type) {
        return groupBaseDao.findByType(type);
    }

    /**
     * 建立功能類別節點
     *
     * @param itemGroup
     * @return
     */
    private FunctionNodeTo createGroupNode(TrMenuItemGroup itemGroup) {
        FunctionNodeTo node = new FunctionNodeTo();
        node.setTitle(itemGroup.getName());
        return node;
    }

    /**
     * 建立功能節點
     *
     * @param item
     * @return
     */
    private FunctionNodeTo createItemNode(
            TrMenuItem item,
            List<String> userRoleNames,
            List<Long> userRoles,
            User user) {

        FunctionNodeTo node = new FunctionNodeTo();
        node.setId(item.getComponentID());
        node.setUrl(item.getUrl());
        node.setIcon(item.getIcon());
        try {
            // 計算筆數
            node.setCnt(
                    this.findTitleCount(
                            item.getCountSql(),
                            userRoleNames,
                            userRoles,
                            user));
            // 設定顏色
            node.setColor(this.findTitleColor(node.getCnt()));
        } catch (Exception e) {
            node.setCnt("0");
            node.setColor("gray");
            log.error("菜單數量查詢失敗！！\n"
                    + "菜單名稱：" + item.getTitle() + "\n"
                    + "菜單位址：" + item.getUrl() + "\n"
                    + "查詢語法：" + item.getCountSql() + "\n"
                    + "登錄成員：" + user.getId() + "\n"
                    + "成員權限：" + userRoleNames + "\n"
                    + "成員角色：" + userRoles + "\n"
                    + "error msg:" + e.getMessage(), e);
        }
        node.setTitle(this.findTittle(node.getCnt(), item.getTitle()));
        this.checkHasReportType(item, node);
        return node;
    }

    public String findTitleCount(String countSql,
            List<String> userRoleNames,
            List<Long> userRoles,
            User user) {

        // 處理原本有些角色判斷使用 security 時. 角色名稱後方會被加上 『:*』
        if (WkStringUtils.notEmpty(userRoleNames)) {
            for (int i = 0; i < userRoleNames.size(); i++) {
                String userRoleName = userRoleNames.get(i);
                if ((userRoleName + "").endsWith(":*")) {
                    userRoleNames.set(i, userRoleName.substring(0, userRoleName.length() - 2));
                }
            }
        }

        if (WkStringUtils.isEmpty(countSql)) {
            return "0";
        }
        countSql = WkStringUtils.safeTrim(countSql);

        // 客製化查詢項目
        if (countSql.startsWith("CUST_QUERY")) {
            return this.processCustQuery(countSql, user);
        }

        // 收件夾
        InboxType inboxType = menuHelper.findInboxType(countSql);
        if (inboxType != null) {
            if (inboxType.isForwardMember()) {
                return reqWorkCountService.countUnreadInboxForForwardMember(user.getSid(), inboxType) + "";
            } else {
                Set<Integer> receiveDepSids = wkUserAndOrgLogic.prepareCanViewDepSidBaseOnOrgLevelWithParallel(
                        user.getSid(),
                        OrgLevel.MINISTERIAL,
                        false);

                return reqWorkCountService.countUnreadInboxForForwardDep(user.getSid(), receiveDepSids) + "";
            }
        }

        // 一般報表
        ReportCntQueryType rpcType = menuHelper.findReportCntQueryType(countSql);

        if (rpcType != null && ReportCntQueryType.WORK_ON_PG.equals(rpcType)) {
            return this.reqWorkCountService.countHasOn(user.getSid(), userRoleNames.contains(ReqPermission.OP_MGR)) + "";
        }

        Map<String, Object> parameters = Maps.newHashMap();
        menuHelper.checkParamUser(countSql, user, parameters);
        menuHelper.checkParamRegexpDeps(countSql, user, parameters);
        menuHelper.checkParamTrace(countSql, user, parameters);
        this.insertRpcParam(rpcType, countSql, parameters);

        Query query = em.createNativeQuery(countSql);

        menuHelper.setQueryParameter(query, parameters);

        try {

            String result = String.valueOf(query.getSingleResult());
            if (!WkStringUtils.isNumber(result)) {
                log.error("查詢失敗! 回傳值非數字！[{}]", result);
                log.error(""
                        + "rpcType :" + rpcType
                        + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(countSql.toString()) + "\r\n】"
                        + "\r\nPARAMs:【\r\n" + com.cy.work.common.utils.WkJsonUtils.getInstance().toPettyJson(parameters) + "\r\n】"
                        + "\r\n");

                return "0";
            }

            return result;
        } catch (Throwable e) {
            log.error("查詢失敗! "
                    + "rpcType :" + rpcType
                    + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(countSql.toString()) + "\r\n】"
                    + "\r\nPARAMs:【\r\n" + com.cy.work.common.utils.WkJsonUtils.getInstance().toPettyJson(parameters) + "\r\n】"
                    + "\r\n");
            throw e;
        }

    }

    /**
     * @param typeStr
     * @param user
     * @return
     */
    private String processCustQuery(String typeStr, User user) {

        // ====================================
        // 解析傳入類別
        // ====================================
        if (WkStringUtils.isEmpty(typeStr) || user == null) {
            return "0";
        }

        CustCntType custCntType = null;
        try {
            custCntType = CustCntType.valueOf(typeStr);
        } catch (Exception e) {
        }
        if (custCntType == null) {
            log.warn("tr_fun_item.count_sql, CustCntType 設定錯誤, 找不到 [" + typeStr + "]");
            return "0";
        }

        switch (custCntType) {
        case CUST_QUERY_WAIT_COMFIRM:
            // ====================================
            // 報表：未領單據查詢
            // ====================================
            // 改為同『首頁待辦-部門未領單』
            return this.reqWorkCountService.countWaitReceiveByDep(user.getSid(), ReqConfirmDepProgStatus.WAIT_CONFIRM) + "";

        case CUST_QUERY_WAIT_CHECK:
            // ====================================
            // 報表：檢查確認單據查詢
            // ====================================
            return this.requireService.countWaitCheck(user.getSid()) + "";

        default:
            break;
        }

        return "0";
    }

    private void insertRpcParam(ReportCntQueryType rpcType, String cntSql, Map<String, Object> parameters) {
        if (rpcType != null) {
            menuHelper.checkParamStartDate(cntSql, rpcType.getStartDateRange(), parameters);
            menuHelper.checkParamEndDate(cntSql, rpcType.getEndDateRange(), parameters);
        }
    }

    public String findTitleColor(String cntStr) {
        Integer cnt = Integer.valueOf(cntStr);
        return cnt > 50 ? "red" : cnt > 25 ? "purple" : cnt > 10 ? "blue" : cnt > 0 ? "green" : "black";
    }

    public String findTittle(String cnt, String title) {
        if (cnt.equals("0")) {
            return title;
        }
        return title + " ( " + cnt + " ) ";
    }

    /**
     * 比對 url 是否已在報表清單內，避免pf menuitem 元件 outcome 錯誤
     *
     * @param item
     * @param node
     */
    private void checkHasReportType(TrMenuItem item, FunctionNodeTo node) {
        // 基礎類型為報表才需進行檢測
        if (item.getMenuItemGroup().getMenuGroupBase().getType().equals(MenuBaseType.HOME)
                || item.getMenuItemGroup().getMenuGroupBase().getType().equals(MenuBaseType.REPORT)) {
            for (ReportType type : ReportType.values()) {
                if (node.getUrl().contains(type.getViewId())) {
                    return;
                }
            }
            log.info("{} 尚未加入至ReportType", item.getTitle());
            node.setUrl(URLService.URL_ADD_NEW_FROM);
            node.setColor("gray");
            node.setTitle(node.getTitle() + "(failed load)");
        }
    }

    /**
     * 是否擁有功能清單權限 <br/>
     *
     * @param itemUrl
     * @param permission
     * @return
     */
    /**
     * @param itemPermissionRole 項目可使用的角色
     * @param userRoles          登入者所有擁有的角色
     * @param userSid
     * @return
     */
    public Boolean hasPermission(String itemPermissionRole, List<String> userRoles, Integer userSid) {
        // ====================================
        // 此項目未設定，代表不檢核
        // ====================================
        if (WkStringUtils.isEmpty(itemPermissionRole)) {
            return Boolean.TRUE;
        }

        // ====================================
        // 處理傳入的使用者角色
        // ====================================
        if (WkStringUtils.isEmpty(userRoles)) {
            return Boolean.FALSE;
        }

        // 處理原本有些角色判斷使用 security 時. 角色名稱後方會被加上 『:*』
        for (int i = 0; i < userRoles.size(); i++) {
            String userRoleName = userRoles.get(i);
            if ((userRoleName + "").endsWith(":*")) {
                userRoles.set(i, userRoleName.substring(0, userRoleName.length() - 2));
            }
        }

        // ====================================
        // 逐筆判斷是否符合項目的可使用權限
        // ====================================
        for (String itemPermissionStr : itemPermissionRole.split(",")) {

            // ====================================
            // 依據規則判斷
            // ====================================
            // REQ-1483 【功能選單】新增權限設定方式
            if (WkStringUtils.safeTrim(itemPermissionStr).toUpperCase().startsWith("RULE:")) {
                return this.checkPermissionByRule(itemPermissionStr, userSid);
            }

            // ====================================
            // 依據使用者角色判斷
            // ====================================
            // 處理原本有些角色判斷使用 security 時. 角色名稱後方會被加上 『:*』
            if ((itemPermissionStr + "").endsWith(":*")) {
                itemPermissionStr = itemPermissionStr.substring(0, itemPermissionStr.length() - 2);
            }

            if (userRoles.contains(itemPermissionStr)) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    /**
     * 檢查權限, by 規則設定
     * 
     * @param ruleName
     * @param userSid
     * @return
     */
    private boolean checkPermissionByRule(String ruleName, Integer userSid) {

        String[] strs = WkStringUtils.safeTrim(ruleName).toUpperCase().split(":");
        if (strs.length < 2) {
            log.error("menu 功能權限判斷 - rule 值設定錯誤:[{}]", ruleName);
            return false;
        }

        // 解析選單權限規則項目
        MenuPermissionRule menuPermissionRule = MenuPermissionRule.safeValueOf(strs[1]);

        switch (menuPermissionRule) {

        case GM_LV1: // 一般GM
            // GM後台參數:GM設定
            return workBackendParamHelper.isGM(userSid);

        case GM_LV2: // GM 管理員
            // GM後台參數:管理者設定
            return workBackendParamHelper.isGMAdmin(userSid);

        case INSPECTOR: // 檢查員
            // GM後台參數:檢查人員名單設定 (有任一系統別權限即可)
            return this.settingCheckConfirmRightService.hasCanCheckUserRight(userSid);

        case REQ_MANAGER: // 需求管理者
            return this.workBackendParamHelper.isRequireAdmin(userSid);

        default:
            break;
        }

        log.error("menu 功能權限判斷 - rule 未實做：[{}]", ruleName);

        return false;
    }

    public TrMenuItem findyByComponentId(String componentId) {
        return trMenuItemRepository.findyByComponentId(componentId);
    }
}
