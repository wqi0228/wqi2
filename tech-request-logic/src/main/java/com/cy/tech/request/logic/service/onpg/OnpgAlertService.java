/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.onpg;

import com.cy.work.common.enums.ReadRecordType;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.CommonService;
import com.cy.tech.request.repository.onpg.WorkOnpgAlertRepo;
import com.cy.tech.request.vo.onpg.WorkOnpgAlert;
import com.cy.tech.request.vo.onpg.WorkOnpgCheckRecord;
import com.cy.tech.request.vo.onpg.WorkOnpgCheckRecordReply;
import com.cy.work.common.enums.WorkSourceType;
import com.google.common.collect.Sets;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.Executors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 最新消息
 *
 * @author shaun
 */
@Component
public class OnpgAlertService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 519668491175046213L;
    @Autowired
    private CommonService commonService;
    @Autowired
    private WorkOnpgAlertRepo alertDao;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void carateReplyAlert(WorkOnpgCheckRecordReply ckrReply, Integer executor, String cateSmallSid) {
        Executors.newCachedThreadPool().execute(() -> {
            Set<User> receiversUsers = this.findSenderBySourceTypeAndCheckRecord(WorkSourceType.TECH_REQUEST, ckrReply.getCheckRecord());
            //只取sid，以免not equals 
            User replyPerson = new User();
            replyPerson.setSid(ckrReply.getCheckRecord().getPerson().getSid());
            receiversUsers.add(replyPerson);
            receiversUsers.forEach(receiver -> {
                alertDao.save(this.createEmptyAlert(ckrReply, receiver));
            });
        });
    }

    private WorkOnpgAlert createEmptyAlert(WorkOnpgCheckRecordReply ckrReply, User receiver) {
        WorkOnpgAlert alert = new WorkOnpgAlert();
        alert.setOnpg(ckrReply.getOnpg());
        alert.setOnpgNo(ckrReply.getOnpg().getOnpgNo());
        alert.setSourceType(ckrReply.getOnpg().getSourceType());
        alert.setSourceSid(ckrReply.getOnpg().getSourceSid());
        alert.setSourceNo(ckrReply.getOnpg().getSourceNo());
        String replyStr = commonService.get(ckrReply.getCheckRecord().getReplyType());
        alert.setOnpgTheme(sdf.format(new Date()) + " " + ckrReply.getOnpg().getTheme() + " - 您有一封" + replyStr + "的資訊，請查看");
        alert.setSender(ckrReply.getPerson());
        alert.setSendDate(new Date());
        alert.setReceiver(receiver);
        alert.setCheckRecord(ckrReply.getCheckRecord());
        alert.setCheckRecordReply(ckrReply);
        alert.setReadStatus(ReadRecordType.UN_READ);
        return alert;
    }

    private Set<User> findSenderBySourceTypeAndCheckRecord(WorkSourceType sourceType, WorkOnpgCheckRecord ckr) {
        return Sets.newHashSet(alertDao.findSenderBySourceTypeAndCheckRecord(sourceType, ckr));
    }

    @Transactional(readOnly = true)
    public WorkOnpgAlert findBySid(String sid) {
        return alertDao.findOne(sid);
    }

    @Transactional(rollbackFor = Exception.class)
    public void save(WorkOnpgAlert alert) {
        alertDao.save(alert);
    }
}
