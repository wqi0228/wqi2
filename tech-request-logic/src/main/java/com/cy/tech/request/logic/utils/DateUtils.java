package com.cy.tech.request.logic.utils;

import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Component;

/**
 * 時間 工具
 *
 * @author aken
 */
@Component
public class DateUtils {

	public static DateTimeFormatter YYYY_MM_DD_HH24_MI = DateTimeFormat.forPattern("yyyy/MM/dd HH:mm");
	public static DateTimeFormatter YYYY_MM_DD = DateTimeFormat.forPattern("yyyy/MM/dd");
	public static DateTimeFormatter YYYY_MM_DD2 = DateTimeFormat.forPattern("yyyy-MM-dd");
	public static DateTimeFormatter YYYY_MM = DateTimeFormat.forPattern("yyyy/MM");

	/**
	 * 將日期轉換為文字格式(yyyy/MM/dd HH:mm)
	 *
	 * @param date
	 * @return
	 */
	public static String toDateTimesString(Date date) {
		if (date == null) {
			return "";
		}
		return YYYY_MM_DD_HH24_MI.print(date.getTime());
	}

	/**
	 * 轉換當天時分秒為00:00:00.0
	 * 
	 * @param date
	 * @return
	 */
	public static Date convertToOriginOfDay(Date date) {
		if (date == null) {
			return null;
		}
		DateTime dateTime = new DateTime(date);

		return dateTime.withTime(0, 0, 0, 0).toDate();
	}

	/**
	 * 轉換當天時分秒為23:59:59.999
	 * 
	 * @param date
	 * @return
	 */
	public static Date convertToEndOfDay(Date date) {
		if (date == null) {
			return null;
		}
		DateTime dateTime = new DateTime(date);

		return dateTime.withTime(23, 59, 59, 999).toDate();
	}

	/**
	 * 轉換為當月第一天，時分秒為00:00:00.0
	 * 
	 * @param date
	 * @return
	 */
	public static Date convertToOriginOfMonth(Date date) {
		if (date == null) {
			return null;
		}
		DateTime dateTime = new DateTime(date);
		dateTime = dateTime.dayOfMonth().withMinimumValue();
		dateTime = dateTime.withTime(0, 0, 0, 0);
		return dateTime.toDate();
	}

	/**
	 * 轉換為當月最後一天，時分秒為23:59:59.999
	 * 
	 * @param date
	 * @return
	 */
	public static Date convertToEndOfMonth(Date date) {
		if (date == null) {
			return null;
		}
		DateTime dateTime = new DateTime(date);
		dateTime = dateTime.dayOfMonth().withMaximumValue();
		dateTime = dateTime.withTime(23, 59, 59, 999);
		return dateTime.toDate();
	}

	/**
	 * 判斷是否為系統日
	 * 
	 * @param date
	 * @return
	 */
	public static boolean isSystemDate(Date date) {
		if (date != null) {
			return YYYY_MM_DD.print(date.getTime()).equals(YYYY_MM_DD.print(new Date().getTime()));
		}
		return false;
	}

	/**
	 * 是否為同一天
	 * 
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static boolean isSameDate(Date date1, Date date2) {
		if (date1 != null && date2 != null) {
			return YYYY_MM_DD.print(date1.getTime()).equals(YYYY_MM_DD.print(date2.getTime()));
		}
		return false;
	}
}
