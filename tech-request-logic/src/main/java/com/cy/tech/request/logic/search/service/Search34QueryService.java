/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.search.view.Search34View;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.send.test.SendTestShowService;
import com.cy.tech.request.vo.constants.ReqPermission;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.worktest.enums.WorkTestInfoStatus;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.common.cache.WkUserWithRolesCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

/**
 * @author marlow_chen
 */
@Service
public class Search34QueryService implements QueryService<Search34View>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6621159316917034680L;

    @Autowired
    private URLService urlService;

    @Autowired
    private SendTestShowService sendTestShowService;

    @Autowired
    private SearchService searchHelper;

    @PersistenceContext
    transient private EntityManager em;

    @SuppressWarnings("unchecked")
    @Override
    public List<Search34View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {

        // ====================================
        // 查詢
        // ====================================

        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        List<Object[]> result = query.getResultList();
        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if (WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================

        // 解析資料-開始
        usageRecord.parserDataStart();

        List<String> userRoleNames = WkUserWithRolesCache.getInstance().findRolesNameByUserSid(execUserSid);
        String reviewManagerRoleName = ReqPermission.ROLE_QA_REVIEW_MGR.replace(":*", "");
        System.out.println("===========" + reviewManagerRoleName);
        boolean isQAManager = userRoleNames.contains(reviewManagerRoleName);

        List<Search34View> viewResult = Lists.newArrayList();
        for (int i = 0; i < result.size(); i++) {
            Object[] record = (Object[]) result.get(i);

            int index = 0;

            String sid = (String) record[index++];
            Date testDate = (Date) record[index++];
            String qaSearchLink = (String) record[index++];
            Integer createDeptSid = (Integer) record[index++];
            Integer createdUser = (Integer) record[index++];
            String testTheme = (String) record[index++];
            String masterTesters = (String) record[index++];
            Date establishDate = (Date) record[index++];
            String testinfoStatus = (String) record[index++];
            String commitStatus = (String) record[index++];
            Date testinfoFinishDate = (Date) record[index++];
            Date expectOnlineDate = (Date) record[index++];
            String testinfoNo = (String) record[index++];
            Date createdDate = (Date) record[index++];
            Integer requireDep = (Integer) record[index++];
            Integer requireUser = (Integer) record[index++];
            String bigCategory = (String) record[index++];
            String middleCategory = (String) record[index++];
            String smallCategory = (String) record[index++];
            String requireTheme = (String) record[index++];
            String requireStatus = (String) record[index++];
            Date requireUpdateDate = (Date) record[index++];

            String requireNo = (String) record[index++];
            Date scheduleFinishDate = (Date) record[index++];
            String qaLinkName = (String) record[index++];
            String slaveTesters = (String) record[index++];

            String qaAuditStatus = (String) record[index++];
            String qaScheduleStatus = (String) record[index++];

            String waitRead = String.valueOf(record[index++]);
            Date readDate = (Date) record[index++];

            Search34View view = new Search34View();
            view.setSid(sid);
            view.setRequireNo(requireNo);
            view.setTestDate(new Date(testDate.getTime()));
            view.setQaSearchLink(qaSearchLink);
            view.setCreateDeptSid(createDeptSid);
            view.setCreatedUser(createdUser);
            view.setTestTheme(testTheme);

            String[] arrMasterTesters = null;
            String[] arrSlaveTesters = null;
            if (masterTesters != null) {
                arrMasterTesters = Arrays.stream(masterTesters.split(",")).map(String::trim).toArray(String[]::new);
            }
            if (slaveTesters != null) {
                arrSlaveTesters = Arrays.stream(slaveTesters.split(",")).map(String::trim).toArray(String[]::new);
            }
            view.setMasterTester(arrMasterTesters);
            view.setSlaveTester(arrSlaveTesters);
            view.setEstablishDate(establishDate);
            view.setTestinfoStatus(WorkTestStatus.valueOf(testinfoStatus).getValue());
            view.setCommitStatus(WorkTestInfoStatus.valueOf(commitStatus).getValue());
            view.setTestinfoFinishDate(testinfoFinishDate);
            view.setExpectOnlineDate(expectOnlineDate);
            view.setTestinfoNo(testinfoNo);
            view.setRequireNo(requireNo);
            view.setRequireSid(requireDep);
            view.setRequireCreateUser(requireUser);
            view.setBigCategory(bigCategory);
            view.setMiddleCategory(middleCategory);
            view.setSmallCategory(smallCategory);
            view.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
            view.setRequireStatus(RequireStatusType.safeValueOf(requireStatus));
            view.setCreatedDate(createdDate);
            view.setRequireUpdateDate(requireUpdateDate);
            view.setCreatedDate(createdDate);
            view.setScheduleFinishDate(scheduleFinishDate);

            view.setLocalUrlLink(urlService.createWorkTestInfoUrl(view.getTestinfoNo()));
            view.setLocalRequireUrlLink(urlService.createLocalUrlLinkParamForTab(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(execUserSid, view.getRequireNo(), 1),
                    URLService.URLServiceAttr.URL_ATTR_TAB_ST, view.getSid()) + "&showRequire=true");

            view.setQaLinkName(qaLinkName);

            view.setQaAuditStatus(WorkTestInfoStatus.safeValueOf(qaAuditStatus));
            view.setQaScheduleStatus(WorkTestInfoStatus.safeValueOf(qaScheduleStatus));

            view.setRenderedTestersLink(sendTestShowService.renderedTestersBtn(
                    isQAManager,
                    WorkTestStatus.safeValueOf(testinfoStatus),
                    view.getQaAuditStatus(),
                    view.getQaScheduleStatus()));

            // ====================================
            // 閱讀記錄
            // ====================================
            ReadRecordType readRecordType = ReadRecordType.UN_READ;
            // 待閱讀
            if (WkStringUtils.isEmpty(waitRead) || "Y".equals(waitRead)) {
                if (readDate == null) {
                    readRecordType = ReadRecordType.UN_READ;
                } else {
                    readRecordType = ReadRecordType.WAIT_READ;
                }
            }
            // 已閱讀
            else if ("N".equals(waitRead)) {
                readRecordType = ReadRecordType.HAS_READ;
            }
            view.setReadRecordType(readRecordType);

            viewResult.add(view);
        }

        // 解析資料-結束
        usageRecord.parserDataEnd();
        return viewResult;
    }
}
