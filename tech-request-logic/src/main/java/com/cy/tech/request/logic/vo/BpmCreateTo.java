/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.vo;

import com.cy.bpm.rest.vo.client.ICreate;
import com.cy.bpm.rest.vo.common.ProcessType;
import com.cy.commons.vo.User;
import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.worktest.WorkTestInfo;

import java.util.Map;
import lombok.Getter;

/**
 *
 * @author shaun
 */
public class BpmCreateTo implements ICreate {

    /**
     * 
     */
    private static final long serialVersionUID = -4407525149867102241L;

    public enum RequireFlowType {

        REQUIRE_UNIT("TechRequest_RequireManager", "技術需求單-需求單位簽核"),
        TECH_UNIT("TechRequest_TechManager", "技術需求單-技術單位簽核"),
        PROTOTYPE_CHECK("TechRequest_PrototypeCheck", "技術需求單-原型確認簽核"),
        TEST_UNIT("TechRequest_SendTest", "技術需求單-送測單位簽核"),;

        @Getter
        private final String definition;
        @Getter
        private final String displayDesc;

        RequireFlowType(String definition, String displayDesc) {
            this.definition = definition;
            this.displayDesc = displayDesc;
        }
    }

    @Getter
    private final ProcessType type;
    @Getter
    private final String definition;
    @Getter
    private final String display;
    @Getter
    private final String description;
    @Getter
    private final String docId;
    @Getter
    private final String executorId;
    @Getter
    private final String executorAgentId;
    @Getter
    private final String executorRoleId;
    @Getter
    transient private final Map<String, Object> parameter;

    /**
     * 需求單
     *
     * @param require
     * @param executor
     * @param exeRoleId
     * @param flowType
     * @param flowPara
     */
    public BpmCreateTo(
            String docNo,
            String userId,
            String exeRoleId,
            RequireFlowType flowType,
            Map<String, Object> flowPara
    ) {
        this.type = ProcessType.SIGNFLOW;
        this.docId = docNo;
        this.executorId = userId;
        this.executorAgentId = userId;
        this.executorRoleId = exeRoleId;
        this.definition = flowType.getDefinition();
        this.display = flowType.getDisplayDesc();
        this.description = flowType.getDisplayDesc();
        this.parameter = flowPara;
    }
    
    public BpmCreateTo(
            String docNo,
            String userId,
            String exeRoleId,
            String flowID,
            String flowDesc,
            Map<String, Object> flowPara
    ) {
        this.type = ProcessType.SIGNFLOW;
        this.docId = docNo;
        this.executorId = userId;
        this.executorAgentId = userId;
        this.executorRoleId = exeRoleId;
        this.definition = flowID;
        this.display = flowDesc;
        this.description = flowDesc;
        this.parameter = flowPara;
    }

    /**
     * 原型確認單
     *
     * @param testInfo
     * @param executor
     * @param exeRoleId
     * @param flowType
     * @param flowPara
     */
    public BpmCreateTo(PtCheck ptCheck, User executor, String exeRoleId, RequireFlowType flowType, Map<String, Object> flowPara) {
        this.definition = flowType.getDefinition();
        this.display = flowType.getDisplayDesc();
        this.description = flowType.getDisplayDesc();
        this.parameter = flowPara;
        this.docId = ptCheck.getPtNo();
        this.executorId = executor.getId();
        this.executorAgentId = executor.getId();
        this.executorRoleId = exeRoleId;
        this.type = ProcessType.SIGNFLOW;
    }

    /**
     * 送測單
     *
     * @param testInfo
     * @param executor
     * @param exeRoleId
     * @param flowType
     * @param flowPara
     */
    public BpmCreateTo(WorkTestInfo testInfo, User executor, String exeRoleId, RequireFlowType flowType, Map<String, Object> flowPara) {
        this.definition = flowType.getDefinition();
        this.display = flowType.getDisplayDesc();
        this.description = flowType.getDisplayDesc();
        this.parameter = flowPara;
        this.docId = testInfo.getTestinfoNo();
        this.executorId = executor.getId();
        this.executorAgentId = executor.getId();
        this.executorRoleId = exeRoleId;
        this.type = ProcessType.SIGNFLOW;
    }

}
