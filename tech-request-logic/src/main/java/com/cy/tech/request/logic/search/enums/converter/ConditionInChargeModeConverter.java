/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.enums.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.cy.tech.request.logic.search.enums.ConditionInChargeMode;

/**
 *
 * @author allen
 */
@Converter
public class ConditionInChargeModeConverter implements AttributeConverter<ConditionInChargeMode, String> {

    @Override
    public ConditionInChargeMode convertToEntityAttribute(String str) {
    	
        if (str == null) {
            return null;
        }
        
        return ConditionInChargeMode.safeValueOf(str);
    }

    @Override
    public String convertToDatabaseColumn(ConditionInChargeMode attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute.name();
    }

}
