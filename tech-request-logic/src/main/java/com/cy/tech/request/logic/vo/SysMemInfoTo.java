/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.vo;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 系統記憶體記錄物件
 *
 * @author shaun
 */
@Getter
@AllArgsConstructor
public class SysMemInfoTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4466507236775236334L;
    private final String date;
    private final long memory;
}
