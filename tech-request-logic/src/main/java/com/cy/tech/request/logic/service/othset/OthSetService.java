/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.othset;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.config.exception.OthSetEditExceptions;
import com.cy.tech.request.logic.helper.systemnotify.SysNotifyHelper;
import com.cy.tech.request.logic.service.AssignNoticeService;
import com.cy.tech.request.logic.service.FormNumberService;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.RequireProcessCompleteRollbackService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.RequireShowService;
import com.cy.tech.request.logic.service.TrSubNoticeInfoService;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepService;
import com.cy.tech.request.logic.vo.OthSetTo;
import com.cy.tech.request.repository.TrNamedQueryRepository;
import com.cy.tech.request.repository.require.ReqModifyRepo;
import com.cy.tech.request.repository.require.othset.OthSetAlreadyReplyRepo;
import com.cy.tech.request.repository.require.othset.OthSetAttachmentRepo;
import com.cy.tech.request.repository.require.othset.OthSetHistoryRepo;
import com.cy.tech.request.repository.require.othset.OthSetRepo;
import com.cy.tech.request.repository.result.TransRequireVO;
import com.cy.tech.request.vo.enums.OthSetHistoryBehavior;
import com.cy.tech.request.vo.enums.OthSetStatus;
import com.cy.tech.request.vo.enums.RequireTransProgramType;
import com.cy.tech.request.vo.enums.SubNoticeType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.othset.OthSet;
import com.cy.tech.request.vo.require.othset.OthSetAlreadyReply;
import com.cy.tech.request.vo.require.othset.OthSetAttachment;
import com.cy.tech.request.vo.require.othset.OthSetHistory;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.AttachmentService;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shaun
 */
@Slf4j
@Component
public class OthSetService implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2429213791302162734L;
    private static OthSetService instance;

    public static OthSetService getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        OthSetService.instance = this;
    }

    @Autowired
    private RequireShowService reqShowService;
    @Autowired
    private RequireService reqService;
    @Autowired
    private OrganizationService orgService;

    @Autowired
    @Qualifier("othset_attach")
    private AttachmentService<OthSetAttachment, OthSet, String> attachService;
    @Autowired
    private OthSetShowService ossService;
    @Autowired
    private OthSetAlertService osaService;
    @Autowired
    private FormNumberService formNumberService;
    @Autowired
    private WkJsoupUtils jsoupUtils;
    @Autowired
    private OthSetRepo othsetDao;
    @Autowired
    private OthSetHistoryRepo osHistoryDao;
    @Autowired
    private OthSetAlreadyReplyRepo aReplyDao;
    @Autowired
    private OthSetAttachmentRepo attachDao;
    @Autowired
    private ReqModifyRepo reqModifyDao;
    @Autowired
    private TrSubNoticeInfoService subNoticeInfoService;
    @Autowired
    private transient TrNamedQueryRepository trNamedQueryRepository;
    @Autowired
    private OthSetHistoryService othSetHistoryService;
    @Autowired
    private AssignNoticeService assignNoticeService;
    @Autowired
    private RequireProcessCompleteRollbackService requireProcessCompleteRollbackService;
    @Autowired
    private RequireConfirmDepService requireConfirmDepService;
    @Autowired
    private SysNotifyHelper sysNotifyHelper;

    /**
     * 建立空主檔
     *
     * @param executor
     * @param require
     * @return
     */
    public OthSet createEmptyOthSet(User executor, Require require) {
        OthSet othset = new OthSet();
        othset.setRequire(require);
        othset.setRequireNo(require.getRequireNo());
        othset.setTheme(null);
        othset.setContent(null);
        othset.setContentCss(null);
        othset.setCreateCompany(orgService.findUserCompany(orgService.findBySid(executor.getPrimaryOrg().getSid())));
        othset.setCreateDep(orgService.findBySid(executor.getPrimaryOrg().getSid()));
        othset.setStatus(Activation.ACTIVE);
        othset.setCreatedUser(executor);
        othset.setCreatedDate(new Date());
        // 通知單位由下拉主題進行對應
        return othset;
    }

    @Transactional(rollbackFor = Exception.class)
    public OthSet saveByNewOthset(
            Require require,
            OthSet othset,
            User executor) throws UserMessageException, SystemDevelopException {

        Date sysDate = new Date();

        // ====================================
        // 權限檢核
        // ====================================
        Require nR = reqService.findByReqObj(require);
        if (!reqShowService.showOthSetBtn(nR, executor, reqShowService.isContainAssign(nR, executor))) {
            throw new UserMessageException("無新增其它設定資訊權限！！");
        }

        // ====================================
        // 新增 tr_os (其它設定資訊) 檔
        // ====================================
        othset.setContent(jsoupUtils.clearCssTag(othset.getContentCss()));
        othset.setNote(jsoupUtils.clearCssTag(othset.getNoteCss()));

        othset.setOsStatus(OthSetStatus.PROCESSING);
        othset.setOsNo(formNumberService.generateOsNum());// 產生送測單號
        // save
        othset = this.save(othset, executor, sysDate);
        // 連結附加檔案
        attachService.linkRelation(othset.getAttachments(), othset, executor);

        // ====================================
        // 建立通知訊息
        // ====================================
        osaService.createOthSetAlert(othset, executor);

        // ====================================
        // 建立通知單位 異動紀錄
        // ====================================
        subNoticeInfoService.createFirstSubChangeRecord(
                SubNoticeType.OS_INFO_DEP,
                require.getSid(),
                require.getRequireNo(),
                othset.getSid(),
                othset.getOsNo(),
                othset.getNoticeDeps(),
                executor.getSid());

        // ====================================
        // 非分派單位成員開單時, 進行自動分派 (如果需要的話)
        // ====================================
        this.assignNoticeService.processForAutoAddAssignDep(
                require.getSid(),
                require.getRequireNo(),
                othset.getCreateDep().getSid(),
                "新增其他設定資訊單",
                executor,
                sysDate);

        // ====================================
        // 若需求製作進度為已完成, 將進度倒回進行中
        // ====================================
        this.requireProcessCompleteRollbackService.executeProcess(
                require.getSid(),
                executor,
                sysDate,
                "再次新增其他設定資訊單");

        // ====================================
        // 再次開啟執行單位的【需求完成-確認流程】 (若需要開啟的話)
        // ====================================
        this.requireConfirmDepService.changeProgStatusCompleteByAddNewSubCase(
                require.getRequireNo(),
                require.getSid(),
                executor.getSid(),
                othset.getCreateDep().getSid(),
                "再次新增其他設定資訊單",
                sysDate);

        // ====================================
        // 更新主檔
        // ====================================
        require.setHasOthSet(Boolean.TRUE);
        reqModifyDao.updateByHasOthset(require, require.getHasOthSet());

        // ====================================
        // 處理系統通知 (新增其他資訊單)
        // ====================================
        try {
            this.sysNotifyHelper.processForAddOtherSet(
                    require.getSid(),
                    othset.getTheme(),
                    null, // 新增時無異動前單位
                    (othset.getNoticeDeps() != null) ? othset.getNoticeDeps().getValue() : Lists.newArrayList(),
                    executor.getSid());
        } catch (Exception e) {
            log.error("執行系統通知失敗!" + e.getMessage(), e);
        }

        return othset;
    }

    @Transactional(rollbackFor = Exception.class)
    public OthSet save(OthSet othset) {
        return othsetDao.save(othset);
    }

    public OthSet save(OthSet othset, User executor) {
        return this.save(othset, executor);
    }

    public OthSet save(OthSet othset, User executor, Date sysDate) {
        othset.setUpdatedDate(sysDate);
        othset.setUpdatedUser(executor);
        return othsetDao.save(othset);
    }

    private void addLog(OthSet editOthset) {
        log.info("其他設定資訊:單號-" + ((editOthset != null) ? editOthset.getOsNo() : "") + "");

        String depsName = "";
        if (editOthset.getNoticeDeps() == null || WkStringUtils.isEmpty(editOthset.getNoticeDeps().getValue())) {
            depsName = "為空";
        } else {
            depsName = WkOrgUtils.findNameBySidStrs(editOthset.getNoticeDeps().getValue(), ",");
        }
        log.info("『修改後通知部門』:" + depsName);
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveByEditOthset(
            Require require, 
            OthSet editOthset, 
            User executor, 
            OthSetTo tempTo)
            throws IllegalAccessException, OthSetEditExceptions {
        
        if (ossService.disableEdit(reqService.findByReqObj(require), this.findByOsNo(editOthset.getOsNo()), executor)) {
            throw new IllegalAccessException("無編輯其它設定資訊權限！！");
        }
        
        this.addLog(editOthset);
        OthSet backupOthset = this.findByOsNo(editOthset.getOsNo());
        JsonStringListTo noticeDeps = backupOthset.getNoticeDeps();
        this.checkOthSetByBeforeSave(tempTo, backupOthset);
        editOthset.setContent(jsoupUtils.clearCssTag(editOthset.getContentCss()));
        editOthset.setNote(jsoupUtils.clearCssTag(editOthset.getNoteCss()));
        attachService.findAttachsByLazy(editOthset).forEach(each -> each.setKeyChecked(Boolean.TRUE));
        attachService.linkRelation(editOthset.getAttachments(), editOthset, executor);
        this.modifyByEditOthSet(editOthset, executor);
        // 建立子程序 異動紀錄
        subNoticeInfoService.createSubChangeRecord(SubNoticeType.OS_INFO_DEP,
                require.getSid(),
                require.getRequireNo(), editOthset.getSid(), editOthset.getOsNo(), noticeDeps, editOthset.getNoticeDeps(), executor.getSid());
    }

    @Transactional(readOnly = true)
    public List<OthSet> findByRequire(Require require) {
        if (require == null || Strings.isNullOrEmpty(require.getSid())) {
            return Lists.newArrayList();
        }
        return othsetDao.findByRequireOrderByCreatedDateDesc(require);
    }

    @Transactional(readOnly = true)
    public OthSetStatus findOthsetStatus(OthSet othset) {
        return othsetDao.findOthsetStatus(othset);
    }

    @Transactional(readOnly = true)
    public List<String> findSidsByRequire(Require require) {
        return othsetDao.findSidsByRequire(require);
    }

    @Transactional(readOnly = true)
    public OthSet findByOsNo(String osNo) {
        return othsetDao.findByOsNo(osNo);
    }

    @Transactional(readOnly = true)
    public List<OthSet> initTabInfo(Require require) {
        if (require == null || Strings.isNullOrEmpty(require.getSid())) {
            return Lists.newArrayList();
        }
        List<OthSet> othsets = othsetDao.findByRequireOrderByCreatedDateDesc(require);
        List<OthSetHistory> historys = osHistoryDao.findByOsNoIn(othsets.stream().map(each -> each.getOsNo()).collect(Collectors.toList()));
        if (historys == null) {
            historys = Lists.newArrayList();
        }
        List<OthSetAlreadyReply> replys = Lists.newArrayList();
        List<OthSetAttachment> attachs = Lists.newArrayList();
        if (!historys.isEmpty()) {
            List<String> historySids = historys.stream().map(each -> each.getSid()).collect(Collectors.toList());
            replys = aReplyDao.findReplyByHistoryInOrderByUpdateDateDesc(historySids);
            attachs = attachDao.findAttachByHistoryInOrderByUpdateDateDesc(historySids);
        }
        Map<String, List<OthSetAlreadyReply>> replysMapKeyIsReplySid = replys.stream().collect(Collectors.groupingBy(each -> each.getReply().getSid()));
        historys.stream()
                .filter(each -> each.getReply() != null)
                .filter(each -> replysMapKeyIsReplySid.containsKey(each.getReply().getSid()))
                .forEach(each -> each.getReply().setAlreadyReplys(replysMapKeyIsReplySid.get(each.getReply().getSid())));
        Map<String, List<OthSetAttachment>> attachMapKeyIsHistorySid = attachs.stream().collect(Collectors.groupingBy(each -> each.getHistory().getSid()));
        historys.stream()
                .filter(each -> attachMapKeyIsHistorySid.containsKey(each.getSid()))
                .forEach(each -> each.setAttachments(attachMapKeyIsHistorySid.get(each.getSid())));
        Map<String, List<OthSetHistory>> historysMapKeyIsOsNo = historys.stream().collect(Collectors.groupingBy(OthSetHistory::getOsNo));
        othsets.stream()
                .filter(op -> historysMapKeyIsOsNo.containsKey(op.getOsNo()))
                .forEach(op -> op.setHistorys(
                        historysMapKeyIsOsNo.get(op.getOsNo()).stream()
                                .filter(history -> !history.getBehavior().equals(OthSetHistoryBehavior.REPLY_AND_REPLY))
                                .collect(Collectors.toList())));
        return othsets;
    }

    /**
     * 轉換為自訂物件
     *
     * @param osNo
     * @return
     */
    public OthSetTo findToByOsNo(String osNo) {
        OthSet obj = this.findByOsNo(osNo);
        if (obj == null) {
            log.error("查無該OthSet資料，OthSet單號：" + osNo);
            return null;
        }
        OthSetTo to = new OthSetTo();
        to.setSid(obj.getSid());
        to.setOsNo(obj.getOsNo());
        to.setRequireNo(obj.getRequireNo());
        to.setNoticeDeps(obj.getNoticeDeps());
        to.setOsStatus(obj.getOsStatus());
        to.setTheme(obj.getTheme());
        to.setFinishDate(obj.getFinishDate());
        to.setCancelDate(obj.getCancelDate());
        to.setContent(obj.getContent());
        to.setContentCss(obj.getContentCss());
        to.setNote(obj.getNote());
        to.setNoteCss(obj.getNoteCss());
        to.setUpdatedDate(obj.getUpdatedDate());
        return to;
    }

    /**
     * 在存檔前進行檢核
     *
     * @param tempTo   Onpg副本
     * @param dbSource DB即時資料
     * @throws OthSetEditExceptions
     */
    private void checkOthSetByBeforeSave(OthSetTo tempTo, OthSet dbSource) throws OthSetEditExceptions {
        if (tempTo.getUpdatedDate().compareTo(dbSource.getUpdatedDate()) != 0) {
            StringBuilder errorMsg = new StringBuilder();
            if (!this.checkSameByNoticeDeps(tempTo.getNoticeDeps().getValue(), dbSource.getNoticeDeps().getValue())) {
                errorMsg.append("通知單位、");
            }
            if (!tempTo.getTheme().equals(dbSource.getTheme())) {
                errorMsg.append("主題、");
            }
            if (!tempTo.getContentCss().equals(dbSource.getContentCss())) {
                errorMsg.append("內容、");
            }
            if (!tempTo.getNoteCss().equals(dbSource.getNoteCss())) {
                errorMsg.append("備註、");
            }
            if (dbSource.getFinishDate() != null
                    && tempTo.getFinishDate().compareTo(dbSource.getFinishDate()) != 0) {
                errorMsg.append("完成日、");
            }
            if (dbSource.getCancelDate() != null
                    && tempTo.getCancelDate().compareTo(dbSource.getCancelDate()) != 0) {
                errorMsg.append("取消日、");
            }
            if (!tempTo.getOsStatus().equals(dbSource.getOsStatus())) {
                errorMsg.append("狀態、");
            }
            if (errorMsg.length() != 0) {
                String errMsg = "OthSet單號：" + tempTo.getOsNo() + " ，來源單號：" + tempTo.getRequireNo();
                errMsg += "<BR/>異動欄位：" + errorMsg.toString().substring(0, errorMsg.length() - 1);
                throw new OthSetEditExceptions(errMsg);
            }
        }
    }

    /**
     * 判斷通知單位是否相同
     *
     * @param noticeDepsByTemp
     * @param noticeDepsByDb
     * @return
     */
    private boolean checkSameByNoticeDeps(List<String> noticeDepsByTemp, List<String> noticeDepsByDb) {
        if (noticeDepsByTemp.size() != noticeDepsByDb.size()) {
            return false;
        }
        return noticeDepsByTemp.stream()
                .allMatch(each -> noticeDepsByDb.contains(each));
    }

    /**
     * 存檔只部份更新
     *
     * @param obj
     * @param executor
     */
    @Transactional(rollbackFor = Exception.class)
    private void modifyByEditOthSet(OthSet obj, User executor) {
        OthSet backupObj = this.findByOsNo(obj.getOsNo());
        backupObj.setNoticeDeps(obj.getNoticeDeps());
        backupObj.setTheme(obj.getTheme());
        backupObj.setContent(obj.getContent());
        backupObj.setContentCss(obj.getContentCss());
        backupObj.setNote(obj.getNote());
        backupObj.setNoteCss(obj.getNoteCss());
        backupObj.setUpdatedDate(new Date());
        backupObj.setUpdatedUser(executor);
        othsetDao.save(backupObj);
    }

    /**
     * 批次轉單程式-建立單位轉發(其他設定)
     * 
     * @return
     */
    public List<TransRequireVO> findOthSetByCreateDeptAndUser(List<Integer> deptSids, List<Integer> userSids) {
        List<TransRequireVO> resultList = trNamedQueryRepository.findOthSetByCreateDeptAndUser(deptSids, userSids);
        return convert(resultList);
    }

    /**
     * 批次轉單程式-通知單位轉發(其他設定)
     * 
     * @return
     */
    public List<TransRequireVO> findOthSetByNotifiedDept(String depts) {
        List<TransRequireVO> resultList = trNamedQueryRepository.findOthSetByNotifiedDept(depts);
        return convert(resultList);
    }

    private List<TransRequireVO> convert(List<TransRequireVO> resultList) {
        for (TransRequireVO vo : resultList) {
            vo.setType(RequireTransProgramType.OTHSET.name());
            vo.setCreateDept(WkOrgCache.getInstance().findNameBySid(vo.getDeptSid()));
            vo.setCreateUserName(WkUserCache.getInstance().findBySid(vo.getUserSid()).getId());
            vo.setStatus(OthSetStatus.valueOf(vo.getStatus()).getVal());
        }
        return resultList;
    }

    /**
     * 計算完成或未完成的單據
     * 
     * @param sourceNo   需求單號
     * @param createDeps 過濾建立部門 (為空時不過濾)
     * @param isComplete true:完成 / false:未完成
     * @return
     */
    public Integer countByCompleteStatus(
            String sourceNo,
            List<Org> createDeps,
            boolean isComplete) {

        // 取得本次所有需要過濾的狀態
        List<OthSetStatus> filterStatus = Lists.newArrayList(OthSetStatus.values()).stream()
                .filter(ptStatus -> isComplete == ptStatus.isInFinish())
                .collect(Collectors.toList());

        // 需過濾建立部門
        if (WkStringUtils.notEmpty(createDeps)) {
            return this.othsetDao.queryCountByCreateDepAndInOthSetStatus(
                    sourceNo,
                    createDeps,
                    filterStatus);
        }

        // 僅檢查狀態
        return this.othsetDao.queryCountByOthSetStatus(
                sourceNo,
                filterStatus);
    }

    /**
     * 計算完成或未完成的單據
     * 
     * @param sourceNo   需求單號
     * @param createDeps 過濾建立部門 (為空時不過濾)
     * @param isComplete true:完成 / false:未完成
     * @return
     */
    public List<OthSet> queryByCompleteStatus(
            String sourceNo,
            List<Org> createDeps,
            boolean isComplete) {

        // 取得本次所有需要過濾的狀態
        List<OthSetStatus> filterStatus = Lists.newArrayList(OthSetStatus.values()).stream()
                .filter(ptStatus -> isComplete == ptStatus.isInFinish())
                .collect(Collectors.toList());

        // 需過濾建立部門
        return this.othsetDao.queryByCreateDepAndInOthSetStatus(
                sourceNo,
                createDeps,
                filterStatus);

    }

    /**
     * 強制完成
     * 
     * @param require             需求單主檔
     * @param forceCompleteStatus 強制完成類型
     * @param execUser            執行者
     * @param execDate            執行時間
     * @param createDeps          指定立案單位 （傳入null時,代表不限定.該單據下子單全關）
     * @return 已強制結案單據資料
     */
    @Transactional(rollbackFor = Exception.class)
    public List<OthSet> forceComplete(
            Require require,
            OthSetStatus forceCompleteStatus,
            User execUser,
            Date execDate,
            List<Org> createDeps) {

        if (forceCompleteStatus == null || !forceCompleteStatus.isForceCompleteStatus()) {
            throw new SystemDevelopException("傳入的 OthSetStatus 不是可以處理的類型：[" + forceCompleteStatus + "]", InfomationLevel.ERROR);
        }

        // ====================================
        // 取得所有未完成單據
        // ====================================
        // 收集未完成狀態
        List<OthSetStatus> notCompleteStatus = Lists.newArrayList(OthSetStatus.values()).stream()
                .filter(status -> !status.isInFinish())
                .collect(Collectors.toList());

        // 查詢
        List<OthSet> othSets = null;

        if (createDeps == null) {
            // 取得需求單下所有子單
            othSets = this.othsetDao.findByRequireNoAndOsStatusIn(
                    require.getRequireNo(),
                    notCompleteStatus);
        } else {
            // 取得需求單下傳入單位開立的子單
            othSets = this.othsetDao.findByRequireNoAndOsStatusInAndCreateDepIn(
                    require.getRequireNo(),
                    notCompleteStatus,
                    createDeps);
        }

        if (WkStringUtils.isEmpty(othSets)) {
            log.info("沒有需要強制關閉的【其他設定資訊】單");
            return Lists.newArrayList();
        }

        // ====================================
        // 逐筆完成
        // ====================================
        for (OthSet othSet : othSets) {

            log.info("[{}]:其他設定資訊單[{}]因 {} 而關閉!",
                    othSet.getRequireNo(),
                    othSet.getOsNo(),
                    forceCompleteStatus.getVal());

            // 歷程
            OthSetHistory othSetHistory = this.othSetHistoryService.createEmptyHistory(othSet, execUser, execDate);
            othSetHistory.setBehavior(OthSetHistoryBehavior.FORCE_CLOSE);
            othSetHistory.setBehaviorStatus(forceCompleteStatus);
            this.osHistoryDao.save(othSetHistory);

            this.othSetHistoryService.putHistoryToOthset(othSetHistory.getOthset(), othSetHistory);
            this.othSetHistoryService.sortHistory(othSetHistory.getOthset());

            // 更新主檔
            othSet.setOsStatus(forceCompleteStatus);
            othSet.setFinishDate(new Date());
            othSet.setUpdatedDate(execDate);
            othSet.setUpdatedUser(execUser);
            this.othsetDao.save(othSet);
        }

        return othSets;
    }
}
