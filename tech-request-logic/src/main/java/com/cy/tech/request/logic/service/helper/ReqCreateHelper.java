/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.helper;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.AssignNoticeService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.TemplateService;
import com.cy.tech.request.logic.service.onpg.OnpgService;
import com.cy.tech.request.vo.enums.CateConfirmType;
import com.cy.tech.request.vo.enums.ReqCateType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.template.component.ComBase;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author shaun
 */
@Component
public class ReqCreateHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7560892685877882392L;
    @Autowired
    private RequireService requireService;
    @Autowired
    private TemplateService tempService;
    @Autowired
    private OnpgService opService;
    @Autowired
    private AssignNoticeService assignNoticeService;


    /**
     * 建立內部需求對應邏輯
     *
     * @param req
     * @param executor
     * @param isFinishToComplete
     * @return
     * @throws UserMessageException
     */
    @Transactional(rollbackFor = Exception.class)
    public WorkOnpg createInternalCate(Require req, User executor) throws UserMessageException {
        // 若為內部需求（大類設定），不需處理
        if (!req.getMapping().getBig().getReqCateType().equals(ReqCateType.INTERNAL)) {
            return null;
        }
        // 已層簽時，不需處理
        if (req.getHasReqUnitSign()) {
            req.setRequireStatus(RequireStatusType.NEW_INSTANCE);
            return null;
        }

        return this.createInternalAction(
                req.getSid(),
                req.getRequireNo(),
                executor,
                new Date());
    }

    /**
     * 建立自動分派及判斷是否產生ONPG<BR/>
     * 需求單位簽核完成時也要觸發
     *
     * @param require
     * @param executor
     * @return
     * @throws UserMessageException
     * @throws SystemDevelopException
     */
    @Transactional(rollbackFor = Exception.class)
    public WorkOnpg createInternalAction(
            String requireSid,
            String requireNo,
            User executor,
            Date execDate) throws UserMessageException {

        Require require = this.requireService.findByReqSid(requireSid);

        // ====================================
        // 更新主檔
        // ====================================
        // 需求製作進度＝進行中、
        require.setRequireStatus(RequireStatusType.PROCESS);
        // 檢查確認碼寫入n (小寫n)、分派資訊需寫入需求單的填單單位
        require.setCategoryConfirmCode(CateConfirmType.n);

        require = this.requireService.save(require);

        // ====================================
        // 自動分派給登入者單位
        // ====================================
        this.assignNoticeService.processForInternalAdd(
                requireSid,
                requireNo,
                executor,
                execDate);

        require = this.requireService.findByReqSid(requireSid);

        // ====================================
        // 需要時, 自動 on 程式 (小類的自動 on 程式設定)
        // ====================================
        // 需要自動產生ON程式

        if (require.getMapping().getSmall().getAutoCreateOnpg()) {
            Map<String, ComBase> valueMap = tempService.loadTemplateFieldValue(require);
            return this.opService.createInternalOnpg(
                    requireSid,
                    requireNo,
                    tempService.findTheme(require, valueMap),
                    tempService.findEditorByFieldName(require, valueMap, TemplateService.FIELD_NAME_CONTEXT),
                    tempService.findEditorByFieldName(require, valueMap, TemplateService.FIELD_NAME_NOTE),
                    executor,
                    execDate);

        }

        return null;
    }

}
