package com.cy.tech.request.logic.service.setting;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.OrgLevel;
import com.cy.work.backend.logic.WorkBackendParamService;
import com.cy.work.backend.logic.constants.WkBackendConstants;
import com.cy.work.backend.vo.WorkBackendParam;
import com.cy.work.backend.vo.WorkBackendParamVO;
import com.cy.work.backend.vo.enums.WkBackendParam;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.common.vo.converter.SplitListIntConverter;
import com.cy.work.logic.WkLogicUtil;
import com.google.common.collect.Lists;

/**
 * @author allen1214_wu
 *
 */
@Service
public class SettingAdminBackandParamMaintainService {

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private transient WorkBackendParamService workBackendParamService;
    @Autowired
    private transient WkLogicUtil wkLogicUtil;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 查詢可設定的參數
     * 
     * @return
     */
    public List<WorkBackendParamVO> findAllCanSettingParam() {

        // ====================================
        // 查詢所有參數
        // ====================================
        List<WorkBackendParam> entitys = workBackendParamService.findAll();

        // ====================================
        // 過濾並轉型
        // ====================================
        List<WorkBackendParamVO> vos = Lists.newArrayList();
        if (WkStringUtils.notEmpty(entitys)) {


            vos = entitys.stream()
                    .filter(entity -> entity.getKeyword() != null) // 過濾 Keyword 對不到列舉的參數
                    .filter(entity -> entity.getKeyword().isCanSetting()) // 過濾不可設定的參數
                    //.sorted(comparator) // 依據 SEQ、keyword 排序
                    .map(entity -> new WorkBackendParamVO(entity))
                    .collect(Collectors.toList());
        }

        // ====================================
        // DB中不存在, 但列舉中有此參數者也要顯示
        // ====================================
        // 整裡DB已存在的參數
        Set<WkBackendParam> existKeywordSet = vos.stream()
                .map(WorkBackendParamVO::getKeyword)
                .collect(Collectors.toSet());

        for (WkBackendParam wkBackendParam : WkBackendParam.values()) {
            // 無需顯示時不處理
            if (!wkBackendParam.isCanSetting()) {
                continue;
            }
            // 已存在時(DB已有資料)不處理
            if (existKeywordSet.contains(wkBackendParam)) {
                continue;
            }
            // 補空的VO
            WorkBackendParamVO vo = new WorkBackendParamVO();
            vo.setKeyword(wkBackendParam);
            vo.setDescription(wkBackendParam.getDefaultDescr());

            vos.add(vo);
        }
        
        // ====================================
        // DB中不存在, 但列舉中有此參數者也要顯示
        // ====================================
        // 排序
        // 1.系統別
        Comparator<WorkBackendParamVO> comparator = Comparator
                .comparing(each -> each.getKeyword().getWkBackendParamUseSysType().ordinal());
        comparator = comparator.reversed();
        //2.參數名稱
        comparator = comparator.thenComparing(Comparator.comparing(each -> each.getKeyword().name()));
        
        vos = vos.stream()
                .sorted(comparator)
                .collect(Collectors.toList());

        // ====================================
        // 準備參數內容顯示
        // ====================================
        SplitListIntConverter splitListIntConverter = new SplitListIntConverter();
        for (WorkBackendParamVO workBackendParamVO : vos) {

            String contentShow = "";

            switch (workBackendParamVO.getKeyword().getWkBackendParamValueType()) {
            case TEXT:
                contentShow = workBackendParamVO.getContent();
                break;

            case DEP_SID: // 部門SID
                List<Integer> orgSids = splitListIntConverter.convertToEntityAttribute(workBackendParamVO.getContent());
                contentShow = WkOrgUtils.prepareDepsNameByTreeStyle(orgSids, 10);
                break;

            case DEP_SID_INCLUDE: // 部門-含以下 (值僅存放最高單位)

                // 取得所有最上層的單位
                List<Integer> topOrgSids = splitListIntConverter.convertToEntityAttribute(workBackendParamVO.getContent());
                // 算出含以下單位
                Set<Integer> topWithChildOrgSidsIntegers = WkOrgUtils.prepareAllDepsByTopmost(topOrgSids, false);

                contentShow = WkOrgUtils.prepareDepsNameByTreeStyle(topWithChildOrgSidsIntegers, 10);
                break;

            case USER_SID:
                List<Integer> userSids = splitListIntConverter.convertToEntityAttribute(workBackendParamVO.getContent());
                contentShow = WkUserUtils.prepareUserNameWithDep(
                        userSids,
                        OrgLevel.MINISTERIAL,
                        true,
                        "-",
                        "<br/>",
                        false);
                break;
            default:
                throw new SystemDevelopException(
                        "未實做處理WkParamType:[" + workBackendParamVO.getKeyword().getWkBackendParamValueType() + "]");
            }

            workBackendParamVO.setContentShow(contentShow);

        }

        return vos;

    }

    /**
     * save by WorkBackendParamVO
     * 
     * @param workBackendParamVO workBackendParamVO
     * @param execUserSid        執行者 sid
     * @throws UserMessageException 檢核錯誤時傳出
     */
    @CacheEvict(value = WkBackendConstants.CACHE_WORK_PARAM, allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public String saveByVO(
            WorkBackendParamVO workBackendParamVO,
            Integer execUserSid) throws UserMessageException {
        // ====================================
        // 檢核
        // ====================================
        if (workBackendParamVO == null || workBackendParamVO.getKeyword() == null) {
            throw new UserMessageException("傳入資料錯誤!", InfomationLevel.ERROR);
        }

        // ====================================
        // insert or update
        // ====================================
        String message = this.workBackendParamService.save(
                workBackendParamVO.getKeyword(),
                workBackendParamVO.getContent(),
                execUserSid);

        // ====================================
        // 更新需求單快取
        // ====================================
        wkLogicUtil.clearRequireRestEhCache();

        return message;
    }
}
