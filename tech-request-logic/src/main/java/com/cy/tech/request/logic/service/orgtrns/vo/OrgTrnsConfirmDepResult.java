package com.cy.tech.request.logic.service.orgtrns.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cy.tech.request.logic.vo.RequireConfirmDepVO;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 *
 */
@Slf4j
public class OrgTrnsConfirmDepResult implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -8012979314406418402L;

    /**
	 * @param orgTrnsAssignSendInfoForTrnsTO
	 * @param oldAssignDepSids
	 * @param newAssignDepSids
	 */
	public OrgTrnsConfirmDepResult(
	        RequireConfirmDepVO requireConfirmDepVO,
	        List<Integer> oldAssignDepSids,
	        List<Integer> newAssignDepSids) {

		if (oldAssignDepSids == null) {
			oldAssignDepSids = Lists.newArrayList();
		}

		if (newAssignDepSids == null) {
			newAssignDepSids = Lists.newArrayList();
		}

		// 確認單位檔
		this.requireConfirmDep = requireConfirmDepVO;

		// 來源分派單位(確認單位由此複數單位收束)
		this.assignDepSids = newAssignDepSids;

		// 加派部門
		this.plusAssignDepSid = Lists.newArrayList(
		        WkCommonUtils.listLeaves(
		                newAssignDepSids,
		                oldAssignDepSids));

		// 減派部門
		this.minusAssignDepSid = Lists.newArrayList(
				WkCommonUtils.listLeaves(
		        oldAssignDepSids,
		        newAssignDepSids));
	}

	/**
	 * 加派部門
	 */
	@Getter
	private List<Integer> plusAssignDepSid;
	/**
	 * 減派部門
	 */
	@Getter
	private List<Integer> minusAssignDepSid;
	/**
	 * 來源分派單位(確認單位由此複數單位收束)
	 */
	@Getter
	private List<Integer> assignDepSids;

	@Getter
	@Setter
	public RequireConfirmDepVO requireConfirmDep;

	@Getter
	@Setter
	public Set<RequireConfirmDepVO> beforeConfirmDeps = Sets.newHashSet();

	/**
	 * 取得本筆資料計算後的 處理類型
	 * 
	 * @return
	 * @throws SystemDevelopException
	 */
	public ConfirmDepResultProcType getProcType(
	        Integer confirmDepSid,
	        Map<Integer, OrgTrnsConfirmDepResult> srcConfirmDepResultMapByDepSid) throws SystemDevelopException {

		// 存在確認檔
		if (this.requireConfirmDep != null) {

			// 存在確認檔, 但已無分派單位 -> 刪除
			if (WkStringUtils.isEmpty(assignDepSids)) {
				return ConfirmDepResultProcType.DELETE;
			}

			if (WkStringUtils.isEmpty(plusAssignDepSid) && WkStringUtils.isEmpty(minusAssignDepSid)) {
				// 無加減派部門 -> 無需異動
				return ConfirmDepResultProcType.NONE;
			} else {
				// 有加減派部門 -> 異動分派單位
				return ConfirmDepResultProcType.MODIFY_ASSIGN_DEP;
			}

		}

		// 不存在確認檔
		if (this.requireConfirmDep == null) {
			// 有分派單位
			if (WkStringUtils.notEmpty(assignDepSids)) {

				// 有轉換前確認單位者為移轉, 反之為新增
				if (WkStringUtils.notEmpty(this.beforeConfirmDeps)) {

					// 需有任何一筆前身為刪除, 才可移轉 (前身已經沒有分派單位)
					for (RequireConfirmDepVO beforeConfirmDep : this.beforeConfirmDeps) {

						OrgTrnsConfirmDepResult result = srcConfirmDepResultMapByDepSid.get(
						        beforeConfirmDep.getDepSid());

						if (WkStringUtils.isEmpty(result.getAssignDepSids())) {
							this.beforeConfirmDeps.clear();
							this.beforeConfirmDeps.add(beforeConfirmDep);
							return ConfirmDepResultProcType.TRNS;
						}
					}
				}
				this.beforeConfirmDeps.clear();
				return ConfirmDepResultProcType.ADD;
			} else if (WkStringUtils.notEmpty(minusAssignDepSid)) {
				// 減派後, 無分派部門, 且無需求確認檔 (可能本來就沒產生)
				return ConfirmDepResultProcType.NONE;
			}
		}

		log.error("開發時期錯誤：沒有判斷到的條件!\r\n"
		        + new com.google.gson.GsonBuilder().setPrettyPrinting().create().toJson(this));
		throw new SystemDevelopException("開發時期錯誤：沒有判斷到的條件!");
	}

	/**
	 * @author allen1214_wu 需求確認單位,處理類型
	 */

	public enum ConfirmDepResultProcType {
		NONE("無需處理", true, false),
		MODIFY_ASSIGN_DEP("異動分派單位", true, false),
		ADD("新增", true, true),
		DELETE("需刪除", false, false),
		TRNS("移轉", true, true);

		/**
		 * 說明
		 */
		@Getter
		private final String label;

		/**
		 * 是否需檢查負責人
		 */
		@Getter
		private final boolean checkOwner;

		/**
		 * 
		 */
		@Getter
		private final boolean showAssignDebugMessage;

		ConfirmDepResultProcType(String label, boolean checkOwner, boolean showAssignDebugMessage) {
			this.label = label;
			this.checkOwner = checkOwner;
			this.showAssignDebugMessage = showAssignDebugMessage;
		}

		/**
		 * @param dbStr
		 * @return
		 */
		public static ConfirmDepResultProcType trnsFromStr(String dbStr) {
			for (ConfirmDepResultProcType c : ConfirmDepResultProcType.values()) {
				if (c.name().equals(dbStr)) {
					return c;
				}
			}
			return null;
		}
	}
}
