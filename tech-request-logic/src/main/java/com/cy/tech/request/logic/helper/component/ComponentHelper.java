/**
 * 
 */
package com.cy.tech.request.logic.helper.component;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.service.SimpleCategoryService;
import com.cy.tech.request.logic.vo.SelectedItemPrepareInfoTo;
import com.cy.tech.request.vo.category.SimpleBigCategoryVO;
import com.cy.tech.request.vo.category.SimpleMiddleCategoryVO;
import com.cy.tech.request.vo.category.SimpleSmallCategoryVO;
import com.cy.tech.request.vo.constants.CacheConstants;
import com.cy.tech.request.vo.enums.CategoryType;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.NoArgsConstructor;

/**
 * @author allen1214_wu
 *
 */
@NoArgsConstructor
@Service
public class ComponentHelper implements InitializingBean, Serializable {

    // ========================================================================
    // InitializingBean
    // ========================================================================

    /**
     * 
     */
    private static final long serialVersionUID = -6568254283800984764L;

    private static ComponentHelper instance;

    public static ComponentHelper getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        ComponentHelper.instance = this;
    }

    // ========================================================================
    // 工具區
    // ========================================================================
    /**
     * 類別資料服務
     */
    @Autowired
    private SimpleCategoryService simpleCategoryService;

    // ========================================================================
    // 變數
    // ========================================================================
    public final String FORCE_MESSAGE_NO_ITEM = "【未設定】";
    public final String FORCE_MESSAGE_ALL_ITEM = "【全部】";
    /**
     * 項目資訊 KEY : 項目類別
     */
    public final String ITEM_INFO_CATEGORY_TYPE = "ITEM_INFO_CATEGORY_TYPE";

    /**
     * 項目資訊 KEY : 父項SID
     */
    public final String ITEM_INFO_PARENT_SID = "ITEM_INFO_PARENT_SID";

    // ========================================================================
    // 方法區
    // ========================================================================

    /**
     * @param allItems
     * @param blackList
     * @return
     */
    public SelectedItemPrepareInfoTo prepareSelectedItemInfo(
            List<WkItem> allItems,
            List<String> blackList) {

        // ====================================
        // 防呆
        // ====================================
        if (WkStringUtils.isEmpty(allItems)) {
            return new SelectedItemPrepareInfoTo(FORCE_MESSAGE_NO_ITEM);
        }

        // ====================================
        // 取得所有項目Sid
        // ====================================
        List<String> allItemSid = allItems.stream()
                .map(WkItem::getSid)
                .collect(Collectors.toList());

        // ====================================
        // 排除沒有用的黑名單項目 (可能已被停用)
        // ====================================
        // 黑名單與所有項目作交集
        blackList.retainAll(allItemSid);

        // ====================================
        // 沒有可用的黑名單
        // ====================================
        if (WkStringUtils.isEmpty(blackList)) {
            return new SelectedItemPrepareInfoTo(FORCE_MESSAGE_ALL_ITEM);
        }

        // ====================================
        // 黑名單交集後(移除不存在項目)數量，等於全部項目數量，代表未設定 (選擇項目為空)
        // ====================================
        if (blackList.size() == allItems.size()) {
            return new SelectedItemPrepareInfoTo(FORCE_MESSAGE_NO_ITEM);
        }

        // ====================================
        // 計算黑名單比例
        // ====================================
        // 黑名單數量 / 全部數量
        double blockListPercent = Double.parseDouble(blackList.size() + "") / Double.parseDouble(allItemSid.size() + "");

        // 黑名單小於3成時，反轉顯示(顯示排除項目)
        boolean isReverse = blockListPercent < 0.3;

        // 判斷要顯示的名單
        List<String> showSids = blackList;
        if (!isReverse) {
            // 整理出正向名單 (排除黑名單)
            showSids = allItemSid.stream()
                    .filter(sid -> !blackList.contains(sid))
                    .collect(Collectors.toList());
        }

        // ====================================
        // 取得 selected 項目
        // ====================================
        final List<String> effectivelyFinalShowSids = showSids;

        List<String> selectedItemSids = allItems.stream()
                .filter(item -> effectivelyFinalShowSids.contains(item.getSid()))
                .map(WkItem::getSid)
                .collect(Collectors.toList());

        List<String> selectedItemNames = allItems.stream()
                .filter(item -> effectivelyFinalShowSids.contains(item.getSid()))
                .map(WkItem::getName)
                .collect(Collectors.toList());

        // ====================================
        // 回傳
        // ====================================
        return new SelectedItemPrepareInfoTo(isReverse, selectedItemSids, selectedItemNames);
    }

    /**
     * 強制訊息特別處理 for 系統通知設定
     * 
     * @param selectedItemPrepareInfoTo
     * @return
     */
    public String prepareForceMessageForSettingSysNotify(SelectedItemPrepareInfoTo selectedItemPrepareInfoTo) {
        String forceMessage = selectedItemPrepareInfoTo.getForceMessage();

        if (FORCE_MESSAGE_NO_ITEM.equals(forceMessage)) {
            forceMessage = "<span class='WS1-1-2b'>【全部不通知】</span>";
        }

        if (FORCE_MESSAGE_ALL_ITEM.equals(forceMessage)) {
            forceMessage = "<span class='WS1-1-3b'>【全部通知】</span>";
        }

        return forceMessage;
    }

    // ========================================================================
    // 需求項目公用方法
    // ========================================================================
    /**
     * @return
     */
    @Cacheable(cacheNames = CacheConstants.CACHE_ALL_CATEGORY_PICKER)
    public List<WkItem> prepareAllCategoryItems() {

        // ====================================
        // 查詢所有類別資料
        // ====================================
        // 查詢所有大類
        List<SimpleBigCategoryVO> bigCategorys = this.simpleCategoryService.findAllBigCategory();
        // 查詢所有中類
        List<SimpleMiddleCategoryVO> middleCategorys = this.simpleCategoryService.findAllMiddleCategory();
        // 查詢所有小類
        List<SimpleSmallCategoryVO> smallCategorys = this.simpleCategoryService.findAllSmallCategory();

        // ====================================
        // 將需求類別資料轉為 WkItem
        // ====================================
        return this.prepareCategoryToWkItems(bigCategorys, middleCategorys, smallCategorys);
    }

    /**
     * 準備所有需求項目（除了小類）
     * 
     * @return
     */
    public List<WkItem> prepareAllCategoryItemsWithoutSmall() {
        // ====================================
        // 類別資料
        // ====================================
        // 查詢所有大類
        List<SimpleBigCategoryVO> bigCategorys = this.simpleCategoryService.findAllBigCategory();
        // 查詢所有中類
        List<SimpleMiddleCategoryVO> middleCategorys = this.simpleCategoryService.findAllMiddleCategory();
        // ====================================
        // 將需求類別資料轉為 WkItem
        // ====================================
        return this.prepareCategoryToWkItems(bigCategorys, middleCategorys, null);
    }

    /**
     * 將需求類別資料轉為 WkItem
     * 
     * @param bigCategorys    大類
     * @param middleCategorys 中類
     * @param smallCategorys  小類
     * @return WkItems
     */
    private List<WkItem> prepareCategoryToWkItems(
            List<SimpleBigCategoryVO> bigCategorys,
            List<SimpleMiddleCategoryVO> middleCategorys,
            List<SimpleSmallCategoryVO> smallCategorys) {

        // ====================================
        // 避免傳入資料為 null 時發生錯誤
        // ====================================
        if (bigCategorys == null) {
            bigCategorys = Lists.newArrayList();
        }

        if (middleCategorys == null) {
            middleCategorys = Lists.newArrayList();
        }

        if (smallCategorys == null) {
            smallCategorys = Lists.newArrayList();
        }

        // ====================================
        // 轉為 WkItem 資料
        // ====================================
        List<WkItem> allItems = Lists.newArrayList();
        Map<String, WkItem> itemMapBySid = Maps.newHashMap();
        // Long itemSeq = Long.valueOf("0");
        // 處理大類
        for (SimpleBigCategoryVO bigVO : bigCategorys) {
            // create item
            WkItem item = new WkItem(bigVO.getSid(), bigVO.getName(), bigVO.isActive());

            // 搜尋名稱
            item.getItemNamesForSearch().add(bigVO.getName());

            item.setShowTreeClass(CategoryType.BIG.getStyleClass());
            // item.setShowTreeName("<font class='" + CategoryType.BIG.getStyleClass() +
            // "'>" + bigVO.getName() + "</font>");

            // 註記為大項
            item.getOtherInfo().put(ITEM_INFO_CATEGORY_TYPE, CategoryType.BIG.name());
            // 註記父項SID (大類無父項)
            item.getOtherInfo().put(ITEM_INFO_PARENT_SID, "");
            // 排序
            item.setItemSeq((WkStringUtils.isNumber(bigVO.getId())) ? Long.valueOf(bigVO.getId()) : Long.MAX_VALUE);
            // 強制展開第一層
            item.setForceExpand(true);

            // 加入結果集
            allItems.add(item);
            itemMapBySid.put(bigVO.getSid(), item);
        }

        // 處理中類
        for (SimpleMiddleCategoryVO middleVO : middleCategorys) {
            // create item
            WkItem item = new WkItem(middleVO.getSid(), middleVO.getName(), middleVO.isActive());
            item.setShowTreeClass(CategoryType.MIDDLE.getStyleClass());

            // 搜尋名稱
            item.getItemNamesForSearch().add(middleVO.getName());

            // item.setShowTreeName("<font class='" + CategoryType.MIDDLE.getStyleClass() +
            // "'>" + middleVO.getName() + "</font>");
            // 註記為中項
            item.getOtherInfo().put(ITEM_INFO_CATEGORY_TYPE, CategoryType.MIDDLE.name());
            // 註記父項SID
            item.getOtherInfo().put(ITEM_INFO_PARENT_SID, middleVO.getParentBigCategorySid());
            // 排序
            // item.setItemSeq(itemSeq++);
            item.setItemSeq((WkStringUtils.isNumber(middleVO.getId())) ? Long.valueOf(middleVO.getId()) : Long.MAX_VALUE);

            // 加入結果集
            allItems.add(item);
            itemMapBySid.put(middleVO.getSid(), item);
        }

        // 處理小類
        for (SimpleSmallCategoryVO smallVO : smallCategorys) {
            // create Item
            WkItem item = new WkItem(smallVO.getSid(), smallVO.getName(), smallVO.isActive());
            item.setShowTreeClass(CategoryType.SMALL.getStyleClass());

            // 搜尋名稱
            item.getItemNamesForSearch().add(smallVO.getName());
            // item.setShowTreeName("<font class='" + CategoryType.SMALL.getStyleClass() +
            // "'>" + smallVO.getName() + "</font>");
            // 註記為中項
            item.getOtherInfo().put(ITEM_INFO_CATEGORY_TYPE, CategoryType.SMALL.name());
            // 註記父項SID
            item.getOtherInfo().put(ITEM_INFO_PARENT_SID, smallVO.getParentMiddleCategorySid());
            // 排序
            // item.setItemSeq(itemSeq++);
            item.setItemSeq((WkStringUtils.isNumber(smallVO.getId())) ? Long.valueOf(smallVO.getId()) : Long.MAX_VALUE);

            // 加入結果集
            allItems.add(item);
            itemMapBySid.put(smallVO.getSid(), item);
        }

        // ====================================
        // 建立父子關係
        // ====================================
        for (WkItem wkItem : allItems) {
            // 以父項 SID 取得父項 (無父項則為 null)
            WkItem parentItem = itemMapBySid.get(wkItem.getOtherInfo().get(ITEM_INFO_PARENT_SID));
            // 設定
            wkItem.setParent(parentItem);
        }

        // ====================================
        // 清掉無連結資料
        // ====================================
        List<WkItem> finalAllItems = Lists.newArrayList();
        for (WkItem wkItem : allItems) {
            CategoryType categoryType = this.getItemCategoryType(wkItem);

            if (CategoryType.SMALL.equals(categoryType)) {
                // 需可連結到大類 (兩層)
                if (wkItem.getParent() == null || wkItem.getParent().getParent() == null) {
                    continue;
                }
            }

            if (CategoryType.MIDDLE.equals(categoryType)) {
                // 需可連結到大類 (一層)
                if (wkItem.getParent() == null) {
                    continue;
                }
            }
            // 檢核無誤
            finalAllItems.add(wkItem);
        }

        // ====================================
        //
        // ====================================
        return finalAllItems;
    }

    /**
     * 取得項目的類別資訊
     * 
     * @param wkItem
     * @return
     */
    public CategoryType getItemCategoryType(WkItem wkItem) {
        if (wkItem == null) {
            return null;
        }

        return CategoryType.safeValueOf(
                wkItem.getOtherInfo().get(ITEM_INFO_CATEGORY_TYPE));

    }
}
