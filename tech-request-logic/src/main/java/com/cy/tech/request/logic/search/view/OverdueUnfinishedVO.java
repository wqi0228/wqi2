package com.cy.tech.request.logic.search.view;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Convert;

import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.vo.converter.UrgencyTypeConverter;

import lombok.Getter;
import lombok.Setter;

public class OverdueUnfinishedVO extends BaseSearchView implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 4408057458505316134L;
    @Getter
    @Setter
    private String requireSid;
    @Getter
    @Setter
    private Date createDate;
    @Getter
    @Setter
    private String bigCategoryName;
    @Getter
    @Setter
    private String middleCategoryName;
    @Getter
    @Setter
    private String smallCategoryName;
    @Getter
    @Setter
    private Integer depSid;
    @Getter
    @Setter
    private Integer createUserSid;
    @Getter
    @Setter
    private Date assignDate;
    @Getter
    @Setter
    private Integer inteStaffUserSid;
    @Getter
    @Setter
    private Integer executeDays;
    @Getter
    @Setter
    private String deptSids;
    @Getter
    @Setter
    private Integer overdueDays;
    @Getter
    @Setter
    private Date hopeDate;
    @Getter
    @Setter
    private String overdue;
    @Getter
    @Setter
    /** 緊急度 */
    @Convert(converter = UrgencyTypeConverter.class)
    private UrgencyType urgency = UrgencyType.GENERAL;

    public String getUrgencyDesc() {
        if (urgency != null) {
            return urgency.getValue();
        }
        return "";
    }

    /** convert columns */
    @Getter
    @Setter
    /** 本地端連結網址 */
    private String localUrlLink;
    @Getter
    @Setter
    private String deptNames;
    @Getter
    @Setter
    private String createUserName;
    @Getter
    @Setter
    private String inteStaffUserName;
    @Getter
    @Setter
    private String deptName;

    /**
     * 需求成立日
     */
    @Getter
    @Setter
    private Date requireEstablishDate;
    /**
     * 需求完成日
     */
    @Getter
    @Setter
    private Date requireFinishDate;
}
