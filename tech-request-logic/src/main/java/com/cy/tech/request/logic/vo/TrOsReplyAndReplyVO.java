/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * 其它設定資訊 - 回覆的回覆物件
 *
 * @author brain0925_liao
 */
public class TrOsReplyAndReplyVO implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 6775766392343055711L;
    public TrOsReplyAndReplyVO(String os_reply_and_reply_sid,
            String os_reply_sid, String os_history_sid,
            String os_sid, String os_no,
            String require_sid,
            String require_no,
            Integer reply_person_dep,
            Integer reply_person_sid,
            Date reply_dt, Date reply_udt,
            String reply_content, String reply_content_css,
            List<TrOsAttachmentVO> trOsAttachmentVOs) {
        this.os_reply_and_reply_sid = os_reply_and_reply_sid;
        this.os_reply_sid = os_reply_sid;
        this.os_history_sid = os_history_sid;
        this.os_sid = os_sid;
        this.os_no = os_no;
        this.require_sid = require_sid;
        this.require_no = require_no;
        this.reply_person_dep = reply_person_dep;
        this.reply_person_sid = reply_person_sid;
        this.reply_dt = reply_dt;
        this.reply_udt = reply_udt;
        this.reply_content = reply_content;
        this.reply_content_css = reply_content_css;
        this.trOsAttachmentVOs = trOsAttachmentVOs;
    }

    @Getter
    private final String os_reply_and_reply_sid;
    @Getter
    private final String os_reply_sid;
    @Setter
    @Getter
    private String os_history_sid;
    @Getter
    private final String os_sid;
    @Getter
    private final String os_no;
    @Getter
    private final String require_sid;
    @Getter
    private final String require_no;
    @Getter
    private final Integer reply_person_dep;
    @Getter
    private final Integer reply_person_sid;
    @Getter
    private final Date reply_dt;
    @Getter
    private final Date reply_udt;
    @Getter
    private final String reply_content;
    @Getter
    private final String reply_content_css;
    @Getter
    private List<TrOsAttachmentVO> trOsAttachmentVOs;

}
