/**
 * 
 */
package com.cy.tech.request.logic.service;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.logic.helper.systemnotify.SysNotifyHelper;
import com.cy.tech.request.logic.service.setting.SettingCheckConfirmRightService;
import com.cy.tech.request.logic.utils.ProcessLog;
import com.cy.tech.request.logic.vo.CheckItmesInfoByRequireTo;
import com.cy.tech.request.repository.require.RequireCheckItemRepository;
import com.cy.tech.request.vo.enums.CheckItemStatus;
import com.cy.tech.request.vo.enums.ReqCateType;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireCheckItem;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.common.vo.value.to.ShowInfo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * 需求單-檢查項檔 Service
 * 
 * @author allen1214_wu
 */
@Service
@Slf4j
public class RequireCheckItemService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3236787301464614398L;
    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private transient RequireTraceService requireTraceService;
    @Autowired
    private transient RequireService requireService;
    @Autowired
    private transient SysNotifyHelper sysNotifyHelper;
    @Autowired
    private transient SettingCheckConfirmRightService settingCheckConfirmRightService;
    @Autowired
    private transient RequireCheckItemRepository requireCheckItemRepository;
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private JdbcTemplate jdbcTemplate;

    // ========================================================================
    // 方法
    // ========================================================================

    /**
     * 建立新的物件
     * 
     * @param requireSid    需求單 sid
     * @param checkItemType 檢查項目類別
     * @param createdUser   建立者 sid
     * @param createdDate   建立日期
     * @return RequireCheckItem
     */
    private RequireCheckItem createNewEntity(
            String requireSid,
            RequireCheckItemType checkItemType,
            Integer createdUser,
            Date createdDate) {
        RequireCheckItem entity = new RequireCheckItem();

        entity.setCreatedDate(createdDate);
        entity.setCreatedUser(createdUser);
        entity.setStatus(Activation.ACTIVE);
        entity.setRequireSid(requireSid);
        entity.setCheckItemType(checkItemType);

        return entity;
    }

    /**
     * 執行檢查確認
     * 
     * @throws UserMessageException
     */
    @Transactional(rollbackFor = Exception.class)
    public void execConfirmCheck(
            String requireSid,
            List<RequireCheckItemType> confirmCheckItems,
            Integer execUserSid,
            Date execDate) throws UserMessageException {

        // ====================================
        // 查詢單據檢查項目
        // ====================================
        // 查詢
        List<RequireCheckItem> requireCheckItems = this.requireCheckItemRepository.findByRequireSid(requireSid);
        // 檢核
        if (WkStringUtils.isEmpty(requireCheckItems)) {
            log.warn("[{}]取不到檢查項目 (from tr_require_check_item where status=0)", requireSid);
            throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }
        // 建立索引
        Map<RequireCheckItemType, RequireCheckItem> requireCheckItemsMapByType = requireCheckItems.stream()
                .collect(Collectors.toMap(
                        RequireCheckItem::getCheckItemType,
                        each -> each));

        // ====================================
        // 依據傳入的檢查項目，逐筆處理
        // ====================================
        for (RequireCheckItemType checkItemType : confirmCheckItems) {
            // 取得檢查項目資料檔
            RequireCheckItem requireCheckItem = requireCheckItemsMapByType.get(checkItemType);

            // 檢核為空
            if (requireCheckItem == null) {
                String message = WkMessage.NEED_RELOAD
                        + "檢查項目：[" + checkItemType.getDescr() + "] 可能已從單據移除!";
                log.warn(message);
                throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
            }

            // 檢查項目已檢查
            if (requireCheckItem.getCheckDate() != null) {
                String message = WkMessage.NEED_RELOAD
                        + "檢查項目：[" + checkItemType.getDescr() + "],"
                        + "已由 " + WkUserUtils.findNameBySid(requireCheckItem.getCheckUserSid()) + "檢查完成!";
                log.warn(message);
                throw new UserMessageException(message, InfomationLevel.WARN);
            }

            // 更新檢查項目的檢查資訊 -> 已檢查
            requireCheckItem.setCheckDate(execDate);
            requireCheckItem.setCheckUserSid(execUserSid);
            // 不更新 update 資訊, 當修改檢查項目時，才異動

            // save
            this.requireCheckItemRepository.save(requireCheckItem);
        }
    }

    /**
     * 查詢單據的檢查項目資料(包含停用)
     * 
     * @param requireSid
     * @return
     */
    public List<RequireCheckItem> findAllCheckItemsByRequireSid(String requireSid) {
        return this.requireCheckItemRepository.findByRequireSid(requireSid);
    }

    /**
     * 查詢單據已選擇的類別
     * 
     * @param requireSid
     * @return
     */
    public List<RequireCheckItem> findCheckItemsByRequireSid(String requireSid) {

        // ====================================
        // 查詢
        // ====================================
        List<RequireCheckItem> requireCheckItems = this.requireCheckItemRepository.findByRequireSid(requireSid);

        if (WkStringUtils.isEmpty(requireCheckItems)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 過濾停用資料
        // ====================================
        requireCheckItems = requireCheckItems.stream()
                .filter(RequireCheckItem::isActive)
                .collect(Collectors.toList());

        // ====================================
        // 以項目順序排序
        // ====================================
        Comparator<RequireCheckItem> comparator = Comparator.comparing(checkItem -> {
            // 防呆, 為空時排到最後 (應該不可能發生)
            if (checkItem.getCheckItemType() == null) {
                return Integer.MAX_VALUE;
            }
            return checkItem.getCheckItemType().ordinal();
        });

        requireCheckItems = requireCheckItems.stream()
                .sorted(comparator)
                .collect(Collectors.toList());

        return requireCheckItems;
    }

    /**
     * 查詢單據已選擇的類別
     * 
     * @param requireSid
     * @return
     */
    public List<RequireCheckItem> findCheckItemsByRequireSid(List<String> requireSids) {

        if (WkStringUtils.isEmpty(requireSids)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 查詢
        // ====================================
        List<List<String>> processParts = WkCommonUtils.averageAssign(requireSids, 3000);
        if (processParts.size() > 1) {
            log.debug("查詢 RequireCheckItem , 分" + processParts.size() + "次");
        }

        List<RequireCheckItem> allResults = Lists.newArrayList();

        for (List<String> currRequireSids : processParts) {
            List<RequireCheckItem> requireCheckItems = this.requireCheckItemRepository.findByRequireSidIn(
                    currRequireSids);
            if (WkStringUtils.notEmpty(requireCheckItems)) {
                allResults.addAll(requireCheckItems);
            }
        }

        return allResults;
    }

    /**
     * 查詢單據已選擇的類別
     * 
     * @param requireSid
     * @return
     */
    public List<RequireCheckItemType> findCheckItemTypesByRequireSid(String requireSid) {

        // ====================================
        // 查詢
        // ====================================
        List<RequireCheckItem> requireCheckItems = this.findCheckItemsByRequireSid(requireSid);

        if (WkStringUtils.isEmpty(requireCheckItems)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 收集資料 RequireCheckItem -> RequireCheckItemType
        // ====================================
        return requireCheckItems.stream()
                .map(RequireCheckItem::getCheckItemType)
                .collect(Collectors.toList());
    }

    /**
     * 查詢傳入使用者，在此單據的待檢查項目
     * 
     * @param requireSid 需求單 sid
     * @param userSid    user sid
     * @return
     */
    public List<RequireCheckItemType> findUserCanCheckItemTypeByRequireSid(String requireSid, Integer userSid) {

        // ====================================
        // 取得人員檢查權限
        // ====================================
        // 查詢可檢查項目權限
        List<RequireCheckItemType> canCheckItems = this.settingCheckConfirmRightService.findUserCheckRights(userSid);
        // 無檢查權限
        if (WkStringUtils.isEmpty(canCheckItems)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 查詢單據待檢查項目
        // ====================================
        // 1.查詢檢查項目檔
        // 2.過濾未檢查 (checkDate == null)
        Set<RequireCheckItemType> waitCheckItemTypes = this.findCheckItemsByRequireSid(requireSid).stream()
                .filter(requireCheckItem -> requireCheckItem.getCheckDate() == null)
                .map(RequireCheckItem::getCheckItemType)
                .collect(Collectors.toSet());

        // 無待檢查項目
        if (WkStringUtils.isEmpty(waitCheckItemTypes)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 比對『檢查權限』與『待檢查項目』
        // ====================================
        List<RequireCheckItemType> resultCanCheckItemTypes = Lists.newArrayList();
        for (RequireCheckItemType waitCheckItemType : waitCheckItemTypes) {
            // 『有權限』的『待檢查項目』則加入清單
            if (canCheckItems.contains(waitCheckItemType)) {
                resultCanCheckItemTypes.add(waitCheckItemType);
            }
        }

        return resultCanCheckItemTypes;
    }

    /**
     * 查詢單據未檢核項目
     * 
     * @param requireSid 需求單 sid
     * @return
     */
    public List<RequireCheckItemType> findWaitCheckItems(String requireSid) {
        return this.findCheckItemsByRequireSid(requireSid).stream()
                .filter(requireCheckItem -> requireCheckItem.getCheckDate() == null)
                .map(RequireCheckItem::getCheckItemType)
                .collect(Collectors.toList());
    }

    /**
     * 查詢單據已檢核項目
     * 
     * @param requireSid 需求單 sid
     * @return
     */
    public List<RequireCheckItemType> findCheckedItems(String requireSid) {
        return this.findCheckItemsByRequireSid(requireSid).stream()
                .filter(requireCheckItem -> requireCheckItem.getCheckDate() != null)
                .map(RequireCheckItem::getCheckItemType)
                .collect(Collectors.toList());
    }

    /**
     * 是否已全部檢查完成
     * 
     * @param requireSid 需求單 SID
     * @return true/false
     */
    public boolean isCheckComplete(String requireSid) {

        // ====================================
        // 查詢所有項目
        // ====================================
        List<RequireCheckItem> requireCheckItems = this.findCheckItemsByRequireSid(requireSid);

        if (WkStringUtils.isEmpty(requireCheckItems)) {
            throw new SystemDevelopException("找不到任何一個單據的檢查項目!");
        }

        // ====================================
        // 計算未檢查數量
        // ====================================
        long waitCheckItemCount = requireCheckItems.stream()
                .filter(checkItem -> checkItem.getCheckDate() == null)
                .count();

        // 未檢查數量為0 時, 代表已全部檢查完成
        return waitCheckItemCount == 0;

    }

    /**
     * 有任何一個項目已檢查)
     * 
     * @param requireSid
     * @return
     */
    public boolean hasChecked(String requireSid) {

        // 查詢所有項目
        List<RequireCheckItem> requireCheckItems = this.findCheckItemsByRequireSid(requireSid);

        if (WkStringUtils.isEmpty(requireCheckItems)) {
            return false;
        }

        // 有任何一筆已檢查
        for (RequireCheckItem requireCheckItem : requireCheckItems) {
            if (requireCheckItem.getCheckDate() != null) {
                return true;
            }
        }

        return false;
    }

    /**
     * 查詢單據已選擇的類別
     * 
     * @param requireSid
     * @return
     */
    public ShowInfo prepareSelectedItemTypesInfoByRequireSid(
            Require require, Integer loginUserSid) {

        // ====================================
        // 防呆
        // ====================================
        if (require == null
                || require.getRequireStatus() == null
                || require.getMapping() == null
                || require.getMapping().getBig() == null
                || require.getMapping().getBig().getReqCateType() == null) {
            return new ShowInfo("");
        }

        // ====================================
        // 取得所有檢查項目
        // ====================================
        List<RequireCheckItem> requireCheckItems = this.requireCheckItemRepository.findByRequireSid(
                require.getSid());

        // ====================================
        // 查詢可檢查項目權限
        // ====================================
        List<RequireCheckItemType> canCheckItems = settingCheckConfirmRightService.findUserCheckRights(
                loginUserSid);

        // ====================================
        // 取得檢查資訊
        // ====================================
        // 取得檢查資訊
        CheckItmesInfoByRequireTo checkItmesInfoByRequireTo = this.prepareCheckItemsInfo(
                require.getSid(),
                require.getMapping().getBig().getReqCateType(),
                require.getRequireStatus(),
                requireCheckItems,
                canCheckItems);

        // ====================================
        // 單據檢視畫面系統別欄位顯示文字
        // ====================================
        ShowInfo showInfo = new ShowInfo(checkItmesInfoByRequireTo.getCheckItemsInfoForFormView());

        // ====================================
        // 為檢查卻後流程，顯示檢查歷程
        // ====================================
        // 檢查確認前，無需顯示歷程，直接 return
        if (require.getRequireStatus().isFlowBeforeWaitCheck()) {
            return showInfo;
        }

        String tooltip = "";
        tooltip += "<h4>檢查歷程</h3>";
        tooltip += "<hr/>";
        tooltip += checkItmesInfoByRequireTo.getCheckConfirmInfo();
        tooltip = "<table style='border:2px #cccccc solid;'><tr><td style='white-space:nowrap;'>" + tooltip + "</td></tr></table>";

        showInfo.setTooltip(tooltip);

        return showInfo;
    }

    /**
     * @param requireSid
     * @param selectedCheckItemTypes
     * @param execUserSid
     * @param execDate
     */
    public void saveCheckItemTypes(
            String requireSid,
            List<RequireCheckItemType> selectedCheckItemTypes,
            Integer execUserSid,
            Date execDate) {

        // ====================================
        // 查詢已存在資料
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);

        // ====================================
        // 查詢已存在資料
        // ====================================
        // 查詢
        List<RequireCheckItem> requireCheckItems = this.requireCheckItemRepository.findByRequireSid(requireSid);

        // 以 RequireCheckItemType 做 index
        Map<RequireCheckItemType, RequireCheckItem> requireCheckItemMapByType = requireCheckItems.stream()
                .collect(Collectors.toMap(
                        RequireCheckItem::getCheckItemType,
                        each -> each));

        // ====================================
        // 依據選擇項目，新增或更新
        // ====================================
        List<RequireCheckItem> saveEntities = Lists.newArrayList();
        for (RequireCheckItemType checkItemType : selectedCheckItemTypes) {

            // 嘗試取得已存在的檢查類別資料
            RequireCheckItem requireCheckItem = requireCheckItemMapByType.get(checkItemType);

            // 不存在時新建一筆
            if (requireCheckItem == null) {
                // 建立新資料物件
                requireCheckItem = this.createNewEntity(requireSid, checkItemType, execUserSid, execDate);
                // 加入待更新list
                saveEntities.add(requireCheckItem);
            }
            // 需要更新為『有選擇』
            else if (Activation.INACTIVE.equals(requireCheckItem.getStatus())) {
                // 更新欄位資料
                requireCheckItem.setStatus(Activation.ACTIVE);
                requireCheckItem.setUpdatedUser(execUserSid);
                requireCheckItem.setUpdatedDate(execDate);
                // 加入待更新list
                saveEntities.add(requireCheckItem);
            }
        }

        // ====================================
        // 未選擇資料 - 更新為『未選擇』
        // ====================================
        for (RequireCheckItem requireCheckItem : requireCheckItems) {
            // 本來就停用, 略過
            if (Activation.INACTIVE.equals(requireCheckItem.getStatus())) {
                continue;
            }
            // 此筆資料的類別，有被畫面選擇，無需更新
            if (selectedCheckItemTypes.contains(requireCheckItem.getCheckItemType())) {
                continue;
            }

            // 更新欄位資料
            requireCheckItem.setStatus(Activation.INACTIVE);
            requireCheckItem.setUpdatedUser(execUserSid);
            requireCheckItem.setUpdatedDate(execDate);

            // 為待檢查確認時，清除檢查記錄
            if (RequireStatusType.WAIT_CHECK.equals(require.getRequireStatus())) {
                requireCheckItem.setCheckUserSid(null);
                requireCheckItem.setCheckDate(null);
            }

            // 加入待更新list
            saveEntities.add(requireCheckItem);
        }

        // ====================================
        // save (insert or update)
        // ====================================
        this.requireCheckItemRepository.save(saveEntities);
    }

    /**
     * 異動檢查項目 (系統別)
     * 
     * @param requireSid          需求單 sid
     * @param afterCheckItemTypes 異動後的檢查項目 (系統別)
     * @param addTrace            新增追蹤記錄
     * @param execUserSid         執行者 sid
     * @param execDate            執行時間
     * @throws UserMessageException 檢核錯誤時拋出
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateCheckItemTypes(
            String requireSid,
            List<RequireCheckItemType> afterCheckItemTypes,
            boolean addTrace,
            Integer execUserSid,
            Date execDate) throws UserMessageException {

        if (WkStringUtils.isEmpty(afterCheckItemTypes)) {
            throw new UserMessageException("請輸入系統別!", InfomationLevel.WARN);
        }

        // ====================================
        // 查詢異動前的檢查項目
        // ====================================
        // 查詢
        List<RequireCheckItemType> beforCheckItemTypes = this.findCheckItemTypesByRequireSid(requireSid);

        // 檢查異動前後相同
        if (WkCommonUtils.compare(beforCheckItemTypes, afterCheckItemTypes)) {

            List<String> itemsDescr = afterCheckItemTypes.stream()
                    .map(each -> each.getDescr())
                    .collect(Collectors.toList());

            log.debug("無需異動檢查項目:{}", String.join("、", itemsDescr));
            return;
        }

        // ====================================
        // 計時 start
        // ====================================
        ProcessLog processLog = new ProcessLog(requireSid, "", "異動系統別");

        // ====================================
        // update require
        // ====================================
        this.saveCheckItemTypes(requireSid, afterCheckItemTypes, execUserSid, execDate);

        // ====================================
        // REQ-1671 【系統通知】新增通知類別『異動系統別 (for GM)』
        // ====================================
        // 發送系統通知
        this.sysNotifyHelper.processForConfirmItemChange(
                requireSid,
                beforCheckItemTypes,
                afterCheckItemTypes,
                execUserSid);

        // ====================================
        // 不寫追蹤
        // ====================================
        if (!addTrace) {
            return;
        }

        // ====================================
        // 寫追蹤 insert trace
        // ====================================
        String traceContent = this.requireTraceService.createChangeCheckItemTypes(
                requireSid,
                beforCheckItemTypes,
                afterCheckItemTypes,
                WkUserCache.getInstance().findBySid(execUserSid),
                execDate);

        // log
        log.info(processLog.prepareCompleteLog(traceContent));

    }

    /**
     * 再檢查確認前，檢查使用者權限
     * 
     * @param requireSid        需求單sid
     * @param execUserSid       執行者 user sid
     * @param confirmCheckItems 要檢查的項目
     * @return 有錯誤時，回傳檢核說明
     */
    public String verifyUserRightWhenCheckConfirm(
            String requireSid,
            Integer execUserSid,
            List<RequireCheckItemType> confirmCheckItems) {

        if (WkStringUtils.isEmpty(confirmCheckItems)) {
            return "未選擇檢查項目!";
        }

        // 查詢 executor 可以檢查的項目 (已經被檢查的項目不會查出來)
        List<RequireCheckItemType> userCanCheckItemTypes = this.findUserCanCheckItemTypeByRequireSid(
                requireSid,
                execUserSid);

        // 『要檢查的項目』 - 『可檢查的項目』 = 『要檢查但無權限』的項目
        List<RequireCheckItemType> noRightCheckItems = Lists.newArrayList(confirmCheckItems);
        noRightCheckItems.removeAll(userCanCheckItemTypes);

        // 跳出警示訊息
        if (WkStringUtils.notEmpty(noRightCheckItems)) {
            String itemNames = noRightCheckItems.stream()
                    .map(RequireCheckItemType::getDescr)
                    .collect(Collectors.joining("、"));
            log.warn("[{}]無檢查權限, 可能原因為未設定檢查人員，或該項目已檢查完成", itemNames);
            return WkMessage.NEED_RELOAD
                    + "<br/>"
                    + "<br/>"
                    + "無[" + itemNames + "]檢查權限, 可能原因為未設定為檢查人員，或該項目已檢查完成";
        }

        return "";
    }

    /**
     * 清除檢查資訊
     */
    public void clearCheckInfo(RequireCheckItem requireCheckItem) {
        requireCheckItem.setCheckUserSid(null);
        requireCheckItem.setCheckDate(null);
        this.requireCheckItemRepository.save(requireCheckItem);
    }

    /**
     * 檢查資訊
     * 
     * @param requireSid        需求單 SID
     * @param reqCateType       內外部需求型態
     * @param requireStatusType 製作進度
     * @param requireCheckItems 檢查項目
     * @param canCheckItems     可檢查項目
     * @return 檢查資訊文字 (html)
     */
    public CheckItmesInfoByRequireTo prepareCheckItemsInfo(
            String requireSid,
            ReqCateType reqCateType,
            RequireStatusType requireStatusType,
            List<RequireCheckItem> requireCheckItems,
            List<RequireCheckItemType> canCheckItems) {

        CheckItmesInfoByRequireTo checkItmesInfoByRequireTo = new CheckItmesInfoByRequireTo();

        // ====================================
        // 防呆
        // ====================================
        if (WkStringUtils.isEmpty(requireSid)) {
            log.warn("傳入 SID 為空！");
            return checkItmesInfoByRequireTo;
        }

        if (reqCateType == null || requireStatusType == null) {
            log.warn("傳入需求單狀態有誤！requireSid:[{}],reqCateType:[{}],requireStatusType:[{}] ",
                    requireSid,
                    reqCateType,
                    requireStatusType);
            return checkItmesInfoByRequireTo;
        }

        if (WkStringUtils.isEmpty(requireCheckItems)) {
            log.warn("傳入需求單狀態有誤，檢查項目為空！requireSid:[{}],reqCateType:[{}],requireStatusType:[{}] ",
                    requireSid,
                    reqCateType,
                    requireStatusType);
            return checkItmesInfoByRequireTo;
        }

        // 排序檢查項目
        requireCheckItems = requireCheckItems.stream()
                .sorted(Comparator.comparing(each -> each.getCheckItemType().ordinal()))
                .collect(Collectors.toList());

        // 準備一組非停用的
        List<RequireCheckItemType> acticeRequireCheckItems = requireCheckItems.stream()
                .filter(RequireCheckItem::isActive)
                .map(RequireCheckItem::getCheckItemType)
                .collect(Collectors.toList());

        // ====================================
        // 【規則.1】為內部需求時，不顯示檢查資訊
        // ====================================
        if (ReqCateType.INTERNAL.equals(reqCateType)) {

            // 檢查資訊
            checkItmesInfoByRequireTo.setCheckConfirmInfo(
                    WkHtmlUtils.addClass(
                            CheckItemStatus.INTERNAL.getDescr(),
                            CheckItemStatus.INTERNAL.getCssClass()));

            // 單據檢視畫面資訊
            checkItmesInfoByRequireTo.setCheckItemsInfoForFormView(
                    acticeRequireCheckItems.stream()
                            .map(each -> each.getDescr())
                            .collect(Collectors.joining("、")));

            // 非停用項目
            checkItmesInfoByRequireTo.setCheckItemTypes(acticeRequireCheckItems);

            return checkItmesInfoByRequireTo;
        }

        // ====================================
        // 【規則.2】 流程是否位於檢查確認之前 -> 不顯示顯查字串
        // ====================================
        if (requireStatusType.isFlowBeforeWaitCheck()) {

            // 單據檢視畫面資訊
            checkItmesInfoByRequireTo.setCheckItemsInfoForFormView(
                    acticeRequireCheckItems.stream()
                            .map(each -> each.getDescr())
                            .collect(Collectors.joining("、")));

            // 非停用項目
            checkItmesInfoByRequireTo.setCheckItemTypes(acticeRequireCheckItems);

            return checkItmesInfoByRequireTo;
        }

        // ====================================
        // 【規則.3】 製作進度 = 檢查確認
        // ====================================
        if (RequireStatusType.WAIT_CHECK.equals(requireStatusType)) {
            return this.prepareCheckItemsInfo4WaitCheck(requireCheckItems, canCheckItems);
        }

        // ====================================
        // 【規則.4】 其他狀態顯示
        // ====================================
        return this.prepareCheckItemsInfo4Other(requireCheckItems, canCheckItems);

    }

    private CheckItmesInfoByRequireTo prepareCheckItemsInfo4WaitCheck(
            List<RequireCheckItem> requireCheckItems,
            List<RequireCheckItemType> canCheckItems) {

        CheckItmesInfoByRequireTo checkItmesInfoByRequireTo = new CheckItmesInfoByRequireTo();

        // 逐筆處理
        List<String> checkItemsInfoForFormViews = Lists.newArrayList();
        List<String> checkConfirmInfos = Lists.newArrayList();

        boolean isHasWaitCheckItem = false;

        for (RequireCheckItem currRequireCheckItem : requireCheckItems) {

            // ====================================
            // 待檢查確認使用時，不顯示已被移除的項目
            // ====================================
            if (!currRequireCheckItem.isActive()) {
                continue;
            }

            // ====================================
            //
            // ====================================
            if (currRequireCheckItem.getCheckDate() == null) {
                isHasWaitCheckItem = true;
            }

            // ====================================
            // 檢查項目
            // ====================================
            RequireCheckItemType checkItemType = currRequireCheckItem.getCheckItemType();
            checkItmesInfoByRequireTo.getCheckItemTypes().add(checkItemType);

            // ====================================
            // 檢查人員
            // ====================================
            Integer checkUserSid = currRequireCheckItem.getCheckUserSid();
            if (currRequireCheckItem.getCheckDate() != null) {
                checkItmesInfoByRequireTo.getCheckConfirmUserSids().add(checkUserSid);
            }

            // ====================================
            // 檢查資訊、項目資訊
            // ====================================
            // ------------------
            // 項目名稱
            // ------------------
            // 檢查資訊
            String checkConfirmInfo = checkItemType.getDescr() + "&nbsp;";
            // 檢查項目資訊
            String checkItemsInfoForFormView = checkItemType.getDescr() + "&nbsp;";

            // ------------------
            // 檢查狀態
            // ------------------
            // 判斷檢查項目狀態
            CheckItemStatus checkItemStatus = null;
            // 【已檢查】
            if (currRequireCheckItem.getCheckDate() != null) {
                checkItemStatus = CheckItemStatus.CHECKED;
            }
            // 尚未檢查
            else {
                // 【非項目檢查人員】：當項目未檢查，且登入者無檢查項目權限
                if (!canCheckItems.contains(checkItemType)) {
                    checkItemStatus = CheckItemStatus.NO_RIGHT;
                }
                // 【待檢查】
                else {
                    checkItemStatus = CheckItemStatus.WAIT_CHECK;
                }
            }
            String currCheckConfirmInfo = WkHtmlUtils.addClass(checkItemStatus.getDescr(), checkItemStatus.getCssClass());
            checkConfirmInfo += currCheckConfirmInfo;
            checkItemsInfoForFormView += currCheckConfirmInfo;

            // ------------------
            // 檢查資訊
            // ------------------
            if (currRequireCheckItem.getCheckDate() != null) {
                // 檢查時間
                checkConfirmInfo += "&nbsp;" + WkDateUtils.formatDate(
                        currRequireCheckItem.getCheckDate(),
                        WkDateUtils.YYYY_MM_DD_HH24_mm_ss);
                // 檢查人員
                checkConfirmInfo += "&nbsp;(" + WkUserUtils.findNameBySid(checkUserSid) + ")";
            }

            checkConfirmInfos.add(checkConfirmInfo);

            checkItemsInfoForFormViews.add(checkItemsInfoForFormView);
        }

        checkItmesInfoByRequireTo.setCheckConfirmInfo(String.join("<br/>", checkConfirmInfos));
        checkItmesInfoByRequireTo.setCheckItemsInfoForFormView(String.join("、", checkItemsInfoForFormViews));
        // 註記異常資料(項目都檢查完了, 但是製作進度還留在待檢查)
        checkItmesInfoByRequireTo.setExceptionCase(!isHasWaitCheckItem);

        return checkItmesInfoByRequireTo;
    }

    private CheckItmesInfoByRequireTo prepareCheckItemsInfo4Other(
            List<RequireCheckItem> requireCheckItems,
            List<RequireCheckItemType> canCheckItems) {

        CheckItmesInfoByRequireTo checkItmesInfoByRequireTo = new CheckItmesInfoByRequireTo();

        List<String> checkConfirmInfos = Lists.newArrayList();

        for (RequireCheckItem currRequireCheckItem : requireCheckItems) {

            // ====================================
            // 檢查項目
            // ====================================
            RequireCheckItemType checkItemType = currRequireCheckItem.getCheckItemType();
            // 收集非停用項目
            if (currRequireCheckItem.isActive()) {
                checkItmesInfoByRequireTo.getCheckItemTypes().add(checkItemType);
            }

            // ====================================
            // 檢查人員 (不分停用項目，有檢查過就算)
            // ====================================
            Integer checkUserSid = currRequireCheckItem.getCheckUserSid();
            if (currRequireCheckItem.getCheckDate() != null) {
                checkItmesInfoByRequireTo.getCheckConfirmUserSids().add(checkUserSid);
            }

            // ====================================
            // 檢查資訊、項目資訊
            // ====================================
            // ------------------
            // 項目名稱
            // ------------------
            // 檢查資訊
            String checkConfirmInfo = checkItemType.getDescr() + "&nbsp;";

            // ------------------
            // 檢查狀態
            // ------------------
            // 判斷檢查狀態
            CheckItemStatus checkItemStatus = null;
            if (currRequireCheckItem.getCheckDate() != null) {
                if (currRequireCheckItem.isActive()) {
                    // 【已檢查】
                    checkItemStatus = CheckItemStatus.CHECKED;
                } else {
                    // 【已檢查，但項目已移除】
                    checkItemStatus = CheckItemStatus.CHECKED_BUT_REMOVE;
                }
            } else {
                if (currRequireCheckItem.isActive()) {
                    // 【檢查後新增項目，無需檢查】
                    checkItemStatus = CheckItemStatus.ADD_AFTER_CHECK;
                } else {
                    // 已移除項目 , 且沒有檢查記錄 ->略過
                    continue;
                }
            }

            String currCheckConfirmInfo = WkHtmlUtils.addClass(checkItemStatus.getDescr(), checkItemStatus.getCssClass());
            checkConfirmInfo += currCheckConfirmInfo;

            // ------------------
            // 檢查資訊
            // ------------------
            if (currRequireCheckItem.getCheckDate() != null) {
                // 檢查時間
                checkConfirmInfo += "&nbsp;" + WkDateUtils.formatDate(
                        currRequireCheckItem.getCheckDate(),
                        WkDateUtils.YYYY_MM_DD_HH24_mm_ss);
                // 檢查人員
                checkConfirmInfo += "&nbsp;(" + WkUserUtils.findNameBySid(checkUserSid) + ")";
            }

            checkConfirmInfos.add(checkConfirmInfo);
        }

        checkItmesInfoByRequireTo.setCheckConfirmInfo(String.join("<br/>", checkConfirmInfos));
        checkItmesInfoByRequireTo.setCheckItemsInfoForFormView(
                String.join("、",
                        checkItmesInfoByRequireTo.getCheckItemTypes()
                                .stream()
                                .map(each -> each.getDescr())
                                .collect(Collectors.toList())));

        return checkItmesInfoByRequireTo;
    }

    /**
     * 取得單據已檢查的項目
     * 
     * @param requireSids
     * @return 整裡後的結構為：Map[require, List[RequireCheckItemType]]
     */
    public Map<String, List<RequireCheckItemType>> prepareCheckedItemsByRequireSid(
            List<String> requireSids) {

        // ====================================
        // 查詢已檢查的項目
        // ====================================
        List<RequireCheckItem> checkedItems = this.requireCheckItemRepository.findByRequireSidInAndCheckDateNotNull(requireSids);

        if (WkStringUtils.isEmpty(checkedItems)) {
            Maps.newHashMap();
        }

        // ====================================
        // 整裡資料
        // ====================================
        // 整裡後的結構為：Map<require, List<RequireCheckItemType>>
        return checkedItems.stream()
                .collect(Collectors.groupingBy(
                        RequireCheckItem::getRequireSid,
                        Collectors.mapping(
                                RequireCheckItem::getCheckItemType,
                                Collectors.toList())));
    }

}
