/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.vo;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * 自訂物件 - 客戶資料
 *
 * @author kasim
 */
@EqualsAndHashCode(of = {"sid"})
public class CustomerTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4308000688588859321L;
    @Getter
    private Long sid;
    @Getter
    private String alias;
    @Getter
    private String name;
    @Getter
    private String loginCode;
    @Getter
    private String customerTypeName;

    public CustomerTo(Long sid) {
        this.sid = sid;
    }

    public CustomerTo(Long sid, String alias, String name, String loginCode, String customerTypeName) {
        this.sid = sid;
        this.alias = alias;
        this.name = name;
        this.loginCode = loginCode;
        this.customerTypeName = customerTypeName;
    }

}
