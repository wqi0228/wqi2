/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.vo;

import com.cy.tech.request.vo.enums.OthSetStatus;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */
public class TrOsVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5632046125494974039L;
    public TrOsVO(String os_sid, String os_no,
            String require_sid, String require_no,
            String theme, String content,
            String contentCss, String note,
            String noteCss, Date finishDate,
            Date cancelDate, OthSetStatus osStatus,
            List<String> noticeDepSids, List<TrOsHistoryVO> trOsHistoryVOs,
            List<TrOsAttachmentVO> trOsAttachmentVOs, Integer create_usr,
            Date create_dt, Integer update_usr, Date update_dt,Integer dep_Sid) {
        this.os_sid = os_sid;
        this.os_no = os_no;
        this.require_sid = require_sid;
        this.require_no = require_no;
        this.theme = theme;
        this.content = content;
        this.contentCss = contentCss;
        this.note = note;
        this.noteCss = noteCss;
        this.finishDate = finishDate;
        this.cancelDate = cancelDate;
        this.osStatus = osStatus;
        this.noticeDepSids = noticeDepSids;
        this.trOsHistoryVOs = trOsHistoryVOs;
        this.trOsAttachmentVOs = trOsAttachmentVOs;
        this.create_usr = create_usr;
        this.create_dt = create_dt;
        this.update_usr = update_usr;
        this.update_dt = update_dt;
        this.dep_Sid = dep_Sid;
    }
    
    @Getter
    private final Integer dep_Sid;
    @Getter
    private final String os_sid;
    @Getter
    private final String os_no;
    @Getter
    private final String require_sid;
    @Getter
    private final String require_no;
    @Getter
    private final String theme;
    @Getter
    private final String content;
    @Getter
    private final String contentCss;
    @Getter
    private final String note;
    @Getter
    private final String noteCss;
    @Getter
    private final Date finishDate;
    @Getter
    private final Date cancelDate;
    @Getter
    private final OthSetStatus osStatus;
    @Getter
    private final List<String> noticeDepSids;
    @Getter
    private final List<TrOsHistoryVO> trOsHistoryVOs;
    @Getter
    private final List<TrOsAttachmentVO> trOsAttachmentVOs;
    @Getter
    private final Integer create_usr;
    @Getter
    private final Date create_dt;
    @Getter
    private final Integer update_usr;
    @Getter
    private final Date update_dt;
}
