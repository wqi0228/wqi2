package com.cy.tech.request.logic.service;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.tech.request.repository.notify.NotificationEventRepository;
import com.cy.tech.request.vo.enums.NotificationEventType;
import com.cy.tech.request.vo.require.notify.NotificationEvent;
import com.cy.tech.request.vo.value.to.NotificationEventVO;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.vo.worktest.enums.WorkTestInfoStatus;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkDateUtils;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author aken_kao
 *
 */
@Slf4j
@Service
public class NotificationService {
    @Autowired
    private NotificationEventRepository notificationEventRepository;
    @Autowired
    private URLService urlService;
	@Autowired
	private transient WorkBackendParamHelper workBackendParamHelper;
    
    @Transactional(rollbackFor = Exception.class)
    public void notifyQaUsersByStEvent(List<WorkTestInfo> sourceNoList, NotificationEventType eventType, Integer loginUserSid) {
        
        
        // QA加入排程, 才發送通知
        sourceNoList.removeIf(w -> !WorkTestInfoStatus.JOIN_SCHEDULE.equals(w.getQaScheduleStatus()));
        
        if (CollectionUtils.isNotEmpty(sourceNoList)) {
        	Set<Integer> qaUsers = workBackendParamHelper.getQAUserSids();
            this.notifyUsersByStEvent(
            		sourceNoList, 
            		qaUsers, 
            		eventType, 
            		loginUserSid);
        }
    }
    
    /**
     * 送測單事件通知使用者
     * @param sourceNoList
     * @param userSids
     * @param eventType
     * @param loginUserSid
     */
    @Transactional(rollbackFor = Exception.class)
    public void notifyUsersByStEvent(List<WorkTestInfo> sourceNoList, Set<Integer> userSids, NotificationEventType eventType, Integer loginUserSid) {
        Date currentDate = new Date();
        for(WorkTestInfo info : sourceNoList) {
            List<NotificationEvent> entities = Lists.newArrayList();
            for(Integer userSid : userSids) {
                NotificationEvent entity = new NotificationEvent();
                entity.setReceiver(userSid);
                entity.setEventType(eventType);
                entity.setEventDesc(getSendTestEventDesc(eventType, info));
                entity.setSourceNo(info.getTestinfoNo());
                entity.setSourceTheme(info.getTheme());
                entity.setCreatedDate(currentDate);
                entity.setCreatedUser(loginUserSid);
                
                entities.add(entity);
            }
            
            notificationEventRepository.save(entities);
        }
    }
    
    /**
     * 標示為已閱讀
     * @param userSid
     */
    @Transactional(rollbackFor = Exception.class)
    public void markRead(List<NotificationEventVO> list) {
        for(NotificationEventVO vo : list) {
            NotificationEvent entity = notificationEventRepository.findOne(vo.getSid());
            notificationEventRepository.delete(entity);
        }
    }
    
    /**
     * 查詢通知 by 個人
     * @param userSid
     */
    public List<NotificationEventVO> findAllByUserSid(Integer userSid) {
        return convert(notificationEventRepository.findAllByUserSid(userSid));
    }
    
    private List<NotificationEventVO> convert(List<NotificationEvent> list) {
        List<NotificationEventVO> resultList = Lists.newArrayList();
        for(NotificationEvent entity : list) {
            try {
                NotificationEventVO vo = new NotificationEventVO();
                vo.setSid(entity.getSid());
                vo.setEventType(entity.getEventType());
                vo.setEventDesc(entity.getEventDesc());
                vo.setCreatedDate(entity.getCreatedDate());
                vo.setCreatedUser(WkUserCache.getInstance().findBySid(entity.getCreatedUser()));
                vo.setSourceNo(entity.getSourceNo());
                vo.setSourceTheme(entity.getSourceTheme());
                vo.setUrl(urlService.createWorkTestInfoUrl(entity.getSourceNo()));
                
                resultList.add(vo);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return resultList;
    }
    
    /**
     * 依事件類型取得描述
     * @param eventType
     * @param info
     * @return
     */
    private String getSendTestEventDesc(NotificationEventType eventType, WorkTestInfo info) {
        switch(eventType) {
        case UPDATE_ONLINE_DATE:
        case BATCH_UP_OL_DATE:
            String dateStr = "空值";
            if(info.getExpectOnlineDate() != null) {
                dateStr =  WkDateUtils.YYYY_MM_DD2.print(info.getExpectOnlineDate().getTime());
            }
            return String.format(eventType.getFormat(), dateStr);
        case UPDATE_ESTABLISH_DATE:
        case BATCH_UP_ES_DATE:
            dateStr =  WkDateUtils.YYYY_MM_DD2.print(info.getEstablishDate().getTime());
            return String.format(eventType.getFormat(), dateStr);
        case UPDATE_THTEM:
        case UPDATE_CONTENT:
        case UPDATE_NOTE:
            return eventType.getFormat();
        case UPDATE_FB_NO:
            String fbId = "空值";
            if (info.getFbId() != null) {
                fbId = String.valueOf(info.getFbId());
            }
            return String.format(eventType.getFormat(), fbId);
        default:
            break;
        }
        return "";
    }
    
    /**
     * 例行排程housekeeping 移除停用帳號通知資料
     * 最新通知
     */
    @Transactional(rollbackFor = Exception.class)
    public void housekeeping() {
        List<Integer> userSids = notificationEventRepository.findDistinctReceiver();
        List<User> users = WkUserCache.getInstance().findBySids(userSids);         
        users.removeIf(u -> u.getStatus().equals(Activation.ACTIVE));
        // 使用者停用-移除通知
        for(User user : users) {
            List<NotificationEvent> entities = notificationEventRepository.findByReceiver(user.getSid());
            notificationEventRepository.delete(entities);
            log.info("清除需求單最新通知, 已離職員工:{}", user.getId());
        }
    }
}
