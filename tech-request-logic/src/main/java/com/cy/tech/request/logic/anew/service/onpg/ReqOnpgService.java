/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.anew.service.onpg;

import com.cy.tech.request.repository.anew.onpg.ReqWorkOnpgRepo;
import com.cy.tech.request.vo.anew.onpg.ReqWorkOnpg;
import com.google.common.base.Preconditions;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ON程式 服務
 *
 * @author kasim
 */
@Component
public class ReqOnpgService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 9012221137649053247L;
    @Autowired
    private ReqWorkOnpgRepo onpgDao;

    /**
     * 依據sids 查詢
     *
     * @param sids
     * @return
     */
    public List<ReqWorkOnpg> findBySourceSidIn(List<String> sourceSids) {
        Preconditions.checkState(sourceSids != null && !sourceSids.isEmpty(), "參數不可為空!!");
        return onpgDao.findBySourceSidIn(sourceSids);
    }

    /**
     * 依據sids 查詢
     *
     * @param sids
     * @return
     */
    public List<ReqWorkOnpg> findByOnpgNoIn(List<String> onpgNos) {
        Preconditions.checkState(onpgNos != null && !onpgNos.isEmpty(), "參數不可為空!!");
        return onpgDao.findByOnpgNoIn(onpgNos);
    }
}
