package com.cy.tech.request.logic.service.helper;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.bpm.rest.client.BpmSettingClient;
import com.cy.bpm.rest.client.ProcessClient;
import com.cy.bpm.rest.client.to.BrSignCompLevelSettingTo;
import com.cy.bpm.rest.to.RoleTo;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.BpmService;
import com.cy.tech.request.logic.service.ReqUnitBpmService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.SimpleCategoryService;
import com.cy.tech.request.logic.service.setting.SettingSpecSignGroupManager;
import com.cy.tech.request.logic.utils.ProcessLog;
import com.cy.tech.request.logic.vo.BpmCreateTo;
import com.cy.tech.request.logic.vo.BpmCreateTo.RequireFlowType;
import com.cy.tech.request.vo.anew.enums.SignLevelType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireUnitSignInfo;
import com.cy.tech.request.vo.require.setting.SettingSpecSignMode;
import com.cy.tech.request.vo.setting.SettingSpecSignGroup;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Service
@Slf4j
public class RequireCreateUnitSignFlowHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2079118766151187384L;
    // ========================================================================
    // 服務元件區
    // ========================================================================
    @Autowired
    private transient RequireService requireService;
    @Autowired
    private transient SimpleCategoryService simpleCategoryService;
    @Autowired
    private transient SettingSpecSignGroupManager settingSpecSignGroupManager;
    @Autowired
    private transient BpmService bpmService;
    @Autowired
    transient private ProcessClient bpmProcessClient;
    @Autowired
    transient private ReqUnitBpmService reqUnitBpmService;

    /**
     * BPM特殊設定Client
     */
    @Autowired
    private BpmSettingClient bpmSettingClient;

    // ========================================================================
    // 私有 列舉
    // ========================================================================
    private enum ReqUnitflowType {
        /** 直達終點 (僅申請人簽名) */
        TO_END,
        /** 指定簽名（一人） */
        SIGNER01,
        /** 指定簽名（二人） */
        SIGNER02,
        /** 指定簽名（三人） */
        SIGNER03,
        /** 層簽 */
        ORG_LEVEL
    }

    private enum ReqUnitflowParam {
        /** 流程走向 */
        flowType,
        /** 層簽的最高簽核層級 */
        sign_lv,
        /** 指定簽名者 01 */
        signer01RoleID,
        /** 指定簽名者 02 */
        signer02RoleID,
        /** 指定簽名者 03 */
        signer03RoleID
    }

    // ========================================================================
    // 主要方法區
    // ========================================================================
    /**
     * 執行建立需求單位簽核流程
     * 
     * @param requireSid
     * @param execUser
     * @param execDate
     * @throws UserMessageException
     * @throws SystemOperationException
     */
    @Transactional(rollbackFor = Exception.class)
    public Require process(
            String requireSid,
            Date execDate) throws UserMessageException, SystemOperationException {

        // ====================================
        // 查詢主檔
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);

        // 防呆
        if (require == null) {
            log.warn("找不到需求單主檔資料 requireSid:[{}]", requireSid);
            throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }

        // 防呆
        if (require.getCreateDep() == null) {
            log.warn("找不到需求單建立部門(dep_sid)資料 requireSid:[{}]", requireSid);
            throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }

        // 防呆
        if (require.getCreatedUser() == null) {
            log.warn("找不到需求單建立人員(create_usr)資料 requireSid:[{}]", requireSid);
            throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }

        // ====================================
        // 判斷無需此次無需建立 BPM 流程
        // ====================================
        // 已存在層簽 => PASS
        if (require.getReqUnitSign() != null) {
            return require;
        }

        // 排除還無需建立流程的狀態
        // 只有以下狀態為『待建立層簽』階段
        // 1.null : 一般提交
        // 2.DRAFT：草稿提交
        // 3.NEW_INSTANCE ：TODO 待確認
        List<RequireStatusType> status = Lists.newArrayList(RequireStatusType.DRAFT, RequireStatusType.NEW_INSTANCE);
        if (require.getRequireStatus() == null ||
                !status.contains(require.getRequireStatus())) {
            return require;
        }

        // ====================================
        // log
        // ====================================
        ProcessLog processLog = new ProcessLog(require.getRequireNo(), "", "建立需求單位簽核流程");
        log.info(processLog.prepareStartLog());

        // ====================================
        // 取得單據簽核層級
        // ====================================
        // 查詢
        SignLevelType formSignLevelType = this.simpleCategoryService.findSignLevelBySid(
                require.getMapping().getSmall().getSid());

        // 防呆
        if (formSignLevelType == null) {
            String errorMessage = WkMessage.EXECTION + "單據未設定簽核層級！smallSid[" + require.getMapping().getSmall().getSid() + "]";
            log.info(processLog.prepareProcessLog(errorMessage));
            throw new SystemOperationException(errorMessage, InfomationLevel.ERROR);
        }

        // 此單據類別無需簽核 => PASS
        if (SignLevelType.NO_SIGN.equals(formSignLevelType)) {
            log.info(processLog.prepareProcessLog("特殊單據規則：免需求單位簽核"));
            return require;
        }

        // ====================================
        // 取得特殊簽核設定
        // ====================================
        // 查詢
        SettingSpecSignGroup settingSpecSignGroup = this.settingSpecSignGroupManager.findGroupByDepSid(
                require.getCreateDep().getSid());

        // 該單位免簽
        // 1.判斷該部門有無特殊設定
        // 2.模式為限制最高層級
        // 3.層級為免簽
        if (settingSpecSignGroup != null) {
            // 簽核模式
            if (SettingSpecSignMode.SIGN_LEVEL_LIMIT.equals(settingSpecSignGroup.getSpecSignMode())) {
                // 簽核層級
                if (SignLevelType.NO_SIGN.equals(settingSpecSignGroup.getSignLevel())) {
                    String depName = require.getCreateDep().getSid() + " " + WkOrgUtils.getOrgName(require.getCreateDep());
                    log.info(processLog.prepareProcessLog("單位特殊規則：[" + depName + "] 免需求單位簽核"));
                    return require;
                }
            }
        }

        // ====================================
        // 取得申請者BPM 相關資料
        // ====================================
        // 取得申請者簽核流程(自己向上簽核順序)
        List<RoleTo> creatorUserSignFlowNodes = this.bpmService.prepareUserSignFlow(require.getCreatedUser().getSid());
        if (WkStringUtils.isEmpty(creatorUserSignFlowNodes)) {
            String errorMessage = WkMessage.EXECTION + "BPM 錯誤，找不到使用者的角色！UserID:[" + require.getCreatedUser().getId() + "]";
            log.info(processLog.prepareProcessLog(errorMessage));
            throw new SystemOperationException(errorMessage, InfomationLevel.ERROR);
        }

        // 自己的角色
        RoleTo creatorRoleTo = creatorUserSignFlowNodes.get(0);

        // ====================================
        // 判斷流程走向，並準備簽核流程參數
        // ====================================
        Map<String, Object> bpmParams = this.prepareBpmParamaters(
                formSignLevelType,
                settingSpecSignGroup,
                require.getCreatedUser(),
                require.getCreateDep(),
                processLog);

        // ====================================
        // 建立 BPM 簽核流程
        // ====================================
        // 建立簽核流程, 並取得 bpmInstanceId
        String bpmInstanceId = "";
        try {
            BpmCreateTo to = new BpmCreateTo(
                    require.getRequireNo(),
                    require.getCreatedUser().getId(),
                    creatorRoleTo.getId(),
                    RequireFlowType.REQUIRE_UNIT,
                    bpmParams);

            log.info(processLog.prepareProcessLog("\r\n開始建立BPM流程, 參數：【" + WkJsonUtils.getInstance().toJsonWithOutPettyJson(bpmParams) + "】"));

            bpmInstanceId = bpmProcessClient.create(to);

            log.info("bpmInstanceId:[{}]", bpmInstanceId);

        } catch (Exception e) {
            String errorMessage = WkMessage.EXECTION + "建立簽核流程失敗!" + e.getMessage();
            log.error(errorMessage, e);
            throw new SystemOperationException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 建立 RequireUnitSignInfo (insert tr_require_manager_sign_info)
        // ====================================
        // 建立物件後, 下面一起存檔
        RequireUnitSignInfo rmsi = new RequireUnitSignInfo();
        rmsi.setInstanceStatus(InstanceStatus.NEW_INSTANCE);
        rmsi.setRequire(this.requireService.findByReqSid(requireSid));
        rmsi.setRequireNo(require.getRequireNo());
        rmsi.setBpmInstanceId(bpmInstanceId);

        // 更新水管圖資訊
        try {
            this.reqUnitBpmService.setupReqUnitSignInfo(rmsi);
        } catch (ProcessRestException e) {
            String errorMessage = WkMessage.EXECTION + "更新水管圖資訊失敗!" + e.getMessage();
            log.error(errorMessage, e);
            throw new SystemOperationException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 更新主檔狀態 update tr_require
        // ====================================
        this.requireService.updateHasReqUnitSignAndRequireStatus(
                requireSid,
                true,
                rmsi,
                RequireStatusType.NEW_INSTANCE);

        return this.requireService.findByReqSid(requireSid);

    }

    // ========================================================================
    // 計算簽核流程
    // ========================================================================
    /**
     * 準備需求單位簽核流程BPM參數
     * 
     * @param formSignLevelType    單據設定的簽核層級
     * @param settingSpecSignGroup 特殊簽核流程設定檔
     * @param creator              建單者
     * @param createDep            建單單位
     * @param processLog           processLog
     * @return BPM 流程參數
     * @throws SystemOperationException 檢核錯誤時拋出
     */
    private Map<String, Object> prepareBpmParamaters(
            SignLevelType formSignLevelType,
            SettingSpecSignGroup settingSpecSignGroup,
            User creator,
            Org createDep,
            ProcessLog processLog) throws SystemOperationException {

        // ====================================
        // 取得申請者簽核流程(簽核角色順序)
        // ====================================
        List<RoleTo> creatorUserSignFlowNodes = this.bpmService.prepareUserSignFlow(creator.getSid());
        if (WkStringUtils.isEmpty(creatorUserSignFlowNodes)) {
            String errorMessage = WkMessage.EXECTION + "BPM 錯誤，找不到使用者的角色！UserID:[" + creator.getId() + "]";
            log.info(processLog.prepareProcessLog(errorMessage));
            throw new SystemOperationException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 非特殊設定單位，走一般簽核流程
        // ====================================
        if (settingSpecSignGroup == null) {
            return this.prepareReqUnitSignBPMParams_OrgLevel(
                    creatorUserSignFlowNodes,
                    formSignLevelType,
                    createDep.getCompany().getId(),
                    processLog);
        }

        // ====================================
        // 特殊設定單位：限制最高簽核層級
        // ====================================
        else if (SettingSpecSignMode.SIGN_LEVEL_LIMIT.equals(settingSpecSignGroup.getSpecSignMode())) {
            return this.prepareReqUnitSignBPMParams_SignLevelLimit(
                    creatorUserSignFlowNodes,
                    formSignLevelType,
                    settingSpecSignGroup.getSignLevel(),
                    createDep.getCompany().getId(),
                    processLog);
        }

        // ====================================
        // 特殊設定單位：僅簽一層
        // ====================================
        else if (SettingSpecSignMode.UP_ONE_LEVEL.equals(settingSpecSignGroup.getSpecSignMode())) {
            return this.prepareReqUnitSignBPMParams_UpOneLevel(
                    creatorUserSignFlowNodes,
                    formSignLevelType,
                    createDep.getCompany().getId(),
                    processLog);
        }

        // ====================================
        // 特殊設定單位：指定簽核角色
        // ====================================
        else if (SettingSpecSignMode.SPEC_SINGER.equals(settingSpecSignGroup.getSpecSignMode())) {
            return this.prepareReqUnitSignBPMParams_SepcSigner(
                    settingSpecSignGroup.getSignerRoleIds(),
                    creator.getId(),
                    createDep,
                    processLog);
        }

        // ====================================
        // 防呆：沒有實做的模式
        // ====================================
        else {
            log.error(WkMessage.DEV_NOT_IMPL + "沒有定義的 SettingSpecSignMode 特殊簽核模式[" + settingSpecSignGroup.getSpecSignMode() + "]");
            throw new SystemDevelopException(WkMessage.EXECTION, InfomationLevel.ERROR);
        }

    }

    /**
     * 準備需求單位簽核流程BPM參數 - 僅申請人簽名
     * 
     * @return
     */
    private Map<String, Object> prepareBpmParamaters_OnlyApplicant() {
        Map<String, Object> params = Maps.newHashMap();
        params.put(ReqUnitflowParam.flowType.name(), ReqUnitflowType.TO_END.name());
        return params;
    }

    /**
     * 準備需求單位簽核流程參數<br/>
     * 層簽
     * 
     * @param creatorUserSignFlowNodes 建立者的簽核流程
     * @param formSignLevelType        單據設定的簽核層級
     * @param compId                   公司別
     * @param processLog               processLog
     * @return BPM 流程參數
     * @throws SystemOperationException
     */
    private Map<String, Object> prepareReqUnitSignBPMParams_OrgLevel(
            final List<RoleTo> creatorUserSignFlowNodes,
            SignLevelType formSignLevelType,
            String compId,
            ProcessLog processLog) throws SystemOperationException {

        // ====================================
        // 判斷不用層簽
        // ====================================
        // 取得申請者角色
        RoleTo creatorRoleTo = creatorUserSignFlowNodes.get(0);

        // 申請者角色層級已經 高於等於 單據設定的簽核層級 : 不用層簽 (數字小的層級越高)
        // yi-fan 中間部門缺損時，應自動往上一層
        // EX 通訊應用組 -> 研發一處 單據簽核層級為部
        // => 簽核結果 組長 -> 處長
        if (creatorRoleTo.getRoleLevel() <= formSignLevelType.getBpmSignLvNum()) {
            return this.prepareBpmParamaters_OnlyApplicant();
        }

        // ====================================
        // 準備BPM參數
        // ====================================

        Map<String, Object> params = Maps.newHashMap();
        // 簽核路線為層簽
        params.put(ReqUnitflowParam.flowType.name(), ReqUnitflowType.ORG_LEVEL.name());
        // 簽核層級參數

        // REQ-1695 【簽核層級】簽核層級調整 呼叫 bpm-rest 特殊設定
        String sign_lv = this.findUnitBpmLevel(compId, formSignLevelType);

        params.put(ReqUnitflowParam.sign_lv.name(), sign_lv);

        return params;
    }

    /**
     * 取得簽核流程 sign_lv 參數
     * 
     * @param compId            公司別
     * @param formSignLevelType 簽核層級
     * @return
     * @throws SystemOperationException
     */
    private String findUnitBpmLevel(String compId, SignLevelType formSignLevelType) throws SystemOperationException {

        String result = "預設路線-最高至層級";

        try {
            BrSignCompLevelSettingTo brSignCompLevelSettingTo = this.bpmSettingClient.getSignCompLevelSetting(compId);

            switch (formSignLevelType) {
            case THE_PANEL:
                result += brSignCompLevelSettingTo.getPanelLevel();
                break;

            case MINISTERIAL:
                result += brSignCompLevelSettingTo.getMinisterialLevel();
                break;

            case DIVISION_LEVEL:
                result += brSignCompLevelSettingTo.getDivisionLevel();
                break;

            case GROUPS:
                result += brSignCompLevelSettingTo.getGroupLevel();
                break;

            case CEO:
                result += "1";
                break;

            default:
                String errotMessage = "未預期的簽核層級:[" + formSignLevelType + "]";
                log.error(errotMessage);
                throw new SystemOperationException(errotMessage);
            }

        } catch (Exception e) {
            String errotMessage = "取得單位層級失敗!" + e.getMessage();
            log.error(errotMessage, e);
            throw new SystemOperationException(errotMessage);
        }

        return result;
    }

    /**
     * 準備需求單位簽核流程參數<br/>
     * 特殊設定單位：限制最高簽核層級
     * 
     * @param creatorUserSignFlowNodes 建立者的簽核流程
     * @param formSignLevelType        單據設定的簽核層級
     * @param limitSignLevelType       限制的最高簽核層級
     * @param compId                   公司別ID
     * @param processLog               processLog
     * @return BPM 流程參數
     * @throws SystemOperationException 檢核錯誤時拋出
     */
    private Map<String, Object> prepareReqUnitSignBPMParams_SignLevelLimit(
            final List<RoleTo> creatorUserSignFlowNodes,
            SignLevelType formSignLevelType,
            SignLevelType limitSignLevelType,
            String compId,
            ProcessLog processLog) throws SystemOperationException {

        log.info(processLog.prepareProcessLog("進入特殊單據規則判斷:限制最高簽核層級"));

        // ====================================
        // 防呆
        // ====================================
        if (limitSignLevelType == null) {
            String errorMessage = WkMessage.EXECTION + "單位特殊規則：限制最高簽核層級，但設定層級為空!";
            log.info(processLog.prepareProcessLog(errorMessage));
            throw new SystemOperationException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 判斷簽核層級
        // ====================================
        // 預設為單據簽核層級
        SignLevelType signLevelType = formSignLevelType;
        // 當單據簽核層級，高於限制的最高層級，以最高為準
        if (formSignLevelType.getBpmSignLvNum() < limitSignLevelType.getBpmSignLvNum()) {
            signLevelType = limitSignLevelType;
        }

        // ====================================
        // 組層簽參數
        // ====================================
        return this.prepareReqUnitSignBPMParams_OrgLevel(
                creatorUserSignFlowNodes,
                signLevelType,
                compId,
                processLog);
    }

    /**
     * 準備需求單位簽核流程參數<br/>
     * 特殊設定單位：僅簽一層
     * 
     * @param creatorUserSignFlowNodes 建立者的簽核流程
     * @param formSignLevelType        單據設定的簽核層級
     * @param limitSignLevelType       限制的最高簽核層級
     * @param compId                   公司別ID
     * @param processLog               processLog
     * @return BPM 流程參數
     * @throws SystemOperationException 檢核錯誤時拋出
     */
    private Map<String, Object> prepareReqUnitSignBPMParams_UpOneLevel(
            final List<RoleTo> creatorUserSignFlowNodes,
            SignLevelType formSignLevelType,
            String compId,
            ProcessLog processLog) throws SystemOperationException {

        log.info(processLog.prepareProcessLog("進入特殊單據規則判斷:僅簽一層"));

        // ====================================
        // 往上簽核流程中僅有自己時，無需層簽
        // ====================================
        if (creatorUserSignFlowNodes.size() <= 1) {
            return this.prepareBpmParamaters_OnlyApplicant();
        }

        // ====================================
        // 判斷層簽
        // ====================================
        for (int i = 0; i < creatorUserSignFlowNodes.size(); i++) {

            // 第一個為自己, 跳過
            if (i == 0) {
                continue;
            }

            RoleTo roleTo = creatorUserSignFlowNodes.get(i);

            // 判斷角色層級已經高於簽核層級
            // 直接跑無需層簽
            if (roleTo.getRoleLevel() < formSignLevelType.getBpmSignLvNum()) {
                break;
            }

            // 判斷角色層級個位數不為 1 時跳過
            // (副主管或其他BPM特殊設定簽核人員)
            if (roleTo.getRoleLevel() % 10 != 1) {
                continue;
            }

            // 轉換為簽核層級
            SignLevelType signLevelType = SignLevelType.valueOfByBpmSignLvNum(roleTo.getRoleLevel());
            if (signLevelType == null) {
                String errorMessage = WkMessage.EXECTION + "無法處理的簽核層級：[" + roleTo.getRoleLevel() + "]";
                log.info(processLog.prepareProcessLog(errorMessage));
                throw new SystemOperationException(errorMessage, InfomationLevel.ERROR);
            }

            // 一般 case , 找到第二筆即結束 (僅簽一層)
            return this.prepareReqUnitSignBPMParams_OrgLevel(
                    creatorUserSignFlowNodes,
                    signLevelType,
                    compId,
                    processLog);

        }

        // ====================================
        // 無法判斷時，僅簽自己
        // ====================================
        log.warn("特殊設定單位：僅簽一層，但無法判斷流程規則\r\n簽核順序[{}]\r\n單據簽核層級:[{}]",
                WkJsonUtils.getInstance().toPettyJson(creatorUserSignFlowNodes),
                formSignLevelType);

        return this.prepareBpmParamaters_OnlyApplicant();
    }

    /**
     * 準備需求單位簽核流程參數<br/>
     * 特殊設定單位：指定簽核人員
     * 
     * @param signerRoleIds 指定簽核人員BPM角色ID
     * @param creatorUserID 建單者 ID
     * @param createDep     建單單位
     * @param processLog    processLog
     * @return BPM 流程參數
     * @throws SystemOperationException 檢核錯誤時拋出
     */
    private Map<String, Object> prepareReqUnitSignBPMParams_SepcSigner(
            final List<String> signerRoleIds,
            String creatorUserID,
            Org createDep,
            ProcessLog processLog) throws SystemOperationException {

        String depName = createDep.getSid() + " " + WkOrgUtils.getOrgName(createDep);
        String templetErrorMessage = WkMessage.EXECTION + "單位特殊規則：[" + depName + "] ";

        // ====================================
        // 防呆
        // ====================================
        if (WkStringUtils.isEmpty(signerRoleIds)) {
            String errorMessage = templetErrorMessage + "未設定簽核角色";
            log.info(processLog.prepareProcessLog(errorMessage));
            throw new SystemOperationException(errorMessage, InfomationLevel.ERROR);
        }

        String rolesStr = signerRoleIds.stream().collect(Collectors.joining("->"));
        log.info(processLog.prepareProcessLog("進入特殊單據規則判斷:指定簽核人員[" + rolesStr + "]"));

        // ====================================
        // 反轉簽核人員順序
        // ====================================
        // 複製一份避免影響到外層
        List<String> reverseSignerRoleIds = signerRoleIds.stream().collect(Collectors.toList());
        // 反轉-從後面算回來
        Collections.reverse(reverseSignerRoleIds);

        // ====================================
        // 逐筆檢查設定中需要簽名的角色 (由最後一個推算回來)
        // ====================================
        // 判斷命中的角色ID
        List<String> targetSingerRoleIds = Lists.newArrayList();
        List<String> targetSingerIds = Lists.newArrayList();

        for (String singerRoleId : reverseSignerRoleIds) {
            // 查詢符合該角色下的使用者
            List<String> userIDs = this.bpmService.findUsersByRoleID(singerRoleId);

            // 檢查該角色無人符合
            if (WkStringUtils.isEmpty(userIDs)) {
                String errorMessage = templetErrorMessage + "設定簽核角色無符合使用者！roleID:[" + singerRoleId + "]";
                log.info(processLog.prepareProcessLog(errorMessage));
                throw new SystemOperationException(errorMessage, InfomationLevel.ERROR);
            }

            // 檢查該角色有多人符合
            if (userIDs.size() > 1) {
                String errorMessage = templetErrorMessage + "設定簽核角色有多個符合使用者！roleID:[" + singerRoleId + "]";
                log.info(processLog.prepareProcessLog(errorMessage));
                throw new SystemOperationException(errorMessage, InfomationLevel.ERROR);
            }

            // 簽核者
            String currSignerID = userIDs.get(0);
            User signUser = WkUserCache.getInstance().findById(currSignerID);
            if (signUser == null) {
                String errorMessage = templetErrorMessage + "WERP 對應的簽核者！UserID:[" + currSignerID + "]";
                log.info(processLog.prepareProcessLog(errorMessage));
                throw new SystemOperationException(errorMessage, InfomationLevel.ERROR);
            }

            // 比對，當簽核者等於執行者時，後面的簽核節點不用再 append
            // 例如 簽核流程 yi-fan -> Linus -> Aon，申請者為 Linus 時，
            // 第一次比對到 aon ->加入 簽核人員 list
            // 第二次比對到 linus-> 等於申請人，故中斷簽核鍊
            // 結果為僅簽 aon, yi-fan不用簽

            // 比對簽核者為申請者 -> 中斷簽核鍊
            if (WkCommonUtils.compareByStr(creatorUserID, currSignerID)) {
                break;
            }

            // 角色是同一個人，跳過
            if (targetSingerIds.contains(currSignerID)) {
                continue;
            }

            // 將簽核者加入清單
            targetSingerRoleIds.add(singerRoleId);
            targetSingerIds.add(currSignerID);
        }

        // ====================================
        // 組BPM 參數
        // ====================================

        // 無需簽核人員，直達 END
        if (WkStringUtils.isEmpty(targetSingerRoleIds)) {
            return this.prepareBpmParamaters_OnlyApplicant();
        }

        Map<String, Object> params = Maps.newHashMap();

        // 依據簽核人員個數，決定路徑和參數
        if (targetSingerRoleIds.size() == 1) {
            // 僅簽一人
            params.put(ReqUnitflowParam.flowType.name(), ReqUnitflowType.SIGNER01.name());
            // 第一個簽核者
            params.put(ReqUnitflowParam.signer01RoleID.name(), targetSingerRoleIds.get(0));
        } else if (targetSingerRoleIds.size() == 2) {
            // 簽名者有2人
            params.put(ReqUnitflowParam.flowType.name(), ReqUnitflowType.SIGNER02.name());
            // 第一個簽核者
            params.put(ReqUnitflowParam.signer01RoleID.name(), targetSingerRoleIds.get(0));
            // 第二個簽核者
            params.put(ReqUnitflowParam.signer02RoleID.name(), targetSingerRoleIds.get(1));
        } else if (targetSingerRoleIds.size() == 3) {
            // 簽名者有3人
            params.put(ReqUnitflowParam.flowType.name(), ReqUnitflowType.SIGNER02.name());
            // 第一個簽核者
            params.put(ReqUnitflowParam.signer01RoleID.name(), targetSingerRoleIds.get(0));
            // 第二個簽核者
            params.put(ReqUnitflowParam.signer02RoleID.name(), targetSingerRoleIds.get(1));
            // 第三個簽核者
            params.put(ReqUnitflowParam.signer02RoleID.name(), targetSingerRoleIds.get(2));

        } else {
            // 設定錯誤
            String errorMessage = templetErrorMessage + "簽核者設定個數超過上限[" + targetSingerRoleIds.size() + "]";
            log.info(processLog.prepareProcessLog(errorMessage));
            throw new SystemOperationException(errorMessage, InfomationLevel.ERROR);
        }

        return params;
    }

}
