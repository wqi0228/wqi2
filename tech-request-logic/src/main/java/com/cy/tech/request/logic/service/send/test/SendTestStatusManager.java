package com.cy.tech.request.logic.service.send.test;

import java.io.Serializable;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.vo.worktest.enums.WorkTestInfoStatus;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.google.common.base.Preconditions;

import lombok.extern.slf4j.Slf4j;

/**
 * 送測狀態管理
 * 
 * @author allen1214_wu
 */
@Slf4j
@Component
public class SendTestStatusManager implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4531482768922784753L;
    @Autowired
    private SendTestService sendTestService;

    /**
     * 新建檔
     */
    public void changeStatusByNewCreate(WorkTestInfo testInfo) {

        log.debug("送測狀態管理:新建檔");

        // 狀態 - 送測
        testInfo.setTestinfoStatus(WorkTestStatus.SONGCE);

        // 比對送測單位中，是否包含 QA單位
        boolean isContainsQaDepts = sendTestService.isContainsQADepts(testInfo);

        // 若有 QA 單位, 走QA排程
        if (isContainsQaDepts) {
            testInfo.setQaAuditStatus(WorkTestInfoStatus.WAIT_APPROVE);
            testInfo.setCommitStatus(WorkTestInfoStatus.UNDEFINED);
            testInfo.setQaScheduleStatus(WorkTestInfoStatus.UNDEFINED);
            log.debug("通知單位包含QA, 走QA排測流程");
        } else {
            testInfo.setQaAuditStatus(WorkTestInfoStatus.UNNEEDED_APPROVE);
            testInfo.setCommitStatus(WorkTestInfoStatus.UNNEEDED_COMMIT);
            testInfo.setQaScheduleStatus(WorkTestInfoStatus.UNNEEDED_SCHEDULE);
            log.debug("通知單位包未含QA, 走一般流程");
        }

    }

    /**
     * 簽核 - 送簽
     */
    public void changeStatusBySignProcess(WorkTestInfo testInfo) {

        if (!WorkTestStatus.SONGCE.equals(testInfo.getTestinfoStatus())) {
            Preconditions.checkArgument(false, " 送測狀態非【送測】時，不可送簽");
        }

        // 送測狀態:"送測簽核中"
        testInfo.setTestinfoStatus(WorkTestStatus.SIGN_PROCESS);
        // 有送簽
        testInfo.setHasSign(Boolean.TRUE);

        // 待閱原因 - 送測簽核中
        testInfo.setReadReason(WaitReadReasonType.TEST_SIGN_PROCESS);
        // 待閱異動日
        testInfo.setReadUpdateDate(new Date());
    }

    /**
     * 簽核 - 核准
     * 
     * @param testInfo
     */
    public void changeStatusBySignApprove(WorkTestInfo testInfo) {

        if (!WorkTestStatus.SIGN_PROCESS.equals(testInfo.getTestinfoStatus())) {
            Preconditions.checkArgument(false, " 送測狀態非【送測簽核中時】時，不可核准");
        }

        // 狀態改回送測
        testInfo.setTestinfoStatus(WorkTestStatus.SONGCE);

        // 待閱原因 - "送測已核准"
        testInfo.setReadReason(WaitReadReasonType.TEST_APPROVE);
        // 待閱異動日
        testInfo.setReadUpdateDate(new Date());
    }
    
    /**
     * 退測
     * 
     * @param testInfo
     */
    public void changeStatusByReject(WorkTestInfo testInfo) {

        //狀態：退測
        testInfo.setTestinfoStatus(WorkTestStatus.ROLL_BACK_TEST);
        //待閱原因：退測
        testInfo.setReadReason(WaitReadReasonType.TEST_ROLL_BACK);
        testInfo.setReadUpdateDate(new Date());
    }

}
