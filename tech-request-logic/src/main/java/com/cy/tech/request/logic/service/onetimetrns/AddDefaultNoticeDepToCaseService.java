package com.cy.tech.request.logic.service.onetimetrns;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.AssignNoticeService;
import com.cy.tech.request.logic.service.AssignSendInfoService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkConstants;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 * 特殊需求轉檔 - 無通知單位單據, 補預設通知單位
 */
@Slf4j
@Service
public class AddDefaultNoticeDepToCaseService {

	// ========================================================================
	// 服務元件區
	// ========================================================================
	@Autowired
	private transient RequireService requireService;
	@Autowired
	private transient AssignSendInfoService assignSendInfoService;
	@Autowired
	private transient AssignNoticeService assignNoticeService;
	//@Autowired
	//private transient CategorySettingService categorySettingService;
	@Autowired
	private transient WkUserCache wkUserCache;

	// ========================================================================
	//
	// ========================================================================
	private final List<RequireStatusType> requireStatus = Lists.newArrayList(
	        RequireStatusType.PROCESS,
	        RequireStatusType.COMPLETED,
	        RequireStatusType.CLOSE,
	        RequireStatusType.AUTO_CLOSED);

	// ========================================================================
	// 方法區
	// ========================================================================

	public void processByDate(
	        Date startCreateDate,
	        Date endCreateDate) throws UserMessageException {

		if (startCreateDate == null) {
			startCreateDate = new Date();
		}

		if (endCreateDate == null) {
			endCreateDate = new Date();
		}

		log.info("\r\n開始執行：預設通知單位填補, 條件:{}~{}",
		        WkDateUtils.formatDate(
		                startCreateDate,
		                WkDateUtils.YYYY_MM_DD),
		        WkDateUtils.formatDate(
		                endCreateDate,
		                WkDateUtils.YYYY_MM_DD));

		// ====================================
		// 依據傳入的起始日期，取得之後開立的單據
		// ====================================
		List<Require> requires = requireService.findByRequireStatusAndCreateDate(
		        this.requireStatus,
		        startCreateDate,
		        endCreateDate);

		

		// ====================================
		// 轉檔
		// ====================================
		this.startProcess(requires);

	}

	/**
	 * @param condition_requireNos
	 * @throws UserMessageException
	 */
	public void processByRequireNos(
	        String condition_requireNos) throws UserMessageException {

		log.info("\r\n開始執行：預設通知單位填補, 條件:\r\n{}",
		        condition_requireNos);

		// ====================================
		// 整理
		// ====================================
		// 需求單號字串
		List<String> requireNos = null;
		String currCondition_requireNos = WkStringUtils.safeTrim(condition_requireNos);
		// 去斷行
		currCondition_requireNos = currCondition_requireNos.replaceAll("\r\n", currCondition_requireNos);
		currCondition_requireNos = currCondition_requireNos.replaceAll("\n", currCondition_requireNos);
		// 去空白
		currCondition_requireNos = currCondition_requireNos.replaceAll(" ", currCondition_requireNos);
		if (WkStringUtils.notEmpty(currCondition_requireNos)) {
			requireNos = Lists.newArrayList(currCondition_requireNos.split(","));
		}
		// 去除空白資料
		if (WkStringUtils.notEmpty(requireNos)) {
			requireNos = requireNos.stream().filter(requireNo -> WkStringUtils.notEmpty(requireNo)).collect(Collectors.toList());
		}

		// ====================================
		// 查詢主檔
		// ====================================
		// 查詢
		List<Require> qryRequires = this.requireService.findByRequireNoIn(requireNos);
		// 過濾狀態
		List<Require> requires = Lists.newArrayList();
		for (Require require : qryRequires) {
			if (!this.requireStatus.contains(require.getRequireStatus())) {
				log.debug("[{}] 狀態為:{} ->  pass ",
				        require.getRequireNo(),
				        require.getRequireStatus());
				continue;
			}
			requires.add(require);
		}

		// ====================================
		// 轉檔
		// ====================================
		this.startProcess(requires);

	}

	private void startProcess(
	        List<Require> requires) throws UserMessageException {

		if (WkStringUtils.isEmpty(requires)) {
			throw new UserMessageException("未查到可轉檔的資料", InfomationLevel.WARN);
		}
		
		log.debug("共{}筆", requires.size());

		User execUser = wkUserCache.findBySid(WkConstants.ADMIN_USER_SID);
		Date execDate = new Date();

		// ====================================
		// 查詢通知設定資料
		// ====================================
		// 收集 sid
		List<String> requireSids = requires.stream()
		        .map(Require::getSid)
		        .collect(Collectors.toList());

		Map<String, List<Integer>> noticeDepSidsMapByRequireSid = this.assignSendInfoService.findDepSidByRequireSidAndStatus(
		        requireSids,
		        AssignSendType.SEND);

		Map<String, List<Integer>> assignDepSidsMapByRequireSid = this.assignSendInfoService.findDepSidByRequireSidAndStatus(
		        requireSids,
		        AssignSendType.ASSIGN);

		// ====================================
		//
		// ====================================
		for (Require require : requires) {
			// 取得以單據 sid 取得已設定的通知單位
			List<Integer> noticeDepSids = noticeDepSidsMapByRequireSid.get(require.getSid());
			// 不為空時不處理
			if (WkStringUtils.notEmpty(noticeDepSids)) {
				log.debug("[{}] 已設定 ->  pass ", require.getRequireNo());
				continue;
			}

			// 取得預設通知單位
			List<Integer> defaultNoticeDepSids = this.findDefaultNoticeDepSids(require);
			if (WkStringUtils.isEmpty(defaultNoticeDepSids)) {
				log.debug("[{}] {}, 無預設通知單位 -> pass",
				        require.getRequireNo(),
				        require.getMapping().getSmallName());
				continue;
			}

			// 取德已經分派的單位
			List<Integer> assignDepSids = assignDepSidsMapByRequireSid.get(require.getSid());

			// 計算要新增的通知單位
			Set<Integer> addNoticeDeps = Sets.newHashSet(defaultNoticeDepSids);
			addNoticeDeps.removeAll(assignDepSids);

			if (WkStringUtils.isEmpty(addNoticeDeps)) {
				log.debug("[{}] 所有預設通知單位都已經分派 -> pass",
				        require.getRequireNo());
				continue;
			}

			log.debug("[{}] start process ", require.getRequireNo());

			this.assignNoticeService.processForAddNoticeDeps(
			        require.getSid(),
			        require.getRequireNo(),
			        Lists.newArrayList(addNoticeDeps),
			        "轉檔",
			        execUser,
			        execDate
			        );
		}

	}

	private Map<String, List<Integer>> defaultNoticeDepSidsCacheMapBySmallSid = Maps.newHashMap();

	/**
	 * @param require
	 * @return
	 */
	private List<Integer> findDefaultNoticeDepSids(Require require) {
		String smallSid = require.getMapping().getSmall().getSid();

		List<Integer> noticeDepSids = defaultNoticeDepSidsCacheMapBySmallSid.get(smallSid);

		if (WkStringUtils.notEmpty(noticeDepSids)) {
			return noticeDepSids;
		}

		//因後續移除程式, 故註解掉
		//noticeDepSids = categorySettingService.findDefaultNoticeDepsBySmallCategory(
		//        smallSid);

		defaultNoticeDepSidsCacheMapBySmallSid.put(smallSid, noticeDepSids);

		return noticeDepSids;
	}

}
