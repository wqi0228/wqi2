/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.send.test;

import com.cy.work.common.enums.ReadRecordType;
import com.cy.commons.vo.User;
import com.cy.tech.request.repository.worktest.WorkTestAlertRepo;
import com.cy.tech.request.vo.worktest.WorkTestAlert;
import com.cy.tech.request.vo.worktest.WorkTestAlreadyReply;
import com.cy.tech.request.vo.worktest.WorkTestReply;
import com.cy.work.common.enums.WorkSourceType;
import com.google.common.collect.Sets;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.Executors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 最新消息
 *
 * @author shaun
 */
@Component
public class SendTestAlertService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3117217427146888503L;
    @Autowired
    private WorkTestAlertRepo alertDao;
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void carateReplyAlert(WorkTestAlreadyReply aReply) {
        Executors.newCachedThreadPool().execute(() -> {
            Set<User> receiversUsers = this.findSenderBySourceTypeAndReply(WorkSourceType.TECH_REQUEST, aReply.getReply());
            //只取sid，以免not equals 
            User replyPerson = new User();
            replyPerson.setSid(aReply.getReply().getPerson().getSid());
            receiversUsers.add(replyPerson);
            receiversUsers.forEach(receiver -> {
                alertDao.save(this.createEmptyAlert(aReply, receiver));
            });
        });
    }

    private WorkTestAlert createEmptyAlert(WorkTestAlreadyReply aReply, User receiver) {
        WorkTestAlert alert = new WorkTestAlert();
        alert.setTestInfo(aReply.getTestInfo());
        alert.setTestinfoNo(aReply.getTestInfo().getTestinfoNo());
        alert.setSourceType(aReply.getTestInfo().getSourceType());
        alert.setSourceSid(aReply.getTestInfo().getSourceSid());
        alert.setSourceNo(aReply.getTestInfo().getSourceNo());
        alert.setTestinfoTheme(sdf.format(new Date()) + " " + aReply.getTestInfo().getTheme() + " - 回覆通知");
        alert.setSender(aReply.getPerson());
        alert.setSendDate(new Date());
        alert.setReceiver(receiver);
        alert.setReply(aReply.getReply());
        alert.setAlreadyReply(aReply);
        alert.setReadStatus(ReadRecordType.UN_READ);
        return alert;
    }

    private Set<User> findSenderBySourceTypeAndReply(WorkSourceType sourceType, WorkTestReply reply) {
        return Sets.newHashSet(alertDao.findSenderBySourceTypeAndReply(sourceType, reply));
    }

    @Transactional(readOnly = true)
    public WorkTestAlert findBySid(String sid) {
        return alertDao.findOne(sid);
    }

    @Transactional(rollbackFor = Exception.class)
    public void save(WorkTestAlert alert) {
        alertDao.save(alert);
    }
}
