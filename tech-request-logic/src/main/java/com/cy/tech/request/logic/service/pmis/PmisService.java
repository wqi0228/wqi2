package com.cy.tech.request.logic.service.pmis;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.utils.DateUtils;
import com.cy.tech.request.logic.vo.PmisResponseVO;
import com.cy.tech.request.repository.require.pmis.PmisHistoryRepository;
import com.cy.tech.request.vo.require.pmis.PmisHistory;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkStringUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * @author aken_kao
 */
@Slf4j
@Service
public class PmisService {

    @Autowired
    private PmisHistoryRepository pmisHistoryRepository;
    @Value("${pmis.server.url}")
    private String pmisUrl;

    /**
     * 新增一筆pmis history, 新增一筆追蹤
     * 
     * @param jsonMap
     * @param loginUser
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveByMap(
            Map<String, Object> jsonMap,
            Integer requireCreateDepSid,
            User loginUser) {

        PmisHistory pmisHistory = new PmisHistory();
        String requireNo = (String) jsonMap.get("werp_id");

        pmisHistory.setDItem((String) jsonMap.get("d_item"));
        pmisHistory.setDMidItem((String) jsonMap.get("d_mid_item"));
        pmisHistory.setDTheme((String) jsonMap.get("d_theme"));
        pmisHistory.setDReason((String) jsonMap.get("d_reason"));
        pmisHistory.setDDesc((String) jsonMap.get("d_desc"));
        pmisHistory.setDDept(requireCreateDepSid);
        pmisHistory.setDDate(DateUtils.YYYY_MM_DD2.parseDateTime((String) jsonMap.get("d_date")).toDate());
        pmisHistory.setDCoDept(jsonMap.get("d_co_dept").toString());
        pmisHistory.setNoteWerpId(requireNo);
        pmisHistory.setNoteWerpLink((String) jsonMap.get("werp_link"));
        pmisHistory.setFbCaseNo((Integer) jsonMap.get("case_number"));

        pmisHistory.setCreatedDate(new Date());
        pmisHistory.setCreatedUser(loginUser);

        save(pmisHistory);
    }

    @Transactional(rollbackFor = Exception.class)
    public void save(PmisHistory entity) {
        pmisHistoryRepository.save(entity);
    }

    public List<PmisHistory> findByRequireNo(String requireNo) {
        return pmisHistoryRepository.findByRequireNo(requireNo);
    }

    /**
     * @param requireNo
     * @return
     */
    public boolean hasPmisHistory(String requireNo) {
        return pmisHistoryRepository.isExistByRequireNo(requireNo);
        // return CollectionUtils.isNotEmpty(findByRequireNo(requireNo));
    }

    /**
     * 組織異動用: 傳入 require_sid ，判斷是否需要同步PMIS
     * 1. tr_to_pmis_history 需存在
     * 2. 結案者不同步
     * 
     * @param requireSids 需求單 sid
     * @return
     */
    public Set<String> findNeedTrnasForOrgTrnas(Set<String> requireSids) {
        List<String> resultRequireSids = this.pmisHistoryRepository.findNeedTrnasForOrgTrnas(requireSids);
        if (WkStringUtils.isEmpty(resultRequireSids)) {
            return Sets.newHashSet();
        }
        return Sets.newHashSet(resultRequireSids);
    }

    /**
     * 發送至PMIS
     * 
     * @param requireNo 單號
     * @param jsonBody  發送資料
     * @return
     * @throws UserMessageException
     */
    public PmisResponseVO sendDataToPmis(String requireNo, String jsonBody) throws UserMessageException {
        RestTemplate restTemplate = new RestTemplate();
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        messageConverters.add(new StringHttpMessageConverter());
        restTemplate.setMessageConverters(messageConverters);

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<String> request = new HttpEntity<String>(jsonBody, header);
        try {
            String resultJson = restTemplate.postForObject(pmisUrl, request, String.class);
            log.info("【轉PMIS】 {} to pmis & response:{}", requireNo, resultJson);

            return new ObjectMapper().readValue(resultJson, PmisResponseVO.class);

        } catch (ResourceAccessException | HttpClientErrorException e) {
            String message = String.format("連線PMIS失敗, 請確認主機狀態:[%s]", pmisUrl);
            log.error(message, e);
            throw new UserMessageException(message, InfomationLevel.ERROR);

        } catch (Exception e) {
            String message = String.format("發送至PMIS時發生錯誤:[%s]", e.getMessage());
            log.error(message, e);
            throw new UserMessageException(message, InfomationLevel.ERROR);
        }
    }

}
