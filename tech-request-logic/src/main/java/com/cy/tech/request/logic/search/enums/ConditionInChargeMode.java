package com.cy.tech.request.logic.search.enums;

import com.cy.work.common.utils.WkStringUtils;

import lombok.Getter;

/**
 * 主責單位查詢模式
 *
 * @author Allen
 */
public enum ConditionInChargeMode {

	NO_FILTER("請選擇", false, false),
	IN_CHARGE_DEP("已設定主責單位", true, false),
	IN_CHARGE_USR("已設定負責人", false, true),
	WAIT_SETTING("未設定主責單位", false, false),
	EXCEPTION_INACTIVE("已停用異常檢查", false, false),
	;

	/**
	 * 說明
	 */
	@Getter
	private final String descr;

	/**
	 * 顯示部門選單
	 */
	@Getter
	private final boolean showDepMenu;
	/**
	 * 顯示人員選單
	 */
	@Getter
	private final boolean showUserMenu;

	private ConditionInChargeMode(String descr, boolean showDepMenu, boolean showUserMenu) {
		this.descr = descr;
		this.showDepMenu = showDepMenu;
		this.showUserMenu = showUserMenu;
	}

	/**
	 * @param str
	 * @return
	 */
	public static ConditionInChargeMode safeValueOf(String str) {
		if (WkStringUtils.notEmpty(str)) {
			for (ConditionInChargeMode enumType : ConditionInChargeMode.values()) {
				if (enumType.name().equals(str)) {
					return enumType;
				}
			}
		}
		return null;
	}
}
