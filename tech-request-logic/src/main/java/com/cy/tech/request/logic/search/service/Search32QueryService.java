/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.search.view.Search32View;
import com.cy.tech.request.logic.service.URLService;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

/**
 * @author marlow_chen
 */
@Service
public class Search32QueryService implements QueryService<Search32View>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8936457370233533940L;
    @Autowired
    private URLService urlService;

    @PersistenceContext
    transient private EntityManager em;

    @Override
    public List<Search32View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {
        // ====================================
        // 查詢
        // ====================================
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        @SuppressWarnings("unchecked")
        List<Object[]> result = query.getResultList();
        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if (WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();
        List<Search32View> viewResult = Lists.newArrayList();
        for (int i = 0; i < result.size(); i++) {

            Object[] record = result.get(i);

            int index = 0;
            String sid = (String) record[index++];
            Date testDate = (Date) record[index++];
            Integer createDep = (Integer) record[index++];
            Integer createdUser = (Integer) record[index++];
            String testTheme = (String) record[index++];
            String commitStatus = (String) record[index++];
            Date establishDate = (Date) record[index++];
            Date createdDate = (Date) record[index++];
            String requireNo = (String) record[index++];
            String waitRead = String.valueOf(record[index++]);
            Date readDate = (Date) record[index++];

            Search32View v = new Search32View();
            v.setSid(sid);
            v.setRequireNo(requireNo);
            v.setTestDate(testDate);
            v.setCreateDep(createDep);
            v.setCreatedUser(createdUser);
            v.setTestTheme(testTheme);
            v.setCommitStatus(commitStatus);
            v.setCreatedDate(createdDate);
            v.setEstablishDate(establishDate);
            v.setLocalUrlLink(urlService.createLocalUrlLinkParamForTab(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(execUserSid, v.getRequireNo(), 1),
                    URLService.URLServiceAttr.URL_ATTR_TAB_ST, v.getSid()));

            // ====================================
            // 閱讀記錄
            // ====================================
            ReadRecordType readRecordType = ReadRecordType.UN_READ;
            // 待閱讀
            if (WkStringUtils.isEmpty(waitRead) ||"Y".equals(waitRead)) {
                if (readDate == null) {
                    readRecordType = ReadRecordType.UN_READ;
                } else {
                    readRecordType = ReadRecordType.WAIT_READ;
                }
            }
            // 已閱讀
            else if ("N".equals(waitRead)) {
                readRecordType = ReadRecordType.HAS_READ;
            }
            v.setReadRecordType(readRecordType);

            viewResult.add(v);
        }

        // 解析資料-結束
        usageRecord.parserDataEnd();
        return viewResult;
    }
}
