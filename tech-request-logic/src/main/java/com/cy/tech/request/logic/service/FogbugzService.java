/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import com.cy.commons.enums.Activation;
import com.cy.commons.util.FusionUrlServiceUtils;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.fb.rest.client.controller.FBClient;
import com.cy.fb.rest.client.vo.MappingUser;
import com.cy.fb.rest.client.vo.Person;
import com.cy.fb.rest.client.vo.Project;
import com.cy.tech.request.logic.enumerate.PropKeyType;
import com.cy.tech.request.logic.service.URLService.URLServiceAttr;
import com.cy.tech.request.logic.service.helper.FogbugzContentHelper;
import com.cy.tech.request.logic.vo.UrlParamTo;
import com.cy.tech.request.repository.require.FogbugzAssignRepository;
import com.cy.tech.request.repository.require.FogbugzProjectMappingRepository;
import com.cy.tech.request.repository.require.ReqModifyRepo;
import com.cy.tech.request.vo.require.FogbugzAssign;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import com.cy.tech.request.vo.template.component.ComBase;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * FB服務
 *
 * @author shaun
 */
@Component
@Slf4j
public class FogbugzService implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 3749515489278739023L;
    /** fb issue page url */
	@Getter
	@Value("${fbservice.fogBugzUrl}")
	private String PROPERTY_FOGBUGZURL;
	private final String req02Path = "/require/require02.xhtml";
	@Value("${tech.request.fb.link.dead.line:365}")
	private Integer fbLinkDeadLine;

	@Autowired
	private RequireService reqService;
	@Autowired
	private RequireShowService reqShowService;
	@Autowired
	private SearchService searchService;
	@Autowired
	private FogbugzContentHelper fbContentHelper;
	@Autowired
	private URLService urlService;

	@Autowired
	private FBClient fbClient;
	@Autowired
	private ReqModifyRepo reqModifyDao;
	@Autowired
	private FogbugzAssignRepository fbDao;
	@Autowired
	private FogbugzProjectMappingRepository fbMappingDao;
	@Autowired
	private OrganizationService organizationService;
	@Autowired
	private AssignNoticeService assignNoticeService;

	/**
	 * 開發本機環境使用, 開發階段, 停止排程
	 */
	@Value("${dev.fixFbMappingUser.sid}")
	private String devFixFbLeaderSid;

	public Integer getDevFixFbLeaderSid() {
		try {
			if (WkStringUtils.isNumber(devFixFbLeaderSid)) {
				return Integer.parseInt(devFixFbLeaderSid);
			}
		} catch (Exception e) {
		}
		return null;
	}

	@Transactional(readOnly = true)
	public List<FogbugzAssign> findByRequire(Require require) {
		List<FogbugzAssign> result = fbDao.findByRequireOrderByUpdatedDateDesc(require);
		result.forEach(each -> each.setForwardToFbUrl(this.createFbUrl(each.getFbId())));
		return result;
	}

	/**
	 * 建立FB連結
	 *
	 * @param fbId
	 * @return
	 */
	public String createFbUrl(Integer fbId) {
		if (fbId == null) {
			return null;
		}
		return this.findFbApiUrl() + fbId;
	}

	public String findFbApiUrl() {
		return PROPERTY_FOGBUGZURL + "default.asp?";
	}

	/**
	 * 驗證權限
	 * 
	 * @param require
	 * @param executor
	 * @throws UserMessageException
	 */
	public Project verifyAuthAndFindFBProject(
	        Require require,
	        User executor) throws UserMessageException {
		// ====================================
		// 檢核權限
		// ====================================
		// 判斷可執行權限
		boolean isExecRight = reqShowService.showTransFogbugz(reqService.findByReqObj(require), executor, true);
		if (!isExecRight) {
			throw new UserMessageException(
			        WkMessage.NEED_RELOAD,
			        "使用者無權限執行轉 FB",
			        InfomationLevel.WARN);
		}

		// ====================================
		// 查詢登入者單位
		// ====================================
		Org dep = null;
		if (executor != null && executor.getPrimaryOrg() != null) {
			dep = WkOrgCache.getInstance().findBySid(executor.getPrimaryOrg().getSid());
		}
		if (dep == null) {
			throw new UserMessageException(
			        WkMessage.NEED_RELOAD,
			        "取得登入資訊失敗!",
			        InfomationLevel.WARN);
		}

		// ====================================
		// 查詢對應的 FB 專案資料
		// ====================================
		try {
			return this.findAndCheckFbProjectByPrimaryOrg(dep);
		} catch (IllegalArgumentException e) {
			throw new UserMessageException(
			        e.getMessage(),
			        InfomationLevel.WARN);
		}
	}

	/**
	 * 轉FB
	 * 
	 * @param require
	 * @param fbProject
	 * @param fogbugzAssign
	 * @param executor
	 * @param execDate
	 * @param selectTemplateItem
	 * @param comValueMap
	 * @return
	 * @throws UserMessageException
	 * @throws SystemDevelopException
	 * @throws UnsupportedEncodingException
	 */
	@Transactional(rollbackFor = Exception.class)
	public Require save(
	        Require require,
	        Project fbProject,
	        FogbugzAssign fogbugzAssign,
	        List<FieldKeyMapping> selectTemplateItem,
	        Map<String, ComBase> comValueMap,
	        User executor,
	        Date execDate) throws UserMessageException, SystemDevelopException, UnsupportedEncodingException {

		// ====================================
		// 處理
		// ====================================
		// 產生需求單傳送內文
		String requireContentStr = fbContentHelper.createRequireStrContent(
		        require,
		        executor,
		        this.createRequireLinkURL(require),
		        selectTemplateItem,
		        comValueMap);

		// create or update
		if (fogbugzAssign == null) {
			this.createFBAssign(
			        require,
			        fbProject,
			        requireContentStr,
			        executor,
			        execDate);
		} else {
			this.updateFBAssign(
			        require,
			        fogbugzAssign,
			        requireContentStr,
			        executor,
			        execDate);
		}

		// ====================================
		// 更新需求單主檔狀態
		// ====================================
		require.setHasFbRecord(Boolean.TRUE);
		reqModifyDao.updateFbRecord(require, require.getHasFbRecord(), executor, new Date());

		// ====================================
		// 非分派單位成員開單時, 進行自動分派 (如果需要的話)
		// ====================================
		this.assignNoticeService.processForAutoAddAssignDep(
		        require.getSid(),
		        require.getRequireNo(),
		        executor.getPrimaryOrg().getSid(), // TODO 應改為立案單位, 但目前無此欄位
		        "轉FB",
		        executor,
		        execDate);

		return require;
	}

	private String createRequireLinkURL(Require require) {
		UrlParamTo urlTo = this.createEmptyUrlTo(require);
		String urlLinkParam = urlService.createUrlLinkParam(URLServiceAttr.URL_ATTR_M, urlTo);

		return buildContextPath() + req02Path + urlLinkParam;
	}

	private UrlParamTo createEmptyUrlTo(Require reuqire) {
		UrlParamTo to = new UrlParamTo();
		to.setDl(searchService.addDay(Calendar.getInstance(), fbLinkDeadLine));
		to.setNo(reuqire.getRequireNo());
		return to;
	}

	/**
	 * 尋找建立FB案件成員對應的專案群組名稱
	 *
	 * @param executor
	 * @return
	 */
	private Project findAndCheckFbProjectByPrimaryOrg(Org org) throws IllegalArgumentException {
		String fbProjectName = fbMappingDao.findProjectNameByDept(org);
		if (Strings.isNullOrEmpty(fbProjectName)) {
			throw new IllegalArgumentException(WkOrgUtils.getOrgName(org) + " 未建立fogbugz部門專案映射資訊，請洽E化發展部。");
		}
		log.info("建立送測FB資訊 -> 執行部門:" + org.getId() + " fbProjectName:" + fbProjectName);
		Project project = fbClient.findProjectByName(fbProjectName);
		if (project == null) {
			throw new IllegalArgumentException("FB查無此專案名稱【" + fbProjectName + "】，請洽系統管理員！！");
		}
		if (project.getIxPersonOwner() == null) {
			throw new IllegalArgumentException("FB專案【" + fbProjectName + "】無設定負責人員，請洽ＦＢ管理員！！");
		}
		log.info("建立送測FB資訊 -> 執行部門:" + org.getId() + " ixPersonOwner:" + project.getIxPersonOwner());
		User leader = this.findUserByFbUserId(project.getIxPersonOwner());
		if (leader == null) {
			Person person = fbClient.findPersonById(project.getIxPersonOwner());
			throw new IllegalArgumentException("FB帳號【" + person.getFullName() + "】無設定ERP人員對應，請洽系統管理員！！");
		}
		log.info("建立送測FB資訊 -> 執行部門:" + org.getId() + " leader:" + leader.getId());
		return project;
	}

	/**
	 * @param require
	 * @param executor
	 * @return
	 */
	public FogbugzAssign findByRequireAndUser(Require require, User executor) {

		// ====================================
		// 查詢此需求單所有的FB
		// ====================================
		List<FogbugzAssign> fogbugzAssigns = this.findByRequire(require);
		if (WkStringUtils.isEmpty(fogbugzAssigns)) {
			return null;
		}

		// ====================================
		// 比對建立者為
		// ====================================
		for (FogbugzAssign fogbugzAssign : fogbugzAssigns) {
			if (fogbugzAssign.getCreatedUser() != null
			        && fogbugzAssign.getCreatedUser().equals(executor)) {
				return fogbugzAssign;
			}
		}
		return null;
	}

	/**
	 * @param require
	 * @param fbProject
	 * @param requireContentStr
	 * @param executor
	 * @param execDate
	 * @throws UnsupportedEncodingException
	 */
	private void createFBAssign(
	        Require require,
	        Project fbProject,
	        String requireContentStr,
	        User executor,
	        Date execDate)
	        throws UnsupportedEncodingException {

		log.info("開始執行轉FB(新增) - 需求單號:{}, Project:{}",
		        require.getRequireNo(),
		        fbProject.getsProject());

		// ====================================
		// 建立 fb case (remote)
		// ====================================
		int fbId = this.createFBIssue(require, fbProject, requireContentStr);

		// ====================================
		// 建立 FogbugzAssign
		// ====================================
		FogbugzAssign fa = new FogbugzAssign();
		fa.setCreatedDate(execDate);
		fa.setCreatedUser(executor);
		fa.setUpdatedDate(execDate);
		fa.setUpdatedUser(executor);
		fa.setFbId(fbId);
		fa.setFbProject(fbProject.getsProject());
		fa.setRequire(require);
		fa.setRequireNo(require.getRequireNo());
		fa.setStatus(Activation.ACTIVE);
		fbDao.save(fa);

		log.info("完成執行轉FB(新增) - fbId:{}",
		        fbId);
	}

	/**
	 * 更新FB CASE
	 *
	 * @param require
	 * @param executor
	 * @param requireContentStr
	 * @throws java.io.UnsupportedEncodingException
	 */
	public void updateFBAssign(
	        Require require,
	        FogbugzAssign fogbugzAssign,
	        String requireContentStr,
	        User executor,
	        Date execDate) throws UnsupportedEncodingException {

		log.info("開始執行轉FB(更新) - 需求單號:{}, fbId:{}",
		        require.getRequireNo(),
		        fogbugzAssign.getFbId());

		fbClient.editContent(
		        fogbugzAssign.getFbId(),
		        requireContentStr,
		        this.createFbTags(require));

		fogbugzAssign.setUpdatedDate(execDate);
		fogbugzAssign.setUpdatedUser(executor);
		fbDao.save(fogbugzAssign);
	}

	/**
	 * 從技術事業群提供的mapping restful service 中抓取對應的fusion user
	 *
	 * @param ixPerson
	 * @return
	 * @throws IllegalArgumentException
	 */
	private User findUserByFbUserId(Integer ixPerson) throws IllegalArgumentException {
		// ====================================
		// 開發環境無法連到 mapping restful service , 回傳固定 user
		// ====================================
		Integer fakeFbUserSid = this.getDevFixFbLeaderSid();
		if (fakeFbUserSid != null) {
			log.error("開發環境固定回傳 FB user , 若正式環境出現此訊息即為錯誤:" + fakeFbUserSid);
			return WkUserCache.getInstance().findBySid(fakeFbUserSid);
		}

		// ====================================
		// 查詢
		// ====================================
		MappingUser mappingUser = fbClient.findByFbUserId(ixPerson);
		String userID = mappingUser.getTgName();
		User result = WkUserCache.getInstance().findById(userID);

		// 查不到的時候改全小寫試試看 (werp id 只會有小寫)
		if (result == null) {
			userID = WkStringUtils.safeTrim(mappingUser.getTgName()).toLowerCase();
			result = WkUserCache.getInstance().findById(userID);
		}

		log.info("建立送測FB資訊 -> ixPersonOwner:" + ixPerson + " mappingUser.tgName:" + mappingUser.getTgName());
		return result;
	}

	private int createFBIssue(Require require, Project fbProject, String requireContentStr) throws UnsupportedEncodingException {
		String theme = searchService.getThemeStr(require);

		return fbClient.createCase(
		        fbProject.getIxProject(),
		        fbProject.getIxPersonOwner(),
		        theme,
		        requireContentStr,
		        this.createFbTags(require));
	}

	private List<String> createFbTags(Require require) {
		return Lists.newArrayList(require.getMapping().getBigName(), require.getMapping().getSmallName(), "WERP");
	}

	/**
	 * 建立送測轉FB資訊
	 *
	 * @param require
	 * @param executor
	 * @param testInfo
	 * @return
	 * @throws IllegalArgumentException
	 * @throws UnsupportedEncodingException
	 */
	public int createSendTestFB(Require require, WorkTestInfo testInfo, User executor) throws IllegalArgumentException, UnsupportedEncodingException {
		log.info("需求單單號:[" + require.getRequireNo() + "]送測單號[" + testInfo.getTestinfoNo() + "]執行人員:[" + executor.getName() + "]");
		Org dep = organizationService.findBySid(executor.getPrimaryOrg().getSid());
		Project fbProject = this.findAndCheckFbProjectByPrimaryOrg(dep);
		String testInfoContentStr = fbContentHelper.createWorkTestInfoStrContent(testInfo, executor, this.createSendTestUrl(require, testInfo.getSid()));
		int fbId = this.createFBIssue(testInfo, fbProject, testInfoContentStr);
		log.info("需求單單號:[" + require.getRequireNo() + "]執行人員:[" + executor.getName() + "]FBID[" + fbId + "]");
		return fbId;
	}

	private String createSendTestUrl(Require require, String testinfoSid) {
		UrlParamTo para = this.createEmptyUrlTo(require);
		return buildContextPath() + req02Path
		        + urlService.createUrlLinkParamForTab(URLService.URLServiceAttr.URL_ATTR_M, para, URLService.URLServiceAttr.URL_ATTR_TAB_ST, testinfoSid);
	}

	private int createFBIssue(WorkTestInfo testInfo, Project fbProject, String requireContentStr) throws UnsupportedEncodingException {
		log.info("建立送測FB資訊 -> ixPersonOwner:" + fbProject.getIxPersonOwner() + "ixProject:" + fbProject.getIxProject() + " tsProject:"
		        + fbProject.getsProject());
		return fbClient.createCase(fbProject.getIxProject(), fbProject.getIxPersonOwner(), testInfo.getTheme(), requireContentStr);
	}

	/**
	 * 取得建立者單位
	 */
	@Transactional(readOnly = true)
	public List<Org> findCreatedOrgByRequire(Require require) {
		List<Org> result = Lists.newArrayList();
		// 查詢
		List<FogbugzAssign> fogbugzAssigns = fbDao.findByRequireOrderByUpdatedDateDesc(require);

		// 組裝
		for (FogbugzAssign fogbugzAssign : fogbugzAssigns) {
			User createUser = fogbugzAssign.getCreatedUser();
			if (createUser != null && createUser.getPrimaryOrg() != null) {
				result.add(createUser.getPrimaryOrg().toOrg());
			}
		}
		return result;
	}

	private String buildContextPath() {
		return FusionUrlServiceUtils.getUrlByPropKey(PropKeyType.TECH_REQUEST_AP_URL.getValue());
	}
}
