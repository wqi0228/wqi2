/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.enumerate;

import com.cy.work.common.utils.WkStringUtils;

import lombok.Getter;

/**
 * 日期類型
 *
 * @author jason_h
 */
public enum DateType {

	PREVIOUS_MONTH("上個月"),
	THIS_MONTH("本月份"),
	NEXT_MONTH("下個月"),
	TODAY("今日"),
	NULL("清空");

	/**
	 * 說明
	 */
	@Getter
	private final String descr;

	private DateType(String descr) {
		this.descr = descr;
	}

	/**
	 * @param name
	 * @return
	 */
	public static DateType safeValueOf(String name) {
		if (WkStringUtils.notEmpty(name)) {
			for (DateType type : DateType.values()) {
				if (type.name().equals(name)) {
					return type;
				}
			}
		}
		return null;
	}
}
