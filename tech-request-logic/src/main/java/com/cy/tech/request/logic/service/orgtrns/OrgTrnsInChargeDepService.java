package com.cy.tech.request.logic.service.orgtrns;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsDtVO;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsWorkVerifyVO;
import com.cy.tech.request.repository.require.TrnsBackupRepository;
import com.cy.tech.request.vo.enums.RequireTransProgramType;
import com.cy.tech.request.vo.enums.TrnsType;
import com.cy.tech.request.vo.require.TrnsBackup;
import com.cy.work.common.constant.WkConstants;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.Gson;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 *
 */
@NoArgsConstructor
@Service
@Slf4j
public class OrgTrnsInChargeDepService {

	// ========================================================================
	// 私有容器
	// ========================================================================
	@Getter
	@Setter
	private class OrgTrnsInChargeDepBackupTo implements Serializable {
		/**
         * 
         */
        private static final long serialVersionUID = -2590914658440017983L;
        private String requireSid;
		private Integer beforeInChargeDepSid;
	}

	// ========================================================================
	// 服務元件區
	// ========================================================================
	@Autowired
	private transient OrgTrnsService orgTrnsService;
	@Autowired
	private transient SearchService searchService;
	@Autowired
	private transient NativeSqlRepository nativeSqlRepository;
	@Autowired
	private transient SearchResultHelper searchResultHelper;
	@Autowired
	private transient RequireService requireService;
	@Autowired
	private transient TrnsBackupRepository trnsBackupRepository;

	@Autowired
	@Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
	private transient JdbcTemplate jdbcTemplate;

	// ========================================================================
	// 查詢方法區
	// ========================================================================
	public List<OrgTrnsDtVO> queryTrnsData(List<OrgTrnsWorkVerifyVO> allTrnsWorks) {
		// ====================================
		// 收集部門
		// ====================================
		Set<Integer> depSids = Sets.newHashSet();
		if (WkStringUtils.notEmpty(allTrnsWorks)) {
			for (OrgTrnsWorkVerifyVO vo : allTrnsWorks) {
				depSids.add(vo.getBeforeOrgSid());
			}
		}

		// ====================================
		// 查詢
		// ====================================
		// SQL
		StringBuffer varname1 = new StringBuffer();
		varname1.append("SELECT tr.require_sid            as sid, ");
		varname1.append("       tr.require_no             as caseNo, ");
		varname1.append("       tid.field_content         as theme, ");
		varname1.append("       tr.create_dt              as createDate_src, ");
		varname1.append("       tr.create_usr             as createUserSid, ");
		varname1.append("       tr.require_status         as caseStatus_Src, ");
		varname1.append("       tr.in_charge_usr          as inChargeUserSid, ");
		varname1.append("       tr.in_charge_dep          as inChargeDepSid ");
		varname1.append("FROM   tr_require tr ");

		varname1.append("INNER JOIN (SELECT tid.require_sid, ");
		varname1.append("                   tid.field_content ");
		varname1.append("            FROM   tr_index_dictionary tid ");
		varname1.append("            WHERE  1 = 1 ");
		varname1.append("                   AND tid.field_name = '主題') AS tid ");
		varname1.append("        ON tr.require_sid = tid.require_sid ");

		varname1.append("WHERE tr.in_charge_dep IN (:inChargeDep) ");
		varname1.append("ORDER BY tr.require_no ASC ");

		// 查詢參數
		Map<String, Object> parameters = Maps.newHashMap();
		parameters.put("inChargeDep", depSids);

		// 查詢
		List<OrgTrnsDtVO> results = nativeSqlRepository.getResultList(
		        varname1.toString(), parameters, OrgTrnsDtVO.class);

		log.info("查詢結果:{}筆", results.size());
		
		if (WkStringUtils.isEmpty(results)) {
			return Lists.newArrayList();
		}

		// ====================================
		// 準備顯示欄位
		// ====================================
		// 轉共同欄位
		this.orgTrnsService.prepareShowInfo(results, RequireTransProgramType.REQUIRE);

		// 轉私有欄位
		for (OrgTrnsDtVO resultVO : results) {
			
			resultVO.setRequireSid(resultVO.getSid());
			
			// 主題
			if (WkStringUtils.notEmpty(resultVO.getTheme())) {
				resultVO.setTheme(searchService.combineFromJsonStr(resultVO.getTheme()));
			}

			// 主責單位
			if (resultVO.getInChargeDepSid() != null) {
				resultVO.setInChargeDepName(searchResultHelper.prepareDepDescr(resultVO.getInChargeDepSid()));
			}
			// 主責單位負責人員
			if (resultVO.getInChargeUserSid() != null) {
				resultVO.setInChargeUserName(searchResultHelper.prepareUsrDescr(resultVO.getInChargeUserSid()));
			}
		}

		return results;
	}

	// ========================================================================
	// 轉檔方法區
	// ========================================================================
	/**
	 * @param selectedDtVO
	 * @param execDate
	 * @throws UserMessageException
	 */
	@Transactional(rollbackFor = Exception.class)
	public void processTrns(
			Map<Integer, Integer> depTrnsMapping,
	        OrgTrnsDtVO selectedDtVO,
	        Date execDate) throws UserMessageException {
		try {
			this.requireService.updateInCharge(
			        selectedDtVO.getRequireSid(),
			        depTrnsMapping.get(selectedDtVO.getInChargeDepSid()),
			        selectedDtVO.getInChargeUserSid(),
			        WkConstants.ADMIN_USER_SID,
			        execDate,
			        true);
		} catch (UserMessageException e) {
			throw new UserMessageException("找不到需求單資料檔");
		}
	}

	/**
	 * save tr_trns_backup
	 * @param selectedDtVOList
	 * @param effectiveDate
	 * @param execUserSid
	 * @param sysdate
	 */
	@Transactional(rollbackFor = Exception.class)
	public void processBackupInfo(
	        List<OrgTrnsDtVO> selectedDtVOList,
	        Date effectiveDate,
	        Integer execUserSid,
	        Date sysdate) {

		// ====================================
		// 準備記錄資料
		// ====================================
		List<OrgTrnsInChargeDepBackupTo> backupTos = Lists.newArrayList();
		for (OrgTrnsDtVO selectedDtVO : selectedDtVOList) {
			OrgTrnsInChargeDepBackupTo backupTo = new OrgTrnsInChargeDepBackupTo();
			backupTos.add(backupTo);
			backupTo.setRequireSid(selectedDtVO.getRequireSid());
			backupTo.setBeforeInChargeDepSid(selectedDtVO.getInChargeDepSid());
		}

		// ====================================
		// 準備備份資料檔
		// ====================================
		TrnsBackup trnsBackup = new TrnsBackup();
		trnsBackup.setCreatedUser(execUserSid);
		trnsBackup.setCreatedDate(sysdate);
		trnsBackup.setTrnsType(TrnsType.ORG_TRNS_IN_CHARGE_DEP.getValue());
		trnsBackup.setCustKey(WkDateUtils.formatDate(effectiveDate, WkDateUtils.YYYY_MM_DD2));
		trnsBackup.setBackData(new Gson().toJson(backupTos));

		// ====================================
		// save tr_trns_backup
		// ====================================
		trnsBackupRepository.save(trnsBackup);

	}
}
