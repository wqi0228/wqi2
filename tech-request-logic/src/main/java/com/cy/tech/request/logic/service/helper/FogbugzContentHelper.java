/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.helper;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.CommonService;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.TemplateService;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import com.cy.tech.request.vo.template.component.ComBase;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.work.common.utils.WkJsoupUtils;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author shaun
 */
@Slf4j
@Component
public class FogbugzContentHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1439677081938680833L;
    @Autowired
    private TemplateService tempService;
    @Autowired
    private UserService userService;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private WkStringUtils toolsString;
    @Autowired
    private WkJsoupUtils jsoupUtils;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

    /**
     * 產生需求單傳送內文<BR/>
     * from require entity
     *
     * @param require
     * @param executor
     * @param erpLinkUrl
     * @return
     */
    public String createRequireStrContent(Require require, User executor, String erpLinkUrl) {
        List<FieldKeyMapping> templateItem = tempService.loadTemplateFieldByMapping(require.getMapping());
        Map<String, ComBase> comValueMap = tempService.loadTemplateFieldValue(require);
        return this.createRequireStrContent(require, executor, erpLinkUrl, templateItem, comValueMap);
    }

    /**
     * 產生需求單傳送內文<BR/>
     * from page (速度較快，直接由頁面模版提供內容解析)
     *
     * @param require
     * @param executor
     * @param erpLinkUrl
     * @param selectTemplateItem
     * @param comValueMap
     * @return
     */
    public String createRequireStrContent(Require require, User executor, String erpLinkUrl, List<FieldKeyMapping> selectTemplateItem, Map<String, ComBase> comValueMap) {
        StringBuilder sb = new StringBuilder();
        sb.append(erpLinkUrl).append("\r\n");
        sb.append(this.createReqHeader(require, executor));
        sb.append(this.createReqContent(require.getRequireNo(), selectTemplateItem, comValueMap));
        return sb.toString();
    }

    /**
     * 需求單表頭資訊
     *
     * @param require
     * @return
     */
    private String createReqHeader(Require require, User executor) {
        StringBuilder sb = new StringBuilder();
        sb.append("--------------------------------------------").append(" \r\n");
        sb.append(toolsString.padEndEmpty("轉FB人員", 12, " ")).append("：").append(executor.getName()).append(" \r\n");
        sb.append(Strings.padEnd("需求單位", 6, '　')).append("：").append(orgService.showParentDep(orgService.findBySid(require.getCreateDep().getSid()))).append(" \r\n");
        sb.append(Strings.padEnd("需求人員", 6, '　')).append("：").append(userService.getUserName(require.getCreatedUser())).append(" \r\n");
        sb.append(Strings.padEnd("立單日期", 6, '　')).append("：").append(sdf.format(require.getCreatedDate())).append(" \r\n");
        sb.append(Strings.padEnd("單號", 6, '　')).append("：").append(require.getRequireNo()).append(" \r\n");
        sb.append(Strings.padEnd("需求製作進度", 6, '　')).append("：").append(this.getRequireStatusName(require)).append(" \r\n");
        sb.append(Strings.padEnd("需求類別", 6, '　')).append("：").append(require.getMapping().getBigName()).append(" \r\n");
        sb.append(Strings.padEnd("中類", 6, '　')).append("：").append(require.getMapping().getMiddleName()).append(" \r\n");
        sb.append(Strings.padEnd("小類", 6, '　')).append("：").append(require.getMapping().getSmallName()).append(" \r\n");
        sb.append(Strings.padEnd("緊急度", 6, '　')).append("：").append(this.getUrgencyTypeName(require)).append(" \r\n");
        sb.append("--------------------------------------------").append(" \r\n");
        return sb.toString();
    }

    private String getRequireStatusName(Require require) {
        if (require.getRequireStatus() == null) {
            return "";
        }
        return commonService.get(require.getRequireStatus());
    }

    private String getUrgencyTypeName(Require require) {
        if (require.getUrgency() == null) {
            return "";
        }
        return commonService.get(require.getUrgency());
    }

    /**
     * 建立模版內文
     *
     * @param selectTemplateItem
     * @param comValueMap
     * @return
     */
    private String createReqContent(String reqNo, List<FieldKeyMapping> selectTemplateItem, Map<String, ComBase> comValueMap) {
        int maxFieldNameLen = this.findMaxFieldNameLen(selectTemplateItem);
        StringBuilder sb = new StringBuilder();
        selectTemplateItem.stream()
                  .filter(each -> comValueMap.containsKey(each.getComId()))
                  .forEach(each -> {
                      sb.append("\r\n");
                      ComBase com = comValueMap.get(each.getComId());
                      if (com != null) {
                          sb.append(com.getCompositionText(each.getShowFieldName(), maxFieldNameLen));
                      } else {
                          log.error("cant find combase from comValueMap:require no = " + reqNo + " , comId =" + each.getComId());
                      }
                  });
        return sb.toString();
    }

    /**
     * 取得最長欄位名稱長度
     *
     * @param selectTemplateItem
     * @return
     */
    private int findMaxFieldNameLen(List<FieldKeyMapping> selectTemplateItem) {
        int maxLen = 0;
        for (FieldKeyMapping each : selectTemplateItem) {
            if (each.getShowFieldName()) {
                int fieldNameLen = Strings.isNullOrEmpty(each.getFieldName()) ? 0 : each.getFieldName().getBytes().length;
                maxLen = maxLen > fieldNameLen ? maxLen : fieldNameLen;
            }
        }
        return maxLen;
    }

    /**
     * 建立送測FB文字
     *
     * @param testInfo
     * @param executor
     * @param erpLinkUrl
     * @return
     */
    public String createWorkTestInfoStrContent(WorkTestInfo testInfo, User executor, String erpLinkUrl) {
        StringBuilder sb = new StringBuilder();
        sb.append(erpLinkUrl).append("\r\n");
        sb.append("--------------------------------------------").append(" \r\n");
        sb.append(Strings.padEnd("送測單位", 6, '　')).append("：").append(this.createSendTestDepStr(testInfo)).append(" \r\n");
        sb.append(Strings.padEnd("送測主題", 6, '　')).append("：").append(testInfo.getTheme()).append(" \r\n");
        sb.append(Strings.padEnd("送測內容", 6, '　')).append("：").append(" \r\n");
        sb.append(this.cssChangeToFBText(testInfo.getContentCss())).append(" \r\n");
        sb.append(Strings.padEnd("備註說明", 6, '　')).append("：").append(" \r\n");
        sb.append(this.cssChangeToFBText(testInfo.getNoteCss())).append(" \r\n");
        return sb.toString();
    }

    private String createSendTestDepStr(WorkTestInfo testInfo) {
        StringBuilder sb = new StringBuilder();
        testInfo.getSendTestDep().getValue().forEach((each) -> {
            sb.append(orgService.getOrgName(each)).append("、");
        });
        sb.delete(sb.lastIndexOf("、"), sb.length());
        return sb.toString();
    }

    private String cssChangeToFBText(String value) {
        if (Strings.isNullOrEmpty(value)) {
            return "";
        }
        String warpTemp = "%newLine%";
        String jumpLine = "\r\n";
        String temp = value;
        temp = temp.replaceAll(jumpLine, warpTemp);
        temp = temp.replaceAll("\r", warpTemp);
        temp = temp.replaceAll("\n", warpTemp);
        temp = temp.replaceFirst("<div>", warpTemp);
        temp = temp.replaceAll("</div>", warpTemp);
        temp = temp.replaceAll("<BR>", warpTemp);
        temp = temp.replaceAll("<br>", warpTemp);
        temp = temp.replaceAll("</BR>", warpTemp);
        temp = temp.replaceAll("</br>", warpTemp);
        temp = jsoupUtils.clearCssTag(temp);
        temp = temp.replaceAll(warpTemp, jumpLine);
        return temp;
    }

}
