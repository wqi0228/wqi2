/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.othset;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.RequireTraceService;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.repository.require.othset.OthSetAttachmentRepo;
import com.cy.tech.request.vo.enums.OthSetAttachmentBehavior;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.require.othset.OthSet;
import com.cy.tech.request.vo.require.othset.OthSetAttachment;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkAttachUtils;
import com.cy.work.common.vo.AttachmentService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * OthSet 附加檔案服務
 *
 * @author shaun
 */
@Component("othset_attach")
public class OthSetAttachService implements AttachmentService<OthSetAttachment, OthSet, String>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3047807071468770398L;
    @Value("${erp.attachment.root}")
    private String attachPath;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private UserService userService;
    @Autowired
    private RequireTraceService traceService;
    @Autowired
    private OthSetAttachmentRepo attachDao;
    @Autowired
    private WkAttachUtils attachUtils;

    @Transactional(readOnly = true)
    @Override
    public OthSetAttachment findBySid(String sid) {
        OthSetAttachment a = attachDao.findOne(sid);
        return a;
    }

    @Transactional(readOnly = true)
    @Override
    public List<OthSetAttachment> findAttachsByLazy(OthSet othset) {
        try {
            othset.getAttachments().size();
        } catch (LazyInitializationException e) {
            //log.debug("findAttachsByLazy lazy init error :" + e.getMessage());
            othset.setAttachments(this.findAttachs(othset));
        }
        return othset.getAttachments();
    }

    @Transactional(readOnly = true)
    @Override
    public List<OthSetAttachment> findAttachs(OthSet e) {
        if (Strings.isNullOrEmpty(e.getSid())) {
            if (e.getAttachments() == null) {
                e.setAttachments(Lists.newArrayList());
            }
            return e.getAttachments();
        }
        return attachDao.findByOthsetAndStatusAndHistoryIsNullOrderByCreatedDateDesc(e, Activation.ACTIVE);
    }

    @Override
    public String getAttachPath() {
        return attachPath;
    }

    @Override
    public String getDirectoryName() {
        return "tech-request";
    }

    @Override
    public OthSetAttachment createEmptyAttachment(String fileName, User executor, Org dep) {
        return (OthSetAttachment) attachUtils.createEmptyAttachment(new OthSetAttachment(), fileName, executor, dep);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public OthSetAttachment updateAttach(OthSetAttachment attach) {
        OthSetAttachment a = attachDao.save(attach);
        return a;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveAttach(OthSetAttachment attach) {
        attachDao.save(attach);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveAttach(List<OthSetAttachment> attachs) {
        attachDao.save(attachs);
    }

    @Override
    public String depAndUserName(OthSetAttachment attachment) {
        if (attachment == null) {
            return "";
        }
        User usr = WkUserCache.getInstance().findBySid(attachment.getCreatedUser().getSid());
        String parentDepStr = orgService.showParentDep(orgService.findBySid(usr.getPrimaryOrg().getSid()));
        return parentDepStr + "-" + usr.getName();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void linkRelation(List<OthSetAttachment> attachments, OthSet othset, User login) {
        attachments.forEach(each -> this.setRelation(each, othset));
        this.saveAttach(attachments);
        othset.setAttachments(this.findAttachs(othset));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void linkRelation(OthSetAttachment ra, OthSet othset, User login) {
        this.setRelation(ra, othset);
        this.updateAttach(ra);
    }

    private void setRelation(OthSetAttachment ra, OthSet othset) {
        ra.setOthset(othset);
        ra.setOsNo(othset.getOsNo());
        ra.setRequire(othset.getRequire());
        ra.setRequireNo(othset.getRequireNo());
        ra.setBehavior(OthSetAttachmentBehavior.FIRST_FILE);
        if (!ra.getKeyChecked()) {
            ra.setStatus(Activation.INACTIVE);
        }
        ra.setKeyChecked(Boolean.FALSE);
    }

    /**
     * 刪除
     *
     * @param attachments
     * @param attachment
     * @param othset
     * @param login
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void changeStatusToInActive(List<OthSetAttachment> attachments, OthSetAttachment attachment, OthSet othset, User login) {
        OthSetAttachment ra = (OthSetAttachment) attachment;
        ra.setStatus(Activation.INACTIVE);
        this.updateAttach(attachment);
        othset.setAttachments(this.findAttachs(othset));
        this.createDeleteTrace(attachment, othset, login);
    }

    private void createDeleteTrace(OthSetAttachment attachment, OthSet othset, User login) {
        String content = ""
                  + "檔案名稱：【" + attachment.getFileName() + "】\n"
                  + "上傳成員：【" + userService.getUserName(attachment.getCreatedUser()) + "】\n"
                  + "\n"
                  + "刪除來源：【其它設定資訊 － 單號：" + othset.getOsNo() + "】\n"
                  + "刪除成員：【" + login.getName() + "】\n"
                  + "刪除註記：" + attachment.getSid();
        traceService.createRequireTrace(othset.getRequire().getSid(), login, RequireTraceType.DELETE_ATTACHMENT, content);
    }

}
