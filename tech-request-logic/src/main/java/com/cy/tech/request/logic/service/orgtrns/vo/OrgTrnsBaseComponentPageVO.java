package com.cy.tech.request.logic.service.orgtrns.vo;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;

import com.cy.tech.request.vo.enums.RequireTransProgramType;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author allen1214_wu
 */
@NoArgsConstructor
public class OrgTrnsBaseComponentPageVO implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 3149455699528459968L;
    @Getter
    @Setter
    private OrgTrnsWorkVerifyVO selVerifyDtVO;
    // Data table 顯示資料
    @Getter
    @Setter
    private List<OrgTrnsDtVO> showDtVOList = Lists.newArrayList();

    // Data table 顯示資料
    @Getter
    @Setter
    private List<OrgTrnsDtVO> filteredVOList;

    // Data table 的選擇資料
    @Getter
    @Setter
    private List<OrgTrnsDtVO> selectedDtVOList = Lists.newArrayList();
    /**
     * 單據類別
     */
    @Getter
    @Setter
    private RequireTransProgramType trnsType;

    /**
     * 記錄傳入的所有組織異動資料
     */
    @Getter
    @Setter
    private List<OrgTrnsWorkVerifyVO> allTrnsWorks;

    /**
     * 案件狀態過濾選項
     * 
     * @return
     */
    public List<SelectItem> getCaseFilterItems() {
        if (WkStringUtils.isEmpty(showDtVOList)) {
            return Lists.newArrayList();
        }

        List<SelectItem> selectItems = Lists.newArrayList(new SelectItem());

        selectItems.addAll(showDtVOList.stream()
                .map(vo -> vo.getCaseStatusDesc()) // 取得狀態說明文字
                .distinct() // 過濾重複
                .map(desc -> new SelectItem(desc, desc))// 轉為 SelectItem
                .collect(Collectors.toList()));

        return selectItems;
    }
}
