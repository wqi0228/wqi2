/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.onpg;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.CommonService;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.RequireTraceService;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.repository.onpg.WorkOnpgAttachmentRepo;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.onpg.WorkOnpgAttachment;
import com.cy.tech.request.vo.onpg.WorkOnpgHistory;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgAttachmentBehavior;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkAttachUtils;
import com.cy.work.common.vo.AttachmentService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Work Onpg History 附加檔案服務
 *
 * @author shaun
 */
@Component("onpg_history_attach")
public class OnpgHistoryAttachService implements AttachmentService<WorkOnpgAttachment, WorkOnpgHistory, String>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1982261074946880722L;
    @Value("${erp.attachment.root}")
    private String attachPath;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private UserService userService;
    @Autowired
    private RequireTraceService traceService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private WorkOnpgAttachmentRepo attachDao;
    @Autowired
    private WkAttachUtils attachUtils;

    @Transactional(readOnly = true)
    @Override
    public WorkOnpgAttachment findBySid(String sid) {
        WorkOnpgAttachment a = attachDao.findOne(sid);
        return a;
    }

    @Transactional(readOnly = true)
    @Override
    public List<WorkOnpgAttachment> findAttachsByLazy(WorkOnpgHistory history) {
        try {
            history.getAttachments().size();
        } catch (LazyInitializationException e) {
            //log.debug("findAttachsByLazy lazy init error :" + e.getMessage());
            history.setAttachments(this.findAttachs(history));
        }
        return history.getAttachments();
    }

    @Transactional(readOnly = true)
    @Override
    public List<WorkOnpgAttachment> findAttachs(WorkOnpgHistory e) {
        if (Strings.isNullOrEmpty(e.getSid())) {
            if (e.getAttachments() == null) {
                e.setAttachments(Lists.newArrayList());
            }
            return e.getAttachments();
        }
        return attachDao.findByHistoryAndStatusOrderByCreatedDateDesc(e, Activation.ACTIVE);
    }

    @Override
    public String getAttachPath() {
        return attachPath;
    }

    @Override
    public String getDirectoryName() {
        return "tech-request";
    }

    @Override
    public WorkOnpgAttachment createEmptyAttachment(String fileName, User executor, Org dep) {
        return (WorkOnpgAttachment) attachUtils.createEmptyAttachment(new WorkOnpgAttachment(), fileName, executor, dep);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public WorkOnpgAttachment updateAttach(WorkOnpgAttachment attach) {
        WorkOnpgAttachment a = attachDao.save(attach);
        return a;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveAttach(WorkOnpgAttachment attach) {
        attachDao.save(attach);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveAttach(List<WorkOnpgAttachment> attachs) {
        attachDao.save(attachs);
    }

    @Override
    public String depAndUserName(WorkOnpgAttachment attachment) {
        if (attachment == null) {
            return "";
        }
        User usr = WkUserCache.getInstance().findBySid(attachment.getCreatedUser().getSid());
        String parentDepStr = orgService.showParentDep(orgService.findBySid(usr.getPrimaryOrg().getSid()));
        return parentDepStr + "-" + usr.getName();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void linkRelation(List<WorkOnpgAttachment> attachments, WorkOnpgHistory e, User login) {
        attachments.forEach(each -> this.setRelation(each, e));
        this.saveAttach(attachments);
        e.setAttachments(this.findAttachs(e));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void linkRelation(WorkOnpgAttachment ra, WorkOnpgHistory e, User login) {
        this.setRelation(ra, e);
        this.updateAttach(ra);
    }

    private void setRelation(WorkOnpgAttachment ra, WorkOnpgHistory e) {
        ra.setOnpg(e.getOnpg());
        ra.setOnpgNo(e.getOnpg().getOnpgNo());
        ra.setSourceType(e.getOnpg().getSourceType());
        ra.setSourceSid(e.getOnpg().getSourceSid());
        ra.setSourceNo(e.getOnpg().getSourceNo());
        ra.setHistory(e);
        ra.setBehavior(this.findBehavior(e));
        if (!ra.getKeyChecked()) {
            ra.setStatus(Activation.INACTIVE);
        }
        ra.setKeyChecked(Boolean.FALSE);
    }

    private WorkOnpgAttachmentBehavior findBehavior(WorkOnpgHistory e) {
        if (e.getBehavior() == null) {
            return null;
        }

        switch (e.getBehavior()) {
        case CHECK_RECORD:
            return WorkOnpgAttachmentBehavior.CHECK_RECORD_FILE;
        case GM_REPLY:
            return WorkOnpgAttachmentBehavior.GM_REPLY_FILE;
        case QA_REPLY:
            return WorkOnpgAttachmentBehavior.QA_REPLY_FILE;
        case GENERAL_REPLY:
            return WorkOnpgAttachmentBehavior.GENERAL_REPLY_FILE;
        case CHECK_RECORD_REPLY:
            return WorkOnpgAttachmentBehavior.CHECK_RECORD_REPLY_FILE;
        case CHECK_MEMO:
            return WorkOnpgAttachmentBehavior.CHECK_MEMO_FILE;
        case CANCEL_ONPG:
            return WorkOnpgAttachmentBehavior.CANCEL_ONPG_FILE;
        default:
            break;
        }
        return null;
    }

    /**
     * 刪除
     *
     * @param attachments
     * @param attachment
     * @param history
     * @param login
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void changeStatusToInActive(List<WorkOnpgAttachment> attachments, WorkOnpgAttachment attachment, WorkOnpgHistory history, User login) {
        WorkOnpgAttachment ra = (WorkOnpgAttachment) attachment;
        ra.setStatus(Activation.INACTIVE);
        this.updateAttach(attachment);
        history.setAttachments(this.findAttachs(history));
        this.createDeleteTrace(attachment, history, login);
    }

    private void createDeleteTrace(WorkOnpgAttachment attachment, WorkOnpgHistory history, User login) {
        Require require = new Require();
        require.setSid(history.getSourceSid());
        require.setRequireNo(history.getSourceNo());
        String content = ""
                + "檔案名稱：【" + attachment.getFileName() + "】\n"
                + "上傳成員：【" + userService.getUserName(attachment.getCreatedUser()) + "】\n"
                + "\n"
                + "刪除來源：【ON程式 － 單號：" + history.getOnpgNo() + " － " + commonService.get(history.getBehavior()) + "】\n"
                + "刪除成員：【" + login.getName() + "】\n"
                + "刪除註記：" + attachment.getSid();
        traceService.createRequireTrace(require.getSid(), login, RequireTraceType.DELETE_ATTACHMENT, content);
    }

}
