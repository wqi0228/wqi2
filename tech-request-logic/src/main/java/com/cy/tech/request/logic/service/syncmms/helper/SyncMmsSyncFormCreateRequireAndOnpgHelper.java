/**
 * 
 */
package com.cy.tech.request.logic.service.syncmms.helper;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.service.RequireTraceService;
import com.cy.tech.request.logic.service.TemplateService;
import com.cy.tech.request.logic.service.helper.RequireFlowHelper;
import com.cy.tech.request.logic.service.onpg.OnpgService;
import com.cy.tech.request.logic.service.syncmms.to.SyncFormTo;
import com.cy.tech.request.logic.vo.WorkOnpgTo;
import com.cy.tech.request.repository.template.CategoryKeyMappingRepository;
import com.cy.tech.request.vo.log.LogSyncMmsActionType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.template.CategoryKeyMapping;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import com.cy.tech.request.vo.template.component.ComBase;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.exception.alert.TechRequestAlertException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Service
@Slf4j
public class SyncMmsSyncFormCreateRequireAndOnpgHelper {

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    transient private CategoryKeyMappingRepository categoryKeyMappingRepository;
    @Autowired
    transient private TemplateService templateService;
    @Autowired
    transient private RequireFlowHelper requireFlowHelper;
    @Autowired
    private transient OnpgService onpgService;
    @Autowired
    private transient RequireTraceService requireTraceService;
    @Autowired
    private transient SyncMmsSyncFormProcessDispatcher syncMmsCommonLogic;

    // ========================================================================
    // 主方法
    // ========================================================================
    /**
     * 新增需求單和ON程式單
     * 
     * @param syncFormTo MMS 維護單資料
     * @throws SystemOperationException 檢核失敗時拋出
     */
    // @Transactional(rollbackFor = { Exception.class })
    public void process(
            SyncFormTo syncFormTo)
            throws SystemOperationException {

        // 定義處理型態
        syncFormTo.setLogSyncMmsActionType(LogSyncMmsActionType.CREATE_REQ_AND_ONPG);

        // ====================================
        // 查詢需求單單據類別資料
        // ====================================
        CategoryKeyMapping categoryKeyMapping = findCategoryKeyMapping(syncFormTo.getLoginCompID());
        // 取得模版欄位
        List<FieldKeyMapping> fieldKeyMappings = this.templateService.loadTemplateFieldByMapping(categoryKeyMapping);
        if (WkStringUtils.isEmpty(fieldKeyMappings)) {
            String errorMessage = "取得模版欄位失敗keyMappingID:[" + categoryKeyMapping.getId() + "]";
            log.error(errorMessage);
            throw new TechRequestAlertException(errorMessage, syncFormTo.getLoginCompID());
        }
        // 設定模版內容值
        Map<String, ComBase> comValueMap = this.templateService.createNewDocValueMap(fieldKeyMappings);

        // ====================================
        // 主單資料
        // ====================================
        Require require = new Require();
        require.setMapping(categoryKeyMapping);

        // 主責單位
        require.setInChargeDep(syncFormTo.getCreateUser().getPrimaryOrg().getSid());
        require.setInChargeUsr(syncFormTo.getCreateUser().getSid());
        // 緊急度
        require.setUrgency(categoryKeyMapping.getMiddle().getUrgency());
        // 期望完成日 => (維護日期)
        // 在此設定無效 於 requireService.initRequire 處理
        // require.setRequireEstablishDate(syncFormTo.getMaintenanceDate());
        // 廳主
        require.setCustomer(syncFormTo.getWorkCustomer());
        // 提出客戶
        require.setAuthor(syncFormTo.getWorkCustomer());

        // 主題
        this.syncMmsCommonLogic.setRequireTheme(comValueMap, syncFormTo.getTheme(), syncFormTo.getLoginCompID());
        // 內容
        this.syncMmsCommonLogic.setRequireContent(comValueMap, syncFormTo.getContent(), syncFormTo.getLoginCompID());

        this.templateService.changeThemeValueByCustomer(syncFormTo.getWorkCustomer(), comValueMap);

        // ====================================
        // 新增需求單和ON程式單
        // ====================================
        try {
            this.requireFlowHelper.processForCreateSaveInNoTransactional(
                    require,
                    syncFormTo.getCheckItemTypes(),
                    comValueMap,
                    null,
                    null,
                    syncFormTo,
                    syncFormTo.getCreateUser(),
                    new Date());

            syncFormTo.setRequire(require);
            syncFormTo.setRequireNo(require.getRequireNo());

        } catch (UserMessageException e) {
            // 為了 roll back
            WkCommonUtils.logWithStackTraceByFullClassNamePrefix(InfomationLevel.ERROR, e.getMessage(), "com.cy.");
            throw new TechRequestAlertException(e.getMessage(), syncFormTo.getLoginCompID());
        } catch (Exception e) {
            // 為了 roll back
            String errorMessage = WkMessage.EXECTION + e.getMessage();
            log.error(errorMessage, e);
            throw new TechRequestAlertException(e.getMessage(), syncFormTo.getLoginCompID());
        }

        // ====================================
        // 取得ON程式單號
        // ====================================
        List<WorkOnpgTo> workOnpgTos = this.onpgService.findByRequireSid(require.getSid());
        if (WkStringUtils.isEmpty(workOnpgTos)) {
            String errorMessage = "未找到對應ON程式單!需求單 sid:[" + require.getSid() + "]";
            log.error(errorMessage);
            throw new TechRequestAlertException(errorMessage, syncFormTo.getLoginCompID());
        }
        // 應該僅會產生一筆
        syncFormTo.setWorkOnpgTo(workOnpgTos.get(0));
        syncFormTo.setOnpgNo(workOnpgTos.get(0).getOnpgNo());

        // ====================================
        // 寫需求單追蹤
        // ====================================
        this.requireTraceService.createSyncMmsTrace(syncFormTo);

    }

    // ========================================================================
    // 內部方法
    // ========================================================================
    /**
     * @param loginCompId 登入公司別ID(可為空)
     * @return CategoryKeyMapping
     * @throws SystemOperationException 錯誤時拋出
     */
    private CategoryKeyMapping findCategoryKeyMapping(String loginCompId) throws SystemOperationException {
        // 取得單據類別ID
        String keyMappingID = SyncMmsParamHelper.getInstance().getReqCategoryKeyMappingID();
        // 查詢單據類別資料
        List<CategoryKeyMapping> categoryKeyMappings = this.categoryKeyMappingRepository.findById(
                keyMappingID);
        // 取得最大的版本號
        return categoryKeyMappings.stream()
                .max(Comparator.comparing(CategoryKeyMapping::getVersion))
                .get();
    }

}
