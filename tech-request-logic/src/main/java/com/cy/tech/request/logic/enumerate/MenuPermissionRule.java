/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.enumerate;

import com.cy.work.common.utils.WkStringUtils;

import lombok.Getter;

/**
 * @author allen1214_wu
 * 選單權限規則項目
 */
public enum MenuPermissionRule {

	GM_LV1("一般GM"),
	GM_LV2("GM管理員"),
	INSPECTOR("檢查員"),
	REQ_MANAGER("需求管理者"),
	;

	/**
	 * 說明
	 */
	@Getter
	private final String descr;

	private MenuPermissionRule(String descr) {
		this.descr = descr;
	}

	/**
	 * @param name
	 * @return
	 */
	public static MenuPermissionRule safeValueOf(String name) {
		if (WkStringUtils.notEmpty(name)) {
			for (MenuPermissionRule type : MenuPermissionRule.values()) {
				if (type.name().equals(name)) {
					return type;
				}
			}
		}
		return null;
	}
}
