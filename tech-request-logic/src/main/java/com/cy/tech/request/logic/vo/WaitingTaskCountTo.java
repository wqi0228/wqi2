package com.cy.tech.request.logic.vo;

import lombok.Getter;
import java.io.Serializable;

/**
 * @author allen1214_wu
 *
 */
public class WaitingTaskCountTo implements Serializable {

    
    /**
     * 
     */
    private static final long serialVersionUID = 1479090967150673907L;

    /** 待辦名稱 */
    @Getter
    private String toDoName;
    
    /** 待辦筆數 */
    @Getter
    private String toDoCount;
    
    /** 待辦連結 */
    @Getter
    private String toDoUrl;
    
    /** 待辦連結 */
    @Getter
    private String todoClz;

    /**
     * @param toDoName
     * @param toDoCount
     * @param toDoUrl
     * @param todoClz
     */
    public WaitingTaskCountTo(String toDoName, int toDoCount, String toDoUrl, String todoClz) {
        this.toDoName = toDoName;
        this.toDoCount = toDoCount + "";
        this.toDoUrl = toDoUrl;
        this.todoClz = todoClz;
    }

}
