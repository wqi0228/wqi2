/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.logic.service.helper.AssignSendSearchHelper;
import com.cy.tech.request.logic.vo.AssignBacksageTo;
import com.cy.tech.request.logic.vo.GroupItemTo;
import com.cy.tech.request.logic.vo.GroupItemTo.GroupItemToType;
import com.cy.tech.request.repository.require.AssignSendInfoRepo;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.require.AssignSendInfo;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.value.to.SetupInfoTo;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.value.to.ItemsCollectionDiffTo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * 分派通知服務
 *
 * @author shaun
 */
@Slf4j
@Component
public class AssignSendInfoService implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4908682215535131996L;
    private static AssignSendInfoService instance;

    public static AssignSendInfoService getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        AssignSendInfoService.instance = this;
    }

    @Autowired
    private AssignSendInfoRepo assignSendInfoRepo;
    @Autowired
    private CommonService commonService;
    @PersistenceContext
    transient private EntityManager em;
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private AssignSendSearchHelper rassHelper;
    @Autowired
    private UserService userService;

    /**
     * 插入失敗時記錄錯誤訊息
     *
     * @param requireSid
     * @param modifyItems
     */
    private void backstageInsertFaild(String requireSid, List<AssignBacksageTo> modifyItems) {
        modifyItems.stream().filter(to -> to.getRequireSid().equals(requireSid)).forEach(to -> to.setSucess(false));
    }

    private AssignBacksageTo bulidAssingBacksageTo(Object[] array) {
        int idx = 0;
        String requireSid = (String) array[idx++];
        String requireNo = (String) array[idx++];
        String reqType = (String) array[idx++];
        Date createDt = (Date) array[idx++];
        Integer sourceOrgSid = (Integer) array[idx++];
        return new AssignBacksageTo(requireSid, requireNo, RequireStatusType.valueOf(reqType), createDt, sourceOrgSid, true);
    }

    /**
     * @param requireSid
     * @param requireNo
     * @param executor
     * @param createDate
     * @param type
     * @return
     */
    private AssignSendInfo createEmptyInfo(
            String requireSid,
            String requireNo,
            User executor,
            Date createDate,
            AssignSendType type) {

        AssignSendInfo rasi = new AssignSendInfo();
        rasi.setCreatedUser(executor);
        rasi.setCreatedDate(createDate);
        rasi.setRequireSid(requireSid);
        rasi.setRequireNo(requireNo);
        rasi.setStatus(Activation.ACTIVE);
        rasi.setType(type);
        return rasi;
    }

    /**
     * 建立空的分派通知資訊entity
     * 
     * @param requireSid
     * @param requireNo
     * @param executor
     * @param createDate
     * @param type
     * @param depSids
     * @return
     */
    public AssignSendInfo createInfo(
            String requireSid,
            String requireNo,
            User executor,
            Date createDate,
            AssignSendType type,
            List<Integer> depSids) {

        // 初始化 entity
        AssignSendInfo rasi = this.createEmptyInfo(requireSid, requireNo, executor, createDate, type);

        // 設定單位資訊
        SetupInfoTo info = new SetupInfoTo();
        if (WkStringUtils.notEmpty(depSids)) {
            for (Integer depSid : depSids) {
                info.getDepartment().add(depSid + "");
            }
        }
        rasi.setInfo(info);
        // 現行系統 無 user 、group 資訊 故略過

        return rasi;
    }

    /**
     * 後台進行分派時記錄下載
     *
     * @param items
     * @param sourceOrg
     * @param targetOrg
     * @return
     */
    public String downloadHandlerMsg(List<AssignBacksageTo> items, Integer sourceOrg, Integer targetOrg) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        StringBuilder sb = new StringBuilder();
        sb.append("來源對應部門：").append(orgService.getOrgName(sourceOrg)).append("\n");
        sb.append("目標新增部門：").append(orgService.getOrgName(targetOrg)).append("\n");
        sb.append("處理筆數：").append(items.size()).append("\n");
        sb.append("失敗筆數：").append(items.stream().filter(item -> !item.isSucess()).count()).append("\n");
        sb.append("記錄日期：").append(sdf.format(new Date())).append("\n");
        sb.append("------------------------------------\n");
        items.forEach(each -> {
            sb.append("需求單ID：").append(each.getRequireSid()).append(" ");
            sb.append("需求單號：").append(each.getRequireNo()).append(" ");
            sb.append("製作進度：").append(commonService.get(each.getReqType())).append(" ");
            sb.append("建單日期：").append(sdf.format(each.getCreateDt())).append(" ");
            sb.append("異動成功：").append(each.isSucess()).append(" ");
            sb.append("\n");
        });
        return sb.toString();
    }

    /**
     * 後台進行分派時記錄下載
     * 
     * @param items
     * @param recoveryOrg
     * @return
     */
    public String downloadRecoveryMsg(List<AssignBacksageTo> items, Integer recoveryOrg) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        StringBuilder sb = new StringBuilder();
        sb.append("還原部門：").append(orgService.getOrgName(recoveryOrg)).append("\n");
        sb.append("處理筆數：").append(items.size()).append("\n");
        sb.append("失敗筆數：").append(items.stream().filter(item -> !item.isSucess()).count()).append("\n");
        sb.append("記錄日期：").append(sdf.format(new Date())).append("\n");
        sb.append("------------------------------------\n");
        items.forEach(each -> {
            sb.append("需求單ID：").append(each.getRequireSid()).append(" ");
            sb.append("需求單號：").append(each.getRequireNo()).append(" ");
            sb.append("製作進度：").append(commonService.get(each.getReqType())).append(" ");
            sb.append("建單日期：").append(sdf.format(each.getCreateDt())).append(" ");
            sb.append("還原成功：").append(each.isSucess()).append(" ");
            sb.append("\n");
        });
        return sb.toString();
    }

    /**
     * 尋找 被分派單位的工程師以及被分派單位主管(包含部級主管 與 處級主管)，(含向下單位)
     *
     * @param require
     * @return
     */
    @Transactional(readOnly = true)
    public Set<User> findAllAssignChildDepOfUsers(Require require) {
        List<AssignSendInfo> rasi = this.findByRequireAndType(require, AssignSendType.ASSIGN);
        Set<String> usersStr = Sets.newHashSet();
        Set<String> deptsStr = Sets.newHashSet();
        rasi.forEach(assign -> {
            usersStr.addAll(assign.getInfo().getUsers());
            deptsStr.addAll(assign.getInfo().getDepartment());
            assign.getGroupInfo().getGroups().forEach(group -> {
                usersStr.addAll(group.getInfo().getUsers());
                deptsStr.addAll(group.getInfo().getDepartment());
            });
        });
        Set<User> result = Sets.newHashSet();
        for (String orgStr : deptsStr) {
            Org org = orgService.findBySid(orgStr);
            result.addAll(orgService.findOrgManagers(org));
            result.addAll(userService.findByPrimaryOrg(Activation.ACTIVE, org));
            List<Org> childs = orgService.findChildOrgs(org);
            for (Org child : childs) {
                if (child != null) {
                    result.addAll(userService.findByPrimaryOrg(Activation.ACTIVE, child));
                }
            }

            // for (Iterator iterator = childs.iterator(); iterator.hasNext();) {
            // Org child = (Org) iterator.next();
            //
            // }
            // for (Org child : childs) {
            //
            // }
        }
        // deptsStr.forEach(orgStr -> {
        //
        // });
        result.addAll(WkUserCache.getInstance().findBySidStrs(usersStr));
        return result;
    }

    /**
     * 尋找 被分派單位的工程師以及被分派單位主管(包含部級主管 與 處級主管)
     *
     * @param require
     * @return
     */
    @Transactional(readOnly = true)
    public Set<User> findAllAssignDepOfUsers(Require require) {
        List<AssignSendInfo> rasi = this.findByRequireAndType(require, AssignSendType.ASSIGN);
        Set<String> usersStr = Sets.newHashSet();
        Set<String> deptsStr = Sets.newHashSet();
        rasi.forEach(assign -> {
            usersStr.addAll(assign.getInfo().getUsers());
            deptsStr.addAll(assign.getInfo().getDepartment());
            assign.getGroupInfo().getGroups().forEach(group -> {
                usersStr.addAll(group.getInfo().getUsers());
                deptsStr.addAll(group.getInfo().getDepartment());
            });
        });
        Set<User> result = Sets.newHashSet();
        deptsStr.forEach(orgStr -> {
            Org org = orgService.findBySid(orgStr);
            result.addAll(orgService.findOrgManagers(org));
            result.addAll(userService.findByPrimaryOrg(Activation.ACTIVE, org));
        });
        result.addAll(WkUserCache.getInstance().findBySidStrs(usersStr));
        return result;
    }

    /**
     * 查詢分派單位資料 (v7.0以後資料應該只有一筆)
     *
     * @param requireSid
     * @return
     */
    @Transactional(readOnly = true)
    public List<AssignSendInfo> findAssignByRequireSid(String requireSid) {
        return assignSendInfoRepo.findByRequireSidAndTypeAndStatus(requireSid, AssignSendType.ASSIGN, Activation.ACTIVE);
    }

    /**
     * 查詢分派單位
     *
     * @param requireSid
     * @return
     */
    public Set<Integer> findAssignDepsByRequireSid(String requireSid) {
        Map<AssignSendType, AssignSendInfo> resultMap = this.findByRequireSidAndStatus(requireSid);
        // 取得原分派資料
        AssignSendInfo assignInfo = resultMap.get(AssignSendType.ASSIGN);
        List<Integer> assignDepSids = this.prepareDeps(assignInfo);

        if (WkStringUtils.isEmpty(assignDepSids)) {
            return Sets.newHashSet();
        }

        return Sets.newHashSet(assignDepSids);
    }

    /**
     * 查詢需異動分派資料
     * <p/>
     * 1.查詢來源組織<BR/>
     * 2.過濾目標組織<BR/>
     * 3.過濾製作進度<BR/>
     * 4.過濾日期
     * <p/>
     * <p>
     * 當來源組織群組已包含目標組織時略過
     *
     * @param sourceOrg
     * @param targetOrg
     * @param reqStatus
     * @param start
     * @param end
     * @param transNos
     * @return
     */
    public List<AssignBacksageTo> findBackstageModifyItems(Integer sourceOrg, Integer targetOrg, List<String> reqStatus, Date start, Date end,
            List<String> transNos) {
        Map<String, Object> param = Maps.newHashMap();
        String findQuery = "SELECT tr.require_sid,tr.require_no,tr.require_status,tr.create_dt,assi.dep_sid FROM tr_assign_send_search_info assi "
                + "  LEFT JOIN tr_require tr ON tr.require_sid = assi.require_sid "
                + "  WHERE assi.type = 0 "
                + "    AND assi.dep_sid = :sourceOrg "
                + "    AND assi.require_sid NOT IN ( "
                + "             SELECT ai.require_sid FROM tr_assign_send_info ai "
                + "               JOIN (SELECT MAX(ai.create_dt) as maxTime FROM tr_assign_send_info ai GROUP BY ai.require_sid) "
                + "                 AS temp ON ai.create_dt IN (temp.maxTime) "
                + "              WHERE ai.require_sid = assi.require_sid "
                + "                AND ai.info LIKE :targetOrg "
                + "                AND ai.type = 0 "
                + "                               ) "
                + "    AND tr.require_status IN (:reqStatus)";
        if (transNos.isEmpty()) {
            findQuery += "    AND tr.create_dt BETWEEN :start AND :end ";
            param.put("start", start);
            param.put("end", end);
        } else {
            findQuery += "    AND tr.require_no in (:transNos) ";
            param.put("transNos", transNos);
        }
        findQuery += " ORDER BY tr.create_dt DESC ";
        param.put("sourceOrg", sourceOrg);
        param.put("targetOrg", "%\"" + targetOrg + "\"%");
        param.put("reqStatus", reqStatus);
        Query query = em.createNativeQuery(findQuery);
        param.entrySet().stream().forEach(entry -> query.setParameter(entry.getKey(), entry.getValue()));
        @SuppressWarnings("unchecked")
        List<Object[]> result = query.getResultList();
        return result.stream()
                .map(objs -> this.bulidAssingBacksageTo(objs))
                .collect(Collectors.toList());
    }

    /**
     * 查詢後台分派還原物件
     *
     * @param recoveryOrg
     * @param recoveryReqNos
     * @return
     */
    public List<AssignBacksageTo> findBackstageModifyRecoveryItems(Integer recoveryOrg, List<String> recoveryReqNos) {
        String findQuery = "SELECT tr.require_sid,tr.require_no,tr.require_status,tr.create_dt,assi.dep_sid FROM tr_assign_send_search_info assi "
                + "  LEFT JOIN tr_require tr ON tr.require_sid = assi.require_sid "
                + "  WHERE assi.type = 0 "
                + "    AND assi.dep_sid = :recoveryOrg "
                + "    AND assi.require_no IN (:recoveryReqNos) "
                + " ORDER BY tr.create_dt DESC ";
        Map<String, Object> param = Maps.newHashMap();
        param.put("recoveryOrg", recoveryOrg);
        param.put("recoveryReqNos", recoveryReqNos);
        Query query = em.createNativeQuery(findQuery);
        param.entrySet().stream().forEach(entry -> query.setParameter(entry.getKey(), entry.getValue()));
        @SuppressWarnings("unchecked")
        List<Object[]> result = query.getResultList();
        return result.stream()
                .map(obj -> this.bulidAssingBacksageTo(obj))
                .collect(Collectors.toList());
    }

    /**
     * 取回需求單對應的分派通知設定
     *
     * @param require
     * @param typeOne
     * @return
     */
    @Transactional(readOnly = true)
    public List<AssignSendInfo> findByRequireAndType(Require require, AssignSendType typeOne) {
        return this.findByRequireAndType(require.getSid(), typeOne);
    }

    /**
     * 取回需求單對應的分派通知設定
     *
     * @param requireSid
     * @param typeOne
     * @return
     */
    @Transactional(readOnly = true)
    public List<AssignSendInfo> findByRequireAndType(String requireSid, AssignSendType typeOne) {
        return assignSendInfoRepo.findByRequireSidAndTypeOrderByCreatedDateDesc(requireSid, typeOne);
    }

    /**
     * @param requireSid
     * @return
     */
    @Transactional(readOnly = true)
    public Map<AssignSendType, AssignSendInfo> findByRequireSidAndStatus(String requireSid) {
        // 初始化處理結果容器
        Map<AssignSendType, AssignSendInfo> resultMap = Maps.newHashMap();

        // 查詢
        List<AssignSendInfo> qryLsit = assignSendInfoRepo.findByRequireSidAndStatusOrderByCreatedDateDesc(requireSid, Activation.ACTIVE);
        if (WkStringUtils.isEmpty(qryLsit)) {
            return resultMap;
        }

        // 依據類別(分派/通知)做分類.且僅取日期最大的一筆
        for (AssignSendInfo assignSendInfo : qryLsit) {
            // 已存在則跳過 (已取到日期最大的一筆)
            if (resultMap.containsKey(assignSendInfo.getType())) {
                continue;
            }
            resultMap.put(assignSendInfo.getType(), assignSendInfo);
        }

        return resultMap;
    }

    /**
     * 取回分派或通知資訊 (for v7, v7.0.0 後應該僅會有一筆)
     *
     * @param requireSid
     * @param typeOne
     * @return
     */
    @Transactional(readOnly = true)
    public AssignSendInfo findByRequireSidAndType(String requireSid, AssignSendType typeOne) {
        return this.assignSendInfoRepo.findTopByRequireSidAndTypeAndStatusOrderByCreatedDateDesc(requireSid, typeOne, Activation.ACTIVE);
    }

    /**
     * 查詢多筆單據的通知/分派單位設定
     *
     * @param requireSids
     * @param type
     * @return
     * @throws UserMessageException
     */
    public Map<String, List<Integer>> findDepSidByRequireSidAndStatus(
            List<String> requireSids,
            AssignSendType type) throws UserMessageException {

        if (WkStringUtils.isEmpty(requireSids)) {
            throw new UserMessageException("傳入 requireSids 為空!", InfomationLevel.WARN);
        }

        // ====================================
        // 查詢
        // ====================================
        List<AssignSendInfo> results = this.assignSendInfoRepo.findByRequireSidInAndTypeAndStatus(
                requireSids,
                type,
                Activation.ACTIVE);

        if (WkStringUtils.isEmpty(results)) {
            return Maps.newHashMap();
        }

        // ====================================
        // 以 RequireSid 分類
        // ====================================
        Map<String, List<AssignSendInfo>> mapByRequireSid = results.stream()
                .collect(Collectors.groupingBy(
                        AssignSendInfo::getRequireSid,
                        Collectors.mapping(
                                v -> v,
                                Collectors.toList())));

        // ====================================
        // 整理資料
        // ====================================
        Map<String, List<Integer>> depSidsMapByRequireSid = Maps.newHashMap();
        for (String requireSid : mapByRequireSid.keySet()) {
            // 取得單一需求單下的所有分派/通知設定檔
            List<AssignSendInfo> infos = mapByRequireSid.get(requireSid);
            if (WkStringUtils.isEmpty(infos)) {
                continue;
            }

            // 取得日期最大的一筆, 最終設定 (舊資料可能有多筆)
            AssignSendInfo lastInfo = infos
                    .stream()
                    .max(Comparator.comparing(AssignSendInfo::getCreatedDate))
                    .orElse(null);

            if (lastInfo == null) {
                continue;
            }

            // 取得設定的單位
            // 以 require sid 為 key 放入結果 map
            depSidsMapByRequireSid.put(
                    requireSid,
                    this.prepareDeps(lastInfo));

        }

        return depSidsMapByRequireSid;
    }

    /**
     * 取回需求單對應的分派通知設定 (排除停用)
     *
     * @param requireSid
     * @param typeOne
     * @return
     */
    public AssignSendInfo findLastActiveByRequireAndType(String requireSid, AssignSendType typeOne) {
        // 查詢
        List<AssignSendInfo> result = assignSendInfoRepo.findByRequireSidAndTypeAndStatusOrderByCreatedDateDesc(requireSid, typeOne, Activation.ACTIVE);
        if (WkStringUtils.isEmpty(result)) {
            return null;
        }
        // 最新的一筆才是設定資料
        return result.get(0);
    }

    /**
     * @param requireSid
     * @param typeOne
     * @return
     */
    public Set<Integer> findLastActiveDepSidByRequireAndType(String requireSid, AssignSendType typeOne) {
        // 查詢
        List<AssignSendInfo> result = assignSendInfoRepo.findByRequireSidAndTypeAndStatusOrderByCreatedDateDesc(requireSid, typeOne, Activation.ACTIVE);

        if (WkStringUtils.isEmpty(result)) {
            return Sets.newHashSet();
        }

        List<Integer> results = this.prepareDeps(result.get(0));
        if (WkStringUtils.isEmpty(results)) {
            return Sets.newHashSet();
        }

        return Sets.newHashSet(results);
    }

    /**
     * 取得最新分派單位 by 需求單
     *
     * @param require
     * @param typeOne
     * @return
     */
    @Transactional(readOnly = true)
    public AssignSendInfo findNewestByRequireAndType(Require require, AssignSendType typeOne) {
        List<AssignSendInfo> resultList = assignSendInfoRepo.findByRequireSidAndTypeOrderByCreatedDateDesc(require.getSid(), typeOne);
        if (CollectionUtils.isNotEmpty(resultList)) {
            return resultList.get(0);
        }
        return null;
    }

    @Transactional(readOnly = true)
    public List<Org> findOrgByGroupItems(List<GroupItemTo> items) {
        return items.stream().filter(each -> each.getType().equals(GroupItemToType.DEPT) || each.getType().equals(GroupItemToType.GROUP_DEPT))
                .map(each -> orgService.findBySid(each.getKey()))
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<User> findUserByGroupItems(List<GroupItemTo> items) {
        return items.stream().filter(each -> each.getType().equals(GroupItemToType.EMPLOY) || each.getType().equals(GroupItemToType.GROUP_EMPLOY))
                .map(each -> WkUserCache.getInstance().findBySid(each.getKey()))
                .collect(Collectors.toList());
    }

    /**
     * 後台需進行分派內容顯示
     *
     * @param items
     * @param sourceOrg
     * @param targetOrg
     * @param transNos
     * @param isCnt
     * @return
     */
    public String htmlHandlerMsg(List<AssignBacksageTo> items, Integer sourceOrg, Integer targetOrg, List<String> transNos, boolean isCnt) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        StringBuilder sb = new StringBuilder();
        items.forEach(each -> {
            if (!transNos.isEmpty()) {
                transNos.remove(each.getRequireNo());
            }
            sb.append("需求單ID：").append(each.getRequireSid()).append("<BR/>");
            sb.append("需求單號：").append(each.getRequireNo()).append("<BR/>");
            sb.append("製作進度：").append(commonService.get(each.getReqType())).append("<BR/>");
            sb.append("建單日期：").append(sdf.format(each.getCreateDt())).append("<BR/>");
            if (!isCnt) {
                sb.append("異動成功：").append(each.isSucess()).append("\"<BR/>");
            }
            sb.append("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░<BR/>");
        });
        String handlerCntStr = isCnt ? "預計處理筆數：" : "處理筆數：";
        sb.append(handlerCntStr).append(items.size()).append("<BR/>");
        sb.append("來源對應部門：").append(orgService.getOrgName(sourceOrg)).append("<BR/>");
        sb.append("目標新增部門：").append(orgService.getOrgName(targetOrg)).append("<BR/>");
        if (!transNos.isEmpty()) {
            sb.append("轉換單號有誤：").append(transNos.stream().collect(Collectors.joining("、"))).append("<BR/>");
        }
        if (!isCnt) {
            sb.append("失敗筆數：").append(items.stream().filter(item -> !item.isSucess()).count()).append("<BR/>");
        }
        return sb.toString();
    }

    /**
     * 後台需進行分派還原內容顯示
     *
     * @param items
     * @param recoveryOrgSid
     * @param isCnt
     * @return
     */
    public String htmlHandlerMsgByRecovery(List<AssignBacksageTo> items, Integer recoveryOrgSid, boolean isCnt) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        StringBuilder sb = new StringBuilder();
        items.forEach(each -> {
            sb.append("需求單ID：").append(each.getRequireSid()).append("<BR/>");
            sb.append("需求單號：").append(each.getRequireNo()).append("<BR/>");
            sb.append("製作進度：").append(commonService.get(each.getReqType())).append("<BR/>");
            sb.append("建單日期：").append(sdf.format(each.getCreateDt())).append("<BR/>");
            if (!isCnt) {
                sb.append("還原成功：").append(each.isSucess()).append("\"<BR/>");
            }
            sb.append("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░<BR/>");
        });
        String handlerCntStr = isCnt ? "預計處理筆數：" : "處理筆數：";
        sb.append(handlerCntStr).append(items.size()).append("<BR/>");
        sb.append("還原部門：").append(orgService.getOrgName(recoveryOrgSid)).append("<BR/>");
        if (!isCnt) {
            sb.append("失敗筆數：").append(items.stream().filter(item -> !item.isSucess()).count()).append("<BR/>");
        }
        return sb.toString();
    }

    /**
     * 執行單筆異動資料
     *
     * @param entity
     * @param info
     * @param targetOrg
     */
    @Transactional(rollbackFor = Exception.class)
    public void insetModify(AssignSendInfo entity, SetupInfoTo info, Integer targetOrg) {
        assignSendInfoRepo.updateSetupInfo(entity, info);
        rassHelper.createSearchInfoByBackstage(entity, targetOrg);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<String> minusAssignDept(List<Integer> recoveryOrgs, List<String> requireSids) {
        List<String> resultList = Lists.newArrayList();
        List<AssignSendInfo> modifyEntitys = assignSendInfoRepo.findBackstageModifyItems(requireSids);
        for (AssignSendInfo entity : modifyEntitys) {
            try {
                SetupInfoTo info = entity.getInfo();
                for (Integer orgSid : recoveryOrgs) {
                    if (info.getDepartment().contains(orgSid.toString())) {
                        info.getDepartment().remove(orgSid.toString());
                        this.removeModify(entity, info, orgSid);
                    }
                }
                resultList.add(entity.getRequireNo());
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        } ;

        return resultList;
    }

    /**
     * 執行後台插入分派
     *
     * @param targetOrg
     * @param modifyItems
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public List<AssignBacksageTo> modifyBackstageModifyItems(Integer targetOrg, List<AssignBacksageTo> modifyItems) {
        List<String> requireSids = modifyItems.stream().map(AssignBacksageTo::getRequireSid).collect(Collectors.toList());
        List<AssignSendInfo> modifyEntitys = assignSendInfoRepo.findBackstageModifyItems(requireSids);
        modifyEntitys.forEach(entity -> {
            try {
                SetupInfoTo info = entity.getInfo();
                info.getDepartment().add(targetOrg.toString());
                this.insetModify(entity, info, targetOrg);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                this.backstageInsertFaild(entity.getRequireSid(), modifyItems);
            }
        });
        return modifyItems;
    }

    @Transactional(rollbackFor = Exception.class)
    public List<AssignBacksageTo> modifyBackstageRecoveryItems(Integer recoveryOrg, List<AssignBacksageTo> modifyItems) {
        List<String> requireSids = modifyItems.stream().map(AssignBacksageTo::getRequireSid).collect(Collectors.toList());
        List<AssignSendInfo> modifyEntitys = assignSendInfoRepo.findBackstageModifyItems(requireSids);
        modifyEntitys.forEach(entity -> {
            try {
                SetupInfoTo info = entity.getInfo();
                if (info.getDepartment().contains(recoveryOrg.toString())) {
                    info.getDepartment().remove(recoveryOrg.toString());
                }
                this.removeModify(entity, info, recoveryOrg);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                this.backstageInsertFaild(entity.getRequireSid(), modifyItems);
            }
        });
        return modifyItems;
    }

    /**
     * 取得設定物件中的部門
     *
     * @param info
     * @return
     */
    public List<Integer> prepareDeps(AssignSendInfo info) {

        if (info == null
                || info.getInfo() == null
                || WkStringUtils.isEmpty(info.getInfo().getDepartment())) {
            return Lists.newArrayList();
        }

        return this.prepareDeps(info.getInfo());
    }

    public List<Integer> prepareDeps(SetupInfoTo setupInfoTo) {

        if (setupInfoTo == null
                || WkStringUtils.isEmpty(setupInfoTo.getDepartment())) {
            return Lists.newArrayList();
        }

        Set<Integer> depSids = Sets.newHashSet();

        for (String depSid : setupInfoTo.getDepartment()) {
            try {
                depSids.add(Integer.parseInt(depSid));
            } catch (Exception e) {
                log.warn("傳入的 depsid 非數字:[" + depSid + "]");
            }
        }

        return Lists.newArrayList(depSids);
    }

    /**
     * 比對
     *
     * @param oldDepSids
     * @param newDepSids
     * @return
     */
    public ItemsCollectionDiffTo<Integer> prepareModifyDepsInfo(
            List<Integer> oldDepSids,
            List<Integer> newDepSids) {

        // 新單位為 null 時, 代表未修改 (本次分派/通知 無異動此類別資料僅舊資料會發生)
        if (newDepSids == null) {
            return new ItemsCollectionDiffTo<Integer>(Lists.newArrayList(), Lists.newArrayList());
        }

        // 比對新舊單位
        return WkCommonUtils.itemCollectionDiff(oldDepSids, newDepSids);
    }

    /**
     * 執行單筆異動資料(移除)
     *
     * @param entity
     * @param info
     * @param targetOrg
     */
    @Transactional(rollbackFor = Exception.class)
    public void removeModify(AssignSendInfo entity, SetupInfoTo info, Integer targetOrg) {
        assignSendInfoRepo.updateSetupInfo(entity, info);
        rassHelper.removeSearchInfoByBackstage(entity.getRequireSid(), targetOrg);
    }

    /**
     * 轉需求單（ONPG）增加分派資訊
     *
     * @param require
     * @param executor
     * @param depSid
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateAssignSendInfoByIssueTransOnpg(
            Require require,
            User executor,
            String depSid) {

        List<AssignSendInfo> assignInfos = this.findByRequireAndType(require, AssignSendType.ASSIGN);
        SetupInfoTo dbInfo = new SetupInfoTo();
        if (assignInfos != null && !assignInfos.isEmpty()) {
            dbInfo = assignInfos.get(0).getInfo();
        }
        List<String> hasAssignOrgSids = dbInfo.getDepartment().stream()
                .map(each -> each)
                .collect(Collectors.toList());

        if (!hasAssignOrgSids.contains(depSid)) {
            hasAssignOrgSids.add(depSid);
        }

        orgService.findChildOrgsByBasicOrg(orgService.findBySid(depSid))
                .forEach(each -> {
                    String key = String.valueOf(each.getSid());
                    if (!hasAssignOrgSids.contains(key)) {
                        hasAssignOrgSids.add(key);
                    }
                });

        if (hasAssignOrgSids.isEmpty()) {
            return;
        }
        AssignSendInfo obj = new AssignSendInfo();
        obj.setRequireSid(require.getSid());
        obj.setRequireNo(require.getRequireNo());
        obj.setType(AssignSendType.ASSIGN);
        obj.setCreatedUser(executor);
        obj.setCreatedDate(new Date());
        obj.setStatus(Activation.ACTIVE);
        SetupInfoTo info = new SetupInfoTo();
        info.setDepartment(hasAssignOrgSids);
        obj.setInfo(info);
        assignSendInfoRepo.save(obj);
    }

}
