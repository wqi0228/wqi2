package com.cy.tech.request.logic.service.pps6;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cy.tech.request.repository.pps6.Pps6ModelerExecSettingRepository;
import com.cy.tech.request.repository.pps6.Pps6ModelerFlowRepository;
import com.cy.tech.request.vo.pps6.Pps6ModelerExecSetting;
import com.cy.tech.request.vo.pps6.Pps6ModelerExecSettingVO;
import com.cy.tech.request.vo.pps6.Pps6ModelerFlow;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * PPS6 Modeler 流程定義資料檔 Service
 * 
 * @author allen1214_wu
 */
@Service
@Slf4j
public class Pps6ModelerExecSettingService implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -9026209550834816189L;
    // ========================================================================
	// 服務區
	// ========================================================================
	@Autowired
	private transient Pps6ModelerFlowRepository pps6ModelerFlowRepository;
	@Autowired
	private transient Pps6ModelerExecSettingRepository pps6ModelerExecSettingRepository;
	@PersistenceContext
	private transient EntityManager entityManager;

	// ========================================================================
	// 方法區
	// ========================================================================

	/**
	 * 查詢全部
	 * 
	 * @return
	 */
	public List<Pps6ModelerExecSettingVO> findAll() {
		// ====================================
		// 查詢全部
		// ====================================
		List<Pps6ModelerExecSetting> entities = this.pps6ModelerExecSettingRepository.findAll();
		if (WkStringUtils.isEmpty(entities)) {
			return Lists.newArrayList();
		}

		// ====================================
		// 轉 VO
		// ====================================
		return entities.stream()
		        .map(entity -> new Pps6ModelerExecSettingVO(entity))
		        .collect(Collectors.toList());
	}

	/**
	 * @param flowId
	 * @param execSettingVOs
	 * @param execUserSid
	 * @throws SystemOperationException
	 */
	// @Transactional(rollbackFor = Exception.class)
	public void deleteInsert(
	        String flowId,
	        List<Pps6ModelerExecSettingVO> execSettingVOs,
	        Integer execUserSid) throws SystemOperationException {

		// ====================================
		// 檢查主檔是否存在
		// ====================================
		Pps6ModelerFlow pps6ModelerFlow = this.pps6ModelerFlowRepository.findByFlowId(flowId);
		if (pps6ModelerFlow == null) {
			throw new SystemOperationException("pps6_modeler_flow 檔不存在流程代號flowId:[" + flowId + "]", InfomationLevel.WARN);
		}

		log.info("流程:[{},{}] 開始更新執行設定檔 pps6_modeler_exec_setting", flowId, pps6ModelerFlow.getFlowName());

		// ====================================
		// delete
		// ====================================
		// this.pps6ModelerExecSettingRepository.deleteByFlowSid(pps6ModelerFlow.getSid());
		List<Pps6ModelerExecSetting> pps6ModelerExecSettings = this.pps6ModelerExecSettingRepository.findByFlowSid(pps6ModelerFlow.getSid());
		if (WkStringUtils.notEmpty(pps6ModelerExecSettings)) {
			this.pps6ModelerExecSettingRepository.delete(pps6ModelerExecSettings);
			entityManager.flush();
			log.info("流程:[{}] 刪除[{}]", flowId, pps6ModelerExecSettings.size());
		}
		

		// ====================================
		// insert
		// ====================================
		if (WkStringUtils.isEmpty(execSettingVOs)) {
			log.info("流程:[{}] 傳入執行設定為空, 不做 insert", flowId);
			return;
		}

		// 轉 Pps6ModelerExecSetting
		List<Pps6ModelerExecSetting> execSettings = Lists.newArrayList();
		Date execDate = new Date();
		for (Pps6ModelerExecSettingVO vo : execSettingVOs) {
			Pps6ModelerExecSetting entity = new Pps6ModelerExecSetting();

			BeanUtils.copyProperties(vo, entity);
			/*
			 * entity.setExecId(vo.getExecId()); entity.setVariable(vo.isVariable());
			 * entity.setExecType(vo.getExecType()); entity.setName(vo.getName());
			 * entity.setUserDescr(vo.getUserDescr());
			 * entity.setModelerNodeName(vo.getModelerNodeName());
			 **/

			entity.setFlowSid(pps6ModelerFlow.getSid());
			entity.setCreatedUser(execUserSid);
			entity.setCreatedDate(execDate);

			execSettings.add(entity);
		}

		this.pps6ModelerExecSettingRepository.save(execSettings);
		log.info("流程:[{}] 新增[{}]筆", flowId, execSettings.size());

	}

}
