/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * QA待審單據頁面顯示vo (search33.xhtml)
 *
 * @author marlow_chen
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "testInfoNo" })
public class Search33View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2459867885841340881L;
    /** 送測日 */
    private Date testDate;
    /** 填單人所歸屬的部門sid */
    private Integer createDeptSid;
    /** 建立者 */
    private Integer createdUser;
    /** 送測主題 */
    private String testTheme;
    /** 待審狀態 */
    private String qaAuditStatus;
    /** 預計完成日 */
    private Date establishDate;
    /** 納入排程 */
    private String qaScheduleStatus;
    /** 審核日期 */
    private Date qaAuditDate;
    /** 建立日期 */
    private Date createdDate;
    /** 預計上線日 */
    private Date expectOnlineDate;
    /** 送測單號 */
    private String testInfoNo;
    /** 填單人所歸屬的部門 */
    private String createDept;
    /** 本地端連結網址 */
    private String localUrlLink;
    /** 送測狀態 */
    private String testInfoStatus;
    /** 審核人員 */
    private Integer approveUser;

}
