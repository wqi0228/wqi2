/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import com.cy.commons.vo.User;
import com.cy.work.common.enums.UrgencyType;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author shaun
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Require04View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 229616400665950920L;
    /** 需求單sid */
    private String requireSid;
    /** 緊急度 */
    private UrgencyType urgency;
    /** 立單人員 */
    private User createdUser;
    /** 本地端連結網址 */
    private String localUrlLink;

}
