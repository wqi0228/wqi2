/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.search.view.Search09View;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

/**
 * @author jason_h
 */
@Service
public class Search09QueryService implements QueryService<Search09View>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7135001205821653007L;
    @Autowired
    private URLService urlService;
    @Autowired
    private SearchService searchHelper;

    @PersistenceContext
    transient private EntityManager em;

    @Override
    public List<Search09View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {
        
        // ====================================
        // 查詢
        // ====================================

        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        @SuppressWarnings("unchecked")
        List<Object[]> result = query.getResultList();
        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if (WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }
        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();
        List<Search09View> viewResult = Lists.newArrayList();
        for (int i = 0; i < result.size(); i++) {

            Search09View v = new Search09View();

            Object[] record = (Object[]) result.get(i);
            int index = 0;
            String sid = (String) record[index++];
            String requireNo = (String) record[index++];
            String requireTheme = (String) record[index++];
            Integer urgency = (Integer) record[index++];
            Date prototypeCreatedDate = (Date) record[index++];
            Integer prototypeCreateDep = (Integer) record[index++];
            Integer prototypeCreatedUser = (Integer) record[index++];
            Date approvalDate = (Date) record[index++];
            Date finishDate = (Date) record[index++];
            String bigName = (String) record[index++];
            String middleName = (String) record[index++];
            String smallName = (String) record[index++];
            Integer requireCreateDep = (Integer) record[index++];
            Integer requireCreatedUser = (Integer) record[index++];
            String defaultSignedName = (String) record[index++];
            String instanceStatus = (String) record[index++];
            Integer version = (Integer) record[index++];
            String prototypeStatus = (String) record[index++];
            String ptTheme = (String) record[index++];
            // 處理共通欄位 (一定要擺在『取值』最後面, 否則 index 會不對)
            v.prepareCommonColumn(record, index);

            v.setSid(sid);
            v.setRequireNo(requireNo);
            v.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
            v.setUrgency(UrgencyType.values()[urgency]);
            v.setPrototypeCreatedDate(prototypeCreatedDate);
            v.setPrototypeCreateDep(prototypeCreateDep);
            v.setPrototypeCreatedUser(prototypeCreatedUser);
            v.setApprovalDate(approvalDate);
            v.setFinishDate(finishDate);
            v.setBigName(bigName);
            v.setMiddleName(middleName);
            v.setSmallName(smallName);
            v.setRequireCreateDep(requireCreateDep);
            v.setRequireCreatedUser(requireCreatedUser);
            v.setDefaultSignedName(defaultSignedName);
            v.setInstanceStatus(InstanceStatus.fromPaperCode(instanceStatus));
            v.setVersion(version);
            v.setPrototypeStatus(PtStatus.valueOf(prototypeStatus));
            v.setPtTheme(ptTheme);
            v.setLocalUrlLink(urlService.createLocalUrlLinkParamForTab(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(execUserSid, v.getRequireNo(), 1),
                    URLService.URLServiceAttr.URL_ATTR_TAB_PT, v.getSid()));

            viewResult.add(v);
        }
        // 解析資料-結束
        usageRecord.parserDataEnd();

        return viewResult;
    }
}
