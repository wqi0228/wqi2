/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.helper;

import com.cy.tech.request.vo.enums.ComType;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import com.cy.tech.request.vo.template.component.ComBase;
import com.cy.tech.request.vo.template.component.ComBaseInteractive;
import com.cy.tech.request.vo.template.component.ComSelectItemTypeOne;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 模版工廠 <BR/>
 * 1.建立單據的資料控制Map<BR/>
 * 2.
 *
 * @author shaun
 */
@Slf4j
@Component
public class TemplateComponentHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6792455664958113609L;

    /**
     * 建立新單的資料控制Map
     *
     * @param template
     * @return
     */
    public Map<String, ComBase> createNewDocValueMap(List<FieldKeyMapping> template) {
        Map<String, ComBase> dataControllMap = Maps.newHashMap();
        template.forEach(each -> {
            ComBase controllObj = this.createEmptyComBaseRefByComType(each.getFieldComponentType());
            if (controllObj != null) {
                controllObj.setDefValue(each);
                dataControllMap.put(each.getComId(), controllObj);
            }
        });
        return dataControllMap;
    }

    /**
     * 建立空的模版欄位物件對應元件
     *
     * @param type
     * @return
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public ComBase createEmptyComBaseRefByComType(ComType type) {
        try {
            Class typeClz = type.getComClz();
            return (ComBase) typeClz.getConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
            log.error("建立元件控制實例失敗。" + e.getMessage(), e);
            return null;
        }
    }

    /**
     * 載入模版欄位預設資料
     *
     * @param fields
     * @return
     */
    public Map<String, ComBase> loadTemplateFieldComBaseMap(List<FieldKeyMapping> fields, boolean isTriggerOnChangeEvent) {
        Map<String, ComBase> dataControllMap = Maps.newHashMap();
        fields.forEach(each -> {
            ComBase obj = this.createEmptyComBaseRefByComType(each.getFieldComponentType());
            obj.setDefValue(each);
            //only for DBA category, custom rule
            if (obj instanceof ComSelectItemTypeOne && isTriggerOnChangeEvent) {
                ((ComSelectItemTypeOne) obj).getDisplayComponents().add(ComBaseInteractive.class.getSimpleName());
            }
            dataControllMap.put(obj.getComId(), obj);
        });
        return dataControllMap;
    }
}
