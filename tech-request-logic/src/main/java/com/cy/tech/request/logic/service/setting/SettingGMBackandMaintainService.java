/**
 * 
 */
package com.cy.tech.request.logic.service.setting;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.service.RequireCheckItemService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.utils.ProcessLog;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 *
 */
@Slf4j
@Service
public class SettingGMBackandMaintainService implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 5974042607471929432L;
    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private transient RequireService requireService;
    @Autowired
    private transient RequireCheckItemService requireCheckItemService;
    
    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 單據系統別轉換作業
     * 
     * @throws UserMessageException 找不到單號時拋出
     */
    public void f01_process(
            String requireNo,
            List<RequireCheckItemType> selectedCheckItemTypes,
            Integer execUserSid) throws UserMessageException {

        ProcessLog processLog = new ProcessLog(requireNo, "", "後台維護-異動系統別");
        log.info(processLog.prepareStartLog());

        // ====================================
        // 查詢主檔
        // ====================================
        // 查詢
        Require require = this.requireService.findByReqNo(requireNo);

        if (require == null) {
            throw new UserMessageException("輸入的需求單號：『" + requireNo + "』找不到!", InfomationLevel.WARN);
        }

        // ====================================
        // save
        // ====================================
        this.requireCheckItemService.updateCheckItemTypes(
                require.getRequireNo(), 
                selectedCheckItemTypes,
                true,
                execUserSid, 
                new Date());

        log.info(processLog.prepareCompleteLog());
    }
}
