package com.cy.tech.request.logic.vo;

import java.util.List;

import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 *
 */
@Setter
@Getter
public class CheckItmesInfoByRequireTo {

    /**
     * 檢查資訊 for 單據檢視畫面
     */
    private String checkItemsInfoForFormView = "";

    /**
     * 檢查資訊
     */
    private String checkConfirmInfo = "";
    /**
     * 檢查確認人員
     */
    private List<Integer> checkConfirmUserSids = Lists.newArrayList();
    /**
     * 單據檢查項目
     */
    private List<RequireCheckItemType> checkItemTypes = Lists.newArrayList();
    /**
     * 為異常單據:項目都檢查完了, 但是製作進度還留在待檢查
     */ 
    private boolean exceptionCase = false;

}
