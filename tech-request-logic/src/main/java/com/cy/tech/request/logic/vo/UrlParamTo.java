/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 網址JSON物件
 *
 * @author shaun
 */
@Data
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UrlParamTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4974891980164337221L;

    /** 單號 或 任何 key 值 */
    @JsonProperty(value = "no")
    private String no;

    /** 需求單號 */
    @JsonProperty(value = "reqNo")
    private String reqNo;

    /** Deadline 到期日 */
    @JsonProperty(value = "dl")
    private Date dl;

    /** 可閱組織 */
    @JsonProperty(value = "org")
    private List<Integer> org;

    /** 可閱成員 */
    @JsonProperty(value = "user")
    private List<Integer> user;
    
    public UrlParamTo(){}
    
    public UrlParamTo(String no){
        this.no = no;
    }
}
