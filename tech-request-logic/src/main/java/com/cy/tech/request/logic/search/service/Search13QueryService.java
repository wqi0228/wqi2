/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.search.view.Search13View;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.worktest.enums.WorkTestInfoStatus;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author jason_h
 */
@Service
@Slf4j
public class Search13QueryService implements QueryService<Search13View>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5010925303016681685L;
    @Autowired
    private URLService urlService;
    @Autowired
    private WkJsonUtils jsonUtils;
    @Autowired
    private SearchService searchHelper;
    @PersistenceContext
    transient private EntityManager em;

    @SuppressWarnings("unchecked")
    @Override
    public List<Search13View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {

        // ====================================
        // 查詢
        // ====================================

        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        List<Object[]> result = query.getResultList();
        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if (WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();
        List<Search13View> viewResult = Lists.newArrayList();
        for (int i = 0; i < result.size(); i++) {

            Search13View v = new Search13View();

            Object[] record = (Object[]) result.get(i);
            int index = 0;
            String sid = (String) record[index++];
            String requireNo = (String) record[index++];
            String requireTheme = (String) record[index++];
            Integer urgency = (Integer) record[index++];
            Date createdDate = (Date) record[index++];
            Integer createDep = (Integer) record[index++];
            Integer createdUser = (Integer) record[index++];
            String testTheme = (String) record[index++];
            String bigName = (String) record[index++];
            String middleName = (String) record[index++];
            String smallName = (String) record[index++];
            Integer requireDepSid = (Integer) record[index++];
            Integer requireUserSid = (Integer) record[index++];
            String testinfoStatus = (String) record[index++];
            String readReason = (String) record[index++];
            String testDescription = (String) record[index++];
            Date establishDate = (Date) record[index++];
            Date finishDate = (Date) record[index++];
            Date readUpdateDate = (Date) record[index++];
            String testinfoNo = (String) record[index++];
            String sendTestDep = (String) record[index++];
            String requireStatus = (String) record[index++];
            String qaAuditStatus = (String) record[index++];
            Date testDate = (Date) record[index++];
            Date expectOnlineDate = (Date) record[index++];

            // 處理共通欄位 (一定要擺在『取值』最後面, 否則 index 會不對)
            v.prepareCommonColumn(record, index);

            v.setSid(sid);
            v.setRequireNo(requireNo);
            v.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
            v.setUrgency(UrgencyType.values()[urgency]);
            v.setCreatedDate(createdDate);
            v.setCreateDep(createDep);
            v.setCreateDepName(WkOrgUtils.findNameBySid(createDep));
            v.setCreatedUser(createdUser);
            v.setTestTheme(testTheme);
            v.setBigName(bigName);
            v.setMiddleName(middleName);
            v.setSmallName(smallName);
            v.setRequireDep(requireDepSid);
            v.setRequireUser(requireUserSid);
            v.setTestinfoStatus(WorkTestStatus.safeValueOf(testinfoStatus));
            v.setQaAuditStatus(WorkTestInfoStatus.safeValueOf(qaAuditStatus));
            v.setWaitReadReasonType(WaitReadReasonType.safeValueOf(readReason));
            v.setTestDescription(testDescription);
            v.setEstablishDate(establishDate);
            v.setFinishDate(finishDate);
            v.setReadUpdateDate(readUpdateDate);
            v.setTestinfoNo(testinfoNo);
            v.setTestDate(testDate);
            v.setExpectOnlineDate(expectOnlineDate);
            try {
                v.setSendTestDep(jsonUtils.fromJson(sendTestDep, JsonStringListTo.class));
            } catch (IOException ex) {
                log.error(ex.getMessage(), ex);
            }
            v.setRequireStatus(RequireStatusType.valueOf(requireStatus));
            v.setLocalUrlLink(urlService.createLocalUrlLinkParamForTab(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(execUserSid, v.getRequireNo(), 1),
                    URLService.URLServiceAttr.URL_ATTR_TAB_ST, v.getSid()));
            viewResult.add(v);
        }
        // 解析資料-結束
        usageRecord.parserDataEnd();
        return viewResult;
    }
}
