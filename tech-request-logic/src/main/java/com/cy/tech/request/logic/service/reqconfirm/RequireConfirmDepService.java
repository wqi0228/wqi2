package com.cy.tech.request.logic.service.reqconfirm;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.reqconfirm.to.KeepConfirmDepOwnerTo;
import com.cy.tech.request.logic.vo.RequireConfirmDepVO;
import com.cy.tech.request.repository.require.RequireConfirmDepRepository;
import com.cy.tech.request.vo.constants.CacheConstants;
import com.cy.tech.request.vo.enums.ReqConfirmDepProcType;
import com.cy.tech.request.vo.enums.ReqConfirmDepProgStatus;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireConfirmDep;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkConstants;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 需求完成確認單位檔 service
 * 
 * @author allen1214_wu
 */
@Service
@NoArgsConstructor
@Slf4j
public class RequireConfirmDepService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2739092522289366238L;

    // ========================================================================
    // 服務元件區
    // ========================================================================
    @Autowired
    private transient NativeSqlRepository nativeSqlRepository;
    @Autowired
    private transient RequireConfirmDepHistoryService requireConfirmDepHistoryService;
    @Autowired
    private transient RequireConfirmDepRepository requireConfirmDepRepository;
    @Autowired
    private transient RequireService requireService;
    @Autowired
    private transient WorkBackendParamHelper workBackendParamHelper;
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private JdbcTemplate jdbcTemplate;

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * 因開立新子單, 而倒回進度狀態 【已完成->進行中】
     * 
     * @param requireNo
     * @param requireSid
     * @param execUserSid
     * @param createDepSid  單據歸屬單位 SID
     * @param operationName
     * @param sysDate
     */
    public void changeProgStatusCompleteByAddNewSubCase(
            String requireNo,
            String requireSid,
            Integer execUserSid,
            Integer createDepSid,
            String operationName,
            Date sysDate) {

        // ====================================
        // 由執行者單位, 收束為需求完成確認單位 sid
        // ====================================
        Integer requireConfirmDepSid = this.prepareRequireConfirmDepSid(createDepSid);

        // ====================================
        // 查詢狀態為已完成者
        // ====================================
        // 查詢
        RequireConfirmDep requireConfirmDep = this.requireConfirmDepRepository.findByRequireSidAndDepSidAndProgStatus(
                requireSid,
                requireConfirmDepSid,
                ReqConfirmDepProgStatus.COMPLETE);

        if (requireConfirmDep == null) {
            return;
        }

        // ====================================
        // 執行重新開啟流程
        // ====================================
        this.reOpenProgStatus(
                requireConfirmDep,
                requireNo,
                execUserSid,
                sysDate,
                ReqConfirmDepProcType.ADD_SUB_CASE,
                "因【" + operationName + "】重新開啟確認流程");
    }

    /**
     * 因開立新子單, 而倒回進度狀態 【已完成->進行中】
     * 
     * @param requireNo
     * @param requireSid
     * @param execUserSid
     * @param createDepSid  單據歸屬單位 SID
     * @param operationName
     * @param sysDate
     */
    public void reOpenProgStatusByUpdateSubCase(
            String requireNo,
            String requireSid,
            Integer execUserSid,
            Integer createDepSid,
            String operationName,
            Date sysDate) {

        // ====================================
        // 由執行者單位, 收束為需求完成確認單位 sid
        // ====================================
        Integer requireConfirmDepSid = this.prepareRequireConfirmDepSid(createDepSid);

        // ====================================
        // 查詢狀態為已完成者
        // ====================================
        List<ReqConfirmDepProgStatus> progStatusList = Lists.newArrayList(ReqConfirmDepProgStatus.values())
                .stream()
                .filter(ReqConfirmDepProgStatus::isInFinish)
                .collect(Collectors.toList());
        // 查詢
        RequireConfirmDep requireConfirmDep = this.requireConfirmDepRepository.findByRequireSidAndDepSidAndProgStatusIn(
                requireSid,
                requireConfirmDepSid,
                progStatusList);

        if (requireConfirmDep == null) {
            return;
        }

        // ====================================
        // 執行重新開啟流程
        // ====================================
        this.reOpenProgStatus(
                requireConfirmDep,
                requireNo,
                execUserSid,
                sysDate,
                ReqConfirmDepProcType.REOPEN_BY_REQ_COMPLETE_ROLLBACK,
                "因【" + operationName + "】重新開啟確認流程");
    }

    /**
     * 將狀態由無須確認，倒回前一個狀態 (限定強制需求完成使用)
     * 
     * @param requireSid
     */
    public void changeProgStatusPassByRequireCompleteRollback(
            String requireNo,
            String requireSid,
            Integer execUserSid,
            Date sysDate,
            String traceMessage) {

        // ====================================
        // 查詢為『無須確認』的資料檔
        // ====================================
        // 查詢
        List<RequireConfirmDep> requireConfirmDeps = this.requireConfirmDepRepository.queryByRequireSidAndProgStatus(
                requireSid,
                Lists.newArrayList(ReqConfirmDepProgStatus.PASS));

        if (WkStringUtils.isEmpty(requireConfirmDeps)) {
            return;
        }

        // ====================================
        // 逐筆處理
        // ====================================
        for (RequireConfirmDep requireConfirmDep : requireConfirmDeps) {

            this.reOpenProgStatus(
                    requireConfirmDep,
                    requireNo,
                    execUserSid,
                    sysDate,
                    ReqConfirmDepProcType.REOPEN_BY_REQ_COMPLETE_ROLLBACK,
                    "因反需求完成，再次開啟確認流程!");

        }
    }

    /**
     * 將狀態為未完成者, 轉為無須確認 (限定強制需求完成使用)
     * 
     * @param requireSid
     */
    public void changeProgStatusToPassByForceComplete(
            String requireNo,
            String requireSid,
            Integer execUserSid,
            Date sysDate) {

        // ====================================
        // 查詢需要轉為『無須確認』的資料檔
        // ====================================
        // 取得狀態屬於『未完成』的進度類型
        List<ReqConfirmDepProgStatus> filterStatus = Lists.newArrayList(ReqConfirmDepProgStatus.values()).stream()
                .filter(ptStatus -> !ptStatus.isInFinish())
                .collect(Collectors.toList());
        // 查詢
        List<RequireConfirmDep> requireConfirmDeps = this.requireConfirmDepRepository.queryByRequireSidAndProgStatus(
                requireSid,
                filterStatus);

        // ====================================
        // 逐筆處理
        // ====================================
        String message = "因執行強制需求完成，單位無須再行確認!";

        for (RequireConfirmDep requireConfirmDep : requireConfirmDeps) {

            // ====================================
            // 異動主檔
            // ====================================
            requireConfirmDep.setConfirmDate(null);
            requireConfirmDep.setConfirmUser(null);
            requireConfirmDep.setCompleteType(null);
            requireConfirmDep.setCompleteMemo(message);
            requireConfirmDep.setProgStatus(ReqConfirmDepProgStatus.PASS);
            this.save(requireConfirmDep);

            // ====================================
            // 建立歷程
            // ====================================
            this.requireConfirmDepHistoryService.saveHistory(
                    requireNo,
                    new RequireConfirmDepVO(requireConfirmDep),
                    ReqConfirmDepProcType.PASS_BY_REQ_COMPLETE,
                    message,
                    execUserSid,
                    sysDate);

            log.info("單號:["
                    + requireNo
                    + "]["
                    + requireConfirmDep.getDepSid()
                    + "-"
                    + WkOrgCache.getInstance().findNameBySid(requireConfirmDep.getDepSid())
                    + "] "
                    + message);
        }
    }

    /**
     * 計算完成或未完成的單據
     * 
     * @param sourceNo   需求單號
     * @param createDeps 過濾建立部門 (為空時不過濾)
     * @param isComplete true:完成 / false:未完成
     * @return
     */
    public Integer countByCompleteStatus(
            String requireSid,
            boolean isComplete) {

        // 取得本次所有需要過濾的狀態
        List<ReqConfirmDepProgStatus> filterStatus = Lists.newArrayList(ReqConfirmDepProgStatus.values()).stream()
                .filter(ptStatus -> isComplete == ptStatus.isInFinish())
                .collect(Collectors.toList());

        return this.requireConfirmDepRepository
                .queryCountByRequireSidAndProgStatus(requireSid, filterStatus);
    }

    /**
     * 以需求單sid查詢非停用的分派單位
     * 
     * @param requireSid
     * @param status
     * @return
     */
    public List<RequireConfirmDepVO> findActiveByRequireSid(String requireSid) {

        // ====================================
        // 查詢
        // ====================================
        List<RequireConfirmDep> deps = this.requireConfirmDepRepository.findByRequireSidAndStatus(requireSid, Activation.ACTIVE);

        if (WkStringUtils.isEmpty(deps)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 轉型
        // ====================================
        return deps.stream().map(each -> new RequireConfirmDepVO(each)).collect(Collectors.toList());
    }

    /**
     * 以需求單號查詢
     * 
     * @param requireSid
     * @return
     */
    public List<RequireConfirmDep> findByRequireSid(String requireSid) {
        return this.requireConfirmDepRepository.findByRequireSid(requireSid);
    };

    /**
     * 以需求單號查詢
     * 
     * @param requireSid
     * @return
     */
    public List<RequireConfirmDepVO> findVOByRequireSid(String requireSid) {

        return this.requireConfirmDepRepository.findByRequireSid(requireSid)
                .stream()
                .filter(entity -> Activation.ACTIVE.equals(entity.getStatus()))
                .map(entity -> new RequireConfirmDepVO(entity))
                .collect(Collectors.toList());
    };

    /**
     * 以需求單號查詢
     * 
     * @param requireSids
     * @return
     */
    public List<RequireConfirmDepVO> findByRequireSids(
            Set<String> requireSids) {

        if (WkStringUtils.isEmpty(requireSids)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 查詢
        // ====================================
        List<RequireConfirmDep> deps = this.requireConfirmDepRepository.findByRequireSidInAndStatus(
                Lists.newArrayList(requireSids), Activation.ACTIVE);

        if (WkStringUtils.isEmpty(deps)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 轉型
        // ====================================
        return deps.stream().map(each -> new RequireConfirmDepVO(each)).collect(Collectors.toList());
    }

    /**
     * 查詢單一單據, 單一需求確認部門資料
     * 
     * @param requireSid 需求單 sid
     * @param depSid     部門 sid
     * @return 單筆
     */
    public RequireConfirmDep findByRequireSidAndDepSid(
            String requireSid,
            Integer depSid) {
        return this.requireConfirmDepRepository.findByRequireSidAndDepSidAndStatus(
                requireSid,
                depSid,
                Activation.ACTIVE);
    }

    /**
     * @param sid
     * @return
     */
    public RequireConfirmDep findBySid(Long sid) {
        // 查詢
        return this.requireConfirmDepRepository.findBySid(sid);
    }

    public Long findMaxSid() {
        return this.requireConfirmDepRepository.findMaxSid();
    }

    /**
     * 以 SID 查詢
     * 
     * @param sid
     * @return
     */
    public RequireConfirmDepVO findVoBySid(Long sid) {
        // 查詢
        RequireConfirmDep RequireConfirmDep = this.requireConfirmDepRepository.findOne(sid);
        // 轉型後回傳
        return RequireConfirmDep == null ? null : new RequireConfirmDepVO(RequireConfirmDep);
    }

    /**
     * g
     * 
     * @param requireSid
     * @return
     */
    public boolean isExsitRequireConfirmDep(String requireSid) {
        return this.requireConfirmDepRepository.isExsitByRequireSid(requireSid);
    }

    /**
     * 組分派歷程資訊
     * 
     * @param requireConfirmDepSid
     * @param assignDepSids
     * @return
     */
    public String prepareAssignHistoryMessage(Integer requireConfirmDepSid, List<Integer> assignDepSids, boolean isSortDep) {
        // 此方法目的為找出『需求確認單位』是由哪些『分派單位』收束而成
        if (WkStringUtils.isEmpty(assignDepSids) || requireConfirmDepSid == null) {
            return "";
        }

        Set<Integer> relationDepSids = Sets.newHashSet();

        // 逐筆分派單位比對計算結果
        for (Integer assignDepSid : assignDepSids) {
            // 計算收束結果 (僅傳入一個單位, 回傳僅會回傳一筆)
            Org currRequireConfirmDep = this.prepareRequireConfirmDep(assignDepSid);
            if (currRequireConfirmDep != null && currRequireConfirmDep.getSid() != null) {
                // 比對是否為本次要比對的
                if (WkCommonUtils.compareByStr(currRequireConfirmDep.getSid(), requireConfirmDepSid)) {
                    relationDepSids.add(assignDepSid);
                }
            }
        }

        if (WkStringUtils.isEmpty(relationDepSids)) {
            return "";
        }

        return WkOrgUtils.findNameBySid(Lists.newArrayList(relationDepSids), "、", false, isSortDep);
    }

    /**
     * 依據特殊規則, 轉負責人名稱
     * 
     * @param userSid
     * @param depSid
     * @return
     */
    public String prepareOwnerName(Integer userSid, Integer depSid) {

        String ownerName = "";

        if (WkCommonUtils.compareByStr(userSid, WkConstants.MANAGER_VIRTAUL_USER_SID)) {
            // 單位主管
            Org dep = WkOrgCache.getInstance().findBySid(depSid);
            if (dep != null && dep.getManager() != null) {
                ownerName = dep.getManager().getName() + " (預設主管)";
            } else {
                ownerName = "單位主管";
            }

        } else if (userSid == null) {
            // 應該不會發生
            ownerName = "未設定";
        } else {
            // 為一般使用者
            User owner = WkUserCache.getInstance().findBySid(userSid);
            if (owner != null) {
                ownerName = (Activation.INACTIVE.equals(owner.getStatus()) ? "(停用)" : "") + owner.getName();
            } else {
                ownerName = "找不到對應暱稱 [" + userSid + "]";
            }
        }

        // log.debug(WkOrgUtils.findNameBySid(depSid) + ":" + userSid + " -> [" + ownerName + "]");

        return ownerName;
    }

    /**
     * 傳入需求確認單位, 可能由哪些分派部門組成
     * 
     * @param reqConfirmDepSid 需求確認單位sid
     * @return org list
     */
    public List<Org> prepareReqConfirmDepRelationDep(Integer reqConfirmDepSid) {

        // 部以下的需求確認單位, 為組級+部級收束
        // 處以上僅為本身

        Org dep = WkOrgCache.getInstance().findBySid(reqConfirmDepSid);
        if (dep == null) {
            return Lists.newArrayList();
        }

        // 若大於部級, 僅包含本身單位
        if (WkOrgUtils.compareOrgLevel(dep.getLevel(), OrgLevel.MINISTERIAL) == 1) {
            return Lists.newArrayList(dep);
        }

        List<Org> createDeps = Lists.newArrayList();
        createDeps.add(dep);
        // 查詢子單位
        createDeps.addAll(WkOrgCache.getInstance().findAllChild(reqConfirmDepSid));

        return createDeps;
    }

    /**
     * 計算需求完成確認單位 (收束到組)<br/>
     * 因批次轉檔, 建立快取
     * 
     * @param depSid
     * @return
     */
    @Cacheable(cacheNames = CacheConstants.CACHE_prepareRequireConfirmDep, key = "#depSid")
    public Org prepareRequireConfirmDep(Integer depSid) {
        // 查詢
        Org dep = WkOrgCache.getInstance().findBySid(Integer.valueOf(depSid));
        // 過濾錯誤資料
        if (dep == null) {
            return null;
        }

        // REQ-1691【全系統】 移除收束到部規則
        // 取基本部門到組級
        // 若單位超過組級，或直接上層超過組級，則取傳入單位
        Org targetDep = WkOrgUtils.prepareBasicDep(dep.getSid(), OrgLevel.THE_PANEL);
        return targetDep;
    }

    /**
     * 計算哪些單位需要進行需求確認 (原收束到部, 後已取消此規則)
     * 
     * @param assignDepSids
     * @return
     */
    public List<Org> prepareRequireConfirmDeps(List<Integer> assignDepSids) {
        return this.prepareRequireConfirmDeps(assignDepSids, false, true);
    }

    /**
     * 計算哪些單位需要進行需求確認 (依據分派單位, 收束到部)
     * 
     * @param assignDepSids
     * @return
     */
    public List<Org> prepareRequireConfirmDeps(List<Integer> assignDepSids, boolean isLog, boolean isSort) {

        // 防呆
        if (WkStringUtils.isEmpty(assignDepSids)) {
            log.debug("未傳入分派單位");
            return Lists.newArrayList();
        }

        // debug 訊息
        List<String> logMsgLines = Lists.newArrayList();
        if (isLog) {
            logMsgLines.add("被分派單位(" + assignDepSids.size() + "):" + WkOrgUtils.findNameBySid(assignDepSids, "、", true));
        }

        // ====================================
        // 收集分派單位
        // ====================================
        // 用 set 收集, 避免收束後，有重複的需求完成確認單位
        Set<Org> requireConfirmDepSet = Sets.newLinkedHashSet();

        // 收集收束後結果
        for (Integer depSid : Sets.newLinkedHashSet(assignDepSids)) {
            // 計算收束後部門
            Org requireConfirmDep = this.prepareRequireConfirmDep(depSid);
            // 過濾錯誤資料
            if (requireConfirmDep == null) {
                continue;
            }
            requireConfirmDepSet.add(requireConfirmDep);
        }

        List<Org> resultDeps = Lists.newArrayList(requireConfirmDepSet);
        if (isLog) {
            logMsgLines.add("需確認完成單位計算結果(" + resultDeps.size() + "):" + WkOrgUtils.joinOrgName(resultDeps, "、", true));
        }

        // 依據組織樹順序排序
        if (isSort) {
            resultDeps = WkOrgUtils.sortOrgByOrgTree(Lists.newArrayList(requireConfirmDepSet));
            if (isLog) {
                logMsgLines.add("再次排序結果(" + resultDeps.size() + "):" + WkOrgUtils.joinOrgName(resultDeps, "、", true));
            }
        }

        if (isLog) {
            log.debug("\r\n計算需求確認單位\r\n" + String.join("\r\n", logMsgLines));
        }
        return resultDeps;
    }

    /**
     * 計算需求完成確認單位 (收束到部)
     * 
     * @param depSid
     * @return
     */
    public Integer prepareRequireConfirmDepSid(Integer depSid) {
        Org targetDep = prepareRequireConfirmDep(depSid);
        if (targetDep != null) {
            return targetDep.getSid();
        }
        return null;
    }

    /**
     * @param assignDepSids
     * @return
     */
    public Map<Integer, Set<Integer>> prepareRequireConfirmDepSidMap(Set<Integer> assignDepSids) {

        Map<Integer, Set<Integer>> resultMap = Maps.newHashMap();
        if (WkStringUtils.isEmpty(assignDepSids)) {
            return resultMap;
        }

        for (Integer assignDepSid : assignDepSids) {
            Org requireConfirmDep = this.prepareRequireConfirmDep(assignDepSid);
            if (requireConfirmDep == null) {
                continue;
            }

            Set<Integer> assignDeps = resultMap.get(requireConfirmDep.getSid());
            if (assignDeps == null) {
                assignDeps = Sets.newHashSet();
                resultMap.put(requireConfirmDep.getSid(), assignDeps);
            }

            assignDeps.add(assignDepSid);
        }

        return resultMap;
    }

    /**
     * 計算哪些單位需要進行需求確認 (依據分派單位, 收束到部)
     * 
     * @param assignDepSids
     * @return
     */
    public List<Integer> prepareRequireConfirmDepSids(List<Integer> assignDepSids) {
        return this.prepareRequireConfirmDeps(assignDepSids, false, false).stream().map(Org::getSid).collect(Collectors.toList());
    }

    /**
     * 取得 user 可操作的相關單位 (依據重要性排序)<br/>
     * 優先序1：主要部門<br/>
     * 優先序2：管理部門<br/>
     * 優先序3：管理部門的子部門
     * 
     * @param userSid
     * @return
     */
    public List<Integer> prepareUserMatchDepSids(Integer userSid) {

        // 以 LinkedHashSet 為容器, 防重複, 並記錄加入順序
        LinkedHashSet<Integer> matchDepSids = Sets.newLinkedHashSet();

        // ====================================
        // 查詢 user 資料檔
        // ====================================
        User user = WkUserCache.getInstance().findBySid(userSid);
        if (user == null || Activation.INACTIVE.equals(user.getStatus())) {
            return Lists.newArrayList();
        }

        // ====================================
        // 優先序1：主要部門
        // ====================================
        if (user.getPrimaryOrg() != null && user.getPrimaryOrg().getSid() != null) {
            // 加入主要部門
            matchDepSids.add(user.getPrimaryOrg().getSid());
            // 加入部門上升到部後單位
            Org baseDep = WkOrgUtils.prepareBasicDep(user.getPrimaryOrg().getSid(), OrgLevel.MINISTERIAL);
            matchDepSids.add(baseDep.getSid());
        }

        // ====================================
        // 優先序2： 管理部門
        // ====================================
        // 紀錄管理子部門
        List<Org> managerChildDeps = Lists.newArrayList();
        // 取得 user 管理單位
        List<Org> managerDeps = WkOrgCache.getInstance().findManagerOrgs(userSid);

        if (WkStringUtils.notEmpty(managerDeps)) {
            // 先以組織樹排序
            managerDeps = WkOrgUtils.sortOrgByOrgTree(managerDeps);
            // 依照順序加入
            for (Org managerDep : managerDeps) {
                if (managerDep != null && managerDep.getSid() != null) {
                    // 加入管理部門
                    matchDepSids.add(managerDep.getSid());

                    // 加入部門上升到部後單位
                    Org baseDep = WkOrgUtils.prepareBasicDep(managerDep.getSid(), OrgLevel.MINISTERIAL);
                    matchDepSids.add(baseDep.getSid());

                    // 取得管理部門的子單位
                    List<Org> currManagerChildDeps = WkOrgCache.getInstance().findAllChild(managerDep.getSid());
                    if (WkStringUtils.notEmpty(currManagerChildDeps)) {
                        managerChildDeps.addAll(currManagerChildDeps);
                    }
                }
            }
        }

        // ====================================
        // 優先序3：管理部門的子部門
        // ====================================
        if (WkStringUtils.notEmpty(managerChildDeps)) {
            // 依照順序加入
            for (Org managerChildDep : managerChildDeps) {
                if (managerChildDep != null) {
                    // 加入管理子部門
                    matchDepSids.add(managerChildDep.getSid());

                    // 加入部門上升到部後單位
                    Org baseDep = WkOrgUtils.prepareBasicDep(managerChildDep.getSid(), OrgLevel.MINISTERIAL);
                    matchDepSids.add(baseDep.getSid());
                }
            }
        }

        return matchDepSids.stream()
                .distinct()
                .collect(Collectors.toList());
    }

    /**
     * 以下條件查詢
     * 
     * @param inDepSids
     * @param progStatus
     * @return 回傳需求單 sid、負責人 sid
     * @return
     */
    public List<RequireConfirmDepVO> queryByDepSidInAndProgStatus(
            Collection<Integer> inDepSids,
            ReqConfirmDepProgStatus progStatus,
            boolean isAlwaysGetAssignDeps,
            boolean isForOwnerSearch) {

        if (WkStringUtils.isEmpty(inDepSids)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 查詢
        // ====================================
        String sql = this.prepareSqlForQueryByDepSidInAndProgStatus(inDepSids, progStatus, isAlwaysGetAssignDeps, isForOwnerSearch);

        if (this.workBackendParamHelper.isShowRequireSearchSql() ||
                this.workBackendParamHelper.isShowRequireSearch07DebugLog()) {
            log.info(""
                    + "\r\n取得需求確認單位檔資料SQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql) + "\r\n】"
                    + "\r\n");
        }

        long startime = System.currentTimeMillis();
        List<Map<String, Object>> results = this.jdbcTemplate.queryForList(sql.toString());
        if (this.workBackendParamHelper.isShowRequireSearch07DebugLog()) {
            log.info(WkCommonUtils.prepareCostMessage(startime, "取得需求確認單位檔資料[" + results.size() + "]筆"));
        }

        if (WkStringUtils.isEmpty(results)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 轉型
        // ====================================
        List<RequireConfirmDepVO> resultVOs = Lists.newArrayList();
        for (Map<String, Object> resultRow : results) {
            // 需求單Sid
            String requireSid = resultRow.get("require_sid") + "";
            // 部門 sid
            Integer depSid = resultRow.get("dep_sid") == null ? -1 : (Integer) resultRow.get("dep_sid");
            // 確認部門 SID
            Integer ownerSid = resultRow.get("owner_sid") == null ? -1 : (Integer) resultRow.get("owner_sid");
            // 被分派部門
            String assignDepSidsStr = resultRow.get("assignDepSidsStr") == null ? "" : resultRow.get("assignDepSidsStr") + "";

            RequireConfirmDepVO vo = new RequireConfirmDepVO(requireSid, depSid, ownerSid, assignDepSidsStr);

            if (resultRow.get("prog_status") != null) {
                vo.setProgStatusStrForSearch07(resultRow.get("prog_status") + "");
            }

            resultVOs.add(vo);
        }

        return resultVOs;
    }

    public String prepareSqlForQueryByDepSidInAndProgStatus(
            Collection<Integer> inDepSids,
            ReqConfirmDepProgStatus progStatus,
            boolean isAlwaysGetAssignDeps,
            boolean isForOwnerSearch) {

        // ====================================
        // 相關單位
        // ====================================
        String inDepSidsStr = inDepSids.stream()
                .map(sid -> sid + "")
                .collect(Collectors.joining(","));

        if (WkStringUtils.isEmpty(inDepSidsStr)) {
            inDepSidsStr = "-999";
        }

        // ====================================
        // 限制主單狀態
        // ====================================
        String requireStatusTypesStr = Lists.newArrayList(RequireStatusType.values()).stream()
                .filter(status -> status.isAssignSearch())
                .map(RequireStatusType::name)
                .collect(Collectors.joining("', '", "'", "'"));

        StringBuffer sql = new StringBuffer();
        sql.append("SELECT r.require_sid, ");
        sql.append("       r.dep_sid, ");
        sql.append("       r.owner_sid, ");
        sql.append("       r.prog_status ");

        if (isAlwaysGetAssignDeps) {
            sql.append("      ,(SELECT Group_concat (DISTINCT assign_info.dep_sid SEPARATOR ',') ");
            sql.append("               FROM   tr_assign_send_search_info assign_info ");
            sql.append("               WHERE  assign_info.require_sid = r.require_sid ");
            sql.append("                      AND assign_info.type = 0) ");
            sql.append("       AS assignDepSidsStr ");
        } else if (isForOwnerSearch) {
            // 資料為『負責人為部門主管』時，才需要撈分派單位
            sql.append("       ,CASE ");
            sql.append("         WHEN r.owner_sid <> -1 THEN '' ");
            sql.append("         ELSE (SELECT Group_concat (DISTINCT assign_info.dep_sid SEPARATOR ',') ");
            sql.append("               FROM   tr_assign_send_search_info assign_info ");
            sql.append("               WHERE  assign_info.require_sid = r.require_sid ");
            sql.append("                      AND assign_info.type = 0) ");
            sql.append("       end AS assignDepSidsStr ");
        }

        sql.append("FROM   tr_require_confirm_dep r ");
        sql.append("WHERE  r.status = 0 ");

        if (WkStringUtils.notEmpty(progStatus)) {
            sql.append("       AND r.prog_status = '" + progStatus.name() + "' ");
        }

        sql.append("       AND r.dep_sid IN ( " + inDepSidsStr + " ) ");
        sql.append("       AND EXISTS (SELECT tr.require_sid ");
        sql.append("                   FROM   tr_require tr ");
        sql.append("                   WHERE  tr.require_sid = r.require_sid ");
        sql.append("                          AND tr.status = 0 ");
        sql.append("                          AND tr.require_status IN ( " + requireStatusTypesStr + " )) ");

        return sql.toString();
    }

    /**
     * @param requireSids
     * @param depSids
     * @return
     */
    public Map<String, List<RequireConfirmDepVO>> queryByReqSidAndDepSids(List<String> requireSids, List<Integer> depSids) {

        // ====================================
        // 查詢
        // ====================================
        List<RequireConfirmDep> results = this.requireConfirmDepRepository.findByRequireSidInAndDepSidIn(
                requireSids, depSids);

        if (WkStringUtils.isEmpty(results)) {
            return Maps.newHashMap();
        }

        // 轉型為 VO
        List<RequireConfirmDepVO> vos = results.stream().map(each -> new RequireConfirmDepVO(each)).collect(Collectors.toList());

        // ====================================
        // list 轉 map （groupingBy）
        // ====================================
        Map<String, List<RequireConfirmDepVO>> requireConfirmDepVOMapByRequireSid = vos.stream()
                .collect(Collectors.groupingBy(
                        RequireConfirmDepVO::getRequireSid,
                        Collectors.mapping(
                                each -> each,
                                Collectors.toList())));

        return requireConfirmDepVOMapByRequireSid;
    }

    /**
     * @param requireSids
     * @param depSids
     * @return
     */
    public Map<String, List<RequireConfirmDepVO>> queryByReqSidsForNewsearch03(List<String> requireSids) {

        // 此方法用 hibernate 會因為 sid 過多而出現 java.nio.BufferOverflowException
        // 故改用 jdbcTemplate

        // ====================================
        // 需求單 sid
        // ====================================
        if (WkStringUtils.isEmpty(requireSids)) {
            return Maps.newHashMap();
        }

        String requireSidsStr = requireSids.stream().collect(Collectors.joining("', '", "'", "'"));

        // ====================================
        // 查詢
        // ====================================
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT require_sid, ");
        sql.append("       dep_sid, ");
        sql.append("       owner_sid, ");
        sql.append("       prog_status ");
        sql.append("FROM   tr_require_confirm_dep ");
        sql.append("WHERE  require_sid IN ( " + requireSidsStr + " ) ");
        sql.append("  AND  status = 0 ");

        List<Map<String, Object>> dbRowDataMaps = this.jdbcTemplate.queryForList(sql.toString());

        if (WkStringUtils.isEmpty(dbRowDataMaps)) {
            return Maps.newHashMap();
        }

        // ====================================
        // 封裝
        // ====================================
        List<RequireConfirmDepVO> vos = Lists.newArrayList();
        for (Map<String, Object> dbRowDataMap : dbRowDataMaps) {
            RequireConfirmDepVO vo = new RequireConfirmDepVO();

            vos.add(vo);
            // 需求單sid
            vo.setRequireSid(dbRowDataMap.get("require_sid") + "");
            // 部門sid
            if (dbRowDataMap.get("dep_sid") != null) {
                vo.setDepSid((Integer) dbRowDataMap.get("dep_sid"));
            }
            // 負責人sid
            if (dbRowDataMap.get("owner_sid") != null) {
                vo.setOwnerSid((Integer) dbRowDataMap.get("owner_sid"));
            }
            //

            if (dbRowDataMap.get("prog_status") != null) {
                vo.setProgStatusStr(dbRowDataMap.get("prog_status") + "");
            }
        }

        // ====================================
        // list 轉 map （groupingBy）
        // ====================================
        Map<String, List<RequireConfirmDepVO>> requireConfirmDepVOMapByRequireSid = vos.stream()
                .collect(Collectors.groupingBy(
                        RequireConfirmDepVO::getRequireSid,
                        Collectors.mapping(
                                each -> each,
                                Collectors.toList())));

        return requireConfirmDepVOMapByRequireSid;
    }

    /**
     * 計算需求完成確認單位檔數量
     * 
     * @param requireSid 需求單 sid
     * @return
     */
    public Integer queryCountByRequireSid(String requireSid) {
        return this.requireConfirmDepRepository.queryCountByRequireSid(requireSid);
    }

    /**
     * @param requireSid
     * @return
     */
    public List<Integer> queryDepSidByRequireSid(String requireSid) {
        return this.requireConfirmDepRepository.querydepSidByRequireSid(requireSid);
    }

    /**
     * 以 nativeSql 查詢『單一需求』, 『所有單位』的『確認進度』
     * 
     * @param requireSid 需求單 sid
     * @return Map<DepSid , prog_status>
     * @throws Exception
     */
    public Map<Integer, String> queryProgStatus(String requireSid) {

        // ====================================
        // 以 nativeSql 查詢『單一需求』, 『所有單位』的『確認進度』
        // ====================================
        List<Map<String, Object>> dataRows = this.nativeSqlRepository.selectRowsBySingleField(
                "tr_require_confirm_dep",
                Lists.newArrayList("dep_sid", "prog_status"),
                "require_sid",
                Sets.newHashSet(requireSid));

        if (dataRows == null || dataRows.size() == 0) {
            return Maps.newHashMap();
        }

        // ====================================
        // 組裝回結果 Map<DepSid , prog_status>
        // ====================================
        Map<Integer, String> progStatusMapByDepSid = Maps.newHashMap();

        for (Map<String, Object> dataRowMap : dataRows) {
            String dep_sid = dataRowMap.get("dep_sid") + "";
            if (!WkStringUtils.isNumber(dep_sid)) {
                continue;
            }

            progStatusMapByDepSid.put(
                    Integer.parseInt(dep_sid),
                    dataRowMap.get("prog_status") + "");
        }

        return progStatusMapByDepSid;
    }

    /**
     * 重新開啟需求單位確認流程
     * 
     * @param requireConfirmDep
     * @param requireNo
     * @param execUserSid
     * @param sysDate
     * @param reqConfirmDepProcType
     * @param traceMessage
     */
    public void reOpenProgStatus(
            RequireConfirmDep requireConfirmDep,
            String requireNo,
            Integer execUserSid,
            Date sysDate,
            ReqConfirmDepProcType reqConfirmDepProcType,
            String traceMessage) {

        // ====================================
        // 檢核狀態不為已完成時不處理
        // ====================================
        if (requireConfirmDep == null || requireConfirmDep.getProgStatus() == null ||
                !requireConfirmDep.getProgStatus().isInFinish()) {
            return;
        }

        // ====================================
        // 異動主檔
        // ====================================
        // 準備異動欄位
        requireConfirmDep.setUpdatedUser(execUserSid);
        requireConfirmDep.setUpdatedDate(sysDate);
        requireConfirmDep.setConfirmDate(null);
        requireConfirmDep.setConfirmUser(null);
        requireConfirmDep.setCompleteType(null);
        requireConfirmDep.setCompleteMemo("");

        // 倒回之前的狀態 （由是否曾領單過 flag 來判斷）
        // 單位執行狀態
        ReqConfirmDepProgStatus reqConfirmDepProgStatus = ReqConfirmDepProgStatus.WAIT_CONFIRM;
        if (requireConfirmDep.isReceived()) {
            // 確認單位已領過單, 自動還原為進行中
            reqConfirmDepProgStatus = ReqConfirmDepProgStatus.IN_PROCESS;
        }
        requireConfirmDep.setProgStatus(reqConfirmDepProgStatus);

        // save
        this.save(requireConfirmDep);

        // ====================================
        // 建立歷程
        // ====================================
        this.requireConfirmDepHistoryService.saveHistory(
                requireNo,
                new RequireConfirmDepVO(requireConfirmDep),
                reqConfirmDepProcType,
                traceMessage,
                execUserSid,
                sysDate);

        log.info("單號:["
                + requireNo
                + "]["
                + requireConfirmDep.getDepSid()
                + "-"
                + WkOrgCache.getInstance().findNameBySid(requireConfirmDep.getDepSid())
                + "] "
                + traceMessage);
    }

    /**
     * save
     * 
     * @param requireConfirmDep
     */
    public RequireConfirmDep save(RequireConfirmDep requireConfirmDep) {
        return this.requireConfirmDepRepository.save(requireConfirmDep);
    }

    /**
     * save
     * 
     * @param requireConfirmDep
     */
    public List<RequireConfirmDep> saveAll(List<RequireConfirmDep> requireConfirmDeps) {
        return this.requireConfirmDepRepository.save(requireConfirmDeps);
    }

    /**
     * save<br/>
     * 1.需求完成確認單位檔 (若原有分派單位不在清單中, 會做 delete)<br/>
     * 2.需求完成確認單位 - 歷程檔
     * 
     * @param requireSid       需求單 sid
     * @param requireNo        需求單號
     * @param oldAssignDepSids 原有的被分派單位
     * @param newAssignDepSids 新的被分派單位
     * @param execUserSid      執行者 sid
     * @param sysDate          系統時間
     * @param addReasonType    新增需求確認單位的原因 (動作來源)
     * @param autoAssignDesc   (RequireConfirmAddReasonType = AUTO_ASSIGN 時, 需輸入說明)
     * @throws UserMessageException 查不到登入者資料時拋出
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveRequireConfirmDeps(
            String requireSid,
            String requireNo,
            List<Integer> oldAssignDepSids,
            List<Integer> newAssignDepSids,
            Integer execUserSid,
            Date sysDate,
            RequireConfirmAddReasonType addReasonType,
            String autoAssignDesc,
            boolean isShowDebug) throws UserMessageException {

        Long startTime = System.currentTimeMillis();
        String procTitle = "儲存需求完成確認單位 (tr_require_confirm_dep、tr_require_confirm_dep_history)";

        // ====================================
        // 查詢現有的需求完成確認單位檔
        // ====================================
        List<RequireConfirmDep> oldRequireConfirmDeps = this.findByRequireSid(requireSid);
        if (oldRequireConfirmDeps == null) {
            oldRequireConfirmDeps = Lists.newArrayList();
        }

        // 轉 sid list
        List<Integer> oldRequireConfirmDepSids = oldRequireConfirmDeps.stream()
                .map(RequireConfirmDep::getDepSid)
                .collect(Collectors.toList());

        // create index by DepSid
        Map<Integer, RequireConfirmDep> oldRequireConfirmDepMapByDepSid = oldRequireConfirmDeps.stream()
                .collect(Collectors.toMap(RequireConfirmDep::getDepSid, entity -> entity));

        // ====================================
        // 依據規則, 計算需進行需求確認的單位
        // ====================================
        // 查詢
        List<Org> newRequireConfirmDep = this.prepareRequireConfirmDeps(newAssignDepSids);
        if (newRequireConfirmDep == null) {
            newRequireConfirmDep = Lists.newArrayList();
        }
        // 轉 sid list
        List<Integer> newRequireConfirmDepSids = newRequireConfirmDep.stream()
                .map(Org::getSid)
                .collect(Collectors.toList());

        // ====================================
        // 刪除被移除的指派單位
        // ====================================
        this.saveRequireConfirmDeps_Remove(
                requireNo,
                oldAssignDepSids,
                newAssignDepSids,
                oldRequireConfirmDeps,
                newRequireConfirmDepSids,
                execUserSid,
                sysDate,
                isShowDebug);

        // ====================================
        // 加入需新增的需求確認單位
        // ====================================
        this.saveRequireConfirmDeps_Add(
                requireNo,
                requireSid,
                oldAssignDepSids,
                newAssignDepSids,
                newRequireConfirmDepSids,
                oldRequireConfirmDepSids,
                oldRequireConfirmDepMapByDepSid,
                execUserSid,
                sysDate,
                addReasonType,
                autoAssignDesc,
                isShowDebug);

        if (isShowDebug) {
            log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
        }
    }

    /**
     * @param requireSid
     * @param oldAssignDepSids
     * @param newAssignDepSids
     * @param newRequireConfirmDepSids
     * @param oldRequireConfirmDepSids
     * @param oldRequireConfirmDepMapByDepSid
     * @param execUserSid
     * @param sysDate
     * @param addReasonType                   新增需求確認單位的原因 (動作來源)
     * @param autoAssignDesc                  (RequireConfirmAddReasonType =
     *                                            AUTO_ASSIGN 時, 需輸入說明)
     * @return
     * @throws UserMessageException 查不到登入者資料時拋出
     */
    @Transactional(rollbackFor = Exception.class)
    public List<Integer> saveRequireConfirmDeps_Add(
            String requireNo,
            String requireSid,
            List<Integer> oldAssignDepSids,
            List<Integer> newAssignDepSids,
            List<Integer> newRequireConfirmDepSids,
            List<Integer> oldRequireConfirmDepSids,
            Map<Integer, RequireConfirmDep> oldRequireConfirmDepMapByDepSid,
            Integer execUserSid,
            Date sysDate,
            RequireConfirmAddReasonType addReasonType,
            String autoAssignDesc,
            boolean isShowDebug) throws UserMessageException {

        // ====================================
        // 防呆
        // ====================================
        if (oldAssignDepSids == null) {
            oldAssignDepSids = Lists.newArrayList();
        }
        if (newAssignDepSids == null) {
            newAssignDepSids = Lists.newArrayList();
        }
        if (newRequireConfirmDepSids == null) {
            newRequireConfirmDepSids = Lists.newArrayList();
        }
        if (oldRequireConfirmDepSids == null) {
            oldRequireConfirmDepSids = Lists.newArrayList();
        }
        if (oldRequireConfirmDepMapByDepSid == null) {
            oldRequireConfirmDepMapByDepSid = Maps.newHashMap();
        }

        // ====================================
        // 取得此次新增的分派單位
        // ====================================
        // 此次加派的單位 (複製一份)
        List<Integer> addAssignDepSids = newAssignDepSids.stream().collect(Collectors.toList());
        // 新指派單位 - 舊指派單位 = 加派的單位
        if (WkStringUtils.notEmpty(oldAssignDepSids)) {
            addAssignDepSids.removeAll(oldAssignDepSids);
        }

        // ====================================
        // 取得執行者收束到部的結果
        // ====================================
        User execUser = WkUserCache.getInstance().findBySid(execUserSid);
        if (execUser == null) {
            log.warn("找不到執行者資料userSid:[{}]", execUserSid);
            throw new UserMessageException(WkMessage.NEED_RELOAD);
        }
        if (execUser.getPrimaryOrg() == null || execUser.getPrimaryOrg().getSid() == null) {
            log.warn("找不到執行者主要單位userSid:[{}]", execUserSid);
            throw new UserMessageException(WkMessage.NEED_RELOAD);
        }

        Integer execUserRequireConfirmDepSid = this.prepareRequireConfirmDepSid(
                execUser.getPrimaryOrg().getSid());

        // ====================================
        // 主要負責人
        // ====================================
        Integer ownerSid = execUserSid;
        // 為內部需求自動分派時，因為有簽核的關係，執行者不一定為申請人，故重抓為單據申請者
        if (RequireConfirmAddReasonType.AUTO_ASSIGN_BY_CREATE_INTERNAL.equals(addReasonType)) {
            Require require = this.requireService.findByReqSid(requireSid);
            if (require == null || require.getCreatedUser() == null) {
                throw new UserMessageException(WkMessage.NEED_RELOAD);
            }
            ownerSid = require.getCreatedUser().getSid();
        }

        // ====================================
        // 逐筆處理
        // ====================================
        // 收集所有新增部門
        List<Integer> addedRequireConfirmDepSid = Lists.newArrayList();

        for (Integer requireConfirmDepSid : newRequireConfirmDepSids) {

            // ====================================
            // 準備異動歷程所需的資訊
            // ====================================
            // 取得與此『需求確認』單位相關的『被分派單位』
            String relationDepName = this.prepareAssignHistoryMessage(requireConfirmDepSid, addAssignDepSids, true);
            // 歷程檔 - 說明
            String histroryMemo = "分派【" + relationDepName + "】";
            // 歷程檔 - 操作類別
            ReqConfirmDepProcType procType = null;

            String depName = requireConfirmDepSid
                    + " "
                    + WkOrgUtils.prepareBreadcrumbsByDepName(
                            requireConfirmDepSid,
                            OrgLevel.DIVISION_LEVEL,
                            false,
                            "-");

            // ====================================
            // 新增需求完成確認單位檔 Require_Confirm_Dep
            // ====================================
            RequireConfirmDep requireConfirmDep = null;

            // 不存在於舊資料中時，為需新增
            if (!oldRequireConfirmDepSids.contains(requireConfirmDepSid)) {
                // 初始化欄位值
                requireConfirmDep = new RequireConfirmDep();
                requireConfirmDep.setCreatedUser(execUserSid);
                requireConfirmDep.setCreatedDate(sysDate);
                requireConfirmDep.setRequireSid(requireSid);
                requireConfirmDep.setDepSid(requireConfirmDepSid);

                // 根據是否分派單位，走不同流程
                if (WkCommonUtils.compareByStr(requireConfirmDepSid, execUserRequireConfirmDepSid)) {

                    // 分派自己部門時，自動領單
                    requireConfirmDep.setOwnerSid(ownerSid);
                    // 註記略過領單階段 (設為已領過單)
                    requireConfirmDep.setReceived(true);
                    // 初始狀態即為進行中, 無需領單
                    requireConfirmDep.setProgStatus(ReqConfirmDepProgStatus.IN_PROCESS);

                    // 異動歷程說明
                    if (RequireConfirmAddReasonType.AUTO_ASSIGN_BY_CREATE_INTERNAL.equals(addReasonType)) {
                        histroryMemo = "因建立內部需求" + histroryMemo;
                    }
                    histroryMemo += "，並產生需確認流程。";

                } else {

                    // 分派別人部門時，走正常流程
                    // 初始預設為該部門主管，為避免組織異動，一開始塞 -1 , 代表需動態抓部門主管
                    requireConfirmDep.setOwnerSid(WkConstants.MANAGER_VIRTAUL_USER_SID);
                    // 需求完成進度初始值為『待確認』
                    requireConfirmDep.setProgStatus(ReqConfirmDepProgStatus.WAIT_CONFIRM);
                    // 異動歷程說明
                    histroryMemo += "，產生需確認流程，並指定負責人為單位主管。";
                }

                // 自動分派時，客製追蹤說明
                if (RequireConfirmAddReasonType.AUTO_ASSIGN_BY_CREATE_CHILD_CASE.equals(addReasonType)) {
                    // 異動歷程說明
                    histroryMemo = "因" + autoAssignDesc + histroryMemo;
                    histroryMemo += "，並產生需確認流程。";
                }

                // 存檔
                requireConfirmDep = this.save(requireConfirmDep);
                // 加入新增list
                addedRequireConfirmDepSid.add(requireConfirmDepSid);
                // 註記處理類別為分派新增
                procType = ReqConfirmDepProcType.ASSIGN_CREATE;

            } else if (WkStringUtils.notEmpty(relationDepName)) {
                // 僅新增分派單位, 未因此產生需確認流程
                requireConfirmDep = oldRequireConfirmDepMapByDepSid.get(requireConfirmDepSid);
                procType = ReqConfirmDepProcType.ASSIGN;

            } else {
                // 此需求確認單位檔, 與此次分派動作無關
                procType = null;
            }

            // ====================================
            // 新增異動歷程檔 Require_Confirm_Dep_History
            // ====================================
            // 備註:批次轉檔時 oldRequireConfirmDepMapByDepSid 傳入空值 , 故 requireConfirmDep 為空
            if (procType != null && requireConfirmDep != null) {
                this.requireConfirmDepHistoryService.saveHistory(
                        requireNo,
                        new RequireConfirmDepVO(requireConfirmDep),
                        procType,
                        histroryMemo,
                        ownerSid,
                        sysDate);
                if (isShowDebug) {
                    log.info("【" + depName + "】" + histroryMemo);
                }
            }
        }
        return addedRequireConfirmDepSid;
    }

    /**
     * 依據被減派的單位 , 移除確認檔
     * 
     * @param oldAssignDepSids
     * @param newAssignDepSids
     * @param oldRequireConfirmDeps
     * @param newRequireConfirmDepSids
     * @param execUserSid
     * @param sysDate
     */
    @Transactional(rollbackFor = Exception.class)
    private List<Integer> saveRequireConfirmDeps_Remove(
            String requireNo,
            List<Integer> oldAssignDepSids,
            List<Integer> newAssignDepSids,
            List<RequireConfirmDep> oldRequireConfirmDeps,
            List<Integer> newRequireConfirmDepSids,
            Integer execUserSid,
            Date sysDate,
            boolean isShowDebug) {

        // ====================================
        // 防呆
        // ====================================
        if (oldAssignDepSids == null) {
            oldAssignDepSids = Lists.newArrayList();
        }
        if (newAssignDepSids == null) {
            newAssignDepSids = Lists.newArrayList();
        }
        if (oldRequireConfirmDeps == null) {
            oldRequireConfirmDeps = Lists.newArrayList();
        }
        if (newRequireConfirmDepSids == null) {
            newRequireConfirmDepSids = Lists.newArrayList();
        }

        // ----------------------
        // 取得此次移除的分派單位(減派)
        // ----------------------
        // 此次被移除的單位 （避免影響原資料 clone 一份）
        List<Integer> deleteAssignDepSids = oldAssignDepSids.stream().collect(Collectors.toList());
        // 舊指派單位 - 新指派單位 = 減派的單位
        if (deleteAssignDepSids != null) {
            deleteAssignDepSids.removeAll(newAssignDepSids);
        }

        // ====================================
        // 逐筆處理
        // ====================================
        List<Integer> removedRequireConfirmDepSid = Lists.newArrayList();

        for (RequireConfirmDep oldRequireConfirmDep : oldRequireConfirmDeps) {

            // ====================================
            // 準備異動歷程所需的資訊
            // ====================================
            // 取得與此『需求確認』單位相關的『被減派單位』
            String relationDepName = this.prepareAssignHistoryMessage(oldRequireConfirmDep.getDepSid(), deleteAssignDepSids, true);
            // 歷程檔 - 說明
            String histroryMemo = "減派【" + relationDepName + "】";
            // 歷程檔 - 操作類別
            ReqConfirmDepProcType procType = null;
            String depName = oldRequireConfirmDep.getDepSid()
                    + " "
                    + WkOrgUtils.prepareBreadcrumbsByDepName(
                            oldRequireConfirmDep.getDepSid(),
                            OrgLevel.DIVISION_LEVEL,
                            false,
                            "-");

            // ====================================
            // 移除需求完成確認單位檔 Require_Confirm_Dep
            // ====================================
            // 判斷：不存在於『新的需確認單位清單』, 且狀態『不為完成』
            if (!newRequireConfirmDepSids.contains(oldRequireConfirmDep.getDepSid())) {

                if (!ReqConfirmDepProgStatus.COMPLETE.equals(oldRequireConfirmDep.getProgStatus())) {
                    // 記錄被刪除的部門
                    removedRequireConfirmDepSid.add(oldRequireConfirmDep.getDepSid());
                    // 刪除資料
                    this.requireConfirmDepRepository.delete(oldRequireConfirmDep);
                    histroryMemo += "，故移除確認流程";
                    procType = ReqConfirmDepProcType.REMOVE_ASSIGN_DELETE;
                } else {
                    histroryMemo += "，但單位已完成確認。故保留確認資料";
                    procType = ReqConfirmDepProcType.REMOVE_ASSIGN;
                }

            } else if (WkStringUtils.notEmpty(relationDepName)) {
                // 僅新增減派單位, 未因此影響【需求完成確認單位檔】
                procType = ReqConfirmDepProcType.REMOVE_ASSIGN;

            } else {
                // 此需求確認單位檔, 與此次動作無關
                procType = null;
            }

            // ====================================
            // 新增異動歷程檔 Require_Confirm_Dep_History
            // ====================================
            if (procType != null) {
                this.requireConfirmDepHistoryService.saveHistory(
                        requireNo,
                        new RequireConfirmDepVO(oldRequireConfirmDep),
                        procType,
                        histroryMemo,
                        execUserSid,
                        sysDate);
                if (isShowDebug) {
                    log.info(String.format("【%s-%s】:%s",
                            requireNo,
                            depName,
                            histroryMemo));
                }
            }
        }

        return removedRequireConfirmDepSid;
    }

    /**
     * 更新負責人
     * 
     * @param sid
     * @param ownerSid
     */
    public RequireConfirmDep updateOwnerBySid(Long sid, Integer ownerSid) {
        RequireConfirmDep requireConfirmDep = this.findBySid(sid);
        requireConfirmDep.setOwnerSid(ownerSid);
        return this.requireConfirmDepRepository.save(requireConfirmDep);
        // this.requireConfirmDepRepository.setOwner(sid, ownerSid);
    }

    // ========================================================================
    // 排程
    // ========================================================================
    /**
     * 自動異動『需求確認單位負責人』
     */
    @Transactional(rollbackFor = Exception.class)
    public void keepConfirmDepOwner() {

        // ====================================
        // 兜組 SQL
        // ====================================
        String progStatusCondition = Lists.newArrayList(ReqConfirmDepProgStatus.values()).stream()
                .filter(status -> !status.isInFinish())
                .map(ReqConfirmDepProgStatus::name)
                .collect(Collectors.joining("', '", "'", "'"));

        StringBuffer sql = new StringBuffer();
        sql.append("SELECT confirmDep.require_confirm_sid   requireConfirmSid, ");
        sql.append("       confirmDep.dep_sid               depSid, ");
        sql.append("       confirmDep.owner_sid             ownerSid, ");
        sql.append("       confirmDep.require_sid           requireSid, ");
        sql.append("       req.require_no                   requireNo ");
        sql.append("FROM   tr_require_confirm_dep confirmDep ");
        sql.append("       INNER JOIN tr_require req ");
        sql.append("               ON req.require_sid = confirmDep.require_sid ");
        sql.append("WHERE  confirmDep.status = 0 ");
        sql.append("       AND confirmDep.owner_sid != " + WkConstants.MANAGER_VIRTAUL_USER_SID + " ");
        sql.append("       AND confirmDep.prog_status IN ( " + progStatusCondition + " ); ");

        // ====================================
        // 查詢
        // ====================================
        List<KeepConfirmDepOwnerTo> keepConfirmDepOwnerTos = this.nativeSqlRepository.getResultList(
                sql.toString(), null, KeepConfirmDepOwnerTo.class);

        if (WkStringUtils.isEmpty(keepConfirmDepOwnerTos)) {
            log.debug("自動異動需求確認單位負責人：無需處理 (沒有符合狀態的資料)");
            return;
        }

        // ====================================
        // 處理
        // ====================================
        List<KeepConfirmDepOwnerTo> waitUpdates = Lists.newArrayList();
        for (KeepConfirmDepOwnerTo keepConfirmDepOwnerTo : keepConfirmDepOwnerTos) {
            // --------------------
            // 檢查負責人帳號停用
            // --------------------
            if (!WkUserUtils.isActive(keepConfirmDepOwnerTo.getOwnerSid())) {
                keepConfirmDepOwnerTo.setModifyReason("原負責人帳號停用，自動移轉為單位主管");
                waitUpdates.add(keepConfirmDepOwnerTo);
                continue;
            }

            // --------------------
            // 檢查負責人移轉單位
            // --------------------
            // 取得需求單位以下所有單位
            Set<Integer> orgSids = Sets.newHashSet(keepConfirmDepOwnerTo.getDepSid());
            Set<Integer> childOrgSids = WkOrgCache.getInstance().findAllChildSids(keepConfirmDepOwnerTo.getDepSid());
            if (WkStringUtils.notEmpty(childOrgSids)) {
                orgSids.addAll(childOrgSids);
            }
            // 取得單位下所有User
            Set<Integer> allUserSids = WkUserCache.getInstance().findUserSidByOrgsWithManager(
                    orgSids, Activation.ACTIVE);

            if (!allUserSids.contains(keepConfirmDepOwnerTo.getOwnerSid())) {
                keepConfirmDepOwnerTo.setModifyReason("原負責人已不在原單位，自動移轉為單位主管");
                waitUpdates.add(keepConfirmDepOwnerTo);
                continue;
            }
        }

        if (WkStringUtils.isEmpty(waitUpdates)) {
            log.debug("自動異動需求確認單位負責人：無需處理 (沒有需要異動的負責人)");
            return;
        }

        // ====================================
        // UPDATE
        // ====================================
        String updateRequireConfirmSids = waitUpdates.stream()
                .map(to -> to.getRequireConfirmSid() + "")
                .collect(Collectors.joining(", "));

        String updateSQL = ""
                + " UPDATE tr_require_confirm_dep "
                + "    SET owner_sid  = " + WkConstants.MANAGER_VIRTAUL_USER_SID
                + "  WHERE require_confirm_sid IN (" + updateRequireConfirmSids + ")";

        this.nativeSqlRepository.executeNativeSql(updateSQL, null);

        // ====================================
        // 追蹤
        // ====================================
        this.requireConfirmDepHistoryService.saveKeepConfirmDepOwnerTrace(waitUpdates);

    }

}
