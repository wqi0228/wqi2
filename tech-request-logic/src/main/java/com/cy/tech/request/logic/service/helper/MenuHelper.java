/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.helper;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.enumerate.ReportCntQueryType;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.vo.enums.InboxType;
import com.cy.work.logic.WkUserAndOrgLogic;
import com.google.common.base.Strings;

/**
 * @author shaun
 */
@Component
public class MenuHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 471620414906878533L;

    @Autowired
    private SearchService searchService;

    @Autowired
    private WkUserAndOrgLogic wkUserAndOrgLogic;

    public void checkParamUser(String countSql, User user, Map<String, Object> parameters) {
        if (countSql.contains(":user")) {
            parameters.put("user", user.getSid());
        }
    }

    public void checkParamRegexpDeps(
            String countSql,
            User user,
            Map<String, Object> parameters) {
        if (countSql.contains(":regexoDepSids")) {

            String regexp = wkUserAndOrgLogic.prepareCanViewDepSidBaseOnOrgLevelWithParallel(
                    user.getSid(),
                    OrgLevel.MINISTERIAL,
                    false) // 全部排除特殊可閱部門
                    .stream().map(each -> "\"" + each + "\"")
                    .collect(Collectors.joining(","));
            parameters.put("regexoDepSids", Strings.isNullOrEmpty(regexp) ? "nerverFind" : regexp);
        }
    }

    public void checkParamStartDate(String countSql, int startDateRange, Map<String, Object> parameters) {
        if (countSql.contains(":startDate")) {
            Date date = searchService.addDay(Calendar.getInstance(), startDateRange);
            parameters.put("startDate", searchService.transStartDate(date));
        }
    }

    public void checkParamEndDate(String countSql, int endDateRange, Map<String, Object> parameters) {
        if (countSql.contains(":endDate")) {
            Date date = searchService.addDay(Calendar.getInstance(), endDateRange);
            parameters.put("endDate", searchService.transEndDate(date));
        }
    }

    public void checkParamTrace(String countSql, User user, Map<String, Object> parameters) {
        if (countSql.contains(":traceDep")) {
            parameters.put("traceDep", user.getPrimaryOrg().getSid());
        }
        if (countSql.contains(":traceUser")) {
            parameters.put("traceUser", user.getSid());
        }
    }

    public void setQueryParameter(Query query, Map<String, Object> parameters) {
        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
    }

    public InboxType findInboxType(String countSql) {
        for (InboxType each : InboxType.values()) {
            if (countSql.equals("INBOX_" + each.name())) {
                return each;
            }
        }
        return null;
    }

    public ReportCntQueryType findReportCntQueryType(String countSql) {
        for (ReportCntQueryType each : ReportCntQueryType.values()) {
            if (countSql.equals("RPCQ_" + each.name())) {
                return each;
            }
        }
        return null;
    }

}
