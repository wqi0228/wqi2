/**
 * 
 */
package com.cy.tech.request.logic.service.syncmms;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.cy.commons.vo.User;
import com.cy.employee.rest.client.simple.EmployeeInfoClient;
import com.cy.employee.rest.client.vo.EmployeeInfo;
import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.customer.ReqWorkCustomerHelper;
import com.cy.tech.request.logic.service.log.LogSyncMmsService;
import com.cy.tech.request.logic.service.onpg.OnpgService;
import com.cy.tech.request.logic.service.syncmms.helper.SyncMmsParamHelper;
import com.cy.tech.request.logic.service.syncmms.helper.SyncMmsSyncFormProcessDispatcher;
import com.cy.tech.request.logic.service.syncmms.to.SyncFormTo;
import com.cy.tech.request.logic.utils.RequireSkypeAlertHelper;
import com.cy.tech.request.logic.vo.WorkOnpgTo;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.log.LogSyncMms;
import com.cy.tech.request.vo.log.LogSyncMmsActionType;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.client.syncmms.WorkSyncMmsClient;
import com.cy.work.client.syncmms.WorkSyncMmsFindFormClient;
import com.cy.work.client.syncmms.to.formdata.SyncMmsFormDataActionType;
import com.cy.work.client.syncmms.to.formdata.SyncMmsFormDataModel;
import com.cy.work.client.syncmms.to.token.SyncMmsTokenModel;
import com.cy.work.common.cache.WkCommonCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.alert.TechRequestAlertException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.customer.vo.WorkCustomer;
import com.cy.work.customer.vo.enums.EnableType;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Service
@Slf4j
public class SyncMmsSyncFormLogic {

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    transient private EmployeeInfoClient employeeInfoClient;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private LogSyncMmsService logSyncMmsService;
    @Autowired
    private transient OnpgService onpgService;
    @Autowired
    transient private WorkSyncMmsClient workSyncMmsClient;
    @Autowired
    transient private WorkSyncMmsFindFormClient workSyncMmsFindFormClient;
    @Autowired
    transient private ReqWorkCustomerHelper workCustomerService;
    @Autowired
    transient private SyncMmsSyncFormProcessDispatcher syncMmsCommonLogic;

    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    transient private JdbcTemplate jdbcTemplate;

    // ========================================================================
    // 變數
    // ========================================================================
    /**
     * 
     */
    private final String processName = "【MMS 系統介接-同步MMS維護單】";

    /**
     * WkCommonCache name
     */
    private static final String cacheNameForFindAllEmployeeInfo = SyncMmsSyncFormLogic.class.getSimpleName() + "_findUserByGMailAccount_FindAll";

    /**
     * WkCommonCache 逾期時間:半小時更新一次
     */
    private static final Integer cacheOverdueForFindAllEmployeeInfo = 3 * 60 * 1000;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 執行
     * 
     * @param loginCompID 登入公司別 (非人為操作時可為空)
     * @return 執行筆數
     * @throws SystemOperationException 錯誤時拋出
     */
    public int process(String loginCompID) throws SystemOperationException {

        // ====================================
        // 取得 token
        // ====================================
        SyncMmsTokenModel syncMmsTokenModel = null;
        try {
            syncMmsTokenModel = this.workSyncMmsClient.getToken(
                    SyncMmsParamHelper.getInstance().getAPIHostDomain(),
                    SyncMmsParamHelper.getInstance().getAPIGetTokenClientID(),
                    SyncMmsParamHelper.getInstance().getAPIGetTokenClientSecret(),
                    loginCompID);
        } catch (SystemOperationException e) {
            throw e;
        } catch (Exception e) {
            String errorMessage = processName + "取得 token 失敗! [" + e.getMessage() + "]";
            log.error(errorMessage, e);
            throw new TechRequestAlertException(errorMessage, loginCompID);
        }

        log.info(processName + "：取得 token 成功!");

        // ====================================
        // 處理新資料
        // ====================================
        int procCount = this.processNewData(
                SyncMmsParamHelper.getInstance().getAPIHostDomain(),
                SyncMmsParamHelper.getInstance().getAPIPortal(),
                syncMmsTokenModel);

        // ====================================
        // 處理之前異常的資料
        // ====================================
        procCount += this.processDoAgain(
                SyncMmsParamHelper.getInstance().getAPIHostDomain(),
                SyncMmsParamHelper.getInstance().getAPIPortal(),
                syncMmsTokenModel);

        // ====================================
        // 避免新單查不到，進行需求單同步
        // ====================================
        // 避免同時同步. 取消此邏輯
        // try {
        // this.syncMmsRequireDataLogic.process(false, loginCompID);
        // } catch (Exception e) {
        // String errorMessage = processName + "同步需求單失敗!" + e.getMessage();
        // log.error(errorMessage, e);
        // throw new TechRequestAlertException(errorMessage, loginCompID);
        // }

        return procCount;

    }

    /**
     * @param hostDomain
     * @param portal
     * @param syncMmsTokenModel
     * @throws SystemOperationException
     */
    private int processDoAgain(
            String hostDomain,
            String portal,
            SyncMmsTokenModel syncMmsTokenModel) throws SystemOperationException {

        // ====================================
        // 查詢上次失敗，需要重做的維護單
        // ====================================
        List<String> waitDoAgainMmsIds = this.logSyncMmsService.queryMmsIdsByWaitDoAgain();

        if (WkStringUtils.isEmpty(waitDoAgainMmsIds)) {
            log.info(processName + "：此次沒有需要『失敗重做』的資料!");
            return 0;
        }

        // ====================================
        // 逐筆處理
        // ====================================
        for (String mmsId : waitDoAgainMmsIds) {

            // -------------------
            // 同步維護單資料
            // -------------------
            SyncMmsFormDataModel mmsFormDataModel = null;
            try {
                mmsFormDataModel = this.workSyncMmsFindFormClient.findMMSFormByMmsId(hostDomain, portal, syncMmsTokenModel, mmsId);
            } catch (SystemOperationException e) {
                throw e;
            } catch (Exception e) {
                String errorMessage = processName + "同步資料失敗! [" + e.getMessage() + "]";
                log.error(errorMessage, e);
                throw new TechRequestAlertException(errorMessage, syncMmsTokenModel.getCompID());
            }

            // -------------------
            // 處理單一單據
            // -------------------
            this.processMmsFormData(mmsFormDataModel, syncMmsTokenModel);

        }

        return waitDoAgainMmsIds.size();

    }

    private int processNewData(
            String hostDomain,
            String portal,
            SyncMmsTokenModel syncMmsTokenModel) throws SystemOperationException {

        // ====================================
        // 取得查詢起日
        // ====================================
        Date latestMmsDate = this.logSyncMmsService.queryLatestMmsDate();
        log.info(processName + "查詢起日:[" + WkDateUtils.formatDate(latestMmsDate, WkDateUtils.YYYY_MM_DD_HH24_mm_ss) + "]");

        // ====================================
        // 取得MMS維護單
        // ====================================
        List<SyncMmsFormDataModel> mmsFormDatas = Lists.newArrayList();
        try {
            mmsFormDatas = this.workSyncMmsFindFormClient.findMMSFormListByStartDateTime(
                    hostDomain, portal, syncMmsTokenModel, latestMmsDate);
        } catch (SystemOperationException e) {
            throw e;
        } catch (Exception e) {
            String errorMessage = processName + "同步資料失敗! [" + e.getMessage() + "]";
            log.error(errorMessage, e);
            throw new TechRequestAlertException(errorMessage, syncMmsTokenModel.getCompID());
        }

        if (WkStringUtils.isEmpty(mmsFormDatas)) {
            log.info(processName + "：此次沒有需要同步的資料!");
            return 0;
        }

        log.info(processName + "取得需處理資料[" + mmsFormDatas.size() + "]筆");

        // ====================================
        // 逐筆處理
        // ====================================
        for (SyncMmsFormDataModel workData : mmsFormDatas) {
            this.processMmsFormData(workData, syncMmsTokenModel);
        }

        return mmsFormDatas.size();
    }

    /**
     * 執行MMS取回的資料 (單筆)
     * 
     * @param mmsFormDataModel  MMS 單據資料
     * @param syncMmsTokenModel
     */
    private LogSyncMms processMmsFormData(
            SyncMmsFormDataModel mmsFormDataModel,
            SyncMmsTokenModel syncMmsTokenModel) {

        // ====================================
        // 判斷此資料是否已經執行過了
        // ====================================
        boolean isProcessedAndSuccess = this.logSyncMmsService.isProcessedAndSuccess(
                mmsFormDataModel.getMmsID(),
                mmsFormDataModel.getMmsUpdateDate());

        if (isProcessedAndSuccess) {
            log.info(processName + "此資料已有同步成功記錄，無需處理！mmsID:[{}] mmsDateTime:[{}] ",
                    mmsFormDataModel.getMmsID(),
                    WkDateUtils.formatDate(mmsFormDataModel.getMmsUpdateDate(), WkDateUtils.YYYY_MM_DD_HH24_mm_ss2));

            // 1.不用打API回傳
            // 2.不寫 logSyncMms
            return null;
        }

        // ====================================
        // 來源資料解析
        // ====================================
        SyncFormTo syncFormTo = null;
        try {
            syncFormTo = this.prepareSyncFormTo(mmsFormDataModel, syncMmsTokenModel.getCompID());
        } catch (Exception e) {
            String errorMessage = "解析來源資料失敗!" + e.getMessage();
            log.error("MMS同步-" + errorMessage, e);
            syncFormTo = new SyncFormTo(mmsFormDataModel, syncFormTo.getLoginCompID());
            syncFormTo.addErrorInfo(errorMessage);
            // Exception 時也要打 API 故不 return;
        }

        // ====================================
        // 執行，並將處理結果同步回MMS
        // ====================================
        try {
            this.syncMmsCommonLogic.processAndSyncResult(syncFormTo, syncMmsTokenModel);
        } catch (SystemOperationException e) {
            syncFormTo.addErrorInfo(0, e.getMessage() + "<br/>");
            if (LogSyncMmsActionType.NONE.equals(syncFormTo.getLogSyncMmsActionType())) {
                syncFormTo.setLogSyncMmsActionType(LogSyncMmsActionType.EXCEPTION);
            }
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + e.getMessage() + "<br/>";
            log.error("MMS同步-" + errorMessage, e);
            syncFormTo.addErrorInfo(0, e.getMessage());
            syncFormTo.setLogSyncMmsActionType(LogSyncMmsActionType.EXCEPTION);
        }

        // ====================================
        // log
        // ====================================
        LogSyncMms logSyncMms = new LogSyncMms();
        logSyncMms.setCreatedUser(syncFormTo.getCreateUser() == null ? 1 : syncFormTo.getCreateUser().getSid());
        logSyncMms.setCreatedDate(new Date());

        // 處理結果
        logSyncMms.setSuccess(syncFormTo.getLogSyncMmsActionType().isSuccess());
        // 動作類別
        logSyncMms.setActionType(syncFormTo.getLogSyncMmsActionType());

        // 需求單 SID
        if (syncFormTo.getRequire() != null) {
            logSyncMms.setRequireSid(syncFormTo.getRequire().getSid());
        }
        // ON程式單 SID
        if (syncFormTo.getWorkOnpgTo() != null) {
            logSyncMms.setOnpgSid(syncFormTo.getWorkOnpgTo().getSid());
        }

        // MMS維護單ID
        logSyncMms.setMmsID(syncFormTo.getMmsID());
        // MMS維護單異動時間
        logSyncMms.setMmsDateTime(syncFormTo.getMmsUpdateDate());
        // API receive data (由MMS收到的完整資料)
        logSyncMms.setMmsReceiveData(mmsFormDataModel);
        // 回應給MMS的處理結果資料
        logSyncMms.setProcessResult(syncFormTo.getProcessResult());
        // memo
        if (syncFormTo.getLogSyncMmsActionType() != null
                && syncFormTo.getLogSyncMmsActionType().isUpdateSuccess()) {
            // 為異動資料時，寫異動記錄
            logSyncMms.setMemo(syncFormTo.getModifyInfosStr("\r\n"));
        } else {
            logSyncMms.setMemo(syncFormTo.getErrorInfosStr("\r\n"));
        }

        // insert
        this.logSyncMmsService.save(logSyncMms);

        // ====================================
        // console log
        // ====================================
        if (!logSyncMms.isSuccess()) {
            // 有錯誤訊息時，印出詳細LOG
            String message = String.format(""
                    + "\r\n==================================================================="
                    + "\r\n【MMS同步 處理失敗!】"
                    + "\r\n【工作ID】:[%s]"
                    + "\r\n【錯誤訊息】：[\r\n%s\r\n]"
                    + "\r\n【MMS資料】:[\r\n%s\r\n]"
                    + "\r\n【發送資料】:[\r\n%s\r\n]"
                    + "\r\n===================================================================",
                    syncFormTo.getMmsID(),
                    this.formatLogInfo(syncFormTo.getErrorInfosStr("\r\n")),
                    this.formatLogInfo(WkJsonUtils.getInstance().toPettyJson(mmsFormDataModel)),
                    this.formatLogInfo(WkJsonUtils.getInstance().toPettyJson(syncFormTo.getProcessResult())));
            log.error(message);
            RequireSkypeAlertHelper.getInstance().sendSkypeAlert(message);
        } else {
            log.info(""
                    + "\r\n==================================================================="
                    + "\r\n【MMS同步 處理成功!】"
                    + "\r\n【動作】：[{}]"
                    + "\r\n【維護單號】:[{}]"
                    + "\r\n【需求單號】:[{}]"
                    + "\r\n【程式單號】:[{}]"
                    + "\r\n【回應資料】:[{}]"
                    + "\r\n===================================================================",
                    logSyncMms.getActionType().getDescr() + " [" + logSyncMms.getActionType() + "]",
                    syncFormTo.getMmsID(),
                    syncFormTo.getRequireNo(),
                    syncFormTo.getOnpgNo(),
                    this.formatLogInfo(WkJsonUtils.getInstance().toPettyJson(syncFormTo.getProcessResult())));
        }

        // -------------------
        // 更新已處理註記
        // -------------------
        this.logSyncMmsService.updateIsDoAgain(syncFormTo.getMmsID(), logSyncMms);

        return logSyncMms;
    }

    private String formatLogInfo(String logInfo) {
        logInfo = WkStringUtils.safeTrim(logInfo);
        logInfo = logInfo.replaceAll("<br/>", "\n");
        logInfo = logInfo.replaceAll("\r\n", "\n");
        logInfo = logInfo.replaceAll("\n", "\r\n\t");
        return "\t" + logInfo;
    }

    /**
     * 解析收到的資料
     * 
     * @param mmsFormDataModel MMS收到的資料
     * @param loginCompId      登入公司ID (可為空)
     * @return SyncFormTo
     */
    private SyncFormTo prepareSyncFormTo(
            SyncMmsFormDataModel mmsFormDataModel,
            String loginCompId) {

        // 說明：檢核錯誤不停止往下檢核, 會判斷完所有資料

        // ====================================
        // 初始化 SyncFormTo
        // ====================================
        SyncFormTo syncFormTo = new SyncFormTo(mmsFormDataModel, loginCompId);

        // ====================================
        // 逐一處理每個欄位
        // ====================================

        // ---------------------
        // 1.異動類型 (change_type)
        // ---------------------
        if (syncFormTo.getActionType() == null) {
            String errorMessage = "傳入【異動類型】無法解析或為空 change_type:[" + syncFormTo.getChangeType_src() + "]";
            log.error(errorMessage);
            syncFormTo.addErrorInfo(errorMessage);
        }

        // ---------------------
        // 2.維護單ID (order_code)
        // ---------------------
        if (WkStringUtils.isEmpty(syncFormTo.getMmsID())) {
            String errorMessage = "傳入【工作ID】(order_code) 為空";
            log.error(errorMessage);
            syncFormTo.addErrorInfo(errorMessage);
        }
        syncFormTo.setMmsID(WkStringUtils.safeTrim(syncFormTo.getMmsID()));

        // ---------------------
        // 3.資料修改時間 (modified_date)
        // ---------------------
        if (syncFormTo.getMmsUpdateDate() == null) {
            String errorMessage = "傳入【資料修改時間】(modified_date) 為空";
            log.error(errorMessage);
            syncFormTo.addErrorInfo(errorMessage);
        }

        // ---------------------
        // 4.立單人員 (creator)
        // ---------------------
        try {
            User createUser = this.findUserByGMailAccount(syncFormTo.getCreateUserGmailAccount(), loginCompId);
            syncFormTo.setCreateUser(createUser);
        } catch (SystemOperationException e) {
            syncFormTo.addErrorInfo(e.getMessage());
        } catch (Exception e) {
            String errorMessage = "解析G-mail 帳號(creator)失敗! [" + syncFormTo.getCreateUserGmailAccount() + "]" + e.getMessage();
            log.error(errorMessage, e);
            syncFormTo.addErrorInfo(errorMessage);
        }

        // ---------------------
        // 5.需求單號 (demand_source_order)
        // ---------------------
        if (WkStringUtils.isEmpty(syncFormTo.getRequireNo())
                && SyncMmsFormDataActionType.DELETE.equals(syncFormTo.getActionType())) {
            syncFormTo.addErrorInfo("為刪除時，需求單號(demand_source_order)不可為空!");

        } else if (WkStringUtils.notEmpty(syncFormTo.getRequireNo())) {
            // 單號去空白+轉大寫
            syncFormTo.setRequireNo(WkStringUtils.safeTrim(syncFormTo.getRequireNo()).toUpperCase());

            // 查詢需求單
            Require require = this.requireService.findByReqNo(syncFormTo.getRequireNo());

            // 檢查
            if (require == null) {
                syncFormTo.addErrorInfo("需求單號(demand_source_order)[" + syncFormTo.getRequireNo() + "]不存在!");
            }
            // 處理狀態檢查 (1:可附掛ON程式單)
            else if (require.getRequireStatus().getSyncMMSStatus() != 1) {
                syncFormTo.addErrorInfo("需求單(demand_source_order)[" + syncFormTo.getRequireNo() + "]處理進度為[" + require.getRequireStatus().getValue() + "]無法附掛ON程式單!");
            }

            syncFormTo.setRequire(require);
        }

        // ---------------------
        // 6.廳主 (domain)
        // ---------------------
        if (!SyncMmsFormDataActionType.DELETE.equals(syncFormTo.getActionType())
                && WkStringUtils.isEmpty(syncFormTo.getRequireNo())) {
            // 查詢廳主
            WorkCustomer workCustomer = this.workCustomerService.findCopyOne(syncFormTo.getCusId());
            syncFormTo.setWorkCustomer(workCustomer);
            // 檢查
            if (workCustomer == null) {
                syncFormTo.addErrorInfo("找不到對應廳主資料!domain:[" + syncFormTo.getCusId() + "]");
            } else {

                // 為資料新增 (require 為空)
                boolean isAdd = (syncFormTo.getRequire() == null);
                // 為update (require 已經存在, 且廳主資料前後不同)
                boolean isUpdate = syncFormTo.getRequire() != null
                        && !WkCommonUtils.compareByStr(
                                syncFormTo.getCusId(),
                                syncFormTo.getRequire().getCustomer().getSid());

                // 檢查廳主是否停用
                if (isAdd || isUpdate) {
                    if (!EnableType.ENABLE.equals(workCustomer.getEnable())) {
                        syncFormTo.addErrorInfo("傳入廳主已停用!domain:[" + syncFormTo.getCusId() + "]");
                    }
                }
            }
        }

        // ---------------------
        // 7.系統別 (system)
        // ---------------------
        if (!SyncMmsFormDataActionType.DELETE.equals(syncFormTo.getActionType())
                && WkStringUtils.isEmpty(syncFormTo.getRequireNo())) {
            List<RequireCheckItemType> checkItemTypes = Lists.newArrayList();
            if (WkStringUtils.notEmpty(syncFormTo.getCheckItemTypesStr())) {
                for (String checkItemTypeStr : syncFormTo.getCheckItemTypesStr().split(",")) {
                    if (WkStringUtils.isEmpty(checkItemTypeStr)) {
                        continue;
                    }
                    RequireCheckItemType requireCheckItemType = RequireCheckItemType.safeValueOf(checkItemTypeStr);
                    if (requireCheckItemType == null) {
                        syncFormTo.addErrorInfo("傳入系統別資料有誤!system:[" + syncFormTo.getCheckItemTypesStr() + "]");
                    } else {
                        checkItemTypes.add(requireCheckItemType);
                    }
                }
            }

            if (WkStringUtils.isEmpty(checkItemTypes)) {
                syncFormTo.addErrorInfo("傳入系統別資料為空!system:[" + syncFormTo.getCheckItemTypesStr() + "]");
            }

            syncFormTo.setCheckItemTypes(checkItemTypes);
        }

        // ---------------------
        // 8.是否通知市場 (notice)
        // ---------------------
        if (!SyncMmsFormDataActionType.DELETE.equals(syncFormTo.getActionType())) {
            String noticeMarket_src = WkStringUtils.safeTrim(syncFormTo.getNoticeMarket_src());
            if (WkStringUtils.isEmpty(noticeMarket_src)) {
                syncFormTo.addErrorInfo("傳入【是否通知市場】(notice)欄位為空!");
            } else if (!"1".equals(noticeMarket_src)
                    && !"0".equals(noticeMarket_src)) {
                syncFormTo.addErrorInfo("傳入【是否通知市場】(notice)資料無法辨識![" + noticeMarket_src + "]");
            }
        }

        // ---------------------
        // 9.主題 (job)
        // ---------------------
        if (!SyncMmsFormDataActionType.DELETE.equals(syncFormTo.getActionType())) {
            // 檢查新的主題為空
            if (WkStringUtils.isEmpty(syncFormTo.getTheme())) {
                syncFormTo.addErrorInfo("傳入主題(job)為空!");
            }
            syncFormTo.setTheme(WkStringUtils.safeTrim(syncFormTo.getTheme()));
        }

        // ---------------------
        // 10.內容 (on_program_content)
        // ---------------------
        if (!SyncMmsFormDataActionType.DELETE.equals(syncFormTo.getActionType())) {
            // 去 HTML tag 之後再檢查
            String noneCssOnpgCentent = WkStringUtils.safeTrim(
                    WkJsoupUtils.getInstance().htmlToText(
                            syncFormTo.getContent()));
            noneCssOnpgCentent = noneCssOnpgCentent.replaceAll("\r\n", "");
            noneCssOnpgCentent = noneCssOnpgCentent.replaceAll("\n", "");
            // 檢查
            if (WkStringUtils.isEmpty(noneCssOnpgCentent)) {
                syncFormTo.addErrorInfo("傳入內容(on_program_content)為空!");
            }
        }

        // ---------------------
        // 11.維護日期 (ONPG 預計完成日) (maintenance_date)
        // ---------------------
        if (!SyncMmsFormDataActionType.DELETE.equals(syncFormTo.getActionType())) {
            if (syncFormTo.getMaintenanceDate() == null) {
                String errorMessage = "傳入【維護時間】(maintenance_date) 為空";
                log.error(errorMessage);
                syncFormTo.addErrorInfo(errorMessage);
            }
        }

        // ---------------------
        // 12.ON程式單號 (on_program_order)
        // ---------------------
        if (WkStringUtils.isEmpty(syncFormTo.getOnpgNo())
                && SyncMmsFormDataActionType.DELETE.equals(syncFormTo.getActionType())) {
            syncFormTo.addErrorInfo("為刪除時，ON程式單(on_program_order)不可為空!");

        } else if (WkStringUtils.notEmpty(syncFormTo.getOnpgNo())) {
            // 單號去空白+轉大寫
            syncFormTo.setOnpgNo(WkStringUtils.safeTrim(syncFormTo.getOnpgNo()).toUpperCase());

            // 查詢ON程式單
            WorkOnpgTo workOnpgTo = this.onpgService.findToByOnpgNo(syncFormTo.getOnpgNo());

            // 檢查
            if (workOnpgTo == null) {
                syncFormTo.addErrorInfo("ON程式單(on_program_order)[" + syncFormTo.getOnpgNo() + "]不存在!");
            } else if (!workOnpgTo.isFromMms()) {
                syncFormTo.addErrorInfo("ON程式單(on_program_order)[" + syncFormTo.getOnpgNo() + "]非由MMS建立，無法異動!");
            }
            syncFormTo.setWorkOnpgTo(workOnpgTo);
        }

        return syncFormTo;
    }

    /**
     * 依據 GMail Account , 取得對應的 WERP User 資料
     * 
     * @param gmailAccount GMail Account
     * @param loginCompId  登入公司別 （可為空）
     * @return User
     * @throws SystemOperationException 系統處理錯誤時拋出 (WERP內部錯誤, 不回應給MMS)
     */
    private User findUserByGMailAccount(
            String gmailAccount,
            String loginCompId) throws SystemOperationException {

        // ====================================
        // 檢查傳入值
        // ====================================
        if (WkStringUtils.isEmpty(gmailAccount)) {
            String errorMessage = "傳入【立單人員】(creator) 為空！";
            log.error(errorMessage);
            throw new TechRequestAlertException(errorMessage, loginCompId);
        }

        gmailAccount = WkStringUtils.safeTrim(gmailAccount);

        // ====================================
        // 取得MAIL帳號對應MAP
        // ====================================
        // 由cache 取出
        Map<String, Integer> userSidMapByGmailAccount = WkCommonCache.getInstance().getCache(
                cacheNameForFindAllEmployeeInfo, null, cacheOverdueForFindAllEmployeeInfo);

        // 未建立逾期時，重建map資料
        if (WkStringUtils.isEmpty(userSidMapByGmailAccount)) {
            // employeeInfoClient 取得所有員工資料
            List<EmployeeInfo> employeeInfos = this.employeeInfoClient.findEmployeeInfoAll();
            if (WkStringUtils.isEmpty(employeeInfos)) {
                String errorMessage = WkMessage.PROCESS_FAILED + "無法取得員工資料(EmployeeInfo)";
                log.error(errorMessage);
                throw new TechRequestAlertException(errorMessage, loginCompId);
            }

            userSidMapByGmailAccount = Maps.newHashMap();

            // 轉為 map
            for (EmployeeInfo employeeInfo : employeeInfos) {
                // 資料不完整時略過
                if (WkStringUtils.isEmpty(employeeInfo.getGmail())) {
                    // 並不是每個帳號一定都有 GMail by yifan
                    continue;
                }

                // 處理GMail 帳號
                String currGmailAccount = WkStringUtils.safeTrim(employeeInfo.getGmail());
                // 取 @ 前面的資料為帳號
                if (currGmailAccount.indexOf("@") > 0) {
                    currGmailAccount = currGmailAccount.split("@")[0];
                }
                // 忽略大小寫
                currGmailAccount = currGmailAccount.toUpperCase();

                // 放入 map<GmailAccount, userSid>
                userSidMapByGmailAccount.put(currGmailAccount, employeeInfo.getMappedUserSid());
            }

            // 放入快取
            WkCommonCache.getInstance().putCache(cacheNameForFindAllEmployeeInfo, null, userSidMapByGmailAccount);
        }

        // ====================================
        // 取得對應 user
        // ====================================
        // 由map 取出對應 USER SID
        Integer userSid = userSidMapByGmailAccount.get(gmailAccount.toUpperCase());
        if (userSid == null) {
            String errorMessage = "傳入立單人員(creator):【" + gmailAccount + "】無法在WERP找到對應資料！";
            log.error(errorMessage);
            throw new TechRequestAlertException(errorMessage, loginCompId);
        }
        // 查詢 user 資料
        User user = WkUserCache.getInstance().findBySid(userSid);
        if (user == null) {
            String errorMessage = "無法取得USER資料!UserSid:[" + userSid + "]";
            log.error(errorMessage);
            throw new TechRequestAlertException(errorMessage, loginCompId);
        }

        return user;
    }

}
