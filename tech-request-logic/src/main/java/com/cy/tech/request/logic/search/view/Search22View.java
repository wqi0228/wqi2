/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import com.cy.commons.enums.Activation;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.trace.vo.enums.TraceStatus;
import com.cy.work.trace.vo.enums.TraceType;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 退件暫存單據查詢頁面顯示vo (search22.xhtml)
 *
 * @author jason_h
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Search22View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3263219193608150749L;
    /** 部 */
    private Boolean hasForwardDep;
    /** 個 */
    private Boolean hasForwardMember;
    /** 關 */
    private Boolean hasLink;
    /** 追蹤類型 */
    private TraceType traceType;
    /** 小類 */
    private String smallName;
    /** 追蹤提醒日 */
    private Date noticeDate;
    /** 新增者 */
    private Integer traceCreatedUser;
    /** 追蹤狀態 */
    private TraceStatus traceStatus;
    /** 追蹤事項 */
    private String memo;
    /** 追蹤事項_CSS格式 */
    private String memoCss;
    /** 最後異動時間 */
    private Date requireUpdatedDate;
    /** 追蹤時間 */
    private Date traceCreatedDate;
    /** 緊急度 */
    private UrgencyType urgency;
    /** 本地端連結網址 */
    private String localUrlLink;
    /** 單據狀態 */
    private Activation status;
    /** 是否有勾選欄位 */
    private boolean hasSelCheckBox;
}
