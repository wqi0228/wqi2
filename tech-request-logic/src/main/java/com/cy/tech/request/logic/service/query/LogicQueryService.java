/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.query;

import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Component;

/**
 * 執行QueryService
 *
 * @author brain0925_liao
 */
@Component("logicQueryService")
public class LogicQueryService {

    @PersistenceContext
    transient private EntityManager em;

    /**
     * 執行Query
     *
     * @param sql 查詢語法
     * @param parameters 參數
     * @return
     */
    @SuppressWarnings("rawtypes")
    public List findWithQuery(String sql, Map<String, Object> parameters) {
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        return query.getResultList();
    }
}
