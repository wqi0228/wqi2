/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.send.test;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.CommonService;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.RequireTraceService;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.repository.worktest.WorkTestAttachmentRepo;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.worktest.WorkTestAttachment;
import com.cy.tech.request.vo.worktest.WorkTestInfoHistory;
import com.cy.tech.request.vo.worktest.enums.WorkTestAttachmentBehavior;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkAttachUtils;
import com.cy.work.common.vo.AttachmentService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;
import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * WorkTestInfo 附加檔案服務
 *
 * @author shaun
 */
@Component("send_test_history_attach")
public class SendTestHistoryAttachService implements AttachmentService<WorkTestAttachment, WorkTestInfoHistory, String>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 356197155316439772L;
    @Value("${erp.attachment.root}")
    private String attachPath;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private UserService userService;
    @Autowired
    private RequireTraceService traceService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private WorkTestAttachmentRepo attachDao;
    @Autowired
    private WkAttachUtils attachUtils;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

    @Transactional(readOnly = true)
    @Override
    public WorkTestAttachment findBySid(String sid) {
        WorkTestAttachment a = attachDao.findOne(sid);
        return a;
    }

    @Transactional(readOnly = true)
    @Override
    public List<WorkTestAttachment> findAttachsByLazy(WorkTestInfoHistory history) {
        try {
            history.getAttachments().size();
        } catch (LazyInitializationException e) {
            //log.debug("findAttachsByLazy lazy init error :" + e.getMessage());
            history.setAttachments(this.findAttachs(history));
        }
        return history.getAttachments();
    }

    @Transactional(readOnly = true)
    @Override
    public List<WorkTestAttachment> findAttachs(WorkTestInfoHistory e) {
        if (Strings.isNullOrEmpty(e.getSid())) {
            if (e.getAttachments() == null) {
                e.setAttachments(Lists.newArrayList());
            }
            return e.getAttachments();
        }
        return attachDao.findByHistoryAndStatusOrderByCreatedDateDesc(e, Activation.ACTIVE);
    }

    @Override
    public String getAttachPath() {
        return attachPath;
    }

    @Override
    public String getDirectoryName() {
        return "tech-request";
    }

    @Override
    public WorkTestAttachment createEmptyAttachment(String fileName, User executor, Org dep) {
        return (WorkTestAttachment) attachUtils.createEmptyAttachment(new WorkTestAttachment(), fileName, executor, dep);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public WorkTestAttachment updateAttach(WorkTestAttachment attach) {
        WorkTestAttachment a = attachDao.save(attach);
        return a;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveAttach(WorkTestAttachment attach) {
        attachDao.save(attach);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveAttach(List<WorkTestAttachment> attachs) {
        attachDao.save(attachs);
    }

    @Override
    public String depAndUserName(WorkTestAttachment attachment) {
        if (attachment == null) {
            return "";
        }
        User usr = WkUserCache.getInstance().findBySid(attachment.getCreatedUser().getSid());
        String parentDepStr = orgService.showParentDep(WkOrgCache.getInstance().findBySid(usr.getPrimaryOrg().getSid()));
        return parentDepStr + "-" + usr.getName();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void linkRelation(List<WorkTestAttachment> attachments, WorkTestInfoHistory e, User login) {
        attachments.forEach(each -> this.setRelation(each, e));
        this.saveAttach(attachments);
        e.setAttachments(this.findAttachs(e));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void linkRelation(WorkTestAttachment ra, WorkTestInfoHistory e, User login) {
        this.setRelation(ra, e);
        this.updateAttach(ra);
    }

    private void setRelation(WorkTestAttachment ra, WorkTestInfoHistory e) {
        ra.setTestInfo(e.getTestInfo());
        ra.setTestinfoNo(e.getTestInfo().getTestinfoNo());
        ra.setSourceType(e.getTestInfo().getSourceType());
        ra.setSourceSid(e.getTestInfo().getSourceSid());
        ra.setSourceNo(e.getTestInfo().getSourceNo());
        ra.setHistory(e);
        ra.setBehavior(this.findBehavior(e));
        ra.setDescSystem(this.createDescSysStr(ra));
        if (!ra.getKeyChecked()) {
            ra.setStatus(Activation.INACTIVE);
        }
        ra.setKeyChecked(Boolean.FALSE);
    }

    private WorkTestAttachmentBehavior findBehavior(WorkTestInfoHistory e) {
        return WorkTestAttachmentBehavior.valueOf(e.getBehavior().name());
    }

    private String createDescSysStr(WorkTestAttachment attach) {
        String behaviorStr = commonService.get(attach.getBehavior());
        return "(" + behaviorStr + " - " + sdf.format(attach.getCreatedDate()) + ")";
    }

    /**
     * 刪除
     *
     * @param attachments
     * @param attachment
     * @param history
     * @param login
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void changeStatusToInActive(List<WorkTestAttachment> attachments, WorkTestAttachment attachment, WorkTestInfoHistory history, User login) {
        WorkTestAttachment ra = (WorkTestAttachment) attachment;
        ra.setStatus(Activation.INACTIVE);
        this.updateAttach(attachment);
        history.setAttachments(this.findAttachs(history));
        this.createDeleteTrace(attachment, history, login);
    }

    private void createDeleteTrace(WorkTestAttachment attachment, WorkTestInfoHistory history, User login) {
        Require require = new Require();
        require.setSid(history.getSourceSid());
        require.setRequireNo(history.getSourceNo());
        String content = ""
                + "檔案名稱：【" + attachment.getFileName() + "】\n"
                + "上傳成員：【" + userService.getUserName(attachment.getCreatedUser()) + "】\n"
                + "\n"
                + "刪除來源：【送測 － 單號：" + history.getTestinfoNo() + " － " + commonService.get(history.getBehavior()) + "】\n"
                + "刪除成員：【" + login.getName() + "】\n"
                + "刪除註記：" + attachment.getSid();
        traceService.createRequireTrace(require.getSid(), login, RequireTraceType.DELETE_ATTACHMENT, content);
    }
}
