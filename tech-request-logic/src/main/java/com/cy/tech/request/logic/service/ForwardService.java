/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.repository.require.AlertInboxRepository;
import com.cy.tech.request.repository.require.TrAlertInboxRepository;
import com.cy.tech.request.vo.enums.ForwardType;
import com.cy.tech.request.vo.require.AlertInbox;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.TrAlertInbox;
import com.cy.tech.request.vo.require.TrAlertInboxHisVO;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 轉寄服務 -> 收件夾
 *
 * @author shaun
 */
@Component
public class ForwardService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1856028606338864380L;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private AlertInboxService inboxService;
    @Autowired
    private AlertInboxRepository alertInboxDao;
    @Autowired
    private TrAlertInboxRepository trAlertInboxRepository;

    /**
     * v 取全部轉發部門資訊
     *
     * @param require
     * @return
     */
    @Transactional(readOnly = true)
    public List<AlertInbox> findDeptByRequireOrderByCreatedDateDesc(Require require) {
        return alertInboxDao.findByRequireAndForwardTypeOrderByCreatedDateDesc(require, ForwardType.FORWARD_DEPT);
    }

    /**
     * 取得轉發資訊
     * 
     * @param requireNo
     * @return
     */
    @Transactional(readOnly = true)
    public List<AlertInbox> findDeptByRequireOrderByCreatedDateDesc(String requireNo) {
        return alertInboxDao.findByRequireNoAndForwardTypeOrderByCreatedDateDesc(
                requireNo,
                ForwardType.FORWARD_DEPT);
    }

    /**
     * 取得全部已轉發部門資訊(Org)
     *
     * @param require
     * @param status
     * @return
     */
    @Transactional(readOnly = true)
    public List<Org> findReceiveDepByRequireAndStatus(Require require, Activation status) {
        List<Org> jpaResult = alertInboxDao.findReceiveDepByRequireAndStatus(require, status, ForwardType.FORWARD_DEPT);
        List<Org> result = Lists.newArrayList();
        jpaResult.forEach(each -> {
            Org org = orgService.findBySid(each.getSid());
            if (!result.contains(org)) {
                result.add(org);
            }
        });
        return result;
    }

    @Transactional(readOnly = true)
    public List<Org> findReceiveDepByRequireNoAndStatus(String requireNo, Activation status) {
        List<AlertInbox> jpaResult = alertInboxDao.findByRequireNoAndStatusAndForwardType(requireNo, status, ForwardType.FORWARD_DEPT);
        return jpaResult.stream().distinct().map(a -> a.getReceiveDep()).collect(Collectors.toList());
    }

    /**
     * 取得全部已轉發部門資訊(Org)
     *
     * @param require
     * @param status
     * @param senders
     * @return
     */
    @Transactional(readOnly = true)
    public List<Org> findReceiveDepByRequireAndStatusAndSender(Require require, Activation status, List<User> senders) {
        List<Org> jpaResult = alertInboxDao.findReceiveDepByRequireAndStatusAndSenderIn(require, status, ForwardType.FORWARD_DEPT, senders);
        List<Org> result = Lists.newArrayList();
        jpaResult.forEach(each -> {
            Org org = orgService.findBySid(each.getSid());
            if (!result.contains(org)) {
                result.add(org);
            }
        });
        return result;
    }

    /**
     * 取得全部已轉發部門資訊(Org)
     *
     * @param require
     * @param status
     * @param sendDeps
     * @return
     */
    @Transactional(readOnly = true)
    public List<Org> findReceiveDepByRequireAndStatusAndSenderDepIn(Require require, Activation status, List<Org> sendDeps) {
        List<Org> jpaResult = alertInboxDao.findReceiveDepByRequireAndStatusAndSenderDepIn(require, status, ForwardType.FORWARD_DEPT, sendDeps);
        List<Org> result = Lists.newArrayList();
        jpaResult.forEach(each -> {
            Org org = orgService.findBySid(each.getSid());
            if (!result.contains(org)) {
                result.add(org);
            }
        });
        return result;
    }

    /**
     * 儲存個人轉寄
     *
     * @param require
     * @param executor
     * @param targetUsers
     * @param messageCss
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean saveForwardMember(Require require, Integer executor, List<User> targetUsers, String messageCss) {
        inboxService.createInboxMember(
                require.getRequireNo(),
                executor,
                targetUsers.stream().map(User::getSid).collect(Collectors.toList()),
                messageCss);
        return true;
    }

    /**
     * 取得全部轉發成員 v
     *
     * @param require
     * @return
     */
    @Transactional(readOnly = true)
    public List<AlertInbox> findMemberByRequireOrderByCreatedDateDesc(Require require) {
        return alertInboxDao.findByRequireAndForwardTypeOrderByCreatedDateDesc(require, ForwardType.FORWARD_MEMBER);
    }

    @Transactional(readOnly = true)
    public List<User> findReceiveDepByRequireAndStatusAndRecipients(Require require, Activation status, List<User> targetUsers) {
        List<User> checkUsers = alertInboxDao.findReceiveByRequireAndStatusAndRecipients(require, status, targetUsers, ForwardType.FORWARD_MEMBER);
        List<User> fullUsers = Lists.newArrayList();
        checkUsers.stream()
                .map(each -> WkUserCache.getInstance().findBySid(each.getSid()))
                .filter(usr -> !fullUsers.contains(usr))
                .forEach(usr -> fullUsers.add(usr));
        return fullUsers;
    }

    /**
     * 依據需求單, 查詢所有轉寄資料
     * 
     * @param requireNo 需求單號
     * @return
     */
    public Map<ForwardType, List<TrAlertInboxHisVO>> prepareForwardInfoByRequire(String requireNo) {
        // ================================
        // 依據需求單, 查詢所有轉寄資料
        // ================================
        List<TrAlertInbox> entities = this.trAlertInboxRepository.findByRequireNo(requireNo);
        if (entities == null) {
            entities = Lists.newArrayList();
        }

        // ================================
        // 準備排序規則
        // ================================
        // 取得所有部門排序資料
        Map<Integer, Integer> sortSeqMapByOrgSid = WkOrgCache.getInstance().findAllOrgOrderSeqMap();

        // 排序
        // 1.轉寄時間反向排序
        Comparator<TrAlertInboxHisVO> comparator = Comparator.comparing(
                TrAlertInboxHisVO::getCreatedDate, Comparator.reverseOrder());

        // 2.以收件部門順序排序
        comparator = comparator.thenComparing(Comparator.comparing(
                vo -> WkOrgUtils.prepareOrgSortSeq(sortSeqMapByOrgSid, vo.getReceiveDep())));

        // 3.以收件人名稱
        comparator = comparator.thenComparing(Comparator.comparing(
                vo -> WkUserUtils.findNameBySid(vo.getReceive())));

        // ================================
        // 轉為 VO, 並處理顯示資訊
        // ================================
        List<TrAlertInboxHisVO> allVOs = entities.stream()
                .map(trAlertInbox -> {
                    // 建立 VO
                    TrAlertInboxHisVO alertInboxVO = new TrAlertInboxHisVO(trAlertInbox);
                    // 處理顯示資訊
                    this.prepareForwardInfoByRequire_prepareShowInfo(alertInboxVO);
                    // return
                    return alertInboxVO;
                })
                .collect(Collectors.toList());

        // ================================
        // 整理轉寄部門資料
        // ================================
        // 分類-取得類別為『轉寄部門』
        List<TrAlertInboxHisVO> forwardDepInfos = allVOs.stream()
                .filter(trAlertInboxHisVO -> ForwardType.FORWARD_DEPT.equals(trAlertInboxHisVO.getForwardType()))
                .collect(Collectors.toList());

        // 排序
        forwardDepInfos = forwardDepInfos.stream().sorted(comparator).collect(Collectors.toList());

        // ================================
        // 整理轉寄個人資料
        // ================================
        // 分類-取得類別為『轉寄個人』
        List<TrAlertInboxHisVO> forwardPersonInfos = allVOs.stream()
                .filter(trAlertInboxHisVO -> ForwardType.FORWARD_MEMBER.equals(trAlertInboxHisVO.getForwardType()))
                .collect(Collectors.toList());

        // 排序
        forwardPersonInfos = forwardPersonInfos.stream().sorted(comparator).collect(Collectors.toList());

        // ================================
        // 組裝資料, 並回傳
        // ================================
        Map<ForwardType, List<TrAlertInboxHisVO>> resultMap = Maps.newHashMap();
        resultMap.put(ForwardType.FORWARD_DEPT, forwardDepInfos);
        resultMap.put(ForwardType.FORWARD_MEMBER, forwardPersonInfos);

        return resultMap;
    }

    /**
     * 準備 view 顯示內容
     * 
     * @param alertInboxVO
     */
    private void prepareForwardInfoByRequire_prepareShowInfo(TrAlertInboxHisVO alertInboxVO) {

        // 被轉寄部門 (只有轉寄部門才有)
        if (alertInboxVO.getReceiveDep() != null) {
            alertInboxVO.setReceiveDepShowName(
                    WkOrgUtils.makeupDepName(
                            WkOrgUtils.prepareBreadcrumbsByDepName(
                                    alertInboxVO.getReceiveDep(),
                                    OrgLevel.MINISTERIAL,
                                    false,
                                    "->"),
                            "->"));
        }

        // 收件者資訊(只有轉寄個人才有)
        if (alertInboxVO.getReceive() != null) {
            alertInboxVO.setReceiveShowName(WkUserUtils.findNameBySid(alertInboxVO.getReceive()));

            // for 排序用
            User user = WkUserCache.getInstance().findBySid(alertInboxVO.getReceive());
            if (user != null) {
                alertInboxVO.setReceiveDep(user.getPrimaryOrg().getSid());
            }

        }

        // 轉寄者資訊
        alertInboxVO.setSenderShowName(WkUserUtils.findNameBySid(alertInboxVO.getSender()));

        // 轉寄時間
        alertInboxVO.setCreatedDateShowName(
                WkDateUtils.formatDate(
                        alertInboxVO.getCreatedDate(),
                        WkDateUtils.YYYY_MM_DD_HH24_MI));

        // 狀態
        if (!alertInboxVO.isActive()) {
            alertInboxVO.setForwardStatus("&nbsp;(收回)");
        }

    }
}
