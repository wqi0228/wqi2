package com.cy.tech.request.logic.service.reqconfirm;

public enum RequireConfirmAddReasonType {
    // 
    /**
     * 開立子單、轉FB時，若使用者非隸屬於分派單位成員，則自動分派該單位,
     */
    AUTO_ASSIGN_BY_CREATE_CHILD_CASE,
    /**
     * 來自：內部需求新增單據時自動分派
     */
    AUTO_ASSIGN_BY_CREATE_INTERNAL,
    /**
     * 一般分派
     */
    NORMAL,
}
