/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.search.view.Search12View;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author jason_h
 */
@Service
@Slf4j
public class Search12QueryService implements QueryService<Search12View>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3749923369230915200L;
    @Autowired
    private URLService urlService;
    @Autowired
    private WkJsonUtils jsonUtils;
    @Autowired
    private SearchService searchHelper;

    @PersistenceContext
    transient private EntityManager em;

    @Override
    public List<Search12View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {
        // ====================================
        // 查詢
        // ====================================

        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        @SuppressWarnings("unchecked")
        List<Object[]> result = query.getResultList();
        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if (WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }
        
        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();
        List<Search12View> viewResult = Lists.newArrayList();
        for (int i = 0; i < result.size(); i++) {

            Search12View v = new Search12View();

            Object[] record = (Object[]) result.get(i);
            int index = 0;
            String sid = (String) record[index++];
            String requireNo = (String) record[index++];
            String fieldName = (String) record[index++];
            String fieldContent = (String) record[index++];
            if (this.checkDoubleView(viewResult, sid)) {
                setThemeOrCash(viewResult, sid, fieldName, fieldContent);
                continue;
            }
            Integer urgency = (Integer) record[index++];
            Date createdDate = (Date) record[index++];
            String bigName = (String) record[index++];
            String middleName = (String) record[index++];
            String smallName = (String) record[index++];
            Integer createDep = (Integer) record[index++];
            Integer createdUser = (Integer) record[index++];
            Integer customer = (Integer) record[index++];
            Integer author = (Integer) record[index++];
            // 處理共通欄位 (一定要擺在『取值』最後面, 否則 index 會不對)
            v.prepareCommonColumn(record, index);

            v.setSid(sid);
            v.setRequireNo(requireNo);
            this.setThemeOrCash(v, fieldName, fieldContent);
            v.setUrgency(UrgencyType.values()[urgency]);
            v.setCreatedDate(createdDate);
            v.setBigName(bigName);
            v.setMiddleName(middleName);
            v.setSmallName(smallName);
            v.setCreateDep(createDep);
            v.setCreatedUser(createdUser);
            if (customer != null) {
                v.setCustomer(customer.longValue());
            }
            if (author != null) {
                v.setAuthor(author.longValue());
            }

            v.setLocalUrlLink(urlService.createLoacalURLLink(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(execUserSid, v.getRequireNo(), 1)));
            viewResult.add(v);
        }
        // 解析資料-結束
        usageRecord.parserDataEnd();
        return viewResult;
    }

    private boolean checkDoubleView(List<Search12View> viewResult, String viewSid) {
        Search12View view = new Search12View();
        view.setSid(viewSid);
        return viewResult.contains(view);
    }

    private void setThemeOrCash(List<Search12View> viewResult, String viewSid, String fieldName, String fieldContent) {
        Search12View tempView = new Search12View();
        tempView.setSid(viewSid);
        int index = viewResult.indexOf(tempView);
        if (index == -1) {
            return;
        }
        Search12View view = viewResult.get(index);
        this.setThemeOrCash(view, fieldName, fieldContent);
    }

    private void setThemeOrCash(Search12View view, String fieldName, String fieldContent) {
        if ("主題".equals(fieldName)) {
            view.setRequireTheme(searchHelper.combineFromJsonStr(fieldContent));
        }
        if ("是否收費".equals(fieldName)) {
            try {
                // amountInfo.get(0) 是布林值 ； amountInfo.get(1) 才是金額
                List<String> cashInfo = jsonUtils.fromJsonToList(fieldContent, String.class);
                String castStr = Strings.isNullOrEmpty(cashInfo.get(1)) ? "0" : cashInfo.get(1);
                view.setCash(new BigDecimal(castStr));
            } catch (NumberFormatException ex) {
                log.error("單號： " + view.getRequireNo() + "轉換金額失敗！  " + ex.getMessage(), ex);
            } catch (IOException ex) {
                log.error("單號： " + view.getRequireNo() + "轉換JSON格式失敗！  " + ex.getMessage(), ex);
            }
        }
    }

}
