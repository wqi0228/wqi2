/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.anew.vo.enums;

/**
 * 需求單位 簽核層級（對應BPM自訂層級）
 *
 * @author kasim
 */
public enum ReqFlowType {

    /** 組 */
    GROUP("SIGN_LV_41"),
    /** 部 */
    DEPARTMENT("SIGN_LV_31"),
    /** 處 */
    OFFICE("SIGN_LV_21"),
    /** 群 */
    BUSSINESSGROUP("SIGN_LV_11"),
    /** 執行長 */
    CEO("SIGN_LV_01");

    private final String val;

    ReqFlowType(String value) {
        this.val = value;
    }

    public String getVal() {
        return val;
    }
}
