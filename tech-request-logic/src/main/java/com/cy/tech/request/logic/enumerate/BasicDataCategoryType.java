package com.cy.tech.request.logic.enumerate;

import com.cy.tech.request.vo.category.BasicDataBigCategory;
import com.cy.tech.request.vo.category.BasicDataMiddleCategory;
import com.cy.tech.request.vo.category.BasicDataSmallCategory;
import lombok.Getter;

/**
 *
 * @author shaun
 */
public enum BasicDataCategoryType {

    /** 類型 */
    BIG(BasicDataBigCategory.class),
    /** 中類 */
    MIDDLE(BasicDataMiddleCategory.class),
    /** 小類 */
    SMALL(BasicDataSmallCategory.class);

    @Getter
    private final Class<?> clz;

    BasicDataCategoryType(Class<?> clz) {
        this.clz = clz;
    }
}
