/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.onpg;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.RequireTraceService;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.repository.onpg.WorkOnpgAttachmentRepo;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.onpg.WorkOnpgAttachment;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgAttachmentBehavior;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkAttachUtils;
import com.cy.work.common.vo.AttachmentService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * WorkOnpg 附加檔案服務
 *
 * @author shaun
 */
@Component("onpg_attach")
public class OnpgAttachService implements AttachmentService<WorkOnpgAttachment, WorkOnpg, String>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8052522917920449968L;
    @Value("${erp.attachment.root}")
    private String attachPath;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private UserService userService;
    @Autowired
    private RequireTraceService traceService;
    @Autowired
    private WorkOnpgAttachmentRepo attachDao;
    @Autowired
    private WkAttachUtils attachUtils;

    @Transactional(readOnly = true)
    @Override
    public WorkOnpgAttachment findBySid(String sid) {
        WorkOnpgAttachment a = attachDao.findOne(sid);
        return a;
    }

    @Transactional(readOnly = true)
    @Override
    public List<WorkOnpgAttachment> findAttachsByLazy(WorkOnpg onpg) {
        try {
            onpg.getAttachments().size();
        } catch (LazyInitializationException e) {
            //log.debug("findAttachsByLazy lazy init error :" + e.getMessage(), e);
            onpg.setAttachments(this.findAttachs(onpg));
        }
        return onpg.getAttachments();
    }

    @Transactional(readOnly = true)
    @Override
    public List<WorkOnpgAttachment> findAttachs(WorkOnpg e) {
        if (Strings.isNullOrEmpty(e.getSid())) {
            if (e.getAttachments() == null) {
                e.setAttachments(Lists.newArrayList());
            }
            return e.getAttachments();
        }
        return attachDao.findByOnpgAndStatusAndHistoryIsNullOrderByCreatedDateDesc(e, Activation.ACTIVE);
    }

    @Override
    public String getAttachPath() {
        return attachPath;
    }

    @Override
    public String getDirectoryName() {
        return "tech-request";
    }

    @Override
    public WorkOnpgAttachment createEmptyAttachment(String fileName, User executor, Org dep) {
        return (WorkOnpgAttachment) attachUtils.createEmptyAttachment(new WorkOnpgAttachment(), fileName, executor, dep);
    }

    @Transactional
    @Override
    public WorkOnpgAttachment updateAttach(WorkOnpgAttachment attach) {
        WorkOnpgAttachment a = attachDao.save(attach);
        return a;
    }

    @Transactional
    @Override
    public void saveAttach(WorkOnpgAttachment attach) {
        attachDao.save(attach);
    }

    @Transactional
    @Override
    public void saveAttach(List<WorkOnpgAttachment> attachs) {
        attachDao.save(attachs);
    }

    @Override
    public String depAndUserName(WorkOnpgAttachment attachment) {
        if (attachment == null) {
            return "";
        }
        User usr = WkUserCache.getInstance().findBySid(attachment.getCreatedUser().getSid());
        String parentDepStr = orgService.showParentDep(orgService.findBySid(usr.getPrimaryOrg().getSid()));
        return parentDepStr + "-" + usr.getName();
    }

    @Transactional
    @Override
    public void linkRelation(List<WorkOnpgAttachment> attachments, WorkOnpg onpg, User login) {
        attachments.forEach(each -> this.setRelation(each, onpg));
        this.saveAttach(attachments);
        onpg.setAttachments(this.findAttachs(onpg));
    }

    @Transactional
    @Override
    public void linkRelation(WorkOnpgAttachment ra, WorkOnpg onpg, User login) {
        this.setRelation(ra, onpg);
        this.updateAttach(ra);
    }

    private void setRelation(WorkOnpgAttachment ra, WorkOnpg onpg) {
        ra.setOnpg(onpg);
        ra.setOnpgNo(onpg.getOnpgNo());
        ra.setSourceType(onpg.getSourceType());
        ra.setSourceSid(onpg.getSourceSid());
        ra.setSourceNo(onpg.getSourceNo());
        ra.setBehavior(WorkOnpgAttachmentBehavior.FIRST_FILE);
        if (!ra.getKeyChecked()) {
            ra.setStatus(Activation.INACTIVE);
        }
        ra.setKeyChecked(Boolean.FALSE);
    }

    /**
     * 刪除
     *
     * @param attachments
     * @param attachment
     * @param onpg
     * @param login
     */
    @Transactional
    @Override
    public void changeStatusToInActive(List<WorkOnpgAttachment> attachments, WorkOnpgAttachment attachment, WorkOnpg onpg, User login) {
        WorkOnpgAttachment ra = (WorkOnpgAttachment) attachment;
        ra.setStatus(Activation.INACTIVE);
        this.updateAttach(attachment);
        onpg.setAttachments(this.findAttachs(onpg));
        this.createDeleteTrace(attachment, onpg, login);
    }

    private void createDeleteTrace(WorkOnpgAttachment attachment, WorkOnpg onpg, User login) {
        Require require = new Require();
        require.setSid(onpg.getSourceSid());
        require.setRequireNo(onpg.getSourceNo());
        String content = ""
                + "檔案名稱：【" + attachment.getFileName() + "】\n"
                + "上傳成員：【" + userService.getUserName(attachment.getCreatedUser()) + "】\n"
                + "\n"
                + "刪除來源：【ON程式 － 單號：" + onpg.getOnpgNo() + "】\n"
                + "刪除成員：【" + login.getName() + "】\n"
                + "刪除註記：" + attachment.getSid();
        traceService.createRequireTrace(require.getSid(), login, RequireTraceType.DELETE_ATTACHMENT, content);
    }

}
