/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.search.enums.ReqSubType;
import com.cy.tech.request.logic.search.view.Search26BaseView;
import com.cy.tech.request.logic.search.view.Search26View;
import com.cy.tech.request.logic.service.CommonService;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.vo.converter.SetupInfoToConverter;
import com.cy.tech.request.vo.enums.OthSetStatus;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.cy.tech.request.vo.value.to.SetupInfoTo;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author jason_h
 */
@Service
@Slf4j
public class Search26QueryService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5962485839290497007L;
    @Autowired
    private URLService urlService;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private UserService userService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private SearchService searchService;
    @Autowired
    private WkJsonUtils jsonUtils;

    private final SetupInfoToConverter infoConverter = new SetupInfoToConverter();

    @PersistenceContext
    transient private EntityManager em;

    public List<Search26BaseView> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {

        // ====================================
        // 查詢
        // ====================================
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        rawParameters.stream().forEach(entry -> query.setParameter(entry.getKey(), entry.getValue()));

        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        @SuppressWarnings("unchecked")
        List<Object[]> result = query.getResultList();
        //資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if(WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();
        List<Search26BaseView> viewResult = Lists.newArrayList();

        for (int i = 0; i < result.size(); i++) {
            int idx = 0;
            Object[] record = result.get(i);
            // 需求單資訊
            String reqSid = (String) record[idx++];
            String reqNo = (String) record[idx++];
            String reqTheme = (String) record[idx++];
            Integer urgency = (Integer) record[idx++];
            Date reqCreDt = (Date) record[idx++];
            Date requireEstablishDate = (Date) record[idx++];
            String reqStatus = (String) record[idx++];
            Date reqHopeDt = (Date) record[idx++];
            Date requireFinishDate = (Date) record[idx++];
            Date reqCloseDt = (Date) record[idx++];
            Integer reqCreateDep = (Integer) record[idx++];
            Integer reqCreateUser = (Integer) record[idx++];
            Integer inteStaffUser = (Integer) record[idx++];

            String assignDepsStr = (String) record[idx++];
            // 子程序資訊
            String subType = (String) record[idx++];
            String subSid = (String) record[idx++];
            String subTheme = (String) record[idx++];
            String subNoticeStr = (String) record[idx++];
            Date subCreDt = (Date) record[idx++];
            String subStatus = (String) record[idx++];
            Date subEsDt = (Date) record[idx++];

            Search26View v = new Search26View();
            // 需求單資訊
            v.setReqSid(reqSid);
            v.setReqNo(reqNo);
            v.setRequireTheme(searchService.combineFromJsonStr(reqTheme));
            v.setUrgency(UrgencyType.values()[urgency]);
            v.setSubType(ReqSubType.valueOf(subType));
            if (v.getSubType().equals(ReqSubType.ROOT)) {
                v.setReqCreDt(reqCreDt);
                RequireStatusType reqStatusType = RequireStatusType.valueOf(reqStatus);
                v.setReqStatus(commonService.get(reqStatusType));
                v.setReqHopeDt(reqHopeDt);

                v.setReqFinishDt(requireFinishDate);
                v.setReqCloseDt(reqCloseDt);
                v.setReqCreateDep(orgService.getOrgName(reqCreateDep));
                v.setReqCreatedUser(userService.getUserName(reqCreateUser));
                v.setInteStaffUser(this.buildInteStaffShowStr(inteStaffUser));

                v.setAssignDepsStr(this.buildAssignStr(assignDepsStr));

                // REQ-1031
                v.setReqExecDays(
                        this.searchService.calcExecDays(
                                reqStatusType,
                                requireEstablishDate,
                                requireFinishDate));

            } else {
                // 子程序資訊
                v.setSubSid(subSid);
                v.setSubTheme(subTheme);
                v.setSubNoticeStr(this.createSubNoticeStr(v.getSubType(), subNoticeStr));
                v.setSubCreDt(subCreDt);
                v.setSubStatus(this.createSubStatusStr(v.getSubType(), subStatus));
                v.setSubEsDt(subEsDt);
            }
            v.setLocalUrlLink(this.createLocalUrlLink(execUserSid, v.getSubType(), reqSid, reqNo, subSid));
            viewResult.add(v);
        }
        // 解析資料-結束
        usageRecord.parserDataEnd();
        return viewResult;
    }

    private String buildInteStaffShowStr(Integer inte) {
        if (inte == null) {
            return "";
        }
        User inteUsr = WkUserCache.getInstance().findBySid(inte);
        return orgService.getOrgName(inteUsr.getPrimaryOrg()) + " - " + inteUsr.getName();
    }

    /**
     * 組合分派部門字串
     *
     * @param assignInfo
     * @return
     */
    private String buildAssignStr(String assignInfo) {
        if (Strings.isNullOrEmpty(assignInfo)) {
            return "";
        }
        SetupInfoTo infoObj = infoConverter.convertToEntityAttribute(assignInfo);
        return "分派單位：" + infoObj.getDepartment().stream()
                .map(depSid -> WkOrgCache.getInstance().findNameBySid(depSid))
                .collect(Collectors.joining("、"));
    }

    /**
     * 子程序通知
     *
     * @param subType
     * @param noticeStr
     * @return
     */
    private String createSubNoticeStr(ReqSubType subType, String noticeStr) {
        if (Strings.isNullOrEmpty(noticeStr)) {
            return "";
        }
        try {
            JsonStringListTo jsonTo = jsonUtils.fromJson(noticeStr, JsonStringListTo.class);
            if (subType.equals(ReqSubType.PT)) {
                return "通知人員：" + jsonTo.getValue().stream()
                        .map(each -> userService.getUserName(each))
                        .sorted()
                        .collect(Collectors.joining("、"));
            } else {
                return "通知單位：" + jsonTo.getValue().stream()
                        .map(each -> orgService.getOrgName(each))
                        .sorted()
                        .collect(Collectors.joining("、"));
            }
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
            return "";
        }
    }

    /**
     * 子程序狀態
     *
     * @param subType
     * @param subStatus
     * @return
     */
    private String createSubStatusStr(ReqSubType subType, String subStatus) {
        switch (subType) {
        case PT:
            return commonService.get(PtStatus.valueOf(subStatus));
        case WT:
            return commonService.get(WorkTestStatus.valueOf(subStatus));
        case OP:
            return commonService.get(WorkOnpgStatus.valueOf(subStatus));
        case OS:
            return commonService.get(OthSetStatus.valueOf(subStatus));
        default:
            return "";
        }
    }

    private String createLocalUrlLink(Integer execUserSid, ReqSubType subType, String reqSid, String reqNo, String subSid) {
        URLService.URLServiceAttr subAttr = null;
        switch (subType) {
        case ROOT:
            return urlService.createLocalUrlLinkParamForTab(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(execUserSid, reqNo, 1),
                    URLService.URLServiceAttr.URL_ATTR_TAB_AS, reqSid);
        case PT:
            subAttr = URLService.URLServiceAttr.URL_ATTR_TAB_PT;
            break;
        case WT:
            subAttr = URLService.URLServiceAttr.URL_ATTR_TAB_ST;
            break;
        case OP:
            subAttr = URLService.URLServiceAttr.URL_ATTR_TAB_OP;
            break;
        case OS:
            subAttr = URLService.URLServiceAttr.URL_ATTR_TAB_OS;
            break;
        }
        return urlService.createLocalUrlLinkParamForTab(
                URLService.URLServiceAttr.URL_ATTR_M,
                urlService.createSimpleUrlTo(execUserSid, reqNo, 1),
                subAttr, subSid);
    }

}
