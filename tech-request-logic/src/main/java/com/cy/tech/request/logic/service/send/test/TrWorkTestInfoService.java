/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.send.test;

import com.cy.tech.request.repository.worktest.TrWorkTestInfoRepo;
import com.cy.tech.request.vo.worktest.TrWorkTestInfo;
import com.cy.work.common.vo.value.to.JsonStringListTo;

import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 送測Service
 *
 * @author brain0925_liao
 */
@Component
public class TrWorkTestInfoService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8299249799681841000L;
    /** TrWorkTestInfoRepo */
    @Autowired
    private TrWorkTestInfoRepo trWorkTestInfoRepo;

    /**
     * 取得送測物件 By testinfoNo
     *
     * @param testinfoNo
     * @return
     */
    public TrWorkTestInfo findByTestinfoNo(String testinfoNo) {
        return trWorkTestInfoRepo.findByTestinfoNo(testinfoNo);
    }

    /**
     * 更新送測建立部門 By testinfoNo
     *
     * @param depSid 建立部門Sid
     * @param testinfoNo
     */
    public void updateDepsidByTestinfoNo(Integer depSid, String testinfoNo) {
        trWorkTestInfoRepo.updateDepsid(testinfoNo, depSid);
    }

    /**
     * 更新送測通知部門 By sid
     *
     * @param sid 送測Sid
     * @param sendTestDep 通知部門物件
     * @return
     */
    public int updateSendTestDep(String sid, JsonStringListTo sendTestDep) {
        return trWorkTestInfoRepo.updateSendTestDep(sid, sendTestDep);
    }
}
