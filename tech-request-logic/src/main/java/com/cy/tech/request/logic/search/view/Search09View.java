/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.enums.UrgencyType;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 原型確認簽核進度查詢頁面顯示vo (search09.xhtml)
 *
 * @author jason_h
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Search09View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -9102095378854673113L;
    /** 緊急度 */
    private UrgencyType urgency;
    /** 原型確認-立單日期 */
    private Date prototypeCreatedDate;
    /** 原型確認-填單單位 */
    private Integer prototypeCreateDep;
    /** 原型確認-填單人員 */
    private Integer prototypeCreatedUser;
    /** 核准日 */
    private Date approvalDate;
    /** 預計原型確認完成日 */
    private Date finishDate;
    /** 需求類別 */
    private String bigName;
    /** 中類 */
    private String middleName;
    /** 小類" */
    private String smallName;
    /** 需求單-需求單位 */
    private Integer requireCreateDep;
    /** 需求單-需求人員 */
    private Integer requireCreatedUser;
    private String defaultSignedName;
    /** 原型確認審核狀態 */
    private InstanceStatus instanceStatus;
    /** 原型確認版號 */
    private Integer version;
    /** 原型確認進度 */
    private PtStatus prototypeStatus;
    /** 原型確認-主題 */
    private String ptTheme;
    /** 本地端連結網址 */
    private String localUrlLink;
}
