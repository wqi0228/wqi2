/**
 * 
 */
package com.cy.tech.request.logic.service;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.logic.vo.ProceeAddAssignDepForInternalTo;
import com.cy.tech.request.repository.require.AssignSendInfoNativeToRepository;
import com.cy.work.common.constant.WkConstants;
import com.cy.tech.request.vo.converter.AssignSendGroupsToConverter;
import com.cy.tech.request.vo.converter.SetupInfoToConverter;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.TrnsType;
import com.cy.tech.request.vo.require.AssignSendInfoHistory;
import com.cy.tech.request.vo.require.AssignSendInfoNativeTo;
import com.cy.tech.request.vo.require.AssignSendSearchInfoVO;
import com.cy.tech.request.vo.require.RequireConfirmDep;
import com.cy.tech.request.vo.require.RequireConfirmDepHistory;
import com.cy.tech.request.vo.require.TrnsBackup;
import com.cy.tech.request.vo.value.to.AssignSendGroupsTo;
import com.cy.tech.request.vo.value.to.SetupInfoTo;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 *
 */
@Slf4j
@Service
public class Set10V70TrneBatchHelper implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 3209619896321506008L;
    @Autowired
	@Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
	private transient JdbcTemplate jdbcTemplate;
	@Autowired
	private transient EntityManager entityManager;
	@Autowired
	private transient AssignSendInfoNativeToRepository assignSendInfoNativeToRepository;
	@Autowired
	private transient NativeSqlRepository nativeSqlRepository;
	/**
	 * 每次批次執行筆數
	 */
	private final Integer BATCH_UPDATE_SIZE = 200;
	private final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:sss");

	/**
	 * @param userSid
	 * @param sysdate
	 * @param assignSendInfoSids
	 */
	@Transactional(rollbackFor = Exception.class)
	public void backupAssignSendInfo(
	        Integer userSid,
	        Date sysdate,
	        List<String> assignSendInfoSids) {

		if (WkStringUtils.isEmpty(assignSendInfoSids)) {
			return;
		}
		// ====================================
		// 查詢
		// ====================================
		Long startTime = System.currentTimeMillis();
		List<AssignSendInfoNativeTo> assignSendInfos = this.assignSendInfoNativeToRepository.findBySidIn(assignSendInfoSids);
		log.debug(WkCommonUtils.prepareCostMessage(startTime, "備份前查詢要被刪除的 tr_assign_send_info [" + assignSendInfoSids.size() + "]筆"));

		// ====================================
		// 以 require_no 分組
		// ====================================
		Map<String, List<AssignSendInfoNativeTo>> assignSendInfosMapByRequireNo = assignSendInfos.stream()
		        .collect(Collectors.groupingBy(
		                AssignSendInfoNativeTo::getRequireNo,
		                Collectors.mapping(
		                        each -> each,
		                        Collectors.toList())));

		// ====================================
		// 紀錄備份檔, 備份將要刪除的 tr_assign_send_info
		// ====================================
		List<TrnsBackup> inserTrnsBackups = Lists.newArrayList();
		// 相同的單號整理在同一筆備份資料
		for (Entry<String, List<AssignSendInfoNativeTo>> assignSendInfosSet : assignSendInfosMapByRequireNo.entrySet()) {
			TrnsBackup trnsBackup = new TrnsBackup();
			trnsBackup.setCreatedUser(userSid);
			trnsBackup.setCreatedDate(sysdate);
			trnsBackup.setTrnsType(TrnsType.ASSIGN_SEND_INFO_TO_HISTORY.name());
			// 以單號為 key
			trnsBackup.setCustKey(assignSendInfosSet.getKey());
			// 相同單號的寫在同一筆
			trnsBackup.setBackData(WkJsonUtils.getInstance().toJsonWithOutPettyJson(assignSendInfosSet.getValue()));
			inserTrnsBackups.add(trnsBackup);
		}

		// ====================================
		// 寫入
		// ====================================
		this.batchInserTrnsBackups(inserTrnsBackups);
	}

	/**
	 * 批次刪除
	 * 
	 * @param tableName
	 * @param columnName
	 * @param columnValues
	 */
	public void batchDeleteByColumn(
	        String tableName,
	        String columnName,
	        Set<String> columnValues) {

		Long startTime = System.currentTimeMillis();
		String procTitle = "DELETE FROM " + tableName + " By " + columnName;
		// log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

		if (WkStringUtils.isEmpty(columnValues)) {
			log.warn(procTitle);
			log.warn("傳入的 where 條件為空，不執行");
			return;
		}

		String deleteSQL = ""
		        + "DELETE FROM " + tableName + " "
		        + " WHERE " + columnName + " IN ('"
		        + String.join("', '", columnValues)
		        + "');";

		int rows = this.jdbcTemplate.update(deleteSQL);
		log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle + " 共刪除[" + rows + "]筆"));
	}

	/**
	 * 批次更新
	 * 
	 * @param tableName
	 * @param setColumnName
	 * @param setColumnValue
	 * @param whereColumnName
	 * @param whereColumnValues
	 */
	public void batchUpdateByColumn(
	        String tableName,
	        String setColumnName,
	        String setColumnValue,
	        String whereColumnName,
	        Set<String> whereColumnValues) {

		Long startTime = System.currentTimeMillis();
		String procTitle = "UPDATE  " + tableName + " By " + whereColumnName;
		// log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

		if (WkStringUtils.isEmpty(whereColumnValues)) {
			log.warn("【" + procTitle + "】: 傳入的 whereColumnValues 為空，不執行");
			return;
		}

		String updateSQL = ""
		        + "UPDATE " + tableName + " "
		        + "SET " + setColumnName + " = '" + setColumnValue + "' "
		        + "WHERE " + whereColumnName + " IN ('"
		        + String.join("', '", whereColumnValues)
		        + "');";

		int rows = this.jdbcTemplate.update(updateSQL);
		log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle + " 共更新[" + rows + "]筆"));
	}

	/**
	 * 批次新增 tr_assign_send_info_history
	 * 
	 * @param insertHistorys
	 */
	@Transactional(rollbackFor = Exception.class)
	public void batchInsertAssignSendInfo(List<AssignSendInfoNativeTo> assignSendInfoNativeTos) {

		if (WkStringUtils.isEmpty(assignSendInfoNativeTos)) {
			log.warn("傳入的 assignSendInfos 為空，不執行");
			return;
		}
		Long startTime = System.currentTimeMillis();
		String procTitle = "INSERT tr_assign_send_info [" + assignSendInfoNativeTos.size() + "]筆";
		log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

		StringBuffer insertSQL = new StringBuffer();
		insertSQL.append("INSERT ");
		insertSQL.append("INTO ");
		insertSQL.append("    tr_assign_send_info ");
		insertSQL.append("    ( ");
		insertSQL.append("        info_sid, ");
		insertSQL.append("        require_sid, ");
		insertSQL.append("        require_no, ");
		insertSQL.append("        type, ");
		insertSQL.append("        create_usr, ");
		insertSQL.append("        create_dt, ");
		insertSQL.append("        status, ");
		insertSQL.append("        info, ");
		insertSQL.append("        group_info ");
		insertSQL.append("    ) ");
		insertSQL.append("    VALUES ");
		insertSQL.append("    ( ");
		insertSQL.append("        ?, "); // info_sid
		insertSQL.append("        ?, "); // require_sid
		insertSQL.append("        ?, "); // require_no
		insertSQL.append("        ?, "); // type
		insertSQL.append("        ?, "); // create_usr
		insertSQL.append("        ?, "); // create_dt
		insertSQL.append("        ?, "); // status
		insertSQL.append("        ?, "); // info
		insertSQL.append("        ? "); // group_info
		insertSQL.append("    );");

		int totalSize = assignSendInfoNativeTos.size();
		for (int i = 0; i < totalSize; i += BATCH_UPDATE_SIZE) {
			int index = (i + BATCH_UPDATE_SIZE) > totalSize ? totalSize : i + BATCH_UPDATE_SIZE;
			// 取得本次批次執行筆數
			final List<AssignSendInfoNativeTo> batchList = assignSendInfoNativeTos.subList(i, index);

			this.jdbcTemplate.batchUpdate(insertSQL.toString(), new BatchPreparedStatementSetter() {

				@Override
				public int getBatchSize() {
					return batchList.size();
				}

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {

					AssignSendInfoNativeTo item = batchList.get(i);
					int index = 1;
					// info_sid
					ps.setString(index++, item.getSid());
					// require_sid
					ps.setString(index++, item.getRequireSid());
					// require_no
					ps.setString(index++, item.getRequireNo());
					// type
					ps.setInt(index++, item.getType().ordinal());
					// create_usr
					ps.setInt(index++, item.getCreatedUser());
					// create_dt
					ps.setString(index++, df.format(item.getCreatedDate()));
					// status
					ps.setInt(index++, item.getStatus().ordinal());
					// info
					ps.setString(index++, item.getInfo());
					// group_info
					ps.setString(index++, item.getGroupInfo());
				}
			});
		}
		log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
	}

	/**
	 * 批次新增 tr_assign_send_info_history
	 * 
	 * @param insertHistorys
	 */
	@Transactional(rollbackFor = Exception.class)
	public void batchInsertAssignSendInfoHistorys(List<AssignSendInfoHistory> insertHistorys) {

		Long startTime = System.currentTimeMillis();
		String procTitle = "INSERT tr_assign_send_info_history [" + insertHistorys.size() + "]筆";
		// log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

		if (WkStringUtils.isEmpty(insertHistorys)) {
			log.warn(procTitle);
			log.warn("傳入的資料為空，不執行");
			return;
		}

		StringBuffer insertSQL = new StringBuffer();
		insertSQL.append("INSERT ");
		insertSQL.append("INTO ");
		insertSQL.append("    tr_assign_send_info_history ");
		insertSQL.append("    ( ");
		insertSQL.append("        status, ");
		insertSQL.append("        create_usr, ");
		insertSQL.append("        create_dt, ");
		insertSQL.append("        require_sid, ");
		insertSQL.append("        add_assign_dep, ");
		insertSQL.append("        delete_assign_dep, ");
		insertSQL.append("        add_send_dep, ");
		insertSQL.append("        delete_send_dep ");
		insertSQL.append("    ) ");
		insertSQL.append("    VALUES ");
		insertSQL.append("    ( ");
		insertSQL.append("        0, "); // status
		insertSQL.append("        ?, "); // create_usr
		insertSQL.append("        ?, "); // create_dt
		insertSQL.append("        ?, "); // require_sid
		insertSQL.append("        ?, "); // add_assign_dep
		insertSQL.append("        ?, "); // delete_assign_dep
		insertSQL.append("        ?, "); // add_send_dep
		insertSQL.append("        ? "); // delete_send_dep
		insertSQL.append("    );");

		int totalSize = insertHistorys.size();
		for (int i = 0; i < totalSize; i += BATCH_UPDATE_SIZE) {
			int index = (i + BATCH_UPDATE_SIZE) > totalSize ? totalSize : i + BATCH_UPDATE_SIZE;
			// 取得本次批次執行筆數
			final List<AssignSendInfoHistory> batchList = insertHistorys.subList(i, index);

			this.jdbcTemplate.batchUpdate(insertSQL.toString(), new BatchPreparedStatementSetter() {

				@Override
				public int getBatchSize() {
					return batchList.size();
				}

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {

					AssignSendInfoHistory item = batchList.get(i);
					int index = 1;
					// create_usr
					ps.setInt(index++, item.getCreatedUser());
					// create_dt
					ps.setString(index++, df.format(item.getCreatedDate()));
					// require_sid
					ps.setString(index++, item.getRequireSid());
					// add_assign_dep
					ps.setString(index++, item.getAddAssignDeps());
					// delete_assign_dep
					ps.setString(index++, item.getDeleteAssignDeps());
					// add_send_dep
					ps.setString(index++, item.getAddSendDeps());
					// delete_send_dep
					ps.setString(index++, item.getDeleteSendDeps());
				}
			});
		}
		log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
	}

	/**
	 * 批次新增 tr_assign_send_search_info
	 * 
	 * @param insertAssignSendSearchInfoVOs
	 */
	@Transactional(rollbackFor = Exception.class)
	public void batchInsertAssignSendSearchInfos(
	        List<AssignSendSearchInfoVO> insertAssignSendSearchInfoVOs) {

		//Long startTime = System.currentTimeMillis();
		String procTitle = "INSERT tr_assign_send_search_info [" + insertAssignSendSearchInfoVOs.size() + "]筆";
		// log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

		if (WkStringUtils.isEmpty(insertAssignSendSearchInfoVOs)) {
			log.warn(procTitle);
			log.warn("傳入的資料為空，不執行");
			return;
		}

		StringBuffer insertSQL = new StringBuffer();
		insertSQL.append("INSERT ");
		insertSQL.append("INTO ");
		insertSQL.append("    tr_assign_send_search_info ");
		insertSQL.append("    ( ");
		insertSQL.append("        create_usr, ");
		insertSQL.append("        create_dt, ");
		insertSQL.append("        search_sid, ");
		insertSQL.append("        require_sid, ");
		insertSQL.append("        require_no, ");
		insertSQL.append("        type, ");
		insertSQL.append("        dep_sid ");
		insertSQL.append("    ) ");
		insertSQL.append("    VALUES ");
		insertSQL.append("    ( ");
		insertSQL.append("        ?, "); // create_usr
		insertSQL.append("        ?, "); // create_dt
		insertSQL.append("        ?, "); // search_sid
		insertSQL.append("        ?, "); // require_sid
		insertSQL.append("        ?, "); // require_no
		insertSQL.append("        ?, "); // type
		insertSQL.append("        ? "); // dep_sid
		insertSQL.append("    );");

		int totalSize = insertAssignSendSearchInfoVOs.size();
		for (int i = 0; i < totalSize; i += BATCH_UPDATE_SIZE) {
			int index = (i + BATCH_UPDATE_SIZE) > totalSize ? totalSize : i + BATCH_UPDATE_SIZE;
			// 取得本次批次執行筆數
			final List<AssignSendSearchInfoVO> batchList = insertAssignSendSearchInfoVOs.subList(i, index);

			this.jdbcTemplate.batchUpdate(insertSQL.toString(), new BatchPreparedStatementSetter() {

				@Override
				public int getBatchSize() {
					return batchList.size();
				}

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {

					AssignSendSearchInfoVO item = batchList.get(i);
					int index = 1;
					// create_usr
					ps.setInt(index++, item.getCreatedUserSid());
					// create_dt
					ps.setString(index++, df.format(item.getCreatedDate()));
					// search_sid
					ps.setString(index++, UUID.randomUUID().toString());
					// require_sid
					ps.setString(index++, item.getRequireSid());
					// require_no
					ps.setString(index++, item.getRequireNo());
					// type
					ps.setInt(index++, item.getType().ordinal());
					// dep_sid
					ps.setInt(index++, item.getDepSid());
				}
			});
		}
		//log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
	}

	/**
	 * 批次新增 tr_require_confirm_dep_history
	 * 
	 * @param insertRequireConfirmDepHistorys
	 */
	@Transactional(rollbackFor = Exception.class)
	public void batchInsertRequireConfirmDepHistory(
	        List<RequireConfirmDepHistory> insertRequireConfirmDepHistorys,
	        boolean isPrepareConfirmSid) {

		String procTitle = "INSERT [tr_require_confirm_dep_history] [" + insertRequireConfirmDepHistorys.size() + "]筆";

		if (WkStringUtils.isEmpty(insertRequireConfirmDepHistorys)) {
			log.warn(procTitle);
			log.warn("傳入的資料為空，不執行");
			return;
		}

		// ====================================
		// 準備缺的 require_confirm_sid
		// ====================================
		// 因批次執行 insert tr_require_confirm_dep , 故資料準備階段無法得知 require_confirm_sid
		// 在此由資料庫中取回
		if (isPrepareConfirmSid) {
			this.prepareRequireConfirmDepHistory(insertRequireConfirmDepHistorys);
		}

		// ====================================
		// 執行
		// ====================================
		Long startTime = System.currentTimeMillis();
		// log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

		StringBuffer insertSQL = new StringBuffer();
		insertSQL.append("INSERT ");
		insertSQL.append("INTO ");
		insertSQL.append("    tr_require_confirm_dep_history ");
		insertSQL.append("    ( ");
		insertSQL.append("        status, ");
		insertSQL.append("        create_usr, ");
		insertSQL.append("        create_dt, ");
		insertSQL.append("        create_dep, ");
		insertSQL.append("        require_confirm_sid, ");
		insertSQL.append("        require_sid, ");
		insertSQL.append("        dep_sid, ");
		insertSQL.append("        proc_type, ");
		insertSQL.append("        memo ");
		insertSQL.append("    ) ");
		insertSQL.append("    VALUES ");
		insertSQL.append("    ( ");
		insertSQL.append("        0, "); // status
		insertSQL.append("        ?, "); // create_usr
		insertSQL.append("        ?, "); // create_dt
		insertSQL.append("        ?, "); // create_dep
		insertSQL.append("        ?, "); // require_confirm_sid
		insertSQL.append("        ?, "); // require_sid
		insertSQL.append("        ?, "); // dep_sid
		insertSQL.append("        ?, "); // proc_type
		insertSQL.append("        ? "); // memo
		insertSQL.append("    );");

		int totalSize = insertRequireConfirmDepHistorys.size();
		for (int i = 0; i < totalSize; i += BATCH_UPDATE_SIZE) {
			int index = (i + BATCH_UPDATE_SIZE) > totalSize ? totalSize : i + BATCH_UPDATE_SIZE;
			// 取得本次批次執行筆數
			final List<RequireConfirmDepHistory> batchList = insertRequireConfirmDepHistorys.subList(i, index);

			this.jdbcTemplate.batchUpdate(insertSQL.toString(), new BatchPreparedStatementSetter() {

				@Override
				public int getBatchSize() {
					return batchList.size();
				}

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					RequireConfirmDepHistory item = batchList.get(i);
					int index = 1;
					// create_usr
					ps.setInt(index++, item.getCreatedUser());
					// create_dt
					ps.setString(index++, df.format(item.getCreatedDate()));
					// create_dep
					ps.setInt(index++, item.getCreateDep());
					// require_confirm_sid
					ps.setLong(index++, item.getRequireConfirmSid());
					// require_sid
					ps.setString(index++, item.getRequireSid());
					// dep_sid
					ps.setLong(index++, item.getDepSid());
					// proc_type
					ps.setString(index++, item.getProgType().name());
					// memo
					ps.setString(index++, item.getMemo());
				}
			});
		}
		log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
	}

	/**
	 * 批次新增 tr_require_confirm_dep
	 * 
	 * @param insertRequireConfirmDeps
	 * @throws UserMessageException
	 */
	@Transactional(rollbackFor = Exception.class)
	public void batchInsertRequireConfirmDeps(List<RequireConfirmDep> insertRequireConfirmDeps) throws UserMessageException {

		Long startTime = System.currentTimeMillis();
		String procTitle = "INSERT tr_require_confirm_dep [" + insertRequireConfirmDeps.size() + "]筆";
		// log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

		if (WkStringUtils.isEmpty(insertRequireConfirmDeps)) {
			log.warn(procTitle);
			log.warn("傳入的資料為空，不執行");
			return;
		}

		// 檢查傳入資料有重複
		Set<String> checkSet = Sets.newHashSet();
		for (RequireConfirmDep requireConfirmDep : insertRequireConfirmDeps) {
			String key = requireConfirmDep.getRequireSid() + "--" + requireConfirmDep.getDepSid();
			if (checkSet.contains(key)) {
				throw new UserMessageException("重複：[" + key + "]", InfomationLevel.ERROR);
			}
		}

		StringBuffer insertSQL = new StringBuffer();
		insertSQL.append("INSERT ");
		insertSQL.append("INTO ");
		insertSQL.append("    tr_require_confirm_dep ");
		insertSQL.append("    ( ");
		insertSQL.append("        status, ");
		insertSQL.append("        create_usr, ");
		insertSQL.append("        create_dt, ");
		insertSQL.append("        require_sid, ");
		insertSQL.append("        dep_sid, ");
		insertSQL.append("        owner_sid, ");
		insertSQL.append("        prog_status ");
		insertSQL.append("    ) ");
		insertSQL.append("    VALUES ");
		insertSQL.append("    ( ");
		insertSQL.append("        0, "); // status
		insertSQL.append("        ?, "); // create_usr
		insertSQL.append("        ?, "); // create_dt
		insertSQL.append("        ?, "); // require_sid
		insertSQL.append("        ?, "); // dep_sid
		insertSQL.append("        " + WkConstants.MANAGER_VIRTAUL_USER_SID + ", ");// owner_sid
		insertSQL.append("        ? "); // prog_status
		insertSQL.append("    );");

		int totalSize = insertRequireConfirmDeps.size();
		for (int i = 0; i < totalSize; i += BATCH_UPDATE_SIZE) {
			int index = (i + BATCH_UPDATE_SIZE) > totalSize ? totalSize : i + BATCH_UPDATE_SIZE;
			// 取得本次批次執行筆數
			final List<RequireConfirmDep> batchList = insertRequireConfirmDeps.subList(i, index);

			this.jdbcTemplate.batchUpdate(insertSQL.toString(), new BatchPreparedStatementSetter() {
				@Override
				public int getBatchSize() {
					return batchList.size();
				}

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					RequireConfirmDep item = batchList.get(i);
					int index = 1;
					// create_usr
					ps.setInt(index++, item.getCreatedUser());
					// create_dt
					ps.setString(index++, df.format(item.getCreatedDate()));
					// require_sid
					ps.setString(index++, item.getRequireSid());
					// dep_sid
					ps.setLong(index++, item.getDepSid());
					// prog_status
					ps.setString(index++, item.getProgStatus().name());
				}
			});
		}
		log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
	}

	/**
	 * 批次新增 tr_trns_backup
	 * 
	 * @param insertHistorys
	 */
	@Transactional(rollbackFor = Exception.class)
	public void batchInserTrnsBackups(List<TrnsBackup> inserTrnsBackups) {

		Long startTime = System.currentTimeMillis();
		String procTitle = "INSERT tr_trns_backup [" + inserTrnsBackups.size() + "]筆";
		// log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

		if (WkStringUtils.isEmpty(inserTrnsBackups)) {
			log.warn(procTitle);
			log.warn("傳入的資料為空，不執行");
			return;
		}

		StringBuffer insertSQL = new StringBuffer();
		insertSQL.append("INSERT ");
		insertSQL.append("INTO ");
		insertSQL.append("    tr_trns_backup ");
		insertSQL.append("    ( ");
		insertSQL.append("        status, ");
		insertSQL.append("        create_usr, ");
		insertSQL.append("        create_dt, ");
		insertSQL.append("        trns_type, ");
		insertSQL.append("        cust_key, ");
		insertSQL.append("        back_data ");
		insertSQL.append("    ) ");
		insertSQL.append("    VALUES ");
		insertSQL.append("    ( ");
		insertSQL.append("        0, "); // status
		insertSQL.append("        ?, "); // create_usr
		insertSQL.append("        ?, "); // create_dt
		insertSQL.append("        ?, "); // trns_type
		insertSQL.append("        ?, "); // cust_key
		insertSQL.append("        ? "); // back_data
		insertSQL.append("    );");

		int totalSize = inserTrnsBackups.size();
		for (int i = 0; i < totalSize; i += BATCH_UPDATE_SIZE) {
			int index = (i + BATCH_UPDATE_SIZE) > totalSize ? totalSize : i + BATCH_UPDATE_SIZE;
			// 取得本次批次執行筆數
			final List<TrnsBackup> batchList = inserTrnsBackups.subList(i, index);

			this.jdbcTemplate.batchUpdate(insertSQL.toString(), new BatchPreparedStatementSetter() {

				@Override
				public int getBatchSize() {
					return batchList.size();
				}

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {

					TrnsBackup item = batchList.get(i);
					int index = 1;
					// create_usr
					ps.setInt(index++, item.getCreatedUser());
					// create_dt
					ps.setString(index++, df.format(item.getCreatedDate()));
					// trns_type
					ps.setString(index++, item.getTrnsType());
					// cust_key
					ps.setString(index++, item.getCustKey());
					// back_data
					ps.setString(index++, item.getBackData());
				}
			});
		}
		log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
	}

	private String prepareKey(String requireSid, Integer depSid) {
		return requireSid + ":" + depSid;
	}

	/**
	 * 因批次執行 insert tr_require_confirm_dep , 故資料準備階段無法得知 require_confirm_sid
	 * 在此郵資料庫中取回
	 * 
	 * @param insertRequireConfirmDepHistorys
	 */
	private void prepareRequireConfirmDepHistory(List<RequireConfirmDepHistory> insertRequireConfirmDepHistorys) {

		Long startTime = System.currentTimeMillis();
		String procTitle = "prepareRequireConfirmDepHistory [" + insertRequireConfirmDepHistorys.size() + "]筆";
		// log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

		// ====================================
		// 收集 RequireSid
		// ====================================
		Set<String> requireSids = Sets.newHashSet();
		for (RequireConfirmDepHistory requireConfirmDepHistory : insertRequireConfirmDepHistorys) {
			requireSids.add(requireConfirmDepHistory.getRequireSid());
		}

		// ====================================
		// 建立查詢物件
		// ====================================
		String sql = ""
		        + "SELECT require_sid, "
		        + "       dep_sid, "
		        + "       require_confirm_sid "
		        + "  FROM tr_require_confirm_dep"
		        + " WHERE require_sid in (:requireSids) ";

		Query query = entityManager.createNativeQuery(sql);
		query.setParameter("requireSids", Lists.newArrayList(requireSids));

		@SuppressWarnings("unchecked")
		List<Object[]> results = query.getResultList();
		// log.debug("tr_require_confirm_dep:[" + results.size() + "]");

		Map<String, Long> requireConfirmSidMapByReqSidDepSid = Maps.newHashMap();
		for (Object[] objects : results) {
			String requireSid = (String) objects[0];
			Integer depSid = (Integer) objects[1];
			Long requireConfirmSid = Long.valueOf(objects[2] + "");

			requireConfirmSidMapByReqSidDepSid.put(
			        this.prepareKey(requireSid, depSid),
			        requireConfirmSid);
		}

		for (RequireConfirmDepHistory requireConfirmDepHistory : insertRequireConfirmDepHistorys) {
			Long requireConfirmSid = requireConfirmSidMapByReqSidDepSid.get(
			        this.prepareKey(
			                requireConfirmDepHistory.getRequireSid(),
			                requireConfirmDepHistory.getDepSid()));

			if (requireConfirmSid == null) {
				WkCommonUtils.logWithStackTrace(InfomationLevel.ERROR, "找不到對應的 require_confirm_sid! "
				        + "require_sid:[" + requireConfirmDepHistory.getRequireSid() + "] "
				        + "depSid:[" + requireConfirmDepHistory.getDepSid() + "] ");
				continue;
			}

			requireConfirmDepHistory.setRequireConfirmSid(requireConfirmSid);
		}

		log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
	}

	/**
	 * 查詢已存在確認檔的需求單 sid
	 * 
	 * @param requireNos
	 * @return
	 */
	public List<String> queryExsitConfirmDep() {

		String sql = ""
		        + "SELECT DISTINCT require_sid "
		        + "  FROM tr_require_confirm_dep; ";

		Query query = entityManager.createNativeQuery(sql);

		@SuppressWarnings("unchecked")
		List<String> results = query.getResultList();

		List<String> requireSids = Lists.newArrayList();
		for (String objects : results) {
			requireSids.add(objects + "");
		}
		return requireSids;
	}

	/**
	 * 查詢已存在確認檔的需求單 sid
	 * 
	 * @param requireNos
	 * @return
	 */
	public List<AssignSendSearchInfoVO> querySearchInfoDepSidByRequireNo(List<String> requireNos) {

		String sql = ""
		        + "SELECT require_no as requireNo, "
		        + "       dep_sid    as depSid, "
		        + "       type       as typeInt "
		        + "  FROM tr_assign_send_search_info "
		        + " WHERE require_no in (:requireNos); ";

		Map<String, Object> parameters = Maps.newHashMap();
		parameters.put("requireNos", requireNos);

		return this.nativeSqlRepository.getResultList(sql, parameters, AssignSendSearchInfoVO.class);
	}

	/**
	 * update tr_require.trns_flag_for_v70 為 true
	 * 
	 * @param requireNos
	 */
	@Transactional(rollbackFor = Exception.class)
	public void updateRequire(Set<String> requireNos, boolean v70TrnsFlag) {

		Long startTime = System.currentTimeMillis();
		String procTitle = "UPDATE FROM tr_require.trns_flag_for_v70";
		// log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

		if (WkStringUtils.isEmpty(requireNos)) {
			log.warn(procTitle);
			log.warn("傳入的資料為空，不執行");
			return;
		}

		String updateSQL = ""
		        + "UPDATE tr_require "
		        + "   SET trns_flag_for_v70 = " + (v70TrnsFlag ? "1" : "0")
		        + " WHERE require_no IN ('"
		        + String.join("', '", requireNos)
		        + "');";

		int rows = this.jdbcTemplate.update(updateSQL);

		log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle + "共[" + rows + "]筆"));
	}

	/**
	 * 批次更新 tr_assign_send_info
	 * 
	 * @param insertHistorys
	 */
	@Transactional(rollbackFor = Exception.class)
	public void batchUpdateAssignSendInfoForAddByInternal(
	        List<ProceeAddAssignDepForInternalTo> proceeAddAssignDepForInternalTos) {

		// 過濾掉需要 insert 者
		List<ProceeAddAssignDepForInternalTo> procList = proceeAddAssignDepForInternalTos.stream()
		        .filter(to -> WkStringUtils.notEmpty(to.getInfoSid()))
		        .collect(Collectors.toList());

		if (WkStringUtils.isEmpty(procList)) {
			log.warn("傳入的 proceeAddAssignDepForInternalTos 為空，不執行");
			return;
		}
		Long startTime = System.currentTimeMillis();
		String procTitle = "update tr_assign_send_info [" + procList.size() + "]筆";
		log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

		StringBuffer updateSQL = new StringBuffer();
		updateSQL.append("UPDATE tr_assign_send_info ");
		updateSQL.append("SET    info = ? ");
		updateSQL.append("WHERE  tr_assign_send_info.info_sid = ?; ");

		SetupInfoToConverter setupInfoToConverter = new SetupInfoToConverter();

		int totalSize = procList.size();
		for (int i = 0; i < totalSize; i += BATCH_UPDATE_SIZE) {
			int index = (i + BATCH_UPDATE_SIZE) > totalSize ? totalSize : i + BATCH_UPDATE_SIZE;
			// 取得本次批次執行筆數
			final List<ProceeAddAssignDepForInternalTo> batchList = procList.subList(i, index);

			this.jdbcTemplate.batchUpdate(updateSQL.toString(), new BatchPreparedStatementSetter() {

				@Override
				public int getBatchSize() {
					return batchList.size();
				}

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {

					ProceeAddAssignDepForInternalTo item = batchList.get(i);

					SetupInfoTo setupInfoTo = setupInfoToConverter.convertToEntityAttribute(item.getInfo());

					if (setupInfoTo == null) {
						log.info(new com.google.gson.GsonBuilder().setPrettyPrinting().create().toJson(item));
						throw new RuntimeException("xx");
					}

					setupInfoTo.getDepartment().add(item.getDepSid() + "");

					int index = 1;
					// info
					ps.setString(index++, setupInfoToConverter.convertToDatabaseColumn(setupInfoTo));
					// info_sid
					ps.setString(index++, item.getInfoSid());
				}
			});
		}
		log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
	}

	/**
	 * @param proceeAddAssignDepForInternalTos
	 */
	@Transactional(rollbackFor = Exception.class)
	public void batchInsertAssignSendSearchInfoForAddByInternal(List<ProceeAddAssignDepForInternalTo> proceeAddAssignDepForInternalTos) {

		Date sysDate = new Date();

		if (WkStringUtils.isEmpty(proceeAddAssignDepForInternalTos)) {
			log.warn("傳入的 proceeAddAssignDepForInternalTos 為空，不執行");
			return;
		}
		Long startTime = System.currentTimeMillis();
		String procTitle = "INSERT tr_assign_send_search_info [" + proceeAddAssignDepForInternalTos.size() + "]筆";
		log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

		StringBuffer insertSQL = new StringBuffer();
		insertSQL.append("INSERT ");
		insertSQL.append("INTO ");
		insertSQL.append("    tr_assign_send_search_info ");
		insertSQL.append("    ( ");
		insertSQL.append("        create_usr, ");
		insertSQL.append("        create_dt, ");
		insertSQL.append("        search_sid, ");
		insertSQL.append("        require_sid, ");
		insertSQL.append("        require_no, ");
		insertSQL.append("        type, ");
		insertSQL.append("        dep_sid ");
		insertSQL.append("    ) ");
		insertSQL.append("    VALUES ");
		insertSQL.append("    ( ");
		insertSQL.append("        ?, "); // create_usr
		insertSQL.append("        ?, "); // create_dt
		insertSQL.append("        ?, "); // search_sid
		insertSQL.append("        ?, "); // require_sid
		insertSQL.append("        ?, "); // require_no
		insertSQL.append("        ?, "); // type
		insertSQL.append("        ? "); // dep_sid
		insertSQL.append("    );");

		int totalSize = proceeAddAssignDepForInternalTos.size();
		for (int i = 0; i < totalSize; i += BATCH_UPDATE_SIZE) {
			int index = (i + BATCH_UPDATE_SIZE) > totalSize ? totalSize : i + BATCH_UPDATE_SIZE;
			// 取得本次批次執行筆數
			final List<ProceeAddAssignDepForInternalTo> batchList = proceeAddAssignDepForInternalTos.subList(i, index);

			this.jdbcTemplate.batchUpdate(insertSQL.toString(), new BatchPreparedStatementSetter() {

				@Override
				public int getBatchSize() {
					return batchList.size();
				}

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {

					ProceeAddAssignDepForInternalTo item = batchList.get(i);
					int index = 1;
					// create_usr
					ps.setInt(index++, 1);
					// create_dt
					ps.setString(index++, df.format(sysDate));
					// search_sid
					ps.setString(index++, UUID.randomUUID().toString());
					// require_sid
					ps.setString(index++, item.getRequireSid());
					// require_no
					ps.setString(index++, item.getRequireNo());
					// type
					ps.setInt(index++, AssignSendType.ASSIGN.ordinal());
					// dep_sid
					ps.setLong(index++, item.getDepSid());
				}
			});
		}
		log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
	}

	/**
	 * 批次新增 tr_assign_send_info_history
	 * 
	 * @param insertHistorys
	 */
	@Transactional(rollbackFor = Throwable.class)
	public void batchInsertAssignSendInfoForAddByInternal(List<ProceeAddAssignDepForInternalTo> proceeAddAssignDepForInternalTos) {

		// 過濾掉為 update 者
		List<ProceeAddAssignDepForInternalTo> procList = proceeAddAssignDepForInternalTos.stream()
		        .filter(to -> WkStringUtils.isEmpty(to.getInfoSid()))
		        .collect(Collectors.toList());

		if (WkStringUtils.isEmpty(procList)) {
			log.warn("傳入的 proceeAddAssignDepForInternalTos 為空，不執行");
			return;
		}
		Long startTime = System.currentTimeMillis();
		String procTitle = "INSERT tr_assign_send_info [" + procList.size() + "]筆";
		log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

		StringBuffer insertSQL = new StringBuffer();
		insertSQL.append("INSERT ");
		insertSQL.append("INTO ");
		insertSQL.append("    tr_assign_send_info ");
		insertSQL.append("    ( ");
		insertSQL.append("        info_sid, ");
		insertSQL.append("        require_sid, ");
		insertSQL.append("        require_no, ");
		insertSQL.append("        type, ");
		insertSQL.append("        create_usr, ");
		insertSQL.append("        create_dt, ");
		insertSQL.append("        status, ");
		insertSQL.append("        info, ");
		insertSQL.append("        group_info ");
		insertSQL.append("    ) ");
		insertSQL.append("    VALUES ");
		insertSQL.append("    ( ");
		insertSQL.append("        ?, "); // info_sid
		insertSQL.append("        ?, "); // require_sid
		insertSQL.append("        ?, "); // require_no
		insertSQL.append("        ?, "); // type
		insertSQL.append("        ?, "); // create_usr
		insertSQL.append("        ?, "); // create_dt
		insertSQL.append("        ?, "); // status
		insertSQL.append("        ?, "); // info
		insertSQL.append("        ? "); // group_info
		insertSQL.append("    );");

		int totalSize = procList.size();
		for (int i = 0; i < totalSize; i += BATCH_UPDATE_SIZE) {
			int index = (i + BATCH_UPDATE_SIZE) > totalSize ? totalSize : i + BATCH_UPDATE_SIZE;
			// 取得本次批次執行筆數
			final List<ProceeAddAssignDepForInternalTo> batchList = procList.subList(i, index);

			this.jdbcTemplate.batchUpdate(insertSQL.toString(), new BatchPreparedStatementSetter() {

				@Override
				public int getBatchSize() {
					return batchList.size();
				}

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {

					ProceeAddAssignDepForInternalTo item = batchList.get(i);
					int index = 1;
					// info_sid
					ps.setString(index++, UUID.randomUUID().toString());
					// require_sid
					ps.setString(index++, item.getRequireSid());
					// require_no
					ps.setString(index++, item.getRequireNo());
					// type
					ps.setInt(index++, AssignSendType.ASSIGN.ordinal());
					// create_usr
					ps.setInt(index++, 1);
					// create_dt
					ps.setString(index++, df.format(new Date()));
					// status
					ps.setInt(index++, 0);
					// info
					SetupInfoTo setupInfoTo = new SetupInfoTo();
					setupInfoTo.getDepartment().add(item.getDepSid() + "");
					ps.setString(index++, new SetupInfoToConverter().convertToDatabaseColumn(setupInfoTo));
					// group_info
					ps.setString(index++, new AssignSendGroupsToConverter().convertToDatabaseColumn(new AssignSendGroupsTo()));

				}
			});
		}
		log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
	}

}
