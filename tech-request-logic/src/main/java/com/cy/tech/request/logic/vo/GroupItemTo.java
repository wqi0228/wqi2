/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.vo;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 群組清單顯示用
 *
 * @author shaun
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"key", "name", "type"})
public class GroupItemTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8964933868387833846L;
    /**
     * 型態
     */
    public enum GroupItemToType {

        DEPT("部門"),
        EMPLOY("個人"),
        GROUP_DEPT("部門"),
        GROUP_EMPLOY("個人");

        @Getter
        private final String typeName;

        private GroupItemToType(String typeName) {
            this.typeName = typeName;
        }
    }

    public interface GroupItemToMethod extends Serializable {

        public void delete(GroupItemTo to);
    }

    /** 組織或個人sid */
    private String key;
    /* 
     * 1.DEPT   = 組織名稱 <BR/>
     * 2.EMPLOY = 組織名稱 - 個人<BR/>
     * 3.GROUP_DEPT   = 組織名稱 (群組名稱) <BR/>
     * 4.GROUP_EMPLOY = 組織名稱 - 個人 (群組名稱)
     */
    private String name;
    /** 群組sid如果有的話... */
    private String groupSid;
    /** 清單型態 */
    private GroupItemToType type;
    /** 是否disabled按鈕 */
    private Boolean disabledBtn = Boolean.FALSE;
    /** 觸動方法代理方法 */
    private GroupItemToMethod method;

}
