/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import lombok.Getter;
import lombok.Setter;

/**
 * 收藏夾顯示vo (home01.xhtml)
 *
 * @author jason_h
 */
public class Home01View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -310146737371315036L;
    /** 收藏時間 */
    @Getter
    @Setter
    private Date createdDate;
    /** 本地端連結網址 */
    @Getter
    @Setter
    private String localUrlLink;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(super.getSid());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Home01View other = (Home01View) obj;
        if (!Objects.equals(super.getSid(), other.getSid())) {
            return false;
        }
        return true;
    }
}
