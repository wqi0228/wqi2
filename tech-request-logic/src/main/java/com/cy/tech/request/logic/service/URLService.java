/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import java.io.IOException;
import java.io.Serializable;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.util.FusionUrlServiceUtils;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.helper.URLHelper;
import com.cy.tech.request.logic.vo.UrlParamTo;
import com.cy.tech.request.vo.constants.ReqPermission;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.cache.WkUserWithRolesCache;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 網址列服務
 *
 * @author shaun
 */
@Slf4j
@Component
public class URLService implements Serializable, InitializingBean {

    /**
     * 
     */
    private static final long serialVersionUID = -9064669940675517869L;

    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static URLService instance;

    /**
     * @return instance
     */
    public static URLService getInstance() { return instance; }

    private static void setInstance(URLService instance) { URLService.instance = instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    // ========================================================================
    // service
    // ========================================================================
    @Autowired
    private SearchService searchService;
    @Autowired
    private WkJsonUtils jsonUtils;
    @Autowired
    private URLHelper urlHelper;

    // ========================================================================
    // service
    // ========================================================================
    /**
     * 網址參數歸屬數性
     */
    public enum URLServiceAttr {

        /** 關聯群組 */
        URL_ATTR_L("l"),
        /** 需求單 */
        URL_ATTR_M("m"),
        /** 搜尋報表 */
        URL_ATTR_S("s"),
        /** 指定tab - 原型確認 */
        URL_ATTR_TAB_PT("pt"),
        /** 指定tab - 送測 */
        URL_ATTR_TAB_ST("st"),
        /** 指定tab - ON程式 */
        URL_ATTR_TAB_OP("op"),
        /** 指定tab - 其它資料設定程式 */
        URL_ATTR_TAB_OS("os"),
        /** 指定tab - 分派 */
        URL_ATTR_TAB_AS("as"),
        /** 指定tab - 收呈報 */
        URL_ATTR_TAB_INBOX_REPORT("ix_rep"),
        /** 指定tab - 收個人 */
        URL_ATTR_TAB_INBOX_PERSONAL("ix_per"),
        /** 指定tab - 收指示 */
        URL_ATTR_TAB_INBOX_INSTRUCTION("ix_ins"),
        /** 指定tab - 寄件備份 */
        URL_ATTR_TAB_INBOX_SEND("ix_send"),
        // 顯示抬頭
        URL_ATTR_SHOW_TITLE_BTN("cb"),
        /** 單據簽核作業模式 */
        URL_ATTR_SHOW_TITLE_BTN_FROM_FORMSIGN("mode"),

        /** 未領單據查詢 - 負責人 */
        URL_ATTR_SEARCH_OWNERSID("owrsid"),

        // 分派/通知單據查詢, 查詢類別
        URL_ATTR_SEARCH07_CONDITION_TYPE("cType"),
        
        // ON程式一覽表 (首頁待辦)
        URL_ATTR_SEARCH16_FROM_PORTAL("mode"),

        // 被通知單據查詢
        URL_ATTR_NEWSEARCH03_CONDITION_TYPE("cType"),

        /**
         * 管理員以單號開啟連結
         */
        URL_ATTR_ADMIN_BY_NO("am"),
        /**
         * 管理員以SID開啟連結
         */
        URL_ATTR_ADMIN_BY_SID("bm"),
        ;

        @Getter
        private final String attr;

        URLServiceAttr(String attrStr) {
            this.attr = attrStr;
        }

    }

    public final static String ILLEAL_ACCESS = "IllegalAccess ";
    public final static String ERROR_CODE_ATTR = "code";
    public final static String ERROR_CODE_NO_PERMISSION = "1";
    public final static String ERROR_CODE_URL_LINK_EXPIRED = "2";
    public final static String ERROR_CODE_URL_ATTR_FAILD = "3";

    // some random salt
    private static final byte[] SALT = { (byte) 0x21, (byte) 0x21, (byte) 0xF0, (byte) 0x55, (byte) 0xC3, (byte) 0x9F, (byte) 0x5A, (byte) 0x75 };

    private final static int ITERATION_COUNT = 31;

    /**
     * 新增單據
     */
    public final static String URL_ADD_NEW_FROM = "/require/require01.xhtml";

    private final static String LOCAL_REQ_LINK = "../require/require02.xhtml";
    private final static String DRAFT_REQ_LINK = "../require/require06.xhtml";
    private final static String WORK_TEST_INFO_LINK = "../sendTest/singleWorkTest.xhtml";

    /**
     * 建立加密的URL參數 <BR/>
     *
     * @param attr
     * @param to
     * @return
     */
    public String createUrlLinkParam(URLServiceAttr attr, UrlParamTo to) {
        try {
            String jsonStr = jsonUtils.toJson(to);
            return "?" + attr.getAttr() + "=" + this.encrypt(jsonStr);
        } catch (IOException ex) {
            log.error("parser UrlTo to json fail!!" + ex.getMessage(), ex);
        }
        return "";
    }

    /**
     * 還原URL參數
     *
     * @param urlParam
     * @return
     */
    public UrlParamTo recoveryUrlParamTo(String urlParam) throws IllegalArgumentException {
        try {
            String jsonStr = this.decrypt(urlParam);
            return jsonUtils.fromJson(jsonStr, UrlParamTo.class);
        } catch (Exception ex) {
            log.error("parser json to UrlTo fail!!" + ex.getMessage(), ex);
            throw new IllegalArgumentException(ERROR_CODE_URL_ATTR_FAILD, ex);
        }
    }

    public String encrypt(String input) {
        if (input == null) {
            throw new IllegalArgumentException();
        }
        try {

            KeySpec keySpec = new PBEKeySpec(null, SALT, ITERATION_COUNT);
            AlgorithmParameterSpec paramSpec = new PBEParameterSpec(SALT, ITERATION_COUNT);

            SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);

            Cipher ecipher = Cipher.getInstance(key.getAlgorithm());
            ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);

            byte[] enc = ecipher.doFinal(input.getBytes());

            String res = new String(Base64.encodeBase64(enc));
            // escapes for url
            res = res.replace('+', '-').replace('/', '_').replace("%", "%25").replace("\n", "%0A");

            return res;

        } catch (NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException
                | IllegalBlockSizeException | BadPaddingException e) {
            log.error(e.getMessage(), e);
        }
        return "";

    }

    public String decrypt(String token) {
        if (token == null) {
            return null;
        }
        try {
            String input = token.replace("%0A", "\n").replace("%25", "%").replace('_', '/').replace('-', '+');
            byte[] dec = Base64.decodeBase64(input.getBytes());

            KeySpec keySpec = new PBEKeySpec(null, SALT, ITERATION_COUNT);
            AlgorithmParameterSpec paramSpec = new PBEParameterSpec(SALT, ITERATION_COUNT);

            SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);

            Cipher dcipher = Cipher.getInstance(key.getAlgorithm());
            dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);

            byte[] decoded = dcipher.doFinal(dec);
            String result = new String(decoded);
            return result;

        } catch (NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException
                | IllegalBlockSizeException | BadPaddingException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    public void checkDeadline(UrlParamTo to) throws IllegalArgumentException {
        if (to.getDl() == null) {
            return;
        }
        Date date = new Date();
        if (to.getDl().before(date)) {
            throw new IllegalArgumentException(ERROR_CODE_URL_LINK_EXPIRED);
        }
    }

    public void checkUnit(UrlParamTo to, User login) throws IllegalArgumentException {
        boolean checkOrg = to.getOrg() != null && !to.getOrg().isEmpty();
        boolean checkUser = to.getUser() != null && !to.getUser().isEmpty();
        if (!checkOrg && !checkUser) {
            return;
        }
        if (checkUser && this.checkUserOrIsUserManager(to.getUser(), login)) {
            return;
        }
        Org dep = WkOrgCache.getInstance().findBySid(login.getPrimaryOrg().getSid());
        if (checkOrg && this.checkOrgOrIsAboveOrgs(to.getOrg(), dep)) {
            return;
        }
        throw new IllegalArgumentException(ERROR_CODE_NO_PERMISSION);
    }

    /**
     * 檢查是否為可閱成員或其可閱成員的上層主管
     *
     * @return
     */
    private boolean checkUserOrIsUserManager(List<Integer> userSids, User login) {
        if (userSids.contains(login.getSid())) {
            return true;
        }
        for (Integer userSid : userSids) {
            User user = WkUserCache.getInstance().findBySid(userSid);
            Set<Integer> orgManagerSids = WkOrgUtils.findOrgManagerUserSidByOrgSid(
                    user.getPrimaryOrg().getSid());
            if (orgManagerSids.contains(login.getSid())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 檢查是否為可閱部門或可閱部門的上層部門
     *
     * @param orgSids
     * @param loginOrg
     * @return
     */
    private boolean checkOrgOrIsAboveOrgs(List<Integer> orgSids, Org loginOrg) {
        if (orgSids.contains(loginOrg.getSid())) {
            return true;
        }
        for (Integer orgSid : orgSids) {
            Set<Integer> parentOrgSids = WkOrgUtils.prepareParentDepUntilOrgLevel(orgSid, OrgLevel.GROUPS);
            if (parentOrgSids.contains(loginOrg.getSid())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 建立本地端服務連結 (外部服務連結請勿呼叫)
     *
     * @param attr
     * @param key
     * @param expiredDay
     * @return
     */
    public String createLoacalURLLink(URLServiceAttr attr, UrlParamTo to) {
        return LOCAL_REQ_LINK + this.createUrlLinkParam(attr, to);
    }

    public String encryptUrlPararm(UrlParamTo to) {
        return this.createUrlLinkParam(URLServiceAttr.URL_ATTR_M, to);
    }

    /**
     * 建立草稿連結網址
     *
     * @param attr
     * @param to
     * @return
     */
    public String createDraftURLLink(URLServiceAttr attr, UrlParamTo to) {
        return DRAFT_REQ_LINK + this.createUrlLinkParam(attr, to);
    }

    /**
     * 建立簡單網址列
     *
     * @param attr
     * @param key
     * @param expiredDay
     * @return
     */
    public String createSimpleURLLink(URLServiceAttr attr, String key, int expiredDay) {
        return this.createSimpleURLLink(attr, key, null, expiredDay);
    }

    /**
     * 建立簡單網址列
     *
     * @param attr
     * @param key
     * @param reqNo
     * @param expiredDay
     * @return
     */
    public String createSimpleURLLink(URLServiceAttr attr, String key, String reqNo, int expiredDay) {
        UrlParamTo to = new UrlParamTo();
        to.setNo(key);
        to.setReqNo(reqNo);
        to.setDl(searchService.addDay(Calendar.getInstance(), expiredDay));
        return this.createUrlLinkParam(attr, to);
    }

    /**
     * 解析URL參數 <BR/>
     *
     * @param urlParam
     * @param login
     * @return
     */
    public String parseIllegalUrlParam(
            String urlParam,
            User login,
            String loginCompID) {
        try {
            UrlParamTo to = this.recoveryUrlParamTo(urlParam);

            // ====================================
            // 需求單系統管理員
            // ====================================
            // 具有 需求單專案管理員 角色
            boolean hasPermissionRole = WkUserUtils.isUserHasRole(
                    login.getSid(),
                    loginCompID,
                    ReqPermission.ROLE_SYS_ADMIN);

            if (hasPermissionRole) {
                this.urlHelper.returnAuthInterceptor(to.getNo(), "admin");
                return to.getNo();
            }

            // ====================================
            // 需求單專案管理員
            // ====================================
            // 具有 需求單專案管理員 角色
            hasPermissionRole = WkUserUtils.isUserHasRole(
                    login.getSid(),
                    loginCompID,
                    ReqPermission.ROLE_PROJECT_MGR);

            if (hasPermissionRole) {
                this.urlHelper.returnAuthInterceptor(to.getNo(), "需求單專案管理員");
                return to.getNo();
            }

            // ====================================
            // 檢查可閱權限
            // ====================================
            // 查詢登入者所有角色
            List<Long> roleSids = WkUserWithRolesCache.getInstance().findRoleSidsByUserAndLoginCompID(
                    login.getSid(),
                    loginCompID);

            if (urlHelper.checkPermissions(to, login, loginCompID, roleSids)) {
                return to.getNo();
            }

            log.info("user {}, {} 無閱讀權限!", login.getId(), to.getNo());

        } catch (Exception e) {
            return ILLEAL_ACCESS + e.getMessage();
        }

        return ILLEAL_ACCESS + ERROR_CODE_NO_PERMISSION;
    }

    /**
     * 是否有可閱權限
     * 
     * @param requireNo
     * @param login
     * @param loginCompID
     * @param roleSids
     * @return
     */
    public boolean hasReadPermission(String requireNo, User login, String loginCompID, List<Long> roleSids) {
        return urlHelper.checkPermissions(requireNo, login, loginCompID, roleSids);
    }

    /**
     * 建立本地端服務連結 - 頁籤 (外部服務連結請勿呼叫)
     *
     * @param attr
     * @param attrTab
     * @param to
     * @param tabSid
     * @return
     */
    public String createLocalUrlLinkParamForTab(URLServiceAttr attr, UrlParamTo to, URLServiceAttr attrTab, String tabSid) {
        return LOCAL_REQ_LINK + this.createUrlLinkParamForTab(attr, to, attrTab, tabSid);
    }

    /**
     * 建立加密的URL參數 - 切換頁籤
     *
     * @param attr
     * @param attrTab
     * @param to
     * @param tabSid
     * @return
     */
    public String createUrlLinkParamForTab(URLServiceAttr attr, UrlParamTo to, URLServiceAttr attrTab, String tabSid) {
        try {
            String jsonStr = jsonUtils.toJson(to);
            return "?" + attr.getAttr() + "=" + this.encrypt(jsonStr) + "&" + attrTab.getAttr() + "=" + tabSid;
        } catch (IOException ex) {
            log.error("parser UrlTo to json fail!!" + ex.getMessage(), ex);
        }
        return "";
    }

    /**
     * 建立空的URL物件
     *
     * @param userSid
     * @param reuqireNo
     * @return
     */
    public UrlParamTo createSimpleUrlTo(Integer userSid, String reuqireNo, int expiredDay) {
        UrlParamTo to = new UrlParamTo();
        to.setUser(Lists.newArrayList(userSid));
        to.setDl(searchService.addDay(Calendar.getInstance(), 1));
        to.setNo(reuqireNo);
        return to;
    }

    /**
     * 建立開啟送測單URL
     * 
     * @param testInfoNo
     * @return
     */
    public String createWorkTestInfoUrl(String testInfoNo) {
        if (Strings.isNullOrEmpty(testInfoNo)) {
            return "";
        }
        return String.format("%s?testInfoNo=%s", WORK_TEST_INFO_LINK, testInfoNo);
    }

    /**
     * 建立需求單URL
     * 
     * @param requireNo
     * @return
     */
    public String buildRequireURL(String requireNo) {
        if (Strings.isNullOrEmpty(requireNo)) {
            return "";
        }
        String servletPath = createLoacalURLLink(URLServiceAttr.URL_ATTR_M, new UrlParamTo(requireNo)).substring(2);

        String url = FusionUrlServiceUtils.getUrlByPropKey("tech.request.ap.url");
        return String.format("%s%s", url, servletPath);
    }

    /**
     * 建立系統通知設定URL
     * 
     * @return
     */
    public String buildSettingSysNotifyURL() {
        String url = FusionUrlServiceUtils.getUrlByPropKey("tech.request.ap.url");
        return String.format("%s%s",
                url,
                "/setting/setting_sys_notify.xhtml");
    }

    public String buildRequireAPURL(String compId) throws SystemOperationException {
        // 依據公司別取得網址
        String domainUrl = WkOrgCache.getInstance().findDomainUrlByCompId(compId);

        // 取得需求單AP 路徑
        String apPath = FusionUrlServiceUtils.getUrlByPropKey("tech.request.ap.url");
        String[] apPaths = apPath.split("/");
        apPath = apPaths[apPaths.length - 1];

        String url = String.format("%s%s", domainUrl, apPath);
        return url;
    }

    /**
     * @param requireNo
     * @param compId
     * @return
     * @throws SystemOperationException
     */
    public String buildRequireURLForMail(String requireNo, String compId) throws SystemOperationException {

        // 依據公司別取得網址
        String domainUrl = WkOrgCache.getInstance().findDomainUrlByCompId(compId);
        // System.out.println("domainUrl:" + domainUrl);

        // 取得需求單AP 路徑
        String apPath = FusionUrlServiceUtils.getUrlByPropKey("tech.request.ap.url");
        String[] apPaths = apPath.split("/");
        apPath = apPaths[apPaths.length - 1];
        // System.out.println("apPath:" + apPath);

        // 取得單據網址
        String servletPath = createLoacalURLLink(URLServiceAttr.URL_ATTR_M, new UrlParamTo(requireNo)).substring(2);
        // System.out.println("servletPath:" + servletPath);

        String url = String.format("%s%s%s", domainUrl, apPath, servletPath);
        // System.out.println("url:" + url);
        return url;
    }

    /**
     * 外部連結，並開啟子單tab
     * 
     * @param requireNo
     * @param compId
     * @param urlServiceAttr
     * @param tabSid
     * @return
     * @throws SystemOperationException
     */
    public String buildRequireURLWithTabForMail(
            String requireNo,
            String compId,
            URLServiceAttr urlServiceAttr,
            String tabSid) throws SystemOperationException {

        String url = this.buildRequireURLForMail(requireNo, compId);
        url += "?";
        url += urlServiceAttr;
        url += "=";
        url += tabSid;

        return url;
    }

    /**
     * @param compID
     * @return
     * @throws SystemOperationException
     */
    public String buildFullAddFormUrl(String compID) throws SystemOperationException {

        String fullUrl = WkCommonUtils.findDomainUrlByCompId(compID);

        // 開發環境, 有 port 號
        if (fullUrl.contains(":")) {
            return ".." + URL_ADD_NEW_FROM;
        }

        // 取得 專案 路徑
        String apPath = FusionUrlServiceUtils.getUrlByPropKey("tech.request.ap.url");
        if (apPath.startsWith("/")) {
            apPath = apPath.substring(1);
        }

        fullUrl += apPath;

        return fullUrl + apPath + URL_ADD_NEW_FROM;

    }
}
