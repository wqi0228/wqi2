/**
 * 
 */
package com.cy.tech.request.logic.service.syncmms.helper;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.vo.Org;
import com.cy.tech.request.logic.service.TemplateService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.URLService.URLServiceAttr;
import com.cy.tech.request.logic.service.syncmms.to.SyncFormTo;
import com.cy.tech.request.vo.enums.ComType;
import com.cy.tech.request.vo.log.LogSyncMmsActionType;
import com.cy.tech.request.vo.template.component.ComBase;
import com.cy.tech.request.vo.template.component.ComEditorTypeOne;
import com.cy.tech.request.vo.template.component.ComTextTypeOne;
import com.cy.work.backend.logic.WorkBackendParamService;
import com.cy.work.backend.vo.enums.WkBackendParam;
import com.cy.work.client.syncmms.WorkSyncMmsClient;
import com.cy.work.client.syncmms.to.formdata.SyncMmsFormDataActionType;
import com.cy.work.client.syncmms.to.processresult.SyncMmsProcessResultModel;
import com.cy.work.client.syncmms.to.token.SyncMmsTokenModel;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.alert.TechRequestAlertException;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Service
@Slf4j
public class SyncMmsSyncFormProcessDispatcher {

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private transient URLService urlService;
    @Autowired
    transient private WorkBackendParamService workBackendParamService;
    @Autowired
    transient private WorkSyncMmsClient workSyncMmsClient;
    @Autowired
    transient private SyncMmsSyncFormCreateRequireAndOnpgHelper syncMmsSyncFormCreateRequireAndOnpgHelper;
    @Autowired
    transient private SyncMmsSyncFormCreateOnpgHelper syncMmsSyncFormCreateOnpgHelper;
    @Autowired
    transient private SyncMmsSyncFormUpdateHelper syncMmsSyncFormUpdateHelper;
    @Autowired
    transient private SyncMmsSyncFormCancelHelper syncMmsSyncFormCancelHelper;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 執行，並將結果同步回MMS (打MMS 回傳結果失敗時，roll back)
     * 
     * @param syncFormTo
     * @param syncMmsTokenModel
     * @return
     * @throws SystemOperationException
     */
    @Transactional(rollbackFor = { Exception.class })
    public void processAndSyncResult(
            SyncFormTo syncFormTo,
            SyncMmsTokenModel syncMmsTokenModel) throws SystemOperationException {

        // ====================================
        // 分配處理
        // ====================================
        // 分配
        SystemOperationException rollbackException = null;
        try {
            this.processDispatcher(syncFormTo);
        } catch (SystemOperationException e) {
            syncFormTo.setLogSyncMmsActionType(LogSyncMmsActionType.EXCEPTION);
            syncFormTo.addErrorInfo(e.getMessage());
            rollbackException = e;
            // exception 也要打API, 故不 return;
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + e.getMessage() + "<br/>";
            log.error("MMS同步-" + errorMessage, e);
            // 錯誤訊息放在最上方
            syncFormTo.setLogSyncMmsActionType(LogSyncMmsActionType.EXCEPTION);
            syncFormTo.addErrorInfo(errorMessage);
            rollbackException = new TechRequestAlertException(errorMessage, syncFormTo.getLoginCompID());;
            // exception 也要打API, 故不 return;
        }

        // log.error("\r\n" + WkJsonUtils.getInstance().toPettyJson(syncFormTo.getErrorInfos()));

        // ====================================
        // 將處理結果，回傳給MMS
        // ====================================
        // 當狀況為無需更新時，也不用打MMS API (更新:不回傳資料本筆會卡住)
        if (LogSyncMmsActionType.UPDATE_NONE.equals(syncFormTo.getLogSyncMmsActionType())) {
            log.info(LogSyncMmsActionType.UPDATE_NONE.getDescr() + " [無需回傳結果給MMS]");
        }

        try {
            this.syncProcessResult(syncMmsTokenModel, syncFormTo);
        } catch (SystemOperationException e) {
            syncFormTo.setLogSyncMmsActionType(LogSyncMmsActionType.CALL_API_FAIL);
            throw e;
        } catch (Exception e) {
            String errorMessage = "CALL API 【回寫ON程式單號】失敗！" + e.getMessage() + "<br/>";
            log.error(errorMessage, e);
            syncFormTo.setLogSyncMmsActionType(LogSyncMmsActionType.EXCEPTION);
            // 再次拋出Exception , 強制讓前方執行資料 roll back
            throw new TechRequestAlertException(errorMessage, syncFormTo.getLoginCompID());
        }

        if (rollbackException != null) {
            throw rollbackException;
        }
    }

    /**
     * 執行分配器
     * 
     * @param syncFormTo
     * @throws SystemOperationException
     */
    private void processDispatcher(SyncFormTo syncFormTo) throws SystemOperationException {

        // ====================================
        // 依據資料狀態，分配處理動作
        // ====================================
        // ------------------
        // 前方檢核有錯誤，不再動作
        // ------------------
        if (syncFormTo.isError()) {
            syncFormTo.setLogSyncMmsActionType(LogSyncMmsActionType.VIRIFY_FAIL);
            return;
        }
        // ------------------
        // 新增需求單與ON程式單
        // ------------------
        else if (SyncMmsFormDataActionType.SAVE.equals(syncFormTo.getActionType())
                && WkStringUtils.isEmpty(syncFormTo.getRequire())
                && WkStringUtils.isEmpty(syncFormTo.getWorkOnpgTo())) {
            this.syncMmsSyncFormCreateRequireAndOnpgHelper.process(syncFormTo);
            return;
        }
        // ------------------
        // 僅新增ON程式單 (附掛於已存在需求單)
        // ------------------
        else if (SyncMmsFormDataActionType.SAVE.equals(syncFormTo.getActionType())
                && WkStringUtils.notEmpty(syncFormTo.getRequire())
                && WkStringUtils.isEmpty(syncFormTo.getWorkOnpgTo())) {
            this.syncMmsSyncFormCreateOnpgHelper.process(syncFormTo);
            return;
        }
        // ------------------
        // UPDATE 內容
        // ------------------
        else if (SyncMmsFormDataActionType.SAVE.equals(syncFormTo.getActionType())
                && WkStringUtils.notEmpty(syncFormTo.getRequire())
                && WkStringUtils.notEmpty(syncFormTo.getWorkOnpgTo())) {
            this.syncMmsSyncFormUpdateHelper.process(syncFormTo);
            return;
        }
        // ------------------
        // DELETE (取消) ON 程式單
        // ------------------
        else if (WkStringUtils.notEmpty(syncFormTo.getRequire())
                && WkStringUtils.notEmpty(syncFormTo.getWorkOnpgTo())
                && SyncMmsFormDataActionType.DELETE.equals(syncFormTo.getActionType())) {
            this.syncMmsSyncFormCancelHelper.process(syncFormTo);
            return;
        }
        // ------------------
        // 其他狀態
        // ------------------
        String errorMessage = "無法判斷執行類型，請聯繫資訊人員!";
        log.error(errorMessage);
        throw new TechRequestAlertException(errorMessage, syncFormTo.getLoginCompID());
    }

    /**
     * 回傳處理結果
     * 
     * @param syncMmsTokenModel
     * @param syncFormTo
     * @return
     * @throws SystemOperationException
     */
    private void syncProcessResult(
            SyncMmsTokenModel syncMmsTokenModel,
            SyncFormTo syncFormTo) throws SystemOperationException {

        // ====================================
        // 組裝處理結果MODEL
        // ====================================
        SyncMmsProcessResultModel processResult = new SyncMmsProcessResultModel();
        syncFormTo.setProcessResult(processResult);

        // 維護單ID
        processResult.setMmsID(syncFormTo.getMmsID());

        // 準備同步回MMS的處理結果欄位 (處理成功時)
        if (!syncFormTo.isError()) {
            this.prepareProcessResult(
                    syncFormTo,
                    processResult);
        }

        processResult.setProcessResult(syncFormTo.isError() ? "werp_fail" : "werp_success");

        // 取得錯誤訊息 (檢核成功時為空)
        if (syncFormTo.isError()) {
            String errorMessage = syncFormTo.getErrorInfosStr("<br/>");
            String tt = System.currentTimeMillis() + "";
            tt = tt.substring(tt.length() - 5);
            processResult.setErrorMessage(errorMessage + "[" + tt + "]");
        }

        // ====================================
        // 取得MMS API 參數
        // ====================================
        String hostDomain = this.workBackendParamService.findTextByKeyword(WkBackendParam.MMS_SYNC_API_DOMAIN);
        if (WkStringUtils.isEmpty(hostDomain)) {
            String errorMessage = "未設定系統參數：" + WkBackendParam.MMS_SYNC_API_DOMAIN;
            log.error(errorMessage);
            throw new TechRequestAlertException(errorMessage, syncMmsTokenModel.getCompID());
        }
        String portal = this.workBackendParamService.findTextByKeyword(WkBackendParam.MMS_SYNC_API_PORTAL);
        if (WkStringUtils.isEmpty(portal)) {
            String errorMessage = "未設定系統參數：" + WkBackendParam.MMS_SYNC_API_PORTAL;
            log.error(errorMessage);
            throw new TechRequestAlertException(errorMessage, syncMmsTokenModel.getCompID());
        }

        // ====================================
        // 將處理結果, 回傳給MMS
        // ====================================
        try {
            this.workSyncMmsClient.syncProcessResult(
                    hostDomain,
                    portal,
                    syncMmsTokenModel,
                    Lists.newArrayList(processResult));
        } catch (SystemOperationException e) {
            throw e;
        } catch (Exception e) {
            String errorMessage = "API 回傳處理結果失敗!" + e.getMessage();
            log.error(errorMessage, e);
            throw new TechRequestAlertException(errorMessage, syncMmsTokenModel.getCompID());
        }

    }

    /**
     * 更新主題欄位值
     * 
     * @param comValueMap  元件 map
     * @param requireTheme 主題字串
     * @param loginCompID  登入者公司ID (可為空)
     * @throws SystemOperationException
     */
    public void setRequireTheme(
            Map<String, ComBase> comValueMap,
            String requireTheme,
            String loginCompID) throws SystemOperationException {
        if (comValueMap == null) {
            log.error("傳入comValueMap為空!");
            return;
        }

        for (ComBase comBase : comValueMap.values()) {
            if (ComType.TEXT_TYPE_ONE.equals(comBase.getType())
                    && TemplateService.FIELD_NAME_THEME.equals(comBase.getName())) {
                ComTextTypeOne currCom = (ComTextTypeOne) comBase;
                currCom.setValue02(requireTheme);
                return;
            }
        }

        String errorMessage = "找不到需求單單據的『主題』欄位，請洽WERP人員";
        log.error(errorMessage);
        throw new TechRequestAlertException(errorMessage, loginCompID);
    }

    /**
     * 更新內容欄位值
     * 
     * @param comValueMap       元件 map
     * @param requireCssContent 內容字串 (含 HTML tag)
     * @param loginCompID       登入者公司ID (可為空)
     * @throws SystemOperationException 錯誤時拋出
     */
    public void setRequireContent(
            Map<String, ComBase> comValueMap,
            String requireCssContent,
            String loginCompID) throws SystemOperationException {
        if (comValueMap == null) {
            log.error("傳入comValueMap為空!");
            return;
        }

        for (ComBase comBase : comValueMap.values()) {
            if (ComType.EDITOR_TYPE_ONE.equals(comBase.getType())
                    && TemplateService.FIELD_NAME_CONTEXT.equals(comBase.getName())) {
                ComEditorTypeOne currCom = (ComEditorTypeOne) comBase;
                currCom.setValue01(requireCssContent);
                currCom.change();
                return;
            }
        }

        String errorMessage = "找不到需求單單據的『內容』欄位，請洽WERP人員";
        log.error(errorMessage);
        throw new TechRequestAlertException(errorMessage, loginCompID);
    }

    /**
     * 準備回傳資料
     * 
     * @param syncFormTo
     * @param processResult
     * @throws SystemOperationException
     */
    public void prepareProcessResult(
            SyncFormTo syncFormTo,
            SyncMmsProcessResultModel processResult) throws SystemOperationException {

        // ====================================
        // 防呆
        // ====================================
        List<String> errorMessages = Lists.newArrayList();

        if (syncFormTo.getRequire() == null) {
            errorMessages.add("需求單資料為空");
        }
        if (WkStringUtils.isEmpty(syncFormTo.getRequire().getRequireNo())) {
            errorMessages.add("未產生需求單號");
        }
        if (syncFormTo.getWorkOnpgTo() == null) {
            errorMessages.add("ON程式單單資料為空");
        }
        if (WkStringUtils.isEmpty(syncFormTo.getWorkOnpgTo().getOnpgNo())) {
            errorMessages.add("未產ON程式單號");
        }

        if (WkStringUtils.notEmpty(errorMessages)) {
            String errorMessage = "檢核失敗【" + errorMessages.stream().collect(Collectors.joining()) + "】";
            log.error(errorMessage);
            throw new TechRequestAlertException(errorMessage, syncFormTo.getLoginCompID());
        }

        // ====================================
        // 取得公司資料
        // ====================================
        Org comp = WkOrgCache.getInstance().findBySid(syncFormTo.getCreateUser().getPrimaryOrg().getCompanySid());
        if (comp == null) {
            String errorMessage = "找不到對應公司資料! createUser:[" + syncFormTo.getCreateUser().getSid() + "]";
            log.error(errorMessage);
            throw new TechRequestAlertException(errorMessage, syncFormTo.getLoginCompID());
        }

        // ====================================
        // 取得URL
        // ====================================
        String requireURL = this.urlService.buildRequireURLForMail(
                syncFormTo.getRequireNo(),
                comp.getId());

        String onpgURL = this.urlService.buildRequireURLWithTabForMail(
                syncFormTo.getRequireNo(),
                comp.getId(), URLServiceAttr.URL_ATTR_TAB_OP, syncFormTo.getWorkOnpgTo().getSid());

        // ====================================
        // 組裝處理結果資訊
        // ====================================
        // 需求單號
        processResult.setRequireNo(syncFormTo.getRequire().getRequireNo());
        // 需求單連結網址
        processResult.setRequireURL(requireURL);
        // on程式單號
        processResult.setOnpgNo(syncFormTo.getWorkOnpgTo().getOnpgNo());
        // on程式單連結網址
        processResult.setOnpgURL(onpgURL);

    }

}
