package com.cy.tech.request.logic.vo;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 分派/通知單位資訊容器
 * @author allen1214_wu
 */
@Getter
@Setter
@NoArgsConstructor
public class AssignSendDepsInfoVO implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -4179712211410741209L;
    /**
     * 
     */
    private String settingDepinfoTag;
    /**
     * 
     */
    private List<Integer> assignDepSids;
    /**
     * 分派單位設定資訊
     */
    private String assignDepInfo;
    /**
     * 
     */
    private String assignDepInfoForTooltip;
    /**
     * 通知單位設定資訊
     */
    private String sendDepInfo;
    /**
     * 
     */
    private String sendDepInfoForTooltip;
}
