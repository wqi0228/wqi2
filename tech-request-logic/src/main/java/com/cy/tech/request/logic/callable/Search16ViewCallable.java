/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.callable;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import com.cy.tech.request.logic.search.view.Search16View;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.utils.ToolsDate;
import com.cy.tech.request.logic.vo.SimpleDateFormatEnum;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.common.vo.value.to.JsonStringListTo;

import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Slf4j
public class Search16ViewCallable implements Callable<Search16View> {

    private URLService urlService;

    private WkJsonUtils jsonUtils;

    private SearchService searchHelper;

    private Map<Integer, String> customerNameMapByCustId;

    private Object[] record;

    private Integer execUserSid;

    private int index;

    public Search16ViewCallable(Object[] record, Integer execUserSid, URLService urlService,
            WkJsonUtils jsonUtils, SearchService searchHelper,
            Map<Integer, String> customerNameMapByCustId, int index) {
        this.record = record;
        this.execUserSid = execUserSid;
        this.urlService = urlService;
        this.jsonUtils = jsonUtils;
        this.searchHelper = searchHelper;
        this.customerNameMapByCustId = customerNameMapByCustId;
        this.index = index;
    }

    @Override
    public Search16View call() throws Exception {

        Search16View v = new Search16View();

        int idx = 0;
        String sid = (String) record[idx++];
        String requireNo = (String) record[idx++];
        String requireTheme = (String) record[idx++];
        Integer urgency = (Integer) record[idx++];
        Date createdDate = (Date) record[idx++];
        Integer createDep = (Integer) record[idx++];
        Integer createdUserSid = (Integer) record[idx++];
        String onPgTheme = (String) record[idx++];
        String bigName = (String) record[idx++];
        String middleName = (String) record[idx++];
        String smallName = (String) record[idx++];
        Integer requireDepSid = (Integer) record[idx++];
        Integer requireUserSid = (Integer) record[idx++];
        String onPgStatus = (String) record[idx++];
        String readReason = (String) record[idx++];
        String onPgDescription = (String) record[idx++];
        Date establishDate = (Date) record[idx++];
        Date finishDate = (Date) record[idx++];
        Date updateDate = (Date) record[idx++];
        String onPgNo = (String) record[idx++];
        String noticeDeps = (String) record[idx++];
        Date requireCreatedDate = (Date) record[idx++];
        Integer customer = (Integer) record[idx++];
        Integer author = (Integer) record[idx++];
        String requireStatus = (String) record[idx++];
        // 處理共通欄位 (一定要擺在『取值』最後面, 否則 index 會不對)
        v.prepareCommonColumn(record, idx);

        v.setIndex(index);

        v.setSid(sid);
        v.setRequireNo(requireNo);
        v.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
        v.setUrgency(UrgencyType.values()[urgency]);
        v.setCreatedDate(createdDate);
        v.setCreatedDateStr(ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateTime.getValue(), createdDate));
        v.setCreateDep(createDep);
        v.setCreateDepName(WkOrgUtils.findNameBySid(createDep));
        v.setCreatedUser(createdUserSid);
        v.setCreatedUserName(WkUserUtils.findNameBySid(createdUserSid));
        v.setOnPgTheme(onPgTheme);
        v.setBigName(bigName);
        v.setMiddleName(middleName);
        v.setSmallName(smallName);
        v.setRequireDep(requireDepSid);
        v.setRequireDepName(WkOrgUtils.findNameBySid(requireDepSid));
        v.setRequireUser(requireUserSid);
        v.setRequireUserName(WkUserUtils.findNameBySid(requireUserSid));
        v.setOnPgStatus(WorkOnpgStatus.valueOf(onPgStatus));
        v.setWaitReadReasonType(WaitReadReasonType.safeValueOf(readReason));
        v.setOnPgDescription(onPgDescription);
        v.setEstablishDate(establishDate);
        v.setEstablishDateStr(ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), establishDate));
        v.setEstablishDateSortStr(ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateNoLine.getValue(), establishDate));
        v.setFinishDate(finishDate);
        v.setFinishDateStr(ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), finishDate));
        v.setUpdateDate(updateDate);
        v.setUpdateDateStr(ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), updateDate));

        v.setOnpgNo(onPgNo);
        try {
            v.setNoticeDeps(jsonUtils.fromJson(noticeDeps, JsonStringListTo.class));
            List<String> noticeDepSids = v.getNoticeDeps().getValue();
            if (WkStringUtils.notEmpty(noticeDepSids)) {
                v.setNoticeDepsName(WkOrgUtils.findNameBySidStrs(noticeDepSids, "、"));
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        v.setRequireCreatedDate(requireCreatedDate);

        if (customer != null) {
            v.setCustomer(customer.longValue());
            v.setCustomerName(this.customerNameMapByCustId.get(customer.intValue()));
        }

        if (author != null) {
            v.setAuthor(author.longValue());
            v.setAuthorName(this.customerNameMapByCustId.get(author.intValue()));
        }
        v.setRequireStatus(RequireStatusType.valueOf(requireStatus));
        v.setLocalUrlLink(urlService.createLocalUrlLinkParamForTab(
                URLService.URLServiceAttr.URL_ATTR_M,
                urlService.createSimpleUrlTo(execUserSid, v.getRequireNo(), 1),
                URLService.URLServiceAttr.URL_ATTR_TAB_OP, v.getSid()));

        return v;
    }

}
