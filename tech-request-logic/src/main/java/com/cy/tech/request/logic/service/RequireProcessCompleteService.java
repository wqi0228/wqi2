package com.cy.tech.request.logic.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.helper.systemnotify.SysNotifyHelper;
import com.cy.tech.request.logic.service.helper.RequireFinishHelper;
import com.cy.tech.request.logic.service.onpg.OnpgService;
import com.cy.tech.request.logic.service.othset.OthSetService;
import com.cy.tech.request.logic.service.pt.PtService;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepService;
import com.cy.tech.request.logic.utils.ProcessLog;
import com.cy.tech.request.repository.require.ReqModifyRepo;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.OthSetStatus;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireFinishCodeType;
import com.cy.tech.request.vo.enums.RequireFinishMethodType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireConfirmDep;
import com.cy.tech.request.vo.require.RequireTrace;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * 需求製作進度 - 完成
 * 
 * @author allen1214_wu
 */
@Slf4j
@Component
public class RequireProcessCompleteService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5066759526302717798L;
    // =======================================================================================
    // 服務物件區
    // =======================================================================================
    @Autowired
    private transient PtService ptService;
    @Autowired
    private transient OnpgService onpgService;
    @Autowired
    private transient RequireTraceService traceService;
    @Autowired
    private transient RequireConfirmDepService requireConfirmDepService;
    @Autowired
    private transient RequireFinishHelper reqFinishHelper;
    @Autowired
    private transient RequireService requireService;
    @Autowired
    private transient RequireShowService reqShowService;
    @Autowired
    private transient ReqModifyRepo reqModifyDao;
    @Autowired
    private transient OthSetService othSetService;
    @Autowired
    transient private RequireProcessCloseService requireProcessCloseService;
    @Autowired
    transient private SysNotifyHelper sysNotifyHelper;
    @Autowired
    transient private RequireReadRecordHelper requireReadRecordHelper;

    // =======================================================================================
    // 執行完成方法區
    // =======================================================================================
    /**
     * 執行強制需求單完成
     *
     * @param require
     * @param execUser
     * @param completeMemoTrace
     * @param noticeMsg
     * @throws UserMessageException
     */
    @Transactional(rollbackFor = Exception.class)
    public void executeByForce(Require require,
            User execUser,
            RequireTrace completeMemoTrace) throws UserMessageException {

        Date sysDate = new Date();

        // ====================================
        // 檢查是否可『需求完成』
        // ====================================
        boolean isShowRequireCompleteBtn = reqShowService.showRequireCompleteBtn(
                requireService.findByReqObj(require),
                execUser);

        if (!isShowRequireCompleteBtn) {
            log.warn("執行強制完成，但單據狀態已異動 -> 畫面未更新?");
            throw new UserMessageException("單據狀態已異動, 請重新整理頁面", InfomationLevel.WARN);
        }

        // ====================================
        // start log
        // ====================================
        ProcessLog processLog = new ProcessLog(require.getRequireNo(), "", "強制需求完成");
        log.info(processLog.prepareStartLog());

        // ====================================
        // 強制結子單
        // ====================================
        this.process_ForceCloseSubCase(require, execUser, sysDate);

        // ====================================
        // 將未確認的需求完成單位狀態, 改為無須確認
        // ====================================
        this.requireConfirmDepService.changeProgStatusToPassByForceComplete(
                require.getRequireNo(),
                require.getSid(),
                execUser.getSid(),
                sysDate);

        // ====================================
        // 組追蹤訊息內容
        // ====================================
        String completeTraceMesssage = this.reqFinishHelper.prepareForceCompleteTraceMesssage(require, execUser, sysDate);

        // ====================================
        // 執行需求完成
        // ====================================
        this.process(
                require,
                execUser,
                sysDate,
                completeTraceMesssage,
                completeTraceMesssage.contains("尚未") ? RequireFinishCodeType.HALF_COMPLETE : RequireFinishCodeType.COMPLETE,
                RequireFinishMethodType.MANUAL);

        // ====================================
        // 新增追蹤：需求完成資訊補充
        // ====================================
        if (WkStringUtils.notEmpty(completeMemoTrace.getRequireTraceContent())) {
            completeMemoTrace.setCreatedDate(sysDate);
            completeMemoTrace.setRequireTraceType(RequireTraceType.REQUIRE_FINISH_MEMO);
            traceService.save(completeMemoTrace);

            require.setReadReason(ReqToBeReadType.NEW_COMPLETED_INFO_MEMO);
            reqModifyDao.save(require);
        }

        log.info(processLog.prepareCompleteLog());
    }

    /**
     * 執行需求完成 by 所有被分派(收束到部)單位確認完成 (歸類為自動結案)<br/>
     * 符合以下條件才執行<br/>
     * &nbsp;1.製作進度需為 進行中 (RequireStatusType.PROCESS)<br/>
     * &nbsp;2.所有存在的需求確認單位檔均已跑完確認流程
     * 
     * @param requireSid
     * @param execUser
     * @param sysDate
     * @param isRemoveAssign 有執行減派
     * @param isBatchTrans   為批次轉檔
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean executeByAllDepConfirm(
            String requireSid,
            User execUser,
            Date sysDate,
            boolean isRemoveAssign,
            boolean isBatchTrans) {

        // ====================================
        // 檢查主檔狀態
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);
        if (require == null || !RequireStatusType.PROCESS.equals(require.getRequireStatus())) {
            return false;
        }

        // ====================================
        // 查詢現有的需求完成確認單位檔
        // ====================================
        List<RequireConfirmDep> requireConfirmDeps = this.requireConfirmDepService.findByRequireSid(require.getSid());
        if (requireConfirmDeps == null) {
            requireConfirmDeps = Lists.newArrayList();
        }

        // 檢核是否有未完成的執行單位
        boolean isUnfinished = requireConfirmDeps.stream()
                .anyMatch(each -> each.getProgStatus() != null
                        && !each.getProgStatus().isInFinish());

        if (isUnfinished) {
            if (isBatchTrans) {
                log.debug("[{}]：尚有需求完成確認單位未完成確認, 不執行需求完成!",
                        require.getRequireNo());
            }
            return false;
        } else {
            if (isBatchTrans) {
                log.info("[{}]：所有單位皆已確認完成，開始執行需求完成！",
                        require.getRequireNo());
            }
        }

        // ====================================
        // 組追蹤訊息內容
        // ====================================
        String completeTraceMesssage = this.reqFinishHelper.prepareRequireCompleteByAllDepConfirmTraceMesssage(require);
        if (isRemoveAssign) {
            completeTraceMesssage = execUser.getName() + "執行減派後<br/>" + completeTraceMesssage;
        }

        // ====================================
        // 執行需求完成
        // ====================================
        this.process(
                require,
                execUser,
                sysDate,
                completeTraceMesssage,
                completeTraceMesssage.contains("尚未") ? RequireFinishCodeType.HALF_COMPLETE : RequireFinishCodeType.COMPLETE,
                RequireFinishMethodType.AUTO);

        return true;
    }

    // =======================================================================================
    // 核心流程
    // =======================================================================================
    /**
     * 執行需求完成 (核心流程)
     * 
     * @param require
     * @param execUser
     * @param sysDate
     * @param traceMessage
     * @param requireFinishCodeType
     * @param requireFinishMethodType
     */
    @Transactional(rollbackFor = Exception.class)
    private void process(
            Require require,
            User execUser,
            Date sysDate,
            String traceMessage,
            RequireFinishCodeType requireFinishCodeType,
            RequireFinishMethodType requireFinishMethodType) {

        ProcessLog processLog = new ProcessLog(require.getRequireNo(), "", "需求完成");
        log.info(processLog.prepareStartLog());

        // ====================================
        // 新增追蹤：需求完成
        // ====================================
        // 準備資料
        RequireTrace traceByComplete = traceService.createNewTrace(require.getSid(), execUser, sysDate);

        // 組追蹤訊息內容
        traceByComplete.setRequireTraceContent(traceMessage);
        traceByComplete.setRequireTraceContentCss(traceMessage);

        // 追蹤類別:需求完成
        traceByComplete.setRequireTraceType(RequireTraceType.REQUIRE_FINISH);
        this.traceService.save(traceByComplete);

        // ====================================
        // 更新主單狀態
        // ====================================
        require.setFinishCode(requireFinishCodeType);
        require.setFinishMethod(requireFinishMethodType);
        require.setFinishDate(sysDate);
        require.setRequireFinishUsr(execUser);
        require.setRequireStatus(RequireStatusType.COMPLETED);
        require.setReadReason(ReqToBeReadType.COMPLETED);
        require.setHasTrace(Boolean.TRUE);

        reqModifyDao.updateReqComplete(
                require,
                require.getHasTrace(),
                require.getFinishCode(),
                require.getFinishMethod(),
                require.getFinishDate(),
                require.getRequireFinishUsr(),
                require.getRequireStatus(),
                require.getReadReason(),
                execUser,
                sysDate);

        require = this.requireService.findByReqSid(require.getSid());

        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.REQUIRE,
                require.getSid(),
                execUser.getSid());

        // ====================================
        // 需求完成後自動結案
        // ====================================
        boolean isClose = false;
        if (require.getMapping().getSmall().getCheckFinishAutoClosed()) {
            // 自動結案 (v7.0 取消技術主管簽核流程)
            this.requireProcessCloseService.executeByAutoClose(require, execUser);
            isClose = true;
        }

        // ====================================
        // 處理系統通知 (待結案)
        // ====================================
        // 若已自動結案不需發此通知
        if (!isClose) {
            try {
                this.sysNotifyHelper.processForWaitClose(require.getSid(), execUser.getSid());
            } catch (Exception e) {
                log.error("執行系統通知失敗!" + e.getMessage(), e);
            }
        }

        log.info(processLog.prepareCompleteLog());
    }

    /**
     * 強制結子單
     */
    @Transactional(rollbackFor = Exception.class)
    private void process_ForceCloseSubCase(
            Require require,
            User execUser,
            Date sysDate) {

        // ====================================
        // start log
        // ====================================
        ProcessLog processLog = new ProcessLog(require.getRequireNo(), "", "強制結子單");
        log.info(processLog.prepareStartLog());

        // ====================================
        // 原型確認單
        // ====================================
        // createDeps （傳入null時,代表不限定.該單據下子單全關）
        this.ptService.forceComplete(require, PtStatus.FORCE_CLOSE, execUser, sysDate, null);

        // ====================================
        // 送測單
        // ====================================
        // 暫不處理, 未得到 QA 單位回覆可以結掉

        // ====================================
        // ONPG單
        // ====================================
        // createDeps （傳入null時,代表不限定.該單據下子單全關）
        this.onpgService.forceComplete(require, WorkOnpgStatus.FORCE_CLOSE, execUser, sysDate, null);

        // ====================================
        // 其他設定資訊
        // ====================================
        // createDeps （傳入null時,代表不限定.該單據下子單全關）
        this.othSetService.forceComplete(require, OthSetStatus.FORCE_CLOSE, execUser, sysDate, null);

        // ====================================
        // complete log
        // ====================================
        log.info(processLog.prepareCompleteLog());
    }

}
