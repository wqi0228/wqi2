/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.tros;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.query.LogicQueryService;
import com.cy.tech.request.logic.vo.AttachmentVO;
import com.cy.tech.request.logic.vo.TrOsAttachmentVO;
import com.cy.tech.request.logic.vo.TrOsHistoryVO;
import com.cy.tech.request.logic.vo.TrOsReplyAndReplyVO;
import com.cy.tech.request.logic.vo.TrOsReplyVO;
import com.cy.tech.request.repository.require.RequireRepository;
import com.cy.tech.request.repository.require.tros.TrOsAttachmentRepository;
import com.cy.tech.request.repository.require.tros.TrOsHistoryRepository;
import com.cy.tech.request.repository.require.tros.TrOsReplyAndReplyRepository;
import com.cy.tech.request.repository.require.tros.TrOsReplyRepository;
import com.cy.tech.request.repository.require.tros.TrOsRepository;
import com.cy.tech.request.vo.enums.OthSetHistoryBehavior;
import com.cy.tech.request.vo.enums.OthSetStatus;
import com.cy.tech.request.vo.enums.tros.TrOsType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.tros.TrOs;
import com.cy.tech.request.vo.require.tros.TrOsHistory;
import com.cy.tech.request.vo.require.tros.TrOsReply;
import com.cy.tech.request.vo.require.tros.TrOsReplyAndReply;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkJsoupUtils;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * TrOsHistoryService
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class TrOsHistoryService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8662413384404113365L;
    /** LogicQueryService */
    @Autowired
    @Qualifier("logicQueryService")
    private LogicQueryService logicQueryService;
    /** TrOsRepository */
    @Autowired
    private TrOsRepository trOsRepository;
    /** WkJsoupUtils */
    @Autowired
    private WkJsoupUtils jsoupUtils;
    /** TrOsReplyRepository */
    @Autowired
    private TrOsReplyRepository trOsReplyRepository;
    /** TrOsHistoryRepository */
    @Autowired
    private TrOsHistoryRepository trOsHistoryRepository;
    /** TrOsAttachmentRepository */
    @Autowired
    private TrOsAttachmentRepository trOsAttachmentRepository;
    /** RequireRepository */
    @Autowired
    private RequireRepository requireRepository;
    /** OrganizationService */
    @Autowired
    private OrganizationService orgService;
    /** TrOsAlertService */
    @Autowired
    private TrOsAlertService trOsAlertService;
    /** TrOsReplyAndReplyRepository */
    @Autowired
    private TrOsReplyAndReplyRepository trOsReplyAndReplyRepository;

    /**
     * 建立完成Hitory
     *
     * @param os_sid 其他設定資訊Sid
     * @param os_no 其他設定資訊單號
     * @param require_sid 需求單Sid
     * @param require_no 需求單單號
     * @param reply_person_dep 執行完成者部門Sid
     * @param reply_person_sid 執行完成者Sid
     * @param reply_content 回覆內容
     * @param attachmentVOs 附件
     * @return
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public TrOsHistory createFinishHistory(String os_sid, String os_no, String require_sid,
            String require_no, Integer reply_person_dep,
            Integer reply_person_sid, String reply_content,
            List<AttachmentVO> attachmentVOs) {
        Require require = requireRepository.findOne(require_sid);
        TrOs trOs = trOsRepository.findOne(os_sid);
        Preconditions.checkState(require != null, "需求單取得失敗！");
        Preconditions.checkState(trOs != null, "其他設定資訊取得失敗！");
        boolean statusFinished = (trOs.getOsStatus().equals(OthSetStatus.FINISH) && require.getCloseCode());
        Preconditions.checkState(!statusFinished, "其他設定資訊已完成！");
        Preconditions.checkState(trOs.getOsStatus().equals(OthSetStatus.PROCESSING), "其他設定資訊非進行中不可進行完成！");
        User finishUser = WkUserCache.getInstance().findBySid(reply_person_sid);
        //需求單位向上層級的所有主管
        List<User> fillDocUnitManagers = orgService.findOrgManagers(WkOrgCache.getInstance().findBySid(require.getCreateDep().getSid()));
        List<String> notify = trOs.getNoticeDeps().getValue();
        String createReqDepSid = String.valueOf(require.getCreateDep().getSid());
        String loginDepSid = String.valueOf(finishUser.getPrimaryOrg().getSid());
        boolean permission = ((fillDocUnitManagers.contains(finishUser) || loginDepSid.equals(createReqDepSid)) || notify.contains(loginDepSid));
        //通知單位執行 且 通知單位中有需求單位,需求單位成員不可執行此功能
        Preconditions.checkState(permission, "無權限進行操作！");
        trOs.setFinishDate(new Date());
        trOs.setOsStatus(OthSetStatus.FINISH);
        trOs.setUpdate_usr(reply_person_sid);
        trOs.setUpdatedDate(new Date());
        trOsRepository.save(trOs);
        TrOsHistory resultTrOsHistory = this.createReplyHistory(os_sid, os_no,
                require_sid, require_no, reply_person_dep, reply_person_sid,
                reply_content, attachmentVOs, OthSetHistoryBehavior.FINISH_OS);
        List<Integer> noticeDeps = Lists.newArrayList();
        if (trOs.getNoticeDeps().getValue() != null && !trOs.getNoticeDeps().getValue().isEmpty()) {
            trOs.getNoticeDeps().getValue().forEach(item -> {
                noticeDeps.add(Integer.valueOf(item));
            });

        }

        trOsAlertService.createFinishAlert(os_sid, os_no, require_sid,
                require_no, trOs.getTheme(), noticeDeps, reply_person_sid);
        return resultTrOsHistory;
    }

    /**
     * 建立回覆的回覆Hitory
     *
     * @param os_sid 其他設定資訊Sid
     * @param os_reply_sid 回覆Sid
     * @param reply_person_dep 回覆的回覆的使用者部門Sid
     * @param reply_person_sid 回覆的回覆的使用者Sid
     * @param reply_content 回覆的回覆的內容
     * @param attachmentVOs 附件
     * @return
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public TrOsHistory createReplyAndReplyHistory(String os_sid, String os_reply_sid, Integer reply_person_dep,
            Integer reply_person_sid, String reply_content,
            List<AttachmentVO> attachmentVOs) {
        TrOs trOs = trOsRepository.findOne(os_sid);
        Preconditions.checkState(trOs != null, "其他設定資訊取得失敗！");
        TrOsReply trOsReply = trOsReplyRepository.findOne(os_reply_sid);
        Preconditions.checkState(trOsReply != null, "回覆物件取得失敗！");
        TrOsHistory resultTrOsHistory = this.doCreateReplyAndReplyHistory(os_sid, trOsReply.getOs_no(),
                trOsReply.getRequire_sid(), trOsReply.getRequire_no(), trOsReply.getSid(), reply_person_dep, reply_person_sid,
                reply_content, attachmentVOs, OthSetHistoryBehavior.REPLY_AND_REPLY);

        trOsAlertService.createReplyAndReplyAlert(trOsReply.getOs_sid(), trOsReply.getOs_no(),
                trOsReply.getRequire_sid(), trOsReply.getRequire_no(), trOs.getTheme(), reply_person_sid,
                trOsReply.getReply_person_sid(), trOsReply.getSid(),
                resultTrOsHistory.getOs_reply_and_reply_sid());
        return resultTrOsHistory;
    }

    /**
     * 建立取消History
     *
     * @param os_sid 其他設定資訊Sid
     * @param os_no 其他設定資訊單號
     * @param require_sid 需求單Sid
     * @param require_no 需求單單號
     * @param reply_person_dep 執行取消者部門Sid
     * @param reply_person_sid 執行取消者Sid
     * @param reply_content 回覆內容
     * @param attachmentVOs 附件
     * @return
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public TrOsHistory createCancelHistory(String os_sid, String os_no, String require_sid,
            String require_no, Integer reply_person_dep,
            Integer reply_person_sid, String reply_content,
            List<AttachmentVO> attachmentVOs) {
        Preconditions.checkState(!Strings.isNullOrEmpty(jsoupUtils.clearCssTag(reply_content)), "請輸入取消原因！！");
        //檢測取消狀態
        Require require = requireRepository.findOne(require_sid);
        TrOs trOs = trOsRepository.findOne(os_sid);
        Preconditions.checkState(require != null, "需求單取得失敗！");
        Preconditions.checkState(trOs != null, "其他設定資訊取得失敗！");
        boolean statusCanceled = (trOs.getOsStatus().equals(OthSetStatus.INVALID) && require.getCloseCode());
        Preconditions.checkState(!statusCanceled, "其他設定資訊已作廢！");
        Preconditions.checkState(trOs.getOsStatus().equals(OthSetStatus.PROCESSING), "其他設定資訊非進行中不可進行取消！");
        User createUser = WkUserCache.getInstance().findBySid(trOs.getCreate_usr());
        List<User> canClickDisableList = orgService.findOrgManagers(WkOrgCache.getInstance().findBySid(createUser.getPrimaryOrg().getSid()));
        if (!canClickDisableList.contains(createUser)) {
            canClickDisableList.add(createUser);
        }
        User cancelUser = WkUserCache.getInstance().findBySid(reply_person_sid);
        Preconditions.checkState(canClickDisableList.contains(cancelUser), "無權限進行操作！");
        trOs.setCancelDate(new Date());
        trOs.setOsStatus(OthSetStatus.INVALID);
        trOs.setUpdate_usr(reply_person_sid);
        trOs.setUpdatedDate(new Date());
        trOsRepository.save(trOs);
        TrOsHistory resultTrOsHistory = this.createReplyHistory(os_sid, os_no,
                require_sid, require_no, reply_person_dep, reply_person_sid,
                reply_content, attachmentVOs, OthSetHistoryBehavior.CANCEL_OS);
        List<Integer> noticeDeps = Lists.newArrayList();
        if (trOs.getNoticeDeps().getValue() != null && !trOs.getNoticeDeps().getValue().isEmpty()) {
            trOs.getNoticeDeps().getValue().forEach(item -> {
                noticeDeps.add(Integer.valueOf(item));
            });

        }

        trOsAlertService.createCancelAlert(os_sid, os_no, require_sid,
                require_no, trOs.getTheme(), noticeDeps, reply_person_sid);

        //osaService.createCancelAlert(historyDao.findOne(resultTrOsHistory.getSid()));
        return resultTrOsHistory;
    }

    /**
     * 修改回覆
     *
     * @param updateUserSid 更新者Sid
     * @param historySid HistorySid
     * @param replySid 回覆Sid
     * @param content 內容
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public void modifyReplyHistory(Integer updateUserSid, String historySid, String replySid, String content) {
        trOsHistoryRepository.updateTrOsReplyContent(updateUserSid, new Date(), historySid);
        trOsReplyRepository.updateTrOsReplyContent(new Date(), jsoupUtils.clearCssTag(content), content, replySid);
    }

    /**
     * 修改回覆的回覆
     *
     * @param updateUserSid 更新者Sid
     * @param historySid HistorySid
     * @param replyAndReplySid 回覆的回覆Sid
     * @param content 內容
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public void modifyReplyAndReplyHistory(Integer updateUserSid, String historySid, String replyAndReplySid, String content) {
        trOsHistoryRepository.updateTrOsReplyContent(updateUserSid, new Date(), historySid);
        trOsReplyAndReplyRepository.updateTrOsReplyAndReplyContent(new Date(), jsoupUtils.clearCssTag(content), content, replyAndReplySid);
    }

    /**
     * 建立回覆的History
     *
     * @param os_sid 其他設定資訊Sid
     * @param os_no 其他設定資訊單號
     * @param require_sid 需求單Sid
     * @param require_no 需求單單號
     * @param reply_person_dep 回覆者部門Sid
     * @param reply_person_sid 回覆者Sid
     * @param reply_content 回覆內容
     * @param attachmentVOs 附件
     * @param othSetHistoryBehavior 行為
     * @return
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public TrOsHistory createReplyHistory(String os_sid, String os_no, String require_sid,
            String require_no, Integer reply_person_dep,
            Integer reply_person_sid, String reply_content, List<AttachmentVO> attachmentVOs,
            OthSetHistoryBehavior othSetHistoryBehavior) {

        TrOsReply trOsReply = new TrOsReply();
        trOsReply.setOs_sid(os_sid);
        trOsReply.setOs_no(os_no);
        trOsReply.setRequire_sid(require_sid);
        trOsReply.setRequire_no(require_no);
        trOsReply.setReply_content_css(reply_content);
        trOsReply.setReply_content(jsoupUtils.clearCssTag(reply_content));
        trOsReply = this.doCreateTrOsReply(trOsReply, reply_person_dep, reply_person_sid);
        TrOs trOs = trOsRepository.findOne(os_sid);
        TrOsHistory trOsHistory = new TrOsHistory();
        trOsHistory.setOs_sid(os_sid);
        trOsHistory.setOs_no(os_no);
        trOsHistory.setRequire_sid(require_sid);
        trOsHistory.setRequire_no(require_no);
        if (OthSetHistoryBehavior.CANCEL_OS.equals(othSetHistoryBehavior) || OthSetHistoryBehavior.FINISH_OS.equals(othSetHistoryBehavior)) {
            trOsHistory.setInputInfo(jsoupUtils.clearCssTag(reply_content));
            trOsHistory.setInputInfoCss(reply_content);
        }
        trOsHistory.setBehavior(othSetHistoryBehavior);
        trOsHistory.setBehaviorStatus(trOs.getOsStatus());
        trOsHistory.setOs_reply_sid(trOsReply.getSid());
        trOsHistory = doCreateTrOsHistory(trOsHistory, reply_person_sid);
        if (attachmentVOs != null && !attachmentVOs.isEmpty()) {
            String historySid = trOsHistory.getSid();
            attachmentVOs.forEach(item -> {
                trOsAttachmentRepository.updateTrOsAttachmentMappingInfo(os_sid, os_no, require_sid, require_no,
                        historySid, item.getAttDesc(), reply_person_dep, reply_person_sid, new Date(), item.getAttSid());
            });
        }
        return trOsHistory;
    }

    /**
     * 執行建立回覆的回覆History
     *
     * @param os_sid 其他設定資訊Sid
     * @param os_no 其他設定資訊單號
     * @param require_sid 需求單Sid
     * @param require_no 需求單單號
     * @param os_reply_sid 回覆Sid
     * @param reply_person_dep 回覆的回覆的使用者部門Sid
     * @param reply_person_sid 回覆的回覆的使用者Sid
     * @param reply_content 回覆的回覆內容
     * @param attachmentVOs 附件
     * @param othSetHistoryBehavior 行為
     * @return
     */
    private TrOsHistory doCreateReplyAndReplyHistory(String os_sid, String os_no, String require_sid,
            String require_no, String os_reply_sid, Integer reply_person_dep,
            Integer reply_person_sid, String reply_content, List<AttachmentVO> attachmentVOs,
            OthSetHistoryBehavior othSetHistoryBehavior) {
        TrOsReplyAndReply trOsReplyAndReply = new TrOsReplyAndReply();
        trOsReplyAndReply.setOs_sid(os_sid);
        trOsReplyAndReply.setOs_no(os_no);
        trOsReplyAndReply.setRequire_sid(require_sid);
        trOsReplyAndReply.setRequire_no(require_no);
        trOsReplyAndReply.setOs_reply_sid(os_reply_sid);
        trOsReplyAndReply.setReply_content_css(reply_content);
        trOsReplyAndReply.setReply_content(jsoupUtils.clearCssTag(reply_content));
        trOsReplyAndReply = this.doCreateTrOsReplyAndReply(trOsReplyAndReply, reply_person_dep, reply_person_sid);
        TrOs trOs = trOsRepository.findOne(os_sid);
        TrOsHistory trOsHistory = new TrOsHistory();
        trOsHistory.setOs_sid(os_sid);
        trOsHistory.setOs_no(os_no);
        trOsHistory.setRequire_sid(require_sid);
        trOsHistory.setRequire_no(require_no);
        trOsHistory.setBehavior(othSetHistoryBehavior);
        trOsHistory.setBehaviorStatus(trOs.getOsStatus());
        trOsHistory.setOs_reply_and_reply_sid(trOsReplyAndReply.getSid());
        trOsHistory = doCreateTrOsHistory(trOsHistory, reply_person_sid);
        if (attachmentVOs != null && !attachmentVOs.isEmpty()) {
            String historySid = trOsHistory.getSid();
            attachmentVOs.forEach(item -> {
                trOsAttachmentRepository.updateTrOsAttachmentMappingInfo(os_sid, os_no, require_sid, require_no,
                        historySid, item.getAttDesc(), reply_person_dep, reply_person_sid, new Date(), item.getAttSid());
            });
        }
        return trOsHistory;
    }

    /**
     * 執行建立TrOsHistory
     *
     * @param trOsHistory TrOsHistory物件
     * @param createUserSid 建立者Sid
     * @return
     */
    private TrOsHistory doCreateTrOsHistory(TrOsHistory trOsHistory, Integer createUserSid) {
        trOsHistory.setStatus(Activation.ACTIVE);
        trOsHistory.setCreate_usr(createUserSid);
        trOsHistory.setCreate_dt(new Date());
        trOsHistory.setUpdate_usr(createUserSid);
        trOsHistory.setUpdate_dt(new Date());
        return trOsHistoryRepository.save(trOsHistory);
    }

    /**
     * 執行建立TrOsReply
     *
     * @param trOsReply TrOsReply物件
     * @param reply_person_dep 建立者部門Sid
     * @param reply_person_sid 建立者Sid
     * @return
     */
    private TrOsReply doCreateTrOsReply(TrOsReply trOsReply, Integer reply_person_dep,
            Integer reply_person_sid) {
        trOsReply.setReply_person_dep(reply_person_dep);
        trOsReply.setReply_person_sid(reply_person_sid);
        trOsReply.setReply_dt(new Date());
        trOsReply.setReply_udt(new Date());
        return trOsReplyRepository.save(trOsReply);
    }

    /**
     * 執行建立TrOsReplyAndReply
     *
     * @param trOsReplyAndReply TrOsReplyAndReply物件
     * @param reply_person_dep 建立者部門Sid
     * @param reply_person_sid 建立者Sid
     * @return
     */
    private TrOsReplyAndReply doCreateTrOsReplyAndReply(TrOsReplyAndReply trOsReplyAndReply, Integer reply_person_dep,
            Integer reply_person_sid) {
        trOsReplyAndReply.setReply_person_dep(reply_person_dep);
        trOsReplyAndReply.setReply_person_sid(reply_person_sid);
        trOsReplyAndReply.setReply_dt(new Date());
        trOsReplyAndReply.setReply_udt(new Date());
        return trOsReplyAndReplyRepository.save(trOsReplyAndReply);
    }

    /**
     * 取得HistoryList By request_Sid And s_os_sid
     *
     * @param request_Sid 需求單Sid
     * @param s_os_sid 其他設定資訊Sid
     * @return
     */
    public List<TrOsHistoryVO> getTrOsByRequestSid(String request_Sid, String s_os_sid) {
        List<TrOsHistoryVO> trOsVOs = Lists.newArrayList();
        try {
            Map<String, Object> parameters = Maps.newHashMap();
            StringBuilder builder = new StringBuilder();
            //其它設定資訊 回覆
            builder.append("SELECT ");
            builder.append("'" + TrOsType.tr_os_reply.name() + "' as typeName,"
                    + " tor.os_reply_sid as sid,"
                    + " tor.os_reply_sid as os_reply_sid,"
                    + " '' as os_reply_and_reply_sid,"
                    + " '' as os_history_sid,"
                    + " tor.os_sid as os_sid,"
                    + " tor.os_no as os_no,"
                    + " tor.require_sid as require_sid,"
                    + " tor.require_no as require_no,"
                    + " tor.reply_person_dep as reply_person_dep,"
                    + " tor.reply_person_sid as reply_person_sid,"
                    + " tor.reply_dt as reply_dt,"
                    + " tor.reply_udt as reply_udt,"
                    + " tor.reply_content as content,"
                    + " tor.reply_content_css as content_css,"
                    + " '' as behavior,"
                    + " '' as behavior_status,"
                    + " null as create_usr,"
                    + " null as create_dt,"
                    + " null as update_usr,"
                    + " null as update_dt,"
                    + "'' as file_name,"
                    + "'' as description"
                    + " FROM tr_os_reply tor "
                    + " WHERE tor.require_sid = '" + request_Sid + "' and tor.os_sid = '" + s_os_sid + "' ");
            builder.append(" UNION ALL ");
            //其它設定資訊 回覆 的回覆
            builder.append("SELECT "
                    + "'" + TrOsType.tr_os_reply_and_reply.name() + "' as typeName,"
                    + "torr.os_reply_and_reply_sid as sid,"
                    + "torr.os_reply_sid as os_reply_sid,"
                    + "torr.os_reply_and_reply_sid as os_reply_and_reply_sid,"
                    + "'' as os_history_sid,"
                    + "torr.os_sid as os_sid,"
                    + "torr.os_no as os_no,"
                    + "torr.require_sid as require_sid,"
                    + "torr.require_no as require_no,"
                    + "torr.reply_person_dep as reply_person_dep,"
                    + "torr.reply_person_sid as reply_person_sid,"
                    + "torr.reply_dt as reply_dt,"
                    + "torr.reply_udt as reply_udt,"
                    + "torr.reply_content as content,"
                    + "torr.reply_content_css as content_css,"
                    + " '' as behavior,"
                    + " '' as behavior_status,"
                    + " null as create_usr,"
                    + " null as create_dt,"
                    + " null as update_usr,"
                    + " null as update_dt,"
                    + "'' as file_name,"
                    + "'' as description "
                    + " FROM tr_os_reply_and_reply torr "
                    + " WHERE torr.require_sid = '" + request_Sid + "' and torr.os_sid = '" + s_os_sid + "' ");
            builder.append(" UNION ALL ");
            //其它設定資訊 歷程
            builder.append(" SELECT "
                    + "'" + TrOsType.tr_os_history.name() + "' as typeName,"
                    + "toh.os_history_sid as sid,"
                    + "toh.os_reply_sid as os_reply_sid,"
                    + "toh.os_reply_and_reply_sid as os_reply_and_reply_sid,"
                    + "toh.os_history_sid as os_history_sid,"
                    + "toh.os_sid as os_sid,"
                    + "toh.os_no as os_no,"
                    + "toh.require_sid as require_sid,"
                    + "toh.require_no as require_no,"
                    + "null as reply_person_dep,"
                    + "null as reply_person_sid,"
                    + "null as reply_dt,"
                    + "null as reply_udt,"
                    + "toh.input_info as content,"
                    + "toh.input_info_css as content_css,"
                    + "toh.behavior as behavior,"
                    + "toh.behavior_status as behavior_status,"
                    + "toh.create_usr as create_usr,"
                    + "toh.create_dt as create_dt,"
                    + "toh.update_usr as update_usr,"
                    + "toh.update_dt as update_dt,"
                    + "'' as file_name,"
                    + "'' as description"
                    + " FROM tr_os_history toh"
                    + " WHERE toh.require_sid = '" + request_Sid + "' and toh.os_sid = '" + s_os_sid + "'");
            builder.append(" UNION ALL ");
            //其它設定資訊 歷程附件
            builder.append(" SELECT "
                    + "'" + TrOsType.tr_os_attachment.name() + "' as typeName,"
                    + "toa.os_attachment_sid as sid,"
                    + "'' as os_reply_sid,"
                    + "'' as os_reply_and_reply_sid,"
                    + "toa.os_history_sid as os_history_sid,"
                    + "toa.os_sid as os_sid,"
                    + "toa.os_no as os_no,"
                    + "toa.require_sid as require_sid,"
                    + "toa.require_no as require_no,"
                    + "toa.department as reply_person_dep,"
                    + "null as reply_person_sid,"
                    + "null as reply_dt,"
                    + "null as reply_udt,"
                    + "null as content,"
                    + "null as content_css,"
                    + "toa.behavior as behavior,"
                    + " '' as behavior_status,"
                    + "toa.create_usr as create_usr,"
                    + "toa.create_dt as create_dt,"
                    + "toa.update_usr as update_usr,"
                    + "toa.update_dt as update_dt,"
                    + "toa.file_name as file_name,"
                    + "toa.description as description"
                    + " FROM tr_os_attachment toa "
                    + " WHERE toa.require_sid = '" + request_Sid + "' and toa.status = 0 and toa.os_sid = '" + s_os_sid + "' ");
            
            @SuppressWarnings("rawtypes")
            List result = logicQueryService.findWithQuery(builder.toString(), parameters);
            List<TrOsAttachmentVO> trOsAttachmentVOs = Lists.newArrayList();
            List<TrOsReplyAndReplyVO> trOsReplyAndReplyVOs = Lists.newArrayList();
            List<TrOsReplyVO> trOsReplyVOs = Lists.newArrayList();
            List<TrOsHistoryVO> trOsHistoryVOs = Lists.newArrayList();

            for (int i = 0; i < result.size(); i++) {
                try {
                    Object[] record = (Object[]) result.get(i);
                    String typeNameStr = (String) record[0];
                    TrOsType typeName = TrOsType.valueOf(typeNameStr);
                    String sid = (String) record[1];
                    String os_reply_sid = "";
                    if (record[2] != null) {
                        os_reply_sid = (String) record[2];
                    }
                    String os_reply_and_reply_sid = "";
                    if (record[3] != null) {
                        os_reply_and_reply_sid = (String) record[3];
                    }
                    String os_history_sid = "";
                    if (record[4] != null) {
                        os_history_sid = (String) record[4];
                    }
                    String os_sid = (String) record[5];
                    String os_no = (String) record[6];
                    String require_sid = (String) record[7];
                    String require_no = (String) record[8];
                    Integer reply_person_dep = null;
                    if (record[9] != null) {
                        reply_person_dep = (Integer) record[9];
                    }
                    Integer reply_person_sid = null;
                    if (record[10] != null) {
                        reply_person_sid = (Integer) record[10];
                    }
                    Date reply_dt = null;
                    if (record[11] != null) {
                        reply_dt = (Date) record[11];
                    }
                    Date reply_udt = null;
                    if (record[12] != null) {
                        reply_udt = (Date) record[12];
                    }
                    String content = "";
                    if (record[13] != null) {
                        content = (String) record[13];
                    }
                    String content_css = "";
                    if (record[14] != null) {
                        byte[] msageBytpe = (byte[]) record[14];
                        content_css = msageBytpe == null ? "" : new String(msageBytpe, "UTF-8");
                    }
                    String behaviorStr = (String) record[15];
                    OthSetHistoryBehavior othSetHistoryBehavior = null;
                    if (!Strings.isNullOrEmpty(behaviorStr) && !typeName.equals(TrOsType.tr_os_attachment)) {
                        try {
                            othSetHistoryBehavior = OthSetHistoryBehavior.valueOf(behaviorStr);
                        } catch (Exception e) {
                            log.error("主檔上傳的附件", e);
                        }
                    }
                    String behavior_statusStr = (String) record[16];
                    OthSetStatus othSetStatus = null;
                    if (!Strings.isNullOrEmpty(behavior_statusStr)) {
                        othSetStatus = OthSetStatus.valueOf(behavior_statusStr);
                    }
                    Integer create_usr = null;
                    if (record[17] != null) {
                        create_usr = (Integer) record[17];
                    }
                    Date create_dt = null;
                    if (record[18] != null) {
                        create_dt = (Date) record[18];
                    }
                    Integer update_usr = null;
                    if (record[19] != null) {
                        update_usr = (Integer) record[19];
                    }
                    Date update_dt = null;
                    if (record[20] != null) {
                        update_dt = (Date) record[20];
                    }
                    String file_name = (String) record[21];
                    String description = (String) record[22];
                    if (typeName.equals(TrOsType.tr_os_attachment)) {
                        TrOsAttachmentVO tv = new TrOsAttachmentVO(sid, os_history_sid, os_sid, os_no, require_sid, require_no, reply_person_dep,
                                othSetHistoryBehavior, create_usr, create_dt, update_usr, update_dt, file_name, description);
                        trOsAttachmentVOs.add(tv);
                    } else if (typeName.equals(TrOsType.tr_os_reply_and_reply)) {
                        TrOsReplyAndReplyVO trv = new TrOsReplyAndReplyVO(sid, os_reply_sid, os_history_sid, os_sid, os_no,
                                require_sid, require_no, reply_person_dep, reply_person_sid, reply_dt, reply_udt, content, content_css, Lists.newArrayList());
                        trOsReplyAndReplyVOs.add(trv);
                    } else if (typeName.equals(TrOsType.tr_os_reply)) {
                        TrOsReplyVO tor = new TrOsReplyVO(sid, os_history_sid, os_sid, os_no, require_sid, require_no, reply_person_dep, reply_person_sid, reply_dt, reply_udt,
                                content, content_css, Lists.newArrayList(), Lists.newArrayList());
                        trOsReplyVOs.add(tor);
                    } else if (typeName.equals(TrOsType.tr_os_history)) {
                        TrOsHistoryVO thv = new TrOsHistoryVO(sid, os_reply_sid, os_reply_and_reply_sid, null, os_sid, os_no, require_sid, require_no, content, content_css, othSetHistoryBehavior,
                                othSetStatus, create_usr, create_dt, update_usr, update_dt);
                        trOsHistoryVOs.add(thv);
                    }
                } catch (Exception e) {
                    log.error("getTrOsByRequestSid Error", e);
                }
            }
            Collections.sort(trOsAttachmentVOs, trOsAttachmentVOComparator);
            Collections.sort(trOsReplyAndReplyVOs, trOsReplyAndReplyVOComparator);
            Collections.sort(trOsReplyVOs, trOsReplyVOComparator);

            //其它設定資訊 - 歷程(包含回覆)
            List<TrOsHistoryVO> replyHistory = trOsHistoryVOs.stream()
                    .filter(each -> s_os_sid.equals(each.getOs_sid()) && (OthSetHistoryBehavior.REPLY.equals(each.getBehavior())))
                    .collect(Collectors.toList());

            List<TrOsHistoryVO> realReplyHistory = Lists.newArrayList();
            if (replyHistory != null && !replyHistory.isEmpty()) {
                replyHistory.forEach(replyItem -> {
                    if (Strings.isNullOrEmpty(replyItem.getOs_reply_sid())) {
                        log.error("問題資料,歷程類型-回覆,卻無os_reply_sid. os_no:[" + replyItem.getOs_no() + "] os_history_sid:[" + replyItem.getOs_history_sid() + "]");
                        return;
                    }
                    List<TrOsReplyVO> filterTrOsReplyVOs = trOsReplyVOs.stream()
                            .filter(each -> replyItem.getOs_reply_sid().equals(each.getOs_reply_sid()) && s_os_sid.equals(each.getOs_sid()))
                            .collect(Collectors.toList());
                    if (filterTrOsReplyVOs == null || filterTrOsReplyVOs.isEmpty()) {
                        log.error("問題資料,歷程類型-回覆的回覆,卻無os_reply_sid. os_no:[" + replyItem.getOs_no() + "] "
                                + "os_reply_sid:[" + replyItem.getOs_reply_sid() + "]");
                        return;
                    }
                    TrOsReplyVO selTrOsReplyVO = filterTrOsReplyVOs.get(0);
                    selTrOsReplyVO.setOs_history_sid(replyItem.getOs_history_sid());

                    //回覆附件
                    List<TrOsAttachmentVO> replyAtt = trOsAttachmentVOs.stream()
                            .filter(each -> replyItem.getOs_history_sid().equals(each.getOs_history_sid()) && s_os_sid.equals(each.getOs_sid()))
                            .collect(Collectors.toList());
                    selTrOsReplyVO.setTrOsAttachmentVOs(replyAtt);
                    List<TrOsReplyAndReplyVO> replyAndReplys = trOsReplyAndReplyVOs.stream()
                            .filter(each -> s_os_sid.equals(each.getOs_sid()) && each.getOs_reply_sid().equals(replyItem.getOs_reply_sid()))
                            .collect(Collectors.toList());
                    List<TrOsReplyAndReplyVO> realReplyAndReplys = Lists.newArrayList();
                    //取得回覆的回覆附件
                    if (replyAndReplys != null && !replyAndReplys.isEmpty()) {
                        replyAndReplys.forEach(replyAndReplyItem -> {
                            List<TrOsHistoryVO> replyAndReplyHistorys = trOsHistoryVOs.stream()
                                    .filter(each -> s_os_sid.equals(each.getOs_sid())
                                            && each.getOs_reply_and_reply_sid().equals(replyAndReplyItem.getOs_reply_and_reply_sid()) && (OthSetHistoryBehavior.REPLY_AND_REPLY.equals(each.getBehavior())))
                                    .collect(Collectors.toList());
                            if (replyAndReplyHistorys == null || replyAndReplyHistorys.isEmpty()) {
                                log.error("問題資料,歷程類型-回覆的回覆,卻無os_reply_and_reply_sid. os_no:[" + replyItem.getOs_no() + "] "
                                        + "os_reply_and_reply_sid:[" + replyAndReplyItem.getOs_reply_and_reply_sid() + "]");

                            } else {
                                TrOsHistoryVO replyAndReplyHistory = replyAndReplyHistorys.get(0);
                                List<TrOsAttachmentVO> replyAndReplyAtt = trOsAttachmentVOs.stream()
                                        .filter(each -> replyAndReplyHistory.getOs_history_sid().equals(each.getOs_history_sid()) && s_os_sid.equals(each.getOs_sid()))
                                        .collect(Collectors.toList());
                                replyAndReplyItem.setOs_history_sid(replyAndReplyHistory.getOs_history_sid());
                                replyAndReplyItem.getTrOsAttachmentVOs().addAll(replyAndReplyAtt);
                                realReplyAndReplys.add(replyAndReplyItem);
                            }
                        });
                    }
                    selTrOsReplyVO.setTrOsReplyAndReplyVO(realReplyAndReplys);
                    replyItem.setTrOsReplyVO(selTrOsReplyVO);
                    realReplyHistory.add(replyItem);
                });
            }
            //其它設定資訊 - 歷程(包含完成,取消)
            List<TrOsHistoryVO> processHistory = trOsHistoryVOs.stream()
                    .filter(each -> s_os_sid.equals(each.getOs_sid()) && (OthSetHistoryBehavior.CANCEL_OS.equals(each.getBehavior())
                            || OthSetHistoryBehavior.FINISH_OS.equals(each.getBehavior())))
                    .collect(Collectors.toList());
            
            //其它設定資訊 - 附件
            @SuppressWarnings("unused")
            List<TrOsAttachmentVO> trosAtt = trOsAttachmentVOs.stream()
                    .filter(each -> Strings.isNullOrEmpty(each.getOs_history_sid()) && s_os_sid.equals(each.getOs_sid()))
                    .collect(Collectors.toList());
            
            List<TrOsHistoryVO> allHistorys = Lists.newArrayList();
            allHistorys.addAll(realReplyHistory);
            allHistorys.addAll(processHistory);
            Collections.sort(allHistorys, trOsHistoryVOComparator);
            trOsVOs.addAll(allHistorys);

        } catch (Exception e) {
            log.error("getTrOsByRequestSid", e);
        }
        return trOsVOs;
    }

    transient private Comparator<TrOsAttachmentVO> trOsAttachmentVOComparator = new Comparator<TrOsAttachmentVO>() {
        @Override
        public int compare(TrOsAttachmentVO obj1, TrOsAttachmentVO obj2) {
            final Date seq1 = obj1.getCreate_dt();
            final Date seq2 = obj2.getCreate_dt();
            return seq2.compareTo(seq1);
        }
    };

    transient private Comparator<TrOsReplyAndReplyVO> trOsReplyAndReplyVOComparator = new Comparator<TrOsReplyAndReplyVO>() {
        @Override
        public int compare(TrOsReplyAndReplyVO obj1, TrOsReplyAndReplyVO obj2) {
            final Date seq1 = obj1.getReply_udt();
            final Date seq2 = obj2.getReply_udt();
            return seq2.compareTo(seq1);
        }
    };

    transient private Comparator<TrOsReplyVO> trOsReplyVOComparator = new Comparator<TrOsReplyVO>() {
        @Override
        public int compare(TrOsReplyVO obj1, TrOsReplyVO obj2) {
            final Date seq1 = obj1.getReply_udt();
            final Date seq2 = obj2.getReply_udt();
            return seq2.compareTo(seq1);
        }
    };

    transient private Comparator<TrOsHistoryVO> trOsHistoryVOComparator = new Comparator<TrOsHistoryVO>() {
        @Override
        public int compare(TrOsHistoryVO obj1, TrOsHistoryVO obj2) {
            final Date seq1 = obj1.getUpdate_dt();
            final Date seq2 = obj2.getUpdate_dt();
            return seq2.compareTo(seq1);
        }
    };
}
