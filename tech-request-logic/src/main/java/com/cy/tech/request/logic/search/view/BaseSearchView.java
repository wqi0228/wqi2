/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Convert;

import com.cy.tech.request.logic.search.service.SearchResultHelper;
import com.cy.tech.request.vo.converter.ReqToBeReadTypeConverter;
import com.cy.tech.request.vo.converter.RequireStatusTypeConverter;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.work.common.cache.WkCommonCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.logic.lib.manager.WorkCommonReadRecordManager;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

/**
 * 所有的報表頁面顯示vo都要繼承
 *
 * @author jason_h
 */
@Data
@EqualsAndHashCode(of = { "sid" })
@Slf4j
public abstract class BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4467838302751239603L;
    /** vo sid */
    private String sid;
    /** 需求單號 */
    private String requireNo;
    /** 需求單主題 */
    private String requireTheme;

    /** 製作進度 */
    @Convert(converter = RequireStatusTypeConverter.class)
    private RequireStatusType requireStatus;
    public String getRequireStatusDesc() {
        if (requireStatus != null) {
            return requireStatus.getValue();
        }
        return "";
    }

    /** 追 */
    private Boolean hasTrace;

    /**
     * 檢查項目 (系統別)
     */
    private String checkItemsDescr;
    private List<RequireCheckItemType> checkItemTypes;
    /**
     * 主責單位
     */
    private Integer inChargeDepSid;
    /**
     * 主責單位
     */
    private String inChargeDepDescr;
    /**
     * 負責人
     */
    private Integer inChargeUsrSid;
    /**
     * 負責人
     */
    private String inChargeUsrDescr;

    /**
     * 檢查確認資訊
     */
    private String checkConfirmInfo;

    /**
     * 檢查人員
     */
    private List<Integer> checkConfirmUserSids = Lists.newArrayList();

    /**
     * 為異常單據:項目都檢查完了, 但是製作進度還留在待檢查
     */
    private boolean exceptionCase = false;

    /**
     * 是否待閱讀
     */
    private String waitRead;
    /**
     * 閱讀時間
     */
    private Date readDate;

    /**
     * 閱讀狀態
     */
    private ReadRecordType readRecordType = ReadRecordType.UN_READ;

    /**
     * 閱讀狀態說明
     * 
     * @return
     */
    public String getReadRecordTypeDescr() {
        if (this.readRecordType == null) {
            return "";
        }
        return this.readRecordType.getValue();
    }

    /** 需求單 待閱原因 */
    @Convert(converter = ReqToBeReadTypeConverter.class)
    private ReqToBeReadType readReason;

    public String getReadReasonDesc() {
        if (readReason != null) {
            return readReason.getLabel();
        }
        return "";
    }

    /** 子單待閱原因 */
    private WaitReadReasonType waitReadReasonType;

    private String checkItemNames;

    /**
     * 待閱原因 說明
     * 
     * @return 待閱原因 說明
     */
    public String getReadReasonDescr() {
        if (ReadRecordType.WAIT_READ.equals(this.readRecordType)) {
            if (this.readReason != null) {
                return this.readReason.getLabel();
            } else if (this.waitReadReasonType != null) {
                return this.waitReadReasonType.getValue();
            }
        }
        return "";
    }

    /**
     * 準備共用欄位資料
     * 
     * @param record     data row
     * @param startIndex 起始處理的 index
     */
    public void prepareCommonColumn(
            Object[] record,
            Integer startIndex) {

        if (record == null) {
            return;
        }

        SearchResultHelper searchResultHelper = SearchResultHelper.getInstance();

        // ====================================
        // 檢查項目 (系統別)
        // ====================================
        if (record.length > startIndex) {
            // 取得回傳資料字串
            String checkItmesStr = WkStringUtils.safeTrim(record[startIndex++]);
            // 處理系統別欄位資訊
            this.prepareCheckItmes(checkItmesStr);
        }

        // ====================================
        // 主責單位 SID
        // ====================================
        if (record.length > startIndex) {
            int currIndex = startIndex++;
            if (record[currIndex] != null) {
                Integer inChargeDepSid = (Integer) record[currIndex];
                this.setInChargeDepSid(inChargeDepSid);
                this.setInChargeDepDescr(searchResultHelper.prepareDepDescr(inChargeDepSid));
            }
        }

        // ====================================
        // 負責人 SID
        // ====================================
        if (record.length > startIndex) {
            int currIndex = startIndex++;
            if (record[currIndex] != null) {
                Integer inChargeUsrSid = (Integer) record[currIndex];
                this.setInChargeUsrSid(inChargeUsrSid);
                this.setInChargeUsrDescr(searchResultHelper.prepareUsrDescr(inChargeUsrSid));
            }
        }

        // ====================================
        // 閱讀記錄
        // ====================================
        // 是否為待閱讀狀態
        String waitRead = null;
        if (record.length > startIndex) {
            int currIndex = startIndex++;
            if (record[currIndex] != null) {
                waitRead = String.valueOf(record[currIndex]);
            }
        }
        // 閱讀時間
        Date readDate = null;
        if (record.length > startIndex) {
            int currIndex = startIndex++;
            if (record[currIndex] != null) {
                readDate = (Date) record[currIndex];
            }
        }

        // 待閱讀
        if ("Y".equals(waitRead)) {
            if (readDate == null) {
                this.readRecordType = ReadRecordType.UN_READ;
            } else {
                this.readRecordType = ReadRecordType.WAIT_READ;
            }
        }
        // 已閱讀
        else if ("N".equals(waitRead)) {
            this.readRecordType = ReadRecordType.HAS_READ;
        }

    }

    public void prepareCommonColumn() {

        SearchResultHelper searchResultHelper = SearchResultHelper.getInstance();
        WorkCommonReadRecordManager workCommonReadRecordManager = WorkCommonReadRecordManager.getInstance();

        // 系統別
        this.prepareCheckItmes(this.checkItemNames);
        // 負責單位
        this.setInChargeDepDescr(searchResultHelper.prepareDepDescr(inChargeDepSid));
        // 負責人
        this.setInChargeUsrDescr(searchResultHelper.prepareUsrDescr(inChargeUsrSid));

        // 閱讀記錄
        ReadRecordType readRecordType = workCommonReadRecordManager.transToReadRecordType(
                this.getWaitRead(),
                this.getReadDate());

        this.setReadRecordType(readRecordType);

    }

    // 快取名稱
    private static final String cacheNameForPrepareCheckItmes = BaseSearchView.class.getSimpleName() + "_prepareCheckItmes";

    /**
     * 處理系統別欄位資訊 (快取處理)
     * 
     * @param checkItmesStr 系統別資料字串
     */
    @SuppressWarnings("unchecked")
    private void prepareCheckItmes(
            String checkItmesStr) {

        checkItmesStr = WkStringUtils.safeTrim(checkItmesStr);

        if (WkStringUtils.isEmpty(checkItmesStr)) {
            return;
        }

        List<String> cacheKeys = Lists.newArrayList(checkItmesStr);

        Map<String, Object> dataMap = WkCommonCache.getInstance().getCache(
                cacheNameForPrepareCheckItmes,
                cacheKeys,
                WkCommonCache.getInstance().getTimeToLiveMilliseconds());

        if (dataMap == null) {
            dataMap = Maps.newHashMap();

            // 用 Set 過濾重複
            Set<RequireCheckItemType> currCheckItemTypes = Sets.newHashSet();
            for (String checkItmeStr : checkItmesStr.split(",")) {

                if (WkStringUtils.isEmpty(checkItmeStr)) {
                    continue;
                }

                RequireCheckItemType checkItemType = RequireCheckItemType.safeValueOf(checkItmeStr);
                if (checkItemType == null) {
                    log.warn("無法解析的 RequireCheckItemType : [{}]", checkItmeStr);
                    continue;
                }
                currCheckItemTypes.add(checkItemType);

            }

            // 排序
            List<RequireCheckItemType> sortCheckItemTypes = currCheckItemTypes.stream()
                    .sorted(Comparator.comparing(each -> each.ordinal()))
                    .collect(Collectors.toList());

            String sortCheckItemTypeDescrs = sortCheckItemTypes.stream()
                    .map(RequireCheckItemType::getDescr)
                    .collect(Collectors.joining("、"));

            // 快取
            dataMap.put("sortCheckItemTypes", sortCheckItemTypes);
            dataMap.put("sortCheckItemTypeDescrs", sortCheckItemTypeDescrs);

            WkCommonCache.getInstance().putCache(cacheNameForPrepareCheckItmes, cacheKeys, dataMap);
        }

        this.setCheckItemTypes((List<RequireCheckItemType>) dataMap.get("sortCheckItemTypes"));
        this.setCheckItemsDescr(dataMap.get("sortCheckItemTypeDescrs") + "");
    }

}
