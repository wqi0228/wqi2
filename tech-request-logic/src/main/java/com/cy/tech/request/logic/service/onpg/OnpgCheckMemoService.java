/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.onpg;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.RequireReadRecordHelper;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.repository.onpg.WorkOnpgCheckMemoRepo;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.onpg.WorkOnpgCheckMemo;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgHistoryBehavior;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.work.common.utils.WkJsoupUtils;

/**
 * @author shaun
 */
@Component
public class OnpgCheckMemoService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8181703616115824426L;
    @Autowired
    private RequireService reqService;
    @Autowired
    private OnpgService opService;
    @Autowired
    private OnpgShowService opsService;
    @Autowired
    private OnpgHistoryService ophService;
    @Autowired
    private WkJsoupUtils jsoupUtils;
    @Autowired
    private WorkOnpgCheckMemoRepo memoDao;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private transient RequireReadRecordHelper requireReadRecordHelper;

    public WorkOnpgCheckMemo createEmptyCheckMemo(WorkOnpg onpg, User executor) {
        WorkOnpgCheckMemo memo = new WorkOnpgCheckMemo();
        memo.setOnpg(onpg);
        memo.setOnpgNo(onpg.getOnpgNo());
        memo.setSourceType(onpg.getSourceType());
        memo.setSourceSid(onpg.getSourceSid());
        memo.setSourceNo(onpg.getSourceNo());
        memo.setPerson(executor);
        memo.setDept(orgService.findBySid(executor.getPrimaryOrg().getSid()));
        memo.setCreatedDate(new Date());
        memo.setUpdatedDate(new Date());
        memo.setHistory(ophService.createEmptyHistory(onpg, executor));
        memo.getHistory().setBehavior(WorkOnpgHistoryBehavior.CHECK_MEMO);
        return memo;
    }

    @Transactional(readOnly = true)
    public List<WorkOnpgCheckMemo> findByOnpg(WorkOnpg onpg) {
        try {
            onpg.getCheckMemos().size();
        } catch (LazyInitializationException e) {
            // log.debug("findByOnpg lazy init error :" + e.getMessage(), e);
            onpg.setCheckMemos(memoDao.findByOnpgOrderByUpdatedDateDesc(onpg));
        }
        return onpg.getCheckMemos();
    }

    /**
     * 檢查註記視窗-確認
     * 
     * @param memo
     * @throws IllegalAccessException
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveByNewCheckMemo(WorkOnpgCheckMemo memo) throws IllegalAccessException {
        if (opsService.disableCheckMemo(
                reqService.findByReqSid(memo.getSourceSid()),
                opService.findByOnpgNo(memo.getOnpgNo()),
                memo.getPerson())) {
            throw new IllegalAccessException("無權限進行操作");
        }
        memo.setContent(jsoupUtils.clearCssTag(memo.getContentCss()));
        memo.setSid(memoDao.save(memo).getSid());

        WorkOnpg onpg = opService.copyOnpg(memo.getOnpg());
        this.findByOnpg(onpg).add(memo);
        if (onpg.getOnpgStatus().equals(WorkOnpgStatus.ALREADY_ON)) {
            onpg.setOnpgStatus(WorkOnpgStatus.CHECK_REPLY);
        }
        onpg.setReadReason(WaitReadReasonType.ONPG_ANNOTATION);
        onpg.setReadUpdateDate(new Date());
        onpg.setFinishDate(new Date());
        ophService.createCheckMemoHistory(onpg, memo);
        opService.save(onpg, memo.getPerson());

        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.WORKONPG,
                onpg.getSid(),
                memo.getPerson().getSid());
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveByEditCheckMemo(WorkOnpgCheckMemo memo, User executor) throws IllegalAccessException {
        if (opsService.disableCheckMemo(
                reqService.findByReqSid(memo.getSourceSid()),
                opService.findByOnpgNo(memo.getOnpgNo()),
                executor)) {
            throw new IllegalAccessException("無權限進行操作");
        }
        memo.setUpdatedDate(new Date());
        memoDao.save(memo);
        ophService.update(memo.getHistory(), executor);
        ophService.sortHistory(memo.getOnpg());
    }

}
