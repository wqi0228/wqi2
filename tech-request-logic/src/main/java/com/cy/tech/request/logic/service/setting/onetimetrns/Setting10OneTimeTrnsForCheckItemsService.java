package com.cy.tech.request.logic.service.setting.onetimetrns;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.require.RequireCheckItem;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

/**
 * 一次性轉檔 - 檢查項目 (系統別)
 * 
 * @author allen1214_wu
 *
 */
@Slf4j
@Service
public class Setting10OneTimeTrnsForCheckItemsService {

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private transient NativeSqlRepository nativeSqlRepository;
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private transient JdbcTemplate jdbcTemplate;

    // ========================================================================
    // 變數
    // ========================================================================
    /**
     * 每次批次執行筆數
     */
    private final Integer BATCH_UPDATE_SIZE = 200;
    private final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:sss");

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 
     */
    @Transactional(rollbackFor = Exception.class)
    public void process() {

        // ====================================
        // 查詢待轉檔資料
        // ====================================
        // 查詢
        List<TrnsCheckItemsTo> trnsTos = this.findSrcData();
        // 無資料時不處理
        if (WkStringUtils.isEmpty(trnsTos)) {
            return;
        }

        // ====================================
        // 資料整理
        // ====================================
        // 記錄要新增的 tr_require_check_item
        List<RequireCheckItem> requireCheckItems = Lists.newArrayList();

        for (TrnsCheckItemsTo trnsTo : trnsTos) {

            String webCodeStr = WkStringUtils.safeTrim(trnsTo.getWebCode());

            // 舊系統別轉換成新的
            RequireCheckItemType checkItemType = RequireCheckItemType.safeValueOf(webCodeStr);

            try {
                // 有比對到為單一項目
                if (checkItemType != null) {
                    requireCheckItems.add(this.createRequireCheckItem(trnsTo, checkItemType));
                }
                // 為複合項目
                else if ("BBIN_XBB".equals(webCodeStr)) {
                    // BBIN
                    requireCheckItems.add(this.createRequireCheckItem(trnsTo, RequireCheckItemType.BBIN));
                    // XBB
                    requireCheckItems.add(this.createRequireCheckItem(trnsTo, RequireCheckItemType.XBB));
                } else {
                    log.error("無法判斷的系統別資料! web_code:[{}]", webCodeStr);
                }
            } catch (SystemOperationException e) {
                // 判斷檢查資訊異常, 需個別檢視資料
                log.error("判斷檢查資訊異常, 需個別檢視資料!" + e.getMessage());
            }
        }

        // ====================================
        // INSTER tr_require_check_item
        // ====================================
        this.batchInsertRequireCheckItem(requireCheckItems);

        // ====================================
        // UPDATE tr_require.trns_flag_for_checkitem
        // ====================================
        this.updateTrnsFlag(requireCheckItems);

    }

    /**
     * @param trnsTo
     * @param checkItemType
     * @return
     * @throws SystemOperationException
     */
    private RequireCheckItem createRequireCheckItem(
            TrnsCheckItemsTo trnsTo,
            RequireCheckItemType checkItemType) throws SystemOperationException {

        // ====================================
        // 基本資料
        // ====================================
        RequireCheckItem entity = new RequireCheckItem();

        // 建立時間
        if (RequireStatusType.DRAFT.name().equals(trnsTo.getRequireStatus())) {
            // 草稿時期還沒有 create date , 以草稿建立時間為準
            entity.setCreatedDate(trnsTo.getRequireDraftDate());
        } else {
            // (以立案日為準)
            entity.setCreatedDate(trnsTo.getRequireCreateDate());
        }

        // 建立人員
        entity.setCreatedUser(trnsTo.getRequireCreateUserSid());
        
        if (WkStringUtils.isEmpty(entity.getCreatedDate())
                || WkStringUtils.isEmpty(entity.getCreatedUser())) {
            log.error(new com.google.gson.GsonBuilder().setPrettyPrinting().create().toJson(trnsTo));
            throw new SystemOperationException(
                    "\r\nRequireCreateDate 或 RequireCreateUserSid 為空!sid:[" + trnsTo.getRequireSid()+ "]", 
                    InfomationLevel.ERROR
                    );
        }

        // 資料狀態
        entity.setStatus(Activation.ACTIVE);
        // 註記由轉檔轉入
        entity.setFromTrns(true);

        // 需求單sid
        entity.setRequireSid(trnsTo.getRequireSid());
        // 檢查項目
        entity.setCheckItemType(checkItemType);

        // ====================================
        // 已檢查 的資訊
        // ====================================
        // 符合『已檢查』的的主單狀態
        List<String> requireStatusTypes = Lists.newArrayList(
                RequireStatusType.PROCESS,
                RequireStatusType.COMPLETED,
                RequireStatusType.CLOSE,
                RequireStatusType.AUTO_CLOSED).stream()
                .map(RequireStatusType::name)
                .collect(Collectors.toList());

        // 符合狀態時 ,放入檢查人員和檢查時間
        if (requireStatusTypes.contains(trnsTo.getRequireStatus())) {
            
            if(trnsTo.getCheckUserSid()==null && trnsTo.getPassUserSid() ==null) {
                log.error(new com.google.gson.GsonBuilder().setPrettyPrinting().create().toJson(trnsTo));
                throw new SystemOperationException(
                        "\r\nCheckUserSid 或 PassUserSid 皆為空!sid:[" + trnsTo.getRequireSid()+ "]", 
                        InfomationLevel.ERROR);
            }
            

            // ====================================
            // 外部需求檢查資訊 - 依據追蹤記錄 CONFIRMED_CHECK (檢查確認)
            // ====================================
            if (trnsTo.getCheckUserSid() != null) {
                entity.setCheckUserSid(trnsTo.getCheckUserSid());
                entity.setCheckDate(trnsTo.getCheckDate());
                entity.setUpdatedUser(trnsTo.getCheckUserSid());
                entity.setUpdatedDate(trnsTo.getCheckDate());
            }
            // ====================================
            // 內需求檢查資訊 - 依據追蹤記錄 REQUIRE_ESTABLISH (需求成立)
            // ====================================
            else if (trnsTo.getPassUserSid() != null) {
                entity.setCheckUserSid(trnsTo.getPassUserSid());
                entity.setCheckDate(trnsTo.getPassDate());
                entity.setUpdatedUser(trnsTo.getPassUserSid());
                entity.setUpdatedDate(trnsTo.getPassDate());
            }
            // ====================================
            // 沒判斷到的狀態
            // ====================================
            else {
                log.error("無法確認檢查人員和時間！");
                log.info(new com.google.gson.GsonBuilder().setPrettyPrinting().create().toJson(trnsTo));
                throw new SystemOperationException("無法確認檢查人員和時間！", InfomationLevel.ERROR);
            }
        }

        // ====================================
        // 完成
        // ====================================
        return entity;

    }

    /**
     * 查詢來源資料
     */
    private List<TrnsCheckItemsTo> findSrcData() {

        // ====================================
        // 準備 SQL
        // ====================================
        StringBuffer varname1 = new StringBuffer();
        varname1.append("SELECT req.require_sid           AS requireSid, ");
        varname1.append("       req.require_status        AS requireStatus, ");
        varname1.append("       req.web_code              AS webCode, ");
        varname1.append("       bigCategory.req_category  AS requireCategory, ");
        varname1.append("       req.create_usr            AS requireCreateUserSid, ");
        varname1.append("       req.create_dt             AS requireCreateDate, ");
        varname1.append("       req.draft_dt              AS requireDraftDate, ");
        varname1.append("       traceEx.create_usr        AS checkUserSid, ");
        varname1.append("       traceEx.create_dt         AS checkDate, ");
        varname1.append("       traceIn.create_usr        AS passUserSid, ");
        varname1.append("       traceIn.create_dt         AS passDate ");
        varname1.append("FROM   tr_require req ");
        varname1.append("       INNER JOIN tr_category_key_mapping mapping ");
        varname1.append("              ON mapping.key_sid = req.mapping_sid ");
        varname1.append("       INNER JOIN tr_basic_data_big_category bigCategory ");
        varname1.append("              ON bigCategory.basic_data_big_category_sid = mapping.big_category_sid ");

        // 檢查確認 for 外部需求
        varname1.append("       LEFT JOIN tr_require_trace traceEx ");
        varname1.append("              ON traceEx.require_sid = req.require_sid ");
        varname1.append("                 AND traceEx.require_trace_type = 'CONFIRMED_CHECK' ");

        // 需求成立 for 內部需求
        varname1.append("       LEFT JOIN tr_require_trace traceIn ");
        varname1.append("              ON traceIn.require_sid = req.require_sid ");
        varname1.append("                 AND traceIn.require_trace_type = 'REQUIRE_ESTABLISH' ");

        varname1.append("       LEFT JOIN tr_require_check_item checkitem ");
        varname1.append("              ON checkitem.require_sid = req.require_sid ");
        varname1.append("             AND checkitem.from_trns = 1 "); // 避免轉過的再撈出來-|
        varname1.append("WHERE  checkitem.check_item_sid IS NULL "); // 避免轉過的再撈出來-| (checkitem 已經有資料的不處理)
        varname1.append("AND    req.trns_flag_for_checkitem = 0 "); // 避免轉過的再撈出來-| (主檔註記轉過的不處理)

        varname1.append("GROUP  BY req.require_sid");

        // ====================================
        // 查詢
        // ====================================
        List<TrnsCheckItemsTo> trnsTos = nativeSqlRepository.getResultList(
                varname1.toString(),
                null,
                TrnsCheckItemsTo.class);

        return trnsTos;
    }

    /**
     * @param requireCheckItems
     */
    @Transactional(rollbackFor = Exception.class)
    private void batchInsertRequireCheckItem(List<RequireCheckItem> requireCheckItems) {

        if (WkStringUtils.isEmpty(requireCheckItems)) {
            log.warn("傳入的為空，不執行");
            return;
        }

        Long startTime = System.currentTimeMillis();
        String procTitle = "INSERT tr_require_check_item [" + requireCheckItems.size() + "]筆";
        log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

        StringBuffer insertSQL = new StringBuffer();
        insertSQL.append("INSERT INTO tr_require_check_item ");
        insertSQL.append("            ( ");
        // insertSQL.append(" check_item_sid, "); //auto
        insertSQL.append("             status, ");
        insertSQL.append("             create_usr, ");
        insertSQL.append("             create_dt, ");
        insertSQL.append("             update_usr, ");
        insertSQL.append("             update_dt, ");
        insertSQL.append("             require_sid, ");
        insertSQL.append("             check_item_type, ");
        insertSQL.append("             check_user_sid, ");
        insertSQL.append("             check_date, ");
        insertSQL.append("             from_trns) ");
        insertSQL.append("VALUES      (  ");
        // insertSQL.append(" 0, "); //check_item_sid
        insertSQL.append("              0, "); // status
        insertSQL.append("              ?, "); // 1.create_usr
        insertSQL.append("              ?, "); // 2.create_dt
        insertSQL.append("              ?, "); // 3.update_usr
        insertSQL.append("              ?, "); // 4.update_dt
        insertSQL.append("              ?, "); // 5.require_sid
        insertSQL.append("              ?, "); // 6.check_item_type
        insertSQL.append("              ?, "); // 7.check_user_sid
        insertSQL.append("              ?, "); // 8.check_date
        insertSQL.append("              1  "); // from_trns
        insertSQL.append("              );");

        int totalSize = requireCheckItems.size();
        for (int i = 0; i < totalSize; i += BATCH_UPDATE_SIZE) {
            int index = (i + BATCH_UPDATE_SIZE) > totalSize ? totalSize : i + BATCH_UPDATE_SIZE;
            // 取得本次批次執行筆數
            final List<RequireCheckItem> batchList = requireCheckItems.subList(i, index);

            this.jdbcTemplate.batchUpdate(insertSQL.toString(), new BatchPreparedStatementSetter() {

                @Override
                public int getBatchSize() {
                    return batchList.size();
                }

                @Override
                public void setValues(PreparedStatement ps, int i) throws SQLException {
                    RequireCheckItem item = batchList.get(i);
                    try {

                        int index = 1;
                        // create_usr
                        ps.setInt(index++, item.getCreatedUser());
                        // create_dt
                        ps.setString(index++, item.getCreatedDate() == null ? null : df.format(item.getCreatedDate()));
                        // update_usr (integer 不可以為 null, 改以 String 傳入)
                        ps.setString(index++, item.getUpdatedUser() == null ? null : item.getUpdatedUser() + "");
                        // update_dt
                        ps.setString(index++, item.getUpdatedDate() == null ? null : df.format(item.getUpdatedDate()));
                        // require_sid
                        ps.setString(index++, item.getRequireSid());
                        // check_item_type
                        ps.setString(index++, item.getCheckItemType().name());
                        // check_user_sid (integer 不可以為 null, 改以 String 傳入)
                        ps.setString(index++, item.getCheckUserSid() == null ? null : item.getCheckUserSid() + "");
                        // check_date
                        ps.setString(index++, item.getCheckDate() == null ? null : df.format(item.getCheckDate()));
                    } catch (Exception e) {
                        log.error("傳入資料解析錯誤!");
                        log.info(new com.google.gson.GsonBuilder().setPrettyPrinting().create().toJson(item));
                        throw e;
                    }

                }
            });
        }
        log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
    }

    /**
     * 更新主檔的轉檔 flag
     * 
     * @param requireCheckItems
     */
    private void updateTrnsFlag(List<RequireCheckItem> requireCheckItems) {

        if (WkStringUtils.isEmpty(requireCheckItems)) {
            return;
        }

        // ====================================
        // 收集有轉檔的 需求單 sid
        // ====================================
        Set<String> requireSids = requireCheckItems.stream()
                .map(RequireCheckItem::getRequireSid)
                .collect(Collectors.toSet());

        // ====================================
        // 組 SQL
        // ====================================
        // 避免參數過多發生錯誤, 直接將SID組入字串
        String sql = ""
                + "UPDATE tr_require "
                + "   SET trns_flag_for_checkitem = 1 "
                + " WHERE require_sid IN ( '" + String.join("' ,'", requireSids) + "'); ";

        // ====================================
        // 執行 SQL
        // ====================================
        jdbcTemplate.execute(sql);
    }

}
