/**
 * 
 */
package com.cy.tech.request.logic.service.syncmms.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.tech.request.logic.service.RequireTraceService;
import com.cy.tech.request.logic.service.onpg.OnpgService;
import com.cy.tech.request.logic.service.syncmms.to.SyncFormTo;
import com.cy.tech.request.vo.log.LogSyncMmsActionType;
import com.cy.work.common.exception.SystemOperationException;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Service
@Slf4j
public class SyncMmsSyncFormCancelHelper {
    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private transient OnpgService onpgService;
    @Autowired
    private RequireTraceService requireTraceService;

    // ========================================================================
    // 主方法
    // ========================================================================
    /**
     * 僅新增ON程式單 (附掛於已存在需求單)
     * 
     * @param syncFormTo
     * @throws SystemOperationException
     */
    @Transactional(rollbackFor = { Exception.class })
    public void process(
            SyncFormTo syncFormTo)
            throws SystemOperationException {

        syncFormTo.setLogSyncMmsActionType(LogSyncMmsActionType.CANCEL_ONPG);

        // isFromMms 已經在準備欄位時比對
        // if(!syncFormTo.getWorkOnpgTo().isFromMms()) {}

        // ====================================
        // 檢查ON程式單據狀態是否為『無需動作』
        // ====================================
        if (2 == syncFormTo.getWorkOnpgTo().getOnpgStatus().getInCancelFromMMSSync()) {
            String message = ""
                    + "單號(MMS)[" + syncFormTo.getMmsID() + "] "
                    + "ON程式單號[" + syncFormTo.getWorkOnpgTo().getOnpgNo() + "]，"
                    + "單據狀態為【" + syncFormTo.getWorkOnpgTo().getOnpgStatus().getLabel() + "】，無需動作";

            syncFormTo.setLogSyncMmsActionType(LogSyncMmsActionType.CANCEL_NONE);
            syncFormTo.addErrorInfo(message);
            log.warn(message);
            return;
        }

        // ====================================
        // 檢查ON程式單據狀態是否為『無法取消』
        // ====================================
        if (0 == syncFormTo.getWorkOnpgTo().getOnpgStatus().getInCancelFromMMSSync()) {
            String message = ""
                    + "單號(MMS)[" + syncFormTo.getMmsID() + "] "
                    + "ON程式單號[" + syncFormTo.getWorkOnpgTo().getOnpgNo() + "]，"
                    + "單據狀態為【" + syncFormTo.getWorkOnpgTo().getOnpgStatus().getLabel() + "】，無法取消！";

            syncFormTo.setLogSyncMmsActionType(LogSyncMmsActionType.CANCEL_DENY);
            syncFormTo.addErrorInfo(message);
            log.warn(message);
            return;
        }

        // ====================================
        // ON程式取消取消
        // ====================================
        this.onpgService.cancelByMms(syncFormTo);

        // ====================================
        // 寫需求單追蹤
        // ====================================
        this.requireTraceService.createSyncMmsTrace(syncFormTo);

    }
}
