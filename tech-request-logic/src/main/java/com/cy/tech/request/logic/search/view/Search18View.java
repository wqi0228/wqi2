/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import com.cy.tech.request.logic.search.enums.Search18SubType;
import com.cy.work.common.enums.UrgencyType;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 測試提醒清單 (search18.xhtml)
 *
 * @author jason_h
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Search18View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1933967031818020034L;
    /** 需求日期 (立單日期) */
    private Date requireDate;
    /** 需求類別 */
    private String bigName;
    /** 中類 */
    private String middleName;
    /** 小類 */
    private String smallName;
    /** 需求單位 */
    private Integer requireDep;
    /** 需求人員 */
    private Integer requireUser;
    /** 需求單-緊急度 */
    private UrgencyType urgency;

    /** 期望完成日 */
    private Date hopeDate;

    /** 子程序單據確認類型 (原型確認 | 送測) */
    private Search18SubType subConfirmStatus;
    /** 子程序 sid */
    private String subSid;
    /** 子程序單據狀態 (原型確認 | 送測) */
    private String subStatus;
    /** 子程序單據建立日期 (原型確認 | 送測) */
    private Date subCreatedDate;
    /** 子程序單據建立部門 (原型確認 | 送測) */
    private Integer subCreateDep;
    /** 子程序單據建立者(原型確認 | 送測) */
    private Integer subCreatedUser;
    /** 子程序單據主題(送測) */
    private String subTheme;
    /** 子程序預計完成日期 (原型確認 | 送測) */
    private Date subFinishDate;
    /** 本地端連結網址 */
    private String localUrlLink;
}
