/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.othset;

import com.cy.tech.request.repository.require.othset.OthSetThemeRepo;
import com.cy.tech.request.vo.require.othset.OthSetTheme;
import com.cy.tech.request.vo.require.othset.OthSetThemeDetail;

import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 其它設定資訊主題
 *
 * @author shaun
 */
@Component
public class OthSetThemeService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -764079501055252413L;
    @Autowired
    private OthSetThemeRepo themeRepo;

    public List<OthSetTheme> findAll() {
        return themeRepo.findAll();
    }

    public OthSetTheme findBySid(String sid) {
        return this.themeRepo.findOne(sid);
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveDetail(String sid, List<Integer> noticeDeps) {
        OthSetTheme othSetTheme = themeRepo.findOne(sid);
        othSetTheme.getNoticeDetails().clear();
        for (Integer orgSid : noticeDeps) {
            OthSetThemeDetail detail = new OthSetThemeDetail();
            detail.setOthSetTheme(othSetTheme);
            detail.setDepSid(orgSid);
            othSetTheme.getNoticeDetails().add(detail);
        }
        themeRepo.save(othSetTheme);
    }

    @Transactional(rollbackFor = Exception.class)
    public void update(OthSetTheme othSetTheme) {
        OthSetTheme entity = themeRepo.findOne(othSetTheme.getSid());
        entity.setTheme(othSetTheme.getTheme());
        entity.setContext(othSetTheme.getContext());

        themeRepo.save(entity);
    }

    public OthSetTheme findByTheme(String theme) {
        return themeRepo.findByTheme(theme);
    }

    public OthSetTheme findOne(String sid) {
        return themeRepo.findOne(sid);
    }

    @Transactional(rollbackFor = Exception.class)
    public void addEmptyOthSetTheme() {
        OthSetTheme othSetTheme = new OthSetTheme();
        othSetTheme.setTheme("");
        othSetTheme.setContext("");
        othSetTheme.setSeq(maxSeq() + 1);
        themeRepo.save(othSetTheme);
    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(String sid) {
        themeRepo.delete(sid);
    }

    public Integer maxSeq() {
        Integer orderNo = themeRepo.maxSeq();
        if (orderNo == null) {
            orderNo = 0;
        }
        return orderNo;
    }
}
