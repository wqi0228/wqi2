package com.cy.tech.request.logic.helper.systemnotify;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.setting.sysnotify.SettingSysNotifyService;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.setting.sysnotify.to.SettingSysNotifyMainInfo;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.notify.logic.manager.WorkNotifyManager;
import com.cy.work.notify.vo.enums.NotifyMode;
import com.cy.work.notify.vo.enums.NotifyType;
import com.cy.work.notify.vo.to.NotifyTo;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Service
@Slf4j
public class SysNotifyByPushHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4627723634367481192L;
    // ========================================================================
    // 服務區
    // ========================================================================
    @Autowired
    private transient SettingSysNotifyService settingSysNotifyService;
    @Autowired
    private transient WorkNotifyManager workNotifyManager;

    // ========================================================================
    // 核心方法區
    // ========================================================================
    /**
     * 依據部門建立通知 (執行者不會收到)
     * 
     * @param require     需求單主檔
     * @param theme       主題
     * @param notifyType  通知類別
     * @param depSids     通知部門 sid
     * @param execUserSid 執行者 sid
     */
    protected void processNotifyByDeps(
            Require require,
            String theme,
            NotifyType notifyType,
            List<Integer> depSids,
            Integer execUserSid,
            boolean isShowDebug) {

        // ====================================
        // 取得推撥部門成員
        // ====================================
        List<Integer> userSids = WkUserCache.getInstance().findUserWithManagerByOrgSids(depSids, null).stream()
                .map(User::getSid)
                .collect(Collectors.toList());

        // ====================================
        // 過濾出有開啟通知的 user
        // ====================================
        Map<Integer, SettingSysNotifyMainInfo> notifySettingInfoMapByUserSid = this.settingSysNotifyService.filterNeedNoticeUser(
                notifyType,
                require.getMapping().getSmall().getSid(),
                require.getCustomer().getSid(),
                NotifyMode.PUSH,
                userSids);

        // ====================================
        // 建立推撥
        // ====================================
        // 執行
        this.process(
                notifyType,
                Lists.newArrayList(notifySettingInfoMapByUserSid.keySet()),
                require.getSid(),
                require.getRequireNo(),
                theme,
                execUserSid,
                isShowDebug);
    }

    /**
     * 依據使用者建立通知 (執行者不會收到)
     * 
     * @param require     需求單主檔
     * @param theme       主題
     * @param notifyType  通知類別
     * @param userSids    通知使用者 sid
     * @param execUserSid 執行者 sid
     */
    protected void processNotifyByUser(
            Require require,
            String theme,
            NotifyType notifyType,
            Collection<Integer> userSids,
            Integer execUserSid,
            boolean isShowDebug
            ) {

        // ====================================
        // 過濾出有開啟通知的 user
        // ====================================
        Map<Integer, SettingSysNotifyMainInfo> notifySettingInfoMapByUserSid = this.settingSysNotifyService.filterNeedNoticeUser(
                notifyType,
                require.getMapping().getSmall().getSid(),
                require.getCustomer().getSid(),
                NotifyMode.PUSH,
                userSids);

        // ====================================
        // 建立推撥
        // ====================================
        // 執行
        this.process(
                notifyType,
                Lists.newArrayList(notifySettingInfoMapByUserSid.keySet()),
                require.getSid(),
                require.getRequireNo(),
                theme,
                execUserSid,
                isShowDebug);
    }

    /**
     * 執行程序
     * 
     * @param notifyType     通知類別
     * @param notifyUserSids 通知使用者 sid
     * @param requireSid     需求單 sid
     * @param requireNo      需求單號
     * @param requireTheme   需求單主題
     * @param execUserSid    執行者 sid
     */
    private void process(
            NotifyType notifyType,
            Collection<Integer> notifyUserSids,
            String requireSid,
            String requireNo,
            String requireTheme,
            Integer execUserSid,
            boolean isShowDebug) {

        // ====================================
        // 扣掉自己不需要通知
        // ====================================
        notifyUserSids = notifyUserSids.stream()
                .filter(userSid -> !WkCommonUtils.compareByStr(userSid, execUserSid)) // 自己不需要通知
                .collect(Collectors.toList());

        // ====================================
        // 不需要通知
        // ====================================
        if (WkStringUtils.isEmpty(notifyUserSids)) {
            if (isShowDebug) {
                log.info("需求單號：[{}], 方式：[{}], 類型:[{}]  => 沒有需要通知的 user",
                        requireNo,
                        NotifyMode.PUSH.getDescr(),
                        notifyType);
            }
            return;
        }

        // ====================================
        // 先刪除舊有同性質通知
        // ====================================
        this.workNotifyManager.delete(requireNo, notifyType, notifyUserSids, "先刪除舊有同性質通知");

        // ====================================
        // 建立推撥資料
        // ====================================
        NotifyTo to = new NotifyTo(requireSid, requireNo, WorkSourceType.TECH_REQUEST, notifyType);
        to.setTheme(String.format("主題：%s", requireTheme));
        this.workNotifyManager.createNotify(to, notifyUserSids, execUserSid);

        if (isShowDebug) {
            log.info("需求單號：[{}], 方式：[{}], 類型:[{}]  => 已建立：[{}]",
                    requireNo,
                    NotifyMode.PUSH.getDescr(),
                    notifyType,
                    WkUserUtils.findNameBySid(notifyUserSids, "、"));
        }
    }
}
