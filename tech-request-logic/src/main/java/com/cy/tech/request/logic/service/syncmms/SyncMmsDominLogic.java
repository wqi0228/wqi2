package com.cy.tech.request.logic.service.syncmms;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.service.customer.ReqWorkCustomerHelper;
import com.cy.tech.request.logic.service.syncmms.helper.SyncMmsParamHelper;
import com.cy.work.client.syncmms.WorkSyncMmsClient;
import com.cy.work.client.syncmms.to.domain.SyncMmsSyncDomainRequestModel;
import com.cy.work.client.syncmms.to.domain.SyncMmsSyncDomainRequestTo;
import com.cy.work.client.syncmms.to.token.SyncMmsTokenModel;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.alert.TechRequestAlertException;
import com.cy.work.customer.vo.WorkCustomer;
import com.cy.work.customer.vo.enums.EnableType;

import lombok.extern.slf4j.Slf4j;

/**
 * MMS 系統介接 - 同步廳主資料
 * 
 * @author allen1214_wu
 */
@Service
@Slf4j
public class SyncMmsDominLogic implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -8599894719369052494L;

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private ReqWorkCustomerHelper workCustomerService;

    @Autowired
    private WorkSyncMmsClient workSyncMmsClient;

    // ========================================================================
    // 變數
    // ========================================================================
    /**
     * 
     */
    private final String processName = "【MMS 系統介接-同步廳主資料】";

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 執行
     * 
     * @param loginCompID 登入公司別 (若有的話)
     * @return 同步筆數
     * @throws SystemOperationException 錯誤時拋出
     */
    public int process(String loginCompID) throws SystemOperationException {

        log.info(processName + "：start");

        // ====================================
        // 取得所有廳主資料 (非停用)
        // ====================================
        // 查詢
        List<WorkCustomer> workCustomers = this.workCustomerService.findAll();

        // 轉為 API 資料格式
        List<SyncMmsSyncDomainRequestModel> syncMmsSyncDomainModels = workCustomers.stream()
                .map(workCustomer -> {
                    // 組名稱
                    String name = String.format(
                            "%s@%s%s",
                            workCustomer.getName(),
                            workCustomer.getLoginCode(),
                            workCustomer.getCustomerType() == null ? "" : (" (" + workCustomer.getCustomerType().getSimpleName() + ")"));
                    // enable
                    Integer enable = EnableType.ENABLE.equals(workCustomer.getEnable()) ? 1 : 0;
                    // 產生資料封裝物件
                    return new SyncMmsSyncDomainRequestModel(workCustomer.getSid() + "", name, enable);
                })
                .collect(Collectors.toList());

        SyncMmsSyncDomainRequestTo requestTo = new SyncMmsSyncDomainRequestTo(syncMmsSyncDomainModels);

        log.info(processName + "：取得廳主資料共 [" + syncMmsSyncDomainModels.size() + "] 筆");

        // ====================================
        // 取得 token
        // ====================================
        SyncMmsTokenModel syncMmsTokenModel = null;
        try {
            syncMmsTokenModel = this.workSyncMmsClient.getToken(
                    SyncMmsParamHelper.getInstance().getAPIHostDomain(),
                    SyncMmsParamHelper.getInstance().getAPIGetTokenClientID(),
                    SyncMmsParamHelper.getInstance().getAPIGetTokenClientSecret(),
                    loginCompID);
        } catch (SystemOperationException e) {
            throw e;
        } catch (Exception e) {
            String errorMessage = processName + "取得 token 失敗! [" + e.getMessage() + "]";
            log.error(errorMessage, e);
            throw new TechRequestAlertException(errorMessage, syncMmsTokenModel.getCompID());
        }

        log.info(processName + "：取得 token 成功!");

        // ====================================
        // 開始傳送資料
        // ====================================
        log.info(processName + "：開始發送廳主資料到MMS!");
        try {
            this.workSyncMmsClient.syncDomains(
                    SyncMmsParamHelper.getInstance().getAPIHostDomain(),
                    SyncMmsParamHelper.getInstance().getAPIPortal(),
                    syncMmsTokenModel, requestTo);
        } catch (SystemOperationException e) {
            throw e;
        } catch (Exception e) {
            String errorMessage = processName + "同步資料失敗! [" + e.getMessage() + "]";
            log.error(errorMessage, e);
            throw new TechRequestAlertException(errorMessage, syncMmsTokenModel.getCompID());
        }

        log.info(processName + "：同步完成!");

        return syncMmsSyncDomainModels.size();
    }
}
