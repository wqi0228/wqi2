/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.repository.require.TrAlertInboxRepository;
import com.cy.tech.request.vo.enums.ForwardType;
import com.cy.tech.request.vo.enums.ReadRecordStatus;
import com.cy.tech.request.vo.require.TrAlertInbox;
import com.cy.tech.request.vo.require.TrAlertInboxVO;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * @author brain0925_liao
 */
@Component
public class TrAlertInboxService {

    @Autowired
    private TrAlertInboxRepository trAlertInboxRepository;

    public TrAlertInbox getTrAlertInboxBySid(String sid) {
        return trAlertInboxRepository.findOne(sid);
    }

    public List<TrAlertInbox> getWorkInboxBySids(List<String> sids) {
        return trAlertInboxRepository.findByInboxBySids(sids);
    }

    public List<TrAlertInbox> findActiveAll() {
        return trAlertInboxRepository.findActiveAll();
    }

    public TrAlertInbox createWorkInbox(TrAlertInbox workInbox, Integer loginUserSid) {
        workInbox.setStatus(Activation.ACTIVE);
        workInbox.setCreatedUser(loginUserSid);
        workInbox.setCreatedDate(new Date());
        workInbox.setUpdatedDate(new Date());
        return trAlertInboxRepository.save(workInbox);
    }

    public TrAlertInbox updateWorkInbox(TrAlertInbox workInbox, Integer loginUserSid) {
        workInbox.setUpdatedUser(loginUserSid);
        workInbox.setUpdatedDate(new Date());
        return trAlertInboxRepository.save(workInbox);
    }

    public TrAlertInbox updateWorkInboxOnly(TrAlertInbox workInbox) {
        return trAlertInboxRepository.save(workInbox);
    }

    public List<TrAlertInbox> findByInboxByRequireSidAndForwardType(String requireSid, ForwardType forwardType) {
        return trAlertInboxRepository.findByInboxByRequireSidAndForwardType(requireSid, forwardType);
    }

    public List<TrAlertInbox> findByInboxByRequireAndForwardType(String requireSid, ForwardType forwardType) {
        return trAlertInboxRepository.findByInboxByRequireSidAndForwardType(requireSid, forwardType);
    }

    /**
     * 以下列條件查詢 (僅回傳非停用)
     * 
     * @param requireNo   需求單號
     * @param forwardType 轉寄類型
     * @return TrAlertInbox list
     */
    public List<TrAlertInboxVO> findByRequireNoAndForwardType(
            String requireNo,
            ForwardType forwardType) {

        // 查詢
        List<TrAlertInbox> entities = this.trAlertInboxRepository.findByRequireNoAndForwardTypeAndStatusOrderByCreatedDate(
                requireNo,
                forwardType,
                Activation.ACTIVE);

        if (WkStringUtils.isEmpty(entities)) {
            return Lists.newArrayList();
        }

        // 轉 VO
        return entities.stream()
                .map(each -> new TrAlertInboxVO(each))
                .collect(Collectors.toList());

    }

    /**
     * 取得轉寄部門、個人所有被轉寄人員
     * 
     * @param requireSid
     * @return
     */
    public Set<Integer> findReciveUserSids(String requireSid) {
        // ====================================
        // 查詢轉寄資訊
        // ====================================
        List<TrAlertInbox> alertInboxs = this.trAlertInboxRepository.findByRequireSidAndStatus(requireSid, Activation.ACTIVE);

        // ====================================
        // 收集轉寄人員
        // ====================================

        Set<Integer> allReceiveUserSids = Sets.newHashSet();
        for (TrAlertInbox trAlertInbox : alertInboxs) {

            // ----------------------
            // 轉寄部門
            // ----------------------
            if (ForwardType.FORWARD_DEPT.equals(trAlertInbox.getForwardType())) {

                Set<Integer> userSids = WkUserCache.getInstance().findUserSidByOrgsWithManager(
                        Lists.newArrayList(trAlertInbox.getReceiveDep()), Activation.ACTIVE);

                if (WkStringUtils.notEmpty(userSids)) {
                    allReceiveUserSids.addAll(userSids);
                }

            }

            // ----------------------
            // 轉寄人員
            // ----------------------
            if (ForwardType.FORWARD_MEMBER.equals(trAlertInbox.getForwardType())) {
                if (trAlertInbox.getReceive() != null) {
                    allReceiveUserSids.add(trAlertInbox.getReceive());
                }
            }
        }

        return allReceiveUserSids.stream()
                .filter(userSid -> WkUserUtils.isActive(userSid))
                .collect(Collectors.toSet());

    }

    /**
     * 查詢轉寄個人的收件者
     * 
     * @param requireNo 需求單號
     * @param sender    寄件者
     * @return
     */
    public List<Integer> findReceiverByRequireNoAndSender(
            String requireNo,
            Integer sender) {

        // 查詢
        List<Integer> recivers = this.trAlertInboxRepository.findReceiverByRequireNoAndSender(requireNo, sender);

        if (recivers == null) {
            return Lists.newArrayList();
        }

        return recivers;

    }

    public List<TrAlertInbox> findByInboxByInboxGroupSid(String alert_group_sid) {
        return trAlertInboxRepository.findByInboxByInboxGroupSid(alert_group_sid);
    }

    public void readWorkInbox(String wr_Sid, Integer loginUserSid, Integer loginUserDepSid) {
        trAlertInboxRepository.updateReadRecordStatus(ReadRecordStatus.READ, new Date(), wr_Sid,
                ReadRecordStatus.UNREAD, loginUserSid, loginUserDepSid, ForwardType.FORWARD_DEPT, ForwardType.FORWARD_MEMBER);
    }

    @Transactional(rollbackFor = Exception.class)
    public TrAlertInbox save(TrAlertInbox entity) {
        return trAlertInboxRepository.save(entity);
    }

    @Transactional(rollbackFor = Exception.class)
    public void save(List<TrAlertInbox> entities) {
        this.trAlertInboxRepository.save(entities);
    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(TrAlertInbox entity) {
        trAlertInboxRepository.delete(entity);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deleteAll(List<TrAlertInbox> entities) {
        trAlertInboxRepository.delete(entities);
    }

    public TrAlertInbox findByInboxByRequireSidAndReceiveDep(String requireSid, List<Integer> receiveDeptSids, Integer targetOrg) {
        List<TrAlertInbox> resultList = findByInboxByRequireSidAndReceiveDepts(requireSid, receiveDeptSids);
        TrAlertInbox isExists = findByInboxByRequireSidAndReceiveDep(requireSid, targetOrg);
        if (CollectionUtils.isNotEmpty(resultList) && isExists == null) {
            return resultList.get(0);
        }
        return null;
    }

    public List<TrAlertInbox> findByInboxByRequireSidAndReceiveDepts(String requireSid, List<Integer> targetOrgs) {
        return trAlertInboxRepository.findByInboxByRequireSidAndReceiveDepts(requireSid, targetOrgs);
    }

    public TrAlertInbox findByInboxByRequireSidAndReceiveDep(String requireSid, Integer targetOrg) {
        List<TrAlertInbox> resultList = trAlertInboxRepository.findByInboxByRequireSidAndReceiveDep(requireSid, targetOrg);
        if (CollectionUtils.isNotEmpty(resultList)) {
            return resultList.get(0);
        }
        return null;
    }
}
