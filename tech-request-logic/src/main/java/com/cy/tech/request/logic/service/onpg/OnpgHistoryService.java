/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.onpg;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.RequireReadRecordHelper;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.repository.onpg.WorkOnpgHistoryRepo;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.onpg.WorkOnpgAttachment;
import com.cy.tech.request.vo.onpg.WorkOnpgCheckMemo;
import com.cy.tech.request.vo.onpg.WorkOnpgCheckRecord;
import com.cy.tech.request.vo.onpg.WorkOnpgCheckRecordReply;
import com.cy.tech.request.vo.onpg.WorkOnpgHistory;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgHistoryBehavior;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.vo.AttachmentService;
import com.google.common.collect.Ordering;
import com.google.common.primitives.Longs;

/**
 * @author shaun
 */
@Component
public class OnpgHistoryService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2654059911312060319L;
    @Autowired
    private RequireService reqService;
    @Autowired
    private OnpgService opService;
    @Autowired
    private OnpgShowService opsService;
    @Autowired
    @Qualifier("onpg_history_attach")
    private AttachmentService<WorkOnpgAttachment, WorkOnpgHistory, String> attachService;
    @Autowired
    private WkJsoupUtils jsoupUtils;
    @Autowired
    private WorkOnpgHistoryRepo historyDao;

    @Autowired
    private transient RequireReadRecordHelper requireReadRecordHelper;

    @Transactional(readOnly = true)
    public List<WorkOnpgHistory> findHistorys(WorkOnpg onpg) {
        try {
            onpg.getHistorys().size();
        } catch (LazyInitializationException e) {
            // log.debug("findHistorys lazy init error :" + e.getMessage(), e);
            onpg.setHistorys(historyDao.findByOnpg(onpg));
        }
        return onpg.getHistorys();
    }

    /**
     * @param onpg
     * @param createUser
     * @return
     */
    public WorkOnpgHistory createEmptyHistory(WorkOnpg onpg, User createUser) {
        return this.createEmptyHistory(onpg, createUser, new Date());
    }

    /**
     * @param onpg
     * @param createUser
     * @param sysDate
     * @return
     */
    public WorkOnpgHistory createEmptyHistory(WorkOnpg onpg, User createUser, Date sysDate) {
        WorkOnpgHistory history = new WorkOnpgHistory();
        history.setOnpg(onpg);
        history.setOnpgNo(onpg.getOnpgNo());
        history.setSourceType(onpg.getSourceType());
        history.setSourceSid(onpg.getSourceSid());
        history.setSourceNo(onpg.getSourceNo());
        history.setBehaviorStatus(onpg.getOnpgStatus());
        history.setStatus(Activation.ACTIVE);
        history.setCreatedUser(createUser);
        history.setCreatedDate(sysDate);
        history.setUpdatedUser(createUser);
        history.setUpdatedDate(sysDate);
        return history;
    }

    @Transactional(rollbackFor = Exception.class)
    public void update(WorkOnpgHistory history, User executor) {
        history.setUpdatedDate(new Date());
        history.setUpdatedUser(executor);
        historyDao.save(history);
        attachService.findAttachsByLazy(history).forEach(each -> each.setKeyChecked(Boolean.TRUE));
        attachService.linkRelation(history.getAttachments(), history, executor);
        this.sortHistory(history.getOnpg());
    }

    public void sortHistory(WorkOnpg onpg) {
        List<WorkOnpgHistory> historys = this.findHistorys(onpg);
        Ordering<WorkOnpgHistory> ordering = new Ordering<WorkOnpgHistory>() {
            @Override
            public int compare(WorkOnpgHistory left, WorkOnpgHistory right) {
                return Longs.compare(right.getUpdatedDate().getTime(), left.getUpdatedDate().getTime());
            }
        };
        Collections.sort(historys, ordering);
    }

    public void putHistoryToOnpg(WorkOnpg onpg, WorkOnpgHistory history) {
        List<WorkOnpgHistory> infoHistorys = this.findHistorys(onpg);
        infoHistorys.add(history);
    }

    @Transactional(rollbackFor = Exception.class)
    public void createCheckRecordHistory(WorkOnpgCheckRecord checkRecord, User executor) {
        WorkOnpgHistory history = checkRecord.getHistory();
        history.setBehaviorStatus(checkRecord.getOnpg().getOnpgStatus());
        history.setCheckRecord(checkRecord);
        history.setBehavior(this.transHistoryBehavior(checkRecord));
        history.setVisiable(this.canVisiable(history));
        WorkOnpgHistory nH = historyDao.save(history);
        attachService.linkRelation(history.getAttachments(), nH, executor);
        checkRecord.setHistory(nH);
        this.putHistoryToOnpg(checkRecord.getOnpg(), nH);
    }

    private WorkOnpgHistoryBehavior transHistoryBehavior(WorkOnpgCheckRecord checkRecord) {
        return WorkOnpgHistoryBehavior.valueOf(checkRecord.getReplyType().name());
    }

    private Boolean canVisiable(WorkOnpgHistory history) {
        return !history.getBehaviorStatus().equals(opService.findOnpgStatus(history.getOnpg()));
    }

    @Transactional(rollbackFor = Exception.class)
    public void createCheckRecordReplyHistory(WorkOnpgCheckRecordReply ckrReply, User executor) {
        WorkOnpgHistory history = ckrReply.getHistory();
        history.setCheckRecordReply(ckrReply);
        history.setBehavior(WorkOnpgHistoryBehavior.CHECK_RECORD_REPLY);
        WorkOnpgHistory nH = historyDao.save(history);
        attachService.linkRelation(history.getAttachments(), nH, executor);
        ckrReply.setHistory(nH);
    }

    /**
     * 建立 預設完成日異動歷程記錄
     *
     * @param editOnpg
     * @param backupOnpg
     * @param executor
     */
    @Transactional(rollbackFor = Exception.class)
    public void createEsDtChangeHistory(WorkOnpg editOnpg, WorkOnpg backupOnpg, User executor) {
        WorkOnpgHistory history = this.createEmptyHistory(editOnpg, executor);
        history.setBehavior(WorkOnpgHistoryBehavior.MODIFY_ESTIMATE_DT);
        history.setVisiable(Boolean.TRUE);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        history.setReason("預計完成日由" + sdf.format(backupOnpg.getEstablishDate()) + "改至" + sdf.format(editOnpg.getEstablishDate()));
        history.setReasonCss(history.getReason());
        WorkOnpgHistory nH = historyDao.save(history);
        this.putHistoryToOnpg(editOnpg, nH);
        this.sortHistory(editOnpg);
    }

    /**
     * 建立 備註異動歷程記錄
     *
     * @param editOnpg
     * @param executor
     */
    @Transactional(rollbackFor = Exception.class)
    public void createOnpgNoteChangeHistory(WorkOnpg editOnpg, User executor) {
        WorkOnpgHistory history = this.createEmptyHistory(editOnpg, executor);
        history.setBehavior(WorkOnpgHistoryBehavior.MODIFY_MEMO_INFO);
        history.setVisiable(Boolean.TRUE);
        history.setReason("變更備註資訊");
        history.setReasonCss(history.getReason());
        WorkOnpgHistory nH = historyDao.save(history);
        this.putHistoryToOnpg(editOnpg, nH);
        this.sortHistory(editOnpg);
    }

    /**
     * 建立 檢查註記 歷程記錄
     *
     * @param editOnpg
     * @param memo
     */
    @Transactional(rollbackFor = Exception.class)
    public void createCheckMemoHistory(WorkOnpg editOnpg, WorkOnpgCheckMemo memo) {
        WorkOnpgHistory history = memo.getHistory();
        history.setCheckMemo(memo);
        history.setBehavior(WorkOnpgHistoryBehavior.CHECK_MEMO);
        history.setVisiable(this.canVisiable(history));
        WorkOnpgHistory nH = historyDao.save(history);
        memo.setHistory(nH);
        attachService.linkRelation(history.getAttachments(), nH, WkUserCache.getInstance().findBySid(history.getCreatedUser().getSid()));
        this.putHistoryToOnpg(editOnpg, nH);
        this.sortHistory(editOnpg);
    }

    /**
     * ON程式取消
     * 
     * @param history
     * @throws UserMessageException
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveCancelHistory(WorkOnpgHistory history) throws UserMessageException {
        if (opsService.disableCancel(
                reqService.findByReqSid(history.getSourceSid()),
                opService.findByOnpgNo(history.getOnpgNo()),
                WkUserCache.getInstance().findBySid(history.getCreatedUser().getSid()))) {

            throw new UserMessageException("無權限進行操作", InfomationLevel.WARN);

        }
        history.setBehavior(WorkOnpgHistoryBehavior.CANCEL_ONPG);
        history.setBehaviorStatus(WorkOnpgStatus.CANCEL_ONPG);
        history.setVisiable(Boolean.TRUE);
        history.setReason(jsoupUtils.clearCssTag(history.getReasonCss()));
        WorkOnpgHistory nH = historyDao.save(history);

        WorkOnpg onpg = history.getOnpg();
        onpg.setOnpgStatus(WorkOnpgStatus.CANCEL_ONPG);
        onpg.setReadReason(WaitReadReasonType.ONPG_CANCEL);
        onpg.setReadUpdateDate(new Date());
        onpg.setCancelDate(new Date());
        opService.save(onpg, WkUserCache.getInstance().findBySid(history.getCreatedUser().getSid()));

        attachService.linkRelation(history.getAttachments(), nH, WkUserCache.getInstance().findBySid(history.getCreatedUser().getSid()));
        this.putHistoryToOnpg(nH.getOnpg(), nH);
        this.sortHistory(history.getOnpg());

        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.WORKONPG,
                onpg.getSid(),
                history.getCreatedUser().getSid());
    }

    /**
     * 檢查完成
     * @param req
     * @param history
     * @param depSid
     * @param hasSmallCategoryRole
     * @throws IllegalAccessException
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveCompleteHistory(Require req, WorkOnpgHistory history, Integer depSid, boolean hasSmallCategoryRole)
            throws IllegalAccessException {
        Require dbReq = reqService.findByReqObj(req);
        // ====================================
        // 檢查是否有權權限
        // ====================================
        if (opsService.disableCheckComplate(
                dbReq,
                opService.findByOnpgNo(history.getOnpgNo()),
                WkUserCache.getInstance().findBySid(history.getCreatedUser().getSid()),
                depSid, hasSmallCategoryRole)) {
            throw new IllegalAccessException("執行檢查完成失敗。");
        }

        // ====================================
        // 新增 onpg history
        // ====================================
        history.setBehavior(WorkOnpgHistoryBehavior.FINISH_CHECK);
        history.setBehaviorStatus(WorkOnpgStatus.CHECK_COMPLETE);
        history.setVisiable(Boolean.TRUE);
        WorkOnpgHistory nH = historyDao.save(history);

        // ====================================
        // 更新 onpg 主檔
        // ====================================
        WorkOnpg onpg = opService.copyOnpg(history.getOnpg());
        onpg.setOnpgStatus(WorkOnpgStatus.CHECK_COMPLETE);
        onpg.setReadReason(WaitReadReasonType.ONPG_CHECK_COMPLETE);
        onpg.setReadUpdateDate(new Date());
        onpg.setFinishDate(new Date());
        opService.save(onpg, WkUserCache.getInstance().findBySid(history.getCreatedUser().getSid()));

        // ====================================
        // 附件
        // ====================================
        attachService.linkRelation(history.getAttachments(), nH, WkUserCache.getInstance().findBySid(history.getCreatedUser().getSid()));

        this.putHistoryToOnpg(nH.getOnpg(), nH);
        this.sortHistory(history.getOnpg());
        
        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.WORKONPG,
                onpg.getSid(),
                history.getCreatedUser().getSid());
    }

    /**
     * 新增可見的on程式單記錄
     * 
     * @param workOnpgNo on程式單號
     * @param behavior   歷程行為
     * @param reasonCss  顯示內容
     * @param executor   執行者
     * @throws SystemOperationException 錯誤時拋出
     */
    @Transactional(rollbackFor = Exception.class)
    public void createCanViewWorkOnpgHistory(
            String workOnpgNo,
            WorkOnpgHistoryBehavior behavior,
            String reasonCss,
            User executor) throws SystemOperationException {

        WorkOnpg editOnpg = this.opService.findByOnpgNo(workOnpgNo);
        if (editOnpg == null) {
            throw new SystemOperationException("找不到ON程式單資料:[" + workOnpgNo + "]", InfomationLevel.ERROR);
        }

        WorkOnpgHistory history = this.createEmptyHistory(editOnpg, executor);
        history.setBehavior(behavior);
        history.setVisiable(Boolean.TRUE);
        history.setReason(WkJsoupUtils.getInstance().htmlToText(reasonCss));
        history.setReasonCss(reasonCss);
        WorkOnpgHistory nH = historyDao.save(history);
        this.putHistoryToOnpg(editOnpg, nH);
        this.sortHistory(editOnpg);
    }

    /**
     * @param workOnpgHistory
     */
    public void save(WorkOnpgHistory workOnpgHistory) {
        this.historyDao.save(workOnpgHistory);
    }

}
