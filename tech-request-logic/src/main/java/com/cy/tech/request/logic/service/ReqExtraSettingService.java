package com.cy.tech.request.logic.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.vo.User;
import com.cy.tech.request.repository.require.ReqExtraSettingRepository;
import com.cy.tech.request.vo.enums.ExtraSettingType;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.setting.RequireExtraSetting;
import com.cy.work.common.utils.WkStringUtils;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ReqExtraSettingService implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 72768718664492024L;
    @Autowired
	private transient ReqExtraSettingRepository reqExtraSettingDao;
	@Autowired
	private transient RequireService requireService;

	@Transactional(rollbackFor = Exception.class)
	public Boolean isNcsUnDone(String sid) {
		RequireExtraSetting enity = reqExtraSettingDao.findValueBySidAndCode(sid, RequireTraceType.NCS_CHECK_OK.name());
		if (enity != null) {
			return "N".equals(enity.getValue().toUpperCase());
		}

		return false;
	}

	/**
	 * 設定需求單的 SABA 欄位設定
	 * 
	 * @param requireSid
	 * @param sabaEabled
	 * @param execUser
	 * @param sysDate
	 */
	public void saveSabaByRequire(
	        String requireSid,
	        boolean sabaEabled,
	        User execUser,
	        Date sysDate) {

		// ====================================
		// 查詢需求單物件
		// ====================================
		Require require = requireService.findByReqSid(requireSid);

		// ====================================
		// 查詢已存在資料
		// ====================================
		List<RequireExtraSetting> settings = this.reqExtraSettingDao.findByRequire(require);

		// ====================================
		// 新增或刪除
		// ====================================
		if (WkStringUtils.notEmpty(settings)) {
			// 已存在更新設定
			this.updateSabaSetting(settings, sabaEabled, execUser, sysDate);
		} else {
			this.createSabaSetting(require, sabaEabled, execUser, sysDate);
		}
	}

	/**
	 * @param requireSid
	 * @param sabaEabled
	 * @param settings
	 */
	private void updateSabaSetting(
	        List<RequireExtraSetting> settings,
	        boolean sabaEabled,
	        User execUser,
	        Date sysDate) {
		String value = sabaEabled ? "Y" : "N";

		for (RequireExtraSetting setting : settings) {

			// 僅異動『是否開放沙巴』
			if (!ExtraSettingType.SABA_ENABLED.getCode().equals(setting.getCode())) {
				continue;
			}

			// 設定值不同時才更新
			if (value.equals(setting.getValue())) {
				continue;
			}

			setting.setValue(value);
			setting.setUpdatedUser(execUser);
			setting.setCreatedDate(sysDate);
			this.reqExtraSettingDao.save(setting);

			log.info("No. {} update SABA config to extra table. set {}",
			        setting.getRequire().getRequireNo(),
			        value);
		}
	}

	/**
	 * @param require
	 * @param sabaEabled
	 * @param execUser
	 * @param sysDate
	 */
	private void createSabaSetting(
	        Require require,
	        boolean sabaEabled,
	        User execUser,
	        Date sysDate) {

		log.info("No. {} add SABA config to extra table.", require.getRequireNo());

		// ====================================
		// insert saba setting
		// ====================================
		RequireExtraSetting model = new RequireExtraSetting();
		model.setRequire(require);
		model.setCode(ExtraSettingType.SABA_ENABLED.getCode());
		model.setName(ExtraSettingType.SABA_ENABLED.getName());
		model.setValue(sabaEabled ? "Y" : "N");
		model.setCreatedDate(new Date());
		model.setCreatedUser(require.getCreatedUser());
		
		this.reqExtraSettingDao.save(model);

		// ====================================
		// insert NCS confirm setting
		// ====================================
		model = new RequireExtraSetting();
		model.setRequire(require);
		model.setCode(ExtraSettingType.NCS_CONFIRM.getCode());
		model.setName(ExtraSettingType.NCS_CONFIRM.getName());
		model.setValue("N");
		model.setCreatedDate(new Date());
		model.setCreatedUser(require.getCreatedUser());
		
		this.reqExtraSettingDao.save(model);
	}

}
