/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.search.view.InboxView;
import com.cy.tech.request.logic.vo.TransDepBacksageTo;
import com.cy.tech.request.repository.require.AlertInboxRepository;
import com.cy.tech.request.repository.require.AlertInboxSendGroupRepository;
import com.cy.tech.request.repository.require.ReqModifyRepo;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.ForwardType;
import com.cy.tech.request.vo.enums.InboxType;
import com.cy.tech.request.vo.enums.ReadRecordStatus;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.require.AlertInbox;
import com.cy.tech.request.vo.require.AlertInboxSendGroup;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.TrAlertInbox;
import com.cy.tech.request.vo.value.to.ReadRecordGroupAdvanceTo;
import com.cy.work.backend.logic.WorkBackendParamService;
import com.cy.work.backend.vo.enums.WkBackendParam;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.logic.lib.manager.to.WorkCommonReadRecordTo;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkEntityUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.common.vo.value.to.ReadRecordTo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * 收件夾服務
 *
 * @author shaun
 */
@Slf4j
@Component
public class AlertInboxService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6331797276705438234L;
    @Autowired
    private CommonService commonService;
    @Autowired
    private RequireService requireService;
    @Autowired
    private AlertInboxSendGroupRepository alertSendGroupDao;
    @Autowired
    private ForwardService forwardService;
    @Autowired
    private AlertInboxRepository alertInboxDao;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private UserService userService;
    @Autowired
    private WkJsoupUtils jsoupUtils;
    @Autowired
    private WkEntityUtils entityUtils;
    @Autowired
    private ReqModifyRepo reqModifyDao;
    @PersistenceContext
    transient private EntityManager em;
    @Autowired
    private TrAlertInboxService trAlertInboxService;
    @Autowired
    private transient WorkBackendParamService workBackendParamService;
    @Autowired
    private transient RequireReadRecordHelper requireReadRecordHelper;

    /**
     * 建立部門轉發 收件資訊
     *
     * @param require
     * @param sender
     * @param receiveDeps
     */
    @Transactional(rollbackFor = Exception.class)
    public void createInboxDep(Require require, User sender, List<Org> receiveDeps) {
        List<AlertInboxSendGroup> sendGroups = alertSendGroupDao.findByRequireNoAndForwardTypeAndSend(require.getRequireNo(), ForwardType.FORWARD_DEPT, sender);
        if (sendGroups == null || sendGroups.isEmpty()) {
            this.createDeptInboxSendGroup(require, sender, receiveDeps);
            return;
        }
        if (sendGroups.size() > 1) {
            log.error("需求單號：" + require.getRequireNo() + " 轉發者：" + sender.getName()
                    + " 超過1個部門轉發群組，請進行檢查...(需求單一個人僅有一個轉發部門群組!!)");
        }
        this.updateDeptInboxSendGroup(require, sendGroups.get(0), sender, receiveDeps);
    }

    private void createDeptInboxSendGroup(Require require, User sender, List<Org> receiveDeps) {
        AlertInboxSendGroup sendGroup = this.createEmptyDeptInboxGroup(require, sender, receiveDeps);
        AlertInboxSendGroup group = new AlertInboxSendGroup();
        group.setSid(sendGroup.getSid());
        List<AlertInbox> alertInboxs = receiveDeps.stream().map(each -> {
            AlertInbox inbox = this.createEmptyInbox(require, sender);
            inbox.setForwardType(ForwardType.FORWARD_DEPT);
            inbox.setInboxType(InboxType.INCOME_DEPT);
            inbox.setReceiveDep(new Org(each.getSid()));
            inbox.setSendInboxGroup(group);
            return inbox;
        }).collect(Collectors.toList());
        alertInboxDao.save(alertInboxs);
    }

    private AlertInboxSendGroup createEmptyDeptInboxGroup(Require r, User sender, List<Org> receiveDeps) {
        AlertInboxSendGroup sendGroup = new AlertInboxSendGroup();
        sendGroup.setRequire(r);
        sendGroup.setRequireNo(r.getRequireNo());
        sendGroup.setForwardType(ForwardType.FORWARD_DEPT);
        sendGroup.setSendTo(WkOrgUtils.findNameByOrgs(receiveDeps, "、"));
        sendGroup.setStatus(Activation.ACTIVE);
        sendGroup.setCreatedDate(new Date());
        sendGroup.setCreatedUser(sender);
        return alertSendGroupDao.save(sendGroup);
    }

    private void updateDeptInboxSendGroup(Require require, AlertInboxSendGroup sendGroup, User sender, List<Org> receiveDeps) {
        List<Org> filterOrg = forwardService.findReceiveDepByRequireAndStatus(require, Activation.ACTIVE);
        if (filterOrg != null) {
            receiveDeps.removeAll(filterOrg);
        }

        AlertInboxSendGroup group = new AlertInboxSendGroup();
        group.setSid(sendGroup.getSid());
        List<AlertInbox> alertInboxs = receiveDeps.stream().map(each -> {
            AlertInbox inbox = this.createEmptyInbox(require, sender);
            inbox.setForwardType(ForwardType.FORWARD_DEPT);
            inbox.setInboxType(InboxType.INCOME_DEPT);
            inbox.setReceiveDep(new Org(each.getSid()));
            inbox.setSendInboxGroup(group);
            return inbox;
        }).collect(Collectors.toList());
        alertInboxDao.save(alertInboxs);

        List<Org> currReceiveDeps = alertInboxs.stream()
                .filter(alert -> alert.getStatus().equals(Activation.ACTIVE))
                .map(alert -> alert.getReceiveDep())
                .collect(Collectors.toList());

        sendGroup.setSendTo(WkOrgUtils.findNameByOrgs(currReceiveDeps, "、"));
        sendGroup.setUpdatedUser(sender);
        sendGroup.setUpdatedDate(new Date());
        alertSendGroupDao.updateSendToStr(sendGroup, sendGroup.getSendTo(), sender, new Date());
    }

    @Transactional(rollbackFor = Exception.class)
    public void inactiveInbox(Require require, User executor, List<AlertInbox> inboxs) {
        List<User> inactiveUser = Lists.newArrayList();
        List<Org> inactiveOrg = Lists.newArrayList();
        inboxs.forEach(each -> {
            if (each.getForwardType().equals(ForwardType.FORWARD_MEMBER)) {
                inactiveUser.add(each.getReceive());
            }
            if (each.getForwardType().equals(ForwardType.FORWARD_DEPT)) {
                inactiveOrg.add(each.getReceiveDep());
            }
        });
        if (!inactiveUser.isEmpty()) {
            this.inactiveInboxMember(require, executor, inactiveUser);
        }
        if (!inactiveOrg.isEmpty()) {
            this.inactiveInboxDep(require, executor, inactiveOrg);
        }
    }

    /**
     * 停用轉發部門 收件資訊
     *
     * @param require
     * @param executor
     * @param inactiveDep
     */
    @Transactional(rollbackFor = Exception.class)
    public void inactiveInboxDep(Require require, User executor, List<Org> inactiveDep) {
        List<User> inactiveUsers = userService.findUserByPrimaryOrgIn(Activation.ACTIVE, inactiveDep);
        if (this.cantInactive(require, inactiveUsers)) {
            return;
        }
        List<AlertInbox> requireDepsInbox = alertInboxDao.findByRequireAndStatusAndReceiveDeps(require, Activation.ACTIVE, inactiveDep);
        requireDepsInbox.forEach(alert -> {
            alert.setStatus(Activation.INACTIVE);
            alert.setUpdatedUser(new User(executor.getSid()));
            alert.setUpdatedDate(new Date());
        });
        alertInboxDao.save(requireDepsInbox);
    }

    private boolean cantInactive(Require require, List<User> inactiveUsers) {
        Require nRre = requireService.findByReqObj(require);
        Map<String, ReadRecordTo> records = nRre.getReadRecordGroup().getRecords();
        return inactiveUsers.stream().anyMatch(each -> records.containsKey(String.valueOf(each.getSid()))
                && records.get(String.valueOf(each.getSid())).getType().equals(ReadRecordType.HAS_READ));
    }

    @Transactional(rollbackFor = Exception.class)
    public void inactiveInboxMember(Require require, User executor, List<User> inactiveUsers) {
        if (this.cantInactive(require, inactiveUsers)) {
            return;
        }
        List<AlertInbox> memberInboxs = alertInboxDao.findByRequireAndStatusAndReceive(require, Activation.ACTIVE, inactiveUsers);
        memberInboxs.forEach(alert -> {
            alert.setStatus(Activation.INACTIVE);
            alert.setUpdatedUser(new User(executor.getSid()));
            alert.setUpdatedDate(new Date());
        });
        alertInboxDao.save(memberInboxs);
    }

    /**
     * 建立 空 收件資訊
     *
     * @param reuqire
     * @param sender
     * @param forwardType
     * @param inboxType
     * @param messageCss
     * @param receiveInfoTo
     * @return
     */
    private AlertInbox createEmptyInbox(Require reuqire, User executor) {
        AlertInbox inbox = new AlertInbox();
        Require req = new Require();
        req.setSid(reuqire.getSid());
        inbox.setRequire(req);
        inbox.setRequireNo(reuqire.getRequireNo());
        inbox.setSender(new User(executor.getSid()));
        inbox.setSenderDep(new Org(WkUserCache.getInstance().findBySid(executor.getSid()).getPrimaryOrg().getSid()));
        inbox.setStatus(Activation.ACTIVE);
        inbox.setCreatedDate(new Date());
        inbox.setCreatedUser(new User(executor.getSid()));
        return inbox;
    }

    /**
     * 建立個人轉發 收件資訊<BR/>
     *
     * @param require
     * @param sender
     * @param receiveMembers
     * @param messageCss
     */
    @Transactional(rollbackFor = Exception.class)
    public void createInboxMember(
            String requireNo,
            Integer senderUserSid,
            List<Integer> receiverUserSids,
            String messageCss) {

        // ================================
        // 準備資料
        // ================================
        // 需求單主檔
        Require require = requireService.findByReqNo(requireNo);
        // 寄件者資料
        User sender = WkUserCache.getInstance().findBySid(senderUserSid);

        Date sysTime = new Date();

        // ================================
        // tr_alert_inbox_send_group
        // ================================
        AlertInboxSendGroup sendGroup = this.createEmptyMemberInboxGroup(
                require,
                WkUserCache.getInstance().findBySid(senderUserSid),
                receiverUserSids,
                messageCss);

        String groupSid = sendGroup.getSid();

        // ================================
        // tr_alert_inbox_send_group
        // ================================
        for (Integer receiverUserSid : receiverUserSids) {

            // 寄件者資料
            User receiver = WkUserCache.getInstance().findBySid(receiverUserSid);

            TrAlertInbox inbox = new TrAlertInbox();
            inbox.setRequireSid(require.getSid());
            inbox.setRequireNo(require.getRequireNo());
            inbox.setForwardType(ForwardType.FORWARD_MEMBER);
            inbox.setSender(senderUserSid);
            inbox.setSenderDep(sender.getPrimaryOrg().getSid());
            inbox.setInboxType(this.prepareInboxType(senderUserSid, receiverUserSid));
            inbox.setReceive(receiverUserSid);

            inbox.setReceiveDep(receiver.getPrimaryOrg().getSid());
            inbox.setMessage(jsoupUtils.clearCssTag(messageCss));
            inbox.setMessageCss(messageCss);
            inbox.setAlert_group_sid(groupSid);

            // 非發送者變為待閱
            if (!sender.getSid().equals(receiverUserSid)) {
                inbox.setRead_record(ReadRecordStatus.UNREAD);
            } else {
                inbox.setRead_record(ReadRecordStatus.READ);
                inbox.setRead_dt(sysTime);
            }

            // insert
            this.trAlertInboxService.createWorkInbox(inbox, senderUserSid);
        }

        // ================================
        // 更新需求單主檔
        // ================================
        require.setHasForwardMember(true); // 轉寄個人時, 必定為 true
        require.setUpdatedUser(sender);
        require.setUpdatedDate(sysTime);
        this.reqModifyDao.updateForwardMember(require, true, sender, sysTime);

        // ================================
        // 更新閱讀記錄檔 (待閱讀)
        // ================================
        if (WkStringUtils.notEmpty(receiverUserSids)) {
            this.requireReadRecordHelper.saveUsersToWaitReadWithoutExecUser(
                    FormType.REQUIRE,
                    require.getSid(),
                    receiverUserSids,
                    senderUserSid);
        }

    }

    public AlertInboxSendGroup createEmptyMemberInboxGroup(Require r, User sender, List<Integer> receiveMembers, String messageCss) {
        AlertInboxSendGroup sendGroup = new AlertInboxSendGroup();
        sendGroup.setRequire(r);
        sendGroup.setRequireNo(r.getRequireNo());
        sendGroup.setMessage(jsoupUtils.clearCssTag(messageCss));
        sendGroup.setMessageCss(messageCss);
        sendGroup.setForwardType(ForwardType.FORWARD_MEMBER);
        sendGroup.setSendTo(WkUserUtils.findNameBySid(receiveMembers, "、"));
        sendGroup.setStatus(Activation.ACTIVE);
        sendGroup.setCreatedDate(new Date());
        sendGroup.setCreatedUser(sender);
        return alertSendGroupDao.save(sendGroup);
    }

    /**
     * 準備收件夾類別
     * 
     * @param senderUserSid   寄件者
     * @param receiverUserSid 收件者
     * @return InboxType
     */
    private InboxType prepareInboxType(Integer senderUserSid, Integer receiverUserSid) {
        // ====================================
        // 取得 bigBoss 參數
        // ====================================
        // 查詢
        List<Integer> bigBossUserSids = this.workBackendParamService.findIntsByKeyword(
                WkBackendParam.BIG_BOSS_SIDS);

        // ====================================
        // 判斷關係
        // ====================================
        int relation = WkUserUtils.compareUserRelation(
                senderUserSid,
                receiverUserSid,
                bigBossUserSids.stream().collect(Collectors.toSet()));

        // ====================================
        // 回傳對應 InboxType
        // ====================================
        if (relation > 0) {
            // sender > receiver 指示
            return InboxType.INCOME_INSTRUCTION;
        } else if (relation < 0) {
            // sender < receiver 呈報
            return InboxType.INCOME_REPORT;
        } else {
            // 平級- 轉寄
            return InboxType.INCOME_MEMBER;
        }

    }

    @Transactional(readOnly = true)
    public AlertInboxSendGroup findSendGroup(String sid) {
        return alertSendGroupDao.findOne(sid);
    }

    @Transactional(readOnly = true)
    public List<AlertInboxSendGroup> findSendGroupByRequireAndSenderIn(Require require, List<User> senders) {
        return alertInboxDao.findSendGroupByRequireAndSenderIn(require, senders);
    }

    public ReadRecordGroupAdvanceTo createInboxReadRecord(
            List<WorkCommonReadRecordTo> readRecordTos,
            List<AlertInbox> inboxs
    ) {

        ReadRecordGroupAdvanceTo readRecordGroupAdvanceTo = new ReadRecordGroupAdvanceTo();
        if (inboxs == null || inboxs.isEmpty()) {
            return readRecordGroupAdvanceTo;
        }

        // ====================================
        // 收集轉寄部門 user
        // ====================================
        Set<Integer> resceiveUserSids = Sets.newHashSet();
        for (AlertInbox inbox : inboxs) {

            if (!Activation.ACTIVE.equals(inbox.getStatus())) {
                continue;
            }

            // 轉發部門
            if (inbox.getForwardType().equals(ForwardType.FORWARD_DEPT)) {
                if (inbox.getReceiveDep() != null) {
                    Set<Integer> orgUserSids = WkUserCache.getInstance().findUserSidByOrgsWithManager(
                            Lists.newArrayList(inbox.getReceiveDep().getSid()),
                            Activation.ACTIVE);
                    if (WkStringUtils.notEmpty(orgUserSids)) {
                        resceiveUserSids.addAll(orgUserSids);
                    }
                }
            }
            // 轉發個人
            else {
                if (inbox.getReceive() != null) {
                    resceiveUserSids.add(inbox.getReceive().getSid());
                }
            }
        }
        // ====================================
        // 以 userSid 做 index
        // ====================================
        Map<Integer, WorkCommonReadRecordTo> readRecordToMapByUserSid = WkCommonUtils.safeStream(readRecordTos)
                .collect(Collectors.toMap(
                        WorkCommonReadRecordTo::getUserSid,
                        each -> each));

        // ====================================
        // 組
        // ====================================
        List<WorkCommonReadRecordTo> resultReadRecordTos = Lists.newArrayList();
        if (WkStringUtils.notEmpty(readRecordTos)) {
            resultReadRecordTos.addAll(readRecordTos);
        }

        // 加入閱讀記錄中不存在的 user
        for (Integer resceiveUserSid : resceiveUserSids) {
            if (readRecordToMapByUserSid.containsKey(resceiveUserSid)) {
                continue;
            }
            WorkCommonReadRecordTo readRecordTo = new WorkCommonReadRecordTo();
            readRecordTo.setUserSid(resceiveUserSid);
            resultReadRecordTos.add(readRecordTo);
        }

        readRecordGroupAdvanceTo.setValues(resultReadRecordTos);
        
        return readRecordGroupAdvanceTo;

    }

    @Transactional(readOnly = true)
    public AlertInbox findOne(String sid) {
        return alertInboxDao.findOne(sid);
    }

    @Transactional(readOnly = true)
    public List<AlertInbox> findLazyInboxs(AlertInboxSendGroup sendGroup) {
        try {
            sendGroup.getInboxs().size();
        } catch (LazyInitializationException e) {
            // log.debug("findLazyInboxs lazy init error :" + e.getMessage(), e);
            sendGroup.setInboxs(alertInboxDao.findBySendInboxGroupOrderByCreatedDateDesc(sendGroup));
        }
        return sendGroup.getInboxs();
    }

    @Transactional(readOnly = true)
    public Map<AlertInboxSendGroup, List<AlertInbox>> getCacheByForwardType(ForwardType forwardType) {
        List<Object[]> nativeResult = alertInboxDao.findByForwardTypeWithNative(forwardType.name());
        return nativeResult.stream()
                .map(each -> {
                    AlertInbox inbox = new AlertInbox();
                    inbox.setSid((String) each[0]);
                    inbox.setCreatedDate((Date) each[1]);
                    Org receiveDep = new Org();
                    receiveDep.setSid((Integer) each[2]);
                    inbox.setReceiveDep(receiveDep);
                    inbox.setForwardType(ForwardType.valueOf((String) each[3]));
                    Integer ordinal = (Integer) each[4];
                    inbox.setStatus(Activation.values()[ordinal]);
                    inbox.setUpdatedDate((Date) each[5]);
                    AlertInboxSendGroup group = new AlertInboxSendGroup();
                    group.setSid((String) each[6]);
                    inbox.setSendInboxGroup(group);
                    return inbox;
                })
                .collect(
                        Collectors.groupingBy(each -> each.getSendInboxGroup()));
    }

    /**
     * 查詢登入者針對此張需求單是否有任何收件訊息
     *
     * @param require
     * @param user
     * @param inboxType
     * @return
     */
    @Transactional(readOnly = true)
    public boolean hasAnyInbox(Require require, User login, InboxType inboxType) {
        return alertInboxDao.hasInbox(require.getSid(), login.getSid(), inboxType.name());
    }

    /**
     * 查詢收件夾TAB資訊
     *
     * @param require
     * @param login
     * @param inboxType
     * @return
     */
    @Transactional(readOnly = true)
    public List<InboxView> findReqInboxView(Require require, User login, InboxType inboxType) {
        StringBlobConverter blobConverter = new StringBlobConverter();
        List<Object[]> result = alertInboxDao.findReqInboxWithNative(require.getSid(), login.getSid(), inboxType.name());
        List<InboxView> viewResult = Lists.newArrayList();
        for (int i = 0; i < result.size(); i++) {
            Object[] record = (Object[]) result.get(i);
            String requireSid = (String) record[0];
            String requireNo = (String) record[1];
            String alertSid = (String) record[2];
            String alertGroupSid = (String) record[3];
            String inboxTypeStr = (String) record[4];
            Integer senderSid = (Integer) record[5];
            Integer receiveSid = (Integer) record[6];
            Date createdDate = (Date) record[7];
            byte[] messageBlob = (byte[]) record[8];

            InboxView v = new InboxView();
            v.setRequireSid(requireSid);
            v.setRequireNo(requireNo);
            v.setAlertSid(alertSid);
            v.setAlertGroupSid(alertGroupSid);
            v.setInboxType(InboxType.valueOf(inboxTypeStr));
            v.setSender(WkUserCache.getInstance().findBySid(senderSid));
            v.setReceive(WkUserCache.getInstance().findBySid(receiveSid));
            v.setCreatedDate(createdDate);
            v.setMessageCss(blobConverter.convertToEntityAttribute(messageBlob));

            viewResult.add(v);
        }
        return viewResult;
    }

    @Transactional(readOnly = true)
    public List<String> findSidsByRequire(Require require, User login, InboxType inboxType) {
        return alertInboxDao.findSidsByRequire(require.getSid(), login.getSid(), inboxType.name());
    }

    public List<TransDepBacksageTo> findBackstageModifyItems(Integer sourceOrg, Integer targetOrg, List<String> reqStatus, List<String> reqNos) {
        String findQuery = "SELECT tr.require_sid,"
                + "       tr.require_no,"
                + "       tr.require_status,"
                + "       tr.create_dt,"
                + "       ai.receive_dep,"
                + "       ai.alert_sid,"
                + "       ai.alert_group_sid FROM tr_alert_inbox ai "
                + "  LEFT JOIN tr_require tr ON tr.require_sid = ai.require_sid "
                + "  WHERE ai.inbox_type = 'INCOME_DEPT' "
                + "    AND ai.status = 0 "
                + "    AND ai.receive_dep = :sourceOrg "
                + "    AND tr.require_status IN (:reqStatus) "
                + "    AND ai.require_no IN (:reqNos) "
                + "    AND ai.alert_group_sid NOT IN ( "// 去除已存在的目標
                + "        SELECT aig.alert_group_sid FROM tr_alert_inbox_send_group aig "
                + "          INNER JOIN tr_alert_inbox ai ON aig.alert_group_sid = ai.alert_group_sid"
                + "         WHERE ai.receive_dep = :targetOrg"
                + "           AND ai.require_no IN (:reqNos) "
                + "    )"
                + " GROUP BY require_sid,alert_group_sid "
                + " ORDER BY ai.create_dt DESC ";
        Map<String, Object> param = Maps.newHashMap();
        param.put("sourceOrg", sourceOrg);
        param.put("targetOrg", targetOrg);
        param.put("reqStatus", reqStatus);
        param.put("reqNos", reqNos);
        Query query = em.createNativeQuery(findQuery);
        param.entrySet().stream().forEach(entry -> query.setParameter(entry.getKey(), entry.getValue()));
        @SuppressWarnings("unchecked")
        List<Object> result = query.getResultList();
        return result.stream()
                .map(obj -> this.bulidTransDepBacksageTo((Object[]) obj))
                .collect(Collectors.toList());
    }

    private TransDepBacksageTo bulidTransDepBacksageTo(Object[] array) {
        int idx = 0;
        String requireSid = (String) array[idx++];
        String requireNo = (String) array[idx++];
        String reqType = (String) array[idx++];
        Date createDt = (Date) array[idx++];
        Integer sourceOrgSid = (Integer) array[idx++];
        String aiSid = (String) array[idx++];
        String aiGroupSid = (String) array[idx++];
        return new TransDepBacksageTo(
                requireSid,
                requireNo,
                RequireStatusType.valueOf(reqType),
                createDt,
                sourceOrgSid,
                aiSid,
                aiGroupSid,
                true);
    }

    /**
     * 後台需進行轉換轉寄內容顯示
     *
     * @param items
     * @param sourceOrg
     * @param targetOrg
     * @param isCnt
     * @return
     */
    public String htmlHandlerMsg(List<TransDepBacksageTo> items, Integer sourceOrg, Integer targetOrg, boolean isCnt) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        StringBuilder sb = new StringBuilder();
        items.forEach(each -> {
            sb.append("需求單ID  ：").append(each.getRequireSid()).append("<BR/>");
            sb.append("需求單號  ：").append(each.getRequireNo()).append("<BR/>");
            sb.append("製作進度  ：").append(commonService.get(each.getReqType())).append("<BR/>");
            sb.append("建單日期  ：").append(sdf.format(each.getCreateDt())).append("<BR/>");
            sb.append("收件ID    ：").append(each.getAlertInboxSid()).append("<BR/>");
            sb.append("收件群組ID：").append(each.getAlertInboxGroupSid()).append("<BR/>");
            if (!isCnt) {
                sb.append("異動成功：").append(each.isSucess()).append("\"<BR/>");
            }
            sb.append("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░<BR/>");
        });
        String handlerCntStr = isCnt ? "預計處理筆數：" : "處理筆數：";
        sb.append(handlerCntStr).append(items.size()).append("<BR/>");
        sb.append("來源對應部門：").append(orgService.getOrgName(sourceOrg)).append("<BR/>");
        sb.append("目標新增部門：").append(orgService.getOrgName(targetOrg)).append("<BR/>");
        if (!isCnt) {
            sb.append("失敗筆數：").append(items.stream().filter(item -> !item.isSucess()).count()).append("<BR/>");
        }
        return sb.toString();
    }

    /**
     * 後台進行轉換轉寄時記錄下載
     *
     * @param items
     * @param sourceOrg
     * @param targetOrg
     * @return
     */
    public String downloadHandlerMsg(List<TransDepBacksageTo> items, Integer sourceOrg, Integer targetOrg) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        StringBuilder sb = new StringBuilder();
        sb.append("來源對應部門：").append(orgService.getOrgName(sourceOrg)).append("\n");
        sb.append("目標新增部門：").append(orgService.getOrgName(targetOrg)).append("\n");
        sb.append("處理筆數：").append(items.size()).append("\n");
        sb.append("失敗筆數：").append(items.stream().filter(item -> !item.isSucess()).count()).append("\n");
        sb.append("記錄日期：").append(sdf.format(new Date())).append("\n");
        sb.append("------------------------------------\n");
        items.forEach(each -> {
            sb.append("需求單ID：").append(each.getRequireSid()).append(" ");
            sb.append("需求單號：").append(each.getRequireNo()).append(" ");
            sb.append("製作進度：").append(commonService.get(each.getReqType())).append(" ");
            sb.append("建單日期：").append(sdf.format(each.getCreateDt())).append(" ");
            sb.append("收件ID    ：").append(each.getAlertInboxSid()).append(" ");
            sb.append("收件群組ID：").append(each.getAlertInboxGroupSid()).append(" ");
            sb.append("異動成功：").append(each.isSucess()).append(" ");
            sb.append("\n");
        });
        return sb.toString();
    }

    /**
     * 執行後台插入轉寄資訊
     *
     * @param targetOrg
     * @param modifyItems
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public List<TransDepBacksageTo> modifyBackstageModifyItems(Integer targetOrg, List<TransDepBacksageTo> modifyItems) {
        List<String> alertSids = modifyItems.stream().map(TransDepBacksageTo::getAlertInboxSid).collect(Collectors.toList());
        List<AlertInbox> modifyEntitys = alertInboxDao.findAll(alertSids);
        modifyEntitys.forEach(entity -> {
            try {
                AlertInbox newEntity = new AlertInbox();
                entityUtils.copyProperties(entity, newEntity);
                newEntity.setReceiveDep(new Org(targetOrg));
                newEntity.setSid("");
                this.insetModify(newEntity, entity.getSendInboxGroup().getSid());
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                this.backstageInsertFaild(entity.getSid(), modifyItems);
            }
        });
        return modifyItems;
    }

    /**
     * 執行單筆異動資料
     *
     * @param entity
     * @param info
     * @param targetOrg
     */
    @Transactional(rollbackFor = Exception.class)
    private void insetModify(AlertInbox newEntity, String alertGroupSid) {
        alertInboxDao.save(newEntity);
        this.rebuildGroupSendTo(alertGroupSid);
    }

    @Transactional(rollbackFor = Exception.class)
    private void rebuildGroupSendTo(String alertGroupSid) {
        AlertInboxSendGroup group = alertSendGroupDao.findOne(alertGroupSid);
        List<AlertInbox> inboxs = group.getInboxs();
        String sendTo = inboxs.stream()
                .map(each -> orgService.getOrgName(each.getReceiveDep()))
                .collect(Collectors.joining("、"));
        group.setSendTo(sendTo);
        alertSendGroupDao.updateSendToStr(group, sendTo);
    }

    /**
     * 插入失敗時記錄錯誤訊息
     *
     * @param requireSid
     * @param modifyItems
     */
    private void backstageInsertFaild(String alertSid, List<TransDepBacksageTo> modifyItems) {
        modifyItems.stream().filter(to -> to.getAlertInboxSid().equals(alertSid)).forEach(to -> to.setSucess(false));
    }

    /**
     * 查詢後台轉換轉寄還原物件
     *
     * @param recoveryOrg
     * @param recoveryAlertGroupSids
     * @return
     */
    public List<TransDepBacksageTo> findBackstageModifyRecoveryItems(Integer recoveryOrg, List<String> recoveryAlertGroupSids) {
        String findQuery = "SELECT tr.require_sid,"
                + "       tr.require_no,"
                + "       tr.require_status,"
                + "       tr.create_dt,"
                + "       ai.receive_dep,"
                + "       ai.alert_sid,"
                + "       ai.alert_group_sid FROM tr_alert_inbox ai "
                + "  LEFT JOIN tr_require tr ON tr.require_sid = ai.require_sid "
                + "  WHERE ai.inbox_type = 'INCOME_DEPT' "
                + "    AND ai.status = 0 "
                + "    AND ai.receive_dep = :recoveryOrg"
                + "    AND ai.alert_group_sid IN (:recoveryAlertGroupSids)"
                + " GROUP BY require_sid,alert_group_sid "
                + " ORDER BY ai.create_dt DESC ";
        Map<String, Object> param = Maps.newHashMap();
        param.put("recoveryOrg", recoveryOrg);
        param.put("recoveryAlertGroupSids", recoveryAlertGroupSids);
        Query query = em.createNativeQuery(findQuery);
        param.entrySet().stream().forEach(entry -> query.setParameter(entry.getKey(), entry.getValue()));

        @SuppressWarnings("unchecked")
        List<Object> result = query.getResultList();
        return result.stream()
                .map(obj -> this.bulidTransDepBacksageTo((Object[]) obj))
                .collect(Collectors.toList());
    }

    /**
     * 後台需進行轉換轉寄還原內容顯示
     *
     * @param items
     * @param recoveryOrgSid
     * @param isCnt
     * @return
     */
    public String htmlHandlerMsgByRecovery(List<TransDepBacksageTo> items, Integer recoveryOrgSid, boolean isCnt) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        StringBuilder sb = new StringBuilder();
        items.forEach(each -> {
            sb.append("需求單ID：").append(each.getRequireSid()).append("<BR/>");
            sb.append("需求單號：").append(each.getRequireNo()).append("<BR/>");
            sb.append("製作進度：").append(commonService.get(each.getReqType())).append("<BR/>");
            sb.append("建單日期：").append(sdf.format(each.getCreateDt())).append("<BR/>");
            sb.append("收件ID    ：").append(each.getAlertInboxSid()).append("<BR/>");
            sb.append("收件群組ID：").append(each.getAlertInboxGroupSid()).append("<BR/>");
            if (!isCnt) {
                sb.append("還原成功：").append(each.isSucess()).append("<BR/>");
            }
            sb.append("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░<BR/>");
        });
        String handlerCntStr = isCnt ? "預計處理筆數：" : "處理筆數：";
        sb.append(handlerCntStr).append(items.size()).append("<BR/>");
        sb.append("還原部門：").append(orgService.getOrgName(recoveryOrgSid)).append("<BR/>");
        if (!isCnt) {
            sb.append("失敗筆數：").append(items.stream().filter(item -> !item.isSucess()).count()).append("<BR/>");
        }
        return sb.toString();
    }

    /**
     * 執行後台轉換轉寄還原
     *
     * @param targetOrg
     * @param modifyItems
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public List<TransDepBacksageTo> modifyBackstageRecoveryItems(Integer recoveryOrg, List<TransDepBacksageTo> modifyItems) {
        List<String> alertSids = modifyItems.stream().map(TransDepBacksageTo::getAlertInboxSid).collect(Collectors.toList());
        List<AlertInbox> modifyEntitys = alertInboxDao.findAll(alertSids);
        modifyEntitys.forEach(entity -> {
            try {
                this.removeModify(entity, entity.getSendInboxGroup().getSid());
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                this.backstageInsertFaild(entity.getRequire().getSid(), modifyItems);
            }
        });
        return modifyItems;
    }

    /**
     * 執行單筆異動資料(移除)
     *
     * @param entity
     * @param info
     * @param targetOrg
     */
    @Transactional(rollbackFor = Exception.class)
    private void removeModify(AlertInbox entity, String alertGroupSid) {
        alertInboxDao.deleteBySid(entity.getSid());
        this.rebuildGroupSendTo(alertGroupSid);
    }

    /**
     * 後台進行轉換轉寄時記錄下載
     *
     * @param items
     * @param sourceOrg
     * @param targetOrg
     * @return
     */
    public String downloadRecoveryMsg(List<TransDepBacksageTo> items, Integer recoveryOrg) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        StringBuilder sb = new StringBuilder();
        sb.append("還原部門：").append(orgService.getOrgName(recoveryOrg)).append("\n");
        sb.append("處理筆數：").append(items.size()).append("\n");
        sb.append("失敗筆數：").append(items.stream().filter(item -> !item.isSucess()).count()).append("\n");
        sb.append("記錄日期：").append(sdf.format(new Date())).append("\n");
        sb.append("------------------------------------\n");
        items.forEach(each -> {
            sb.append("需求單ID：").append(each.getRequireSid()).append(" ");
            sb.append("需求單號：").append(each.getRequireNo()).append(" ");
            sb.append("製作進度：").append(commonService.get(each.getReqType())).append(" ");
            sb.append("建單日期：").append(sdf.format(each.getCreateDt())).append(" ");
            sb.append("收件ID    ：").append(each.getAlertInboxSid()).append(" ");
            sb.append("收件群組ID：").append(each.getAlertInboxGroupSid()).append(" ");
            sb.append("還原成功：").append(each.isSucess()).append(" ");
            sb.append("\n");
        });
        return sb.toString();
    }

    /**
     * 轉寄單位轉發-增加
     * 
     * @param targetOrg
     * @param requireSids
     * @param sourceOrg
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public List<String> addTransDept(Integer targetOrg, List<String> requireSids, List<Integer> sourceOrg) {
        List<String> resultList = Lists.newArrayList();
        for (String sid : requireSids) {
            TrAlertInbox entity = trAlertInboxService.findByInboxByRequireSidAndReceiveDep(sid, sourceOrg, targetOrg);
            if (entity == null) {
                log.info("需求單require sid {}, TrAlertInbox entity not found!", sid);
                continue;
            }
            try {
                TrAlertInbox trAlertInbox = new TrAlertInbox();
                entityUtils.copyProperties(entity, trAlertInbox);
                trAlertInbox.setSid(null); // by add
                trAlertInbox.setReceiveDep(targetOrg);

                trAlertInboxService.save(trAlertInbox);

                AlertInboxSendGroup group = alertSendGroupDao.findOne(trAlertInbox.getAlert_group_sid());
                List<AlertInbox> inboxs = group.getInboxs();
                String sendTo = inboxs.stream()
                        .map(each -> orgService.getOrgName(each.getReceiveDep()))
                        .collect(Collectors.joining("、"));
                group.setSendTo(sendTo);
                alertSendGroupDao.updateSendToStr(group, sendTo + "、" + orgService.getOrgName(targetOrg));

                resultList.add(entity.getRequireNo());
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return resultList;
    }

    /**
     * 轉寄單位轉發-還原
     * 
     * @param recoveryOrg
     * @param requireSids
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public List<String> minusTransDept(List<Integer> recoveryOrgs, List<String> requireSids) {
        Set<String> resultSet = Sets.newHashSet();
        for (String sid : requireSids) {
            List<TrAlertInbox> inboxList = trAlertInboxService.findByInboxByRequireSidAndReceiveDepts(sid, recoveryOrgs);
            if (CollectionUtils.isEmpty(inboxList)) {
                log.info("需求單require sid {}, TrAlertInbox entity not found!", sid);
                continue;
            }
            try {
                trAlertInboxService.deleteAll(inboxList);
                List<AlertInboxSendGroup> groupList = alertSendGroupDao.findByRequireSidAndForwardType(sid, ForwardType.FORWARD_DEPT);
                for (AlertInboxSendGroup group : groupList) {
                    List<AlertInbox> inboxs = group.getInboxs();
                    inboxs.removeIf(inbox -> {
                        return recoveryOrgs.contains(inbox.getReceiveDep().getSid());
                    });
                    String sendTo = inboxs.stream()
                            .map(each -> orgService.getOrgName(each.getReceiveDep()))
                            .collect(Collectors.joining("、"));
                    group.setSendTo(sendTo);
                    alertSendGroupDao.updateSendToStr(group, sendTo);
                    resultSet.add(group.getRequireNo());
                }

            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return Lists.newArrayList(resultSet);
    }
}
