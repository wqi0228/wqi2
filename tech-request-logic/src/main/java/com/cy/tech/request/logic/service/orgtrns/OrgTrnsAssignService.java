package com.cy.tech.request.logic.service.orgtrns;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.AssignSendInfoService;
import com.cy.tech.request.logic.service.RequireProcessCompleteRollbackService;
import com.cy.tech.request.logic.service.RequireProcessCompleteService;
import com.cy.tech.request.logic.service.Set10V70TrneBatchHelper;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsAssignSendInfoForTrnsTO;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsConfirmDepResult;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsDtVO;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsWorkVerifyVO;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsConfirmDepResult.ConfirmDepResultProcType;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepService;
import com.cy.tech.request.logic.vo.RequireConfirmDepVO;
import com.cy.work.common.constant.WkConstants;
import com.cy.tech.request.vo.converter.SetupInfoToConverter;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.ReqConfirmDepProcType;
import com.cy.tech.request.vo.enums.ReqConfirmDepProgStatus;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.RequireTransProgramType;
import com.cy.tech.request.vo.require.RequireConfirmDepHistory;
import com.cy.tech.request.vo.value.to.SetupInfoTo;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.HikariPoolMXBean;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Slf4j
@NoArgsConstructor
@Service
public class OrgTrnsAssignService {

    // ========================================================================
    // 服務元件區
    // ========================================================================
    @Autowired
    private transient AssignSendInfoService assignSendInfoService;
    @Autowired
    private transient NativeSqlRepository nativeSqlRepository;
    @Autowired
    private transient OrgTrnsAssignNoticeService orgTrnsAssignNoticeService;
    @Autowired
    private transient OrgTrnsBatchHelper orgTrnsBatchHelper;
    @Autowired
    private transient OrgTrnsService orgTrnsService;
    @Autowired
    private transient RequireConfirmDepService requireConfirmDepService;
    @Autowired
    private transient RequireProcessCompleteRollbackService requireProcessCompleteRollbackService;
    @Autowired
    private transient RequireProcessCompleteService requireProcessCompleteService;
    @Autowired
    private transient Set10V70TrneBatchHelper set10V70TrneBatchHelper;
    @Autowired
    private transient WorkBackendParamHelper workBackendParamHelper;

    // ========================================================================
    // 變數區
    // ========================================================================
    private Map<String, String> cacheMap;
    private Long maxConfirmDepSid = Long.parseLong("0");

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * @param confirmVO
     * @param memo
     * @param sysDate
     */
    private RequireConfirmDepHistory createRequireConfirmDepHistory(
            RequireConfirmDepVO requireConfirmDep,
            String memo,
            Date sysDate) {
        RequireConfirmDepHistory history = new RequireConfirmDepHistory();
        history.setCreatedUser(1);
        history.setCreatedDate(sysDate);
        history.setCreateDep(1);
        history.setRequireConfirmSid(requireConfirmDep.getSid());
        history.setRequireSid(requireConfirmDep.getRequireSid());
        history.setDepSid(requireConfirmDep.getDepSid());
        history.setProgType(ReqConfirmDepProcType.ORG_TRNS);
        history.setMemo(memo);
        return history;
    }

    /**
     * @param sid
     * @param depSid
     * @param srcTrnsTo
     * @param sysDate
     * @return
     */
    private RequireConfirmDepVO createRequireConfirmDepVO(
            Long sid,
            Integer depSid,
            OrgTrnsAssignSendInfoForTrnsTO srcTrnsTo,
            Date sysDate) {

        // ====================================
        // 判斷確認的狀態
        // ====================================
        // 取得製作進度
        RequireStatusType requireStatusType = RequireStatusType.safeValueOf(srcTrnsTo.getRequireStatus());
        // 需求確認狀態
        ReqConfirmDepProgStatus progStatus = ReqConfirmDepProgStatus.PASS;
        // 僅有進行中單據需要確認, 其他已完成、結案等狀態, 直接無需確認
        if (RequireStatusType.PROCESS.equals(requireStatusType)) {
            progStatus = ReqConfirmDepProgStatus.WAIT_CONFIRM;
        }

        // 建立新增資料
        RequireConfirmDepVO requireConfirmDep = new RequireConfirmDepVO();
        requireConfirmDep.setSid(sid);
        requireConfirmDep.setCreatedUser(1);
        requireConfirmDep.setCreatedDate(sysDate);
        requireConfirmDep.setRequireSid(srcTrnsTo.getRequireSid());
        requireConfirmDep.setDepSid(depSid);
        requireConfirmDep.setOwnerSid(WkConstants.MANAGER_VIRTAUL_USER_SID);
        requireConfirmDep.setProgStatus(progStatus);

        return requireConfirmDep;
    }

    /**
     * 組加減派部門異動記錄資訊
     * 
     * @param confirmDepResult
     * @return
     */
    private String prepareAssignDepModifyMemo(OrgTrnsConfirmDepResult confirmDepResult) {
        String memo = "";

        if (WkStringUtils.notEmpty(confirmDepResult.getPlusAssignDepSid())) {
            memo += "<br/>加派:" + WkOrgUtils.findNameBySid(
                    confirmDepResult.getPlusAssignDepSid(), ",", false, false);
        }

        if (WkStringUtils.notEmpty(confirmDepResult.getMinusAssignDepSid())) {
            memo += "<br/>減派:" + WkOrgUtils.findNameBySid(
                    confirmDepResult.getMinusAssignDepSid(), ",", false, false);
        }
        return memo;
    }

    /**
     * 檢查是否需異動負責人
     * 
     * @param requireConfirmDep
     * @param updateOwnerToManagerByConfirmSid
     * @param insertRequireConfirmDepHistorys
     * @param sysDate
     */
    private void prepareChangeOwner(
            RequireConfirmDepVO requireConfirmDep,
            Set<Long> updateOwnerToManagerByConfirmSid,
            List<RequireConfirmDepHistory> insertRequireConfirmDepHistorys,
            Date sysDate) {

        if (requireConfirmDep == null) {
            return;
        }

        Integer ownerSid = requireConfirmDep.getOwnerSid();

        // 固定為單位主管時, 無需處理
        if (ownerSid == WkConstants.MANAGER_VIRTAUL_USER_SID) {
            return;
        }

        // 取得確認單位以下所有使用者sid (非停用)
        List<Integer> depUserSids = WkUserUtils.findAllUserByDepSid(requireConfirmDep.getDepSid())
                .stream()
                .filter(user -> Activation.ACTIVE.equals(user.getStatus()))
                .map(user -> user.getSid()).collect(Collectors.toList());

        // 判斷 owner 是否還存在於確認單位下
        if (!depUserSids.contains(ownerSid)) {
            // 加入 update 清單 (update為單位主管)
            updateOwnerToManagerByConfirmSid.add(requireConfirmDep.getSid());
            // 新增異動記錄
            String memo = "原負責人 [" + WkUserUtils.findNameBySid(ownerSid) + "] 已離開單位, 故異動為單位主管!";
            insertRequireConfirmDepHistorys.add(
                    this.createRequireConfirmDepHistory(
                            requireConfirmDep,
                            memo,
                            sysDate));
        }
    }

    /**
     * @param confirmDepSid
     * @param confirmDepResultMapByConfirmSid
     * @param oldAssignDepSidMap
     * @param newAssignDepSidMap
     * @param requireConfirmDepMapByDepSid
     * @param allTrnsDepMappingByAfter
     */
    private void prepareConfirmDepResult(
            Integer confirmDepSid,
            Map<Integer, OrgTrnsConfirmDepResult> confirmDepResultMapByConfirmSid,
            Map<Integer, Set<Integer>> oldAssignDepSidMap,
            Map<Integer, Set<Integer>> newAssignDepSidMap,
            Map<Integer, RequireConfirmDepVO> requireConfirmDepMapByDepSid,
            Map<Integer, Integer> allTrnsDepMappingByAfter) {

        // 由 map 中, 取回確認單位的分析資料
        OrgTrnsConfirmDepResult orgTrnsConfirmDepResult = confirmDepResultMapByConfirmSid.get(confirmDepSid);
        // 若不存在時新增
        if (orgTrnsConfirmDepResult == null) {
            List<Integer> oldAssignDepSids = Lists.newArrayList();
            if (WkStringUtils.notEmpty(oldAssignDepSidMap.get(confirmDepSid))) {
                oldAssignDepSids = Lists.newArrayList(oldAssignDepSidMap.get(confirmDepSid));
            }

            List<Integer> newAssignDepSids = Lists.newArrayList();
            if (WkStringUtils.notEmpty(newAssignDepSidMap.get(confirmDepSid))) {
                newAssignDepSids = Lists.newArrayList(newAssignDepSidMap.get(confirmDepSid));
            }

            orgTrnsConfirmDepResult = new OrgTrnsConfirmDepResult(
                    requireConfirmDepMapByDepSid.get(confirmDepSid),
                    oldAssignDepSids,
                    newAssignDepSids);

            confirmDepResultMapByConfirmSid.put(confirmDepSid, orgTrnsConfirmDepResult);
        }

        // 收集轉換前所屬的收束單位
        Set<RequireConfirmDepVO> beforeConfirmDeps = Sets.newHashSet();

        // 分析此『確認單位』的『分派單位』是否經由轉換而來
        // (多筆分派單位收束到部 = 確認單位)
        for (Integer assignDepSid : orgTrnsConfirmDepResult.getAssignDepSids()) {

            // allTrnsDepMappingByAfter -> Map<轉換後單位SID, 轉換前單位SID>
            // 分派單位不是經由轉檔而來
            if (!allTrnsDepMappingByAfter.containsKey(assignDepSid)) {
                continue;
            }
            // 取得轉換前 dep_sid
            Integer beforeAssignDepSid = allTrnsDepMappingByAfter.get(assignDepSid);
            // 取得轉換前收束單位
            Org beforeConfirmOrg = this.requireConfirmDepService.prepareRequireConfirmDep(beforeAssignDepSid);
            if (beforeConfirmOrg == null) {
                continue;
            }

            // 取得轉換前收束單位確認資料檔
            RequireConfirmDepVO beforerequireConfirmDep = requireConfirmDepMapByDepSid.get(beforeConfirmOrg.getSid());
            if (beforerequireConfirmDep == null) {
                continue;
            }
            // 取得轉換後收束單位確認資料檔
            RequireConfirmDepVO afterRequireConfirmDep = requireConfirmDepMapByDepSid.get(confirmDepSid);

            // 轉換前收束單位 = 轉換後收束單位 -> pass
            if (beforerequireConfirmDep.equals(afterRequireConfirmDep)) {
                continue;
            }

            beforeConfirmDeps.add(beforerequireConfirmDep);
        }

        // 收集轉換前的資料
        if (beforeConfirmDeps.size() > 0) {
            orgTrnsConfirmDepResult.getBeforeConfirmDeps().addAll(beforeConfirmDeps);
        }
    }

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * 查詢組織異動設定檔
     * 
     * @param pageVO
     */
    public List<OrgTrnsDtVO> queryTrnsData(List<OrgTrnsWorkVerifyVO> allTrnsWorks) {

        // ====================================
        // 收集分派單位
        // ====================================
        // 收集分派部門
        Set<Integer> assignDepSids = allTrnsWorks.stream()
                .map(OrgTrnsWorkVerifyVO::getBeforeOrgSid)
                .collect(Collectors.toSet());

        // 轉檔對應關係 (原部門 -> 新部門) (可能有 1對多的資料, 故用 groupingBy)
        Map<Integer, List<Integer>> trnsDepMapping = allTrnsWorks.stream()
                .collect(Collectors.groupingBy(
                        OrgTrnsWorkVerifyVO::getBeforeOrgSid,
                        Collectors.mapping(
                                OrgTrnsWorkVerifyVO::getAfterOrgSid,
                                Collectors.toList())));

        for (Entry<Integer, List<Integer>> entry : trnsDepMapping.entrySet()) {
            List<Integer> sortedDeps = WkOrgUtils.sortByOrgTree(entry.getValue());
            trnsDepMapping.put(entry.getKey(), sortedDeps);
        }

        // ====================================
        // 查詢有分派單位的單據
        // ====================================
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT tr.require_sid    AS sid, ");
        sql.append("       tr.require_no     AS caseNo, ");
        sql.append("       tid.field_content AS theme, ");
        sql.append("       tr.create_dt      AS createDate_src, ");
        sql.append("       tr.create_usr     AS createUserSid, ");
        sql.append("       tr.require_status AS caseStatus_Src, ");
        sql.append("       info.info         AS assignInfo_src ");
        sql.append("FROM   tr_require tr ");
        sql.append("       INNER JOIN tr_assign_send_info info ");
        sql.append("               ON tr.require_sid = info.require_sid ");
        sql.append("              AND info.type = " + AssignSendType.ASSIGN.ordinal() + " ");
        // 取得日期最大的
        sql.append("              AND info.create_dt = (SELECT Max(ai.create_dt) AS maxTime ");
        sql.append("                                      FROM tr_assign_send_info ai ");
        sql.append("                                     WHERE info.require_sid = ai.require_sid ");
        sql.append("                                       AND info.type = ai.type ");
        sql.append("                                     GROUP BY ai.require_sid) ");

        sql.append("       INNER JOIN (SELECT tid.require_sid, ");
        sql.append("                          tid.field_content ");
        sql.append("                   FROM   tr_index_dictionary tid ");
        sql.append("                   WHERE  1 = 1 ");
        sql.append("                          AND tid.field_name = '主題') AS tid ");
        sql.append("               ON tr.require_sid = tid.require_sid ");
        sql.append("WHERE  EXISTS (SELECT dep_sid ");
        sql.append("               FROM   tr_assign_send_search_info si ");
        sql.append("               WHERE  tr.require_sid = si.require_sid ");
        sql.append("                      AND si.type = " + AssignSendType.ASSIGN.ordinal() + " ");
        sql.append("                      AND si.dep_sid IN ( :assignDepSids )) ");

        // varname1.append(" AND tr.require_no = 'TGTR20181204012' ");
        sql.append("GROUP BY tr.require_sid ");
        sql.append("ORDER BY tr.require_no DESC ");
        sql.append("LIMIT " + workBackendParamHelper.findReqOrgTrnsCount() + "  ");

        // 查詢參數
        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put("assignDepSids", Lists.newArrayList(assignDepSids));

        // 查詢
        Long startTime = System.currentTimeMillis();
        List<OrgTrnsDtVO> results = nativeSqlRepository.getResultList(
                sql.toString(), parameters, OrgTrnsDtVO.class);
        log.info(WkCommonUtils.prepareCostMessage(startTime, "分派單位轉檔查詢:" + results.size() + "筆"));

        if (WkStringUtils.isEmpty(results)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 準備顯示欄位
        // ====================================
        startTime = System.currentTimeMillis();
        // 轉共同欄位
        this.orgTrnsService.prepareShowInfo(results, RequireTransProgramType.REQUIRE);
        log.info(WkCommonUtils.prepareCostMessage(startTime, "分派單位轉檔,轉共同顯示欄位"));

        SetupInfoToConverter setupInfoToConverter = new SetupInfoToConverter();

        // 轉私有欄位
        startTime = System.currentTimeMillis();
        this.cacheMap = Maps.newHashMap();
        for (OrgTrnsDtVO resultVO : results) {
            // 主題
            // 加速, 暫不轉換
            // if (WkStringUtils.notEmpty(resultVO.getTheme())) {
            // resultVO.setTheme(searchService.combineFromJsonStr(resultVO.getTheme()));
            // }

            // 分派部門
            if (WkStringUtils.notEmpty(resultVO.getAssignInfo_src())) {
                // 取得部門 sid
                SetupInfoTo setupInfoTo = setupInfoToConverter.convertToEntityAttribute(resultVO.getAssignInfo_src());
                List<Integer> currAssignDepSids = assignSendInfoService.prepareDeps(setupInfoTo);

                // 過濾需為本次轉檔部門
                Map<Integer, List<Integer>> assignDepSidsMapping = currAssignDepSids.stream()
                        .filter(depSid -> assignDepSids.contains(depSid))
                        .collect(Collectors.toMap(
                                depSid -> depSid,
                                depSid -> trnsDepMapping.get(depSid)));

                // log.info("\r\n" +
                // new
                // com.google.gson.GsonBuilder().setPrettyPrinting().create().toJson(assignDepSidsMapping));

                resultVO.setAssignDepSidsMapping(assignDepSidsMapping);
                // 組分派單位顯示名稱
                resultVO.setAssignDepNames(this.queryTrnsData_prepareTrnsInfo(assignDepSidsMapping));
            }

        }
        log.info(WkCommonUtils.prepareCostMessage(startTime, "分派單位轉檔,轉私有顯示欄位"));

        return results;
    }

    /**
     * 準備畫面顯示資料
     * 
     * @param cacheMap
     * @param assignDepSidsMapping
     * @return
     */
    private String queryTrnsData_prepareTrnsInfo(Map<Integer, List<Integer>> assignDepSidsMapping) {

        if (assignDepSidsMapping == null || assignDepSidsMapping.size() == 0) {
            return "";
        }

        if (cacheMap == null) {
            cacheMap = Maps.newHashMap();
        }

        List<String> trnsDepinfos = Lists.newArrayList();

        List<Integer> beforeDeps = Lists.newArrayList(assignDepSidsMapping.keySet());

        // 單位排序
        beforeDeps = WkOrgUtils.sortByOrgTree(beforeDeps);

        for (Integer beforeDepSid : beforeDeps) {

            List<Integer> afterDepSids = assignDepSidsMapping.get(beforeDepSid);

            String cacheKey = beforeDepSid
                    + "@"
                    + String.join(
                            "_",
                            afterDepSids
                                    .stream()
                                    .map(sid -> sid + "")
                                    .collect(Collectors.toList()));

            // 若已經產生過, 則由快取中取回名稱
            if (cacheMap.containsKey(cacheKey)) {
                trnsDepinfos.add(cacheMap.get(cacheKey));
                continue;
            }

            String trnsDepinfoStr = "";
            // 轉換前單位
            trnsDepinfoStr += "【" + beforeDepSid + "_" + WkOrgUtils.findNameByDecorationStyle(beforeDepSid, false) + "】";
            // 右箭頭符號
            trnsDepinfoStr += "&nbsp;<i class=\"fa fa-arrow-right\" aria-hidden=\"true\"></i>&nbsp;";
            // 轉換後單位
            List<String> afterDepInfo = Lists.newArrayList();
            for (Integer afterDepSid : afterDepSids) {
                afterDepInfo.add(afterDepSid + "_" + WkOrgUtils.findNameByDecorationStyle(afterDepSid, false));
            }
            if (afterDepInfo.size() > 1) {
                trnsDepinfoStr += "<span style='color:red'>";
            }
            trnsDepinfoStr += "【" + String.join("、", afterDepInfo) + "】";
            if (afterDepInfo.size() > 1) {
                trnsDepinfoStr += "</span>";
            }

            trnsDepinfos.add(trnsDepinfoStr);
            cacheMap.put(cacheKey, trnsDepinfoStr);

        }

        // 以斷行分隔, 並組合成一個字串
        return String.join("<br/>", trnsDepinfos);

    }

    /**
     * 執行轉檔
     * 
     * @param selectedDtVOList
     * @param showDtVOList
     * @param sysDate
     * @throws SystemDevelopException
     */
    @Transactional(rollbackFor = Exception.class)
    public void processTrns(
            List<OrgTrnsDtVO> selectedDtVOList,
            List<OrgTrnsDtVO> showDtVOList,
            Date sysDate) throws SystemDevelopException {

        long startTime = System.currentTimeMillis();
        log.debug(WkCommonUtils.prepareCostMessageStart("分派單位轉檔:" + selectedDtVOList.size() + "筆"));

        this.maxConfirmDepSid = Long.parseLong("0");

        // ====================================
        // 重置 Connection pool 解決執行速度越來越慢的問題
        // ====================================
        HikariDataSource hikariDs = (HikariDataSource) WorkSpringContextHolder.getBean("dataSource");
        HikariPoolMXBean poolBean = hikariDs.getHikariPoolMXBean();
        if (poolBean != null) {
            poolBean.softEvictConnections();
            log.debug("Hikari softEvictConnections");
        }

        // ====================================
        // 收集所有要轉換單位資料
        // ====================================
        // Map<轉換前單位SID, 轉換後單位SID>
        // 可能會有一對多的狀況 (轉換前單位 -> 多個轉換後單位) , 轉換結果取第一筆
        Map<Integer, Integer> allTrnsDepMappingByBefore = Maps.newHashMap();

        Map<Integer, Integer> allTrnsDepMappingByAfter = Maps.newHashMap();

        // 逐筆收集
        for (OrgTrnsDtVO orgTrnsDtVO : showDtVOList) {
            // 取得單位轉檔對應
            Map<Integer, List<Integer>> assignDepSidsMapping = orgTrnsDtVO.getAssignDepSidsMapping();
            // 收集單位轉檔對應
            for (Entry<Integer, List<Integer>> entry : assignDepSidsMapping.entrySet()) {
                if (entry.getValue() != null && WkStringUtils.notEmpty(entry.getValue())) {
                    // 轉換前單位
                    Integer beforDepSid = entry.getKey();
                    // 轉換後單位 (僅取第一筆)
                    Integer afterDepSid = entry.getValue().get(0);
                    // 已存在不取代 (先進優先)
                    if (!allTrnsDepMappingByBefore.containsKey(beforDepSid)) {
                        allTrnsDepMappingByBefore.put(beforDepSid, afterDepSid);
                    }

                    if (!allTrnsDepMappingByAfter.containsKey(afterDepSid)) {
                        allTrnsDepMappingByAfter.put(afterDepSid, beforDepSid);
                    }

                }
            }
        }

        log.debug("收集所有要轉換單位資料 完成");

        // ====================================
        // 準備分派單位異動資料
        // ====================================
        List<OrgTrnsAssignSendInfoForTrnsTO> assignSendInfoForTrnsVOs = this.orgTrnsAssignNoticeService.prepareAssignSendInfo(
                selectedDtVOList,
                allTrnsDepMappingByBefore,
                AssignSendType.ASSIGN,
                sysDate);

        log.debug("準備分派單位異動資料 完成");

        // ====================================
        // 轉分派單位
        // ====================================
        this.orgTrnsAssignNoticeService.trnsAssignSendInfo(
                assignSendInfoForTrnsVOs,
                AssignSendType.ASSIGN,
                sysDate);

        log.debug("轉分派單位 完成");

        // ====================================
        // 轉需求單位檔
        // ====================================
        this.processTrns_trnsRequireConfirmDep(
                assignSendInfoForTrnsVOs,
                allTrnsDepMappingByAfter,
                sysDate);

        log.debug("轉需求單位檔 完成");

        log.debug(WkCommonUtils.prepareCostMessage(startTime, "分派單位轉檔"));

    }

    /**
     * 準備需求確認檔轉檔資料
     * 
     * @param assignSendInfoForTrnsVOs
     * @param allTrnsDepMappingByAfter
     * @param targetUpdateOwnerToManagerByConfirmSid
     * @param targetUpdateDepSidMapByConfirmSid
     * @param targetDeleteConfirmDepByConfirmSid
     * @param targetInsertRequireConfirmDepVOs
     * @param targetInsertRequireConfirmDepHistorys
     * @param targetUpdateRequireConfirmDepHistorys
     * @param targetModifyRequireSids
     * @param sysDate
     * @throws SystemDevelopException
     */
    private void processTrns_prepareRequireConfirmDep(
            List<OrgTrnsAssignSendInfoForTrnsTO> assignSendInfoForTrnsVOs,
            Map<Integer, Integer> allTrnsDepMappingByAfter,
            Set<Long> targetUpdateOwnerToManagerByConfirmSid,
            Map<Long, Integer> targetUpdateDepSidMapByConfirmSid,
            Set<Long> targetDeleteConfirmDepByConfirmSid,
            List<RequireConfirmDepVO> targetInsertRequireConfirmDepVOs,
            List<RequireConfirmDepHistory> targetInsertRequireConfirmDepHistorys,
            Set<String> targetUpdateRequireConfirmDepHistorys,
            Set<String> targetModifyRequireSids,
            Date sysDate) throws SystemDevelopException {

        // ====================================
        // 查詢進度確認單位檔
        // ====================================
        // 收集所有需求單號
        Set<String> requireSids = assignSendInfoForTrnsVOs.stream()
                .map(OrgTrnsAssignSendInfoForTrnsTO::getRequireSid)
                .collect(Collectors.toSet());

        // 查詢此需求單已存在的『確認單位檔』
        List<RequireConfirmDepVO> oldRequireConfirmDeps = this.requireConfirmDepService.findByRequireSids(requireSids);
        if (oldRequireConfirmDeps == null) {
            oldRequireConfirmDeps = Lists.newArrayList();
        }

        // ====================================
        // 準備需求單位確認檔比對資料
        // ====================================
        for (OrgTrnsAssignSendInfoForTrnsTO srcTrnsTo : assignSendInfoForTrnsVOs) {

            // ====================================
            // 單據製作進度在流程：『檢查確認』以前的狀態不需處理 （還沒開始進入分派）
            // ====================================
            if (srcTrnsTo.getRequireStatusType() != null
                    && !srcTrnsTo.getRequireStatusType().isShowRequireConfirmPanel()) {
                log.info("pass by req status [" + srcTrnsTo.getRequireStatus() + "] :" + srcTrnsTo.getRequireNo());
                continue;
            }

            // ====================================
            // 取出此需求單的已存在的『確認單位檔』
            // ====================================
            List<RequireConfirmDepVO> currOldRequireConfirmDeps = oldRequireConfirmDeps.stream()
                    .filter(entity -> WkCommonUtils.compareByStr(entity.getRequireSid(), srcTrnsTo.getRequireSid()))
                    .collect(Collectors.toList());

            // 有分派單位, 但無確認檔者, 為需求進度確認功能開發前的舊資料, 無需轉檔
            if (WkStringUtils.notEmpty(srcTrnsTo.getDepSids())
                    && WkStringUtils.isEmpty(currOldRequireConfirmDeps)) {
                continue;
            }

            // ====================================
            // 分析需求確認單位資料 (OrgTrnsConfirmDepResult)
            // ====================================
            // 計算
            Map<Integer, OrgTrnsConfirmDepResult> confirmDepResultMapByConfirmDepSid = this
                    .processTrns_prepareRequireConfirmDep_single(
                            srcTrnsTo,
                            allTrnsDepMappingByAfter,
                            currOldRequireConfirmDeps,
                            sysDate);

            // 單據無任何一筆需求確認資料時, 不處理 (應該不可能發生)
            if (confirmDepResultMapByConfirmDepSid.size() == 0) {
                continue;
            }

            // ====================================
            // 建立需求確認單位轉檔資料
            // ====================================
            this.processTrns_prepareRequireConfirmDep_createDBData(
                    srcTrnsTo,
                    confirmDepResultMapByConfirmDepSid,
                    targetUpdateOwnerToManagerByConfirmSid,
                    targetUpdateDepSidMapByConfirmSid,
                    targetDeleteConfirmDepByConfirmSid,
                    targetInsertRequireConfirmDepVOs,
                    targetInsertRequireConfirmDepHistorys,
                    targetUpdateRequireConfirmDepHistorys,
                    targetModifyRequireSids,
                    sysDate);
        }
    }

    /**
     * 建立需求確認單位轉檔資料
     * 
     * @param srcTrnsTo
     * @param srcConfirmDepResultMapByDepSid
     * @param targetUpdateOwnerToManagerByConfirmSid
     * @param targetUpdateDepSidMapByConfirmSid
     * @param targetDeleteConfirmDepByConfirmSid
     * @param targetInsertRequireConfirmDepVOs
     * @param targetInsertRequireConfirmDepHistorys
     * @param targetUpdateRequireConfirmDepHistorys
     * @param targetModifyRequireSids
     * @param sysDate
     * @throws SystemDevelopException
     */
    private void processTrns_prepareRequireConfirmDep_createDBData(
            OrgTrnsAssignSendInfoForTrnsTO srcTrnsTo,
            Map<Integer, OrgTrnsConfirmDepResult> srcConfirmDepResultMapByDepSid,
            Set<Long> targetUpdateOwnerToManagerByConfirmSid,
            Map<Long, Integer> targetUpdateDepSidMapByConfirmSid,
            Set<Long> targetDeleteConfirmDepByConfirmSid,
            List<RequireConfirmDepVO> targetInsertRequireConfirmDepVOs,
            List<RequireConfirmDepHistory> targetInsertRequireConfirmDepHistorys,
            Set<String> targetUpdateRequireConfirmDepHistorys,
            Set<String> targetModifyRequireSids,
            Date sysDate) throws SystemDevelopException {

        String requireSid = srcTrnsTo.getRequireSid();
        // log.debug("RequireNo:" + srcTrnsTo.getRequireNo());

        for (Entry<Integer, OrgTrnsConfirmDepResult> entry : srcConfirmDepResultMapByDepSid.entrySet()) {
            // ====================================
            //
            // ====================================
            // 確認單位 sid
            Integer confirmDepSid = entry.getKey();
            // String confirmDepName = WkOrgCache.getInstance().findNameBySid(depSid);
            // 確認單位分析結果
            OrgTrnsConfirmDepResult confirmDepResult = entry.getValue();
            // 處理類型
            ConfirmDepResultProcType procType = confirmDepResult.getProcType(confirmDepSid, srcConfirmDepResultMapByDepSid);

            String historyMemo = "";

            // ====================================
            // 依據處理狀態處理
            // ====================================
            // ----------------
            // 無需執行
            // ----------------
            if (ConfirmDepResultProcType.NONE.equals(procType)) {

            }
            // ----------------
            // 異動分派單位
            // ----------------
            else if (ConfirmDepResultProcType.MODIFY_ASSIGN_DEP.equals(procType)) {
                historyMemo = "因組織異動" + this.prepareAssignDepModifyMemo(confirmDepResult);

                // 加入異動單據 (x)
                // 單位狀態未異動, 不影響流程推進
            }
            // ----------------
            // 新增
            // ----------------
            else if (ConfirmDepResultProcType.ADD.equals(procType)) {
                // 取得流水號
                if (this.maxConfirmDepSid == 0) {
                    this.maxConfirmDepSid = this.requireConfirmDepService.findMaxSid() + 1000;
                }
                // 產生RequireConfirmDep 檔資料
                RequireConfirmDepVO requireConfirmDep = this.createRequireConfirmDepVO(
                        this.maxConfirmDepSid++,
                        confirmDepSid,
                        srcTrnsTo,
                        sysDate);

                targetInsertRequireConfirmDepVOs.add(requireConfirmDep);
                // 放回容器
                confirmDepResult.setRequireConfirmDep(requireConfirmDep);

                // 加入異動記錄
                historyMemo = "因組織異動" + this.prepareAssignDepModifyMemo(confirmDepResult);
                historyMemo += "<br/>並產生需求確認流程，單位確認狀態為【" + requireConfirmDep.getProgStatus().getLabel() + "】";

                // 加入異動單據
                targetModifyRequireSids.add(requireSid);
            }

            // ----------------
            // 刪除
            // ----------------
            else if (ConfirmDepResultProcType.DELETE.equals(procType)) {
                // 加入刪除列表
                targetDeleteConfirmDepByConfirmSid.add(confirmDepResult.getRequireConfirmDep().getSid());
                // 加入異動記錄
                historyMemo = "因組織異動" + this.prepareAssignDepModifyMemo(confirmDepResult);
                historyMemo += "<br/>並移除需求確認流程:" + WkOrgCache.getInstance().findNameBySid(confirmDepSid);
                historyMemo += "<br/>單位確認狀態為【" + confirmDepResult.getRequireConfirmDep().getProgStatus().getLabel()
                        + "】";

                // 加入異動單據
                targetModifyRequireSids.add(requireSid);
            }
            // ----------------
            // 移轉
            // ----------------
            else if (ConfirmDepResultProcType.TRNS.equals(procType)) {

                // 取得移轉前確認檔
                RequireConfirmDepVO beforeConfirmDep = Lists.newArrayList(confirmDepResult.beforeConfirmDeps).get(0);
                if (beforeConfirmDep == null) {
                    continue;
                }

                Long dataRowSid = beforeConfirmDep.getSid();
                Integer beforeConfirmDepSid = beforeConfirmDep.getDepSid();
                Integer afterConfirmDepSid = confirmDepSid;

                // 記錄要 update 的歷史記錄檔
                // dataKey = require_confirm_sid｜轉換前確認單位SID| 轉換後確認單位SID
                String dataKey = dataRowSid + "@" + beforeConfirmDepSid + "@" + afterConfirmDepSid;
                targetUpdateRequireConfirmDepHistorys.add(dataKey);

                // 改為移轉後的 depsid
                beforeConfirmDep.setDepSid(afterConfirmDepSid);
                // 確認單位檔放到正位 （判斷異動負責人用）
                confirmDepResult.setRequireConfirmDep(beforeConfirmDep);
                // 加入移轉列表
                targetUpdateDepSidMapByConfirmSid.put(dataRowSid, afterConfirmDepSid);

                // 異動記錄
                historyMemo = ""
                        + "因組織異動, 進度確認單位移轉：<br/>"
                        + WkOrgUtils.findNameByDecorationStyle(beforeConfirmDep.getDepSid(), false)
                        + " -> "
                        + WkOrgUtils.findNameByDecorationStyle(confirmDepSid, false);
                historyMemo += this.prepareAssignDepModifyMemo(confirmDepResult);

                // 加入異動單據 (x)
                // 移轉為原狀態移轉, 不影響流程推進
            }

            // ----------------
            // 未定義
            // ----------------
            else {
                throw new SystemDevelopException("未定義的處理狀態 ConfirmDepResultProcType:[" + procType + "]");
            }

            // ====================================
            // 寫異動記錄
            // ====================================
            if (WkStringUtils.notEmpty(historyMemo)) {
                targetInsertRequireConfirmDepHistorys.add(
                        this.createRequireConfirmDepHistory(
                                confirmDepResult.getRequireConfirmDep(),
                                historyMemo,
                                sysDate));
            }

            // ====================================
            // 檢查負責人是否需異動
            // ====================================
            // 依據處理類型決定
            if (procType.isCheckOwner()) {
                this.prepareChangeOwner(
                        confirmDepResult.getRequireConfirmDep(),
                        targetUpdateOwnerToManagerByConfirmSid,
                        targetInsertRequireConfirmDepHistorys,
                        sysDate);
            }

        }
    }

    /**
     * @param trnsTo
     * @param allTrnsDepMapping
     * @param requireConfirmDepsByRequireSid
     * @param sysDate
     */
    private Map<Integer, OrgTrnsConfirmDepResult> processTrns_prepareRequireConfirmDep_single(
            OrgTrnsAssignSendInfoForTrnsTO trnsTo,
            Map<Integer, Integer> allTrnsDepMappingByAfter,
            List<RequireConfirmDepVO> requireConfirmDepsByRequireSid,
            Date sysDate) {

        // 將此需求單已存在的『需求確認單位』資料, 以 depSid 進行索引
        // Map<確認單位SID, 需求確認單位資料>
        Map<Integer, RequireConfirmDepVO> requireConfirmDepMapByDepSid = requireConfirmDepsByRequireSid.stream()
                .collect(Collectors.toMap(
                        RequireConfirmDepVO::getDepSid,
                        vo -> vo));

        // ====================================
        // 計算收束後(確認)單位, 並做資料整理
        // ====================================
        // 舊的分派單位
        Set<Integer> oldAssignDepSids = Sets.newHashSet(trnsTo.getDepSids());
        // 新的分派單位
        Set<Integer> newAssignDepSids = Sets.newHashSet(trnsTo.getNewAssignDepSids());

        // Map<確認單位SID, List<來源分派單位(確認單位由此複數單位收束)>>
        Map<Integer, Set<Integer>> oldAssignDepSidMap = this.requireConfirmDepService
                .prepareRequireConfirmDepSidMap(oldAssignDepSids);

        Map<Integer, Set<Integer>> newAssignDepSidMap = this.requireConfirmDepService
                .prepareRequireConfirmDepSidMap(newAssignDepSids);

        // ====================================
        // 準備確認單位檔比對資料 (新+舊確認單位 , 取聯集)
        // ====================================
        // 確認單位檔比對資料
        Map<Integer, OrgTrnsConfirmDepResult> confirmDepResultMapByConfirmSid = Maps.newHashMap();

        // 處理舊分派單位
        for (Integer confirmDepSid : oldAssignDepSidMap.keySet()) {
            this.prepareConfirmDepResult(
                    confirmDepSid,
                    confirmDepResultMapByConfirmSid,
                    oldAssignDepSidMap,
                    newAssignDepSidMap,
                    requireConfirmDepMapByDepSid,
                    allTrnsDepMappingByAfter);
        }

        // 處理新分派單位
        for (Integer confirmDepSid : newAssignDepSidMap.keySet()) {
            this.prepareConfirmDepResult(
                    confirmDepSid,
                    confirmDepResultMapByConfirmSid,
                    oldAssignDepSidMap,
                    newAssignDepSidMap,
                    requireConfirmDepMapByDepSid,
                    allTrnsDepMappingByAfter);
        }

        return confirmDepResultMapByConfirmSid;
    }

    /**
     * 轉需求單位檔
     * 
     * @param assignSendInfoForTrnsVOs
     * @param allTrnsDepMappingByAfter
     * @param sysDate
     * @throws SystemDevelopException
     */
    private void processTrns_trnsRequireConfirmDep(
            List<OrgTrnsAssignSendInfoForTrnsTO> assignSendInfoForTrnsVOs,
            Map<Integer, Integer> allTrnsDepMappingByAfter,
            Date sysDate) throws SystemDevelopException {

        // ====================================
        // 資料收集容器
        // ====================================
        // 負責人異動資料
        Set<Long> targetUpdateOwnerToManagerByConfirmSid = Sets.newHashSet();
        // 要更新單位的確認檔
        Map<Long, Integer> targetUpdateDepSidMapByConfirmSid = Maps.newHashMap();
        // 要刪除的確認檔
        Set<Long> targetDeleteConfirmDepByConfirmSid = Sets.newHashSet();
        // 要新增的確認檔
        List<RequireConfirmDepVO> targetInsertRequireConfirmDepVOs = Lists.newArrayList();
        // 要新增的歷史記錄檔
        List<RequireConfirmDepHistory> targetInsertRequireConfirmDepHistorys = Lists.newArrayList();
        // 要異動的歷史記錄檔
        Set<String> targetUpdateRequireConfirmDepHistorys = Sets.newHashSet();

        // 有異動需求確認單位的單據
        Set<String> targetModifyRequireSids = Sets.newHashSet();

        // ====================================
        // 準備轉檔資料
        // ====================================
        this.processTrns_prepareRequireConfirmDep(
                assignSendInfoForTrnsVOs,
                allTrnsDepMappingByAfter,
                targetUpdateOwnerToManagerByConfirmSid,
                targetUpdateDepSidMapByConfirmSid,
                targetDeleteConfirmDepByConfirmSid,
                targetInsertRequireConfirmDepVOs,
                targetInsertRequireConfirmDepHistorys,
                targetUpdateRequireConfirmDepHistorys,
                targetModifyRequireSids,
                sysDate);

        // Long to String
        Set<String> targetUpdateOwnerToManager = targetUpdateOwnerToManagerByConfirmSid
                .stream().map(v -> v + "")
                .collect(Collectors.toSet());

        Set<Long> trnsConfirmSids = targetUpdateDepSidMapByConfirmSid.keySet();

        Set<String> targetDeleteConfirmDep = targetDeleteConfirmDepByConfirmSid
                .stream()
                .filter(confirmSid -> !trnsConfirmSids.contains(confirmSid)) // 要移轉的不刪除
                .map(confirmSid -> confirmSid + "")
                .collect(Collectors.toSet());

        if (WkStringUtils.notEmpty(targetUpdateDepSidMapByConfirmSid)) {

        }

        // ====================================
        // 批次異動資料
        // ====================================
        // ----------------
        // 異動已不存在的負責人
        // ----------------
        // update tr_require_confirm_dep set owner_sid = -1 where require_confirm_sid =?
        log.debug("將異動已不存在的負責人為單位主管");
        this.set10V70TrneBatchHelper.batchUpdateByColumn(
                "tr_require_confirm_dep",
                "owner_sid",
                WkConstants.MANAGER_VIRTAUL_USER_SID + "",
                "require_confirm_sid",
                targetUpdateOwnerToManager);

        // ----------------
        // 移轉確認檔的 dep_sid
        // ----------------
        // update tr_require_confirm_dep set dep_sid = :value where require_confirm_sid
        // = :key
        log.debug("移轉確認檔");
        this.orgTrnsBatchHelper.batchUpdateRequireConfirmDep(targetUpdateDepSidMapByConfirmSid, sysDate);

        // ----------------
        // 異動確認檔異動記錄
        // ----------------
        log.debug("異動確認檔異動記錄 移轉dep Sid");
        this.orgTrnsBatchHelper.batchUpdateRequireConfirmDepHistory(
                targetUpdateRequireConfirmDepHistorys,
                sysDate);

        // ----------------
        // 刪除無用的確認檔
        // ----------------
        log.debug("刪除無用的確認檔");
        // delete from tr_assign_send_search_info where require_sid in (:require_sids)
        this.set10V70TrneBatchHelper.batchDeleteByColumn(
                "tr_require_confirm_dep",
                "require_confirm_sid",
                targetDeleteConfirmDep);

        // ----------------
        // 新增確認檔
        // ----------------
        // insert tr_require_confirm_dep
        log.debug("新增確認檔");
        this.orgTrnsBatchHelper.batchInsertRequireConfirmDeps(targetInsertRequireConfirmDepVOs);

        // ----------------
        // 新增確認檔異動記錄
        // ----------------
        // insert tr_require_confirm_dep_history
        log.debug("新增確認檔異動記錄");
        this.set10V70TrneBatchHelper.batchInsertRequireConfirmDepHistory(
                targetInsertRequireConfirmDepHistorys, false);

        // ====================================
        // 需求流程推進
        // ====================================
        User execUser = WkUserCache.getInstance().findBySid(1);

        int index = 0;
        for (String requireSid : targetModifyRequireSids) {

            // ====================================
            // 若需要 (現有分派單位皆已完成確認), 則執行需求完成
            // ====================================
            this.requireProcessCompleteService.executeByAllDepConfirm(
                    requireSid,
                    execUser,
                    sysDate,
                    true,
                    true
                    );

            // ====================================
            // 若需要 (製作進度為已完成，但現有分派單位有任一未完成確認), 則執行反需求完成
            // ====================================
            this.requireProcessCompleteRollbackService.executeByModifyAssignDep(
                    requireSid,
                    execUser,
                    sysDate);

            index++;
            if (index % 100 == 0) {
                log.info(index + " ok");
            }
        }
    }
}
