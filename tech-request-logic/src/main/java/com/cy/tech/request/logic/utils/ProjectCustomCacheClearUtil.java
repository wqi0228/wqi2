package com.cy.tech.request.logic.utils;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.system.rest.client.util.SystemProjectCache;
import com.cy.tech.request.logic.config.ReqEhCacheHelper;
import com.cy.work.common.cache.WkCommonCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 專案自訂快取清除工具
 * 
 * @author allen1214_wu
 */
@Slf4j
@Component
public class ProjectCustomCacheClearUtil implements SystemProjectCache {

    @Autowired
    transient private ReqEhCacheHelper reqEhCacheHelper;

    /**
     * 最後一次執行結果
     */
    @Getter
    private List<String> lastExecResult = Lists.newArrayList();

    @Override
    public void doClearProjectCache() {
        this.doClear();
    }

    /**
     * 
     */
    public synchronized void doClear() {

        // 清除上次執行結果 (初始化)
        lastExecResult.clear();
        //
        long startTime = 0;
        String message = "";

        // ====================================
        // WK-COMMON
        // ====================================
        try {
            startTime = System.currentTimeMillis();
            WkCommonCache.getInstance().clearAllCache();
            message = "清除 WK-COMMON 快取成功! 共耗時:[" + WkCommonUtils.calcCostSec(startTime) + "sec]";
            log.info(message);
        } catch (Exception e) {
            message = "清除 WK-COMMON 快取失敗! " + e.getMessage();
            log.error(message, e);
        }
        lastExecResult.add(message);

        // ====================================
        // EHCache
        // ====================================
        try {
            startTime = System.currentTimeMillis();
            this.reqEhCacheHelper.clearCache();
            message = "清除 需求單 EhCache 快取成功! 共耗時:[" + WkCommonUtils.calcCostSec(startTime) + "sec]";
            log.info(message);
        } catch (Exception e) {
            message = "清除 需求單 EhCache 快取 失敗! " + e.getMessage();
            log.error(message, e);
        }
        lastExecResult.add(message);
    }
}
