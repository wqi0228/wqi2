/**
 * 
 */
package com.cy.tech.request.logic.search.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.OrgLevel;

import com.cy.tech.request.logic.search.view.BaseSearchView;
import com.cy.tech.request.logic.service.RequireCheckItemService;
import com.cy.tech.request.logic.service.setting.SettingCheckConfirmRightService;
import com.cy.tech.request.logic.vo.CheckItmesInfoByRequireTo;
import com.cy.tech.request.logic.vo.RequireSimpleInfoTo;
import com.cy.tech.request.vo.enums.CheckItemStatus;
import com.cy.tech.request.vo.enums.ReqCateType;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.require.RequireCheckItem;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 *
 */
@Component
@Slf4j
public class SearchResultHelper implements Serializable, InitializingBean {

    /**
     * 
     */
    private static final long serialVersionUID = 3832552012711852104L;
    // ====================================
    // implement InitializingBean
    // ====================================
    private static SearchResultHelper instance;

    /**
     * get WkCommonCache Instance
     * 
     * @return WkCommonCache
     */
    public static SearchResultHelper getInstance() {
        return instance;
    }

    /**
     * fix for fireBugs warning
     * 
     * @param instance
     */
    private static void setInstance(SearchResultHelper instance) {
        SearchResultHelper.instance = instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    // ====================================
    // 服務區
    // ====================================
    @Autowired
    public transient RequireCheckItemService requireCheckItemService;
    @Autowired
    public transient SettingCheckConfirmRightService settingCheckConfirmRightService;
    @Autowired
    public transient NativeSqlRepository nativeSqlRepository;

    // ====================================
    // 方法區
    // ====================================

    /**
     * @param depSid
     * @param userSid
     * @return
     */
    public boolean filterInactiveByInCharge(Integer depSid, Integer userSid) {
        // 檢查主責單位已停用
        if (!WkOrgUtils.isActive(depSid)) {
            return true;
        }
        // 檢查主要負責人已停用
        if (!WkUserUtils.isActive(userSid)) {
            return true;
        }
        return false;
    }

    /**
     * 過濾『檢查項目』(系統別)
     * 
     * @param requireCaseCheckItems    單筆需求單所有的檢查項目
     * @param queryConditionCheckItems 查詢條件
     * @param isFilterWaitChekItems    是否開啟過濾『有權限、且待檢查項目』
     * @param canCheckItems            個人有權限檢查的項目
     * @return
     */
    public boolean filterCheckItems(
            List<RequireCheckItemType> requireCaseCheckItems,
            List<RequireCheckItemType> queryConditionCheckItems,
            boolean isFilterWaitChekItems,
            List<RequireCheckItemType> canCheckItems) {

        // 無任何項目檢查權限
        if (isFilterWaitChekItems && WkStringUtils.isEmpty(canCheckItems)) {
            return false;
        }
        
        // ====================================
        // 防呆
        // ====================================
        if (requireCaseCheckItems == null) {
            requireCaseCheckItems = Lists.newArrayList();
        }
        if (queryConditionCheckItems == null) {
            queryConditionCheckItems = Lists.newArrayList();
        }
        if (canCheckItems == null) {
            canCheckItems = Lists.newArrayList();
        }

        List<RequireCheckItemType> canShowCheckItems = Lists.newArrayList(queryConditionCheckItems);
        // 交集 (查詢條件 和 可檢查項目 交集 = 可顯示項目)
        if (isFilterWaitChekItems) {
            canShowCheckItems.retainAll(canCheckItems);
        }

        // 逐筆檢查
        for (RequireCheckItemType requireCheckItemType : requireCaseCheckItems) {
            if (canShowCheckItems.contains(requireCheckItemType)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 單位顯示
     * 
     * @param depSid 單位 Sid
     * @return
     */
    public String prepareDepDescr(Integer depSid) {

        if (depSid == null || !WkOrgCache.getInstance().isExsit(depSid)) {
            return "";
        }

        String descr = WkOrgUtils.prepareBreadcrumbsByDepName(
                depSid,
                OrgLevel.MINISTERIAL,
                true, "-");
        if (!WkOrgUtils.isActive(depSid)) {
            descr = WkHtmlUtils.addStrikethroughStyle(descr, "停用");
        }

        return descr;
    }

    /**
     * @param userSid
     * @return
     */
    public String prepareUsrDescr(Integer userSid) {

        if (userSid == null || !WkUserCache.getInstance().isExsit(userSid)) {
            return "";
        }

        String descr = WkUserUtils.findNameBySid(userSid);
        if (!WkUserUtils.isActive(userSid)) {
            descr = WkHtmlUtils.addStrikethroughStyle(descr, "停用");
        }

        return descr;
    }

    /**
     * @param results               查詢結果集
     * @param loginUserSid          登入者 SID
     * @param isShowByCanCheckItems 當項目未檢查，且登入者無檢查項目權限時，顯示【非項目檢查人員】 (否則顯示【待檢查】)
     */
    public void prepareCheckConfirmInfo(
            List<BaseSearchView> results,
            Integer loginUserSid,
            boolean isShowByCanCheckItems) {

        // ====================================
        // 檢核無需處理
        // ====================================
        if (WkStringUtils.isEmpty(results)) {
            return;
        }

        // ====================================
        // 查詢可檢查項目權限
        // ====================================
        List<RequireCheckItemType> canCheckItems = settingCheckConfirmRightService.findUserCheckRights(
                loginUserSid);

        // ====================================
        // 收集 requireSid
        // ====================================
        List<String> requireSids = results.stream()
                .map(BaseSearchView::getSid)
                .distinct()
                .collect(Collectors.toList());

        // ====================================
        // 取得需求單狀態
        // ====================================
        Map<String, RequireSimpleInfoTo> requireInfoMapByRequireSid = this.findCaseStatus(requireSids);

        // ====================================
        // 查詢檢查確認檔資訊
        // ====================================
        // 查詢
        List<RequireCheckItem> requireCheckItems = this.requireCheckItemService.findCheckItemsByRequireSid(requireSids);

        // list 轉 map （grouping By RequireSid）
        Map<String, List<RequireCheckItem>> requireCheckItemsMapByRequireSid = requireCheckItems.stream()
                .collect(Collectors.groupingBy(
                        RequireCheckItem::getRequireSid,
                        Collectors.mapping(
                                each -> each,
                                Collectors.toList())));

        // ====================================
        // 組裝檢查資訊
        // ====================================
        for (BaseSearchView searchView : results) {

            // 取得需求單資訊
            RequireSimpleInfoTo reqInfo = requireInfoMapByRequireSid.get(searchView.getSid());
            if (reqInfo == null) {
                log.warn("找不到需求單資料：[{}]", searchView.getSid());
                continue;
            }

            // 為內部需求時，不顯示檢查資訊
            if (ReqCateType.INTERNAL.name().equals(reqInfo.getReqCateType())) {
                searchView.setCheckConfirmInfo(WkHtmlUtils.addClass(
                        CheckItemStatus.INTERNAL.getDescr(),
                        CheckItemStatus.INTERNAL.getCssClass()));
                continue;
            }

            // 依據製作進度，決定是否顯示檢查資訊
            RequireStatusType requireStatusType = RequireStatusType.safeValueOf(reqInfo.getRequireStatus());
            if (requireStatusType == null || !requireStatusType.isShowRequireCheckHistory()) {
                continue;
            }

            // 取得單據的檢查項目檔
            List<RequireCheckItem> currRequireCheckItemsByRequireSid = requireCheckItemsMapByRequireSid.get(searchView.getSid());
            if (WkStringUtils.isEmpty(currRequireCheckItemsByRequireSid)) {
                continue;
            }

            // 取得檢查資訊
            CheckItmesInfoByRequireTo checkItmesInfoByRequireTo = this.requireCheckItemService.prepareCheckItemsInfo(
                    reqInfo.getRequireSid(),
                    ReqCateType.safeValueOf(reqInfo.getReqCateType()),
                    requireStatusType,
                    currRequireCheckItemsByRequireSid,
                    canCheckItems);

            searchView.setCheckItemTypes(checkItmesInfoByRequireTo.getCheckItemTypes());
            searchView.setCheckConfirmInfo(checkItmesInfoByRequireTo.getCheckConfirmInfo());
            searchView.setCheckConfirmUserSids(checkItmesInfoByRequireTo.getCheckConfirmUserSids());
            // 異常狀態 REQ-1593
            searchView.setExceptionCase(checkItmesInfoByRequireTo.isExceptionCase());
        }
    }

    private Map<String, RequireSimpleInfoTo> findCaseStatus(List<String> requireSids) {

        if (WkStringUtils.isEmpty(requireSids)) {
            return Maps.newHashMap();
        }

        // ====================================
        // 查詢
        // ====================================
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT req.require_sid    AS requireSid, ");
        sql.append("       req.require_status AS requireStatus, ");
        sql.append("       bcg.req_category   AS reqCateType ");
        sql.append("FROM   tr_require req ");
        sql.append("       INNER JOIN tr_category_key_mapping km ");
        sql.append("               ON km.key_sid = req.mapping_sid ");
        sql.append("       INNER JOIN tr_basic_data_big_category bcg ");
        sql.append("               ON bcg.basic_data_big_category_sid = km.big_category_sid ");
        sql.append("WHERE  req.require_sid IN ( '" + String.join("','", requireSids) + "' )");
        sql.append("GROUP BY req.require_sid ");

        List<RequireSimpleInfoTo> results = this.nativeSqlRepository.getResultList(sql.toString(), null, RequireSimpleInfoTo.class);

        if (WkStringUtils.isEmpty(results)) {
            return Maps.newHashMap();
        }

        // ====================================
        // 整理資料 map by requireSid
        // ====================================
        return results.stream()
                .collect(Collectors.toMap(
                        RequireSimpleInfoTo::getRequireSid,
                        each -> each));

    }
}
