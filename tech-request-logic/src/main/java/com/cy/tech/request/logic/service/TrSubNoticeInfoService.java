/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.logic.vo.TrSubNoticeInfoTo;
import com.cy.tech.request.repository.require.TrSubNoticeInfoRepository;
import com.cy.tech.request.vo.enums.SubNoticeType;
import com.cy.tech.request.vo.require.TrSubNoticeInfo;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 子程序異動記錄 Service
 *
 * @author kasim
 */
@Component
@Slf4j
public class TrSubNoticeInfoService implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 9004677468303976800L;
    @Autowired
	private TrSubNoticeInfoRepository subNoticeInfoRepo;
	@Autowired
	private UserService userService;
	@Autowired
	private OrganizationService orgService;

	private String spanRedStyle = "<span style=\"color:red\">";
	private String spanBlueStyle = "<span style=\"color:blue\">";
	private String endSpan = "</span>";

	/**
	 * 建立 第一筆異動紀錄
	 *
	 * @param type
	 * @param require
	 * @param subProcessSid
	 * @param subProcessNo
	 * @param noticeInfo
	 * @param createdUser
	 */
	@Transactional(rollbackFor = Exception.class)
	public void createFirstSubChangeRecord(
	        SubNoticeType type,
	        String requireSid,
	        String requireNo,
	        String subProcessSid, String subProcessNo, JsonStringListTo noticeInfo, Integer createdUser) {
		this.createSubChangeRecord(type, requireSid, requireNo, subProcessSid, subProcessNo, null, noticeInfo, createdUser);
	}

	/**
	 * 建立 異動紀錄
	 *
	 * @param type
	 * @param require
	 * @param subProcessSid
	 * @param subProcessNo
	 * @param dbInfo
	 * @param noticeInfo
	 * @param createdUser
	 */
	@Transactional(rollbackFor = Exception.class)
	public void createSubNoticeChangeRecord(
	        SubNoticeType type,
	        String requireSid,
	        String requireNo,
	        String subProcessSid,
	        String subProcessNo,
	        Set<Integer> oldNoticeSids,
	        Set<Integer> newNoticeSids,
	        Integer createdUser) {

		List<String> oldSids = Lists.newArrayList();
		for (Integer sidStr : oldNoticeSids) {
			oldSids.add(sidStr + "");
		}

		List<String> newSids = Lists.newArrayList();
		for (Integer sidStr : newNoticeSids) {
			newSids.add(sidStr + "");
		}

		// 比對新舊資料無差異時, 不寫異動記錄
		if (WkCommonUtils.compare(oldSids, newSids)) {
			log.debug("新增異動記錄：[" + type.getName() + "] 比對無差異 pass");
			return;
		}

		this.create(
		        type,
		        requireSid,
		        requireNo,
		        subProcessSid,
		        subProcessNo,
		        oldSids,
		        newSids,
		        createdUser,
		        new Date());

	}

	/**
	 * 建立 異動紀錄
	 *
	 * @param type
	 * @param require
	 * @param subProcessSid
	 * @param subProcessNo
	 * @param dbInfo
	 * @param noticeInfo
	 * @param createdUser
	 */
	@Transactional(rollbackFor = Exception.class)
	public void createSubChangeRecord(
	        SubNoticeType type,
	        String requireSid,
	        String requireNo,
	        String subProcessSid,
	        String subProcessNo,
	        JsonStringListTo dbInfo,
	        JsonStringListTo noticeInfo,
	        Integer createdUser) {

		// 排除 null
		noticeInfo = this.getJsonStringListTo(noticeInfo);
		dbInfo = this.getJsonStringListTo(dbInfo);

		// 比對新舊資料無差異時, 不寫異動記錄
		if (WkCommonUtils.compare(noticeInfo.getValue(), dbInfo.getValue())) {
			return;
		}

		this.create(
		        type,
		        requireSid,
		        requireNo,
		        subProcessSid,
		        subProcessNo,
		        dbInfo.getValue(),
		        noticeInfo.getValue(),
		        createdUser,
		        new Date());
	}

	/**
	 * 取得自訂物件
	 *
	 * @param to
	 * @return
	 */
	private JsonStringListTo getJsonStringListTo(JsonStringListTo to) {
		if (to == null) {
			return new JsonStringListTo();
		}
		if (to.getValue() == null) {
			return new JsonStringListTo();
		}
		return to;
	}

	/**
	 * 建立異動紀錄
	 * 
	 * @param type          子程序異動記錄 通知類型
	 * @param requireSid    需求單 SID
	 * @param requireNo     需求單號
	 * @param subProcessSid 子單SID
	 * @param subProcessNo  子單單號
	 * @param oldSids       舊的人員或部門 SID
	 * @param newSids       新的人員或部門 SID
	 * @param createdUser   操作者
	 * @param sysDate       系統時間
	 */
	@Transactional(rollbackFor = Exception.class)
	private void create(SubNoticeType type,
	        String requireSid,
	        String requireNo,
	        String subProcessSid,
	        String subProcessNo,
	        List<String> oldSids,
	        List<String> newSids,
	        Integer createdUser,
	        Date sysDate) {

		// ====================================
		// 準備資料
		// ====================================
		if (sysDate == null) {
			sysDate = new Date();
		}

		JsonStringListTo oldNoticeInfo = new JsonStringListTo();
		if (oldSids == null) {
			oldSids = Lists.newArrayList();
		}
		oldNoticeInfo.setValue(oldSids);

		JsonStringListTo newNoticeInfo = new JsonStringListTo();
		if (newSids == null) {
			newSids = Lists.newArrayList();
		}
		newNoticeInfo.setValue(newSids);

		// ====================================
		// insert
		// ====================================
		subNoticeInfoRepo.save(new TrSubNoticeInfo(
		        UUID.randomUUID().toString(),
		        type,
		        requireSid,
		        requireNo,
		        subProcessSid,
		        subProcessNo,
		        oldNoticeInfo,
		        newNoticeInfo,
		        Activation.ACTIVE,
		        createdUser,
		        sysDate,
		        createdUser,
		        sysDate));
	}

	/**
	 * 取得有效資料
	 *
	 * @param requireSid
	 * @param subProcessSid
	 * @param types
	 * @return
	 */
	public List<TrSubNoticeInfoTo> findActiveByRequireSidAndSubProcessSidAndTypeIn(
	        String requireSid, String subProcessSid, List<SubNoticeType> types) {
		Preconditions.checkState(!Strings.isNullOrEmpty(requireSid), "需求單資訊不可為空!!");
		Preconditions.checkState(!Strings.isNullOrEmpty(subProcessSid), "子程序不可為空!!");
		Preconditions.checkNotNull(types != null && !types.isEmpty(), "異動記錄型態不可為空，請確認");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		return subNoticeInfoRepo.findByStatusAndRequireSidAndSubProcessSidAndTypeIn(
		        Activation.ACTIVE, requireSid, subProcessSid, types).stream()
		        .map(each -> {
			        return new TrSubNoticeInfoTo(
			                each.getSid(),
			                userService.getUserName(each.getUpdatedUser()),
			                this.getBeforeInfo(each.getType(), each.getOldNoticeInfo().getValue(), each.getNewNoticeInfo().getValue()),
			                this.getAfterInfoInfo(each.getType(), each.getOldNoticeInfo().getValue(), each.getNewNoticeInfo().getValue()),
			                sdf.format(each.getUpdatedDate()));
		        })
		        .collect(Collectors.toList());
	}

	/**
	 * 取得異動前資訊
	 *
	 * @param type
	 * @param oldValue
	 * @param newValue
	 * @return
	 */
	private String getBeforeInfo(SubNoticeType type, List<String> oldValue, List<String> newValue) {
		if (SubNoticeType.PT_NOTICE_MEMBER.equals(type)) {
			return this.getBeforeInfoByUser(oldValue, newValue);
		}
		return this.getBeforeInfoByOrg(oldValue, newValue);
	}

	/**
	 * 取得異動前資訊
	 *
	 * @param oldValue
	 * @param newValue
	 * @return
	 */
	private String getBeforeInfoByOrg(List<String> oldValue, List<String> newValue) {
		return oldValue.stream()
		        .map(each -> {
			        if (newValue.contains(each)) {
				        return orgService.getOrgName(each);
			        }
			        return spanRedStyle + orgService.getOrgName(each) + endSpan;
		        })
		        .collect(Collectors.joining("、"));
	}

	/**
	 * 取得異動前資訊
	 *
	 * @param oldValue
	 * @param newValue
	 * @return
	 */
	private String getBeforeInfoByUser(List<String> oldValue, List<String> newValue) {
		return oldValue.stream()
		        .map(each -> {
			        if (newValue.contains(each)) {
				        return userService.getUserName(each);
			        }
			        return spanRedStyle + userService.getUserName(each) + endSpan;
		        })
		        .collect(Collectors.joining("、"));
	}

	/**
	 * 取得異動後資訊
	 *
	 * @param type
	 * @param oldValue
	 * @param newValue
	 * @return
	 */
	private String getAfterInfoInfo(SubNoticeType type, List<String> oldValue, List<String> newValue) {
		if (SubNoticeType.PT_NOTICE_MEMBER.equals(type)) {
			return this.getAfterInfoInfoByUser(oldValue, newValue);
		}
		return this.getAfterInfoInfoByOrg(oldValue, newValue);
	}

	/**
	 * 取得異動後資訊
	 *
	 * @param oldValue
	 * @param newValue
	 * @return
	 */
	private String getAfterInfoInfoByOrg(List<String> oldValue, List<String> newValue) {
		return newValue.stream()
		        .map(each -> {
			        if (oldValue.contains(each)) {
				        return orgService.getOrgName(each);
			        }
			        return spanBlueStyle + orgService.getOrgName(each) + endSpan;
		        })
		        .collect(Collectors.joining("、"));
	}

	/**
	 * 取得異動後資訊
	 *
	 * @param oldValue
	 * @param newValue
	 * @return
	 */
	private String getAfterInfoInfoByUser(List<String> oldValue, List<String> newValue) {
		return newValue.stream()
		        .map(each -> {
			        if (oldValue.contains(each)) {
				        return userService.getUserName(each);
			        }
			        return spanBlueStyle + userService.getUserName(each) + endSpan;
		        })
		        .collect(Collectors.joining("、"));
	}

	/**
	 * 取得通知異動紀錄
	 */
	public List<TrSubNoticeInfo> findNoticeInfoByTypeAndNo(SubNoticeType type, String no) {
		return subNoticeInfoRepo.findNoticeInfoByTypeAndNo(type, no);
	}

}
