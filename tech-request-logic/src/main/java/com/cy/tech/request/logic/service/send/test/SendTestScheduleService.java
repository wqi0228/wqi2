package com.cy.tech.request.logic.service.send.test;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.logic.utils.DateUtils;
import com.cy.tech.request.repository.setting.WorkTestScheduleRepository;
import com.cy.tech.request.vo.setting.WorkTestSchedule;
import com.cy.tech.request.vo.value.to.WorkTestScheduleVO;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

/**
 * @author aken_kao
 */
@Slf4j
@Service
public class SendTestScheduleService {

    @Autowired
    private WorkTestScheduleRepository workTestScheduleRepository;
    
    @Transactional(rollbackFor = Exception.class)
    public void saveOrUpdate(WorkTestScheduleVO vo, Integer loginUserSid){
        WorkTestSchedule schedule = findByEventId(vo.getEventId());
        if(schedule == null){
            schedule = new WorkTestSchedule();
            schedule.setCreatedDate(new Date());
            schedule.setCreatedUser(loginUserSid);
        } else {
            schedule.setUpdatedDate(new Date());
            schedule.setUpdatedUser(loginUserSid);
        }
        schedule.setEventId(vo.getEventId());
        schedule.setScheduleName(vo.getScheduleName());
        schedule.setStartDate(vo.getStartDate());
        schedule.setEndDate(DateUtils.convertToEndOfDay(vo.getEndDate()));
        schedule.setColorCode(vo.getColorCode());
        schedule.setFull("Y".equals(vo.getIsFull()));
        workTestScheduleRepository.save(schedule);
    }
    
    /**
     * 刪除, 停用資料
     * @param eventId
     */
    @Transactional(rollbackFor = Exception.class)
    public void inactiveData(String eventId, Integer loginUserSid) throws Exception{
        WorkTestSchedule schedule = findByEventId(eventId);
        if(schedule != null){
            schedule.setStatus(Activation.INACTIVE);
            schedule.setUpdatedDate(new Date());
            schedule.setUpdatedUser(loginUserSid);
            workTestScheduleRepository.save(schedule);
        } else {
            log.info("could not find WorkTestSchedule entity, by eventId={}", eventId);
            throw new Exception("could not delete data!");
        }
    }

    public WorkTestSchedule findByEventId(String eventId) {
        return workTestScheduleRepository.findByEventId(eventId);
    }

    public List<WorkTestScheduleVO> findByDateRange(Date startDate, Date endDate){
        return convert(workTestScheduleRepository.findByDateRange(startDate, endDate));
    }
    
    private List<WorkTestScheduleVO> convert(List<WorkTestSchedule> list){
        List<WorkTestScheduleVO> resultList = Lists.newArrayList();
        if(!CollectionUtils.isEmpty(list)){
            for(WorkTestSchedule entity : list){
                WorkTestScheduleVO vo = new WorkTestScheduleVO(entity);
                resultList.add(vo);
            }
        }
        return resultList;
    }
    
    public boolean isFullByDate(Date checkDate){
        return workTestScheduleRepository.countHasFullByDate(checkDate) > 0;
    }
}
