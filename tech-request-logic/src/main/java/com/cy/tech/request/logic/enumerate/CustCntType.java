/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.enumerate;

import lombok.Getter;

/**
 * 選單顯示數量, 客製化查詢類別
 * <P/>
 * fun_item 裡url 型式必需為 CUST_QUERY_ 開頭
 *
 * @author Allen
 */
public enum CustCntType {
    
    CUST_QUERY_WAIT_COMFIRM("未領單據查詢"),
    CUST_QUERY_WAIT_CHECK("待檢查確認單據查詢");;
	
    @Getter
    private final String label;

    private CustCntType(String label) {
        this.label = label;
    }
}
