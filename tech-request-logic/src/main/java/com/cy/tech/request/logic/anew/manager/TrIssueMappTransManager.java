/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.anew.manager;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.anew.config.ReqLogicConstants;
import com.cy.tech.request.logic.service.AssignNoticeService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.RequireTraceService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.utils.DateUtils;
import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.group.logic.RelevanceManager;
import com.cy.work.group.vo.WorkLinkGroup;
import com.cy.work.group.vo.WorkLinkGroupDetail;
import com.cy.work.mapp.create.trans.logic.MappCreateTransManager;
import com.cy.work.mapp.create.trans.vo.MappCreateTrans;
import com.cy.work.mapp.create.trans.vo.enums.MappTransType;
import com.cy.work.tech.issue.client.exception.TransReqClientClientException;
import com.cy.work.tech.issue.client.req.TransReqClient;
import com.cy.work.tech.issue.client.to.vo.TransReqTo;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.HtmlUtils;

/**
 * 轉入需求單 管理
 *
 * @author kasim
 */
@Component
@Slf4j
public class TrIssueMappTransManager implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -2144308761200084712L;
    @Autowired
	private MappCreateTransManager mappCreateTransManager;
	@Autowired
	private TransReqClient transReqClient;
	@Autowired
	private RequireTraceService requireTraceService;
	@Autowired
	private RequireService requireService;
	@Autowired
	private ReqLogicConstants logicConstants;
	@Autowired
	private URLService urlService;
	@Autowired
	private RelevanceManager relevanceManager;
	@Autowired
	private AssignNoticeService assignNoticeService;

	/**
	 * 查詢 by sid
	 *
	 * @param sid
	 * @return
	 */
	private MappCreateTrans findOne(String sid) {
		if (!Strings.isNullOrEmpty(sid)) {
			return mappCreateTransManager.findBySid(sid);
		}
		return null;
	}

	/**
	 * 取得尚未轉入的資料
	 *
	 * @param sid
	 * @param userSid
	 * @return
	 */
	public MappCreateTrans getNoMappTransBySidAndUser(String sid, Integer userSid) {
		MappCreateTrans obj = this.findOne(sid);
		if (obj == null
		        || !Strings.isNullOrEmpty(obj.getTargetSid())
		        || !obj.getCreatedUser().equals(userSid)) {
			return null;
		}
		return obj;
	}

	/**
	 * 取得尚未轉入的資料
	 *
	 * @param sid
	 * @param userSid
	 * @return
	 */
	public MappCreateTrans getNoOnpgTransBySidAndUser(String sid, Integer userSid) {
		MappCreateTrans obj = this.findOne(sid);
		if (obj == null
		        || !Strings.isNullOrEmpty(obj.getOnpgNo())
		        || !MappTransType.ISSUE_TO_REQ_ONPG.equals(obj.getType())
		        || !obj.getCreatedUser().equals(userSid)) {
			return null;
		}
		return obj;
	}

	/**
	 * 取得為案件單轉入的需求單sid
	 *
	 * @param reqSids
	 * @return
	 */
	public List<String> createTransFromIssue(List<String> reqSids) {
		if (reqSids == null || reqSids.isEmpty()) {
			return Lists.newArrayList();
		}
		return mappCreateTransManager.findTargetSidByTypeInAndTargetSids(
		        mappCreateTransManager.getTypesByTransReq(), reqSids);
	}

	/**
	 * 檢查trans物件是否已被建立相關資訊
	 *
	 * @param transSid
	 * @throws UserMessageException
	 */
	public void checkInputInfo(String transSid) throws UserMessageException {
		MappCreateTrans mappTrans = mappCreateTransManager.findBySid(transSid);

		if (!MappTransType.ISSUE_TO_REQ_PROGRAM_REQ.equals(mappTrans.getType())) {
			boolean isAnyMappBySourceSidAndTypeIn = mappCreateTransManager.isAnyMappBySourceSidAndTypeIn(
			        mappTrans.getSourceSid(), mappCreateTransManager.getTypesByTransReq());

			if (isAnyMappBySourceSidAndTypeIn) {
				throw new UserMessageException("案件端來源已寫入需求單相關資訊！！", InfomationLevel.WARN);
			}
		}
	}

	public void processIssueRequireMapping(
	        String transSid,
	        String requireSid,
	        String requireNo,
	        WorkOnpg workOnpg,
	        User execUser,
	        Date execDate) throws SystemOperationException {

		// 沒 transSid 代表非轉單, 不執行
		if (Strings.isNullOrEmpty(transSid)) {
			return;
		}

		// ====================================
		// 取得基礎資料
		// ====================================
		// 取得轉單檔
		MappCreateTrans mappTrans = mappCreateTransManager.findBySid(transSid);
		String onpgNo = workOnpg == null ? null : workOnpg.getOnpgNo();

		// 取得需求單資料
		Require require = this.requireService.findByReqSid(requireSid);

		// ====================================
		// 取得案件單狀態 (call rest receive)
		// ====================================
		String dispatchStatus = "";
		String issueStatus = "";
		try {
			// 取得案件單派工狀態
			dispatchStatus = transReqClient.findDispatchStatus(Long.valueOf(mappTrans.getSourceSid()));
			// 取得案件單狀態
			issueStatus = transReqClient.findIssueStatus(Long.valueOf(mappTrans.getSourceSid()));
		} catch (Exception e) {
			log.error("取得案件單資料失敗!", e);
			throw new SystemOperationException(WkMessage.EXECTION, InfomationLevel.ERROR);
		}

		// ====================================
		// 寫需求單追蹤
		// ====================================
		this.requireTraceService.createIssueTransOnpgTrace(
		        requireSid,
		        this.prepareTrnsTraceContent(false, requireNo, mappTrans, dispatchStatus, issueStatus, workOnpg),
		        execUser,
		        execDate);

		// ====================================
		//
		// ====================================
		String groupSid = "";
		try {
			groupSid = this.transReqClient.processForIssueTrnsRequire(
			        transSid,
			        requireSid,
			        requireNo,
			        require.getHopeDate(),
			        onpgNo,
			        this.prepareTrnsTraceContent(true, requireNo, mappTrans, dispatchStatus, issueStatus, workOnpg),
			        execUser.getSid());

			log.debug("[{}]:處理案件單轉需求單程序 (call 案件單 rest) 完成!",
			        requireNo);

		} catch (Exception e) {
			log.error("呼叫案件單 REST 失敗!", e);
			throw new SystemOperationException(WkMessage.EXECTION, InfomationLevel.ERROR);
		}

		// ====================================
		// update 關聯資訊 to 需求單主檔
		// ====================================
		if (WkStringUtils.notEmpty(groupSid)) {

			// 案件單 rest 不見得以經 commit , 故直接塞 groupSid 即可
			// 查詢關聯主檔
			// WorkLinkGroup workLinkGroup = workLinkGroupManager.findBySid(groupSid);

			require.setLinkGroup(new WorkLinkGroup(groupSid));
			require.setHasLink(Boolean.TRUE);
			require = this.requireService.save(require);
		}
	}

	/**
	 * 建立需求單、需求單追蹤、案件單追蹤、及與案件單對應
	 * 
	 * @param transSid
	 * @param requireSid
	 * @param requireNo
	 * @param workOnpg
	 * @param execUser
	 * @param execDate
	 * @throws UserMessageException
	 * @throws SystemOperationException
	 */
	@Transactional(rollbackFor = Exception.class)
	public void saveByMappTrans(
	        String transSid,
	        String requireSid,
	        String requireNo,
	        WorkOnpg workOnpg,
	        User execUser,
	        Date execDate) throws UserMessageException, SystemOperationException {

		if (Strings.isNullOrEmpty(transSid)) {
			return;
		}

		MappCreateTrans mappTrans = mappCreateTransManager.findBySid(transSid);
		String onpgNo = workOnpg == null ? null : workOnpg.getOnpgNo();

		// ====================================
		// 取得案件單狀態 (call rest receive)
		// ====================================
		String dispatchStatus = "";
		String issueStatus = "";
		try {
			// 取得案件單派工狀態
			dispatchStatus = transReqClient.findDispatchStatus(Long.valueOf(mappTrans.getSourceSid()));
			// 取得案件單狀態
			issueStatus = transReqClient.findIssueStatus(Long.valueOf(mappTrans.getSourceSid()));
		} catch (Exception e) {
			log.error("取得案件單資料失敗!", e);
			throw new SystemOperationException(WkMessage.EXECTION, InfomationLevel.ERROR);
		}

		// ====================================
		// 更新對應資料
		// ====================================
		mappTrans = mappCreateTransManager.updateTarget(
		        requireNo,
		        mappTrans,
		        requireSid,
		        onpgNo,
		        execUser.getSid(),
		        execDate);

		// ====================================
		// 建立轉入關連
		// ====================================
		try {
			this.createRelevanceMapping(
			        requireSid,
			        mappTrans,
			        execUser.getSid());
		} catch (TransReqClientClientException e) {
			log.error("呼叫案件單 REST 失敗! [" + e.getMessage() + "]", e);
			throw new SystemOperationException("呼叫案件單 REST 失敗! [" + e.getMessage() + "]", InfomationLevel.ERROR);
		}

		// ====================================
		// 寫需求單追蹤記錄 (insert)
		// ====================================

		this.requireTraceService.createIssueTransOnpgTrace(
		        requireSid,
		        mappTrans,
		        dispatchStatus,
		        issueStatus,
		        execUser,
		        execDate,
		        workOnpg);

		// ====================================
		// 同步預計上線日至案件單
		// ====================================
		this.syncHopeDateToTechnicalCase(requireSid, execUser.getSid());

		// ====================================
		// 寫案件單追蹤記錄 (call rest insert)
		// ====================================
		TransReqTo to = new TransReqTo(
		        mappTrans.getSid(),
		        this.prepareTrnsTraceContent(
		                true,
		                requireNo,
		                mappTrans,
		                dispatchStatus,
		                issueStatus,
		                workOnpg),
		        execUser.getSid());

		try {
			transReqClient.createReply(to);
			log.debug("[{}]:寫案件單轉入追蹤 - 案件單 (call rest insert)",
			        requireNo);
		} catch (Exception e) {
			log.error("寫案件單追蹤記錄失敗!", e);
			throw new SystemOperationException(WkMessage.EXECTION, InfomationLevel.ERROR);
		}
	}

	/**
	 * 同步預計上線日至案件單<br/>
	 * 1.需為內部需求<br/>
	 * 2.有關連需求單
	 * 
	 * @param requireSid
	 * @param execUserSid
	 * @throws UserMessageException
	 * @throws SystemOperationException
	 */
	public void syncHopeDateToTechnicalCase(String requireSid, Integer execUserSid) throws UserMessageException, SystemOperationException {

		// ====================================
		// 查詢相關資料
		// ====================================
		// 主檔
		Require require = this.requireService.findByReqSid(requireSid);
		if (require == null) {
			throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
		}
		// 為外部需求時不處理
		if (!this.requireService.isTypeInternal(require)) {
			return;
		}

		// 取得對應資料
		List<MappCreateTrans> mappList = this.findByTargetSid(require.getSid());
		if (WkStringUtils.isEmpty(mappList)) {
			return;
		}

		// 取得最新期望完成日
		String issueSid = mappList.get(0).getSourceSid();
		Date maxHopeDate = this.findMaxHopeDateBySourceSid(mappList.get(0).getSourceSid());

		// ====================================
		// 有調整時才更新
		// ====================================
		if (DateUtils.isSameDate(require.getHopeDate(), maxHopeDate)) {
			return;
		}

		// ====================================
		// 更新案件單期望完成日
		// ====================================
		try {
			transReqClient.updateExpectedDate(
			        Long.valueOf(issueSid),
			        DateUtils.YYYY_MM_DD.print(maxHopeDate.getTime()),
			        execUserSid);
		} catch (Exception e) {
			log.error("同步預計上線日至案件單失敗!", e);
			throw new SystemOperationException(WkMessage.EXECTION, InfomationLevel.ERROR);
		}

		log.info("[{}] 同步『預計上線日至案件單』:[{}]-{}",
		        require.getRequireNo(),
		        issueSid,
		        maxHopeDate);
	}

	/**
	 * 建立需求單、需求單追蹤、案件單追蹤、及與案件單對應
	 *
	 * @param transSid
	 * @param require
	 * @param onpgNo
	 * @param executor
	 * @param depSid
	 * @throws TransReqClientClientException
	 * @throws SystemDevelopException
	 * @throws UserMessageException
	 * @throws NumberFormatException 
	 */
	@Transactional(rollbackFor = Exception.class)
	public void saveByOnpg(String transSid, Require require, WorkOnpg workOnpg, User executor, Integer depSid) throws UserMessageException, NumberFormatException, TransReqClientClientException
	       {
		if (Strings.isNullOrEmpty(transSid)) {
			return;
		}
		Preconditions.checkArgument(require != null, "參數不可為空！！");
		Preconditions.checkArgument(!Strings.isNullOrEmpty(workOnpg.getOnpgNo()), "參數不可為空！！");
		Preconditions.checkArgument(executor != null, "參數不可為空！！");
		Preconditions.checkArgument(depSid != null, "參數不可為空！！");
		MappCreateTrans mappTrans = mappCreateTransManager.findBySid(transSid);
		mappTrans = mappCreateTransManager.updateOnpgNo(mappTrans, workOnpg.getOnpgNo(), executor.getSid());
		String dispatchStatus = transReqClient.findDispatchStatus(Long.valueOf(mappTrans.getSourceSid()));
		String issueStatus = transReqClient.findIssueStatus(Long.valueOf(mappTrans.getSourceSid()));
		requireTraceService.createIssueTransOnpgTrace(require.getSid(), mappTrans, dispatchStatus, issueStatus, executor,
		        new Date(), workOnpg);
		TransReqTo to = new TransReqTo(mappTrans.getSid(),
		        this.prepareTrnsTraceContent(true, require.getRequireNo(), mappTrans, dispatchStatus, issueStatus, workOnpg),
		        executor.getSid());
		transReqClient.createReply(to);

		// ====================================
		// 加派自己單位
		// ====================================
		this.assignNoticeService.processForIssueTrns(
		        require.getSid(),
		        require.getRequireNo(),
		        executor,
		        new Date());

		// assignSendInfoService.updateAssignSendInfoByIssueTransOnpg(require, executor,
		// String.valueOf(depSid));
	}

	/**
	 * 準備案件單轉需求單的追蹤內容
	 * 
	 * @param isForIssue     產出內容是否為案件單用 (是:案件單, 否:需求單)
	 * @param requireNo      需求單號
	 * @param mappTrans      單據轉換對應
	 * @param dispatchStatus 案件單分派狀態
	 * @param issueStatus    案件單單據狀態
	 * @param workOnpg       自動 on 程式資料 (如果有有的話)
	 * @return
	 */
	private String prepareTrnsTraceContent(
	        boolean isForIssue,
	        String requireNo,
	        MappCreateTrans mappTrans,
	        String dispatchStatus,
	        String issueStatus,
	        WorkOnpg workOnpg) {

		StringBuilder content = new StringBuilder();
		content.append("1.派工狀態：")
		        .append(dispatchStatus)
		        .append("<br>");
		content.append("2.案件狀態：")
		        .append(issueStatus)
		        .append("<br>");

		if (isForIssue) {
			content.append("3.轉入需求單號：<a target=\"_blank\" href=\"")
			        .append(logicConstants.getOpenerReqUrlKey())
			        .append(urlService.createSimpleURLLink(URLService.URLServiceAttr.URL_ATTR_M, requireNo, 999))
			        .append("\" style=\"color: blue !important;text-decoration: underline !important;\">")
			        .append(requireNo)
			        .append("</a><br>");
		} else {
			content.append("3.轉入案件單號：")
			        .append("<a target=\"_blank\" href=\"")
			        .append(logicConstants.getOpenerIssueUrlKey())
			        .append(mappTrans.getMessageTo().getIssueNo())
			        .append("\" style=\"color: blue !important;text-decoration: underline !important;\">")
			        .append(mappTrans.getMessageTo().getIssueNo())
			        .append("</a><br>");
		}

		content.append("4.需求類別：")
		        .append(mappTrans.getMessageTo().getSmallCategoryName());

		if (workOnpg != null && WkStringUtils.notEmpty(workOnpg.getOnpgNo())) {
			content.append("<br>")
			        .append("5.轉入的ON程式單號：")
			        .append(workOnpg.getOnpgNo());
			content.append("<br>")
			        .append("6.ON程式主題：")
			        .append(HtmlUtils.htmlEscape(workOnpg.getTheme()));
		}
		return content.toString();
	}

	/**
	 * 建立轉入關聯
	 * 
	 * @param requireSid
	 * @param mappCreateTrans
	 * @param executor
	 * @return
	 * @throws UserMessageException
	 * @throws TransReqClientClientException
	 */
	@Transactional(rollbackFor = Exception.class)
	private Require createRelevanceMapping(
	        String requireSid,
	        MappCreateTrans mappCreateTrans,
	        Integer execUser) throws UserMessageException, TransReqClientClientException {

		// ====================================
		// 查詢主檔
		// ====================================
		Require require = this.requireService.findByReqSid(requireSid);
		if (require == null) {
			throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
		}

		// ====================================
		// 建立關連
		// ====================================
		WorkLinkGroupDetail linkDetail1 = new WorkLinkGroupDetail(
		        mappCreateTrans.getSid(),
		        mappCreateTrans.getMessageTo().getIssueNo(),
		        WorkSourceType.TECH_ISSUE);

		WorkLinkGroupDetail linkDetail2 = new WorkLinkGroupDetail(
		        require.getSid(),
		        require.getRequireNo(),
		        WorkSourceType.TECH_REQUEST);

		WorkLinkGroup group = relevanceManager.createRelevance(
		        Lists.newArrayList(linkDetail1, linkDetail2),
		        execUser);

		// ====================================
		// update 主檔
		// ====================================
		if (group != null) {
			require.setLinkGroup(group);
			require.setHasLink(Boolean.TRUE);
			require = this.requireService.save(require);
		}

		log.debug("[{}]-建立轉入關連 work_link_groupinfo:[{}]",
		        require.getRequireNo(),
		        group.getSid());

		return require;
	}

	/**
	 * 搜尋所有mapping設定 by 關聯需求單sid
	 * 
	 * @param requireSid
	 * @return
	 */
	public List<MappCreateTrans> findByTargetSid(String requireSid) {
		return mappCreateTransManager.findByTargetSid(requireSid);
	}

	/**
	 * 取得最新期望完成日
	 * 
	 * @param sourceSid
	 * @return
	 */
	public Date findMaxHopeDateBySourceSid(String sourceSid) {
		return mappCreateTransManager.findMaxHopeDateBySourceSid(sourceSid);
	}
}
