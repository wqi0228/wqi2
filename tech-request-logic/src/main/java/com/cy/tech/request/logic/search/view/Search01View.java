/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import java.io.Serializable;
import java.util.Date;

import com.cy.work.common.enums.UrgencyType;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 需求單查詢頁面顯示vo (search01.xhtml)
 *
 * @author jason_h
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Search01View extends BaseSearchView implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 2957457743303248722L;
    /** 緊急度 */
	private UrgencyType urgency;
	private Boolean hasForwardDep;
	private Boolean hasForwardMember;
	private Boolean hasLink;
	private Date createdDate;
	private Date updatedDate;
	private String bigName;
	private String middleName;
	private String smallName;
	private Integer createDep;
	private Integer createdUser;
	private Boolean checkAttachment;
	private Boolean hasAttachment;
	/** 期望完成日 */
	private Date hopeDate;

	/** 本地端連結網址 */
	private String localUrlLink;
	/** 立 - 自己單位建立的需求單（含底下組別） */
	private Boolean create;
	/** 分 - 分派至自己單位的需求單 (使用者所歸屬的單位) */
	private Boolean assign;
	/** 轉 - 轉寄至自己單位的需求單 (使用者所歸屬的單位) */
	private Boolean forward;
	/** 廳主 */
	private Long customer;
	/** 統整人員 */
	private Integer inteStaffUser;

	private Integer resultIndex;
}
