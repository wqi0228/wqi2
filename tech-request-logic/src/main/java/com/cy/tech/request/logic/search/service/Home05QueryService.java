/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.search.view.Home05View;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.vo.require.AlertInboxSendGroup;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author kasim
 */
@Service
@Slf4j
public class Home05QueryService implements QueryService<Home05View>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2777325503044317489L;
    @Autowired
    private URLService urlService;
    @Autowired
    private SearchService searchHelper;

    @PersistenceContext
    transient private EntityManager em;

    @Override
    public List<Home05View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {

        // ====================================
        // 查詢
        // ====================================
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        
        //資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        @SuppressWarnings("unchecked")
        List<Object[]> result = query.getResultList();
        //資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if(WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }
        
        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();
        List<Home05View> viewResult = Lists.newArrayList();
        for (int i = 0; i < result.size(); i++) {

            Home05View v = new Home05View();
            int index = 0;

            Object[] record = (Object[]) result.get(i);
            String sid = (String) record[index++];
            String requireNo = (String) record[index++];
            String requireTheme = (String) record[index++];
            Integer urgency = (Integer) record[index++];
            Boolean hasForwardDep = (Boolean) record[index++];
            Boolean hasForwardMember = (Boolean) record[index++];
            Boolean hasLink = (Boolean) record[index++];
            String inboxSendGroup = (String) record[index++];
            Integer senderDep = (Integer) record[index++];
            Integer sender = (Integer) record[index++];
            Date createdDate = (Date) record[index++];
            String bigName = (String) record[index++];
            Date requireUpdatedDate = (Date) record[index++];
            String message = (String) record[index++];
            String messageCss = "";
            try {
                byte[] msageBytpe = (byte[]) record[index++];
                messageCss = msageBytpe == null ? "" : new String(msageBytpe, "UTF-8");
            } catch (IOException ex) {
                log.error(ex.getMessage(), ex);
            }
            String hasTrace = (String) record[index++];
            // 處理共通欄位 (一定要擺在『取值』最後面, 否則 index 會不對)
            v.prepareCommonColumn(record, index);

            v.setSid(sid);
            v.setRequireNo(requireNo);
            v.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
            v.setUrgency(UrgencyType.values()[urgency]);
            v.setHasForwardDep(hasForwardDep);
            v.setHasForwardMember(hasForwardMember);
            v.setHasLink(hasLink);
            AlertInboxSendGroup group = new AlertInboxSendGroup();
            group.setSid(inboxSendGroup);
            v.setInboxSendGroup(group);
            v.setSenderDep(senderDep);
            v.setSender(sender);
            v.setCreatedDate(createdDate);
            v.setBigName(bigName);
            v.setRequireUpdatedDate(requireUpdatedDate);
            v.setMessage(message);
            v.setMessageCss(messageCss);
            v.setLocalUrlLink(urlService.createLocalUrlLinkParamForTab(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(execUserSid, v.getRequireNo(), 1),
                    URLService.URLServiceAttr.URL_ATTR_TAB_INBOX_PERSONAL, inboxSendGroup));
            v.setHasTrace(Boolean.valueOf(hasTrace));
            viewResult.add(v);
        }
        // 解析資料-結束
        usageRecord.parserDataEnd();
        return viewResult;
    }
}
