/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.send.test;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.RequireReadRecordHelper;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.repository.worktest.WorkTestInfoHistoryRepo;
import com.cy.tech.request.repository.worktest.WorkTestInfoRepo;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.worktest.WorkTestAlreadyReply;
import com.cy.tech.request.vo.worktest.WorkTestAttachment;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.vo.worktest.WorkTestInfoHistory;
import com.cy.tech.request.vo.worktest.WorkTestQAReport;
import com.cy.tech.request.vo.worktest.WorkTestReply;
import com.cy.tech.request.vo.worktest.enums.WorkTestInfoHistoryBehavior;
import com.cy.tech.request.vo.worktest.enums.WorkTestInfoStatus;
import com.cy.tech.request.vo.worktest.enums.WorkTestRollbackType;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.backend.logic.WorkBackendParamService;
import com.cy.work.backend.vo.enums.WkBackendParam;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.vo.Attachment;
import com.cy.work.common.vo.AttachmentService;
import com.google.common.collect.Ordering;
import com.google.common.primitives.Longs;

/**
 * 送測歷程服務
 *
 * @author shaun
 */
@Component
public class SendTestHistoryService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8412648494339201466L;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

    @Autowired
    private RequireService reqService;
    @Autowired
    private transient WorkBackendParamService workBackendParamService;
    @Autowired
    private transient WorkBackendParamHelper workBackendParamHelper;
    @Autowired
    private SendTestShowService stsService;
    @Autowired
    @Qualifier("send_test_history_attach")
    private AttachmentService<WorkTestAttachment, WorkTestInfoHistory, String> attachService;
    @Autowired
    private transient RequireReadRecordHelper requireReadRecordHelper;
    @Autowired
    private WkJsoupUtils jsoupUtils;
    @Autowired
    private WorkTestInfoRepo testinfoDao;
    @Autowired
    private WorkTestInfoHistoryRepo historyDao;
    @Autowired
    private SendTestService stService;
    @Autowired
    private SendTestAlertService sendTestAlertService;

    @Autowired
    private SendTestStatusManager sendTestStatusManager;

    /**
     * 因頁面render呼叫次數過多, 不宜直接存取資料庫
     * 
     * @param testInfo
     * @return
     */
    @Transactional(readOnly = true)
    public List<WorkTestInfoHistory> findInfoHistorysByLazy(WorkTestInfo testInfo) {
        try {
            testInfo.getInfoHistorys().size();
        } catch (LazyInitializationException e) {
            // log.debug("findInfoHistorys lazy init error :" + e.getMessage(), e);
            testInfo.setInfoHistorys(historyDao.findByTestInfo(testInfo));
        }
        return testInfo.getInfoHistorys();
    }

    @Transactional(readOnly = true)
    public List<WorkTestInfoHistory> findInfoHistorys(WorkTestInfo testInfo) {
        return historyDao.findByTestInfo(testInfo);
    }

    /**
     * prepare to save
     * 
     * @param testInfo
     * @param createUser
     * @return
     */
    public WorkTestInfoHistory createEmptyHistory(WorkTestInfo testInfo, User createUser) {
        WorkTestInfoHistory history = new WorkTestInfoHistory();
        history.setTestInfo(testInfo);
        history.setTestinfoNo(testInfo.getTestinfoNo());
        history.setSourceType(testInfo.getSourceType());
        history.setSourceSid(testInfo.getSourceSid());
        history.setSourceNo(testInfo.getSourceNo());
        history.setBehaviorStatus(testInfo.getTestinfoStatus());
        history.setStatus(Activation.ACTIVE);
        history.setCreatedUser(createUser);
        history.setCreatedDate(new Date());
        history.setUpdatedUser(createUser);
        history.setUpdatedDate(new Date());
        return history;
    }

    @Transactional(rollbackFor = Exception.class)
    public void save(WorkTestInfo testInfo, WorkTestInfoHistoryBehavior behavior, String reason, User loginUser) {
        WorkTestInfoHistory history = createEmptyHistory(testInfo, loginUser);
        history.setBehavior(behavior);
        history.setReason(reason);
        history.setReasonCss(reason);
        historyDao.save(history);
    }

    @Transactional(rollbackFor = Exception.class)
    public void update(WorkTestInfoHistory history, User executor) {
        history.setUpdatedDate(new Date());
        history.setUpdatedUser(executor);
        historyDao.save(history);
        attachService.findAttachsByLazy(history).forEach(each -> ((Attachment<String>) each).setKeyChecked(Boolean.TRUE));
        attachService.linkRelation(history.getAttachments(), history, executor);
        this.sortHistory(history.getTestInfo());
    }

    public void sortHistory(WorkTestInfo testInfo) {
        List<WorkTestInfoHistory> historys = this.findInfoHistorysByLazy(testInfo);
        Ordering<WorkTestInfoHistory> ordering = new Ordering<WorkTestInfoHistory>() {
            @Override
            public int compare(WorkTestInfoHistory left, WorkTestInfoHistory right) {
                return Longs.compare(right.getUpdatedDate().getTime(), left.getUpdatedDate().getTime());
            }
        };
        Collections.sort(historys, ordering);
    }

    private void putHistoryToTestInfo(WorkTestInfo testInfo, WorkTestInfoHistory history) {
        List<WorkTestInfoHistory> infoHistorys = this.findInfoHistorysByLazy(testInfo);
        infoHistorys.add(history);
    }

    @Transactional(rollbackFor = Exception.class)
    public void createReplyHistory(WorkTestReply reply, User executor) {
        WorkTestInfoHistory history = reply.getHistory();
        history.setBehaviorStatus(reply.getTestInfo().getTestinfoStatus());
        history.setReply(reply);
        history.setBehavior(WorkTestInfoHistoryBehavior.REPLY);
        history.setVisiable(this.canVisiable(history));
        WorkTestInfoHistory nH = historyDao.save(history);
        attachService.linkRelation(history.getAttachments(), nH, executor);
        reply.setHistory(nH);
        this.putHistoryToTestInfo(reply.getTestInfo(), nH);
    }

    private Boolean canVisiable(WorkTestInfoHistory history) {
        return !history.getBehaviorStatus().equals(stService.findInfoStatus(history.getTestInfo()));
    }

    @Transactional(rollbackFor = Exception.class)
    public void createAlreadyReplyHistory(WorkTestAlreadyReply aReply, User executor) {
        WorkTestInfoHistory history = aReply.getHistory();
        history.setAlreadyReply(aReply);
        history.setBehavior(WorkTestInfoHistoryBehavior.REPLY_AND_REPLY);
        historyDao.save(history);
        attachService.linkRelation(history.getAttachments(), history, executor);
    }

    /**
     * 建立 預設完成日異動歷程記錄
     *
     * @param editInfo
     * @param backupInfo
     * @param executor
     */
    @Transactional(rollbackFor = Exception.class)
    public void createEsDtChangeHistory(WorkTestInfo editInfo, WorkTestInfo backupInfo, User executor) {
        WorkTestInfoHistory history = this.createEmptyHistory(editInfo, executor);
        history.setBehavior(WorkTestInfoHistoryBehavior.MODIFY_ESTABLISH_DATE);
        history.setVisiable(Boolean.TRUE);
        history.setReason("預計完成日由" + sdf.format(backupInfo.getEstablishDate()) + "改至" + sdf.format(editInfo.getEstablishDate()));
        history.setReasonCss(history.getReason());
        WorkTestInfoHistory nH = historyDao.save(history);
        this.putHistoryToTestInfo(editInfo, nH);
        this.sortHistory(editInfo);
    }

    /**
     * 儲存退測紀錄
     * 
     * @param history
     * @throws IllegalAccessException
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveRollbackHistory(WorkTestInfoHistory history, User loginUser) throws IllegalAccessException {
        // ====================================
        // 查詢最新主檔資訊
        // ====================================
        Require require = reqService.findByReqSid(history.getSourceSid());

        // ====================================
        // 檢核可操作權限
        // ====================================
        if (!stsService.renderedRollbackTest(require, history.getTestInfo(), loginUser) && history.getSid() == null) {
            throw new IllegalAccessException("無操作權限，送測單已被更新！");
        }
        // =====================================
        // update 退測原因 (若歷程記錄已存在時)
        // =====================================
        // only for update reason
        if (history.getSid() != null) {
            // 更新 送測異動紀錄 檔
            WorkTestInfoHistory entity = findOne(history.getSid());
            entity.setReason(jsoupUtils.clearCssTag(history.getReasonCss()));
            entity.setReasonCss(history.getReasonCss());
            historyDao.save(entity);
            // 更新附加檔案
            entity.getAttachments().stream().forEach(attach -> attach.setKeyChecked(true));
            attachService.linkRelation(entity.getAttachments(), entity, loginUser);
            return;
        }

        // =====================================
        // 更新測試主檔
        // =====================================
        WorkTestInfo testInfo = testinfoDao.findOne(history.getTestInfo().getSid());
        // 異動狀態:【退測】
        this.sendTestStatusManager.changeStatusByReject(testInfo);

        stService.save(testInfo, loginUser);

        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.WORKTESTSIGNINFO,
                testInfo.getSid(),
                loginUser.getSid());

        // =====================================
        // add trace record
        // =====================================
        history.setBehavior(WorkTestInfoHistoryBehavior.ROLL_BACK);
        history.setBehaviorStatus(WorkTestStatus.ROLL_BACK_TEST);
        history.setVisiable(Boolean.TRUE);
        history.setReason(jsoupUtils.clearCssTag(history.getReasonCss()));

        // 判斷退測單位類型
        // 取得技術事業群
        List<Integer> techGroupSids = this.workBackendParamService.findIntsByKeyword(WkBackendParam.REQ_TECH_GROUP_DEP_SID);
        // 取得市場事業群
        List<Integer> marketGroupSids = this.workBackendParamService.findIntsByKeyword(WkBackendParam.REQ_MARKET_GROUP_DEP_SID);

        WorkTestRollbackType rollbackType = this.findRollbackTypeBy(
                techGroupSids,
                marketGroupSids,
                WkOrgCache.getInstance().findBySid(
                        WkUserCache.getInstance().findBySid(history.getCreatedUser().getSid()).getPrimaryOrg().getSid()));

        history.setRollBackType(rollbackType);

        WorkTestInfoHistory newWorkTestInfoHistory = historyDao.save(history);

        attachService.linkRelation(history.getAttachments(), newWorkTestInfoHistory, WkUserCache.getInstance().findBySid(history.getCreatedUser().getSid()));
        this.putHistoryToTestInfo(newWorkTestInfoHistory.getTestInfo(), newWorkTestInfoHistory);
        this.sortHistory(history.getTestInfo());

        // =====================================
        // add alert
        // =====================================
        if (WorkTestInfoStatus.JOIN_SCHEDULE.equals(history.getTestInfo().getQaScheduleStatus())) {
            sendTestAlertService.save(stService.createUnReadAlert(testInfo, loginUser, WorkTestInfoHistoryBehavior.ROLL_BACK));
        }
    }

    /**
     * 判斷執行退測人員歸屬單位(群)
     *
     * @param executor
     * @return
     */
    private WorkTestRollbackType findRollbackTypeBy(List<Integer> techGroupSids, List<Integer> marketGroupSids, Org org) {
        if (org == null) {
            return WorkTestRollbackType.UNKNOW;
        }
        if (techGroupSids.contains(org.getSid())) {
            return WorkTestRollbackType.TECH;
        }
        if (marketGroupSids.contains(org.getSid())) {
            return WorkTestRollbackType.MARKET;
        }
        if (org.getParent() == null) {
            return WorkTestRollbackType.UNKNOW;
        }
        return this.findRollbackTypeBy(techGroupSids, marketGroupSids, WkOrgCache.getInstance().findBySid(org.getParent().getSid()));
    }

    /**
     * 建立 重測歷程記錄
     *
     * @param editInfo
     * @param executor
     */
    @Transactional(rollbackFor = Exception.class)
    public void createRetestHistory(WorkTestInfo editInfo, User executor, String reason) {
        WorkTestInfoHistory history = this.createEmptyHistory(editInfo, executor);
        history.setBehavior(WorkTestInfoHistoryBehavior.RETEST);
        history.setVisiable(Boolean.TRUE);
        history.setReason(reason);
        history.setReasonCss(history.getReason());
        WorkTestInfoHistory nH = historyDao.save(history);
        this.putHistoryToTestInfo(editInfo, nH);
        this.sortHistory(editInfo);
    }

    /**
     * 建立 重測歷程記錄
     *
     * @param editInfo
     * @param qaReport
     */
    @Transactional(rollbackFor = Exception.class)
    public void createQAReportHistory(WorkTestInfo editInfo, WorkTestQAReport qaReport) {
        WorkTestInfoHistory history = qaReport.getHistory();
        history.setQaReport(qaReport);
        history.setBehavior(WorkTestInfoHistoryBehavior.QA_TEST_COMPLETE);
        history.setBehaviorStatus(editInfo.getTestinfoStatus());
        history.setVisiable(Boolean.TRUE);
        WorkTestInfoHistory nH = historyDao.save(history);
        qaReport.setHistory(nH);
        attachService.linkRelation(history.getAttachments(), nH, WkUserCache.getInstance().findBySid(history.getCreatedUser().getSid()));
        this.putHistoryToTestInfo(editInfo, nH);
        this.sortHistory(editInfo);
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveCancelTestHistory(WorkTestInfoHistory history, User loginUser) throws IllegalAccessException {
        if (!stsService.renderedCancelTest(history.getTestInfo(), loginUser) && history.getSid() == null) {
            throw new IllegalAccessException("無操作權限，送測單已被更新！");
        }
        // only for update reason
        if (history.getSid() != null) {
            WorkTestInfoHistory entity = findOne(history.getSid());
            entity.setReason(jsoupUtils.clearCssTag(history.getReasonCss()));
            entity.setReasonCss(history.getReasonCss());
            update(entity, loginUser);
            return;
        }

        WorkTestInfo testInfo = stService.findOne(history.getTestInfo().getSid());
        testInfo.setTestinfoStatus(WorkTestStatus.CANCEL_TEST);
        testInfo.setReadReason(WaitReadReasonType.TEST_CANCEL);
        testInfo.setReadUpdateDate(new Date());
        testInfo.setCancelDate(new Date());
        stService.save(testInfo, loginUser);

        history.setBehavior(WorkTestInfoHistoryBehavior.CANCEL_TEST);
        history.setBehaviorStatus(WorkTestStatus.CANCEL_TEST);
        history.setVisiable(Boolean.TRUE);
        history.setReason(jsoupUtils.clearCssTag(history.getReasonCss()));
        WorkTestInfoHistory nH = historyDao.save(history);

        attachService.linkRelation(history.getAttachments(), nH, WkUserCache.getInstance().findBySid(history.getCreatedUser().getSid()));
        this.putHistoryToTestInfo(nH.getTestInfo(), nH);
        this.sortHistory(history.getTestInfo());
        
        
        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.WORKTESTSIGNINFO,
                testInfo.getSid(),
                loginUser.getSid());
        

        // add alert
        if (workBackendParamHelper.isQAUser(loginUser.getSid())) {
            sendTestAlertService.save(
                    stService.createUnReadAlert(
                            testInfo,
                            loginUser,
                            WorkTestInfoHistoryBehavior.CANCEL_TEST));
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveTestCompleteHistory(WorkTestInfoHistory history, User loginUser) throws IllegalAccessException {
        if (!stsService.renderedTestComplate(
                reqService.findByReqSid(history.getSourceSid()),
                stService.findByTestinfoNo(history.getTestinfoNo()),
                WkUserCache.getInstance().findBySid(history.getCreatedUser().getSid()))) {
            throw new IllegalAccessException("無操作權限，送測單已被更新！");
        }

        history.setBehavior(WorkTestInfoHistoryBehavior.TEST_COMPLETE);
        history.setBehaviorStatus(WorkTestStatus.TEST_COMPLETE);
        history.setFinishTestReply(jsoupUtils.clearCssTag(history.getFinishTestReplyCss()));
        history.setVisiable(Boolean.TRUE);
        WorkTestInfoHistory nH = historyDao.save(history);

        WorkTestInfo testInfo = stService.findOne(history.getTestInfo().getSid());
        testInfo.setTestinfoStatus(WorkTestStatus.TEST_COMPLETE);
        testInfo.setReadReason(WaitReadReasonType.TEST_COMPLETE);
        testInfo.setReadUpdateDate(new Date());
        testInfo.setFinishDate(new Date());
        stService.save(testInfo, loginUser);

        attachService.linkRelation(history.getAttachments(), nH, WkUserCache.getInstance().findBySid(history.getCreatedUser().getSid()));
        this.putHistoryToTestInfo(nH.getTestInfo(), nH);
        this.sortHistory(history.getTestInfo());
        
        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.WORKTESTSIGNINFO,
                testInfo.getSid(),
                loginUser.getSid());
    }

    public WorkTestInfoHistory findOne(String sid) {
        return historyDao.findOne(sid);
    }
}
