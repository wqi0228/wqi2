/**
 * 
 */
package com.cy.tech.request.logic.service.log;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.repository.log.LogSyncMmsRepository;
import com.cy.tech.request.vo.log.LogSyncMms;

import lombok.extern.slf4j.Slf4j;

/**
 * MMS維護單同步記錄 Service
 * 
 * @author allen1214_wu
 */
@Service
@Slf4j
public class LogSyncMmsService {

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private LogSyncMmsRepository logSyncMmsRepository;
    
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private JdbcTemplate jdbcTemplate;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 取得最近的 MMS 資料時間
     * 
     * @return date
     */
    public Date queryLatestMmsDate() {
        return this.logSyncMmsRepository.queryLatestMmsDate();
    };

    /**
     * 查詢需要重新執行的單據
     * 
     * @return
     */
    public List<String> queryMmsIdsByWaitDoAgain() {
        return this.logSyncMmsRepository.queryMmsIdsByWaitDoAgain();
    }

    /**
     * Insert or update
     * 
     * @param logSyncMms
     */
    public void save(LogSyncMms logSyncMms) {
        this.logSyncMmsRepository.save(logSyncMms);
    }

    /**
     * 依據 MMSID, 資料時間 判斷是否已成功執行
     * 
     * @param mmsID       mmsID
     * @param mmsDateTime 資料時間
     * @return
     */
    public Boolean isProcessedAndSuccess(
            String mmsID,
            Date mmsDateTime) {
        return this.logSyncMmsRepository.isProcessedAndSuccess(mmsID, mmsDateTime, true, Activation.ACTIVE);
    }

    /**
     * 將執行失敗，且未重做的記錄，更新為已重做
     * @param mmsID
     * @param logSyncMms
     */
    public void updateIsDoAgain(String mmsID, LogSyncMms logSyncMms) {

        // ====================================
        // 兜組 SQL
        // ====================================
        String sql = ""
                + " UPDATE tr_log_sync_mms "
                + "    SET is_sync_again = 1, "
                + "        update_usr = 1, "
                + "        update_dt = NOW() "
                + " WHERE mms_id = '" + mmsID + "' "
                + "   AND is_success = 'N' " //執行失敗
                + "   AND is_sync_again = 0 "; //還沒重新同步

        // 當本次也失敗時，保留最後一筆失敗，使其下次啟動時，再做一次
        if (logSyncMms != null && !logSyncMms.isSuccess()) {
            sql += " AND log_sid != '" + logSyncMms.getSid() + "'; ";
        }

        // ====================================
        // 執行
        // ====================================
        int row = this.jdbcTemplate.update(sql);
        log.debug(" tr_log_sync_mms 更新『執行失敗』記錄，共更新 [{}] 筆!", row);

    }
}
