/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.vo;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 類別節點-頁面顯示用
 *
 * @author kasim
 */
@EqualsAndHashCode(of = {"sid", "name", "cateClz"})
@Data
public class CateNodeTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1166197489685954721L;

    private String sid;

    private String name;

    @SuppressWarnings("rawtypes")
    private Class cateClz;
    
    private String id;

    @SuppressWarnings("rawtypes")
    public CateNodeTo(String sid, String name, Class cateClz, String id) {
        this.sid = sid;
        this.name = name;
        this.cateClz = cateClz;
        this.id = id;
    }
}
