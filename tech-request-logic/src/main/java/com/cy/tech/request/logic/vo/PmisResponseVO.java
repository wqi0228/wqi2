package com.cy.tech.request.logic.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

/**
 * @author aken_kao
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PmisResponseVO {
    
    @Getter
    @Setter
    private boolean ok = false;
    
    @Getter
    @Setter
    private String message = "";
 
    public PmisResponseVO(){
        
    }
    
    public PmisResponseVO(String message){
        this.message = message;
    }
}
