/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import java.io.Serializable;
import java.util.Date;

import com.cy.tech.request.vo.worktest.enums.WorkTestInfoStatus;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 送測排程表頁面顯示vo (search34.xhtml)
 *
 * @author marlow_chen
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Search34View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -45931929347121284L;
    /** 送測日 */
    private Date testDate;
    /** 送測日字串 */
    private String testDateStr;
    /** QA搜尋連結 */
    private String qaSearchLink;
    /** 填單人所歸屬的部門sid */
    private Integer createDeptSid;
    /** 建立者 */
    private Integer createdUser;
    /** 送測主題 */
    private String testTheme;
    /** 主測 */
    private String[] masterTester;
    /** 協測 */
    private String[] slaveTester;
    /** 排定完成日 */
    private Date scheduleFinishDate;
    /** 預計完成日 */
    private Date establishDate;
    /** 送測狀態 */
    private String testinfoStatus;
    /** 文件補齊 */
    private String commitStatus;
    /** 實際完成日 */
    private Date testinfoFinishDate;
    /** 預計上線日 */
    private Date expectOnlineDate;
    /** 送測單號 */
    private String testinfoNo;
    /** 建立日期 */
    private Date createdDate;
    /** 需求單位 */
    private Integer requireSid;
    /** 需求人員 */
    private Integer requireCreateUser;
    /** 需求類別 */
    private String bigCategory;
    /** 中類 */
    private String middleCategory;
    /** 小類 */
    private String smallCategory;
    /** 需求主題 */
    private String requireTheme;
    /** 異動日期 */
    private Date requireUpdateDate;
    /** 本地端連結網址 */
    private String localUrlLink;
    /** 本地端連結網址(for需求單) */
    private String localRequireUrlLink;
    /** QA連結顯示名稱 */
    private String qaLinkName;
    /** 填單人所歸屬的部門 */
    private String createDept;
    /**
     * 主測人員(頁面顯示用
     */
    private String masterTesterNames;
    /**
     * 協測人員(頁面顯示用
     */
    private String slaveTesterNames;
    /**
     * hasColor
     */
    private boolean hasColor;

    private WorkTestInfoStatus qaAuditStatus;
    private WorkTestInfoStatus qaScheduleStatus;

    private boolean renderedTestersLink = false;

}
