/**
 * 
 */
package com.cy.tech.request.logic.service;

import java.io.Serializable;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.helper.systemnotify.SysNotifyHelper;
import com.cy.tech.request.logic.utils.ProcessLog;
import com.cy.tech.request.repository.require.ReqModifyRepo;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireTrace;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.google.common.base.Preconditions;

import lombok.extern.slf4j.Slf4j;

/**
 * 需求單- 結案
 * 
 * @author allen1214_wu
 */
@Slf4j
@Component
public class RequireProcessCloseService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7169363157188262547L;
    // =======================================================================================
    // 服務物件區
    // =======================================================================================
    @Autowired
    private transient RequireService requireService;
    @Autowired
    private transient RequireShowService requireShowService;
    @Autowired
    private transient RequireTraceService requireTraceService;
    @Autowired
    private transient ReqModifyRepo reqModifyRepo;
    @Autowired
    private transient SysNotifyHelper sysNotifyHelper;
    @Autowired
    transient private ReqUnitBpmService reqUnitBpmService;
    @Autowired
    private transient RequireReadRecordHelper requireReadRecordHelper;

    // =======================================================================================
    // 方法區
    // =======================================================================================
    /**
     * 執行需求單結案
     * 
     * @param require      需求單主檔
     * @param executor     執行者
     * @param closeTrace   結案說明(追蹤檔)
     * @param isForceClose 是否為強制結案
     * @throws UserMessageException 單據狀態、權限檢核失敗時拋出
     */
    @Transactional(rollbackFor = Exception.class)
    public void executeByUser(
            Require require,
            User executor,
            RequireTrace closeTrace,
            boolean isForceClose) throws UserMessageException {

        // ====================================
        // 重新查詢 DB 資訊
        // ====================================
        Require requireDB = this.requireService.findByReqSid(require.getSid());

        // ====================================
        // 單據狀態、權限檢核
        // ====================================
        // 一般結案
        if (isForceClose && requireShowService.showCloseBtn(requireDB, executor.getSid())) {
            throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }

        // 強制結案
        if (!isForceClose && requireShowService.showForceCloseBtn(requireDB, executor.getSid())) {
            throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }

        // ====================================
        // 呼叫核心結案程序
        // ====================================
        this.process(require, executor, closeTrace, RequireStatusType.CLOSE);

    }

    /**
     * 執行需求單完成自動結案
     *
     * @param require
     * @param executor
     */
    @Transactional(rollbackFor = Exception.class)
    public void executeByAutoClose(Require require, User executor) {
        // ====================================
        // 建立追蹤檔
        // ====================================
        RequireTrace closeTrace = new RequireTrace();
        closeTrace.setCreatedDate(new Date());
        closeTrace.setCreatedUser(executor);
        closeTrace.setRequire(require);
        closeTrace.setRequireNo(require.getRequireNo());
        closeTrace.setRequireTraceType(RequireTraceType.AUTO_CLOSED);
        closeTrace.setRequireTraceContent("此需求單為自動結案的類型，需求完成後系統進行自動結案。");

        // ====================================
        // 呼叫核心結案程序
        // ====================================
        this.process(require, executor, closeTrace, RequireStatusType.AUTO_CLOSED);
    }

    /**
     * 批次結案
     * @param require
     * @param executor
     * @param isForceCloce
     * @param closeMemo
     * @throws SystemOperationException
     */
    @Transactional(rollbackFor = Exception.class)
    public void executeByBatchClose(
            Require require,
            User executor,
            boolean isForceCloce,
            String closeMemo) throws SystemOperationException {

        // ====================================
        // 相關流程結案 (for 強制結案)
        // ====================================
        if (require.getReqUnitSign() != null
                && !InstanceStatus.CLOSED.equals(require.getReqUnitSign().getInstanceStatus())) {
            try {
                this.reqUnitBpmService.updateSignInfo(require, InstanceStatus.CLOSED);
            } catch (Exception e) {
                String message = "申請單位簽核流程結案失敗![" + e.getMessage() + "]";
                log.error(message, e);
                throw new SystemOperationException("申請單位簽核流程結案失敗![" + e.getMessage() + "]");
            }

        }

        // ====================================
        // 建立追蹤檔
        // ====================================
        RequireTrace closeTrace = requireTraceService.createNewTrace(require.getSid(), executor);
        closeTrace.setRequireTraceType(RequireTraceType.CLOSE_MEMO);

        String traceContent = isForceCloce ? "強制結案原因：\r\n" : "";
        traceContent += closeMemo;
        closeTrace.setRequireTraceContent(traceContent);

        // ====================================
        // 結案
        // ====================================
        this.process(require, executor, closeTrace, RequireStatusType.CLOSE);

    }

    // =======================================================================================
    // 核心流程
    // =======================================================================================
    /**
     * 執行需求單結案 (核心方法，不對外開放)
     * 
     * @param require
     * @param executor
     * @param closeTrace
     * @param closeType
     */
    @Transactional(rollbackFor = Exception.class)
    private void process(
            Require require,
            User executor,
            RequireTrace closeTrace,
            RequireStatusType closeType) {

        // ====================================
        // 檢核
        // ====================================
        Preconditions.checkArgument(closeType != null
                && (RequireStatusType.CLOSE.equals(closeType) || RequireStatusType.AUTO_CLOSED.equals(closeType)),
                "結案型態不符合");

        // ====================================
        // start log
        // ====================================
        String message = "需求單結案";
        if (closeTrace.getRequireTraceType() != null) {
            message += "-" + closeTrace.getRequireTraceType().getLabel();
        }
        ProcessLog processLog = new ProcessLog(require.getRequireNo(), "", message);
        log.info(processLog.prepareStartLog());

        // ====================================
        // 儲存追蹤檔
        // ====================================
        this.requireTraceService.save(closeTrace);

        // ====================================
        // 更新主檔狀態
        // ====================================
        // 強制結案,直接由進行中改為結案狀態時，會少掉完成時間
        if (require.getFinishDate() == null) {
            require.setFinishDate(new Date());
        }

        require.setHasTrace(Boolean.TRUE);
        require.setCloseCode(Boolean.TRUE);
        require.setCloseDate(new Date());
        require.setCloseUser(executor);
        require.setRequireStatus(closeType);
        require.setReadReason(ReqToBeReadType.CLOSED);

        this.reqModifyRepo.updateReqClose(
                require,
                require.getHasTrace(),
                require.getCloseCode(),
                require.getCloseDate(),
                require.getCloseUser(),
                require.getRequireStatus(),
                require.getReadReason(),
                require.getCloseUser(), require.getCloseDate());

        // ====================================
        // 將『已經讀取』過單據的人，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.REQUIRE,
                require.getSid(),
                executor.getSid());

        // ====================================
        // 處理系統通知
        // ====================================
        try {
            this.sysNotifyHelper.processForClose(require.getSid(), executor.getSid());
        } catch (Exception e) {
            log.error("執行系統通知失敗!" + e.getMessage(), e);
        }

        // ====================================
        // complete log
        // ====================================
        log.info(processLog.prepareCompleteLog());
    }

}
