/**
 * 
 */
package com.cy.tech.request.logic.service.setting.onetimetrns;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.logic.service.AssignNoticeService;
import com.cy.tech.request.logic.service.setting.onetimetrns.to.RequireDepRebuildTo;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkConstants;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Slf4j
@Service
public class OneTimeTrnsForRequireDepOwnerService implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 2432416728072643111L;

    // ========================================================================
    // 服務區
    // ========================================================================
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private AssignNoticeService assignNoticeService;
    @Autowired
    private OneTimeTrnsForRequireDepCommonService requireDepCommonService;

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * 查詢未處理
     * 
     * @return
     * @throws UserMessageException
     */
    public Map<String, String> findWaitTransRequireSids(String targetRequireNo) {

        String statusStr = Lists.newArrayList(
                RequireStatusType.PROCESS).stream()
                .map(v -> v.name())
                .collect(Collectors.joining("','", "'", "'"));

        // ====================================
        // 查詢待轉檔
        // ====================================
        StringBuffer varname1 = new StringBuffer();
        varname1.append("SELECT require_sid , require_no ");
        varname1.append("FROM   tr_require  ");
        varname1.append("WHERE  1=1 ");
        varname1.append("  AND  require_status  IN (" + statusStr + ")  ");

        if (WkStringUtils.notEmpty(targetRequireNo)) {
            varname1.append("  AND  require_no = '" + targetRequireNo + "' ");
        }

        varname1.append("ORDER  BY create_dt DESC ");
        //varname1.append("LIMIT  10;");

        List<Map<String, Object>> dataMaps = this.jdbcTemplate.queryForList(varname1.toString());
        if (WkStringUtils.isEmpty(dataMaps)) {
            return Maps.newHashMap();
        }

        log.debug("未轉主檔查詢:[{}]筆", dataMaps.size());

        return dataMaps.stream()
                .collect(Collectors.toMap(
                        dataMap -> dataMap.get("require_sid") + "",
                        dataMap -> dataMap.get("require_no") + ""));

    }

    @Transactional(rollbackFor = Exception.class)
    public void process(
            String requireSid,
            String requireNo,
            Set<Integer> beforeAssignDepSids,
            Set<Integer> beforeNoticeDepSids,
            List<RequireDepRebuildTo> requireDepRebuildTos) throws UserMessageException {

        if (beforeAssignDepSids == null) {
            beforeAssignDepSids = Sets.newHashSet();
        }
        if (beforeNoticeDepSids == null) {
            beforeNoticeDepSids = Sets.newHashSet();
        }
        if (requireDepRebuildTos == null) {
            requireDepRebuildTos = Lists.newArrayList();
        }

        // ====================================
        // 查詢現有的需求完成確認單位檔
        // ====================================
        // 以 dep sid 做索引
        Map<Integer, RequireDepRebuildTo> requireConfirmDepMapByDepSid = requireDepRebuildTos.stream()
                .collect(Collectors.toMap(
                        RequireDepRebuildTo::getDepSid,
                        vo -> vo));

        // 建立以需求確認單位檔的單位為 key 的容器 (分派單位 -> 需求確認單位對應)
        Map<Integer, Set<Integer>> assignDepSidsMapByConfirmDepSid = requireDepRebuildTos.stream()
                .collect(Collectors.toMap(
                        RequireDepRebuildTo::getDepSid,
                        vo -> Sets.newHashSet()));

        // ====================================
        // 整理分派單位 -> 需求確認單位對應
        // ====================================
        // 有問題的需求確認單位
        Set<Integer> warnDepSids = Sets.newHashSet();

        // 將分派單位以『收束到部』的邏輯歸類到需求確認單位檔
        for (Integer assignDepSid : beforeAssignDepSids) {

            // 收束到部
            Org baseDep = WkOrgUtils.prepareBasicDep(assignDepSid, OrgLevel.MINISTERIAL);

            // 試試看自己是否已經為需求單位 (for 已吃新規則的單據)
            if (assignDepSidsMapByConfirmDepSid.containsKey(assignDepSid)) {
                assignDepSidsMapByConfirmDepSid.get(assignDepSid).add(assignDepSid);
            }
            // 『分派單位』歸類到對應（收束）的『需求確認單位』
            else if (assignDepSidsMapByConfirmDepSid.containsKey(baseDep.getSid())) {
                assignDepSidsMapByConfirmDepSid.get(baseDep.getSid()).add(assignDepSid);

            }
            // 分派單位找不到對應的需求確認單位? 應該不可能發生
            else {
                warnDepSids.add(assignDepSid);
                log.warn("意外!!! reqSid:[{}], assignDepSid:[{}]", requireSid, assignDepSid);
            }
        }

        // ====================================
        //
        // ====================================

        Set<Integer> usededOwner = Sets.newHashSet();

        // 新的分派部門清單
        Set<Integer> afterAssignDepSids = Sets.newHashSet();
        if (WkStringUtils.notEmpty(warnDepSids)) {
            afterAssignDepSids.addAll(warnDepSids);
        }

        // 有調整的需求分派單位檔
        List<RequireDepRebuildTo> updateRequireConfirmDeps = Lists.newArrayList();

        for (Entry<Integer, Set<Integer>> entrySet : assignDepSidsMapByConfirmDepSid.entrySet()) {

            // 對應的分派單位 sid
            Set<Integer> assignDepSids = entrySet.getValue();
            // 需求確認單位資料
            RequireDepRebuildTo requireDepRebuildTo = requireConfirmDepMapByDepSid.get(entrySet.getKey());
            // 需求確認單位 sid
            Integer confirmDepSid = requireDepRebuildTo.getDepSid();

            // 沒掛負責人不轉
            if (WkConstants.MANAGER_VIRTAUL_USER_SID.equals(requireDepRebuildTo.getOwnerSid())) {
                afterAssignDepSids.addAll(assignDepSids);
                continue;
            }

            // 取得負責人單位

            // 排除負責人未設定
            // 排除這個負責人已經命中過
            if (!WkConstants.MANAGER_VIRTAUL_USER_SID.equals(requireDepRebuildTo.getOwnerSid())
                    && !usededOwner.contains(requireDepRebuildTo.getOwnerSid())) {
                // 查詢負責人資料
                User owner = WkUserCache.getInstance().findBySid(requireDepRebuildTo.getOwnerSid());

                // 取得負責人部門
                Integer ownerDepSid = null;
                if (owner != null && owner.getPrimaryOrg() != null) {
                    ownerDepSid = owner.getPrimaryOrg().getSid();
                }

                // 判斷負責人 和 需求確認單位不一樣
                if (!WkCommonUtils.compareByStr(ownerDepSid, confirmDepSid)) {
                    // 若負責人的新單位, 不在原本的需求確認單位中
                    if (!assignDepSidsMapByConfirmDepSid.containsKey(ownerDepSid)) {

                        log.info("[{}]:因負責人:[{}]單位異動，單位[{}]->[{}]",
                                requireNo,
                                owner.getSid() + "-" + WkUserUtils.findNameBySid(owner.getSid()),
                                WkOrgUtils.findNameBySid(confirmDepSid),
                                WkOrgUtils.findNameBySid(ownerDepSid));

                        // 加派負責人單位，減派所有原對應分派單位 (assignDepSids)
                        afterAssignDepSids.add(ownerDepSid);

                        // 要轉檔的需求單位
                        requireDepRebuildTo.setDepSid(ownerDepSid);
                        updateRequireConfirmDeps.add(requireDepRebuildTo);

                        // 註記這個 user 已經命中過
                        usededOwner.add(requireDepRebuildTo.getOwnerSid());

                        continue;
                    }
                }
            }

            // 其他狀況分派單位保持不變
            afterAssignDepSids.addAll(assignDepSids);
        }

        // ====================================
        // 異動需求確認單位
        // ====================================
        if (WkStringUtils.notEmpty(updateRequireConfirmDeps)) {
            for (RequireDepRebuildTo requireDepRebuildTo : updateRequireConfirmDeps) {
                this.requireDepCommonService.updateDepSid(requireDepRebuildTo);
            }
        }

        // ====================================
        // 重建
        // ====================================
        this.assignNoticeService.processForRebuild(
                requireSid,
                requireNo,
                Lists.newArrayList(beforeAssignDepSids),
                Lists.newArrayList(beforeAssignDepSids),
                Lists.newArrayList(afterAssignDepSids),
                Lists.newArrayList(beforeNoticeDepSids), // 通知單位不會變
                WkUserCache.getInstance().findBySid(WkConstants.ADMIN_USER_SID));

    }

}
