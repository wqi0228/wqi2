/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.vo;

import com.cy.tech.request.vo.enums.RequireStatusType;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 後台轉寄查詢物件
 *
 * @author shaun
 */
@EqualsAndHashCode(of = "alertInboxSid")
@AllArgsConstructor
@ToString
@Data
public class TransDepBacksageTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7932576263321293612L;
    /** 需求單sid */
    private String requireSid;
    /** 需求單號 */
    private String requireNo;
    /** 需求製作進度 */
    private RequireStatusType reqType;
    /** 需求單建立日期 */
    private Date createDt;
    /** 收件夾接收部門sid */
    private Integer sourceOrgSid;
    /** 收件sid */
    private String alertInboxSid;
    /** 收件群組sid */
    private String alertInboxGroupSid;
    /** 後台異動是否成功處理 */
    private boolean sucess;

}
