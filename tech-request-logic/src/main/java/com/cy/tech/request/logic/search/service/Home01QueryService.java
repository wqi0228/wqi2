/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.tech.request.logic.search.view.Home01View;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

/**
 * 收藏夾
 *
 * @author shaun
 */
@Component("h01Query")
public class Home01QueryService implements QueryService<Home01View>, Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -8559806136892730163L;
    @Autowired
    private URLService urlService;
    @Autowired
    private SearchService searchHelper;

    @PersistenceContext
    transient private EntityManager em;

    @Override
    public List<Home01View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {

        // ====================================
        // 查詢
        // ====================================
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        @SuppressWarnings("unchecked")
        List<Object[]> result = query.getResultList();
        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if(WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();
        List<Home01View> viewResult = Lists.newArrayList();
        for (int i = 0; i < result.size(); i++) {

            Home01View v = new Home01View();
            int index = 0;

            Object[] record = (Object[]) result.get(i);
            String sid = (String) record[index++];
            String requireNo = (String) record[index++];
            String requireTheme = (String) record[index++];
            Date createdDate = (Date) record[index++];
            // 處理共通欄位 (一定要擺在『取值』最後面, 否則 index 會不對)
            v.prepareCommonColumn(record, index);

            v.setSid(sid);
            v.setRequireNo(requireNo);
            v.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
            v.setCreatedDate(createdDate);
            v.setLocalUrlLink(urlService.createLoacalURLLink(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(execUserSid, v.getRequireNo(), 1)));
            viewResult.add(v);
        }
        // 解析資料-結束
        usageRecord.parserDataEnd();
        return viewResult;
    }
}
