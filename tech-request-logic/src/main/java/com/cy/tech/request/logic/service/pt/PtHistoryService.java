/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.pt;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.tech.request.repository.pt.PtHistoryRepo;
import com.cy.tech.request.vo.pt.PtAlreadyReply;
import com.cy.tech.request.vo.pt.PtAttachment;
import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.pt.PtHistory;
import com.cy.tech.request.vo.pt.PtReply;
import com.cy.tech.request.vo.pt.enums.PtHistoryBehavior;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.vo.AttachmentService;
import com.google.common.collect.Ordering;
import com.google.common.primitives.Longs;

/**
 * 原型確認歷程服務
 *
 * @author shaun
 */
@Component
public class PtHistoryService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5647517832361356733L;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

    @Autowired
    @Qualifier("pt_history_attach")
    private AttachmentService<PtAttachment, PtHistory, String> attachService;
    @Autowired
    private PtHistoryRepo historyDao;
    @Autowired
    private PtService stService;

    @Transactional(readOnly = true)
    public List<PtHistory> findHistorys(PtCheck ptCheck) {
        try {
            ptCheck.getHistorys().size();
        } catch (LazyInitializationException e) {
            // log.debug("findHistorys lazy init error :" + e.getMessage(), e);
            ptCheck.setHistorys(historyDao.findByPtCheck(ptCheck));
        }
        return ptCheck.getHistorys();
    }

    public PtHistory createEmptyHistory(PtCheck ptCheck, User createUser) {
        PtHistory history = new PtHistory();
        history.setPtCheck(ptCheck);
        history.setPtNo(ptCheck.getPtNo());
        history.setSourceType(ptCheck.getSourceType());
        history.setSourceSid(ptCheck.getSourceSid());
        history.setSourceNo(ptCheck.getSourceNo());
        history.setBehaviorStatus(ptCheck.getPtStatus());
        history.setStatus(Activation.ACTIVE);
        history.setCreatedUser(createUser);
        history.setCreatedDate(new Date());
        history.setUpdatedUser(createUser);
        history.setUpdatedDate(new Date());
        return history;
    }

    @Transactional(rollbackFor = Exception.class)
    public void update(PtHistory history, User executor) {
        history.setUpdatedDate(new Date());
        history.setUpdatedUser(executor);
        historyDao.save(history);
        List<PtAttachment> ptAttachments = attachService.findAttachsByLazy(history);
        for (PtAttachment ptAttachment : ptAttachments) {
            ptAttachment.setKeyChecked(Boolean.TRUE);
        }
        attachService.linkRelation(history.getAttachments(), history, executor);
        this.sortHistory(history.getPtCheck());
    }

    public void sortHistory(PtCheck ptCheck) {
        List<PtHistory> historys = this.findHistorys(ptCheck);
        Ordering<PtHistory> ordering = new Ordering<PtHistory>() {
            @Override
            public int compare(PtHistory left, PtHistory right) {
                return Longs.compare(right.getUpdatedDate().getTime(), left.getUpdatedDate().getTime());
            }
        };
        Collections.sort(historys, ordering);
    }

    private void putHistoryToPt(PtCheck ptCheck, PtHistory history) {
        List<PtHistory> infoHistorys = this.findHistorys(ptCheck);
        infoHistorys.add(history);
    }

    @Transactional(rollbackFor = Exception.class)
    public void createReplyHistory(PtReply reply, User executor) {
        PtHistory history = reply.getHistory();
        history.setBehaviorStatus(reply.getPtCheck().getPtStatus());
        history.setReply(reply);
        history.setBehavior(PtHistoryBehavior.REPLY);
        history.setVisiable(this.canVisiable(history));
        PtHistory nH = historyDao.save(history);
        attachService.linkRelation(history.getAttachments(), nH, executor);
        reply.setHistory(nH);
        this.putHistoryToPt(reply.getPtCheck(), nH);
    }

    private Boolean canVisiable(PtHistory history) {
        return !history.getBehaviorStatus().equals(stService.findPtStatus(history.getPtCheck()));
    }

    @Transactional(rollbackFor = Exception.class)
    public void createAlreadyReplyHistory(PtAlreadyReply aReply, User executor) {
        PtHistory history = aReply.getHistory();
        history.setAlreadyReply(aReply);
        history.setBehavior(PtHistoryBehavior.REPLY_AND_REPLY);
        PtHistory nH = historyDao.save(history);
        attachService.linkRelation(history.getAttachments(), nH, executor);
        aReply.setHistory(nH);
    }

    /**
     * 建立 預設完成日異動歷程記錄
     *
     * @param editInfo
     * @param backupInfo
     * @param executor
     */
    @Transactional(rollbackFor = Exception.class)
    public void createEsDtChangeHistory(PtCheck editInfo, PtCheck backupInfo, User executor) {
        PtHistory history = this.createEmptyHistory(editInfo, executor);
        history.setBehavior(PtHistoryBehavior.MODIFY_ESTABLISH_DATE);
        history.setVisiable(Boolean.TRUE);
        history.setReason("預計完成日由" + sdf.format(backupInfo.getEstablishDate()) + "改至" + sdf.format(editInfo.getEstablishDate()));
        history.setReasonCss(history.getReason());
        PtHistory nH = historyDao.save(history);
        this.putHistoryToPt(editInfo, nH);
        this.sortHistory(editInfo);
    }

    /**
     * 建立強制完成歷程
     * 
     * @param history
     * @param executor
     */
    public void saveForceCloceHistory(PtCheck ptCheck, User executor) {
        PtHistory history = this.createEmptyHistory(ptCheck, executor);
        history.setBehavior(PtHistoryBehavior.FORCE_CLOSE);
        history.setVisiable(Boolean.TRUE);
        history.setReason("因強制完成而關閉");
        history.setReasonCss("因強制完成而關閉");
        PtHistory nH = historyDao.save(history);
        this.putHistoryToPt(history.getPtCheck(), nH);
        this.sortHistory(history.getPtCheck());
    }

    /**
     * 建立 重作歷程記錄
     *
     * @param editInfo
     * @param executor
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveRedoHistory(PtHistory history, User executor) {
        history.setBehavior(PtHistoryBehavior.REDO);
        history.setVisiable(Boolean.TRUE);
        history.setReason("執行重作");
        history.setReasonCss(history.getReason());
        PtHistory nH = historyDao.save(history);
        attachService.linkRelation(history.getAttachments(), nH, WkUserCache.getInstance().findBySid(history.getCreatedUser().getSid()));
        this.putHistoryToPt(history.getPtCheck(), nH);
        this.sortHistory(history.getPtCheck());
    }

}
