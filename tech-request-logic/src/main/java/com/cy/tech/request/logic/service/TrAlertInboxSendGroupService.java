/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.repository.require.TrAlertInboxSendGroupRepository;
import com.cy.tech.request.vo.enums.ForwardType;
import com.cy.tech.request.vo.require.TrAlertInboxSendGroup;
import com.google.common.collect.Lists;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author brain0925_liao
 */
@Component
public class TrAlertInboxSendGroupService {

    @Autowired
    private TrAlertInboxSendGroupRepository trAlertInboxSendGroupRepository;

    public TrAlertInboxSendGroup getTrAlertInboxSendGroupBySid(String alert_group_sid) {
        return trAlertInboxSendGroupRepository.findOne(alert_group_sid);
    }

    public List<TrAlertInboxSendGroup> getInboxSendGroupByRequireSidAndLoginUserSid(String requireSid, Integer loginUserSid) {
        List<TrAlertInboxSendGroup> wRInboxSendGroups = trAlertInboxSendGroupRepository.findByUserSidAndRequireSid(requireSid, loginUserSid);
        if (wRInboxSendGroups == null) {
            return Lists.newArrayList();
        }
        return wRInboxSendGroups;
    }

    /**
     * 以下列條件查詢
     * 
     * @param requireNo   需求單號
     * @param forwardType 轉寄類別
     * @param createdUser 建立者
     * @return
     */
    public List<TrAlertInboxSendGroup> findByRequireNoAndForwardTypeAndcreatedUser(
            String requireNo,
            ForwardType forwardType,
            Integer createdUser) {

        return this.trAlertInboxSendGroupRepository.findByRequireNoAndForwardTypeAndCreatedUserAndStatus(
                requireNo,
                forwardType,
                createdUser,
                Activation.ACTIVE);

    }

    public TrAlertInboxSendGroup createInboxSendGroup(TrAlertInboxSendGroup wRInboxSendGroup, Integer loginUserSid) {
        wRInboxSendGroup.setStatus(Activation.ACTIVE);
        wRInboxSendGroup.setCreatedUser(loginUserSid);
        wRInboxSendGroup.setCreatedDate(new Date());
        wRInboxSendGroup.setUpdatedDate(new Date());
        return trAlertInboxSendGroupRepository.save(wRInboxSendGroup);
    }

    public TrAlertInboxSendGroup updateInboxSendGroup(TrAlertInboxSendGroup wRInboxSendGroup, Integer loginUserSid) {
        wRInboxSendGroup.setUpdatedUser(loginUserSid);
        wRInboxSendGroup.setUpdatedDate(new Date());
        return trAlertInboxSendGroupRepository.save(wRInboxSendGroup);
    }

    /**
     * insert or update
     * @param trAlertInboxSendGroup
     */
    public void save(TrAlertInboxSendGroup trAlertInboxSendGroup) {
        this.trAlertInboxSendGroupRepository.save(trAlertInboxSendGroup);
    }
    
    /**
     * @param trAlertInboxSendGroup
     */
    public void delete(TrAlertInboxSendGroup trAlertInboxSendGroup) {
        this.trAlertInboxSendGroupRepository.delete(trAlertInboxSendGroup);
    }
}
