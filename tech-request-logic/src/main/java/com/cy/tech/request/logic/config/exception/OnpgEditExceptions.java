/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.config.exception;

/**
 * 編輯異常拋出
 *
 * @author kasim
 */
public class OnpgEditExceptions extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 4271374800473602222L;

    public OnpgEditExceptions(String message) {
        super(message);
    }

    public OnpgEditExceptions(String message, Throwable cause) {
        super(message, cause);
    }
}
