package com.cy.tech.request.logic.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.bpm.rest.client.BpmOrganizationClient;
import com.cy.bpm.rest.client.ProcessClient;
import com.cy.bpm.rest.client.TaskClient;
import com.cy.bpm.rest.to.RoleTo;
import com.cy.bpm.rest.vo.ProcessTask;
import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.bpm.rest.vo.client.ISign;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.vo.BpmCreateTo;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BpmSignTestService {

    @Autowired
    transient private ProcessClient bpmProcessClient;
    @Autowired
    transient private BpmOrganizationClient bpmOrganizationClient;
    @Autowired
    transient private TaskClient bpmTaskClient;

    public String process() {

        // 簽核模擬人員
        List<String> signSimulationUserIds = Lists.newArrayList(
                "allen1214_wu", // 51
                "yi-fan", // 41,31
                "linus_tseng", // 21
                "aon_lin", // 11
                "jk" // 1
        );

        List<ProcessTaskBase> tasks = Lists.newArrayList();

        // ====================================
        // 建立流程
        // ====================================
        String instanceId = "";
        try {
            instanceId = this.createFlowByNormal(signSimulationUserIds.get(0));
            log.info("建立測試流程成功！instanceId：[{}]", instanceId);
        } catch (Exception e) {
            String message = "建立測試流程失敗!" + e.getMessage();
            log.error(message, e);
            return message;
        }

        // ====================================
        // 層簽
        // ====================================
        for (int i = 0; i < signSimulationUserIds.size(); i++) {
            // ========================
            // 節點簽核人員
            // ========================
            String currSignUserID = signSimulationUserIds.get(i);

            // ========================
            // 查詢可簽人員
            // ========================
            List<String> canSignUserIDs = Lists.newArrayList();
            try {
                canSignUserIDs = bpmTaskClient.findTaskCanSignUserIds(instanceId);

                log.info("instanceId：[" + instanceId + "] 回傳可簽人員:" + new com.google.gson.Gson().toJson(canSignUserIDs));

            } catch (ProcessRestException e) {
                String message = "查詢可簽核人員時失敗! instanceId：[" + instanceId + "]," + e.getMessage();
                log.error(message, e);
                return message;
            }

            // 檢查可簽人員, 是否為預計模擬簽核人員
            if (!canSignUserIDs.contains(currSignUserID)) {
                String message = ""
                        + "模擬失敗! instanceId：[" + instanceId + "]\r\n"
                        + "模擬資料:[" + String.join("->", signSimulationUserIds) + "]\r\n"
                        + "預計簽核人員:[" + currSignUserID + "]\r\n"
                        + "回傳可簽人員:" + new com.google.gson.Gson().toJson(canSignUserIDs) + "\r\n";

                log.error(message);
                return message;
            }

            // ========================
            // 簽名
            // ========================
            BpmSignTo signTo = new BpmSignTo(currSignUserID, instanceId);
            try {
                this.bpmProcessClient.sign(signTo);
                log.info("簽核成功！instanceId：[{}],signUserID:[{}] ", instanceId, currSignUserID);
            } catch (ProcessRestException e) {
                String message = "進行簽名時失敗! instanceId：[" + instanceId + "] signUserID:[" + currSignUserID + "]," + e.getMessage();
                log.error(message, e);
                return message;
            }

            // ========================
            // 測試查詢模擬圖
            // ========================
            try {
                tasks = bpmTaskClient.findSimulationChart(
                        instanceId,
                        "admin");
                //log.info("查詢水管圖成功！instanceId：[{}] \r\n {}", instanceId, new GsonBuilder().setPrettyPrinting().create().toJson(tasks));
                log.info("查詢水管圖成功！instanceId：[{}]", instanceId);
            } catch (ProcessRestException e) {
                String message = "查詢水管圖時失敗! instanceId：[" + instanceId + "] signUserID:[" + currSignUserID + "]," + e.getMessage();
                log.error(message, e);
                return message;
            }
        }

        // ====================================
        // 組簽核歷程
        // ====================================
        List<String> signFlow = Lists.newArrayList();
        for (ProcessTaskBase processTaskBase : tasks) {
            if (processTaskBase instanceof ProcessTaskHistory) {
                ProcessTaskHistory currTask = (ProcessTaskHistory) processTaskBase;
                signFlow.add("(簽)" + currTask.getExecutorRoleName() + ":" + currTask.getExecutorName());
            }

            if (processTaskBase instanceof ProcessTask) {
                ProcessTask currTask = (ProcessTask) processTaskBase;
                signFlow.add("(待簽)" + currTask.getRoleName() + ":" + currTask.getUserName());
            }
        }

        
        String message = "模擬完成:\r\n" + String.join(" -> ", signFlow);
        log.info(message);
        return message;
    }

    public String createFlowByNormal(String userId) throws Exception {

        // ====================================
        // 查詢使用者資料
        // ====================================
        User user = WkUserCache.getInstance().findById(userId);
        if (user == null) {
            throw new Exception("查無使用者:[" + userId + "]");
        }
        if (user.getPrimaryOrg() == null) {
            throw new Exception("使用者無公司別資料:[" + userId + "]");
        }

        // ====================================
        // 公司
        // ====================================
        Org comp = WkOrgCache.getInstance().findBySid(user.getPrimaryOrg().getCompanySid());

        // ====================================
        // BPM開單角色，需從BPM Rest Service 取得
        // ====================================
        // 查詢起始使用者角色
        String startUserRoleId = this.bpmOrganizationClient.findUserRoleByCompany(
                userId,
                comp.getId());

        if (WkStringUtils.isEmpty(startUserRoleId)) {
            throw new Exception("使用者在公司:[" + comp.getId() + "], 無任何角色資料:[" + userId + "]");
        }

        // 角色資料明細
        RoleTo roleTo = this.bpmOrganizationClient.findRoleToById(startUserRoleId);

        // ====================================
        // 判斷最高簽核層級
        // ====================================
        Integer topRoleLevel = roleTo.getRoleLevel();
        RoleTo currRoleTo = roleTo;
        while (true) {
            // 判斷已無上層角色 - 跳出
            if (WkStringUtils.isEmpty(currRoleTo.getParentRoleId())) {
                break;
            }

            // 查詢角色資料明細
            currRoleTo = this.bpmOrganizationClient.findRoleToById(currRoleTo.getParentRoleId());
            // 記錄角色層級
            topRoleLevel = currRoleTo.getRoleLevel();
        }

        String bpmSignLevel = "預設路線-最高至層級41";
        // String bpmSignLevel = "SIGN_LV_41";
        if (topRoleLevel < 11) {
            bpmSignLevel = "預設路線-最高至層級1";
            // bpmSignLevel = "SIGN_LV_01";
        } else if (topRoleLevel < 21) {
            bpmSignLevel = "預設路線-最高至層級11";
            // bpmSignLevel = "SIGN_LV_11";
        } else if (topRoleLevel < 31) {
            bpmSignLevel = "預設路線-最高至層級31";
            // bpmSignLevel = "SIGN_LV_21";
        } else if (topRoleLevel < 41) {
            bpmSignLevel = "預設路線-最高至層級31";
            // bpmSignLevel = "SIGN_LV_31";
        }

        // ====================================
        // 建立流程
        // ====================================
        Map<String, Object> parameter = new HashMap<>();

        if (!Strings.isNullOrEmpty(bpmSignLevel)) {
            parameter.put("sign_lv", bpmSignLevel);
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss_sss");

        BpmCreateTo to = new BpmCreateTo(
                "ST" + sdf.format(new Date()),
                userId,
                startUserRoleId,
                // "bpm_monitor", //
                "bpm_sign_test",
                "BPM簽核測試",
                parameter);

        return bpmProcessClient.create(to);
    }

    public class BpmSignTo implements ISign {

        /**
         * 
         */
        private static final long serialVersionUID = -3149724614288678174L;
        @Getter
        private final String instanceId;
        @Getter
        private final String executorId;
        @Getter
        private final String comment;
        @Getter
        private final Map<String, Object> parameter;

        public BpmSignTo(String userId, String bpmId) {
            this.executorId = userId;
            this.instanceId = bpmId;
            this.comment = "";
            this.parameter = new HashMap<>();
        }
    }

}
