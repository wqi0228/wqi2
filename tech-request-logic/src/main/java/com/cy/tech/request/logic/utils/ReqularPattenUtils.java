package com.cy.tech.request.logic.utils;

import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 檢查 | 覆蓋字串用的正則表達式
 *
 * @author shaun
 */
@Component
public class ReqularPattenUtils implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5287731777289910583L;
    @Autowired
    private WkStringUtils stringUtils;
//    /**
//     * 覆蓋SQL Query Like的非法字元
//     *
//     * @param str
//     * @return
//     */
//    public String replaceIllegalSqlLikeStr(String str) {
//        return str.replaceAll(ReqularPattenUtils.REPLACE_FUZZY_PATTEN, "'$1$2'");
//    }
    private final static String REPLACE_FUZZY_PATTEN = "(%)+|(_)";
    private final static String HELF_EMPTY = " ";
    private final static String FULL_EMPTY = "　";
    private final static String PERCENT = "%";
    private final static String BACK_SLASH = "\\\\";
    private final static String REPLACE_CHAR = "_";

    /**
     * 覆蓋SQL Query Like的非法字元
     *
     * @param str
     * @return
     */
    public String replaceIllegalSqlLikeStr(String str) {
        str = WkStringUtils.safeTrim(str);
        
        if (Strings.isNullOrEmpty(str)) {
            return "";
        }
        if (str.length() == 1 || this.isAllSameSpChar(str)) {
            return this.replaceSpStr(str);
        }
        String replaceHelfEmpty = str.replaceAll(HELF_EMPTY, REPLACE_CHAR);
        String replaceFullEmpty = replaceHelfEmpty.replaceAll(FULL_EMPTY, REPLACE_CHAR);
        String replacePercent = replaceFullEmpty.replaceAll(PERCENT, REPLACE_CHAR);
        String replaceBackslash = replacePercent.replaceAll(BACK_SLASH, PERCENT);
        return replaceBackslash;
    }

    private String replaceSpStr(String str) {
        if (str.contains("\\")) {
            return str.replaceAll("\\\\", "\\\\\\\\");
        }
        return str.replaceAll(ReqularPattenUtils.REPLACE_FUZZY_PATTEN, "\\\\$1$2");
    }

    /**
     * 檢測是否全部為相同字元
     *
     * @param str
     * @return
     */
    private boolean isAllSameSpChar(String str) {
        if (!str.contains("%") && !str.contains("_") && !str.contains("\\")) {
            return false;
        }
        char[] charArray = str.toCharArray();
        char prevC = 0;
        for (char c : charArray) {
            if (prevC == 0) {
                prevC = c;
                continue;
            }
            if (prevC != c) {
                return false;
            }
            prevC = c;
        }
        return true;
    }

    /**
     * 從模糊字串取得單號(如回傳空字串則表示不符合規則)
     *
     * @param compId
     * @param fuzzyText
     * @return
     */
    public String getRequireNo(String compId, String fuzzyText) {
        //去空白
        fuzzyText = WkStringUtils.safeTrim(fuzzyText);
        if (!Strings.isNullOrEmpty(fuzzyText) && this.checkRequireNo(compId, fuzzyText)) {
            return fuzzyText;
        }
        return "";
    }

    /**
     * 判斷模糊字串是否為案件單號
     *
     * @param compId
     * @param fuzzyText
     * @return
     */
    private Boolean checkRequireNo(String compId, String fuzzyText) {
        if (!fuzzyText.startsWith(compId + "TR")) {
            return Boolean.FALSE;
        }
        if (fuzzyText.length() != 15) {
            return Boolean.FALSE;
        }
        if (!stringUtils.isNumeric(fuzzyText.substring(4, 15))) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

}
