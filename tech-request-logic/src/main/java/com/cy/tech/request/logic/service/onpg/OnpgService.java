/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.onpg;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.anew.manager.TrIssueMappTransManager;
import com.cy.tech.request.logic.config.exception.OnpgEditExceptions;
import com.cy.tech.request.logic.helper.systemnotify.SysNotifyHelper;
import com.cy.tech.request.logic.service.AssignNoticeService;
import com.cy.tech.request.logic.service.FormNumberService;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.RequireProcessCompleteRollbackService;
import com.cy.tech.request.logic.service.RequireReadRecordHelper;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.RequireShowService;
import com.cy.tech.request.logic.service.TrSubNoticeInfoService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepService;
import com.cy.tech.request.logic.service.syncmms.helper.SyncMmsParamHelper;
import com.cy.tech.request.logic.service.syncmms.to.SyncFormTo;
import com.cy.tech.request.logic.vo.WorkOnpgTo;
import com.cy.tech.request.repository.TrNamedQueryRepository;
import com.cy.tech.request.repository.onpg.WorkOnpgAttachmentRepo;
import com.cy.tech.request.repository.onpg.WorkOnpgCheckRecordReplyRepo;
import com.cy.tech.request.repository.onpg.WorkOnpgHistoryRepo;
import com.cy.tech.request.repository.onpg.WorkOnpgRepo;
import com.cy.tech.request.repository.result.TransRequireVO;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.RequireTransProgramType;
import com.cy.tech.request.vo.enums.SubNoticeType;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.onpg.WorkOnpgAttachment;
import com.cy.tech.request.vo.onpg.WorkOnpgCheckRecordReply;
import com.cy.tech.request.vo.onpg.WorkOnpgHistory;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgHistoryBehavior;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.backend.logic.WorkBackendParamService;
import com.cy.work.backend.vo.enums.WkBackendParam;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.common.vo.AttachmentService;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * @author shaun
 */
@Slf4j
@Component
public class OnpgService implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4742416626749486128L;
    private static OnpgService instance;

    public static OnpgService getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        OnpgService.instance = this;
    }

    @Autowired
    private transient WorkBackendParamService workBackendParamService;
    @Autowired
    private transient RequireShowService reqShowService;
    @Autowired
    private transient RequireService requireService;
    @Autowired
    private transient OrganizationService orgService;
    @Autowired
    private transient OnpgShowService opsService;
    @Autowired
    private transient OnpgHistoryService ophService;
    @Autowired
    @Qualifier("onpg_attach")
    private AttachmentService<WorkOnpgAttachment, WorkOnpg, String> attachService;
    @Autowired
    private transient URLService urlService;
    @Autowired
    private transient FormNumberService formNumberService;
    @Autowired
    private transient RequireReadRecordHelper requireReadRecordHelper;
    @Autowired
    private transient WkJsoupUtils jsoupUtils;
    @Autowired
    private transient WorkOnpgRepo onpgDao;
    @Autowired
    private transient WorkOnpgHistoryRepo opHistoryDao;
    @Autowired
    private transient WorkOnpgCheckRecordReplyRepo ckReplyDao;
    @Autowired
    private transient WorkOnpgAttachmentRepo attachDao;
    @Autowired
    private transient TrSubNoticeInfoService subNoticeInfoService;
    @Autowired
    private transient TrNamedQueryRepository trNamedQueryRepository;
    @Autowired
    private transient OnpgHistoryService onpgHistoryService;
    @Autowired
    private transient RequireProcessCompleteRollbackService requireProcessCompleteRollbackService;
    @Autowired
    private transient RequireConfirmDepService requireConfirmDepService;
    @Autowired
    transient private TrIssueMappTransManager trIssueMappTransManager;
    @Autowired
    transient private AssignNoticeService assignNoticeService;
    @Autowired
    transient private SysNotifyHelper sysNotifyHelper;

    /**
     * 建立空主檔
     * 
     * @param requireSid
     * @param requireNo
     * @param executor
     * @param execDate
     * @return
     */
    public WorkOnpg createEmptyOnpg(
            String requireSid,
            String requireNo,
            User executor,
            Date execDate) {
        WorkOnpg onpg = new WorkOnpg();

        Require require = this.requireService.findByReqSid(requireSid);

        onpg.setSourceType(WorkSourceType.TECH_REQUEST);
        onpg.setSourceSid(requireSid);
        onpg.setSourceNo(requireNo);
        onpg.setTheme(workBackendParamService.findTextByKeyword(WkBackendParam.OP_DF_THEME));
        onpg.setContent(workBackendParamService.findTextByKeyword(WkBackendParam.OP_DF_CONTENT));
        onpg.setContentCss(onpg.getContent());
        onpg.setCreateCompany(orgService.findUserCompany(orgService.findBySid(executor.getPrimaryOrg().getSid())));
        onpg.setCreateDep(orgService.findBySid(executor.getPrimaryOrg().getSid()));
        onpg.setStatus(Activation.ACTIVE);
        onpg.setCreatedUser(executor);
        onpg.setCreatedDate(new Date());

        // ====================================
        // 預設的通知單位
        // ====================================
        Set<String> noticeDepSids = Sets.newHashSet();

        // 加入 ON程式 - 預設通知單位 List
        List<Integer> opDepSids = workBackendParamService.findIntsByKeyword(WkBackendParam.OP_DF_NOTICE_DEPS);
        if (WkStringUtils.notEmpty(opDepSids)) {
            noticeDepSids.addAll(opDepSids.stream().map(each -> each + "").collect(Collectors.toList()));
        }

        // 加入需求單位
        noticeDepSids.add(require.getCreateDep().getSid() + "");

        // 加入需求單位 & 上層組織的sid (排除群)
        noticeDepSids.add(require.getCreateDep().getSid() + "");

        List<Org> createDepParents = WkOrgCache.getInstance().findAllParent(
                require.getCreateDep().getSid(),
                OrgLevel.DIVISION_LEVEL);

        if (WkStringUtils.notEmpty(createDepParents)) {
            for (Org createDepParent : createDepParents) {
                noticeDepSids.add(createDepParent.getSid() + "");
            }
        }

        onpg.getNoticeDeps().setValue(Lists.newArrayList(noticeDepSids));

        return onpg;
    }

    /**
     * 檢查新檔時必要輸入資訊
     *
     * @param onpg
     */
    public void checkInputInfo(WorkOnpg onpg) {
        Preconditions.checkArgument(onpg.getEstablishDate() != null, "預計完成日不可為空白，請重新輸入！！");
        Preconditions.checkArgument(!Strings.isNullOrEmpty(onpg.getTheme()), "主題不可為空白，請重新輸入！！");
        Preconditions.checkArgument(!onpg.getNoticeDeps().getValue().isEmpty(), "請選擇通知單位！！");
        Preconditions.checkArgument(!Strings.isNullOrEmpty(jsoupUtils.clearCssTag(onpg.getContentCss())), " 內容不可為空白，請重新輸入！！");
    }

    @Transactional(rollbackFor = Exception.class)
    public WorkOnpg saveByNewOnpg(
            Require require,
            WorkOnpg onpg,
            Integer ownerDepSid,
            User executor,
            String issueCreateOnpgKeySid,
            boolean isFromMMS) throws UserMessageException {
        return this.saveByNewOnpgInNoTransactional(require, onpg, ownerDepSid, executor, issueCreateOnpgKeySid, isFromMMS);
    }

    /**
     * 新增一筆 onpg
     * 
     * @param require               需求單主檔
     * @param onpg                  ONPG 主檔
     * @param ownerDepSid           歸屬單位
     * @param executor              執行者
     * @param issueCreateOnpgKeySid
     * @return
     * @throws UserMessageException
     */
    @Transactional(rollbackFor = Exception.class)
    public WorkOnpg saveByNewOnpgInNoTransactional(
            Require require,
            WorkOnpg onpg,
            Integer ownerDepSid,
            User executor,
            String issueCreateOnpgKeySid,
            boolean isFromMMS) throws UserMessageException {

        Date sysDate = new Date();
        require = requireService.findByReqSid(require.getSid());

        String actionDescr = onpg.isFromMms() ? "MMS維護單同步ON程式單" : "開立ON程式單";

        // ====================================
        // 權限檢核
        // ====================================
        // 1.onpg 使用權限
        // 2.為案件單轉onpg

        if (!isFromMMS) {
            if (WkStringUtils.isEmpty(issueCreateOnpgKeySid)) {
                // 是否為分派單位成員
                boolean isContainAssigned = reqShowService.isContainAssign(require, executor);
                // 檢查是否可新增ON程式單
                if (!reqShowService.showOnpgBtn(require, isContainAssigned)) {
                    throw new UserMessageException("無新增ON程式權限！！", InfomationLevel.WARN);
                }
            }
        }

        // ====================================
        // 更新onpg主檔
        // ====================================
        onpg.setContent(jsoupUtils.htmlToText(onpg.getContentCss()));
        onpg.setNote(jsoupUtils.htmlToText(onpg.getNoteCss()));
        onpg.setOnpgStatus(WorkOnpgStatus.ALREADY_ON);
        onpg.setOnpgNo(formNumberService.generateOpNum()); // 產生單號
        onpg = this.save(onpg, executor, sysDate);
        this.attachService.linkRelation(onpg.getAttachments(), onpg, executor);

        // ====================================
        // 建立讀記錄
        // 1.執行者 -> 已讀
        // 2.填單單位+通知單位人員 -> 待閱讀
        // ====================================
        this.requireReadRecordHelper.saveExcutorReadAndOtherUserWaitRead(
                FormType.WORKONPG,
                onpg.getSid(),
                this.prepareReadRecordUserSids(onpg),
                executor.getSid());

        // ====================================
        // 非分派單位成員開單時, 進行自動分派 (如果需要的話)
        // ====================================
        this.assignNoticeService.processForAutoAddAssignDep(
                require.getSid(),
                require.getRequireNo(),
                ownerDepSid,
                "因" + actionDescr + "而加派",
                executor,
                sysDate);

        // ====================================
        // 建立子程序 異動紀錄
        // ====================================
        subNoticeInfoService.createFirstSubChangeRecord(SubNoticeType.ONPG_INFO_DEP,
                require.getSid(),
                require.getRequireNo(),
                onpg.getSid(),
                onpg.getOnpgNo(),
                onpg.getNoticeDeps(),
                executor.getSid());

        // ====================================
        // 若需求製作進度為已完成, 將進度倒回進行中
        // ====================================
        this.requireProcessCompleteRollbackService.executeProcess(
                require.getSid(),
                executor,
                sysDate,
                actionDescr);

        // ====================================
        // 再次開啟執行單位的【需求完成-確認流程】 (若需要開啟的話)
        // ====================================
        this.requireConfirmDepService.changeProgStatusCompleteByAddNewSubCase(
                require.getRequireNo(),
                require.getSid(),
                executor.getSid(),
                onpg.getCreateDep().getSid(),
                actionDescr,
                sysDate);

        // ====================================
        // 處理系統通知 (新增ON程式單)
        // ====================================
        try {
            this.sysNotifyHelper.processForAddOnpg(
                    require.getSid(),
                    onpg.getTheme(),
                    null, // 新增時，無異動前資料
                    (onpg.getNoticeDeps() != null) ? onpg.getNoticeDeps().getValue() : Lists.newArrayList(),
                    executor.getSid());
        } catch (Exception e) {
            log.error("執行系統通知失敗!" + e.getMessage(), e);
        }

        // ====================================
        // 案件單轉 ON程式單
        // ====================================
        if (WkStringUtils.notEmpty(issueCreateOnpgKeySid)) {
            try {
                trIssueMappTransManager.saveByOnpg(
                        issueCreateOnpgKeySid,
                        require,
                        onpg,
                        executor,
                        executor.getPrimaryOrg().getSid());
            } catch (UserMessageException e) {
                throw e;
            } catch (Exception e) {
                String errorMessage = WkMessage.PROCESS_FAILED + e.getMessage();
                log.error(errorMessage, e);
                throw new UserMessageException(errorMessage);
            }
        }

        // ====================================
        // 更新需求單主檔
        // ====================================
        require = this.requireService.findByReqSid(require.getSid());
        require.setHasOnpg(true);
        this.requireService.save(require);

        return onpg;
    }

    @Transactional(rollbackFor = Exception.class)
    public WorkOnpg save(WorkOnpg onpg) {
        return onpgDao.save(onpg);
    }

    public WorkOnpg save(WorkOnpg onpg, User executor) {
        onpg.setUpdatedDate(new Date());
        onpg.setUpdatedUser(executor);
        return onpgDao.save(onpg);
    }

    public WorkOnpg save(WorkOnpg onpg, User executor, Date sysDate) {
        onpg.setUpdatedDate(sysDate);
        onpg.setUpdatedUser(executor);
        return onpgDao.save(onpg);
    }

    /**
     * 編輯存檔Onpg
     * 
     * @param require    需求單
     * @param editOnpg   變更的Onpg
     * @param executor   異動者
     * @param tempOnpgTo editOnpg 副本
     * @throws UserMessageException
     * @throws OnpgEditExceptions
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveByEditOnpg(
            Require require,
            WorkOnpg editOnpg,
            User executor,
            WorkOnpgTo tempOnpgTo)
            throws UserMessageException {

        // 取得未異動前資料
        WorkOnpg backupOnpg = this.findByOnpgNo(editOnpg.getOnpgNo());
        // ====================================
        // 檢核
        // ====================================
        // 權限檢核
        if (opsService.disableNotifyDep(
                requireService.findByReqObj(require),
                this.findByOnpgNo(editOnpg.getOnpgNo()),
                executor)) {
            throw new UserMessageException("無編輯ON程式權限！！", InfomationLevel.ERROR);
        }

        // 在存檔前進行檢核 (避免開啟到編輯其間，有其他人異動資料)
        this.checkWorkOnpgByBeforeSave(tempOnpgTo, backupOnpg);

        // ====================================
        // 存檔
        // ====================================
        this.saveEdit(require, editOnpg, executor);
    }

    /**
     * 儲存編輯資料
     * 
     * @param require
     * @param editOnpg
     * @param executor
     */
    public WorkOnpg saveEdit(
            Require require,
            WorkOnpg editOnpg,
            User executor) {

        // 取得未異動前資料
        WorkOnpg backupOnpg = this.findByOnpgNo(editOnpg.getOnpgNo());

        // ====================================
        // 更新通知單位 + 建立異動記錄 + 首頁推撥
        // ====================================
        this.modifyNoticeDeps(
                require,
                editOnpg.getOnpgNo(),
                editOnpg.getNoticeDeps().getValue(),
                executor);

        // ====================================
        // 更新讀記錄 (已包含異動通知單位需新增的人員)
        // 1.執行者 -> 已讀
        // 2.填單單位+通知單位人員 -> 待閱讀
        // 3.刪除被移除的通知單位人員
        // ====================================
        this.requireReadRecordHelper.resetUserList(
                FormType.WORKONPG,
                editOnpg.getSid(),
                this.prepareReadRecordUserSids(editOnpg),
                executor.getSid());

        // ====================================
        // update
        // ====================================
        // 內容
        editOnpg.setContent(jsoupUtils.htmlToText(editOnpg.getContentCss()));
        // 備註
        editOnpg.setNote(jsoupUtils.htmlToText(editOnpg.getNoteCss()));

        // 異動預計完成日時，新增異動記錄
        this.checkChangeEstablishDate(editOnpg, backupOnpg, executor);

        // 異動備註時，新增異動記錄
        this.checkChangeNote(editOnpg, backupOnpg, executor);

        // 附檔處理
        List<WorkOnpgAttachment> attachment = attachService.findAttachsByLazy(editOnpg);
        for (WorkOnpgAttachment workOnpgAttachment : attachment) {
            workOnpgAttachment.setKeyChecked(Boolean.TRUE);
        }
        attachService.linkRelation(editOnpg.getAttachments(), editOnpg, executor);

        // update
        return this.modifyByEditOnpg(editOnpg, executor);
    }

    /**
     * 更新通知單位 + 建立異動記錄 + 首頁推撥
     * 
     * @param require
     * @param onpgNo
     * @param afterNoticeDeps
     * @param executor
     */
    @Transactional(rollbackFor = Exception.class)
    public void modifyNoticeDeps(
            Require require,
            String onpgNo,
            List<String> afterNoticeDeps,
            User executor) {

        if (afterNoticeDeps == null) {
            afterNoticeDeps = Lists.newArrayList();
        }

        // ====================================
        // 查詢目前 DB 資料
        // ====================================
        // 異動前資料(拷貝一份, 與 hibernate 脫勾)
        WorkOnpg beforeWorkOnpg = new WorkOnpg();
        BeanUtils.copyProperties(this.findByOnpgNo(onpgNo), beforeWorkOnpg);

        // 未異動時 pass
        if (WkCommonUtils.compare(beforeWorkOnpg.getNoticeDeps().getValue(), afterNoticeDeps)) {
            return;
        }

        // ====================================
        // 更新異動單位 (update noticeDeps)
        // ====================================
        this.onpgDao.updateNoticeDeps(beforeWorkOnpg.getSid(), new JsonStringListTo(afterNoticeDeps));

        // ====================================
        // 建立通知單位 異動紀錄
        // ====================================
        this.subNoticeInfoService.createSubChangeRecord(
                SubNoticeType.ONPG_INFO_DEP,
                require.getSid(),
                require.getRequireNo(),
                beforeWorkOnpg.getSid(),
                beforeWorkOnpg.getOnpgNo(),
                new JsonStringListTo(beforeWorkOnpg.getNoticeDeps().getValue()),
                new JsonStringListTo(afterNoticeDeps),
                executor.getSid());

        // ====================================
        // 處理系統通知 (新增ON程式單)
        // ====================================
        try {
            this.sysNotifyHelper.processForAddOnpg(
                    require.getSid(),
                    beforeWorkOnpg.getTheme(),
                    beforeWorkOnpg.getNoticeDeps().getValue(),
                    afterNoticeDeps,
                    executor.getSid());
        } catch (Exception e) {
            log.error("執行系統通知失敗!" + e.getMessage(), e);
        }
    }

    /**
     * 重新編輯後，需檢查是否有異動預計完成日
     *
     * @param editOnpg
     * @param backupOnpg
     * @param executor
     */
    private void checkChangeEstablishDate(WorkOnpg editOnpg, WorkOnpg backupOnpg, User executor) {
        if (editOnpg.getEstablishDate().compareTo(backupOnpg.getEstablishDate()) != 0) {
            ophService.createEsDtChangeHistory(editOnpg, backupOnpg, executor);
            editOnpg.setReadReason(WaitReadReasonType.ONPG_UPDATE_ESTABLISHDATE);
            editOnpg.setReadUpdateDate(new Date());
        }
    }

    /**
     * 重新編輯後，需檢查是否有異動預計完成日
     *
     * @param editOnpg
     * @param backupOnpg
     * @param executor
     */
    private void checkChangeNote(WorkOnpg editOnpg, WorkOnpg backupOnpg, User executor) {
        if (!editOnpg.getNote().equals(backupOnpg.getNote())) {
            ophService.createOnpgNoteChangeHistory(editOnpg, executor);
            editOnpg.setReadReason(WaitReadReasonType.ONPG_UPDATE_NOTE);
            editOnpg.setReadUpdateDate(new Date());
        }
    }

    /**
     * 準備閱讀記錄 (填單單位+通知單位 人員)
     *
     * @param onpg
     * @return
     */
    private Set<Integer> prepareReadRecordUserSids(WorkOnpg onpg) {

        // ====================================
        // 要被通知的單位
        // ====================================
        Set<Integer> allDepSids = Sets.newHashSet();

        // 建單單位
        if (onpg.getCreateDep() != null) {
            allDepSids.add(onpg.getCreateDep().getSid());
        }

        // on程式單通知單位
        Set<Integer> noticeDepSids = onpg.getNoticeDepSids();
        if (WkStringUtils.notEmpty(noticeDepSids)) {
            allDepSids.addAll(noticeDepSids);
        }

        // ====================================
        // 取得部門下所有 user
        // ====================================
        Set<Integer> allUserSids = WkUserCache.getInstance().findUserSidByOrgsWithManager(
                allDepSids, Activation.ACTIVE);

        // ====================================
        // 為組時，把部長拉進來 (組織扁平化調整 原最高為處, 降為部)
        // ====================================
        if (onpg.getCreateDep() != null
                && onpg.getCreateDep().getParent() != null
                && OrgLevel.THE_PANEL.equals(onpg.getCreateDep().getLevel())) {

            Set<Integer> managerUserSids = WkOrgUtils.findOrgManagerUserSidByOrgSid(
                    onpg.getCreateDep().getParent().getSid());
            if (WkStringUtils.notEmpty(managerUserSids)) {
                allUserSids.addAll(managerUserSids);
            }
        }

        // 移除停用人員
        return allUserSids.stream()
                .filter(userSid -> WkUserUtils.isActive(userSid))
                .collect(Collectors.toSet());
    }

    @Transactional(readOnly = true)
    public List<WorkOnpg> findByRequire(Require require) {
        if (require == null || Strings.isNullOrEmpty(require.getSid())) {
            return Lists.newArrayList();
        }
        return onpgDao.findBySourceSidOrderByCreatedDateDesc(require.getSid());
    }

    /**
     * @param requireSid
     * @return
     */
    public List<WorkOnpgTo> findByRequireSid(String requireSid) {

        // 查詢
        List<WorkOnpg> WorkOnpgs = onpgDao.findBySourceSidOrderByCreatedDateDesc(requireSid);

        // 轉To
        List<WorkOnpgTo> results = Lists.newArrayList();
        if (results != null) {
            for (WorkOnpg workOnpg : WorkOnpgs) {
                results.add(this.convert2To(workOnpg));
            }
        }

        return results;
    }

    /**
     * 以需求單號查詢
     * 
     * @param requireNo 需求單號
     * @return 筆數
     */
    public Integer countActiveByRequireNo(String requireNo) {
        return this.onpgDao.countActiveByRequireNo(requireNo);
    }

    public String createSearch17ViewUrlLink(WorkOnpg onpg) {
        return "../search/search17.xhtml" + urlService.createSimpleURLLink(URLService.URLServiceAttr.URL_ATTR_S, onpg.getOnpgNo(), onpg.getSourceNo(), 1);
    }

    @Transactional(readOnly = true)
    public List<String> findSidsBySourceSid(String sourceSid) {
        return onpgDao.findSidsBySourceTypeAndSourceSid(WorkSourceType.TECH_REQUEST, sourceSid);
    }

    @Transactional(readOnly = true)
    public WorkOnpg findByOnpgNo(String onpgNo) {
        return onpgDao.findByOnpgNo(onpgNo);
    }

    @Transactional(readOnly = true)
    public Integer findConutByStatus(String requireNo, WorkOnpgStatus status) {
        return onpgDao.findCountBySourceTypeAndSourceNoAndStatus(WorkSourceType.TECH_REQUEST, requireNo, Lists.newArrayList(status));
    }

    @Transactional(readOnly = true)
    public WorkOnpgStatus findOnpgStatus(WorkOnpg onpg) {
        return onpgDao.findOnpgStatus(onpg);
    }

    /**
     * 建立內部流程ON程式
     * 
     * @param requireSid
     * @param theme
     * @param cssContent
     * @param cssNote
     * @param executor
     * @param execDate
     * @return
     * @throws UserMessageException
     */
    @Transactional(rollbackFor = Exception.class)
    public WorkOnpg createInternalOnpg(
            String requireSid,
            String requireNo,
            String theme,
            String cssContent,
            String cssNote,
            User executor,
            Date execDate) throws UserMessageException {

        // ====================================
        // 建立 onpg 主檔 (insert work_onpg)
        // ====================================
        WorkOnpg onpg = this.createEmptyOnpg(requireSid, requireNo, executor, execDate);

        if (!onpg.getNoticeDeps().getValue().contains(executor.getPrimaryOrg().getSid().toString())) {
            onpg.getNoticeDeps().getValue().add(executor.getPrimaryOrg().getSid().toString());
        }
        onpg.setEstablishDate(this.createToDayEsTime());
        onpg.setTheme(theme);
        onpg.setContentCss(cssContent);
        onpg.setContent(jsoupUtils.clearCssTag(onpg.getContentCss()));
        onpg.setNoteCss(cssNote);
        onpg.setNote(jsoupUtils.clearCssTag(onpg.getNoteCss()));
        onpg.setOnpgStatus(WorkOnpgStatus.ALREADY_ON);
        onpg.setOnpgNo(this.formNumberService.generateOpNum()); // 產生單號
        onpg = onpgDao.save(onpg);

        // ====================================
        // 建立讀記錄
        // 1.執行者 -> 已讀
        // 2.填單單位+通知單位人員 -> 待閱讀
        // ====================================
        this.requireReadRecordHelper.saveExcutorReadAndOtherUserWaitRead(
                FormType.WORKONPG,
                onpg.getSid(),
                this.prepareReadRecordUserSids(onpg),
                executor.getSid());

        // ====================================
        // 更新主檔 (tr_require)
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);
        require.setHasOnpg(true);
        this.requireService.save(require);

        // ====================================
        // 建立子程序-通知部門 異動紀錄 (tr_sub_notice_info)
        // ====================================
        subNoticeInfoService.createFirstSubChangeRecord(SubNoticeType.ONPG_INFO_DEP,
                require.getSid(),
                require.getRequireNo(),
                onpg.getSid(),
                onpg.getOnpgNo(),
                onpg.getNoticeDeps(),
                executor.getSid());

        return onpg;
    }

    /**
     * 建立ON程式單 for MMS 同步
     * 
     * @param syncFormTo 取回的MMS 表單資料
     * @param creater    建立者
     * @param execDate   執行時間
     * @return
     * @throws UserMessageException
     */
    // @Transactional(rollbackFor = Exception.class) //因為要同步回MMS不可鎖在這邊
    public WorkOnpg createForMmsSync(
            SyncFormTo syncFormTo,
            Date execDate) throws UserMessageException {

        // ====================================
        // 建立 ONPG 主檔 (insert work_onpg)
        // ====================================
        WorkOnpg onpg = this.createEmptyOnpg(
                syncFormTo.getRequire().getSid(),
                syncFormTo.getRequire().getRequireNo(),
                syncFormTo.getCreateUser(),
                execDate);

        // 預計完成日
        onpg.setEstablishDate(syncFormTo.getMaintenanceDate());
        // 主題
        onpg.setTheme(syncFormTo.getTheme());
        // 內容
        onpg.setContentCss(syncFormTo.getContent());
        // 註記資料來自 MMS 產生
        onpg.setFromMms(true);

        // ====================================
        // 處理通知單位
        // ====================================
        // distinct
        Set<Integer> noticeDepSids = Sets.newHashSet();
        if (WkStringUtils.notEmpty(onpg.getNoticeDeps().getValue())) {
            noticeDepSids = onpg.getNoticeDeps().getValue().stream()
                    .map(depSid -> Integer.parseInt(depSid))
                    .collect(Collectors.toSet());
        }
        // 加入建單者單位
        noticeDepSids.add(syncFormTo.getCreateUser().getPrimaryOrg().getSid());
        // 加入市場單位
        if (syncFormTo.isNoticeMarket()) {
            Set<Integer> marketDepSids = SyncMmsParamHelper.getInstance().findMmsSyncNoticeMarketDepSids();
            if (WkStringUtils.notEmpty(marketDepSids)) {
                noticeDepSids.addAll(marketDepSids);
            }
        }

        // 去除重停用單位，並轉 string
        List<String> finalNoticeDepSids = noticeDepSids.stream()
                .filter(depSid -> WkOrgUtils.isActive(depSid))
                .map(depSid -> depSid + "")
                .collect(Collectors.toList());

        onpg.getNoticeDeps().setValue(finalNoticeDepSids);

        // ====================================
        // 新增ON程式單
        // ====================================
        try {
            this.saveByNewOnpgInNoTransactional(
                    syncFormTo.getRequire(),
                    onpg,
                    syncFormTo.getCreateUser().getPrimaryOrg().getSid(),
                    syncFormTo.getCreateUser(),
                    "",
                    true);
        } catch (UserMessageException e) {
            throw e;
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + e.getMessage();
            log.error(errorMessage, e);
            throw new UserMessageException(errorMessage);
        }
        return onpg;
    }

    private Date createToDayEsTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }

    /**
     * 初始化Tab顯示用物件
     *
     * @param require
     * @return
     */
    @Transactional(readOnly = true)
    public List<WorkOnpg> initTabInfo(Require require) {
        if (require == null || Strings.isNullOrEmpty(require.getSid())) {
            return Lists.newArrayList();
        }
        List<WorkOnpg> onpgs = onpgDao.findBySourceSidOrderByCreatedDateDesc(require.getSid());

        List<WorkOnpgHistory> historys = Lists.newArrayList();
        if (WkStringUtils.notEmpty(onpgs)) {
            historys = opHistoryDao.findByOnpgNoIn(onpgs.stream().map(each -> each.getOnpgNo()).collect(Collectors.toList()));
            if (historys == null) {
                historys = Lists.newArrayList();
            }
        }

        List<WorkOnpgCheckRecordReply> replys = Lists.newArrayList();
        List<WorkOnpgAttachment> attachs = Lists.newArrayList();
        if (!historys.isEmpty()) {
            List<String> historySids = historys.stream().map(each -> each.getSid()).collect(Collectors.toList());
            replys = ckReplyDao.findReplyByHistoryInOrderByUpdateDateDesc(historySids);
            attachs = attachDao.findAttachByHistoryInOrderByUpdateDateDesc(historySids);
        }
        Map<String, List<WorkOnpgCheckRecordReply>> replysMapKeyIsCrSid = replys.stream()
                .collect(Collectors.groupingBy(each -> each.getCheckRecord().getSid()));
        historys.stream()
                .filter(each -> each.getCheckRecord() != null)
                .filter(each -> replysMapKeyIsCrSid.containsKey(each.getCheckRecord().getSid()))
                .forEach(each -> each.getCheckRecord().setCheckRecordReplys(replysMapKeyIsCrSid.get(each.getCheckRecord().getSid())));
        Map<String, List<WorkOnpgAttachment>> attachMapKeyIsHistorySid = attachs.stream().collect(Collectors.groupingBy(each -> each.getHistory().getSid()));
        historys.stream()
                .filter(each -> attachMapKeyIsHistorySid.containsKey(each.getSid()))
                .forEach(each -> each.setAttachments(attachMapKeyIsHistorySid.get(each.getSid())));
        Map<String, List<WorkOnpgHistory>> historysMapKeyIsOpNo = historys.stream().collect(Collectors.groupingBy(WorkOnpgHistory::getOnpgNo));
        onpgs.stream()
                .filter(op -> historysMapKeyIsOpNo.containsKey(op.getOnpgNo()))
                .forEach(op -> {

                    List<WorkOnpgHistory> currHistorys = historysMapKeyIsOpNo.get(op.getOnpgNo());
                    if (WkStringUtils.notEmpty(currHistorys)) {
                        List<WorkOnpgHistory> filterdHistorys = currHistorys.stream()
                                .filter(currHistory -> !WorkOnpgHistoryBehavior.CHECK_RECORD_REPLY.equals(currHistory.getBehavior()))
                                .collect(Collectors.toList());
                        op.setHistorys(filterdHistorys);
                    }

                });
        return onpgs;
    }

    /**
     * 轉換為自訂物件
     *
     * @param onpgNo
     * @return
     */
    public WorkOnpgTo findToByOnpgNo(String onpgNo) {
        WorkOnpg onpg = this.findByOnpgNo(onpgNo);
        if (onpg == null) {
            log.error("查無該onpg資料，onpg單號：" + onpgNo);
            return null;
        }
        return convert2To(onpg);
    }

    public WorkOnpgTo convert2To(WorkOnpg onpg) {
        if (onpg == null) {
            return null;
        }
        WorkOnpgTo to = new WorkOnpgTo();
        to.setSid(onpg.getSid());
        to.setStatus(onpg.getStatus());
        to.setOnpgNo(onpg.getOnpgNo());
        to.setSourceNo(onpg.getSourceNo());
        to.setTheme(onpg.getTheme());
        to.setContent(onpg.getContent());
        to.setContentCss(onpg.getContentCss());
        to.setNote(onpg.getNote());
        to.setNoteCss(onpg.getNoteCss());
        to.setNoticeDeps(onpg.getNoticeDeps());
        to.setEstablishDate(onpg.getEstablishDate());
        to.setFinishDate(onpg.getFinishDate());
        to.setCancelDate(onpg.getCancelDate());
        to.setOnpgStatus(onpg.getOnpgStatus());
        to.setUpdatedDate(onpg.getUpdatedDate());
        to.setFromMms(onpg.isFromMms());

        if (onpg.getCreateDep() != null) {
            to.setCreateDepSid(onpg.getCreateDep().getSid());
        }

        if (onpg.getCreatedUser() != null) {
            to.setCreateUsr(onpg.getCreatedUser().getSid());
        }

        return to;
    }

    /**
     * 在存檔前進行檢核 (避免開啟到編輯其間，有其他人異動資料)
     *
     * @param tempOnpgTo Onpg副本
     * @param dbSource   DB即時資料
     * @throws OnpgEditExceptions
     * @throws UserMessageException
     */
    private void checkWorkOnpgByBeforeSave(WorkOnpgTo tempOnpgTo, WorkOnpg dbSource) throws UserMessageException {
        long tempUpdatedDate = tempOnpgTo.getUpdatedDate() == null ? Long.MAX_VALUE : tempOnpgTo.getUpdatedDate().getTime();
        long dbSourceUpdatedDate = dbSource.getUpdatedDate() == null ? Long.MAX_VALUE : dbSource.getUpdatedDate().getTime();

        if (tempUpdatedDate != dbSourceUpdatedDate) {
            List<String> columnNames = Lists.newArrayList();
            if (!this.checkSameByNoticeDeps(tempOnpgTo.getNoticeDeps().getValue(), dbSource.getNoticeDeps().getValue())) {
                columnNames.add("通知單位");
            }
            if (!tempOnpgTo.getTheme().equals(dbSource.getTheme())) {
                columnNames.add("主題");
            }
            if (!tempOnpgTo.getContentCss().equals(dbSource.getContentCss())) {
                columnNames.add("內容");
            }
            if (!tempOnpgTo.getNoteCss().equals(dbSource.getNoteCss())) {
                columnNames.add("備註");
            }
            if (dbSource.getEstablishDate() != null
                    && tempOnpgTo.getEstablishDate().compareTo(dbSource.getEstablishDate()) != 0) {
                columnNames.add("預計完成日");
            }
            if (dbSource.getFinishDate() != null
                    && tempOnpgTo.getFinishDate().compareTo(dbSource.getFinishDate()) != 0) {
                columnNames.add("檢查完成日");
            }
            if (dbSource.getCancelDate() != null
                    && tempOnpgTo.getCancelDate().compareTo(dbSource.getCancelDate()) != 0) {
                columnNames.add("取消日");
            }
            if (!tempOnpgTo.getOnpgStatus().equals(dbSource.getOnpgStatus())) {
                columnNames.add("狀態");
            }
            if (WkStringUtils.notEmpty(columnNames)) {
                String errMsg = "此 ON 程式單內容已經被異動，請重整頁面!";
                errMsg += "<br/>異動欄位：" + String.join("、", columnNames);
                throw new UserMessageException(errMsg, InfomationLevel.WARN);
            }
        }
    }

    /**
     * 判斷通知單位是否相同
     *
     * @param noticeDepsByTemp
     * @param noticeDepsByDb
     * @return
     */
    private boolean checkSameByNoticeDeps(List<String> noticeDepsByTemp, List<String> noticeDepsByDb) {
        if (noticeDepsByTemp.size() != noticeDepsByDb.size()) {
            return false;
        }
        return noticeDepsByTemp.stream()
                .allMatch(each -> noticeDepsByDb.contains(each));
    }

    /**
     * 存檔只部份更新
     *
     * @param obj
     * @param executor
     */
    @Transactional(rollbackFor = Exception.class)
    private WorkOnpg modifyByEditOnpg(WorkOnpg obj, User executor) {
        WorkOnpg backupOnpg = this.findByOnpgNo(obj.getOnpgNo());
        backupOnpg.setNoticeDeps(obj.getNoticeDeps());
        backupOnpg.setTheme(obj.getTheme());
        backupOnpg.setContent(obj.getContent());
        backupOnpg.setContentCss(obj.getContentCss());
        backupOnpg.setNote(obj.getNote());
        backupOnpg.setNoteCss(obj.getNoteCss());
        backupOnpg.setEstablishDate(obj.getEstablishDate());
        backupOnpg.setReadUpdateDate(obj.getReadUpdateDate());
        backupOnpg.setReadReason(obj.getReadReason());
        backupOnpg.setReadRecord(obj.getReadRecord());
        backupOnpg.setUpdatedDate(new Date());
        backupOnpg.setUpdatedUser(executor);
        return onpgDao.save(backupOnpg);
    }

    /**
     * 複製
     *
     * @param obj
     */
    public WorkOnpg copyOnpg(WorkOnpg obj) {
        WorkOnpg backupOnpg = this.findByOnpgNo(obj.getOnpgNo());
        obj.setNoticeDeps(backupOnpg.getNoticeDeps());
        obj.setTheme(backupOnpg.getTheme());
        obj.setContent(backupOnpg.getContent());
        obj.setContentCss(backupOnpg.getContentCss());
        obj.setNote(backupOnpg.getNote());
        obj.setNoteCss(backupOnpg.getNoteCss());
        obj.setEstablishDate(backupOnpg.getEstablishDate());
        obj.setFinishDate(backupOnpg.getFinishDate());
        obj.setCancelDate(backupOnpg.getCancelDate());
        obj.setReadUpdateDate(backupOnpg.getReadUpdateDate());
        obj.setReadReason(backupOnpg.getReadReason());
        obj.setReadRecord(backupOnpg.getReadRecord());
        obj.setOnpgStatus(backupOnpg.getOnpgStatus());
        obj.setCreateCompany(backupOnpg.getCreateCompany());
        obj.setCreateDep(backupOnpg.getCreateDep());
        obj.setStatus(backupOnpg.getStatus());
        obj.setCreatedUser(backupOnpg.getCreatedUser());
        obj.setCreatedDate(backupOnpg.getCreatedDate());
        obj.setUpdatedUser(backupOnpg.getUpdatedUser());
        obj.setUpdatedDate(backupOnpg.getUpdatedDate());
        // obj.setCheckMemos(backupOnpg.getCheckMemos());
        // obj.setHistorys(backupOnpg.getHistorys());
        // obj.setCheckRecords(backupOnpg.getCheckRecords());
        // obj.setAttachments(backupOnpg.getAttachments());
        return obj;
    }

    /**
     * 批次轉單程式-建立單位轉發(ON程式)
     * 
     * @return
     */
    public List<TransRequireVO> findOnpgByCreateDeptAndUser(List<Integer> deptSids, List<Integer> userSids) {
        List<TransRequireVO> resultList = trNamedQueryRepository.findOnpgByCreateDeptAndUser(deptSids, userSids);
        return convert(resultList);
    }

    /**
     * 批次轉單程式-通知單位轉發(ON程式)
     * 
     * @return
     */
    public List<TransRequireVO> findOnpgByNotifiedDept(String depts) {
        List<TransRequireVO> resultList = trNamedQueryRepository.findOnpgByNotifiedDept(depts);
        return convert(resultList);
    }

    private List<TransRequireVO> convert(List<TransRequireVO> resultList) {
        for (TransRequireVO vo : resultList) {
            vo.setType(RequireTransProgramType.WORKONPG.name());
            vo.setCreateDept(WkOrgCache.getInstance().findNameBySid(vo.getDeptSid()));
            vo.setCreateUserName(WkUserCache.getInstance().findBySid(vo.getUserSid()).getId());
            vo.setStatus(WorkOnpgStatus.valueOf(vo.getStatus()).getLabel());
        }
        return resultList;
    }

    /**
     * 計算完成或未完成的單據
     * 
     * @param sourceNo   需求單號
     * @param createDeps 過濾建立部門 (為空時不過濾)
     * @param isComplete true:完成 / false:未完成
     * @return
     */
    public Integer countByCompleteStatus(
            String sourceNo,
            List<Org> createDeps,
            boolean isComplete) {

        // 取得本次所有需要過濾的狀態
        List<WorkOnpgStatus> filterStatus = Lists.newArrayList(WorkOnpgStatus.values()).stream()
                .filter(ptStatus -> isComplete == ptStatus.isInFinish())
                .collect(Collectors.toList());

        // 需過濾建立部門
        if (WkStringUtils.notEmpty(createDeps)) {
            return this.onpgDao.queryCountByCreateDepAndInOnpgStatus(
                    WorkSourceType.TECH_REQUEST,
                    sourceNo,
                    createDeps,
                    filterStatus);
        }

        // 僅檢查狀態
        return this.onpgDao.queryCountByOnpgStatus(
                WorkSourceType.TECH_REQUEST,
                sourceNo,
                filterStatus);
    }

    /**
     * 計算完成或未完成的單據
     * 
     * @param sourceNo   需求單號
     * @param createDeps 過濾建立部門 (為空時不過濾)
     * @param isComplete true:完成 / false:未完成
     * @return
     */
    public List<WorkOnpg> queryByCompleteStatus(
            String sourceNo,
            List<Org> createDeps,
            boolean isComplete) {

        // 取得本次所有需要過濾的狀態
        List<WorkOnpgStatus> filterStatus = Lists.newArrayList(WorkOnpgStatus.values()).stream()
                .filter(ptStatus -> isComplete == ptStatus.isInFinish())
                .collect(Collectors.toList());

        // 需過濾建立部門

        return this.onpgDao.queryByCreateDepAndInOnpgStatus(
                WorkSourceType.TECH_REQUEST,
                sourceNo,
                createDeps,
                filterStatus);

    }

    /**
     * 強制完成
     * 
     * @param require             需求單主檔
     * @param forceCompleteStatus 強制完成類型
     * @param execUser            執行者
     * @param execDate            執行時間
     * @param createDeps          指定立案單位 （傳入null時,代表不限定）
     * @return 已強制結案單據資料
     */
    @Transactional(rollbackFor = Exception.class)
    public List<WorkOnpg> forceComplete(
            Require require,
            WorkOnpgStatus forceCompleteStatus,
            User execUser,
            Date execDate,
            List<Org> createDeps) {

        if (forceCompleteStatus == null || !forceCompleteStatus.isForceCompleteStatus()) {
            throw new SystemDevelopException("傳入的 WorkOnpgStatus 不是可以處理的類型：[" + forceCompleteStatus + "]", InfomationLevel.ERROR);
        }

        // ====================================
        // 取得所有未完成單據
        // ====================================
        // 收集未完成狀態
        List<WorkOnpgStatus> notCompleteStatus = Lists.newArrayList(WorkOnpgStatus.values()).stream()
                .filter(status -> !status.isInFinish())
                .collect(Collectors.toList());

        List<WorkOnpg> workOnpgs = null;

        if (createDeps == null) {
            // 取得需求單下所有子單
            workOnpgs = this.onpgDao.findBySourceTypeAndSourceSidAndOnpgStatusIn(
                    WorkSourceType.TECH_REQUEST,
                    require.getSid(),
                    notCompleteStatus);
        } else {
            // 取得需求單下傳入單位開立的子單
            workOnpgs = this.onpgDao.findBySourceTypeAndSourceSidAndOnpgStatusInAndCreateDepIn(
                    WorkSourceType.TECH_REQUEST,
                    require.getSid(),
                    notCompleteStatus,
                    createDeps);
        }

        if (WkStringUtils.isEmpty(workOnpgs)) {
            log.debug("沒有需要強制關閉的onpg單");
            return Lists.newArrayList();
        }

        // ====================================
        // 逐筆處理
        // ====================================
        for (WorkOnpg workOnpg : workOnpgs) {

            log.info("[{}]:ONPG單[{}]因 {} 而關閉!",
                    workOnpg.getSourceNo(),
                    workOnpg.getOnpgNo(),
                    forceCompleteStatus.getLabel());

            // ------------------------------
            // 歷程檔
            // ------------------------------
            WorkOnpgHistory history = this.onpgHistoryService.createEmptyHistory(
                    workOnpg, execUser, execDate);

            history.setBehavior(WorkOnpgHistoryBehavior.FORCE_CLOSE);
            history.setBehaviorStatus(forceCompleteStatus);
            history.setVisiable(Boolean.TRUE);
            history.setReason("因強制完成而關閉");
            history.setReasonCss("因強制完成而關閉");
            this.onpgHistoryService.save(history);

            // ------------------------------
            // 異動主檔狀態
            // ------------------------------
            workOnpg.setOnpgStatus(forceCompleteStatus);
            workOnpg.setReadReason(WaitReadReasonType.FORCE_CLOSE);
            workOnpg.setReadUpdateDate(execDate);
            workOnpg.setFinishDate(execDate);
            workOnpg.setUpdatedDate(execDate);
            workOnpg.setUpdatedUser(execUser);
            this.save(workOnpg);

            this.onpgHistoryService.putHistoryToOnpg(workOnpg, history);
            this.onpgHistoryService.sortHistory(workOnpg);

            // ------------------------------
            // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
            // ------------------------------
            this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                    FormType.WORKONPG,
                    workOnpg.getSid(),
                    execUser.getSid());
        }

        return workOnpgs;
    }

    /**
     * 依據需求單號查詢
     * 
     * @param sourceNo
     */
    public List<WorkOnpgTo> findByRequireNoAndCreateDepSids(String requireNo, List<Integer> depSids) {

        // ====================================
        // 檢查傳入參數為空
        // ====================================
        if (WkStringUtils.isEmpty(requireNo) || WkStringUtils.isEmpty(depSids)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 轉 org
        // ====================================
        List<Org> deps = Lists.newArrayList();
        for (Integer depSid : depSids) {
            Org dep = WkOrgCache.getInstance().findBySid(depSid);
            if (dep != null) {
                deps.add(dep);
            }
        }

        // ====================================
        // 查詢
        // ====================================
        List<WorkOnpg> items = this.onpgDao.findBySourceTypeAndStatusAndSourceNoAndCreateDepIn(
                WorkSourceType.TECH_REQUEST,
                Activation.ACTIVE,
                requireNo,
                deps);

        // ====================================
        // 轉To
        // ====================================
        List<WorkOnpgTo> results = Lists.newArrayList();
        if (items != null) {
            for (WorkOnpg item : items) {
                results.add(this.convert2To(item));
            }
        }

        return results;
    }

    /**
     * ON程式取消 By MMS
     * 
     * @param syncFormTo
     */
    public void cancelByMms(SyncFormTo syncFormTo) {

        // ====================================
        // 查詢主單
        // ====================================
        WorkOnpg onpg = this.findByOnpgNo(syncFormTo.getOnpgNo());

        // ====================================
        // 更新ON程式單狀態
        // ====================================

        onpg.setOnpgStatus(WorkOnpgStatus.CANCEL_ONPG);
        onpg.setReadReason(WaitReadReasonType.ONPG_CANCEL);
        onpg.setReadUpdateDate(new Date());
        onpg.setCancelDate(new Date());

        this.save(onpg, syncFormTo.getCreateUser());

        syncFormTo.setWorkOnpgTo(this.convert2To(onpg));

        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.WORKONPG,
                onpg.getSid(),
                syncFormTo.getCreateUser().getSid());

        // ====================================
        // 記錄
        // ====================================
        WorkOnpgHistory workOnpgHistory = this.onpgHistoryService.createEmptyHistory(onpg, syncFormTo.getCreateUser());
        workOnpgHistory.setBehavior(WorkOnpgHistoryBehavior.CANCEL_ONPG);
        workOnpgHistory.setVisiable(Boolean.TRUE);

        // 兜組追蹤內容
        String traceContent = ""
                + WkHtmlUtils.addBlueBlodClass("維護管理系統(MMS) ON程式單同步")
                + "<br/>"
                + "<br/>" + WkHtmlUtils.addBlueBlodClass("執行資訊如下：")
                + "<br/>執行：【" + syncFormTo.getLogSyncMmsActionType().getDescr() + "】"
                + "<br/>工作ID：【" + syncFormTo.getMmsID() + "】"
                + "<br/>ON程式單：【" + syncFormTo.getOnpgNo() + "】"
                + "<br/>";

        workOnpgHistory.setReasonCss(traceContent);
        workOnpgHistory.setReason(jsoupUtils.htmlToText(traceContent));

        this.onpgHistoryService.save(workOnpgHistory);

    }
}
