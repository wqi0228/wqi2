/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.config;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.google.common.collect.Lists;

/**
 *
 * @author shaun
 */
@Configuration("com.cy.tech.request.logic.config")
@ComponentScan("com.cy.tech.request.logic")
@Import({
        com.cy.work.logic.config.WorkLogicConfig.class,
        // repo - tech-request
        com.cy.tech.request.repository.config.RepositoryConfig.class,
        // client - bpm
        com.cy.bpm.rest.client.config.RestClientConfig.class,
        // client - bfb-client
        com.cy.fb.rest.client.config.FbRestClientConfig.class,
        // client - issue-client
        com.cy.work.tech.issue.client.config.TechIssueRestClientConfig.class,
        // client 需求單 client
        com.cy.tech.request.config.TechReqRestClientConfig.class,
        // repo - work-trace
        com.cy.work.trace.repository.config.RepositoryConfig.class
})
public class LogicConfig {

	@Bean
	public RestTemplate restTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(new FormHttpMessageConverter());
		messageConverters.add(new StringHttpMessageConverter(StandardCharsets.UTF_8));
		messageConverters.add(new MappingJackson2HttpMessageConverter());
		restTemplate.setMessageConverters(messageConverters);
	
		
		return restTemplate;
	}
	
	@Bean
    public HttpHeaders basicHeaders() {
        HttpHeaders headers = new HttpHeaders();
        List<MediaType> acceptableMediaTypes = Lists.newArrayList();
        acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
        acceptableMediaTypes.add(MediaType.TEXT_PLAIN);
        headers.setAccept(acceptableMediaTypes);

        List<Charset> acceptableCharSet = Lists.newArrayList();
        acceptableCharSet.add(Charset.defaultCharset());
        acceptableCharSet.add(Charset.forName("UTF-8"));
        headers.setAcceptCharset(acceptableCharSet);
        return headers;
    }

}
