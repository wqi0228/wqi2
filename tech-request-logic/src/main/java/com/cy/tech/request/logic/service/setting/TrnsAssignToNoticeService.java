package com.cy.tech.request.logic.service.setting;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.AssignNoticeService;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmAddReasonType;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepService;
import com.cy.tech.request.logic.vo.AssignNoticeProcessTo;
import com.cy.tech.request.logic.vo.RequireConfirmDepVO;
import com.cy.tech.request.vo.converter.SetupInfoToConverter;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.ReqConfirmDepProgStatus;
import com.cy.tech.request.vo.value.to.SetupInfoTo;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * 『分派單位』轉『通知單位』
 * 
 * @author allen1214_wu
 */
@Slf4j
@Service
public class TrnsAssignToNoticeService {

    // ========================================================================
    // 服務元件區
    // ========================================================================
    @Autowired
    private transient NativeSqlRepository nativeSqlRepository;
    @Autowired
    private transient EntityManager entityManager;
    @Autowired
    private transient AssignNoticeService assignNoticeService;
    @Autowired
    private transient RequireConfirmDepService requireConfirmDepService;

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * 執行轉檔
     * 
     * @param trnsAssignToNoticeDepSids
     * @throws UserMessageException
     * @throws SystemDevelopException
     */
    public String process(
            Set<Integer> trnsAssignToNoticeDepSids,
            String minRequireNo,
            String maxRequireNo,
            User execUser,
            Date sysDate) throws UserMessageException, SystemDevelopException {

        log.info("開始執行：『分派單位』轉『通知單位』，單號[" + minRequireNo + "～" + maxRequireNo + "]");

        // ====================================
        // 防呆
        // ====================================
        if (WkStringUtils.isEmpty(trnsAssignToNoticeDepSids)) {
            return "";
        }

        // ====================================
        // 查詢需要轉檔的資料
        // ====================================
        List<AssignNoticeProcessTo> assignNoticeProcessTos = this.findRequireConfirmDep(
                trnsAssignToNoticeDepSids,
                minRequireNo,
                maxRequireNo);

        if (WkStringUtils.isEmpty(assignNoticeProcessTos)) {
            log.info("單號[" + minRequireNo + "～" + maxRequireNo + "未查詢到需要轉檔的資料");
            return "";
        }

        // ====================================
        // 分析分派轉通知單位
        // ====================================
        this.prepareByRequireConfirmDep(assignNoticeProcessTos, trnsAssignToNoticeDepSids);

        // 經過分析後, 沒有必須要執行的資料
        if (WkStringUtils.isEmpty(assignNoticeProcessTos)) {
            return "";
        }

        // ====================================
        // 跑分派通知設定流程
        // ====================================
        String errorMessage = this.assignNoticeService.batchDoPocess(
                assignNoticeProcessTos,
                execUser,
                sysDate,
                RequireConfirmAddReasonType.AUTO_ASSIGN_BY_CREATE_CHILD_CASE,
                "分派單位轉通知單位");

        log.info("執行完成");

        entityManager.clear();

        return errorMessage;
    }

    /**
     * 查詢需減派的需求確認單位資料 （）tr_require_confirm_dep
     * 
     * @param trnsDepSids
     * @return
     * @throws UserMessageException
     */
    private List<AssignNoticeProcessTo> findRequireConfirmDep(
            Set<Integer> trnsDepSids,
            String minRequireNo,
            String maxRequireNo) throws UserMessageException {

        // ====================================
        // 查詢
        // ====================================
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT req.require_sid     AS requireSid, ");
        sql.append("       req.require_no      AS requireNo, ");
        sql.append("       assignInfo.info     AS assignInfoJsonStr, ");
        sql.append("       assignInfo.info_sid AS assignInfoSid, ");
        sql.append("       noticeInfo.info     AS noticeInfoJsonStr, ");
        sql.append("       noticeInfo.info_sid AS noticeInfoSid ");

        // 限定單據狀態為 進行中、已完成
        sql.append("FROM   (SELECT * "
                + "			  FROM tr_require reqq  "
                + "          WHERE reqq.require_status IN ('PROCESS','COMPLETED') "
                + "            AND reqq.require_no >= :minRequireNo "
                + "            AND reqq.require_no <= :maxRequireNo "
                + "        ) req ");

        // 分派單位
        sql.append("       LEFT JOIN tr_assign_send_info assignInfo ");
        sql.append("              ON req.require_sid = assignInfo.require_sid ");
        sql.append("                 AND assignInfo.type = " + AssignSendType.ASSIGN.ordinal() + " ");
        sql.append("                 AND assignInfo.create_dt = (SELECT Max(ai1.create_dt) AS maxTime ");
        sql.append("                                             FROM   tr_assign_send_info ai1 ");
        sql.append("                                             WHERE  assignInfo.require_sid = ai1.require_sid ");
        sql.append("                                                    AND assignInfo.type = ai1.type ");
        sql.append("                                             GROUP  BY ai1.require_sid) ");

        // 通知單位
        sql.append("       LEFT JOIN tr_assign_send_info noticeInfo ");
        sql.append("              ON req.require_sid = noticeInfo.require_sid ");
        sql.append("                 AND noticeInfo.type = " + AssignSendType.SEND.ordinal() + " ");
        sql.append("                 AND noticeInfo.create_dt = (SELECT Max(ai2.create_dt) AS maxTime ");
        sql.append("                                             FROM   tr_assign_send_info ai2 ");
        sql.append("                                             WHERE  noticeInfo.require_sid = ai2.require_sid ");
        sql.append("                                                    AND noticeInfo.type = ai2.type ");
        sql.append("                                             GROUP  BY ai2.require_sid) ");

        // 限定有分派單位的資料
        sql.append("WHERE  EXISTS (SELECT * ");
        sql.append("               FROM   tr_assign_send_search_info searchInfo ");
        sql.append("               WHERE  req.require_sid = searchInfo.require_sid ");
        sql.append("                      AND searchInfo.type = " + AssignSendType.ASSIGN.ordinal() + " ");
        sql.append("                      AND searchInfo.dep_sid IN ( :trnsDepSids )) ");

        sql.append("ORDER BY req.require_no ");

        // 確認檔狀態為已完成、無需確認者不轉
        // sql.append(" AND cDep.prog_status NOT IN ( 'COMPLETE', 'PASS' ) ");

        // 參數
        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put("trnsDepSids", trnsDepSids);
        parameters.put("minRequireNo", minRequireNo);
        parameters.put("maxRequireNo", maxRequireNo);

        // 查詢
        List<AssignNoticeProcessTo> results = nativeSqlRepository.getResultList(
                sql.toString(), parameters, AssignNoticeProcessTo.class);

        if (WkStringUtils.isEmpty(results)) {
            log.info("已經沒有需要轉檔的資料");
            return Lists.newArrayList();
            // throw new UserMessageException("已經沒有需要轉檔的資料", InfomationLevel.INFO);
        }

        // ====================================
        // 資料轉置
        // ====================================
        //
        for (AssignNoticeProcessTo to : results) {
            // JSON 轉分派部門
            to.setOriginalAssignDepSids(this.convertJsonStrToDepInfo(to.getAssignInfoJsonStr()));
            // JSON 轉通知部門
            to.setOriginalNoticeDepSids(this.convertJsonStrToDepInfo(to.getNoticeInfoJsonStr()));
        }

        return results;
    }

    /**
     * 分析分派轉通知單位
     * 
     * @param assignNoticeProcessTos
     * @param trnsAssignToNoticeDepSids
     */
    private void prepareByRequireConfirmDep(
            List<AssignNoticeProcessTo> assignNoticeProcessTos,
            Set<Integer> trnsAssignToNoticeDepSids) {

        if (WkStringUtils.isEmpty(assignNoticeProcessTos) || WkStringUtils.isEmpty(trnsAssignToNoticeDepSids)) {
            return;
        }

        // ====================================
        // 查詢需求確認單位檔的執行狀態 (tr_require_confirm_dep.prog_status)
        // ====================================
        Map<String, ReqConfirmDepProgStatus> progStatusMapByReqConfirmDepKey = this.queryReqConfirmDepProgStatus(
                assignNoticeProcessTos, trnsAssignToNoticeDepSids);

        // ====================================
        // 依據需求確認狀態, 決定單位是否可減派
        // ====================================
        // 需求單位已確認者, 且減派後會移除需求單位者,不可減派
        // 說明：『需求單位來源分派單位』：多組分派單位收束後，為需求單位

        // 逐筆執行
        List<AssignNoticeProcessTo> passDatas = Lists.newArrayList();
        for (AssignNoticeProcessTo assignNoticeProcessTo : assignNoticeProcessTos) {

            // 分析要被減派(轉通知單位)的分派單位
            Set<Integer> deleteAssignDepSids = this.analysisDeleteAssignDeps(
                    assignNoticeProcessTo,
                    progStatusMapByReqConfirmDepKey,
                    trnsAssignToNoticeDepSids);

            // 沒有要減派的單位, 代表此筆無需處理
            if (WkStringUtils.isEmpty(deleteAssignDepSids)) {
                passDatas.add(assignNoticeProcessTo);
                continue;
            }

            // 轉檔後的分派單位 (複製一份轉檔前的)
            Set<Integer> newAssignDepSids = assignNoticeProcessTo.getOriginalAssignDepSids().stream().collect(Collectors.toSet());
            // 轉檔後的通知單位 (複製一份轉檔前的)
            Set<Integer> newNoticeDepSids = assignNoticeProcessTo.getOriginalNoticeDepSids().stream().collect(Collectors.toSet());

            // 逐筆檢查轉檔前的分派單位,是否減派
            for (Integer assignDepSid : assignNoticeProcessTo.getOriginalAssignDepSids()) {
                // 比對該分派單位是否為移轉標的
                if (deleteAssignDepSids.contains(assignDepSid)) {
                    // 從分派單位移除
                    newAssignDepSids.remove(assignDepSid);
                    // 加入通知單位 (因為是 set , 若已存在會略過)
                    newNoticeDepSids.add(assignDepSid);
                }
            }

            // 放回TO
            assignNoticeProcessTo.setNewAssignDepSids(newAssignDepSids);
            assignNoticeProcessTo.setNewNoticeDepSids(newNoticeDepSids);
        }

        // 將不用處理的資料移除
        if (WkStringUtils.notEmpty(passDatas)) {
            assignNoticeProcessTos.removeAll(passDatas);
        }
    }

    /**
     * 分析要減派的單位
     * 
     * @param assignNoticeProcessTo
     * @param progStatusMapByReqConfirmDepKey
     * @param trnsAssignToNoticeDepSids
     * @return
     */
    private Set<Integer> analysisDeleteAssignDeps(
            AssignNoticeProcessTo assignNoticeProcessTo,
            Map<String, ReqConfirmDepProgStatus> progStatusMapByReqConfirmDepKey,
            Set<Integer> trnsAssignToNoticeDepSids) {

        // ====================================
        // 計算『分派單位』收束後的『需求確認單位』
        // ====================================
        // MAP<收束後的需求單位SID, Set<需求單位來源的分派單位>>
        Map<Integer, Set<Integer>> assignDepSidsMaoByReqComfirmDepSid = this.requireConfirmDepService.prepareRequireConfirmDepSidMap(
                assignNoticeProcessTo.getOriginalAssignDepSids());
        // MAP<分派單位, 收束後的需求單位>
        Map<Integer, Integer> reqConfirmSidMapByAssignDepSid = Maps.newHashMap();
        for (Entry<Integer, Set<Integer>> entry : assignDepSidsMaoByReqComfirmDepSid.entrySet()) {
            for (Integer assignDepSid : entry.getValue()) {
                reqConfirmSidMapByAssignDepSid.put(assignDepSid, entry.getKey());
            }
        }

        // ====================================
        // 以每一個『需求確認單位』為單位，分析
        // ====================================
        // 減派單位名單
        Set<Integer> deleteAssignDepSids = Sets.newHashSet();
        // 記錄因需求確認狀態略過的單位
        // Map<需求單確認單位SID, Set<分派單位SID>>
        Map<Integer, Set<Integer>> passTrnsDepSidsByReqDep = Maps.newHashMap();

        for (Entry<Integer, Set<Integer>> entry : assignDepSidsMaoByReqComfirmDepSid.entrySet()) {
            // ===================
            // 取得相關資料
            // ===================
            // 需求確認單位 SID
            Integer reqConfirmDepSid = entry.getKey();
            // 需求確認單位 SID
            Set<Integer> assignDepSids = entry.getValue();

            // 取得需求單位執行狀態
            ReqConfirmDepProgStatus progStatus = progStatusMapByReqConfirmDepKey.get(
                    this.combinReqConfirmDepKey(assignNoticeProcessTo.getRequireSid(), reqConfirmDepSid));

            // ===================
            // 當需求確認單位已確認時，檢查狀態是否要略過該組
            // ===================
            // 1.當需求確認單位，狀態為已確認時，不可移除確認資料
            // 2.當所有『來源分派單位』被減派時，會造成『需求確認單位』流程被移除
            // 1+2時, pass 相關分派單位減派
            if (progStatus != null && ReqConfirmDepProgStatus.COMPLETE.equals(progStatus)) {
                // clone 一份分派單位
                Set<Integer> cloneAssignDepSids = Sets.newHashSet(assignDepSids);
                // 檢查分派單位, 存在於移轉名單中
                for (Integer assignDepSid : assignDepSids) {
                    if (trnsAssignToNoticeDepSids.contains(assignDepSid)) {
                        cloneAssignDepSids.remove(assignDepSid);
                    }
                }
                // 若所有『來源分派單位』都被移除時，則 pass 此組 （會造成已確認的需求單位被移除）
                if (cloneAssignDepSids.size() == 0) {
                    // 記錄 pass 的單位
                    passTrnsDepSidsByReqDep.put(reqConfirmDepSid, assignDepSids);
                    continue;
                }
            }

            // ===================
            // 整理減派單位
            // ===================
            // 所有分派單位, 存在於移轉名單中, 即加入減派名單
            for (Integer assignDepSid : assignDepSids) {
                if (trnsAssignToNoticeDepSids.contains(assignDepSid)) {
                    deleteAssignDepSids.add(assignDepSid);
                }
            }
        }

        // ====================================
        // 印出不轉單位的訊息
        // ====================================
        for (Entry<Integer, Set<Integer>> entry : passTrnsDepSidsByReqDep.entrySet()) {
            String message = "\r\n[%s]：因需求單位-[%s]已確認需求, 故略過轉換分派單位:[%s]";
            message = String.format(
                    message,
                    assignNoticeProcessTo.getRequireNo(),
                    WkOrgUtils.findNameBySid(Lists.newArrayList(entry.getKey()), "-"),
                    WkOrgUtils.findNameBySid(Lists.newArrayList(entry.getValue()), "-"));
            log.info(message);
        }

        return deleteAssignDepSids;
    }

    /**
     * 查尋需求確認單位檔的執行狀態 (tr_require_confirm_dep.prog_status)
     * 
     * @param assignNoticeProcessTos
     * @param trnsAssignToNoticeDepSids
     * @return MAP<需求單SID+需求確認單位SID , 確認狀態>
     */
    private Map<String, ReqConfirmDepProgStatus> queryReqConfirmDepProgStatus(
            List<AssignNoticeProcessTo> assignNoticeProcessTos,
            Set<Integer> trnsAssignToNoticeDepSids) {

        // ====================================
        // 收集所有『需求單號』
        // ====================================
        // 收集需求單號
        Set<String> requireSids = assignNoticeProcessTos.stream()
                .map(AssignNoticeProcessTo::getRequireSid).collect(Collectors.toSet());

        // ====================================
        // 收集所有的『需求確認單位』
        // ====================================
        // 收集所有要移轉的單位
        Set<Integer> allTrnsDeps = Sets.newHashSet();
        for (AssignNoticeProcessTo assignNoticeProcessTo : assignNoticeProcessTos) {
            if (WkStringUtils.isEmpty(assignNoticeProcessTo.getOriginalAssignDepSids())) {
                continue;
            }
            for (Integer assignDepSid : assignNoticeProcessTo.getOriginalAssignDepSids()) {
                if (trnsAssignToNoticeDepSids.contains(assignDepSid)) {
                    allTrnsDeps.add(assignDepSid);
                }
            }
        }

        // 計算所產生的需求確認單位
        List<Org> reqConfirmDeps = this.requireConfirmDepService.prepareRequireConfirmDeps(Lists.newArrayList(allTrnsDeps));

        List<Integer> reqConfirmDepSids = reqConfirmDeps.stream()
                .map(Org::getSid)
                .collect(Collectors.toList());

        // ====================================
        // 查詢『需求確認單位』確認狀態
        // ====================================
        StringBuffer varname1 = new StringBuffer();
        varname1.append("SELECT require_sid AS requireSid, ");
        varname1.append("       dep_sid     AS depSid, ");
        varname1.append("       prog_status AS progStatusStr ");
        varname1.append("FROM   tr_require_confirm_dep ");
        varname1.append("WHERE  status = 0 ");
        varname1.append("       AND require_sid IN ( :requireSids ) ");
        varname1.append("       AND dep_sid IN ( :reqConfirmDepSids )");

        // 參數
        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put("requireSids", requireSids);
        parameters.put("reqConfirmDepSids", reqConfirmDepSids);

        // 查詢
        List<RequireConfirmDepVO> requireConfirmDepVOs = nativeSqlRepository.getResultList(
                varname1.toString(), parameters, RequireConfirmDepVO.class);

        return requireConfirmDepVOs.stream()
                .collect(Collectors.toMap(
                        vo -> this.combinReqConfirmDepKey(vo.getRequireSid(), vo.getDepSid()),
                        vo -> vo.getProgStatus()));
    }

    /**
     * @param requireSid
     * @param depSid
     * @return
     */
    private String combinReqConfirmDepKey(String requireSid, Integer depSid) {
        return requireSid + "|" + depSid;
    }

    private SetupInfoToConverter converter = new SetupInfoToConverter();

    /**
     * @param jsonStr
     * @return
     */
    private Set<Integer> convertJsonStrToDepInfo(String jsonStr) {

        SetupInfoTo setupInfoTo = converter.convertToEntityAttribute(jsonStr);

        if (setupInfoTo == null) {
            return Sets.newHashSet();
        }

        if (WkStringUtils.isEmpty(setupInfoTo.getDepartment())) {
            return Sets.newHashSet();
        }

        Set<Integer> depSids = Sets.newHashSet();

        for (String depSidStr : setupInfoTo.getDepartment()) {
            if (!WkStringUtils.isNumber(depSidStr)) {
                WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "orgsid 非數字!:[" + depSidStr + "]", 10);
                continue;
            }
            depSids.add(Integer.parseInt(depSidStr));

        }

        return depSids;
    }

}
