package com.cy.tech.request.logic.vo;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.logic.enumerate.BasicDataCategoryType;
import com.cy.tech.request.vo.anew.enums.SignLevelType;
import com.cy.tech.request.vo.enums.ReqCateType;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlList;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonManagedReference;

/**
 * 類別基本資料 <BR/>
 * 類型 | 中類 | 小類 <BR/>
 * 集合體
 *
 * @author shaun
 */
@Data
@ToString(exclude = {"childs"})
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid", "id", "type"})
public class BasicDataCategoryTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4783454637424622963L;
    @XmlID
    private String sid;
    private String id;
    private String name;
    private BasicDataCategoryType type;
    @XmlIDREF
    @JsonBackReference
    private BasicDataCategoryTo parent;
    private Boolean checkAttachment;
    private Activation status;
    private String note;
    @XmlIDREF
    @XmlList
    @JsonManagedReference
    private List<BasicDataCategoryTo> childs;
    private String tip;
    /** 可使用單位 */
    private JsonStringListTo canUseDepts = new JsonStringListTo();
    /** 需求分類型態 */
    private ReqCateType reqCateType = ReqCateType.EXTERNAL;
    /** 結案後是可執行on程式 */
    private Boolean whenCloseExeOnpg = Boolean.FALSE;
    /** 是否自動產生ON程式頁籤資訊 */
    private Boolean autoCreateOnpg = Boolean.FALSE;
    /** 權限對應 如果有多個角色字串以 '，' 分隔 */
    private String permissionRole;
    /** ON程式檢查完成可執行的角色 */
    private String execOnpgOkRole;
    /** 需求完成後是否自動結案 */
    private Boolean checkFinishAutoClosed = Boolean.FALSE;
    /** 需求單位簽核層級Type */
    private SignLevelType signLevel;
    /** 是否可顯示於案件單轉需求單選項 */
    private Boolean showItemInTc = Boolean.FALSE;
    /** 預設帶入天數  */
    private Integer defaultDueDays = 7;

    public void setParent(BasicDataCategoryTo parent) {
        this.parent = parent;
        if (parent == null) {
            return;
        }
        if (parent.getChilds() == null) {
            parent.setChilds(new ArrayList<BasicDataCategoryTo>());
        }
        if (!parent.getChilds().contains(this)) {
            parent.getChilds().add(this);
        }
    }

}
