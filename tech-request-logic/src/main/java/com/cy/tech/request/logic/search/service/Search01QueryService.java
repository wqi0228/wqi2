/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.tech.request.logic.callable.Search01ViewCallable;
import com.cy.tech.request.logic.search.view.Search01View;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * @author jason_h
 */
@Component("s01Query")
@Slf4j
public class Search01QueryService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -9083673114070876639L;
    @Autowired
    private URLService urlService;
    @Autowired
    private SearchService searchHelper;
    @PersistenceContext
    transient private EntityManager em;

    @SuppressWarnings("unchecked")
    public List<Search01View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {

        // ====================================
        // 查詢
        // ====================================

        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        rawParameters.stream().forEach(entry -> query.setParameter(entry.getKey(), entry.getValue()));

        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        List<Object[]> result = query.getResultList();
        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if (WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();

        // 取得自己以下單位
        Integer primaryOrgSid = WkUserUtils.findUserPrimaryOrgSid(execUserSid);
        Set<Integer> relationOrgSids = Sets.newHashSet(primaryOrgSid);
        // 管理單位
        Set<Integer> managerOrgSids = WkOrgCache.getInstance().findManagerOrgSids(execUserSid);
        if (WkStringUtils.notEmpty(managerOrgSids)) {
            relationOrgSids.addAll(managerOrgSids);
        }
        // 所以有以下子單位
        Set<Integer> allChildSids = WkOrgCache.getInstance().findAllChildSids(relationOrgSids);
        if (WkStringUtils.notEmpty(allChildSids)) {
            relationOrgSids.addAll(allChildSids);
        }

        List<Search01View> viewResult = Lists.newArrayList();
        int poolNum = 200;
        if (result.size() < poolNum) {
            poolNum = result.size();
        }
        List<String> sids = Lists.newArrayList();
        List<Search01ViewCallable> search01ViewCallables = Lists.newArrayList();
        try {
            ExecutorService pool = Executors.newFixedThreadPool(poolNum);
            try {
                @SuppressWarnings("rawtypes")
                CompletionService completionPool = new ExecutorCompletionService(pool);
                for (int i = 0; i < result.size(); i++) {
                    Object[] record = (Object[]) result.get(i);
                    String sid = (String) record[0];
                    if (sids.contains(sid)) {
                        continue;
                    }
                    sids.add(sid);
                    Search01ViewCallable sv = new Search01ViewCallable(
                            i, record, searchHelper,  urlService, execUserSid, relationOrgSids);
                    search01ViewCallables.add(sv);
                    completionPool.submit(sv);
                }
                IntStream.range(0, search01ViewCallables.size()).forEach(i -> {
                    try {
                        Object obj = completionPool.take().get();
                        if (obj == null) {
                            return;
                        }
                        Search01View search01View = (Search01View) obj;
                        viewResult.add(search01View);
                    } catch (Exception e) {
                        log.error("search01ViewCallables", e);
                    }
                });
            } catch (Exception e) {
                log.error("search01ViewCallables", e);
            } finally {
                pool.shutdown();
            }
        } catch (Exception e) {
            log.error("search01ViewCallables", e);
        }

        // 解析資料-結束
        usageRecord.parserDataEnd();

        return viewResult.stream()
                .sorted(Comparator.comparing(Search01View::getResultIndex))
                .collect(Collectors.toList());
    }

    /**
     * 批次查詢 （避免參數 List<T> 過多時發生 Exception）
     * 
     * @param <T>
     * @param sql
     * @param paramName
     * @param paramList
     * @param otherParameters
     * @return
     */
    public <T> List<String> querySingleResultByBatch(
            String sql,
            String paramName,
            List<T> paramList,
            Map<String, Object> otherParameters) {
        if (otherParameters == null) {
            otherParameters = Maps.newHashMap();
        }

        if (WkStringUtils.isEmpty(paramList)) {
            return Lists.newArrayList();
        }

        // ====================================
        // Query 參數容器
        // ====================================
        //
        Iterator<T> paramsIt = paramList.stream().collect(Collectors.toList()).iterator();
        // 單次查詢使用的參數
        List<T> currQryParams = Lists.newArrayList();
        // 所有的查詢結果 - 收集容器
        Set<String> resultSet = Sets.newHashSet();

        // 每 500 筆 commit 一次
        while (paramsIt.hasNext()) {
            // 收集單次查詢的參數
            currQryParams.add(paramsIt.next());

            if (currQryParams.size() >= 500) {
                otherParameters.put(paramName, currQryParams);

                List<String> currResults = this.findWithQueryForCache(
                        sql,
                        otherParameters);

                if (WkStringUtils.notEmpty(currResults)) {
                    resultSet.addAll(currResults);
                }
                // 清除, 重新收集
                currQryParams.clear();
            }
        }

        // 執行 while loop 最後一次剩下的
        if (currQryParams.size() > 0) {
            otherParameters.put(paramName, currQryParams);

            List<String> currResults = this.findWithQueryForCache(
                    sql,
                    otherParameters);

            if (WkStringUtils.notEmpty(currResults)) {
                resultSet.addAll(currResults);
            }
        }

        return Lists.newArrayList(resultSet);
    }

    @SuppressWarnings("unchecked")
    public List<String> findWithQueryForCache(String sql, Map<String, Object> parameters) {
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        rawParameters.stream().forEach(entry -> query.setParameter(entry.getKey(), entry.getValue()));
        return (List<String>) query.getResultList();
    }

}
