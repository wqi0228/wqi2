/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.sysbackend;

import com.cy.tech.request.logic.vo.SysMemInfoTo;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author shaun
 */
@Component
@Slf4j
public class SysMemInfoService implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -8566313608081933565L;

    private final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

	private final int MB = 1024 * 1024;
	/** 最大記錄數 */
	private final int MAX_REC = 180;// 3H

	/** 記憶體記錄 */
	@Getter
	private List<SysMemInfoTo> userdMemory;
	@Getter
	private List<SysMemInfoTo> freeMemory;
	@Getter
	private List<SysMemInfoTo> totalMemory;

	@PostConstruct
	void init() {
		userdMemory = Lists.newArrayList();
		freeMemory = Lists.newArrayList();
		totalMemory = Lists.newArrayList();
	}

	boolean isFirst_recSysinfo = true;
	/**
	 * 間隔時間 1 分鐘
	 */
	private final long recSysinfoFixedDelay = (1 * 60 * 1000);

	/**
	 * 系統資源記錄(1分鐘一次)
	 */
	@Scheduled(fixedDelay = recSysinfoFixedDelay)
	void recSysinfo() {
		Runtime runtime = Runtime.getRuntime();
		Date time = new Date();
		userdMemory.add(new SysMemInfoTo(sdf.format(time), (runtime.totalMemory() - runtime.freeMemory()) / MB));
		freeMemory.add(new SysMemInfoTo(sdf.format(time), runtime.freeMemory() / MB));
		totalMemory.add(new SysMemInfoTo(sdf.format(time), runtime.totalMemory() / MB));
		if (userdMemory.size() > MAX_REC) {
			userdMemory.remove(0);
			freeMemory.remove(0);
			totalMemory.remove(0);
		}

		// 為避免遺忘此排程，故第一次運作時，印出執行訊息
		if (isFirst_recSysinfo) {
			log.info(String.format(
			        "【排程】記錄系統資源for後台監控, 執行間隔時間=%s sec, ",
			        (this.recSysinfoFixedDelay / 1000)));
			isFirst_recSysinfo = false;
		}
	}

}
