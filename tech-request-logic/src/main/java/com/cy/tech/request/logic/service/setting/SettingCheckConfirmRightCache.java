package com.cy.tech.request.logic.service.setting;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.cy.tech.request.repository.setting.SettingCheckConfirmRightRepository;
import com.cy.tech.request.vo.constants.CacheConstants;
import com.cy.tech.request.vo.setting.SettingCheckConfirmRight;

/**
 * @author allen1214_wu 設定：可檢查確認權限
 */
@Service
public class SettingCheckConfirmRightCache {

	// ========================================================================
	// 服務區
	// ========================================================================
	@Autowired
	transient private SettingCheckConfirmRightRepository settingCheckConfirmRightRepository;

	// ========================================================================
	// 方法區
	// ========================================================================
	/**
	 * 查詢所有系統別設定資料
	 * 
	 * @return
	 */
	@Cacheable(cacheNames = CacheConstants.CACHE_SETTING_CHECK_CONFIRM_RIGHT)
	public List<SettingCheckConfirmRight> findAll() {
		return this.settingCheckConfirmRightRepository.findAll();
	}

	
}
