/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import com.cy.tech.request.vo.enums.OthSetStatus;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 其它資料設定頁面顯示vo (search19.xhtml)
 *
 * @author jason_h
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Search19View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1274081115925449373L;
    /** 其它設定填單日期 */
    private Date osCreateDt;
    /** 其它設定主題 */
    private String osTheme;
    /** 其它設定通知單位 */
    private JsonStringListTo osNoticeDeps;
    /** 其它設定狀態 */
    private OthSetStatus osStatus;
    /** 其它設定填單人員 */
    private Integer createdUser;
    /** 其它設定填單部門 */
    private Integer createDep;
    private String createDepName;
    /** 需求日期 */
    private Date reqCreateDt;
    /** 需求類別 */
    private String bigName;
    /** 中類 */
    private String middleName;
    /** 小類 */
    private String smallName;
    /** 需求單位 */
    private Integer requireDep;
    /** 需求人員 */
    private Integer requireUser;
    /** 需求單-緊急度 */
    private UrgencyType urgency;
    /** 本地端連結網址 */
    private String localUrlLink;
    /** 其他資料設定單號 */
    private String osno;


}
