/**
 * 
 */
package com.cy.tech.request.logic.service.syncmms.to;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.vo.WorkOnpgTo;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.log.LogSyncMmsActionType;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.client.syncmms.to.formdata.SyncMmsFormDataModel;
import com.cy.work.client.syncmms.to.processresult.SyncMmsProcessResultModel;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.customer.vo.WorkCustomer;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 */
public class SyncFormTo extends SyncMmsFormDataModel implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 4684287894861787485L;

    /**
     * 建構子
     * 
     * @param mmsFormDataModel
     */
    public SyncFormTo(SyncMmsFormDataModel mmsFormDataModel, String loginCompID) {
        BeanUtils.copyProperties(mmsFormDataModel, this);
    }

    /**
     * 檢核錯誤的訊息 list
     */
    private List<String> errorInfos = Lists.newArrayList();

    public void addErrorInfo(String errorInfo) {
        if (errorInfos == null) {
            errorInfos = Lists.newArrayList();
        }
        errorInfos.add(errorInfo);
    }

    public void addErrorInfo(int index, String errorInfo) {
        if (errorInfos == null) {
            errorInfos = Lists.newArrayList();
        }
        errorInfos.add(index, errorInfo);
    }

    public boolean isError() { return WkStringUtils.notEmpty(errorInfos); }

    /**
     * 取得錯誤訊息
     * 
     * @return
     */
    public String getErrorInfosStr(String splitStr) {
        if (WkStringUtils.isEmpty(this.errorInfos)) {
            return "";
        }
        return this.errorInfos.stream()
                .collect(Collectors.joining(splitStr));
    }

    /**
     * 立單人員 (由傳入的 G-mail 帳號解析)
     */
    @Setter
    @Getter
    private User createUser;

    /**
     * 需求單主檔
     */
    @Setter
    @Getter
    private Require require;

    /**
     * ON程式單主檔
     */
    @Setter
    @Getter
    private WorkOnpgTo workOnpgTo;

    /**
     * 廳主
     */
    @Setter
    @Getter
    private WorkCustomer workCustomer;

    /**
     * 系統別
     */
    @Setter
    @Getter
    private List<RequireCheckItemType> checkItemTypes;

    /**
     * 處理的動作類型
     */
    @Setter
    @Getter
    private LogSyncMmsActionType logSyncMmsActionType = LogSyncMmsActionType.NONE;

    @Setter
    @Getter
    private SyncMmsProcessResultModel processResult;

    /**
     * 異動欄位說明
     */
    @Setter
    @Getter
    private List<String> modifyInfos = Lists.newArrayList();

    public String getModifyInfosStr(String splitStr) {
        if (WkStringUtils.isEmpty(this.modifyInfos)) {
            return "";
        }
        return this.modifyInfos.stream()
                .collect(Collectors.joining(splitStr));
    }

    /**
     * 登入公司ID (借放)
     */
    @Getter
    private String loginCompID;

}
