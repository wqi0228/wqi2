/**
 * 
 */
package com.cy.tech.request.logic.service.onpg;

import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.onpg.to.Set10CountOnpgTo;
import com.cy.tech.request.logic.service.onpg.to.Set10RequireTo;
import com.cy.tech.request.repository.NewNativeSqlRepository;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.rbt.util.exceloperate.bean.expt.ColumnDataSet;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Slf4j
@Service
public class Set10CountOnpgService {

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private NewNativeSqlRepository nativeSqlRepository;

    // ========================================================================
    // 方法
    // ========================================================================

    public List<ColumnDataSet> processExecStats(Date conditionStartDate, Date conditionEndDate) {

        List<ColumnDataSet> detailDataSetList = Lists.newArrayList();

        // ====================================
        // 組SQL
        // ====================================
        Map<String, Object> parameters = Maps.newHashMap();

        StringBuffer sql = new StringBuffer();
        sql.append("SELECT tr.require_no            AS requireNo, ");
        sql.append("       tr.create_dt             AS createDate, ");
        sql.append("       tr.hope_dt               as hopeDate, ");
        sql.append("       tr.require_finish_dt     AS finishDate, ");
        sql.append("       tr.close_dt              AS closeDate, ");
        sql.append("       tr.require_status        AS requireStatus, ");
        sql.append("       tr.dep_sid               AS depSid ");
        sql.append("FROM   tr_require tr ");
        sql.append("WHERE  1 = 1 ");
        sql.append("       AND tr.require_status NOT IN( 'DRAFT', 'INVALID' ) ");

        // 查詢起日
        if (conditionStartDate == null) {
            sql.append("       AND (  ");
            sql.append("            (tr.create_dt < :conditionStartDate  AND tr.require_status IN( 'PROCESS')  ");
            sql.append("            OR tr.create_dt >= :conditionStartDate ");
            sql.append("       OR (  ");
            parameters.put("conditionStartDate", WkDateUtils.toStartDay(conditionStartDate));
        }

        // 查詢迄日
        if (conditionEndDate != null) {
            sql.append("       AND tr.create_dt <= :conditionEndDate ");
            parameters.put("conditionEndDate", WkDateUtils.toEndDay(conditionEndDate));
        }

        // ====================================
        // 查詢
        // ====================================
        long startTime = System.currentTimeMillis();
        List<Set10RequireTo> set10RequireVOs = this.nativeSqlRepository.getResultList(
                sql.toString(), parameters, Set10RequireTo.class);

        log.debug(WkCommonUtils.prepareCostMessage(startTime, "主查詢, 共:[" + set10RequireVOs.size() + "]筆"));

        if (WkStringUtils.isEmpty(set10RequireVOs)) {
            return detailDataSetList;
        }

        // set10RequireVOs = set10RequireVOs.stream()
        // .filter(vo -> WkOrgUtils.isActive(vo.getDepSid()))
        // .collect(Collectors.toList());

        WkDateUtils wkDateUtils = new WkDateUtils();
        for (Set10RequireTo set10RequireTo : set10RequireVOs) {
            if (set10RequireTo.getCreateDate() != null) {
                set10RequireTo.setCreateDate(wkDateUtils.transStartDate(set10RequireTo.getCreateDate()));
            }
            if (set10RequireTo.getHopeDate() != null) {
                set10RequireTo.setHopeDate(wkDateUtils.transStartDate(set10RequireTo.getHopeDate()));
            }
            if (set10RequireTo.getFinishDate() != null) {
                set10RequireTo.setFinishDate(wkDateUtils.transStartDate(set10RequireTo.getFinishDate()));
            }
            if (set10RequireTo.getCloseDate() != null) {
                set10RequireTo.setCloseDate(wkDateUtils.transStartDate(set10RequireTo.getCloseDate()));
            }
        }

        // ====================================
        // 兜組資料
        // ====================================
        Calendar calendar = Calendar.getInstance();
        Date startDate = wkDateUtils.transStartDate(conditionStartDate);
        Date endDate = wkDateUtils.transStartDate(conditionEndDate);
        Date currentDate = startDate;
        while (currentDate.before(endDate) || currentDate.equals(endDate)) {

            // log.debug("currentDate:" + WkDateUtils.formatDate(currentDate, WkDateUtils.YYYY_MM_DD));

            long currentDateTime = currentDate.getTime();

            // 於當日建立數量
            int createCount = 0;
            // 於當日完成數量
            int finishCount = 0;
            // 於當日結案數量
            int closeCount = 0;
            // 當日逾期未結案數量
            int overdueFinishCount = 0;
            // 當日逾期未結案數量
            int overdueCloseCount = 0;

            long untilFinishTotalMillies = 0;
            long untilCloseTotalMillies = 0;

            for (Set10RequireTo set10RequireTo : set10RequireVOs) {

                if (currentDate.equals(set10RequireTo.getCreateDate())) {
                    createCount += 1;
                }

                if (currentDate.equals(set10RequireTo.getFinishDate())) {
                    finishCount += 1;

                    if (set10RequireTo.getFinishDate().equals(set10RequireTo.getCreateDate())) {
                        untilFinishTotalMillies += 86400000;
                    } else {
                        untilFinishTotalMillies += set10RequireTo.getFinishDate().getTime() - set10RequireTo.getCreateDate().getTime();
                    }
                }

                if (currentDate.equals(set10RequireTo.getCloseDate())) {
                    closeCount += 1;

                    if (set10RequireTo.getCloseDate().equals(set10RequireTo.getCreateDate())) {
                        untilCloseTotalMillies += 86400000;
                    } else {
                        untilCloseTotalMillies += set10RequireTo.getCloseDate().getTime() - set10RequireTo.getCreateDate().getTime();
                    }
                }

                // 統計日當天大於期望日
                if (set10RequireTo.getHopeDate().getTime() <= currentDateTime) {

                    // 今日還未完工/結案，或完工/結案日期大於統計日期當天
                    if (set10RequireTo.getCloseDate() == null) {
                        if (set10RequireTo.getFinishDate() == null
                                || set10RequireTo.getFinishDate().getTime() > currentDateTime) {
                            overdueFinishCount += 1;
                        }
                    }

                    if (set10RequireTo.getCloseDate() == null
                            || set10RequireTo.getCloseDate().getTime() > currentDateTime) {
                        overdueCloseCount += 1;
                    }
                }
            }

            ColumnDataSet columnDataSet = new ColumnDataSet();
            detailDataSetList.add(columnDataSet);

            // 日期
            columnDataSet.setColumn("date", WkDateUtils.formatDate(currentDate, DateTimeFormat.forPattern("yyyy/MM/dd E")));

            // 當日新增數量
            columnDataSet.setColumn("createCount", createCount);
            // 於當日新增數量
            columnDataSet.setColumn("finishCount", finishCount);
            // 於當日結案數量
            columnDataSet.setColumn("closeCount", closeCount);
            // 當日逾期未完成數量
            columnDataSet.setColumn("overdueFinishCount", overdueFinishCount);
            // 當日逾期未結案數量
            columnDataSet.setColumn("overdueCloseCount", overdueCloseCount);

            // 當日完工案件平均執行天數
            if (untilFinishTotalMillies > 0) {

                long avgMillies = (long) (untilFinishTotalMillies / finishCount);

                long daysBetween = TimeUnit.DAYS.convert(avgMillies, TimeUnit.MILLISECONDS);
                if (daysBetween == 0) {
                    daysBetween = 1;
                }

                columnDataSet.setColumn("untilFinishTotalExecDays", daysBetween);
            }
            // 當日結案案件平均執行天數
            if (untilCloseTotalMillies > 0) {

                long avgMillies = (long) (untilCloseTotalMillies / closeCount);

                long daysBetween = TimeUnit.DAYS.convert(avgMillies, TimeUnit.MILLISECONDS);
                if (daysBetween == 0) {
                    daysBetween = 1;
                }

                columnDataSet.setColumn("untilCloseTotalExecDays", daysBetween);
            }

            calendar.setTime(currentDate);
            calendar.add(Calendar.DATE, 1);
            currentDate = calendar.getTime();
        }

        return detailDataSetList;
    }

    public ColumnDataSet process(Date conditionStartDate, Date conditionEndDate) {

        // ====================================
        // 組SQL
        // ====================================

        Map<String, Object> parameters = Maps.newHashMap();

        StringBuffer sql = new StringBuffer();
        sql.append("SELECT onpg.onpg_source_no   AS requireNo, ");
        sql.append("       onpg.onpg_source_sid  AS requireSid, ");
        sql.append("       tr.require_status     AS requireStatus, ");
        sql.append("       onpg.onpg_sid         AS onpgSid, ");
        sql.append("       onpg.onpg_no          AS onpgNo, ");
        sql.append("       onpg.onpg_theme       AS onpgTheme, ");
        sql.append("       onpg.onpg_status      AS onpgStatus, ");
        sql.append("       onpg.dep_sid          AS depSid, ");
        sql.append("       onpg.create_usr       AS createUserSid, ");
        sql.append("       onpg.onpg_estimate_dt AS estimateDate ");

        sql.append("FROM   work_onpg onpg ");
        sql.append("       INNER JOIN tr_require tr ");
        sql.append("               ON tr.require_sid = onpg.onpg_source_sid ");
        sql.append("                  AND tr.status = 0 ");
        sql.append("                  AND tr.require_status NOT IN ( 'INVALID', 'SUSPENDED' ) ");
        sql.append("WHERE  onpg.status = 0 ");
        sql.append("       AND onpg.onpg_status != 'CANCEL_ONPG'");

        // 查詢起日
        if (conditionStartDate != null) {
            sql.append("       AND onpg.onpg_estimate_dt >= :conditionStartDate ");
            parameters.put("conditionStartDate", WkDateUtils.toStartDay(conditionStartDate));
        }

        // 查詢迄日
        if (conditionEndDate != null) {
            sql.append("       AND onpg.onpg_estimate_dt <= :conditionEndDate ");
            parameters.put("conditionEndDate", WkDateUtils.toEndDay(conditionEndDate));
        }

        // ====================================
        // 查詢
        // ====================================
        long startTime = System.currentTimeMillis();
        List<Set10CountOnpgTo> set10CountOnpgTos = this.nativeSqlRepository.getResultList(
                sql.toString(), parameters, Set10CountOnpgTo.class);

        log.debug(WkCommonUtils.prepareCostMessage(startTime, "主查詢, 共:[" + set10CountOnpgTos.size() + "]筆"));

        if (WkStringUtils.isEmpty(set10CountOnpgTos)) {
            return null;
        }

        // ====================================
        // 兜組資料
        // ====================================
        ColumnDataSet columnDataSet = new ColumnDataSet();

        List<RequireStatusType> closeStatus = Lists.newArrayList(
                RequireStatusType.CLOSE, RequireStatusType.AUTO_CLOSED);

        for (Set10CountOnpgTo countOnpgTo : set10CountOnpgTos) {

            CountOnpgResultType countOnpgResultType = CountOnpgResultType.NONE;

            switch (countOnpgTo.getOnpgStatus()) {
            case CHECK_COMPLETE:
            case UNIT_FORCE_CLOSE:
                // 一般檢查完成，或立案單位自己強制結案
                countOnpgResultType = CountOnpgResultType.CLOSE;
                break;
            case FORCE_CLOSE:
                // 一般檢查完成，或立案單位自己強制結案
                countOnpgResultType = CountOnpgResultType.FORCE_CLOSE;
                break;
            case ALREADY_ON:
            case CHECK_REPLY:

                countOnpgResultType = CountOnpgResultType.PROCESS;

                // 逾期未結
                if (WkDateUtils.isAfter(new Date(), countOnpgTo.getEstimateDate(), true)) {

                    if (closeStatus.contains(countOnpgTo.getRequireStatus())) {
                        // 已結案
                        countOnpgResultType = CountOnpgResultType.CLOSE_OVERDUE;
                    } else {

                        if (WorkOnpgStatus.ALREADY_ON.equals(countOnpgTo.getOnpgStatus())) {
                            // 未檢查
                            countOnpgResultType = CountOnpgResultType.UNCHECK_OVERDUE;
                        } else {
                            // 已檢查
                            countOnpgResultType = CountOnpgResultType.CHECKED_OVERDUE;
                        }
                    }
                }
                break;
            default:
                countOnpgResultType = CountOnpgResultType.NONE;
            }

            this.prepareDep(columnDataSet, OrgLevel.GROUPS, countOnpgTo, countOnpgResultType);
        }

        // ====================================
        // 統計 & 排序
        // ====================================
        // 取得部門排序
        Map<Integer, Integer> sortNoMapByDepSid = WkOrgCache.getInstance().findOrgSortNumberMapByDepSid();

        this.recursionPrepareDataInfo(columnDataSet, sortNoMapByDepSid);

        return columnDataSet;
    }

    private void recursionPrepareDataInfo(ColumnDataSet columnDataSet, final Map<Integer, Integer> sortNoMapByDepSid) {

        if (columnDataSet == null) {
            return;
        }

        // ====================================
        // 統計
        // ====================================
        List<String> infos = Lists.newArrayList();
        for (CountOnpgResultType countOnpgResultType : CountOnpgResultType.values()) {
            if (columnDataSet.getColumn(countOnpgResultType.name()) != null) {
                infos.add(countOnpgResultType.descr + ":" + columnDataSet.getColumn(countOnpgResultType.name()));
            }
        }

        columnDataSet.setColumn("info", infos.stream().collect(Collectors.joining("\r\n")));

        // ====================================
        // 取得要排序的資料
        // ====================================
        List<ColumnDataSet> columnDataSets = columnDataSet.getArray("DETAIL");
        if (columnDataSets == null) {
            return;
        }

        // ====================================
        // 排序
        // ====================================

        if ("dep".equals(columnDataSet.getColumn("type"))) {

            // 部門排序器
            Comparator<ColumnDataSet> depComparator = Comparator.comparing(
                    dataSet -> {
                        String sid = dataSet.getColumn("sid") + "";
                        if (WkStringUtils.isEmpty(sid)
                                || !WkStringUtils.isNumber(sid)) {
                            return -1;
                        }

                        Integer orgSid = Integer.parseInt(sid);
                        if (!sortNoMapByDepSid.containsKey(orgSid)) {
                            return -1;
                        }
                        return sortNoMapByDepSid.get(orgSid);
                    });

            columnDataSets = columnDataSets.stream()
                    .sorted(depComparator)
                    .collect(Collectors.toList());

        } else {
            // user 排序器
            Comparator<ColumnDataSet> userComparator = Comparator.comparing(
                    dataSet -> {
                        String sid = dataSet.getColumn("sid") + "";
                        if (WkStringUtils.isEmpty(sid)
                                || !WkStringUtils.isNumber(sid)) {
                            return false;
                        }
                        return WkUserUtils.isActive(Integer.parseInt(sid));
                    });

            userComparator = userComparator.thenComparing(Comparator.comparing(
                    dataSet -> {
                        String sid = dataSet.getColumn("sid") + "";
                        if (WkStringUtils.isEmpty(sid)
                                || !WkStringUtils.isNumber(sid)) {
                            return "";
                        }

                        return WkUserUtils.findNameBySid(Integer.parseInt(sid));
                    }));

            columnDataSets = columnDataSets.stream()
                    .sorted(userComparator)
                    .collect(Collectors.toList());
        }

        columnDataSet.setArrayColumnDataSet("DETAIL", columnDataSets);

        // ====================================
        // 遞迴處理下層
        // ====================================
        for (ColumnDataSet childColumnDataSet : columnDataSets) {
            this.recursionPrepareDataInfo(childColumnDataSet, sortNoMapByDepSid);
        }

    }

    public enum CountOnpgResultType {
        PROCESS("進行中"),
        CLOSE("已結案"),
        FORCE_CLOSE("需求方強制結案"),
        CHECKED_OVERDUE("逾期未結案(已檢查)"),
        UNCHECK_OVERDUE("逾期未結案(未檢查)"),
        CLOSE_OVERDUE("逾期未結案(主單已結案)"),
        NONE("狀態不明");

        @Getter
        private String descr;

        private CountOnpgResultType(
                String descr) {
            this.descr = descr;
        }

    }

    private OrgLevel prepareNextLevel(OrgLevel targetOrgLevel) {
        if (OrgLevel.GROUPS.equals(targetOrgLevel)) {
            return OrgLevel.DIVISION_LEVEL;
        } else if (OrgLevel.DIVISION_LEVEL.equals(targetOrgLevel)) {
            return OrgLevel.MINISTERIAL;
        } else if (OrgLevel.MINISTERIAL.equals(targetOrgLevel)) {
            return OrgLevel.THE_PANEL;
        }
        // 『組』下一層為員工 （回傳null）
        return null;

    }

    private void prepareDep(
            ColumnDataSet parentColumnDataSet,
            OrgLevel targetOrgLevel,
            Set10CountOnpgTo countOnpgTo,
            CountOnpgResultType countOnpgResultType) {

        parentColumnDataSet.setColumn("type", "dep");

        // ----------------------
        // 取得建立單位
        // ----------------------
        Org org = WkOrgCache.getInstance().findBySid(countOnpgTo.getDepSid());
        if (org == null) {
            log.warn("找不到單據建立單位資料! depSid:[{}], onpgNo:[{}] ",
                    countOnpgTo.getDepSid(),
                    countOnpgTo.getOnpgNo());
            return;
        }

        // ----------------------
        // 取得資料
        // ----------------------
        List<ColumnDataSet> columnDataSets = parentColumnDataSet.getArray("DETAIL");
        if (columnDataSets == null) {
            columnDataSets = Lists.newArrayList();
            parentColumnDataSet.setArrayColumnDataSet("DETAIL", columnDataSets);
        }

        // ----------------------
        // 往上取得當下層級的單位
        // ----------------------
        // 例如目標層級『處』
        // 傳入為應用系統組，會回傳E化發展處
        // 傳入總管理處(群級)，會回傳總管理處 (本身比目標大，回傳自己)
        Org uplevelOrg = WkOrgUtils.prepareBaseDep(org.getSid(), targetOrgLevel);

        // 目標級別
        Integer targetOrgLevelIndex = WkOrgUtils.getOrgLevelOrder(targetOrgLevel);
        //
        Integer upOrgLevelIndex = WkOrgUtils.getOrgLevelOrder(uplevelOrg.getLevel());

        // ----------------------
        // 塞本層資料
        // ----------------------
        // 建立層級資料，並加總數量
        String sid = "";
        // up org 等於所要的層級
        if (upOrgLevelIndex == targetOrgLevelIndex) {
            sid = uplevelOrg.getSid() + "";
        }
        // 單位層級缺損 例如 組->處 (缺中間單位)
        else if (upOrgLevelIndex < targetOrgLevelIndex) {
            sid = "--";
        }
        // 傳入單位大於指定層級：單據為上層單位開的
        else if (upOrgLevelIndex > targetOrgLevelIndex) {
            sid = "--";
        }

        // 還不存在時，初始化
        ColumnDataSet columnDataSet = this.findColumnDataSetBySid(columnDataSets, sid);
        if (columnDataSet == null) {
            columnDataSet = new ColumnDataSet();
            columnDataSets.add(columnDataSet);
            columnDataSet.setColumn("sid", sid);
            String orgName = "--";
            if (!"--".equals(sid)) {
                orgName = WkOrgUtils.getOrgName(uplevelOrg);
                if (!WkOrgUtils.isActive(uplevelOrg)) {
                    orgName += " (停用)";
                }
            }
            columnDataSet.setColumn("name", orgName);
        }

        // 塞數量 count + 1 by CountOnpgResultType
        int count = 1;
        if (columnDataSet.getColumn(countOnpgResultType.name()) != null) {
            count = (Integer) columnDataSet.getColumn(countOnpgResultType.name()) + 1;
        }
        columnDataSet.setColumn(countOnpgResultType.name(), count);

        // ----------------------
        // 遞迴
        // ----------------------
        OrgLevel nextOrgLevel = this.prepareNextLevel(targetOrgLevel);

        // 成員
        if (nextOrgLevel == null) {
            this.prepareMember(columnDataSet, nextOrgLevel, countOnpgTo, countOnpgResultType);
        }
        // 部門
        else {
            this.prepareDep(columnDataSet, nextOrgLevel, countOnpgTo, countOnpgResultType);
        }
    }

    private void prepareMember(
            ColumnDataSet parentColumnDataSet,
            OrgLevel targetOrgLevel,
            Set10CountOnpgTo countOnpgTo,
            CountOnpgResultType countOnpgResultType) {

        parentColumnDataSet.setColumn("type", "user");
        // ----------------------
        // 取得建立單據者
        // ----------------------
        User user = WkUserCache.getInstance().findBySid(countOnpgTo.getCreateUserSid());
        if (user == null) {
            log.warn("找不到單據建立者資料! userSid:[{}], onpgNo:[{}] ",
                    countOnpgTo.getCreateUserSid(),
                    countOnpgTo.getOnpgNo());
            return;
        }

        // ----------------------
        // 取得資料
        // ----------------------
        List<ColumnDataSet> columnDataSets = parentColumnDataSet.getArray("DETAIL");
        if (columnDataSets == null) {
            columnDataSets = Lists.newArrayList();
            parentColumnDataSet.setArrayColumnDataSet("DETAIL", columnDataSets);
        }

        // ----------------------
        // 由成員 list 中取得此成員 data set
        // ----------------------
        ColumnDataSet columnDataSet = this.findColumnDataSetBySid(columnDataSets, countOnpgTo.getCreateUserSid() + "");

        // ----------------------
        // 當成員 data set 還不存在時，建立一筆新的
        // ----------------------
        // 1.user sid
        // 2.user info
        if (columnDataSet == null) {
            columnDataSet = new ColumnDataSet();
            columnDataSets.add(columnDataSet);
            columnDataSet.setColumn("sid", countOnpgTo.getCreateUserSid() + "");
            String userName = WkUserUtils.safetyGetName(user);
            if (!WkUserUtils.isActive(user)) {
                userName += " (停用)";
            } else if (!WkCommonUtils.compareByStr(countOnpgTo.getDepSid(), user.getPrimaryOrg().getSid())) {
                userName += " (已轉調部門)";
            }
            columnDataSet.setColumn("name", userName);
        }

        // ----------------------
        // 塞數量 count + 1 by CountOnpgResultType
        // ----------------------
        int count = 1;
        if (columnDataSet.getColumn(countOnpgResultType.name()) != null) {
            count = (Integer) columnDataSet.getColumn(countOnpgResultType.name()) + 1;
        }
        columnDataSet.setColumn(countOnpgResultType.name(), count);

    }

    private ColumnDataSet findColumnDataSetBySid(List<ColumnDataSet> columnDataSets, String sid) {
        for (ColumnDataSet currColumnDataSet : columnDataSets) {
            if (WkCommonUtils.compareByStr(currColumnDataSet.getColumn("sid"), sid)) {
                return currColumnDataSet;
            }
        }
        return null;
    }

    // ========================================================================
    //
    // ========================================================================
    public void prepareAA() {

        // ====================================
        //
        // ====================================

        StringBuffer varname1 = new StringBuffer();
        varname1.append("SELECT tr.require_sid          AS requireSid, ");
        varname1.append("       tr.require_no           AS requireNo, ");
        varname1.append("       dic.field_content       AS themeJsonStr, ");
        varname1.append("       tr.require_establish_dt AS establishDate, ");
        varname1.append("       dep.dep_sid             AS depSid, ");
        varname1.append("       dep.owner_sid           AS ownerSid ");
        varname1.append("FROM   tr_require_confirm_dep dep ");
        varname1.append("       INNER JOIN tr_require tr ");
        varname1.append("               ON tr.require_sid = dep.require_sid ");
        varname1.append("                  AND tr.require_status = 'PROCESS' ");
        varname1.append("                  AND tr.status = 0 ");
        varname1.append("       LEFT JOIN tr_index_dictionary dic ");
        varname1.append("              ON dic.require_sid = tr.require_sid ");
        varname1.append("                 AND dic.field_name = '主題' ");
        varname1.append("WHERE  dep.prog_status IN ( 'WAIT_CONFIRM', 'IN_PROCESS' )");
    }

}
