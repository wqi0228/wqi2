/**
 * 
 */
package com.cy.tech.request.logic.service.setting.onetimetrns.to;

import java.util.Set;

import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 */
public class RequireReAssignTo {

    /**
     * 主單sid
     */
    @Getter
    @Setter
    private String requireSid;

    /**
     * 
     */
    @Getter
    @Setter
    private String requireNo;

    @Getter
    @Setter
    private Set<Integer> beforAssignDepSids;
    @Getter
    @Setter
    private Set<Integer> afterAssignDepSids;

    @Getter
    @Setter
    private Set<Integer> beforNoticeDepSids;
    @Getter
    @Setter
    private Set<Integer> afterNoticeDepSids;

}
