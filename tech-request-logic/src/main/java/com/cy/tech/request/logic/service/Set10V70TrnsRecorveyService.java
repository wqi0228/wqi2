package com.cy.tech.request.logic.service;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.repository.require.TrnsBackupRepository;
import com.cy.tech.request.vo.enums.TrnsType;
import com.cy.tech.request.vo.require.AssignSendInfoNativeTo;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.TrnsBackup;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class Set10V70TrnsRecorveyService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4798357926394463174L;
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private transient JdbcTemplate jdbcTemplate;
    @Autowired
    private transient EntityManager entityManager;
    @Autowired
    private transient NativeSqlRepository nativeSqlRepository;

    @Autowired
    private transient Set10V70TrneBatchHelper set10V70TrneBatchHelper;

    @Autowired
    private transient TrnsBackupRepository trnsBackupRepository;

    /**
     * 以條件查詢需要處理的需求單資料
     * 
     * @param requireNos 條件:需求單號
     * @param minNo      條件:最小需求單號
     * @param maxNo      條件:最大需求單號
     * @return Map<RequireNo , RequireSid>
     * @throws UserMessageException 傳入條件錯誤時拋出
     */
    public Map<String, String> queryRequireNoSidMap(List<String> requireNos, String minNo, String maxNo) throws UserMessageException {

        // ====================================
        // 輸入檢查
        // ====================================
        if (WkStringUtils.isEmpty(requireNos)
                && WkStringUtils.isEmpty(minNo)
                && WkStringUtils.isEmpty(maxNo)) {
            throw new UserMessageException("未輸入復原條件", InfomationLevel.WARN);
        }

        Long startTime = System.currentTimeMillis();
        String procTitle = "query RequireNoSidMap ";
        log.info(WkCommonUtils.prepareCostMessageStart(procTitle));

        // ====================================
        // 兜組 SQL 和查詢參數
        // ====================================
        Map<String, Object> parameters = Maps.newHashMap();

        // SQL
        StringBuffer varname1 = new StringBuffer();
        varname1.append("SELECT tr.require_no  as requireNo, ");
        varname1.append("       tr.require_sid as sid ");
        varname1.append("  FROM tr_require tr ");
        varname1.append(" WHERE tr.v70_trns_flag = 1 ");

        if (WkStringUtils.notEmpty(requireNos)) {
            varname1.append("   AND require_no IN (:requireNos)");
            parameters.put("requireNos", requireNos);
        }

        if (WkStringUtils.notEmpty(minNo)) {
            varname1.append("   AND require_no >= :minNo ");
            parameters.put("minNo", minNo);
        }

        if (WkStringUtils.notEmpty(maxNo)) {
            varname1.append("   AND require_no <= :maxNo ");
            parameters.put("maxNo", maxNo);
        }

        varname1.append(" ORDER BY require_no ");

        // ====================================
        // 查詢
        // ====================================
        // 查詢
        List<Require> results = nativeSqlRepository.getResultList(
                varname1.toString(), parameters, Require.class);

        log.info(WkCommonUtils.prepareCostMessage(startTime, procTitle + "[" + results.size() + "]筆"));

        if (WkStringUtils.isEmpty(results)) {
            return Maps.newHashMap();
        }

        // 轉為 Map<RequireNo , RequireSid>
        return results.stream()
                .collect(Collectors.toMap(
                        Require::getRequireNo,
                        Require::getSid));

    }

    /**
     * 還原
     * 
     * @throws IOException
     */
    @Transactional(rollbackFor = Exception.class)
    public void recorvey(Map<String, String> requireNoSidMap) throws IOException {

        List<String> requireNos = Lists.newArrayList(requireNoSidMap.keySet());
        Set<String> requireSids = Sets.newHashSet(requireNoSidMap.values());

        // ====================================
        // 還原 tr_assign_send_info
        // ====================================
        this.recorveyAssignSendInfo(requireNos);

        // ====================================
        // DELETE tr_assign_send_info_history
        // ====================================
        this.set10V70TrneBatchHelper.batchDeleteByColumn("tr_assign_send_info_history", "require_sid", requireSids);

        // ====================================
        // 無須還原 tr_assign_send_search_info
        // ====================================
        // 無須還原

        // ====================================
        // DELETE tr_require_confirm_dep
        // ====================================
        this.set10V70TrneBatchHelper.batchDeleteByColumn("tr_require_confirm_dep", "require_sid", requireSids);

        // ====================================
        // DELETE tr_require_confirm_dep_history
        // ====================================
        this.set10V70TrneBatchHelper.batchDeleteByColumn("tr_require_confirm_dep_history", "require_sid", requireSids);

        // ====================================
        // update tr_require.v70_trns_flag 為 false
        // ====================================
        this.set10V70TrneBatchHelper.updateRequire(
                Sets.newHashSet(requireNos), false);

        // ====================================
        // 清掉 hibernate 暫存
        // ====================================
        this.entityManager.clear();
    }

    /**
     * @param requireNos
     * @throws IOException
     */
    @Transactional(rollbackFor = Exception.class)
    public void recorveyAssignSendInfo(List<String> requireNos) throws IOException {

        requireNos = requireNos.stream().sorted().collect(Collectors.toList());
        log.debug("\r\n" + String.join("、", requireNos));
        
        // ====================================
        // 查詢要還原的 tr_assign_send_info
        // ====================================
        Long startTime = System.currentTimeMillis();
        String procTitle = "QUERY TrnsBackup";
        log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));
        List<TrnsBackup> trnsBackups = this.trnsBackupRepository.findByTrnsTypeAndCustKeyIn(TrnsType.ASSIGN_SEND_INFO_TO_HISTORY.name(), requireNos);
        log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle + "[" + trnsBackups.size() + "]筆"));

        if (WkStringUtils.isEmpty(trnsBackups)) {
            log.info("沒有需要還原的 tr_assign_send_info ");
            return;
        }

        // ====================================
        // 收集要還原的資料
        // ====================================
        Set<AssignSendInfoNativeTo> assignSendInfoNativeTos = Sets.newHashSet();

        for (TrnsBackup trnsBackup : trnsBackups) {
            String json = trnsBackup.getBackData();
            if (WkStringUtils.isEmpty(json)) {
                continue;
            }

            @SuppressWarnings("unchecked")
            List<AssignSendInfoNativeTo> currAssignSendInfoNativeTos = (List<AssignSendInfoNativeTo>) WkJsonUtils.getInstance().fromJsonByGson(json,
                    new TypeToken<List<AssignSendInfoNativeTo>>() {
                    }.getType());

            if (WkStringUtils.notEmpty(currAssignSendInfoNativeTos)) {
                assignSendInfoNativeTos.addAll(currAssignSendInfoNativeTos);
            }
        }

        // ====================================
        // INSERT tr_assign_send_info
        // ====================================
        this.set10V70TrneBatchHelper.batchInsertAssignSendInfo(Lists.newArrayList(assignSendInfoNativeTos));

    }

}
