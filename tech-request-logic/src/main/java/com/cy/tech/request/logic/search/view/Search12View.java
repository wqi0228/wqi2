/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import com.cy.work.common.enums.UrgencyType;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 收費金額一覽表頁面顯示vo (search12.xhtml)
 *
 * @author jason_h
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Search12View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4640353330129284208L;
    /** 緊急度 */
    private UrgencyType urgency;
    /** 立單日期 */
    private Date createdDate;
    /** 需求類別 */
    private String bigName;
    /** 中類 */
    private String middleName;
    /** 小類" */
    private String smallName;
    /** 需求單位 */
    private Integer createDep;
    /** 需求人員 */
    private Integer createdUser;
    /** 收費金額 */
    private BigDecimal cash;
    /** 廳主 */
    private Long customer;
    /** 客戶 */
    private Long author;
    /** 本地端連結網址 */
    private String localUrlLink;
}
