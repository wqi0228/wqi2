/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author shaun
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class LinkGroupDetailView extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4346058572033984661L;
    /** 本地端連結網址 */
    private String localUrlLink;
}
