/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author saul_chen
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {"sid"})
public class Search31View extends BaseSearchView implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -7137619410461214463L;
    /** vo sid */
    private String sid;
    /** 需求單號 */
    private String requireNo;
    /** 需求單主題 */
    private String requireTheme;
    /** 提出日期 */
    private String osCreateDt;
    /** 其它設定主題 */
    private String osTheme;
    /** 其它設定狀態 */
    private String osStatus;
    /** 其它設定填單人員 */
    private String createdUser;
    /** 其它設定填單部門 */
    private String createDep;
    /** 立單日期 */
    private String reqCreateDt;
    /** 需求類別 */
    private String bigName;
    /** 中類 */
    private String middleName;
    /** 小類 */
    private String smallName;
    /** 需求單位 */
    private String requireDep;
    /** 需求人員 */
    private String requireUser;
    /** 需求單-緊急度 */
    private String urgency;

    /** 本地端連結網址 */
    private String localUrlLink;
}
