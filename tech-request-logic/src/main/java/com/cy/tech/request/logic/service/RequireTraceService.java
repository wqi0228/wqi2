/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.HtmlUtils;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.anew.config.ReqLogicConstants;
import com.cy.tech.request.logic.service.syncmms.to.SyncFormTo;
import com.cy.tech.request.logic.utils.DateUtils;
import com.cy.tech.request.repository.require.RequireTraceRepository;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireTrace;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.mapp.create.trans.vo.MappCreateTrans;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.primitives.Longs;

import lombok.extern.slf4j.Slf4j;

/**
 * 需求單追蹤記錄輔助
 *
 * @author shaun
 */
@Slf4j
@Component
public class RequireTraceService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1137266581033487544L;
    @Autowired
    private transient WkJsoupUtils jsoupUtils;
    @Autowired
    private transient RequireTraceRepository dao;
    @Autowired
    private transient ReqLogicConstants logicConstants;
    @Autowired
    private transient ReqUnitBpmService reqUnitBpmService;
    @Autowired
    private transient RequireService requireService;

    public RequireTrace findOne(String sid) {
        return dao.findOne(sid);
    }

    // @Transactional(readOnly = true)
    public List<RequireTrace> findByRequire(Require require) {
        List<RequireTraceType> types = Lists.newArrayList(RequireTraceType.values());
        return dao.findTraceByRequireFilterTraceStatus(require, types, Activation.ACTIVE);
    }

    public List<RequireTrace> findTraceByTraceType(Require require, RequireTraceType type) {
        return dao.findTraceByTraceType(require, type, Activation.ACTIVE);
    }

    public Integer countTraceByRequireAndTraceTypeAndStatus(Require require, RequireTraceType type) {
        return dao.countTraceByRequireAndTraceTypeAndStatus(require, type, Activation.ACTIVE);
    }

    /**
     * 重新排序追蹤
     *
     * @param traces
     */
    public void sortTrace(List<RequireTrace> traces) {
        Ordering<RequireTrace> byUpDateOrdering = new Ordering<RequireTrace>() {
            @Override
            public int compare(RequireTrace left, RequireTrace right) {
                return Longs.compare(right.getCreatedDate().getTime(), left.getCreatedDate().getTime());
            }
        };
        try {
            Collections.sort(traces, byUpDateOrdering);
        } catch (ConcurrentModificationException e) {
            log.debug(e.getMessage(), e);
        }
    }

    /**
     * 建立新需求單追蹤
     *
     * @param require
     * @param executor
     * @return
     */
    public RequireTrace createNewTrace(String requireSid, User executor) {
        return createNewTrace(requireSid, executor, new Date());
    }

    /**
     * @param require
     * @param executor
     * @param sysDate
     * @return
     */
    public RequireTrace createNewTrace(String requireSid, User executor, Date sysDate) {
        Require require = this.requireService.findByReqSid(requireSid);

        RequireTrace trace = new RequireTrace();
        trace.setCreatedDate(sysDate);
        trace.setCreatedUser(executor);
        trace.setRequire(require);
        trace.setRequireNo(require.getRequireNo());
        return trace;
    }

    /**
     * only save
     *
     * @param trace
     * @return
     */
    public RequireTrace save(RequireTrace trace) {
        return dao.save(trace);
    }

    /**
     * 建立需求成立 追蹤紀錄資訊<BR/>
     * 中類無需【需求主管】簽核流程時，建立追蹤者為需求單建立者<BR/>
     * 中類需要【需求主管】簽核流程時 ，建立追蹤者為簽核主管(BPM 流程觸發核准時，建立追蹤)
     * 
     * @param requireSid
     * @param executor
     * @param execDate
     * @return
     */
    public RequireTrace createRequireEstablishTrace(String requireSid, User executor, Date execDate) {
        RequireTrace trace = this.createNewTrace(requireSid, executor, execDate);
        trace.setRequireTraceType(RequireTraceType.REQUIRE_ESTABLISH);
        trace.setRequireTraceContent("需求成立");// 追蹤內容改為「需求成立」Jason 7/16
        trace = dao.save(trace);
        return trace;
    }

    /**
     * 建立需求作廢 追蹤紀錄資訊<BR/>
     *
     * @param require
     * @param executor
     * @return
     */
    public RequireTrace createRequireInvaildTrace(String requireSid, User executor) {
        RequireTrace trace = this.createNewTrace(requireSid, executor);
        trace.setRequireTraceType(RequireTraceType.REQUIRE_DOC_INVALID);
        trace.setRequireTraceContent("需求單作廢");
        trace = dao.save(trace);
        return trace;
    }

    /**
     * @param requireSid
     * @param requireNo
     * @param requireTraceType
     * @param requireTraceContent
     * @param executorSid
     * @param sysDate
     */
    public void insertTrace(
            String requireSid,
            String requireNo,
            RequireTraceType requireTraceType,
            String requireTraceContent,
            Integer executorSid,
            Date sysDate) {

        RequireTrace trace = new RequireTrace();
        trace.setCreatedDate(sysDate);
        trace.setCreatedUser(WkUserCache.getInstance().findBySid(executorSid));
        trace.setRequire(this.requireService.findByReqSid(requireSid));
        trace.setRequireNo(requireNo);
        trace.setRequireTraceType(requireTraceType);
        trace.setRequireTraceContent(requireTraceContent);

        this.dao.save(trace);
    }

    /**
     * 建立原型確認作廢 追蹤記錄資訊
     *
     * @param require
     * @param testInfo
     * @param executor
     * @param invaildTraceReason
     * @return
     */
    public RequireTrace createPtProcessInvaildTrace(
            String requireSid,
            PtCheck testInfo,
            User executor,
            String invaildTraceReason) {

        RequireTrace trace = this.createNewTrace(requireSid, executor);
        trace.setRequireTraceType(RequireTraceType.PROTOTYPE_CONFIRM_VERIFY_INVAILD);
        trace.setRequireTraceContent("原型確認版本：" + testInfo.getVersion() + " - 原型確認審核作廢\r\n" + invaildTraceReason);
        trace.setReason("原型確認版本：" + testInfo.getVersion() + " - " + invaildTraceReason);
        trace = dao.save(trace);
        return trace;
    }

    public RequireTrace createPtRedoTrace(String requireSid, User executor, String contentCss) {
        RequireTrace trace = this.createNewTrace(requireSid, executor);
        trace.setRequireTraceType(RequireTraceType.PROTOTYPE_CONFIRM_REDO);
        trace.setRequireTraceContentCss(contentCss);
        trace.setRequireTraceContent(jsoupUtils.clearCssTag(contentCss));
        trace = dao.save(trace);
        trace.setCreatedUser(executor);
        return trace;
    }

    /**
     * 建立原型確認 - 功能符合需求記錄資訊
     *
     * @param require
     * @param executor
     * @return
     */
    public RequireTrace createPtFunctionConformTrace(String requireSid, User executor) {
        RequireTrace trace = this.createNewTrace(requireSid, executor);
        trace.setRequireTraceType(RequireTraceType.REQUIRE_FUNCTION_MATCH);
        trace.setRequireTraceContent("功能符合需求");
        trace = dao.save(trace);
        return trace;
    }

    /**
     * 建立重做追蹤物件
     *
     * @param check
     * @param executor
     * @return
     */
    public RequireTrace createEmptyRedoTrace(String requireSid, User executor) {
        RequireTrace trace = this.createNewTrace(requireSid, executor);
        trace.setRequireTraceType(RequireTraceType.PROTOTYPE_CONFIRM_REDO);
        return trace;
    }

    /**
     * 建立需求單回覆<BR/>
     * 前端先初始化後，才進行儲存
     *
     * @param require
     * @param executor
     * @return
     */
    public RequireTrace initRequireReplyTrace(String requireSid, User executor) {
        RequireTrace trace = this.createNewTrace(requireSid, executor);
        trace.setRequireTraceType(RequireTraceType.REQUIRE_INFO_MEMO);
        return trace;
    }

    /**
     * 建立退件追蹤<BR/>
     * 前端先初始化後，才進行儲存
     *
     * @param require
     * @param executor
     * @return
     */
    public RequireTrace initRequireRejectTrace(
            String requireSid,
            User executor,
            String reason) {
        RequireTrace trace = this.createNewTrace(requireSid, executor);
        trace.setRequireTraceType(RequireTraceType.ROLL_BACK_NOTIFY);
        trace.setReason(reason);
        String description = "原分類為：" + trace.getRequire().getMapping().getSmallName() + "\r\n"
                + "中類/小類請更正為：\r\n請主管協助進行作廢程序";
        trace.setRequireTraceContent(description);
        return trace;
    }

    /**
     * 建立送測作廢 追蹤記錄資訊
     *
     * @param require
     * @param testInfo
     * @param executor
     * @param invaildTraceReason
     * @return
     */
    public RequireTrace createSendTestProcessInvaildTrace(
            String requireSid,
            WorkTestInfo testInfo,
            User executor,
            String invaildTraceReason) {
        RequireTrace trace = this.createNewTrace(requireSid, executor);
        trace.setRequireTraceType(RequireTraceType.SEND_TEST_SIGN_INVAILD);
        trace.setRequireTraceContent("送測單號：" + testInfo.getTestinfoNo() + " - 送測審核作廢");
        trace.setReason("送測單號：" + testInfo.getTestinfoNo() + " - " + invaildTraceReason);
        trace = dao.save(trace);
        trace.setCreatedUser(executor);
        return trace;
    }

    /**
     * 建立變更希望完成日 追蹤記錄資訊
     *
     * @param require
     * @param executor
     * @param oldDate
     * @param newDate
     * @return
     */
    public RequireTrace createChangeHopeDateTrace(
            String requireSid,
            User executor,
            Date oldDate,
            Date newDate) {

        RequireTrace trace = this.createNewTrace(requireSid, executor);
        trace.setRequireTraceType(RequireTraceType.MODIFY_HOPE_DT);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        trace.setRequireTraceContent("期望完成日由【" + sdf.format(oldDate) + "】修改至【" + sdf.format(newDate) + "】");
        trace.setRequireTraceContentCss(trace.getRequireTraceContent());
        trace = dao.save(trace);
        return trace;
    }

    /**
     * 建立 追蹤記錄資訊
     *
     * @param require
     * @param executor
     * @param traceType
     * @param traceContent
     * @return
     */
    public RequireTrace createRequireTrace(String requireSid, User executor, RequireTraceType traceType, String traceContent) {
        RequireTrace trace = this.createNewTrace(requireSid, executor);
        trace.setRequireTraceType(traceType);
        trace.setRequireTraceContent(traceContent);
        trace.setRequireTraceContentCss(traceContent);
        return dao.save(trace);
    }

    /**
     * 建立需求單與案件單對應追蹤
     *
     * @param require
     * @param mappTrans
     * @param dispatchStatus
     * @param issueStatus
     * @param executor
     * @param traceCreateDate
     */
    @Transactional(rollbackFor = Exception.class)
    public void createIssueTransOnpgTrace(
            String requireSid,
            MappCreateTrans mappTrans,
            String dispatchStatus,
            String issueStatus,
            User executor,
            Date traceCreateDate,
            WorkOnpg workOnpg) {

        RequireTrace trace = this.createNewTrace(requireSid, executor, traceCreateDate);
        // 案件單轉入
        trace.setRequireTraceType(RequireTraceType.TRANSFER_FROM_TECH);

        StringBuilder content = new StringBuilder();
        content.append("1.派工狀態：")
                .append(dispatchStatus)
                .append("<br>");
        content.append("2.案件狀態：")
                .append(issueStatus)
                .append("<br>");
        content.append("3.轉入案件單號：")
                .append("<a target=\"_blank\" href=\"")
                .append(logicConstants.getOpenerIssueUrlKey())
                .append(mappTrans.getMessageTo().getIssueNo())
                .append("\" style=\"color: blue !important;text-decoration: underline !important;\">")
                .append(mappTrans.getMessageTo().getIssueNo())
                .append("</a><br>");
        content.append("4.需求類別：")
                .append(mappTrans.getMessageTo().getSmallCategoryName());
        if (workOnpg != null && !Strings.isNullOrEmpty(mappTrans.getOnpgNo())) {
            content.append("<br>")
                    .append("5.轉入的ON程式單號：")
                    .append(workOnpg.getOnpgNo());
            content.append("<br>").append("6.ON程式主題：").append(HtmlUtils.htmlEscape(workOnpg.getTheme()));
        }
        trace.setRequireTraceContent(content.toString());
        trace.setRequireTraceContentCss(trace.getRequireTraceContent());

        dao.save(trace);

        log.debug("寫案件單轉入追蹤");
    }

    /**
     * 建立案件單轉入追蹤
     * 
     * @param requireSid      需求單 sid
     * @param content         內容
     * @param executor        執行者
     * @param traceCreateDate 追蹤日期
     */
    @Transactional(rollbackFor = Exception.class)
    public void createIssueTransOnpgTrace(
            String requireSid,
            String content,
            User executor,
            Date traceCreateDate) {

        RequireTrace trace = this.createNewTrace(requireSid, executor, traceCreateDate);
        // 案件單轉入
        trace.setRequireTraceType(RequireTraceType.TRANSFER_FROM_TECH);

        trace.setRequireTraceContent(content);
        trace.setRequireTraceContentCss(content);

        dao.save(trace);

        log.debug("寫案件單轉入追蹤");
    }

    /**
     * 取得最後一次退件時間訊息 至簽核者才會顯示訊息
     * 
     * @param require
     * @param loginUserId
     * @return
     */
    public String evenRollbackAlertMsg(String requireSid, String loginUserId) {

        Require require = this.requireService.findByReqSid(requireSid);

        if (require == null) {
            return null;
        }

        if (!require.getBackCode()) {
            return null;
        }

        if (!RequireStatusType.ROLL_BACK_NOTIFY.equals(require.getRequireStatus())) {
            return null;
        }

        // 取得bpm水管圖
        List<ProcessTaskBase> tasks = reqUnitBpmService.findFlowChartByInstanceId(require.getReqUnitSign().getBpmInstanceId(), loginUserId);
        String bpmNodeUser = CollectionUtils.isNotEmpty(tasks) ? tasks.get(tasks.size() - 1).getUserID() : "";
        if (!bpmNodeUser.equals(loginUserId)) {
            return null;
        }

        // 查詢追蹤
        List<RequireTrace> traces = this.dao.findByRequireAndRequireTraceTypeAndStatusOrderByCreatedDateDesc(
                require,
                RequireTraceType.ROLL_BACK_NOTIFY,
                Activation.ACTIVE);

        if (WkStringUtils.isEmpty(traces)) {
            return null;
        }

        return String.format("系統客服於%s <span style=\"color:red\">退件</span>，請再次確認",
                DateUtils.YYYY_MM_DD_HH24_MI.print(traces.get(0).getCreatedDate().getTime()));
    }

    /**
     * 寫異動系統別追蹤
     * 
     * @param requireSid                   需求單 sid
     * @param beforeSelectedCheckItemTypes 異動前檢查項目
     * @param afterSelectedCheckItemTypes  異動後檢查項目
     * @param executor                     執行者
     * @param execDate                     執行時間
     * @return 追蹤內容
     */
    public String createChangeCheckItemTypes(
            String requireSid,
            List<RequireCheckItemType> beforeSelectedCheckItemTypes,
            List<RequireCheckItemType> afterSelectedCheckItemTypes,
            User executor,
            Date execDate) {

        if (beforeSelectedCheckItemTypes == null) {
            beforeSelectedCheckItemTypes = Lists.newArrayList();
        }

        List<String> before = WkCommonUtils.safeStream(beforeSelectedCheckItemTypes)
                .map(RequireCheckItemType::getDescr)
                .collect(Collectors.toList());

        List<String> after = WkCommonUtils.safeStream(afterSelectedCheckItemTypes)
                .map(RequireCheckItemType::getDescr)
                .collect(Collectors.toList());

        String traceContent = String.format("異動檢查項目:[%s]->[%s]",
                String.join("、", before),
                String.join("、", after));

        RequireTrace trace = this.createNewTrace(requireSid, executor, execDate);
        trace.setRequireTraceType(RequireTraceType.CHANGE_CHECK_ITEMS);
        trace.setRequireTraceContent(traceContent);
        trace.setRequireTraceContentCss(traceContent);

        this.dao.save(trace);

        return traceContent;

    }

    /**
     * 建立主責單位異動資訊
     * 
     * @param oldInchargeDepSid  舊的 depSid
     * @param newInchargeDepSid  新的 depSid
     * @param oldInchargeUserSid 舊的 userSid
     * @param newInchargeUserSid 新的 userSid
     * @return 異動資訊, 為異動時回傳空字串
     */
    public String createChangeInChargeTraceMessage(
            Integer oldInchargeDepSid,
            Integer newInchargeDepSid,
            Integer oldInchargeUserSid,
            Integer newInchargeUserSid) {

        List<String> traceMessage = Lists.newArrayList();

        // 判斷主責單位前後不一致
        if (!WkCommonUtils.compareByStr(oldInchargeDepSid, newInchargeDepSid)) {
            String oldInfo = "未設定";
            if (WkOrgCache.getInstance().isExsit(oldInchargeDepSid)) {
                oldInfo = WkOrgUtils.prepareBreadcrumbsByDepName(oldInchargeDepSid, OrgLevel.MINISTERIAL, true, "-");
            }
            String newInfo = "未設定";
            if (WkOrgCache.getInstance().isExsit(newInchargeDepSid)) {
                newInfo = WkOrgUtils.prepareBreadcrumbsByDepName(newInchargeDepSid, OrgLevel.MINISTERIAL, true, "-");
            }

            traceMessage.add(String.format("主責單位：【%s】->【%s】",
                    oldInfo,
                    newInfo));
        }

        // 判斷主責單位負責人前後不一致
        if (!WkCommonUtils.compareByStr(newInchargeUserSid, oldInchargeUserSid)) {
            String oldInfo = "未設定";
            if (WkUserCache.getInstance().isExsit(oldInchargeUserSid)) {
                oldInfo = WkUserUtils.findNameBySid(oldInchargeUserSid);
            }
            String newInfo = "未設定";
            if (WkUserCache.getInstance().isExsit(newInchargeUserSid)) {
                newInfo = WkUserUtils.findNameBySid(newInchargeUserSid);
            }

            traceMessage.add(String.format("主責單位負責人：【%s】->【%s】",
                    oldInfo,
                    newInfo));
        }

        return String.join("<br/>", traceMessage);
    }

    /**
     * 建立 【維護管理系統(MMS) ON程式單同步】追蹤記錄
     * 
     * @param syncFormTo SyncFormTo
     */
    public void createSyncMmsTrace(
            SyncFormTo syncFormTo) {

        // 兜組追蹤內容
        String traceContent = ""
                + WkHtmlUtils.addBlueBlodClass("維護管理系統(MMS) ON程式單同步")
                + "<br/>"
                + "<br/>" + WkHtmlUtils.addBlueBlodClass("執行資訊如下：")
                + "<br/>執行：【" + syncFormTo.getLogSyncMmsActionType().getDescr() + "】"
                + "<br/>工作ID：【" + syncFormTo.getMmsID() + "】"
                + "<br/>ON程式單：【" + syncFormTo.getOnpgNo() + "】";

        // 欄位異動資訊
        if (WkStringUtils.notEmpty(syncFormTo.getModifyInfos())) {
            traceContent += ""
                    + "<br/>"
                    + "<br/>" + WkHtmlUtils.addBlueBlodClass("異動資訊如下：")
                    + "<br/>" + syncFormTo.getModifyInfosStr("<br/>");
        }

        // 建立追蹤資料
        this.createRequireTrace(
                syncFormTo.getRequire().getSid(),
                syncFormTo.getCreateUser(),
                RequireTraceType.SYNC_MMS,
                traceContent);

    }

    /**
     * 建立 【維護管理系統(MMS) ON程式單同步】期望完成日追蹤記錄
     * 
     * @param syncFormTo  SyncFormTo
     * @param oldHopeDate 舊的期望日
     * @param onpgNo      on 程式單號
     */
    public void createSyncMmsTraceForChangeHopeDate(
            SyncFormTo syncFormTo,
            Date oldHopeDate,
            String onpgNo) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

        // 兜組追蹤內容
        String traceContent = ""
                + WkHtmlUtils.addBlueBlodClass("維護管理系統(MMS) ON程式單同步 - 異動需求單期望日期")
                + "<br/><br/>"
                + "<br/>執行：【" + syncFormTo.getLogSyncMmsActionType().getDescr() + "】"
                + "<br/>工作ID：【" + syncFormTo.getMmsID() + "】"
                + "<br/>ON程式單：【" + onpgNo + "】"
                + "<br/><br/>"
                + "【需求單-期望完成日】由【" + sdf.format(oldHopeDate) + "】修改至【" + sdf.format(syncFormTo.getMaintenanceDate()) + "】";

        // 建立追蹤資料
        this.createRequireTrace(
                syncFormTo.getRequire().getSid(),
                syncFormTo.getCreateUser(),
                RequireTraceType.MODIFY_HOPE_DT,
                traceContent);
    }

}
