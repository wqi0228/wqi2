/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.vo;

import com.cy.tech.request.logic.enumerate.BasicDataCategoryType;
import com.cy.tech.request.vo.template.CategoryKeyMapping;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 需求單 選擇模版樹用 (require01.xhtml - RequireCategoryTreeMBean.java)
 *
 * @author shaun
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"mapping"})
public class MappingTreeTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2708039548865815977L;
    /** 對應mapping */
    private CategoryKeyMapping mapping;
    /** 類別種類 */
    private BasicDataCategoryType type;
    /** 類別名稱 */
    private String name;

}
