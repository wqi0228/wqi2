/**
 * 
 */
package com.cy.tech.request.logic.service.syncmms.helper;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cy.work.backend.logic.WorkBackendParamService;
import com.cy.work.backend.vo.enums.WkBackendParam;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.alert.TechRequestAlertException;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Slf4j
@Service
public class SyncMmsParamHelper implements Serializable, InitializingBean {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 7350646777301721534L;

    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static SyncMmsParamHelper instance;

    /**
     * getInstance
     *
     * @return instance
     */
    public static SyncMmsParamHelper getInstance() { return instance; }

    /**
     * fix for fireBugs warning
     *
     * @param instance
     */
    private static void setInstance(SyncMmsParamHelper instance) { SyncMmsParamHelper.instance = instance; }

    /**
     * afterPropertiesSet
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private WorkBackendParamService workBackendParamService;

    // ========================================================================
    // prop 取值
    // ========================================================================
    /**
     * Client ID
     */
    @Value("${tech.request.mms.api.getToken.clientID}")
    private String apiGetTokenClientID;
    /**
     * Client Secret
     */
    @Value("${tech.request.mms.api.getToken.clientSecret}")
    private String apiGetTokenClientSecret;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 取得 API:get token 的 Client ID
     * 
     * @return Client ID
     * @throws SystemOperationException 未設定時拋出
     */
    public String getAPIGetTokenClientID() throws SystemOperationException {
        if (WkStringUtils.isEmpty(this.apiGetTokenClientID)) {
            String errorMessage = "未設定 properties 環境參數：[tech.request.mms.api.getToken.clientID]";
            log.error(errorMessage);
            throw new TechRequestAlertException(errorMessage);
        }
        return WkStringUtils.safeTrim(this.apiGetTokenClientID);
    }

    /**
     * 取得 API:get token 的 Client Secret
     * 
     * @return Client Secret
     * @throws SystemOperationException 未設定時拋出
     */
    public String getAPIGetTokenClientSecret() throws SystemOperationException {
        if (WkStringUtils.isEmpty(this.apiGetTokenClientSecret)) {
            String errorMessage = "未設定 properties 環境參數：[tech.request.mms.api.getToken.clientSecret]";
            log.error(errorMessage);
            throw new TechRequestAlertException(errorMessage);
        }
        return WkStringUtils.safeTrim(this.apiGetTokenClientSecret);
    }

    /**
     * 取得所有API 的 Host
     * 
     * @return Host Domain
     * @throws SystemOperationException 未設定時拋出
     */
    public String getAPIHostDomain() throws SystemOperationException { return this.findSysParam(WkBackendParam.MMS_SYNC_API_DOMAIN); }

    /**
     * 取得站別
     * 
     * @return 站別
     * @throws SystemOperationException 未設定時拋出
     */
    public String getAPIPortal() throws SystemOperationException { return this.findSysParam(WkBackendParam.MMS_SYNC_API_PORTAL); }

    /**
     * 維護管理系統(MMS) 介接 新增需求單據類別 ID
     * 
     * @return 需求單據類別 ID
     * @throws SystemOperationException 未設定時拋出
     */
    public String getReqCategoryKeyMappingID() throws SystemOperationException { return this.findSysParam(WkBackendParam.MMS_SYNC_REQUIRE_CATEGORY_KEY_MAPPING_ID); }

    /**
     * 維護管理系統(MMS) 介接 通知的市場單位 (會過濾停用單位)
     * 
     * @return
     */
    public Set<Integer> findMmsSyncNoticeMarketDepSids() {

        // 取得設定單位
        List<Integer> depSids = this.workBackendParamService.findIntsByKeyword(
                WkBackendParam.MMS_SYNC_NOTICE_MARKET_DEPS);

        if (WkStringUtils.isEmpty(depSids)) {
            return Sets.newHashSet();
        }

        // 去除重複單位
        return WkOrgUtils.prepareAllDepsByTopmost(depSids, true)
                .stream()
                .collect(Collectors.toSet());

    }

    /**
     * 維護管理系統(MMS) 介接 是否顯示DEBUG資訊
     * 
     * @return boolean
     */
    public boolean isShowMmsSyncDebugLog() { return "Y".equals(workBackendParamService.findTextByKeyword(WkBackendParam.MMS_SYNC_SHOW_DEBUG)); }

    // ========================================================================
    // 內部方法
    // ========================================================================
    /**
     * 取得系統參數
     * 
     * @param wkBackendParam WkBackendParam
     * @return 設定值
     * @throws SystemOperationException 未設定時拋出
     */
    private String findSysParam(WkBackendParam wkBackendParam) throws SystemOperationException {
        String val = this.workBackendParamService.findTextByKeyword(wkBackendParam);
        if (WkStringUtils.isEmpty(val)) {
            String errorMessage = "未設定系統參數：[" + wkBackendParam + "]";
            log.error(errorMessage);
            throw new TechRequestAlertException(errorMessage);
        }
        return WkStringUtils.safeTrim(val);
    }

}
