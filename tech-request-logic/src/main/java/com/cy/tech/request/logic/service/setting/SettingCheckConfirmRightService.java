package com.cy.tech.request.logic.service.setting;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.vo.SettingCheckConfirmRightVO;
import com.cy.tech.request.repository.setting.SettingCheckConfirmRightRepository;
import com.cy.tech.request.vo.constants.CacheConstants;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.setting.SettingCheckConfirmRight;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.logic.WkLogicUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu 設定：可檢查確認權限
 */
@Slf4j
@Service
public class SettingCheckConfirmRightService {

    // ========================================================================
    // 服務區
    // ========================================================================

    @Autowired
    transient private SettingCheckConfirmRightCache settingCheckConfirmRightCache;

    @Autowired
    transient private SettingCheckConfirmRightRepository settingCheckConfirmRightRepository;

    @Autowired
    private transient WkLogicUtil wkLogicUtil;

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * 查詢所有系統別設定資料 (若該系統別無DB設定資料, 自動產生對應VO)
     * 
     * @return
     */
    public List<SettingCheckConfirmRightVO> findAllSetting() {

        // ====================================
        // 查詢所有 DB 已存在的資料
        // ====================================
        // 查詢資料庫中所以已存在的設定資料
        List<SettingCheckConfirmRight> dbDetas = this.settingCheckConfirmRightCache.findAll();
        if (dbDetas == null) {
            dbDetas = Lists.newArrayList();
        }
        // 依據檢查類別分類資料
        Map<RequireCheckItemType, SettingCheckConfirmRight> settingCheckConfirmRightByCheckItemType = dbDetas.stream()
                .collect(Collectors.toMap(
                        SettingCheckConfirmRight::getCheckItemType,
                        each -> each));

        // ====================================
        // 組裝資料 VO
        // ====================================
        List<SettingCheckConfirmRightVO> resultVOs = Lists.newArrayList();

        // 依據可設定的『系統別』項目，組裝VO資料
        for (RequireCheckItemType checkItemType : RequireCheckItemType.values()) {

            // 依據類別取得已設定資料
            SettingCheckConfirmRight entity = settingCheckConfirmRightByCheckItemType.get(checkItemType);

            // 已存在設定資料 -> 轉成 VO
            if (entity != null) {

                // 轉為VO
                SettingCheckConfirmRightVO vo = new SettingCheckConfirmRightVO(entity);

                // 兜組可檢查部門顯示文字
                vo.setShow_canCheckDepts(WkOrgUtils.prepareDepsNameByTreeStyle(vo.getCanCheckDepts(), 10));

                // 兜組可檢查使用者顯示文字
                String userNames = "";
                if (WkStringUtils.notEmpty(vo.getCanCheckUsers())) {
                    userNames = vo.getCanCheckUsers().stream()
                            .map(userSid -> WkUserUtils.prepareUserNameWithDep(userSid, OrgLevel.MINISTERIAL, true, "-"))
                            .collect(Collectors.joining("<br/>"));
                }
                vo.setShow_canCheckUsers(userNames);

                // 加入 result
                resultVOs.add(vo);

            }

            // 不存在設定資料 -> 依據『系統別』產生VO
            else {
                SettingCheckConfirmRightVO vo = new SettingCheckConfirmRightVO();
                vo.setCheckItemType(checkItemType);
                resultVOs.add(vo);
            }
        }

        return resultVOs;

    }

    /**
     * 取得可檢查人員的 SID
     * 
     * @param checkItemType 檢查類別
     * @return 可檢查人員的 SID
     */
    public Set<Integer> findCanCheckUserSids(RequireCheckItemType checkItemType) {

        if (checkItemType == null) {
            throw new SystemDevelopException("傳入檢查類別為空!", InfomationLevel.ERROR);
        }

        // ====================================
        // 查詢所有 DB 已存在的資料
        // ====================================
        // 查詢資料庫中所以已存在的設定資料
        List<SettingCheckConfirmRight> entities = this.settingCheckConfirmRightCache.findAll();
        if (entities == null) {
            entities = Lists.newArrayList();
        }

        SettingCheckConfirmRight setting = null;
        for (SettingCheckConfirmRight settingCheckConfirmRight : entities) {
            if (checkItemType.equals(settingCheckConfirmRight.getCheckItemType())) {
                setting = settingCheckConfirmRight;
                break;
            }
        }

        if (setting == null) {
            log.warn("系統別：[{}], 未設定可檢查部門/人員", checkItemType);
            return Sets.newHashSet();
        }

        // ====================================
        // 整裡可檢查人員
        // ====================================
        Set<Integer> canCheckUserSids = Sets.newHashSet();

        // 可檢查部門
        if (WkStringUtils.notEmpty(setting.getCanCheckDepts())) {
            List<User> users = WkUserCache.getInstance().findByPrimaryOrgSidsIn(setting.getCanCheckDepts());
            if (WkStringUtils.notEmpty(users)) {
                for (User user : users) {
                    canCheckUserSids.add(user.getSid());
                }
            }
        }

        // 可檢查人員
        if (WkStringUtils.notEmpty(setting.getCanCheckUsers())) {
            canCheckUserSids.addAll(setting.getCanCheckUsers());
        }

        return canCheckUserSids;
    }

    /**
     * 判斷使用者是否為『檢查人員』
     * 
     * @param targetUserSid 標的使用者 SID
     * @param checkItemType 檢查項目
     * @return 是/否
     */
    @Cacheable(cacheNames = CacheConstants.CACHE_IS_CANCHECK_USER, key = "{#root.methodName, #targetUserSid, #checkItemType}")
    public boolean isCanCheckUser(Integer targetUserSid, List<RequireCheckItemType> checkItemTypes) {

        // log.debug("isCanCheckUser:{},{}", checkItemType, targetUserSid);

        if (WkStringUtils.isEmpty(checkItemTypes)) {
            return false;
        }

        // ====================================
        // 查詢所有 DB 已存在的資料
        // ====================================
        List<SettingCheckConfirmRight> allSettings = this.settingCheckConfirmRightCache.findAll();
        if (allSettings == null) {
            return false;
        }

        // ====================================
        // 逐筆查找符合的設定權限
        // ====================================
        for (SettingCheckConfirmRight setting : allSettings) {

            // ====================================
            // 非傳入檢查項目，跳過此設定檔判斷
            // ====================================
            if (checkItemTypes.contains(setting.getCheckItemType())) {
                continue;
            }

            // ====================================
            // 傳入使用者是否在設定中
            // ====================================
            if (this.isUserInSetting(setting, targetUserSid)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 查詢使用者有哪些系統別權限
     * 
     * @param targetUserSid
     * @return
     */
    @Cacheable(cacheNames = CacheConstants.CACHE_IS_CANCHECK_USER, key = "{#root.methodName, #targetUserSid}")
    public List<RequireCheckItemType> findUserCheckRights(Integer targetUserSid) {

        // ====================================
        // 查詢所有 DB 已存在的資料
        // ====================================
        // 查詢資料庫中所以已存在的設定資料
        List<SettingCheckConfirmRight> allSettings = this.settingCheckConfirmRightCache.findAll();
        if (allSettings == null) {
            return Lists.newArrayList();
        }

        // ====================================
        // 逐筆查找符合的設定權限
        // ====================================
        List<RequireCheckItemType> checkItems = Lists.newArrayList();
        for (SettingCheckConfirmRight setting : allSettings) {
            // 傳入使用者是否在設定中
            if (this.isUserInSetting(setting, targetUserSid)) {
                checkItems.add(setting.getCheckItemType());
            }
        }
        return checkItems;
    }

    /**
     * 在『任一系統別』設定中，有該使用者的權限<br/>
     * 注意，此方法在此 class 中被呼叫, 沒有效果
     * 
     * @param targetUserSid 標的使用者
     * @return 是/否
     */
    @Cacheable(cacheNames = CacheConstants.CACHE_IS_CANCHECK_USER, key = "{#root.methodName, #targetUserSid}")
    public boolean hasCanCheckUserRight(Integer targetUserSid) {

        // ====================================
        // 查詢所有 DB 已存在的資料
        // ====================================
        // 查詢資料庫中所以已存在的設定資料
        List<SettingCheckConfirmRight> allSettings = this.settingCheckConfirmRightCache.findAll();
        if (allSettings == null) {
            return false;
        }

        // ====================================
        // 任何一組設定檔有目標 user 即命中
        // ====================================
        for (SettingCheckConfirmRight setting : allSettings) {
            if (this.isUserInSetting(setting, targetUserSid)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 傳入使用者是否在設定中
     * 
     * @param setting       SettingCheckConfirmRight
     * @param targetUserSid 標的使用者
     * @return 是/否
     */
    private boolean isUserInSetting(SettingCheckConfirmRight setting, Integer targetUserSid) {
        // ====================================
        // 判斷『可檢查部門』
        // ====================================
        if (WkStringUtils.notEmpty(setting.getCanCheckDepts())) {
            for (Integer orgSid : setting.getCanCheckDepts()) {
                // 取得可檢查部門下所有使用者
                List<User> users = WkUserCache.getInstance().getUsersByPrimaryOrgSid(orgSid);
                // 檢查傳入使用者是否存在於部門
                if (WkStringUtils.notEmpty(users)) {
                    for (User user : users) {
                        if (WkCommonUtils.compareByStr(user.getSid(), targetUserSid)) {
                            return true;
                        }
                    }
                }
            }
        }

        // ====================================
        // 判斷『可檢查人員』
        // ====================================
        if (WkStringUtils.notEmpty(setting.getCanCheckUsers())) {
            // 檢查傳入使用者是否存在於可檢查人員名單
            if (setting.getCanCheckUsers().contains(targetUserSid)) {
                return true;
            }
        }

        return false;
    }

    /**
     * 儲存可檢查部門
     * 
     * @param checkItemType 檢查項目
     * @param canCheckDepts 可檢查部門 sid
     * @param execUserSid   執行者 sid
     */
    @Transactional(rollbackFor = Exception.class)
    @Caching(evict = {
            @CacheEvict(value = CacheConstants.CACHE_SETTING_CHECK_CONFIRM_RIGHT, allEntries = true),
            @CacheEvict(value = CacheConstants.CACHE_IS_CANCHECK_USER, allEntries = true)
    })
    public void saveCanCheckDepts(
            RequireCheckItemType checkItemType,
            List<Integer> canCheckDepts,
            Integer execUserSid) {
        log.info("異動【{}】可檢查部門：【{}】", checkItemType.getDescr(), WkOrgUtils.findNameBySid(canCheckDepts, "、"));
        this.save(checkItemType, true, canCheckDepts, execUserSid);

        // ====================================
        // 更新需求單REST快取
        // ====================================
        wkLogicUtil.clearRequireRestEhCache();

    }

    /**
     * 儲存可檢查人員
     * 
     * @param checkItemType 檢查項目
     * @param canCheckUsers 可檢查人員 sid
     * @param execUserSid   執行者 sid
     */
    @Transactional(rollbackFor = Exception.class)
    @Caching(evict = {
            @CacheEvict(value = CacheConstants.CACHE_SETTING_CHECK_CONFIRM_RIGHT, allEntries = true),
            @CacheEvict(value = CacheConstants.CACHE_IS_CANCHECK_USER, allEntries = true)
    })
    public void saveCanCheckUsers(
            RequireCheckItemType checkItemType,
            List<Integer> canCheckUsers,
            Integer execUserSid) {
        log.info("異動【系統別：{}】可檢查人員：【{}】", checkItemType.getDescr(), WkUserUtils.findNameBySid(canCheckUsers, "、"));
        this.save(checkItemType, false, canCheckUsers, execUserSid);

        // ====================================
        // 更新需求單REST快取
        // ====================================
        wkLogicUtil.clearRequireRestEhCache();
    }

    /**
     * 儲存可檢查權限
     * 
     * @param checkItemType  檢查項目
     * @param isCanCheckDeps 資料是否為可檢查部門 (false 時代表傳入為可檢查人員)
     * @param sids           部門或人員 sid
     * @param execUserSid    執行者 sid
     */
    private void save(
            RequireCheckItemType checkItemType,
            boolean isCanCheckDeps,
            List<Integer> sids,
            Integer execUserSid) {

        // ====================================
        // 查詢已存在資料
        // ====================================
        SettingCheckConfirmRight entity = this.settingCheckConfirmRightRepository.findByCheckItemType(checkItemType);

        // ====================================
        // 整裡 save 欄位資料
        // ====================================
        // 為新增
        if (entity == null) {
            // 初始化新entity
            entity = new SettingCheckConfirmRight();
            entity.setCreatedUser(execUserSid);
            entity.setCreatedDate(new Date());
            entity.setStatus(Activation.ACTIVE);
            // 檢查項目
            entity.setCheckItemType(checkItemType);
        }

        // 為 update
        else {
            entity.setUpdatedUser(execUserSid);
            entity.setUpdatedDate(new Date());
        }

        if (isCanCheckDeps) {
            // 可檢查確認部門
            entity.setCanCheckDepts(sids);
        } else {
            // 可檢查確認人員
            entity.setCanCheckUsers(sids);
        }

        // ====================================
        // save (insert or update)
        // ====================================
        this.settingCheckConfirmRightRepository.save(entity);

        // ====================================
        // 更新需求單REST快取
        // ====================================
        wkLogicUtil.clearRequireRestEhCache();
    }
}
