/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.cy.tech.request.logic.search.view.BaseSearchView;

/**
 *
 * @author jason_h
 */
public interface QueryService<T extends BaseSearchView> extends Serializable {

    public List<T> findWithQuery(
            String sql, 
            Map<String, Object> parameters, 
            Integer execUserSid,
            RequireReportUsageRecord usageRecord
            );
}
