/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.pt;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.config.exception.PtEditExceptions;
import com.cy.tech.request.logic.helper.systemnotify.SysNotifyHelper;
import com.cy.tech.request.logic.service.AssignNoticeService;
import com.cy.tech.request.logic.service.FormNumberService;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.RequireProcessCompleteRollbackService;
import com.cy.tech.request.logic.service.RequireReadRecordHelper;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.RequireShowService;
import com.cy.tech.request.logic.service.RequireTraceService;
import com.cy.tech.request.logic.service.TrSubNoticeInfoService;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepService;
import com.cy.tech.request.logic.vo.PtCheckTo;
import com.cy.tech.request.repository.TrNamedQueryRepository;
import com.cy.tech.request.repository.pt.PtAlreadyReplyRepo;
import com.cy.tech.request.repository.pt.PtAttachmentRepo;
import com.cy.tech.request.repository.pt.PtCheckRepo;
import com.cy.tech.request.repository.pt.PtHistoryRepo;
import com.cy.tech.request.repository.require.ReqModifyRepo;
import com.cy.tech.request.repository.result.TransRequireVO;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.RequireTransProgramType;
import com.cy.tech.request.vo.enums.SubNoticeType;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.pt.PtAlreadyReply;
import com.cy.tech.request.vo.pt.PtAttachment;
import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.pt.PtHistory;
import com.cy.tech.request.vo.pt.enums.PtHistoryBehavior;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.common.vo.AttachmentService;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * 原型確認服務
 *
 * @author shaun
 */
@Slf4j
@Component
public class PtService implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2886742404070343324L;
    private static PtService instance;

    public static PtService getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        PtService.instance = this;
    }

    @Autowired
    private RequireService reqService;
    @Autowired
    private RequireShowService reqShowService;
    @Autowired
    private RequireTraceService rtService;
    @Autowired
    private PtShowService ptShowService;
    @Autowired
    private PtBpmService ptBpmService;
    @Autowired
    private PtHistoryService pthService;
    @Autowired
    @Qualifier("pt_attach")
    private AttachmentService<PtAttachment, PtCheck, String> attachService;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private WkJsoupUtils jsoupUtils;
    @Autowired
    private PtCheckRepo ptDao;
    @Autowired
    private PtHistoryRepo ptHistoryDao;
    @Autowired
    private PtAlreadyReplyRepo aReplyDao;
    @Autowired
    private PtAttachmentRepo attachDao;
    @Autowired
    private ReqModifyRepo reqModifyDao;
    @Autowired
    private TrSubNoticeInfoService subNoticeInfoService;
    @Autowired
    private transient TrNamedQueryRepository trNamedQueryRepository;
    @Autowired
    private RequireProcessCompleteRollbackService requireProcessCompleteRollbackService;
    @Autowired
    private RequireConfirmDepService requireConfirmDepService;
    @Autowired
    private FormNumberService formNumberService;
    @Autowired
    private AssignNoticeService assignNoticeService;
    @Autowired
    private SysNotifyHelper sysNotifyHelper;

    @Autowired
    private transient RequireReadRecordHelper requireReadRecordHelper;

    /**
     * 建立空送測資訊
     *
     * @param executor
     * @param require
     * @return
     */
    public PtCheck createEmptyPtCheck(User executor, Require require) {
        PtCheck ptCheck = new PtCheck();
        ptCheck.setSourceType(WorkSourceType.TECH_REQUEST);
        ptCheck.setSourceSid(require.getSid());
        ptCheck.setSourceNo(require.getRequireNo());
        ptCheck.setCreateCompany(orgService.findUserCompany(WkOrgCache.getInstance().findBySid(executor.getPrimaryOrg().getSid())));
        ptCheck.setCreateDep(WkOrgCache.getInstance().findBySid(executor.getPrimaryOrg().getSid()));
        ptCheck.setStatus(Activation.ACTIVE);
        ptCheck.setCreatedUser(executor);
        ptCheck.setCreatedDate(new Date());
        ptCheck.setNoticeMember(new JsonStringListTo());
        // 20160831取消預設通知成員
        // ptCheck.getNoticeMember().setValue(
        // this.createDefaultNoticeItems(executor, require).stream()
        // .map(User::getSid).map(Object::toString)
        // .collect(Collectors.toList()));
        ptCheck.getNoticeDeps().getValue().addAll(this.createDefaultNoticeDepItems(executor, require));
        return ptCheck;
    }

    /**
     * 需求單位 & 上層組織的sid<BR/>
     * 原型確認填單單位 & 上層組織的sid
     *
     * @param executor
     * @param require
     * @return
     */
    public Set<String> createDefaultNoticeDepItems(User executor, Require require) {
        Set<String> noticeDepSids = Sets.newHashSet();

        // 加入需求單位 & 上層組織的sid
        noticeDepSids.add(require.getCreateDep().getSid() + "");
        List<Org> reqCreateDepParents = WkOrgCache.getInstance().findAllParent(
                require.getCreateDep().getSid(),
                OrgLevel.DIVISION_LEVEL);
        if (WkStringUtils.notEmpty(reqCreateDepParents)) {
            for (Org depParent : reqCreateDepParents) {
                noticeDepSids.add(depParent.getSid() + "");
            }
        }

        // 加入立案單位 & 上層組織的sid
        noticeDepSids.add(executor.getPrimaryOrg().getSid() + "");

        List<Org> ptCreateDepParents = WkOrgCache.getInstance().findAllParent(
                executor.getPrimaryOrg().getSid(),
                OrgLevel.DIVISION_LEVEL);

        if (WkStringUtils.notEmpty(ptCreateDepParents)) {
            for (Org depParent : ptCreateDepParents) {
                noticeDepSids.add(depParent.getSid() + "");
            }
        }

        return noticeDepSids;
    }

    /**
     * 檢查新檔時必要輸入資訊
     *
     * @param ptCheck
     */
    public void checkInputInfo(PtCheck ptCheck) {
        Preconditions.checkArgument(ptCheck.getEstablishDate() != null, "預計完成日不可為空白，請重新輸入！！");
        Preconditions.checkArgument(!Strings.isNullOrEmpty(ptCheck.getTheme()), " 主題不可為空白，請重新輸入！！");
        // 20160831取消預設通知成員
        // Preconditions.checkArgument(!ptCheck.getNoticeMember().getValue().isEmpty(),
        // "請選擇通知成員！！");
        Preconditions.checkArgument(!ptCheck.getNoticeDeps().getValue().isEmpty(), "請選擇通知單位！！");
        Preconditions.checkArgument(!Strings.isNullOrEmpty(jsoupUtils.clearCssTag(ptCheck.getContentCss())), " 內容不可為空白，請重新輸入！！");
    }

    @Transactional(rollbackFor = Exception.class)
    public PtCheck saveByNewPt(
            Require require,
            PtCheck ptCheck,
            User executor) throws ProcessRestException, UserMessageException, SystemDevelopException {

        Date sysDate = new Date();

        // ====================================
        // 權限檢核
        // ====================================
        Require dbRequire = reqService.findByReqObj(require);
        if (!reqShowService.showPrototypeBtn(dbRequire, reqShowService.isContainAssign(require, executor))) {
            throw new UserMessageException("無新增原型確認權限！！", InfomationLevel.WARN);
        }

        // ====================================
        // 更新主檔
        // ====================================
        ptCheck.setContent(jsoupUtils.clearCssTag(ptCheck.getContentCss()));
        ptCheck.setNote(jsoupUtils.clearCssTag(ptCheck.getNoteCss()));

        ptCheck.setPtStatus(PtStatus.PROCESS);
        ptCheck.setVersion(this.createVersion(require));
        // 產生案號
        ptCheck.setPtNo(this.formNumberService.generatePtNum());
        ptCheck = this.save(ptCheck, executor, sysDate);
        attachService.linkRelation(ptCheck.getAttachments(), ptCheck, executor);

        // ====================================
        // 建立讀記錄
        // 1.執行者 -> 已讀
        // 2.填單單位+通知單位人員+通知人員 -> 待閱讀
        // ====================================
        this.requireReadRecordHelper.saveExcutorReadAndOtherUserWaitRead(
                FormType.PTCHECK,
                ptCheck.getSid(),
                this.prepareReadRecordUserSids(ptCheck),
                executor.getSid());

        // ====================================
        // 通知部門、確認人員異動log
        // ====================================
        this.addChangeLog(ptCheck, null);

        // ====================================
        // 若需該小類需要簽核, 則建立簽核流程
        // ====================================
        if (require.getMapping().getSmall().getPtSign()) {
            ptCheck.setPtStatus(PtStatus.SIGN_PROCESS);
            ptCheck.setSignInfo(ptBpmService.createSignInfo(ptCheck, executor));
            ptCheck.setHasSign(Boolean.TRUE);
            ptCheck.setReadReason(WaitReadReasonType.PROTOTYPE_SIGN_PROCESS);
            this.save(ptCheck, executor, sysDate);
        }

        // ====================================
        // 更新需求單主檔
        // ====================================
        require.setHasPrototype(Boolean.TRUE);
        require.setRedoCode(Boolean.FALSE);
        reqModifyDao.updateByHasPrototype(require, require.getHasPrototype(), require.getRedoCode());

        // ====================================
        // 非分派單位成員開單時, 進行自動分派 (如果需要的話)
        // ====================================
        this.assignNoticeService.processForAutoAddAssignDep(
                require.getSid(),
                require.getRequireNo(),
                ptCheck.getCreateDep().getSid(),
                "開立原型確認單",
                executor,
                sysDate);

        // ====================================
        // 若需求製作進度為已完成, 將進度倒回進行中
        // ====================================
        this.requireProcessCompleteRollbackService.executeProcess(
                require.getSid(),
                executor,
                sysDate,
                "再次執行原型確認");

        // ====================================
        // 再次開啟執行單位的【需求完成-確認流程】 (若需要開啟的話)
        // ====================================
        this.requireConfirmDepService.changeProgStatusCompleteByAddNewSubCase(
                require.getRequireNo(),
                require.getSid(),
                executor.getSid(),
                ptCheck.getCreateDep().getSid(),
                "再次執行原型確認",
                sysDate);

        // ====================================
        // 建立通知異動紀錄
        // ====================================
        // 原型確認通知人員
        subNoticeInfoService.createFirstSubChangeRecord(SubNoticeType.PT_NOTICE_MEMBER,
                require.getSid(),
                require.getRequireNo(), ptCheck.getSid(), ptCheck.getPtNo(), ptCheck.getNoticeMember(), executor.getSid());
        // 原型確認通知部門
        subNoticeInfoService.createFirstSubChangeRecord(SubNoticeType.PT_NOTICE_DEP,
                require.getSid(),
                require.getRequireNo(), ptCheck.getSid(), ptCheck.getPtNo(), ptCheck.getNoticeDeps(), executor.getSid());

        // ====================================
        // 處理系統通知 (新增原型確認單)
        // ====================================
        try {
            this.sysNotifyHelper.processForAddPtCheck(
                    require.getSid(),
                    ptCheck.getTheme(),
                    null, // 新增時無異動前通知對象資料
                    null, // 新增時無異動前通知部門資料
                    (ptCheck.getNoticeMember() != null) ? ptCheck.getNoticeMember().getValue() : Lists.newArrayList(),
                    (ptCheck.getNoticeDeps() != null) ? ptCheck.getNoticeDeps().getValue() : Lists.newArrayList(),
                    executor.getSid());
        } catch (Exception e) {
            log.error("執行系統通知失敗!" + e.getMessage(), e);
        }

        log.info("{} 新增原型確認單:{}", executor.getId(), ptCheck.getPtNo());

        return ptCheck;
    }

    /**
     * 建立版本
     *
     * @param require
     * @return
     */
    private Integer createVersion(Require require) {
        return ptDao.findVersionSizeBySource(WorkSourceType.TECH_REQUEST, require.getSid()) + 1;
    }

    @Transactional(rollbackFor = Exception.class)
    public PtCheck save(PtCheck ptCheck) {
        return ptDao.save(ptCheck);
    }

    @Transactional(rollbackFor = Exception.class)
    public PtCheck save(PtCheck ptCheck, User executor) {
        ptCheck.setUpdatedDate(new Date());
        ptCheck.setUpdatedUser(executor);
        return ptDao.save(ptCheck);
    }

    @Transactional(rollbackFor = Exception.class)
    public PtCheck save(PtCheck ptCheck, User executor, Date sysDate) {
        ptCheck.setUpdatedDate(sysDate);
        ptCheck.setUpdatedUser(executor);
        return ptDao.save(ptCheck);
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveByEditPt(Require require, PtCheck edit, User executor, PtCheckTo tempTo) throws IllegalAccessException, PtEditExceptions {

        if (ptShowService.disableEdit(
                reqService.findByReqObj(require),
                this.findByPtNo(edit.getPtNo()),
                executor)) {
            throw new IllegalAccessException("無編輯原型確認權限！！");
        }

        // ====================================
        // 收集異動前資料
        // ====================================
        // 異動前資料(拷貝一份, 與 hibernate 脫勾)
        PtCheck beforePtCheck = new PtCheck();
        BeanUtils.copyProperties(this.findByPtNo(edit.getPtNo()), beforePtCheck);

        JsonStringListTo noticeMember = beforePtCheck.getNoticeMember();
        JsonStringListTo noticeDeps = beforePtCheck.getNoticeDeps();
        this.checkPtCheckByBeforeSave(tempTo, beforePtCheck);
        edit.setContent(jsoupUtils.clearCssTag(edit.getContentCss()));
        edit.setNote(jsoupUtils.clearCssTag(edit.getNoteCss()));
        log.info("需求單:單號-" + ((require != null) ? require.getRequireNo() : "") + " 修改者 : " + executor.getName() + " 修改原型確認");
        this.addChangeLog(edit, beforePtCheck);

        this.checkChangeDstablishDate(edit, beforePtCheck, executor);
        List<PtAttachment> ptAttachments = attachService.findAttachsByLazy(edit);
        for (PtAttachment ptAttachment : ptAttachments) {
            ptAttachment.setKeyChecked(Boolean.TRUE);
        }
        attachService.linkRelation(edit.getAttachments(), edit, executor);
        // 建立子程序 異動紀錄
        subNoticeInfoService.createSubChangeRecord(SubNoticeType.PT_NOTICE_MEMBER,
                require.getSid(),
                require.getRequireNo(), edit.getSid(), edit.getPtNo(), noticeMember, edit.getNoticeMember(), executor.getSid());
        subNoticeInfoService.createSubChangeRecord(SubNoticeType.PT_NOTICE_DEP,
                require.getSid(),
                require.getRequireNo(), edit.getSid(), edit.getPtNo(), noticeDeps, edit.getNoticeDeps(), executor.getSid());
        this.modifyByEditPtCheck(edit, executor);

        // ====================================
        // 更新讀記錄 (已包含異動通知單位需新增的人員)
        // 1.執行者 -> 已讀
        // 2.填單單位+通知單位人員+通知人員 -> 待閱讀
        // 3.刪除被移除的『通知單位人員』和『通知人員』
        // ====================================
        this.requireReadRecordHelper.resetUserList(
                FormType.PTCHECK,
                edit.getSid(),
                this.prepareReadRecordUserSids(edit),
                executor.getSid());

        // ====================================
        // 處理系統通知 (異動原型確認通知對象、單位)
        // ====================================
        try {
            this.processSysNotify(require.getSid(), beforePtCheck, edit, executor.getSid());
        } catch (Exception e) {
            log.error("執行系統通知失敗!" + e.getMessage(), e);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveByEditPtNotify(
            Require require,
            PtCheck edit,
            User executor) throws IllegalAccessException {

        if (ptShowService.disableEditNotify(
                reqService.findByReqObj(require),
                this.findByPtNo(edit.getPtNo()),
                executor)) {
            throw new IllegalAccessException("無編輯通知成員權限！！");
        }
        log.info("需求單:單號-" + ((require != null) ? require.getRequireNo() : "") + " 修改者 : " + executor.getName() + " 修改原型確認通知人員");

        // ====================================
        // 收集異動前資料
        // ====================================
        // 異動前資料(拷貝一份, 與 hibernate 脫勾)
        PtCheck beforePtCheck = new PtCheck();
        BeanUtils.copyProperties(this.findByPtNo(edit.getPtNo()), beforePtCheck);
        this.addChangeLog(edit, beforePtCheck);

        // ====================================
        // 存檔
        // ====================================
        this.save(edit, executor);

        // ====================================
        // 更新讀記錄 (已包含異動通知單位需新增的人員)
        // 1.執行者 -> 已讀
        // 2.填單單位+通知單位人員+通知人員 -> 待閱讀
        // 3.刪除被移除的『通知單位人員』和『通知人員』
        // ====================================
        this.requireReadRecordHelper.resetUserList(
                FormType.PTCHECK,
                edit.getSid(),
                this.prepareReadRecordUserSids(edit),
                executor.getSid());

        // ====================================
        // 建立子程序 異動紀錄
        // ====================================
        subNoticeInfoService.createSubChangeRecord(SubNoticeType.PT_NOTICE_MEMBER,
                require.getSid(),
                require.getRequireNo(), edit.getSid(), edit.getPtNo(), beforePtCheck.getNoticeMember(), edit.getNoticeMember(), executor.getSid());
        subNoticeInfoService.createSubChangeRecord(SubNoticeType.PT_NOTICE_DEP,
                require.getSid(),
                require.getRequireNo(), edit.getSid(), edit.getPtNo(), beforePtCheck.getNoticeDeps(), edit.getNoticeDeps(), executor.getSid());

        // ====================================
        // 處理系統通知 (異動原型確認通知對象、單位)
        // ====================================
        try {
            this.processSysNotify(require.getSid(), beforePtCheck, edit, executor.getSid());
        } catch (Exception e) {
            log.error("執行系統通知失敗!" + e.getMessage(), e);
        }

    }

    private void processSysNotify(
            String requireSid,
            PtCheck beforePtCheck,
            PtCheck afterPtCheck,
            Integer execUserSid) {

        List<String> beforeNoticeUserSids = Lists.newArrayList();
        List<String> beforeNoticeDepSids = Lists.newArrayList();
        if (beforePtCheck != null) {
            if (beforePtCheck.getNoticeMember() != null) {
                beforeNoticeUserSids = beforePtCheck.getNoticeMember().getValue();
            }
            if (beforePtCheck.getNoticeDeps() != null) {
                beforeNoticeDepSids = beforePtCheck.getNoticeDeps().getValue();
            }
        }
        // 異動後資料
        List<String> afterNoticeUserSids = Lists.newArrayList();
        List<String> afterNoticeDepSids = Lists.newArrayList();
        if (afterPtCheck != null) {
            if (afterPtCheck.getNoticeMember() != null) {
                afterNoticeUserSids = afterPtCheck.getNoticeMember().getValue();
            }
            if (afterPtCheck.getNoticeDeps() != null) {
                afterNoticeDepSids = afterPtCheck.getNoticeDeps().getValue();
            }
        }

        // ====================================
        // 處理系統通知 (異動原型確認通知對象、單位)
        // ====================================

        this.sysNotifyHelper.processForAddPtCheck(
                requireSid,
                afterPtCheck.getTheme(),
                beforeNoticeUserSids,
                beforeNoticeDepSids,
                afterNoticeUserSids,
                afterNoticeDepSids,
                execUserSid);

    }

    /**
     * 修改通知部門、確認人員 log
     * 
     * @param edit
     * @param backup
     */
    private void addChangeLog(PtCheck edit, PtCheck backup) {

        log.info("原型確認:單號-" + edit.getPtNo() + " 修改通知部門、確認人員");

        // ====================================
        // 通知部門
        // ====================================
        String names = "";
        if (backup != null) {
            if (backup.getNoticeDeps() == null || WkStringUtils.isEmpty(backup.getNoticeDeps().getValue())) {
                names = "為空";
            } else {
                names = WkOrgUtils.findNameBySidStrs(backup.getNoticeDeps().getValue(), ",");
            }
            log.info("『通知部門修改前』:" + names);
        }

        names = "";
        if (edit.getNoticeDeps() == null || WkStringUtils.isEmpty(edit.getNoticeDeps().getValue())) {
            names = "為空";
        } else {
            names = WkOrgUtils.findNameBySidStrs(edit.getNoticeDeps().getValue(), ",");
        }
        log.info("『通知部門修改後』:" + names);

        // ====================================
        // 確認人員
        // ====================================
        if (backup != null) {
            if (backup.getNoticeMember() == null || WkStringUtils.isEmpty(backup.getNoticeMember().getValue())) {
                names = "為空";
            } else {
                names = WkUserUtils.findNameBySidStrs(backup.getNoticeMember().getValue(), ",");
            }
            log.info("『確認人員修改前』:" + names);
        }

        names = "";
        if (edit.getNoticeMember() == null || WkStringUtils.isEmpty(edit.getNoticeMember().getValue())) {
            names = "為空";
        } else {
            names = WkUserUtils.findNameBySidStrs(edit.getNoticeMember().getValue(), ",");
        }
        log.info("『確認人員修改後』:" + names);

    }

    /**
     * 重新編輯後，需檢查是否有異動預計完成日
     *
     * @param edit
     * @param backup
     * @param executor
     */
    private void checkChangeDstablishDate(PtCheck edit, PtCheck backup, User executor) {
        if (!edit.getEstablishDate().equals(backup.getEstablishDate())) {
            pthService.createEsDtChangeHistory(edit, backup, executor);
            edit.setReadReason(WaitReadReasonType.PROTOTYPE_UPDATE_ESTABLISHDATE);
            edit.setReadUpdateDate(new Date());
        }
    }

    @Transactional(readOnly = true)
    public List<PtCheck> findByRequire(Require require) {
        if (require == null || Strings.isNullOrEmpty(require.getSid())) {
            return Lists.newArrayList();
        }
        return ptDao.findBySourceSidOrderByCreatedDateDesc(require.getSid());
    }

    /**
     * 重新實作 歷程 | 追蹤
     *
     * @param require
     * @param history
     * @param executor
     * @throws ProcessRestException
     */
    @Transactional(rollbackFor = Exception.class)
    public void executeRedo(Require require, PtHistory history, User executor) throws ProcessRestException, IllegalAccessException {
        if (ptShowService.disableRedo(
                reqService.findByReqObj(require),
                this.findByPtNo(history.getPtNo()),
                executor)) {
            throw new IllegalAccessException("無權限進行操作");
        }
        PtCheck ptCheck = history.getPtCheck();
        // 流程部份處理
        ptBpmService.handlerRedo(ptCheck, executor);
        // 原型確認更新
        ptCheck.setPtStatus(PtStatus.REDO);
        ptCheck.setRedoDate(history.getCreatedDate());
        ptCheck.setStatus(Activation.INACTIVE);
        // 待閱讀資訊更新
        ptCheck.setReadReason(WaitReadReasonType.PROTOTYPE_REDO);
        ptCheck.setReadUpdateDate(new Date());
        ptDao.save(ptCheck);
        // 新增追蹤記錄
        rtService.createPtRedoTrace(require.getSid(), executor, history.getReasonCss());
        // 更新需求單狀態
        require.setRedoCode(Boolean.TRUE);
        reqModifyDao.updateByHasPrototype(require, require.getHasPrototype(), require.getRedoCode());
        // 建立歷程
        pthService.saveRedoHistory(history, executor);

        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.PTCHECK,
                ptCheck.getSid(),
                executor.getSid());
    }

    /**
     * 執行 - 功能符合需求
     *
     * @param pc
     * @param executor
     */
    @Transactional(rollbackFor = Exception.class)
    public void executorFunctionConform(
            Require require,
            PtCheck ptCheck,
            User executor) throws IllegalAccessException {
        if (ptShowService.disableFunctionConform(
                reqService.findByReqObj(require),
                this.findByPtNo(ptCheck.getPtNo()),
                executor)) {
            throw new IllegalAccessException("無權限進行操作");
        }
        // 流程更新為結案
        ptBpmService.handlerFunctionConform(ptCheck, executor);
        // 原型確認狀態更新為 5 功能符合需求
        ptCheck.setPtStatus(PtStatus.FUNCTION_CONFORM);
        // 待閱讀資訊更新
        ptCheck.setReadReason(WaitReadReasonType.PROTOTYPE_FUNCTION_CONFORM);
        ptCheck.setReadUpdateDate(new Date());
        ptCheck.setFinishDate(new Date());
        ptDao.save(ptCheck);
        // 新增追蹤記錄
        Date traceDate = rtService.createPtFunctionConformTrace(require.getSid(), executor).getCreatedDate();
        // 更新需求單狀態
        require.setFunctionOkCode(Boolean.TRUE);
        require.setFunctionOkDate(traceDate);
        reqModifyDao.updateByPrototypeFunOk(
                require,
                require.getHasTrace(),
                require.getFunctionOkCode(),
                require.getFunctionOkDate(),
                executor,
                new Date());

        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.PTCHECK,
                ptCheck.getSid(),
                executor.getSid());
    }

    @Transactional(readOnly = true)
    public List<String> findSidsBySourceSid(String sourceSid) {
        return ptDao.findSidsBySourceTypeAndSourceSid(WorkSourceType.TECH_REQUEST, sourceSid);
    }

    @Transactional(readOnly = true)
    public List<PtCheck> findByPtNoIn(List<String> ptNoes) {
        return ptDao.findByPtNoIn(ptNoes);
    }

    public PtCheck findByPtNo(String ptNo) {
        return ptDao.findByPtNo(ptNo);
    }

    @Transactional(readOnly = true)
    public Integer findIncompleteCount(String requireNo) {
        List<PtStatus> status = Lists.newArrayList(
                PtStatus.APPROVE, PtStatus.PROCESS, PtStatus.REDO, PtStatus.SIGN_PROCESS);
        return ptDao.findCountBySourceTypeAndSourceNoAndStatus(WorkSourceType.TECH_REQUEST, requireNo, status);
    }

    @Transactional(readOnly = true)
    public Integer findConutByStatus(String requireNo, PtStatus status) {
        return ptDao.findCountBySourceTypeAndSourceNoAndStatus(WorkSourceType.TECH_REQUEST, requireNo, Lists.newArrayList(status));
    }

    @Transactional(readOnly = true)
    public PtStatus findPtStatus(PtCheck ptCheck) {
        return ptDao.findPtStatus(ptCheck);
    }

    @Transactional(readOnly = true)
    public List<PtCheck> initTabInfo(Require require) {
        if (require == null || Strings.isNullOrEmpty(require.getSid())) {
            return Lists.newArrayList();
        }
        List<PtCheck> pcs = ptDao.findBySourceSidOrderByCreatedDateDesc(require.getSid());
        List<PtHistory> historys = ptHistoryDao.findByPtNoIn(pcs.stream().map(each -> each.getPtNo()).collect(Collectors.toList()));
        if (historys == null) {
            historys = Lists.newArrayList();
        }
        List<PtAlreadyReply> replys = Lists.newArrayList();
        List<PtAttachment> attachs = Lists.newArrayList();
        if (!historys.isEmpty()) {
            List<String> historySids = historys.stream().map(each -> each.getSid()).collect(Collectors.toList());
            replys = aReplyDao.findReplyByHistoryInOrderByUpdateDateDesc(historySids);
            attachs = attachDao.findAttachByHistoryInOrderByUpdateDateDesc(historySids);
        }
        Map<String, List<PtAlreadyReply>> replysMapKeyIsReplySid = replys.stream().collect(Collectors.groupingBy(each -> each.getReply().getSid()));
        historys.stream()
                .filter(each -> each.getReply() != null)
                .filter(each -> replysMapKeyIsReplySid.containsKey(each.getReply().getSid()))
                .forEach(each -> each.getReply().setAlreadyReplys(replysMapKeyIsReplySid.get(each.getReply().getSid())));
        Map<String, List<PtAttachment>> attachMapKeyIsHistorySid = attachs.stream().collect(Collectors.groupingBy(each -> each.getHistory().getSid()));
        historys.stream()
                .filter(each -> attachMapKeyIsHistorySid.containsKey(each.getSid()))
                .forEach(each -> each.setAttachments(attachMapKeyIsHistorySid.get(each.getSid())));
        Map<String, List<PtHistory>> historysMapKeyIsPtNo = historys.stream().collect(Collectors.groupingBy(PtHistory::getPtNo));
        pcs.stream()
                .filter(op -> historysMapKeyIsPtNo.containsKey(op.getPtNo()))
                .forEach(op -> op.setHistorys(
                        historysMapKeyIsPtNo.get(op.getPtNo()).stream()
                                .filter(history -> !history.getBehavior().equals(PtHistoryBehavior.REPLY_AND_REPLY))
                                .collect(Collectors.toList())));
        return pcs;
    }

    /**
     * 轉換為自訂物件
     *
     * @param ptNo
     * @return
     */
    public PtCheckTo findToByPtNo(String ptNo) {
        PtCheck obj = this.findByPtNo(ptNo);
        if (obj == null) {
            log.error("查無該ptNo資料，ptNo單號：" + ptNo);
            return null;
        }
        return convert2To(obj);
    }

    public PtCheckTo convert2To(PtCheck ptCheck) {
        if (ptCheck == null) {
            return null;
        }
        PtCheckTo to = new PtCheckTo();
        to.setSid(ptCheck.getSid());
        to.setPtNo(ptCheck.getPtNo());
        to.setSourceNo(ptCheck.getSourceNo());
        to.setNoticeMember(ptCheck.getNoticeMember());
        to.setNoticeDeps(ptCheck.getNoticeDeps());
        to.setHasSign(ptCheck.getHasSign());
        to.setTheme(ptCheck.getTheme());
        to.setContent(ptCheck.getContent());
        to.setContentCss(ptCheck.getContentCss());
        to.setNote(ptCheck.getNote());
        to.setNoteCss(ptCheck.getNoteCss());
        to.setEstablishDate(ptCheck.getEstablishDate());
        to.setFinishDate(ptCheck.getFinishDate());
        to.setRedoDate(ptCheck.getRedoDate());
        to.setVersion(ptCheck.getVersion());
        to.setPtStatus(ptCheck.getPtStatus());
        to.setUpdatedDate(ptCheck.getUpdatedDate());

        if (ptCheck.getCreateDep() != null) {
            to.setCreateDepSid(ptCheck.getCreateDep().getSid());
        }

        if (ptCheck.getCreatedUser() != null) {
            to.setCreateUsr(ptCheck.getCreatedUser().getSid());
        }

        return to;
    }

    /**
     * 在存檔前進行檢核
     *
     * @param tempTo   副本
     * @param dbSource DB即時資料
     * @throws PtEditExceptions
     */
    private void checkPtCheckByBeforeSave(PtCheckTo tempTo, PtCheck dbSource) throws PtEditExceptions {
        if (tempTo.getUpdatedDate().compareTo(dbSource.getUpdatedDate()) != 0) {
            StringBuilder errorMsg = new StringBuilder();
            if (!this.checkSameByStringListTo(tempTo.getNoticeDeps().getValue(), dbSource.getNoticeDeps().getValue())) {
                errorMsg.append("通知單位、");
            }
            if (!this.checkSameByStringListTo(tempTo.getNoticeMember().getValue(), dbSource.getNoticeMember().getValue())) {
                errorMsg.append("通知對象、");
            }
            if (!tempTo.getTheme().equals(dbSource.getTheme())) {
                errorMsg.append("主題、");
            }
            if (!tempTo.getContentCss().equals(dbSource.getContentCss())) {
                errorMsg.append("內容、");
            }
            if (!tempTo.getNoteCss().equals(dbSource.getNoteCss())) {
                errorMsg.append("備註、");
            }
            if (dbSource.getEstablishDate() != null
                    && tempTo.getEstablishDate().compareTo(dbSource.getEstablishDate()) != 0) {
                errorMsg.append("預計完成日、");
            }
            if (dbSource.getFinishDate() != null
                    && tempTo.getFinishDate().compareTo(dbSource.getFinishDate()) != 0) {
                errorMsg.append("完成日、");
            }
            if (dbSource.getRedoDate() != null
                    && tempTo.getRedoDate().compareTo(dbSource.getRedoDate()) != 0) {
                errorMsg.append("重做日期、");
            }
            if (!tempTo.getPtStatus().equals(dbSource.getPtStatus())) {
                errorMsg.append("狀態、");
            }
            if (dbSource.getHasSign() != null
                    && !tempTo.getHasSign().equals(dbSource.getHasSign())) {
                errorMsg.append("流程、");
            }
            if (dbSource.getVersion() != null
                    && !tempTo.getVersion().equals(dbSource.getVersion())) {
                errorMsg.append("版本 、");
            }
            if (errorMsg.length() != 0) {
                String errMsg = "Pt單號：" + tempTo.getPtNo() + " ，來源單號：" + tempTo.getSourceNo();
                errMsg += "<BR/>異動欄位：" + errorMsg.toString().substring(0, errorMsg.length() - 1);
                throw new PtEditExceptions(errMsg);
            }
        }
    }

    /**
     * 判斷通知是否相同
     *
     * @param noticeByTemp
     * @param noticeByDb
     * @return
     */
    private boolean checkSameByStringListTo(List<String> noticeByTemp, List<String> noticeByDb) {
        if (noticeByTemp.size() != noticeByDb.size()) {
            return false;
        }
        return noticeByTemp.stream()
                .allMatch(each -> noticeByDb.contains(each));
    }

    /**
     * onpg存檔只部份更新
     *
     * @param obj
     * @param executor
     */
    @Transactional(rollbackFor = Exception.class)
    private void modifyByEditPtCheck(PtCheck obj, User executor) {
        PtCheck backupOnpg = this.findByPtNo(obj.getPtNo());
        backupOnpg.setNoticeMember(obj.getNoticeMember());
        backupOnpg.setNoticeDeps(obj.getNoticeDeps());
        backupOnpg.setHasSign(obj.getHasSign());
        backupOnpg.setTheme(obj.getTheme());
        backupOnpg.setContent(obj.getContent());
        backupOnpg.setContentCss(obj.getContentCss());
        backupOnpg.setNote(obj.getNote());
        backupOnpg.setNoteCss(obj.getNoteCss());
        backupOnpg.setEstablishDate(obj.getEstablishDate());
        backupOnpg.setReadUpdateDate(obj.getReadUpdateDate());
        backupOnpg.setReadReason(obj.getReadReason());
        backupOnpg.setReadRecord(obj.getReadRecord());
        backupOnpg.setUpdatedDate(new Date());
        backupOnpg.setUpdatedUser(executor);
        ptDao.save(backupOnpg);
    }

    /**
     * 複製
     *
     * @param obj
     */
    public PtCheck copy(PtCheck obj) {
        PtCheck backupObj = this.findByPtNo(obj.getPtNo());
        if (obj.getUpdatedDate().compareTo(backupObj.getUpdatedDate()) == 0) {
            return obj;
        }
        obj.setNoticeMember(backupObj.getNoticeMember());
        obj.setNoticeDeps(backupObj.getNoticeDeps());
        obj.setHasSign(backupObj.getHasSign());
        obj.setTheme(backupObj.getTheme());
        obj.setContent(backupObj.getContent());
        obj.setContentCss(backupObj.getContentCss());
        obj.setNote(backupObj.getNote());
        obj.setNoteCss(backupObj.getNoteCss());
        obj.setEstablishDate(backupObj.getEstablishDate());
        obj.setFinishDate(backupObj.getFinishDate());
        obj.setRedoDate(backupObj.getRedoDate());
        obj.setVersion(backupObj.getVersion());
        obj.setReadUpdateDate(backupObj.getReadUpdateDate());
        obj.setReadReason(backupObj.getReadReason());
        obj.setReadRecord(backupObj.getReadRecord());
        obj.setPtStatus(backupObj.getPtStatus());
        obj.setCreateCompany(backupObj.getCreateCompany());
        obj.setCreateDep(backupObj.getCreateDep());
        obj.setStatus(backupObj.getStatus());
        obj.setCreatedUser(backupObj.getCreatedUser());
        obj.setCreatedDate(backupObj.getCreatedDate());
        obj.setUpdatedUser(backupObj.getUpdatedUser());
        obj.setUpdatedDate(backupObj.getUpdatedDate());

        // obj.setSignInfo(backupObj.getSignInfo());
        // obj.setHistorys(backupObj.getHistorys());
        // obj.setReplys(backupObj.getReplys());
        // obj.setAttachments(backupObj.getAttachments());
        return obj;
    }

    /**
     * 批次轉單程式-建立單位轉發(原型確認)
     * 
     * @return
     */
    public List<TransRequireVO> findPtByCreateDeptAndUser(List<Integer> deptSids, List<Integer> userSids) {
        List<TransRequireVO> resultList = trNamedQueryRepository.findPtByCreateDeptAndUser(deptSids, userSids);
        return convert(resultList);
    }

    /**
     * 批次轉單程式-通知單位轉發(原型確認)
     * 
     * @return
     */
    public List<TransRequireVO> findPtByNotifiedDept(String depts) {
        List<TransRequireVO> resultList = trNamedQueryRepository.findPtByNotifiedDept(depts);
        return convert(resultList);
    }

    private List<TransRequireVO> convert(List<TransRequireVO> resultList) {
        for (TransRequireVO vo : resultList) {
            vo.setType(RequireTransProgramType.PTCHECK.name());
            vo.setCreateDept(WkOrgCache.getInstance().findNameBySid(vo.getDeptSid()));
            vo.setCreateUserName(WkUserCache.getInstance().findBySid(vo.getUserSid()).getId());
            vo.setStatus(PtStatus.valueOf(vo.getStatus()).getLabel());
        }
        return resultList;
    }

    /**
     * 依據需求單號查詢
     * 
     * @param sourceNo
     */
    public List<PtCheckTo> findBySourceNo(String sourceNo) {
        // 查詢
        List<PtCheck> ptChecks = this.ptDao.findBySourceAndStatusDescVer(
                WorkSourceType.TECH_REQUEST,
                sourceNo, Activation.ACTIVE);

        // 轉To
        List<PtCheckTo> results = Lists.newArrayList();
        if (ptChecks != null) {
            for (PtCheck ptCheck : ptChecks) {
                results.add(this.convert2To(ptCheck));
            }
        }

        return results;
    }

    /**
     * 依據需求單號查詢
     * 
     * @param sourceNo
     */
    public List<PtCheckTo> findByRequireNoAndCreateDepSids(String requireNo, List<Integer> depSids) {

        // ====================================
        // 檢查傳入參數為空
        // ====================================
        if (WkStringUtils.isEmpty(requireNo) || WkStringUtils.isEmpty(depSids)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 轉 org
        // ====================================
        List<Org> deps = Lists.newArrayList();
        for (Integer depSid : depSids) {
            Org dep = WkOrgCache.getInstance().findBySid(depSid);
            if (dep != null) {
                deps.add(dep);
            }
        }

        // ====================================
        // 查詢
        // ====================================
        List<PtCheck> ptChecks = this.ptDao.findBySourceTypeAndStatusAndSourceNoAndCreateDepIn(
                WorkSourceType.TECH_REQUEST,
                Activation.ACTIVE,
                requireNo,
                deps);

        // ====================================
        // 轉To
        // ====================================
        List<PtCheckTo> results = Lists.newArrayList();
        if (ptChecks != null) {
            for (PtCheck ptCheck : ptChecks) {
                results.add(this.convert2To(ptCheck));
            }
        }

        return results;
    }

    /**
     * 計算完成或未完成的單據
     * 
     * @param sourceNo   需求單號
     * @param createDeps 過濾建立部門 (為空時不過濾)
     * @param isComplete true:完成 / false:未完成
     * @return
     */
    public Integer countByCompleteStatus(
            String sourceNo,
            List<Org> createDeps,
            boolean isComplete) {

        // 取得本次所有需要過濾的狀態
        List<PtStatus> filterStatus = Lists.newArrayList(PtStatus.values()).stream()
                .filter(ptStatus -> isComplete == ptStatus.isInFinish())
                .collect(Collectors.toList());

        // 需過濾建立部門
        if (WkStringUtils.notEmpty(createDeps)) {
            return this.ptDao.queryCountByCreateDepAndPtStatus(
                    WorkSourceType.TECH_REQUEST,
                    sourceNo,
                    createDeps,
                    filterStatus);
        }

        // 僅檢查狀態
        return this.ptDao.queryCountByPtStatus(
                WorkSourceType.TECH_REQUEST,
                sourceNo,
                filterStatus);
    }

    /**
     * 取得完成或未完成的單據
     * 
     * @param sourceNo   需求單號
     * @param createDeps 過濾建立部門 (為空時不過濾)
     * @param isComplete true:完成 / false:未完成
     * @return
     */
    public List<PtCheck> queryByCompleteStatus(
            String sourceNo,
            List<Org> createDeps,
            boolean isComplete) {

        // 取得本次所有需要過濾的狀態
        List<PtStatus> filterStatus = Lists.newArrayList(PtStatus.values()).stream()
                .filter(ptStatus -> isComplete == ptStatus.isInFinish())
                .collect(Collectors.toList());

        // 依據建立部門過濾
        return this.ptDao.queryByCreateDepAndPtStatus(WorkSourceType.TECH_REQUEST, sourceNo, createDeps, filterStatus);
    }

    /**
     * 強制完成
     * 
     * @param require             需求單主檔
     * @param forceCompleteStatus 強制完成類型
     * @param execUser            執行者
     * @param execDate            執行時間
     * @param createDeps          指定立案單位 （傳入null時,代表不限定）
     * @return 已強制結案單據資料
     */
    public List<PtCheck> forceComplete(
            Require require,
            PtStatus forceCompleteStatus,
            User execUser,
            Date execDate,
            List<Org> createDeps) {

        if (forceCompleteStatus == null || !forceCompleteStatus.isForceCompleteStatus()) {
            throw new SystemDevelopException("傳入的 PtStatus 不是可以處理的類型：[" + forceCompleteStatus + "]", InfomationLevel.ERROR);
        }

        // ====================================
        // 取得所有未完成單據
        // ====================================
        // 收集未完成狀態
        List<PtStatus> notCompleteStatus = Lists.newArrayList(PtStatus.values()).stream()
                .filter(ptStatus -> !ptStatus.isInFinish())
                .collect(Collectors.toList());

        List<PtCheck> ptChecks = null;

        if (createDeps == null) {
            // 取得需求單下所有子單
            ptChecks = this.ptDao.findBySourceTypeAndStatusAndSourceNoAndPtStatusIn(
                    WorkSourceType.TECH_REQUEST,
                    Activation.ACTIVE,
                    require.getRequireNo(),
                    notCompleteStatus);
        } else {
            // 取得需求單下傳入單位開立的子單
            ptChecks = this.ptDao.findBySourceTypeAndStatusAndSourceNoAndPtStatusInAndCreateDepIn(
                    WorkSourceType.TECH_REQUEST,
                    Activation.ACTIVE,
                    require.getRequireNo(),
                    notCompleteStatus,
                    createDeps);
        }

        if (WkStringUtils.isEmpty(ptChecks)) {
            log.debug("沒有需要關閉的原型確認單");
            return Lists.newArrayList();
        }

        // ====================================
        // 逐筆完成
        // ====================================
        for (PtCheck ptCheck : ptChecks) {

            log.info("[{}]:原型確認單[{}]因 {} 而關閉!",
                    ptCheck.getSourceNo(),
                    ptCheck.getPtNo(),
                    forceCompleteStatus.getLabel());

            // 流程部份不處理, 因強制結案前, 會檢核簽核完畢
            // 原型確認檔更新
            ptCheck.setPtStatus(forceCompleteStatus);
            // ptCheck.setStatus(Activation.INACTIVE);
            ptCheck.setReadReason(WaitReadReasonType.FORCE_CLOSE);
            ptCheck.setReadUpdateDate(execDate);
            ptCheck.setUpdatedUser(execUser);
            ptCheck.setUpdatedDate(execDate);
            ptDao.save(ptCheck);

            // 建立歷程
            pthService.saveForceCloceHistory(ptCheck, execUser);

            // ====================================
            // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
            // ====================================
            this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                    FormType.PTCHECK,
                    ptCheck.getSid(),
                    execUser.getSid());
        }

        return ptChecks;

    }

    /**
     * 更新通知單位 + 建立異動記錄 + 首頁推撥
     * 
     * @param requireSid      require Sid
     * @param requireNo       require No
     * @param ptNo            原型確認單號
     * @param afterNoticeDeps 異動後的通知單位
     * @param executor        執行人員
     * @throws UserMessageException
     */
    @Transactional(rollbackFor = Exception.class)
    public void modifyNoticeDeps(
            String requireSid,
            String requireNo,
            String ptNo,
            List<String> afterNoticeDeps,
            User executor) throws UserMessageException {

        if (afterNoticeDeps == null) {
            afterNoticeDeps = Lists.newArrayList();
        }

        // ====================================
        // 查詢目前 DB 資料
        // ====================================
        // 查詢原型確認主檔
        PtCheck ptCheck = this.findByPtNo(ptNo);
        if (ptCheck == null) {
            throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }

        // 異動前資料(拷貝一份, 與 hibernate 脫勾)
        PtCheck beforePtCheck = new PtCheck();
        BeanUtils.copyProperties(ptCheck, beforePtCheck);

        // 取得異動前通知部門
        List<String> beforeNoticeDeps = Lists.newArrayList();
        if (ptCheck.getNoticeDeps() != null && ptCheck.getNoticeDeps().getValue() != null) {
            beforeNoticeDeps = ptCheck.getNoticeDeps().getValue();
        }

        // 未異動時 pass
        if (WkCommonUtils.compare(beforeNoticeDeps, afterNoticeDeps)) {
            log.info("未異動通知部門");
            return;
        }

        // ====================================
        // 更新主檔
        // ====================================
        // 更新異動單位 (update noticeDeps)
        ptCheck.setNoticeDeps(new JsonStringListTo(afterNoticeDeps));

        // update
        this.ptDao.save(ptCheck);

        // ====================================
        // 更新讀記錄 (已包含異動通知單位需新增的人員)
        // 1.執行者 -> 已讀
        // 2.填單單位+通知單位人員+通知人員 -> 待閱讀
        // 3.刪除被移除的『通知單位人員』和『通知人員』
        // ====================================
        this.requireReadRecordHelper.resetUserList(
                FormType.PTCHECK,
                ptCheck.getSid(),
                this.prepareReadRecordUserSids(ptCheck),
                executor.getSid());

        // ====================================
        // 建立通知單位 異動紀錄
        // ====================================
        this.subNoticeInfoService.createSubChangeRecord(
                SubNoticeType.PT_NOTICE_DEP,
                requireSid,
                requireNo,
                ptCheck.getSid(),
                ptCheck.getPtNo(),
                new JsonStringListTo(beforeNoticeDeps),
                new JsonStringListTo(afterNoticeDeps),
                executor.getSid());

        // ====================================
        // 處理系統通知 (異動原型確認通知對象、單位)
        // ====================================
        try {
            this.processSysNotify(requireSid, beforePtCheck, ptCheck, executor.getSid());
        } catch (Exception e) {
            log.error("執行系統通知失敗!" + e.getMessage(), e);
        }
    }

    /**
     * 準備閱讀記錄 (填單單位+通知單位+通知人員 )
     *
     * @param onpg
     * @return
     */
    private Set<Integer> prepareReadRecordUserSids(PtCheck ptCheck) {

        // ====================================
        // 要被通知的單位
        // ====================================
        Set<Integer> allDepSids = Sets.newHashSet();

        // 建單單位
        if (ptCheck.getCreateDep() != null) {
            allDepSids.add(ptCheck.getCreateDep().getSid());
        }

        // 通知單位
        Set<Integer> noticeDepSids = ptCheck.getNoticeDepSids();
        if (WkStringUtils.notEmpty(noticeDepSids)) {
            allDepSids.addAll(noticeDepSids);
        }

        // ====================================
        // 取得部門下所有 user
        // ====================================
        Set<Integer> allUserSids = WkUserCache.getInstance().findUserSidByOrgsWithManager(
                allDepSids, Activation.ACTIVE);

        // ====================================
        // 為組時，把部長拉進來 (組織扁平化調整 原最高為處, 降為部)
        // ====================================
        if (ptCheck.getCreateDep() != null
                && ptCheck.getCreateDep().getParent() != null
                && OrgLevel.THE_PANEL.equals(ptCheck.getCreateDep().getLevel())) {

            Set<Integer> managerUserSids = WkOrgUtils.findOrgManagerUserSidByOrgSid(
                    ptCheck.getCreateDep().getParent().getSid());
            if (WkStringUtils.notEmpty(managerUserSids)) {
                allUserSids.addAll(managerUserSids);
            }
        }

        // ====================================
        // 通知人員
        // ====================================
        Set<Integer> noticeMemberSids = ptCheck.getNoticeMemberSids();
        if (WkStringUtils.notEmpty(noticeMemberSids)) {
            allUserSids.addAll(noticeMemberSids);
        }

        // 移除停用人員
        return allUserSids.stream()
                .filter(userSid -> WkUserUtils.isActive(userSid))
                .collect(Collectors.toSet());
    }

}
