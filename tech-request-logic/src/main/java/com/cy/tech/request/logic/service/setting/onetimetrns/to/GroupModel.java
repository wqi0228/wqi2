package com.cy.tech.request.logic.service.setting.onetimetrns.to;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class GroupModel implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 712381193267661081L;
    private String groupSid;
    private Integer createdUser;
    private Date createdDate;
    private String groupName;
    private String groupNote;
    private String groupInfoStr;
}
