/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.feedback;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.RequireTraceService;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.repository.require.feedback.ReqFbReplyAttachRepo;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.require.feedback.ReqFbkReply;
import com.cy.tech.request.vo.require.feedback.ReqFbkReplyAttach;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkAttachUtils;
import com.cy.work.common.vo.AttachmentService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 需求資訊補充回覆 附加檔案服務
 *
 * @author shaun
 */
@Component("req_fbk_reply_attach")
public class ReqFbkReplyAttachService implements AttachmentService<ReqFbkReplyAttach, ReqFbkReply, String>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7070237597947412007L;
    @Value("${erp.attachment.root}")
    private String attachPath;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private UserService userService;
    @Autowired
    private RequireTraceService traceService;
    @Autowired
    private ReqFbReplyAttachRepo attachDao;
    @Autowired
    private WkAttachUtils attachUtils;

    @Transactional(readOnly = true)
    @Override
    public ReqFbkReplyAttach findBySid(String sid) {
        ReqFbkReplyAttach a = attachDao.findOne(sid);
        return a;
    }

    @Transactional(readOnly = true)
    @Override
    public List<ReqFbkReplyAttach> findAttachsByLazy(ReqFbkReply reply) {
        try {
            reply.getAttachments().size();
        } catch (LazyInitializationException e) {
            //log.debug("findAttachsByLazy lazy init error :" + e.getMessage());
            reply.setAttachments(this.findAttachs(reply));
        }
        return reply.getAttachments();
    }

    @Transactional(readOnly = true)
    @Override
    public List<ReqFbkReplyAttach> findAttachs(ReqFbkReply reply) {
        if (Strings.isNullOrEmpty(reply.getSid())) {
            if (reply.getAttachments() == null) {
                reply.setAttachments(Lists.newArrayList());
            }
            return reply.getAttachments();
        }
        return attachDao.findByReplyAndStatusOrderByCreatedDateDesc(reply, Activation.ACTIVE);
    }

    @Override
    public String getAttachPath() {
        return attachPath;
    }

    @Override
    public String getDirectoryName() {
        return "tech-request";
    }

    @Override
    public ReqFbkReplyAttach createEmptyAttachment(String fileName, User executor, Org dep) {
        return (ReqFbkReplyAttach) attachUtils.createEmptyAttachment(new ReqFbkReplyAttach(), fileName, executor, dep);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public ReqFbkReplyAttach updateAttach(ReqFbkReplyAttach attach) {
        ReqFbkReplyAttach a = attachDao.save(attach);
        return a;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveAttach(ReqFbkReplyAttach attach) {
        attachDao.save(attach);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveAttach(List<ReqFbkReplyAttach> attachs) {
        attachDao.save(attachs);
    }

    @Override
    public String depAndUserName(ReqFbkReplyAttach attachment) {
        if (attachment == null) {
            return "";
        }
        User usr = WkUserCache.getInstance().findBySid(attachment.getCreatedUser().getSid());
        Org dep = orgService.findBySid(usr.getPrimaryOrg().getSid());
        String parentDepStr = orgService.showParentDep(dep);
        return parentDepStr + "-" + usr.getName();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void linkRelation(List<ReqFbkReplyAttach> attachments, ReqFbkReply e, User login) {
        attachments.forEach(each -> this.setRelation(each, e));
        this.saveAttach(attachments);
        e.setAttachments(this.findAttachs(e));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void linkRelation(ReqFbkReplyAttach ra, ReqFbkReply e, User login) {
        this.setRelation(ra, e);
        this.updateAttach(ra);
    }

    private void setRelation(ReqFbkReplyAttach ra, ReqFbkReply reply) {
        ra.setReply(reply);
        ra.setTrace(reply.getTrace());
        ra.setRequire(reply.getRequire());
        ra.setRequireNo(reply.getRequireNo());
        if (!ra.getKeyChecked()) {
            ra.setStatus(Activation.INACTIVE);
        }
        ra.setKeyChecked(Boolean.FALSE);
    }

    /**
     * 刪除
     *
     * @param attachments
     * @param attachment
     * @param reply
     * @param login
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void changeStatusToInActive(List<ReqFbkReplyAttach> attachments, ReqFbkReplyAttach attachment, ReqFbkReply reply, User login) {
        ReqFbkReplyAttach ra = (ReqFbkReplyAttach) attachment;
        ra.setStatus(Activation.INACTIVE);
        this.updateAttach(attachment);
        reply.setAttachments(this.findAttachs(reply));
        this.createDeleteTrace(attachment, reply, login);
    }

    private void createDeleteTrace(ReqFbkReplyAttach attachment, ReqFbkReply reply, User login) {
        String content = ""
                + "檔案名稱：【" + attachment.getFileName() + "】\n"
                + "上傳成員：【" + userService.getUserName(attachment.getCreatedUser()) + "】\n"
                + "\n"
                + "刪除來源：【需求資訊補充回覆 － 單號：" + reply.getRequireNo() + "】\n"
                + "刪除成員：【" + login.getName() + "】\n"
                + "刪除註記：" + attachment.getSid();
        traceService.createRequireTrace(reply.getRequire().getSid(), login, RequireTraceType.DELETE_ATTACHMENT, content);
    }

}
