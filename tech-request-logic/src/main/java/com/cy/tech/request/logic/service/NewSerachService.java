package com.cy.tech.request.logic.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.repository.category.SimpleBigCategoryRepository;
import com.cy.tech.request.vo.category.SimpleBigCategory;
import com.cy.tech.request.vo.category.SimpleBigCategoryVO;
import com.cy.tech.request.vo.constants.CacheConstants;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

/**
 * @author allen1214_wu
 *
 */
@Service
public class NewSerachService {

    /**
     * 
     */
    @Autowired
    private SimpleBigCategoryRepository bigCategoryDAO;

    /**
     * 查詢所有大類
     */
    @Cacheable(cacheNames = CacheConstants.CACHE_findActiveBigCategory)
    public List<SimpleBigCategoryVO> findActiveBigCategory() {

        // ====================================
        // 查詢
        // ====================================
        List<SimpleBigCategory> results = this.bigCategoryDAO.findByStatusOrderByIdAsc(Activation.ACTIVE);

        if (WkStringUtils.isEmpty(results)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 轉型
        // ====================================
        return results.stream()
                .map(entity -> new SimpleBigCategoryVO(entity))
                .collect(Collectors.toList());
    }
}
