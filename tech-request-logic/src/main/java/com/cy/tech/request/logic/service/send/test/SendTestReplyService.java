/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.send.test;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.RequireReadRecordHelper;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.repository.worktest.WorkTestAlreadyReplyRepo;
import com.cy.tech.request.repository.worktest.WorkTestReplyRepo;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.worktest.WorkTestAlreadyReply;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.vo.worktest.WorkTestReply;
import com.cy.tech.request.vo.worktest.enums.WorkTestInfoHistoryBehavior;
import com.cy.tech.request.vo.worktest.enums.WorkTestInfoStatus;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkJsoupUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.primitives.Longs;

/**
 * 送測回覆處理
 *
 * @author shaun
 */
@Component
public class SendTestReplyService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5408851791689082959L;
    @Autowired
    private RequireService reqService;
    @Autowired
    private SendTestService stService;
    @Autowired
    private SendTestShowService stsService;
    @Autowired
    private SendTestAlertService staService;
    @Autowired
    private SendTestHistoryService sthService;
    @Autowired
    private transient RequireReadRecordHelper requireReadRecordHelper;
    @Autowired
    private WkJsoupUtils jsoupUtils;
    @Autowired
    private WorkTestReplyRepo replyDao;
    @Autowired
    private WorkTestAlreadyReplyRepo aReplyDao;

    @Transactional(readOnly = true)
    public List<WorkTestReply> findReply(WorkTestInfo testInfo) {
        try {
            testInfo.getReplys().size();
        } catch (LazyInitializationException e) {
            // log.debug("findReply lazy init error :" + e.getMessage(), e);
            testInfo.setReplys(replyDao.findByTestInfo(testInfo));
        }
        return testInfo.getReplys();
    }

    /**
     * 尋找原型確認對應的回覆內容
     *
     * @param reply
     * @return
     */
    @Transactional(readOnly = true)
    public List<WorkTestAlreadyReply> findAlreadyReplys(WorkTestReply reply) {
        if (reply == null || Strings.isNullOrEmpty(reply.getSid())) {
            return Lists.newArrayList();
        }
        try {
            reply.getAlreadyReplys().size();
        } catch (LazyInitializationException e) {
            // log.debug("findAlreadyReplys lazy init error :" + e.getMessage(), e);
            reply.setAlreadyReplys(aReplyDao.findByReplyOrderByUpdateDateDesc(reply));
        }
        return reply.getAlreadyReplys();
    }

    public WorkTestReply createEmptyReply(WorkTestInfo info, User executor) {
        WorkTestReply reply = new WorkTestReply();
        reply.setTestInfo(info);
        reply.setTestinfoNo(info.getTestinfoNo());
        reply.setSourceType(info.getSourceType());
        reply.setSourceSid(info.getSourceSid());
        reply.setSourceNo(info.getSourceNo());
        reply.setDep(WkOrgCache.getInstance().findBySid(executor.getPrimaryOrg().getSid()));
        reply.setPerson(executor);
        reply.setDate(new Date());
        reply.setUpdateDate(new Date());
        reply.setHistory(sthService.createEmptyHistory(info, executor));
        reply.getHistory().setBehavior(WorkTestInfoHistoryBehavior.REPLY);
        return reply;
    }

    @Transactional(rollbackFor = Exception.class)
    public WorkTestInfo saveByNewReply(WorkTestReply editReply) throws IllegalAccessException {
        if (stsService.disableTestReply(
                reqService.findByReqSid(editReply.getSourceSid()),
                stService.findByTestinfoNo(editReply.getTestinfoNo()),
                editReply.getPerson())) {
            throw new IllegalAccessException("無權限進行操作");
        }
        editReply.setContent(jsoupUtils.clearCssTag(editReply.getContentCss()));
        WorkTestReply nR = replyDao.save(editReply);
        WorkTestInfo testInfo = stService.findByTestinfoNo(editReply.getTestInfo().getTestinfoNo());
        this.changeStatus(testInfo);

        testInfo.setReadReason(WaitReadReasonType.TEST_HAS_REPLY);
        testInfo.setReadUpdateDate(new Date());
        sthService.createReplyHistory(nR, editReply.getPerson());
        stService.save(testInfo, editReply.getPerson());
        sthService.sortHistory(testInfo);

        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.WORKTESTSIGNINFO,
                testInfo.getSid(),
                editReply.getPerson().getSid());

        return testInfo;
    }

    /**
     * 回覆時狀態更改為測試中
     * 送測狀態, 不需審核 or 不納入排程
     * 重測狀態, 不需審核 or 不納入排程 or (納入排程且文件提交)
     * 
     * @param testInfo
     */
    private void changeStatus(WorkTestInfo testInfo) {
        WorkTestStatus wtStatus = testInfo.getTestinfoStatus();

        boolean isApproved = WorkTestInfoStatus.APPROVED.equals(testInfo.getQaAuditStatus());
        boolean isUnjoinSchedule = WorkTestInfoStatus.UNJOIN_SCHEDULE.equals(testInfo.getQaScheduleStatus());
        boolean isUnneededApprove = WorkTestInfoStatus.UNNEEDED_APPROVE.equals(testInfo.getQaAuditStatus());
        if (wtStatus.equals(WorkTestStatus.SONGCE)) {
            if (isUnneededApprove || (isApproved && isUnjoinSchedule)) {
                testInfo.setTestinfoStatus(WorkTestStatus.TESTING);
            }
        } else if (wtStatus.equals(WorkTestStatus.RETEST)) {
            boolean isJoinSchedule = WorkTestInfoStatus.JOIN_SCHEDULE.equals(testInfo.getQaScheduleStatus());
            boolean isCommited = WorkTestInfoStatus.COMMITED.equals(testInfo.getCommitStatus());
            if (isUnneededApprove || (isApproved && isUnjoinSchedule) || (isApproved && isJoinSchedule && isCommited)) {
                testInfo.setTestinfoStatus(WorkTestStatus.TESTING);
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public WorkTestInfo saveByEditReply(WorkTestReply editReply, User executor) throws IllegalAccessException {
        if (stsService.disableTestReply(
                reqService.findByReqSid(editReply.getSourceSid()),
                stService.findByTestinfoNo(editReply.getTestinfoNo()),
                editReply.getPerson())
                || aReplyDao.findByReplyOrderByUpdateDateDesc(editReply).size() > 0) {
            throw new IllegalAccessException("無權限進行操作");
        }
        editReply.setContent(jsoupUtils.clearCssTag(editReply.getContentCss()));
        editReply.setUpdateDate(new Date());
        sthService.update(editReply.getHistory(), executor);
        WorkTestInfo testInfo = stService.findByTestinfoNo(editReply.getTestInfo().getTestinfoNo());
        testInfo.setReadUpdateDate(new Date());
        replyDao.save(editReply);
        stService.save(testInfo, editReply.getPerson());
        sthService.sortHistory(testInfo);

        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.WORKTESTSIGNINFO,
                testInfo.getSid(),
                editReply.getPerson().getSid());

        return testInfo;
    }

    public WorkTestAlreadyReply createEmptyAlreadyReply(WorkTestReply reply, User executor) {
        WorkTestAlreadyReply areply = new WorkTestAlreadyReply();
        areply.setSourceType(reply.getSourceType());
        areply.setTestInfo(reply.getTestInfo());
        areply.setTestinfoNo(reply.getTestinfoNo());
        areply.setSourceSid(reply.getSourceSid());
        areply.setSourceNo(reply.getSourceNo());
        areply.setReply(reply);
        areply.setDep(WkOrgCache.getInstance().findBySid(executor.getPrimaryOrg().getSid()));
        areply.setPerson(executor);
        areply.setDate(new Date());
        areply.setUpdateDate(new Date());
        areply.setHistory(sthService.createEmptyHistory(reply.getTestInfo(), executor));
        areply.getHistory().setBehavior(WorkTestInfoHistoryBehavior.REPLY_AND_REPLY);
        return areply;
    }

    /**
     * 儲存新回覆
     *
     * @param aReply
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveByNewAlreadyReply(WorkTestAlreadyReply aReply) {
        aReply.setContent(jsoupUtils.clearCssTag(aReply.getContentCss()));
        aReplyDao.save(aReply);
        sthService.createAlreadyReplyHistory(aReply, aReply.getPerson());
        staService.carateReplyAlert(aReply);

        WorkTestReply reply = aReply.getReply();
        this.findAlreadyReplys(reply).add(aReply);
        this.sortAlreadyReplys(aReply.getReply());

        WorkTestInfo testInfo = aReply.getTestInfo();
        testInfo.setReadReason(WaitReadReasonType.TEST_HAS_PERSONAL_REPLY);
        testInfo.setReadUpdateDate(new Date());
        stService.save(testInfo, aReply.getPerson());
        sthService.sortHistory(testInfo);

        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.WORKTESTSIGNINFO,
                testInfo.getSid(),
                aReply.getPerson().getSid());
    }

    /**
     * 儲存編輯回覆
     *
     * @param eaReply
     * @param executor
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveByEditAlreadyReply(WorkTestAlreadyReply eaReply, User executor) {
        eaReply.setContent(jsoupUtils.clearCssTag(eaReply.getContentCss()));
        eaReply.setUpdateDate(new Date());
        sthService.update(eaReply.getHistory(), executor);
        aReplyDao.save(eaReply);
        WorkTestInfo testInfo = eaReply.getTestInfo();
        stService.save(testInfo, eaReply.getPerson());
        this.sortAlreadyReplys(eaReply.getReply());

        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.WORKTESTSIGNINFO,
                testInfo.getSid(),
                eaReply.getPerson().getSid());
    }

    /**
     * 重新排序回覆
     *
     * @param reply
     */
    private void sortAlreadyReplys(WorkTestReply reply) {
        List<WorkTestAlreadyReply> replys = this.findAlreadyReplys(reply);
        Ordering<WorkTestAlreadyReply> byReplyUpDateOrdering = new Ordering<WorkTestAlreadyReply>() {
            @Override
            public int compare(WorkTestAlreadyReply left, WorkTestAlreadyReply right) {
                return Longs.compare(right.getUpdateDate().getTime(), left.getUpdateDate().getTime());
            }
        };
        Collections.sort(replys, byReplyUpDateOrdering);
    }
}
