/**
 * 
 */
package com.cy.tech.request.logic.service.syncmms.helper;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.service.RequireTraceService;
import com.cy.tech.request.logic.service.onpg.OnpgService;
import com.cy.tech.request.logic.service.syncmms.to.SyncFormTo;
import com.cy.tech.request.vo.log.LogSyncMmsActionType;
import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.exception.alert.TechRequestAlertException;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Service
@Slf4j
public class SyncMmsSyncFormCreateOnpgHelper {

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private transient OnpgService onpgService;
    @Autowired
    private transient RequireTraceService requireTraceService;
    @Autowired
    private transient SyncMmsSyncFormCommonHelper syncMmsSyncFormCommonHelper;

    // ========================================================================
    // 主方法
    // ========================================================================
    /**
     * 僅新增ON程式單 (附掛於已存在需求單)
     * 
     * @param syncFormTo
     * @throws SystemOperationException
     */
    public void process(
            SyncFormTo syncFormTo)
            throws SystemOperationException {

        Date execDate = new Date();

        // 定義處理型態
        syncFormTo.setLogSyncMmsActionType(LogSyncMmsActionType.CREATE_ONPG);



        // ====================================
        // 建立ON程式單
        // ====================================
        try {
            WorkOnpg workOnpg = this.onpgService.createForMmsSync(
                    syncFormTo,
                    execDate);

            syncFormTo.setOnpgNo(workOnpg.getOnpgNo());
            syncFormTo.setWorkOnpgTo(this.onpgService.convert2To(workOnpg));

        } catch (UserMessageException e) {
            String errorMessage = "建立ON程式單失敗!" + e.getMessage();
            log.error(errorMessage, e);
            throw new TechRequestAlertException(errorMessage, syncFormTo.getLoginCompID());
        }
        
        // ====================================
        // 異動主單期望完成日
        // ====================================
        this.syncMmsSyncFormCommonHelper.postponeRequireHopeDate(
                syncFormTo, 
                syncFormTo.getOnpgNo());

        // ====================================
        // 寫需求單追蹤
        // ====================================
        this.requireTraceService.createSyncMmsTrace(syncFormTo);
    }

}
