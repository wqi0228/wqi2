package com.cy.tech.request.logic.vo;

import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * portal.xml 功能列表節點 Layount:west
 *
 * @author shaun
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class FunctionNodeTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1747774640867689600L;
    private String id;
    private String url;
    private String title;
    private String color;
    private String icon;
    private String cnt;
    private List<FunctionNodeTo> subs = Lists.newArrayList();

}
