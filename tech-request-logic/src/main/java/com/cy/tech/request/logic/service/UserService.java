package com.cy.tech.request.logic.service;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author jason_h
 */
@Component
@Slf4j
public class UserService implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1685975850308962630L;

    private static UserService instance;

    public static UserService getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        UserService.instance = this;
    }

    @Autowired
    private WkUserCache wkUserCache;

    @Autowired
    private OrganizationService orgService;

    public List<User> findByPrimaryOrg(Activation status, Org org) {
        return this.findByPrimary(org.getSid(), status);
    }

    public void clearCache() {
        wkUserCache.updateCache();
    }

    private List<User> findByPrimary(Integer orgSid, Activation status) {
        List<User> users = Lists.newArrayList();

        wkUserCache.findAll().forEach(each -> {
            if (status == null) {
                if (orgSid.equals(each.getPrimaryOrg().getSid())) {
                    users.add(each);
                }
            } else if (orgSid.equals(each.getPrimaryOrg().getSid()) && each.getStatus().equals(status)) {
                users.add(each);
            }
        });
        return users;
    }

    public List<User> findByStatusAndPrimaryOrgIn(Activation status, List<Org> primaryOrgs) {
        return this.findUserByPrimaryOrgIn(status, primaryOrgs);
    }

    public List<User> findUserByPrimaryOrgIn(Activation status, List<Org> primaryOrgs) {
        List<Integer> orgSids = Lists.newArrayList();
        primaryOrgs.forEach(each -> orgSids.add(each.getSid()));
        return this.findUserByPrimaryIn(orgSids, status);
    }

    public List<User> findUserByPrimaryIn(List<Integer> orgSids, Activation status) {
        List<User> users = Lists.newArrayList();
        wkUserCache.findAll().forEach(each -> {
            if (status == null) {
                if (orgSids.contains(each.getPrimaryOrg().getSid())) {
                    users.add(each);
                }
            } else if (orgSids.contains(each.getPrimaryOrg().getSid()) && each.getStatus().equals(status)) {
                users.add(each);
            }
        });
        return users;
    }

    public List<User> findAll(Activation status) {
        if (status == null) {
            return wkUserCache.findAll();
        }
        List<User> users = Lists.newArrayList();
        wkUserCache.findAll().forEach(item -> {
            if (item.getStatus().equals(status)) {
                users.add(item);
            }
        });
        return users;
    }

    public User findByUserNameEquals(String userName) {
        for (User each : wkUserCache.findAll()) {
            if (each.getName().equals(userName) || each.getId().equals(userName)) {
                return each;
            }
        }
        return null;
    }

    public List<User> findByUserNameLike(String userName) {
        return wkUserCache.findAll().stream()
                .filter(each -> each.getStatus().equals(Activation.ACTIVE) && each.getName().toLowerCase().contains(userName.toLowerCase()))
                .collect(Collectors.toList());
    }

    /**
     * 包含啟用/停用
     * 
     * @param userName
     * @return
     */
    public List<User> findAllByLike(String userName) {
        return wkUserCache.findAll().stream()
                .filter(each -> each.getName().toLowerCase().contains(userName.toLowerCase()))
                .filter(each -> each.getPrimaryOrg().getCompanySid() == 2)
                .collect(Collectors.toList());
    }

    /**
     * 取得成員主要部門
     *
     * @param user
     * @return
     */
    public Org findPrimaryOrg(User user) {
        User fullUsr = WkUserCache.getInstance().findBySid(user.getSid());
        return orgService.findBySid(fullUsr.getPrimaryOrg().getSid());
    }

    /**
     * 檢查是否為處級以上主管
     *
     * @param dep
     * @param user
     * @return
     */
    public Boolean checkIsDivisionLvUpManager(User user) {
        Org dep = orgService.findBySid(user.getPrimaryOrg().getSid());
        if (dep.getLevel() == null) {
            log.error("成員：" + user.getName() + " 部門：" + WkOrgUtils.getOrgName(dep) + " 未設定部門級別(DepLv)！！");
            return Boolean.FALSE;
        }
        return dep.getLevel().equals(OrgLevel.DIVISION_LEVEL)
                || dep.getLevel().equals(OrgLevel.GROUPS);
    }

//    /**
//     * 檢查成員所屬部門等級
//     *
//     * @param user
//     * @param checkType
//     * @return
//     */
//    public Boolean checkDepLvByType(User user, OrgLevel checkType) {
//        Org dep = orgService.findBySid(user.getPrimaryOrg().getSid());
//        if (dep.getLevel() == null) {
//            return Boolean.FALSE;
//        }
//        return dep.getLevel().equals(checkType);
//    }

//    public void checkUserDepLv(User user) {
//        Org dep = orgService.findBySid(user.getPrimaryOrg().getSid());
//        if (dep.getLevel() == null) {
//            log.error("成員：" + user.getName() + " 部門：" + WkOrgUtils.getOrgName(dep) + " 未設定部門級別(DepLv)！！");
//            return;
//        }
//
//        String depName = WkOrgUtils.getOrgName(dep);
//
//        if ((depName.endsWith("組") && !dep.getLevel().equals(OrgLevel.THE_PANEL))
//                || (depName.endsWith("室") && !dep.getLevel().equals(OrgLevel.MINISTERIAL))
//                || (depName.endsWith("部") && !dep.getLevel().equals(OrgLevel.MINISTERIAL))
//                || (depName.endsWith("處") && !dep.getLevel().equals(OrgLevel.DIVISION_LEVEL))
//                || (depName.endsWith("群") && !dep.getLevel().equals(OrgLevel.GROUPS))) {
//            log.info("請檢查成員：" + user.getName() + " 部門：" + depName + " 部門等級：" + dep.getLevel() + " 部門級別(fusion.org.DepLevel)是否有誤！！");
//        }
//    }

    /**
     * 查詢使用者名稱
     *
     * @param userSid
     * @return
     */
    public String getUserName(Object obj) {
        if (obj == null) {
            return "";
        }
        User result = null;
        if (obj instanceof User) {
            result = WkUserCache.getInstance().findBySid(((User) obj).getSid());
        }
        if (obj instanceof Integer) {
            result = WkUserCache.getInstance().findBySid((Integer) obj);
        }
        if (obj instanceof String) {
            result = WkUserCache.getInstance().findBySid((String) obj);
        }
        if (result == null) {
            return "";
        }
        return result.getName();
    }

    /**
     * 從成員取得主要公司名稱 <BR>
     * accept class <BR/>
     * User、String、Integer
     *
     * @param user
     * @return
     */
    public String getPrimaryOrgNameByUsr(Object user) {
        if (user == null) {
            return "";
        }
        User result = null;
        if (user instanceof User) {
            result = WkUserCache.getInstance().findBySid(((User) user).getSid());
        }
        if (user instanceof Integer) {
            result = WkUserCache.getInstance().findBySid((Integer) user);
        }
        if (user instanceof String) {
            result = WkUserCache.getInstance().findBySid((String) user);
        }
        if (result == null) {
            return "";
        }
        return orgService.getOrgName(result.getPrimaryOrg());
    }

    public Integer findPrimaryOrgSid(User user) {
        if (user.getPrimaryOrg() == null) {
            return this.findPrimaryOrg(user).getSid();
        }
        return user.getPrimaryOrg().getSid();
    }

    /**
     * 依primaryOrg重新排序user
     *
     * @param users
     */
    public void resortUsersByPrimaryOrg(List<User> users) {
        Collections.sort(users, (User u1, User u2) -> {
            int num = 0;
            num = u1.getPrimaryOrg().getSid().compareTo(u2.getPrimaryOrg().getSid());
            if (num != 0) {
                return num;
            }
            return u1.getSid().compareTo(u2.getSid());
        });
    }

}
