/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.callable;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

import com.cy.tech.request.logic.search.view.Search07View;
import com.cy.tech.request.logic.service.AssignSendInfoService;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.vo.converter.SetupInfoToConverter;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireFinishCodeType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkJsonUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

/**
 * @author brain0925_liao
 */
public class Search07ViewCallable implements Callable<Search07View> {

    private Map<String, RequireFinishCodeType> finishCodeMap;

    private SearchService searchHelper;

    private static SetupInfoToConverter infoConverter = new SetupInfoToConverter();

    private URLService urlService;

    private Integer execUserSid;

    private Object[] record;

    private Set<Integer> assignRelateionDepSids;

    @SuppressWarnings("unused")
    private int index;

    public Search07ViewCallable(int index, WkJsonUtils jsonUtils,
            Map<String, RequireFinishCodeType> finishCodeMap, SearchService searchHelper,
            URLService urlService, Integer execUserSid, Object[] record,
            Set<Integer> assignRelateionDepSids) {
        this.index = index;
        this.finishCodeMap = finishCodeMap;
        this.searchHelper = searchHelper;
        this.urlService = urlService;
        this.execUserSid = execUserSid;
        this.record = record;
        this.assignRelateionDepSids = assignRelateionDepSids;

    }

    @Override
    public Search07View call() throws Exception {

        Search07View search07View = new Search07View();

        int index = 0;
        String sid = (String) record[index++];
        String requireNo = (String) record[index++];
        String requireTheme = (String) record[index++];
        Integer urgency = (Integer) record[index++];
        Date createdDate = (Date) record[index++];
        String bigName = (String) record[index++];
        String middleName = (String) record[index++];
        String smallName = (String) record[index++];
        Integer createDep = (Integer) record[index++];
        Integer createdUser = (Integer) record[index++];
        Boolean requireSuspendedCode = (Boolean) record[index++];
        String requireStatus = (String) record[index++];
        String readReason = (String) record[index++];
        String finishCode = (String) record[index++];
        Date hopeDate = (Date) record[index++];
        Integer inteStaffUser = (Integer) record[index++];
        String assignInfo = (String) record[index++];
        Date assignDate = (Date) record[index++];
        Date updatedDate = (Date) record[index++];

        // REQ-1031
        Date requireEstablishDate = (Date) record[index++];
        Date requireFinishDate = (Date) record[index++];

        // 處理共通欄位 (一定要擺在『取值』最後面, 否則 index 會不對)
        search07View.prepareCommonColumn(record, index);

        search07View.setResultIndex(index);
        search07View.setSid(sid);
        search07View.setRequireNo(requireNo);
        search07View.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
        search07View.setUrgency(UrgencyType.values()[urgency]);
        search07View.setCreatedDate(createdDate);
        search07View.setBigName(bigName);
        search07View.setMiddleName(middleName);
        search07View.setSmallName(smallName);
        search07View.setCreateDep(createDep);
        search07View.setCreatedUser(createdUser);
        search07View.setRequireSuspendedCode(requireSuspendedCode);
        search07View.setRequireStatus(RequireStatusType.valueOf(requireStatus));

        // try {
        // v.setReadRecordGroup(!Strings.isNullOrEmpty(readRecordGroup)
        // ? WkJsonUtils.getInstance().fromJson(readRecordGroup, ReadRecordGroupTo.class)
        // : null);
        // } catch (IOException ex) {
        // log.error(ex.getMessage(), ex);
        // }
        search07View.setReadReason(Strings.isNullOrEmpty(readReason) ? ReqToBeReadType.NO_TO_BE_READ : ReqToBeReadType.valueOf(readReason));
        search07View.setFinishCode(finishCodeMap.get(finishCode));
        search07View.setHopeDate(hopeDate);

        // REQ-1031
        search07View.setExecDays(
                this.searchHelper.calcExecDays(
                        search07View.getRequireStatus(),
                        requireEstablishDate,
                        requireFinishDate));

        // 分派單位
        // this.prepareAssignedDep(search07View, assignInfo, assignRelateionDepSids);

        // 分派單位 sid
        List<Integer> assignDepSids = AssignSendInfoService.getInstance().prepareDeps(
                infoConverter.convertToEntityAttribute(assignInfo));
        search07View.setAssignDepSids(Lists.newArrayList(assignDepSids));

        for (Integer assignDepSid : assignDepSids) {
            if (assignRelateionDepSids.contains(assignDepSid)) {
                search07View.setAssignDep(true);
                break;
            }
        }

        // 分派日期
        search07View.setAssignDate(assignDate);

        search07View.setLocalUrlLink(urlService.createLocalUrlLinkParamForTab(
                URLService.URLServiceAttr.URL_ATTR_M,
                urlService.createSimpleUrlTo(execUserSid, search07View.getRequireNo(), 1),
                URLService.URLServiceAttr.URL_ATTR_TAB_AS, search07View.getSid()));
        search07View.setInteStaffUser(inteStaffUser);

        search07View.setUpdatedDate(updatedDate);

        return search07View;
    }

    // // 快取逾時時間 3 分鐘
    // private static final long cacheOverdueMillisecond = 3 * 60 * 1000;
    // // 快取名稱
    // private static final String cacheNameForPrepareAssignedDep = Search07ViewCallable.class.getSimpleName() +
    // "_prepareAssignedDep";

    /**
     * 準備分派單位資訊
     * 
     * @param v
     * @param assignInfo
     * @param assignRelateionDepSids
     */
    // private void prepareAssignedDep(
    // Search07View v,
    // String assignInfo, Set<Integer> assignRelateionDepSids) {
    //
    // if (WkStringUtils.isEmpty(assignInfo)) {
    // return;
    // }
    //
    // List<String> cacheKeys = Lists.newArrayList(assignInfo);
    //
    // Map<String, Object> dataMap = WkCommonCache.getInstance().getCache(
    // cacheNameForPrepareAssignedDep,
    // cacheKeys,
    // cacheOverdueMillisecond);
    //
    // if (dataMap == null) {
    // dataMap = Maps.newHashMap();
    //
    // // 分派單位 sid
    // List<Integer> assignDepSids = AssignSendInfoService.getInstance().prepareDeps(
    // infoConverter.convertToEntityAttribute(assignInfo));
    //
    // // 分派單位名稱
    // String assignedDepsName = "";
    // String assignedDepsNameForTooltip = "";
    // if (WkStringUtils.notEmpty(assignDepSids)) {
    // assignDepSids = WkOrgUtils.sortByOrgTree(assignDepSids);
    // assignedDepsName = WkOrgUtils.findNameBySid(assignDepSids, "、", false, true);
    // assignedDepsNameForTooltip = WkOrgUtils.prepareDepsNameByTreeStyle(
    // assignDepSids,
    // 20);
    // }
    //
    // // 快取
    // dataMap.put("assignDepSids", assignDepSids);
    // dataMap.put("assignedDepsName", assignedDepsName);
    // dataMap.put("assignedDepsNameForTooltip", assignedDepsNameForTooltip);
    //
    // WkCommonCache.getInstance().putCache(cacheNameForPrepareAssignedDep, cacheKeys, dataMap);
    // }
    //
    // // 為和使用者相關的部門
    // @SuppressWarnings("unchecked")
    // List<Integer> assignDepSids = (List<Integer>) dataMap.get("assignDepSids");
    // for (Integer assignDepSid : assignDepSids) {
    // if (assignRelateionDepSids.contains(assignDepSid)) {
    // v.setAssignDep(true);
    // break;
    // }
    // }
    // // 分派單位名稱
    // //v.setAssignedDeptStr(dataMap.get("assignedDepsName") + "");
    // // 分派單位名稱 tool tip
    // //v.setAssignedDeptTooltip(dataMap.get("assignedDepsNameForTooltip") + "");
    // }
}
