/**
 * 
 */
package com.cy.tech.request.logic.service.setting;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.repository.setting.SettingDefaultAssignSendDepHistoryRepository;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.setting.asdep.SettingDefaultAssignSendDepHistoryVO;
import com.cy.tech.request.vo.setting.asdep.SettingDefaultAssignSendDepHistory;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.common.vo.value.to.ItemsCollectionDiffTo;
import com.google.common.collect.Lists;

/**
 * @author allen1214_wu
 *
 */
@Service
public class SettingDefaultAssignSendDepHistoryService {

    // ========================================================================
    // 服務區
    // ========================================================================
    @Autowired
    transient private SettingDefaultAssignSendDepHistoryRepository settingDefaultAssignSendDepHistoryRepository;

    // ========================================================================
    // 方法區
    // ========================================================================

    public List<SettingDefaultAssignSendDepHistoryVO> findHistory(
            String smallCategorySid,
            RequireCheckItemType checkItemType) {

        // ====================================
        // 查詢
        // ====================================
        List<SettingDefaultAssignSendDepHistory> entities = this.settingDefaultAssignSendDepHistoryRepository
                .findBySmallCategorySidAndCheckItemTypeOrderByCreatedDateDesc(
                        smallCategorySid,
                        checkItemType);

        if (WkStringUtils.isEmpty(entities)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 轉VO
        // ====================================
        List<SettingDefaultAssignSendDepHistoryVO> results = Lists.newArrayList();
        for (SettingDefaultAssignSendDepHistory entity : entities) {
            SettingDefaultAssignSendDepHistoryVO vo = new SettingDefaultAssignSendDepHistoryVO(entity);
            // 異動者名稱
            vo.setUserName(WkUserUtils.findNameBySid(entity.getCreatedUser()));
            // 異動日期
            vo.setDateForDisplay(WkDateUtils.formatDate(vo.getCreatedDate(), WkDateUtils.YYYY_MM_DD_HH24_mm_ss));
            // 新增部門名稱
            vo.setAddDepsName(WkOrgUtils.findNameBySid(entity.getAddDepsInfo(), "、"));
            // 新增部門名稱 (樹狀)
            vo.setAddDepsForTooltip(WkOrgUtils.prepareDepsNameByTreeStyle(entity.getAddDepsInfo(), 40));
            // 移除部門名稱
            vo.setRemoveDepsName(WkOrgUtils.findNameBySid(entity.getRemoveDepsInfo(), "、"));
            // 新增部門名稱 (樹狀)
            vo.setRemoveDepsForTooltip(WkOrgUtils.prepareDepsNameByTreeStyle(entity.getRemoveDepsInfo(), 40));
            
            results.add(vo);
        }

        return results;
    }

    /**
     * 儲存異動記錄
     * 
     * @param smallCategorySid 小類 sid
     * @param checkItemType    檢查項目
     * @param assignSendType   分派/通知
     * @param beforeDepSids    異動前單位 sid
     * @param afterDepSids     異動後單位 sid
     * @param execUserSid      執行者 sid
     * @param execDate         執行日期
     */
    public void saveHistory(
            String smallCategorySid,
            RequireCheckItemType checkItemType,
            AssignSendType assignSendType,
            List<Integer> beforeDepSids,
            List<Integer> afterDepSids,
            Integer execUserSid,
            Date execDate) {

        // ====================================
        // 比較異動
        // ====================================
        ItemsCollectionDiffTo<Integer> diffTo = WkCommonUtils.itemCollectionDiff(beforeDepSids, afterDepSids);

        // ====================================
        // 準備資料
        // ====================================
        // 建立空的 Entity
        SettingDefaultAssignSendDepHistory entity = this.createEmptyEntity(smallCategorySid, checkItemType, assignSendType, execUserSid, execDate);

        // 此次新增單位
        entity.setAddDepsInfo((List<Integer>) diffTo.getPlusItems());
        // 此次移除單位
        entity.setRemoveDepsInfo((List<Integer>) diffTo.getReduceItems());

        // ====================================
        // save
        // ====================================
        this.settingDefaultAssignSendDepHistoryRepository.save(entity);

    }

    /**
     * 建立空的 Entity
     * 
     * @param smallCategorySid 小類 sid
     * @param checkItemType    檢查項目
     * @param assignSendType   分派/通知
     * @param execUserSid      執行者 sid
     * @param execDate         執行日期
     * @return
     */
    public SettingDefaultAssignSendDepHistory createEmptyEntity(
            String smallCategorySid,
            RequireCheckItemType checkItemType,
            AssignSendType assignSendType,
            Integer execUserSid,
            Date execDate) {

        SettingDefaultAssignSendDepHistory entity = new SettingDefaultAssignSendDepHistory();
        entity.setCreatedUser(execUserSid);
        entity.setCreatedDate(execDate);
        entity.setSmallCategorySid(smallCategorySid);
        entity.setCheckItemType(checkItemType);
        entity.setAssignSendType(assignSendType);

        return entity;
    }

}
