/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.enums;

import lombok.Getter;

/**
 * 需求單子程序型別
 *
 * @author shaun
 */
public enum ReqSubType {
    ROOT(""),
    PT("原型確認"),
    WT("送測"),
    OP("ON程式"),
    OS("其它資料設定");

    @Getter
    private final String showName;

    ReqSubType(String showName) {
        this.showName = showName;
    }
}
