/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.vo;

import com.cy.tech.request.vo.enums.OthSetStatus;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author kasim
 */
@Data
@NoArgsConstructor
public class OthSetTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1791265552521785532L;

    private String sid;

    /** 其他設定資訊單號 */
    private String osNo;

    /** 需求單單號 */
    private String requireNo;

    /** 通知單位 */
    private JsonStringListTo noticeDeps;

    /** 設定資訊狀態 */
    private OthSetStatus osStatus;

    /** 其他設定資訊主題 */
    private String theme;

    /** 完成日 */
    private Date finishDate;

    /** 取消日期 */
    private Date cancelDate;

    /** 資訊內容 */
    private String content;

    /** 資訊內容_CSS格式 */
    private String contentCss;

    /** 備註說明 */
    private String note;

    /** 備註說明_有CSS格式 */
    private String noteCss;

    /** 異動日期 */
    private Date updatedDate;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.getSid());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OthSetTo other = (OthSetTo) obj;
        if (!Objects.equals(this.getSid(), other.getSid())) {
            return false;
        }
        return true;
    }
}
