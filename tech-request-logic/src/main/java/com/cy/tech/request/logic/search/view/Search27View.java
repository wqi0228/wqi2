/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import java.io.Serializable;
import java.util.Date;

import com.cy.work.common.enums.UrgencyType;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 需求單查詢頁面顯示vo (search27.xhtml)
 *
 * @author kasim
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Search27View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2956639628494029304L;
    /** 部 */
    private Boolean hasForwardDep;
    /** 個 */
    private Boolean hasForwardMember;
    /** 關 */
    private Boolean hasLink;
    /** 立單日期 */
    private Date createdDate;
    /** 異動日期 */
    private Date updatedDate;
    /** 需求類別 */
    private String bigName;
    /** 中類 */
    private String middleName;
    /** 小類 */
    private String smallName;
    /** 需求單位 */
    private Integer createDep;
    /** 需求人員 */
    private Integer createdUser;
    /** 緊急度 */
    private UrgencyType urgency;
    /** 附加檔案 */
    private Boolean checkAttachment;
    private Boolean hasAttachment;
    /** 期望完成日 */
    private Date hopeDate;

    /** 本地端連結網址 */
    private String localUrlLink;
    /** 立 - 自己單位建立的需求單（含底下組別） */
    private Boolean create;
    /** 分 - 分派至自己單位的需求單 (使用者所歸屬的單位) */
    private Boolean assign;
    /** 轉 - 轉寄至自己單位的需求單 (使用者所歸屬的單位) */
    private Boolean forward;
    /** 廳主 */
    private Long customer;
    /** 是否案件單轉入 */
    private String hasTransByIssue;
}
