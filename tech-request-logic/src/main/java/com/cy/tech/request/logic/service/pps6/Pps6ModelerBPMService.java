/**
 * 
 */
package com.cy.tech.request.logic.service.pps6;

import java.io.Serializable;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.cy.bpm.rest.client.BpmOrganizationClient;
import com.cy.bpm.rest.to.RoleTo;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.util.Base64Utils;
import com.cy.tech.request.logic.service.pps6.vo.Pps6Task;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 *
 */
@Service
@Slf4j
public class Pps6ModelerBPMService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1707269000793080679L;

    // ========================================================================
    // 服務區
    // ========================================================================
    @Autowired
    private transient BpmOrganizationClient organizationClient;

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private HttpHeaders basicHeaders;

    // ========================================================================
    // 變數區
    // ========================================================================
    @Value("${bpm.server.ip}")
    private String bpmIp;
    @Value("${bpm.web.service.port}")
    private String port;

    private String getBpmUrl() {
        return "http://" + bpmIp + ":" + port;
    }

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * @param roleIDs
     * @return
     * @throws ProcessRestException
     */
    public Map<String, List<String>> findRoleUser(Set<String> roleIDs) throws ProcessRestException {

        // ====================================
        // 防呆
        // ====================================
        if (WkStringUtils.isEmpty(roleIDs)) {
            return Maps.newHashMap();
        }

        // ====================================
        // 逐筆查詢角色下的使用者
        // ====================================
        Map<String, List<String>> roleUsersSetByRoleID = Maps.newHashMap();

        for (String roleID : roleIDs) {
            List<String> userIDs = this.organizationClient.findUserFromRole(roleID);
            if (WkStringUtils.isEmpty(userIDs)) {
                log.warn("角色：[{}] 已無使用者!", roleID);
                continue;
            }
            roleUsersSetByRoleID.put(roleID, userIDs);
        }

        return roleUsersSetByRoleID;
    }

    /**
     * 取得使用者所有角色明細
     * @param userId
     * @return
     * @throws ProcessRestException
     */
    public List<RoleTo> findUserRole(String userId) throws ProcessRestException {

        // ====================================
        // 查詢使用者所有角色ID
        // ====================================
        List<String> roleIds = this.organizationClient.findRoleFromUser(userId);
        if(WkStringUtils.isEmpty(roleIds)) {
            return Lists.newArrayList();
        }
        
        // ====================================
        // 逐一查詢角色明細資料
        // ====================================
        List<RoleTo> roleInfos = Lists.newArrayList(); 
        for (String roleId : roleIds) {
            RoleTo roleTo = this.findRoleIno(roleId);
            if(roleTo==null) {
                log.warn("BPM角色代號查詢不到明細! roleId:[{}]", roleId);
                continue;
            }
            roleInfos.add(roleTo);
        }

        return roleInfos;
    }

    /**
     * 查詢角色明細資料
     * @param roleId
     * @return
     * @throws ProcessRestException
     */
    public RoleTo findRoleIno(String roleId) throws ProcessRestException {
        return this.organizationClient.findRoleToById(roleId);
    }

    /**
     * @param userId
     * @return
     */
    public List<Pps6Task> getTodo(String userId) {
        URI targetUrl = UriComponentsBuilder.fromUriString(getBpmUrl())
                .path("/utms/task/tasks/user/" + userId)
                .build()
                .toUri();

        ResponseEntity<String> result = restTemplate.exchange(
                targetUrl,
                HttpMethod.GET,
                new HttpEntity<String>(basicHeaders),
                String.class);

        if (result.getStatusCode().equals(HttpStatus.SEE_OTHER)) {
            throw new RuntimeException(Base64Utils.decode(result.getHeaders().getFirst("error")));
        }

        return this.parseJson(result.getBody());
    }

    /**
     * 解析JSON格式
     *
     * @param result : response json string data
     */
    private List<Pps6Task> parseJson(String result) {
        if (WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }
        
        //Object obj = new Gson().fromJson(result, Object.class);
        //log.info(new com.google.gson.GsonBuilder().setPrettyPrinting().create().toJson(obj));

        Pps6Task[] results = new Gson().fromJson(result, Pps6Task[].class);

        return Lists.newArrayList(results);
    }

}
