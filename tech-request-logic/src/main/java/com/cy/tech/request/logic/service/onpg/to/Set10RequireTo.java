/**
 * 
 */
package com.cy.tech.request.logic.service.onpg.to;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 */
public class Set10RequireTo implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -2974550865243342037L;

    @Getter
    @Setter
    private String requireNo;
    @Getter
    @Setter
    private Date createDate;
    @Getter
    @Setter
    private Date hopeDate;
    @Getter
    @Setter
    private Date finishDate;
    @Getter
    @Setter
    private Date closeDate;
    @Getter
    @Setter
    private String requireStatus;
    @Getter
    @Setter
    private Integer depSid;

}
