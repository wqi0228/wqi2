/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.enums;

import lombok.Getter;

/**
 *
 * @author shaun
 */
public enum Search18SubType {
    /** 全部 */
    ALL("全部"),
    /** 原型確認 */
    PT("原型確認"),
    /** 送測 */
    ST("送測"),;

    @Getter
    private final String showName;

    Search18SubType(String showName) {
        this.showName = showName;
    }
}
