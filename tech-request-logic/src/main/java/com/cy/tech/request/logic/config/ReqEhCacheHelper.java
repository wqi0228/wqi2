package com.cy.tech.request.logic.config;

import org.springframework.stereotype.Service;
import com.cy.tech.request.vo.constants.CacheConstants;
import com.cy.work.backend.logic.constants.WkBackendConstants;
import com.cy.work.common.utils.WkEhCacheUtils;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import net.sf.ehcache.CacheManager;

import java.io.Serializable;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.ehcache.EhCacheCacheManager;

@Service
@Slf4j
public class ReqEhCacheHelper implements Serializable, InitializingBean {

	/**
     * 
     */
    private static final long serialVersionUID = 1820904570388808273L;
    // ========================================================================
	// InitializingBean
	// ========================================================================
	private static ReqEhCacheHelper instance;

	/**
	 * getInstance
	 * 
	 * @return instance
	 */
	public static ReqEhCacheHelper getInstance() {
		return instance;
	}

	/**
	 * fix for fireBugs warning
	 * 
	 * @param instance
	 */
	private static void setInstance(ReqEhCacheHelper instance) {
		ReqEhCacheHelper.instance = instance;
	}

	/**
	 * afterPropertiesSet
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		setInstance(this);
	}

	
	// ========================================================================
	// 
	// ========================================================================
	@Autowired
	transient private WkEhCacheUtils wkEhCacheUtils;
	@Autowired
	transient private CacheManager cacheManager;

	@Getter
	@Value("${tech.request.cache.default.timeToLiveSeconds}")
	private int defaultTimeToLiveSeconds;
	@Getter
	@Value("${tech.request.cache.default.timeToIdleSeconds}")
	private int defaultTimeToIdleSeconds;

	@Getter
	@Value("${tech.request.cache.short.timeToIdleSeconds}")
	private int shortTimeToIdleSeconds;
	@Getter
	@Value("${tech.request.cache.short.timeToLiveSeconds}")
	private int shortTimeToLiveSeconds;

	public EhCacheCacheManager createCacheManager(CacheManager cacheManager) {

		log.info("=============createCacheManager===========");

		try {
			// 建立預設時間快取
			this.createDefaultCache(cacheManager);

			// 建立短快取
			this.createShortCache(cacheManager);

		} catch (Exception e) {
			log.error("建立 ehcache 組態失敗!", e);
		}

		return new EhCacheCacheManager(cacheManager);
	}

	/**
	 * 清除快取
	 */
	public void clearCache() {
	    if(cacheManager==null) {
	        log.error("cacheManager is null");
	    }
	    cacheManager.clearAll();	    
		log.info("清除 ehcache");
	}

	/**
	 * 建立預設長度快取
	 * 
	 * @throws Exception
	 */
	private void createDefaultCache(CacheManager cacheManager) throws Exception {

		Map<String, String> types = Maps.newHashMap();
		types.put(CacheConstants.CACHE_TEMP_FIELD, "需求單模版");
		types.put(CacheConstants.CACHE_PORTAL_MENU, "需求單入口選單");
		types.put(CacheConstants.CACHE_CATE_KEY_MAPPING, "需求單類別模版Key");
		types.put(CacheConstants.CACHE_findActiveBigCategory, "需求單查詢所有大類");
		types.put(CacheConstants.CACHE_prepareRequireConfirmDep, "需求確認單位計算結果");
		types.put(CacheConstants.CACHE_SETTING_CHECK_CONFIRM_RIGHT, "可檢查確認權限 for find all");
		types.put(CacheConstants.CACHE_IS_CANCHECK_USER, "使用者可檢查確認權限");
		types.put(WkBackendConstants.CACHE_WORK_PARAM, "工作紀錄參數");

		for (Entry<String, String> cacheType : types.entrySet()) {
			wkEhCacheUtils.addCache(
			        cacheManager,
			        cacheType.getKey(),
			        cacheType.getValue(),
			        defaultTimeToLiveSeconds,
			        defaultTimeToIdleSeconds);
		}
	}

	/**
	 * 建立預設長度快取
	 * 
	 * @throws Exception
	 */
	private void createShortCache(CacheManager cacheManager) throws Exception {

		Map<String, String> types = Maps.newHashMap();
		types.put(CacheConstants.CACHE_ALL_CATEGORY_PICKER, "所有類別組合");

		for (Entry<String, String> cacheType : types.entrySet()) {
			wkEhCacheUtils.addCache(
			        cacheManager,
			        cacheType.getKey(),
			        cacheType.getValue(),
			        shortTimeToIdleSeconds,
			        shortTimeToIdleSeconds);
		}
	}
}
