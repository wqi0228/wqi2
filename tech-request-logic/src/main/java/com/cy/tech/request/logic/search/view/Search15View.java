/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.vo.value.to.JsonStringListTo;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 送測異動明細表頁面顯示vo (search15.xhtml)
 *
 * @author jason_h
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Search15View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5235980063812360450L;
    /** 需求單-緊急度 */
    private UrgencyType urgency;
    /** 需求日期 */
    private Date requireDate;
    /** 需求單位 */
    private Integer requireDep;
    /** 需求人員 */
    private Integer requireUser;
    /** 需求類別 */
    private String bigName;
    /** 中類 */
    private String middleName;
    /** 小類 */
    private String smallName;
    /** 送測單建立日期 */
    private Date createdDate;
    /** 送測單建立部門 */
    private Integer createDep;
    private String createDepName;
    /** 送測單建立者 */
    private Integer createdUser;
    /** 送測主題 */
    private String testTheme;
    /** 送測單位 */
    private JsonStringListTo sendTestDep;
    /** 送測單號 */
    private String testinfoNo;
    /** 送測狀態 */
    private WorkTestStatus behaviorStatus;
    /** 異動日期(異動明細單的建立日期) */
    private Date testHistoryCreatedDate;
    /** 執行人員 */
    private Integer testHistoryCreatedUser;
    /** 說明 */
    private String reason;
    /** 送測主檔Sid */
    private String testInfoSid;
    /** 本地端連結網址 */
    private String localUrlLink;
}
