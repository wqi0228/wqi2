/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.helper;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.logic.enumerate.BasicDataCategoryType;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.logic.vo.BasicDataCategoryTo;
import com.cy.tech.request.repository.category.BasicDataBigCategoryRepository;
import com.cy.tech.request.repository.category.BasicDataMiddleCategoryRepository;
import com.cy.tech.request.repository.category.BasicDataSmallCategoryRepository;
import com.cy.tech.request.repository.template.CategoryKeyMappingRepository;
import com.cy.tech.request.vo.category.BasicDataBigCategory;
import com.cy.tech.request.vo.category.BasicDataMiddleCategory;
import com.cy.tech.request.vo.category.BasicDataSmallCategory;
import com.cy.tech.request.vo.template.CategoryKeyMapping;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 鍵值物件 查找 | 新增 | 組合新鍵值 | 異動
 *
 * @author shaun
 */
@Component
public class TemplateKeyMappingHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -9125701541835766598L;
    @Autowired
    private BasicDataBigCategoryRepository bigDao;
    @Autowired
    private BasicDataMiddleCategoryRepository middleDao;
    @Autowired
    private BasicDataSmallCategoryRepository smallDao;
    @Autowired
    private CategoryKeyMappingRepository mappingDao;
    @Autowired
    private ReqularPattenUtils reqularUtils;

    @Transactional(readOnly = true)
    public List<CategoryKeyMapping> findByFuzzyTextAndMaxVersion(String searchText, List<Activation> status) {
        String realSearchText = "%" + reqularUtils.replaceIllegalSqlLikeStr(searchText) + "%";
        return mappingDao.findByFuzzyTextAndMaxVersion(realSearchText, status);
    }

    @Transactional(readOnly = true)
    public List<Integer> findByIdAllVersionList(String id) {
        return mappingDao.findByIdAllVersionList(id);
    }

    /**
     * 檢查是否已存在相同的mappingId
     *
     * @param to
     * @return
     */
    @Transactional(readOnly = true)
    public Boolean isExistMappingId(BasicDataCategoryTo to) {
        if (!to.getType().equals(BasicDataCategoryType.SMALL) || Strings.isNullOrEmpty(to.getSid())) {
            return Boolean.FALSE;
        }
        BasicDataSmallCategory small = smallDao.findOne(to.getSid());
        BasicDataMiddleCategory middle = small.getParentMiddleCategory();
        BasicDataBigCategory big = middle.getParentBigCategory();
        return mappingDao.isExistId(this.assembleMappingId(big, middle, small));
    }

    /**
     * 組合 MappingId
     *
     * @param big
     * @param middle
     * @param small
     * @return
     */
    public String assembleMappingId(BasicDataBigCategory big, BasicDataMiddleCategory middle, BasicDataSmallCategory small) {
        String mappingId = big.getSid() + middle.getSid() + small.getSid();
        return String.valueOf(mappingId.hashCode());
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveMapping(BasicDataCategoryTo to, Boolean isAdd) {
        boolean isSmallType = to.getType().equals(BasicDataCategoryType.SMALL);
        if (isAdd && !isSmallType) {
            return;
        }
        switch (to.getType()) {
            case BIG:
                this.saveMappingByBig(to);
                break;
            case MIDDLE:
                this.saveMappingByMiddle(to);
                break;
            case SMALL:
                this.saveMappingBySmall(to.getId());
                break;
        }
    }

    private void saveMappingByBig(BasicDataCategoryTo to) {
        List<CategoryKeyMapping> mappings = mappingDao.findByBig(bigDao.findOne(to.getSid()));
        this.setAndSaveMappings(mappings, to);
    }

    private void setAndSaveMappings(List<CategoryKeyMapping> mappings, BasicDataCategoryTo to) {
        for (CategoryKeyMapping each : mappings) {
            each.setBigName(to.getName());
        }
        mappingDao.save(mappings);
    }

    private void saveMappingByMiddle(BasicDataCategoryTo to) {
        List<CategoryKeyMapping> mappings = mappingDao.findByMiddle(middleDao.findOne(to.getSid()));
        this.setAndSaveMappings(mappings, to);
    }

    private void saveMappingBySmall(String smallId) {
        BasicDataSmallCategory small = smallDao.findById(smallId);
        BasicDataMiddleCategory middle = small.getParentMiddleCategory();
        BasicDataBigCategory big = middle.getParentBigCategory();
        String mappingId = this.assembleMappingId(big, middle, small);
        List<CategoryKeyMapping> mappings = mappingDao.findById(mappingId);
        //執行新建
        if (mappings.isEmpty()) {
            CategoryKeyMapping mapping = new CategoryKeyMapping();
            mapping.setId(mappingId);
            mapping.setVersion(1);
            mapping.setBig(big);
            mapping.setBigName(big.getName());
            mapping.setMiddle(middle);
            mapping.setMiddleName(middle.getName());
            mapping.setSmall(small);
            mapping.setSmallName(small.getName());
            //小類停用mapping 也要停用
            mapping.setStatus(small.getStatus());
            mappingDao.save(mapping);
            return;
        }
        /** 更新舊的 */
        for (CategoryKeyMapping each : mappings) {
            each.setBig(big);
            each.setBigName(big.getName());
            each.setMiddle(middle);
            each.setMiddleName(middle.getName());
            each.setSmall(small);
            each.setSmallName(small.getName());
            //小類停用mapping 也要停用
            each.setStatus(small.getStatus());
        }
        mappingDao.save(mappings);

    }

}
