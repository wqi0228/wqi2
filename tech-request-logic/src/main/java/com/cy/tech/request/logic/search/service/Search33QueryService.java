/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.search.view.Search33View;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.vo.worktest.enums.WorkTestInfoStatus;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

/**
 * @author marlow_chen
 */
@Service
public class Search33QueryService implements QueryService<Search33View>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7629257563078272971L;

    @Autowired
    private URLService urlService;

    @PersistenceContext
    transient private EntityManager em;

    @SuppressWarnings("unchecked")
    @Override
    public List<Search33View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {

        // ====================================
        // 查詢
        // ====================================

        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        List<Object[]> result = query.getResultList();
        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if (WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();
        List<Search33View> viewResult = Lists.newArrayList();
        for (int i = 0; i < result.size(); i++) {
            Object[] record = (Object[]) result.get(i);

            int index = 0;
            String sid = (String) record[index++];
            Date testDate = (Date) record[index++];
            Integer createDeptSid = (Integer) record[index++];
            Integer createdUser = (Integer) record[index++];
            String testTheme = (String) record[index++];
            String qaAuditStatus = (String) record[index++];
            Date establishDate = (Date) record[index++];
            String qaScheduleStatus = (String) record[index++];
            Date qaAuditDate = (Date) record[index++];
            Date createdDate = (Date) record[index++];
            String requireNo = (String) record[index++];
            Date expectOnlineDate = (Date) record[index++];
            String testInfoNo = (String) record[index++];
            String testInfoStatus = (String) record[index++];
            Integer approveUser = (Integer) record[index++];
            String waitRead = String.valueOf(record[index++]);
            Date readDate = (Date) record[index++];

            Search33View v = new Search33View();
            v.setSid(sid);
            v.setRequireNo(requireNo);
            v.setTestDate(testDate);
            v.setCreateDeptSid(createDeptSid);
            v.setCreateDept(WkOrgUtils.prepareBreadcrumbsByDepName(createDeptSid, OrgLevel.DIVISION_LEVEL, false, "-"));
            v.setCreatedUser(createdUser);
            v.setTestTheme(testTheme);
            v.setQaAuditStatus(WorkTestInfoStatus.valueOf(qaAuditStatus).getValue());
            v.setQaScheduleStatus(WorkTestInfoStatus.valueOf(qaScheduleStatus).getValue());
            v.setQaAuditDate(qaAuditDate);
            v.setExpectOnlineDate(expectOnlineDate);

            v.setCreatedDate(createdDate);
            v.setEstablishDate(establishDate);
            v.setTestInfoNo(testInfoNo);
            v.setLocalUrlLink(urlService.createWorkTestInfoUrl(v.getTestInfoNo()));
            v.setTestInfoStatus(WorkTestStatus.valueOf(testInfoStatus).getValue());
            v.setApproveUser(approveUser);
            
            
            // ====================================
            // 閱讀記錄
            // ====================================
            ReadRecordType readRecordType = ReadRecordType.UN_READ;
            // 待閱讀
            if (WkStringUtils.isEmpty(waitRead) || "Y".equals(waitRead)) {
                if (readDate == null) {
                    readRecordType = ReadRecordType.UN_READ;
                } else {
                    readRecordType = ReadRecordType.WAIT_READ;
                }
            }
            // 已閱讀
            else if ("N".equals(waitRead)) {
                readRecordType = ReadRecordType.HAS_READ;
            }
            v.setReadRecordType(readRecordType);
            
            viewResult.add(v);
        }
        return viewResult;
    }

    public Search33View convertToView(WorkTestInfo workTestInfo, User loginUser) {
        Search33View view = new Search33View();
        view.setSid(workTestInfo.getSid());
        view.setRequireNo(workTestInfo.getSourceNo());
        view.setTestDate(workTestInfo.getTestDate());
        view.setCreateDeptSid(workTestInfo.getCreateDep().getSid());
        // 組單位名稱,至少到處級
        view.setCreateDept(WkOrgUtils.prepareBreadcrumbsByDepName(view.getCreateDeptSid(), OrgLevel.DIVISION_LEVEL, false, "-"));
        view.setCreatedUser(workTestInfo.getCreatedUser().getSid());
        view.setTestTheme(workTestInfo.getTheme());
        view.setQaAuditStatus(workTestInfo.getQaAuditStatus().getValue());
        view.setQaScheduleStatus(workTestInfo.getQaScheduleStatus().getValue());
        view.setQaAuditDate(workTestInfo.getQaAuditDate());
        view.setExpectOnlineDate(workTestInfo.getExpectOnlineDate());

        view.setCreatedDate(workTestInfo.getCreatedDate());
        view.setEstablishDate(workTestInfo.getEstablishDate());
        view.setTestInfoNo(workTestInfo.getTestinfoNo());

        view.setLocalUrlLink(urlService.createLocalUrlLinkParamForTab(
                URLService.URLServiceAttr.URL_ATTR_M,
                urlService.createSimpleUrlTo(null, view.getRequireNo(), 1),
                URLService.URLServiceAttr.URL_ATTR_TAB_ST, view.getSid()));

        return view;
    }
}
