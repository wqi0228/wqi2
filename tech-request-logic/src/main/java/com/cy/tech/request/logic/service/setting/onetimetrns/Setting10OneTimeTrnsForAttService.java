package com.cy.tech.request.logic.service.setting.onetimetrns;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.system.rest.client.vo.OrgTransMappingTo;
import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgTransMappingUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 一次性轉檔 - 轉附加檔案
 * 
 * @author allen1214_wu
 */
@Service
public class Setting10OneTimeTrnsForAttService {

    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private transient JdbcTemplate jdbcTemplate;

    //private final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:sss");

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 
     */
    @Transactional(rollbackFor = Exception.class)
    public void process(String companyId) {

        this.findTransDepMapping(companyId);
        
        
        
        

    }

    private Map<Integer, Integer> findTransDepMapping(String companyId) {

        List<Date> allDates = WkOrgTransMappingUtils.getInstance().findOrgTransMappingEffectiveDates(companyId);

        List<OrgTransMappingTo> allMappings = Lists.newArrayList();

        for (Date effectiveDate : allDates) {
            List<OrgTransMappingTo> orgTransMappingTo = WkOrgTransMappingUtils.getInstance().findOrgTrnsMapping(
                    companyId, effectiveDate);
            if (WkStringUtils.notEmpty(orgTransMappingTo)) {
                allMappings.addAll(orgTransMappingTo);
            }
        }

        // 生效日排序
        allMappings = allMappings.stream()
                .sorted(Comparator.comparing(OrgTransMappingTo::getEffectiveDate))
                .collect(Collectors.toList());

        Map<Integer, Integer> beforeDepSidMapByAfterDepSid = Maps.newHashMap();
        Map<Integer, Integer> afterDepSidMapByBeforeDepSid = Maps.newHashMap();
        for (OrgTransMappingTo orgTransMappingTo : allMappings) {
            if (beforeDepSidMapByAfterDepSid.containsKey(orgTransMappingTo.getBeforeOrgSid())) {
                Integer currAfterDepSid = beforeDepSidMapByAfterDepSid.get(orgTransMappingTo.getBeforeOrgSid());
                if (WkCommonUtils.compareByStr(orgTransMappingTo.getAfterOrgSid(), currAfterDepSid)) {
                    String message = "設定迴圈! sid:[%s] 轉換前單位:[%s],轉換後單位:[%s]";
                    message = String.format(message,
                            orgTransMappingTo.getSid() + "",
                            orgTransMappingTo.getBeforeOrgSid() + "-" + WkOrgUtils.findNameBySid(orgTransMappingTo.getBeforeOrgSid()),
                            orgTransMappingTo.getAfterOrgSid() + "-" + WkOrgUtils.findNameBySid(orgTransMappingTo.getAfterOrgSid()));
                    continue;
                }
            }

            beforeDepSidMapByAfterDepSid.put(orgTransMappingTo.getAfterOrgSid(), orgTransMappingTo.getBeforeOrgSid());
            afterDepSidMapByBeforeDepSid.put(orgTransMappingTo.getBeforeOrgSid(), orgTransMappingTo.getAfterOrgSid());

        }

        Map<Integer, Integer> finalDepSidMapByAfterDepSid = Maps.newHashMap();

        for (Integer afterDepSid : beforeDepSidMapByAfterDepSid.keySet()) {
            Integer point = Integer.parseInt(afterDepSid + "");

            // 找到最新
            while (afterDepSidMapByBeforeDepSid.containsKey(point)) {
                point = afterDepSidMapByBeforeDepSid.get(point);
            }

            finalDepSidMapByAfterDepSid.put(afterDepSid, point);
        }

        return finalDepSidMapByAfterDepSid;
    }

}
