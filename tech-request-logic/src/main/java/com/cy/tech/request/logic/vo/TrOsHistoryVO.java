/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.vo;

import com.cy.tech.request.vo.enums.OthSetHistoryBehavior;
import com.cy.tech.request.vo.enums.OthSetStatus;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author brain0925_liao
 */
public class TrOsHistoryVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7150876609667733659L;
    public TrOsHistoryVO(String os_history_sid, String os_reply_sid, String os_reply_and_reply_sid,
            TrOsReplyVO trOsReplyVO, String os_sid,
            String os_no, String require_sid, String require_no,
            String content, String content_css,
            OthSetHistoryBehavior behavior,
            OthSetStatus behaviorStatus, Integer create_usr, Date create_dt, Integer update_usr, Date update_dt) {
        this.os_history_sid = os_history_sid;
        this.os_reply_sid = os_reply_sid;
        this.os_reply_and_reply_sid = os_reply_and_reply_sid;
        this.trOsReplyVO = trOsReplyVO;
        this.os_sid = os_sid;
        this.os_no = os_no;
        this.require_sid = require_sid;
        this.require_no = require_no;
        this.content = content;
        this.content_css = content_css;
        this.behavior = behavior;
        this.behaviorStatus = behaviorStatus;
        this.create_usr = create_usr;
        this.create_dt = create_dt;
        this.update_usr = update_usr;
        this.update_dt = update_dt;
    }

    @Getter
    private final String os_history_sid;
    @Setter
    @Getter
    private TrOsReplyVO trOsReplyVO;
    @Getter
    private final String os_reply_sid;
    @Getter
    private final String os_reply_and_reply_sid;
    @Getter
    private final String os_sid;
    @Getter
    private final String os_no;
    @Getter
    private final String require_sid;
    @Getter
    private final String require_no;
    @Getter
    private final String content;
    @Getter
    private final String content_css;
    @Getter
    private final OthSetHistoryBehavior behavior;
    @Getter
    private final OthSetStatus behaviorStatus;
    @Getter
    private final Integer create_usr;
    @Getter
    private final Date create_dt;
    @Getter
    private final Integer update_usr;
    @Getter
    private final Date update_dt;
}
