/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.onpg;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.logic.vo.WorkOnpgTo;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.onpg.WorkOnpgHistory;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgHistoryBehavior;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 送測顯示處理
 *
 * @author shaun
 */
@Slf4j
@Component
public class OnpgShowService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3471517631560842481L;
    @Autowired
    private OnpgCheckRecordService ckrService;
    @Autowired
    private transient WorkBackendParamHelper workBackendParamHelper;
    @Autowired
    private UserService userService;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private WkOrgCache wkOrgCache;
    @Autowired
    private OnpgService onpgService;

    /**
     * 組合送測單位抬頭訊息
     *
     * @param onpg
     * @return
     */
    public String findNoticeDepTitle(WorkOnpg onpg) {
        if (onpg == null) {
            return "";
        }
        return onpg.getNoticeDeps().getValue().stream()
                .map(each -> orgService.getOrgName(each))
                .sorted()
                .collect(Collectors.joining("、"));
    }

    /**
     * 組合送測單位抬頭訊息
     *
     * @param onpg
     * @return
     */
    public List<String> findNoticeDepTitleByOp(WorkOnpg onpg) {
        if (onpg == null) {
            return Lists.newArrayList();
        }
        return onpg.getNoticeDeps().getValue().stream()
                .map(each -> orgService.getOrgName(each))
                .sorted()
                .collect(Collectors.toList());
    }

    /**
     * 關閉編輯
     *
     * @param req
     * @param onpg
     * @param login
     * @return
     */
    public boolean disableEdit(Require require, WorkOnpg workOnpg, User login) {

        // 由MMS 建立的單，需要在『預計完成日』後才可以編輯 (MMS 鎖單後編輯)
        if (workOnpg.isFromMms()) {
            // 判斷現在時間，是否在預計完成日當日00:00前，是則鎖定
            if (WkDateUtils.isBefore(new Date(), WkDateUtils.convertToOriginOfDay(workOnpg.getEstablishDate()), false)) {
                return true;
            }
        }

        // 依據單據狀態，檢核是否可編輯
        String canNotEditReason = this.verifyCanEditByStatus(require, workOnpg, false);
        if (WkStringUtils.notEmpty(canNotEditReason)) {
            if (canNotEditReason.contains("異常")) {
                log.error("檢核ON程式單是否可編輯時發生錯誤:[{}]", canNotEditReason);
            }
            return false;
        }

        // 填寫ON程式人員及其主管或被通知的成員可以進行編輯
        // Integer primaryOrgSid = userService.findPrimaryOrgSid(login);
        // boolean isNoticeMember = onpg.getNoticeDeps().getValue().contains(String.valueOf(primaryOrgSid));
        // 被通知成員進去後, 會被另外一個規則鎖定, 故移除被通知成員編輯權限 (與編輯頁面權限相同)

        return !this.isCreatedAndAboveManager(workOnpg, login);
    }

    /**
     * 依據單據狀態，檢核是否可編輯
     * 
     * @param require  需求單主檔
     * @param workOnpg on程式單主檔
     * @return 不可編輯時，回傳不可編輯原因
     */
    public String verifyCanEditByStatus(
            Require require,
            WorkOnpg workOnpg,
            boolean isMmsUpdate) {
        return this.verifyCanEditByToStatus(
                require,
                this.onpgService.convert2To(workOnpg),
                isMmsUpdate);
    }

    /**
     * 依據單據狀態，檢核是否可編輯
     * 
     * @param require  需求單主檔
     * @param workOnpg on程式單主檔
     * @return 不可編輯時，回傳不可編輯原因
     */
    public String verifyCanEditByToStatus(
            Require require,
            WorkOnpgTo workOnpgTo,
            boolean isMmsUpdate) {
        if (require == null || workOnpgTo == null) {
            return "單據狀態異常！找不到相關的需求單或ON程式單，不可編輯";
        }

        if (!Activation.ACTIVE.equals(require.getStatus())) {
            return "需求單:[" + require.getRequireNo() + "]已停用，不可編輯";
        }

        if (require.getRequireStatus() == null) {
            return "需求單:[" + require.getRequireNo() + "]製作進度異常，不可編輯";
        }

        if (!isMmsUpdate &&
                !require.getRequireStatus().isCanEditSubCase()) {
            return "需求單:[" + require.getRequireNo() + "]製作進度為：[" + require.getRequireStatus().getValue() + "]，不可編輯";
        }

        if (!Activation.ACTIVE.equals(workOnpgTo.getStatus())) {
            return "ON程式單:[" + workOnpgTo.getOnpgNo() + "]已停用，不可編輯";
        }

        if (workOnpgTo.getOnpgStatus() == null) {
            return "ON程式單:[" + workOnpgTo.getOnpgNo() + "]狀態異常，不可編輯";
        }

        if (!isMmsUpdate
                && !workOnpgTo.getOnpgStatus().isCanEdit()) {
            return "ON程式單:[" + workOnpgTo.getOnpgNo() + "]狀態為：[" + workOnpgTo.getOnpgStatus().getLabel() + "]，不可編輯";
        }

        // MMS
        if (isMmsUpdate
                && !workOnpgTo.getOnpgStatus().isCanUpdateFromMmsSync()) {
            return "ON程式單:[" + workOnpgTo.getOnpgNo() + "]狀態為：[" + workOnpgTo.getOnpgStatus().getLabel() + "]，不可編輯";
        }

        return "";
    }

    /**
     * 關閉編輯
     *
     * @param req
     * @param onpg
     * @param login
     * @return
     */
    public boolean disableNotifyDep(Require req, WorkOnpg onpg, User login) {
        // 填寫ON程式人員及其主管或被通知的成員可以進行編輯
        Integer primaryOrgSid = userService.findPrimaryOrgSid(login);
        boolean isNoticeMember = onpg.getNoticeDeps().getValue().contains(String.valueOf(primaryOrgSid));
        return !(this.isCreatedAndAboveManager(onpg, login) || isNoticeMember);
    }

    /**
     * 檢查登入者是否為填單者及其上層主管
     *
     * @param onpg
     * @param login
     * @return
     */
    public boolean isCreatedAndAboveManager(WorkOnpg onpg, User login) {
        User createUser = WkUserCache.getInstance().findBySid(onpg.getCreatedUser().getSid());

        List<User> canClickDisableList = orgService.findOrgManagers(
                orgService.findBySid(
                        createUser.getPrimaryOrg().getSid()));

        if (!canClickDisableList.contains(createUser)) {
            canClickDisableList.add(createUser);
        }
        return canClickDisableList.contains(login);
    }

    /**
     * 關閉檢查記錄
     *
     * @param req
     * @param onpg
     * @param login
     * @return
     */
    public boolean disableCheckRecord(Require req, WorkOnpg onpg, User login) {
        if (this.disableReplyByStatus(req, onpg)) {
            return true;
        }
        boolean canRely = this.isCreatedAndAboveManager(onpg, login);
        Integer primaryOrgSid = userService.findPrimaryOrgSid(login);
        boolean canReplyDep = onpg.getNoticeDeps().getValue().contains(String.valueOf(primaryOrgSid));
        return !(canRely || canReplyDep);
    }

    private boolean disableReplyByStatus(Require req, WorkOnpg onpg) {
        if (onpg == null || req == null || req.getRequireStatus().equals(RequireStatusType.SUSPENDED)) {
            return true;
        }
        if (req.getCloseCode() && !req.getWhenCloseExeOnpg()) {
            return true;
        }
        WorkOnpgStatus status = onpg.getOnpgStatus();
        boolean disableStatus = status.equals(WorkOnpgStatus.CANCEL_ONPG);
        if (disableStatus) {
            return disableStatus;
        }
        return false;
    }

    /**
     * 關閉GM回覆
     *
     * @param onpg
     * @param login
     * @param hasSysGmRole 系統服務處GM
     * @return
     */
    public boolean disableGMReply(Require req, WorkOnpg onpg, User login) {

        if (onpg == null) {
            log.warn("disableGMReply : 傳入WorkOnpg 為空!");
            return true;
        }

        // 需有『系統參數 GM_LV1』
        if (!workBackendParamHelper.isGM(login.getSid())) {
            return true;
        }

        // 判斷是否在通知部門內
        if (this.isInNoticeDeps(login, onpg.getNoticeDeps().getValue())) {
            return false;
        }

        return true;

    }

    /**
     * 關閉QA回覆
     *
     * @param onpg
     * @param login
     * @return
     */
    public boolean disableQAReply(Require req, WorkOnpg onpg, User login) {

        if (onpg == null) {
            log.warn("disableQAReply : 傳入WorkOnpg 為空!");
            return true;
        }

        // 需為QA軟體測試部成員
        if (!this.workBackendParamHelper.isQAUser(login.getSid())) {
            return true;
        }

        // 判斷是否在通知部門內
        if (this.isInNoticeDeps(login, onpg.getNoticeDeps().getValue())) {
            return false;
        }

        return true;
    }

    /**
     * @param login
     * @param noticeDeps
     * @return
     */
    private boolean isInNoticeDeps(User login, List<String> noticeDeps) {

        if (login == null) {
            log.warn("isInDeps : 傳入User或為空!");
            return true;
        }

        if (login.getPrimaryOrg() == null) {
            log.warn("isInDeps : 登入者主要部門為空!");
            return false;
        }

        // 無通知部門
        if (WkStringUtils.isEmpty(noticeDeps)) {
            return false;
        }

        // 取得user 相關的部門
        Set<String> loginUserDeps = Sets.newHashSet();
        loginUserDeps.add(login.getPrimaryOrg().getSid() + "");
        List<Org> managerOrgs = wkOrgCache.findManagerOrgs(login.getSid());
        if (WkStringUtils.notEmpty(managerOrgs)) {
            for (Org org : managerOrgs) {
                loginUserDeps.add(org.getSid() + "");
            }
        }

        // 比對通知部門
        for (String noticeDepSid : noticeDeps) {
            noticeDepSid = WkStringUtils.safeTrim(noticeDepSid);
            if (!WkStringUtils.isNumber(noticeDepSid)) {
                log.warn("disableGMReply : 通知部門有非數字SID：[" + noticeDepSid + "]");
                continue;
            }

            // 比對在通知部門內, 可使用
            if (loginUserDeps.contains(noticeDepSid)) {
                return true;
            }

            // 檢查登入部門為通知部門的子部門
            List<Org> childDeps = wkOrgCache.findAllChild(Integer.parseInt(noticeDepSid));
            for (Org childDep : childDeps) {
                // 比對部門為通知部門子單位 可使用
                if (loginUserDeps.contains(childDep.getSid() + "")) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 關閉一般回覆<BR/>
     * 當有GM回覆或QA回覆權限時，關閉一般回覆
     *
     * @param onpg
     * @param login
     * @return
     */
    public boolean disableGeneralReply(Require req, WorkOnpg onpg, User login) {

        // 檢核狀態
        if (this.disableReplyByStatus(req, onpg)) {
            return true;
        }

        if (login == null) {
            return true;
        }

        // 有 GM角色時鎖定一般回覆
        if (workBackendParamHelper.isGM(login.getSid())) {
            return true;
        }

        // 有 QA角色時鎖定一般回覆
        if (this.workBackendParamHelper.isQAUser(login.getSid())) {
            return true;
        }

        // 檢查登入者是否為填單者及其上層主管
        if (this.isCreatedAndAboveManager(onpg, login)) {
            return false;
        }

        // 判斷是否在通知部門內
        if (this.isInNoticeDeps(login, onpg.getNoticeDeps().getValue())) {
            return false;
        }

        return true;
    }

    /**
     * 顯示回覆編輯(GM回覆 | GA回覆 |一般)
     *
     * @param req
     * @param onpg
     * @param history
     * @param login
     * @return
     */
    public boolean showCheckRecordEdit(Require req, WorkOnpg onpg, WorkOnpgHistory history, User login) {
        // 檢查該歷程行為是否為回覆型態
        if (!this.isCheckRecordBehavior(history)) {
            return false;
        }

        if (history.getCheckRecord() == null) {
            log.error("onpg history behavior is " + history.getBehavior() + " but CheckRecord is null !! history sid = " + history.getSid());
            return false;
        }

        if (!ckrService.findCheckRecordReplys(history.getCheckRecord()).isEmpty()) {
            return false;
        }

        if (this.disableReplyByStatus1(req, onpg)) {
            return false;
        }

        return login.getSid().equals(history.getCheckRecord().getPerson().getSid());
    }

    private boolean disableReplyByStatus1(Require req, WorkOnpg onpg) {
        if (onpg == null || req == null) {
            return true;
        }
        WorkOnpgStatus status = onpg.getOnpgStatus();
        boolean disableStatus = status.equals(WorkOnpgStatus.CANCEL_ONPG);
        if (disableStatus) {
            return disableStatus;
        }
        return false;
    }

    /**
     * 關閉取消
     *
     * @param req
     * @param onpg
     * @param login
     * @return
     */
    public boolean disableCancel(Require req, WorkOnpg onpg, User login) {
        if (onpg == null || req == null
                || req.getRequireStatus().equals(RequireStatusType.SUSPENDED)
                || req.getRequireStatus().equals(RequireStatusType.COMPLETED)) {
            return true;
        }
        if (req.getCloseCode() && !req.getWhenCloseExeOnpg()) {
            return true;
        }

        // 狀態為完成時，不可再進行取消
        WorkOnpgStatus status = onpg.getOnpgStatus();
        if (status != null && status.isInFinish()) {
            return true;
        }
        return !this.isCreatedAndAboveManager(onpg, login);
    }

    /**
     * 關閉檢查註記
     *
     * @param req
     * @param onpg
     * @param login
     * @return
     */
    public boolean disableCheckMemo(Require req, WorkOnpg onpg, User login) {
        return onpg == null || req == null;
    }

    /**
     * 顯示編輯CheckMemo按鍵
     *
     * @param req
     * @param history
     * @param login
     * @return
     */
    public boolean showCheckMemoEdit(Require req, WorkOnpgHistory history, User login) {
        if (history == null || history.getBehavior() == null) {
            return false;
        }
        return history.getBehavior().equals(WorkOnpgHistoryBehavior.CHECK_MEMO) && login.getSid().equals(history.getCreatedUser().getSid());
    }

    /**
     * 關閉檢查完成
     *
     * @param req
     * @param onpg
     * @param login
     * @param depSid
     * @param hasSmallCategoryRole
     * @return
     */
    public boolean disableCheckComplate(Require req, WorkOnpg onpg, User login, Integer depSid, boolean hasSmallCategoryRole) {

        // 注意：調整判斷規則時, 請同步更新頁面按鈕 tooltip 說明

        // ====================================
        // 檢核固定不可使用的單據狀態
        // ====================================
        if (onpg == null || req == null
                || req.getRequireStatus().equals(RequireStatusType.SUSPENDED)
                || req.getRequireStatus().equals(RequireStatusType.COMPLETED)) {
            return true;
        }
        
        
        

//        if (req.getCloseCode() && !req.getWhenCloseExeOnpg()) {
//            return true;
//        }

        WorkOnpgStatus status = onpg.getOnpgStatus();
        boolean disableStatus = status.equals(WorkOnpgStatus.CANCEL_ONPG) || status.equals(WorkOnpgStatus.CHECK_COMPLETE);
        if (disableStatus) {
            return true;
        }
        
        
        // 由MMS 建立的單，需要在『預計完成日』後才可以檢查完成 (MMS 鎖單後編輯)
        if (onpg.isFromMms()) {
            // 判斷現在時間，是否在預計完成日當日00:00前，是則鎖定
            if (WkDateUtils.isBefore(new Date(), WkDateUtils.convertToOriginOfDay(onpg.getEstablishDate()), false)) {
                return true;
            }
        }

        // ====================================
        // 可用權限：主單『需求者+直系往上所有立案單位主管』相關人員可確認
        // ====================================
        // 取得主單立案單位向上所有主管人員
        Set<Integer> mainCaseRelationUserSids = Sets.newHashSet();
        // 立案者
        if (req != null && req.getCreatedUser() != null) {
            mainCaseRelationUserSids.add(req.getCreatedUser().getSid());
        }

        // 單位+直系上層所有單位的主管
        if (req != null && req.getCreateDep() != null) {
            Set<Integer> directManagerUserSids = this.wkOrgCache.findDirectManagerUserSids(
                    req.getCreateDep().getSid());
            if (WkStringUtils.notEmpty(directManagerUserSids)) {
                mainCaseRelationUserSids.addAll(directManagerUserSids);
            }
        }
        // 比對登入者
        if (mainCaseRelationUserSids.contains(login.getSid())) {
            return false;
        }

        // ====================================
        // 可用權限：ON程式單『立案者+直系往上所有立案單位主管』相關人員可確認
        // ====================================
        // 取得ON程式單立案單位向上所有主管人員
        Set<Integer> onpgRelationUserSids = Sets.newHashSet();
        // 立案者
        if (onpg != null && onpg.getCreatedUser() != null) {
            onpgRelationUserSids.add(onpg.getCreatedUser().getSid());
        }

        // 單位+直系上層所有單位的主管
        if (onpg != null && onpg.getCreateDep() != null) {
            Set<Integer> directManagerUserSids = this.wkOrgCache.findDirectManagerUserSids(
                    onpg.getCreateDep().getSid());
            if (WkStringUtils.notEmpty(directManagerUserSids)) {
                onpgRelationUserSids.addAll(directManagerUserSids);
            }
        }

        // 比對登入者
        if (onpgRelationUserSids.contains(login.getSid())) {
            return false;
        }

        // ====================================
        // 可用權限：市場
        // ====================================
        // 條件增加 REQ-698 (當以下角色登入者, 為通知單位時)
        // 1.檢查完成執行者:市場整合處,檢查完成執行者-會員:*
        // 2.通知單位
        if (hasSmallCategoryRole && this.hasInNoticeDep(onpg, depSid)) {
            return false;
        }

        // REQ-1409 移除『案件單轉oN』類型單據,只有GM能檢查確認
        // 當需求單中類為案件單轉入時
        // 可執行的角色:具備系統服務處GM權限
        // List<String> issueMiddNames =
        // workParamService.findListByKeyword(WorkParamConstatnt.TECH_ISSUE_CREATE_MIDDLE_NAME);
        // if (issueMiddNames.contains(req.getMapping().getMiddleName())) {
        // return !hasSysGmRole;
        // }

        return true;
    }

    /**
     * 登入單位是否在通知單位中
     *
     * @param onpg
     * @param depSid
     * @return
     */
    private boolean hasInNoticeDep(WorkOnpg onpg, Integer depSid) {
        return onpg.getNoticeDeps().getValue().stream()
                .anyMatch(each -> each.equals(String.valueOf(depSid)));
    }

    /**
     * 關閉ONPG附加檔案上傳功能
     *
     * @param testInfo
     * @return
     */
    public boolean disableUploadFun(Require req, WorkOnpg onpg) {
        if (onpg == null || req == null
                || req.getRequireStatus().equals(RequireStatusType.SUSPENDED)
                || req.getRequireStatus().equals(RequireStatusType.COMPLETED)) {
            return true;
        }
        return req.getCloseCode() && !req.getWhenCloseExeOnpg();
    }

    /**
     * 檢查該歷程行為是否為回覆型態
     *
     * @param history
     * @return
     */
    public boolean isCheckRecordBehavior(WorkOnpgHistory history) {
        if (history == null || history.getBehavior() == null) {
            return false;
        }
        WorkOnpgHistoryBehavior behavior = history.getBehavior();
        return behavior.equals(WorkOnpgHistoryBehavior.CHECK_RECORD)
                || behavior.equals(WorkOnpgHistoryBehavior.GENERAL_REPLY)
                || behavior.equals(WorkOnpgHistoryBehavior.GM_REPLY)
                || behavior.equals(WorkOnpgHistoryBehavior.QA_REPLY);
    }
}
