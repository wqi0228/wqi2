package com.cy.tech.request.logic.search.service;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepService;
import com.cy.tech.request.logic.vo.RequireConfirmDepVO;
import com.cy.tech.request.vo.enums.ReqConfirmDepProgStatus;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkConstants;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.sp.logic.manager.RoleInquireDepManager;
import com.google.common.collect.Lists;

/**
 * 需求完成確認單位相關查詢 helper
 * 
 * @author allen1214_wu
 */
@Service
public class ReqConfirmDepSearchHelper {

    // ========================================================================
    // 工具區
    // ========================================================================
    @Autowired
    private transient RequireConfirmDepService requireConfirmDepService;
    @Autowired
    private transient RoleInquireDepManager roleInquireDepManager;

    // ========================================================================
    // 方法區
    // ========================================================================

    /**
     * 查詢個人相關分派單據筆數
     * 
     * @param reqConfirmDepProgStatus 分派單位確認狀態
     * @param loginUsersid            登入者 sid
     * @return
     * @throws UserMessageException
     */
    public int countRequireByRequireConfirmDeAndLoginUserRelation(
            ReqConfirmDepProgStatus reqConfirmDepProgStatus,
            Integer loginUsersid,
            String loginCompID) throws UserMessageException {

        // ====================================
        // 查詢登入者資料
        // ====================================
        User loginUser = WkUserCache.getInstance().findBySid(loginUsersid);
        if (loginUser == null) {
            return 0;
        }

        // ====================================
        // 查詢相關的需求確認檔
        // ====================================
        List<RequireConfirmDepVO> requireConfirmDepVOs = this.prepareRequireConfirmDepByLoginUserRelation(
                reqConfirmDepProgStatus,
                null,
                loginUsersid,
                loginCompID,
                loginUser.getName(),
                true,
                false);

        if (WkStringUtils.isEmpty(requireConfirmDepVOs)) {
            return 0;
        }

        // ====================================
        // 特殊規則過濾
        // ====================================
        requireConfirmDepVOs = this.onlySeePrimaryFilter(requireConfirmDepVOs, loginUser);

        // ====================================
        // 收集單據數量 by 主單
        // ====================================
        return (int) requireConfirmDepVOs.stream()
                .map(RequireConfirmDepVO::getRequireSid)
                .distinct()
                .count();
    }

    /**
     * 特殊規則過濾
     * 
     * @param requireConfirmDepVOs RequireConfirmDepVO list
     * @param user                 user
     * @return
     */
    private List<RequireConfirmDepVO> onlySeePrimaryFilter(List<RequireConfirmDepVO> requireConfirmDepVOs, User user) {

        // ====================================
        // 查詢是否為特殊規則指定單位
        // ====================================
        // REQ-1691 【個人未領單】 移除收束到部規則
        // Set<Integer> spcRuleDepSids = this.workBackendParamHelper.findReqConfirmDepOnlySeePrimaryOrgDepSids();
        // if (!spcRuleDepSids.contains(user.getPrimaryOrg().getSid())) {
        // return requireConfirmDepVOs;
        // }

        // ====================================
        // 計算自己相關的部門
        // ====================================
        Set<Integer> spcRuleAssignDepSids = this.requireConfirmDepService.prepareUserMatchDepSids(user.getSid())
                .stream()//去重複
                .collect(Collectors.toSet());

        // ====================================
        // 過濾
        // ====================================
        List<RequireConfirmDepVO> filtedRequireConfirmDepVOs = Lists.newArrayList();

        for (RequireConfirmDepVO requireConfirmDepVO : requireConfirmDepVOs) {
            // 取得真實分派單位
            Set<Integer> assignDepSidsByRequire = requireConfirmDepVO.getAssignDepSidsByRequire();
            // 比對真實分派單位是否包含於『自己相關的單位』
            for (Integer depSid : spcRuleAssignDepSids) {
                if (assignDepSidsByRequire.contains(depSid)) {
                    filtedRequireConfirmDepVOs.add(requireConfirmDepVO);
                    break;
                }
            }
        }

        return filtedRequireConfirmDepVOs;
    }

    /**
     * 查詢與登入者相關的需求完成確認單位檔 (RequireConfirmDep)<br/>
     * 1.取得與 user 相關的部門 <br/>
     * 2.處理負責人暱稱過濾<br/>
     * 
     * @param reqConfirmDepProgStatus 單位確認進度
     * @param fillterDepSids
     * @param loginUsersid            登入者 sid
     * @param loginCompID             登入者 公司別 ID
     * @param fillterOwnerName        過濾負責人
     * @param isAlwaysGetAssignDeps
     * @param isIncludeSpcCanview
     * @return
     * @throws UserMessageException
     */
    public List<RequireConfirmDepVO> prepareRequireConfirmDepByLoginUserRelation(
            ReqConfirmDepProgStatus reqConfirmDepProgStatus,
            Collection<Integer> fillterDepSids,
            Integer loginUsersid,
            String loginCompID,
            String fillterOwnerName,
            boolean isAlwaysGetAssignDeps,
            boolean isIncludeSpcCanview) throws UserMessageException {

        // 取得負責人查詢字串去空白
        fillterOwnerName = WkStringUtils.safeTrim(fillterOwnerName);

        // ====================================
        // 取得登入者資料
        // ====================================
        if (loginUsersid == null) {
            throw new UserMessageException(WkMessage.SESSION_TIMEOUT, InfomationLevel.WARN);
        }

        // ====================================
        // 查詢與登入者相關的待領單據
        // ====================================
        // 取得與使用者相關部門
        Set<Integer> matchDepSids = this.requireConfirmDepService.prepareUserMatchDepSids(loginUsersid)
                .stream()
                .collect(Collectors.toSet());
        
        // 加入殊可閱部門
        if (isIncludeSpcCanview) {
            Set<Integer> spDepSids = this.roleInquireDepManager.findRoleInquireDepSidsByUser(loginUsersid);
            if (WkStringUtils.notEmpty(spDepSids)) {
                matchDepSids.addAll(spDepSids);
            }
        }

        // 有部門過濾時，先濾掉沒在清單中的部門
        if (WkStringUtils.notEmpty(fillterDepSids)) {
            matchDepSids = matchDepSids.stream()
                    .filter(depSid -> fillterDepSids.contains(depSid))
                    .collect(Collectors.toSet());
        }

        // 查詢
        List<RequireConfirmDepVO> requireConfirmDepVOs = this.requireConfirmDepService.queryByDepSidInAndProgStatus(
                matchDepSids,
                reqConfirmDepProgStatus,
                isAlwaysGetAssignDeps,
                WkStringUtils.notEmpty(fillterOwnerName));

        // 為空時回傳
        if (WkStringUtils.isEmpty(requireConfirmDepVOs)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 過濾負責人
        // 1.找不到符合暱稱的 user , 直接 return
        // 2.若無相關的 RequireConfirmDep 直接 return
        // ====================================

        // 無須過濾負責人時回傳
        if (WkStringUtils.isEmpty(fillterOwnerName)) {
            return requireConfirmDepVOs;
        }

        // --------------------------------
        // 過濾符合條件的需求單位確認檔 (RequireConfirmDep)
        // --------------------------------
        // 取得需要過濾的 user sid
        List<Integer> conditionOwnerSidsByLike = WkUserUtils.findByNameLike(fillterOwnerName, loginCompID);

        // 找不到符合暱稱的 user
        if (WkStringUtils.isEmpty(conditionOwnerSidsByLike)) {
            return Lists.newArrayList();
        }

        // 過濾符合比對 user 的資料
        List<RequireConfirmDepVO> fillterByOwnerVOs = requireConfirmDepVOs.stream()
                .filter(vo -> this.isMatchOwner(vo, conditionOwnerSidsByLike))
                .collect(Collectors.toList());

        return fillterByOwnerVOs;
    }

    /**
     * 判斷負責人是否符合指定人員
     * 
     * @param vo
     * @param conditionOwnerSidsByLike
     * @return
     */
    public boolean isMatchOwner(RequireConfirmDepVO vo, List<Integer> conditionOwnerSidsByLike) {

        Integer ownerSid = vo.getOwnerSid();

        // ====================================
        // 比對該確認單位已指定負責人
        // ====================================
        // 命中, 負責人於查詢名單內
        if (conditionOwnerSidsByLike.contains(ownerSid)) {
            return true;
        }
        // 負責人已經指定，但沒有命中
        if (!WkCommonUtils.compareByStr(WkConstants.MANAGER_VIRTAUL_USER_SID, ownerSid)) {
            return false;
        }

        // ====================================
        // 未指定負責人 (為單位主管)
        // ====================================
        // 取得確認單位資料
        Org confirmDep = WkOrgCache.getInstance().findBySid(vo.getDepSid());
        if (confirmDep == null) {
            return false;
        }

        // 1.比對『確認單位』的主管
        if (confirmDep.getManager() != null) {
            if (conditionOwnerSidsByLike.contains(confirmDep.getManager().getSid())) {
                return true;
            }
        }

        // 2.若『確認單位』為部級，代表有可能有分派單位被收束，故加入可比對的組級主管 (有被真實分派的組)
        if (OrgLevel.MINISTERIAL.equals(confirmDep.getLevel())) {

            // 取得以下所有組級單位
            List<Org> childDeps = WkOrgCache.getInstance().findAllChild(confirmDep.getSid());

            for (Org childDep : childDeps) {

                // 比對該子單位真的有被分派
                if (vo.getAssignDepSidsByRequire().contains(childDep.getSid())) {

                    // 比對該子單位主管
                    if (childDep.getManager() != null) {
                        if (conditionOwnerSidsByLike.contains(childDep.getManager().getSid())) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
}
