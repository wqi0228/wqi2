package com.cy.tech.request.logic.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.cy.tech.request.client.ReqFormNumberClient;
import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.exception.alert.TechRequestAlertException;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * 單號產生服務
 * 
 * @author allen1214_wu
 */
@Service
@Slf4j
public class FormNumberService {

    /**
     * jdbcTemplate
     */
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private transient JdbcTemplate jdbcTemplate;

    @Autowired
    private transient ReqFormNumberClient reqFormNumberClient;

    /**
     * 是否為單號產生中心 (為 REST) （true 時直接執行， false 時打 rest）<br/>
     * 讓產生單號的地方都在同一個AP，避免單號重複
     * REST 需透過 ApplicationContextAware, 將此值改成 true
     */
    public static boolean isFormNumberCenter = false;

    /**
     * 日期格式
     */
    private SimpleDateFormat datePatten = new SimpleDateFormat("yyyyMMdd");

    /**
     * 流水號長度
     */
    public final int NO_LENGTH = 3;

    /**
     * 產生原型確認序號
     * 
     * @return
     * @throws UserMessageException
     */
    public synchronized String generateTRNum(String compId) throws UserMessageException {

        // AP本身不為REST時，由呼叫REST產生序號，避免多組AP同時觸發時，序號重複
        if (!FormNumberService.isFormNumberCenter) {
            try {
                return this.callGeneratorCenter(FormType.REQUIRE, compId);
            } catch (SystemOperationException e) {
                throw new UserMessageException(e);
            }
        }

        // EX : TGTR20160802018
        String preFix = compId + "TR";
        return this.generateNum(preFix, "tr_require", "require_no");
    }

    /**
     * 產生草稿單號
     * 
     * @return
     * @throws UserMessageException
     */
    public synchronized String generateDraftNum(String compId) throws UserMessageException {

        // AP本身不為REST時，由呼叫REST產生序號，避免多組AP同時觸發時，序號重複
        if (!FormNumberService.isFormNumberCenter) {
            try {
                return this.callGeneratorCenter(FormType.REQUIRE_DREFT, compId);
            } catch (SystemOperationException e) {
                throw new UserMessageException(e);
            }
        }

        // EX : TEMPTG20181206003
        String preFix = "TEMP" + compId;
        return this.generateNum(preFix, "tr_require", "require_no");
    }

    /**
     * 產生原型確認單單號
     * 
     * @return
     * @throws UserMessageException
     */
    public synchronized String generatePtNum() throws UserMessageException {
        // AP本身不為REST時，由呼叫REST產生序號，避免多組AP同時觸發時，序號重複
        if (!FormNumberService.isFormNumberCenter) {
            try {
                return this.callGeneratorCenter(FormType.PTCHECK, "");
            } catch (SystemOperationException e) {
                throw new UserMessageException(e);
            }
        }
        return this.generateNum("PT", "work_pt_check", "pt_check_no");
    }

    /**
     * 產生送測單單號
     * 
     * @return
     * @throws UserMessageException
     */
    public synchronized String generateWtNum() throws UserMessageException {

        // AP本身不為REST時，由呼叫REST產生序號，避免多組AP同時觸發時，序號重複
        if (!FormNumberService.isFormNumberCenter) {
            try {
                return this.callGeneratorCenter(FormType.WORKTESTSIGNINFO, "");
            } catch (SystemOperationException e) {
                throw new UserMessageException(e);
            }
        }

        return this.generateNum("WT", "work_test_info", "testinfo_no");
    }

    /**
     * 產生on程式單單號
     * 
     * @return
     * @throws UserMessageException
     */
    public synchronized String generateOpNum() throws UserMessageException {
        // AP本身不為REST時，由呼叫REST產生序號，避免多組AP同時觸發時，序號重複
        if (!FormNumberService.isFormNumberCenter) {
            try {
                return this.callGeneratorCenter(FormType.WORKONPG, "");
            } catch (SystemOperationException e) {
                throw new UserMessageException(e);
            }
        }

        return this.generateNum("OP", "work_onpg", "onpg_no");
    }

    /**
     * 產生其它設定資訊單單號
     * 
     * @return
     * @throws UserMessageException
     */
    public synchronized String generateOsNum() throws UserMessageException {

        // AP本身不為REST時，由呼叫REST產生序號，避免多組AP同時觸發時，序號重複
        if (!FormNumberService.isFormNumberCenter) {
            try {
                return this.callGeneratorCenter(FormType.OTHSET, "");
            } catch (SystemOperationException e) {
                throw new UserMessageException(e);
            }
        }

        return this.generateNum("OS", "tr_os", "os_no");
    }

    /**
     * 產生序號
     * 
     * @param sysName      系統名稱
     * @param tableName    系統主表
     * @param noColumnName 欄位名稱
     * @return
     */
    private String generateNum(String sysName, String tableName, String noColumnName) {
        // 今日序號前綴
        String todayNoPrefix = sysName + datePatten.format(new Date());

        // 查詢序號最大值
        String maxNo = this.queryMaxNum(tableName, noColumnName, todayNoPrefix);
        maxNo = WkStringUtils.safeTrim(maxNo);

        // 從未產生序號, 或最大值非今日序號時 , 回傳 001
        if (WkStringUtils.isEmpty(maxNo) || !maxNo.startsWith(todayNoPrefix)) {
            String no = this.generateCaseNum(todayNoPrefix, this.naverDuplicate(todayNoPrefix, 0));
            log.debug("產生單號：[{}]", no);
            return no;
        }

        // 解析已存在的最大序號序號
        String dbMaxSNoStr = "0";
        if (maxNo.length() > todayNoPrefix.length()) {
            dbMaxSNoStr = maxNo.substring(todayNoPrefix.length());
        }

        // 非數字時, 回傳初始序號 (應該不可能)
        if (!WkStringUtils.isNumber(dbMaxSNoStr)) {
            return this.generateCaseNum(todayNoPrefix, this.naverDuplicate(todayNoPrefix, 0));
        }

        // 本次流水號為最大流水號 + 1
        int serialNo = this.naverDuplicate(todayNoPrefix, Integer.parseInt(dbMaxSNoStr));

        String no = this.generateCaseNum(todayNoPrefix, serialNo);
        log.debug("產生單號：[{}]", no);
        return no;
    }

    /**
     * 依據 pattern 產生案號
     * 
     * @param todayNoPrefix
     * @param serialNo
     * @return
     */
    private String generateCaseNum(String todayNoPrefix, Integer serialNo) {
        return todayNoPrefix + WkStringUtils.padding(serialNo + "", '0', NO_LENGTH, true);
    }

    /**
     * 查詢欄位最大值
     * 
     * @param tableName
     * @param noColumnName
     * @return
     */
    private String queryMaxNum(String tableName, String noColumnName, String todayNoPrefix) {

        StringBuffer varname1 = new StringBuffer();
        varname1.append("SELECT Max(" + noColumnName + ") ");
        varname1.append("FROM   " + tableName + " ");
        varname1.append("WHERE  " + noColumnName + " like '" + todayNoPrefix + "%' ");

        return this.jdbcTemplate.queryForObject(varname1.toString(), new Object[] {}, String.class);
    }

    /**
     * 記錄已使用過的序號, 避免重複
     */
    private Map<String, Integer> usedMapByTodayNoPrefix = Maps.newHashMap();

    /**
     * 1.將傳入序號 + 1 2.將新序號與已使用序號比對, 避免重複
     * 
     * @param todayNoPrefix
     * @param dbMaxSNo
     * @return
     */
    private Integer naverDuplicate(String todayNoPrefix, int dbMaxSNo) {
        // 新序號 = 資料庫中最大的序號 + 1
        int newSNo = dbMaxSNo + 1;

        // 判斷產生的新序號還未用過
        // 1.本組序號今日還未產生
        // 2.產生的新序號大於已用過的序號
        if (!usedMapByTodayNoPrefix.containsKey(todayNoPrefix)
                || newSNo > usedMapByTodayNoPrefix.get(todayNoPrefix)) {

            // 將新序號放入已使用區
            usedMapByTodayNoPrefix.put(todayNoPrefix, newSNo);
            // 回傳新序號
            return newSNo;
        }

        // 遞迴重取新序號
        return naverDuplicate(todayNoPrefix, newSNo);
    }

    /**
     * 呼叫單號產生中心取得單號
     * 
     * @param formType 單據類別
     * @param compId   公司代碼
     * @return
     * @throws SystemOperationException
     */
    private String callGeneratorCenter(FormType formType, String compId) throws SystemOperationException {
        try {

            String formNo = this.reqFormNumberClient.formNumberGenerator(formType.name(), compId);
            log.debug("call rest 取得單號:[{}]", formNo);
            return formNo;
        } catch (Exception e) {
            String errorMessage = WkMessage.EXECTION + "(取得單號失敗!)";
            log.error(errorMessage + "FormType[" + formType + "] compId:[" + compId + "]\r\n" + e.getMessage(), e);
            throw new TechRequestAlertException(errorMessage, compId);
        }
    }

}
