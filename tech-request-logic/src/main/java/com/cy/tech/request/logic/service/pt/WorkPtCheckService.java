/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.pt;

import com.cy.tech.request.repository.pt.WorkPtCheckRepo;
import com.cy.tech.request.vo.pt.WorkPtCheck;
import com.cy.work.common.vo.value.to.JsonStringListTo;

import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 原型確認Service
 *
 * @author brain0925_liao
 */
@Component
public class WorkPtCheckService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7587131957099652408L;
    /** WorkPtCheckRepo */
    @Autowired
    private WorkPtCheckRepo workPtCheckRepo;

    /**
     * 取得原型確認 By ptNo
     *
     * @param ptNo
     * @return
     */
    public WorkPtCheck findByPtNo(String ptNo) {
        return workPtCheckRepo.findByPtNo(ptNo);
    }

    /**
     * 更新原型確認建立部門 By ptNo
     *
     * @param depSid 建立部門Sid
     * @param ptNo
     */
    public void updateDepsidByPtNo(Integer depSid, String ptNo) {
        workPtCheckRepo.updateDepsid(ptNo, depSid);
    }

    /**
     * 更新原型確認通知部門 By Sid
     *
     * @param sid 原型確認Sid
     * @param noticeDeps 通知部門物件
     * @return
     */
    public int updateNoticeDeps(String sid, JsonStringListTo noticeDeps) {
        return workPtCheckRepo.updateNoticeDeps(sid, noticeDeps);
    }

}
