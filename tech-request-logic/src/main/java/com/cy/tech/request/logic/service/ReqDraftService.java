/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.tech.request.repository.require.RequireRepository;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireAttachment;
import com.cy.tech.request.vo.template.CategoryKeyMapping;
import com.cy.tech.request.vo.template.component.ComBase;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.vo.AttachmentService;
import com.google.common.base.Preconditions;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 需求單草稿服務
 *
 * @author shaun
 */
@Slf4j
@Component
public class ReqDraftService implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -1414792318991565190L;
    @Autowired
	@Qualifier("require_attach")
	private AttachmentService<RequireAttachment, Require, String> reqAttachService;
	@Autowired
	private RequireService requireService;
	@Autowired
	private TemplateService templateService;
	@Autowired
	private RequireRepository requireDao;

	/**
	 * 儲存草稿
	 * @param execUser
	 * @param editRequire
	 * @param selectedCheckItemTypes
	 * @param mapping
	 * @param valueMap
	 * @return
	 * @throws UserMessageException
	 */
	@Transactional(rollbackFor = Exception.class)
	public Require saveDraft(
	        User execUser,
	        Require editRequire,
	        List<RequireCheckItemType> selectedCheckItemTypes,
	        CategoryKeyMapping mapping,
	        Map<String, ComBase> valueMap) throws UserMessageException {

		Date execDate = new Date();

		// 設定初始化欄位值 for 草稿
		this.requireService.initDraft(editRequire, mapping, execUser.getSid(), execDate);

		// 儲存主檔資料, 且異動相關資料
		this.requireService.saveContentAndRelationData(
		        editRequire,
		        selectedCheckItemTypes,
		        valueMap,
		        execUser,
		        execDate,
		        false);

		return editRequire;

	}

	/**
     * 草稿更新<BR/>
     * 1. 更新模版內容 <BR/>
     * 2. 重建索引、css內容重建 <BR/>
	 * @param execUser
	 * @param editRequire
	 * @param selectedCheckItemTypes
	 * @param valueMap
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public Require updateDraft(
	        User execUser,
	        Require editRequire,
	        List<RequireCheckItemType> selectedCheckItemTypes,
	        Map<String, ComBase> valueMap) {

		Date execDate = new Date();

		// 儲存主檔資料, 且異動相關資料
		this.requireService.saveContentAndRelationData(
		        editRequire,
		        selectedCheckItemTypes,
		        valueMap,
		        execUser,
		        execDate,
		        false);

		return editRequire;

	}

	/**
	 * 檢查是否可提交草稿
	 *
	 * @param editDraft
	 */
	public void checkCanSubmit(Require editDraft) {
		Preconditions.checkArgument(
		        editDraft.getMapping().getSmall().getStatus().equals(Activation.ACTIVE),
		        "需求單模板已停用，請重新依據需求資訊重新選擇模板填寫需求，謝謝！");
		Preconditions.checkArgument(
		        templateService.isLastVerByMapping(editDraft.getMapping()),
		        "需求單模版版本已異動，請重新填寫需求單，謝謝！");
	}

	/**
	 * 草稿提交
	 * 
	 * @param executor
	 * @param execDept
	 * @param execComp
	 * @param editRequire
	 * @param valueMap
	 * @return
	 * @throws ProcessRestException
	 * @throws UserMessageException
	 * @throws SystemDevelopException
	 */
	

	@Transactional(rollbackFor = Exception.class)
	public void draftDelete(User executor, Require editReq, Map<String, ComBase> valueMap) {
		Require delReq = requireService.findByReqObj(editReq);
		String delTheme = templateService.findTheme(delReq, valueMap);
		String delExecName = executor.getName();
		String delTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
		requireService.removeCssAndIndex(delReq.getSid());
		requireDao.delete(delReq);
		log.info("需求單建立者:【" + delExecName + "】 於【" + delTime + "】 將需求單主題【" + delTheme + "】進行刪除。");
	}

}
