/**
 * 
 */
package com.cy.tech.request.logic.search.service;

import java.io.Serializable;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.utils.WkStringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Service
@Slf4j
public class SearchCommonHelper implements Serializable, InitializingBean {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -7003870219502067273L;

    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static SearchCommonHelper instance;

    /**
     * @return instance
     */
    public static SearchCommonHelper getInstance() { return instance; }

    private static void setInstance(SearchCommonHelper instance) { SearchCommonHelper.instance = instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    // ========================================================================
    // 服務
    // ========================================================================

    // ========================================================================
    // 方法
    // ========================================================================
    public void showSQLDebugLog(ReportType reportType, String sql, Map<String, Object> parameters) {

        // ====================================
        // 判斷是否需要顯示
        // ====================================
        if (!WorkBackendParamHelper.getInstance().isShowRequireSearchSql()) {
            return;
        }

        // ====================================
        // 組 log
        // ====================================
        try {
            String logContent = "";
            logContent += "\r\n" + reportType.getReportName();
            logContent += "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql) + "\r\n】";
            if (WkStringUtils.notEmpty(parameters)) {
                logContent += "\r\nPARAMs:【\r\n" + com.cy.work.common.utils.WkJsonUtils.getInstance().toPettyJson(parameters) + "\r\n】";
            }
            logContent += "\r\n";
            log.debug(logContent);
        } catch (Exception e) {
            log.error("組SQL LOG 失敗!" + e.getMessage(), e);
        }
    }

}
