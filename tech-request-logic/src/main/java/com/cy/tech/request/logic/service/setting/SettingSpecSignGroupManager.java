package com.cy.tech.request.logic.service.setting;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.repository.setting.SettingSpecSignGroupRepository;
import com.cy.tech.request.vo.setting.SettingSpecSignGroup;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 *
 */
@Slf4j
@Service
public class SettingSpecSignGroupManager implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -581777059763617636L;
    private static SettingSpecSignGroupManager instance;

    public static SettingSpecSignGroupManager getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        SettingSpecSignGroupManager.instance = this;
    }

    @Autowired
    private SettingSpecSignGroupRepository settingSpecSignGroupRepository;

    /**
     * 取得簽核群組設定
     * 
     * @param petitionType 簽呈類別
     * @param depSid       申請單位
     * @return
     * @throws SystemOperationException
     */
    public SettingSpecSignGroup findGroupByDepSid(
            Integer depSid) throws SystemOperationException {

        // ====================================
        // 依據傳入單位，查詢公司別
        // ====================================
        Org dep = WkOrgCache.getInstance().findBySid(depSid);
        if (dep == null || dep.getCompany() == null || dep.getCompany().getSid() == null) {
            log.warn("找不到傳入單位相關資訊 depsid:[{}]", depSid);
            return null;
        }

        // 公司別ID
        String compID = dep.getCompany().getId();

        // ====================================
        // 依據公司別查詢設定資料
        // ====================================
        List<SettingSpecSignGroup> settingSpecSignGroups = this.settingSpecSignGroupRepository.findByCompIDAndStatus(
                compID,
                Activation.ACTIVE);

        if (WkStringUtils.isEmpty(settingSpecSignGroups)) {
            return null;
        }

        // ====================================
        // 取得對應設定檔
        // ====================================
        // 逐筆收集符合的設定檔
        List<SettingSpecSignGroup> targetSignGroups = Lists.newArrayList();
        for (SettingSpecSignGroup settingSignGroup : settingSpecSignGroups) {

            // 比對群組單位
            // 計算設定檔的群組單位
            Set<Integer> groupDepSids = WkOrgUtils.prepareDepSidsByRule(
                    settingSignGroup.getDepSids(),
                    settingSignGroup.isDepIsBelow(),
                    settingSignGroup.getExceptDepSids(),
                    settingSignGroup.isExceptDepIsBelow());

            // 比對傳入單位是否命中
            if (groupDepSids.contains(depSid)) {
                targetSignGroups.add(settingSignGroup);
            }
        }

        // ====================================
        // 檢查設定錯誤
        // ====================================
        if (targetSignGroups.size() > 1) {
            String message = WkMessage.EXECTION + "簽核群組設定錯誤，符合資訊有多筆!";
            log.error(message + "depSid:[{}]\r\n{}", depSid, WkJsonUtils.getInstance().toPettyJson(targetSignGroups));
            throw new SystemOperationException(message, InfomationLevel.ERROR);
        }

        // ====================================
        // 回傳
        // ====================================
        if (WkStringUtils.isEmpty(targetSignGroups)) {
            // 沒找到對應設定
            return null;
        }

        return targetSignGroups.get(0);
    }

}
