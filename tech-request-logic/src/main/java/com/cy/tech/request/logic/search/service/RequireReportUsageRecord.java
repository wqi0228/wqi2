/**
 * 
 */
package com.cy.tech.request.logic.search.service;

import org.springframework.jdbc.core.JdbcTemplate;

import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.logic.lib.component.QueryReportUsageRecordComponent;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.utils.WkStringUtils;

/**
 * @author allen1214_wu
 */
public class RequireReportUsageRecord extends QueryReportUsageRecordComponent {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 2141046153295131568L;

    public RequireReportUsageRecord(ReportType reportType, Integer userSid) {

        super(
                reportType.getViewId(),
                reportType.getReportName(),
                "tr_usage_record",
                userSid,
                WorkSpringContextHolder.getBean(ReqConstants.REQ_JDBC_TEMPLATE, JdbcTemplate.class));

        if (WkStringUtils.isEmpty(reportType)) {
            throw new SystemDevelopException("未傳入 reportType");
        }
        if (WkStringUtils.isEmpty(userSid)) {
            throw new SystemDevelopException("未傳入 userSid");
        }
    }

}
