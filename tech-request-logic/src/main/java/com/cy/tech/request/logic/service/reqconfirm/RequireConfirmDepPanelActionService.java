/**
 * 
 */
package com.cy.tech.request.logic.service.reqconfirm;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.logic.service.RequireProcessCompleteRollbackService;
import com.cy.tech.request.logic.service.RequireProcessCompleteService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.RequireTraceService;
import com.cy.tech.request.logic.service.onpg.OnpgService;
import com.cy.tech.request.logic.service.othset.OthSetService;
import com.cy.tech.request.logic.service.pt.PtService;
import com.cy.tech.request.logic.utils.ProcessLog;
import com.cy.tech.request.logic.vo.RequireConfirmDepVO;
import com.cy.tech.request.vo.enums.OthSetStatus;
import com.cy.tech.request.vo.enums.ReqConfirmDepCompleteType;
import com.cy.tech.request.vo.enums.ReqConfirmDepProcType;
import com.cy.tech.request.vo.enums.ReqConfirmDepProgStatus;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireConfirmDep;
import com.cy.tech.request.vo.require.RequireTrace;
import com.cy.tech.request.vo.require.othset.OthSet;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 需求完成確認單位面板 helper
 * 
 * @author allen1214_wu
 */
@Service
@NoArgsConstructor
@Slf4j
public class RequireConfirmDepPanelActionService {

    // ========================================================================
    // 服務元件區
    // ========================================================================
    @Autowired
    private transient RequireService requireService;
    @Autowired
    private transient RequireConfirmDepService requireConfirmDepService;
    @Autowired
    private transient RequireConfirmDepHistoryService requireConfirmDepHistoryService;
    @Autowired
    private transient RequireProcessCompleteService requireProcessCompleteService;
    @Autowired
    private transient RequireProcessCompleteRollbackService requireProcessCompleteRollbackService;
    @Autowired
    private transient PtService ptService;
    @Autowired
    private transient OnpgService onpgService;
    @Autowired
    private transient OthSetService othSetService;
    @Autowired
    private transient RequireTraceService requireTraceService;

    // ========================================================================
    // 主方法區
    // ========================================================================
    /**
     * 異動負責人
     * 
     * @param sid
     * @param ownerSid
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public RequireConfirmDepVO changeOwner(
            String requireNo,
            RequireConfirmDepVO requireConfirmDepVO,
            Integer newOwnerSid,
            Integer execUserSid,
            Integer execDepSid,
            Date sysDate) {

        // ====================================
        // start log
        // ====================================
        ProcessLog processLog = new ProcessLog(
                requireNo,
                WkOrgUtils.findNameBySid(requireConfirmDepVO.getDepSid()),
                ReqConstants.REQ_CONFIRM + "-" + ReqConfirmDepProcType.CHANGE_OWNER.getLabel());

        log.info(processLog.prepareStartLog());

        // ====================================
        // 更新主檔
        // ====================================
        this.requireConfirmDepService.updateOwnerBySid(requireConfirmDepVO.getSid(), newOwnerSid);

        // ====================================
        // 組異動說明
        // ====================================
        String memo = "【"
                + this.requireConfirmDepService.prepareOwnerName(requireConfirmDepVO.getOwnerSid(), requireConfirmDepVO.getDepSid())
                + "】 → 【"
                + this.requireConfirmDepService.prepareOwnerName(newOwnerSid, requireConfirmDepVO.getDepSid())
                + "】";

        // ====================================
        // 建立異動歷程
        // ====================================
        // save
        this.requireConfirmDepHistoryService.saveHistory(
                requireNo,
                requireConfirmDepVO,
                ReqConfirmDepProcType.CHANGE_OWNER,
                memo,
                execUserSid,
                sysDate);

        // ====================================
        // complete log
        // ====================================
        log.info(processLog.prepareCompleteLog());

        // ====================================
        // 重撈資料
        // ====================================
        return requireConfirmDepService.findVoBySid(requireConfirmDepVO.getSid());
    }

    /**
     * 動作:領單
     */
    @Transactional(rollbackFor = Exception.class)
    public void actionReceive(
            String requireNo,
            Long requireConfirmDepSid,
            String memo,
            Integer execUserSid) {

        // 取得系統日
        Date sysDate = new Date();

        // ====================================
        // 查詢最新的資料
        // ====================================
        RequireConfirmDep requireConfirmDep = requireConfirmDepService.findBySid(requireConfirmDepSid);

        // ====================================
        // start log
        // ====================================
        ProcessLog processLog = new ProcessLog(
                requireNo,
                WkOrgUtils.findNameBySid(requireConfirmDep.getDepSid()),
                ReqConstants.REQ_CONFIRM + "-" + ReqConfirmDepProcType.RECEIVE.getLabel());

        log.info(processLog.prepareStartLog());

        // ====================================
        // update requireConfirmDep
        // ====================================
        requireConfirmDep.setReceived(true);
        requireConfirmDep.setProgStatus(ReqConfirmDepProgStatus.IN_PROCESS);
        requireConfirmDep.setUpdatedUser(execUserSid);
        requireConfirmDep.setUpdatedDate(sysDate);

        this.requireConfirmDepService.save(requireConfirmDep);

        // ====================================
        // 新增 requireConfirmDepHistory
        // ====================================
        this.requireConfirmDepHistoryService.saveHistory(
                requireNo,
                new RequireConfirmDepVO(requireConfirmDep),
                ReqConfirmDepProcType.RECEIVE,
                memo,
                execUserSid,
                sysDate);

        // ====================================
        // complete log
        // ====================================
        log.info(processLog.prepareCompleteLog());
    }

    /**
     * 動作:完成
     * 
     * @param requireNo            需求單號
     * @param requireConfirmDepSid 確認檔 sid
     * @param execUserSid          執行使用者
     * @param completeType         完成類型
     * @param completeMemo         完成說明
     * @return
     * @throws UserMessageException
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean actionComplete(
            String requireNo,
            Long requireConfirmDepSid,
            Integer execUserSid,
            ReqConfirmDepCompleteType completeType,
            String completeMemo) throws UserMessageException {

        // 取得系統日
        Date execDate = new Date();

        User execUser = WkUserCache.getInstance().findBySid(execUserSid);
        if (execUser == null) {
            log.error("系統異常，取不到使用者資料! [" + execUserSid + "]");
            throw new UserMessageException("系統異常，取不到使用者資料!");
        }

        // ====================================
        // 查詢最新的資料
        // ====================================
        // 查詢
        RequireConfirmDep requireConfirmDep = this.requireConfirmDepService.findBySid(requireConfirmDepSid);
        if (requireConfirmDep == null) {
            throw new UserMessageException("分派單位已被更新，將重新讀取資料!");
        }

        Require require = this.requireService.findByReqSid(requireConfirmDep.getRequireSid());
        if (require == null) {
            log.error("系統異常，取不到單據資料! [" + requireConfirmDep.getRequireSid() + "]");
            throw new UserMessageException("系統異常，取不到單據資料!");
        }

        // ====================================
        // start log
        // ====================================
        ProcessLog processLog = new ProcessLog(
                requireNo,
                WkOrgUtils.findNameBySid(requireConfirmDep.getDepSid()),
                ReqConstants.REQ_CONFIRM + "-" + ReqConfirmDepProcType.CONFIRM.getLabel() + "-" + completeType.getLabel());
        log.info(processLog.prepareStartLog());

        // ====================================
        // 結所有子單
        // ====================================
        this.actionComplete_SubCase(
                require,
                requireConfirmDep.getDepSid(),
                completeType,
                execUser,
                execDate);

        // ====================================
        // update requireConfirmDep
        // ====================================
        // 準備 update 資料
        requireConfirmDep.setProgStatus(ReqConfirmDepProgStatus.COMPLETE);
        requireConfirmDep.setCompleteType(completeType);
        requireConfirmDep.setCompleteMemo(completeMemo);
        requireConfirmDep.setConfirmUser(execUserSid);
        requireConfirmDep.setConfirmDate(execDate);
        requireConfirmDep.setUpdatedUser(execUserSid);
        requireConfirmDep.setUpdatedDate(execDate);

        // update
        this.requireConfirmDepService.save(requireConfirmDep);


		// ====================================
		// 若為最後一個完成單位時，異動需求單製作進度為『需求完成』
		// ====================================
		boolean isReqComplete = this.requireProcessCompleteService.executeByAllDepConfirm(
		        require.getSid(),
		        execUser,
		        execDate,
		        false,
		        false);

        // ====================================
        // 新增 requireConfirmDepHistory
        // ====================================
        // 判斷歷程類別
        ReqConfirmDepProcType procType = null;
        if (ReqConfirmDepCompleteType.FINISH.equals(completeType)) {
            procType = ReqConfirmDepProcType.CONFIRM;
        } else if (ReqConfirmDepCompleteType.WONT_DO.equals(completeType)) {
            procType = ReqConfirmDepProcType.WONT_DO;
        } else {
            throw new SystemDevelopException("傳入 completeType 錯誤!");
        }

        String hisMemo = procType.getLabel() + "說明：" + this.addBorderForMemo(completeMemo);
        if (isReqComplete) {
            hisMemo = "完成後同時異動需求製作進度為 【" + RequireStatusType.COMPLETED.getValue() + "】\r\n" + hisMemo;
        }

        this.requireConfirmDepHistoryService.saveHistory(
                requireNo,
                new RequireConfirmDepVO(requireConfirmDep),
                procType,
                hisMemo,
                execUserSid,
                execDate);

        // ====================================
        // complete log
        // ====================================
        log.info(processLog.prepareCompleteLog());

        return isReqComplete;
    }

    /**
     * 強制結子單
     */
    @Transactional(rollbackFor = Exception.class)
    private void actionComplete_SubCase(
            Require require,
            Integer depSid,
            ReqConfirmDepCompleteType completeType,
            User execUser,
            Date sysDate) {

        String message = "";
        String depName = WkOrgUtils.findNameBySid(Integer.valueOf(depSid + ""));

        // ====================================
        // start log
        // ====================================
        ProcessLog processLog = new ProcessLog(
                require.getRequireNo(),
                WkOrgUtils.findNameBySid(depSid),
                "強制結需求確認單位-所有開立子單");

        log.info(processLog.prepareStartLog());

        // ====================================
        // 需求確認單位, 可能由哪些分派部門組成
        // ====================================
        List<Org> createDeps = this.requireConfirmDepService.prepareReqConfirmDepRelationDep(
                Integer.valueOf(depSid + ""));

        // ====================================
        // 原型確認單
        // ====================================
        List<PtCheck> closePtChecks = this.ptService.forceComplete(require, PtStatus.UNIT_FORCE_CLOSE, execUser, sysDate, createDeps);
        if (WkStringUtils.notEmpty(closePtChecks)) {
            message += "<span style='text-decoration:underline;font-weight:bold;'>【原型確認單】" + closePtChecks.size() + " 則:</span><br/>";
            for (PtCheck ptCheck : closePtChecks) {
                message += String.format("★『%s』『%s』：%s<br/>",
                        ptCheck.getPtNo(),
                        ptCheck.getPtStatus().getLabel(),
                        ptCheck.getTheme());
            }
            message += "<br/>";
        }

        // ====================================
        // 送測單
        // ====================================
        // 暫不處理, 未得到 QA 單位回覆可以結掉

        // ====================================
        // ONPG單
        // ====================================
        List<WorkOnpg> closeWorkOnpgs = this.onpgService.forceComplete(require, WorkOnpgStatus.UNIT_FORCE_CLOSE, execUser, sysDate, createDeps);
        if (WkStringUtils.notEmpty(closeWorkOnpgs)) {
            message += "<span style='text-decoration:underline;font-weight:bold;'>【ON程式】" + closeWorkOnpgs.size() + " 則:</span><br/>";
            for (WorkOnpg workOnpg : closeWorkOnpgs) {
                message += String.format("★『%s』『%s』：%s<br/>",
                        workOnpg.getOnpgNo(),
                        workOnpg.getOnpgStatus().getLabel(),
                        workOnpg.getTheme());
            }
            message += "<br/>";
        }

        // ====================================
        // 其他設定資訊
        // ====================================
        List<OthSet> closeOthSetgs = this.othSetService.forceComplete(require, OthSetStatus.UNIT_FORCE_CLOSE, execUser, sysDate, createDeps);
        if (WkStringUtils.notEmpty(closeOthSetgs)) {
            message += "<span style='text-decoration:underline;font-weight:bold;'>【其他設定資訊】" + closeOthSetgs.size() + " 則:</span><br/>";
            for (OthSet othSet : closeOthSetgs) {
                message += String.format("★『%s』『%s』：%s<br/>",
                        othSet.getOsNo(),
                        othSet.getOsStatus().getVal(),
                        othSet.getTheme());
            }
            message += "<br/>";
        }

        if (WkStringUtils.isEmpty(message)) {
            log.info("[{}],需求單位 [{}] 沒有需要強制結案的子單",
                    require.getRequireNo(),
                    depName);
            return;
        }

        // ====================================
        // 寫追蹤資訊
        // ====================================
        message = String.format(
                "<span style='font-weight:bold;'>因【%s】進行需求進度確認，以下單據已強制結案</span><br/><br/>%s",
                depName,
                message);

        // 建立追蹤檔物件 (date 重新 new , 為了排序)
        RequireTrace requireTrace = this.requireTraceService.createNewTrace(
                require.getSid(), execUser, new Date());

        // 組追蹤訊息內容
        requireTrace.setRequireTraceContent(message);
        requireTrace.setRequireTraceContentCss(message);
        // 追蹤類別:因需求確認強制結子單
        requireTrace.setRequireTraceType(RequireTraceType.FORCE_CLOSE_SUBCASE_BY_UNIT_CONFIRM);

        // save
        this.requireTraceService.save(requireTrace);

        // ====================================
        // complete log
        // ====================================
        log.info(processLog.prepareCompleteLog());
    }

    /**
     * 動作:復原
     * 
     * @param requireConfirmDepSid
     * @param execUserSid
     * @param memo
     * @throws SystemOperationException
     */
    @Transactional(rollbackFor = Exception.class)
    public Boolean actionRecovary(
            String requireNo,
            Long requireConfirmDepSid,
            Integer execUserSid,
            String memo) throws SystemOperationException {

        // 取得系統日
        Date sysDate = new Date();

        User execUser = WkUserCache.getInstance().findBySid(execUserSid);
        if (execUser == null) {
            log.error("系統異常，取不到使用者資料! [" + execUserSid + "]");
            throw new SystemOperationException(WkMessage.NEED_RELOAD, InfomationLevel.ERROR);
        }

        // ====================================
        // 查詢最新的資料
        // ====================================
        // 查詢
        RequireConfirmDep requireConfirmDep = this.requireConfirmDepService.findBySid(requireConfirmDepSid);
        if (requireConfirmDep == null) {
            log.error("系統異常，取不到RequireConfirmDep資料! [" + requireConfirmDepSid + "]");
            throw new SystemOperationException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }

        Require require = this.requireService.findByReqSid(requireConfirmDep.getRequireSid());
        if (require == null) {
            log.error("系統異常，取不到單據資料! [" + requireConfirmDep.getRequireSid() + "]");
            throw new SystemOperationException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }

        // ====================================
        // 檢核狀態不對無須執行
        // ====================================
        if (requireConfirmDep != null
                && requireConfirmDep.getProgStatus() != null
                && !requireConfirmDep.getProgStatus().isInFinish()) {
            throw new SystemOperationException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }

        // ====================================
        // start log
        // ====================================
        ProcessLog processLog = new ProcessLog(
                requireNo,
                WkOrgUtils.findNameBySid(requireConfirmDep.getDepSid()),
                ReqConstants.REQ_CONFIRM + "-" + ReqConfirmDepProcType.RECOVERY.getLabel());

        log.info(processLog.prepareStartLog());

        // ====================================
        // update requireConfirmDep、insert tr_require_confirm_dep_history
        // ====================================
        String traceMessage = "復原說明：" + this.addBorderForMemo(memo);
        // 狀態為可反需求完成時, 於以下流程會進行狀態復原, 故在此加上相關訊息
        if (require.getRequireStatus() != null && require.getRequireStatus().isCanUseRequireConfirm()) {
            traceMessage = "復原單位進度，並取消需求完成\r\n" + traceMessage;
        }

        // 重新開啟需求單位確認流程
        this.requireConfirmDepService.reOpenProgStatus(
                requireConfirmDep,
                requireNo,
                execUserSid,
                sysDate,
                ReqConfirmDepProcType.RECOVERY,
                traceMessage);

        // ====================================
        // 若需求製作進度為已完成, 將進度倒回進行中
        // ====================================
        boolean isRoolbackReqStatus = this.requireProcessCompleteRollbackService.executeByConfirmDepRecovery(
                require.getSid(),
                requireConfirmDep,
                execUser,
                sysDate);

        // ====================================
        // complete log
        // ====================================
        log.info(processLog.prepareCompleteLog());

        return isRoolbackReqStatus;
    }

    // ========================================================================
    // 附屬方法區
    // ========================================================================
    /**
     * 說明內容加上外方框
     * 
     * @param memo
     * @return
     */
    private String addBorderForMemo(String memo) {
        if (WkStringUtils.isEmpty(memo)) {
            return "無 ";
        }
        return "<div style='"
                + "width:95%;"
                + "border:1px solid #009ceb;"
                + "padding-left: 5px;"
                + "'>"
                + memo
                + "</span><br/>";
    }



}
