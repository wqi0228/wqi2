/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.send.test;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.logic.vo.WorkTestInfoTo;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.vo.worktest.enums.WorkTestInfoStatus;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * 送測顯示處理
 *
 * @author shaun
 */
@Component
@Slf4j
public class SendTestShowService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7381029144272775303L;
    @Autowired
    private transient WorkBackendParamHelper workBackendParamHelper;
    @Autowired
    private UserService userService;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private SendTestService sendTestService;

    /**
     * 組合送測單位抬頭訊息
     *
     * @param testinfo
     * @return
     */
    public String findSendTestDepTitle(WorkTestInfo testinfo) {
        if (testinfo == null) {
            return "";
        }
        return testinfo.getSendTestDep().getValue().stream()
                .map(each -> WkOrgCache.getInstance().findNameBySid(each))
                .sorted()
                .collect(Collectors.joining("、"));
    }

    /**
     * 組合送測單位抬頭訊息
     *
     * @param testinfo
     * @return
     */
    public List<String> findSendTestDepTitleByOp(String testNo) {
        if (WkStringUtils.isEmpty(testNo)) {
            return Lists.newArrayList();
        }
        WorkTestInfoTo testinfo = this.sendTestService.findToByTestinfoNo(testNo);
        return testinfo.getSendTestDep().getValue().stream()
                .map(each -> WkOrgCache.getInstance().findNameBySid(each))
                .sorted()
                .collect(Collectors.toList());
    }

    /**
     * 關閉編輯
     *
     * @param req
     * @param testInfo
     * @param login
     * @return
     */
    public boolean disableEdit(Require req, WorkTestInfo testInfo, User login) {
        if (testInfo == null || req == null || req.getCloseCode()
                || req.getRequireStatus().equals(RequireStatusType.SUSPENDED)
                || req.getRequireStatus().equals(RequireStatusType.COMPLETED)) {
            return true;
        }
        WorkTestStatus status = testInfo.getTestinfoStatus();
        if (testInfo.getHasSign() && status.equals(WorkTestStatus.SIGN_PROCESS)) {
            InstanceStatus is = testInfo.getSignInfo().getInstanceStatus();
            if (!is.equals(InstanceStatus.NEW_INSTANCE) && !is.equals(InstanceStatus.RECONSIDERATION)) {
                return true;
            }
        }
        if (!status.equals(WorkTestStatus.SONGCE) && !status.equals(WorkTestStatus.TESTING)
                && !status.equals(WorkTestStatus.RETEST) && !status.equals(WorkTestStatus.SIGN_PROCESS)) {
            return true;
        }
        // 填寫送測人員及其主管或被通知的成員可以進行編輯
        Integer primaryOrgSid = userService.findPrimaryOrgSid(login);
        boolean isNoticeMember = testInfo.getSendTestDep().getValue().contains(primaryOrgSid.toString());
        return !(this.isCreatedAndAboveManager(testInfo.getCreatedUser().getSid(), login) || isNoticeMember);
    }

    /**
     * 關閉送測單位
     *
     * @param req
     * @param testInfo
     * @param login
     * @return
     */
    public boolean disableTestDep(Require req, WorkTestInfo testInfo, User login) {
        // 填寫送測人員及其主管或被通知的成員可以進行編輯
        Integer primaryOrgSid = userService.findPrimaryOrgSid(login);
        boolean isNoticeMember = testInfo.getSendTestDep().getValue().contains(primaryOrgSid.toString());
        return !(this.isCreatedAndAboveManager(testInfo.getCreatedUser().getSid(), login) || isNoticeMember);
    }

    /**
     * 檢查登入者是否為填單者及其上層主管
     *
     * @param testInfo
     * @param login
     * @return
     */
    public boolean isCreatedAndAboveManager(Integer createUserSid, User login) {
        User createUser = WkUserCache.getInstance().findBySid(createUserSid);
        List<User> canClickDisableList = orgService.findOrgManagers(WkOrgCache.getInstance().findBySid(createUser.getPrimaryOrg().getSid()));
        if (!canClickDisableList.contains(createUser)) {
            canClickDisableList.add(createUser);
        }
        return canClickDisableList.contains(login);
    }

    /**
     * 關閉FBID編輯
     *
     * @param req
     * @param testInfo
     * @param login
     * @return
     */
    public boolean disableFbIdEdit(Require req, WorkTestInfo testInfo, User login) {
        if (testInfo == null || req == null || req.getCloseCode()
                || req.getRequireStatus().equals(RequireStatusType.SUSPENDED)
                || req.getRequireStatus().equals(RequireStatusType.COMPLETED)) {
            return true;
        }
        return !this.isCreatedAndAboveManager(testInfo.getCreatedUser().getSid(), login);
    }

    /**
     * 關閉測試回覆
     *
     * @param req
     * @param testInfo
     * @param login
     * @return
     */
    public boolean disableTestReply(Require req, WorkTestInfo testInfo, User login) {
        login = WkUserCache.getInstance().findBySid(login.getSid());
        User createUser = WkUserCache.getInstance().findBySid(testInfo.getCreatedUser().getSid());
        List<User> canClickDisableList = orgService.findOrgManagers(WkOrgCache.getInstance().findBySid(createUser.getPrimaryOrg().getSid()));
        if (!canClickDisableList.contains(createUser)) {
            canClickDisableList.add(createUser);
        }

        // 填送測單成員+主管可以回覆
        boolean canRely = this.isCreatedAndAboveManager(testInfo.getCreatedUser().getSid(), login);
        Integer primaryOrgSid = userService.findPrimaryOrgSid(login);
        boolean canReplyDep = testInfo.getSendTestDep().getValue().contains(primaryOrgSid.toString());

        User reqCreateUser = WkUserCache.getInstance().findBySid(req.getCreatedUser().getSid());
        Org reqCreateDep = WkOrgCache.getInstance().findBySid(req.getCreateDep().getSid());
        // 需求單位包含在送測通知單位內時
        boolean sendTestDepContainReqDep = testInfo.getSendTestDep().getValue().contains(String.valueOf(reqCreateDep.getSid()));
        // 需求單位主管群
        boolean canDoManagerByReq = orgService.findOrgManagers(WkOrgCache.getInstance().findBySid(reqCreateUser.getPrimaryOrg().getSid())).contains(login);
        // 需求單位
        Org loginPrimaryOrg = userService.findPrimaryOrg(login);
        boolean canDoDep = reqCreateDep.equals(loginPrimaryOrg);

        return !((canRely || canReplyDep) || (sendTestDepContainReqDep && (canDoManagerByReq || canDoDep)));
    }

    /**
     * 顯示重測
     *
     * @param req
     * @param testInfo
     * @param login
     * @return
     */
    public boolean renderedRetest(Require req, WorkTestInfo testInfo, User login) {
        if (testInfo == null || req == null || req.getCloseCode()
                || req.getRequireStatus().equals(RequireStatusType.SUSPENDED)
                || req.getRequireStatus().equals(RequireStatusType.COMPLETED)) {
            return false;
        }
        if (WorkTestStatus.ROLL_BACK_TEST.equals(testInfo.getTestinfoStatus())) {
            return this.isCreatedAndAboveManager(testInfo.getCreatedUser().getSid(), login);
        }
        return false;
    }

    /**
     * 顯示QA轉測試報告
     *
     * @param req
     * @param testInfo
     * @param login
     * @return
     */
    public boolean renderedTestReportBtn(Require req, WorkTestInfo testInfo, User login) {

        // REQ-1551【送測單】『測試報告』使用權限，移除主單狀態卡控
        // if (testInfo == null || req == null || req.getCloseCode()
        // || req.getRequireStatus().equals(RequireStatusType.SUSPENDED)
        // || req.getRequireStatus().equals(RequireStatusType.COMPLETED)) {
        // return false;
        // }

        if (testInfo == null || req == null) {
            log.error("傳入 WorkTestInfo 或 Require 為空!");
            return false;
        }

        WorkTestStatus status = testInfo.getTestinfoStatus();
        if (WorkTestStatus.SIGN_PROCESS.equals(status)
                || WorkTestStatus.VERIFY_INVAILD.equals(status)
                || WorkTestStatus.ROLL_BACK_TEST.equals(status)
                || WorkTestStatus.CANCEL_TEST.equals(status)) {
            return false;
        }
        boolean isSongce = WorkTestStatus.SONGCE.equals(testInfo.getTestinfoStatus());
        boolean isTesting = WorkTestStatus.TESTING.equals(testInfo.getTestinfoStatus());
        boolean isRetest = WorkTestStatus.RETEST.equals(testInfo.getTestinfoStatus());
        boolean isQaTestComplete = WorkTestStatus.QA_TEST_COMPLETE.equals(testInfo.getTestinfoStatus());
        boolean isApproved = WorkTestInfoStatus.APPROVED.equals(testInfo.getQaAuditStatus());
        boolean isJoinSchedule = WorkTestInfoStatus.JOIN_SCHEDULE.equals(testInfo.getQaScheduleStatus());
        boolean isCommit = WorkTestInfoStatus.COMMITED.equals(testInfo.getCommitStatus());
        if ((isSongce && isApproved && isJoinSchedule && isCommit)
                || (isTesting && isApproved && isJoinSchedule && isCommit)
                || (isRetest && isApproved && isJoinSchedule && isCommit)
                || isQaTestComplete) {

            List<String> qaDepSids = this.workBackendParamHelper.getQADepSids().stream()
                    .map(each -> each + "")
                    .collect(Collectors.toList());

            // 送測單位中包含QA部門
            boolean sendTestUnitContainQaDep = !Collections.disjoint(testInfo.getSendTestDep().getValue(), qaDepSids);
            // 是否為QA部門人員
            Integer primaryOrgSid = userService.findPrimaryOrgSid(login);
            boolean isQaMember = qaDepSids.contains(primaryOrgSid.toString());
            return sendTestUnitContainQaDep && isQaMember;
        }
        return false;
    }

    /**
     * 顯示取消測試
     */
    public boolean renderedCancelTest(WorkTestInfo testInfo, User loginUser) {

        // ====================================
        // 防呆
        // ====================================
        if (testInfo == null) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "傳入 testInfo 為空");
            return false;
        }

        if (loginUser == null) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "傳入 loginUser 為空");
            return false;
        }

        boolean isQaUser = workBackendParamHelper.isQAUser(loginUser);
        boolean isSongce = WorkTestStatus.SONGCE.equals(testInfo.getTestinfoStatus());
        boolean isTesting = WorkTestStatus.TESTING.equals(testInfo.getTestinfoStatus());
        boolean isRetest = WorkTestStatus.RETEST.equals(testInfo.getTestinfoStatus());

        boolean isApproved = WorkTestInfoStatus.APPROVED.equals(testInfo.getQaAuditStatus());
        boolean isWaitApprove = WorkTestInfoStatus.WAIT_APPROVE.equals(testInfo.getQaAuditStatus());
        boolean isUnneedApprove = WorkTestInfoStatus.UNNEEDED_APPROVE.equals(testInfo.getQaAuditStatus());
        boolean isJoinSchedule = WorkTestInfoStatus.JOIN_SCHEDULE.equals(testInfo.getQaScheduleStatus());
        boolean isUnjoinSchedule = WorkTestInfoStatus.UNJOIN_SCHEDULE.equals(testInfo.getQaScheduleStatus());
        boolean isUncommit = WorkTestInfoStatus.UNCOMMIT.equals(testInfo.getCommitStatus());
        boolean isCommit = WorkTestInfoStatus.COMMITED.equals(testInfo.getCommitStatus());

        if (isSongce && isWaitApprove && isQaUser) {
            return true;
        } else if ((isSongce && isWaitApprove)
                || (isSongce && isUnneedApprove)
                || (isSongce && isApproved && isUnjoinSchedule)
                || (isTesting && isUnneedApprove)
                || (isTesting && isApproved && isUnjoinSchedule)
                || (isRetest && isUnneedApprove)
                || (isRetest && isApproved && isUnjoinSchedule)) {

            return isSameUnitAndManagers(testInfo.getCreateDep(), loginUser);
        }

        if ((isSongce && isApproved && isJoinSchedule && isUncommit)
                || (isSongce && isApproved && isJoinSchedule && isCommit)
                || (isTesting && isApproved && isJoinSchedule && isCommit)
                || (isRetest && isApproved && isJoinSchedule && isCommit)) {

            return isQaUser;
        }
        return false;
    }

    /**
     * 顯示測試完成
     *
     * @param req
     * @param testInfo
     * @param login
     * @return
     */
    public boolean renderedTestComplate(Require req, WorkTestInfo testInfo, User login) {
        if (testInfo == null || req == null || req.getCloseCode()
                || req.getRequireStatus().equals(RequireStatusType.SUSPENDED)
                || req.getRequireStatus().equals(RequireStatusType.COMPLETED)) {
            return false;
        }
        WorkTestStatus status = testInfo.getTestinfoStatus();
        if (WorkTestStatus.SIGN_PROCESS.equals(status)
                || WorkTestStatus.VERIFY_INVAILD.equals(status)
                || WorkTestStatus.ROLL_BACK_TEST.equals(status)
                || WorkTestStatus.CANCEL_TEST.equals(status)) {
            return false;
        }
        boolean isSongce = WorkTestStatus.SONGCE.equals(testInfo.getTestinfoStatus());
        boolean isTesting = WorkTestStatus.TESTING.equals(testInfo.getTestinfoStatus());
        boolean isRetest = WorkTestStatus.RETEST.equals(testInfo.getTestinfoStatus());
        boolean isApproved = WorkTestInfoStatus.APPROVED.equals(testInfo.getQaAuditStatus());
        boolean isUnneedApprove = WorkTestInfoStatus.UNNEEDED_APPROVE.equals(testInfo.getQaAuditStatus());
        boolean isUnjoinSchedule = WorkTestInfoStatus.UNJOIN_SCHEDULE.equals(testInfo.getQaScheduleStatus());
        if ((isSongce && isUnneedApprove)
                || (isSongce && isApproved && isUnjoinSchedule)
                || (isTesting && isUnneedApprove)
                || (isTesting && isApproved && isUnjoinSchedule)
                || (isRetest && isUnneedApprove)
                || (isRetest && isApproved && isUnjoinSchedule)) {

            User reqCreateUser = WkUserCache.getInstance().findBySid(req.getCreatedUser().getSid());
            Org reqCreateDep = WkOrgCache.getInstance().findBySid(req.getCreateDep().getSid());
            // 需求單位包含在送測通知單位內時
            boolean sendTestDepContainReqDep = testInfo.getSendTestDep().getValue().contains(String.valueOf(reqCreateDep.getSid()));
            // 需求單位主管群
            boolean canDoManager = orgService.findOrgManagers(WkOrgCache.getInstance().findBySid(reqCreateUser.getPrimaryOrg().getSid())).contains(login);
            // 需求單位
            Org primaryOrg = userService.findPrimaryOrg(login);
            boolean canDoDep = reqCreateDep.equals(primaryOrg);
            return sendTestDepContainReqDep && (canDoDep || canDoManager);
        }
        return false;
    }

    /**
     * 關閉送測歷程查詢
     *
     * @param req
     * @param testInfo
     * @return
     */
    public boolean disableTestHistory(Require req, WorkTestInfo testInfo) {
        if (testInfo == null) {
            return true;
        }
        WorkTestStatus status = testInfo.getTestinfoStatus();
        boolean disableStatus = status.equals(WorkTestStatus.SIGN_PROCESS) || status.equals(WorkTestStatus.VERIFY_INVAILD);
        return disableStatus;
    }

    /**
     * 顯示簽核資訊查詢(渲染)
     *
     * @param req
     * @param testInfo
     * @return
     */
    public boolean showSignInfo(Require req, WorkTestInfo testInfo) {
        if (testInfo == null) {
            return false;
        }
        return testInfo.getHasSign();
    }

    /**
     * 顯示編輯QAReport按鍵
     *
     * @param login
     * @return
     */
    public boolean showQAReportEdit(User loginUser) {
        if (loginUser == null) {
            return false;
        }
        return this.workBackendParamHelper.isQAUser(loginUser.getSid());
    }

    /**
     * 關閉送測附加檔案上傳功能
     *
     * @param testInfo
     * @return
     */
    public boolean disableUploadFun(Require req, WorkTestInfo testInfo) {
        if (testInfo == null || req == null || req.getCloseCode()
                || req.getRequireStatus().equals(RequireStatusType.SUSPENDED)
                || req.getRequireStatus().equals(RequireStatusType.COMPLETED)) {
            return true;
        }
        WorkTestStatus status = testInfo.getTestinfoStatus();
        return !(status.equals(WorkTestStatus.SONGCE) || status.equals(WorkTestStatus.TESTING) || status.equals(WorkTestStatus.RETEST)
                || status.equals(WorkTestStatus.SIGN_PROCESS));
    }

    /**
     * 是否login user 為 "同單位的同仁們" 與 "主管"
     *
     * @param testInfo
     * @param login
     * @return
     */
    public boolean isSameUnitAndManagers(Org org, User login) {
        List<User> users = Lists.newArrayList();
        users.addAll(orgService.findOrgManagers(org));
        users.addAll(WkUserCache.getInstance().getUsersByPrimaryOrgSid(org.getSid()));
        return users.contains(login);
    }

    /**
     * 建單者、以及建單者的 所歸屬的部級所有同仁 (建單者若為處級以上，則歸屬該處級同仁都可修改
     */
    public boolean hasModifyPermission(WorkTestInfo testInfo, Org loginOrg) {
        if (WorkTestStatus.SONGCE.equals(testInfo.getTestinfoStatus())
                || WorkTestStatus.TESTING.equals(testInfo.getTestinfoStatus())
                || WorkTestStatus.RETEST.equals(testInfo.getTestinfoStatus())) {

            Org createDept = WkOrgCache.getInstance().findBySid(testInfo.getCreateDep().getSid());
            List<Org> list = orgService.findChildOrgsByBasicOrg(createDept);
            return list.contains(loginOrg);
        }
        return false;
    }

    /**
     * 是否顯示納入排程按鈕
     */
    public boolean renderedJoinScheduleBtn(WorkTestInfo testInfo, boolean isQAManager) {
        if (sendTestService.isContainsQADepts(testInfo)) {
            boolean isSongce = WorkTestStatus.SONGCE.equals(testInfo.getTestinfoStatus());
            boolean isWaitApprove = WorkTestInfoStatus.WAIT_APPROVE.equals(testInfo.getQaAuditStatus());
            if (isSongce && isWaitApprove) {
                return isQAManager;
            }
        }
        return false;
    }

    /**
     * 是否顯示不納入排程按鈕
     */
    public boolean renderedUnjoinScheduleBtn(WorkTestInfo testInfo, boolean isQAManager) {
        if (sendTestService.isContainsQADepts(testInfo)) {
            boolean isSongce = WorkTestStatus.SONGCE.equals(testInfo.getTestinfoStatus());
            boolean isWaitApprove = WorkTestInfoStatus.WAIT_APPROVE.equals(testInfo.getQaAuditStatus());
            if (isSongce && isWaitApprove) {
                return isQAManager;
            }
        }
        return false;
    }

    /**
     * 顯示退測按鈕
     */
    public boolean renderedRollbackTest(Require req, WorkTestInfo testInfo, User loginUser) {

        // 主單非已完成或需求暫緩
        if (testInfo == null || req == null || req.getCloseCode()
                || req.getRequireStatus().equals(RequireStatusType.SUSPENDED)
                || req.getRequireStatus().equals(RequireStatusType.COMPLETED)) {
            return false;
        }

        // 僅送測、測試中、重測、退測狀態可使用
        if (!(WorkTestStatus.SONGCE.equals(testInfo.getTestinfoStatus())
                || WorkTestStatus.TESTING.equals(testInfo.getTestinfoStatus())
                || WorkTestStatus.RETEST.equals(testInfo.getTestinfoStatus())
                || WorkTestStatus.ROLL_BACK_TEST.equals(testInfo.getTestinfoStatus()))) {
            return false;
        }

        boolean isQaUser = workBackendParamHelper.isQAUser(loginUser);
        boolean isSongce = WorkTestStatus.SONGCE.equals(testInfo.getTestinfoStatus());
        boolean isTesting = WorkTestStatus.TESTING.equals(testInfo.getTestinfoStatus());
        boolean isRetest = WorkTestStatus.RETEST.equals(testInfo.getTestinfoStatus());
        // QA審核狀態 - 已審核
        boolean isApproved = WorkTestInfoStatus.APPROVED.equals(testInfo.getQaAuditStatus());
        // QA審核狀態 - 不需審核
        boolean isUnneedApproved = WorkTestInfoStatus.UNNEEDED_APPROVE.equals(testInfo.getQaAuditStatus());
        // QA排程狀態 - 納入排程
        boolean isJoinSchedule = WorkTestInfoStatus.JOIN_SCHEDULE.equals(testInfo.getQaScheduleStatus());
        // QA排程狀態 - 不納入排程
        boolean isUnjoinSchedule = WorkTestInfoStatus.UNJOIN_SCHEDULE.equals(testInfo.getQaScheduleStatus());
        // QA排程狀態 - 不需排程
        boolean isUnneedSchedule = WorkTestInfoStatus.UNNEEDED_SCHEDULE.equals(testInfo.getQaScheduleStatus());

        if (isJoinSchedule) {
            if ((isSongce && isApproved) || (isTesting && isApproved) || (isRetest && isApproved)) {
                return isQaUser;
            }
        } else if (isUnjoinSchedule || isUnneedSchedule) {
            if ((isSongce && isUnneedApproved)
                    || (isSongce && isApproved && isUnjoinSchedule)
                    || (isTesting && isUnneedApproved)
                    || (isTesting && isApproved && isUnjoinSchedule)
                    || (isRetest && isUnneedApproved)
                    || (isRetest && isApproved && isUnjoinSchedule)) {

                // 需求單的需求單位
                boolean sendTestDepContainReqDep = testInfo.getSendTestDep().getValue().contains(String.valueOf(req.getCreateDep().getSid()));
                return sendTestDepContainReqDep && isSameUnitAndManagers(req.getCreateDep(), loginUser);
            }
        }

        return false;
    }

    /**
     * 是否顯示測試人員按鈕
     * 
     * @return
     */
    public boolean renderedTestersBtn(WorkTestInfo testInfo, boolean isQaManager) {

        return this.renderedTestersBtn(
                isQaManager,
                testInfo.getTestinfoStatus(),
                testInfo.getQaAuditStatus(),
                testInfo.getQaScheduleStatus());
    }

    public boolean renderedTestersBtn(
            boolean isQaManager,
            WorkTestStatus testinfoStatus,
            WorkTestInfoStatus qaAuditStatus,
            WorkTestInfoStatus qaScheduleStatus) {

        if (!isQaManager) {
            return false;
        }

        boolean isSongce = WorkTestStatus.SONGCE.equals(testinfoStatus);
        boolean isTesting = WorkTestStatus.TESTING.equals(testinfoStatus);
        boolean isRetest = WorkTestStatus.RETEST.equals(testinfoStatus);
        boolean isQaTestComplete = WorkTestStatus.QA_TEST_COMPLETE.equals(testinfoStatus);
        boolean isWaitApprove = WorkTestInfoStatus.WAIT_APPROVE.equals(qaAuditStatus);
        boolean isApproved = WorkTestInfoStatus.APPROVED.equals(qaAuditStatus);
        boolean isJoinSchedule = WorkTestInfoStatus.JOIN_SCHEDULE.equals(qaScheduleStatus);

        if ((isSongce && isWaitApprove)
                || (isSongce && isApproved && isJoinSchedule)
                || (isTesting && isApproved && isJoinSchedule)
                || (isRetest && isApproved && isJoinSchedule)
                || isQaTestComplete) {

            return true;
        }

        return false;
    }

    /**
     * 是否顯示文件補齊按鈕
     * 
     * @return
     */
    public boolean renderedCommitPaper(WorkTestInfo testInfo, User loginUser) {
        boolean isSongce = WorkTestStatus.SONGCE.equals(testInfo.getTestinfoStatus());
        boolean isRetest = WorkTestStatus.RETEST.equals(testInfo.getTestinfoStatus());
        boolean isApproved = WorkTestInfoStatus.APPROVED.equals(testInfo.getQaAuditStatus());
        boolean isUncommit = WorkTestInfoStatus.UNCOMMIT.equals(testInfo.getCommitStatus());
        if ((isSongce || isRetest) && isApproved && isUncommit) {

            Org createDep = testInfo.getCreateDep();
            if (createDep == null) {
                log.warn("送測單號：[{}], 申請單位為空!", testInfo.getTestinfoNo());
                return false;
            }

            // ====================================
            // 部以下單位，取基礎部門『部』以下成員
            // ====================================
            if (WkOrgUtils.getOrgLevelOrder(createDep.getLevel()) <= WkOrgUtils.getOrgLevelOrder(OrgLevel.MINISTERIAL)) {

                // -------------------
                // 取得基本部門『部』向下所有部門
                // -------------------
                Set<Org> deps = Sets.newHashSet();

                // 向上收束到部
                Org ministerialDep = WkOrgUtils.prepareBasicDep(testInfo.getCreateDep().getSid(), OrgLevel.MINISTERIAL);
                deps.add(ministerialDep);
                // 子部門
                List<Org> childOrg = WkOrgCache.getInstance().findAllChild(ministerialDep.getSid()).stream()
                        .filter(org -> Activation.ACTIVE.equals(org.getStatus()))
                        .collect(Collectors.toList());

                if (WkStringUtils.notEmpty(childOrg)) {
                    deps.addAll(childOrg);
                }

                // -------------------
                // 取得部門成員
                // -------------------
                Set<Integer> depUsers = Sets.newHashSet();

                for (Org dep : deps) {

                    // 部門成員
                    List<Integer> currDepUserSids = WkUserCache.getInstance().findUsersByPrimaryOrgSid(dep.getSid())
                            .stream()
                            .map(User::getSid)
                            .collect(Collectors.toList());

                    if (WkStringUtils.notEmpty(currDepUserSids)) {
                        depUsers.addAll(currDepUserSids);
                    }

                    // 直系向上所有主管
                    Set<Integer> directManagerUserSids = WkOrgCache.getInstance().findDirectManagerUserSids(dep.getSid());
                    if (WkStringUtils.notEmpty(directManagerUserSids)) {
                        depUsers.addAll(directManagerUserSids);
                    }

                }

                // -------------------
                // 比對登入者
                // -------------------
                return depUsers.contains(loginUser.getSid());

            }
            // ====================================
            // 處以上單位，原單位成員
            // ====================================
            else {
                return isSameUnitAndManagers(testInfo.getCreateDep(), loginUser);
            }

        }

        return false;
    }

    /**
     * 是否顯示QA連結 編輯
     */
    public boolean renderedQALink(WorkTestInfo testInfo, User loginUser) {

        boolean isQaUser = workBackendParamHelper.isQAUser(loginUser);
        boolean isSongce = WorkTestStatus.SONGCE.equals(testInfo.getTestinfoStatus());
        boolean isTesting = WorkTestStatus.TESTING.equals(testInfo.getTestinfoStatus());
        boolean isRetest = WorkTestStatus.RETEST.equals(testInfo.getTestinfoStatus());
        boolean isQaTestComplete = WorkTestStatus.QA_TEST_COMPLETE.equals(testInfo.getTestinfoStatus());
        boolean isApproved = WorkTestInfoStatus.APPROVED.equals(testInfo.getQaAuditStatus());
        boolean isJoinSchedule = WorkTestInfoStatus.JOIN_SCHEDULE.equals(testInfo.getQaScheduleStatus());

        if ((isSongce && isApproved && isJoinSchedule)
                || (isTesting && isApproved && isJoinSchedule)
                || (isRetest && isApproved && isJoinSchedule)
                || (isQaTestComplete && isApproved && isJoinSchedule)) {
            return isQaUser && sendTestService.isContainsQADepts(testInfo);
        }

        return false;

    }

    /**
     * 預計完成日 可修改時機
     */
    public boolean modifyStatusByEstablishDate(WorkTestInfo testInfo, User loginUser) {

        // 可執行的 status
        List<WorkTestStatus> canProcessStatus = Lists.newArrayList(
                WorkTestStatus.SONGCE,
                WorkTestStatus.TESTING,
                WorkTestStatus.RETEST,
                WorkTestStatus.ROLL_BACK_TEST,
                WorkTestStatus.SIGN_PROCESS);

        // 非以上狀態直接排除
        if (!canProcessStatus.contains(testInfo.getTestinfoStatus())) {
            return false;
        }

        // 為 QA user
        if (workBackendParamHelper.isQAUser(loginUser)) {
            return true;
        }

        // 檢查登入者是否為填單者及其上層主管
        if (isCreatedAndAboveManager(testInfo.getCreatedUser().getSid(), loginUser)) {
            return true;
        }

        return false;

    }

    /**
     * 送測日 可修改時機
     */
    public boolean modifyStatusByTestDate(WorkTestInfo testInfo, User loginUser) {
        // 送簽核中
        boolean isSignProcess = WorkTestStatus.SIGN_PROCESS.equals(testInfo.getTestinfoStatus());
        // 送測
        boolean isSongce = WorkTestStatus.SONGCE.equals(testInfo.getTestinfoStatus());
        // QA狀態:待審核
        boolean isWaitApprove = WorkTestInfoStatus.WAIT_APPROVE.equals(testInfo.getQaAuditStatus());

        boolean isApproved = WorkTestInfoStatus.APPROVED.equals(testInfo.getQaAuditStatus());
        boolean isJoinSchedule = WorkTestInfoStatus.JOIN_SCHEDULE.equals(testInfo.getQaScheduleStatus());

        boolean isQaUser = workBackendParamHelper.isQAUser(loginUser);

        if (isSongce && isWaitApprove && isQaUser) {
            return true;
        } else if ((isSignProcess || isSongce) && isWaitApprove) {
            return isCreatedAndAboveManager(testInfo.getCreatedUser().getSid(), loginUser);
        }

        if ((isSongce && isWaitApprove) || (isSongce && isApproved && isJoinSchedule)) {
            return isQaUser;
        }

        return false;

    }

    /**
     * 預計上線日 可修改時機
     */
    public boolean modifyStatusByOnlineDate(WorkTestInfo testInfo, User loginUser) {
        WorkTestStatus workTestStatus = testInfo.getTestinfoStatus();
        WorkTestInfoStatus qaAuditStatus = testInfo.getQaAuditStatus();
        WorkTestInfoStatus qaScheduleStatus = testInfo.getQaScheduleStatus();
        WorkTestInfoStatus commitStatus = testInfo.getCommitStatus();

        return modifyStatusByOnlineDate(workTestStatus, qaAuditStatus, qaScheduleStatus, commitStatus, testInfo.getCreateDep(),
                testInfo.getCreatedUser(), loginUser);
    }

    public boolean modifyStatusByOnlineDate(WorkTestStatus workTestStatus, WorkTestInfoStatus qaAuditStatus,
            WorkTestInfoStatus qaScheduleStatus, WorkTestInfoStatus commitStatus, Org wtCreateOrg, User wtCreateUser, User loginUser) {
        boolean isSignProcess = WorkTestStatus.SIGN_PROCESS.equals(workTestStatus);
        boolean isSongce = WorkTestStatus.SONGCE.equals(workTestStatus);
        boolean isTesting = WorkTestStatus.TESTING.equals(workTestStatus);
        boolean isRetest = WorkTestStatus.RETEST.equals(workTestStatus);
        boolean isQaTestComplete = WorkTestStatus.QA_TEST_COMPLETE.equals(workTestStatus);
        boolean isWaitApprove = WorkTestInfoStatus.WAIT_APPROVE.equals(qaAuditStatus);
        boolean isApproved = WorkTestInfoStatus.APPROVED.equals(qaAuditStatus);
        boolean isJoinSchedule = WorkTestInfoStatus.JOIN_SCHEDULE.equals(qaScheduleStatus);
        boolean isCommit = WorkTestInfoStatus.COMMITED.equals(commitStatus);
        boolean isQaUser = workBackendParamHelper.isQAUser(loginUser);

        if (isSignProcess) {
            return isSameBasicOrg(wtCreateOrg, wtCreateUser, loginUser.getPrimaryOrg().toOrg());
        } else if ((isSongce && isWaitApprove) || (isSongce && isApproved && isJoinSchedule)
                || (isTesting && isApproved && isJoinSchedule && isCommit)
                || (isRetest && isApproved && isJoinSchedule && isCommit)
                || (isQaTestComplete && isApproved && isJoinSchedule && isCommit)) {
            return isSameBasicOrg(wtCreateOrg, wtCreateUser, loginUser.getPrimaryOrg().toOrg()) || isQaUser;
        }

        return false;
    }

    /**
     * 排定完成日 可修改時機
     */
    public boolean modifyStatusByFinishDate(WorkTestInfo testInfo, User loginUser) {
        boolean isApproved = WorkTestInfoStatus.APPROVED.equals(testInfo.getQaAuditStatus());
        boolean isWaitApprove = WorkTestInfoStatus.WAIT_APPROVE.equals(testInfo.getQaAuditStatus());
        boolean isJoinSchedule = WorkTestInfoStatus.JOIN_SCHEDULE.equals(testInfo.getQaScheduleStatus());
        boolean isContainsQADepts = sendTestService.isContainsQADepts(testInfo);

        if ((isWaitApprove && isContainsQADepts) || (isApproved && isJoinSchedule)) {
            return workBackendParamHelper.isQAUser(loginUser);
        }

        return false;
    }

    /**
     * 是否為相同部門 或上層主管
     * 
     * @param wtCreateOrg
     * @param loginOrg
     * @return
     */
    private boolean isSameBasicOrg(Org wtCreateOrg, User wtCreateUser, Org loginUserOrg) {
        List<User> managers = orgService.findOrgManagers(loginUserOrg);
        List<Org> orgList = orgService.findChildOrgsByBasicOrg(loginUserOrg);
        return orgList.contains(wtCreateOrg) || managers.contains(wtCreateUser);
    }
}
