/**
 * 
 */
package com.cy.tech.request.logic.service.onpg.to;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 */
public class Set10RequireConfirmTo implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -2974550965243342037L;

    @Getter
    @Setter
    private String requireSid;
    @Getter
    @Setter
    private String requireNo;
    @Getter
    @Setter
    private String themeJsonStr;
    @Getter
    @Setter
    private Date establishDate;
    @Getter
    @Setter
    private Integer depSid;
    @Getter
    @Setter
    private Integer ownerSid;
}
