package com.cy.tech.request.logic.service.orgtrns.vo;

import java.io.Serializable;
import java.util.List;

import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;

public class OrgTrnsPageVO  implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4508271967312909772L;

    /**
     * 組織異動生效日
     */
    @Getter
    @Setter
    private Long qryEffectiveDate;
    
    /**
     * 組織異動前後對應設定資料查詢結果
     */
    @Getter
    @Setter
    private List<OrgTrnsWorkVerifyVO> verifyDtVOList;
    
    @Getter
    @Setter
    private OrgTrnsWorkVerifyVO editVerifyDtVO;
    
    @Getter
    @Setter
    private OrgTrnsWorkVerifyVO selVerifyDtVO;
    
    /**
     * 
     */
    public OrgTrnsPageVO() {
        //初始化
        this.qryEffectiveDate = System.currentTimeMillis();
        this.verifyDtVOList = Lists.newArrayList();
    }
}
