package com.cy.tech.request.logic.service;

import java.io.IOException;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepService;
import com.cy.tech.request.logic.vo.ProceeAddAssignDepForInternalTo;
import com.cy.tech.request.vo.converter.AssignSendGroupsToConverter;
import com.cy.tech.request.vo.converter.SetupInfoToConverter;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.ReqConfirmDepProcType;
import com.cy.tech.request.vo.enums.ReqConfirmDepProgStatus;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.require.AssignSendInfoForTrnsVO;
import com.cy.tech.request.vo.require.AssignSendInfoHistory;
import com.cy.tech.request.vo.require.AssignSendInfoNativeTo;
import com.cy.tech.request.vo.require.AssignSendSearchInfoVO;
import com.cy.tech.request.vo.require.RequireConfirmDep;
import com.cy.tech.request.vo.require.RequireConfirmDepHistory;
import com.cy.tech.request.vo.value.to.AssignSendGroupsTo;
import com.cy.tech.request.vo.value.to.SetupInfoTo;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.value.to.ItemsCollectionDiffTo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * V7.0.0 轉檔
 * 
 * @author allen1214_wu
 */
@Slf4j
@Service
public class Set10V70TrnsService implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 7580477843510246481L;
    @Autowired
	private transient AssignSendInfoService assignSendInfoService;
	@Autowired
	private transient RequireConfirmDepService requireConfirmDepService;
	@Autowired
	private transient Set10V70TrneBatchHelper set10V70TrneBatchHelper;
	@Autowired
	private transient NativeSqlRepository nativeSqlRepository;
	@Autowired
	private transient EntityManager entityManager;

	/**
	 * 過濾主單需為進行中才處理
	 * 
	 * @param minNo
	 * @param maxNo
	 * @return
	 * @throws UserMessageException
	 */
	public List<AssignSendInfoForTrnsVO> queryForV70Trns(List<String> requireNos, String minNo, String maxNo) throws UserMessageException {

		// ====================================
		// 輸入檢查
		// ====================================
		if (WkStringUtils.isEmpty(requireNos)
		        && WkStringUtils.isEmpty(minNo)
		        && WkStringUtils.isEmpty(maxNo)) {
			throw new UserMessageException("未輸入轉置條件", InfomationLevel.WARN);
		}

		Long startTime = System.currentTimeMillis();
		String procTitle = "query tr_assign_send_info";
		log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

		// SQL
		Map<String, Object> parameters = Maps.newHashMap();
		StringBuffer varname1 = new StringBuffer();
		varname1.append("SELECT asinfo.info_sid            as sid, ");
		varname1.append("       tr.require_sid             as requireSid, ");
		varname1.append("       tr.require_no              as requireNo, ");
		varname1.append("       asinfo.type                as assignSendtype, ");
		varname1.append("       asinfo.info                as depSids_src, ");
		varname1.append("       asinfo.create_usr          as createdUser, ");
		varname1.append("       asinfo.create_dt           as createdDate, ");
		varname1.append("       tr.require_status          as requireStatus ");
		varname1.append("  FROM tr_assign_send_info asinfo ");
		varname1.append("INNER JOIN tr_require tr ");
		varname1.append("        ON tr.require_sid = asinfo.require_sid ");
		varname1.append("       AND tr.require_no = asinfo.require_no "); // 正式區有錯誤資料 .多+一個 join 條件
		varname1.append("       AND tr.v70_trns_flag = 0 ");

		varname1.append(" WHERE 1=1 ");
		if (WkStringUtils.notEmpty(requireNos)) {
			varname1.append("   AND asinfo.require_no IN (:requireNos)");
			parameters.put("requireNos", requireNos);
		}

		if (WkStringUtils.notEmpty(minNo)) {
			varname1.append("   AND asinfo.require_no >= :minNo ");
			parameters.put("minNo", minNo);
		}

		if (WkStringUtils.notEmpty(maxNo)) {
			varname1.append("   AND asinfo.require_no <= :maxNo ");
			parameters.put("maxNo", maxNo);
		}

		varname1.append(" ORDER BY asinfo.require_no ");

		// 查詢
		List<AssignSendInfoForTrnsVO> results = nativeSqlRepository.getResultList(
		        varname1.toString(), parameters, AssignSendInfoForTrnsVO.class);

		if (WkStringUtils.isEmpty(results)) {
			return Lists.newArrayList();
		}

		// 解析 json 字串 (僅取得部門部分, 現行資料其他欄位沒用到)
		for (AssignSendInfoForTrnsVO vo : results) {
			if (WkStringUtils.isEmpty(vo.getDepSids_src())) {
				continue;
			}
			SetupInfoTo setupInfoTo = new SetupInfoToConverter().convertToEntityAttribute(vo.getDepSids_src());
			if (setupInfoTo != null) {
				vo.setDepSids(this.assignSendInfoService.prepareDeps(setupInfoTo));
			}
		}

		log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle + " [" + results.size() + "] 筆"));

		return results;
	}

	/**
	 * 批次處理
	 * 
	 * @param requireNos               本次執行的所有需求單號
	 * @param assignSendInfoMapByReqNo AssignSendInfo 資料 MAP (全)
	 * @param execUserSid              執行者 SID
	 * @throws UserMessageException
	 */
	@Transactional(rollbackFor = Exception.class)
	public void process(
	        List<String> requireNos,
	        Map<String, List<AssignSendInfoForTrnsVO>> assignSendInfoMapByReqNo,
	        List<String> exsitConfirmDepRequireSids,
	        Integer execUserSid) throws UserMessageException {

		if (WkStringUtils.isEmpty(requireNos)) {
			return;
		}

		requireNos = Lists.newArrayList(Sets.newHashSet(requireNos));
		requireNos = requireNos.stream().sorted().collect(Collectors.toList());
		log.debug(requireNos.get(0) + "～" + requireNos.get(requireNos.size() - 1));

		LinkedList<AssignSendInfoHistory> insertAssignSendInfoHistorys = Lists.newLinkedList();
		Set<AssignSendInfoForTrnsVO> deleteAssignSendInfos = Sets.newHashSet();
		List<AssignSendSearchInfoVO> insertAssignSendSearchInfoVOs = Lists.newArrayList();
		List<RequireConfirmDep> insertRequireConfirmDeps = Lists.newArrayList();
		List<RequireConfirmDepHistory> insertRequireConfirmDepHistorys = Lists.newArrayList();

		// 分析用
		Map<String, List<AssignSendInfoForTrnsVO>> lastOneAssignSendInfosMap = Maps.newHashMap();

		Long startTime = System.currentTimeMillis();

		try {
			// ====================================
			// 收集批次更新資料
			// ====================================
			Map<String, Long> costTimeMap = Maps.newLinkedHashMap();
			this.prpareExecuData(
			        requireNos,
			        assignSendInfoMapByReqNo,
			        execUserSid,
			        new Date(),
			        exsitConfirmDepRequireSids,
			        insertAssignSendInfoHistorys,
			        deleteAssignSendInfos,
			        insertAssignSendSearchInfoVOs,
			        insertRequireConfirmDeps,
			        insertRequireConfirmDepHistorys,
			        lastOneAssignSendInfosMap,
			        costTimeMap);

			// 顯示各階段計算時間個別加總
			this.showCostTime(costTimeMap);

			// this.analysis(Sets.newHashSet(requireNos), insertAssignSendSearchInfoVOs,
			// lastOneAssignSendInfosMap);
			// if (true) {
			// return;
			// }

			// ====================================
			// 備份 tr_assign_send_info
			// ====================================
			// 收集要刪除的資料檔 sid
			List<String> assignSendInfoSids = deleteAssignSendInfos.stream()
			        .map(AssignSendInfoForTrnsVO::getSid)
			        .collect(Collectors.toList());

			// 備份
			this.set10V70TrneBatchHelper.backupAssignSendInfo(
			        execUserSid,
			        new Date(),
			        assignSendInfoSids);

			// ====================================
			// 刪除 tr_assign_send_info
			// ====================================
			this.set10V70TrneBatchHelper.batchDeleteByColumn(
			        "tr_assign_send_info",
			        "info_sid",
			        Sets.newHashSet(assignSendInfoSids));

			// ====================================
			// 批次新增 tr_assign_send_info_history
			// ====================================
			this.set10V70TrneBatchHelper.batchInsertAssignSendInfoHistorys(
			        insertAssignSendInfoHistorys);

			// ====================================
			// 刪除 tr_assign_send_search_info (以需求單號)
			// ====================================
			this.set10V70TrneBatchHelper.batchDeleteByColumn(
			        "tr_assign_send_search_info",
			        "require_no",
			        Sets.newHashSet(requireNos));

			// ====================================
			// 批次新增 tr_assign_send_search_info
			// ====================================
			this.set10V70TrneBatchHelper.batchInsertAssignSendSearchInfos(
			        insertAssignSendSearchInfoVOs);

			// ====================================
			// 批次新增 tr_require_confirm_dep
			// ====================================
			this.set10V70TrneBatchHelper.batchInsertRequireConfirmDeps(
			        insertRequireConfirmDeps);

			// ====================================
			// 批次新增 tr_require_confirm_dep_history
			// ====================================
			this.set10V70TrneBatchHelper.batchInsertRequireConfirmDepHistory(
			        insertRequireConfirmDepHistorys, true);

			// ====================================
			// update tr_require.v70_trns_flag 為 true
			// ====================================
			this.set10V70TrneBatchHelper.updateRequire(
			        Sets.newHashSet(requireNos), true);

			// ====================================
			// 清掉 hibernate 暫存
			// ====================================
			this.entityManager.flush();
		} finally {
			insertAssignSendInfoHistorys.clear();
			insertAssignSendInfoHistorys = null;
			deleteAssignSendInfos.clear();
			deleteAssignSendInfos = null;
			insertAssignSendSearchInfoVOs.clear();
			insertAssignSendSearchInfoVOs = null;
			insertRequireConfirmDeps.clear();
			insertRequireConfirmDeps = null;
			insertRequireConfirmDepHistorys.clear();
			insertRequireConfirmDepHistorys = null;
		}
		log.debug(WkCommonUtils.prepareCostMessage(startTime, "單次" + requireNos.size() + "筆需求單處理時間"));
	}

	@SuppressWarnings("unused")
	private void analysis(
	        Set<String> requireNos,
	        List<AssignSendSearchInfoVO> insertAssignSendSearchInfoVOs,
	        Map<String, List<AssignSendInfoForTrnsVO>> lastOneAssignSendInfosMap) {

		// ====================================
		// 查詢將被刪除的資料
		// ====================================
		List<AssignSendSearchInfoVO> deleteInfos = this.set10V70TrneBatchHelper.querySearchInfoDepSidByRequireNo(Lists.newArrayList(requireNos));

		// ====================================
		// 整理清單
		// ====================================
		Map<String, Set<Integer>> assignMap = Maps.newHashMap();
		Map<String, Set<Integer>> sendMap = Maps.newHashMap();

		for (AssignSendSearchInfoVO deleteInfo : deleteInfos) {
			String currRequireNo = deleteInfo.getRequireNo();
			Integer depSid = deleteInfo.getDepSid();

			if ("0".equals(deleteInfo.getTypeInt() + "")) {
				Set<Integer> assignSet = analysis_getSet(assignMap, currRequireNo);
				if (assignSet.contains(depSid)) {
					// log.warn("ASSIGN-[" + currRequireNo + "]-[" + depSid + " ]重複");
				} else {
					assignSet.add(depSid);
				}

			} else {
				Set<Integer> sendSet = analysis_getSet(sendMap, currRequireNo);
				if (sendSet.contains(depSid)) {
					// log.warn("SEND-[" + currRequireNo + "]-[" + depSid + " ]重複");
				} else {
					sendSet.add(depSid);
				}

			}
		}

		// ====================================
		// 比對要 insert 的資料
		// ====================================
		Map<String, Set<Integer>> addAssignMap = Maps.newHashMap();
		Map<String, Set<Integer>> addSendMap = Maps.newHashMap();
		for (AssignSendSearchInfoVO insertInfo : insertAssignSendSearchInfoVOs) {
			String currRequireNo = insertInfo.getRequireNo();
			Integer depSid = insertInfo.getDepSid();

			if (AssignSendType.ASSIGN.equals(insertInfo.getType())) {
				Set<Integer> assignSet = analysis_getSet(assignMap, currRequireNo);
				if (assignSet.contains(depSid)) {
					assignSet.remove(depSid);
				} else {
					analysis_getSet(addAssignMap, currRequireNo).add(depSid);
				}

			} else {
				Set<Integer> sendSet = analysis_getSet(sendMap, currRequireNo);
				if (sendSet.contains(depSid)) {
					sendSet.remove(depSid);
				} else {
					analysis_getSet(addSendMap, currRequireNo).add(depSid);
				}
			}
		}

		// ====================================
		// 印出結果
		// ====================================
		List<String> msgs = Lists.newArrayList();
		if (assignMap.size() > 0) {
			for (String reqNo : assignMap.keySet()) {
				Set<Integer> set = assignMap.get(reqNo);
				for (Integer depSid : set) {
					Org dep = WkOrgCache.getInstance().findBySid(depSid);
					if (dep != null && Activation.ACTIVE.equals(dep.getStatus())) {
						msgs.add(reqNo + ":" + dep.getSid() + " " + WkOrgUtils.getOrgName(dep));
					}
				}
			}
		}
		if (msgs.size() > 0) {
			log.debug("\r\nASSIGN 消失的單位:\r\n" + String.join("、", msgs));
		}

	}

	@SuppressWarnings("unused")
	private void analysis_compare(String requireNo, int type, Set<Integer> depSet, Map<String, List<AssignSendInfoForTrnsVO>> lastOneAssignSendInfosMap) {
		AssignSendInfoForTrnsVO currInfo = null;
		List<AssignSendInfoForTrnsVO> currInfos = lastOneAssignSendInfosMap.get(requireNo);
		if (currInfos != null) {
			for (AssignSendInfoForTrnsVO assignSendInfoForTrnsVO : currInfos) {
				if ((type + "").equals(assignSendInfoForTrnsVO.getAssignSendtype() + "")) {
					currInfo = assignSendInfoForTrnsVO;
				}
			}
		}
		if (currInfo == null) {

		}
	}

	private Set<Integer> analysis_getSet(Map<String, Set<Integer>> map, String key) {
		Set<Integer> set = map.get(key);
		if (set == null) {
			set = Sets.newHashSet();
			map.put(key, set);
		}
		return set;
	}

	/**
	 * 準備所有批次更新的資料
	 * 
	 * @param requireNos
	 * @param assignSendInfoMapByReqNo
	 * @param userSid
	 * @param sysDate
	 * @param insertAssignSendInfoHistorys
	 * @param inserTrnsBackups
	 * @param deleteAssignSendInfo
	 * @param insertAssignSendSearchInfoVOs
	 * @param insertRequireConfirmDeps
	 * @param insertRequireConfirmDepHistorys
	 * @return
	 * @throws IOException
	 */
	private void prpareExecuData(
	        List<String> requireNos,
	        Map<String, List<AssignSendInfoForTrnsVO>> assignSendInfoMapByReqNo,
	        Integer userSid,
	        Date sysDate,
	        List<String> exsitConfirmDepRequireSids,
	        LinkedList<AssignSendInfoHistory> insertAssignSendInfoHistorys,
	        Set<AssignSendInfoForTrnsVO> deleteAssignSendInfo,
	        List<AssignSendSearchInfoVO> insertAssignSendSearchInfoVOs,
	        List<RequireConfirmDep> insertRequireConfirmDeps,
	        List<RequireConfirmDepHistory> insertRequireConfirmDepHistorys,
	        Map<String, List<AssignSendInfoForTrnsVO>> lastOneAssignSendInfosMap,
	        Map<String, Long> costTimeMap) {

		// 避免傳入單號重複
		requireNos = requireNos.stream().distinct().collect(Collectors.toList());

		Long startTime = System.currentTimeMillis();
		String procTitle = "計算異動資料 [" + requireNos.size() + "]筆";
		// log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

		for (String requireNo : requireNos) {
			// 依據單號取得相關的 info 檔
			List<AssignSendInfoForTrnsVO> assignSendInfosByReqNo = assignSendInfoMapByReqNo.get(requireNo);
			if (WkStringUtils.isEmpty(assignSendInfosByReqNo)) {
				continue;
			}
			// 取得 SID
			String requireSid = assignSendInfosByReqNo.get(0).getRequireSid();

			// ====================================
			// 執行 tr_assign_send_info 轉檔 , 除最新日期之外的舊資料(assign + send)
			// 1. 轉 tr_assign_send_info_history
			// 2. delete
			// ====================================
			// Long startTimea = System.currentTimeMillis();
			List<AssignSendInfoForTrnsVO> lastOneAssignSendInfos = this.trnsAssignSendInfo(
			        requireSid,
			        requireNo,
			        assignSendInfosByReqNo,
			        userSid,
			        sysDate,
			        insertAssignSendInfoHistorys,
			        deleteAssignSendInfo);

			lastOneAssignSendInfosMap.put(requireNo, lastOneAssignSendInfos);

			// this.countCostTime(costTimeMap, "trnsAssignSendInfo", startTimea);

			// ====================================
			// 重建 [tr_assign_send_search_info]
			// ====================================
			// startTimea = System.currentTimeMillis();
			this.prepateSearchInfo(
			        requireSid,
			        requireNo,
			        lastOneAssignSendInfos,
			        insertAssignSendSearchInfoVOs);

			// this.countCostTime(costTimeMap, "prepateSearchInfo", startTimea);
			// ====================================
			// 建立需求確認單位資料 [tr_require_confirm_dep] + [tr_require_confirm_dep_history]
			// ====================================
			// startTimea = System.currentTimeMillis();
			this.rebuidReqConfirmDeps(
			        requireSid,
			        requireNo,
			        lastOneAssignSendInfos,
			        insertRequireConfirmDeps,
			        insertRequireConfirmDepHistorys,
			        exsitConfirmDepRequireSids,
			        costTimeMap);

			// this.countCostTime(costTimeMap, "rebuidReqConfirmDeps", startTimea);

		}
		log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
	}

	/**
	 * @param costTimeMap
	 * @param name
	 * @param startTime
	 */
	@SuppressWarnings("unused")
	private void countCostTime(Map<String, Long> costTimeMap, String name, Long startTime) {

		Long costTime = costTimeMap.get(name);
		if (costTime == null) {
			costTime = Long.valueOf(0);
		}
		costTimeMap.put(name, costTime + (System.currentTimeMillis() - startTime));
	}

	/**
	 * @param costTimeMap
	 */
	private void showCostTime(Map<String, Long> costTimeMap) {
		for (String name : costTimeMap.keySet()) {
			double costTime = Double.valueOf(costTimeMap.get(name));
			costTime = costTime / 1000;
			log.debug("【" + name + "】:" + costTime + "秒");
		}
	}

	/**
	 * 建立需求確認單位資料 [tr_require_confirm_dep] + [tr_require_confirm_dep_history]
	 * 
	 * @param require
	 * @param lastOneAssignSendInfos
	 */
	private void rebuidReqConfirmDeps(
	        String requireSid,
	        String requireNo,
	        List<AssignSendInfoForTrnsVO> lastOneAssignSendInfos,
	        List<RequireConfirmDep> insertRequireConfirmDeps,
	        List<RequireConfirmDepHistory> insertRequireConfirmDepHistorys,
	        List<String> exsitConfirmDepRequireSids,
	        Map<String, Long> costTimeMap) {

		// Long startTime = null;

		// ====================================
		// 檢查
		// ====================================
		// 資料已存在, 不處理
		if (exsitConfirmDepRequireSids.contains(lastOneAssignSendInfos.get(0).getRequireSid())) {
			log.warn("單號[" + lastOneAssignSendInfos.get(0).getRequireNo() + "] 已存在需求完成確認單位檔, 不處理!");
			return;
		}

		// ====================================
		// 判斷流程狀態
		// ====================================
		String requireStatusStr = lastOneAssignSendInfos.get(0).getRequireStatus();
		RequireStatusType requireStatus = RequireStatusType.valueOf(requireStatusStr);

		// 還未進入確認流程的單據, 無須產生確認檔. 不處理
		List<RequireStatusType> passList = Lists.newArrayList(
		        RequireStatusType.DRAFT,
		        RequireStatusType.NEW_INSTANCE,
		        RequireStatusType.WAIT_CHECK,
		        RequireStatusType.ROLL_BACK_NOTIFY);

		if (passList.contains(requireStatus)) {
			return;
		}

		// 除了進行中的單據, 其他寫入【無須確認】
		ReqConfirmDepProgStatus reqConfirmDepProgStatus = ReqConfirmDepProgStatus.PASS;
		if (RequireStatusType.PROCESS.equals(requireStatus)) {
			reqConfirmDepProgStatus = ReqConfirmDepProgStatus.WAIT_CONFIRM;
		}

		// ====================================
		// 取得分派單位
		// ====================================
		// 取得分派單位
		List<Integer> assignDepSids = this.getDeps(lastOneAssignSendInfos, AssignSendType.ASSIGN);
		if (assignDepSids == null) {
			assignDepSids = Lists.newArrayList();
		}
		// 過濾已停用
		assignDepSids = assignDepSids.stream()
		        .filter(depSid -> WkOrgUtils.isActive(depSid))
		        .collect(Collectors.toList());

		if (WkStringUtils.isEmpty(assignDepSids)) {
			// 為進行中才顯示
			if (RequireStatusType.PROCESS.equals(requireStatus)) {
				log.warn("[" + lastOneAssignSendInfos.get(0).getRequireNo() + "] 不存在『非停用』的分派單位");
			}
			return;
		}

		// ====================================
		// 計算需求確認單位
		// ====================================
		List<Integer> requireConfirmDepSids = this.requireConfirmDepService.prepareRequireConfirmDepSids(assignDepSids).stream()
		        .distinct()
		        .collect(Collectors.toList());

		// ====================================
		// 取得其他欄位資訊
		// ====================================
		// 取得建立者
		Integer execUserSid = this.getCreateUserSid(lastOneAssignSendInfos);
		User createUser = WkUserCache.getInstance().findBySid(execUserSid);
		if (createUser == null) {
			execUserSid = -1;
		}
		// 取得建立者部門
		Integer execUserDepSid = 2;// 預設 root
		if (createUser != null && createUser.getPrimaryOrg() != null && createUser.getPrimaryOrg().getSid() != null) {
			execUserDepSid = createUser.getPrimaryOrg().getSid();
		}

		// 建立日期
		Date createDate = this.getCreateDate(lastOneAssignSendInfos);
		// ====================================
		// 建立需求確認單位資料
		// ====================================
		// startTime = System.currentTimeMillis();
		for (Integer requireConfirmDepSid : requireConfirmDepSids) {
			// ====================================
			// 新增需求完成確認單位檔 Require_Confirm_Dep
			// ====================================
			RequireConfirmDep requireConfirmDep = new RequireConfirmDep();
			requireConfirmDep = new RequireConfirmDep();
			requireConfirmDep.setCreatedUser(execUserSid);
			requireConfirmDep.setCreatedDate(createDate);
			requireConfirmDep.setRequireSid(requireSid);
			requireConfirmDep.setDepSid(requireConfirmDepSid);
			// 初始預設為該部門主管，為避免組織異動，一開始塞 -1 , 代表需動態抓部門主管
			requireConfirmDep.setOwnerSid(-1);
			// 需求完成進度若為進行中, 初始值為『待確認』
			requireConfirmDep.setProgStatus(reqConfirmDepProgStatus);

			if (insertRequireConfirmDeps == null) {
				insertRequireConfirmDeps = Lists.newArrayList();
			}
			insertRequireConfirmDeps.add(requireConfirmDep);

			// ====================================
			// 新增異動歷程檔 Require_Confirm_Dep_History
			// ====================================
			// Long startTimez = System.currentTimeMillis();
			// 歷程檔 - 說明
			String histroryMemo = ""
			        + "加派【"
			        + this.requireConfirmDepService.prepareAssignHistoryMessage(requireConfirmDepSid, assignDepSids, false)
			        + "】, 產生需確認流程，並指定負責人為單位主管";
			// this.countCostTime(costTimeMap, "歷程檔 - 說明", startTimez);

			RequireConfirmDepHistory history = new RequireConfirmDepHistory();
			history.setCreatedUser(execUserSid);
			history.setCreatedDate(createDate);
			history.setCreateDep(execUserDepSid);
			history.setRequireSid(requireSid);
			history.setDepSid(requireConfirmDepSid);
			history.setProgType(ReqConfirmDepProcType.ASSIGN_CREATE);
			history.setMemo(histroryMemo);

			if (insertRequireConfirmDepHistorys == null) {
				insertRequireConfirmDepHistorys = Lists.newArrayList();
			}
			insertRequireConfirmDepHistorys.add(history);
		}
		// this.countCostTime(costTimeMap, "建立需求確認單位資料", startTime);
	}

	/**
	 * 準備 tr_assign_send_search_info
	 * 
	 * @param requireSid                    本次處理的 需求單 sid
	 * @param requireNo                     本次處理的 需求單號
	 * @param lastoneAssignSendInfos        該單據分派/通知設定資料 (原AssignSendInfo會有多筆,
	 *                                      最後一筆才是最終設定)
	 * @param insertAssignSendSearchInfoVOs 收集要批次 insert 的
	 *                                      [tr_assign_send_search_info]
	 * 
	 */
	private void prepateSearchInfo(
	        String requireSid,
	        String requireNo,
	        List<AssignSendInfoForTrnsVO> lastoneAssignSendInfos,
	        List<AssignSendSearchInfoVO> insertAssignSendSearchInfoVOs) {
		// ====================================
		// 建立新單位資料
		// ====================================
		// 取得分派單位
		List<Integer> newAssignDeps = this.getDeps(lastoneAssignSendInfos, AssignSendType.ASSIGN);
		// 取得通知單位
		List<Integer> newSendDeps = this.getDeps(lastoneAssignSendInfos, AssignSendType.SEND);
		// 取得建立者
		Integer createUserSid = this.getCreateUserSid(lastoneAssignSendInfos);
		// 建立日期
		Date createDate = this.getCreateDate(lastoneAssignSendInfos);

		// 分派單位
		Set<Integer> searchAssignDepSids = Sets.newHashSet();
		if (WkStringUtils.notEmpty(newAssignDeps)) {
			// 加入分派單位
			searchAssignDepSids.addAll(newAssignDeps);
			// 加入需求確認單位
			searchAssignDepSids.addAll(requireConfirmDepService.prepareRequireConfirmDepSids(newAssignDeps));
			// 收集insert資料
			this.createDepSearchInfo(
			        requireSid,
			        requireNo,
			        createUserSid,
			        createDate,
			        AssignSendType.ASSIGN,
			        Lists.newArrayList(searchAssignDepSids),
			        insertAssignSendSearchInfoVOs);

		}

		// 通知單位
		if (WkStringUtils.notEmpty(newSendDeps)) {
			// 收集insert資料
			this.createDepSearchInfo(
			        requireSid,
			        requireNo,
			        createUserSid,
			        createDate,
			        AssignSendType.SEND,
			        Lists.newArrayList(newSendDeps),
			        insertAssignSendSearchInfoVOs);
		}

	}

	@Transactional(rollbackFor = Exception.class)
	private List<AssignSendInfoForTrnsVO> trnsAssignSendInfo(
	        String reqSid,
	        String reqNo,
	        List<AssignSendInfoForTrnsVO> assignSendInfos,
	        Integer userSid,
	        Date sysdate,
	        LinkedList<AssignSendInfoHistory> insertHistorys,
	        Set<AssignSendInfoForTrnsVO> deleteAssignSendInfo) {

		if (WkStringUtils.isEmpty(assignSendInfos)) {
			return null;
		}

		List<AssignSendInfoForTrnsVO> lastoneAssignSendInfos = Lists.newArrayList();

		// ====================================
		// 取得建立時間(index), 並以建立時間排序(早->晚)
		// ====================================
		List<Date> createDates = assignSendInfos.stream()
		        .map(AssignSendInfoForTrnsVO::getCreatedDate)
		        .distinct()
		        .sorted((d1, d2) -> d1.compareTo(d2))
		        .collect(Collectors.toList());

		// ====================================
		// 整理分派資料
		// ====================================
		// 取得所有分派檔
		List<AssignSendInfoForTrnsVO> assignInfos = assignSendInfos.stream()
		        .filter(vo -> vo.getAssignSendtype() == 0)
		        .sorted(Comparator.comparing(AssignSendInfoForTrnsVO::getCreatedDate))
		        .collect(Collectors.toList());

		// 取得最後一筆
		AssignSendInfoForTrnsVO lastAssignInfo = null;
		if (WkStringUtils.notEmpty(assignInfos)) {
			lastAssignInfo = assignInfos.get(assignInfos.size() - 1);
			lastoneAssignSendInfos.add(lastAssignInfo);
		}

		// 以日期進行索引
		Map<Date, AssignSendInfoForTrnsVO> assignInfosMapByCreateDate = assignInfos.stream()
		        .collect(Collectors.toMap(AssignSendInfoForTrnsVO::getCreatedDate, vo -> vo));

		// ====================================
		// 整理通知資料
		// ====================================
		// 取得所有分派檔
		List<AssignSendInfoForTrnsVO> noticeInfos = assignSendInfos.stream()
		        .filter(vo -> vo.getAssignSendtype() == 1)
		        .sorted(Comparator.comparing(AssignSendInfoForTrnsVO::getCreatedDate))
		        .collect(Collectors.toList());

		// 取得最後一筆
		AssignSendInfoForTrnsVO lastNoticeInfo = null;
		if (WkStringUtils.notEmpty(noticeInfos)) {
			lastNoticeInfo = noticeInfos.get(noticeInfos.size() - 1);
			lastoneAssignSendInfos.add(lastNoticeInfo);
		}

		// 以日期進行索引
		Map<Date, AssignSendInfoForTrnsVO> noticeInfosMapByCreateDate = noticeInfos.stream()
		        .collect(Collectors.toMap(AssignSendInfoForTrnsVO::getCreatedDate, vo -> vo));

		// ====================================
		// 以時間為單位, 一個操作日期產生一筆 history
		// ====================================
		List<AssignSendInfoForTrnsVO> waitDeleteInfos = Lists.newArrayList();

		for (int i = 0; i < createDates.size(); i++) {

			// ====================================
			// 本次通知/分派資料
			// ====================================
			// 本次執行日期
			Date currCreateDate = createDates.get(i);
			// 取得本次執行資料 (日期一樣的)
			Map<AssignSendType, List<Integer>> depInfos = this.getAssignNoticeDeps(currCreateDate, assignInfosMapByCreateDate, noticeInfosMapByCreateDate);
			// 本次分派單位
			List<Integer> currAssignDepSids = depInfos.get(AssignSendType.ASSIGN);
			// 取得本次通知單位
			List<Integer> currNoticeDepSids = depInfos.get(AssignSendType.SEND);

			// 取得執行者
			Integer createUserSid = this.getCreateUser(
			        currCreateDate, assignInfosMapByCreateDate, noticeInfosMapByCreateDate);

			// ====================================
			// 上次通知/分派資料
			// ====================================
			// 上一次設定的分派單位
			List<Integer> oldAssignDeps = null;
			// 上一次設定的通知單位
			List<Integer> oldSendDeps = null;

			// 第二次設定後才有前一筆資料
			if (i > 0) {
				Date preCreateDate = createDates.get(i - 1);
				Map<AssignSendType, List<Integer>> preDepInfos = this.getAssignNoticeDeps(
				        preCreateDate,
				        assignInfosMapByCreateDate,
				        noticeInfosMapByCreateDate);
				// 前次分派單位
				oldAssignDeps = preDepInfos.get(AssignSendType.ASSIGN);
				// 前次通知單位
				oldSendDeps = preDepInfos.get(AssignSendType.SEND);
			}

			// ====================================
			// 比對設定單位差異
			// ====================================
			// 分派單位
			ItemsCollectionDiffTo<Integer> assignDepDiffTo = this.assignSendInfoService.prepareModifyDepsInfo(
			        oldAssignDeps,
			        currAssignDepSids);

			// 通知單位
			ItemsCollectionDiffTo<Integer> noticeDepDiffTo = this.assignSendInfoService.prepareModifyDepsInfo(
			        oldSendDeps,
			        currNoticeDepSids);

			// ====================================
			// 記錄要建立的 tr_assign_send_info_history
			// ====================================
			AssignSendInfoHistory assignSendInfoHistory = new AssignSendInfoHistory();
			assignSendInfoHistory.setRequireSid(reqSid);
			assignSendInfoHistory.setAddAssignDeps(WkJsonUtils.getInstance().toJsonWithOutPettyJson(assignDepDiffTo.getPlusItems()));
			assignSendInfoHistory.setDeleteAssignDeps(WkJsonUtils.getInstance().toJsonWithOutPettyJson(assignDepDiffTo.getReduceItems()));
			assignSendInfoHistory.setAddSendDeps(WkJsonUtils.getInstance().toJsonWithOutPettyJson(noticeDepDiffTo.getPlusItems()));
			assignSendInfoHistory.setDeleteSendDeps(WkJsonUtils.getInstance().toJsonWithOutPettyJson(noticeDepDiffTo.getReduceItems()));
			assignSendInfoHistory.setCreatedUser(createUserSid);
			assignSendInfoHistory.setCreatedDate(currCreateDate);

			if (insertHistorys == null) {
				insertHistorys = Lists.newLinkedList();
			}
			// 加入要新增的 tr_assign_send_info_history 列表
			insertHistorys.add(assignSendInfoHistory);

			// ====================================
			// 如果不是最後一次的編輯, 加入刪除資料檔
			// ====================================
			if (lastAssignInfo != null && assignInfosMapByCreateDate.containsKey(currCreateDate)) {
				AssignSendInfoForTrnsVO currAssignInfo = assignInfosMapByCreateDate.get(currCreateDate);
				if (!currAssignInfo.equals(lastAssignInfo)) {
					waitDeleteInfos.add(currAssignInfo);
				}
			}

			if (lastNoticeInfo != null && noticeInfosMapByCreateDate.containsKey(currCreateDate)) {
				AssignSendInfoForTrnsVO currNoticeInfo = noticeInfosMapByCreateDate.get(currCreateDate);
				if (!currNoticeInfo.equals(lastNoticeInfo)) {
					waitDeleteInfos.add(currNoticeInfo);
				}
			}
		}

		// ====================================
		// 加入要刪除的 tr_assign_send_info 列表
		// ====================================
		if (deleteAssignSendInfo == null) {
			deleteAssignSendInfo = Sets.newHashSet();
		}
		if (WkStringUtils.notEmpty(waitDeleteInfos)) {
			deleteAssignSendInfo.addAll(waitDeleteInfos);
		}

		return lastoneAssignSendInfos;

	}

	/**
	 * @param currCreateDate
	 * @param assignInfosMapByCreateDate
	 * @param noticeInfosMapByCreateDate
	 * @return
	 */
	private Map<AssignSendType, List<Integer>> getAssignNoticeDeps(
	        Date currCreateDate,
	        Map<Date, AssignSendInfoForTrnsVO> assignInfosMapByCreateDate,
	        Map<Date, AssignSendInfoForTrnsVO> noticeInfosMapByCreateDate) {

		// 取得本次分派單位
		AssignSendInfoForTrnsVO currAssignInfo = assignInfosMapByCreateDate.get(currCreateDate);
		List<Integer> currAssignDepSids = null;
		if (currAssignInfo != null) {
			currAssignDepSids = currAssignInfo.getDepSids();
		}

		// 取得本次通知單位
		AssignSendInfoForTrnsVO currNoticeInfo = noticeInfosMapByCreateDate.get(currCreateDate);
		List<Integer> currNoticeDepSids = null;
		if (currNoticeInfo != null) {
			currNoticeDepSids = currNoticeInfo.getDepSids();
		}

		Map<AssignSendType, List<Integer>> result = Maps.newHashMap();
		result.put(AssignSendType.ASSIGN, currAssignDepSids);
		result.put(AssignSendType.SEND, currNoticeDepSids);
		return result;
	}

	/**
	 * @param currCreateDate
	 * @param assignInfosMapByCreateDate
	 * @param noticeInfosMapByCreateDate
	 * @return
	 */
	private Integer getCreateUser(
	        Date currCreateDate,
	        Map<Date, AssignSendInfoForTrnsVO> assignInfosMapByCreateDate,
	        Map<Date, AssignSendInfoForTrnsVO> noticeInfosMapByCreateDate) {

		AssignSendInfoForTrnsVO info = assignInfosMapByCreateDate.get(currCreateDate);
		if (info != null && info.getCreatedUser() != null) {
			return info.getCreatedUser();
		}
		info = noticeInfosMapByCreateDate.get(currCreateDate);
		if (info != null && info.getCreatedUser() != null) {
			return info.getCreatedUser();
		}

		log.warn("找不到 create user");
		return -1;
	}

	/**
	 * @param assignSendInfos
	 * @param type
	 * @return
	 */
	private List<Integer> getDeps(List<AssignSendInfoForTrnsVO> assignSendInfos, AssignSendType type) {

		if (WkStringUtils.isEmpty(assignSendInfos)) {
			return Lists.newArrayList();
		}

		for (AssignSendInfoForTrnsVO assignSendInfo : assignSendInfos) {
			if (WkCommonUtils.compareByStr(type.ordinal(), assignSendInfo.getAssignSendtype())) {
				return assignSendInfo.getDepSids();
			}
		}

		return Lists.newArrayList();
	}

	/**
	 * @param assignSendInfos
	 * @return
	 */
	private Integer getCreateUserSid(List<AssignSendInfoForTrnsVO> assignSendInfos) {
		if (WkStringUtils.isEmpty(assignSendInfos)) {
			return 1;
		}

		for (AssignSendInfoForTrnsVO assignSendInfo : assignSendInfos) {
			if (assignSendInfo.getCreatedUser() != null) {
				return assignSendInfo.getCreatedUser();
			}
		}
		return 1;
	}

	/**
	 * @param assignSendInfos
	 * @return
	 */
	private Date getCreateDate(List<AssignSendInfoForTrnsVO> assignSendInfos) {
		if (WkStringUtils.isEmpty(assignSendInfos)) {
			return new Date();
		}

		for (AssignSendInfoForTrnsVO assignSendInfo : assignSendInfos) {
			if (assignSendInfo.getCreatedDate() != null) {
				return assignSendInfo.getCreatedDate();
			}
		}
		return new Date();
	}

	/**
	 * 建立部門查詢資訊 (包含需求完成確認單位)
	 * 
	 * @param require
	 * @param executor
	 * @param createDt
	 * @param type
	 * @param depSids
	 * @throws IOException
	 */
	private void createDepSearchInfo(
	        String requireSid,
	        String requireNo,
	        Integer executorSid,
	        Date createDt,
	        AssignSendType type,
	        List<Integer> depSids,
	        List<AssignSendSearchInfoVO> insertAssignSendSearchInfoVOs) {

		if (WkStringUtils.isEmpty(depSids)) {
			return;
		}

		// ====================================
		// 整理新增單位
		// ====================================
		// 去重複
		// 去停用
		List<Integer> depInfo = Sets.newHashSet(depSids).stream()
		        .filter(depSid -> WkOrgUtils.isActive(depSid))
		        .collect(Collectors.toList());

		// ====================================
		// 建立 vo
		// ====================================
		for (Integer depSid : depInfo) {
			AssignSendSearchInfoVO search = new AssignSendSearchInfoVO();
			search.setCreatedDate(createDt);
			search.setCreatedUserSid(executorSid);
			search.setRequireSid(requireSid);
			search.setRequireNo(requireNo);
			search.setType(type);
			search.setTypeInt(AssignSendType.ASSIGN.equals(type) ? 0 : 1);
			search.setDepSid(depSid);
			insertAssignSendSearchInfoVOs.add(search);
		}
	}

	/**
	 * 內部需求, 加派給自己單位
	 */
	@Transactional(rollbackFor = Throwable.class)
	public void proceeAddAssignDepForInternal() {

		// ====================================
		// 查詢需轉檔的資料
		// ====================================
		// 兜組 SQL
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT DISTINCT req.require_sid   as requireSid, ");
		sql.append("                req.require_no    as requireNo, ");
		sql.append("                req.dep_sid       as depSid, ");
		sql.append("                sendInfo.info_sid as infoSid, ");
		sql.append("                sendInfo.info     as info ");
		sql.append("FROM   tr_require req ");
		sql.append("       JOIN tr_category_key_mapping km ");
		sql.append("         ON km.key_sid = req.mapping_sid ");
		sql.append("       JOIN tr_basic_data_big_category cat ");
		sql.append("         ON cat.basic_data_big_category_sid = km.big_category_sid ");
		sql.append("       LEFT JOIN tr_assign_send_info sendInfo ");
		sql.append("              ON sendInfo.require_sid = req.require_sid ");
		sql.append("                 AND type = 0 ");
		sql.append("                 AND sendInfo.create_dt = (SELECT Max(create_dt) ");
		sql.append("                                           FROM   tr_assign_send_info ");
		sql.append("                                           WHERE  require_sid = req.require_sid ");
		sql.append("                                                  AND type = 0) ");
		sql.append("WHERE  cat.req_category = 'INTERNAL' ");
		sql.append("       AND req.require_status IN ( 'PROCESS', 'COMPLETED' ) ");
		sql.append("       AND NOT EXISTS (SELECT sinfo.search_sid ");
		sql.append("                       FROM   tr_assign_send_search_info sinfo ");
		sql.append("                       WHERE  sinfo.require_sid = req.require_sid ");
		sql.append("                              AND sinfo.dep_sid = req.dep_sid ");
		sql.append("                              AND type = 0) ");
		sql.append("ORDER  BY req.require_no DESC");

		// 查詢
		List<ProceeAddAssignDepForInternalTo> proceeAddAssignDepForInternalTos = nativeSqlRepository.getResultList(
		        sql.toString(), Maps.newHashMap(), ProceeAddAssignDepForInternalTo.class);

		// 過濾info中已有資料
		proceeAddAssignDepForInternalTos = proceeAddAssignDepForInternalTos.stream()
		        .filter(to -> WkStringUtils.safeTrim(to.getInfo()).indexOf("\"" + to.getDepSid() + "\"") < 0)
		        .collect(Collectors.toList());

		// ====================================
		// 查詢需轉檔的資料
		// ====================================
		this.set10V70TrneBatchHelper.batchInsertAssignSendInfoForAddByInternal(proceeAddAssignDepForInternalTos);
		this.set10V70TrneBatchHelper.batchUpdateAssignSendInfoForAddByInternal(proceeAddAssignDepForInternalTos);
		this.set10V70TrneBatchHelper.batchInsertAssignSendSearchInfoForAddByInternal(proceeAddAssignDepForInternalTos);
	}

	@Transactional(rollbackFor = Throwable.class)
	public void clearAssignNoticeDeps(boolean isTrueInsert) {

		SetupInfoToConverter converter = new SetupInfoToConverter();
		Date sysDate = new Date();

		// ====================================
		// 查詢分派檔的資料
		// ====================================
		Long startTime = System.currentTimeMillis();
		String procTitle = "查詢分派檔資料";
		log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

		// 查詢
		List<AssignSendInfoForTrnsVO> assignVOs = nativeSqlRepository.getResultList(
		        this.prepareSql("0"), Maps.newHashMap(), AssignSendInfoForTrnsVO.class);

		// 解析單位
		for (AssignSendInfoForTrnsVO vo : assignVOs) {
			vo.setDepSids(
			        this.assignSendInfoService.prepareDeps(
			                converter.convertToEntityAttribute(
			                        vo.getDepSids_src())));
		}

		log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle + "[" + assignVOs.size() + "]"));

		// ====================================
		// 處理要 insert 的分派檔
		// ====================================
		this.processAssign(assignVOs, sysDate, isTrueInsert);

		// ====================================
		// 查詢通知檔的資料
		// ====================================
		startTime = System.currentTimeMillis();
		procTitle = "查詢通知檔資料";
		log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));
		List<AssignSendInfoForTrnsVO> noticeVOs = nativeSqlRepository.getResultList(
		        this.prepareSql("1"), Maps.newHashMap(), AssignSendInfoForTrnsVO.class);

		// 解析單位
		for (AssignSendInfoForTrnsVO vo : noticeVOs) {
			vo.setDepSids(
			        this.assignSendInfoService.prepareDeps(
			                converter.convertToEntityAttribute(
			                        vo.getDepSids_src())));
		}

		// 重撈分派檔
		assignVOs = nativeSqlRepository.getResultList(
		        this.prepareSql("0"), Maps.newHashMap(), AssignSendInfoForTrnsVO.class);
		// 解析單位
		for (AssignSendInfoForTrnsVO vo : assignVOs) {
			vo.setDepSids(
			        this.assignSendInfoService.prepareDeps(
			                converter.convertToEntityAttribute(
			                        vo.getDepSids_src())));
		}

		log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle + "[" + noticeVOs.size() + "]"));

		// ====================================
		// 處理要 insert 的通知檔
		// ====================================
		this.processNotice(
		        noticeVOs,
		        assignVOs,
		        sysDate,
		        isTrueInsert);
	}

	/**
	 * @param assignVOs
	 * @param sysDate
	 */
	private void processAssign(List<AssignSendInfoForTrnsVO> assignVOs, Date sysDate, boolean isTrueInsert) {

		// ====================================
		// 處理要 insert 的分派檔
		// ====================================
		List<AssignSendInfoNativeTo> insertAssignVOs = Lists.newArrayList();

		for (AssignSendInfoForTrnsVO vo : assignVOs) {
			// 取得分派單位
			List<Integer> depSids = vo.getDepSids();
			if (WkStringUtils.isEmpty(depSids)) {
				continue;
			}

			// 收集非停用的部門
			List<Integer> newDepSids = Lists.newArrayList();
			for (Integer depSid : depSids) {
				if (WkOrgUtils.isActive(depSid)) {
					newDepSids.add(depSid);
				}
			}

			if (newDepSids.isEmpty()) {
				log.warn("單號:[" + vo.getRequireNo() + "]分派單位為空");
				continue;
			}

			// 分派部門數量與處理前一致, 代表沒有停用的分派單位, 無需處理
			if (depSids.size() == newDepSids.size()) {
				continue;
			}

			// 加入待處理清單
			insertAssignVOs.add(this.createAssignSendInfoForTrnsVO(vo, AssignSendType.ASSIGN, newDepSids, sysDate));
		}

		// ====================================
		// insert
		// ====================================
		log.info("分派單位更新, 共" + insertAssignVOs.size() + "筆");
		if (isTrueInsert) {
			this.set10V70TrneBatchHelper.batchInsertAssignSendInfo(insertAssignVOs);
		}
	}

	/**
	 * @param noticeVOs
	 * @param assignVOs
	 * @param sysDate
	 * @param isTrueInsert
	 */
	private void processNotice(
	        List<AssignSendInfoForTrnsVO> noticeVOs,
	        List<AssignSendInfoForTrnsVO> assignVOs,
	        Date sysDate,
	        boolean isTrueInsert) {

		// 轉 map
		Map<String, AssignSendInfoForTrnsVO> assignVoMapByReqSid = assignVOs.stream()
		        .collect(Collectors.toMap(AssignSendInfoForTrnsVO::getRequireSid, vo -> vo));

		// ====================================
		// 資料處理
		// ====================================
		List<AssignSendInfoNativeTo> insertNoticeVOs = Lists.newArrayList();

		for (AssignSendInfoForTrnsVO noticeVo : noticeVOs) {

			// 本來就為空, 略過
			if (WkStringUtils.isEmpty(noticeVo.getDepSids())) {
				continue;
			}

			// 取得分派部門
			List<Integer> assignDeps = Lists.newArrayList();
			AssignSendInfoForTrnsVO assignVo = assignVoMapByReqSid.get(noticeVo.getRequireSid());
			if (assignVo != null) {
				assignDeps = assignVo.getDepSids();
			}

			// 收集留存部門
			List<Integer> newDepSids = Lists.newArrayList();
			for (Integer noticeDepSid : noticeVo.getDepSids()) {

				// 過濾停用部門
				if (!WkOrgUtils.isActive(noticeDepSid)) {
					continue;
				}

				// 已經存在於分派, （通知、分派部門不可同時存在)
				if (assignDeps.contains(noticeDepSid)) {
					continue;
				}

				newDepSids.add(noticeDepSid);
			}

			// 分派部門數量與處理前一致, 代表沒有停用的分派單位, 無需處理
			if (noticeVo.getDepSids().size() == newDepSids.size()) {
				continue;
			}

			if (!newDepSids.isEmpty()) {
				log.warn("單號:[" + noticeVo.getRequireNo() + "]通知單位不為空");
			}

			// 加入待處理清單
			insertNoticeVOs.add(this.createAssignSendInfoForTrnsVO(noticeVo, AssignSendType.SEND, newDepSids, sysDate));
		}

		// ====================================
		// insert
		// ====================================
		log.info("通知單位更新, 共" + insertNoticeVOs.size() + "筆");
		if (isTrueInsert) {
			this.set10V70TrneBatchHelper.batchInsertAssignSendInfo(insertNoticeVOs);
		}
	}

	private SetupInfoToConverter setupInfoToConverter = new SetupInfoToConverter();
	private String groupInfoStr = new AssignSendGroupsToConverter().convertToDatabaseColumn(new AssignSendGroupsTo());

	/**
	 * @param vo
	 * @param type
	 * @param newDepSids
	 * @param sysDate
	 * @return
	 */
	public AssignSendInfoNativeTo createAssignSendInfoForTrnsVO(
	        AssignSendInfoForTrnsVO vo,
	        AssignSendType type,
	        List<Integer> newDepSids,
	        Date sysDate) {

		// 建立insert 用的to
		AssignSendInfoNativeTo insertTo = new AssignSendInfoNativeTo();
		insertTo.setSid(UUID.randomUUID().toString());
		insertTo.setRequireSid(vo.getRequireSid());
		insertTo.setRequireNo(vo.getRequireNo());
		insertTo.setType(type);
		insertTo.setGroupInfo(groupInfoStr);
		insertTo.setStatus(Activation.ACTIVE);
		insertTo.setCreatedDate(sysDate);
		insertTo.setCreatedUser(1);

		// 將過濾後的分派單位放入
		SetupInfoTo setupInfoTo = new SetupInfoTo();
		setupInfoTo.setDepartment(newDepSids.stream().map(depsid -> depsid + "").collect(Collectors.toList()));
		insertTo.setInfo(setupInfoToConverter.convertToDatabaseColumn(setupInfoTo));

		return insertTo;
	}

	private String prepareSql(String type) {

		StringBuffer varname1 = new StringBuffer();
		varname1.append("SELECT asInfo.require_sid as requireSid, ");
		varname1.append("asInfo.require_no         as requireNo, ");
		varname1.append("asInfo.info               as depSids_src ");
		varname1.append("FROM   tr_assign_send_info asInfo ");
		varname1.append("       INNER JOIN (SELECT aa.require_sid, ");
		varname1.append("                          Max(aa.create_dt) AS create_dt ");
		varname1.append("                   FROM   tr_assign_send_info aa ");
		varname1.append("                   WHERE  aa.type = " + type + " ");
		varname1.append("                         and  aa.status = 0 ");
		varname1.append("                   GROUP  BY aa.require_sid) AS lastAsInfo ");
		varname1.append("               ON lastAsInfo.require_sid = asInfo.require_sid ");
		varname1.append("                  AND asInfo.create_dt = lastAsInfo.create_dt ");
		varname1.append("                  AND asInfo.type = " + type + " ");
		varname1.append("       INNER JOIN tr_require req ");
		varname1.append("               ON asInfo.require_sid = req.require_sid ");
		varname1.append("                  AND req.require_status NOT IN ( 'CLOSE', 'AUTO_CLOSED', 'INVALID' ) ");
		varname1.append("WHERE  asInfo.type = " + type + " ");
		varname1.append("  AND asInfo.status = 0 ");
		// varname1.append(" AND asInfo.require_no = 'TGTR20180605024' ");
		varname1.append("ORDER BY asInfo.require_no ");

		return varname1.toString();
	}
}
