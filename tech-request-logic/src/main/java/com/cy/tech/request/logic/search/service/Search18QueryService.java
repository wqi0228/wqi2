/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.search.enums.Search18SubType;
import com.cy.tech.request.logic.search.view.Search18View;
import com.cy.tech.request.logic.service.CommonService;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.enums.UrgencyType;
import com.google.common.collect.Lists;

/**
 * 測試提醒清單
 *
 * @author jason_h
 */
@Service
public class Search18QueryService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 9056093022764376471L;
    @Autowired
    private transient WorkBackendParamHelper workBackendParamHelper;
    @Autowired
    private CommonService commonService;
    @Autowired
    private URLService urlService;
    @Autowired
    private SearchService searchHelper;
    @PersistenceContext
    transient private EntityManager em;

    public List<Search18View> findWithQuery(
            Search18SubType subType,
            String ptSql, Map<String, Object> ptParam,
            String stSql, Map<String, Object> stParam,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {

        // ====================================
        // 查詢
        // ====================================

        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();

        @SuppressWarnings("rawtypes")
        List ptResult = Lists.newArrayList();
        if (Search18SubType.PT.equals(subType)
                || Search18SubType.ALL.equals(subType)) {
            Query ptQuery = em.createNativeQuery(ptSql);
            ptParam.entrySet().stream().forEach((entry) -> ptQuery.setParameter(entry.getKey(), entry.getValue()));
            ptResult = ptQuery.getResultList();
        }

        @SuppressWarnings("rawtypes")
        List stResult = Lists.newArrayList();
        if (Search18SubType.ST.equals(subType)
                || Search18SubType.ALL.equals(subType)) {
            Query stQuery = em.createNativeQuery(stSql);
            stParam.entrySet().stream().forEach((entry) -> stQuery.setParameter(entry.getKey(), entry.getValue()));
            stResult = stQuery.getResultList();
        }

        int resultSize = ptResult.size() + stResult.size();
        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd(resultSize);
        if (resultSize == 0) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();
        List<Search18View> viewResult = Lists.newArrayList();
        switch (subType) {
        case PT:
            this.createView(Search18SubType.PT, viewResult, ptResult, execUserSid);
            break;
        case ST:
            this.createView(Search18SubType.ST, viewResult, stResult, execUserSid);
            break;
        default:
            this.createView(Search18SubType.PT, viewResult, ptResult, execUserSid);
            this.createView(Search18SubType.ST, viewResult, stResult, execUserSid);
            break;
        }
        Collections.sort(viewResult, (p1, p2) -> p2.getSubFinishDate().compareTo(p1.getSubFinishDate()));
        // 解析資料-結束
        usageRecord.parserDataEnd();
        return viewResult;
    }

    private void createView(
            Search18SubType subType,
            List<Search18View> viewResult,
            @SuppressWarnings("rawtypes") List result,
            Integer execUserSid) {

        List<String> qaDepSids = workBackendParamHelper.getQADepSids().stream()
                .map(each -> each + "").collect(Collectors.toList());

        for (int i = 0; i < result.size(); i++) {

            Search18View v = new Search18View();

            int idx = 0;
            Object[] record = (Object[]) result.get(i);
            String sid = (String) record[idx++];
            Date requireDate = (Date) record[idx++];
            String bigName = (String) record[idx++];
            String middleName = (String) record[idx++];
            String smallName = (String) record[idx++];
            Integer requireDep = (Integer) record[idx++];
            Integer requireUser = (Integer) record[idx++];
            String requireTheme = (String) record[idx++];
            Integer urgency = (Integer) record[idx++];
            String requireStatus = (String) record[idx++];
            String requireNo = (String) record[idx++];
            Date hopeDate = (Date) record[idx++];

            String subSid = (String) record[idx++];
            String subStatus = (String) record[idx++];
            Date subCreatedDate = (Date) record[idx++];
            Integer subCreateDep = (Integer) record[idx++];
            Integer subCreatedUser = (Integer) record[idx++];
            Date subFinishDate = (Date) record[idx++];

            String subTheme = (String) record[idx++];
            if (subType.equals(Search18SubType.ST)) {
                String testDeps = (String) record[idx++];
                Boolean isQATestComplete = Boolean.valueOf(record[idx++].toString());
                Boolean isTestComplete = Boolean.valueOf(record[idx++].toString());
                if (this.filterTestinfoStatus(
                        WorkTestStatus.valueOf(subStatus),
                        testDeps,
                        requireDep,
                        qaDepSids,
                        isQATestComplete && isTestComplete)) {
                    continue;
                }
            }

            // 處理共通欄位 (一定要擺在『取值』最後面, 否則 index 會不對)
            v.prepareCommonColumn(record, idx);

            v.setSid(sid);
            v.setRequireDate(requireDate);
            v.setBigName(bigName);
            v.setMiddleName(middleName);
            v.setSmallName(smallName);
            v.setRequireDep(requireDep);
            v.setRequireUser(requireUser);
            v.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
            v.setUrgency(UrgencyType.values()[urgency]);
            v.setRequireStatus(RequireStatusType.valueOf(requireStatus));
            v.setRequireNo(requireNo);

            v.setSubConfirmStatus(subType);
            v.setSubSid(subSid);
            v.setSubStatus(this.findSubStatusCommonName(subType, subStatus));
            v.setSubCreatedDate(subCreatedDate);
            v.setSubCreateDep(subCreateDep);
            v.setSubCreatedUser(subCreatedUser);
            v.setSubTheme(subTheme);
            v.setSubFinishDate(subFinishDate);
            v.setHopeDate(hopeDate);
            if (Search18SubType.ST.equals(subType)) {
                v.setLocalUrlLink(this.createLocalUrlLink(URLService.URLServiceAttr.URL_ATTR_TAB_ST, execUserSid, requireNo, subSid));
            } else {
                v.setLocalUrlLink(this.createLocalUrlLink(URLService.URLServiceAttr.URL_ATTR_TAB_PT, execUserSid, requireNo, subSid));
            }
            viewResult.add(v);
        }
    }

    /**
     * 處理送測狀態過濾條件(已先行過濾 審核作廢 | 取消測試) <BR/>
     * 若送測單位沒有 QA 單位 則 送測狀態≠ 審核作廢 | 取消測試 | 測試完成 <BR/>
     * 若送測單位沒有需求單位 則 送測狀態≠ 審核作廢 | 取消測試 | QA測試完成<BR/>
     * 若送測單位僅有需求單位,且需求單位測試完成,此單據不應該顯示在測試提醒清單中<BR/>
     * 若送測單位僅有 QA 單位,且送測狀態為QA測試完成時,此單據不應該顯示在測試提醒清單中<BR/>
     * 若送測單位包含需求單位及QA單位時,且送測狀態為QA測試完成時,此單據不應該顯示在測試提醒清單中
     *
     * @param status     送測狀態
     * @param requireDep 需求單位
     * @param testDeps   送測部門名單
     * @return
     */
    private boolean filterTestinfoStatus(WorkTestStatus status, String testDeps, Integer requireDep, List<String> qaDepSids, Boolean isAllComplete) {
        boolean justContainReqUnit = testDeps.contains("\"" + requireDep + "\"") && !isContainQaDept(testDeps, qaDepSids);
        boolean justContainQaUnit = !testDeps.contains("\"" + requireDep + "\"") && isContainQaDept(testDeps, qaDepSids);
        boolean containAllUnit = testDeps.contains("\"" + requireDep + "\"") && isContainQaDept(testDeps, qaDepSids);
        return (justContainReqUnit && status.equals(WorkTestStatus.TEST_COMPLETE))
                || (justContainQaUnit && status.equals(WorkTestStatus.QA_TEST_COMPLETE))
                || (containAllUnit && isAllComplete);
    }

    private boolean isContainQaDept(String testDeps, List<String> qaDepSids) {
        return qaDepSids.stream().anyMatch(each -> testDeps.contains("\"" + each + "\""));
    }

    private String findSubStatusCommonName(Search18SubType subType, String statusName) {
        if (subType.equals(Search18SubType.ST)) {
            return commonService.get(WorkTestStatus.valueOf(statusName));
        } else {
            return commonService.get(PtStatus.valueOf(statusName));
        }
    }

    private String createLocalUrlLink(URLService.URLServiceAttr tabAttr, Integer execUserSid, String requireNo, String subSid) {
        return urlService.createLocalUrlLinkParamForTab(
                URLService.URLServiceAttr.URL_ATTR_M,
                urlService.createSimpleUrlTo(execUserSid, requireNo, 1),
                tabAttr, subSid);
    }

}
