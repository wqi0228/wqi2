package com.cy.tech.request.logic.search.view;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Convert;

import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.vo.converter.UrgencyTypeConverter;

import lombok.Getter;
import lombok.Setter;

public class OverdueUnclosedVO extends BaseSearchView implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1433067381595168120L;
    @Getter
    @Setter
    private String requireSid;
    @Getter
    @Setter
    private String bigCategoryName;
    @Getter
    @Setter
    private String middleCategoryName;
    @Getter
    @Setter
    private String smallCategoryName;
    @Getter
    @Setter
    private Date createDate;
    @Getter
    @Setter
    private Integer createUserSid;
    @Getter
    @Setter
    private String createUserName;
    @Getter
    @Setter
    private Date updateDate;
    @Getter
    @Setter
    private Integer depSid;

    @Getter
    @Setter
    private Integer overdueDays;
    @Getter
    @Setter
    private Date hopeDate;

    @Getter
    @Setter
    private Integer inteStaffUserSid;
    @Getter
    @Setter
    /** 本地端連結網址 */
    private String localUrlLink;

    /** 緊急度 */
    @Getter
    @Setter
    @Convert(converter = UrgencyTypeConverter.class)
    private UrgencyType urgency = UrgencyType.GENERAL;

    public String getUrgencyDesc() {
        if (urgency != null) {
            return urgency.getValue();
        }
        return "";
    }

    @Getter
    @Setter
    private String inteStaffUserName;
    @Getter
    @Setter
    private String deptName;
}
