/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import com.cy.commons.vo.User;
import com.cy.tech.request.vo.exception.LockRecordException;
import com.cy.tech.request.repository.require.RequireLockRecordRepository;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireLockRecord;
import com.cy.work.common.cache.WkUserCache;
import com.google.common.base.Optional;
import java.io.Serializable;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 鎖定記錄
 *
 * @author jason_h
 */
@Slf4j
@Component
public class LockService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4467974870772477841L;

    @Autowired
    private RequireLockRecordRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public void lock(Require require, User executor) throws LockRecordException {
        Optional<RequireLockRecord> record = checkLock(require, executor);
        if (!record.isPresent()) {
            RequireLockRecord newRecord = new RequireLockRecord();
            newRecord.setSid(require.getSid());
            newRecord.setRequireNo(require.getRequireNo());
            newRecord.setLockedUser(executor);
            DateTime nowPlusTenMin = new DateTime().plusMinutes(10);
            newRecord.setCreatedDate(nowPlusTenMin.toDate());
            newRecord.setHasLocked(Boolean.TRUE);
            repository.save(newRecord);
            log.debug("單據：" + require.getRequireNo() + "，執行鎖定！！");
            return;
        }

        RequireLockRecord oldRecord = record.get();
        //如果鎖定的時間還是大於現在的時間，則不進行鎖定時間的更新
        if (!oldRecord.getCreatedDate().after(new Date())) {
            oldRecord.setLockedUser(executor);
            DateTime nowPlusTenMin = new DateTime().plusMinutes(10);
            oldRecord.setCreatedDate(nowPlusTenMin.toDate());
            oldRecord.setHasLocked(Boolean.FALSE);
            repository.save(oldRecord);
            log.debug("單據：" + require.getRequireNo() + "，執行鎖定！！");
        }
    }

    private Optional<RequireLockRecord> checkLock(Require require, User executor) throws LockRecordException {
        RequireLockRecord record = repository.findOne(require.getSid());
        if (record == null) {
            return Optional.absent();
        }
        Date now = new Date();
        Date lockedDate = record.getCreatedDate();
        record.setLockedUser(WkUserCache.getInstance().findBySid(record.getLockedUser().getSid()));
        if (lockedDate.after(now) && !record.getLockedUser().equals(executor)) {
            throw new LockRecordException(record.getLockedUser().getName() + " 正在編輯此需求單！");
        }
        return Optional.of(record);
    }

    @Transactional(rollbackFor = Exception.class)
    public void unlock(Require require) {
        RequireLockRecord record = repository.findOne(require.getSid());
        if (record == null) {
            return;
        }
        DateTime nowMinusOneMin = new DateTime().minusMinutes(1);
        record.setCreatedDate(nowMinusOneMin.toDate());
        record.setHasLocked(Boolean.FALSE);
        repository.save(record);
        log.debug("單據：" + require.getRequireNo() + "，執行解鎖！！");
    }

    @Transactional(rollbackFor = Exception.class)
    public void deleteLock(Require require) {
        repository.delete(require.getSid());
    }

    @Transactional(readOnly = true)
    public RequireLockRecord findBySid(String requireSid) {
        if (requireSid == null) {
            return null;
        }
        return repository.findOne(requireSid);
    }

    /**
     * 取得鎖定剩餘的時間
     *
     * @param require
     * @return
     */
    @Transactional(readOnly = true)
    public String getLockedTimeString(Require require) {
        RequireLockRecord record = repository.findOne(require.getSid());
        return new DateTime(record.getCreatedDate()).toString("yyyy-MM-dd HH:mm:ss");
    }
}
