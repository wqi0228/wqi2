/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import com.cy.work.common.enums.UrgencyType;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 退件暫存單據查詢頁面顯示vo (search20.xhtml)
 *
 * @author jason_h
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Search20View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5010967016981551344L;
    /** 緊急度 */
    private UrgencyType urgency;
    /** 部 */
    private Boolean hasForwardDep;
    /** 個 */
    private Boolean hasForwardMember;
    /** 關 */
    private Boolean hasLink;
    /** 退件日期 */
    private Date rejectDate;
    /** 退件人員 */
    private Integer rejectUser;
    /** 立單日期 */
    private Date createdDate;
    /** 需求類別 */
    private String bigName;
    /** 中類 */
    private String middleName;
    /** 小類 */
    private String smallName;
    /** 需求單位 */
    private Integer createDep;
    /** 需求人員 */
    private Integer createdUser;
    /** 退件原因 */
    private String rejectReason;
    /** 退件原因說明 */
    private String rejectContent;
    /** 本地端連結網址 */
    private String localUrlLink;
}
