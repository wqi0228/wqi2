/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.helper;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.repository.template.CategoryKeyMappingRepository;
import com.cy.tech.request.repository.template.FieldKeyMappingRepository;
import com.cy.tech.request.vo.constants.CacheConstants;
import com.cy.tech.request.vo.enums.ComType;
import com.cy.tech.request.vo.template.CategoryKeyMapping;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import com.cy.tech.request.vo.value.to.TemplateDefaultValueTo;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 模版欄位查找 | 新增 | 異動
 *
 * @author shaun
 */
@Component
@Slf4j
public class TemplateFieldKeyMappingHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8545393318197955461L;

    @Autowired
    private FieldKeyMappingRepository fkmDao;

    @Autowired
    private CategoryKeyMappingRepository ckmDao;

    @Cacheable(value = CacheConstants.CACHE_TEMP_FIELD, key = "#mapping.id + #mapping.version")
    @Transactional(readOnly = true)
    public List<FieldKeyMapping> findFieldByMapping(CategoryKeyMapping mapping) {
        return fkmDao.findByMappingIdAndVersion(mapping.getId(), mapping.getVersion(), Activation.ACTIVE);
    }

    @CacheEvict(value = CacheConstants.CACHE_CATE_KEY_MAPPING, allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void saveNewVersion(CategoryKeyMapping mapping, List<FieldKeyMapping> fields) {
        // 防呆檢測，避免蓋到同一個Mapping
        if (ckmDao.isExistIdAndVersion(mapping.getId(), mapping.getVersion())) {
            log.error("mapping id:" + mapping.getId() + " mapping version:" + mapping.getVersion() + "該版本已存在!! 程式自動修正版號...為 :" + (mapping.getVersion() + 1));
            mapping.setVersion(mapping.getVersion() + 1);
            this.saveNewVersion(mapping, fields);
            return;
        }
        mapping.setSid(null);
        mapping = ckmDao.save(mapping);
        // 建立新的Mapping field obj，洗掉sid
        for (FieldKeyMapping each : fields) {
            each.setSid(null);
        }
        this.save(mapping, fields);
    }

    @Caching(evict = {
            @CacheEvict(value = CacheConstants.CACHE_TEMP_FIELD, key = "#mapping.id + #mapping.version"),
            @CacheEvict(value = CacheConstants.CACHE_CATE_KEY_MAPPING, allEntries = true),
            @CacheEvict(value = CacheConstants.CACHE_ALL_CATEGORY_PICKER, allEntries = true),
            @CacheEvict(value = CacheConstants.CACHE_findActiveBigCategory, allEntries = true)
    })
    @Transactional(rollbackFor = Exception.class)
    public void save(CategoryKeyMapping mapping, List<FieldKeyMapping> fields) {
        List<FieldKeyMapping> currentFk = fkmDao.findByMappingIdAndVersion(mapping.getId(), mapping.getVersion(), Activation.ACTIVE);
        if (fields != null) {
            currentFk.removeAll(fields);
        }

        for (FieldKeyMapping each : currentFk) {
            each.setStatus(Activation.INACTIVE);
        }
        // 2.重新設定排序
        int index = 0;
        for (FieldKeyMapping each : fields) {
            each.setMappingVersion(mapping.getVersion());
            each.setSeq(index++);
        }
        fkmDao.save(fields);
    }

    /**
     * 建立空的模版物件
     *
     * @param selectMapping
     * @param comType
     * @return
     */
    public FieldKeyMapping createEmptyFieldKeyMapping(CategoryKeyMapping selectMapping, ComType comType) {
        FieldKeyMapping to = new FieldKeyMapping();
        // 新增模版時，元件存放在dataModel內需要sid作判斷值，存檔時依照uuid2重給
        to.setSid(UUID.randomUUID().toString());
        to.setMappingId(selectMapping.getId());
        to.setMappingVersion(selectMapping.getVersion());
        to.setFieldName("");
        to.setFieldComponentType(comType);
        to.setStatus(Activation.ACTIVE);
        to.setSeq(null);
        to.setRequiredInput(Boolean.TRUE);
        to.setRequiredIndex(Boolean.FALSE);
        to.setShowFieldName(Boolean.TRUE);
        to.setComId("com_" + to.getSid());
        to.setIsSabaComponent(Boolean.FALSE);
        TemplateDefaultValueTo dv = new TemplateDefaultValueTo();
        Map<Integer, Object> mapValue = Maps.newHashMap();
        dv.setValue(mapValue);
        to.setDefaultValue(dv);
        return to;
    }
}
