package com.cy.tech.request.logic.service.reqconfirm;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.reqconfirm.to.KeepConfirmDepOwnerTo;
import com.cy.tech.request.logic.vo.RequireConfirmDepVO;
import com.cy.tech.request.repository.require.RequireConfirmDepHistoryRepository;
import com.cy.tech.request.vo.enums.ReqConfirmDepProcType;
import com.cy.tech.request.vo.require.RequireConfirmDepHistory;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkConstants;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 需求完成確認單位檔 service
 * 
 * @author allen1214_wu
 */
@Service
@NoArgsConstructor
@Slf4j
public class RequireConfirmDepHistoryService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6735313169067784918L;
    /**
     * DAO : 需求完成確認單位-異動歷程檔
     */
    @Autowired
    private RequireConfirmDepHistoryRepository requireConfirmDepHistoryRepository;

    @Autowired
    private RequireConfirmDepService requireConfirmDepService;

    public RequireConfirmDepHistory findBySid(Long sid) {
        return requireConfirmDepHistoryRepository.findBySid(sid);
    }

    /**
     * 以需求單sid查詢非停用的分派單位
     * 
     * @param requireSid
     * @param status
     * @return
     */
    @Transactional(readOnly = true)
    public List<RequireConfirmDepHistory> findRequireConfirmSid(List<Long> requireConfirmSids) {

        // ====================================
        // 查詢
        // ====================================
        List<RequireConfirmDepHistory> results = this.requireConfirmDepHistoryRepository.findByRequireConfirmSidIn(requireConfirmSids);

        if (WkStringUtils.isEmpty(results)) {
            return Lists.newArrayList();
        }

        return results;
    }

    /**
     * 儲存異動歷程
     * 
     * @param requireConfirmDepVO
     * @param progType
     * @param memo
     * @param execUserSid
     * @param sysDate
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveHistory(
            String requireNo,
            RequireConfirmDepVO requireConfirmDepVO,
            ReqConfirmDepProcType progType,
            String memo,
            Integer execUserSid,
            Date sysDate) {
        RequireConfirmDepHistory history = new RequireConfirmDepHistory();

        User user = WkUserCache.getInstance().findBySid(execUserSid);

        history.setCreatedUser(execUserSid);
        history.setCreatedDate(sysDate);
        history.setCreateDep((user == null || user.getPrimaryOrg() == null) ? null : user.getPrimaryOrg().getSid());
        history.setRequireConfirmSid(requireConfirmDepVO.getSid());
        history.setRequireSid(requireConfirmDepVO.getRequireSid());
        history.setDepSid(requireConfirmDepVO.getDepSid());
        history.setProgType(progType);
        history.setMemo(memo);

        this.requireConfirmDepHistoryRepository.save(history);
//        log.debug("[{}]：save tr_require_confirm_dep_history ({} -> {}) ",
//                requireNo,
//                WkOrgUtils.findNameBySid(requireConfirmDepVO.getDepSid()),
//                progType.getLabel());
    }

    /**
     * 新增異動負責人追蹤 (for 自動異動負責人)
     * 
     * @param tos
     */
    public void saveKeepConfirmDepOwnerTrace(List<KeepConfirmDepOwnerTo> tos) {
        Date sysDate = new Date();

        List<RequireConfirmDepHistory> entities = Lists.newArrayList();

        for (KeepConfirmDepOwnerTo keepConfirmDepOwnerTo : tos) {
            RequireConfirmDepHistory history = new RequireConfirmDepHistory();
            entities.add(history);

            history.setCreatedUser(WkConstants.ADMIN_USER_SID);
            history.setCreatedDate(sysDate);
            history.setCreateDep(1);
            history.setRequireConfirmSid(Long.parseLong(keepConfirmDepOwnerTo.getRequireConfirmSid() + ""));
            history.setRequireSid(keepConfirmDepOwnerTo.getRequireSid());
            history.setDepSid(keepConfirmDepOwnerTo.getDepSid());
            history.setProgType(ReqConfirmDepProcType.CHANGE_OWNER);

            String memo = ""
                    + keepConfirmDepOwnerTo.getModifyReason() + "<br/>"
                    + "【"
                    + this.requireConfirmDepService.prepareOwnerName(keepConfirmDepOwnerTo.getOwnerSid(), keepConfirmDepOwnerTo.getDepSid())
                    + "】 → 【"
                    + this.requireConfirmDepService.prepareOwnerName(WkConstants.MANAGER_VIRTAUL_USER_SID, keepConfirmDepOwnerTo.getDepSid())
                    + "】";

            history.setMemo(memo);
            
            log.debug("[{}]-[{}]：{} ",
                    keepConfirmDepOwnerTo.getRequireNo(),
                    WkOrgUtils.findNameBySid(keepConfirmDepOwnerTo.getDepSid()),
                    memo);
        }

        this.requireConfirmDepHistoryRepository.save(entities);
        
        

    }
}
