/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import java.io.Serializable;
import java.util.Date;

import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.vo.value.to.JsonStringListTo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 原型確認一覽表頁面顯示vo (search08.xhtml)
 *
 * @author jason_h
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Search08View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 324760131533696814L;
    /** 緊急度 */
    private UrgencyType urgency;
    /** 原型確認-立單日期 */
    private Date prototypeCreatedDate;
    /** 原型確認-填單單位 */
    private Integer prototypeCreateDep;
    private String prototypeCreateDepName;
    /** 原型確認-填單人員 */
    private Integer prototypeCreatedUser;
    /** 需求類別 */
    private String bigName;
    /** 中類 */
    private String middleName;
    /** 小類" */
    private String smallName;
    /** 需求單-需求單位 */
    private Integer requireCreateDep;
    /** 需求單-需求人員 */
    private Integer requireCreatedUser;
    /** 原型確認狀態 */
    private PtStatus prototypeStatus;
    /** 原型確認審核狀態 */
    private InstanceStatus instanceStatus;
    /** 預計原型確認完成日 */
    private Date finishDate;
    /** 最新原型確認回覆日期 */
    private Date replyDate;
    /** 原型確認版號 */
    private Integer version;
    /** 通知對象 */
    private JsonStringListTo notifyStaffTo;
    /** 通知單位 */
    private JsonStringListTo notifyDepsTo;
    /** 原型確認-主題 */
    private String ptTheme;
    /** 本地端連結網址 */
    private String localUrlLink;
    /** 原型確認單號 */
    private String checkNo;
}
