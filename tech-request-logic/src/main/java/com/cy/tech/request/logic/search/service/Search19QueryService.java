/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.search.view.Search19View;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.vo.enums.OthSetStatus;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author jason_h
 */
@Service
@Slf4j
public class Search19QueryService implements QueryService<Search19View>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1426700356224739131L;
    @Autowired
    private URLService urlService;
    @Autowired
    private WkJsonUtils jsonUtils;
    @Autowired
    private SearchService searchHelper;

    @PersistenceContext
    transient private EntityManager em;

    @Override
    public List<Search19View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        // ====================================
        // 查詢
        // ====================================

        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        @SuppressWarnings("rawtypes")
        List result = query.getResultList();
        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if (WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();
        List<Search19View> viewResult = Lists.newArrayList();
        for (int i = 0; i < result.size(); i++) {

            Search19View v = new Search19View();

            Object[] record = (Object[]) result.get(i);
            int index = 0;
            String sid = (String) record[index++];
            String requireNo = (String) record[index++];
            String requireTheme = (String) record[index++];

            Date osCreateDt = (Date) record[index++];
            String osTheme = (String) record[index++];
            String osNoticeDeps = (String) record[index++];
            String osStatus = (String) record[index++];
            Integer createDep = (Integer) record[index++];
            Integer createdUser = (Integer) record[index++];
            Date reqCreateDt = (Date) record[index++];
            String bigName = (String) record[index++];
            String middleName = (String) record[index++];
            String smallName = (String) record[index++];
            Integer requireDep = (Integer) record[index++];
            Integer requireUser = (Integer) record[index++];
            Integer urgency = (Integer) record[index++];
            String requireStatus = (String) record[index++];
            String osno = (String) record[index++];
            // 處理共通欄位 (一定要擺在『取值』最後面, 否則 index 會不對)
            v.prepareCommonColumn(record, index);

            v.setSid(sid);
            v.setRequireNo(requireNo);
            v.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
            v.setOsCreateDt(osCreateDt);
            v.setOsTheme(osTheme);
            try {
                v.setOsNoticeDeps(jsonUtils.fromJson(osNoticeDeps, JsonStringListTo.class));
            } catch (IOException ex) {
                log.error(ex.getMessage(), ex);
            }
            v.setOsStatus(OthSetStatus.valueOf(osStatus));
            v.setCreateDep(createDep);
            v.setCreateDepName(WkOrgUtils.findNameBySid(createDep));
            v.setCreatedUser(createdUser);
            v.setReqCreateDt(reqCreateDt);
            v.setBigName(bigName);
            v.setMiddleName(middleName);
            v.setSmallName(smallName);
            v.setRequireDep(requireDep);
            v.setRequireUser(requireUser);
            v.setUrgency(UrgencyType.values()[urgency]);
            v.setRequireStatus(RequireStatusType.valueOf(requireStatus));
            v.setLocalUrlLink(urlService.createLocalUrlLinkParamForTab(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(execUserSid, v.getRequireNo(), 1),
                    URLService.URLServiceAttr.URL_ATTR_TAB_OS, v.getSid()));
            viewResult.add(v);
            v.setOsno(osno);
        }
        // 解析資料-結束
        usageRecord.parserDataEnd();
        return viewResult;
    }
}
