package com.cy.tech.request.logic.service.jdbc;

import com.cy.tech.request.logic.config.ReqConstants;
import java.io.Serializable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author kasim
 */
@Slf4j
@Component
public class OnpgJdbcRepo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6276567546252212150L;
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private JdbcTemplate jdbc;

    public String findCateSmallSidByOnpgSid(String onpgSid) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT DISTINCT m.small_category_sid FROM work_onpg op ");
        sql.append("INNER JOIN tr_require r ON r.require_sid = op.onpg_source_sid ");
        sql.append("INNER JOIN tr_category_key_mapping m ON r.mapping_sid = m.key_sid ");
        sql.append("WHERE op.onpg_sid = ? ");
        try {
            return jdbc.queryForObject(sql.toString(), String.class, onpgSid);
        } catch (EmptyResultDataAccessException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }
}
