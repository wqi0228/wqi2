/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.trace.repository.WorkTraceRepository;
import com.cy.work.trace.vo.WorkTrace;
import com.cy.work.trace.vo.enums.TraceSource;
import com.cy.work.trace.vo.enums.TraceStatus;
import com.cy.work.trace.vo.enums.TraceType;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kasim
 */
@Component
public class TraceService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4425990625135800333L;

    @Autowired
    private WorkTraceRepository traceRepository;

    @Autowired
    private WkJsoupUtils jsoupUtils;

    /**
     * 依據條件查詢
     *
     * @param traceInfo
     * @return
     */
    @Transactional(readOnly = true)
    public WorkTrace findOne(WorkTrace traceInfo) {
        return traceRepository.findOne(traceInfo.getSid());
    }

    /**
     * 判斷是否有追蹤資料
     *
     * @param obj
     * @return
     */
    public Boolean hasTrace(Require obj, Org traceDep, User traceUser) {
        if (obj == null || obj.getSid() == null) {
            return Boolean.FALSE;
        }
        return traceRepository.checkTraceInfoByDepAndUserAndTraceStatusAndStatus(
                obj.getRequireNo(), TraceSource.TECH_REQUEST, traceDep,
                traceUser, TraceStatus.UN_FINISHED, Activation.ACTIVE);
    }

    /**
     * 依據條件查詢（個人）
     *
     * @param require
     * @param status
     * @param traceStatus
     * @return
     */
    @Transactional(readOnly = true)
    public List<User> findTraceUserByRequireAndStatusAndTraceStatus(
            Require require, Activation status, TraceStatus traceStatus) {
        return traceRepository.findTraceUserByStatusAndTraceStatusAndTraceTypes(
                require.getRequireNo(), TraceSource.TECH_REQUEST, status, traceStatus,
                Lists.newArrayList(TraceType.OTHER_PERSON, TraceType.PERSONAL));
    }

    /**
     * 依據條件查詢（部門）
     *
     * @param require
     * @param status
     * @param traceStatus
     * @return
     */
    @Transactional(readOnly = true)
    public List<Org> findTraceDepByRequireAndStatusAndTraceStatus(
            Require require, Activation status, TraceStatus traceStatus) {
        return traceRepository.findTraceDepByStatusAndTraceStatusAndTraceType(
                require.getRequireNo(), TraceSource.TECH_REQUEST, status,
                traceStatus, TraceType.DEPARTMENT);

    }

    /**
     * 個人追蹤存檔
     *
     * @param trace
     * @param require
     * @param memoCss
     * @param createdUser
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveByPersonal(WorkTrace trace, Require require, String memoCss, User createdUser) {
        this.crearWorkTrace(trace, require, TraceType.PERSONAL, createdUser, createdUser);
        trace.setMemo(jsoupUtils.clearCssTag(memoCss));
        trace.setMemoCss(memoCss);
        traceRepository.save(trace);
    }

    /**
     * 建立 WorkTrace
     *
     * @param trace
     * @param require
     * @param traceType
     * @param traceUser
     * @param createdUser
     */
    private void crearWorkTrace(WorkTrace trace, Require require, TraceType traceType, User traceUser, User createdUser) {
        trace.setSourceSid(require.getSid());
        trace.setSourceNo(require.getRequireNo());
        trace.setTraceUser(traceUser);
        trace.setSourceType(TraceSource.TECH_REQUEST);
        trace.setTraceStatus(TraceStatus.UN_FINISHED);
        trace.setTraceType(traceType);
        trace.setStatus(Activation.ACTIVE);
        trace.setCreatedDate(new Date());
        trace.setCreatedUser(createdUser);
    }

    /**
     * 其他追蹤存檔
     *
     * @param master
     * @param require
     * @param user
     * @param memoCss
     * @param createdUser
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveByOtherPerson(WorkTrace master, Require require, List<User> users,
            String memoCss, User createdUser) {
        List<WorkTrace> workTraces = Lists.newArrayList();
        String memo = jsoupUtils.clearCssTag(memoCss);
        for (User u : users) {
            WorkTrace obj = new WorkTrace();
            TraceType traceType = TraceType.OTHER_PERSON;
            if (createdUser.equals(u)) {
                traceType = TraceType.PERSONAL;
            }
            this.crearWorkTrace(obj, require, traceType, u, createdUser);
            obj.setMemo(memo);
            obj.setMemoCss(memoCss);
            obj.setNoticeDate(master.getNoticeDate());
            workTraces.add(obj);
        }
        traceRepository.save(workTraces);
    }

    /**
     * 他人追蹤存檔
     *
     * @param trace
     * @param require
     * @param dep
     * @param memoCss
     * @param createdUser
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveByDepartment(WorkTrace trace, Require require, Org dep, String memoCss, User createdUser) {
        trace.setSourceSid(require.getSid());
        trace.setSourceNo(require.getRequireNo());
        trace.setTraceDep(dep);
        trace.setSourceType(TraceSource.TECH_REQUEST);
        trace.setTraceStatus(TraceStatus.UN_FINISHED);
        trace.setTraceType(TraceType.DEPARTMENT);
        trace.setStatus(Activation.ACTIVE);
        trace.setCreatedDate(new Date());
        trace.setCreatedUser(createdUser);
        trace.setMemo(jsoupUtils.clearCssTag(memoCss));
        trace.setMemoCss(memoCss);
        traceRepository.save(trace);
    }

    /**
     * 取消追蹤
     *
     * @param workTrace
     * @param user
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateByInactive(WorkTrace workTrace, User user) {
        if (TraceStatus.FINISHED.equals(workTrace.getTraceStatus())
                || Activation.INACTIVE.equals(workTrace.getStatus())) {
            return;
        }
        workTrace.setStatus(Activation.INACTIVE);
        workTrace.setUpdatedDate(new Date());
        workTrace.setUpdatedUser(user);
        traceRepository.save(workTrace);
    }

    /**
     * 已完成追蹤
     *
     * @param workTrace
     * @param user
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateByFinish(List<String> finishTraceSids, User user) {
        if (CollectionUtils.isEmpty(finishTraceSids)) {
            return;
        }
        Date currentDate = new Date();
        List<WorkTrace> workTraceList = traceRepository.findAll(finishTraceSids);
        for (WorkTrace workTrace : workTraceList) {
            if (TraceStatus.FINISHED.equals(workTrace.getTraceStatus())
                    || Activation.INACTIVE.equals(workTrace.getStatus())) {
                continue;
            }
            workTrace.setTraceStatus(TraceStatus.FINISHED);
            workTrace.setFinishDate(currentDate);
            workTrace.setUpdatedDate(currentDate);
            workTrace.setUpdatedUser(user);
        }
        traceRepository.save(workTraceList);
    }

    /**
     * 修改存檔
     *
     * @param workTrace
     * @param memoCss
     * @param user
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateByMemo(WorkTrace workTrace, String memoCss, User user) {
        workTrace.setMemo(jsoupUtils.clearCssTag(memoCss));
        workTrace.setMemoCss(memoCss);
        workTrace.setUpdatedDate(new Date());
        workTrace.setUpdatedUser(user);
        traceRepository.save(workTrace);
    }

    /**
     * 以需求單號，取得追蹤中的使用者
     * @param requireNo 需求單號
     * @return 追蹤中的使用者 SID
     */
    public Set<Integer> findTraceUserSids(String requireNo) {

        // ====================================
        // 查詢
        // ====================================
        List<WorkTrace> workTraces = this.traceRepository.findBySourceNoAndSourceTypeAndTraceStatusAndStatus(
                requireNo,
                TraceSource.TECH_REQUEST,
                TraceStatus.UN_FINISHED,
                Activation.ACTIVE);

        if (WkStringUtils.isEmpty(workTraces)) {
            return Sets.newHashSet();
        }

        // ====================================
        // 收集追蹤者
        // ====================================
        Set<Integer> traceUserSids = Sets.newHashSet();
        for (WorkTrace workTrace : workTraces) {
            // 非停用使用者
            if (workTrace.getTraceUser() != null && WkUserUtils.isActive(workTrace.getTraceUser())) {
                traceUserSids.add(workTrace.getTraceUser().getSid());
            }

            // 非停用部門
            if (workTrace.getTraceDep() != null && WkOrgUtils.isActive(workTrace.getTraceDep())) {
                // 取得非停用 user , 包含主管
                Set<Integer> depUserSids = WkUserCache.getInstance().findUserSidByOrgsWithManager(
                        Lists.newArrayList(workTrace.getTraceDep().getSid()),
                        Activation.ACTIVE);

                if (WkStringUtils.notEmpty(depUserSids)) {
                    traceUserSids.addAll(depUserSids);
                }
            }
        }

        // ====================================
        // 回傳
        // ====================================
        return traceUserSids;

    }
}
