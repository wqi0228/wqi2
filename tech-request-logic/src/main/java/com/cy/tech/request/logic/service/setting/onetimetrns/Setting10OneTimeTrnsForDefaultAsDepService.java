package com.cy.tech.request.logic.service.setting.onetimetrns;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.vo.converter.SetupInfoToConverter;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.setting.asdep.SettingDefaultAssignSendDepVO;
import com.cy.tech.request.vo.value.to.SetupInfoTo;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.converter.SplitListIntConverter;
import com.google.common.collect.Lists;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

/**
 * 一次性轉檔 - 預設分派/通知單位
 * 
 * @author allen1214_wu
 *
 */
@Slf4j
@Service
public class Setting10OneTimeTrnsForDefaultAsDepService {

    // ========================================================================
    // 服務
    // ========================================================================

    @Autowired
    private transient NativeSqlRepository nativeSqlRepository;
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private transient JdbcTemplate jdbcTemplate;

    // ========================================================================
    // 變數
    // ========================================================================
    /**
     * 每次批次執行筆數
     */
    private final Integer BATCH_UPDATE_SIZE = 200;
    private final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:sss");

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 
     */
    @Transactional(rollbackFor = Exception.class)
    public void process() {

        this.processByType(AssignSendType.ASSIGN);
        this.processByType(AssignSendType.SEND);

    }

    private void processByType(AssignSendType assignSendType) {

        String aliasName = AssignSendType.ASSIGN.equals(assignSendType) ? "assign" : "send";

        // ====================================
        // 準備 SQL
        // ====================================

        StringBuffer varname1 = new StringBuffer();
        varname1.append("SELECT setup." + aliasName + "_sid         as sid, ");
        varname1.append("       setup.basic_data_small_category_sid as smallCategorySid, ");
        varname1.append("       setup." + aliasName + "_info        as depsInfo_src, ");
        varname1.append("       setup.create_usr                    as createdUser, ");
        varname1.append("       setup.create_dt                     as createdDate, ");
        varname1.append("       setup.update_usr                    as updatedUser, ");
        varname1.append("       setup.update_dt                     as updatedDate ");
        varname1.append("FROM   tr_" + aliasName + "_setup setup ");
        varname1.append("       LEFT JOIN tr_setting_default_asdep def ");
        varname1.append("              ON def.basic_data_small_category_sid = setup.basic_data_small_category_sid ");
        varname1.append("                 AND def.assign_send_type = '" + assignSendType.name() + "' ");
        varname1.append("                 AND def.check_item_type = 'BBIN' ");
        varname1.append("WHERE  trns_flag = 0 "); // 僅查詢未轉的
        varname1.append("       AND def.sid is null"); // 新 table 不能有資料

        // ====================================
        // 查詢
        // ====================================
        List<SettingDefaultAssignSendDepVO> vos = nativeSqlRepository.getResultList(
                varname1.toString(),
                null,
                SettingDefaultAssignSendDepVO.class);

        if (WkStringUtils.isEmpty(vos)) {
            log.info("查詢不到需要轉檔的[{}]資料", assignSendType.name());
            return;
        }

        log.info("查詢到[{}]資料[{}]筆!", assignSendType.name(), vos.size());

        // ====================================
        // 資料處理
        // ====================================
        SetupInfoToConverter setupInfoToConverter = new SetupInfoToConverter();
        List<SettingDefaultAssignSendDepVO> insertVos = Lists.newArrayList();
        for (SettingDefaultAssignSendDepVO settingDefaultAssignSendDepVO : vos) {
            // 類別
            settingDefaultAssignSendDepVO.setAssignSendType(assignSendType);
            // 檢查類別固定為BBIN (轉換前只有BBIN)
            settingDefaultAssignSendDepVO.setCheckItemType(RequireCheckItemType.BBIN);

            // 部門sid
            SetupInfoTo info = setupInfoToConverter.convertToEntityAttribute(settingDefaultAssignSendDepVO.getDepsInfo_src());
            List<Integer> depsInfo = Lists.newArrayList();
            if (info != null && WkStringUtils.notEmpty(info.getDepartment())) {
                for (String depSid : info.getDepartment()) {
                    if (WkStringUtils.isNumber(depSid)) {
                        depsInfo.add(Integer.parseInt(depSid));
                    }
                }
            }
            settingDefaultAssignSendDepVO.setDepsInfo(depsInfo);

            //
            insertVos.add(settingDefaultAssignSendDepVO);

            // 複製一份 NONE 的
            SettingDefaultAssignSendDepVO noneVo = WkCommonUtils.cloneObject(
                    settingDefaultAssignSendDepVO,
                    new TypeToken<SettingDefaultAssignSendDepVO>() {
                    }.getType());
            noneVo.setCheckItemType(RequireCheckItemType.NONE);
            insertVos.add(noneVo);

        }

        // ====================================
        // 轉入新 table :tr_setting_default_asdep
        // ====================================
        this.batchInsertRequireCheckItem(insertVos);

        // ====================================
        // 註記原資料已轉檔
        // ====================================
        this.updateTrnsFlag(assignSendType, vos);

    }

    /**
     * @param vos SettingDefaultAssignSendDepVO
     */
    @Transactional(rollbackFor = Exception.class)
    private void batchInsertRequireCheckItem(List<SettingDefaultAssignSendDepVO> vos) {

        if (WkStringUtils.isEmpty(vos)) {
            log.warn("傳入的為空，不執行");
            return;
        }

        SplitListIntConverter splitListIntConverter = new SplitListIntConverter();

        Long startTime = System.currentTimeMillis();
        String procTitle = "INSERT tr_setting_default_asdep [" + vos.size() + "]筆";
        log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

        StringBuffer insertSQL = new StringBuffer();
        insertSQL.append("INSERT INTO tr_setting_default_asdep ");
        insertSQL.append("            (create_usr, ");
        insertSQL.append("             create_dt, ");
        insertSQL.append("             update_usr, ");
        insertSQL.append("             update_dt, ");
        insertSQL.append("             basic_data_small_category_sid, ");
        insertSQL.append("             assign_send_type, ");
        insertSQL.append("             check_item_type, ");
        insertSQL.append("             deps_info) ");
        insertSQL.append("VALUES      ( ?, "); // create_usr
        insertSQL.append("              ?, "); // create_dt
        insertSQL.append("              ?, "); // update_usr
        insertSQL.append("              ?, "); // update_dt
        insertSQL.append("              ?, "); // basic_data_small_category_sid
        insertSQL.append("              ?, "); // assign_send_type
        insertSQL.append("              ?, "); // check_item_type
        insertSQL.append("              ? );");// deps_info
        int totalSize = vos.size();
        for (int i = 0; i < totalSize; i += BATCH_UPDATE_SIZE) {
            int index = (i + BATCH_UPDATE_SIZE) > totalSize ? totalSize : i + BATCH_UPDATE_SIZE;
            // 取得本次批次執行筆數
            final List<SettingDefaultAssignSendDepVO> batchList = vos.subList(i, index);

            this.jdbcTemplate.batchUpdate(insertSQL.toString(), new BatchPreparedStatementSetter() {

                @Override
                public int getBatchSize() {
                    return batchList.size();
                }

                @Override
                public void setValues(PreparedStatement ps, int i) throws SQLException {
                    SettingDefaultAssignSendDepVO item = batchList.get(i);
                    try {

                        int index = 1;
                        // create_usr
                        ps.setInt(index++, item.getCreatedUser());
                        // create_dt
                        ps.setString(index++, item.getCreatedDate() == null ? null : df.format(item.getCreatedDate()));
                        // update_usr (integer 不可以為 null, 改以 String 傳入)
                        ps.setString(index++, item.getUpdatedUser() == null ? null : item.getUpdatedUser() + "");
                        // update_dt
                        ps.setString(index++, item.getUpdatedDate() == null ? null : df.format(item.getUpdatedDate()));

                        // basic_data_small_category_sid
                        ps.setString(index++, item.getSmallCategorySid());
                        // assign_send_type
                        ps.setString(index++, item.getAssignSendType().name());
                        // check_item_type
                        ps.setString(index++, item.getCheckItemType().name());
                        // deps_info
                        ps.setString(index++, String.join(",", splitListIntConverter.convertToDatabaseColumn(item.getDepsInfo())));
                    } catch (Exception e) {
                        log.error("傳入資料解析錯誤!");
                        log.info(new com.google.gson.GsonBuilder().setPrettyPrinting().create().toJson(item));
                        throw e;
                    }

                }
            });
        }
        log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
    }

    /**
     * 更新主檔的轉檔 flag
     * 
     * @param requireCheckItems
     */
    private void updateTrnsFlag(AssignSendType assignSendType, List<SettingDefaultAssignSendDepVO> vos) {

        if (WkStringUtils.isEmpty(vos)) {
            return;
        }

        String aliasName = AssignSendType.ASSIGN.equals(assignSendType) ? "assign" : "send";

        // ====================================
        // 收集有轉檔資料的sid
        // ====================================
        Set<String> sids = vos.stream()
                .map(SettingDefaultAssignSendDepVO::getSid)
                .collect(Collectors.toSet());

        // ====================================
        // 組 SQL
        // ====================================
        // 避免參數過多發生錯誤, 直接將SID組入字串
        String sql = ""
                + "UPDATE tr_" + aliasName + "_setup "
                + "   SET trns_flag = 1 "
                + " WHERE " + aliasName + "_sid IN ( '" + String.join("' ,'", sids) + "'); ";

        // ====================================
        // 執行 SQL
        // ====================================
        jdbcTemplate.execute(sql);
    }

}
