/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.anew.config;

import java.io.Serializable;
import lombok.Getter;
import org.springframework.stereotype.Component;
/**
 * 變數 管理
 *
 * @author kasim
 */
@Component
public class ReqLogicConstants implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7619901175448883030L;
    @Getter
    /** 案件單分頁 url */
    private final String openerIssueUrlKey = "/tech-issue/techIssue/techIssue11.xhtml?technicalCaseNo=";

    @Getter
    /** 需求單分頁 url */
    private final String openerReqUrlKey = "/tech-request/require/require02.xhtml";
}
