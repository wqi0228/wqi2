package com.cy.tech.request.logic.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Id;

import org.springframework.beans.BeanUtils;

import com.cy.commons.enums.OrgLevel;
import com.cy.tech.request.vo.converter.ReqConfirmDepProgStatusConverter;
import com.cy.tech.request.vo.enums.ReqConfirmDepCompleteType;
import com.cy.tech.request.vo.enums.ReqConfirmDepProgStatus;
import com.cy.tech.request.vo.require.RequireConfirmDep;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.common.vo.AbstractEntityVO;
import com.cy.work.common.vo.converter.SplitListIntConverter;
import com.google.common.collect.Sets;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 需求完成確認單位檔
 * 
 * @author allen1214_wu
 */
@EqualsAndHashCode(of = { "sid" }, callSuper = false)
@NoArgsConstructor
public class RequireConfirmDepVO extends AbstractEntityVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3545450613909585628L;

    private static final SplitListIntConverter splitListIntConverter = new SplitListIntConverter();

    /**
     * @param assignDep
     */
    public RequireConfirmDepVO(RequireConfirmDep assignDep) {
        // ====================================
        // copy Properties
        // ====================================
        BeanUtils.copyProperties(assignDep, this);
    }

    /**
     * 建構子
     * 
     * @param requireSid 需求單 SID
     * @param depSid     單位
     * @param ownerSid   負責人 SID
     */
    public RequireConfirmDepVO(
            String requireSid,
            Integer depSid,
            Integer ownerSid,
            String assignDepSidsStr) {

        this.requireSid = requireSid;
        this.depSid = depSid;
        this.ownerSid = ownerSid;
        this.assignDepSidsStr = assignDepSidsStr;
    }

    /**
     * SID
     */
    @Id
    @Getter
    @Setter
    private Long sid;

    /**
     * 需求單 SID
     */
    @Getter
    @Setter
    private String requireSid;

    /**
     * 分派單位 SID
     */
    @Getter
    @Setter
    private Integer depSid;

    /**
     * 分派單位 名稱
     */
    @Getter
    @Setter
    private String depName;

    /**
     * 分派單位 名稱
     */
    @Getter
    @Setter
    private String depFullName;

    /**
     * 分派單位負責人 SID
     */
    @Getter
    @Setter
    private Integer ownerSid;

    /**
     * 分派單位負責人 暱稱
     */
    @Getter
    @Setter
    private String ownerName;

    /**
     * 是否已領單
     */
    @Getter
    @Setter
    private boolean received;

    /**
     * 需求完成進度
     */
    @Getter
    @Setter
    private ReqConfirmDepProgStatus progStatus;

    public void setProgStatusStr(String progStatusStr) {
        this.progStatus = null;
        progStatusStr = WkStringUtils.safeTrim(progStatusStr);
        if (WkStringUtils.isEmpty(progStatusStr)) {
            return;
        }
        try {
            this.progStatus = new ReqConfirmDepProgStatusConverter().convertToEntityAttribute(progStatusStr);
        } catch (Exception e) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.ERROR, "錯誤的 ReqConfirmDepProgStatus 型別資料", 10);
        }
    }

    /**
     * 需求完成狀態
     */
    @Getter
    @Setter
    private ReqConfirmDepCompleteType completeType;

    /**
     * 確認時間
     */
    @Getter
    @Setter
    private Date confirmDate;

    /**
     * 確認者
     */
    @Getter
    @Setter
    private Integer confirmUser;

    /**
     * 需求完成說明
     */
    @Getter
    @Setter
    private String completeMemo;

    @Getter
    @Setter
    private List<RequireConfirmDepTraceVO> traceMsgs;

    /**
     * @return
     */
    public boolean isNoTrace() { return WkStringUtils.isEmpty(this.traceMsgs); }

    /**
     * 分派單位字串
     */
    private String assignDepSidsStr;

    /**
     * 相關的『主單』所有被分派的單位
     */
    public Set<Integer> getAssignDepSidsByRequire() {
        if (WkStringUtils.isEmpty(this.assignDepSidsStr)) {
            return Sets.newHashSet();
        }

        // 此張單據被分派的部門
        return Sets.newHashSet(splitListIntConverter.convertToEntityAttribute(assignDepSidsStr));
    }

    // ============================================================================
    // 以下為 view 顯示加工欄位
    // ============================================================================
    /**
     * 排序序號
     */
    @Getter
    @Setter
    public Integer seqNum = 0;

    /**
     * 進度條 CSS
     * 
     * @return
     */
    public String getProgressStyle() {
        if (this.progStatus == null) {
            return "";
        }
        return "width:" + this.progStatus.getProgressPercent() + "%;"
                + "background-color:" + this.progStatus.getProgressColer() + ";";
    }

    public String getProgressHtml() { return "<div class='assignDepWorkPanel-progress'>"
            + "<div class='assignDepWorkPanel-progress-bar' style='" + this.getProgressStyle() + "' /></div>"; }

    public String getCompleteResultTooltip() {
        if (!ReqConfirmDepProgStatus.COMPLETE.equals(this.progStatus)) {
            return "";
        }

        String message = "";
        message += WkDateUtils.formatDate(this.confirmDate, WkDateUtils.YYYY_MM_DD_HH24_MI);
        message += " ";
        message += WkUserUtils.prepareUserNameWithDep(this.confirmUser, OrgLevel.THE_PANEL, false, "-");
        if (WkStringUtils.notEmpty(this.completeMemo)) {
            message += "：";
            message += this.completeMemo;
        }
        return message;
    }

    /**
     * 處理進度名稱
     * 
     * @return
     */
    public String getProgStatusName() {
        if (this.progStatus == null) {
            return "";
        }

        if (ReqConfirmDepProgStatus.COMPLETE.equals(this.progStatus)) {
            if (this.completeType == null) {
                return this.progStatus.getLabel();
            }
            return this.completeType.getLabel();
        }

        return this.progStatus.getLabel();
    }

    public Integer getProgressPercent() {
        if (this.progStatus == null) {
            return 0;
        }
        return this.progStatus.getProgressPercent();
    }

    
    
    @Getter
    @Setter
    public String progStatusStrForSearch07;
    
    public ReqConfirmDepProgStatus getProgStatusForSearch07() {
        if (WkStringUtils.isEmpty(progStatusStrForSearch07)) {
            return null;
        }
        this.progStatusStrForSearch07 = WkStringUtils.safeTrim(this.progStatusStrForSearch07);
        try {
            return new ReqConfirmDepProgStatusConverter().convertToEntityAttribute(this.progStatusStrForSearch07);
        } catch (Exception e) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.ERROR, "錯誤的 ReqConfirmDepProgStatus 型別資料", 10);
        }
        return null;
    }
}
