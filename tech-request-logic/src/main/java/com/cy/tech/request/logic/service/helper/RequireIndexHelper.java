/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.helper;

import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.repository.require.RequireIndexDictionaryRepository;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireIndexDictionary;
import com.cy.tech.request.vo.template.component.ComBase;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 需求單索引輔助
 *
 * @author shaun
 */
@Component
public class RequireIndexHelper implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 1929003668581227691L;

    @Autowired
	transient private RequireIndexDictionaryRepository requireIndexDictionaryRepository;

	@Autowired
	transient private RequireService requireService;

	/**
	 * 建立索引前刪除原本索引
	 *
	 * @param require
	 */
	@Transactional(rollbackFor = Exception.class)
	public void removeIndex(String requireSid) {
		Require require = this.requireService.findByReqSid(requireSid);
		this.requireIndexDictionaryRepository.deleteByRequire(require);
	}

	@Transactional(readOnly = true)
	public List<RequireIndexDictionary> findByRequire(Require require) {
		return requireIndexDictionaryRepository.findByRequire(require);
	}

	@Transactional(readOnly = true)
	public List<RequireIndexDictionary> findIndexByRequiresAndFieldName(List<Require> requires, String fieldName) {
		return requireIndexDictionaryRepository.findIndexByRequiresAndFieldName(requires, fieldName);
	}

	/**
	 * @param requireSid
	 * @param requireNo
	 * @param coms
	 * @param execDate
	 */
	public void createByComs(
	        String requireSid,
	        String requireNo,
	        Collection<ComBase> coms,
	        Date execDate) {

		if (WkStringUtils.isEmpty(coms)) {
			return;
		}

		// ====================================
		// 取得需求單主檔
		// ====================================
		Require require = this.requireService.findByReqSid(requireSid);
		if (require == null || WkStringUtils.isEmpty(coms)) {
			return;
		}

		// ====================================
		// 建立 RequireIndexDictionary
		// ====================================
		List<RequireIndexDictionary> requireIndexDictionarys = Lists.newArrayList();

		for (ComBase com : coms) {
			if (com.getNeedCreateIndex()) {
				RequireIndexDictionary index = new RequireIndexDictionary();
				index.setFieldName(com.getName());
				index.setFieldContent(com.getIndexContent());
				index.setMapping(require.getMapping());
				index.setRequire(require);
				index.setRequireNo(requireNo);
				index.setCreateDate(execDate);
				index.setLastUpdatedDate(execDate);
				requireIndexDictionarys.add(index);
			}
		}

		if (WkStringUtils.isEmpty(requireIndexDictionarys)) {
			return;
		}

	}

}
