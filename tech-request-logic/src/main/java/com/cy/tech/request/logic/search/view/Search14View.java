/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.enums.UrgencyType;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 送測簽核進度查詢頁面顯示vo (search14.xhtml)
 *
 * @author jason_h
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Search14View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6832921880590344917L;
    /** 需求單-緊急度 */
    private UrgencyType urgency;
    /** 建立日期 */
    private Date createdDate;
    /** 填單人所歸屬的部門 */
    private Integer createDep;
    /** 建立者 */
    private Integer createdUser;
    /** 送測主題 */
    private String testTheme;
    /** 核淮日 */
    private Date approvalDate;
    /** 預計完成日 */
    private Date establishDate;
    /** 需求類別 */
    private String bigName;
    /** 中類 */
    private String middleName;
    /** 小類 */
    private String smallName;
    /** 需求單位 */
    private Integer requireDep;
    /** 需求人員 */
    private Integer requireUser;
    /** 預設簽核人員暱稱 */
    private String defaultSignedName;
    /** 送測審核狀態 */
    private InstanceStatus instanceStatus;
    /** 送測單號 */
    private String testinfoNo;
    /** FB Case No */
    private Integer fbId;
    /** 本地端連結網址 */
    private String localUrlLink;
}
