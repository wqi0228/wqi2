/**
 * 
 */
package com.cy.tech.request.logic.service.setting;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.service.orgtrns.OrgTrnsAssignNoticeService;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsAssignSendInfoForTrnsTO;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * @author allen1214_wu
 */
@Service
public class BatchAddNoticeDepService implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 2275993624020619362L;

    // ========================================================================
    //
    // ========================================================================
    @Autowired
    private OrgTrnsAssignNoticeService orgTrnsAssignNoticeService;
    @Autowired
    private transient EntityManager entityManager;

    // ========================================================================
    //
    // ========================================================================
    /**
     * @param requireSids        要新增通知單位的需求單 sid
     * @param requireNos         要新增通知單位的需求單號
     * @param targetNoticeDepSid 要新增的通知單位 org sid
     * @param execInfos          執行資訊容器
     * @return 執行筆數
     * @throws UserMessageException 錯誤時拋出
     */
    public int process(
            Set<String> requireSids,
            Set<String> requireNos,
            Integer targetNoticeDepSid,
            List<String> execInfos) throws UserMessageException {

        // ====================================
        // 查詢通知單位
        // ====================================
        List<OrgTrnsAssignSendInfoForTrnsTO> noticeInfos = this.orgTrnsAssignNoticeService.queryAssignSendInfo(
                requireSids,
                requireNos,
                AssignSendType.SEND);

        if (WkStringUtils.isEmpty(noticeInfos)) {
            throw new UserMessageException("查詢不到對應單據，請檢查輸入資料是否正確！");
        }

        // ====================================
        // 查詢分派單位
        // ====================================
        List<OrgTrnsAssignSendInfoForTrnsTO> assignInfos = this.orgTrnsAssignNoticeService.queryAssignSendInfo(
                requireSids,
                requireNos,
                AssignSendType.ASSIGN);

        // 以需求單 sid 收集分派單位
        Map<String, List<Integer>> assignDepsMapByRequireSid = assignInfos.stream()
                .collect(Collectors.toMap(
                        OrgTrnsAssignSendInfoForTrnsTO::getRequireSid,
                        OrgTrnsAssignSendInfoForTrnsTO::getDepSids));

        // ====================================
        // 準備通知單位轉置資料
        // ====================================
        List<OrgTrnsAssignSendInfoForTrnsTO> needAddNoticeDepVOs = Lists.newArrayList();

        for (OrgTrnsAssignSendInfoForTrnsTO to : noticeInfos) {
            // 檢查新增的通知部門已經存在於分派單位
            if (assignDepsMapByRequireSid.containsKey(to.getRequireSid())) {
                List<Integer> assignDepSids = assignDepsMapByRequireSid.get(to.getRequireSid());
                if (WkStringUtils.notEmpty(assignDepSids)) {
                    if (assignDepSids.contains(targetNoticeDepSid)) {
                        execInfos.add("[" + to.getRequireNo() + "] 已存在於『分派』單位，無法新增!");
                        continue;
                    }
                }
            }

            // 計算轉換後資料 new AssignDepSids
            // 原有的分派單位資料
            Set<Integer> oldNoticeDepSids = Sets.newHashSet();
            if (WkStringUtils.notEmpty(to.getDepSids())) {
                oldNoticeDepSids.addAll(to.getDepSids());
            }

            // 檢查通知單位已存在
            if (oldNoticeDepSids.contains(targetNoticeDepSid)) {
                execInfos.add("[" + to.getRequireNo() + "] 已存在該通知單位，無需新增!");
                continue;
            }

            // 複製一份，並加入新增單位
            Set<Integer> newNoticeDepSids = oldNoticeDepSids.stream()
                    .collect(Collectors.toSet());
            newNoticeDepSids.add(targetNoticeDepSid);

            to.setNewAssignDepSids(Lists.newArrayList(newNoticeDepSids));

            // ====================================
            // 比對設定單位差異
            // ====================================
            List<List<Integer>> depModifyResult = Lists.newArrayList();
            // 新增單位
            depModifyResult.add(Lists.newArrayList(targetNoticeDepSid));
            // 移除單位 (沒有)
            depModifyResult.add(Lists.newArrayList());

            to.setDepModifyResult(depModifyResult);

            // ====================================
            // 加入處理列表
            // ====================================
            needAddNoticeDepVOs.add(to);
        }

        if (WkStringUtils.isEmpty(needAddNoticeDepVOs)) {
            throw new UserMessageException("沒有需要處理的單據!");
        }

        // ====================================
        // 新增通知單位
        // ====================================
        // 更新資料
        this.orgTrnsAssignNoticeService.trnsAssignSendInfo(
                needAddNoticeDepVOs,
                AssignSendType.SEND,
                new Date());

        // 清除 hibernate 快取
        entityManager.clear();

        // log訊息
        for (OrgTrnsAssignSendInfoForTrnsTO to : needAddNoticeDepVOs) {
            execInfos.add("[" + to.getRequireNo() + "] ok!");
        }

        return needAddNoticeDepVOs.size();
    }
}
