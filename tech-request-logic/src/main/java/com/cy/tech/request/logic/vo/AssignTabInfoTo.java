/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 分派內容資訊
 *
 * @author shaun
 */
@NoArgsConstructor
@EqualsAndHashCode(of = {"createTime", "createName"})
public class AssignTabInfoTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8807664129320385471L;
    /** 分派通知建立時間 */
    @Getter
    @Setter
    private Date createTime;
    /** 分派通知建立者 */
    @Getter
    @Setter
    private String createName;
    /** 分派單位 - sids */
    @Getter
    @Setter
    private List<String> assignUnit;
//    /** 分派成員 - sids */
//    @Getter
//    @Setter
//    private List<String> assignUser;


//通知部份若重新啟用，需解開以下註解 20160325 by shaun    
//    /** 通知單位 - sids */
//    @Getter
//    @Setter
//    private List<String> sendUnit;
//    /** 通知成員 - sids */
//    @Getter
//    @Setter
//    private List<String> sendUser;
}
