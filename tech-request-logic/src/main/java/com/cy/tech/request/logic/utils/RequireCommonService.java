package com.cy.tech.request.logic.utils;

import java.io.Serializable;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

/**
 * @author allen
 *
 */
@Service
public class RequireCommonService implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -258838498340842082L;
    private static RequireCommonService instance;

    public static RequireCommonService getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        RequireCommonService.instance = this;
    }
}
