/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.logic.search.view.Search22View;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.trace.vo.enums.TraceStatus;
import com.cy.work.trace.vo.enums.TraceType;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author jason_h
 */
@Service
@Slf4j
public class Search22QueryService implements QueryService<Search22View>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 566832998713101426L;
    @Autowired
    private URLService urlService;
    @Autowired
    private SearchService searchHelper;

    @PersistenceContext
    transient private EntityManager em;

    @Override
    public List<Search22View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {

        // ====================================
        // 查詢
        // ====================================
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        @SuppressWarnings("rawtypes")
        List result = query.getResultList();
        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if (WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();

        List<Search22View> viewResult = Lists.newArrayList();
        for (int i = 0; i < result.size(); i++) {

            Search22View v = new Search22View();
            int index = 0;

            Object[] record = (Object[]) result.get(i);
            String sid = (String) record[index++];
            Boolean hasForwardDep = (Boolean) record[index++];
            Boolean hasForwardMember = (Boolean) record[index++];
            Boolean hasLink = (Boolean) record[index++];
            String traceType = (String) record[index++];
            String smallName = (String) record[index++];
            Date noticeDate = (Date) record[index++];
            Integer traceCreatedUser = (Integer) record[index++];
            String traceStatus = (String) record[index++];
            String memo = (String) record[index++];
            String memoCss = "";
            try {
                byte[] msageBytpe = (byte[]) record[index++];
                memoCss = msageBytpe == null ? "" : new String(msageBytpe, "UTF-8");
            } catch (IOException ex) {
                log.error(ex.getMessage(), ex);
            }
            String requireTheme = (String) record[index++];
            String requireStatus = (String) record[index++];
            Date requireUpdatedDate = (Date) record[index++];
            Date traceCreatedDate = (Date) record[index++];
            String requireNo = (String) record[index++];
            Integer urgency = (Integer) record[index++];
            Boolean status = (Boolean) record[index++];
            // 處理共通欄位 (一定要擺在『取值』最後面, 否則 index 會不對)
            v.prepareCommonColumn(record, index);

            v.setSid(sid);
            v.setHasForwardDep(hasForwardDep);
            v.setHasForwardMember(hasForwardMember);
            v.setHasLink(hasLink);
            v.setHasTrace(Boolean.TRUE);
            v.setTraceType(TraceType.valueOf(traceType));
            v.setSmallName(smallName);
            v.setNoticeDate(noticeDate);
            v.setTraceCreatedUser(traceCreatedUser);
            v.setTraceStatus(TraceStatus.valueOf(traceStatus));
            v.setMemo(memo);
            v.setMemoCss(memoCss);
            v.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
            v.setRequireStatus(RequireStatusType.valueOf(requireStatus));
            v.setRequireUpdatedDate(requireUpdatedDate);
            v.setTraceCreatedDate(traceCreatedDate);
            v.setRequireNo(requireNo);
            v.setUrgency(UrgencyType.values()[urgency]);
            v.setLocalUrlLink(urlService.createLoacalURLLink(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(execUserSid, v.getRequireNo(), 1)));
            v.setStatus(status ? Activation.INACTIVE : Activation.ACTIVE);

            viewResult.add(v);
        }
        // 解析資料-結束
        usageRecord.parserDataEnd();
        return viewResult;
    }
}
