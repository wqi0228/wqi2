/**
 * 
 */
package com.cy.tech.request.logic.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.helper.systemnotify.SysNotifyHelper;
import com.cy.tech.request.logic.utils.ProcessLog;
import com.cy.tech.request.repository.require.ReqModifyRepo;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireTrace;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * 需求單- 反結案
 * 
 * @author allen1214_wu
 */
@Slf4j
@Component
public class RequireProcessCloseRollbackService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1374263431220015131L;
    // =======================================================================================
    // 服務物件區
    // =======================================================================================
    @Autowired
    private transient RequireService requireService;
    @Autowired
    private transient RequireShowService requireShowService;
    @Autowired
    private transient RequireTraceService requireTraceService;
    @Autowired
    private transient ReqModifyRepo reqModifyRepo;
    @Autowired
    private transient SysNotifyHelper sysNotifyHelper;
    @Autowired
    private transient RequireReadRecordHelper requireReadRecordHelper;

    // =======================================================================================
    // 方法區
    // =======================================================================================
    /**
     * 執行需求單反結案
     * 
     * @param require  需求單主檔
     * @param execUser 執行者
     * @param execDate 執行時間
     * @param isForce  是否為強制反結案
     * @throws UserMessageException 單據狀態、權限檢核失敗時拋出
     */
    @Transactional(rollbackFor = Exception.class)
    public void executeByUser(
            Require require,
            User execUser,
            Date execDate,
            boolean isForce) throws UserMessageException {

        // ====================================
        // 重新查詢 DB 資訊
        // ====================================
        Require requireDB = this.requireService.findByReqSid(require.getSid());

        // ====================================
        // 單據狀態、權限檢核
        // ====================================
        // 一般
        if (isForce && requireShowService.showRollbackCloseBtn(requireDB, execUser.getSid())) {
            throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }

        // 強制
        if (!isForce && requireShowService.showForceRollbackCloseBtn(requireDB, execUser.getSid())) {
            throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }

        // ====================================
        // 呼叫核心程序
        // ====================================
        this.process(requireDB, execUser, execDate, isForce);

    }

    // =======================================================================================
    // 方法區
    // =======================================================================================
    /**
     * 執行需求單反結案 (核心方法，不對外開放)
     * 
     * @param require  需求單主檔
     * @param execUser 執行者
     * @param execDate 執行時間
     * @param isForce  是否為強制反結案
     * @throws UserMessageException 單據狀態、權限檢核失敗時拋出
     */
    @Transactional(rollbackFor = Exception.class)
    private void process(
            Require require,
            User execUser,
            Date execDate,
            boolean isForce) throws UserMessageException {

        String procTypeDesc = isForce ? "強制反結案" : "反結案";

        // ====================================
        // 檢核
        // ====================================
        // 狀態需為『結案』、『自動結案』
        List<RequireStatusType> acceptStatus = Lists.newArrayList(
                RequireStatusType.CLOSE,
                RequireStatusType.AUTO_CLOSED);

        if (!acceptStatus.contains(require.getRequireStatus())) {
            throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }

        // ====================================
        // start log
        // ====================================
        ProcessLog processLog = new ProcessLog(require.getRequireNo(), "", "執行" + procTypeDesc);
        log.info(processLog.prepareStartLog());

        // ====================================
        // 建立追蹤檔
        // ====================================
        RequireTrace closeTrace = new RequireTrace();
        closeTrace.setCreatedDate(new Date());
        closeTrace.setCreatedUser(execUser);
        closeTrace.setRequire(require);
        closeTrace.setRequireNo(require.getRequireNo());
        closeTrace.setRequireTraceType(RequireTraceType.UNREQUIRE_CLOSE);
        closeTrace.setRequireTraceContent("執行" + procTypeDesc + "，製作進度退回『" + RequireStatusType.COMPLETED.getValue() + "』！");

        this.requireTraceService.save(closeTrace);

        // ====================================
        // 更新主檔狀態
        // ====================================
        require.setHasTrace(Boolean.TRUE);
        require.setCloseCode(Boolean.FALSE);
        require.setCloseDate(null);
        require.setCloseUser(null);
        require.setRequireStatus(RequireStatusType.COMPLETED);
        require.setReadReason(ReqToBeReadType.CLOSE_TO_PROCESS);

        this.reqModifyRepo.updateReqClose(
                require,
                require.getHasTrace(),
                require.getCloseCode(),
                require.getCloseDate(),
                require.getCloseUser(),
                require.getRequireStatus(),
                require.getReadReason(),
                require.getCloseUser(),
                require.getCloseDate());

        // ====================================
        // 將『已經讀取』過單據的人，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.REQUIRE,
                require.getSid(),
                execUser.getSid());

        // ====================================
        // 處理系統通知 (待結案)
        // ====================================
        try {
            this.sysNotifyHelper.processForWaitClose(require.getSid(), execUser.getSid());
        } catch (Exception e) {
            log.error("執行系統通知失敗!" + e.getMessage(), e);
        }

        // ====================================
        // complete log
        // ====================================
        log.info(processLog.prepareCompleteLog());
    }
}
