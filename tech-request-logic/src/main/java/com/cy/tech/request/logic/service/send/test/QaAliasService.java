package com.cy.tech.request.logic.service.send.test;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.vo.User;
import com.cy.tech.request.repository.worktest.WorkTestQAAliasRepo;
import com.cy.tech.request.vo.value.to.WorkTestQaAliasVO;
import com.cy.tech.request.vo.worktest.WorkTestQAAlias;
import com.cy.work.common.cache.WkCommonCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * @author aken_kao
 */
@Service
public class QaAliasService {

    @Autowired
    private WorkTestQAAliasRepo workTestQAAliasRepo;

    @Transactional(rollbackFor = Exception.class)
    public void saveOrUpdate(WorkTestQaAliasVO vo, Integer loginUserSid) {
        WorkTestQAAlias entity = null;
        if (vo.getSid() == null) {
            entity = new WorkTestQAAlias();
            entity.setUserSid(vo.getUserSid());
            entity.setUserId(vo.getUserId());
            entity.setUserAlias(vo.getUserAlias().trim());

            entity.setCreatedUser(loginUserSid);
            entity.setCreatedDate(new Date());
        } else {
            entity = findOne(vo.getSid());
            entity.setUserAlias(vo.getUserAlias());
            entity.setUpdatedUser(loginUserSid);
            entity.setUpdatedDate(new Date());
        }

        workTestQAAliasRepo.save(entity);
    }

    /**
     * 轉換成QA人員暱稱字串
     * 
     * @param qaUserList
     * @return
     */
    public String convertToQaAliasNames(String userSids) {
        if (StringUtils.isBlank(userSids)) {
            return "";
        }
        String[] arr = userSids.split(",");
        List<User> userList = WkUserCache.getInstance().findBySidStrs(Lists.newArrayList(arr));
        return this.convertToQaAliasNames(userList);
    }

    /**
     * 轉換成QA人員暱稱字串
     * 
     * @param qaUserList
     * @return
     */
    public String convertToQaAliasNames(List<User> qaUserList) {
        if (CollectionUtils.isEmpty(qaUserList)) {
            return "";
        }
        List<WorkTestQaAliasVO> qaAliasList = convertToQaAliasList(qaUserList);
        List<String> userAlias = qaAliasList.stream().map(vo -> vo.getUserAlias()).collect(Collectors.toList());
        return String.join("、", userAlias);
    }

    /**
     * 轉換成QA人員暱稱VO
     * 若無設置暱稱，預設帶入ID
     * 
     * @param qaUserList
     * @return
     */
    public List<WorkTestQaAliasVO> convertToQaAliasList(List<User> qaUserList) {
        List<WorkTestQaAliasVO> qaAliasList = Lists.newArrayList();
        Map<String, WorkTestQAAlias> aliasMap = findAllAlias();
        for (User user : qaUserList) {
            WorkTestQaAliasVO aliasVO = new WorkTestQaAliasVO();
            aliasVO.setUserSid(user.getSid());
            aliasVO.setUserId(user.getId());
            aliasVO.setOrgName(WkOrgUtils.getOrgName(user.getPrimaryOrg().toOrg()));
            WorkTestQAAlias alias = aliasMap.get(user.getId());
            if (alias != null) {
                aliasVO.setSid(alias.getSid());
                aliasVO.setUserAlias(StringUtils.isNotBlank(alias.getUserAlias()) ? alias.getUserAlias() : alias.getUserId());
            } else {
                aliasVO.setUserAlias(user.getId());
            }
            qaAliasList.add(aliasVO);
        }
        return qaAliasList;
    }

    /**
     * 轉換成QA人員暱稱VO
     * 
     * @param qaUserList
     * @return
     */
    public List<WorkTestQaAliasVO> convertToQaAliasList(String[] userSids) {
        List<WorkTestQaAliasVO> list = Lists.newArrayList();
        if (userSids != null) {
            List<User> user = WkUserCache.getInstance().findBySidStrs(Lists.newArrayList(userSids));
            list = this.convertToQaAliasList(user);
        }
        return list;
    }

    /**
     * 取得所有的QA暱稱
     * 
     * @return
     */
    public synchronized Map<String, WorkTestQAAlias> findAllAlias() {

        // ====================================
        // 由 cache 中取得
        // ====================================
        String uniqueCacheName = this.getClass().getSimpleName() + "_findAllAlias";
        
        Map<String, WorkTestQAAlias> workTestQAAliasMapByUserSid = WkCommonCache.getInstance().getCache(
                uniqueCacheName, null, (1 * 60 * 1000));
        if (workTestQAAliasMapByUserSid != null) {
            return workTestQAAliasMapByUserSid;
        }

        // ====================================
        // 實際處理
        // ====================================
        workTestQAAliasMapByUserSid = Maps.newHashMap();
        for (WorkTestQAAlias alias : findAll()) {
            workTestQAAliasMapByUserSid.put(alias.getUserId(), alias);
        }

        WkCommonCache.getInstance().putCache(uniqueCacheName, null, workTestQAAliasMapByUserSid);

        return workTestQAAliasMapByUserSid;
    }

    public List<WorkTestQAAlias> findAll() {
        return workTestQAAliasRepo.findAll();
    }

    public WorkTestQAAlias findOne(String sid) {
        return workTestQAAliasRepo.findOne(sid);
    }
}
