/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.othset;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.vo.enums.OthSetStatus;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.othset.OthSet;
import com.cy.work.common.cache.WkUserCache;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 送測顯示處理
 *
 * @author shaun
 */
@Component
public class OthSetShowService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 647808505081402416L;
    @Autowired
    private UserService userService;
    @Autowired
    private OrganizationService orgService;

    /**
     * 組合單位抬頭訊息
     *
     * @param othset
     * @return
     */
    public String findNoticeDepTitle(OthSet othset) {
        if (othset == null) {
            return "";
        }
        return othset.getNoticeDeps().getValue().stream()
                .map(each -> orgService.getOrgName(each))
                .sorted()
                .collect(Collectors.joining("、"));
    }

    /**
     * 組合單位抬頭訊息
     *
     * @param othset
     * @return
     */
    public List<String> findNoticeDepTitleByOp(OthSet othset) {
        if (othset == null) {
            return Lists.newArrayList();
        }
        return othset.getNoticeDeps().getValue().stream()
                .map(each -> orgService.getOrgName(each))
                .sorted()
                .collect(Collectors.toList());
    }

    /**
     * 關閉編輯
     *
     * @param req
     * @param othset
     * @param login
     * @return
     */
    public boolean disableEdit(Require req, OthSet othset, User login) {
        if (req == null || othset == null) {
            return true;
        }
        OthSetStatus status = othset.getOsStatus();
        //若尚未完成,則可以點編輯按鈕
        if (status.equals(OthSetStatus.FINISH) && req.getCloseCode()) {
            return true;
        }
        if (!status.equals(OthSetStatus.PROCESSING)) {
            return true;
        }
        Integer primaryOrgSid = userService.findPrimaryOrgSid(login);
        boolean isNoticeMember = othset.getNoticeDeps().getValue().contains(primaryOrgSid.toString());
        return !(this.isCreatedAndAboveManager(othset, login) || isNoticeMember);
    }

    /**
     * 檢查登入者是否為建立者及其上層主管
     *
     * @param othset
     * @param login
     * @return
     */
    public boolean isCreatedAndAboveManager(OthSet othset, User login) {
        User createUser = WkUserCache.getInstance().findBySid(othset.getCreatedUser().getSid());
        List<User> canClickDisableList = orgService.findOrgManagers(orgService.findBySid(createUser.getPrimaryOrg().getSid()));
        if (!canClickDisableList.contains(createUser)) {
            canClickDisableList.add(createUser);
        }
        return canClickDisableList.contains(login);
    }

    /**
     * 關閉回覆
     *
     * @param othset
     * @param login
     * @return
     */
    public boolean disableReply(OthSet othset, User login) {
        return othset == null;
    }

    /**
     * 關閉取消
     *
     * @param othset
     * @param login
     * @return
     */
    public boolean disableCancel(Require req, OthSet othset, User login) {
        if (othset == null) {
            return true;
        }
        OthSetStatus status = othset.getOsStatus();
        //若子單尚未作廢,則可以點取消按鈕
        if (status.equals(OthSetStatus.INVALID) && req.getCloseCode()) {
            return true;
        }
        if (!status.equals(OthSetStatus.PROCESSING)) {
            return true;
        }
        return !this.isCreatedAndAboveManager(othset, login);
    }

    /**
     * 關閉完成
     *
     * @param othset
     * @param login
     * @return
     */
    public boolean disableComplate(Require require, OthSet othset, User login) {
        if (othset == null) {
            return true;
        }
        OthSetStatus status = othset.getOsStatus();
        //若尚未完成,需要可以點完成按鈕
        if (status.equals(OthSetStatus.FINISH) && require.getCloseCode()) {
            return true;
        }
        if (!status.equals(OthSetStatus.PROCESSING)) {
            return true;
        }
        //需求單位向上層級的所有主管
        List<User> fillDocUnitManagers = orgService.findOrgManagers(orgService.findBySid(require.getCreateDep().getSid()));
        List<String> notify = othset.getNoticeDeps().getValue();
        String createReqDepSid = String.valueOf(require.getCreateDep().getSid());
        String loginDepSid = userService.findPrimaryOrgSid(login).toString();
        //通知單位執行 且 通知單位中有需求單位,需求單位成員不可執行此功能
        if (fillDocUnitManagers.contains(login) || loginDepSid.equals(createReqDepSid)) {
            return false;
        }
        return !notify.contains(loginDepSid);
    }

}
