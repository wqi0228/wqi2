/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.pt;

import com.cy.work.common.enums.ReadRecordType;
import com.cy.commons.vo.User;
import com.cy.tech.request.repository.pt.PtAlertRepo;
import com.cy.tech.request.vo.pt.PtAlert;
import com.cy.tech.request.vo.pt.PtAlreadyReply;
import com.cy.tech.request.vo.pt.PtReply;
import com.cy.work.common.enums.WorkSourceType;
import com.google.common.collect.Sets;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.Executors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 最新消息
 *
 * @author shaun
 */
@Component
public class PtAlertService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3520128773803330362L;
    @Autowired
    private PtAlertRepo alertDao;
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void carateReplyAlert(PtAlreadyReply aReply) {
        Executors.newCachedThreadPool().execute(() -> {
            Set<User> receiversUsers = this.findSenderBySourceTypeAndReply(WorkSourceType.TECH_REQUEST, aReply.getReply());
            //只取sid，以免not equals 
            User replyPerson = new User();
            replyPerson.setSid(aReply.getReply().getPerson().getSid());
            receiversUsers.add(replyPerson);
            receiversUsers.forEach(receiver -> {
                alertDao.save(this.createEmptyAlert(aReply, receiver));
            });
        });
    }

    private PtAlert createEmptyAlert(PtAlreadyReply aReply, User receiver) {
        PtAlert alert = new PtAlert();
        alert.setPtCheck(aReply.getPtCheck());
        alert.setPtNo(aReply.getPtCheck().getPtNo());
        alert.setSourceType(aReply.getPtCheck().getSourceType());
        alert.setSourceSid(aReply.getPtCheck().getSourceSid());
        alert.setSourceNo(aReply.getPtCheck().getSourceNo());
        alert.setPtTheme(sdf.format(new Date()) + " " + aReply.getPtCheck().getTheme() + " - 回覆通知");
        alert.setSender(aReply.getPerson());
        alert.setSendDate(new Date());
        alert.setReceiver(receiver);
        alert.setReply(aReply.getReply());
        alert.setAlreadyReply(aReply);
        alert.setReadStatus(ReadRecordType.UN_READ);
        return alert;
    }

    private Set<User> findSenderBySourceTypeAndReply(WorkSourceType sourceType, PtReply reply) {
        return Sets.newHashSet(alertDao.findSenderBySourceTypeAndReply(sourceType, reply));
    }

    @Transactional(readOnly = true)
    public PtAlert findBySid(String sid) {
        return alertDao.findOne(sid);
    }

    @Transactional(rollbackFor = Exception.class)
    public void save(PtAlert alert) {
        alertDao.save(alert);
    }
}
