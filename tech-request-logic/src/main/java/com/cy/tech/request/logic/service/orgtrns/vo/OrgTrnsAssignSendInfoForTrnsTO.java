package com.cy.tech.request.logic.service.orgtrns.vo;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.cy.tech.request.vo.converter.SetupInfoToConverter;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.require.AssignSendInfoForTrnsVO;
import com.cy.tech.request.vo.value.to.SetupInfoTo;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@NoArgsConstructor
@Slf4j
public class OrgTrnsAssignSendInfoForTrnsTO extends AssignSendInfoForTrnsVO {

    /**
     * 
     */
    private static final long serialVersionUID = -6949655459881269911L;
    /**
     * 單位轉換前後差異
     */
    @Setter
    @Getter
    private List<List<Integer>> depModifyResult = Lists.newArrayList();
    /**
     * 
     */
    @Getter
    @Setter
    Map<Integer, OrgTrnsConfirmDepResult> confirmDepResultMapByConfirmDepSid;
    /**
     * 新的部門 sid
     */
    @Getter
    @Setter
    private List<Integer> newAssignDepSids = Lists.newArrayList();


    /**
     * 取得新的分派部門 json string
     * 
     * @return
     */
    public String getNewAssignInfoJsonStr() {
        SetupInfoTo setupInfoTo = new SetupInfoTo();
        setupInfoTo.setDepartment(this.newAssignDepSids.stream().map(sid -> sid + "").collect(Collectors.toList()));
        return new SetupInfoToConverter().convertToDatabaseColumn(setupInfoTo);
    }

    public RequireStatusType getRequireStatusType() {
        try {
            return RequireStatusType.valueOf(this.getRequireStatus());
        } catch (Exception e) {
            log.error("錯誤的 RequireStatusType :[" + this.getRequireStatus() + "]", e);
        }
        return null;
    }

}
