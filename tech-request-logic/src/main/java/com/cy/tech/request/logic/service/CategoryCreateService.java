package com.cy.tech.request.logic.service;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.work.common.utils.WkEntityUtils;
import com.cy.tech.request.logic.enumerate.BasicDataCategoryType;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.logic.vo.BasicDataCategoryTo;
import com.cy.tech.request.repository.category.BasicDataBigCategoryRepository;
import com.cy.tech.request.repository.category.BasicDataMiddleCategoryRepository;
import com.cy.tech.request.repository.category.BasicDataSmallCategoryRepository;
import com.cy.tech.request.repository.template.CategoryKeyMappingRepository;
import com.cy.tech.request.vo.constants.CacheConstants;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.exception.UserMessageException;
import com.cy.tech.request.vo.category.BasicDataBigCategory;
import com.cy.tech.request.vo.category.BasicDataMiddleCategory;
import com.cy.tech.request.vo.category.BasicDataSmallCategory;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 類別 建立<BR/>
 * 大類 | 中類 | 小類
 *
 * @author shaun
 */
@Component
@Slf4j
public class CategoryCreateService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4229415443380099004L;
    @Autowired
    private CategoryKeyMappingRepository ckmDao;
    @Autowired
    private BasicDataBigCategoryRepository bigDao;
    @Autowired
    private BasicDataMiddleCategoryRepository middleDao;
    @Autowired
    private BasicDataSmallCategoryRepository basicDataSmallCategoryRepository;
    @Autowired
    private WkEntityUtils entityUtils;
    @Autowired
    private ReqularPattenUtils reqularUtils;

    @Value("${common.UNRECOGNIZED_TYPE}")
    private String unrecognizedType;
    @Autowired
    private CommonService commonService;

    /**
     * 取得原生Entity轉型為前端通用型別
     *
     * @param status
     * @return
     */
    public List<BasicDataCategoryTo> findByStatus(Activation status) {
        @SuppressWarnings("rawtypes")
        List daoEntitys = this.findAllEntityByStatus(status);
        List<BasicDataCategoryTo> result = Lists.newArrayList();
        for (Object obj : daoEntitys) {
            BasicDataCategoryTo to = new BasicDataCategoryTo();
            entityUtils.copyProperties(obj, to);
            to.setType(this.findType(obj));
            to.setParent(this.findParent(obj));
            result.add(to);
        }
        return result;
    }

    /**
     * 取得原生Entity轉型為前端通用型別
     *
     * @param status
     * @param searchText
     * @return
     */
    @Transactional(readOnly = true)
    public BasicDataCategoryTo findByStatusAndFazzyTextTree(Activation status, String searchText) {
        String realSearchText = "%" + reqularUtils.replaceIllegalSqlLikeStr(searchText) + "%";
        List<Activation> searchStatus = this.transStatusToList(status);
        @SuppressWarnings("rawtypes")
        Map<BasicDataCategoryType, List> relationEntityMap = this.searchAndCollationRelationEntity(searchStatus, realSearchText);
        return this.createBasicToRoot(relationEntityMap);
    }

    private List<Activation> transStatusToList(Activation status) {
        if (status == null) {
            return Lists.newArrayList(Activation.values());
        }
        return Lists.newArrayList(status);
    }

    /**
     * 查找並整理全部關聯Entity
     *
     * @param status
     * @param searchText
     * @return
     */
    @SuppressWarnings("rawtypes")
    private Map<BasicDataCategoryType, List> searchAndCollationRelationEntity(
            List<Activation> status, String searchText) {
        List<BasicDataSmallCategory> smallEntitys = basicDataSmallCategoryRepository.findByStatusAndFuzzyText(status, searchText);
        List<BasicDataMiddleCategory> middleEntitys = middleDao.findByStatusAndFuzzyText(status, searchText);
        List<BasicDataBigCategory> bigEntitys = bigDao.findByStatusAndFuzzyText(status, searchText);
        for (BasicDataSmallCategory small : smallEntitys) {
            if (!middleEntitys.contains(small.getParentMiddleCategory())) {
                middleEntitys.add(small.getParentMiddleCategory());
            }
        }
        for (BasicDataMiddleCategory middle : middleEntitys) {
            if (!bigEntitys.contains(middle.getParentBigCategory())) {
                bigEntitys.add(middle.getParentBigCategory());
            }
        }
        Map<BasicDataCategoryType, List> result = Maps.newEnumMap(BasicDataCategoryType.class);
        result.put(BasicDataCategoryType.SMALL, smallEntitys);
        result.put(BasicDataCategoryType.MIDDLE, middleEntitys);
        result.put(BasicDataCategoryType.BIG, bigEntitys);
        return result;
    }

    /**
     * 建立樹狀To物件
     *
     * @param map
     * @return
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private BasicDataCategoryTo createBasicToRoot(Map<BasicDataCategoryType, List> relationEntityMap) {

        List<BasicDataBigCategory> bigEntitys = relationEntityMap.get(BasicDataCategoryType.BIG);
        BasicDataCategoryTo root = new BasicDataCategoryTo();
        for (BasicDataBigCategory big : bigEntitys) {
            BasicDataCategoryTo bigTo = this.createTo(big);
            this.createMiddleToLeaf(relationEntityMap, bigTo);
            bigTo.setParent(root);
        }
        return root;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void createMiddleToLeaf(Map<BasicDataCategoryType, List> relationEntityMap,
            BasicDataCategoryTo parentTo) {
        List<BasicDataMiddleCategory> middleEntitys = relationEntityMap.get(BasicDataCategoryType.MIDDLE);
        for (BasicDataMiddleCategory middle : middleEntitys) {
            if (!middle.getParentBigCategory().getSid().equals(parentTo.getSid())) {
                continue;
            }
            BasicDataCategoryTo middleTo = this.createTo(middle);
            this.createSubToLeaf(relationEntityMap.get(BasicDataCategoryType.SMALL), middleTo);
            middleTo.setParent(parentTo);
        }
    }

    private void createSubToLeaf(List<BasicDataSmallCategory> smallEntitys, BasicDataCategoryTo parentTo) {
        for (BasicDataSmallCategory small : smallEntitys) {
            if (!small.getParentMiddleCategory().getSid().equals(parentTo.getSid())) {
                continue;
            }
            BasicDataCategoryTo smallTo = this.createTo(small);
            smallTo.setParent(parentTo);
        }
    }

    private BasicDataCategoryTo createTo(Object obj) {
        BasicDataCategoryTo to = new BasicDataCategoryTo();
        entityUtils.copyProperties(obj, to);
        to.setType(this.findType(obj));
        to.setParent(this.findParent(obj));
        to.setTip(this.getTip(to));
        return to;
    }

    public BasicDataCategoryType findType(Object obj) {
        for (BasicDataCategoryType each : BasicDataCategoryType.values()) {
            @SuppressWarnings("rawtypes")
            Class clz = each.getClz();
            if (clz.isInstance(obj)) {
                return each;
            }
        }
        throw new IllegalArgumentException(unrecognizedType);
    }

    public BasicDataCategoryTo findParent(Object obj) {
        if (obj instanceof BasicDataMiddleCategory) {
            BasicDataMiddleCategory middle = (BasicDataMiddleCategory) obj;
            BasicDataBigCategory parentBaseMapping = middle.getParentBigCategory();
            return this.createTo(parentBaseMapping);
        }
        if (obj instanceof BasicDataSmallCategory) {
            BasicDataSmallCategory small = (BasicDataSmallCategory) obj;
            BasicDataMiddleCategory parentMiddleMapping = small.getParentMiddleCategory();
            return this.createTo(parentMiddleMapping);
        }
        return null;
    }

    private String getTip(BasicDataCategoryTo to) {
        String br = "<BR/>";
        StringBuilder sb = new StringBuilder();
        sb.append("代碼:").append(to.getId()).append(br);
        sb.append("名稱:").append(to.getName()).append(br);
        sb.append("屬性:").append(this.getTypeName(to)).append(br);
        sb.append("對應關係:").append(this.getParentName(to)).append(br);
        sb.append("使用狀態:").append(this.getStatusName(to)).append(br);
        sb.append("備註說明:").append(to.getNote());
        return sb.toString();
    }

    private String getTypeName(BasicDataCategoryTo to) {
        return commonService.get(to.getType());
    }

    private String getParentName(BasicDataCategoryTo to) {
        if (to.getParent() == null) {
            return "";
        }
        String typeNmae = commonService.get(to.getParent().getType());
        return typeNmae + "-" + to.getParent().getName();
    }

    private String getStatusName(BasicDataCategoryTo to) {
        return commonService.get(to.getStatus());
    }

    /**
     * 中類 上層 = 類型 小類 上層 = 中類
     *
     * @param <T>
     * @param type
     * @return
     */
    @SuppressWarnings({ "unchecked" })
    @Transactional(readOnly = true)
    public <T> List<T> findMappingEntity(BasicDataCategoryType type) {
        switch (type) {
        case BIG:
            return Lists.newArrayList();
        case MIDDLE:
            return (List<T>) bigDao.findByStatusOrderByIdAsc(Activation.ACTIVE);
        case SMALL:
            return (List<T>) middleDao.findByStatusOrderByIdAsc(Activation.ACTIVE);
        default:
            throw new IllegalArgumentException(unrecognizedType + type);
        }
    }

    /**
     * 使用狀態憐選原生Entity
     *
     * @param <T>
     * @param activeType
     * @return
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Transactional(readOnly = true)
    public <T> List<T> findAllEntityByStatus(Activation activeType) {
        List list = Lists.newArrayList();
        list.addAll(bigDao.findByStatusOrderByIdAsc(activeType));
        list.addAll(middleDao.findByStatusOrderByIdAsc(activeType));
        list.addAll(basicDataSmallCategoryRepository.findByStatusOrderByIdAsc(activeType));
        return list;
    }

    /**
     * 檢查是否有相同ID(新增)
     *
     * @param to
     * @return
     */
    @Transactional(readOnly = true)
    public boolean isExistId(BasicDataCategoryTo to) {
        switch (to.getType()) {
        case BIG:
            return bigDao.isExistId(to.getId());
        case MIDDLE:
            return middleDao.isExistId(to.getId());
        case SMALL:
            return basicDataSmallCategoryRepository.isExistId(to.getId());
        default:
            throw new IllegalArgumentException("不支持的Category型態" + to.getType());
        }
    }

    /**
     * 檢查是否有相同名稱(新增)
     *
     * @param to
     * @return
     */
    @Transactional(readOnly = true)
    public boolean isExistName(BasicDataCategoryTo to) {
        switch (to.getType()) {
        case BIG:
            return bigDao.isExistName(to.getName());
        case MIDDLE:
            return middleDao.isExistName(to.getName());
        case SMALL:
            return basicDataSmallCategoryRepository.isExistName(to.getName());
        default:
            throw new IllegalArgumentException("不支持的Category型態" + to.getType());
        }
    }

    /**
     * 檢查是否有相同ID(編輯)<BR/>
     * 不用檢查自己
     *
     * @param to
     * @return
     */
    @Transactional(readOnly = true)
    public Boolean isExistIdByEdit(BasicDataCategoryTo to) {
        String sid = to.getSid();
        String id = to.getId();
        switch (to.getType()) {
        case BIG:
            return bigDao.isExistIdByEdit(sid, id);
        case MIDDLE:
            return middleDao.isExistIdByEdit(sid, id);
        case SMALL:
            return basicDataSmallCategoryRepository.isExistIdByEdit(sid, id);
        default:
            throw new IllegalArgumentException(unrecognizedType + to.getType());
        }
    }

    /**
     * 檢查是否有相同名稱(編輯)<BR/>
     * 不用檢查自己
     *
     * @param to
     * @return
     */
    @Transactional(readOnly = true)
    public Boolean isExistNameByEdit(BasicDataCategoryTo to) {
        String sid = to.getSid();
        String name = to.getName();
        switch (to.getType()) {
        case BIG:
            return bigDao.isExistNameByEdit(sid, name);
        case MIDDLE:
            return middleDao.isExistNameByEdit(sid, name);
        case SMALL:
            return basicDataSmallCategoryRepository.isExistNameByEdit(sid, name);
        default:
            throw new IllegalArgumentException(unrecognizedType + to.getType());
        }
    }

    /**
     * 是否已有需求單使用該類別<br/>
     * 請注意此檢測應一路往下爬到最底層子類別為止...
     *
     * @param to
     * @return
     */
    @Transactional(readOnly = true)
    public Boolean isAnyDocUse(BasicDataCategoryTo to) {
        return ckmDao.isAnyCategoryRelatedDocBeCreated(to.getSid());
    }

    /**
     * 檢查是否有任何子類別停用
     *
     * @param to
     * @return
     */
    @Transactional(readOnly = true)
    public Boolean isAnyChildIsActive(BasicDataCategoryTo to) {
        switch (to.getType()) {
        case BIG:
            return bigDao.checkChildsByStatus(to.getSid(), Activation.ACTIVE);
        case MIDDLE:
            return middleDao.checkChildsByStatus(to.getSid(), Activation.ACTIVE);
        case SMALL:
            return Boolean.FALSE;
        default:
            throw new IllegalArgumentException(unrecognizedType + to.getType());
        }

    }

    @Transactional(rollbackFor = Exception.class)
    @Caching(evict = {
            @CacheEvict(value = CacheConstants.CACHE_TEMP_FIELD, allEntries = true),
            @CacheEvict(value = CacheConstants.CACHE_CATE_KEY_MAPPING, allEntries = true),
            @CacheEvict(value = CacheConstants.CACHE_ALL_CATEGORY_PICKER, allEntries = true),
            @CacheEvict(value = CacheConstants.CACHE_findActiveBigCategory, allEntries = true)
    })
    public void save(User executor, BasicDataCategoryTo to) throws UserMessageException {
        switch (to.getType()) {
        case BIG:
            this.saveByBig(executor, to);
            break;
        case MIDDLE:
            this.saveByMiddle(executor, to);
            break;
        case SMALL:
            this.saveBySmall(executor, to);
            break;
        default:
            throw new IllegalArgumentException(unrecognizedType + to.getType());
        }
    }

    private void saveByBig(User whoSave, BasicDataCategoryTo to) {
        Date now = new Date();
        BasicDataBigCategory big = new BasicDataBigCategory();
        big.setCreatedDate(now);
        big.setCreatedUser(whoSave);
        if (to.getSid() != null) {
            big = bigDao.findOne(to.getSid());
        }
        entityUtils.copyProperties(to, big);
        big.setUpdatedDate(new Date());
        big.setUpdatedUser(whoSave);
        bigDao.save(big);
    }

    private void saveByMiddle(User whoSave, BasicDataCategoryTo to) {
        Date now = new Date();
        BasicDataMiddleCategory middle = new BasicDataMiddleCategory();
        middle.setCreatedDate(now);
        middle.setCreatedUser(whoSave);
        if (to.getSid() != null) {
            middle = middleDao.findOne(to.getSid());
        }
        entityUtils.copyProperties(to, middle);
        middle.setParentBigCategory(bigDao.findById(to.getParent().getId()));
        middle.setUrgency(UrgencyType.GENERAL);
        middle.setSingleUploadLimited(0);
        middle.setPrototypeUploadLimited(0);
        middle.setUpdatedDate(new Date());
        middle.setUpdatedUser(whoSave);
        middleDao.save(middle);
    }

    /**
     * 更新(新增) 小類
     *
     * @param whoSave
     * @param to
     * @return
     * @throws UserMessageException
     */
    private BasicDataSmallCategory saveBySmall(User whoSave, BasicDataCategoryTo to) throws UserMessageException {
        Date now = new Date();

        BasicDataSmallCategory small = null;
        if (to.getSid() != null) {
            small = this.basicDataSmallCategoryRepository.findBySid(to.getSid());
            if (small == null) {
                String errormessage = WkMessage.NEED_RELOAD + "basic_data_small_category_sid:[" + to.getSid() + "]";
                log.warn(errormessage);
                throw new UserMessageException(errormessage, InfomationLevel.WARN);
            }
            small.setUpdatedDate(new Date());
            small.setUpdatedUser(whoSave);
        } else {
            small = new BasicDataSmallCategory();
            small.setCreatedDate(now);
            small.setCreatedUser(whoSave);
            small.setParentMiddleCategory(middleDao.findById(to.getParent().getId()));
        }

        entityUtils.copyProperties(to, small);

        return this.basicDataSmallCategoryRepository.save(small);
    }

    public BasicDataCategoryTo resetTo(BasicDataCategoryTo to) {
        switch (to.getType()) {
        case BIG:
            BasicDataBigCategory big = bigDao.findOne(to.getSid());
            entityUtils.copyProperties(big, to);
            break;
        case MIDDLE:
            BasicDataMiddleCategory middle = middleDao.findOne(to.getSid());
            entityUtils.copyProperties(middle, to);
            break;
        case SMALL:
            BasicDataSmallCategory small = basicDataSmallCategoryRepository.findOne(to.getSid());
            entityUtils.copyProperties(small, to);
            break;
        }
        return to;
    }

    public BasicDataSmallCategory findSmallCategoryDefaultDueDayBySid(String sid) {
        return basicDataSmallCategoryRepository.findOne(sid);
    }
}
