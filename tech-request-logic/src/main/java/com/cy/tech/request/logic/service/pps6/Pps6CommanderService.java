/**
 *
 */
package com.cy.tech.request.logic.service.pps6;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.bpm.rest.client.BpmOrganizationClient;
import com.cy.bpm.rest.client.TaskClient;
import com.cy.bpm.rest.to.RoleTo;
import com.cy.bpm.rest.vo.ProcessTask;
import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.Role;
import com.cy.commons.vo.User;
import com.cy.commons.vo.special.UserWithRoles;
import com.cy.tech.request.logic.service.pps6.vo.Pps6SignFlowTask;
import com.cy.tech.request.logic.service.pps6.vo.Pps6Task;
import com.cy.tech.request.vo.pps6.Pps6ModelerExecSettingVO;
import com.cy.tech.request.vo.pps6.Pps6ModelerFlowVO;
import com.cy.work.common.cache.WkMenuItemCache;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkRoleCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.cache.WkUserWithRolesCache;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Service
@Slf4j
public class Pps6CommanderService implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4469094191110721034L;
    // ========================================================================
    // 服務區
    // ========================================================================
    @Autowired
    private transient Pps6ModelerService pps6ModelerService;
    @Autowired
    private transient Pps6ModelerFlowService pps6ModelerFlowService;
    @Autowired
    private transient Pps6ModelerBPMService pps6ModelerBPMService;
    @Autowired
    private transient WkUserCache wkUserCache;
    @Autowired
    private transient BpmOrganizationClient organizationClient;
    @Autowired
    private transient TaskClient taskClient;

    // ========================================================================
    // 變數區
    // ========================================================================

    // ========================================================================
    // 方法區
    // ========================================================================

    /**
     * 準備和 user 相關的執行設定資料
     *
     * @param userSid USER SID
     * @return Pps6ModelerExecSettingVO list
     * @throws UserMessageException 資料錯誤時拋出
     */
    public List<Pps6ModelerExecSettingVO> prepareRelationExecSetting(Integer userSid) throws UserMessageException {

        // ====================================
        // 查詢
        // ====================================
        User user = wkUserCache.findBySid(userSid);
        if (user == null) {
            throw new UserMessageException("找不到傳入 user 資料 userSid:[" + userSid + "]", InfomationLevel.WARN);
        }

        // ====================================
        // 查詢全部執行設定資料
        // ====================================
        List<Pps6ModelerExecSettingVO> allExecSettings = this.pps6ModelerService.prepareAllData();

        // ====================================
        // 過濾和 user 相關的執行設定
        // ====================================
        return allExecSettings.stream()
                .filter(vo -> WkStringUtils.notEmpty(vo.getWerpMappingUserIDs()))
                .filter(vo -> vo.getWerpMappingUserIDs().contains(user.getId()))
                .sorted(Comparator.comparing(Pps6ModelerExecSettingVO::getName))
                .collect(Collectors.toList());

    }

    /**
     * @param userSid user sid
     * @return Pps6Task list
     * @throws UserMessageException 傳入資料錯誤時拋出
     */
    public List<Pps6Task> prepareCanSignTask(Integer userSid) throws UserMessageException {

        // ====================================
        // 查詢
        // ====================================
        User user = wkUserCache.findBySid(userSid);
        if (user == null) {
            throw new UserMessageException("找不到傳入 user 資料 userSid:[" + userSid + "]", InfomationLevel.WARN);
        }

        // ====================================
        // 查詢待簽資料
        // ====================================
        List<Pps6Task> pps6Tasks = this.prepareTask(user.getId());

        // ====================================
        // 查詢流程檔
        // ====================================
        // 查詢全部
        List<Pps6ModelerFlowVO> flowVOs = this.pps6ModelerFlowService.findAll();
        // 建立索引
        Map<String, String> flowNameVOMapByFlowId = flowVOs.stream()
                .collect(
                        Collectors.toMap(
                                Pps6ModelerFlowVO::getFlowId,
                                Pps6ModelerFlowVO::getFlowName));

        // ====================================
        // 組裝
        // ====================================
        for (Pps6Task pps6Task : pps6Tasks) {
            pps6Task.setFlowName(flowNameVOMapByFlowId.get(pps6Task.getDefinitionName()));
        }

        return pps6Tasks;
    }

    /**
     * @param userID user ID
     * @return Pps6Task list
     * @throws UserMessageException 處理失敗時拋出
     */
    private List<Pps6Task> prepareTask(String userID) throws UserMessageException {

        // ====================================
        // 查詢待簽
        // ====================================
        List<ProcessTask> processTasks;
        try {
            processTasks = this.taskClient.findTaskByUser(userID);
        } catch (Exception e) {
            log.error("查詢待簽資訊失敗(bpm rest)!" + e.getMessage(), e);
            throw new UserMessageException("查詢待簽資訊失敗(bpm rest)!" + e.getMessage(), InfomationLevel.WARN);
        }

        List<Pps6Task> results = Lists.newArrayList();
        for (ProcessTask processTask : processTasks) {
            Pps6Task pps6Task = new Pps6Task();
            pps6Task.setUserID(processTask.getUserID());
            pps6Task.setRoleID(processTask.getRoleID());
            pps6Task.setRoleName(processTask.getRoleName());
            pps6Task.setInstanceID(processTask.getInstanceID());
            pps6Task.setDefinitionID("");// TODO
            pps6Task.setDefinitionName(processTask.getDefinitionName());
            pps6Task.setDocumentID(processTask.getDocumentID());
            pps6Task.setAgent(processTask.getIsAgent());

            List<String> canSignUserIDs = Lists.newArrayList();

            String errorMessage = "";
            try {
                canSignUserIDs = this.taskClient.findTaskCanSignUserIds(pps6Task.getInstanceID());
            } catch (ProcessRestException e) {
                errorMessage = "查詢可簽人員失敗! InstanceID :[" + pps6Task.getInstanceID() + "] , [" + e.getMessage() + "]";
                log.error(errorMessage, e);
            }

            if (WkStringUtils.isEmpty(canSignUserIDs)) {
                errorMessage = "查詢可簽人員異常, 不應該回傳空值 InstanceID：[" + pps6Task.getInstanceID() + "]";
                log.warn(errorMessage);
            }

            if (WkStringUtils.notEmpty(errorMessage)) {
                pps6Task.setPps6CommanderMemo("<span class='WS1-1-2'>" + errorMessage + "</span>");
                results.add(pps6Task);
                continue;
            }

            // ---------------------------
            // 有可以代理的簽核者
            // ---------------------------
            // 忽略被查詢者本身
            canSignUserIDs.remove(userID);
            if (WkStringUtils.notEmpty(canSignUserIDs)) {
                List<Integer> userSids = canSignUserIDs.stream()
                        .map(currUserID -> {
                            User user = WkUserCache.getInstance().findById(currUserID);
                            if (user != null) {
                                return user.getSid();
                            }
                            return -1;
                        })
                        .collect(Collectors.toList());

                String agentInfo = "<span class='WS1-1-4b'>以下人員可代理簽核(代理人)</span><br/>";
                if (pps6Task.isAgent()) {
                    if (canSignUserIDs.size() == 1) {
                        agentInfo = "<span class='WS1-1-4b'>代理</span><br/>";
                    }
                }
                agentInfo += WkUserUtils.prepareUserNameWithDep(
                        userSids,
                        OrgLevel.THE_PANEL,
                        true,
                        "-",
                        "<br/>",
                        true);
                pps6Task.setPps6CommanderMemo(agentInfo);

            } else {

                // ---------------------------
                // 有可以代理的簽核者
                // ---------------------------
                pps6Task.setPps6CommanderMemo("<span class='WS1-1-2'>注意, 關閉帳號後無人可簽</span>");
            }

            results.add(pps6Task);
        }

        return results;
    }

    /**
     * 準備傳入 user 的 BPM 角資料
     * 
     * @param userSid user SID
     * @return RoleTo list
     * @throws UserMessageException 查詢失敗時拋出
     */
    public List<RoleTo> prepareUserBPMRole(Integer userSid) throws UserMessageException {

        User user = WkUserCache.getInstance().findBySid(userSid);
        if (user == null) {
            return Lists.newArrayList();
        }
        List<RoleTo> userRoles;

        try {
            // 查詢
            userRoles = this.pps6ModelerBPMService.findUserRole(user.getId());
            if (WkStringUtils.isEmpty(userRoles)) {
                return Lists.newArrayList();
            }

            // 以所屬單位、ID排序
            Comparator<RoleTo> comparator = Comparator.comparing(RoleTo::getUnitId);
            comparator = comparator.thenComparing(RoleTo::getId);
            userRoles = userRoles.stream()
                    .sorted(comparator)
                    .collect(Collectors.toList());

            for (RoleTo roleTo : userRoles) {
                // 處理層級樹
                roleTo.setParentRoleId(this.prepareHierarchy(roleTo));

                // BPM所屬單位
                String unitID = roleTo.getUnitId();
                roleTo.setUnitId(unitID + "&nbsp;" + roleTo.getUnitName());
                // 對應cy
                Org orgUnit = WkOrgCache.getInstance().findByIdAndNotCheckEmpty(unitID);
                String werpOrgInfo = "";
                if (orgUnit == null) {
                    werpOrgInfo = "<span class='WS1-1-2b'>對應不到 WERP ORG</span>";
                } else {
                    werpOrgInfo = WkOrgUtils.prepareDepsNameByTreeStyle(
                            orgUnit.getCompany().getId(),
                            Lists.newArrayList(orgUnit.getSid()),
                            10);
                }
                roleTo.setUnitName(werpOrgInfo);
            }

        } catch (Exception e) {
            log.error("查詢使用者BPM角色失敗!" + e.getMessage(), e);
            throw new UserMessageException("查詢使用者BPM角色失敗!" + e.getMessage(), InfomationLevel.WARN);
        }

        return userRoles;
    }

    public String prepareHierarchy(RoleTo roleTo) throws ProcessRestException {

        List<String> roleTos = Lists.newArrayList();
        this.prepareHierarchy_recursion(roleTo, roleTos);

        StringBuilder context = new StringBuilder();
        for (int i = 0; i < roleTos.size(); i++) {
            if (i != 0) {
                context.append("<br/>");
            }

            context.append(WkOrgUtils.prepareDepsNameByTreeStyle_getPreContent(i))
                    .append(roleTos.get(i));

        }

        return context.toString();
    }

    private void prepareHierarchy_recursion(RoleTo roleTo, List<String> roleTos) throws ProcessRestException {

        roleTos.add(0, this.prepareRoleInfoForShow(roleTo));

        if (WkStringUtils.isEmpty(roleTo.getParentRoleId())) {
            return;
        }

        RoleTo parentRoleTo = this.pps6ModelerBPMService.findRoleIno(roleTo.getParentRoleId());
        if (parentRoleTo == null) {
            log.warn("找不到BPM角色資訊!roleID:[{}]", roleTo.getParentRoleId());
            return;
        }

        this.prepareHierarchy_recursion(parentRoleTo, roleTos);

    }

    /**
     * 兜組BPM角色顯示資訊
     * 
     * @param roleTo PM角色資料
     * @return BPM角色顯示資訊
     * @throws ProcessRestException
     */
    private String prepareRoleInfoForShow(RoleTo roleTo) throws ProcessRestException {
        String html = "<span class='font-weight'>" + roleTo.getId() + "</span>"
                + "&nbsp;"
                + "["
                + roleTo.getRoleLevel()
                + "-"
                + roleTo.getName()
                + "]";

        List<String> userIDs = this.organizationClient.findUserFromRole(roleTo.getId());
        List<String> userInfos = Lists.newArrayList();
        for (String userID : userIDs) {
            User user = WkUserCache.getInstance().findByIdAndNotCheckEmpty(userID);
            if (user == null) {
                userInfos.add("ID:[" + userID + "] (對應不到 fusion.user )");
            } else {
                userInfos.add(user.getName() + "&nbsp;(" + user.getId() + ")");
            }
        }

        return "<a href='#' data-toggle='tooltip' title='" + String.join("、", userInfos) + "'>" + html + "</a>";

    }

    /**
     * @param userSid
     * @return
     * @throws UserMessageException
     */
    public String prepareUserManagerDeps(Integer userSid) throws UserMessageException {
        // 查詢所有管理部門
        List<Org> orgs = WkOrgCache.getInstance().findManagerOrgs(userSid);

        if (WkStringUtils.isEmpty(orgs)) {
            return "";
        }

        // 排序
        orgs = WkOrgUtils.sortOrgByOrgTree(orgs);

        List<String> names = Lists.newArrayList();
        for (Org org : orgs) {
            String orgName = WkOrgUtils.prepareBreadcrumbsByDepNameAndMakeup(
                    org.getSid(),
                    OrgLevel.GROUPS,
                    true,
                    "→");

            if (!Activation.ACTIVE.equals(org.getStatus())) {
                orgName = WkHtmlUtils.addStrikethroughStyle(orgName, "停用");
            }

            if (org.getCompany() != null) {
                orgName = "<span class='WS1-1-4b'>" + org.getCompany().getName() + "：</span>"
                        + orgName;
            }

            names.add(orgName);
        }

        return String.join("<br/>", names);
    }

    // ========================================================================
    // 層簽試算
    // ========================================================================

    /**
     * @param userSid
     * @return
     * @throws UserMessageException
     */
    public List<RoleTo> prepareUserOrgSignFlow(Integer userSid) throws UserMessageException {

        RoleTo errorTo = new RoleTo();

        // ====================================
        // 查詢 user 資料
        // ====================================
        User user = WkUserCache.getInstance().findBySid(userSid);
        if (user == null) {
            errorTo.setParentRoleId("未查到 user 資料!");
            return Lists.newArrayList(errorTo);
        }

        if (user.getPrimaryOrg() == null) {
            errorTo.setParentRoleId("使用者" + user.getName() + "(" + user.getSid() + ") 沒有歸屬單位");
            return Lists.newArrayList(errorTo);
        }

        if (user.getPrimaryOrg().getCompanySid() == null) {
            errorTo.setParentRoleId("使用者" + user.getName() + "(" + user.getSid() + ") 單位 (" + user.getPrimaryOrg().getSid() + " )沒有歸屬公司!");
            return Lists.newArrayList(errorTo);
        }

        // ====================================
        // 查詢歸屬公司資料
        // ====================================
        Org comp = WkOrgCache.getInstance().findBySid(user.getPrimaryOrg().getCompanySid());
        if (comp == null) {
            errorTo.setParentRoleId("未查到歸屬公司資料!(orgSid=" + user.getPrimaryOrg().getCompanySid() + ")");
            return Lists.newArrayList(errorTo);
        }

        // ====================================
        // 查詢主要角色
        // ====================================
        String mainBpmRoleID = "";
        try {
            mainBpmRoleID = this.organizationClient.findUserRoleByCompany(user.getId(), comp.getId());
            if (WkStringUtils.isEmpty(mainBpmRoleID)) {
                errorTo.setParentRoleId("找不到使用者主要角色");
                return Lists.newArrayList(errorTo);
            }
        } catch (ProcessRestException e) {
            log.error("查詢使用者BPM主要角色失敗!" + e.getMessage(), e);
            throw new UserMessageException("查詢使用者BPM主要角色失敗!" + e.getMessage(), InfomationLevel.WARN);
        }

        RoleTo mainRoleTo;
        try {
            mainRoleTo = this.organizationClient.findRoleToById(mainBpmRoleID);
            if (mainRoleTo == null) {
                errorTo.setParentRoleId("找不到使用者主要角色資料");
                return Lists.newArrayList(errorTo);
            }
        } catch (ProcessRestException e) {
            log.error("查詢使用者BPM主要角色失敗!" + e.getMessage(), e);
            throw new UserMessageException("查詢使用者BPM主要角色失敗!" + e.getMessage(), InfomationLevel.WARN);
        }

        // ====================================
        // 往上查詢部門
        // ====================================
        List<RoleTo> roleTos = Lists.newArrayList();
        try {
            this.prepareSignFlow_recursion(mainRoleTo, roleTos);
        } catch (ProcessRestException e) {
            log.error("查詢角色階層失敗" + e.getMessage(), e);
            throw new UserMessageException("查詢角色階層失敗!" + e.getMessage(), InfomationLevel.WARN);
        }

        List<Integer> userSids = null;
        for (int i = 0; i < roleTos.size(); i++) {
            RoleTo roleTo = roleTos.get(i);

            List<String> userIDs;
            try {
                userIDs = this.organizationClient.findUserFromRole(roleTo.getId());
            } catch (ProcessRestException e) {
                log.error("查詢角色使用者失敗" + e.getMessage(), e);
                throw new UserMessageException("查詢角色使用者失敗!" + e.getMessage(), InfomationLevel.WARN);
            }

            userSids = Lists.newArrayList();
            for (String currUserID : userIDs) {
                User currUser = WkUserCache.getInstance().findByIdAndNotCheckEmpty(currUserID);
                if (currUser == null) {
                    continue;
                }
                userSids.add(currUser.getSid());
            }

            String userContent = WkUserUtils.prepareUserNameWithDep(
                    userSids,
                    OrgLevel.THE_PANEL,
                    true,
                    "-",
                    "<br/>",
                    true);

            if (i == roleTos.size() - 1 && userSids.size() > 1) {
                userContent = "<span class ='WS1-1-2b'>注意！此簽核節點有多個USER，簽核時會隨一選一個(通常是最上面)</span>" + userContent;
            }

            // 借用欄位來放資訊
            roleTo.setParentRoleId(userContent);

        }

        return roleTos;
    }

    private void prepareSignFlow_recursion(RoleTo roleTo, List<RoleTo> roleTos) throws ProcessRestException {

        roleTos.add(roleTo);
        if (WkStringUtils.isEmpty(roleTo.getParentRoleId())) {
            return;
        }
        RoleTo parentRoleTo = this.pps6ModelerBPMService.findRoleIno(roleTo.getParentRoleId());
        if (parentRoleTo == null) {
            log.warn("找不到BPM角色資訊!roleID:[{}]", roleTo.getParentRoleId());
            return;
        }
        this.prepareSignFlow_recursion(parentRoleTo, roleTos);
    }

    // ========================================================================
    // 水管圖
    // ========================================================================
    public List<Pps6SignFlowTask> prepareFormSignFlow(String instanceID) throws UserMessageException {
        // ====================================
        // 查詢模擬圖
        // ====================================
        List<ProcessTaskBase> processTasks;
        try {
            processTasks = this.taskClient.findSimulationChart(instanceID, "admin");

            log.info(new com.google.gson.GsonBuilder().setPrettyPrinting().create().toJson(processTasks));

        } catch (ProcessRestException e) {
            log.error("查詢水管圖失敗!" + e.getMessage(), e);
            throw new UserMessageException("查詢水管圖失敗!" + e.getMessage(), InfomationLevel.WARN);
        }

        // ====================================
        // 解析資料
        // ====================================
        List<Pps6SignFlowTask> pps6SignFlowTasks = Lists.newArrayList();
        for (ProcessTaskBase processTaskBase : processTasks) {

            Pps6SignFlowTask pps6SignFlowTask = new Pps6SignFlowTask();

            // 是否為代理人
            pps6SignFlowTask.setAgent(processTaskBase.getIsAgent());
            // 是否完成簽核
            pps6SignFlowTask.setCompleted("COMPLETED".equals(processTaskBase.getStatus()));
            // 節點資訊
            pps6SignFlowTask.setNodeInfo(this.prepareFormSignFlow_prepareNodeTaskInfo(processTaskBase.getTaskName()));
            // 節點 user ID
            pps6SignFlowTask.setNodeUserID(processTaskBase.getUserID());
            // 節點 user 資訊
            pps6SignFlowTask.setNodeUserInfo(
                    this.prepareFormSignFlow_prepareNodeUserInfo(
                            processTaskBase.getUserID(),
                            processTaskBase.getUserName(),
                            processTaskBase.getRoleID()));

            pps6SignFlowTask.setNodeRoleID(processTaskBase.getRoleID());

            // 已簽資訊
            if (processTaskBase instanceof ProcessTaskHistory) {

                ProcessTaskHistory taskHistory = (ProcessTaskHistory) processTaskBase;
                // 簽核者資訊
                pps6SignFlowTask.setExecutorUserID(taskHistory.getExecutorID());
                pps6SignFlowTask.setExecutorUserInfo(
                        this.prepareFormSignFlow_prepareNodeUserInfo(
                                taskHistory.getExecutorID(),
                                taskHistory.getExecutorName(),
                                processTaskBase.getRoleID()));

                // 簽核時間
                pps6SignFlowTask.setSignTime(taskHistory.getStartTime());
            }

            pps6SignFlowTasks.add(pps6SignFlowTask);
        }

        return pps6SignFlowTasks;
    }

    /**
     * @param bpmUserID
     * @param bpmUserName
     * @param bpmRoleID
     * @return
     * @throws UserMessageException
     */
    private String prepareFormSignFlow_prepareNodeUserInfo(
            String bpmUserID,
            String bpmUserName,
            String bpmRoleID) throws UserMessageException {

        // ====================================
        //
        // ====================================
        if (WkStringUtils.isEmpty(bpmUserID)) {
            RoleTo roleTo;
            try {
                roleTo = this.organizationClient.findRoleToById(bpmRoleID);
                if (roleTo == null) {
                    throw new UserMessageException("查詢不到BPM角色![" + bpmRoleID + "]", InfomationLevel.WARN);
                }

                List<String> userIDs = this.organizationClient.findUserFromRole(roleTo.getId());

                List<Integer> userSids = Lists.newArrayList();
                for (String currUserID : userIDs) {
                    User currUser = WkUserCache.getInstance().findByIdAndNotCheckEmpty(currUserID);
                    if (currUser == null) {
                        continue;
                    }
                    userSids.add(currUser.getSid());
                }

                String userContent = WkUserUtils.prepareUserNameWithDep(
                        userSids,
                        OrgLevel.THE_PANEL,
                        true,
                        "-",
                        "<br/>",
                        true);

                return "BPM角色:" + roleTo.getName() + "&nbsp;(" + roleTo.getRoleLevel() + ")"
                        + "<br/>"
                        + userContent;

            } catch (ProcessRestException e) {
                log.error("查詢BPM失敗" + e.getMessage(), e);
                throw new UserMessageException("查詢BPM失敗!" + e.getMessage(), InfomationLevel.WARN);
            }

        }

        // ====================================
        // 查詢 user 資料
        // ====================================
        User currUser = WkUserCache.getInstance().findById(bpmUserID);

        // ====================================
        // 組顯示資料
        // ====================================
        // 對應不到 user 資料
        if (currUser == null) {
            return bpmUserName
                    + "(" + WkHtmlUtils.addRedBlodClass("WERP找不到此使用者") + ")";
        }

        // 部門+使用者暱稱字串
        return WkUserUtils.prepareUserNameWithDep(
                currUser.getSid(),
                OrgLevel.THE_PANEL,
                true,
                "-");

    }

    private String prepareFormSignFlow_prepareNodeTaskInfo(String taskName) {
        if (WkStringUtils.isEmpty(taskName)) {
            return "";
        }

        for (String taskPartName : taskName.split("-")) {
            if (WkStringUtils.isEmpty(taskPartName)) {
                continue;
            }
            // 全中文就回傳
            String compareTaskPartName = WkStringUtils.safeTrim(taskPartName).toUpperCase();
            if (!WkStringUtils.isNumberOrCapsEnglish(compareTaskPartName)) {
                return taskPartName;
            }
        }

        // 比對不到
        return taskName;

    }

    // ========================================================================
    // 角色
    // ========================================================================
    public String prepareUserRolesInfo(Integer userSid) {
        UserWithRoles userWithRoles = WkUserWithRolesCache.getInstance().findBySid(userSid);
        if (userWithRoles == null || WkStringUtils.isEmpty(userWithRoles.getRoles())) {
            return "未歸屬於任何角色";
        }

        Comparator<Role> comparator = Comparator.comparing(Role::getStatus, Comparator.reverseOrder());
        comparator = comparator.thenComparing(role -> role.getCompany().getSeqNumber());
        comparator = comparator.thenComparing(role -> role.getGroup().getName());
        comparator = comparator.thenComparing(Role::getName);

        // comparator = comparator.thenComparing(Comparator.comparing(Role::getGroup));

        List<Role> roles = userWithRoles.getRoles().stream()
                .map(sRole -> WkRoleCache.getInstance().findBySid(sRole.getSid()))
                .sorted(comparator)
                .collect(Collectors.toList());

        List<String> rolesDescr = Lists.newArrayList();
        for (Role role : roles) {
            String descr = "";

            if (Activation.INACTIVE.equals(role.getStatus())) {
                descr += "<span color='red'>(停用)</span>";
            }

            if (role.getCompany() != null) {
                descr += role.getCompany().getId();
            }

            if (role.getGroup() != null) {
                descr += "-" + role.getGroup().getName();
            }

            descr += "-" + role.getName();
            if (WkStringUtils.notEmpty(role.getDescription())) {
                descr += "&nbsp;[" + role.getDescription() + "]";
            }

            rolesDescr.add(descr);
        }

        return String.join("<br/>", rolesDescr);
    }

    // ========================================================================
    // 選單
    // ========================================================================
    public String prepareMenuItemsInfo(Integer userSid) {
        return WkMenuItemCache.getInstance().prepareMenuItemContentTreeStyle(userSid);
    }

    // ========================================================================
    // 棄用
    // ========================================================================
    public List<Pps6Task> aa(User user) throws UserMessageException {
        List<Pps6Task> pps6Tasks;
        // 查詢
        try {
            pps6Tasks = pps6ModelerBPMService.getTodo(user.getId());
        } catch (Exception e) {
            if (e.getMessage().contains("Internal Server Error")) {
                throw new UserMessageException("BPM查詢失敗! BPM是否沒有對應資料? userID:[" + user.getId() + "]", InfomationLevel.WARN);
            }

            log.error("BPM通訊失敗!" + e.getMessage(), e);
            throw new UserMessageException("BPM通訊失敗!" + e.getMessage(), InfomationLevel.WARN);
        }

        if (WkStringUtils.isEmpty(pps6Tasks)) {
            return Lists.newArrayList();
        }

        return pps6Tasks;
    }

}
