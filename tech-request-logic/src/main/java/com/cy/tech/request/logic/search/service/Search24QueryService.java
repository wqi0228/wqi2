package com.cy.tech.request.logic.search.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.search.view.Search24View;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

/**
 * @author kasim
 */
@Service
public class Search24QueryService implements QueryService<Search24View>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -152163684922746205L;
    @Autowired
    private SearchService searchHelper;
    @Autowired
    private URLService urlService;
    @PersistenceContext
    transient private EntityManager em;

    @Override
    public List<Search24View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {

        // ====================================
        // 查詢
        // ====================================

        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        rawParameters.forEach(each -> {
            query.setParameter(each.getKey(), each.getValue());
        });
        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        @SuppressWarnings("unchecked")
        List<Object[]> result = query.getResultList();
        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if (WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();
        List<Search24View> viewResult = Lists.newArrayList();
        for (int i = 0; i < result.size(); i++) {

            Search24View v = new Search24View();

            int index = 0;

            Object[] record = result.get(i);
            String sid = (String) record[index++];
            String requireNo = (String) record[index++];
            Integer customer = (Integer) record[index++];
            Integer author = (Integer) record[index++];
            Date createdDate = (Date) record[index++];
            Integer createDep = (Integer) record[index++];
            Integer createdUser = (Integer) record[index++];
            String requireStatus = (String) record[index++];
            Integer inteStaffUser = (Integer) record[index++];
            String readReason = (String) record[index++];
            Date hopeDate = (Date) record[index++];
            Date updatedDate = (Date) record[index++];
            Integer urgency = (Integer) record[index++];
            String bigName = (String) record[index++];
            String middleName = (String) record[index++];
            String smallName = (String) record[index++];
            String requireTheme = (String) record[index++];
            Date issueCreatedDate = (Date) record[index++];
            String issueTheme = (String) record[index++];
            String issueNo = (String) record[index++];
            Integer issueDispatchStatus = (Integer) record[index++];
            String issueStatus = (String) record[index++];
            Date issueExpectedDate = (Date) record[index++];
            Integer issueResponsible = (Integer) record[index++];

            // 處理共通欄位 (一定要擺在『取值』最後面, 否則 index 會不對)
            v.prepareCommonColumn(record, index);

            v.setSid(sid);
            v.setRequireNo(requireNo);
            if (customer != null) {
                v.setCustomer(customer.longValue());
            }
            if (author != null) {
                v.setAuthor(author.longValue());
            }
            v.setCreatedDate(createdDate);
            v.setCreateDep(createDep);
            v.setCreatedUser(createdUser);
            v.setRequireStatus(RequireStatusType.valueOf(requireStatus));
            v.setInteStaffUser(inteStaffUser);
            v.setReadReason(Strings.isNullOrEmpty(readReason) ? ReqToBeReadType.NO_TO_BE_READ : ReqToBeReadType.valueOf(readReason));
            v.setHopeDate(hopeDate);
            v.setUpdatedDate(updatedDate);
            v.setUrgency(UrgencyType.values()[urgency]);

            v.setBigName(bigName);
            v.setMiddleName(middleName);
            v.setSmallName(smallName);

            v.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));

            v.setIssueCreatedDate(issueCreatedDate);
            v.setIssueTheme(issueTheme);
            v.setIssueNo(issueNo);
            v.setIssueDispatchStatus(this.getIssueDispatchStatusName(issueDispatchStatus));
            v.setIssueStatus(this.getIssueStatusName(issueStatus));
            v.setIssueExpectedDate(issueExpectedDate);
            v.setIssueResponsible(issueResponsible);

            v.setLocalUrlLink(urlService.createLoacalURLLink(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(execUserSid, v.getRequireNo(), 1)));
            viewResult.add(v);
        }

        // 解析資料-結束
        usageRecord.parserDataEnd();
        return viewResult;
    }

    /**
     * 轉換對應文字
     *
     * @param issueStatus
     * @return
     */
    public String getIssueStatusName(String issueStatus) {
        if (null != issueStatus) {
            switch (issueStatus) {
            case "PAPER000":
                return "新建檔";
            case "PAPER002":
                return "進行中";
            case "PAPER999":
                return "結案";
            case "PAPER013":
                return "自動結案";
            default:
                break;
            }
        }
        return "";
    }

    /**
     * 轉換對應文字
     *
     * @param issueDispatchStatus
     * @return
     */
    public String getIssueDispatchStatusName(Integer issueDispatchStatus) {
        if (null != issueDispatchStatus) {
            switch (issueDispatchStatus) {
            case 0:
                return "派工";
            case 1:
                return "處理中";
            case 2:
                return "完工退件";
            case 3:
                return "完工";
            default:
                break;
            }
        }
        return "";
    }
}
