/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author saul_chen
 */
@Data
@EqualsAndHashCode(callSuper = false, of = { "ptSid" })
public class Search30View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7564245941475164206L;
    /** vo sid */
    private String ptSid;
    // /** vo sid */
    // private String trSid;
    /** 需求單主題 */
    private String requireTheme;
    /** 需求單號 */
    private String requireNo;
    /** 原型確認-立單日期 */
    private String prototypeCreatedDate;
    /** 核准日 */
    private String approvalDate;
    /** 原型確認-填單單位 */
    private String prototypeCreateDep;
    /** 原型確認-填單人員 */
    private String prototypeCreatedUser;
    /** 需求類別 */
    private String bigName;
    /** 中類 */
    private String middleName;
    /** 小類" */
    private String smallName;
    /** 需求單-需求人員 */
    private String requireCreatedUser;
    /** 預計原型確認完成日 */
    private String estimateFinishDate;
    /** 原型確認-主題 */
    private String ptTheme;

    /** 本地端連結網址 */
    private String localUrlLink;
}
