package com.cy.tech.request.logic.service.report.favorite;

import com.cy.commons.vo.User;
import com.cy.tech.request.repository.report.favorite.HomePageFavoriteRepo;
import com.cy.tech.request.vo.report.favorite.HomePageFavorite;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kasim
 */
@Component
public class HomePageFavoriteService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 438740881858847782L;
    @Autowired
    private HomePageFavoriteRepo homePageRepo;

    /**
     * 抓取使用者快取報表
     *
     * @param createdUser
     * @return
     */
    @Transactional(readOnly = true)
    public HomePageFavorite findByCreatedUser(User createdUser) {
        return homePageRepo.findByCreatedUser(createdUser);
    }

    /**
     * 建立(更新)資料
     *
     * @param obj
     * @param user
     * @param reportTypes
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public HomePageFavorite save(HomePageFavorite obj, User user, List<String> reportTypes) {
        if (obj == null || obj.getSid() == null) {
            obj = new HomePageFavorite();
            obj.setCreatedUser(user);
        }
        obj.getHomePages().setValue(reportTypes);
        obj.setUpdatedDate(new Date());
        return homePageRepo.save(obj);
    }

}
