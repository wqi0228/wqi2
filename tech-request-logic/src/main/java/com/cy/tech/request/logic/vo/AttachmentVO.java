/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.vo;

import java.io.Serializable;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

/**
 * 附件介面物件
 *
 * @author brain0925_liao
 */
public class AttachmentVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5869942357355329720L;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.attSid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AttachmentVO other = (AttachmentVO) obj;
        if (!Objects.equals(this.attSid, other.attSid)) {
            return false;
        }
        return true;
    }

    @Getter
    @Setter
    private String attSid;
    @Getter
    @Setter
    private String attName;
    @Getter
    @Setter
    private String attDesc;
    @Getter
    @Setter
    private String attPath;
    @Getter
    @Setter
    private boolean checked;
    /** Maintain Mode User */
    @Getter
    @Setter
    private boolean editMode = false;
    @Getter
    @Setter
    private boolean showEditBtn = false;
    /** Maintain Mode User */
    @Getter
    @Setter
    private String createTime;
    @Getter
    @Setter
    private String paramCreateTime;
    /** Maintain Mode User */
    @Getter
    @Setter
    private String userInfo;
    @Getter
    @Setter
    private String createUserSId;
    @Getter
    @Setter
    private String showCreateDate;

    public AttachmentVO(String attSid, String attName, String attDesc, String attPath, boolean checked, String createUserSId, String showCreateDate) {
        this.attSid = attSid;
        this.attName = attName;
        this.attDesc = attDesc;
        this.attPath = attPath;
        this.checked = checked;
        this.createUserSId = createUserSId;
        this.showCreateDate = showCreateDate;
    }
}
