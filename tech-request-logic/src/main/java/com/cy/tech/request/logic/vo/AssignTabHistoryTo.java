/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.vo;

import java.io.Serializable;
import java.util.Date;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 分派歷程資訊
 *
 * @author shaun
 */
@NoArgsConstructor
@EqualsAndHashCode(of = {"executorTime", "executorName", "unitSid", "behavior"})
public class AssignTabHistoryTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 590708188237767658L;
    /** 加|減 派時間 */
    @Getter
    @Setter
    private Date executorTime;
    /** 操作者 */
    @Getter
    @Setter
    private String executorName;
    /** 單位 */
    @Getter
    @Setter
    private String unitSid;
    @Getter
    @Setter
    private String unitName;
    /** 單位 sid */
    /** 行為(加|減 派) */
    @Getter
    @Setter
    private String behavior;
    @Getter
    @Setter
    private String rowStyleClz;

    public AssignTabHistoryTo(AssignTabInfoTo asTabTo, String unitSid, String unitName, String behavior, String styleClz) {
        this.setExecutorName(asTabTo.getCreateName());
        this.setExecutorTime(asTabTo.getCreateTime());
        this.setUnitSid(unitSid);
        this.setUnitName(unitName);
        this.setBehavior(behavior);
        this.setRowStyleClz(styleClz);
    }
}
