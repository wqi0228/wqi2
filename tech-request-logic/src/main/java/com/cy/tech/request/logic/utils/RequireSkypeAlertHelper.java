/**
 * 
 */
package com.cy.tech.request.logic.utils;

import java.io.Serializable;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

import com.cy.work.common.exception.alert.TechRequestAlertException;

/**
 * 發送 skype alert
 * 
 * @author allen1214_wu
 */
@Service
public class RequireSkypeAlertHelper implements InitializingBean, Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 2166518793965464646L;

    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static RequireSkypeAlertHelper instance;

    public static RequireSkypeAlertHelper getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        RequireSkypeAlertHelper.instance = this;
    }

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * @param message
     */
    public void sendSkypeAlert(String message) {
        // 僅需要 new 出來即發送
        new TechRequestAlertException(message);
    }

    /**
     * @param message
     */
    public void sendSkypeAlert(String message, String compID) {
        new TechRequestAlertException(message, compID);
    }

}
