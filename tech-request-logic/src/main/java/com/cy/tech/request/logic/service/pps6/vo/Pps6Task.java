package com.cy.tech.request.logic.service.pps6.vo;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@EqualsAndHashCode(of = { "id" })
@NoArgsConstructor
@Getter
@Setter
public class Pps6Task implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3722364722951243318L;
    private String id;
    private String userID;
    private String roleID;
    private String roleName;
    private String instanceID;
    private String definitionID;
    private String definitionName;
    private String documentID;
    private String formName;
    private String flowName;
    /**
     * 是否為代理人
     */
    private boolean agent;
    /**
     * 單據申請者 ID
     */
    private String instanceStarterID;
    /**
     * 單據申請者 名稱
     */
    private String instanceStarterName;

    // ====================================
    // 以下非回傳資料, 畫面顯示用
    // ====================================
    /**
     * 備註1
     */
    private String pps6CommanderMemo;
    /**
     * 單據是否已送出
     */
    private boolean sumbit = true;
}
