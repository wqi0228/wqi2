/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.tech.request.repository.require.FavoriteRepository;
import com.cy.tech.request.vo.require.Favorite;
import com.cy.tech.request.vo.require.Require;
import com.google.common.base.Optional;
import java.io.Serializable;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 收藏夾服務
 *
 * @author shaun
 */
@Component
public class FavoriteService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4566306403633337494L;
    @Autowired
    private FavoriteRepository favoriteDao;

    @Transactional(readOnly = true)
    public Optional<Favorite> findUserFavorite(Require require, User user) {
        Optional<Favorite> result = Optional.fromNullable(favoriteDao.findByRequireAndCreatedUser(require, user));
        return result;
    }

    @Transactional(rollbackFor = Exception.class)
    public void toggle(Require require, User executor, Optional<Favorite> of) {
        if (!of.isPresent()) {
            this.createNewFavorite(require, executor);
            return;
        }
        this.updateFavorite(of.get(), executor);
    }

    @Transactional(readOnly = true)
    public Favorite findBySid(String favoriteSid) {
        return favoriteDao.findOne(favoriteSid);
    }

    private void createNewFavorite(Require require, User executor) {
        Favorite f = this.createEmptyFavorite(require, executor);
        favoriteDao.save(f);
    }

    /**
     * 建立空的最愛
     *
     * @param r
     * @param executor
     * @return
     */
    private Favorite createEmptyFavorite(Require r, User executor) {
        Favorite f = new Favorite();
        f.setCreatedDate(new Date());
        f.setCreatedUser(executor);
        f.setRequire(r);
        f.setRequireNo(r.getRequireNo());
        f.setStatus(Activation.ACTIVE);
        return f;
    }

    /**
     * 更新最愛資料
     *
     * @param fav
     * @param executor
     */
    private void updateFavorite(Favorite fav, User executor) {
        fav.setStatus(fav.getStatus().equals(Activation.ACTIVE) ? Activation.INACTIVE : Activation.ACTIVE);
        fav.setUpdatedDate(new Date());
        fav.setUpdatedUser(executor);
        favoriteDao.save(fav);
    }

}
