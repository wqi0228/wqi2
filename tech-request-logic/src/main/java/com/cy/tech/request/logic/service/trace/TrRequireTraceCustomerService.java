/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.trace;

import com.cy.commons.vo.User;
import com.cy.tech.request.vo.enums.OthSetHistoryBehavior;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.require.trace.TrRequireTrace;
import com.cy.work.common.cache.WkUserCache;

import java.io.Serializable;
import org.springframework.stereotype.Component;

/**
 * 追蹤Service
 *
 * @author brain0925_liao
 */
@Component
public class TrRequireTraceCustomerService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7410706421321703785L;
    /** *
     * 取得刪除其他設定資訊附件追蹤物件
     *
     * @param attSid 附件Sid
     * @param fileName 附件名稱
     * @param createUserSid 附件建立者Sid
     * @param trOs_no 其他設定資訊單號
     * @param loginUserSid 刪除者Sid
     * @param requireSid 需求單Sid
     * @param requireNo 需求單單號
     * @return
     */
    public TrRequireTrace createDeleteTrOsAttachmentTrace(String attSid, String fileName, Integer createUserSid,
            String trOs_no, Integer loginUserSid, String requireSid, String requireNo) {
        User createUser = WkUserCache.getInstance().findBySid(createUserSid);
        User loginUser = WkUserCache.getInstance().findBySid(loginUserSid);
        String content = ""
                + "檔案名稱：【" + fileName + "】\n"
                + "上傳成員：【" + createUser.getName() + "】\n"
                + "\n"
                + "刪除來源：【其它設定資訊 － 單號：" + trOs_no + "】\n"
                + "刪除成員：【" + loginUser.getName() + "】\n"
                + "刪除註記：" + attSid;

        TrRequireTrace tr = new TrRequireTrace();
        tr.setRequireTraceType(RequireTraceType.DELETE_ATTACHMENT);
        tr.setRequire_no(requireNo);
        tr.setRequire_sid(requireSid);
        tr.setRequire_trace_content(content);
        tr.setRequire_trace_content_css(content);
        return tr;
    }

    /**
     * 取得刪除其他設定資訊(回覆,回覆的回覆,完成,取消)附件追蹤物件
     *
     * @param attSid 附件Sid
     * @param fileName 附件名稱
     * @param createUserSid 附件建立者Sid
     * @param trOs_no 其他設定資訊單號
     * @param loginUserSid 刪除者Sid
     * @param requireSid 需求單Sid
     * @param requireNo 需求單單號
     * @param othSetHistoryBehavior 行為
     * @return
     */
    public TrRequireTrace createDeleteTrOsHistoryAttachmentTrace(String attSid, String fileName, Integer createUserSid,
            String trOs_no, Integer loginUserSid, String requireSid, String requireNo, OthSetHistoryBehavior othSetHistoryBehavior) {
        User createUser = WkUserCache.getInstance().findBySid(createUserSid);
        User loginUser = WkUserCache.getInstance().findBySid(loginUserSid);
        String content = ""
                + "檔案名稱：【" + fileName + "】\n"
                + "上傳成員：【" + createUser.getName() + "】\n"
                + "\n"
                + "刪除來源：【其它設定資訊 － 單號：" + trOs_no + " － " + othSetHistoryBehavior.getVal() + "】\n"
                + "刪除成員：【" + loginUser.getName() + "】\n"
                + "刪除註記：" + attSid;
        TrRequireTrace tr = new TrRequireTrace();
        tr.setRequireTraceType(RequireTraceType.DELETE_ATTACHMENT);
        tr.setRequire_no(requireNo);
        tr.setRequire_sid(requireSid);
        tr.setRequire_trace_content(content);
        tr.setRequire_trace_content_css(content);
        return tr;
    }

}
