/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.helper.URLHelper;
import com.cy.tech.request.logic.service.pt.PtBpmService;
import com.cy.tech.request.logic.service.setting.SettingCheckConfirmRightService;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireUnitSignInfo;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.backend.logic.WorkBackendParamService;
import com.cy.work.backend.vo.enums.WkBackendParam;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.customer.vo.enums.CustomerType;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 需求單抬頭按鍵顯示控制
 *
 * @author shaun
 */
@Slf4j
@Component
public class RequireShowService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2226479045321768856L;
    @Autowired
    private transient AssignSendInfoService assignSendInfoService;
    @Autowired
    private BpmService bpmService;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private PtBpmService ptBpmService;
    @Autowired
    private RequireTraceService requireTraceService;
    @Autowired
    private transient WorkBackendParamService workBackendParamService;
    @Autowired
    private transient WorkBackendParamHelper workBackendParamHelper;
    @Autowired
    private transient SettingCheckConfirmRightService settingCheckConfirmRightService;
    @Autowired
    private transient URLHelper urlHelper;
    @Autowired
    private transient RequireCheckItemService requireCheckItemService;

    /**
     * 檢查是否包還在分派設定
     *
     * @param require
     * @param login
     * @return
     */
    public Boolean isContainAssign(Require require, User login) {

        if (require == null || login == null || login.getPrimaryOrg() == null) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "傳入參數有誤！require:[" + require + "]login:[" + login + "]");
            return false;
        }

        // ====================================
        // 登入者主要單位是否為分派單位
        // ====================================
        // 查詢分派單位
        Set<Integer> assignDepSids = this.assignSendInfoService.findAssignDepsByRequireSid(require.getSid());
        if (WkStringUtils.isEmpty(assignDepSids)) {
            return false;
        }

        // 加入分派單位的子單位
        Set<Integer> assignDepWithChildSids = Sets.newHashSet(assignDepSids);
        Set<Integer> childSids = WkOrgCache.getInstance().findAllChildSids(assignDepWithChildSids);
        if (WkStringUtils.notEmpty(childSids)) {
            assignDepWithChildSids.addAll(childSids);
        }

        // 比對
        if (assignDepWithChildSids.contains(login.getPrimaryOrg().getSid())) {
            return true;
        }

        // ====================================
        // 登入者『管理』單位是否為分派單位
        // ====================================
        // 是否含代理部門
        // 取得登入者所管理的單位 （過濾停用）
        List<Org> managerOrgs = WkOrgCache.getInstance().findManagerOrgs(login.getSid()).stream()
                .filter(org -> Activation.ACTIVE.equals(org.getStatus()))
                .collect(Collectors.toList());

        for (Org org : managerOrgs) {
            // 略過停用部門
            if (Activation.INACTIVE.equals(org.getStatus())) {
                continue;
            }

            // 比對是否為分派單位
            if (assignDepWithChildSids.contains(org.getSid())) {
                return true;
            }
        }

        return false;
    }

    /**
     * 顯示分派通知按鈕
     *
     * @param require
     * @param isContainAssigned
     * @param hasExeRole
     * @return
     */
    public Boolean showAssignSendBtn(Require require, Integer loginUserSid) {

        if (require == null || require.getRequireStatus() == null) {
            log.warn("傳入 require 錯誤!");
            return false;
        }

        // 案件不為『需求暫緩』
        if (require.getRequireSuspendedCode()) {
            return false;
        }

        // 案件不為『作廢』
        if (require.getInvalidCode()) {
            return false;
        }

        // 已結案時，此類別可在結案後 on 程式[小類設定]
        if (require.getCloseCode() && require.getWhenCloseExeOnpg()) {
            return true;
        }

        // 未結案時,
        if (!require.getCloseCode()) {
            // 一般使用者製作進度為【進行中】、【需求完成】
            // GM製作進度為【待檢查分派】【進行中】、【需求完成】

            // 檢查員
            if (settingCheckConfirmRightService.hasCanCheckUserRight(loginUserSid)) {
                if (require.getRequireStatus().isCanAssginByGM()) {
                    return true;
                }
            }

            // 一般使用者
            else {
                if (require.getRequireStatus().isCanAssgin()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * 顯示附加檔案鍵
     *
     * @param require
     * @return
     */
    public Boolean showAttachmentBtn(Require require) {
        return Boolean.TRUE;
    }

    /**
     * 是否顯示修客戶改按鈕
     *
     * @param require
     * @param user
     * @param hasExeRole
     * @return
     */
    public Boolean showAuthorBtn(Require require, User user, boolean hasExeRole) {
        // 須滿足１．２．３條件才顯示
        // 1. 需求單狀態需為：待檢查確認/進行中/ 需求暫緩 / 已完成 / 結案 才顯示
        List<RequireStatusType> requireStatusTypes = Lists.newArrayList(RequireStatusType.WAIT_CHECK,
                RequireStatusType.PROCESS, RequireStatusType.SUSPENDED,
                RequireStatusType.COMPLETED, RequireStatusType.CLOSE, RequireStatusType.AUTO_CLOSED);
        if (!requireStatusTypes.contains(require.getRequireStatus())) {
            return Boolean.FALSE;
        }
        // 2. 可以使用此按鈕的角色為：需求單位主管 與 業務單位主管
        List<User> fillDocUnitManagers = orgService.findOrgManagers(orgService.findBySid(require.getCreateDep().getSid()));
        if (!(fillDocUnitManagers.contains(user) || hasExeRole)) {
            return Boolean.FALSE;
        }
        // 3. 小類為 新包網資料提供 不能變更 其他則需為實體廳主才能選是
        if ("新包網資料提供".equals(require.getMapping().getSmallName())) {
            return Boolean.FALSE;
        }
        return !(CustomerType.REAL_CUSTOMER.equals(require.getCustomer().getCustomerType()));
    }

    /**
     * 顯示修改期望完成日按鍵
     *
     * @param require
     * @return
     */
    public Boolean showChangeHopeDateBtn(Require require, User login) {
        try {
            // 需為未結案
            if (require.getCloseCode()) {
                return false;
            }
            // 需求單填單單位成員＋填單單位主管群
            User reqCreate = WkUserCache.getInstance().findBySid(require.getCreatedUser().getSid());
            if (reqCreate.equals(login) || orgService.findOrgManagers(orgService.findBySid(require.getCreateDep().getSid())).contains(login)) {
                return true;
            }
            // 被分派單位成員 ＋被分派單位的主管群
            if (assignSendInfoService.findAllAssignChildDepOfUsers(require).contains(login)) {
                return true;
            }
            // GM判斷條件：來自於WORK_BACKEND_PARAM資料表單中的Config.alert.gm.dep參數
            if (this.workBackendParamHelper.isGM(login.getSid())) {
                return true;
            }
        } catch (Exception e) {
            log.error("showChangeHopeDateBtn ERROR", e);
        }
        return false;
    }

    /**
     * 顯示檢查確認
     * 
     * @param require      需求單 sid
     * @param loginUserSid 登入者 user sid
     * @return
     */
    public Boolean showCheckConfirmBtn(Require require, Integer loginUserSid) {

        // ====================================
        // 單據狀態判斷
        // ====================================
        if (require == null) {
            return false;
        }

        // 必須為『未結案』
        if (require.getCloseCode()) {
            return Boolean.FALSE;
        }

        // 必須為『待檢查確認』
        if (!RequireStatusType.WAIT_CHECK.equals(require.getRequireStatus())) {
            return Boolean.FALSE;
        }

        // ====================================
        // 取得該張單據，此人員可檢查的項目
        // ====================================
        List<RequireCheckItemType> userCanCheckItemTypes = this.requireCheckItemService.findUserCanCheckItemTypeByRequireSid(
                require.getSid(),
                loginUserSid);

        // 有任一項可檢查，即可使用
        return userCanCheckItemTypes.size() > 0;

    }

    /**
     * 顯示結案鍵
     *
     * @param require
     * @param login
     * @return
     */
    public Boolean showCloseBtn(Require require, Integer loginUserSid) {

        // ====================================
        // 防呆
        // ====================================
        if (require == null || loginUserSid == null) {
            return false;
        }

        // ====================================
        // 排除已結案
        // ====================================
        if (require.getCloseCode()) {
            return false;
        }

        // ====================================
        // 狀態需為『需求暫緩』、『已完成』
        // ====================================
        List<RequireStatusType> acceptStatus = Lists.newArrayList(
                RequireStatusType.SUSPENDED,
                RequireStatusType.COMPLETED);

        if (!acceptStatus.contains(require.getRequireStatus())) {
            return false;
        }

        // ====================================
        // 1.需求者
        // ====================================
        User createUser = require.getCreatedUser();
        if (createUser != null && createUser.getSid().equals(loginUserSid)) {
            return Boolean.TRUE;
        }

        // ====================================
        // 2.立案部門向上所有主管
        // ====================================
        Org createDep = require.getCreateDep();
        if (createDep != null) {
            // 查詢直系向上主管
            Set<Integer> directManagerUserSid = WkOrgCache.getInstance().findDirectManagerUserSids(createDep.getSid());
            if (directManagerUserSid.contains(loginUserSid)) {
                return Boolean.TRUE;
            }
        }

        return false;
    }

    /**
     * 顯示『反結案』按鈕
     * 
     * @param require      需求單主檔
     * @param loginUserSid 登入者 sid
     * @return
     */
    public Boolean showRollbackCloseBtn(Require require, Integer loginUserSid) {
        // ====================================
        // 防呆
        // ====================================
        if (require == null || loginUserSid == null) {
            return false;
        }

        // ====================================
        // 單據需為結案狀態
        // ====================================
        if (!require.getCloseCode() || !RequireStatusType.CLOSE.equals(require.getRequireStatus())) {
            return false;
        }

        // ====================================
        // 1.需求者
        // ====================================
        User createUser = require.getCreatedUser();
        if (createUser != null && createUser.getSid().equals(loginUserSid)) {
            return Boolean.TRUE;
        }

        // ====================================
        // 2.立案部門向上所有主管
        // ====================================
        Org createDep = require.getCreateDep();
        if (createDep != null) {
            // 查詢直系向上主管
            Set<Integer> directManagerUserSid = WkOrgCache.getInstance().findDirectManagerUserSids(createDep.getSid());
            if (directManagerUserSid.contains(loginUserSid)) {
                return Boolean.TRUE;
            }
        }

        return false;
    }

    /**
     * 顯示『強制反結案』按鈕
     * 
     * @param require      需求單主檔
     * @param loginUserSid 登入者 sid
     * @return
     */
    public Boolean showForceRollbackCloseBtn(Require require, Integer loginUserSid) {
        // ====================================
        // 防呆
        // ====================================
        if (require == null || loginUserSid == null) {
            return false;
        }

        // ====================================
        // 案件需為結案狀態
        // ====================================
        if (!require.getCloseCode() || !RequireStatusType.CLOSE.equals(require.getRequireStatus())) {
            return false;
        }

        // ====================================
        // 登入者無『反結案』權限 (有反結案權限, 無需使用強制反結案)
        // ====================================
        if (this.showRollbackCloseBtn(require, loginUserSid)) {
            return false;
        }

        // ====================================
        // 1.GM管理者
        // ====================================
        if (workBackendParamHelper.isGMAdmin(loginUserSid)) {
            return Boolean.TRUE;
        }

        // ====================================
        // 2.需求管理者
        // ====================================
        if (workBackendParamHelper.isRequireAdmin(loginUserSid)) {
            return Boolean.TRUE;
        }

        return false;
    }

    /**
     * 是否顯示修改廳主按鈕
     *
     * @param require
     * @param user
     * @param hasExeRole
     * @return
     */
    public Boolean showCustmoerBtn(Require require, User user, boolean hasExeRole) {
        // 須滿足１．２．３條件才顯示
        // 1. 需求單狀態需為：待檢查確認/進行中/ 需求暫緩 / 已完成 / 結案 才顯示
        List<RequireStatusType> requireStatusTypes = Lists.newArrayList(RequireStatusType.WAIT_CHECK,
                RequireStatusType.PROCESS, RequireStatusType.SUSPENDED,
                RequireStatusType.COMPLETED, RequireStatusType.CLOSE, RequireStatusType.AUTO_CLOSED);
        if (!requireStatusTypes.contains(require.getRequireStatus())) {
            return Boolean.FALSE;
        }
        // 2. 小類為 新包網資料提供 才會顯示
        if (!"新包網資料提供".equals(require.getMapping().getSmallName())) {
            return Boolean.FALSE;
        }
        // 3. 可以使用此按鈕的角色為：需求單位主管 與 業務單位主管
        List<User> fillDocUnitManagers = orgService.findOrgManagers(orgService.findBySid(require.getCreateDep().getSid()));
        return fillDocUnitManagers.contains(user) || hasExeRole;
    }

    /**
     * 顯示草稿刪除
     *
     * @param require
     * @param user
     * @return
     */
    public Boolean showDraftDelete(Require require, User user) {
        if (!require.getRequireStatus().equals(RequireStatusType.DRAFT)) {
            return false;
        }
        return require.getCreatedUser().getSid().equals(user.getSid());
    }

    /**
     * 顯示草稿提交
     *
     * @param require
     * @param user
     * @return
     */
    public Boolean showDraftSubmit(Require require, User user) {
        if (!require.getRequireStatus().equals(RequireStatusType.DRAFT)) {
            return false;
        }
        return require.getCreatedUser().getSid().equals(user.getSid());
    }

    /**
     * 顯示編輯鍵
     *
     * @param require
     * @param loginUser    登入者
     * @param isDivisionLv 登入者是否為處級以上主管
     * @return
     */
    public Boolean showEditBtn(Require require, User loginUser) {
        if (require == null) {
            return false;
        }

        // ====================================
        // 1.需為未結案
        // ====================================
        if (require.getCloseCode()) {
            return Boolean.FALSE;
        }

        // ====================================
        // 2.製作進度:草稿, 建單者可編輯
        // ====================================
        Integer createUserSid = require.getCreatedUser().getSid();
        Integer loginSid = loginUser.getSid();
        RequireStatusType rst = require.getRequireStatus();
        if (rst.equals(RequireStatusType.DRAFT) && createUserSid.equals(loginSid)) {
            return true;
        }

        // ====================================
        // 2.製作進度:新建單 - 有單位簽核流程
        // ====================================
        // 取得向上所有單位管理者
        Set<Integer> directParentOrgManagerSid = WkOrgUtils.findOrgAndDirectParentOrgManagerByOrgSid(require.getCreateDep().getSid());

        if (rst.equals(RequireStatusType.NEW_INSTANCE) && require.getHasReqUnitSign()) {
            // 如果有需求單位流程但單位主管已簽核不可編輯，未簽核前再可編輯
            // 但新建檔及再議時主管一樣能編輯
            // -------------------------
            // 找不到 bpm 資料, 直接return false
            // -------------------------
            RequireUnitSignInfo signInfo = require.getReqUnitSign();
            if (signInfo == null) {
                return false;
            }
            // -------------------------
            // BPM 流程狀態為【新建檔】、【再議】時, 建單者和單位主管可編輯
            // -------------------------
            InstanceStatus is = signInfo.getInstanceStatus();
            if (is.equals(InstanceStatus.NEW_INSTANCE) || is.equals(InstanceStatus.RECONSIDERATION)) {
                return createUserSid.equals(loginSid) || directParentOrgManagerSid.contains(loginSid);
            }
            // -------------------------
            // 其他狀態時, 單位主管曾經簽名時可編輯
            // -------------------------
            // 取得水管圖
            List<ProcessTaskBase> tasks = bpmService.findFlowChartByInstanceId(signInfo.getBpmInstanceId(), loginUser.getId());
            String loginId = loginUser.getId();
            // 比對登入者為單位主管, 且不曾簽名
            return !tasks.stream().filter(each -> each instanceof ProcessTaskHistory)
                    .map(each -> (ProcessTaskHistory) each)
                    .anyMatch(hTask -> hTask.getExecutorID().equals(loginId))
                    && directParentOrgManagerSid.contains(loginSid);
        }

        // ====================================
        // 3.製作進度:新建單 - 無單位簽核流程
        // ====================================
        // 建單者、單位主管可編輯
        if (rst.equals(RequireStatusType.NEW_INSTANCE) && !require.getHasReqUnitSign()) {
            return createUserSid.equals(loginSid) || directParentOrgManagerSid.contains(loginSid);
        }

        // ====================================
        // 4.製作進度：退件通知 - 立案單位直系往上主管都可編輯
        // ====================================
        if (rst.equals(RequireStatusType.ROLL_BACK_NOTIFY)) {
            return directParentOrgManagerSid.contains(loginSid);
        }

        // ====================================
        // 4.製作進度：待檢查確認 - 沒有簽核流程- 申請人及其主管可以編輯
        // ====================================
        // 待檢查確認時，如果沒有流程則成員及其主管可以編輯
        if (rst.equals(RequireStatusType.WAIT_CHECK) && !require.getHasReqUnitSign()) {
            return createUserSid.equals(loginSid)
                    || directParentOrgManagerSid.contains(loginSid);
        }
        return Boolean.FALSE;
    }

    /**
     * @param require
     * @param loginUserSid
     * @return
     */
    public Boolean showForceCloseBtn(Require require, Integer loginUserSid) {

        // ====================================
        // 判斷不可執行
        // ====================================
        if (require == null || loginUserSid == null) {
            return false;
        }

        // 原結案鈕可使用時, 強制結案不可使用
        if (this.showCloseBtn(require, loginUserSid)) {
            return false;
        }

        // 不為暫緩或完成, 不可執行
        RequireStatusType requireStatusType = require.getRequireStatus();
        List<RequireStatusType> canUseStatus = Lists.newArrayList(RequireStatusType.SUSPENDED, RequireStatusType.COMPLETED);
        if (!canUseStatus.contains(requireStatusType)) {
            return false;
        }

        // ====================================
        // 判斷『可』執行
        // ====================================
        // 為GM主管可執行 (REQ-1511 由一般 GM 調整為主管)
        if (this.workBackendParamHelper.isGMAdmin(loginUserSid)) {
            return true;
        }

        // 為特殊角色可執行
        if (this.workBackendParamHelper.isRequireAdmin(loginUserSid)) {
            return true;
        }

        return false;

    }

    /**
     * 顯示轉寄鍵
     *
     * @param require
     * @return
     */
    public Boolean showForwardBtn(Require require) {
        return Boolean.TRUE;
    }

    /**
     * 顯示NCS按鈕
     *
     * @param require
     * @param user
     * @return
     */
    public Boolean showNcsBtn(User user) {
        if (user == null || user.getPrimaryOrg() == null) {
            return false;
        }
        String ncsDepCode = workBackendParamService.findTextByKeyword(WkBackendParam.REQ_NCS_CODE);
        if (WkStringUtils.isEmpty(ncsDepCode)) {
            log.error("系統參數未設定:{}", WkBackendParam.REQ_NCS_CODE.getKeyword());
            return false;
        }

        return WkStringUtils.safeTrim(ncsDepCode).equals(user.getPrimaryOrg().getId());
    }

    /**
     * 顯示ON程式確認按鈕
     *
     * @param require
     * @param isContainAssigned 是否在分派單位或成員
     * @return
     */
    public Boolean showOnpgBtn(Require require, Boolean isContainAssigned) {
        if (require == null || !isContainAssigned) {
            return Boolean.FALSE;
        }
        if (require.getCloseCode() && !require.getWhenCloseExeOnpg()) {
            return false;
        }
        RequireStatusType requireStatusType = require.getRequireStatus();
        Boolean hasAssign = require.getHasAssign();
        Boolean isProcess = requireStatusType.equals(RequireStatusType.PROCESS);
        Boolean isCompleted = requireStatusType.equals(RequireStatusType.COMPLETED);
        return (hasAssign && isProcess)
                || (isCompleted)
                || (hasAssign && require.getWhenCloseExeOnpg());
    }

    /**
     * 顯示其它設定資訊按鈕
     *
     * @param require
     * @param isContainAssigned 是否在分派單位或成員
     * @return
     */
    public Boolean showOthSetBtn(Require require, User login, Boolean isContainAssigned) {
        // 結案後不可新增
        if (require.getCloseCode()) {
            return Boolean.FALSE;
        }
        // 未分派不可執行
        if (!require.getHasAssign()) {
            return Boolean.FALSE;
        }

        // 無分派單位權限不可執行
        if (!isContainAssigned) {
            return Boolean.FALSE;
        }

        // 僅進行中、已完成可執行
        return Lists.newArrayList(RequireStatusType.PROCESS, RequireStatusType.COMPLETED)
                .contains(require.getRequireStatus());
    }

    /**
     * 顯示轉PMIS按鈕
     * 
     * @param require
     * @return
     */
    public Boolean showPmisBtn(Require require, boolean hasRole) {

        // 需為PMIS管理者
        if (!hasRole) {
            return false;
        }

        // 單據狀態是否可轉PMIS
        if (require.getRequireStatus() == null || !require.getRequireStatus().isCanTrnsPMIS()) {
            return false;
        }

        // 檢查追蹤筆數需為零 (從未轉過 PMIS)
        Integer transCount = this.requireTraceService.countTraceByRequireAndTraceTypeAndStatus(require, RequireTraceType.TRANS_TO_PMIS);
        if (transCount > 0) {
            return false;
        }

        // 需已經有分派單位
        Set<Integer> assignDepSids = assignSendInfoService.findAssignDepsByRequireSid(require.getSid());
        if (WkStringUtils.isEmpty(assignDepSids)) {
            return false;
        }

        return true;
    }

    /**
     * 顯示原型確認按鈕
     *
     * @param require
     * @param isContainAssigned 使用者是否為分派(子部門、管理部門)單位成員
     * @return
     */
    public Boolean showPrototypeBtn(Require require, Boolean isContainAssigned) {

        // 基本原則：原型確認僅能存在一筆

        // ====================================
        // 必要條件
        // ====================================
        // 防呆
        if (require == null) {
            return Boolean.FALSE;
        }

        // 需為未結案
        if (require.getCloseCode()) {
            return Boolean.FALSE;
        }

        // 登入者需為為被分派(子部門、管理部門)單位成員
        if (!isContainAssigned) {
            return Boolean.FALSE;
        }

        // ====================================
        // 選擇成立條件
        // ====================================
        // 還未產生過原型確認單時
        if (!require.getHasPrototype()) {
            // 有分派單位、製作進度為【進行中】
            if (require.getHasAssign() && RequireStatusType.PROCESS.equals(require.getRequireStatus())) {
                return Boolean.TRUE;
            }
        }

        // 曾產生過原型確認單時，確認是否已標註重做
        if (require.getHasPrototype()) {
            // 原型確認重做
            if (require.getRedoCode()) {
                return Boolean.TRUE;
            }
            // 最後一版流程為【作廢】
            if (ptBpmService.isLastProcessInvaild(require)) {
                return Boolean.TRUE;
            }
        }

        // 需求完成後可再進行原型確認
        if (RequireStatusType.COMPLETED.equals(require.getRequireStatus())) {
            return Boolean.TRUE;
        }

        return Boolean.FALSE;

        /*
         * if (require == null || require.getCloseCode() || !isContainAssigned) { return
         * Boolean.FALSE; } RequireStatusType rst = require.getRequireStatus();
         * Boolean hasPrototype = require.getHasPrototype(); Boolean hasAssign =
         * require.getHasAssign(); Boolean isProcess =
         * rst.equals(RequireStatusType.PROCESS); Boolean isCompleted =
         * rst.equals(RequireStatusType.COMPLETED); Boolean isPrototypeRedo =
         * require.getRedoCode(); if ((!hasPrototype && hasAssign && isProcess &&
         * hasExeRole) || (hasPrototype && isPrototypeRedo && hasExeRole) ||
         * (isCompleted && hasExeRole)) { return Boolean.TRUE; } if (!hasPrototype) {
         * return Boolean.FALSE; } Boolean isLastProcessInvaild =
         * ptBpmService.isLastProcessInvaild(require); return isLastProcessInvaild &&
         * hasExeRole;
         */
    }

    /**
     * 顯示關聯鍵
     *
     * @param require
     * @return
     */
    public Boolean showRelationshipBtn(Require require) {
        return Boolean.TRUE;
    }

    /**
     * 顯示需求資訊補充鍵
     *
     * @param require
     * @return
     */
    public Boolean showRequireAddInfoBtn(Require require) {
        RequireStatusType rst = require.getRequireStatus();
        return !rst.equals(RequireStatusType.NEW_INSTANCE);
    }

    /**
     * 是否顯示『強制需求完成』按鈕
     * 
     * @param require
     * @param login
     * @return
     */
    public Boolean showRequireCompleteBtn(Require require, User login) {

        // ====================================
        // 判斷不可執行
        // ====================================
        if (require == null || login == null) {
            return false;
        }
        // 未進行指派, 不可執行
        if (!require.getHasAssign()) {
            return false;
        }
        // 製作進度非進行中，不可執行
        if (!RequireStatusType.PROCESS.equals(require.getRequireStatus())) {
            return false;
        }

        // ====================================
        // 判斷『可』執行
        // ====================================
        // 為立案者可執行
        if (WkCommonUtils.compareByStr(login.getSid(), require.getCreatedUser().getSid())) {
            return true;
        }

        // 為GM可執行
        if (this.workBackendParamHelper.isGM(login.getSid())) {
            return true;
        }

        // 為特殊角色可執行
        if (this.workBackendParamHelper.isRequireAdmin(login.getSid())) {
            return true;
        }

        // 為主責單位可執行
        if (urlHelper.checkPermissionsByInCharge(require, login.getSid())) {
            return true;
        }

        return false;
    }

    /**
     * 顯示需求完成鍵 (舊規則)
     *
     * @param require
     * @param login
     * @param otherBtnCanClick 當需求製作進度 ＝ 已完成，也要能夠點擊 原型確認 / 送測 / ON程式
     * @return
     */
    @SuppressWarnings("unused")
    private Boolean showRequireCompleteBtn_old(Require require, User login, boolean otherBtnCanClick) {
        if (require.getCloseCode()) {
            return Boolean.FALSE;
        }
        RequireStatusType rst = require.getRequireStatus();
        Boolean hasAssign = require.getHasAssign();
        Boolean isProcess = rst.equals(RequireStatusType.PROCESS);
        Boolean isIncludeAssign = assignSendInfoService.findAllAssignChildDepOfUsers(require).contains(login);
        if (hasAssign && isProcess && isIncludeAssign) {
            return Boolean.TRUE;
        }
        Boolean isCompleted = rst.equals(RequireStatusType.COMPLETED);
        return isCompleted && otherBtnCanClick && isIncludeAssign;
    }

    /**
     * 顯示退件通知鍵
     * 
     * @param require      需求單主檔
     * @param loginUserSid 登入者 sid
     * @return 是否顯示按鈕
     */
    public Boolean showRollBackNotifyBtn(Require require, Integer loginUserSid) {

        if (require == null) {
            return false;
        }

        // 必須為『未結案』
        if (require.getCloseCode()) {
            return Boolean.FALSE;
        }

        // 必須為『待檢查確認』
        if (!RequireStatusType.WAIT_CHECK.equals(require.getRequireStatus())) {
            return Boolean.FALSE;
        }

        // 【退件通知】使用權限
        // 有待檢查項目權限，
        // 且該項目未檢查完成
        List<RequireCheckItemType> canCheckItemType = this.requireCheckItemService.findUserCanCheckItemTypeByRequireSid(
                require.getSid(),
                loginUserSid);

        return canCheckItemType.size() > 0;
    }

    /**
     * 顯示存檔&取消鍵
     *
     * @param require
     * @return
     */
    public Boolean showSaveBtn(Require require) {
        if (require.getCloseCode()) {
            return Boolean.FALSE;
        }
        RequireStatusType rst = require.getRequireStatus();
        return rst.equals(RequireStatusType.DRAFT)
                || rst.equals(RequireStatusType.NEW_INSTANCE)
                || rst.equals(RequireStatusType.WAIT_CHECK)
                || rst.equals(RequireStatusType.ROLL_BACK_NOTIFY);
    }

    /**
     * 顯示送測確認按鈕
     *
     * @param require
     * @param isContainAssigned 是否在分派單位或成員
     * @return
     */
    public Boolean showSendTestBtn(Require require, Boolean isContainAssigned) {
        /*
         * if (require.getCloseCode() || !isContainAssigned) { return Boolean.FALSE; }
         * RequireStatusType rst = require.getRequireStatus(); Boolean hasAssign =
         * require.getHasAssign(); Boolean isProcess =
         * rst.equals(RequireStatusType.PROCESS); Boolean isCompleted =
         * rst.equals(RequireStatusType.COMPLETED); return (hasAssign && hasExeRole &&
         * isProcess) || (isCompleted && hasExeRole);
         */

        // ====================================
        // 必要條件
        // ====================================
        // 防呆
        if (require == null) {
            return Boolean.FALSE;
        }

        // 需為未結案
        if (require.getCloseCode()) {
            return Boolean.FALSE;
        }

        // 登入者需為為被分派(子部門、管理部門)單位成員
        if (!isContainAssigned) {
            return Boolean.FALSE;
        }

        // ====================================
        // 選擇成立條件
        // ====================================
        // 有分派單位、製作進度為【進行中】
        if (require.getHasAssign() && RequireStatusType.PROCESS.equals(require.getRequireStatus())) {
            return Boolean.TRUE;
        }

        // 需求完成後可再進行重新分派
        if (RequireStatusType.COMPLETED.equals(require.getRequireStatus())) {
            return Boolean.TRUE;
        }

        return Boolean.FALSE;

    }

    /**
     * 顯示追蹤鍵
     *
     * @param require
     * @return
     */
    public Boolean showTraceBtn(Require require) {
        if (require.getCloseCode()) {
            return Boolean.FALSE;
        }
        RequireStatusType rst = require.getRequireStatus();
        return rst.equals(RequireStatusType.WAIT_CHECK) || rst.equals(RequireStatusType.PROCESS)
                || rst.equals(RequireStatusType.SUSPENDED) || rst.equals(RequireStatusType.CLOSE) || rst.equals(RequireStatusType.AUTO_CLOSED)
                || rst.equals(RequireStatusType.INVALID);
    }

    /**
     * 顯示轉FB
     *
     * @param require
     * @param userDep
     * @param hasExeRole FB分派執行操作者
     * @return
     */
    public Boolean showTransFogbugz(Require require, User login, boolean hasExeRole) {

        if (require == null || login == null) {
            log.warn("傳入參數有誤！require:[" + require + "]login:[" + login + "]");
            return false;
        }

        // 登入者需有執行角色
        if (!hasExeRole) {
            return false;
        }

        // 結案狀態, 需有『結案後可 on 程式』屬性
        if (require.getCloseCode() && !require.getWhenCloseExeOnpg()) {
            return false;
        }

        // 非結案狀態, 製作進度需為進行中
        if (!require.getCloseCode() && !RequireStatusType.PROCESS.equals(require.getRequireStatus())) {
            return false;
        }

        // 需有分派單位
        if (!require.getHasAssign()) {
            return false;
        }

        // 登入者需為被分派部門
        return this.isContainAssign(require, login);
    }

    /**
     * 顯示緊急度鍵
     * 
     * @param require      需求單主檔
     * @param loginUserSid 登入者 sid
     * @return 是否顯示按鈕
     */
    public Boolean showUrgencyBtn(Require require, Integer loginUserSid) {

        if (require == null) {
            return false;
        }

        // 必須為『未結案』
        if (require.getCloseCode()) {
            return Boolean.FALSE;
        }

        // 必須為『待檢查確認』
        if (!RequireStatusType.WAIT_CHECK.equals(require.getRequireStatus())) {
            return Boolean.FALSE;
        }

        // 查詢單據的檢查項目
        List<RequireCheckItemType> checkItemTypes = this.requireCheckItemService.findCheckItemTypesByRequireSid(
                require.getSid());

        // 需具有『可檢查確認部門/人員』身份
        return settingCheckConfirmRightService.isCanCheckUser(loginUserSid, checkItemTypes);
    }
}
