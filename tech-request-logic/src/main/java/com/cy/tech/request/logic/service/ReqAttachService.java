/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.repository.require.ReqModifyRepo;
import com.cy.tech.request.repository.require.RequireAttachmentRepository;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireAttachment;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkAttachUtils;
import com.cy.work.common.vo.AttachmentService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 *
 * @author shaun
 */
@Component("require_attach")
public class ReqAttachService implements AttachmentService<RequireAttachment, Require, String>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7525953194751097835L;
    @Value("${erp.attachment.root}")
    private String attachPath;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private UserService userService;
    @Autowired
    private RequireTraceService traceService;
    @Autowired
    private WkAttachUtils attachUtils;
    @Autowired
    private RequireAttachmentRepository attachDao;
    @Autowired
    private ReqModifyRepo reqModifyDao;

    @Transactional(readOnly = true)
    @Override
    public RequireAttachment findBySid(String sid) {
        RequireAttachment a = attachDao.findOne(sid);
        return a;
    }

    @Transactional(readOnly = true)
    @Override
    public List<RequireAttachment> findAttachsByLazy(Require require) {
        try {
            require.getAttachments().size();
        } catch (LazyInitializationException e) {
            //log.debug("RequireAttachment lazy init error :" + e.getMessage());
            require.setAttachments(this.findAttachs(require));
        }
        List<RequireAttachment> atts = require.getAttachments().stream()
                .sorted((v1, v2) -> v2.getCreatedDate().compareTo(v1.getCreatedDate()))
                .collect(Collectors.toList());
        require.setAttachments(atts);
        return require.getAttachments();
    }

    @Transactional(readOnly = true)
    @Override
    public List<RequireAttachment> findAttachs(Require require) {
        if (Strings.isNullOrEmpty(require.getSid())) {
            if (require.getAttachments() == null) {
                require.setAttachments(Lists.newArrayList());
            }
            return require.getAttachments();
        }
        require.setAttachments(attachDao.findByRequireAndStatus(require, Activation.ACTIVE));
        return require.getAttachments();
    }

    @Override
    public String getAttachPath() {
        return attachPath;
    }

    @Override
    public String getDirectoryName() {
        return "tech-request";
    }

    @Override
    public RequireAttachment createEmptyAttachment(String fileName, User executor, Org dep) {
        return (RequireAttachment) attachUtils.createEmptyAttachment(new RequireAttachment(), fileName, executor, dep);
    }

    /**
     * 存檔
     *
     * @param attach
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public RequireAttachment updateAttach(RequireAttachment attach) {
        RequireAttachment a = attachDao.save(attach);
        return a;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveAttach(RequireAttachment attach) {
        attachDao.save(attach);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveAttach(List<RequireAttachment> attachs) {
        attachDao.save(attachs);
    }

    /**
     * 讀取附件上傳者及部門
     *
     * @param attachment
     * @return
     */
    @Override
    public String depAndUserName(RequireAttachment attachment) {
        if (attachment == null) {
            return "";
        }
        User usr = WkUserCache.getInstance().findBySid(attachment.getCreatedUser().getSid());
        Org dep = orgService.findBySid(usr.getPrimaryOrg().getSid());
        String parentDepStr = orgService.showParentDep(dep);
        return parentDepStr + "-" + usr.getName();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void linkRelation(List<RequireAttachment> attachments, Require require, User login) {
    	require.setHasAttachment(!attachments.isEmpty());
        attachments.forEach(each -> this.setRelation(each, require));
        reqModifyDao.save(require);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void linkRelation(RequireAttachment ra, Require require, User login) {
    	Require entity = reqModifyDao.findOne(require.getSid());
    	entity.setHasAttachment(Boolean.TRUE);
        this.setRelation(ra, entity);
        entity.getAttachments().add(ra);
        
        entity = reqModifyDao.save(entity);
        require.setHasAttachment(entity.getHasAttachment());
        
    }

    private void setRelation(RequireAttachment ra, Require require) {
        ra.setRequire(require);
        ra.setRequireNo(require.getRequireNo());
        //if (!ra.getKeyChecked()) {
        //    ra.setStatus(Activation.INACTIVE);
        //}
        ra.setKeyChecked(Boolean.FALSE);
    }

    /**
     * 刪除
     *
     * @param attachments
     * @param attachment
     * @param require
     * @param login
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void changeStatusToInActive(List<RequireAttachment> attachments, RequireAttachment attachment, Require require, User login) {
    	Require entity = reqModifyDao.findOne(require.getSid());
    	entity.setHasAttachment(!attachments.isEmpty());
        if (entity.getRequireStatus().equals(RequireStatusType.DRAFT)) {
        	entity.setHasTrace(Boolean.FALSE);
        } else {
        	entity.setHasTrace(Boolean.TRUE);
        }
        for(RequireAttachment ra : entity.getAttachments()){
        	if(ra.getSid().equals(attachment.getSid())){
        		ra.setStatus(Activation.INACTIVE);
        		break;
        	}
        }
        this.createDeleteTrace(attachment, require, login);
        entity = reqModifyDao.save(entity);
        require.setHasAttachment(entity.getHasAttachment());
        
    }

    private void createDeleteTrace(RequireAttachment attachment, Require require, User login) {
        //草稿時期應不用留下追蹤記錄 by shaun
        if (require.getRequireStatus().equals(RequireStatusType.DRAFT)) {
            return;
        }
        String content = ""
                + "檔案名稱：【" + attachment.getFileName() + "】\n"
                + "上傳成員：【" + userService.getUserName(attachment.getCreatedUser()) + "】\n"
                + "\n"
                + "刪除來源：【需求單附加檔案】\n"
                + "刪除成員：【" + login.getName() + "】\n"
                + "刪除註記：" + attachment.getSid();
        traceService.createRequireTrace(require.getSid(), login, RequireTraceType.DELETE_ATTACHMENT, content);
    }

}
