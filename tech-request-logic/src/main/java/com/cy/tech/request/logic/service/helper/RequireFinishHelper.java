package com.cy.tech.request.logic.service.helper;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.BpmService;
import com.cy.tech.request.logic.service.onpg.OnpgService;
import com.cy.tech.request.logic.service.othset.OthSetService;
import com.cy.tech.request.logic.service.pt.PtBpmService;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepService;
import com.cy.tech.request.logic.service.send.test.SendTestBpmService;
import com.cy.tech.request.logic.service.send.test.SendTestService;
import com.cy.tech.request.repository.pt.PtCheckRepo;
import com.cy.tech.request.vo.enums.ReqConfirmDepCompleteType;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireConfirmDep;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

/**
 * 需求結束服務
 *
 * @author shaun
 */
@Component
public class RequireFinishHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5350855358442529756L;
    @Autowired
    private BpmService bpmService;
    @Autowired
    private PtBpmService ptBpmService;
    @Autowired
    private SendTestBpmService stBpmService;
    @Autowired
    private SendTestService stService;
    @Autowired
    private OnpgService onpgService;
    @Autowired
    private OthSetService othSetService;
    @Autowired
    private PtCheckRepo pcDao;
    @Autowired
    private RequireConfirmDepService requireConfirmDepService;


    /**
     * 原型確認是否已完成
     * 
     * @param require
     * @return
     */
    public boolean isPassPrototype(Require require) {

        if (require.getHasPrototype()) {
            // 查詢最後一筆單據狀態
            PtStatus lastStatus = PtStatus.valueOf(
                    pcDao.findStatusBySourceDescPtNo(
                            WorkSourceType.TECH_REQUEST.name(),
                            require.getRequireNo()));

            if (lastStatus == null) {
                return false;
            }

            return lastStatus.isInFinish();
        }
        return true;
    }

    /**
     * 建立需求完成警告<BR/>
     * 流程未簽核部份
     *
     * @param require
     * @return
     */
    public String createCompleteWaringMsg(Require require) {
        StringBuilder sb = new StringBuilder();
        if (require.getHasPrototype()) {
            InstanceStatus ptIs = ptBpmService.findSigninfoStatusByRequire(require);
            if (ptIs != null && bpmService.isProcessingStatus(ptIs)) {
                sb.append("原型確認<FONT COLOR='red'>尚未</FONT>簽核完畢，無法進行需求完成。<BR/>");
            } else {
                // WkCommonUtils.logWithStackTrace(LogLevel.WARN, "此需求單無原型確認流程:" +
                // require.getRequireNo());
            }
        }
        if (require.getHasTestInfo()) {
            int stProcessCnt = stBpmService.findProcessNotCompleteCount(require.getRequireNo());
            if (stProcessCnt > 0) {
                sb.append("送測有").append(stProcessCnt).append("筆<FONT COLOR='red'>尚未</FONT>簽核完畢，無法進行需求完成。<BR/>");
            }
        }
        return sb.toString();
    }

    /**
     * 檢核未簽核
     * 
     * @param require
     * @param confirmDepdepSid
     * @param reqConfirmDepCompleteType
     * @return
     */
    public String validateSignNotCompleteByConfirmDep(
            Require require,
            Integer confirmDepdepSid,
            ReqConfirmDepCompleteType reqConfirmDepCompleteType) {

        // ====================================
        // 需求確認單位, 可能由哪些分派部門組成
        // ====================================
        List<Integer> depSids = this.requireConfirmDepService.prepareReqConfirmDepRelationDep(confirmDepdepSid).stream()
                .map(Org::getSid)
                .collect(Collectors.toList());

        // ====================================
        // 原型確認單
        // ====================================
        StringBuilder sb = new StringBuilder();
        if (require.getHasPrototype()) {
            InstanceStatus ptIs = ptBpmService.queryLastPaperCodeBySourceNoAndDepSid(require.getRequireNo(), depSids);
            if (ptIs != null && bpmService.isProcessingStatus(ptIs)) {
                sb.append("原型確認<font color='red'>尚未</FONT>簽核完畢，無法進行『" + reqConfirmDepCompleteType.getLabel() + "』<br/>");
            }
        }

        // ====================================
        // 送測
        // ====================================
        if (require.getHasTestInfo()) {
            int stProcessCnt = this.stBpmService.queryCountByDepSid(require.getRequireNo(), depSids);
            if (stProcessCnt > 0) {
                sb.append("尚有").append(stProcessCnt).append("筆送測單<font color='red'>尚未</FONT>簽核完畢，無法進行『" + reqConfirmDepCompleteType.getLabel() + "』");
            }
        }

        return sb.toString();
    }

    /**
     * 兜組：強制需求完成『確認』訊息
     * 
     * @param require
     * @return
     */
    public String prepareForceCompleteConfirmMesssage(Require require) {

        List<String> messageLine = Lists.newArrayList();

        boolean hasSubCase = false;
        // ====================================
        // 原型確認單
        // ====================================
        if (require.getHasPrototype()) {
            messageLine.add(this.titleStyle("原型確認"));
            if (this.isPassPrototype(require)) {
                messageLine.add("&nbsp;&nbsp;原型確認功能符合需求！");
            } else {
                messageLine.add(colorStr("&nbsp;&nbsp;【原型確認】尚未完成，將自動關閉！", "red"));
            }
            messageLine.add("");
            hasSubCase = true;
        }
        // ====================================
        // 送測單
        // ====================================
        if (require.getHasTestInfo()) {
            messageLine.add(this.titleStyle("送測"));
            // 一般使用者測試
            Integer stCompleteCnt = stService.findConutByStatus(require.getRequireNo(), WorkTestStatus.TEST_COMPLETE);
            if (stCompleteCnt > 0) {
                messageLine.add("&nbsp;&nbsp;" + colorStr(stCompleteCnt, "blue") + " 筆送測狀態為：測試完成");
            }
            // QA測試
            Integer stQaCompleteCnt = stService.findConutByStatus(require.getRequireNo(), WorkTestStatus.QA_TEST_COMPLETE);
            if (stQaCompleteCnt > 0) {
                messageLine.add("&nbsp;&nbsp;" + colorStr(stCompleteCnt, "blue") + " 筆送測狀態為：QA測試完成");
            }
            // 未完成
            Integer incompleteCnt = this.stService.countByCompleteStatus(require.getRequireNo(), null, false);

            if (incompleteCnt > 0) {
                messageLine.add("&nbsp;&nbsp;"
                        + colorStr(incompleteCnt + "筆", "red")
                        + " 送測尚未完成"
                        + colorStr("【不會】", "red")
                        + "自動關閉！");
            }
            messageLine.add("");
            hasSubCase = true;
        }
        // ====================================
        // ONPG
        // ====================================
        if (require.getHasOnpg()) {
            messageLine.add(this.titleStyle("ONPG"));
            Integer opCompleteCnt = this.onpgService.countByCompleteStatus(require.getRequireNo(), null, true);
            if (opCompleteCnt > 0) {
                messageLine.add("&nbsp;&nbsp;" + colorStr(opCompleteCnt, "blue") + " 筆ON程式處理完成");
            }
            Integer incompleteCnt = this.onpgService.countByCompleteStatus(require.getRequireNo(), null, false);
            if (incompleteCnt > 0) {
                messageLine.add("&nbsp;&nbsp;" + colorStr(incompleteCnt + "筆【ON程式】尚未完成，將自動關閉！", "red"));
            }
            messageLine.add("");
            hasSubCase = true;
        }

        // ====================================
        // 其它設定資訊
        // ====================================
        if (require.getHasOthSet()) {
            messageLine.add(this.titleStyle("其它設定資訊"));
            Integer opCompleteCnt = this.othSetService.countByCompleteStatus(require.getRequireNo(), null, true);
            if (opCompleteCnt > 0) {
                messageLine.add("&nbsp;&nbsp;" + colorStr(opCompleteCnt, "blue") + " 其它設定資訊處理完成");
            }
            Integer incompleteCnt = this.othSetService.countByCompleteStatus(require.getRequireNo(), null, false);
            if (incompleteCnt > 0) {
                messageLine.add("&nbsp;&nbsp;" + colorStr(incompleteCnt + "筆【其它設定資訊】尚未完成，將自動關閉！", "red"));
            }
            messageLine.add("");
            hasSubCase = true;
        }

        // ====================================
        // 無開立子單
        // ====================================
        if (!hasSubCase) {
            messageLine.add(this.titleStyle("子單狀況"));
            messageLine.add("&nbsp;&nbsp;" + colorStr("此需求尚未有任何子程序，請確認需求是否已完成？", "blue"));
            messageLine.add("");
        }

        // ====================================
        // 未完成確認單位
        // ====================================
        messageLine.add(this.titleStyle("分派單位確認狀況"));
        // 查詢已完成確認的單位
        int inCompleteDeps = this.requireConfirmDepService.countByCompleteStatus(require.getSid(), true);
        if (inCompleteDeps > 0) {
            messageLine.add("&nbsp;&nbsp;共 " + colorStr(inCompleteDeps, "blue") + "個分派單位完成確認");
        }
        // 查詢未完成確認的單位
        int unCompleteDeps = this.requireConfirmDepService.countByCompleteStatus(require.getSid(), false);
        if (unCompleteDeps > 0) {
            messageLine.add("&nbsp;&nbsp;" + colorStr("尚有" + unCompleteDeps + "個單位尚未完成確認，將異動為無須確認！", "red"));
        }
        messageLine.add("");

        // ====================================
        // return
        // ====================================
        // 加上折行
        return String.join("<br/>", messageLine);
    }

    /**
     * 兜組：強制需求完成『追蹤』訊息
     * 
     * @param require
     * @return
     */
    public String prepareForceCompleteTraceMesssage(Require require, User executor, Date sysDate) {

        List<String> messageLine = Lists.newArrayList();
        messageLine.add("");
        messageLine.add(executor.getName() + " 於 " + WkDateUtils.formatDate(sysDate, WkDateUtils.YYYY_MM_DD_HH24_MI) + "執行強制需求完成!");
        messageLine.add("");

        boolean hasNotFinish = false;
        // ====================================
        // 原型確認單
        // ====================================
        if (require.getHasPrototype()) {
            messageLine.add(this.titleStyle("原型確認"));
            if (this.isPassPrototype(require)) {
                messageLine.add("&nbsp;&nbsp;原型確認功能符合需求！");
            } else {
                hasNotFinish = true;
                messageLine.add("&nbsp;&nbsp;原型確認尚未完成，已"
                        + colorStr("【自動關閉】", "red")
                        + "！");
                messageLine.add(colorStr("&nbsp;&nbsp;原型確認尚未完成，已自動關閉！", "red"));
            }
            messageLine.add("");
        }
        // ====================================
        // 送測單
        // ====================================
        // 送測單不會自動關閉
        if (require.getHasTestInfo()) {
            messageLine.add(this.titleStyle("送測"));
            // 一般使用者測試
            Integer stCompleteCnt = stService.findConutByStatus(require.getRequireNo(), WorkTestStatus.TEST_COMPLETE);
            if (stCompleteCnt > 0) {
                messageLine.add("&nbsp;&nbsp;" + colorStr(stCompleteCnt, "blue") + " 筆送測狀態為：測試完成");
            }
            // QA測試
            Integer stQaCompleteCnt = stService.findConutByStatus(require.getRequireNo(), WorkTestStatus.QA_TEST_COMPLETE);
            if (stQaCompleteCnt > 0) {
                messageLine.add("&nbsp;&nbsp;" + colorStr(stCompleteCnt, "blue") + " 筆送測狀態為：QA測試完成");
            }
            // 所有未完成
            Integer incompleteCnt = this.onpgService.countByCompleteStatus(require.getRequireNo(), null, false);
            if (incompleteCnt > 0) {
                hasNotFinish = true;
                messageLine.add("&nbsp;&nbsp;"
                        + colorStr(incompleteCnt + "筆 送測尚未完成!", "red"));
            }
            messageLine.add("");
        }
        // ====================================
        // ONPG
        // ====================================
        if (require.getHasOnpg()) {
            messageLine.add(this.titleStyle("ON程式"));
            // 所有完成筆數
            Integer opCompleteCnt = this.onpgService.countByCompleteStatus(require.getRequireNo(), null, true);
            if (opCompleteCnt > 0) {
                messageLine.add("&nbsp;&nbsp;" + colorStr(opCompleteCnt, "blue") + " 筆ON程式處理完成");
            }
            // 所有未完成筆數
            Integer incompleteCnt = this.onpgService.countByCompleteStatus(require.getRequireNo(), null, false);
            if (incompleteCnt > 0) {
                hasNotFinish = true;
                messageLine.add("&nbsp;&nbsp;"
                        + colorStr(incompleteCnt + "筆", "red")
                        + " ON程式尚未完成，已"
                        + colorStr("【自動關閉】", "red")
                        + "！");

            }
            messageLine.add("");
        }

        // ====================================
        // 其它設定資訊
        // ====================================
        if (require.getHasOthSet()) {
            messageLine.add(this.titleStyle("其它設定資訊"));
            Integer opCompleteCnt = this.othSetService.countByCompleteStatus(require.getRequireNo(), null, true);
            if (opCompleteCnt > 0) {
                messageLine.add("&nbsp;&nbsp;" + colorStr(opCompleteCnt, "blue") + " 其它設定資訊處理完成");
            }
            Integer incompleteCnt = this.othSetService.countByCompleteStatus(require.getRequireNo(), null, false);
            if (incompleteCnt > 0) {
                hasNotFinish = true;
                messageLine.add("&nbsp;&nbsp;"
                        + colorStr(incompleteCnt + "筆 其它設定資訊尚未完成!", "red"));

            }
            messageLine.add("");
        }

        // ====================================
        // 第一行後綴
        // ====================================
        if (messageLine.size() == 1) {
            messageLine.set(1, messageLine.get(1) + "，且無子程序。");
        }
        if (hasNotFinish) {
            messageLine.set(1, messageLine.get(1) + "，處理狀況如下：");
        } else {
            messageLine.set(1, messageLine.get(1).replaceAll("強制", "") + "，且所有的子程序皆為最終狀態。");
        }

        // ====================================
        // 未完成確認單位
        // ====================================
        messageLine.add(this.titleStyle("分派單位確認狀況"));
        // 查詢已完成確認的單位
        int inCompleteDeps = this.requireConfirmDepService.countByCompleteStatus(require.getSid(), true);
        if (inCompleteDeps > 0) {
            messageLine.add("&nbsp;&nbsp;共 " + colorStr(inCompleteDeps, "blue") + "個分派單位完成確認");
        }
        // 查詢未完成確認的單位
        int unCompleteDeps = this.requireConfirmDepService.countByCompleteStatus(require.getSid(), false);
        if (unCompleteDeps > 0) {
            messageLine.add("&nbsp;&nbsp;" + colorStr("尚有" + unCompleteDeps + "個單位尚未完成確認，將異動為【無須確認】！", "red"));
        }
        messageLine.add("");

        // ====================================
        // return
        // ====================================
        // 加上折行
        return String.join("<br/>", messageLine);
    }

    /**
     * 兜組：需求完成 追蹤訊息 by 所有分派單位確認完成
     * 
     * @param require
     * @return
     */
    public String prepareRequireCompleteByAllDepConfirmTraceMesssage(Require require) {

        List<String> messageLine = Lists.newArrayList();
        boolean hasSubCase = false;

        messageLine.add("全部分派單位皆已確認完成，執行狀況如下：");
        messageLine.add("");

        // ====================================
        // 需求完成確認單位
        // ====================================
        List<RequireConfirmDep> requireConfirmDeps = this.requireConfirmDepService.findByRequireSid(require.getSid());
        if (WkStringUtils.notEmpty(requireConfirmDeps)) {
            messageLine.add(this.titleStyle("分派單位確認狀況："));
            for (RequireConfirmDep requireConfirmDep : requireConfirmDeps) {
                String depName = "【" + WkOrgCache.getInstance().findNameBySid(requireConfirmDep.getDepSid()) + "】";
                String type = requireConfirmDep.getCompleteType() != null ? requireConfirmDep.getCompleteType().getLabel() : "";
                String dateStr = WkDateUtils.formatDate(requireConfirmDep.getConfirmDate(), WkDateUtils.YYYY_MM_DD_HH24_mm_ss);
                messageLine.add(depName + ": " + type + " (" + dateStr + ")");

            }
            messageLine.add("");
        }

        // ====================================
        // 原型確認單
        // ====================================
        if (require.getHasPrototype()) {
            messageLine.add(this.titleStyle("原型確認"));
            if (this.isPassPrototype(require)) {
                messageLine.add("&nbsp;&nbsp;原型確認功能符合需求！");
            } else {
                // 應該不可能發生
                messageLine.add(colorStr("&nbsp;&nbsp;原型確認尚未完成！", "red"));
                // log.warn("需求完成 by 所有單位確認, 但有原型確認未完成! (需求單位)");
                hasSubCase = true;
            }
            messageLine.add("");
        }
        // ====================================
        // 送測單
        // ====================================
        if (require.getHasTestInfo()) {
            messageLine.add(this.titleStyle("送測"));
            // 一般使用者測試
            Integer stCompleteCnt = stService.findConutByStatus(require.getRequireNo(), WorkTestStatus.TEST_COMPLETE);
            if (stCompleteCnt > 0) {
                messageLine.add("&nbsp;&nbsp;" + colorStr(stCompleteCnt, "blue") + " 筆送測狀態為：測試完成");
            }
            // QA測試
            Integer stQaCompleteCnt = stService.findConutByStatus(require.getRequireNo(), WorkTestStatus.QA_TEST_COMPLETE);
            if (stQaCompleteCnt > 0) {
                messageLine.add("&nbsp;&nbsp;" + colorStr(stCompleteCnt, "blue") + " 筆送測狀態為：QA測試完成");
            }
            // 未完成
            Integer incompleteCnt = this.onpgService.countByCompleteStatus(require.getRequireNo(), null, false);

            if (incompleteCnt > 0) {
                messageLine.add("&nbsp;&nbsp;"
                        + colorStr(incompleteCnt + "筆 送測尚未完成！", "red"));
            }
            messageLine.add("");
            hasSubCase = true;
        }
        // ====================================
        // ONPG
        // ====================================
        if (require.getHasOnpg()) {
            messageLine.add(this.titleStyle("ONPG"));
            Integer opCompleteCnt = this.onpgService.countByCompleteStatus(require.getRequireNo(), null, true);
            if (opCompleteCnt > 0) {
                messageLine.add("&nbsp;&nbsp;" + colorStr(opCompleteCnt, "blue") + " 筆ON程式處理完成");
            }
            Integer incompleteCnt = this.onpgService.countByCompleteStatus(require.getRequireNo(), null, false);
            if (incompleteCnt > 0) {
                messageLine.add("&nbsp;&nbsp;" + colorStr(incompleteCnt + "筆 ON程式尚未完成！", "red"));
            }
            messageLine.add("");
            hasSubCase = true;
        }

        // ====================================
        // 其它設定資訊
        // ====================================
        if (require.getHasOthSet()) {
            messageLine.add(this.titleStyle("其它設定資訊"));
            Integer opCompleteCnt = this.othSetService.countByCompleteStatus(require.getRequireNo(), null, true);
            if (opCompleteCnt > 0) {
                messageLine.add("&nbsp;&nbsp;" + colorStr(opCompleteCnt, "blue") + " 其它設定資訊處理完成");
            }
            Integer incompleteCnt = this.othSetService.countByCompleteStatus(require.getRequireNo(), null, false);
            if (incompleteCnt > 0) {
                messageLine.add("&nbsp;&nbsp;"
                        + colorStr(incompleteCnt + " 筆 其它設定資訊尚未完成！", "red"));
            }
            messageLine.add("");
            hasSubCase = true;
        }

        // ====================================
        // 無開立子單
        // ====================================
        if (!hasSubCase) {
            messageLine.add(this.titleStyle("子單狀況"));
            messageLine.add("&nbsp;&nbsp;" + colorStr("此需求尚未有任何子程序", "blue"));
            messageLine.add("");
        }

        // ====================================
        // return
        // ====================================
        // 加上折行
        return String.join("<br/>", messageLine);
    }

    /**
     * @param msg
     * @param color
     * @return
     */
    private String colorStr(Object msg, String color) {
        return "<span style='color:" + color + ";'>" + msg + "</span>";
    }

    /**
     * 加上標題用 style
     * 
     * @param msg
     * @return
     */
    private String titleStyle(Object msg) {
        return "<span style='font-weight:bold;text-decoration:underline;'>" + msg + "</span>";
    }
}
