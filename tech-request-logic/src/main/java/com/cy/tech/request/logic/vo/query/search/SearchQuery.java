/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.vo.query.search;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.enumerate.ReportType;
import com.cy.tech.request.logic.search.enums.ConditionInChargeMode;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import lombok.Getter;
import lombok.Setter;

/**
 * 查詢(Search)頁面皆要繼承
 *
 * @author kasim
 */
public abstract class SearchQuery implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 285112877785302807L;
    @Getter
	/** 登入單位 */
	protected Org dep;
	@Getter
	@Setter
	/** 登入者 */
	protected User user;
	@Getter
	/** 報表類型 */
	protected ReportType reportType;
	
	
	// ====================================
	// 以下為部分共用欄位 , 不一定每個功能都用到
	// ====================================
	/** 製作進度 */
	@Getter
	@Setter
	protected RequireStatusType selectRequireStatusType;
	/** 待閱原因 */
	@Getter
	@Setter
	protected List<String> selectReqToBeReadType;
	/**
	 * 主責單位查詢模式
	 */
	@Getter
	@Setter
	protected ConditionInChargeMode conditionInChargeMode = ConditionInChargeMode.NO_FILTER;
	
	/**
	 * 不開放選擇主責單位
	 */
	@Getter
    @Setter
    public boolean disableInChargeMode = false;
	
	/**
	 * 主責單位
	 */
	@Getter
	@Setter
	protected List<Integer> inChargeDepSids;
	/**
	 * 主責單位負責人
	 */
	@Getter
	@Setter
	protected List<Integer> inChargeUserSids;
	/**
	 * 系統別 REQ-1456
	 */
	@Getter
	@Setter
	protected List<RequireCheckItemType> checkItemTypes;
	
    /** 主題 REQ-1652 */
    @Getter
    @Setter
    private String theme;

	/**
	 * 共用查詢條件初始化
	 */
	public void publicConditionInit() {
		//製作進度
		this.selectRequireStatusType = null;
				
		//待閱原因
		this.selectReqToBeReadType = Lists.newArrayList(ReqToBeReadType.values()).stream()
				.map(ReqToBeReadType::name)
				.collect(Collectors.toList());
		
		// 主責單位
		this.conditionInChargeMode = ConditionInChargeMode.NO_FILTER;
		this.inChargeDepSids = null;
		this.inChargeUserSids = null;

		// 檢查項目 (系統別)
		this.checkItemTypes = Lists.newArrayList(RequireCheckItemType.values());
		
		//主題
		this.theme = "";
	}

}
