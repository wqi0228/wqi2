package com.cy.tech.request.logic.service.orgtrns.vo;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author allen1214_wu
 */
@NoArgsConstructor
@EqualsAndHashCode(of = "sid")
public class OrgTrnsDtVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7076482213333566669L;

    @Getter
    @Setter
    private String sid;

    /**
     * 單號
     */
    @Getter
    @Setter
    private String caseNo;

    /**
     * 立案日期
     */
    @Getter
    @Setter
    private Date createDate_src;
    @Getter
    @Setter
    private String createDate;

    /**
     * 立案人Sid
     */
    @Getter
    @Setter
    private Integer createUserSid;

    /**
     * 立案人名稱
     */
    @Getter
    @Setter
    private String createUserName;

    /**
     * 立案單位
     */
    @Getter
    @Setter
    private Integer createDepSid;

    /**
     * 立案單位
     */
    @Getter
    @Setter
    private String createDepName;

    /**
     * 案件主題
     */
    @Getter
    @Setter
    private String theme;

    /**
     * 案件狀態
     */
    @Getter
    @Setter
    private String caseStatus_Src;

    /**
     * 案件狀態說明
     */
    @Getter
    @Setter
    private String caseStatusDesc;

    /**
     * 說明
     */
    @Getter
    @Setter
    private String summary;

    /**
     * 主單號碼
     */
    @Getter
    @Setter
    private String masterNo;

    /**
     * 主單號碼
     */
    @Getter
    @Setter
    private String requireSid;

    // ========================================================================
    // 通知
    // ========================================================================
    /**
     * 分派單位 JSON string
     */
    @Getter
    @Setter
    private String noticeInfo_src;

    @Getter
    @Setter
    private Set<Integer> oldNoticeDepSids;

    @Getter
    @Setter
    private Set<Integer> newNoticeDepSids;

    // ========================================================================
    // 分派
    // ========================================================================
    /**
     * 分派單位 JSON string
     */
    @Getter
    @Setter
    private String assignInfo_src;

    /**
     * 分派單位轉檔對應
     */
    @Getter
    @Setter
    private Map<Integer, List<Integer>> assignDepSidsMapping;

    /**
     * 分派單位顯示名稱
     */
    @Getter
    @Setter
    private String assignDepNames;

    // ========================================================================
    // 主責單位
    // ========================================================================
    /**
     * 負責人員
     */
    @Getter
    @Setter
    private Integer inChargeUserSid;

    /**
     * 負責人員名稱
     */
    @Getter
    @Setter
    private String inChargeUserName;

    /**
     * 主責單位
     */
    @Getter
    @Setter
    private Integer inChargeDepSid;

    /**
     * 主責單位名稱
     */
    @Getter
    @Setter
    private String inChargeDepName;

    // ========================================================================
    // 轉寄單位轉檔
    // ========================================================================
    /**
     * tr_alert_inbox.alert_sid
     */
    @Getter
    @Setter
    private String alertSid;

    // ========================================================================
    // 計數
    // ========================================================================

    /**
     * 計數
     */
    @Getter
    @Setter
    private BigInteger cnt;

}
