/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.callable.Search28ViewCallable;
import com.cy.tech.request.logic.search.view.Search28View;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.customer.ReqWorkCustomerHelper;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author jason_h
 */
@Service
@Slf4j
public class Search28QueryService implements QueryService<Search28View>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4019071030087635086L;
    @Autowired
    private URLService urlService;
    @Autowired
    private WkJsonUtils jsonUtils;
    @Autowired
    private SearchService searchHelper;
    @PersistenceContext
    transient private EntityManager em;

    @Autowired
    transient private ReqWorkCustomerHelper customerService;

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public List<Search28View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {

        // ====================================
        // 查詢
        // ====================================

        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        List<Object[]> result = query.getResultList();
        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if (WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();
        List<Search28View> viewResult = Lists.newArrayList();
        List<Search28ViewCallable> search28ViewCallables = Lists.newArrayList();
        for (int i = 0; i < result.size(); i++) {
            Object[] record = (Object[]) result.get(i);

            Search28ViewCallable search28ViewCallable = new Search28ViewCallable(record, execUserSid, urlService,
                    jsonUtils, searchHelper, customerService, i);
            search28ViewCallables.add(search28ViewCallable);
        }
        if (search28ViewCallables.isEmpty()) {
            return viewResult;
        }
        int poolNum = 50;
        if (search28ViewCallables.size() < poolNum) {
            poolNum = search28ViewCallables.size();
        }

        ExecutorService pool = Executors.newFixedThreadPool(poolNum);
        try {
            CompletionService completionPool = new ExecutorCompletionService(pool);
            search28ViewCallables.forEach(item -> {
                completionPool.submit(item);
            });
            IntStream.range(0, search28ViewCallables.size()).forEach(i -> {
                try {
                    Object obj = completionPool.take().get();
                    if (obj == null) {
                        return;
                    }
                    Search28View search28View = (Search28View) obj;
                    viewResult.add(search28View);
                } catch (Exception e) {
                    log.error("search16ViewCallables", e);
                }
            });
        } catch (Exception e) {
            log.error("search16ViewCallables", e);
        } finally {
            pool.shutdown();
        }

        // sort by age
        Collections.sort(viewResult, new Comparator<Search28View>() {
            @Override
            public int compare(Search28View o1, Search28View o2) {
                return o1.getIndex() - o2.getIndex();
            }
        });
        // 解析資料-結束
        usageRecord.parserDataEnd();

        return viewResult;
    }
}
