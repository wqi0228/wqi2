package com.cy.tech.request.logic.service.orgtrns;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.system.rest.client.vo.OrgTransMappingTo;
import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsDtVO;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsWorkVerifyVO;
import com.cy.tech.request.repository.require.TrnsBackupRepository;
import com.cy.tech.request.vo.enums.RequireTransProgramType;
import com.cy.tech.request.vo.enums.TrnsType;
import com.cy.tech.request.vo.require.TrnsBackup;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.Gson;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Slf4j
@NoArgsConstructor
@Service
public class OrgTrnsAlertInboxService {

    // ========================================================================
    // 私有容器
    // ========================================================================
    @Data
    private class OrgTrnsAlertInboxBackupTo implements Serializable {
        private static final long serialVersionUID = -3720712730175134459L;
        private Set<String> trnsAlertSids;
        private Set<String> inactiveAlertSids;
        private Integer beforeDepSid;
        private Integer afterDepSid;
    }

    // ========================================================================
    // 服務元件區
    // ========================================================================
    @Autowired
    private transient OrgTrnsService orgTrnsService;
    @Autowired
    private transient SearchService searchService;
    @Autowired
    private transient NativeSqlRepository nativeSqlRepository;
    @Autowired
    private transient TrnsBackupRepository trnsBackupRepository;
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private transient JdbcTemplate jdbcTemplate;

    // ========================================================================
    // 查詢方法區
    // ========================================================================
    /**
     * @param createDepSid
     * @param trnsType
     * @return
     * @throws UserMessageException
     * @throws SystemDevelopException
     */
    public List<OrgTrnsDtVO> queryByReciveDep(
            Integer reciveDepSid) throws UserMessageException {

        // ====================================
        // 取得部門
        // ====================================
        Org reciveDep = WkOrgCache.getInstance().findBySid(reciveDepSid);
        if (reciveDep == null) {
            throw new UserMessageException("查不到選擇的單位資料 sid:[" + reciveDepSid + "]", InfomationLevel.WARN);
        }

        // ====================================
        // 依據類別查詢
        // ====================================
        return this.query(reciveDepSid);
    }

    /**
     * @param createDepSid
     * @return
     */
    private List<OrgTrnsDtVO> query(Integer reciveDepSid) {

        // ====================================
        // 查詢
        // ====================================
        // SQL
        StringBuffer varname1 = new StringBuffer();
        varname1.append("SELECT tr.require_sid            as sid, ");
        varname1.append("       tr.require_no             as caseNo, ");
        varname1.append("       tid.field_content         as theme, ");
        varname1.append("       tr.create_dt              as createDate_src, ");
        varname1.append("       tr.create_usr             as createUserSid, ");
        varname1.append("       tr.require_status         as caseStatus_Src, ");
        varname1.append("       tr.require_sid            as requireSid, ");
        varname1.append("       inbox.alert_sid           as alertSid "); // 轉寄檔 SID

        varname1.append("FROM   tr_require tr ");

        varname1.append("INNER JOIN (SELECT require_sid, ");
        varname1.append("                   receive_dep,  ");
        varname1.append("                   alert_sid  ");
        varname1.append("            FROM   tr_alert_inbox  ");
        varname1.append("            WHERE  status = 0 ");
        varname1.append("                   AND forward_type = 'FORWARD_DEPT' ");
        varname1.append("                   AND inbox_type = 'INCOME_DEPT' ");
        varname1.append("                   ) AS inbox ");
        varname1.append("        ON inbox.require_sid = tr.require_sid ");

        varname1.append("INNER JOIN (SELECT tid.require_sid, ");
        varname1.append("                   tid.field_content ");
        varname1.append("            FROM   tr_index_dictionary tid ");
        varname1.append("            WHERE  1 = 1 ");
        varname1.append("                   AND tid.field_name = '主題') AS tid ");
        varname1.append("        ON tr.require_sid = tid.require_sid ");

        varname1.append("WHERE inbox.receive_dep = :reciveDepSid ");
        varname1.append("ORDER BY tr.require_no ASC ");

        // 查詢參數
        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put("reciveDepSid", reciveDepSid);

        // 查詢
        List<OrgTrnsDtVO> results = nativeSqlRepository.getResultList(
                varname1.toString(), parameters, OrgTrnsDtVO.class);

        if (WkStringUtils.isEmpty(results)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 準備顯示欄位
        // ====================================
        // 轉共同欄位
        this.orgTrnsService.prepareShowInfo(results, RequireTransProgramType.REQUIRE);

        // 轉私有欄位
        for (OrgTrnsDtVO resultVO : results) {
            // 主題
            if (WkStringUtils.notEmpty(resultVO.getTheme())) {
                resultVO.setTheme(searchService.combineFromJsonStr(resultVO.getTheme()));
            }
        }

        return results;
    }

    // ========================================================================
    // 轉檔方法區
    // ========================================================================
    /**
     * 執行轉檔
     * 
     * @param orgTransMappingTo
     * @param selectedDtVOList
     * @param execUserSid
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int process(
            OrgTransMappingTo orgTransMappingTo,
            List<OrgTrnsDtVO> selectedDtVOList,
            Integer execUserSid) {

        if (WkStringUtils.isEmpty(selectedDtVOList)) {
            return 0;
        }

        Date sysdate = new Date();

        log.info("\r\n開始進行『被轉寄單位』 轉檔 \r\n"
                + "來源單位: " + WkOrgUtils.prepareBreadcrumbsByDepName(orgTransMappingTo.getBeforeOrgSid(), OrgLevel.DIVISION_LEVEL, false, "-") + "\r\n"
                + "轉入單位: " + WkOrgUtils.prepareBreadcrumbsByDepName(orgTransMappingTo.getAfterOrgSid(), OrgLevel.DIVISION_LEVEL, false, "-") + "\r\n"
                + "共" + selectedDtVOList.size() + "筆");

        // ====================================
        // 分類
        // ====================================
        // 當轉換後單位，原本就有被轉寄時 ，無需做資料轉換, 僅停用該筆轉寄 (轉寄資料合併)

        // 收集已選擇項目的 requestSid (distinct)
        Set<String> requestSids = selectedDtVOList.stream()
                .filter(vo -> WkStringUtils.notEmpty(vo.getRequireSid()))
                .map(OrgTrnsDtVO::getRequireSid)
                .collect(Collectors.toSet());

        // 檢查哪些單據的『轉換後單位』已存在
        Set<String> requestSidSetByExsitReceiveDepSid = this.queryRequestSidExsitAlertReceiveDepSid(
                requestSids,
                orgTransMappingTo.getAfterOrgSid());

        // 分類
        // 要轉的資料 sid
        Set<String> alertSidForTrns = Sets.newHashSet();
        // 要停用的資料sid (轉換後單位已有資料，所以需把原本的資料停用, 避免同一個轉寄單位有兩筆)
        Set<String> alertSidForInactive = Sets.newHashSet();
        // 逐筆處理
        for (OrgTrnsDtVO orgTrnsDtVO : selectedDtVOList) {
            if (requestSidSetByExsitReceiveDepSid.contains(orgTrnsDtVO.getRequireSid())) {
                // 已存在轉換後單位
                alertSidForInactive.add(orgTrnsDtVO.getAlertSid());
            } else {
                alertSidForTrns.add(orgTrnsDtVO.getAlertSid());
            }
        }

        // ====================================
        // 更新轉寄單位 UPDATE tr_alert_inbox.receive_dep
        // ====================================
        int updateRowCount = 0;
        Long startTime = null;
        String procTitle = "";

        if (WkStringUtils.notEmpty(alertSidForTrns)) {
            startTime = System.currentTimeMillis();
            procTitle = "UPDATE [tr_alert_inbox] SET [receive_dep = " + orgTransMappingTo.getAfterOrgSid() + "]";

            // 兜組 SQL
            StringBuffer updateSQL = new StringBuffer();
            updateSQL.append("UPDATE tr_alert_inbox ");
            updateSQL.append("SET    receive_dep = " + orgTransMappingTo.getAfterOrgSid() + " ");
            updateSQL.append("WHERE  alert_sid IN ( " + alertSidForTrns.stream().collect(Collectors.joining("','", "'", "'")) + ")");

            int rows = this.jdbcTemplate.update(updateSQL.toString());
            updateRowCount += rows;

            log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle + "共[" + rows + "]筆"));
        }

        // ====================================
        // 停用被合併資料
        // ====================================
        if (WkStringUtils.notEmpty(alertSidForInactive)) {
            startTime = System.currentTimeMillis();
            procTitle = "UPDATE [tr_alert_inbox] SET [status = 1]";

            // 兜組 SQL
            StringBuffer updateSQL = new StringBuffer();
            updateSQL.append("UPDATE tr_alert_inbox ");
            updateSQL.append("SET    status = 1 ");
            updateSQL.append("WHERE  alert_sid IN ( " + alertSidForInactive.stream().collect(Collectors.joining("','", "'", "'")) + ")");

            int rows = this.jdbcTemplate.update(updateSQL.toString());
            updateRowCount += rows;

            log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle + "共[" + rows + "]筆"));
        }

        // ====================================
        // save tr_trns_backup
        // ====================================
        // 準備記錄資料
        OrgTrnsAlertInboxBackupTo backupTo = new OrgTrnsAlertInboxBackupTo();
        backupTo.setTrnsAlertSids(alertSidForTrns);
        backupTo.setInactiveAlertSids(alertSidForInactive);
        backupTo.setBeforeDepSid(orgTransMappingTo.getBeforeOrgSid());
        backupTo.setAfterDepSid(orgTransMappingTo.getAfterOrgSid());

        // 準備備份資料檔
        TrnsBackup trnsBackup = new TrnsBackup();
        trnsBackup.setCreatedUser(execUserSid);
        trnsBackup.setCreatedDate(sysdate);
        trnsBackup.setTrnsType(TrnsType.ORG_TRNS_ALERT_INBOX_RECEVIE_DEP.name());
        trnsBackup.setCustKey(this.prepareCustKey(orgTransMappingTo));
        trnsBackup.setBackData(new Gson().toJson(backupTo));

        // save tr_trns_backup
        this.trnsBackupRepository.save(trnsBackup);

        log.info("轉檔已完成!");
        return updateRowCount;
    }

    private Set<String> queryRequestSidExsitAlertReceiveDepSid(
            Set<String> requestSids,
            Integer afterReceiveDepSid) {

        // ====================================
        // 兜組 SQL
        // ====================================
        StringBuffer varname1 = new StringBuffer();
        varname1.append("SELECT require_sid ");
        varname1.append("  FROM tr_alert_inbox ");
        varname1.append(" WHERE status = 0 ");
        varname1.append("       AND forward_type = 'FORWARD_DEPT' ");
        varname1.append("       AND inbox_type = 'INCOME_DEPT'");
        varname1.append("       AND receive_dep = " + afterReceiveDepSid + " ");
        varname1.append("       AND require_sid IN ( " + requestSids.stream().collect(Collectors.joining("','", "'", "'")) + ")");

        // ====================================
        // 查詢
        // ====================================
        List<String> exsitRequestSids = this.jdbcTemplate.query(varname1.toString(), new RowMapper<String>() {
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getString(1);
            }
        });

        if (WkStringUtils.isEmpty(exsitRequestSids)) {
            return Sets.newHashSet();
        }

        return exsitRequestSids.stream().collect(Collectors.toSet());
    }

    /**
     * @param orgTransMappingTo
     * @return
     */
    private String prepareCustKey(OrgTransMappingTo orgTransMappingTo) {
        return orgTransMappingTo.getBeforeOrgSid() + "-" + orgTransMappingTo.getAfterOrgSid();
    }

    // ========================================================================
    // 計算筆數
    // ========================================================================
    /**
     * 計算待轉筆數
     * 
     * @param orgTransMappingTos OrgTrnsWorkVerifyVO list
     * @return
     */
    public Map<String, Integer> countAllWaitTrans(List<OrgTrnsWorkVerifyVO> orgTransMappingTos) {
        if (WkStringUtils.isEmpty(orgTransMappingTos)) {
            return Maps.newHashMap();
        }

        // ====================================
        // 收集轉換前單位
        // ====================================
        // 收集, 並轉為 SQL where in 字串
        String beforeReceiveDepSids = orgTransMappingTos.stream()
                .map(to -> to.getBeforeOrgSid() + "")
                .collect(Collectors.joining(", "));

        // ====================================
        // 兜組 SQL
        // ====================================
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT receive_dep as createDepSid, ");
        sql.append("       COUNT(*)    as cnt ");
        sql.append("FROM   tr_alert_inbox inbox ");
        sql.append("       INNER JOIN tr_require tr ");
        sql.append("               ON inbox.require_sid = tr.require_sid ");
        sql.append("WHERE  inbox.status = 0 ");
        sql.append("       AND forward_type = 'FORWARD_DEPT' ");
        sql.append("       AND inbox_type = 'INCOME_DEPT' ");
        sql.append("       AND receive_dep IN ( " + beforeReceiveDepSids + " ) ");
        sql.append("GROUP BY inbox.receive_dep ");

        // ====================================
        // 依據單據類別, 逐項查詢
        // ====================================
        Map<String, Integer> waitTransCountMapByDataKey = Maps.newHashMap();

        // 查詢
        List<OrgTrnsDtVO> results = this.nativeSqlRepository.getResultList(
                sql.toString(), null, OrgTrnsDtVO.class);

        // 將有待轉筆數的部門，放入結果 map
        for (OrgTrnsDtVO orgTrnsDtVO : results) {
            String waitCountDataKey = this.orgTrnsService.prepareWaitCountDataKey(
                    "", orgTrnsDtVO.getCreateDepSid());
            waitTransCountMapByDataKey.put(waitCountDataKey, orgTrnsDtVO.getCnt().intValue());
        }

        //log.debug("\r\n" + WkJsonUtils.getInstance().toPettyJson(waitTransCountMapByDataKey));

        return waitTransCountMapByDataKey;
    }
}
