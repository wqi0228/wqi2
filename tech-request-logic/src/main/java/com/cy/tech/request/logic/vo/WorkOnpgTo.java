/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.vo;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author kasim
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = "sid")
public class WorkOnpgTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4431293822962610189L;

    private String sid;

    private Activation status = Activation.ACTIVE;

    private String onpgNo;

    private String sourceNo;

    private JsonStringListTo noticeDeps;

    private String theme;

    private String content;

    private String contentCss;

    private String note;

    private String noteCss;

    private Date establishDate;

    private Date finishDate;

    private Date cancelDate;

    private WorkOnpgStatus onpgStatus;

    private Date updatedDate;

    /**
     * 流程已完成
     * 
     * @return
     */
    public boolean isFinish() { return !Lists.newArrayList(
            WorkOnpgStatus.CANCEL_ONPG,
            WorkOnpgStatus.CHECK_COMPLETE).contains(this.onpgStatus); }

    /**
     * 需求(填單)部門
     */
    private Integer createDepSid;
    /**
     * 需(填單)求者
     */
    private Integer createUsr;

    /**
     * 取得通知單位SID
     * 
     * @return
     */
    public Set<Integer> getNoticeDepSids() {
        if (noticeDeps == null || WkStringUtils.isEmpty(noticeDeps.getValue())) {
            return Sets.newHashSet();
        }
        return noticeDeps.getValue().stream()
                .filter(sidStr -> WkStringUtils.isNumber(sidStr))
                .map(sidStr -> Integer.parseInt(sidStr))
                .collect(Collectors.toSet());
    }

    private boolean fromMms;

}
