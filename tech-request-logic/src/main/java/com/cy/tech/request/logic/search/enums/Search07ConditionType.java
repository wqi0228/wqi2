package com.cy.tech.request.logic.search.enums;

import com.cy.work.common.utils.WkStringUtils;

import lombok.Getter;

/**
 * 預設查詢條件
 * 
 * @author allen1214_wu
 */
public enum Search07ConditionType {

    NOT_FINISH_BY_DEP("A", "部門未完成需求"),
    NOT_FINISH_BY_SELF("B", "個人未完成需求"),
    ASSIGN("C", "已分派"),
    ;

    @Getter
    private final String val;
    @Getter
    private final String label;

    Search07ConditionType(String val, String label) {
        this.val = val;
        this.label = label;
    }

    /**
     * 解析為 Search07ConditionType
     * 
     * @param val
     * @return
     */
    public static Search07ConditionType parseVal(String val) {
        val = WkStringUtils.safeTrim(val);
        for (Search07ConditionType type : Search07ConditionType.values()) {
            if (type.getVal().equals(val)) {
                return type;
            }
        }
        return null;
    }
}
