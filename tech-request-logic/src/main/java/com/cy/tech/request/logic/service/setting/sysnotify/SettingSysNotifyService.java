package com.cy.tech.request.logic.service.setting.sysnotify;

import java.io.Serializable;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.User;
import com.cy.employee.rest.client.simple.SimpleEmployeeClient;
import com.cy.employee.vo.SimpleEmployee;
import com.cy.tech.request.logic.helper.component.mutipker.MultItemPickerByCustomerHelper;
import com.cy.tech.request.logic.helper.component.treepker.TreePickerCategoryHelper;
import com.cy.tech.request.logic.helper.systemnotify.SysNotifyEnumHelper;
import com.cy.tech.request.repository.setting.sysnotify.SettingSysNotifyRepository;
import com.cy.tech.request.vo.setting.sysnotify.SettingSysNotify;
import com.cy.tech.request.vo.setting.sysnotify.to.SettingSysNotifyMainInfo;
import com.cy.tech.request.vo.setting.sysnotify.to.SettingSysNotifyTypeInfo;
import com.cy.tech.request.vo.setting.sysnotify.vo.SettingManagetSysNotifyVO;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.notify.vo.enums.NotifyFilterType;
import com.cy.work.notify.vo.enums.NotifyMode;
import com.cy.work.notify.vo.enums.NotifyType;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * 設定：系統通知設定 service
 * 
 * @author allen1214_wu
 */
@Slf4j
@Service
public class SettingSysNotifyService implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 6664392571734444705L;
    // ========================================================================
	// 服務區
	// ========================================================================
	@Autowired
	private transient SettingSysNotifyRepository settingSysNotifyRepository;
	@Autowired
	private transient TreePickerCategoryHelper treePickerCategoryHelper;
	@Autowired
	private transient MultItemPickerByCustomerHelper multItemPickerByCustomerHelper;
	@Autowired
	private transient WkUserCache wkUserCache;
	@Autowired
	private transient WkOrgCache wkOrgCache;
	@Autowired
	private SimpleEmployeeClient employeeClient;

	// ========================================================================
	// 方法區
	// ========================================================================
	/**
	 * 建立新的 SettingSysNotify entity
	 * 
	 * @param userSid     資料歸屬 user sid
	 * @param execUserSid 執行 user sid
	 * @param sysDate     系統日期
	 * @return
	 */
	private SettingSysNotify createSettingSysNotify(Integer userSid, Integer execUserSid, Date sysDate) {
		SettingSysNotify settingSysNotify = new SettingSysNotify();
		settingSysNotify.setUserSid(userSid);
		settingSysNotify.setCreatedUser(execUserSid);
		settingSysNotify.setCreatedDate(sysDate);
		return settingSysNotify;

	}

	/**
	 * 比對傳入的使用者，是否需要通知 （有開啟通知）
	 * 
	 * @param notifyType  通知類別
	 * @param categorySid 需求單小類 sid
	 * @param custSid     需求單廳主 sid
	 * @param notifyMode  通知類別
	 * @param userSids    比對的使用者
	 * @return 回傳傳入使用者中，需要通知者
	 */
	public Map<Integer, SettingSysNotifyMainInfo> filterNeedNoticeUser(
	        NotifyType notifyType,
	        String categorySid,
	        Long custSid,
	        NotifyMode notifyMode,
	        Collection<Integer> userSids) {

		// ====================================
		// 防呆
		// ====================================
		if (WkStringUtils.isEmpty(userSids)) {
			return Maps.newHashMap();
		}

		// ====================================
		// 查詢所有設定資料
		// ====================================
		List<SettingSysNotify> entities = this.settingSysNotifyRepository.findAll();

		// ====================================
		// 收集需要通知的 user
		// ====================================
		Map<Integer, SettingSysNotifyMainInfo> notifySettingInfoMapByUserSid = Maps.newHashMap();
		for (SettingSysNotify settingSysNotify : entities) {
			// 不在清單中，不用比對
			if (!userSids.contains(settingSysNotify.getUserSid())) {
				continue;
			}

			// 查使用者資料檔
			User user = wkUserCache.findBySid(settingSysNotify.getUserSid());

			// 已停用使用者不通知
			if (!WkUserUtils.isActive(user)) {
				continue;
			}

			// 判定使用者設定值
			if (this.isNeedNotice(
			        settingSysNotify.getSettingInfo(),
			        notifyType,
			        categorySid,
			        custSid,
			        notifyMode)) {

				Integer userSid = settingSysNotify.getUserSid();

				notifySettingInfoMapByUserSid.put(
				        userSid,
				        this.prepareMainInfo(settingSysNotify, userSid));

			}
		}

		return notifySettingInfoMapByUserSid;
	}

	/**
	 * @param userSid
	 * @param notifyType
	 * @param isCategory
	 * @return
	 */
	public List<String> findBlackListByUserSidAndNotifyType(
	        Integer userSid,
	        NotifyType notifyType,
	        NotifyFilterType notifyFilterType) {
		// ====================================
		// 查詢單一使用者設定資訊
		// ====================================
		SettingSysNotifyMainInfo mainInfo = this.findSettingInfoByUserSid(userSid);
		if (mainInfo == null || mainInfo.getTypeInfo() == null) {
			return Lists.newArrayList();
		}

		// ====================================
		// 回傳黑名單
		// ====================================
		if (mainInfo.getTypeInfo().get(notifyType) == null
		        || mainInfo.getTypeInfo().get(notifyType).getBlackList() == null) {
			return Lists.newArrayList();
		}
		return mainInfo.getTypeInfo().get(notifyType).getBlackList().get(notifyFilterType);

	}

	/**
	 * 取得需要通知使用者
	 * 
	 * @param notifyType  通知類別
	 * @param categorySid 需求單小類 sid
	 * @param custSid     需求單廳主 sid
	 * @param notifyMode  通知類別
	 * @return
	 */
	public List<Integer> findNeedNotifyUsers(
	        NotifyType notifyType,
	        String categorySid,
	        Long custSid,
	        NotifyMode notifyMode) {

		// ====================================
		// 查詢所有設定資料
		// ====================================
		List<SettingSysNotify> entities = this.settingSysNotifyRepository.findAll();

		// ====================================
		// 收集需要通知的 user
		// ====================================
		List<Integer> notifyUserSids = Lists.newArrayList();
		for (SettingSysNotify settingSysNotify : entities) {
			// 查使用者資料檔
			User user = wkUserCache.findBySid(settingSysNotify.getUserSid());

			// 已停用使用者不通知
			if (!WkUserUtils.isActive(user)) {
				continue;
			}

			// 判定使用者設定值
			if (this.isNeedNotice(
			        settingSysNotify.getSettingInfo(),
			        notifyType,
			        categorySid,
			        custSid,
			        notifyMode)) {

				notifyUserSids.add(settingSysNotify.getUserSid());
			}
		}

		return notifyUserSids;
	}

	/**
	 * 以 userSid 查詢設定資料
	 * 
	 * @param userSid user sid
	 * @return SettingManagetSysNotifyVO
	 */
	public SettingSysNotifyMainInfo findSettingInfoByUserSid(Integer userSid) {
		// ====================================
		// 查詢
		// ====================================
		SettingSysNotify settingSysNotify = this.settingSysNotifyRepository.findByUserSid(userSid);

		// ====================================
		// 準備資料容器
		// ====================================
		return this.prepareMainInfo(settingSysNotify, userSid);
	}

	/**
	 * 依據使用者設定，判定是否需要通知
	 * 
	 * @param mainInfo    通知設定資料
	 * @param notifyType  通知類別
	 * @param categorySid 需求單小類 sid
	 * @param custSid     需求單廳主 sid
	 * @param notifyMode  通知類別
	 * @return 是/否
	 */
	private boolean isNeedNotice(
	        SettingSysNotifyMainInfo mainInfo,
	        NotifyType notifyType,
	        String categorySid,
	        Long custSid,
	        NotifyMode notifyMode) {

		// ====================================
		// 檢查開關
		// ====================================
		if (!this.isOepnSwitch(mainInfo, notifyType, notifyMode)) {
			return false;
		}

		// ====================================
		// 不存在於黑名單時，回傳 true (兩種設定為聯集)
		// ====================================
		// 取得指定通知類別設定
		SettingSysNotifyTypeInfo notifyTypeInfo = mainInfo.getTypeInfo().get(notifyType);

		if (!notifyTypeInfo.getBlackList().get(NotifyFilterType.SMALL_CATEGORY).contains(categorySid)) {
			return true;
		}

		if (!notifyTypeInfo.getBlackList().get(NotifyFilterType.COSTOMER).contains(custSid + "")) {
			return true;
		}

		return false;
	}

	/**
	 * @param userSettingInfo
	 * @param notifyType
	 * @param notifyMode
	 * @return
	 */
	private boolean isOepnSwitch(
	        SettingSysNotifyMainInfo userSettingInfo,
	        NotifyType notifyType,
	        NotifyMode notifyMode) {
		// ====================================
		// 防呆
		// ====================================
		if (userSettingInfo == null || userSettingInfo.getTypeInfo() == null) {
			return false;
		}

		// ====================================
		// 總開關
		// ====================================
		if (!userSettingInfo.isOpenMainSwich()) {
			return false;
		}

		// ====================================
		// 通知類別設定
		// ====================================
		// 取得指定通知類別設定
		SettingSysNotifyTypeInfo notifyTypeInfo = userSettingInfo.getTypeInfo().get(notifyType);
		if (notifyTypeInfo == null) {
			return false;
		}

		// ====================================
		// 判斷通知模式開關未開啟
		// ====================================
		// 未傳入通知模式時，有任何一個模式開啟則回傳 true
		if (notifyMode == null) {
			for (Boolean isOpenSwitch : notifyTypeInfo.getOpenSwitch().values()) {
				if (isOpenSwitch) {
					return true;
				}
			}
			// 有傳入通知模式
		} else if (notifyTypeInfo.getOpenSwitch().get(notifyMode)) {
			return true;
		}

		// ====================================
		// 未通過判定
		// ====================================
		return false;
	}

	/**
	 * @param userSettingInfo
	 * @param notifyType
	 * @param notifyFilterType
	 * @param itemSid
	 * @return
	 */
	private boolean isOpenSwitchForManager(
	        SettingSysNotifyMainInfo userSettingInfo,
	        NotifyType notifyType,
	        NotifyFilterType notifyFilterType,
	        String itemSid) {

		// ====================================
		// 檢查開關
		// ====================================
		if (!this.isOepnSwitch(userSettingInfo, notifyType, null)) {
			return false;
		}

		// ====================================
		// 檢查黑名單
		// ====================================
		// 取得指定通知類別設定
		SettingSysNotifyTypeInfo notifyTypeInfo = userSettingInfo.getTypeInfo().get(notifyType);
		// 在黑名單中代表關閉
		if (notifyTypeInfo
		        .getBlackList().get(notifyFilterType)
		        .contains(itemSid)) {
			return false;
		}

		// ====================================
		// 通過判定
		// ====================================
		return true;

	}

	/**
	 * 準備設定資料容器
	 * 
	 * @param settingSysNotify
	 * @return
	 */
	public SettingSysNotifyMainInfo prepareMainInfo(SettingSysNotify settingSysNotify, Integer userSid) {
		// ====================================
		// 取得設定資料容器
		// ====================================
		SettingSysNotifyMainInfo info = null;
		if (settingSysNotify != null
		        && settingSysNotify.getSettingInfo() != null) {
			info = settingSysNotify.getSettingInfo();
		} else {

			info = new SettingSysNotifyMainInfo();

			// 取得使用者公司 mail 資料
			// 取得 Employee 資料
			Optional<SimpleEmployee> simpleEmployee = employeeClient.findByMappedUserSid(userSid);
			if (simpleEmployee.isPresent()) {
				info.setNotifyMailAddrs(simpleEmployee.get().getGmail());
			}
		}

		// ====================================
		// 通知 mail
		// ====================================
		if (WkStringUtils.isEmpty(info.getNotifyMailAddrs())) {
			// 取得使用者公司 mail 資料
			Optional<SimpleEmployee> simpleEmployee = employeeClient.findByMappedUserSid(userSid);
			if (simpleEmployee.isPresent()) {
				info.setNotifyMailAddrs(simpleEmployee.get().getGmail());
			}
		}

		// ====================================
		// 填充-類別設定容器
		// ====================================
		if (info.getTypeInfo() == null) {
			info.setTypeInfo(Maps.newHashMap());
		}

		for (NotifyType notifyType : NotifyType.values()) {
			if (!info.getTypeInfo().containsKey(notifyType) || info.getTypeInfo().get(notifyType) == null) {
				info.getTypeInfo().put(notifyType, new SettingSysNotifyTypeInfo());
			}
		}

		return info;

	}

	/**
	 * @param userSids
	 * @param notifyFilterType
	 * @param itemSid
	 * @return
	 */
	public List<SettingManagetSysNotifyVO> prepareSettingInfoByUserSids(
	        List<Integer> userSids,
	        NotifyFilterType notifyFilterType,
	        String itemSid) {

		// ====================================
		// 防呆
		// ====================================
		if (WkStringUtils.isEmpty(userSids)) {
			return Lists.newArrayList();
		}

		// ====================================
		// 查詢
		// ====================================
		// 查詢 tr_setting_sys_notify
		List<SettingSysNotify> settingSysNotifys = this.settingSysNotifyRepository.findByUserSidIn(userSids);
		// 以 user Sid 建立 index
		Map<Integer, SettingSysNotify> settingSysNotifyMapByUserSid = settingSysNotifys.stream()
		        .collect(Collectors.toMap(
		                SettingSysNotify::getUserSid,
		                each -> each));

		// ====================================
		// 轉資料容器 (沒資料的也建立)
		// ====================================
		List<SettingManagetSysNotifyVO> settingVOs = Lists.newArrayList();

		// 沒有資料的 user 也要產生容器
		for (Integer userSid : userSids) {
			// ====================================
			// 取得使用者設定
			// ====================================
			SettingSysNotify userSetting = settingSysNotifyMapByUserSid.get(userSid);

			// ====================================
			// 初始化使用者設定資料容器
			// ====================================
			SettingManagetSysNotifyVO userViewInfo = new SettingManagetSysNotifyVO(userSid, itemSid);
			settingVOs.add(userViewInfo);

			// ====================================
			// 使用者資訊
			// ====================================
			userViewInfo.setUserSid(userSid);
			userViewInfo.setUserName(WkUserUtils.findNameBySid(userSid));
			userViewInfo.setUserInfo(WkUserUtils.prepareUserNameWithDep(userSid, OrgLevel.THE_PANEL, true, "-"));
			User user = WkUserCache.getInstance().findBySid(userSid);
			if (user != null && user.getPrimaryOrg() != null) {
				userViewInfo.setUserDepSid(user.getPrimaryOrg().getSid());
			}

			// ====================================
			// 處理設定資訊
			// ====================================
			this.prepareSettingInfoToView(userViewInfo, userSetting, notifyFilterType, itemSid);
		}

		// ====================================
		// 排序
		// ====================================
		// 取得所有單位排序序號
		Map<Integer, Integer> sortSeqMapByOrgSid = wkOrgCache.findAllOrgOrderSeqMap();

		// 建立排序規則
		// 1.部門排序
		Comparator<SettingManagetSysNotifyVO> comparator = Comparator.comparing(
		        vo -> WkOrgUtils.prepareOrgSortSeq(sortSeqMapByOrgSid, vo.getUserDepSid()));
		// 2.使用者暱稱
		comparator = comparator.thenComparing(Comparator.comparing(SettingManagetSysNotifyVO::getUserName));

		// 排序
		settingVOs = settingVOs.stream()
		        .sorted(comparator)
		        .collect(Collectors.toList());

		return settingVOs;
	}

	/**
	 * @param userViewInfo
	 * @param userSetting
	 * @param notifyFilterType
	 * @param itemSid
	 */
	public void prepareSettingInfoToView(
	        SettingManagetSysNotifyVO userViewInfo,
	        SettingSysNotify userSetting,
	        NotifyFilterType notifyFilterType,
	        String itemSid) {

		// ====================================
		// 此 user 還未設定資料
		// ====================================
		if (userSetting == null || userSetting.getSettingInfo() == null) {
			return;
		}
		// 取得使用者設定資料
		SettingSysNotifyMainInfo userSettingInfo = userSetting.getSettingInfo();

		// ====================================
		// 總開關沒開 (等於所有項目都關閉)
		// ====================================
		if (!userSetting.getSettingInfo().isOpenMainSwich()) {
			return;
		}

		// ====================================
		//
		// ====================================
		for (NotifyType notifyType : SysNotifyEnumHelper.getInstance().getNotifyTypes()) {
			// 判斷開關是否開啟
			boolean isOpenSwich = this.isOpenSwitchForManager(userSettingInfo, notifyType, notifyFilterType, itemSid);
			// 放回容器
			userViewInfo.getOpenSwitchMapByNotifyType().put(notifyType, isOpenSwich);
		}

	}

	/**
	 * @param mainInfo
	 * @param allCategoryItems
	 */
	public void prepareTiggerShowInfo(
	        SettingSysNotifyMainInfo mainInfo,
	        List<WkItem> allCategoryItems,
	        List<WkItem> allCustomerItems) {
		// ====================================
		// 僅保留小類項目
		// ====================================
		List<WkItem> allSmallCategoryItems = this.treePickerCategoryHelper.filterOnlySmallCategory(allCategoryItems);

		// ====================================
		// 準備顯示欄位
		// ====================================
		for (SettingSysNotifyTypeInfo notifyTypeInfo : mainInfo.getTypeInfo().values()) {

			for (NotifyFilterType notifyFilterType : NotifyFilterType.values()) {

				String descr = "";
				String toolTip = "";
				List<String> blackList = notifyTypeInfo.getBlackList().get(notifyFilterType);

				switch (notifyFilterType) {
				// ================
				// 需求小類
				// ================
				case SMALL_CATEGORY:
					// 組顯示說明文字
					descr = this.treePickerCategoryHelper.prepareCategoryShowInfo(
					        allSmallCategoryItems,
					        blackList);
					// 組 tooltip
					toolTip = this.treePickerCategoryHelper.prepareCategoryTooltip(
					        allCategoryItems,
					        allSmallCategoryItems,
					        blackList);

					break;

				case COSTOMER:

					// 組顯示說明文字
					descr = this.multItemPickerByCustomerHelper.prepareShowInfo(
					        allCustomerItems,
					        blackList);
					// 組 tooltip
					toolTip = this.multItemPickerByCustomerHelper.prepareTooltip(
					        allCustomerItems,
					        notifyTypeInfo.getBlackList().get(notifyFilterType));
					break;

				default:
					throw new SystemDevelopException("未實做的項目:[" + notifyFilterType + "]");
				}

				notifyTypeInfo.getShowInfoForFilterDescr().put(notifyFilterType, descr);
				notifyTypeInfo.getShowInfoForFilterTooltip().put(notifyFilterType, toolTip);
			}
		}
	}

	/**
	 * 準備要進行 update 的 SettingSysNotify (還不存在的話會建立一個空的)
	 * 
	 * @param userSid     資料歸屬 user sid
	 * @param execUserSid 執行 user sid
	 * @param sysDate     系統日期
	 * @return
	 */
	public SettingSysNotify prepareUpdateEntity(Integer userSid, Integer execUserSid, Date sysDate) {
		// ====================================
		// 查詢
		// ====================================
		SettingSysNotify settingSysNotify = this.settingSysNotifyRepository.findByUserSid(userSid);
		if (settingSysNotify == null) {
			settingSysNotify = this.createSettingSysNotify(userSid, execUserSid, sysDate);
		}

		// ====================================
		// 取得整裡 main info
		// ====================================
		// 組裝類別設定資料容器
		SettingSysNotifyMainInfo settingInfo = this.prepareMainInfo(settingSysNotify, userSid);
		// 將容器 set 回去
		settingSysNotify.setSettingInfo(settingInfo);

		return settingSysNotify;
	}

	/**
	 * save to DB
	 * 
	 * @param execUserSid      執行 user sid
	 * @param sysDate          系統日期
	 * @param settingSysNotify SettingSysNotify
	 */
	public void save(Integer execUserSid, Date sysDate, SettingSysNotify settingSysNotify) {
		settingSysNotify.setUpdatedDate(sysDate);
		settingSysNotify.setUpdatedUser(execUserSid);
		this.settingSysNotifyRepository.save(settingSysNotify);
	}

	/**
	 * 更新通知項目黑名單
	 * 
	 * @param userSid     資料歸屬 user sid
	 * @param execUserSid 執行者sid
	 * @param notifyType  通知類別
	 * @param isCategory
	 * @param blackList
	 */
	public void updateBlackList(
	        Integer userSid,
	        Integer execUserSid,
	        NotifyType notifyType,
	        NotifyFilterType notifyFilterType,
	        List<String> blackList) {

		Date sysDate = new Date();
		// ====================================
		// 查詢
		// ====================================
		SettingSysNotify settingSysNotify = this.prepareUpdateEntity(userSid, execUserSid, sysDate);

		// ====================================
		// 更新指定類別的設定值
		// ====================================
		// 取得指定類別的設定值
		SettingSysNotifyTypeInfo typeInfo = settingSysNotify.getSettingInfo().getTypeInfo().get(notifyType);

		// 傳入值相同時，不做處理
		List<String> oldBlackList = typeInfo.getBlackList().get(notifyFilterType);
		if (WkCommonUtils.compare(oldBlackList, blackList)) {
			log.debug("傳入值和目前設定值一致，無需更新!NotifyType[{}], 設定過濾方式:[{}]",
			        notifyType.name(),
			        notifyFilterType.getDescr());
			return;
		}

		// 依據通知項目，更新開關值
		typeInfo.getBlackList().put(notifyFilterType, blackList);

		// ====================================
		// update
		// ====================================
		this.save(execUserSid, sysDate, settingSysNotify);

	}

	/**
	 * 更新總開關狀態
	 * 
	 * @param userSid     資料歸屬 user sid
	 * @param execUserSid 執行者sid
	 * @param isOpen      總開關是否開啟
	 */
	public void updateMainSwitch(
	        Integer userSid,
	        Integer execUserSid,
	        boolean isOpen) {

		Date sysDate = new Date();
		// ====================================
		// 查詢
		// ====================================
		SettingSysNotify settingSysNotify = this.prepareUpdateEntity(userSid, execUserSid, sysDate);

		// ====================================
		// 準備更新資料
		// ====================================
		settingSysNotify.getSettingInfo().setOpenMainSwich(isOpen);

		// ====================================
		// update
		// ====================================
		this.save(execUserSid, sysDate, settingSysNotify);

		log.info("已【{}】系統通知總開關!", isOpen ? "開啟" : "關閉");
	}

	/**
	 * 更新項目通知開關
	 * 
	 * @param userSid     資料歸屬 user sid
	 * @param execUserSid 執行者sid
	 * @param notifyType  通知類別
	 * @param isMail
	 * @param isOpen
	 */
	public void updateNotifySwitch(
	        Integer userSid,
	        Integer execUserSid,
	        NotifyType notifyType,
	        NotifyMode notifyMode,
	        boolean isOpen) {

		Date sysDate = new Date();
		// ====================================
		// 查詢
		// ====================================
		SettingSysNotify settingSysNotify = this.prepareUpdateEntity(userSid, execUserSid, sysDate);

		// ====================================
		// 準備更新資料
		// ====================================
		// 取得指定類別的設定值
		SettingSysNotifyTypeInfo typeInfo = settingSysNotify.getSettingInfo().getTypeInfo().get(notifyType);

		// 舊的開關定值同傳入時，不做處理
		boolean oldSetting = typeInfo.getOpenSwitch().get(notifyMode);
		if (oldSetting == isOpen) {
			log.debug("傳入值和目前設定值一致，無需更新!NotifyType[{}], 通知方式:[{}], 開啟:[{}]",
			        notifyType.name(),
			        notifyMode.getDescr(),
			        isOpen);
			return;
		}

		// 更新開關資料
		typeInfo.getOpenSwitch().put(notifyMode, isOpen);

		// ====================================
		// update
		// ====================================
		this.save(execUserSid, sysDate, settingSysNotify);

	}

	/**
	 * 更新通知郵件地址
	 * 
	 * @param userSid      更新的 user sid
	 * @param execUserSid  執行者 sid
	 * @param mailAddrsStr 更新的資料
	 * @throws UserMessageException 檢核輸入資料，有錯誤時拋出
	 */
	public void updateNotifyMailAddrs(
	        Integer userSid,
	        Integer execUserSid,
	        String mailAddrsStr) throws UserMessageException {

		Date sysDate = new Date();

		// ====================================
		// 資料檢核
		// ====================================
		// 資料為空
		if (WkStringUtils.isEmpty(mailAddrsStr)) {
			throw new UserMessageException("輸入資料為空!", InfomationLevel.WARN);
		}

		// 收集輸入資料，並逐筆檢核
		List<String> mailAddrs = Lists.newArrayList();
		for (String mailAddr : mailAddrsStr.split(";")) {
			// 為空時略過
			if (WkStringUtils.isEmpty(mailAddr)) {
				continue;
			}
			// 去空白
			mailAddr = WkStringUtils.safeTrim(mailAddr);
			// 格式檢核
			if (!WkStringUtils.isEMail(mailAddr)) {
				throw new UserMessageException("輸入格式錯誤! [" + mailAddr + "]", InfomationLevel.WARN);
			}

			mailAddrs.add(mailAddr);
		}

		// 資料為空
		if (WkStringUtils.isEmpty(mailAddrsStr)) {
			throw new UserMessageException("輸入資料為空!", InfomationLevel.WARN);
		}

		// 整理字串
		mailAddrsStr = String.join(";", mailAddrs);

		// ====================================
		// 查詢
		// ====================================
		// 查詢主檔
		SettingSysNotify settingSysNotify = this.prepareUpdateEntity(userSid, execUserSid, sysDate);
		// 取得設定容器
		SettingSysNotifyMainInfo settingInfo = settingSysNotify.getSettingInfo();
		// 比對是否無需更新
		if (mailAddrsStr.equals(settingInfo.getNotifyMailAddrs())) {
			log.debug("mail 前後設定相同, 無需更新");
			return;
		}

		// ====================================
		// 準備更新資料
		// ====================================
		settingInfo.setNotifyMailAddrs(mailAddrsStr);

		// ====================================
		// update
		// ====================================
		this.save(execUserSid, sysDate, settingSysNotify);

	}

	/**
	 * @param userSid
	 * @param execUserSid
	 * @param notifyType
	 * @param notifyMode
	 * @param notifyFilterType 通知項目類別
	 * @param itemSid
	 * @param isOpen
	 * @param allItemSids
	 * @throws UserMessageException
	 */
	public void updateOpenSwitchForManager(
	        Integer userSid,
	        Integer execUserSid,
	        NotifyType notifyType,
	        NotifyFilterType notifyFilterType,
	        String itemSid,
	        boolean isOpen,
	        Map<NotifyFilterType, Set<String>> allItemSidsMapByNotifyFilterType) throws UserMessageException {

		Date sysDate = new Date();

		// ====================================
		// 查詢
		// ====================================
		// 主檔 ，若不存在時新建一筆
		SettingSysNotify settingSysNotify = this.prepareUpdateEntity(userSid, execUserSid, sysDate);
		// 設定主資訊
		SettingSysNotifyMainInfo settingInfo = settingSysNotify.getSettingInfo();
		// 取得傳入的『通知類別』的設定值
		SettingSysNotifyTypeInfo typeInfo = settingInfo.getTypeInfo().get(notifyType);

		// ====================================
		// 總開關
		// ====================================
		// 當傳入狀態為開啟時, 把總開關打開 (有開啟任一項時，需把總開關打開才收的到)
		if (isOpen) {
			settingInfo.setOpenMainSwich(true);
		}

		// ====================================
		// 通知方式開關
		// ====================================
		// 取得通知方式開關設定容器
		Map<NotifyMode, Boolean> openSwitchMapByNotifyMode = typeInfo.getOpenSwitch();
		// 舊的值 (複製一份)
		Map<NotifyMode, Boolean> oldOpenSwitch = Maps.newHashMap();
		for (NotifyMode currNotifyMode : openSwitchMapByNotifyMode.keySet()) {
			oldOpenSwitch.put(currNotifyMode, openSwitchMapByNotifyMode.get(currNotifyMode));
		}
		// 當傳入狀態為開啟時, 把所有通知方式打開 (有開啟任意接收項目時，需把通知方式打開才收的到)
		if (isOpen) {
			for (Entry<NotifyMode, Boolean> entrySet : openSwitchMapByNotifyMode.entrySet()) {
				entrySet.setValue(true);
			}
		}

		// ====================================
		// 處理黑名單
		// ====================================
		// ---------------------
		// 開啟
		// ---------------------
		if (isOpen) {
			this.updateOpenSwitchForManager_prepareBlackListByOpen(
			        notifyFilterType,
			        itemSid,
			        oldOpenSwitch,
			        typeInfo.getBlackList(),
			        allItemSidsMapByNotifyFilterType);

		}
		// ---------------------
		// 關閉
		// ---------------------
		else {
			// 處理關閉項目
			this.updateOpenSwitchForManager_prepareBlackListByColse(
			        settingInfo,
			        notifyType,
			        notifyFilterType,
			        itemSid,
			        allItemSidsMapByNotifyFilterType);
		}

		// ====================================
		// update
		// ====================================
		this.save(execUserSid, sysDate, settingSysNotify);
	}

	private void updateOpenSwitchForManager_prepareBlackListByOpen(
	        NotifyFilterType notifyFilterType,
	        String itemSid,
	        Map<NotifyMode, Boolean> oldOpenSwitch,
	        Map<NotifyFilterType, List<String>> blackListMapByNotifyFilterType,
	        Map<NotifyFilterType, Set<String>> allItemsMapByNotifyFilterType) {

		// ====================================
		// 舊設定中是否有任何一項開啟
		// ====================================
		boolean isAnyOpenSwitchInOldStatus = false;
		for (Boolean currIsOpenSwitch : oldOpenSwitch.values()) {
			if (currIsOpenSwitch) {
				isAnyOpenSwitchInOldStatus = true;
				break;
			}
		}

		// ====================================
		//
		// ====================================
		for (NotifyFilterType currNotifyFilterType : NotifyFilterType.values()) {

			Set<String> currBlackList = Sets.newHashSet(blackListMapByNotifyFilterType.get(currNotifyFilterType));
			Set<String> currAllItemSids = allItemsMapByNotifyFilterType.get(currNotifyFilterType);

			// 原使用者關閉開關，且黑名單為空 (視為從未設定過)
			// 將所有通知項目加入黑名單 (僅開啟一項通知的效果)
			if (!isAnyOpenSwitchInOldStatus && WkStringUtils.isEmpty(currBlackList)) {
				currBlackList.addAll(currAllItemSids);
			}

			// 為編輯項目時，移除開啟的通知項目
			if (currNotifyFilterType.equals(notifyFilterType)) {
				currBlackList.remove(itemSid);
			}

			// 維護 (移除停用或不存在的項目)
			currBlackList.retainAll(currAllItemSids);

			// 存回去容器
			blackListMapByNotifyFilterType.put(currNotifyFilterType, Lists.newArrayList(currBlackList));
		}
	}

	/**
	 * @param settingInfo
	 * @param notifyType
	 * @param notifyFilterType
	 * @param itemSid
	 * @param allItemSidsMapByNotifyFilterType
	 */
	private void updateOpenSwitchForManager_prepareBlackListByColse(
	        SettingSysNotifyMainInfo settingInfo,
	        NotifyType notifyType,
	        NotifyFilterType notifyFilterType,
	        String itemSid,
	        Map<NotifyFilterType, Set<String>> allItemSidsMapByNotifyFilterType) {

		// ====================================
		// 檢查是否已經沒有通知項目
		// ====================================

		boolean isNeedCloseMainSwitch = true;

		for (NotifyType currNotifyType : settingInfo.getTypeInfo().keySet()) {

			SettingSysNotifyTypeInfo typeInfo = settingInfo.getTypeInfo().get(currNotifyType);
			// 『黑名單項目』容器
			Map<NotifyFilterType, List<String>> blackListMapByNotifyFilterType = typeInfo.getBlackList();
			// 『通知方式-開關設定』容器
			Map<NotifyMode, Boolean> openSwitchMapByNotifyMode = typeInfo.getOpenSwitch();

			// 依據通知清單類別, 逐筆處理
			boolean isHasDiff = false;
			for (NotifyFilterType currNotifyFilterType : NotifyFilterType.values()) {

				Set<String> currBlackList = Sets.newHashSet(blackListMapByNotifyFilterType.get(currNotifyFilterType));
				Set<String> currAllItemSids = allItemSidsMapByNotifyFilterType.get(currNotifyFilterType);

				// 將關閉項目加到黑名單
				if (currNotifyType.equals(notifyType) &&
				        currNotifyFilterType.equals(notifyFilterType)) {
					currBlackList.add(itemSid);
				}

				// 維護 (移除停用或不存在的項目)
				currBlackList.retainAll(currAllItemSids);
				// 存回去容器
				blackListMapByNotifyFilterType.put(currNotifyFilterType, Lists.newArrayList(currBlackList));

				// 比對是否還有要的通知項目 (黑名單 = 全部)
				if (!WkCommonUtils.compare(currBlackList, currAllItemSids)) {
					isHasDiff = true;
				}
			}

			if (!isHasDiff) {
				// 關閉所有通知方式開關
				for (Entry<NotifyMode, Boolean> entrySet : openSwitchMapByNotifyMode.entrySet()) {
					entrySet.setValue(false);
				}
				// 清空黑名單資料
				for (Entry<NotifyFilterType, List<String>> entrySet : blackListMapByNotifyFilterType.entrySet()) {
					entrySet.setValue(Lists.newArrayList());
				}
			}

			// 檢查此通知項目, 是否還有開啟的通知方式
			for (Entry<NotifyMode, Boolean> entrySet : openSwitchMapByNotifyMode.entrySet()) {
				if (entrySet.getValue()) {
					isNeedCloseMainSwitch = false;
					break;
				}
			}
		}

		// 關閉總開關
		if (isNeedCloseMainSwitch) {
			settingInfo.setOpenMainSwich(false);
		}

	}

	/**
	 * 例行排程 housekeeping 清除停用者設定資料
	 */
	@Transactional(rollbackFor = Exception.class)
	public void housekeeping() {

		// ====================================
		// 查詢全部
		// ====================================
		List<SettingSysNotify> entities = this.settingSysNotifyRepository.findAll();
		
		// ====================================
		// 刪除已停用使用者的設定
		// ====================================
		for (SettingSysNotify settingSysNotify : entities) {
			if(!WkUserUtils.isActive(settingSysNotify.getUserSid())) {
				this.settingSysNotifyRepository.delete(settingSysNotify);
				log.info("清除系統通知設定[tr_setting_sys_notify], 已離職員工:{}", 
						WkUserUtils.findNameBySid(settingSysNotify.getUserSid()));
			}
		}
	}
}
