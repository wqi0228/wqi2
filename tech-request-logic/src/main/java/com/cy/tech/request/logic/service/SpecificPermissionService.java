/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.Role;
import com.cy.tech.request.repository.sp.SpecificRoleInquireDepRepository;
import com.cy.tech.request.vo.sp.SpecificRoleInquireDep;
import com.cy.tech.request.vo.value.to.DepTo;
import java.io.Serializable;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.google.common.collect.Lists;

/**
 *
 * @author jason_h
 */
@Component
@Slf4j
public class SpecificPermissionService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2910057091281061720L;

    @Autowired
    private SpecificRoleInquireDepRepository roleDepRepository;

    @Autowired
    private OrganizationService orgService;

    /**
     * 撈取角色相關的特殊權限(依部門)
     * 
     * @param roleSids
     * @return
     */
    public List<Org> findSpecificDeptsByRoleSidIn(List<Long> roleSids) {
        if (roleSids == null || roleSids.isEmpty()) {
            return Lists.newArrayList();
        }
        List<Role> roles = Lists.newArrayList();
        for (Long sid : roleSids) {
            Role r = new Role();
            try {
                r.setSid(Long.valueOf(sid));
            } catch (NumberFormatException ex) {
                log.error(ex.getMessage(), ex);
                continue;
            }
            roles.add(r);
        }
        List<SpecificRoleInquireDep> specific = roleDepRepository.findByRoleIn(roles);
        return orgService.findBySids(this.getSpecificDeptSids(specific));
    }

    private List<Integer> getSpecificDeptSids(List<SpecificRoleInquireDep> specific) {
        List<Integer> deptSids = Lists.newArrayList();
        for (SpecificRoleInquireDep s : specific) {
            for (DepTo d : s.getDep().getDepInfo()) {
                deptSids.add(d.getSid());
            }
        }
        return deptSids;
    }

}
