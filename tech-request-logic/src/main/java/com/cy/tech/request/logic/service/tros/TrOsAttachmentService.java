/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.tros;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.logic.service.trace.TrRequireTraceCustomerService;
import com.cy.tech.request.repository.require.trace.TrRequireTraceRepository;
import com.cy.tech.request.repository.require.tros.TrOsAttachmentRepository;
import com.cy.tech.request.repository.require.tros.TrOsHistoryRepository;
import com.cy.tech.request.vo.require.trace.TrRequireTrace;
import com.cy.tech.request.vo.require.tros.TrOsAttachment;
import com.cy.tech.request.vo.require.tros.TrOsHistory;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 其他設定資訊相關附件Service
 *
 * @author brain0925_liao
 */
@Component
public class TrOsAttachmentService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2814994098632233861L;
    /** TrOsAttachmentRepository */
    @Autowired
    private TrOsAttachmentRepository trOsAttachmentRepository;
    /** TrRequireTraceCustomerService */
    @Autowired
    private TrRequireTraceCustomerService trRequireTraceCustomerService;
    /** TrRequireTraceRepository */
    @Autowired
    private TrRequireTraceRepository trRequireTraceRepository;
    /** TrOsHistoryRepository */
    @Autowired
    private TrOsHistoryRepository trOsHistoryRepository;

    /**
     * 取得TrOsAttachment By Sid
     *
     * @param sid TrOsAttachmentSid
     * @return
     */
    public TrOsAttachment findBySid(String sid) {
        return trOsAttachmentRepository.findOne(sid);
    }

    /**
     * 刪除其他設定資訊附件
     *
     * @param attSid 附件Sid
     * @param loginUserSid 刪除者Sid
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public void deleteTrOsAttachment(String attSid, Integer loginUserSid) {
        TrOsAttachment tr = findBySid(attSid);
        tr.setStatus(Activation.INACTIVE);
        trOsAttachmentRepository.save(tr);
        TrRequireTrace trt = trRequireTraceCustomerService.createDeleteTrOsAttachmentTrace(tr.getSid(),
                tr.getFile_name(), tr.getCreate_usr(), tr.getOs_no(), loginUserSid, tr.getRequire_sid(),
                tr.getRequire_no());
        this.createTrace(trt, loginUserSid);
    }

    /**
     * 刪除其他設定資訊相關附件(回覆,回覆的回覆,取消,完成)
     *
     * @param attSid 附件Sid
     * @param loginUserSid 刪除者Sid
     * @return
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public TrOsHistory deleteTrOSHistoryAttachment(String attSid, Integer loginUserSid) {
        TrOsAttachment tr = findBySid(attSid);
        tr.setStatus(Activation.INACTIVE);
        trOsAttachmentRepository.save(tr);
        TrOsHistory trOsHistory = trOsHistoryRepository.findOne(tr.getOs_history_sid());
        TrRequireTrace trt = trRequireTraceCustomerService.createDeleteTrOsHistoryAttachmentTrace(tr.getSid(),
                tr.getFile_name(), tr.getCreate_usr(), tr.getOs_no(), loginUserSid, tr.getRequire_sid(),
                tr.getRequire_no(), trOsHistory.getBehavior());
        this.createTrace(trt, loginUserSid);
        return trOsHistory;
    }

    /**
     * 建立追蹤
     *
     * @param trt 追蹤物件
     * @param loginUserSid 建立追蹤者Sid
     */
    private void createTrace(TrRequireTrace trt, Integer loginUserSid) {
        trt.setCreate_dt(new Date());
        trt.setCreate_usr(loginUserSid);
        trRequireTraceRepository.save(trt);
    }

    /**
     * 建立其他設定資訊相關附件
     *
     * @param trOsAttachment 其他設定資訊相關附件物件
     * @param loginUserSid 建立者Sid
     * @return
     */
    public TrOsAttachment createTrOsAttachment(TrOsAttachment trOsAttachment, Integer loginUserSid) {
        trOsAttachment.setStatus(Activation.ACTIVE);
        trOsAttachment.setCreate_dt(new Date());
        trOsAttachment.setCreate_usr(loginUserSid);

        return trOsAttachmentRepository.save(trOsAttachment);
    }

    /**
     * 更新其他設定資訊相關附件
     *
     * @param trOsAttachment 其他設定資訊相關附件物件
     * @param loginUserSid 更新者Sid
     * @return
     */
    public TrOsAttachment updateTrOsAttachment(TrOsAttachment trOsAttachment, Integer loginUserSid) {
        trOsAttachment.setUpdate_dt(new Date());
        trOsAttachment.setUpdate_usr(loginUserSid);
        return trOsAttachmentRepository.save(trOsAttachment);
    }

    /**
     * 取得附件 By 其他設定資訊Sid
     *
     * @param os_sid 其他設定資訊Sid
     * @return
     */
    public List<TrOsAttachment> getTrOsAttachmentByOSSid(String os_sid) {
        return trOsAttachmentRepository.getTrOsAttachmentByOSSid(os_sid);
    }

    /**
     * 取得附件(回覆,回覆的回覆,取消,完成)
     *
     * @param os_sid 其他設定資訊Sid
     * @param os_history_sid HistorySid
     * @return
     */
    public List<TrOsAttachment> getTrOsAttachmentByOSSidAndOsHistorySid(String os_sid, String os_history_sid) {
        return trOsAttachmentRepository.getTrOsAttachmentByOSSidAndOsHistorySid(os_sid, os_history_sid);
    }
}
