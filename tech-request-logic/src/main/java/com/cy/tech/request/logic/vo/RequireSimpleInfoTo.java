package com.cy.tech.request.logic.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequireSimpleInfoTo {
    private String requireSid;
    private String requireStatus;
    private String reqCateType;
}
