package com.cy.tech.request.logic.enumerate;

import lombok.Getter;

/**
 * 報表類型
 *
 * @author jason_h
 */
public enum ReportType {

    NOT_FIND("未發現符合的功能類型", DateType.NULL, " "),
    /** 需求單查詢 */
    REQUIRE("需求單查詢", DateType.NULL, "search01.xhtml"),
    /** 需求單位簽核進度查詢 */
    SIGN("需求單位簽核進度查詢", DateType.THIS_MONTH, "search02.xhtml"),
    /** 檢查確認單據查詢 */
    WAIT_CHECK("檢查確認單據查詢", DateType.NULL, "search03.xhtml"),
    /** 退件資訊查詢 */
    REJECT("退件資訊查詢", DateType.NULL, "search04.xhtml"),
    /** 已分派單據查詢 */
    ASSIGN("被分派單據查詢", DateType.NULL, "search07.xhtml"),
    /** 原型確認一覽表 */
    PROTOTYPE_CHECK("原型確認一覽表", DateType.THIS_MONTH, "search08.xhtml"),
    /** 原型確認簽核進度查詢 */
    PROTOTYPE_SIGN("原型確認簽核進度查詢", DateType.THIS_MONTH, "search09.xhtml"),
    /** 未結案單據查詢 */
    NOT_CLOSE("未結案單據查詢", DateType.NULL, "search10.xhtml"),
    /** 歷史單據查詢 */
    CLOSE("歷史單據查詢", DateType.NULL, "search11.xhtml"),
    /** 收費金額一覽表 */
    AMOUNT_CASH("收費金額一覽表", DateType.THIS_MONTH, "search12.xhtml"),
    /** 送測狀況一覽表 */
    WORK_TEST_INFO("送測狀況一覽表", DateType.NULL, "search13.xhtml"),
    /** 送測簽核進度查詢 */
    WORK_TEST_SIGN("送測簽核進度查詢", DateType.THIS_MONTH, "search14.xhtml"),
    /** 送測異動明細表 */
    WORK_TEST_HISTORY("送測異動明細表", DateType.THIS_MONTH, "search15.xhtml"),
    /** ON程式一覽表 */
    WORK_ON_PG("ON程式一覽表", DateType.NULL, "search16.xhtml"),
    /** ON程式異動明細表 */
    WORK_ON_HISTORY("ON程式異動明細表", DateType.THIS_MONTH, "search17.xhtml"),
    /** ON程式異動明細表 */
    TEST_NOTIFY_LIST("測試提醒清單", DateType.NULL, "search18.xhtml"),
    /** 其它資料設定報表 */
    OTHER_SETUP_SETTING("其它設定資訊", DateType.NULL, "search19.xhtml"),
    /** 退件資訊查詢(部門別) */
    REJECT_DEP("退件資訊查詢", DateType.NULL, "search20.xhtml"),
    /** 需求暫緩單據查詢(部門別) */
    // SUSPEND_DEP("需求暫緩單據查詢", DateType.NULL, "search21.xhtml"),
    /** 追蹤 */
    TRACE("追蹤", DateType.NULL, "search22.xhtml"),
    /** 追蹤 */
    DRAFT("草稿查詢", DateType.NULL, "search23.xhtml"),
    /** 案件單轉入報表查詢 */
    TRANSFER_FROM_TECH("案件單轉入報表", DateType.NULL, "search24.xhtml"),
    /** 執行狀況查詢 */
    EXECUTION_SITUATION("執行狀況查詢", DateType.NULL, "search25.xhtml"),
    /** 需求製作進度 */
    REQUIRE_MAKE_PROGESS("需求製作進度", DateType.NULL, "search26.xhtml"),
    /** 需求單查詢 FOR GM */
    REQUIRE_FOR_GM("需求單查詢(GM)", DateType.NULL, "search27.xhtml"),
    /** 收藏夾查詢 */
    FAVORITES("收藏夾查詢", DateType.NULL, "home01.xhtml"),
    /** 收部門轉發 */
    INCOME_FORWARD_DEPT("收部門轉發", DateType.NULL, "home02.xhtml"),
    /** 寄件備份 */
    INBOX_SEND_BACKUP("寄件備份", DateType.TODAY, "home03.xhtml"),
    /** 收呈報 */
    INBOX_REPORT("收呈報", DateType.NULL, "home04.xhtml"),
    /** 收個人 */
    INBOX_PERSONAL("收個人", DateType.NULL, "home05.xhtml"),
    /** 收指示 */
    INBOX_INSTRUCTION("收指示", DateType.NULL, "home06.xhtml"),
    /** 類別基本資料建立作業 */
    SETTING_CASE_CATE("類別基本資料建立作業", DateType.NULL, "setting01.xhtml"),
    /** 報表快選(自訂) */
    REPORT_CUSTOM("報表快選(自訂)", DateType.NULL, "setting09.xhtml"),
    /** 推播自訂義設定 */
    NOTIFY_CUSTOMIZE("推播自訂義設定", DateType.NULL, "setting11.xhtml"),
    /** 第二分類維護作業 */
    OTHER_CATEGORY("第二分類維護作業", DateType.NULL, "setting13.xhtml"),
    /** ON程式檢查完成提醒清單 */
    WORK_ON_PG_CHECK("ON程式檢查完成提醒清單", DateType.NULL, "search28.xhtml"),
    /** 需求單待結案 */
    WAIT_CLOSE("需求單待結案", DateType.NULL, "search29.xhtml"),
    /** 需求單待結案 */
    TODO_PROTOTYPE_CHECK("待辦：原型確認-待功能確認", DateType.NULL, "search30.xhtml"),
    /** 需求單待結案 */
    TODO_OTHER_SETUP_SETTING("待辦：其他資料設定 - 待完成", DateType.NULL, "search31.xhtml"),
    /** 開放沙巴需求 */
    SABA_LIST("開放沙巴需求", DateType.NULL, "sabaList.xhtml"),
    /** 逾期未結案需求報表 */
    OVERDUE_UNCLOSED("逾期未結案需求報表", DateType.NULL, "overdueUnclosed.xhtml"),
    /** 逾期未完工需求報表 */
    OVERDUE_UNFINISHED("逾期未完工需求報表", DateType.NULL, "overdueUnfinished.xhtml"),
    /** 送測文件提交區 */
    WORK_TEST_SEND("送測文件提交區", DateType.NULL, "search32.xhtml"),
    /** QA待審核單據 */
    WORK_QA_REVIEW("QA待審核單據", DateType.NULL, "search33.xhtml"),
    /** 送測排程表 */
    WORK_SCHEDULE("送測排程表", DateType.NULL, "search34.xhtml"),
    /** 送測排程表 */
    BATCH_ADJUST_DATE("批次調整日期", DateType.NULL, "batchAdjustDate.xhtml"),
    COUNT_ONPG("ON程式統計表", DateType.NULL, "searchCountOnpg.xhtml"),
    
    //========================================================================
    // 新結構報表
    // ========================================================================
    /**
     * 
     */
    NEW_SEARCH01("需求單查詢", DateType.THIS_MONTH, "newsearch01.xhtml"),
    /**
     * 
     */
    NEW_SEARCH02("未領單據查詢", DateType.NULL, "newsearch02.xhtml"),
    /**
     * 
     */
    NEW_SEARCH03("被通知單據查詢", DateType.NULL, "newsearch03.xhtml"),
    ;

    @Getter
    private final String reportName;
    @Getter
    private final DateType dateType;
    @Getter
    private final String viewId;

    ReportType(String reportName, DateType dateType, String viewId) {
        this.reportName = reportName;
        this.dateType = dateType;
        this.viewId = viewId;
    }
}
