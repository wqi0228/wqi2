/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import com.cy.tech.request.logic.search.enums.ReqSubType;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkStringUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 需求製作進度 (search26.xhtml)
 *
 * @author jason_h
 */
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = false, of = { "index" })
public class Search26TreeView extends Search26BaseView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -688223165999145771L;
    /** 需求單 sid */
    private String reqSid;
    /** 需求單號 */
    private String reqNo;
    /** 需求單主題 */
    private String requireTheme;
    /** 緊急度 */
    private UrgencyType urgency;
    /** 需求完成日 */
    private Date reqFinishDt;
    /** 結案日 */
    private Date reqCloseDt;
    /** 需求單位 */
    private String reqCreateDep;
    /** 需求人員 */
    private String reqCreatedUser;
    /** 統整人員 */
    private String inteStaffUser;

    /** 子程序型態 */
    private ReqSubType subType;
    /** 子程序 sid */
    private String subSid;

    /** 項目名稱 */
    private String theme;
    /** 建立日期 */
    private Date creDt;
    /** 程序狀態 */
    private String status;
    /** 預計完成日 */
    private Date esDt;
    /** 補充說明 */
    private String note;
    /** 備註(放罝需求單當前最新處理的子程序) */
    private String memo;
    /** 執行天數 */
    private String execDays;
    /** 列clz */
    private String rowClz;
    /** 上下筆要用的索引 */
    private Integer index;
    /** 連結網址 */
    private String localUrlLink;
    private Boolean isRoot = false;
    private Boolean isSubGroup = false;
    private Boolean isSub = false;
    private Boolean isColumnGone = false;

    public Search26TreeView(Search26View view, Integer index) {
        this.reqSid = view.getReqSid();
        this.reqNo = view.getReqNo();
        this.requireTheme = view.getRequireTheme();
        this.urgency = view.getUrgency();
        // this.reqFinishDt = view.getReqFinishDt();
        // this.reqCloseDt = view.getReqCloseDt();
        // this.reqCreatedUser = view.getReqCreatedUser();
        // this.reqCreateDep = view.getReqCreateDep();
        // this.inteStaffUser = view.getInteStaffUser();
        this.subType = view.getSubType();

        this.index = index;
        this.localUrlLink = view.getLocalUrlLink();
        if (subType.equals(ReqSubType.ROOT)) {
            this.buildByRoot(view);
            return;
        }
        this.buildBySub(view);
    }

    /**
     * 子程序群組節點
     *
     * @param treeView
     * @param subType
     */
    public Search26TreeView(Search26TreeView treeView, ReqSubType subType) {
        this.reqSid = treeView.getReqSid();
        this.reqNo = treeView.getReqNo();
        this.requireTheme = treeView.getRequireTheme();
        this.urgency = treeView.getUrgency();
        // this.reqFinishDt = treeView.getReqFinishDt();
        // this.reqCloseDt = treeView.getReqCloseDt();
        // this.reqCreatedUser = treeView.getReqCreatedUser();
        // this.reqCreateDep = treeView.getReqCreateDep();
        // this.inteStaffUser = treeView.getInteStaffUser();
        this.subType = subType;
        this.theme = subType.getShowName();
        this.isSubGroup = true;
        this.isColumnGone = true;
        this.rowClz = this.findRowStyle();
        // this.index = index;
    }

    /**
     * 需求單節點
     *
     * @param view
     */
    private void buildByRoot(Search26View view) {
        this.theme = view.getRequireTheme();
        this.creDt = view.getReqCreDt();
        this.status = view.getReqStatus();
        this.esDt = view.getReqHopeDt();

        this.reqFinishDt = view.getReqFinishDt();
        this.reqCloseDt = view.getReqCloseDt();
        this.reqCreatedUser = view.getReqCreatedUser();
        this.reqCreateDep = view.getReqCreateDep();
        this.inteStaffUser = view.getInteStaffUser();

        this.note = view.getAssignDepsStr();
        this.execDays = String.valueOf(view.getReqExecDays());
        this.isRoot = true;
        this.rowClz = this.findRowStyle();
    }

    /**
     * 子程序節點
     *
     * @param view
     */
    private void buildBySub(Search26View view) {
        this.subSid = view.getSubSid();
        this.theme = view.getSubTheme();
        this.creDt = view.getSubCreDt();
        this.status = view.getSubStatus();
        this.esDt = view.getSubEsDt();
        this.note = view.getSubNoticeStr();
        this.isSub = true;
        this.isColumnGone = true;
        this.rowClz = this.findRowStyle();
    }

    private String findRowStyle() {
        String style = urgency.equals(UrgencyType.URGENT) ? "urgencyRow" : "";
        if (isRoot) {
            return style + " req_make_node_root_clz";
        }
        if (isSubGroup) {
            return style + " req_make_node_sub_group_clz";
        }
        if (isSub) {
            return style + " req_make_node_sub_clz";
        }
        return style;
    }

    /**
     * 需求單節點備註處理(放罝需求單當前最新處理的子程序)
     *
     * @param reqRootView
     * @param reqGroupViews
     */
    public void resetReqMemo(List<Search26BaseView> reqGroupViews) {
        // 需求單節點本身狀態是否為無效狀態
        if (this.invalidReqStatus()) {
            return;
        }
        List<Search26View> sortSubs = reqGroupViews.stream()
                .filter(base -> !base.getSubType().equals(ReqSubType.ROOT))
                .map(base -> (Search26View) base)
                .sorted((v1, v2) -> v2.getSubCreDt().compareTo(v1.getSubCreDt()))
                .collect(Collectors.toList());
        Optional<Search26View> op = sortSubs.stream().filter(view -> this.validSubStatus(view)).findFirst();
        if (op.isPresent()) {
            this.memo = op.get().getSubType().getShowName();
        }
    }

    /**
     * 需先判斷需求單節點本身狀態是否為有效狀態
     *
     * @return
     */
    private boolean invalidReqStatus() {
        return this.status.equals("作廢") || this.status.equals("結案") || this.status.equals("需求暫緩");
    }

    private boolean validSubStatus(Search26View view) {

        String subType = WkStringUtils.safeTrim(view.getSubStatus());

        switch (view.getSubType()) {
        case PT:
            return !("重新實作".equals(subType) || "審核作廢".equals(subType));
        case WT:
            return !("取消測試".equals(subType) || "退測".equals(subType) || "審核作廢".equals(subType));
        case OP:
            return !("取消ON程式".equals(subType));
        case OS:
            return !("作廢".equals(subType));
        default:
            break;
        }
        return false;
    }

}
