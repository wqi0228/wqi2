package com.cy.tech.request.logic.helper.systemnotify;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import com.cy.work.notify.vo.enums.NotifyFilterType;
import com.cy.work.notify.vo.enums.NotifyMode;
import com.cy.work.notify.vo.enums.NotifyModule;
import com.cy.work.notify.vo.enums.NotifyType;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author allen1214_wu
 *
 */
@Component
@NoArgsConstructor
public class SysNotifyEnumHelper implements Serializable, InitializingBean {

	/**
     * 
     */
    private static final long serialVersionUID = 3572837795104139672L;
    // ========================================================================
	// InitializingBean
	// ========================================================================
	private static SysNotifyEnumHelper instance;

	/**
	 * getInstance
	 * 
	 * @return instance
	 */
	public static SysNotifyEnumHelper getInstance() {
		return instance;
	}

	/**
	 * fix for fireBugs warning
	 * 
	 * @param instance
	 */
	private static void setInstance(SysNotifyEnumHelper instance) {
		SysNotifyEnumHelper.instance = instance;
	}

	/**
	 * afterPropertiesSet
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		setInstance(this);
	}

	// ========================================================================
	//
	// ========================================================================
	/**
	 * 所有通知模組別
	 */
	@Getter
	private List<NotifyModule> notifyModules = Lists.newArrayList(NotifyModule.values());
	/**
	 * 所有通知類別
	 */
	@Getter
	private List<NotifyType> notifyTypes = Lists.newArrayList(NotifyType.values());

	/**
	 * 所有通知方式
	 */
	@Getter
	private List<NotifyMode> notifyModes = Lists.newArrayList(NotifyMode.values());

	/**
	 * 所有模組別
	 */
	@Getter
	private List<NotifyFilterType> notifyFilterTypes = Lists.newArrayList(NotifyFilterType.values());

	/**
	 * @param str
	 * @return
	 */
	public NotifyFilterType trnsToNotifyFilterType(String str) {
		return NotifyFilterType.safeValueOf(str);
	}
}
