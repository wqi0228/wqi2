/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.enums.UrgencyType;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 需求單位簽核進度查詢頁面顯示vo (search02.xhtml)
 *
 * @author jason_h
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Search02View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 9112174442325574558L;
    /** 緊急度 */
    private UrgencyType urgency;
    /** 立單日期 */
    private Date createdDate;
    /** 需求類別 */
    private String bigName;
    /** 中類 */
    private String middleName;
    /** 小類" */
    private String smallName;
    /** 需求單位 */
    private Integer createDep;
    /** 需求人員 */
    private Integer createdUser;
    /** 待審核人員 */
    private String defaultSignedName;
    /** 需求單位審核狀態 */
    private InstanceStatus instanceStatus;
    /** 需求成立日 */
    private Date requireEstablishDate;
    /** 本地端連結網址 */
    private String localUrlLink;
}
