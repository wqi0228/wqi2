package com.cy.tech.request.logic.service.pmis;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.AssignSendInfoHistoryService;
import com.cy.tech.request.logic.service.AssignSendInfoService;
import com.cy.tech.request.logic.service.FogbugzService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.RequireTraceService;
import com.cy.tech.request.logic.service.TemplateService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.URLService.URLServiceAttr;
import com.cy.tech.request.logic.utils.DateUtils;
import com.cy.tech.request.logic.vo.PmisResponseVO;
import com.cy.tech.request.logic.vo.UrlParamTo;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.require.FogbugzAssign;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import com.cy.tech.request.vo.template.component.AbstractComBase;
import com.cy.tech.request.vo.template.component.AbstractComBase.PMIS;
import com.cy.tech.request.vo.template.component.ComBase;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class PmisHelper {
    @Autowired
    private PmisService pmisService;
    @Autowired
    private TemplateService templateService;
    @Autowired
    private RequireTraceService traceService;
    @Autowired
    private RequireService requireService;
    @Autowired
    private URLService urlService;
    @Autowired
    private FogbugzService fogbugzService;
    @Autowired
    private AssignSendInfoHistoryService assignSendInfoHistoryService;
    @Autowired
    private RequireTraceService requireTraceService;
    @Autowired
    private AssignSendInfoService assignSendInfoService;

    @Value("${pmis.token}")
    @Getter
    private String pmisToken;

    /**
     * 轉PMIS (當組織異動時
     * 
     * @throws UserMessageException
     */
    public void transferToPMISWhenOrgChange(String requireSid, Integer loginUserSid) throws UserMessageException {
        if (WkStringUtils.notEmpty(this.pmisService.findNeedTrnasForOrgTrnas(Sets.newHashSet(requireSid)))) {
            log.debug("組織異動, 需求單重新轉PMIS，requireSid:[{}]", requireSid);
            transferToPMIS(requireSid, loginUserSid);
        }
    }

    /**
     * 需求單轉PMIS
     * 
     * @throws UserMessageException
     */
    public void transferToPMIS(String requireSid, Integer loginUserSid) throws UserMessageException {

        User loginUser = WkUserCache.getInstance().findBySid(loginUserSid);

        // ====================================
        // 取得需求單資料
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);
        if (require == null) {
            throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }

        // log.info("同步PMIS-[" + require.getRequireNo() + "]：開始");

        // ====================================
        // 準備轉PMIS資料
        // ====================================
        // 準備欄位資料
        Map<String, Object> jsonMap = this.buildJsonMapForPmis(require);

        // 轉 JSON string
        String json = "";
        try {
            json = new ObjectMapper().writeValueAsString(jsonMap);
        } catch (JsonProcessingException e) {
            String message = String.format("準備轉PMIS資料失敗!,[%s])", e.getMessage());
            log.warn(message);
            throw new UserMessageException(message, InfomationLevel.WARN);
        }

        // ====================================
        // 發送至PMIS
        // ====================================
        // call API
        // log.debug("\r\n" + new GsonBuilder().setPrettyPrinting().create().toJson(jsonMap));
        try {
            PmisResponseVO responseVO = pmisService.sendDataToPmis(require.getRequireNo(), json);
            if (!responseVO.isOk()) {
                String message = String.format("發送至PMIS時發生錯誤,主機回應訊息:[%s]", responseVO.getMessage());
                log.error(message);
                throw new UserMessageException(message, InfomationLevel.ERROR);
            }
        } catch (Exception e) {
            String errorMessage = "將資料同步到 PMIS 時失敗! (" + e.getMessage() + ")";
            log.error(errorMessage, e);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 若為第一次轉PMIS, 寫一筆追蹤
        // ====================================
        try {
            if (CollectionUtils.isEmpty(traceService.findTraceByTraceType(require, RequireTraceType.TRANS_TO_PMIS))) {
                this.requireTraceService.createRequireTrace(
                        require.getSid(),
                        loginUser,
                        RequireTraceType.TRANS_TO_PMIS,
                        "需求資訊轉拋至PMIS系統");
            }
        } catch (Exception e) {
            // TODO add Alert
            log.error("轉拋PMIS成功,但寫追蹤時失敗!" + e.getMessage(), e);
        }

        // ====================================
        // 寫轉拋記錄
        // ====================================
        try {
            this.pmisService.saveByMap(jsonMap, require.getCreateDep().getSid(), loginUser);
        } catch (Exception e) {
            // TODO add Alert
            log.error("轉拋PMIS成功,但寫轉拋記錄時失敗!" + e.getMessage(), e);
        }

        // log.info("同步PMIS-[" + require.getRequireNo() + "]：完成");

    }

    /**
     * 建立轉json時, 使用的Map物件
     * 
     * @param require
     * @return
     * @throws UserMessageException
     * @throws Exception
     */
    private Map<String, Object> buildJsonMapForPmis(Require require) throws UserMessageException {
        Map<String, Object> jsonMap = Maps.newLinkedHashMap();

        // ====================================
        // 查詢最早分派日 (歷史記錄)
        // ====================================
        Date d_date = this.assignSendInfoHistoryService.findFirstAssignDate(require.getSid());
        if (d_date == null) {
            String message = String.format("準備轉PMIS資料失敗!,[%s]尚未分派 (找不到分派記錄)", require.getRequireNo());
            log.warn(message);
            throw new UserMessageException(message, InfomationLevel.WARN);
        }

        // ====================================
        // 組分派部門字串 (察看資料)
        // ====================================
        // 查詢分派部門
        Set<Integer> assignDepSids = this.assignSendInfoService.findAssignDepsByRequireSid(require.getSid());

        // 收束到上層單位
        Set<Integer> tempAsignDepSids = Sets.newHashSet();
        for (Integer depSid : assignDepSids) {
            Set<Integer> parentSids = WkOrgCache.getInstance().findAllParentSid(depSid);

            // 當上層單位已經在清單時，忽略子單位
            boolean isAdd = true;
            if (WkStringUtils.notEmpty(parentSids)) {
                for (Integer parentSid : parentSids) {
                    if (assignDepSids.contains(parentSid)) {
                        isAdd = false;
                        break;
                    }
                }
            }
            if (isAdd) {
                tempAsignDepSids.add(depSid);
            }
        }

        // 排序
        List<Integer> sendAssignDepSids = WkOrgUtils.sortByOrgTree(tempAsignDepSids);

        // 組裝分派單位名稱 list
        List<String> d_co_dept = sendAssignDepSids.stream()
                .map(depSid -> WkOrgUtils.prepareBreadcrumbsByDepName(depSid, OrgLevel.DIVISION_LEVEL, false, "-"))
                .collect(Collectors.toList());

        // List<AssignTabInfoTo> assignTabInfos = tableHelper.createTabInfo(require);
        // List<String> d_co_dept = this.buildAssingDepts(assignTabInfos.get(0));

        // ====================================
        // 取得需求單模板 主題、內容、備註 (for json map)
        // ====================================
        Map<String, String> content = this.buildRequireContent(require);
        String d_theme = content.get("d_theme");
        String d_reason = content.get("d_reason");
        String d_desc = content.get("d_desc");

        // ====================================
        // 準備單據連結 URL
        // ====================================
        String servletPath = urlService.createLoacalURLLink(URLServiceAttr.URL_ATTR_M, new UrlParamTo(require.getRequireNo())).substring(2);
        String url = Faces.getRequest().getRequestURL().toString();
        url = url.substring(0, url.indexOf(Faces.getRequest().getServletPath()));
        url = String.format("%s%s", url, servletPath);

        // ====================================
        // 取得最早的 FB 單據
        // ====================================
        List<FogbugzAssign> fbList = fogbugzService.findByRequire(require);
        Integer fbId = null;
        if (CollectionUtils.isNotEmpty(fbList)) {
            int index = fbList.size() - 1; // 取最早那一筆FBID
            fbId = fbList.get(index).getFbId();
        }

        // ====================================
        // 兜組 API 參數
        // ====================================
        jsonMap.put("token", pmisToken);
        jsonMap.put("d_item", require.getMapping().getBigName());
        jsonMap.put("d_mid_item", require.getMapping().getMiddleName());
        jsonMap.put("d_theme", d_theme);
        jsonMap.put("d_reason", d_reason);
        jsonMap.put("d_desc", d_desc);
        jsonMap.put("d_dept", WkOrgUtils.findNameBySid(require.getCreateDep().getSid()));
        jsonMap.put("d_date", DateUtils.YYYY_MM_DD2.print(d_date.getTime()));
        jsonMap.put("d_co_dept", d_co_dept);
        jsonMap.put("werp_id", require.getRequireNo());
        jsonMap.put("werp_link", url);
        jsonMap.put("case_number", fbId);

        return jsonMap;
    }

    /**
     * 取得需求單模板 主題、內容、備註 (for json map)
     * 
     * @param require
     * @return
     */
    private Map<String, String> buildRequireContent(Require require) {
        Map<String, String> map = Maps.newHashMap();
        List<FieldKeyMapping> templateItem = templateService.loadTemplateFieldByMapping(require.getMapping());
        // 取得模板元件預設值
        Map<String, ComBase> comValueMap = templateService.loadTemplateFieldValue(require);

        List<AbstractComBase> descList = Lists.newArrayList();
        for (FieldKeyMapping each : templateItem) {
            String pmis = (String) each.getDefaultValue().getValue().get(ComBase.PMIS_IDX);
            ComBase comBase = comValueMap.get(each.getComId());
            if (WkStringUtils.notEmpty(pmis) && comBase instanceof AbstractComBase) {
                if (PMIS.valueOf(pmis).equals(PMIS.THEME)) {
                    map.put("d_theme", ((AbstractComBase) comBase).transferToPmisValue());
                } else if (PMIS.valueOf(pmis).equals(PMIS.REASON)) {
                    map.put("d_reason", ((AbstractComBase) comBase).transferToPmisValue());
                } else if (PMIS.valueOf(pmis).equals(PMIS.DESC)) {
                    descList.add((AbstractComBase) comBase);
                }
            }
        }
        if (descList.size() > 1) {
            StringBuffer d_desc = new StringBuffer();
            int count = descList.size();
            for (int i = 0; i < count; i++) {
                d_desc.append(descList.get(i).getName());
                d_desc.append(":");
                d_desc.append(descList.get(i).transferToPmisValue());
                if (i != (count - 1)) {
                    d_desc.append("\r\n\r\n");
                }
            }
            map.put("d_desc", d_desc.toString());
        } else if (descList.size() == 1) {
            map.put("d_desc", descList.get(0).transferToPmisValue());
        }

        return map;
    }
}
