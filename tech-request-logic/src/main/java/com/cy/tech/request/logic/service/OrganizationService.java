/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.enums.OrgType;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.commons.vo.simple.SimpleOrg;
import com.cy.commons.vo.simple.SimpleUser;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Joiner;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;

/**
 * @author shaun
 */
@Slf4j
@Component
public class OrganizationService implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4944035795974575097L;

    private static OrganizationService instance;

    public static OrganizationService getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        OrganizationService.instance = this;
    }

    @Autowired
    private WkOrgCache wkOrgCache;

    public OrganizationService() {
    }

    public void clearCache() {
        wkOrgCache.updateCache();
    }

    public List<Org> findAll(Activation status) {
        List<Org> orgs = Lists.newArrayList();
        wkOrgCache.findAll().forEach(item -> {
            if (status.equals(item.getStatus())) {
                orgs.add(item);
            }
        });
        return orgs;
    }

    public List<Org> findAll() {
        return wkOrgCache.findAll();
    }

    public List<Org> findAllCompany() {
        List<Org> comps = Lists.newArrayList();
        wkOrgCache.findAll().forEach(item -> {
            if (item.getStatus().equals(Activation.ACTIVE) && item.getType() != null && !item.getType().equals(OrgType.DEPARTMENT)) {
                comps.add(item);
            }
        });
        return comps;
    }

    public Org findBySid(Integer sid) {
        return wkOrgCache.findBySid(sid);
    }

    public Org findBySid(String sid) {
        return this.findBySid(Integer.valueOf(sid));
    }

    /**
     * @param status
     * @param company
     * @param keyword
     * @return
     */
    public List<Org> findByStatusAndCompanyAndOrgNameLike(Activation status, Org company, String keyword) {

        // 查詢公司下的單位
        List<Org> deps = this.findAllChildrenByCompanyAndStatus(company, status);
        if (deps == null) {
            return Lists.newArrayList();
        }

        // 關鍵字為空時,回傳全部
        if (WkStringUtils.isEmpty(keyword)) {
            return deps;
        }

        // like keyword
        return deps.stream()
                .filter(org -> WkOrgUtils.getOrgName(org).toUpperCase().contains(keyword.toUpperCase()))
                .collect(Collectors.toList());

    }

    public List<Org> findAllChildrenByCompanyAndStatus(Org company, Activation status) {
        if (status == null) {
            return this.findAll().stream()
                    .filter(each -> each.getCompany() != null && each.getCompany().getSid().equals(company.getSid()))
                    .collect(Collectors.toList());
        }
        return this.findAll(status).stream()
                .filter(each -> each.getCompany() != null && each.getCompany().getSid().equals(company.getSid()))
                .collect(Collectors.toList());
    }

    public Org findByOrgId(String orgId) {
        return wkOrgCache.findById(orgId);
    }

    public List<Org> findBySids(List<Integer> sids) {
        List<Org> orgs = Lists.newArrayList();
        sids.forEach(each -> {
            Org selOrg = wkOrgCache.findBySid(each);
            if (selOrg != null) {
                orgs.add(selOrg);
            }
        });
        return orgs;
    }

    public List<Org> findBySids(Collection<String> sids) {
        if (sids == null || sids.isEmpty()) {
            return Lists.newArrayList();
        }
        List<Integer> intSids = Lists.newArrayList();
        for (String each : sids) {
            intSids.add(Integer.valueOf(each));
        }
        return this.findBySids(intSids);
    }

    /**
     * 不分停用
     *
     * @param parent
     * @return
     */
    public List<Org> findByParentOrg(Org parent) {
        if (parent == null) {
            return Lists.newArrayList();
        }
        List<Org> orgs = Lists.newArrayList();
        wkOrgCache.findAll().forEach(item -> {
            if (item.getParent() != null && item.getParent().getSid().equals(parent.getSid())) {
                orgs.add(item);
            }
        });
        return orgs;
    }

    public List<Org> findByParentOrg(Org parent, Activation status) {
        if (parent == null) {
            return Lists.newArrayList();
        }
        List<Org> orgs = Lists.newArrayList();
        wkOrgCache.findAll().forEach(item -> {
            if (item.getParent() != null && item.getParent().getSid().equals(parent.getSid()) && status.equals(item.getStatus())) {
                orgs.add(item);
            }
        });
        return orgs;
        // return status.equals(Activation.ACTIVE)
        // ? this.findByParent(parent, orgActiveCache)
        // : this.findByParent(parent, orgInactiveCache);
    }

    // private List<Org> findByParent(Org parent, List<Org> results) {
    //
    // cache.values().forEach(each -> {
    // if (each.getParentOrg() == null) {
    // log.debug("each.getParentOrg() == null => " + each.getSid());
    // }
    // if (each.getParentOrg() != null &&
    // each.getParentOrg().getSid().equals(parent.getSid())) {
    // orgs.add(each);
    // }
    // });
    // return orgs;
    // }
    public String showParentDep(Org org) {
        if (org.getLevel() != null && (org.getLevel().equals(OrgLevel.GROUPS) || org.getLevel().equals(OrgLevel.DIVISION_LEVEL))) {
            return WkOrgUtils.getOrgName(org);
        }
        if (org.getType().equals(OrgType.HEAD_OFFICE) || org.getType().equals(OrgType.BRANCH_OFFICE)) {
            return WkOrgUtils.getOrgName(org);
        }
        return this.parseDivision(this.showParentDepInner(org));
    }

    /**
     * 顯示所有上層組織到「處」級為止
     *
     * @param dep 部門
     * @return
     */
    public String showParentDepInner(Org dep) {

        if (dep == null) {
            return "";
        }

        if (dep.getType() == null) {
            return WkOrgUtils.getOrgName(dep);
        }

        if (!OrgType.DEPARTMENT.equals(dep.getType())) {
            return WkOrgUtils.getOrgName(dep);
        }
        if (dep.getParent() == null) {
            return WkOrgUtils.getOrgName(dep);
        }
        Org parentOrg = wkOrgCache.findBySid(dep.getParent().getSid());
        if (!OrgType.DEPARTMENT.equals(parentOrg.getType())) {
            return WkOrgUtils.getOrgName(dep);
        }
        return showParentDepInner(parentOrg) + "-" + WkOrgUtils.getOrgName(dep);
    }

    private String parseDivision(String result) {
        if (!result.contains("群")) {
            return result;
        }
        String[] array = result.split("-");
        int groupCnt = 0;
        int divCnt = 0;
        for (String each : array) {
            if (each.contains("群")) {
                groupCnt++;
            }
            if (each.contains("處")) {
                divCnt++;
            }
        }
        if (groupCnt > 0 && divCnt > 0) {
            List<String> strList = Lists.newArrayList();
            int idx = 0;
            for (String each : array) {
                if (idx++ != 0) {
                    strList.add(each);
                }
            }
            return Joiner.on(" - ").join(strList);
        }
        return result;
    }

    public Org findUserCompany(Org org) {
        if (!org.getType().equals(OrgType.DEPARTMENT)) {
            return org;
        }
        if (org.getParent() == null) {
            return org;
        }
        Org parentOrg = wkOrgCache.findBySid(org.getParent().getSid());
        return this.findUserCompany(parentOrg);
    }

    /**
     * 取得基本部門向下所有單位
     *
     * @param org
     * @return
     */
    public List<Org> findChildOrgsByBasicOrg(Org org) {
        Org basicOrg = getBasicOrg(org);
        return findChildOrgs(basicOrg);
    }

    /**
     * 如CompanyCategory為部門（DEPARTMENT）且DepartmentLevel為組層級（THE_PANEL），則向上取到非組層級
     *
     * @param org
     * @return basicOrg（至少為部級單位）
     */
    private Org getBasicOrg(Org org) {
        Org basicOrg = wkOrgCache.findBySid(org.getSid());
        if (OrgType.DEPARTMENT.equals(basicOrg.getType())
                && OrgLevel.THE_PANEL.equals(basicOrg.getLevel())) {
            Org parentOrg = wkOrgCache.findBySid(basicOrg.getParent().getSid());
            basicOrg = getBasicOrg(parentOrg);
        }
        return basicOrg;
    }

    /**
     * 抓取子部門
     *
     * @param org
     * @return
     */
    public List<Org> findChildOrgs(Org org) {
        Org orgNew = this.findBySid(org.getSid());
        List<Org> orgs = Lists.newArrayList();
        wkOrgCache.getChildOrgs(orgNew.getSid()).forEach(item -> {
            orgs.add(item);
            findChildOrgs(item, orgs);
        });
        orgs.add(orgNew);
        return orgs;
    }

    private void findChildOrgs(Org org, List<Org> allOrgs) {
        Org orgNew = this.findBySid(org.getSid());
        List<Org> orgs = wkOrgCache.getChildOrgs(orgNew.getSid());
        if (orgs == null) {
            return;
        }
        orgs.forEach(item -> {
            allOrgs.add(item);
            findChildOrgs(item, allOrgs);
        });
    }

    /**
     * 建立ParentOrg所包含的部門資料
     *
     * @return
     */
    public Map<Integer, List<Org>> createParentOrg() {
        Map<Integer, List<Org>> parentOrgMap = Maps.newHashMap();
        List<Org> allOrgs = this.findAll(Activation.ACTIVE);
        Integer key;
        List<Org> value;
        for (Org o : allOrgs) {
            if (o.getParent() == null) {
                continue;
            }
            key = o.getParent().getSid();
            value = Lists.newArrayList();
            if (parentOrgMap.get(key) != null) {
                value = (List<Org>) parentOrgMap.get(key);
            }
            value.add(o);
            parentOrgMap.put(key, value);
        }
        return parentOrgMap;
    }

    /**
     * 尋找部門向上全部的管理者
     *
     * @param org
     * @return
     */
    public List<User> findOrgManagers(Org org) {
        if (org == null) {
            return Lists.newArrayList();
        }
        List<Integer> orgs = Lists.newArrayList();
        this.findAllAboveOrgSid(orgs, this.findBySid(org.getSid()));
        List<User> users = Lists.newArrayList();
        this.findBySids(orgs).forEach(currOrg -> {

            if (currOrg != null && WkStringUtils.notEmpty(currOrg.getManagers())) {
                for (SimpleUser user : currOrg.getManagers()) {
                    User currUser = WkUserCache.getInstance().findBySid(user.getSid());
                    users.add(currUser);
                }
            }
        });
        return users;
    }

    /**
     * 尋找部門向上全部的管理者
     *
     * @param orgSid
     * @return
     */
    public List<User> findOrgManagers(Integer orgSid) {
        if (orgSid == null) {
            return Lists.newArrayList();
        }
        return this.findOrgManagers(this.findBySid(orgSid));
    }

    public void findAllAboveOrgSid(List<Integer> orgSids, Org org) {
        if (org == null) {
            return;
        }
        orgSids.add(org.getSid());
        Org orgNew = wkOrgCache.findBySid(org.getSid());
        if (orgNew.getParent() == null) {
            return;
        }
        Org parentOrg = wkOrgCache.findBySid(orgNew.getParent().getSid());
        this.findAllAboveOrgSid(orgSids, parentOrg);
    }

    /**
     * 抓取上層部門(含本身部門單位
     *
     * @param org
     * @return parentOrgs
     */
    public List<Org> findParentOrgsAndSelf(Integer orgSid) {
        Org org = findBySid(orgSid);
        if (org != null) {
            List<Org> parentOrgs = Lists.newArrayList(org);
            return findParentOrgs(parentOrgs, org);
        }
        return Lists.newArrayList();
    }

    /**
     * 抓取上層部門(不含本身部門單位
     *
     * @param org
     * @return parentOrgs
     */
    public List<Org> findParentOrgs(Org org) {
        List<Org> parentOrgs = Lists.newArrayList();
        return findParentOrgs(parentOrgs, org);
    }

    /**
     * 抓取上層部門(遞迴
     *
     * @param orgs
     * @param org
     * @return parentOrgs
     */
    private List<Org> findParentOrgs(List<Org> orgs, Org org) {
        if (org.getParent() == null) {
            return orgs;
        }
        Org parentOrg = wkOrgCache.findBySid(org.getParent().getSid());
        if (!orgs.contains(parentOrg)) {
            orgs.add(parentOrg);
        }
        return findParentOrgs(orgs, parentOrg);
    }

    public Org findByUUID(String uuid) {
        return wkOrgCache.findAll().stream()
                .filter(o -> o.getUuid().equals(uuid))
                .findFirst().get();
    }

    public String getOrgName(Object org) {
        if (org == null) {
            return "";
        }

        if (org instanceof Org) {
            return WkOrgUtils.getOrgName((Org) org);
        }

        if (org instanceof SimpleOrg) {
            return WkOrgUtils.getOrgName(((SimpleOrg) org).toOrg());
        }

        if (org instanceof Integer) {
            return this.wkOrgCache.findNameBySid((Integer) org);
        }
        if (org instanceof String) {
            return this.wkOrgCache.findNameBySid((String) org);
        }

        return "";
    }

    /**
     * 取得基本部門向下所有單位
     *
     * @param org
     * @return
     */
    public List<Org> findChildOrgsByMinisterial(Org org) {
        Org basicOrg = getBasicOrgByMinisterial(org);
        return findChildOrgs(basicOrg);
    }

    /**
     * 取的基本單位至少為 處 層級
     *
     * @param org
     * @return basicOrg（至少為處級單位）
     */
    public Org getBasicOrgByMinisterial(Org org) {
        Org basicOrg = wkOrgCache.findBySid(org.getSid());
        if (OrgType.DEPARTMENT.equals(basicOrg.getType())
                && Lists.newArrayList(OrgLevel.THE_PANEL, OrgLevel.MINISTERIAL).contains(basicOrg.getLevel())) {
            Org parentOrg = wkOrgCache.findBySid(basicOrg.getParent().getSid());
            basicOrg = getBasicOrgByMinisterial(parentOrg);
        }
        return basicOrg;
    }

    /**
     * 不分停用
     *
     * @param parent
     * @return
     */
    public List<Org> findByParentSid(Integer parentSid) {
        return this.findByParentOrg(this.findBySid(parentSid));
    }

    public List<Org> findByParentSid(Integer parentSid, Activation status) {
        return this.findByParentOrg(this.findBySid(parentSid));
    }

    /**
     * 撈取該使用者的管理部門 (含底下)
     * 
     * @param userSid
     * @return
     */
    public List<Org> getAllManagerOrgs(Integer userSid) {
        try {
            List<Org> managerOrgs = this.wkOrgCache.findManagerOrgs(userSid);
            List<Org> allManangers = Lists.newArrayList();
            managerOrgs.forEach(item -> {
                if (item != null) {
                    allManangers.addAll(findChildOrgs(item));
                }
            });
            return allManangers;
        } catch (Exception e) {
            log.error("getAllManagerOrgs ERROR", e);
        }
        return Lists.newArrayList();
    }

    /**
     * 取得完整部門名稱(部-組) OR (處-部-組)等
     *
     * @param sid
     * @param fullOrgName
     * @param excludDepLevel
     * @return
     */
    private String getFullOrgName(Integer sid, String fullOrgName, List<OrgLevel> excludDepLevel) {
        if (!Strings.isNullOrEmpty(fullOrgName)) {
            fullOrgName = "-" + fullOrgName;
        }
        Org org = this.findBySid(sid);
        if (org != null) {
            fullOrgName = WkOrgUtils.getOrgName(org) + fullOrgName;
        } else {
            fullOrgName = "[" + sid + "]" + fullOrgName;
            return fullOrgName;
        }

        if (OrgType.DEPARTMENT.equals(org.getType())
                && org.getParent() != null
                && org.getParent().getLevel() != null
                && !excludDepLevel.contains(org.getParent().getLevel())) {
            fullOrgName = this.getFullOrgName(org.getParent().getSid(), fullOrgName, excludDepLevel);
        }
        return fullOrgName;
    }

    /**
     * 取得完整部門名稱(處-部-組)等
     *
     * @param sid
     * @return
     */
    public String getFullOrgNameByGroups(Integer sid) {
        return this.getFullOrgName(sid, "", Lists.newArrayList(OrgLevel.GROUPS));
    }
}
