/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.onpg;

import com.cy.tech.request.repository.onpg.TrWorkOnpgRepo;
import com.cy.tech.request.vo.onpg.TrWorkOnpg;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * On程式Service
 *
 * @author brain0925_liao
 */
@Component
public class TrWorkOnpgService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7432122574757517386L;
    /** TrWorkOnpgRepo */
    @Autowired
    private TrWorkOnpgRepo trWorkOnpgRepo;

    /**
     * 取得On程式物件 By onpgNo
     *
     * @param onpgNo
     * @return
     */
    public TrWorkOnpg findByOnpgNo(String onpgNo) {
        return trWorkOnpgRepo.findByOnpgNo(onpgNo);
    }

    /**
     * 更新On程式建立部門 By onpgNo
     *
     * @param depSid 建立部門Sid
     * @param onpgNo
     */
    public void updateDepsidByPtNo(Integer depSid, String onpgNo) {
        trWorkOnpgRepo.updateDepsid(onpgNo, depSid);
    }

    /**
     * 更新On程式通知部門 By Sid
     *
     * @param sid On程式Sid
     * @param noticeDeps 通知部門物件
     * @return
     */
    public int updateNoticeDeps(String sid, JsonStringListTo noticeDeps) {
        return trWorkOnpgRepo.updateNoticeDeps(sid, noticeDeps);
    }
}
