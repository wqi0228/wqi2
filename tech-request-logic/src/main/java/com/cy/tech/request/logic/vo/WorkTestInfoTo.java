/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.vo;

import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author kasim
 */
@Data
@NoArgsConstructor
public class WorkTestInfoTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -315521618550714631L;

    /** 送測單sid */
    private String sid;

    /** 送測單號 */
    private String testinfoNo;

    /** 送測來源 單號 */
    private String sourceNo;

    /** 挑選的送測單位 */
    private JsonStringListTo sendTestDep;

    /** 是否有流程 */
    private Boolean hasSign;

    /** 送測主題 */
    private String theme;

    /** 送測內容 */
    private String content;

    /** 送測內容 含css */
    private String contentCss;

    /** 備註說明 */
    private String note;

    /** 備註說明 含css */
    private String noteCss;

    /** 預計完成日 */
    private Date establishDate;

    /** 實際完成日 */
    private Date finishDate;

    /** 送測取消日 */
    private Date cancelDate;

    /** 送測狀態 */
    private WorkTestStatus testinfoStatus;

    /** FB Case No */
    private Integer fbId;

    /** 異動日期 */
    private Date updatedDate;
    
    
    /**
     * 需求(填單)部門
     */
    private Integer createDepSid;
    /**
     * 需(填單)求者
     */
    private Integer createUsr;
    
    /**
     * 流程已完成
     * @return
     */
    public boolean isFinish() {
        return !Lists.newArrayList(WorkTestStatus.SIGN_PROCESS,
                WorkTestStatus.SONGCE,
                WorkTestStatus.TESTING).contains(this.testinfoStatus);
    }
    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.getSid());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WorkTestInfoTo other = (WorkTestInfoTo) obj;
        if (!Objects.equals(this.getSid(), other.getSid())) {
            return false;
        }
        return true;
    }
}
