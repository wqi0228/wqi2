package com.cy.tech.request.logic.service.orgtrns;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.system.rest.client.vo.OrgTransMappingTo;
import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.logic.service.orgtrns.emun.OrgTransSqlType;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsDtVO;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsWorkVerifyVO;
import com.cy.tech.request.logic.service.pmis.PmisHelper;
import com.cy.tech.request.repository.require.TrnsBackupRepository;
import com.cy.tech.request.vo.enums.RequireTransProgramType;
import com.cy.tech.request.vo.require.TrnsBackup;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Slf4j
@NoArgsConstructor
@Service
public class OrgTrnsCreateDepService {

    // ========================================================================
    // 私有容器
    // ========================================================================
    @Data
    private class OrgTrnsCreateDepBackupTo implements Serializable {
        /**
         * 
         */
        private static final long serialVersionUID = -3720712730175134455L;
        private List<String> sids;
        private List<String> caseNos;
        private String restoreSQL;
    }

    // ========================================================================
    // 服務元件區
    // ========================================================================
    @Autowired
    private transient OrgTrnsService orgTrnsService;
    @Autowired
    private transient PmisHelper pmisHelper;
    @Autowired
    private transient NativeSqlRepository nativeSqlRepository;
    @Autowired
    private transient TrnsBackupRepository trnsBackupRepository;
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private transient JdbcTemplate jdbcTemplate;

    // ========================================================================
    // 查詢方法入口區
    // ========================================================================
    /**
     * @param createDepSid
     * @param transProgramType
     * @return
     * @throws UserMessageException
     */
    public List<OrgTrnsDtVO> query(
            Integer createDepSid,
            RequireTransProgramType transProgramType) throws UserMessageException {

        // ====================================
        // 取得部門
        // ====================================
        Org createDep = WkOrgCache.getInstance().findBySid(createDepSid);
        if (createDep == null) {
            throw new UserMessageException("查不到傳入的單位資料 sid:[" + createDepSid + "]", InfomationLevel.WARN);
        }

        // ====================================
        // 查詢
        // ====================================
        // 兜組查詢SQL
        String sql = this.prepareQuerySQL(createDepSid + "", transProgramType, OrgTransSqlType.DETAIL);

        // 查詢
        List<OrgTrnsDtVO> results = this.nativeSqlRepository.getResultList(
                sql.toString(), null, OrgTrnsDtVO.class);

        // ====================================
        // 準備顯示欄位
        // ====================================
        this.orgTrnsService.prepareShowInfo(results, transProgramType);

        return results;
    }

    /**
     * 計算筆數
     * 
     * @param orgTrnsWorkVerifyVO 單位轉換前後資料
     * @param transProgramType    RequireTransProgramType
     * @return
     * @throws UserMessageException
     */
    public Integer count(
            OrgTrnsWorkVerifyVO orgTrnsWorkVerifyVO,
            RequireTransProgramType transProgramType) throws UserMessageException {

        // ====================================
        // 取得部門
        // ====================================
        Org createDep = WkOrgCache.getInstance().findBySid(orgTrnsWorkVerifyVO.getBeforeOrgSid());
        if (createDep == null) {
            throw new UserMessageException("查不到傳入的單位資料 sid:[" + orgTrnsWorkVerifyVO.getBeforeOrgSid() + "]", InfomationLevel.WARN);
        }

        // ====================================
        // 依據類別查詢
        // ====================================
        // 兜組查詢SQL
        String sql = this.prepareQuerySQL(createDep.getSid() + "", transProgramType, OrgTransSqlType.COUNT);

        // 查詢
        Number number = this.jdbcTemplate.queryForObject(sql, Integer.class);
        return (number != null ? number.intValue() : 0);
    }

    /**
     * 查詢要轉檔的單據 sid
     * 
     * @param orgTransMappingTo
     * @return case sid list
     */
    public List<String> findNeedTransCaseSids(
            OrgTransMappingTo orgTransMappingTo,
            RequireTransProgramType transProgramType) {
        // 兜組 查詢 SQL
        String sql = this.prepareQuerySQL(
                orgTransMappingTo.getBeforeOrgSid() + "",
                transProgramType,
                OrgTransSqlType.ALLTRANS);
        // 查詢要 update 的單據
        return this.jdbcTemplate.queryForList(sql, String.class);
    }

//    private String prepareQuerySQL(
//            String createDepSidStr,
//            RequireTransProgramType transProgramType,
//            OrgTransSqlType sqlType) {
//
//        String sql = this.prepareQuerySQLx(createDepSidStr, transProgramType, sqlType);
//        log.error(""
//                + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql) + "\r\n】"
//                + "\r\n");
//
//        return sql;
//    }

    /**
     * 準備子單查詢的 SQL
     * 
     * @param createDepSidStr
     * @param transProgramType
     * @param isCount
     * @return
     */
    private String prepareQuerySQL(
            String createDepSidStr,
            RequireTransProgramType transProgramType,
            OrgTransSqlType sqlType) {

        // ====================================
        // 兜組筆數查詢 SQL
        // ====================================
        if (OrgTransSqlType.COUNT.equals(sqlType)) {
            return ""
                    + "SELECT COUNT(*) "
                    + "  FROM " + transProgramType.getTableName() + " "
                    + " WHERE " + transProgramType.getCreateDepColName() + " = " + createDepSidStr + "; ";
        }

        // ====================================
        // 計算全部筆數用
        // ====================================
        if (OrgTransSqlType.ALL_COUNT.equals(sqlType)) {
            return ""
                    + "SELECT " + transProgramType.getCreateDepColName() + " as createDepSid, "
                    + "           COUNT(*)                                   as cnt "
                    + "  FROM " + transProgramType.getTableName() + " "
                    + " WHERE " + transProgramType.getCreateDepColName() + " IN (" + createDepSidStr + ") "
                    + " GROUP BY  " + transProgramType.getCreateDepColName() + "; ";
        }

        // ====================================
        // 全部轉檔用
        // ====================================
        if (OrgTransSqlType.ALLTRANS.equals(sqlType)) {
            return ""
                    + "SELECT " + transProgramType.getSidColName() + " as sid "
                    + "  FROM " + transProgramType.getTableName() + " "
                    + " WHERE " + transProgramType.getCreateDepColName() + " = " + createDepSidStr + "; ";
        }

        // ====================================
        // 兜組明細查詢 SQL
        // ====================================
        if (OrgTransSqlType.DETAIL.equals(sqlType)) {

            if (RequireTransProgramType.REQUIRE.equals(transProgramType)) {
                StringBuffer sql = new StringBuffer();
                sql.append("SELECT tr.require_sid            as sid, ");
                sql.append("       tr.require_no             as caseNo, ");
                sql.append("       tid.field_content         as theme, ");
                sql.append("       tr.create_dt              as createDate_src, ");
                sql.append("       tr.create_usr             as createUserSid, ");
                sql.append("       tr.require_status         as caseStatus_Src ");
                sql.append("FROM   tr_require tr ");

                sql.append("INNER JOIN (SELECT tid.require_sid, ");
                sql.append("                   tid.field_content ");
                sql.append("            FROM   tr_index_dictionary tid ");
                sql.append("            WHERE  1 = 1 ");
                sql.append("                   AND tid.field_name = '主題') AS tid ");
                sql.append("        ON tr.require_sid = tid.require_sid ");

                sql.append("WHERE tr.dep_sid = " + createDepSidStr + " ");
                sql.append("ORDER BY tr.require_no ASC ");

                return sql.toString();

            } else {
                StringBuffer sql = new StringBuffer();
                sql.append("SELECT " + transProgramType.getSidColName() + "            as sid, ");
                sql.append("       " + transProgramType.getCaseNoColName() + "         as caseNo, ");
                sql.append("       " + transProgramType.getThemeColName() + "          as theme, ");
                sql.append("       create_dt                                           as createDate_src, ");
                sql.append("       create_usr                                          as createUserSid, ");
                sql.append("       " + transProgramType.getStatusColName() + "         as caseStatus_Src, ");
                sql.append("       " + transProgramType.getMasterCaseNoColName() + "   as masterNo ");

                sql.append(" FROM " + transProgramType.getTableName() + "  ");
                sql.append("WHERE " + transProgramType.getCreateDepColName() + " = " + createDepSidStr + " ");
                sql.append("ORDER BY " + transProgramType.getStatusColName() + " ASC; ");
                return sql.toString();
            }

        }

        throw new SystemDevelopException("沒有定義的 SQL 類別:[" + sqlType + "]", InfomationLevel.ERROR);

    }

    // ========================================================================
    // 轉檔方法區
    // ========================================================================
    @Transactional(rollbackFor = Exception.class)
    public int processAllTrns(
            OrgTransMappingTo orgTransMappingTo,
            RequireTransProgramType transProgramType,
            Integer execUserSid) throws UserMessageException {

        // ====================================
        // 查詢要轉檔的資料
        // ====================================
        // 兜組 查詢 SQL
        String sql = this.prepareQuerySQL(orgTransMappingTo.getBeforeOrgSid() + "", transProgramType, OrgTransSqlType.ALLTRANS);
        // 查詢要 update 的單據 sid
        List<String> caseSids = this.jdbcTemplate.queryForList(sql, String.class);

        if (WkStringUtils.isEmpty(caseSids)) {
            String transInfo = "【" + WkOrgUtils.findNameBySid(orgTransMappingTo.getBeforeOrgSid()) + "】-【" + transProgramType.getDescr() + "】：已經沒有可以轉檔的資料";
            throw new UserMessageException(transInfo, InfomationLevel.INFO);
        }

        // ====================================
        // 開始轉檔
        // ====================================
        return this.processTranBySids(orgTransMappingTo, transProgramType, Sets.newHashSet(caseSids), execUserSid);
    }

    /**
     * @param orgTransMappingTo
     * @param transProgramType
     * @param caseSids
     * @param execUserSid
     * @return
     * @throws UserMessageException
     */
    @Transactional(rollbackFor = Exception.class)
    public int processTranBySids(
            OrgTransMappingTo orgTransMappingTo,
            RequireTransProgramType transProgramType,
            Set<String> caseSids,
            Integer execUserSid) throws UserMessageException {

        if (WkStringUtils.isEmpty(caseSids)) {
            return 0;
        }

        log.info("\r\n開始進行 [" + transProgramType.getDescr() + "] 建立單位 轉檔 \r\n"
                + "來源單位: " + WkOrgUtils.prepareBreadcrumbsByDepName(orgTransMappingTo.getBeforeOrgSid(), OrgLevel.DIVISION_LEVEL, false, "-") + "\r\n"
                + "轉入單位: " + WkOrgUtils.prepareBreadcrumbsByDepName(orgTransMappingTo.getAfterOrgSid(), OrgLevel.DIVISION_LEVEL, false, "-") + "\r\n"
                + "共" + caseSids.size() + "筆");

        // ====================================
        // update create department
        // ====================================
        int rows = this.update(
                transProgramType,
                orgTransMappingTo.getAfterOrgSid(),
                Sets.newHashSet(caseSids));

        log.info("更新成功，共" + rows + "筆");

        // ====================================
        // save tr_trns_backup
        // ====================================
        this.processCreateBackupForAllTrans(
                orgTransMappingTo,
                transProgramType,
                Sets.newHashSet(caseSids),
                execUserSid,
                new Date());

        return rows;
    }

    /**
     * 需求單轉檔 + 同步PMIS
     * 
     * @param orgTransMappingTo
     * @param transProgramType
     * @param requireSid
     * @param execUserSid
     * @throws UserMessageException
     */
    @Transactional(rollbackFor = Exception.class)
    public void processRequireTranBySidWithPMIS(
            OrgTransMappingTo orgTransMappingTo,
            RequireTransProgramType transProgramType,
            String requireSid,
            Integer execUserSid) throws UserMessageException {

        // ====================================
        // 轉檔
        // ====================================
        this.processTranBySids(
                orgTransMappingTo,
                transProgramType,
                Sets.newHashSet(requireSid),
                execUserSid);

        // ====================================
        // transferToPMIS
        // ====================================
        // 最後才發PMIS , 失敗時可roll back 前方異動資料
        this.pmisHelper.transferToPMIS(requireSid, execUserSid);

    }

    @Transactional(rollbackFor = Exception.class)
    public int processDetailTrns(
            OrgTransMappingTo orgTransMappingTo,
            Integer execUserSid,
            RequireTransProgramType transProgramType,
            List<OrgTrnsDtVO> selectedDtVOList) {

        if (WkStringUtils.isEmpty(selectedDtVOList)) {
            return 0;
        }

        Date sysdate = new Date();

        log.info("\r\n開始進行 [" + transProgramType.getDescr() + "] 建立單位 轉檔 \r\n"
                + "來源單位: " + WkOrgUtils.prepareBreadcrumbsByDepName(orgTransMappingTo.getBeforeOrgSid(), OrgLevel.DIVISION_LEVEL, false, "-") + "\r\n"
                + "轉入單位: " + WkOrgUtils.prepareBreadcrumbsByDepName(orgTransMappingTo.getAfterOrgSid(), OrgLevel.DIVISION_LEVEL, false, "-") + "\r\n"
                + "共" + selectedDtVOList.size() + "筆");

        // ====================================
        // prepare update keys
        // ====================================
        Set<String> sids = Sets.newHashSet(
                selectedDtVOList.stream()
                        .map(OrgTrnsDtVO::getSid)
                        .collect(Collectors.toList()));

        // ====================================
        // update create department
        // ====================================
        int rows = this.update(
                transProgramType,
                orgTransMappingTo.getAfterOrgSid(),
                sids);

        // ====================================
        // save tr_trns_backup
        // ====================================
        this.processCreateBackupForDetailTrans(
                orgTransMappingTo,
                transProgramType,
                selectedDtVOList,
                execUserSid,
                sysdate);

        log.info("轉檔已完成!");
        return rows;
    }

    /**
     * update
     * 
     * @param transProgramType  轉檔類型
     * @param setCreateSidValue 轉換後建立單位 sid
     * @param caseSids          單據 sid
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    private int update(
            RequireTransProgramType transProgramType,
            Integer setCreateSidValue,
            Set<String> caseSids) {

        if (WkStringUtils.isEmpty(caseSids)) {
            log.warn("傳入的 update 條件為空，不執行");
            return 0;
        }

        Long startTime = System.currentTimeMillis();
        String procTitle = "UPDATE " + transProgramType.getTableName();
        log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

        // 兜組 SQL
        String updateSQL = this.prepareUpdateSQL(transProgramType, setCreateSidValue, caseSids);
        // update
        int rows = this.jdbcTemplate.update(updateSQL);

        log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle + "共[" + rows + "]筆"));

        return rows;
    }

    /**
     * 準備 update SQL
     * 
     * @param transProgramType  轉檔類型
     * @param setCreateSidValue 轉換後建立單位 sid
     * @param caseSids          單據 sid
     * @return
     */
    private String prepareUpdateSQL(
            RequireTransProgramType transProgramType,
            Integer setCreateSidValue,
            Set<String> caseSids) {
        return ""
                + "UPDATE " + transProgramType.getTableName() + " "
                + "   SET " + transProgramType.getCreateDepColName() + " = " + setCreateSidValue + " "
                + " WHERE " + transProgramType.getSidColName() + " IN ('"
                + String.join("', '", caseSids)
                + "');";
    }

    /**
     * 建立轉檔備份
     * 
     * @param orgTransMappingTo 部門轉換對應設定資料
     * @param transProgramType  轉換程式 type
     * @param selectedDtVOList  選擇轉檔的資料
     * @param execUserSid       執行者 sid
     * @param sysdate           系統日期
     */
    @Transactional(rollbackFor = Exception.class)
    private void processCreateBackupForDetailTrans(
            OrgTransMappingTo orgTransMappingTo,
            RequireTransProgramType transProgramType,
            List<OrgTrnsDtVO> selectedDtVOList,
            Integer execUserSid,
            Date sysdate) {

        // ====================================
        // 準備記錄資料
        // ====================================
        OrgTrnsCreateDepBackupTo backupTo = new OrgTrnsCreateDepBackupTo();
        backupTo.setSids(selectedDtVOList.stream().map(OrgTrnsDtVO::getSid).collect(Collectors.toList()));
        backupTo.setCaseNos(selectedDtVOList.stream().map(OrgTrnsDtVO::getCaseNo).collect(Collectors.toList()));

        Set<String> sids = selectedDtVOList.stream().map(OrgTrnsDtVO::getCaseNo).collect(Collectors.toSet());
        String restoreSQL = this.prepareUpdateSQL(transProgramType, orgTransMappingTo.getBeforeOrgSid(), sids);
        backupTo.setRestoreSQL(restoreSQL);

        // ====================================
        // createTrnsBackup
        // ====================================
        this.createTrnsBackup(
                orgTransMappingTo,
                transProgramType,
                WkJsonUtils.getInstance().toJsonWithOutPettyJson(backupTo),
                execUserSid,
                sysdate);
    }

    /**
     * 建立轉檔備份
     * 
     * @param orgTransMappingTo 部門轉換對應設定資料
     * @param transProgramType  轉換程式 type
     * @param selectedDtVOList  選擇轉檔的資料
     * @param execUserSid       執行者 sid
     * @param sysdate           系統日期
     */
    @Transactional(rollbackFor = Exception.class)
    private void processCreateBackupForAllTrans(
            OrgTransMappingTo orgTransMappingTo,
            RequireTransProgramType transProgramType,
            Set<String> transSids,
            Integer execUserSid,
            Date sysdate) {

        // ====================================
        // 準備復原SQL
        // ====================================
        String restoreSQL = this.prepareUpdateSQL(transProgramType, orgTransMappingTo.getBeforeOrgSid(), transSids);

        // ====================================
        // createTrnsBackup
        // ====================================
        this.createTrnsBackup(
                orgTransMappingTo,
                transProgramType,
                restoreSQL,
                execUserSid,
                sysdate);
    }

    /**
     * 建立轉檔記錄
     * 
     * @param orgTransMappingTo 部門轉換對應設定資料
     * @param transProgramType  轉換程式 type
     * @param backData          備份資料
     * @param execUserSid       執行者 sid
     * @param sysdate           系統日期
     */
    private void createTrnsBackup(
            OrgTransMappingTo orgTransMappingTo,
            RequireTransProgramType transProgramType,
            String backData,
            Integer execUserSid,
            Date sysdate) {

        // ====================================
        // 準備備份資料檔
        // ====================================
        TrnsBackup trnsBackup = new TrnsBackup();
        trnsBackup.setCreatedUser(execUserSid);
        trnsBackup.setCreatedDate(sysdate);
        trnsBackup.setTrnsType(transProgramType.getTrnsType().getValue());
        trnsBackup.setCustKey(this.prepareCustKey(orgTransMappingTo));
        trnsBackup.setBackData(backData);

        // ====================================
        // save tr_trns_backup
        // ====================================
        trnsBackupRepository.save(trnsBackup);

    }

    /**
     * @param orgTransMappingTo
     * @return
     */
    private String prepareCustKey(OrgTransMappingTo orgTransMappingTo) {
        return orgTransMappingTo.getBeforeOrgSid() + "-" + orgTransMappingTo.getAfterOrgSid();
    }

    /**
     * 計算待轉筆數
     * 
     * @param orgTransMappingTos
     * @return
     */
    public Map<String, Integer> countAllWaitTrans(List<OrgTrnsWorkVerifyVO> orgTransMappingTos) {
        if (WkStringUtils.isEmpty(orgTransMappingTos)) {
            return Maps.newHashMap();
        }

        // ====================================
        // 收集轉換前單位
        // ====================================
        // 收集, 並轉為 SQL where in 字串
        String beforeCreateDepSids = orgTransMappingTos.stream()
                .map(to -> to.getBeforeOrgSid() + "")
                .collect(Collectors.joining(", "));

        // ====================================
        // 依據單據類別, 逐項查詢
        // ====================================
        Map<String, Integer> waitTransCountMapByDataKey = Maps.newHashMap();

        for (RequireTransProgramType requireTransProgramType : RequireTransProgramType.values()) {
            // 兜組查詢SQL
            String sql = this.prepareQuerySQL(beforeCreateDepSids, requireTransProgramType, OrgTransSqlType.ALL_COUNT);

            // 查詢
            List<OrgTrnsDtVO> results = this.nativeSqlRepository.getResultList(
                    sql.toString(), null, OrgTrnsDtVO.class);

            if (WkStringUtils.isEmpty(results)) {
                continue;
            }

            // 將有待轉筆數的部門，放入結果 map
            for (OrgTrnsDtVO orgTrnsDtVO : results) {
                String waitCountDataKey = this.orgTrnsService.prepareWaitCountDataKey(
                        requireTransProgramType.name(),
                        orgTrnsDtVO.getCreateDepSid());

                // 0筆不記錄
                if (WkCommonUtils.compareByStr(orgTrnsDtVO.getCnt(), "0")) {
                    continue;
                }

                waitTransCountMapByDataKey.put(waitCountDataKey, orgTrnsDtVO.getCnt().intValue());
            }
        }

        return waitTransCountMapByDataKey;
    }
}
