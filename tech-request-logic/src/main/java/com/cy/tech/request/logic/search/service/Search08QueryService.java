/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.search.view.Search08View;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author jason_h
 */
@Service
@Slf4j
public class Search08QueryService implements QueryService<Search08View>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7035615487715428356L;
    @Autowired
    private URLService urlService;
    @Autowired
    private SearchService searchHelper;
    @Autowired
    transient private OrganizationService orgService;

    @PersistenceContext
    transient private EntityManager em;

    @Override
    public List<Search08View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {

        // ====================================
        // 查詢
        // ====================================

        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        @SuppressWarnings("unchecked")
        List<Object[]> result = query.getResultList();
        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if (WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();
        List<Search08View> viewResult = Lists.newArrayList();
        for (int i = 0; i < result.size(); i++) {
            Search08View v = new Search08View();

            int index = 0;
            Object[] record = (Object[]) result.get(i);
            String sid = (String) record[index++];
            String requireNo = (String) record[index++];
            String requireTheme = (String) record[index++];
            Integer urgency = (Integer) record[index++];
            Date prototypeCreatedDate = (Date) record[index++];
            Integer prototypeCreateDep = (Integer) record[index++];
            Integer prototypeCreatedUser = (Integer) record[index++];
            String bigName = (String) record[index++];
            String middleName = (String) record[index++];
            String smallName = (String) record[index++];
            Integer requireCreateDep = (Integer) record[index++];
            Integer requireCreatedUser = (Integer) record[index++];
            String prototypeStatus = (String) record[index++];
            String instanceStatus = (String) record[index++];
            Date finishDate = (Date) record[index++];
            Date replyDate = (Date) record[index++];
            Integer version = (Integer) record[index++];
            String notifyStaffTo = (String) record[index++];
            String notifyDepsTo = (String) record[index++];
            String readReason = (String) record[index++];
            String requireStatus = (String) record[index++];
            String ptTheme = (String) record[index++];
            String checkNo = (String) record[index++];
            // 處理共通欄位 (一定要擺在『取值』最後面, 否則 index 會不對)
            v.prepareCommonColumn(record, index);

            v.setSid(sid);
            v.setRequireNo(requireNo);
            v.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
            v.setUrgency(UrgencyType.values()[urgency]);
            v.setPrototypeCreatedDate(prototypeCreatedDate);
            v.setPrototypeCreateDep(prototypeCreateDep);
            v.setPrototypeCreateDepName(orgService.getOrgName(prototypeCreateDep));
            v.setPrototypeCreatedUser(prototypeCreatedUser);
            v.setBigName(bigName);
            v.setMiddleName(middleName);
            v.setSmallName(smallName);
            v.setRequireCreateDep(requireCreateDep);
            v.setRequireCreatedUser(requireCreatedUser);
            v.setPrototypeStatus(PtStatus.valueOf(prototypeStatus));
            v.setInstanceStatus(InstanceStatus.fromPaperCode(instanceStatus));
            v.setFinishDate(finishDate);
            v.setReplyDate(replyDate);
            v.setVersion(version);
            try {
                v.setNotifyStaffTo(Strings.isNullOrEmpty(notifyStaffTo)
                        ? null
                        : WkJsonUtils.getInstance().fromJson(notifyStaffTo, JsonStringListTo.class));
            } catch (IOException ex) {
                log.error(ex.getMessage(), ex);
            }
            try {
                v.setNotifyDepsTo(Strings.isNullOrEmpty(notifyDepsTo)
                        ? null
                        : WkJsonUtils.getInstance().fromJson(notifyDepsTo, JsonStringListTo.class));
            } catch (IOException ex) {
                log.error(ex.getMessage(), ex);
            }

            v.setWaitReadReasonType(WaitReadReasonType.safeValueOf(readReason));
            v.setRequireStatus(RequireStatusType.safeValueOf(requireStatus));

            v.setPtTheme(ptTheme);
            v.setLocalUrlLink(urlService.createLocalUrlLinkParamForTab(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(execUserSid, v.getRequireNo(), 1),
                    URLService.URLServiceAttr.URL_ATTR_TAB_PT, v.getSid()));
            v.setCheckNo(checkNo);
            viewResult.add(v);
        }

        // 解析資料-結束
        usageRecord.parserDataEnd();

        return viewResult;
    }
}
