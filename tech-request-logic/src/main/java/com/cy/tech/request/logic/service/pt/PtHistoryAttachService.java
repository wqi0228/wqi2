/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.pt;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.CommonService;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.RequireTraceService;
import com.cy.tech.request.repository.pt.PtAttachmentRepo;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.pt.PtAttachment;
import com.cy.tech.request.vo.pt.PtHistory;
import com.cy.tech.request.vo.pt.enums.PtAttachmentBehavior;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkAttachUtils;
import com.cy.work.common.vo.AttachmentService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;
import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 原型確認歷程 附加檔案服務
 *
 * @author shaun
 */
@Component("pt_history_attach")
public class PtHistoryAttachService implements AttachmentService<PtAttachment, PtHistory, String>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4816909314387215589L;
    @Value("${erp.attachment.root}")
    private String attachPath;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private RequireTraceService traceService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private PtAttachmentRepo attachDao;
    @Autowired
    private WkAttachUtils attachUtils;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

    @Transactional(readOnly = true)
    @Override
    public PtAttachment findBySid(String sid) {
        PtAttachment a = attachDao.findOne(sid);
        return a;
    }

    @Transactional(readOnly = true)
    @Override
    public List<PtAttachment> findAttachsByLazy(PtHistory history) {
        try {
            history.getAttachments().size();
        } catch (LazyInitializationException e) {
            //log.debug("findAttachsByLazy lazy init error :" + e.getMessage());
            history.setAttachments(this.findAttachs(history));
        }
        return history.getAttachments();
    }

    @Transactional(readOnly = true)
    @Override
    public List<PtAttachment> findAttachs(PtHistory e) {
        if (Strings.isNullOrEmpty(e.getSid())) {
            if (e.getAttachments() == null) {
                e.setAttachments(Lists.newArrayList());
            }
            return e.getAttachments();
        }
        return attachDao.findByHistoryAndStatusOrderByCreatedDateDesc(e, Activation.ACTIVE);
    }

    @Override
    public String getAttachPath() {
        return attachPath;
    }

    @Override
    public String getDirectoryName() {
        return "tech-request";
    }

    @Override
    public PtAttachment createEmptyAttachment(String fileName, User executor, Org dep) {
        return (PtAttachment) attachUtils.createEmptyAttachment(new PtAttachment(), fileName, executor, dep);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public PtAttachment updateAttach(PtAttachment attach) {
        PtAttachment a = attachDao.save(attach);
        return a;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveAttach(PtAttachment attach) {
        attachDao.save(attach);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveAttach(List<PtAttachment> attachs) {
        attachDao.save(attachs);
    }

    @Override
    public String depAndUserName(PtAttachment attachment) {
        if (attachment == null) {
            return "";
        }
        User usr = WkUserCache.getInstance().findBySid(attachment.getCreatedUser().getSid());
        String parentDepStr = orgService.showParentDep(WkOrgCache.getInstance().findBySid(usr.getPrimaryOrg().getSid()));
        return parentDepStr + "-" + usr.getName();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void linkRelation(List<PtAttachment> attachments, PtHistory e, User login) {
        attachments.forEach(each -> this.setRelation(each, e));
        this.saveAttach(attachments);
        e.setAttachments(this.findAttachs(e));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void linkRelation(PtAttachment ra, PtHistory e, User login) {
        this.setRelation(ra, e);
        this.updateAttach(ra);
    }

    private void setRelation(PtAttachment ra, PtHistory e) {
        ra.setPtCheck(e.getPtCheck());
        ra.setPtNo(e.getPtNo());
        ra.setSourceType(e.getPtCheck().getSourceType());
        ra.setSourceSid(e.getPtCheck().getSourceSid());
        ra.setSourceNo(e.getPtCheck().getSourceNo());
        ra.setHistory(e);
        ra.setBehavior(this.findBehavior(e));
        ra.setDescSystem(this.createDescSysStr(ra));
        if (!ra.getKeyChecked()) {
            ra.setStatus(Activation.INACTIVE);
        }
        ra.setKeyChecked(Boolean.FALSE);
    }

    private PtAttachmentBehavior findBehavior(PtHistory e) {
        return PtAttachmentBehavior.valueOf(e.getBehavior().name());
    }

    private String createDescSysStr(PtAttachment attach) {
        String behaviorStr = commonService.get(attach.getBehavior());
        return "(" + behaviorStr + " - " + sdf.format(attach.getCreatedDate()) + ")";
    }

    /**
     * 刪除
     *
     * @param attachments
     * @param attachment
     * @param history
     * @param login
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void changeStatusToInActive(List<PtAttachment> attachments, PtAttachment attachment, PtHistory history, User login) {
        PtAttachment ra = (PtAttachment) attachment;
        ra.setStatus(Activation.INACTIVE);
        this.updateAttach(attachment);
        history.setAttachments(this.findAttachs(history));
        this.createDeleteTrace(attachment, history, login);
    }

    private void createDeleteTrace(PtAttachment attachment, PtHistory history, User login) {
        Require require = new Require();
        require.setSid(history.getSourceSid());
        require.setRequireNo(history.getSourceNo());
        String content = ""
                + "檔案名稱：【" + attachment.getFileName() + "】\n"
                + "上傳成員：【" + login.getName() + "】\n"
                + "\n"
                + "刪除來源：【原型確認 － 版本：V" + history.getPtCheck().getVersion() + " － " + commonService.get(history.getBehavior()) + "】\n"
                + "刪除成員：【" + login.getName() + "】\n"
                + "刪除註記：" + attachment.getSid();
        traceService.createRequireTrace(require.getSid(), login, RequireTraceType.DELETE_ATTACHMENT, content);
    }

}
