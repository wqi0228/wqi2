/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.config.exception;

/**
 * @author kasim
 */
public class OthSetEditExceptions extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 5119586483960859994L;

    public OthSetEditExceptions(String message) {
        super(message);
    }

    public OthSetEditExceptions(String message, Throwable cause) {
        super(message, cause);
    }
}
