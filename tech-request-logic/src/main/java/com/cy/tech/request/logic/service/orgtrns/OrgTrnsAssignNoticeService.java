/**
 * 
 */
package com.cy.tech.request.logic.service.orgtrns;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.tech.request.logic.service.AssignSendInfoService;
import com.cy.tech.request.logic.service.Set10V70TrneBatchHelper;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsAssignSendInfoForTrnsTO;
import com.cy.tech.request.logic.service.orgtrns.vo.OrgTrnsDtVO;
import com.cy.tech.request.vo.converter.SetupInfoToConverter;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.require.AssignSendInfoForTrnsVO;
import com.cy.tech.request.vo.require.AssignSendInfoHistory;
import com.cy.tech.request.vo.require.AssignSendSearchInfoVO;
import com.cy.tech.request.vo.value.to.SetupInfoTo;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.value.to.ItemsCollectionDiffTo;
import com.google.common.collect.Lists;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Slf4j
@NoArgsConstructor
@Service
public class OrgTrnsAssignNoticeService {

    // ========================================================================
    // 服務元件區
    // ========================================================================
    @Autowired
    private transient AssignSendInfoService assignSendInfoService;
    @Autowired
    private transient NativeSqlRepository nativeSqlRepository;
    @Autowired
    private transient OrgTrnsBatchHelper orgTrnsBatchHelper;
    @Autowired
    private transient Set10V70TrneBatchHelper set10V70TrneBatchHelper;

    // ========================================================================
    // 方法區
    // ========================================================================

    /**
     * 轉分派單位
     * 
     * @param selectedDtVOList  選擇的資料
     * @param allTrnsDepMapping 單位轉換前後對應關係
     * @param type              轉換類型 (分派/通知)
     * @param sysDate           系統時間
     * @throws SystemDevelopException
     */
    @Transactional(rollbackFor = Exception.class)
    public void trnsAssignSendInfo(
            List<OrgTrnsDtVO> selectedDtVOList,
            Map<Integer, Integer> allTrnsDepMapping,
            AssignSendType type,
            Date sysDate) throws SystemDevelopException {

        // ====================================
        // 準備異動資料
        // ====================================
        List<OrgTrnsAssignSendInfoForTrnsTO> assignSendInfoForTrnsVOs = this.prepareAssignSendInfo(
                selectedDtVOList,
                allTrnsDepMapping,
                type,
                sysDate);

        // ====================================
        // 執行轉檔
        // ====================================
        this.trnsAssignSendInfo(assignSendInfoForTrnsVOs, type, sysDate);

    }

    /**
     * 轉分派單位
     * 
     * @param selectedDtVOList  選擇的資料
     * @param allTrnsDepMapping 單位轉換前後對應關係
     * @param type              轉換類型 (分派/通知)
     * @param sysDate           系統時間
     * @throws SystemDevelopException
     */
    public void trnsAssignSendInfo(
            List<OrgTrnsAssignSendInfoForTrnsTO> assignSendInfoForTrnsVOs,
            AssignSendType type,
            Date sysDate) throws SystemDevelopException {

        // ====================================
        // 更新分派單位檔 tr_assign_send_info
        // ====================================
        this.orgTrnsBatchHelper.batchUpdateAssignSendInfo(assignSendInfoForTrnsVOs, type, sysDate);

        // ====================================
        // 新增異動記錄檔 tr_assign_send_info_history
        // ====================================
        // 收集異動記錄資料
        List<AssignSendInfoHistory> assignSendInfoHistorys = Lists.newArrayList();
        for (OrgTrnsAssignSendInfoForTrnsTO trnsTo : assignSendInfoForTrnsVOs) {
            assignSendInfoHistorys.add(
                    this.createAssignSendInfoHistory(
                            trnsTo.getRequireSid(),
                            trnsTo.getDepModifyResult(),
                            type,
                            sysDate));
        }

        // insert tr_assign_send_info_history
        this.set10V70TrneBatchHelper.batchInsertAssignSendInfoHistorys(assignSendInfoHistorys);

        // ====================================
        // 更新 tr_assign_send_search_info (DELETE-INSERT)
        // ====================================
        // 收集新的
        List<AssignSendSearchInfoVO> insertAssignSendSearchInfoVOs = Lists.newArrayList();
        for (OrgTrnsAssignSendInfoForTrnsTO to : assignSendInfoForTrnsVOs) {
            // 建立資料物件
            List<AssignSendSearchInfoVO> currVOs = this.orgTrnsBatchHelper.createDepSearchInfo(
                    to.getRequireSid(),
                    to.getRequireNo(),
                    1,
                    sysDate,
                    type,
                    to.getNewAssignDepSids());

            if (WkStringUtils.notEmpty(currVOs)) {
                insertAssignSendSearchInfoVOs.addAll(currVOs);
            }
        }

        // 收集要執行的需求單 SID
        Set<String> requireSids = assignSendInfoForTrnsVOs.stream()
                .map(OrgTrnsAssignSendInfoForTrnsTO::getRequireSid)
                .collect(Collectors.toSet());

        // 刪除舊資料 delete from tr_assign_send_search_info where require_sid in
        // (:require_sids)
        this.orgTrnsBatchHelper.batchDeleteAssignSendSearchInfo(requireSids, type);

        // insert tr_assign_send_search_info
        this.set10V70TrneBatchHelper.batchInsertAssignSendSearchInfos(insertAssignSendSearchInfoVOs);
    }

    /**
     * 建立異動檔資料
     * 
     * @param sysDate
     * @return
     * @throws SystemDevelopException
     */
    private AssignSendInfoHistory createAssignSendInfoHistory(
            String requireSid,
            List<List<Integer>> depModifyResult,
            AssignSendType type,
            Date sysDate) throws SystemDevelopException {

        if (depModifyResult == null || depModifyResult.size() < 2) {
            throw new SystemDevelopException("未傳入比對資料");
        }

        String addDepJsonStr = WkJsonUtils.getInstance().toJsonWithOutPettyJson(depModifyResult.get(0));
        String deleteDepJsonStr = WkJsonUtils.getInstance().toJsonWithOutPettyJson(depModifyResult.get(1));

        AssignSendInfoHistory assignSendInfoHistory = new AssignSendInfoHistory();
        assignSendInfoHistory.setRequireSid(requireSid);
        assignSendInfoHistory.setCreatedUser(1);
        assignSendInfoHistory.setCreatedDate(sysDate);

        // 依據傳入資料類型，放入對應欄位 (分派/通知)
        if (AssignSendType.ASSIGN.equals(type)) {
            assignSendInfoHistory.setAddAssignDeps(addDepJsonStr);
            assignSendInfoHistory.setDeleteAssignDeps(deleteDepJsonStr);
        } else {
            assignSendInfoHistory.setAddSendDeps(addDepJsonStr);
            assignSendInfoHistory.setDeleteSendDeps(deleteDepJsonStr);
        }

        return assignSendInfoHistory;
    }

    /**
     * 準備通知/分派轉檔資料
     * 
     * @param selectedDtVOList
     * @param showDtVOList
     * @param sysdate
     * @return
     */
    public List<OrgTrnsAssignSendInfoForTrnsTO> prepareAssignSendInfo(
            List<OrgTrnsDtVO> selectedDtVOList,
            Map<Integer, Integer> allTrnsDepMapping,
            AssignSendType type,
            Date sysdate) {

        // ====================================
        // 查詢分派檔資料
        // ====================================
        // 收集要處理的需求單資料
        Set<String> requireSids = selectedDtVOList.stream()
                .map(OrgTrnsDtVO::getSid)
                .collect(Collectors.toSet());

        if (WkStringUtils.isEmpty(requireSids)) {
            return Lists.newArrayList();
        }

        // 查詢
        List<OrgTrnsAssignSendInfoForTrnsTO> assignSendInfoForTrnsVOs = this.queryAssignSendInfo(
                requireSids,
                null,
                type);

        if (WkStringUtils.isEmpty(selectedDtVOList)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 收集所有要轉換單位資料
        // ====================================
        for (OrgTrnsAssignSendInfoForTrnsTO vo : assignSendInfoForTrnsVOs) {
            // ====================================
            // 計算轉換後資料 new AssignDepSids
            // ====================================
            // 原有的分派單位資料
            List<Integer> oldAssignDepSids = Lists.newArrayList(vo.getDepSids());

            // 轉換後的分派單位資料
            List<Integer> newAssignDepSids = Lists.newArrayList();
            // 計算
            for (Integer depSid : oldAssignDepSids) {
                if (allTrnsDepMapping.containsKey(depSid)) {
                    // 需轉換的單位
                    newAssignDepSids.add(allTrnsDepMapping.get(depSid));
                } else {
                    // 無需轉換的單位
                    newAssignDepSids.add(depSid);
                }
            }

            vo.setNewAssignDepSids(newAssignDepSids);

            // ====================================
            // 比對設定單位差異 for AssignSendInfoHistory
            // ====================================
            ItemsCollectionDiffTo<Integer> assignDepDiffTo = this.assignSendInfoService.prepareModifyDepsInfo(
                    oldAssignDepSids,
                    newAssignDepSids);

            List<List<Integer>> depModifyResult = Lists.newArrayList();
            depModifyResult.add(Lists.newArrayList(assignDepDiffTo.getPlusItems()));
            depModifyResult.add(Lists.newArrayList(assignDepDiffTo.getReduceItems()));

            vo.setDepModifyResult(depModifyResult);
        }

        return assignSendInfoForTrnsVOs;
    }

    /**
     * @param requireSids
     * @param requireNos
     * @param type
     * @return
     */
    public List<OrgTrnsAssignSendInfoForTrnsTO> queryAssignSendInfo(
            Set<String> requireSids,
            Set<String> requireNos,
            AssignSendType type) {

        Long startTime = System.currentTimeMillis();
        String procTitle = "查詢分派檔";
        WkCommonUtils.prepareCostMessageStart(procTitle);

        // ====================================
        // 兜組SQL
        // ====================================
        StringBuffer varname1 = new StringBuffer();
        varname1.append("SELECT asinfo.info_sid            as sid, ");
        varname1.append("       tr.require_sid             as requireSid, ");
        varname1.append("       tr.require_no              as requireNo, ");
        varname1.append("       asinfo.type                as assignSendtype, ");
        varname1.append("       asinfo.info                as depSids_src, ");
        varname1.append("       asinfo.create_usr          as createdUser, ");
        varname1.append("       asinfo.create_dt           as createdDate, ");
        varname1.append("       tr.require_status          as requireStatus ");

        varname1.append("FROM   tr_assign_send_info asinfo ");

        varname1.append("INNER  JOIN tr_require tr ");
        varname1.append("       ON tr.require_sid = asinfo.require_sid ");
        varname1.append("       AND tr.require_no = asinfo.require_no "); // 正式區有錯誤資料 .多+一個 join 條件

        varname1.append("WHERE  asinfo.type = " + type.ordinal() + "   ");
        varname1.append(this.prepareSidAndNoCondition(requireSids, requireNos));
        varname1.append("  AND  asinfo.create_dt = (SELECT MAX(ai.create_dt) AS maxTime ");
        varname1.append("                             FROM tr_assign_send_info ai ");
        varname1.append("                            WHERE asinfo.require_sid = ai.require_sid ");
        varname1.append("                                  AND asinfo.type = ai.type ");
        varname1.append("                            GROUP BY ai.require_sid) ");

        varname1.append("GROUP BY asinfo.require_sid ");
        varname1.append("ORDER BY asinfo.require_no ");

        // 查詢
        List<OrgTrnsAssignSendInfoForTrnsTO> results = nativeSqlRepository.getResultList(
                varname1.toString(), null, OrgTrnsAssignSendInfoForTrnsTO.class);

        if (WkStringUtils.isEmpty(results)) {
            return Lists.newArrayList();
        }

        // 解析 JSON 字串 (僅取得部門部分, 現行資料其他欄位沒用到)
        SetupInfoToConverter setupInfoToConverter = new SetupInfoToConverter();
        for (AssignSendInfoForTrnsVO vo : results) {
            if (WkStringUtils.isEmpty(vo.getDepSids_src())) {
                continue;
            }
            SetupInfoTo setupInfoTo = setupInfoToConverter.convertToEntityAttribute(vo.getDepSids_src());
            if (setupInfoTo != null) {
                vo.setDepSids(this.assignSendInfoService.prepareDeps(setupInfoTo));
            }
        }

        log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle + " [" + results.size() + "] 筆"));
        return results;
    }

    private String prepareSidAndNoCondition(
            Set<String> requireSids,
            Set<String> requireNos) {

        String requireSidsStr = "";
        if (WkStringUtils.notEmpty(requireSids)) {
            requireSidsStr = requireSids.stream()
                    .collect(Collectors.joining("', '", "tr.require_sid IN ('", "')"));
        }

        String requireNosStr = "";
        if (WkStringUtils.notEmpty(requireNos)) {
            requireNosStr = requireNos.stream()
                    .collect(Collectors.joining("', '", "tr.require_no IN ('", "')"));
        }

        // sid 單號, 皆有值
        if (WkStringUtils.notEmpty(requireSidsStr)
                && WkStringUtils.notEmpty(requireNosStr)) {
            return " AND (" + requireSidsStr + " OR " + requireNosStr + " )";
        }
        // 僅sid
        else if (WkStringUtils.notEmpty(requireSidsStr)) {
            return " AND " + requireSidsStr + " ";
        }
        // 僅單號
        else if (WkStringUtils.notEmpty(requireNosStr)) {
            return " AND " + requireNosStr + " ";
        }

        // 兩種都為空時，強制查不到
        return " AND 1=0 ";
    }
}
