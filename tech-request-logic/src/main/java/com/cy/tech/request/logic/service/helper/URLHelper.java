/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.helper;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.AssignSendInfoService;
import com.cy.tech.request.logic.service.BpmService;
import com.cy.tech.request.logic.service.ForwardService;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.SpecificPermissionService;
import com.cy.tech.request.logic.service.onpg.OnpgService;
import com.cy.tech.request.logic.service.othset.OthSetService;
import com.cy.tech.request.logic.service.pt.PtService;
import com.cy.tech.request.logic.service.send.test.SendTestService;
import com.cy.tech.request.logic.service.setting.SettingCheckConfirmRightService;
import com.cy.tech.request.logic.vo.UrlParamTo;
import com.cy.tech.request.repository.require.RequireUnitSignInfoRepository;
import com.cy.tech.request.vo.constants.ReqPermission;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.require.AssignSendInfo;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireUnitSignInfo;
import com.cy.tech.request.vo.require.othset.OthSet;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.backend.logic.WorkBackendParamService;
import com.cy.work.backend.vo.enums.WkBackendParam;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.logic.WkUserAndOrgLogic;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * 網址列服務 協助
 *
 * @author kasim
 */
@Component
@Slf4j
public class URLHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5350039472867703943L;
    @Autowired
    private RequireService requireService;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private AssignSendInfoService assignSendInfoService;
    @Autowired
    private ForwardService forwardService;
    @Autowired
    private PtService ptService;
    @Autowired
    private SendTestService sendTestService;
    @Autowired
    private OnpgService onpgService;
    @Autowired
    private OthSetService othSetService;
    @Autowired
    private RequireUnitSignInfoRepository reqSignInfoDao;
    @Autowired
    private SpecificPermissionService specificPermissionService;
    @Autowired
    private BpmService bpmService;
    @Autowired
    private SettingCheckConfirmRightService settingCheckConfirmRightService;
    @Autowired
    private transient WorkBackendParamService workBackendParamService;
    @Autowired
    transient private WorkBackendParamHelper workBackendParamHelper;
    @Autowired
    transient private WkUserAndOrgLogic wkUserAndOrgLogic;

    /**
     * 檢核相關需求單觀看權限
     * 
     * @param to          UrlParamTo
     * @param loginUser   登入USER
     * @param loginCompID 登入公司 ID
     * @param roleSids    登入 user 角色清單
     * @return
     */
    public Boolean checkPermissions(
            UrlParamTo to,
            User loginUser,
            String loginCompID,
            List<Long> roleSids) {
        String reqNo = to.getReqNo();
        if (Strings.isNullOrEmpty(reqNo)) {
            reqNo = to.getNo();
        }

        return checkPermissions(reqNo, loginUser, loginCompID, roleSids);
    }

    /**
     * 檢核相關需求單觀看權限
     * 
     * @param requireNo   需求單號
     * @param loginUser   登入USER
     * @param loginCompID 登入公司 ID
     * @param roleSids    登入 user 角色清單
     * @return
     */
    public Boolean checkPermissions(
            String requireNo,
            User loginUser,
            String loginCompID,
            List<Long> roleSids) {

        if (loginUser == null) {
            log.warn("loginUser is null");
            return false;
        }

        Require require = requireService.findByReqNo(requireNo);
        if (require == null) {
            log.warn("requireNo not find : [{}]", requireNo);
            return Boolean.FALSE;
        }

        // 取得可閱部門 (主單位+子單位+管理單位+管理單位子單位+特殊可閱部門)
        Set<Integer> canViewDepSids = this.wkUserAndOrgLogic.prepareCanViewDepSids(loginUser.getSid(), true);

        List<Org> spOrgList = specificPermissionService.findSpecificDeptsByRoleSidIn(roleSids);
        List<Integer> spOrgSidsList = spOrgList.stream().map(o -> o.getSid()).collect(Collectors.toList());

        // 具有 需求單專案管理員 角色
        if (WkUserUtils.isUserHasRole(
                loginUser.getSid(),
                loginCompID,
                ReqPermission.ROLE_SYS_ADMIN)) {
            return this.returnAuthInterceptor(requireNo, "admin");
        }

        // GM (一般同仁)
        // REQ-1486 【GM後台維護】新增『GM設定』
        if (workBackendParamHelper.isGM(loginUser.getSid())) {
            return this.returnAuthInterceptor(requireNo, "GM");
        }

        // 具有 需求單專案管理員 角色
        if (WkUserUtils.isUserHasRole(
                loginUser.getSid(),
                loginCompID,
                ReqPermission.ROLE_PROJECT_MGR)) {
            return this.returnAuthInterceptor(requireNo, "需求單專案管理員");
        }

        // 立案人員
        if (WkCommonUtils.compareByStr(require.getCreatedUser().getSid(), loginUser.getSid())) {
            return this.returnAuthInterceptor(requireNo, "登入者為立案人員");
        }

        // 立案單位
        if (canViewDepSids.contains(require.getCreateDep().getSid())) {
            return this.returnAuthInterceptor(requireNo, "可閱『需求單立案單位』");
        }

        // 主責單位
        if (this.checkPermissionsByInCharge(require, loginUser.getSid())) {
            return this.returnAuthInterceptor(requireNo, "主責單位");
        }

        // 分派部門
        if (this.checkPermissionsByAssignDep(require, loginUser, spOrgSidsList)) {
            return this.returnAuthInterceptor(requireNo, "分派部門");
        }

        // 通知部門
        if (this.checkPermissionsByNoticeDep(require, loginUser, spOrgSidsList)) {
            return this.returnAuthInterceptor(requireNo, "通知部門");
        }

        // 可檢查分派
        if (settingCheckConfirmRightService.hasCanCheckUserRight(loginUser.getSid())) {
            return this.returnAuthInterceptor(requireNo, "檢查分派人員");
        }

        // 轉寄部門
        if (this.checkPermissionsByForwardDep(require, loginUser, spOrgSidsList)) {
            return this.returnAuthInterceptor(requireNo, "轉寄部門");
        }

        // 被轉寄人員
        if (this.checkPermissionsByForwardMember(require, loginUser.getSid())) {
            return this.returnAuthInterceptor(requireNo, "被轉寄人員");
        }

        // 送測的填單單位 / 送測的通知單位
        if (this.checkPermissionsByWorkTest(require, loginUser, canViewDepSids)) {
            return this.returnAuthInterceptor(requireNo, "送測的填單單位 / 送測的通知單位");
        }

        // on程式的填單單位 / on程式的通知單位
        if (this.checkPermissionsByOnpg(require, loginUser, canViewDepSids)) {
            return this.returnAuthInterceptor(requireNo, "on程式的填單單位 / on程式的通知單位");
        }

        // 其他資料設定的填單單位 / 其他資料設定的通知單位
        if (this.checkPermissionsByOthSet(require, loginUser, canViewDepSids)) {
            return this.returnAuthInterceptor(requireNo, "其他資料設定的填單單位 / 其他資料設定的通知單位");
        }

        // 原型確認通知對象 / 原型確認通知單位
        if (this.checkPermissionsByPtCheck(require, loginUser, canViewDepSids)) {
            return this.returnAuthInterceptor(requireNo, "原型確認通知對象 / 原型確認通知單位");
        }

        // 需求單位簽核資訊 (BPM)
        if (this.checkPermissionsByReqSignInfo(require, loginUser.getId())) {
            return this.returnAuthInterceptor(requireNo, "需求單位簽核資訊 (BPM)");
        }

        return false;
    }

    public boolean returnAuthInterceptor(String requireNo, String authDesc) {
        if (isShowDebugLog()) {
            log.debug("[{}] can view by [{}]",
                    requireNo,
                    authDesc);
        }
        return true;
    }

    private boolean isShowDebugLog() {
        String text = this.workBackendParamService.findTextByKeyword(WkBackendParam.REQUIRE_DEBUG_AUTH_RIGHT);
        return "Y".equals(text);
    }

    /**
     * 該部門 是否為登入者部門 該部門主管 是否為登入者
     * 
     * @param loginUser
     * @param org
     * @return
     */
    private boolean isSelfOrgOrLeader(List<Integer> orgSids, User loginUser) {
        if (CollectionUtils.isNotEmpty(orgSids)) {
            return orgSids.stream()
                    .flatMap(orgSid -> orgService.findParentOrgsAndSelf(orgSid).stream())
                    .anyMatch(org -> org.getSid().equals(loginUser.getPrimaryOrg().getSid())
                            || (org.getManager() != null && org.getManager().getSid().equals(loginUser.getSid())));
        }
        return false;
    }

    /**
     * 檢核相關需求單觀看權限(需求單分派單位)
     *
     * @param require
     * @param loginDep
     * @param loginUser
     * @return
     */
    private Boolean checkPermissionsByAssignDep(Require require, User loginUser, List<Integer> spOrgList) {
        AssignSendInfo assignSendInfo = assignSendInfoService.findNewestByRequireAndType(require, AssignSendType.ASSIGN);
        if (assignSendInfo != null) {
            // 取得分派單位
            Set<Integer> assignDepts = assignSendInfo.getInfo().getDepartment().stream().map(sid -> Integer.valueOf(sid)).collect(Collectors.toSet());

            // 組織扁平化後. 不在收束到部級
            // 依據分派單位產生需求確認單位 (上層到部級也要可以看)
            // assignDepts.addAll(
            // requireConfirmDepService.prepareRequireConfirmDepSids(
            // Lists.newArrayList(assignDepts)));

            //
            Set<Integer> allChildSids = WkOrgCache.getInstance().findAllChildSids(assignDepts);
            if (WkStringUtils.notEmpty(allChildSids)) {
                assignDepts.addAll(allChildSids);
            }

            // 是否含有特殊可閱角色(部門)權限
            boolean hasSpOrgPermission = CollectionUtils.containsAny(assignDepts, spOrgList);
            if (hasSpOrgPermission) {
                return true;
            }

            // 判斷是否為分派單位的成員或部門主管
            return isSelfOrgOrLeader(Lists.newArrayList(assignDepts), loginUser) || hasSpOrgPermission;
        }
        return false;
    }

    private Boolean checkPermissionsByNoticeDep(Require require, User loginUser, List<Integer> spOrgList) {
        // 查詢通知單位
        AssignSendInfo assignSendInfo = assignSendInfoService.findNewestByRequireAndType(require, AssignSendType.SEND);
        if (assignSendInfo != null) {
            Set<Integer> assignDepts = assignSendInfo.getInfo().getDepartment().stream().map(sid -> Integer.valueOf(sid)).collect(Collectors.toSet());
            // 是否含有特殊可閱角色(部門)權限
            boolean hasSpOrgPermission = CollectionUtils.containsAny(assignDepts, spOrgList);
            return isSelfOrgOrLeader(Lists.newArrayList(assignDepts), loginUser) || hasSpOrgPermission;
        }
        return false;
    }

    /**
     * 檢核相關需求單觀看權限(需求單轉寄單位)
     *
     * @param require
     * @param loginDep
     * @param loginUser
     * @return
     */
    private Boolean checkPermissionsByForwardDep(Require require, User loginUser, List<Integer> spOrgList) {
        List<Integer> receiveDeps = forwardService.findDeptByRequireOrderByCreatedDateDesc(require).stream()
                .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                .map(each -> each.getReceiveDep().getSid())
                .collect(Collectors.toList());

        // 是否含有特殊可閱角色(部門)權限
        boolean hasSpOrgPermission = CollectionUtils.containsAny(receiveDeps, spOrgList);
        return isSelfOrgOrLeader(receiveDeps, loginUser) || hasSpOrgPermission;
    }

    /**
     * 檢核觀看權限 by 個人轉寄
     *
     * @param require
     * @param loginUser
     * @return
     */
    private Boolean checkPermissionsByForwardMember(Require require, Integer loginUser) {
        return forwardService.findMemberByRequireOrderByCreatedDateDesc(require).stream()
                .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                .anyMatch(each -> loginUser.equals(each.getReceive().getSid()));
    }

    /**
     * 檢核相關需求單觀看權限(原型確認通知對象 / 原型確認通知單位)
     *
     * @param require
     * @param loginDep
     * @param loginUser
     * @return
     */
    private Boolean checkPermissionsByPtCheck(
            Require require,
            User loginUser,
            Set<Integer> canViewDepSids) {

        if (loginUser == null || loginUser.getSid() == null || loginUser.getPrimaryOrg() == null) {
            return false;
        }
        Integer loginUserSid = loginUser.getSid();
        Integer loginUserDepSid = loginUser.getPrimaryOrg().getSid();

        // ====================================
        // 查詢單據的送測單
        // ====================================
        List<PtCheck> ptChecks = ptService.findByRequire(require)
                .stream()
                .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                .collect(Collectors.toList());

        if (WkStringUtils.isEmpty(ptChecks)) {
            return false;
        }

        // ====================================
        // 逐筆檢查
        // ====================================

        for (PtCheck ptCheck : ptChecks) {
            // 立案人
            if (ptCheck.getCreatedUser() != null
                    && WkCommonUtils.compareByStr(loginUserSid, ptCheck.getCreatedUser().getSid())) {
                return true;
            }
            // 立案單位可閱 (主要單位(含以下)+管理單位(含以下)+特殊可閱)
            if (ptCheck.getCreateDep() != null
                    && canViewDepSids.contains(ptCheck.getCreateDep().getSid())) {
                return true;
            }

            // 通知人員
            if (ptCheck.getNoticeMemberSids() != null
                    && ptCheck.getNoticeMemberSids().contains(loginUserSid)) {
                return true;
            }

            // 通知單位
            if (ptCheck.getNoticeDepSids() != null
                    && ptCheck.getNoticeDepSids().contains(loginUserDepSid)) {
                return true;
            }
        }

        return false;
    }

    /**
     * 檢核相關需求單觀看權限(送測的填單單位 / 送測的通知單位)
     *
     * @param require
     * @param loginDep
     * @param loginUser
     * @return
     */
    private Boolean checkPermissionsByWorkTest(
            Require require,
            User loginUser,
            Set<Integer> canViewDepSids) {

        if (loginUser == null || loginUser.getSid() == null || loginUser.getPrimaryOrg() == null) {
            return false;
        }
        Integer loginUserSid = loginUser.getSid();
        Integer loginUserDepSid = loginUser.getPrimaryOrg().getSid();

        // ====================================
        // 查詢單據的送測單
        // ====================================
        List<WorkTestInfo> workTestInfos = sendTestService.findByRequire(require).stream()
                .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                .collect(Collectors.toList());

        if (WkStringUtils.isEmpty(workTestInfos)) {
            return false;
        }

        // ====================================
        // 逐筆檢查送測單
        // ====================================

        for (WorkTestInfo workTestInfo : workTestInfos) {
            // 立案人
            if (workTestInfo.getCreatedUser() != null
                    && WkCommonUtils.compareByStr(loginUserSid, workTestInfo.getCreatedUser().getSid())) {
                return true;
            }
            // 立案單位可閱 (主要單位(含以下)+管理單位(含以下)+特殊可閱)
            if (workTestInfo.getCreateDep() != null
                    && canViewDepSids.contains(workTestInfo.getCreateDep().getSid())) {
                return true;
            }

            // 送測單位
            if (workTestInfo.getSendTestDepSids() != null
                    && workTestInfo.getSendTestDepSids().contains(loginUserDepSid)) {
                return true;
            }
        }

        return false;

    }

    /**
     * 檢核相關需求單觀看權限(on程式的填單單位 / on程式的通知單位)
     *
     * @param require
     * @param loginDep
     * @param loginUser
     * @return
     */
    private Boolean checkPermissionsByOnpg(
            Require require,
            User loginUser,
            Set<Integer> canViewDepSids) {

        if (loginUser == null || loginUser.getSid() == null || loginUser.getPrimaryOrg() == null) {
            return false;
        }
        Integer loginUserSid = loginUser.getSid();
        Integer loginUserDepSid = loginUser.getPrimaryOrg().getSid();

        // ====================================
        // 查詢單據
        // ====================================
        List<WorkOnpg> workOnpgs = onpgService.findByRequire(require).stream()
                .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                .collect(Collectors.toList());

        if (WkStringUtils.isEmpty(workOnpgs)) {
            return false;
        }

        // ====================================
        // 逐筆檢查ON程式單
        // ====================================

        for (WorkOnpg workOnpg : workOnpgs) {
            // 立案人
            if (workOnpg.getCreatedUser() != null
                    && WkCommonUtils.compareByStr(loginUserSid, workOnpg.getCreatedUser().getSid())) {
                return true;
            }
            // 立案單位可閱 (主要單位(含以下)+管理單位(含以下)+特殊可閱)
            if (workOnpg.getCreateDep() != null
                    && canViewDepSids.contains(workOnpg.getCreateDep().getSid())) {
                return true;
            }

            // 通知單位
            if (workOnpg.getNoticeDepSids() != null
                    && workOnpg.getNoticeDepSids().contains(loginUserDepSid)) {
                return true;
            }
        }

        return false;
    }

    /**
     * 檢核相關需求單觀看權限(其他資料設定的填單單位 / 其他資料設定的通知單位)
     *
     * @param require
     * @param loginDep
     * @param loginUser
     * @return
     */
    private Boolean checkPermissionsByOthSet(
            Require require,
            User loginUser,
            Set<Integer> canViewDepSids) {

        if (loginUser == null || loginUser.getSid() == null || loginUser.getPrimaryOrg() == null) {
            return false;
        }
        Integer loginUserSid = loginUser.getSid();
        Integer loginUserDepSid = loginUser.getPrimaryOrg().getSid();

        // ====================================
        // 查詢單據
        // ====================================
        List<OthSet> othSets = othSetService.findByRequire(require).stream()
                .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                .collect(Collectors.toList());

        if (WkStringUtils.isEmpty(othSets)) {
            return false;
        }

        // ====================================
        // 逐筆檢查單據
        // ====================================
        for (OthSet othSet : othSets) {
            // 立案人
            if (othSet.getCreatedUser() != null
                    && WkCommonUtils.compareByStr(loginUserSid, othSet.getCreatedUser().getSid())) {
                return true;
            }
            // 立案單位可閱 (主要單位(含以下)+管理單位(含以下)+特殊可閱)
            if (othSet.getCreateDep() != null
                    && canViewDepSids.contains(othSet.getCreateDep().getSid())) {
                return true;
            }

            // 通知單位
            if (othSet.getNoticeDepSids() != null
                    && othSet.getNoticeDepSids().contains(loginUserDepSid)) {
                return true;
            }
        }

        return false;
    }

    /**
     * 需求單位
     */
    private Boolean checkPermissionsByReqSignInfo(Require require, String loginUserId) {
        RequireUnitSignInfo jpaSignInfo = reqSignInfoDao.findByRequire(require);
        if (jpaSignInfo == null
                || jpaSignInfo.getCanSignedIdsTo() == null
                || jpaSignInfo.getCanSignedIdsTo().getValue() == null) {
            return Boolean.FALSE;
        }
        // if login user in BPM SignInfo history
        List<ProcessTaskBase> tasks = bpmService.findFlowChartByInstanceId(jpaSignInfo.getBpmInstanceId(), loginUserId);
        for (ProcessTaskBase task : tasks) {
            if (task instanceof ProcessTaskHistory) {
                ProcessTaskHistory history = (ProcessTaskHistory) task;
                if (history.getExecutorID().equals(loginUserId)) {
                    return Boolean.TRUE;
                }
            }
        }
        return jpaSignInfo.getCanSignedIdsTo().getValue().stream()
                .anyMatch(each -> loginUserId.equals(each));
    }

    /**
     * 檢查是否有主責單位權限
     * 
     * @param require      需求單資料檔
     * @param loginUserSid 登入者 userSid
     * @return
     */
    public boolean checkPermissionsByInCharge(Require require, Integer loginUserSid) {

        // 防呆
        if (require == null) {
            return false;
        }

        // ====================================
        // 比對主要負責人
        // ====================================
        if (WkCommonUtils.compareByStr(require.getInChargeUsr(), loginUserSid)) {
            return true;
        }

        // ====================================
        // 比對主責單位相關成員
        // ====================================
        // 取得主責單位資料
        Integer inChargeDepSid = require.getInChargeDep();
        if (inChargeDepSid == null) {
            return false;
        }

        // 為單位主管 (包含直系向上單位)
        if (WkOrgCache.getInstance().findDirectManagerUserSids(inChargeDepSid).contains(loginUserSid)) {
            return true;
        }

        // 取得部門所有 user
        List<User> depUsers = WkUserCache.getInstance().findUsersByPrimaryOrgSid(inChargeDepSid);
        // 比對是否主責單位成員
        for (User user : depUsers) {
            if (WkCommonUtils.compareByStr(user.getSid(), loginUserSid)) {
                return true;
            }
        }
        return false;
    }
}
