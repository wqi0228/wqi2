/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import com.cy.tech.request.logic.search.enums.ReqSubType;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * 需求製作進度 (search26.xhtml)
 *
 * @author jason_h
 */
@Getter
@Setter
public class Search26BaseView extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6803418994348332448L;
    /** 需求單 sid */
    private String reqSid;
    /** 需求單號 */
    private String reqNo;
    /** 子程序型態 */
    private ReqSubType subType;
    /** 子程序 sid */
    private String subSid;
    /** 上下筆要用的索引 */
    private Integer index;
    /** 連結網址 */
    private String localUrlLink;
}
