/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.pt;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.RequireReadRecordHelper;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.repository.pt.PtAlreadyReplyRepo;
import com.cy.tech.request.repository.pt.PtReplyRepo;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.pt.PtAlreadyReply;
import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.pt.PtReply;
import com.cy.tech.request.vo.pt.enums.PtHistoryBehavior;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkJsoupUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.primitives.Longs;

import lombok.extern.slf4j.Slf4j;

/**
 * 原型確認回覆處理
 *
 * @author shaun
 */
@Component
@Slf4j
public class PtReplyService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4344712160929382395L;
    @Autowired
    private RequireService reqService;
    @Autowired
    private PtService stService;
    @Autowired
    private PtShowService ptsService;
    @Autowired
    private PtAlertService staService;
    @Autowired
    private PtHistoryService sthService;
    @Autowired
    private transient RequireReadRecordHelper requireReadRecordHelper;
    @Autowired
    private WkJsoupUtils jsoupUtils;
    @Autowired
    private PtReplyRepo replyDao;
    @Autowired
    private PtAlreadyReplyRepo aReplyDao;

    public List<PtReply> findReply(PtCheck ptCheck) {
        try {
            ptCheck.getReplys().size();
        } catch (LazyInitializationException e) {
            //log.debug("findReply lazy init error :" + e.getMessage(), e);
            ptCheck.setReplys(replyDao.findByPtCheck(ptCheck));
        }
        return ptCheck.getReplys();
    }

    /**
     * 尋找原型確認對應的回覆內容
     *
     * @param reply
     * @return
     */
    public List<PtAlreadyReply> findAlreadyReplys(PtReply reply) {
        if (reply == null || Strings.isNullOrEmpty(reply.getSid())) {
            return Lists.newArrayList();
        }
        try {
            reply.getAlreadyReplys().size();
        } catch (LazyInitializationException e) {
            //log.debug("findAlreadyReplys lazy init error :" + e.getMessage(), e);
            reply.setAlreadyReplys(aReplyDao.findByReplyOrderByUpdateDateDesc(reply));
        }
        return reply.getAlreadyReplys();
    }

    public PtReply createEmptyReply(PtCheck info, User executor) {
        PtReply reply = new PtReply();
        reply.setPtCheck(info);
        reply.setPtNo(info.getPtNo());
        reply.setSourceType(info.getSourceType());
        reply.setSourceSid(info.getSourceSid());
        reply.setSourceNo(info.getSourceNo());
        reply.setDep(WkOrgCache.getInstance().findBySid(executor.getPrimaryOrg().getSid()));
        reply.setPerson(executor);
        reply.setDate(new Date());
        reply.setUpdateDate(new Date());
        reply.setHistory(sthService.createEmptyHistory(info, executor));
        reply.getHistory().setBehavior(PtHistoryBehavior.REPLY);
        return reply;
    }

    /**
     * 新增原型確認回覆
     * @param editReply
     * @throws IllegalAccessException
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveByNewReply(PtReply editReply) throws IllegalAccessException {
        if (ptsService.disableReply(
                reqService.findByReqSid(editReply.getSourceSid()),
                stService.findByPtNo(editReply.getPtNo()),
                editReply.getPerson())) {
            throw new IllegalAccessException("無權限進行操作");
        }
        editReply.setContent(jsoupUtils.clearCssTag(editReply.getContentCss()));
        log.info("saveByNewReply1:[" + editReply.getPtCheck().getEstablishDate()+"]");
        PtReply nR = replyDao.save(editReply);
        PtCheck ptCheck = stService.copy(editReply.getPtCheck());
        PtStatus ptStatus = ptCheck.getPtStatus();
        if (!ptStatus.equals(PtStatus.REDO)
                && !ptStatus.equals(PtStatus.FUNCTION_CONFORM)) {
            ptCheck.setPtStatus(PtStatus.PROCESS);
        }
        ptCheck.setReadReason(WaitReadReasonType.PROTOTYPE_HAS_REPLY);
        ptCheck.setReadUpdateDate(new Date());
        log.info("saveByNewReply2:[" +  nR.getPtCheck().getEstablishDate()+"]");
        sthService.createReplyHistory(nR, editReply.getPerson());
        log.info("saveByNewReply3:[" +  ptCheck.getEstablishDate()+"]");
        stService.save(ptCheck, editReply.getPerson());
        sthService.sortHistory(ptCheck);
        
        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.PTCHECK,
                ptCheck.getSid(),
                editReply.getPerson().getSid());
    }

    /**
     * 編輯原型確認回覆
     * @param editReply
     * @param executor
     * @throws IllegalAccessException
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveByEditReply(PtReply editReply, User executor) throws IllegalAccessException {
        if (ptsService.disableReply(
                reqService.findByReqSid(editReply.getSourceSid()),
                stService.findByPtNo(editReply.getPtNo()),
                editReply.getPerson())
                || aReplyDao.findByReplyOrderByUpdateDateDesc(editReply).size() > 0) {
            throw new IllegalAccessException("無權限進行操作");
        }
        editReply.setContent(jsoupUtils.clearCssTag(editReply.getContentCss()));
        editReply.setUpdateDate(new Date());
        sthService.update(editReply.getHistory(), executor);
        PtCheck ptCheck = stService.copy(editReply.getPtCheck());
        ptCheck.setReadUpdateDate(new Date());
        replyDao.save(editReply);
        stService.save(ptCheck, editReply.getPerson());
        sthService.sortHistory(ptCheck);
        
        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.PTCHECK,
                ptCheck.getSid(),
                editReply.getPerson().getSid());
    }

    public PtAlreadyReply createEmptyAlreadyReply(PtReply reply, User executor) {
        PtAlreadyReply areply = new PtAlreadyReply();
        areply.setSourceType(reply.getSourceType());
        areply.setPtCheck(reply.getPtCheck());
        areply.setPtNo(reply.getPtNo());
        areply.setSourceSid(reply.getSourceSid());
        areply.setSourceNo(reply.getSourceNo());
        areply.setReply(reply);
        areply.setDep(WkOrgCache.getInstance().findBySid(executor.getPrimaryOrg().getSid()));
        areply.setPerson(executor);
        areply.setDate(new Date());
        areply.setUpdateDate(new Date());
        areply.setHistory(sthService.createEmptyHistory(reply.getPtCheck(), executor));
        areply.getHistory().setBehavior(PtHistoryBehavior.REPLY_AND_REPLY);
        return areply;
    }

    /**
     * 儲存新回覆
     *
     * @param aReply
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveByNewAlreadyReply(PtAlreadyReply aReply) {
        aReply.setContent(jsoupUtils.clearCssTag(aReply.getContentCss()));
        aReply.setSid(aReplyDao.save(aReply).getSid());
        sthService.createAlreadyReplyHistory(aReply, aReply.getPerson());
        staService.carateReplyAlert(aReply);

        PtReply reply = aReply.getReply();
        this.findAlreadyReplys(reply).add(aReply);
        this.sortAlreadyReplys(aReply.getReply());

        PtCheck ptCheck = aReply.getPtCheck();
        ptCheck.setReadReason(WaitReadReasonType.PROTOTYPE_HAS_REPLY);
        ptCheck.setReadUpdateDate(new Date());
        stService.save(ptCheck, aReply.getPerson());
        sthService.sortHistory(ptCheck);
        
        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.PTCHECK,
                ptCheck.getSid(),
                aReply.getPerson().getSid());
    }

    /**
     * 儲存編輯回覆
     *
     * @param eaReply
     * @param executor
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveByEditAlreadyReply(PtAlreadyReply eaReply, User executor) {
        eaReply.setContent(jsoupUtils.clearCssTag(eaReply.getContentCss()));
        eaReply.setUpdateDate(new Date());
        sthService.update(eaReply.getHistory(), executor);
        aReplyDao.save(eaReply);
        PtCheck ptCheck = eaReply.getPtCheck();
        stService.save(ptCheck, eaReply.getPerson());
        this.sortAlreadyReplys(eaReply.getReply());
        
        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.PTCHECK,
                ptCheck.getSid(),
                eaReply.getPerson().getSid());
    }

    /**
     * 重新排序回覆
     *
     * @param reply
     */
    private void sortAlreadyReplys(PtReply reply) {
        List<PtAlreadyReply> replys = this.findAlreadyReplys(reply);
        Ordering<PtAlreadyReply> byReplyUpDateOrdering = new Ordering<PtAlreadyReply>() {
            @Override
            public int compare(PtAlreadyReply left, PtAlreadyReply right) {
                return Longs.compare(right.getUpdateDate().getTime(), left.getUpdateDate().getTime());
            }
        };
        Collections.sort(replys, byReplyUpDateOrdering);
    }
}
