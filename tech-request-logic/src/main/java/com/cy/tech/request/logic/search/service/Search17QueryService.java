/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.search.view.Search17View;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author jason_h
 */
@Service
@Slf4j
public class Search17QueryService implements QueryService<Search17View>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3157368684760730454L;
    @Autowired
    private URLService urlService;
    @Autowired
    private WkJsonUtils jsonUtils;
    @Autowired
    private SearchService searchHelper;
    @PersistenceContext
    transient private EntityManager em;

    @Override
    public List<Search17View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {
        // ====================================
        // 查詢
        // ====================================

        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        @SuppressWarnings("unchecked")
        List<Object[]> result = query.getResultList();
        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if (WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();

        List<Search17View> viewResult = Lists.newArrayList();
        for (int i = 0; i < result.size(); i++) {
            Search17View v = new Search17View();

            Object[] record = (Object[]) result.get(i);
            int index = 0;
            String sid = (String) record[index++];
            String requireNo = (String) record[index++];
            String requireTheme = (String) record[index++];
            Integer urgency = (Integer) record[index++];
            Integer requireDep = (Integer) record[index++];
            Date requireDate = (Date) record[index++];
            Integer requireUser = (Integer) record[index++];
            String bigName = (String) record[index++];
            String middleName = (String) record[index++];
            String smallName = (String) record[index++];
            String requireStatus = (String) record[index++];
            Date createdDate = (Date) record[index++];
            Integer createDep = (Integer) record[index++];
            Integer createdUser = (Integer) record[index++];
            String onpgTheme = (String) record[index++];
            String noticeDep = (String) record[index++];
            String onpgNo = (String) record[index++];
            String behaviorStatus = (String) record[index++];
            Date onpgHistoryCreatedDate = (Date) record[index++];
            Integer onpgHistoryCreatedUser = (Integer) record[index++];
            String reason = (String) record[index++];
            String onpgSid = (String) record[index++];
            // 處理共通欄位 (一定要擺在『取值』最後面, 否則 index 會不對)
            v.prepareCommonColumn(record, index);

            v.setSid(sid);
            v.setRequireNo(requireNo);
            v.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
            v.setUrgency(UrgencyType.values()[urgency]);
            v.setRequireDep(requireDep);
            v.setRequireDate(requireDate);
            v.setRequireUser(requireUser);
            v.setBigName(bigName);
            v.setMiddleName(middleName);
            v.setSmallName(smallName);
            v.setRequireStatus(RequireStatusType.valueOf(requireStatus));
            v.setCreatedDate(createdDate);
            v.setCreateDep(createDep);
            v.setCreateDepName(WkOrgUtils.findNameBySid(createDep));
            v.setCreatedUser(createdUser);
            v.setOnpgTheme(onpgTheme);
            try {
                v.setNoticeDep(jsonUtils.fromJson(noticeDep, JsonStringListTo.class));
            } catch (IOException ex) {
                log.error(ex.getMessage(), ex);
            }
            v.setOnpgNo(onpgNo);
            v.setBehaviorStatus(WorkOnpgStatus.valueOf(behaviorStatus));
            v.setOnpgHistoryCreatedDate(onpgHistoryCreatedDate);
            v.setOnpgHistoryCreatedUser(onpgHistoryCreatedUser);
            v.setReason(reason);
            v.setOnpgSid(onpgSid);
            v.setLocalUrlLink(urlService.createLocalUrlLinkParamForTab(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(execUserSid, v.getRequireNo(), 1),
                    URLService.URLServiceAttr.URL_ATTR_TAB_OP, onpgSid));
            viewResult.add(v);
        }
        // 解析資料-結束
        usageRecord.parserDataEnd();
        return viewResult;
    }
}
