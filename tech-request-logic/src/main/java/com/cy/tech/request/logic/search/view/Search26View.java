/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import com.cy.tech.request.logic.search.enums.ReqSubType;
import com.cy.work.common.enums.UrgencyType;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 需求製作進度 (search26.xhtml)
 *
 * @author jason_h
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Search26View extends Search26BaseView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -9079175415024557609L;
    /** 需求單 sid */
    private String reqSid;
    /** 需求單號 */
    private String reqNo;
    /** 需求單主題 */
    private String requireTheme;
    /** 緊急度 */
    private UrgencyType urgency;
    /** 立單日期 */
    private Date reqCreDt;
    /** 需求製作進度 */
    private String reqStatus;
    /** 期望完成日 */
    private Date reqHopeDt;
    /** 需求完成日 */
    private Date reqFinishDt;
    /** 結案日 */
    private Date reqCloseDt;
    /** 需求單位 */
    private String reqCreateDep;
    /** 需求人員 */
    private String reqCreatedUser;
    /** 統整人員 */
    private String inteStaffUser;

    /** 分派單位 */
    private String assignDepsStr;
    /** 需求單執行天數 */
    private int reqExecDays;

    /** 子程序型態 */
    private ReqSubType subType;
    /** 子程序 sid */
    private String subSid;
    /** 子程序 主題 */
    private String subTheme;
    /** 子程序 通知字串 */
    private String subNoticeStr;
    /** 子程序 立單日期 */
    private Date subCreDt;
    /** 子程序 狀態 */
    private String subStatus;
    /** 子程序 預計完成日 */
    private Date subEsDt;

    /** 上下筆要用的索引 */
    private Integer index;
    /** 連結網址 */
    private String localUrlLink;
}
