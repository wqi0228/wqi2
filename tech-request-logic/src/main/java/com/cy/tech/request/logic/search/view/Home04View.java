/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import com.cy.tech.request.vo.require.AlertInboxSendGroup;
import com.cy.work.common.enums.UrgencyType;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 收呈報轉發頁面顯示vo (home04.xhtml)
 *
 * @author kasim
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Home04View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -791118956567514826L;
    /** 緊急度 */
    private UrgencyType urgency;
    /** 部 */
    private Boolean hasForwardDep;
    /** 個 */
    private Boolean hasForwardMember;
    /** 關 */
    private Boolean hasLink;
    /** 寄件備份 */
    private AlertInboxSendGroup inboxSendGroup;
    /** 寄發單位 */
    private Integer senderDep;
    /** 寄發者 */
    private Integer sender;
    /** 寄發時間 */
    private Date createdDate;
    /** 類型 */
    private String bigName;
    /** 需求單-異動日期 */
    private Date requireUpdatedDate;
    /** 本地端連結網址 */
    private String localUrlLink;

}
