/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.othset;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.repository.require.othset.OthSetHistoryRepo;
import com.cy.tech.request.vo.enums.OthSetHistoryBehavior;
import com.cy.tech.request.vo.enums.OthSetStatus;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.othset.OthSet;
import com.cy.tech.request.vo.require.othset.OthSetAlreadyReply;
import com.cy.tech.request.vo.require.othset.OthSetAttachment;
import com.cy.tech.request.vo.require.othset.OthSetHistory;
import com.cy.tech.request.vo.require.othset.OthSetReply;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.vo.AttachmentService;
import com.google.common.collect.Ordering;
import com.google.common.primitives.Longs;
import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shaun
 */
@Component
public class OthSetHistoryService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6867933374492234906L;
    @Autowired
    private RequireService reqService;
    @Autowired
    private OthSetService osService;
    @Autowired
    private OthSetShowService ossService;
    @Autowired
    private OthSetAlertService osaService;
    @Autowired
    @Qualifier("othset_history_attach")
    private AttachmentService<OthSetAttachment, OthSetHistory, String> attachService;
    @Autowired
    private WkJsoupUtils jsoupUtils;
    @Autowired
    private OthSetHistoryRepo historyDao;

    @Transactional(readOnly = true)
    public List<OthSetHistory> findHistorys(OthSet othset) {
        try {
            othset.getHistorys().size();
        } catch (LazyInitializationException e) {
            //log.debug("findHistorys lazy init error :" + e.getMessage());
            othset.setHistorys(historyDao.findByOthset(othset));
        }
        return othset.getHistorys();
    }

    public OthSetHistory createEmptyHistory(OthSet othset, User createUser) {
        return this.createEmptyHistory(othset, createUser, new Date());
    }
    
    public OthSetHistory createEmptyHistory(OthSet othset, User createUser, Date sysDate) {
        OthSetHistory history = new OthSetHistory();
        history.setOthset(othset);
        history.setOsNo(othset.getOsNo());
        history.setRequire(othset.getRequire());
        history.setRequireNo(othset.getRequireNo());
        history.setBehaviorStatus(othset.getOsStatus());
        history.setStatus(Activation.ACTIVE);
        history.setCreatedUser(createUser);
        history.setCreatedDate(sysDate);
        history.setUpdatedUser(createUser);
        history.setUpdatedDate(sysDate);
        return history;
    }

    @Transactional(rollbackFor = Exception.class)
    public void update(OthSetHistory history, User executor) {
        history.setUpdatedDate(new Date());
        history.setUpdatedUser(executor);
        historyDao.save(history);
        attachService.findAttachsByLazy(history).forEach(each -> each.setKeyChecked(Boolean.TRUE));
        attachService.linkRelation(history.getAttachments(), history, executor);
        this.sortHistory(history.getOthset());
    }

    public void sortHistory(OthSet othset) {
        List<OthSetHistory> historys = this.findHistorys(othset);
        Ordering<OthSetHistory> ordering = new Ordering<OthSetHistory>() {
            @Override
            public int compare(OthSetHistory left, OthSetHistory right) {
                return Longs.compare(right.getUpdatedDate().getTime(), left.getUpdatedDate().getTime());
            }
        };
        Collections.sort(historys, ordering);
    }

    public void putHistoryToOthset(OthSet othset, OthSetHistory history) {
        List<OthSetHistory> infoHistorys = this.findHistorys(othset);
        infoHistorys.add(history);
    }

    @Transactional(rollbackFor = Exception.class)
    public void createReplyHistory(OthSetReply reply, User executor) {
        OthSetHistory history = reply.getHistory();
        history.setBehaviorStatus(reply.getOthset().getOsStatus());
        history.setReply(reply);
        history.setBehavior(OthSetHistoryBehavior.REPLY);
        OthSetHistory nH = historyDao.save(history);
        attachService.linkRelation(history.getAttachments(), nH, executor);
        reply.setHistory(nH);
        this.putHistoryToOthset(reply.getOthset(), nH);
    }

    @Transactional(rollbackFor = Exception.class)
    public void createAlreadyHistory(OthSetAlreadyReply aReply, User executor) {
        OthSetHistory history = aReply.getHistory();
        history.setAlreadyReply(aReply);
        history.setBehavior(OthSetHistoryBehavior.REPLY_AND_REPLY);
        OthSetHistory nH = historyDao.save(history);
        attachService.linkRelation(history.getAttachments(), nH, executor);
        aReply.setHistory(nH);
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveCancelHistory(OthSetHistory history) throws IllegalAccessException {
        if (ossService.disableCancel(
                reqService.findByReqObj(history.getRequire()),
                osService.findByOsNo(history.getOsNo()),
                WkUserCache.getInstance().findBySid(history.getCreatedUser().getSid()))) {
            throw new IllegalAccessException("無權限進行操作");
        }
        history.setBehavior(OthSetHistoryBehavior.CANCEL_OS);
        history.setBehaviorStatus(OthSetStatus.INVALID);
        history.setInputInfo(jsoupUtils.clearCssTag(history.getInputInfoCss()));
        OthSetHistory nH = historyDao.save(history);

        OthSet othset = history.getOthset();
        othset.setOsStatus(OthSetStatus.INVALID);
        othset.setCancelDate(new Date());
        osService.save(othset, WkUserCache.getInstance().findBySid(history.getCreatedUser().getSid()));
        osaService.createCancelAlert(nH);

        attachService.linkRelation(history.getAttachments(), nH, WkUserCache.getInstance().findBySid(history.getCreatedUser().getSid()));
        this.putHistoryToOthset(nH.getOthset(), nH);
        this.sortHistory(history.getOthset());
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveCompleteHistory(Require req, OthSetHistory history) throws IllegalAccessException {
        // ====================================
        // 權限檢核
        // ====================================
        if (ossService.disableComplate(
                reqService.findByReqObj(req),
                osService.findByOsNo(history.getOsNo()),
                WkUserCache.getInstance().findBySid(history.getCreatedUser().getSid()))) {
            throw new IllegalAccessException("無權限進行操作");
        }
        
        history.setBehavior(OthSetHistoryBehavior.FINISH_OS);
        history.setBehaviorStatus(OthSetStatus.FINISH);
        OthSetHistory nH = historyDao.save(history);
        osaService.createCompleteAlert(nH);

        OthSet othset = history.getOthset();
        othset.setOsStatus(OthSetStatus.FINISH);
        othset.setFinishDate(new Date());
        osService.save(othset, WkUserCache.getInstance().findBySid(history.getCreatedUser().getSid()));

        attachService.linkRelation(history.getAttachments(), nH, WkUserCache.getInstance().findBySid(history.getCreatedUser().getSid()));
        this.putHistoryToOthset(nH.getOthset(), nH);
        this.sortHistory(history.getOthset());
    }

}
