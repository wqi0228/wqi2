/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.bpm.rest.client.BpmOrganizationClient;
import com.cy.bpm.rest.client.ProcessClient;
import com.cy.bpm.rest.client.TaskClient;
import com.cy.bpm.rest.to.RoleTo;
import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.bpm.rest.vo.common.ProcessType;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.commons.vo.simple.SimpleUser;
import com.cy.tech.request.logic.vo.BpmCreateTo;
import com.cy.tech.request.logic.vo.BpmCreateTo.RequireFlowType;
import com.cy.tech.request.logic.vo.BpmSignTo;
import com.cy.tech.request.repository.pt.PtSignInfoRepo;
import com.cy.tech.request.repository.require.RequireUnitSignInfoRepository;
import com.cy.tech.request.repository.require.TechManagerSignInfoRepository;
import com.cy.tech.request.repository.worktest.WorkTestSignInfoRepo;
import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.pt.PtSignInfo;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireUnitSignInfo;
import com.cy.tech.request.vo.require.TechManagerSignInfo;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.vo.worktest.WorkTestSignInfo;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * 流程服務
 *
 * @author shaun
 */
@Slf4j
@Component
public class BpmService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3168146888772000380L;
    @Autowired
    transient private ProcessClient bpmProcessClient;
    @Autowired
    transient private BpmOrganizationClient bpmOrganizationClient;
    @Autowired
    transient private TaskClient bpmTaskClient;

    @Autowired
    private transient WkUserCache wkUserCache;
    @Autowired
    private transient WkOrgCache wkOrgCache;
    @Autowired
    private transient RequireUnitSignInfoRepository rmsiDao;
    @Autowired
    private transient TechManagerSignInfoRepository rtmsiDao;
    @Autowired
    private transient PtSignInfoRepo ptsiDao;
    @Autowired
    private transient WorkTestSignInfoRepo wtsiDao;

    /**
     * 取得水管圖 by BPM InstanceId
     * 
     * @param bpmId
     * @param loginUserId
     * @return
     */
    public List<ProcessTaskBase> findFlowChartByInstanceId(String bpmId, String loginUserId) {
        try {
            return bpmTaskClient.findSimulationChart(bpmId, loginUserId);
        } catch (ProcessRestException e) {
            if (e.getMessage() == null) {
                log.warn("找不到的BPM資料bpmId:[{}]", bpmId);
            } else {
                log.warn(e.getMessage(), e);
            }
        }
        return Lists.newArrayList();
    }

    @SuppressWarnings("deprecation")
    public List<ProcessTaskBase> findSimulationById(String bpmId) throws ProcessRestException {
        return bpmTaskClient.findSimulationChart(bpmId);
    }

    /**
     * 尋找當前任務預設執行人員名稱
     *
     * @param lastTask
     * @return
     * @throws ProcessRestException
     */
    public String findTaskDefaultUserName(ProcessTaskBase lastTask) throws ProcessRestException {
        // 最後節點如果為歷史任務的話就無待簽人員..
        if (lastTask instanceof ProcessTaskHistory) {
            return "";
        }
        if (!Strings.isNullOrEmpty(lastTask.getUserName())) {
            return lastTask.getUserName();
        }
        List<String> userids = bpmOrganizationClient.findUserFromRole(lastTask.getRoleID());
        if (userids.isEmpty()) {
            return "";
        }
        User user = wkUserCache.findById(userids.get(0));
        return user.getName();
    }

    /**
     * 尋找當前任務可簽核人員列表
     *
     * @param bpmId
     * @return
     * @throws ProcessRestException
     */
    public List<String> findTaskCanSignedUserIds(String bpmId) throws ProcessRestException {
        return bpmTaskClient.findTaskCanSignUserIds(bpmId);
    }

    /**
     * 執行 - 原型確認 - 建立流程
     *
     * @param require
     * @param executor
     * @return
     * @throws ProcessRestException
     */
    public String createPtSignFlow(PtCheck ptCheck, User executor) throws ProcessRestException {
        // 抓取執行人公司
        Org dep = wkOrgCache.findBySid(executor.getPrimaryOrg().getSid());

        // BPM開單角色，需從BPM Rest Service 取得
        String executorRoleId = bpmOrganizationClient.findUserRoleByCompany(
                executor.getId(),
                dep.getCompany().getId());

        BpmCreateTo to = new BpmCreateTo(ptCheck, executor, executorRoleId, RequireFlowType.PROTOTYPE_CHECK, new HashMap<>());
        return bpmProcessClient.create(to);
    }

    /**
     * 執行 - 送測審核 - 建立流程
     *
     * @param testInfo
     * @param executor
     * @return
     * @throws ProcessRestException
     */
    public String createSendTestSignFlow(WorkTestInfo testInfo, User executor) throws ProcessRestException {
        // 抓取執行人公司
        Org dep = wkOrgCache.findBySid(executor.getPrimaryOrg().getSid());

        // BPM開單角色，需從BPM Rest Service 取得
        String executorRoleId = bpmOrganizationClient.findUserRoleByCompany(
                executor.getId(),
                dep.getCompany().getId());

        BpmCreateTo to = new BpmCreateTo(testInfo, executor, executorRoleId, RequireFlowType.TEST_UNIT, new HashMap<>());
        return bpmProcessClient.create(to);
    }

    /**
     * 儲存簽核資訊
     *
     * @param signInfo
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public RequireUnitSignInfo updateReqUnitSignInfo(RequireUnitSignInfo signInfo) {
        if (signInfo == null || signInfo.getRequire() == null) {
            throw new IllegalArgumentException();
        }
        if (Strings.isNullOrEmpty(signInfo.getSid())) {
            RequireUnitSignInfo jpaSignInfo = rmsiDao.findByRequire(signInfo.getRequire());
            signInfo.setSid(jpaSignInfo.getSid());
        }
        return rmsiDao.save(signInfo);
    }

    /**
     * 儲存簽核資訊
     *
     * @param signInfo
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public TechManagerSignInfo updateTechManagerSignInfo(TechManagerSignInfo signInfo) {
        if (signInfo == null || signInfo.getRequire() == null) {
            throw new IllegalArgumentException();
        }
        if (Strings.isNullOrEmpty(signInfo.getSid())) {
            TechManagerSignInfo jpaSignInfo = rtmsiDao.findByRequire(signInfo.getRequire());
            signInfo.setSid(jpaSignInfo.getSid());
        }
        return rtmsiDao.save(signInfo);
    }

    /**
     * 儲存簽核資訊
     *
     * @param signInfo
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public PtSignInfo updatePtSignInfo(PtSignInfo signInfo) {
        if (signInfo == null || signInfo.getPtCheck() == null) {
            throw new IllegalArgumentException();
        }
        if (Strings.isNullOrEmpty(signInfo.getSid())) {
            PtSignInfo ptSignInfo = ptsiDao.findByPtCheck(signInfo.getPtCheck());
            if (ptSignInfo == null) {
                throw new IllegalArgumentException();
            }
            signInfo.setSid(ptSignInfo.getSid());
        }
        return ptsiDao.save(signInfo);
    }

    /**
     * 儲存簽核資訊
     *
     * @param signInfo
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public WorkTestSignInfo updateSendTestSignInfo(WorkTestSignInfo signInfo) {
        if (signInfo == null || signInfo.getTestInfo() == null) {
            throw new IllegalArgumentException();
        }
        if (Strings.isNullOrEmpty(signInfo.getSid())) {
            WorkTestSignInfo testSignInfo = wtsiDao.findByTestInfo(signInfo.getTestInfo());
            if (testSignInfo == null) {
                throw new IllegalArgumentException();
            }
            signInfo.setSid(testSignInfo.getSid());
        }
        return wtsiDao.save(signInfo);
    }

    /**
     * 檢查是否能夠進行流程動作，除了復原
     *
     * @param executor
     * @param bpmId
     * @return
     * @throws ProcessRestException
     */
    public void checkCanDoExceptRecovery(User executor, String bpmId) throws ProcessRestException {
        List<String> canSignIds = this.findTaskCanSignedUserIds(bpmId);
        if (!canSignIds.contains(executor.getId())) {
            throw new ProcessRestException("無執行權限");
        }
    }

    /**
     * 檢查是否能夠進行流程動作，除了作廢
     *
     * @param executor
     * @param bpmId
     * @return
     * @throws ProcessRestException
     */
    public void checkCanDoInvaild(User executor, String bpmId) throws ProcessRestException {
        List<String> canSignIds = this.findTaskCanSignedUserIds(bpmId);
        if (canSignIds.contains(executor.getId())) {
            return;
        }
        // 主管也能作廢
        Set<User> userManagers = Sets.newHashSet();
        for (String userID : canSignIds) {

            User canDoMember = wkUserCache.findById(userID);

            if (canDoMember == null || canDoMember.getPrimaryOrg() == null) {
                continue;
            }

            // 取得所有上層單位
            List<Org> deps = wkOrgCache.findAllParent(canDoMember.getPrimaryOrg().getSid());
            // 加入本身單位
            deps.add(canDoMember.getPrimaryOrg().toOrg());

            for (Org org : deps) {
                if (org != null && WkStringUtils.notEmpty(org.getManagers())) {
                    for (SimpleUser user : org.getManagers()) {
                        User currUser = WkUserCache.getInstance().findBySid(user.getSid());
                        userManagers.add(currUser);
                    }
                }
            }
        }

        if (userManagers.contains(executor)) {
            return;
        }

        // 自己也能作廢
        if (this.findSimulationById(bpmId).stream()
                .filter(each -> each instanceof ProcessTaskHistory)
                .map(each -> (ProcessTaskHistory) each)
                .anyMatch(history -> history.getExecutorID().equals(executor.getId()))) {
            return;
        }
        throw new ProcessRestException("無執行權限");
    }

    public void checkCanRecovery(User executor, String bpmId) throws ProcessRestException {
        List<ProcessTaskBase> tasks = this.findSimulationById(bpmId);
        // 任務模擬圖需超過兩位才能進行復原，結案也無法復原
        if (tasks.size() <= 1) {
            throw new ProcessRestException("己無執行權限！");
        }
        InstanceStatus iStatus = this.createInstanceStatus(bpmId, tasks);
        // 如果為核准..
        int recoverySub = iStatus.equals(InstanceStatus.APPROVED) ? 1 : 2;
        ProcessTaskBase rTask = tasks.get(tasks.size() - recoverySub);
        if (rTask instanceof ProcessTaskHistory) {
            if (executor.getId().equals(((ProcessTaskHistory) rTask).getExecutorID())) {
                return;
            }
        }
        throw new ProcessRestException("已無執行權限！");
    }

    /**
     * 建立流程狀態
     *
     * @param bpmId
     * @param tasks
     * @return
     * @throws com.cy.bpm.rest.vo.exception.ProcessRestException
     */
    public InstanceStatus createInstanceStatus(String bpmId, List<ProcessTaskBase> tasks) throws ProcessRestException {
        ProcessTaskBase firstInfo = null;
        // 新建檔，第一節點未簽核時
        if (!tasks.isEmpty()) {
            firstInfo = tasks.get(0);
        }
        if (firstInfo == null) {
            log.error("流程ID:" + bpmId + " 取得任務節點為空值，請檢查相關流程資料...");
            return InstanceStatus.NOT_INPUT;// 如果有問題暫時回傳無意義值
        }

        // 核准，狀態為結束且最後節點簽完
        ProcessTaskBase majorTask = bpmTaskClient.findMajorTask(bpmId);
        boolean isFinish = majorTask == null;

        // 如果為歷史的任務代表已簽核過
        boolean firstDoSign = firstInfo instanceof ProcessTaskHistory;
        if (!firstDoSign && !isFinish) {
            if (firstInfo.getRollbackInfo().isEmpty()) {
                return InstanceStatus.NEW_INSTANCE;
            } else {
                return InstanceStatus.RECONSIDERATION;
            }
        }

        if (!isFinish) {
            // 待簽核，流程未結束且，第一節點簽了，但第二節點未簽核時
            ProcessTaskBase secondInfo = tasks.get(1);
            boolean secondDoSign = secondInfo instanceof ProcessTaskHistory;
            if (firstDoSign && !secondDoSign) {
                return InstanceStatus.WAITING_FOR_APPROVE;
            }
        }

        ProcessTaskBase lastInfo = tasks.get(tasks.size() - 1);
        boolean lastDoSign = lastInfo instanceof ProcessTaskHistory;
        if (lastDoSign && isFinish) {
            return InstanceStatus.APPROVED;
        }

        // 簽核中
        return InstanceStatus.APPROVING;
    }

    /**
     * 執行復原
     *
     * @param executor
     * @param recoveryTask
     * @throws ProcessRestException
     */
    public void doRecovery(User executor, ProcessTaskHistory recoveryTask) throws ProcessRestException {
        bpmProcessClient.recovery(executor.getId(), ProcessType.SIGNFLOW, recoveryTask);
    }

    /**
     * 執行退回
     *
     * @param executor
     * @param rollBackTask
     * @param comment
     * @throws ProcessRestException
     */
    public void doRollBack(User executor, ProcessTaskHistory rollBackTask, String comment) throws ProcessRestException {
        bpmProcessClient.rollback(executor.getId(), ProcessType.SIGNFLOW, rollBackTask, comment);
    }

    /**
     * 作廢流程
     *
     * @param executor
     * @param bpmId
     * @throws ProcessRestException
     */
    public void invaildProcess(User executor, String bpmId) throws ProcessRestException {
        bpmProcessClient.terminate(bpmId, executor.getId());
    }

    /**
     * 執行 - 需求單位流程 - 簽核
     *
     * @param require
     * @param executor
     * @param comment
     * @throws ProcessRestException
     */
    public void doReqUnitSign(Require require, User executor, String comment) throws ProcessRestException {
        BpmSignTo signTo = new BpmSignTo(require.getReqUnitSign().getBpmInstanceId(), executor, comment, new HashMap<>());
        bpmProcessClient.sign(signTo);
    }

    /**
     * 執行 - 原型確認流程 - 簽核
     *
     * @param check
     * @param executor
     * @param comment
     * @throws ProcessRestException
     */
    public void doPtSign(PtCheck ptCheck, User executor, String comment) throws ProcessRestException {
        if (ptCheck.getSignInfo() == null) {
            throw new ProcessRestException(
                    "需求單單號:" + ptCheck.getSourceNo() + " 原型確認單號:" + ptCheck.getPtNo() + " 原型確認資訊版本:V" + ptCheck.getVersion() + "，未觸發原型確認流程...");
        }
        String bpmId = ptCheck.getSignInfo().getBpmInstanceId();
        BpmSignTo signTo = new BpmSignTo(bpmId, executor, comment, new HashMap<>());
        bpmProcessClient.sign(signTo);
    }

    /**
     * 執行 - 送測審核 - 簽核
     *
     * @param testInfo
     * @param executor
     * @param comment
     * @throws ProcessRestException
     */
    public void doSendTestSign(WorkTestInfo testInfo, User executor, String comment) throws ProcessRestException {
        if (testInfo.getSignInfo() == null) {
            throw new ProcessRestException("送測單:未觸發送測審核流程...送測單號:" + testInfo.getTestinfoNo() + " 需求單單號:" + testInfo.getSourceNo());
        }
        String bpmId = testInfo.getSignInfo().getBpmInstanceId();
        BpmSignTo signTo = new BpmSignTo(bpmId, executor, comment, new HashMap<>());
        bpmProcessClient.sign(signTo);
    }

    /**
     * 新建檔、待簽核、簽核中、再議 皆為流程進行中
     *
     * @param is
     * @return
     */
    public boolean isProcessingStatus(InstanceStatus is) {
        return is.equals(InstanceStatus.NEW_INSTANCE) || is.equals(InstanceStatus.WAITING_FOR_APPROVE)
                || is.equals(InstanceStatus.APPROVING) || is.equals(InstanceStatus.RECONSIDERATION);
    }

    /**
     * 收集需求單位簽核流程人員
     * 
     * @param require   需求單主檔
     * @param isHistory
     * @return user sid
     */
    public List<Integer> findReqUnitSignFlowUser(Require require, boolean isHistory) {

        // ====================================
        // 收集簽核流程使用者
        // ====================================
        // 使用者 sid
        Set<Integer> userSids = Sets.newHashSet();

        // 加入立案者
        userSids.add(require.getCreatedUser().getSid());

        // 沒有簽核流程者直接回傳
        if (!require.getHasReqUnitSign() && require.getReqUnitSign() == null) {
            return Lists.newArrayList(userSids);
        }

        // 查水管圖
        List<ProcessTaskBase> tasks = this.findFlowChartByInstanceId(
                require.getReqUnitSign().getBpmInstanceId(), "");

        if (WkStringUtils.isEmpty(tasks)) {
            return Lists.newArrayList(userSids);
        }

        // 收集流程節點人員
        for (ProcessTaskBase processTaskBase : tasks) {
            String userID = "";
            if (isHistory) {
                ProcessTaskHistory taskHistory = (ProcessTaskHistory) processTaskBase;
                userID = taskHistory.getExecutorID();
            } else {
                userID = processTaskBase.getUserID();
            }

            User user = this.wkUserCache.findById(userID);
            if (user != null) {
                userSids.add(user.getSid());
            }
        }

        return Lists.newArrayList(userSids);

    }

    public List<RoleTo> prepareUserSignFlow(Integer userSid) throws SystemOperationException {

        // ====================================
        // 查詢 user 資料
        // ====================================
        User user = WkUserCache.getInstance().findBySid(userSid);
        if (user == null) {
            String message = WkMessage.EXECTION + "(找不到建單者資料:[" + userSid + "])";
            log.error(message);
            throw new SystemOperationException(message, InfomationLevel.ERROR);
        }

        if (user.getPrimaryOrg() == null) {
            String message = WkMessage.EXECTION + "建單者:" + user.getName() + "(" + user.getSid() + ") 沒有歸屬單位";
            log.error(message);
            throw new SystemOperationException(message, InfomationLevel.ERROR);
        }

        if (user.getPrimaryOrg().getCompanySid() == null) {
            String message = WkMessage.EXECTION + "建單者:" + user.getName() + "(" + user.getSid() + ") 單位 (" + user.getPrimaryOrg().getSid() + " )沒有歸屬公司!";
            log.error(message);
            throw new SystemOperationException(message, InfomationLevel.ERROR);
        }

        // ====================================
        // 查詢歸屬公司資料
        // ====================================
        Org comp = WkOrgCache.getInstance().findBySid(user.getPrimaryOrg().getCompanySid());
        if (comp == null) {
            String message = WkMessage.EXECTION + "未查到歸屬公司資料!(orgSid=" + user.getPrimaryOrg().getCompanySid() + ")";
            log.error(message);
            throw new SystemOperationException(message, InfomationLevel.ERROR);
        }

        // ====================================
        // 查詢主要角色
        // ====================================
        String mainBpmRoleID = "";
        try {
            mainBpmRoleID = this.bpmOrganizationClient.findUserRoleByCompany(user.getId(), comp.getId());
            if (WkStringUtils.isEmpty(mainBpmRoleID)) {
                String message = WkMessage.EXECTION + "找不到使用者主要角色!(" + user.getId() + ")";
                log.error(message);
                throw new SystemOperationException(message, InfomationLevel.ERROR);
            }
        } catch (ProcessRestException e) {
            log.error("查詢使用者BPM主要角色失敗!" + e.getMessage(), e);
            throw new SystemOperationException("查詢使用者BPM主要角色失敗!" + e.getMessage(), InfomationLevel.ERROR);
        }

        RoleTo mainRoleTo;
        try {
            mainRoleTo = this.bpmOrganizationClient.findRoleToById(mainBpmRoleID);
            if (mainRoleTo == null) {
                String message = WkMessage.EXECTION + "找不到使用者主要角色資料!(" + user.getId() + ")";
                log.error(message);
                throw new SystemOperationException(message, InfomationLevel.ERROR);
            }
        } catch (ProcessRestException e) {
            log.error("查詢使用者BPM主要角色失敗!" + e.getMessage(), e);
            throw new SystemOperationException("查詢使用者BPM主要角色失敗!" + e.getMessage(), InfomationLevel.ERROR);
        }

        // ====================================
        // 遞迴往上查詢角色
        // ====================================
        List<RoleTo> roleTos = Lists.newArrayList();
        try {
            this.prepareSignFlow_recursion(mainRoleTo, roleTos);
        } catch (ProcessRestException e) {
            log.error("查詢角色階層失敗" + e.getMessage(), e);
            throw new SystemOperationException("查詢角色階層失敗!" + e.getMessage(), InfomationLevel.ERROR);
        }

        return roleTos;
    }

    /**
     * 遞迴往上查詢角色
     * 
     * @param roleTo
     * @param roleTos
     * @throws ProcessRestException
     */
    private void prepareSignFlow_recursion(RoleTo roleTo, List<RoleTo> roleTos) throws ProcessRestException {

        roleTos.add(roleTo);
        if (WkStringUtils.isEmpty(roleTo.getParentRoleId())) {
            return;
        }
        RoleTo parentRoleTo = this.bpmOrganizationClient.findRoleToById(roleTo.getParentRoleId());
        if (parentRoleTo == null) {
            log.warn("找不到BPM角色資訊!roleID:[{}]", roleTo.getParentRoleId());
            return;
        }
        this.prepareSignFlow_recursion(parentRoleTo, roleTos);
    }

    /**
     * 以 BPM 角色 ID, 查詢符合該角色的使用者
     * 
     * @param bpmRoleID BPM 角色 ID
     * @return User IDs
     * @throws SystemOperationException 處理失敗時拋出
     */
    public List<String> findUsersByRoleID(String bpmRoleID) throws SystemOperationException {

        try {
            return this.bpmOrganizationClient.findUserFromRole(bpmRoleID);
        } catch (ProcessRestException e) {
            log.error("查詢角色使用者失敗!" + e.getMessage(), e);
            throw new SystemOperationException("查詢使用者BPM主要角色失敗!" + e.getMessage(), InfomationLevel.ERROR);
        }

    }

}
