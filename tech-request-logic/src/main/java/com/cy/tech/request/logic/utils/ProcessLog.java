package com.cy.tech.request.logic.utils;

import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;

/**
 * @author allen1214_wu
 */
public class ProcessLog {

    private String requireNo;
    private String subInfo;
    private String processDesc;
    private Long startTime = 0L;

    /**
     * @param requireNo
     * @param subInfo
     * @param processDesc
     */
    public ProcessLog(String requireNo, String subInfo, String processDesc) {
        super();
        this.requireNo = requireNo;
        this.subInfo = subInfo;
        this.processDesc = processDesc;
        this.startTime = System.currentTimeMillis();
    }

    /**
     * @return
     */
    public String prepareStartLog() {
        return this.preparePrefix() +
                "start 『" + processDesc + "』";
    }

    /**
     * @return
     */
    public String prepareProcessLog(String processLog) {
        return this.preparePrefix() + processLog;
    }

    /**
     * @return
     */
    public String prepareCompleteLog() {
        return this.preparePrefix() +
                "complete 『" + processDesc + "』(" + WkCommonUtils.calcCostSec(this.startTime) + " sec)";
    }

    /**
     * @return
     */
    public String prepareCompleteLog(String addInfo) {
        return this.preparePrefix()
                + "complete 『" + processDesc + "』"
                + "," + addInfo
                + "(" + WkCommonUtils.calcCostSec(this.startTime) + " sec)";
    }

    /**
     * @return
     */
    private String preparePrefix() {
        String str = "[" + this.requireNo + "]";
        if (WkStringUtils.notEmpty(this.subInfo)) {
            str += "-[" + this.subInfo + "]";
        }
        str += "：";

        return str;
    }

}
