/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import java.io.Serializable;
import java.util.Date;

import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.vo.value.to.JsonStringListTo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * ON程式一覽表頁面顯示vo (search16.xhtml)
 *
 * @author jason_h
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Search16View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2748397024651368933L;
    private int index;
    /** 需求單-緊急度 */
    private UrgencyType urgency;
    /** 填單日期 */
    private Date createdDate;
    /** 填單日期 */
    private String createdDateStr;

    /** 填單人所歸屬的部門 */
    private Integer createDep;
    /** 填單人所歸屬的部門名稱 */
    private String createDepName;
    /** 建立者 */
    private Integer createdUser;
    /** 建立者名稱 */
    private String createdUserName;
    /** ON程式主題 */
    private String onPgTheme;
    /** 需求類別 */
    private String bigName;
    /** 中類 */
    private String middleName;
    /** 小類 */
    private String smallName;
    /** 需求單位 */
    private Integer requireDep;
    /** 需求單位名稱 */
    private String requireDepName;
    /** 需求人員 */
    private Integer requireUser;
    /** 需求人員名稱 */
    private String requireUserName;
    /** ON程式狀態 */
    private WorkOnpgStatus onPgStatus;
    /** 說明 (ON程式狀態為「取消ON程式」時顯示) */
    private String onPgDescription;
    /** 預計完成日 */
    private Date establishDate;
    /** 預計完成日字串 */
    private String establishDateStr;

    private String establishDateSortStr;
    /** 檢查完成日 */
    private Date finishDate;
    /** 檢查完成日字串 */
    private String finishDateStr;
    /** 異動日 */
    private Date updateDate;
    /** 異動日 */
    private String updateDateStr;
    /** ON程式單號 */
    private String onpgNo;
    /** 通知單位 */
    private JsonStringListTo noticeDeps;
    /** 通知單位名稱 */
    private String noticeDepsName;
    /** 需求日期 */
    private Date requireCreatedDate;
    /** 廳主 */
    private Long customer;
    /** 廳主顯示名稱 */
    private String customerName;
    /** 客戶 */
    private Long author;
    /** 客戶顯示名稱 */
    private String authorName;
    /** 本地端連結網址 */
    private String localUrlLink;

}
