/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.search.view.Search15View;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author jason_h
 */
@Service
@Slf4j
public class Search15QueryService implements QueryService<Search15View>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8114257623907555686L;
    @Autowired
    private URLService urlService;
    @Autowired
    private WkJsonUtils jsonUtils;
    @Autowired
    private SearchService searchHelper;
    @PersistenceContext
    transient private EntityManager em;

    @Override
    public List<Search15View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {

        // ====================================
        // 查詢
        // ====================================
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        } 
        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        @SuppressWarnings("unchecked")
        List<Object[]> result = query.getResultList();
        //資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if(WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();
        List<Search15View> viewResult = Lists.newArrayList();
        for (int i = 0; i < result.size(); i++) {
            Search15View v = new Search15View();
            int index = 0;
            Object[] record = (Object[]) result.get(i);
            String sid = (String) record[index++];
            String requireNo = (String) record[index++];
            String requireTheme = (String) record[index++];
            Integer urgency = (Integer) record[index++];
            Integer requireDep = (Integer) record[index++];
            Date requireDate = (Date) record[index++];
            Integer requireUser = (Integer) record[index++];
            String bigName = (String) record[index++];
            String middleName = (String) record[index++];
            String smallName = (String) record[index++];
            String requireStatus = (String) record[index++];
            Date createdDate = (Date) record[index++];
            Integer createDep = (Integer) record[index++];
            Integer createdUser = (Integer) record[index++];
            String testTheme = (String) record[index++];
            String sendTestDep = (String) record[index++];
            String testinfoNo = (String) record[index++];
            String behaviorStatus = (String) record[index++];
            String behavior = (String) record[index++];
            Date testHistoryCreatedDate = (Date) record[index++];
            Integer testHistoryCreatedUser = (Integer) record[index++];
            String reason = (String) record[index++];
            String testInfoSid = (String) record[index++];
            // 處理共通欄位 (一定要擺在『取值』最後面, 否則 index 會不對)
            v.prepareCommonColumn(record, index);

            v.setSid(sid);
            v.setRequireNo(requireNo);
            v.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
            v.setUrgency(UrgencyType.values()[urgency]);
            v.setRequireDep(requireDep);
            v.setRequireDate(requireDate);
            v.setRequireUser(requireUser);
            v.setBigName(bigName);
            v.setMiddleName(middleName);
            v.setSmallName(smallName);
            v.setRequireStatus(RequireStatusType.valueOf(requireStatus));
            v.setCreatedDate(createdDate);
            v.setCreateDep(createDep);
            v.setCreateDepName(WkOrgUtils.findNameBySid(createDep));
            v.setCreatedUser(createdUser);
            v.setTestTheme(testTheme);
            try {
                v.setSendTestDep(jsonUtils.fromJson(sendTestDep, JsonStringListTo.class));
            } catch (IOException ex) {
                log.error(ex.getMessage(), ex);
            }
            v.setTestinfoNo(testinfoNo);
            if (WorkTestStatus.QA_TEST_COMPLETE.name().equals(behavior)) {
                v.setBehaviorStatus(WorkTestStatus.QA_TEST_COMPLETE);
            } else {
                v.setBehaviorStatus(WorkTestStatus.valueOf(behaviorStatus));
            }
            v.setTestHistoryCreatedDate(testHistoryCreatedDate);
            v.setTestHistoryCreatedUser(testHistoryCreatedUser);
            v.setReason(reason);
            v.setTestInfoSid(testInfoSid);
            v.setLocalUrlLink(urlService.createLocalUrlLinkParamForTab(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(execUserSid, v.getRequireNo(), 1),
                    URLService.URLServiceAttr.URL_ATTR_TAB_ST, testInfoSid));
            viewResult.add(v);
        }
        // 解析資料-結束
        usageRecord.parserDataEnd();
        return viewResult;
    }
}
