/**
 * 
 */
package com.cy.tech.request.logic.vo;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * 已選擇項目處理結果容器
 * 
 * @author allen1214_wu
 *
 */
@Getter
@Setter
public class SelectedItemPrepareInfoTo {

	/**
	 * 強制訊息
	 * @param forceMessage
	 */
	public SelectedItemPrepareInfoTo(String forceMessage) {
		this.forceMessage = forceMessage;
	}

	public SelectedItemPrepareInfoTo(boolean reverse, List<String> selectedItemSids, List<String> selectedItemNames) {
		this.reverse = reverse;
		this.selectedItemSids = selectedItemSids;
		this.selectedItemNames = selectedItemNames;
	}

	/**
	 * 
	 */
	private String forceMessage;
	/**
	 * 資料反轉 (白名單 -> 黑名單)
	 */
	private boolean reverse;
	/**
	 * 已選擇項目的 SID
	 */
	private List<String> selectedItemSids;
	/**
	 * 已選擇項目的 名稱
	 */
	private List<String> selectedItemNames;
}
