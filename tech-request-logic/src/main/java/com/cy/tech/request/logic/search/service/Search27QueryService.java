/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.search.view.Search27View;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * @author kasim
 */
@Service
public class Search27QueryService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1008975542673595353L;
    @Autowired
    private URLService urlService;
    @Autowired
    private SearchService searchHelper;
    @PersistenceContext
    transient private EntityManager em;

    public List<Search27View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {
        // ====================================
        // 查詢
        // ====================================

        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        rawParameters.stream().forEach(entry -> query.setParameter(entry.getKey(), entry.getValue()));

        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        @SuppressWarnings("unchecked")
        List<Object[]> result = query.getResultList();
        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if (WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();

        List<Search27View> viewResult = Lists.newArrayList();
        
        // 取得自己以下單位
        Integer primaryOrgSid = WkUserUtils.findUserPrimaryOrgSid(execUserSid);
        Set<Integer> relationOrgSids = Sets.newHashSet(primaryOrgSid);
        // 管理單位
        Set<Integer> managerOrgSids = WkOrgCache.getInstance().findManagerOrgSids(execUserSid);
        if (WkStringUtils.notEmpty(managerOrgSids)) {
            relationOrgSids.addAll(managerOrgSids);
        }
        // 所以有以下子單位
        Set<Integer> allChildSids = WkOrgCache.getInstance().findAllChildSids(relationOrgSids);
        if (WkStringUtils.notEmpty(allChildSids)) {
            relationOrgSids.addAll(allChildSids);
        }
        
        for (int i = 0; i < result.size(); i++) {
            Object[] record = (Object[]) result.get(i);

            Integer index = 0;

            String sid = (String) record[index++];
            String requireNo = (String) record[index++];
            String requireTheme = (String) record[index++];
            Boolean hasForwardDep = (Boolean) record[index++];
            Boolean hasForwardMember = (Boolean) record[index++];
            Boolean hasLink = (Boolean) record[index++];
            Date createdDate = (Date) record[index++];
            String bigName = (String) record[index++];
            String middleName = (String) record[index++];
            String smallName = (String) record[index++];
            Integer createDepSid = (Integer) record[index++];
            Integer createdUser = (Integer) record[index++];
            Integer urgency = (Integer) record[index++];
            String requireStatus = (String) record[index++];
            String readReason = (String) record[index++];
            Boolean checkAttachment = (Boolean) record[index++];
            Boolean hasAttachment = (Boolean) record[index++];
            Date hopeDate = (Date) record[index++];
            Integer customer = (Integer) record[index++];
            Date updatedDate = (Date) record[index++];

            Search27View v = new Search27View();
            v.setSid(sid);
            v.setRequireNo(requireNo);
            v.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
            v.setHasForwardDep(hasForwardDep);
            v.setHasForwardMember(hasForwardMember);
            v.setHasLink(hasLink);
            v.setCreatedDate(createdDate);
            v.setBigName(bigName);
            v.setMiddleName(middleName);
            v.setSmallName(smallName);
            v.setCreateDep(createDepSid);
            v.setCreatedUser(createdUser);
            v.setUrgency(UrgencyType.values()[urgency]);
            v.setRequireStatus(RequireStatusType.valueOf(requireStatus));
            v.setUpdatedDate(updatedDate);
            v.setReadReason(Strings.isNullOrEmpty(readReason) ? ReqToBeReadType.NO_TO_BE_READ : ReqToBeReadType.valueOf(readReason));
            v.setCheckAttachment(checkAttachment);
            v.setHasAttachment(hasAttachment);
            v.setHopeDate(hopeDate);
            v.setLocalUrlLink(urlService.createLoacalURLLink(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(execUserSid, v.getRequireNo(), 1)));
            v.setCreate(relationOrgSids.contains(createDepSid));
            if (customer != null) {
                v.setCustomer(customer.longValue());
            }

            // 處理共通欄位 (一定要擺在最後面, 否則 index 會不對)
            v.prepareCommonColumn(record, index);

            viewResult.add(v);
        }
        // 解析資料-結束
        usageRecord.parserDataEnd();
        return viewResult;
    }

    @SuppressWarnings("unchecked")
    public List<String> findWithQueryForCache(String sql, Map<String, Object> parameters, User executor) {
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        rawParameters.stream().forEach(entry -> query.setParameter(entry.getKey(), entry.getValue()));
        return (List<String>) query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<String> findWithQueryForCache(String sql) {
        Query query = em.createNativeQuery(sql);
        return (List<String>) query.getResultList();
    }

}
