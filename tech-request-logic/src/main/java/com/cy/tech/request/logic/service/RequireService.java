/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.logic.helper.systemnotify.SysNotifyHelper;
import com.cy.tech.request.logic.service.helper.RequireCssHelper;
import com.cy.tech.request.logic.service.helper.RequireFinishHelper;
import com.cy.tech.request.logic.service.helper.RequireIndexHelper;
import com.cy.tech.request.logic.service.setting.SettingCheckConfirmRightService;
import com.cy.tech.request.logic.service.syncmms.to.SyncFormTo;
import com.cy.tech.request.logic.utils.ProcessLog;
import com.cy.tech.request.logic.utils.ReqularPattenUtils;
import com.cy.tech.request.repository.TrNamedQueryRepository;
import com.cy.tech.request.repository.require.ReqModifyRepo;
import com.cy.tech.request.repository.require.RequireRepository;
import com.cy.tech.request.repository.require.RequireTempNotifyRepository;
import com.cy.tech.request.repository.result.SabaQueryResult;
import com.cy.tech.request.repository.result.TransRequireVO;
import com.cy.tech.request.vo.enums.CateConfirmType;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.ReqCateType;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.enums.RequireTransProgramType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireAttachment;
import com.cy.tech.request.vo.require.RequireCheckItem;
import com.cy.tech.request.vo.require.RequireCssContent;
import com.cy.tech.request.vo.require.RequireFbkAttachment;
import com.cy.tech.request.vo.require.RequireIndexDictionary;
import com.cy.tech.request.vo.require.RequireTempNotify;
import com.cy.tech.request.vo.require.RequireTrace;
import com.cy.tech.request.vo.require.RequireUnitSignInfo;
import com.cy.tech.request.vo.template.CategoryKeyMapping;
import com.cy.tech.request.vo.template.component.ComBase;
import com.cy.tech.request.vo.template.component.ComBaseCss;
import com.cy.tech.request.vo.template.component.ComRadioTypeThree;
import com.cy.tech.request.vo.template.component.ComSaba;
import com.cy.tech.request.vo.template.component.to.ComRadioTo;
import com.cy.tech.request.vo.value.to.RequireContentTo;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.common.vo.AttachmentService;
import com.cy.work.common.vo.value.to.ReadRecordTo;
import com.cy.work.customer.vo.WorkCustomer;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * 需求單服務
 *
 * @author shaun
 */
@Slf4j
@Service
public class RequireService implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6256420573550617614L;
    private static RequireService instance;

    public static RequireService getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        RequireService.instance = this;
    }

    @Autowired
    private ReqModifyService reqModifyService;
    @Autowired
    private RequireShowService requireShowService;
    @Autowired
    private ReqUnitBpmService reqUnitBpmService;
    @Autowired
    private ReqExtraSettingService reqExtraSettingService;
    @Autowired
    private RequireTraceService requireTraceService;
    @Autowired
    private UserService userService;
    @Autowired
    @Qualifier("require_attach")
    private AttachmentService<RequireAttachment, Require, String> reqAttachService;
    @Autowired
    @Qualifier("require_fbk_attach")
    private AttachmentService<RequireFbkAttachment, RequireTrace, String> fbkAttachService;
    @Autowired
    private SearchService searchService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private RequireCssHelper requireCssHelper;
    @Autowired
    private RequireIndexHelper requireIndexHelper;
    @Autowired
    private RequireFinishHelper reqFinishHelper;
    @Autowired
    private ReqularPattenUtils reqularUtils;
    @Autowired
    private WkJsoupUtils jsoupUtils;
    @Autowired
    private RequireRepository requireRepository;
    @Autowired
    private ReqModifyRepo reqModifyRepo;
    @Autowired
    private RequireTempNotifyRepository requireTempNotifyRepository;
    @Autowired
    private TrNamedQueryRepository trNamedQueryRepository;
    @Autowired
    private transient WkOrgCache wkOrgCache;
    @Autowired
    private transient WkUserCache wkUserCache;
    @Autowired
    private transient FormNumberService formNumberService;
    @PersistenceContext
    private transient EntityManager entityManager;
    @Autowired
    private transient SettingCheckConfirmRightService settingCheckConfirmRightService;
    @Autowired
    private transient SysNotifyHelper sysNotifyHelper;
    @Autowired
    private transient RequireCheckItemService requireCheckItemService;
    @Autowired
    private transient AssignNoticeService assignNoticeService;
    @Autowired
    private TemplateService templateService;
    @Autowired
    private transient RequireReadRecordHelper requireReadRecordHelper;
    @Autowired
    private transient TrAlertInboxService trAlertInboxService;
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private JdbcTemplate jdbcTemplate;

    /**
     * 新建檔
     *
     * @return
     */
    public Require createEmptyReq(Integer plusDay) {
        Require req = new Require();
        req.setHopeDate(searchService.transEndDate(searchService.addDay(Calendar.getInstance(), plusDay)));
        return req;
    }

    /**
     * 取得CloseCode By Sid
     *
     * @param sid
     * @return
     */
    public Boolean getCloseCodeBySid(String sid) {
        return requireRepository.findCloseCodeBySid(sid).get(0);
    }

    public Org getCreateDepBySid(String sid) {
        return requireRepository.findCreateDepBySid(sid).get(0);
    }

    /**
     * 是否彈出附加檔案提示
     *
     * @param mapping
     * @param require
     * @return
     */
    public boolean isPopupAttachNotify(CategoryKeyMapping mapping, Require require) {
        return mapping.getSmall().getCheckAttachment() && reqAttachService.findAttachsByLazy(require).isEmpty();
    }

    /**
     * 第一次建立需求單應檢查附加檔案是否有選擇
     *
     * @param require
     * @param attachs
     */
    public void firstSaveAttachHandler(Require require, List<RequireAttachment> attachs) {

        if (require == null) {
            return;
        }
        if (attachs == null) {
            attachs = Lists.newArrayList();
        }

        require.setAttachments(
                attachs.stream()
                        .filter(each -> each instanceof RequireAttachment)
                        .map(each -> (RequireAttachment) each)
                        .collect(Collectors.toList()));
    }

    /**
     * 建立需求單內容器
     *
     * @param valueMap
     * @return
     */
    public RequireContentTo createContentTo(Map<String, ComBase> valueMap) {
        RequireContentTo contentTo = new RequireContentTo();
        contentTo.setComValueMap(valueMap);
        return contentTo;
    }

    /**
     * 新增需求單沙巴、NCS確認設定
     * 
     * @param require
     * @param valueMap
     * @return
     */
    public Require saveSabaSetting(
            String requireSid,
            String requireNo,
            Map<String, ComBase> valueMap,
            User execUser,
            Date sysDate) {

        outLoop: for (String key : valueMap.keySet()) {
            ComBase comBase = valueMap.get(key);

            if (comBase instanceof ComRadioTypeThree) {
                ComRadioTypeThree radio = (ComRadioTypeThree) comBase;
                if (radio.getValue01() == null) {
                    break outLoop;
                }

                for (String k : radio.getValue01().keySet()) {
                    ComRadioTo to = radio.getValue01().get(k);
                    if (to.getIsSabaComponent()) {
                        log.info("{} add SABA config SABA_ENABLED = {}, by ComRadioTypeThree",
                                requireNo,
                                to.getValue01());
                        reqExtraSettingService.saveSabaByRequire(requireSid, to.getValue01(), execUser, sysDate);

                        break outLoop;
                    }
                }

            } else if (comBase instanceof ComSaba) {
                ComSaba comsaba = ((ComSaba) comBase);
                if (comsaba.getIsSabaComponent()) {
                    log.info("{} add SABA config SABA_ENABLED = {}, by ComSaba interface",
                            requireNo,
                            comsaba.getValue01());
                    reqExtraSettingService.saveSabaByRequire(requireSid, comsaba.getValue01(), execUser, sysDate);
                    break;
                }
            }
        }

        return this.findByReqSid(requireSid);
    }

    @Transactional(rollbackFor = { Exception.class })
    public Require update(
            User executor,
            Require editRequire,
            List<RequireCheckItemType> selectedCheckItemTypes,
            Require backupReq,
            Map<String, ComBase> valueMap) throws UserMessageException {
        return this.updateWhitoutTransactional(executor, editRequire, selectedCheckItemTypes, backupReq, valueMap);
    }

    /**
     * 需求單存檔(更新)<BR/>
     * 1. 更新模版內容 <BR/>
     * 2. 檢查變更廳主資料 <BR/>
     * 3. 重建索引、css內容重建 <BR/>
     * 
     * @param executor               執行者
     * @param editRequire            主檔
     * @param selectedCheckItemTypes 選擇的檢查項目
     * @param backupReq              舊的主檔
     * @param valueMap
     * @return
     * @throws UserMessageException 編輯防呆比對失敗時拋出
     */
    public Require updateWhitoutTransactional(
            User executor,
            Require editRequire,
            List<RequireCheckItemType> selectedCheckItemTypes,
            Require backupReq,
            Map<String, ComBase> valueMap) throws UserMessageException {

        Date sysDate = new Date();

        // ====================================
        // 編輯防呆
        // ====================================
        this.checkEditConflict(editRequire, backupReq);

        // ====================================
        // start log
        // ====================================
        ProcessLog processLog = new ProcessLog(editRequire.getRequireNo(), "", "需求單 內容 update ");
        // log.info(processLog.prepareStartLog());

        // ====================================
        // 新增異動記錄 (追蹤 tr_require_trace)
        // ====================================
        boolean hasTrace = reqModifyService.changeCustomerAction(
                editRequire,
                executor,
                valueMap);

        // ====================================
        // 儲存 SABA 設定 (tr_require_extra_setting)
        // ====================================
        this.saveSabaSetting(
                editRequire.getSid(),
                editRequire.getRequireNo(),
                valueMap,
                executor,
                sysDate);

        // ====================================
        // 更新主檔欄位資料 (tr_require)
        // ====================================
        Require entity = this.findByReqSid(editRequire.getSid());

        // 緊急度
        entity.setUrgency(editRequire.getUrgency());
        // 廳主
        entity.setCustomer(editRequire.getCustomer());
        // 提出客戶
        entity.setAuthor(editRequire.getAuthor());
        // 模版內容
        entity.setContent(this.createContentTo(valueMap));
        // 主責單位
        entity.setInChargeDep(editRequire.getInChargeDep());
        entity.setInChargeUsr(editRequire.getInChargeUsr());

        // 預計完成日
        entity.setRequireEstablishDate(editRequire.getRequireEstablishDate());

        // 是否有追蹤
        if (hasTrace) {
            entity.setHasTrace(true);
        }

        // 異動資訊
        entity.setUpdatedDate(sysDate);
        entity.setUpdatedUser(executor);

        // save
        editRequire = requireRepository.save(entity);

        // ====================================
        // 儲存 檢查項目檔 (save require_check_item)
        // ====================================
        this.requireCheckItemService.saveCheckItemTypes(
                editRequire.getSid(),
                selectedCheckItemTypes,
                executor.getSid(),
                sysDate);

        // ====================================
        // 建立類容索引
        // ====================================
        editRequire = this.removeAndBuildCssAndIndex(
                editRequire.getSid(),
                editRequire.getRequireNo(),
                valueMap,
                sysDate);

        // ====================================
        // complete log
        // ====================================
        log.info(processLog.prepareCompleteLog());

        return editRequire;
    }

    /**
     * 編輯防呆(簡易型..後續要加入比對系統)
     * 
     * @param editReq
     * @param backupReq
     * @throws UserMessageException
     */
    private void checkEditConflict(Require editReq, Require backupReq) throws UserMessageException {
        StringBuilder errorMsg = new StringBuilder();
        Require currReq = this.findByReqObj(backupReq);

        if (currReq.getCustomer() != null && !currReq.getCustomer().equals(backupReq.getCustomer())) {
            errorMsg.append("　　原本編輯廳主：").append(commonService.get(editReq.getCustomer().getName())).append("<BR/>");
            errorMsg.append("　　　　最新廳主：").append(commonService.get(currReq.getCustomer().getName())).append("<BR/>");
        }
        if (currReq.getAuthor() != null && !currReq.getAuthor().equals(backupReq.getAuthor())) {
            errorMsg.append("　　原本編輯提出客戶：").append(commonService.get(editReq.getAuthor().getName())).append("<BR/>");
            errorMsg.append("　　　　最新提出客戶：").append(commonService.get(currReq.getAuthor().getName())).append("<BR/>");
        }
        if (currReq.getUrgency() != null && !currReq.getUrgency().equals(backupReq.getUrgency())) {
            errorMsg.append("　　原本編輯緊急度：").append(commonService.get(editReq.getUrgency())).append("<BR/>");
            errorMsg.append("　　　　最新緊急度：").append(commonService.get(currReq.getUrgency())).append("<BR/>");
        }

        Map<String, ComBase> cuMap = currReq.getContent().getComValueMap();
        Map<String, ComBase> etMap = editReq.getContent().getComValueMap();
        Map<String, ComBase> bkMap = backupReq.getContent().getComValueMap();
        cuMap.keySet().stream().forEach(key -> {
            ComBase cuCom = cuMap.get(key);
            if (bkMap.containsKey(key)) {
                ComBase bkCom = bkMap.get(key);
                ComBase etCom = etMap.get(key);
                String cuIdx = cuCom.getIndexContent();
                String bkIdx = bkCom.getIndexContent();
                String etIdx = etCom.getIndexContent();
                if (cuIdx != null && !cuIdx.equals(bkIdx)) {
                    errorMsg.append("　").append("原本編輯").append(etCom.getName()).append("：").append(etIdx).append("<BR/>");
                    errorMsg.append("　").append("　　最新").append(cuCom.getName()).append("：").append(cuIdx).append("<BR/>");
                }
            } else {
                errorMsg.append("　　　　").append("新增").append(cuCom.getName()).append("欄位：").append(cuCom.getIndexContent()).append("<BR/>");
            }
        });
        if (errorMsg.length() != 0) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            errorMsg.append("異動成員:").append(userService.getUserName(currReq.getUpdatedUser())).append("<BR/>");
            errorMsg.append("異動時間:").append(sdf.format(currReq.getUpdatedDate())).append("<BR/>");
            errorMsg.insert(0, "原因：<BR/>");
            throw new UserMessageException(errorMsg.toString());
        }
    }

    /**
     * 移除css資料及索引並重建
     *
     * @param require
     */
    @Transactional(rollbackFor = Exception.class)
    public Require removeAndBuildCssAndIndex(
            String requireSid,
            String requireNo,
            Map<String, ComBase> mapValue,
            Date execDate) {

        // ====================================
        // 先刪除舊的
        // ====================================
        this.removeCssAndIndex(requireSid);

        // ====================================
        // 重建一份
        // ====================================
        return this.buildCssContentAndReqIndex(requireSid, requireNo, mapValue, execDate);
    }

    /**
     * 移除css資料及索引
     *
     * @param require
     */
    @Transactional(rollbackFor = Exception.class)
    public void removeCssAndIndex(String requireSid) {
        requireCssHelper.removeCssContent(requireSid);
        requireIndexHelper.removeIndex(requireSid);
    }

    /**
     * 如果有變更期望完成日時，需加入追蹤
     *
     * @param executor
     * @param require
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Require saveByChangeHopeDate(User executor, Require require) {
        Require entity = this.findByReqObj(require);
        Preconditions.checkArgument(requireShowService.showChangeHopeDateBtn(entity, executor), "變更期望完成日失敗！！");

        // ====================================
        // 寫追蹤
        // ====================================
        Date oldHopeDate = entity.getHopeDate();
        requireTraceService.createChangeHopeDateTrace(require.getSid(), executor, oldHopeDate, require.getHopeDate());

        // ====================================
        // 更新主檔
        // ====================================
        require.setHasTrace(Boolean.TRUE);
        require.setReadReason(ReqToBeReadType.CHANGE_HOPE_DATE);

        reqModifyRepo.updateHopeDate(
                require,
                require.getHasTrace(),
                require.getReadReason(),
                require.getHopeDate(),
                executor,
                new Date());

        // ====================================
        // 將『已經讀取』過單據的人，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.REQUIRE,
                require.getSid(),
                executor.getSid());

        return require;
    }

    /**
     * 檢查需求單是否有此單號
     *
     * @param requireNo
     * @return
     */
    @Transactional(readOnly = true)
    public Boolean isExistNo(String requireNo) {
        return requireRepository.isExistNo(requireNo);
    }

    @Transactional(readOnly = true)
    public Require findByReqNo(String requireNo) {
        return requireRepository.findByRequireNo(requireNo);
    }

    /**
     * @param requireSid
     * @return
     */
    @Transactional(readOnly = true)
    public Require findByReqSid(String requireSid) {
        if (WkStringUtils.isEmpty(requireSid)) {
            return null;
        }
        Require require = requireRepository.findBySid(requireSid);
        if (require == null) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.ERROR, "傳入requireSid找不到:[" + requireSid + "]", 30);
        }
        return require;
    }

    @Transactional(readOnly = true)
    public Require findByReqObj(Require require) {
        if (WkStringUtils.isEmpty(require)) {
            return null;
        }

        Require result = this.findByReqSid(require.getSid());
        // Require require = requireDao.findOne(requireSid);
        if (result == null) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.ERROR, "傳入requireSid找不到:[" + require.getSid() + "]", 20);
        }

        return result;
    }

    /**
     * 單純需求單entity(無裝配組織)
     *
     * @param requireNoes
     * @param sort
     * @return
     */
    @Transactional(readOnly = true)
    public List<Require> findByRequireNoInAndSort(List<String> requireNoes, Sort sort) {
        if (requireNoes == null || requireNoes.isEmpty()) {
            return Lists.newArrayList();
        }
        if (sort == null) {
            sort = new Sort(Direction.DESC, "createdDate");
        }
        return requireRepository.findByRequireNoIn(requireNoes, sort);
    }

    /**
     * 單純需求單entity(無裝配組織)
     *
     * @param requireNoes
     * @return
     */
    @Transactional(readOnly = true)
    public List<Require> findByRequireNoIn(List<String> requireNoes) {
        return requireRepository.findByRequireNoIn(requireNoes);
    }

    /**
     * 檢查確認 & 分派/通知 (兩者需包在同一個 transaction)
     * 
     * @param requireSid              需求單 sid
     * @param effectiveTime
     * @param hasAssignPermissionRole 有分派角色
     * @param newAssignDepSids        新的分派單位清單
     * @param newNoticeDepSids        新的通知單位清單
     * @param isConfirmCheck          是否要進行『檢查確認』
     * @param confirmCheckItems       此次要『檢查確認』的項目
     * @param execUserSid             執行者 user sid
     * @param sysDate                 系統時間
     * @throws UserMessageException     檢核錯誤時拋出
     * @throws SystemOperationException 系統執行錯誤時拋出
     */
    @Transactional(rollbackFor = Exception.class)
    public void executeConfirmCheckAndAssignNotice(
            String requireSid,
            Long effectiveTime,
            List<Integer> newAssignDepSids,
            List<Integer> newNoticeDepSids,
            boolean isConfirmCheck,
            List<RequireCheckItemType> confirmCheckItems,
            Integer execUserSid,
            Date sysDate) throws UserMessageException, SystemOperationException {

        Require require = this.findByReqSid(requireSid);

        // ====================================
        // 執行動作：『檢查確認』
        // ====================================
        if (isConfirmCheck) {
            try {
                this.executeConfirmCheck(
                        requireSid,
                        confirmCheckItems,
                        execUserSid,
                        sysDate);
            } catch (UserMessageException e) {
                throw e;

            } catch (Exception e) {
                log.error("檢查確認失敗! :" + e.getMessage(), e);
                throw new SystemOperationException("檢查確認失敗, 請恰系統人員!", InfomationLevel.ERROR);
            }
        }

        // ====================================
        // 設定分派/通知資訊
        // ====================================
        try {
            this.assignNoticeService.process(
                    require.getSid(),
                    require.getRequireNo(),
                    effectiveTime,
                    WkUserCache.getInstance().findBySid(execUserSid),
                    newAssignDepSids,
                    newNoticeDepSids,
                    isConfirmCheck,
                    sysDate);
        } catch (Exception e) {
            // 動態代理, 會被包裝成 UndeclaredThrowableException
            if (e instanceof UserMessageException) {
                throw (UserMessageException) e;
            }
            if (e.getCause() instanceof UserMessageException) {
                throw (UserMessageException) e.getCause();
            }

            log.error("分派/通知單位設定失敗:" + e.getMessage(), e);
            throw new SystemOperationException("分派/通知單位設定失敗, 請恰系統人員!", InfomationLevel.ERROR);
        }

    }

    /**
     * 執行檢查確認
     * 
     * @param requireSid
     * @param requireNo
     * @param execUserSid
     * @param sysDate
     * @throws UserMessageException
     */
    @Transactional(rollbackFor = Exception.class)
    private void executeConfirmCheck(
            String requireSid,
            List<RequireCheckItemType> confirmCheckItems,
            Integer execUserSid,
            Date sysDate) throws UserMessageException {

        // ====================================
        // 查詢主檔
        // ====================================
        Require require = this.findByReqSid(requireSid);

        // ====================================
        // 檢查是否可執行
        // ====================================
        // 檢查
        String errorMessage = this.requireCheckItemService.verifyUserRightWhenCheckConfirm(
                requireSid,
                execUserSid,
                confirmCheckItems);
        // 有錯誤時拋出
        if (WkStringUtils.notEmpty(errorMessage)) {
            throw new UserMessageException(errorMessage);
        }

        // ====================================
        // 更新需求單-檢查項目檔
        // ====================================
        this.requireCheckItemService.execConfirmCheck(
                requireSid,
                confirmCheckItems,
                execUserSid,
                sysDate);

        // ====================================
        // 寫追蹤記錄
        // ====================================
        String traceMessage = ""
                + "【"
                + confirmCheckItems.stream()
                        .map(RequireCheckItemType::getDescr)
                        .collect(Collectors.joining("】、【"))
                + "】"
                + RequireTraceType.CONFIRMED_CHECK.getLabel()
                + "完成";

        this.requireTraceService.insertTrace(
                requireSid,
                require.getRequireNo(),
                RequireTraceType.CONFIRMED_CHECK,
                traceMessage,
                execUserSid, sysDate);

        // ====================================
        // 確認是否全部項目都檢查完成了
        // ====================================
        // 檢查
        boolean isCheckComplete = this.requireCheckItemService.isCheckComplete(requireSid);
        // 未檢查完成時，不繼續往下做 (異動單據製作進度)
        if (!isCheckComplete) {
            return;
        }

        // ====================================
        // 技術事業群組管簽核
        // ====================================
        // v7.0 取消技術事業群組管簽核

        // ====================================
        // 異動製作進度：『待檢查確認』->『進行中』
        // ====================================
        this.changeRequireStatusWaitConfirmToProcess(require, execUserSid);
    }

    /**
     * 執行異動『檢查項目』for 『待檢查確認』
     * 
     * @param requireSid          需求單 sid
     * @param afterCheckItemTypes 異動後檢查項目
     * @param execUserSid         執行者 sid
     * @param execDate            執行時間
     * @throws UserMessageException 檢核錯誤時拋出
     */
    public void executeChangeCheckitemForWaitConform(String requireSid,
            List<RequireCheckItemType> afterCheckItemTypes,
            Integer execUserSid,
            Date execDate) throws UserMessageException {

        // ====================================
        // 查詢主檔
        // ====================================
        Require require = this.findByReqSid(requireSid);

        // 防呆
        if (require == null) {
            log.error("找不到主檔!SID:[{}]", requireSid);
            throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }

        // 防呆
        if (!RequireStatusType.WAIT_CHECK.equals(require.getRequireStatus())) {
            log.error("製作進度不是『待檢查確認』,RequireStatus:[{}]", require.getRequireStatus());
            throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }

        // ====================================
        // 更新檢查項目檔
        // ====================================
        this.requireCheckItemService.updateCheckItemTypes(
                requireSid,
                afterCheckItemTypes,
                true,
                execUserSid,
                execDate);

        // ====================================
        // 當全部檢查完成時，自動異動製作進度
        // ====================================
        // 檢查是否全部完成檢查了
        boolean isCheckComplete = this.requireCheckItemService.isCheckComplete(requireSid);

        // 自動異動製作進度：『待檢查確認』->『進行中』
        if (isCheckComplete) {
            this.changeRequireStatusWaitConfirmToProcess(require, execUserSid);
        }
    }

    /**
     * 異動製作進度：『待檢查確認』->『進行中』
     * 
     * @param require
     * @param execUserSid
     */
    private void changeRequireStatusWaitConfirmToProcess(Require require, Integer execUserSid) {
        // ====================================
        // 更新主檔狀態
        // ====================================
        // 欄位: 有追蹤
        require.setHasTrace(true);
        // 欄位：狀態->進行中
        require.setRequireStatus(RequireStatusType.PROCESS);
        // 欄位：待閱原因
        require.setReadReason(require.getHasAssign() ? ReqToBeReadType.ASSIGNED : ReqToBeReadType.UNASSIGNED);
        // 欄位：分類確認碼- 一般需求類別 - 已確認
        require.setCategoryConfirmCode(CateConfirmType.Y);

        this.save(require);

        // ====================================
        // 將『已經讀取』過單據的人，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.REQUIRE,
                require.getSid(),
                execUserSid);
    }

    /**
     * 執行退件通知
     * 
     * @param trace
     * @param executor
     * @param require
     * @param taskHistoryIndex 退件節點 index
     * @throws UserMessageException
     * @throws ProcessRestException
     */
    @Transactional(rollbackFor = Exception.class)
    public void executeRejectNotity(RequireTrace trace,
            User executor,
            Require require,
            int taskHistoryIndex) throws UserMessageException {
        // ====================================
        // 執行權限檢核
        // ====================================
        if (!requireShowService.showRollBackNotifyBtn(this.findByReqObj(require), executor.getSid())) {
            throw new UserMessageException(WkMessage.NEED_RELOAD, "權限檢核失敗!", InfomationLevel.ERROR);
        }

        // ====================================
        // 退件通知 - BPM 處理事項
        // ====================================
        try {
            reqUnitBpmService.doExecuteRejectNotity(
                    require,
                    executor.getId(),
                    taskHistoryIndex,
                    trace.getRequireTraceContent());

        } catch (ProcessRestException e) {
            log.error("BPM流程 rollback 失敗!" + e.getMessage(), e);
            throw new UserMessageException(WkMessage.EXECTION, e.getMessage(), InfomationLevel.ERROR);
        }

        // ====================================
        // 清除所有已分派/通知單位
        // ====================================
        this.assignNoticeService.processClearAllAssignNoticeDeps(
                require.getSid(),
                require.getRequireNo(),
                "執行退件通知",
                executor,
                new Date());

        // ====================================
        // 清除檢查資訊
        // ====================================
        // 連停用的項目, 若有檢查資訊一起清除
        List<RequireCheckItem> requireCheckItems = this.requireCheckItemService.findAllCheckItemsByRequireSid(require.getSid());
        List<String> clearTypes = Lists.newArrayList();
        // 比對已檢查時，清除檢查資訊
        for (RequireCheckItem requireCheckItem : requireCheckItems) {
            if (requireCheckItem.getCheckDate() != null) {
                this.requireCheckItemService.clearCheckInfo(requireCheckItem);
                clearTypes.add(requireCheckItem.getCheckItemType().getDescr());
            }
        }

        // 加上清除記錄
        if (WkStringUtils.notEmpty(clearTypes)) {
            String message = "因檢查確認-退件，清除系統別：[" + String.join("],[", clearTypes) + "] 的檢查資訊，需重新檢查";
            trace.setRequireTraceContent(trace.getRequireTraceContent() + "\r\n\r\n\r\n" + message);
            // 有時會是空的?
            if (WkStringUtils.notEmpty(trace.getRequireTraceContentCss())) {
                trace.setRequireTraceContentCss(trace.getRequireTraceContentCss() + "<br/><br/>" + message);
            }
        }

        // ====================================
        // 寫追蹤檔
        // ====================================
        requireTraceService.save(trace);

        // ====================================
        // 更新主檔
        // ====================================
        require.setRequireStatus(RequireStatusType.ROLL_BACK_NOTIFY);
        require.setReadReason(ReqToBeReadType.GM_REVIEW_BOUNCED);
        require.setHasTrace(Boolean.TRUE);
        require.setBackCode(Boolean.TRUE);
        reqModifyRepo.updateRejectNotify(
                require,
                require.getHasTrace(),
                require.getRequireStatus(),
                require.getReadReason(),
                require.getBackCode(),
                executor, new Date());

        // ====================================
        // 將『已經讀取』過單據的人，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.REQUIRE,
                require.getSid(),
                executor.getSid());
    }

    @Transactional(readOnly = true)
    public RequireTrace initRequireReply(String requireSid, User executor) {

        Require require = this.findByReqSid(requireSid);

        List<RequireTrace> historyTraces = requireTraceService.findByRequire(require);
        if (historyTraces != null && !historyTraces.isEmpty()) {
            RequireTrace selfToadyTrace = this.findToadySelfReply(executor, historyTraces);
            if (selfToadyTrace != null) {
                return selfToadyTrace;
            }
        }
        return requireTraceService.initRequireReplyTrace(requireSid, executor);
    }

    @Transactional(readOnly = true)
    public RequireTrace initRequireRejectTrace(String requireSid, User executor, String reason) {
        return requireTraceService.initRequireRejectTrace(requireSid, executor, reason);
    }

    private RequireTrace findToadySelfReply(User executor, List<RequireTrace> replyTraces) {

        if (WkStringUtils.isEmpty(replyTraces)) {
            return null;
        }

        // 1. 為登入者本人
        // 2. 需為『需求資訊補充』
        // 3. 以日期反向排序 （可以取到最新的）
        replyTraces = replyTraces.stream()
                .filter(trace -> trace.getCreatedUser().getSid().equals(executor.getSid()))
                .filter(trace -> RequireTraceType.REQUIRE_INFO_MEMO.equals(trace.getRequireTraceType()))
                .sorted(Comparator.comparing(RequireTrace::getCreatedDate).reversed())
                .collect(Collectors.toList());

        Date todayStart = searchService.transStartDate(new Date());
        Date todayEnd = searchService.transEndDate(new Date());

        // 過濾是否為今日
        for (RequireTrace each : replyTraces) {
            if (each.getCreatedDate().after(todayStart) && each.getCreatedDate().before(todayEnd)) {
                return each;
            }
        }
        return null;
    }

    /**
     * 儲存需求資訊回覆補充
     *
     * @param require
     * @param execUser
     * @param trace
     * @param isAddNotify
     * @return
     * @throws UserMessageException
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveRequireReply(Require require, User execUser, RequireTrace trace, Boolean isAddNotify) throws UserMessageException {

        if (!requireShowService.showRequireAddInfoBtn(this.findByReqObj(require))) {
            throw new UserMessageException(WkMessage.SESSION_TIMEOUT, InfomationLevel.WARN);
        }

        // ====================================
        // 更新主檔
        // ====================================
        boolean isOld = !Strings.isNullOrEmpty(trace.getSid());
        trace.setRequireTraceContent(jsoupUtils.clearCssTag(trace.getRequireTraceContentCss()));
        trace.setCreatedDate(new Date());
        requireTraceService.save(trace);
        if (isOld) {
            fbkAttachService.findAttachsByLazy(trace).forEach(each -> each.setKeyChecked(Boolean.TRUE));
            require.setReadReason(ReqToBeReadType.UPDATE_REQ_INFO_MEMO);
        } else {
            require.setReadReason(ReqToBeReadType.NEW_REQ_INFO_MEMO);
        }
        fbkAttachService.linkRelation(trace.getAttachments(), trace, execUser);

        // ====================================
        // 將『已經讀取』過單據的人，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.REQUIRE,
                require.getSid(),
                execUser.getSid());

        // ====================================
        // 處理系統通知 (需求回覆補充)
        // ====================================
        if (isAddNotify) {
            try {
                this.sysNotifyHelper.processForAddInfo(require.getSid(), execUser.getSid());
            } catch (Exception e) {
                log.error("執行系統通知失敗!" + e.getMessage(), e);
            }
        }
    }

    /**
     * 即時查詢資料
     *
     * @param requires
     * @return
     */
    @Transactional(readOnly = true)
    public List<Require> findRelevanceByRequire(List<Require> requires) {
        return requireRepository.findRelevanceByRequire(requires);
    }

    /**
     * 關聯查詢
     *
     * @param hasLink
     * @param excludeRequire
     * @param relevanceText
     * @param startRelevanceDate
     * @param endRelevanceDate
     * @return
     */
    @Transactional(readOnly = true)
    public List<Require> findRelevanceByDateAndTextAndExcludeRequire(Boolean hasLink, Require excludeRequire, String relevanceText,
            Date startRelevanceDate, Date endRelevanceDate) {
        String text = "%" + reqularUtils.replaceIllegalSqlLikeStr(relevanceText) + "%";
        return requireRepository.findRelevanceByDateAndTextAndExcludeRequire(hasLink, excludeRequire, startRelevanceDate, endRelevanceDate, text);
    }

    /**
     * 群組成員如果任一人已閱讀，是不可以進行轉寄回收
     *
     * @param records
     * @param users
     * @return
     */
    private boolean readRecordCanRecovery(Map<String, ReadRecordTo> records, List<User> users) {
        return !users.stream()
                .filter(user -> records.containsKey(user.getSid().toString()))
                .map(user -> records.get(user.getSid().toString()))
                .anyMatch(to -> to.getType().equals(ReadRecordType.HAS_READ));
    }

    /**
     * 過濾出部門裡，已讀取資料部門
     *
     * @param require
     * @param hasNotReadOrg
     * @return
     */
    @Transactional(readOnly = true)
    public List<Org> filterHasReadOrg(Require require, List<Org> hasNotReadOrg) {
        Map<String, ReadRecordTo> records = require.getReadRecordGroup().getRecords();
        List<Org> hasReadOrg = Lists.newArrayList();
        hasNotReadOrg.stream().forEach(org -> {
            List<User> users = userService.findByPrimaryOrg(Activation.ACTIVE, org);
            boolean orgReadRecordCanRecovery = this.readRecordCanRecovery(records, users);
            if (!orgReadRecordCanRecovery) {
                hasReadOrg.add(org);
            }
        });
        return hasReadOrg;
    }

    /**
     * 將部門成員已讀取過的轉寄部門過濾出來
     * 
     * @param RequireNo 需求單號
     * @param depSids   需判斷的部門
     * @return 已讀取過的部門 sid
     */
    @Transactional(readOnly = true)
    public List<Integer> filterHasReadOrgSid(String RequireNo, List<Integer> depSids) {

        // 防呆
        if (WkStringUtils.isEmpty(depSids)) {
            return Lists.newArrayList();
        }

        // 查詢需求單主檔
        Require require = this.findByReqNo(RequireNo);

        // sid -> org
        List<Org> orgs = WkOrgCache.getInstance().findBySids(depSids);

        // 判斷
        List<Org> hasReadOrgs = this.filterHasReadOrg(require, orgs);

        if (WkStringUtils.isEmpty(hasReadOrgs)) {
            return Lists.newArrayList();
        }

        // org -> sid
        return hasReadOrgs.stream().map(Org::getSid).collect(Collectors.toList());
    }

    /**
     * 建立需求完成警告<BR/>
     * 流程未簽核部份
     *
     * @param require
     * @return
     */
    public String createCompleteWaringMsg(Require require) {
        return reqFinishHelper.createCompleteWaringMsg(this.findByReqNo(require.getRequireNo()));
    }

    /**
     * 兜組需求完成確認訊息
     *
     * @param require
     * @return
     */
    public String prepareRequireCompleteConfirmMesssag(Require require) {
        return reqFinishHelper.prepareForceCompleteConfirmMesssage(this.findByReqNo(require.getRequireNo()));
    }

    /**
     * 取回廳主資料
     *
     * @param require
     * @return
     */
    public WorkCustomer findCustomerByRequire(Require require) {
        return requireRepository.findCustomerByRequire(require);
    }

    /**
     * 取回客戶資料
     *
     * @param require
     * @return
     */
    public WorkCustomer findAuthorByRequire(Require require) {
        return requireRepository.findAuthorByRequire(require);
    }

    /**
     * 檢查此需求單是否為內部類別，及是否要自動建立ONPG
     *
     * @param req
     * @return
     */
    public boolean isPopupConfirmOnpgNotify(Require req) {
        return this.isTypeInternal(req) && req.getMapping().getSmall().getAutoCreateOnpg();
    }

    /**
     * 檢查是否為內部需求類別
     *
     * @param req
     * @return
     */
    public boolean isTypeInternal(Require req) {
        return ReqCateType.INTERNAL.equals(req.getMapping().getBig().getReqCateType());
    }

    /**
     * 修改紀錄 by 增加需求單附加檔案
     *
     * @param req
     * @param user
     */
    public void updateRecordByAddReqAttachment(Require req, User user) {
        this.updateRecordByAttachment(req, user, ReqToBeReadType.ADD_REQ_ATTACHMENT);
    }

    /**
     * 修改紀錄 by 異動需求資訊回覆補充
     *
     * @param reqSid
     * @param user
     */
    public void updateRecordByUpdateReqInfoMemo(Require req, User user) {
        this.updateRecordByAttachment(req, user, ReqToBeReadType.UPDATE_REQ_INFO_MEMO);
    }

    /**
     * 修改紀錄 by 刪除需求單附加檔案
     *
     * @param req
     * @param user
     */
    public void updateRecordByDelReqAttachment(Require req, User user) {
        this.updateRecordByAttachment(req, user, ReqToBeReadType.DEL_REQ_ATTACHMENT);
    }

    /**
     * 修改紀錄 by 修改附加檔案描述資訊內容
     *
     * @param req
     * @param user
     */
    public void updateRecordByUpdateReqAttachment(Require req, User user) {
        this.updateRecordByAttachment(req, user, ReqToBeReadType.UPDATE_REQ_ATTACHMENT);
    }

    /**
     * 更新附加檔案相關紀錄
     *
     * @param req
     * @param user
     * @param type
     * @return
     */
    private void updateRecordByAttachment(Require req, User user, ReqToBeReadType type) {

        // ====================================
        // 異動主檔
        // ====================================
        Require dbRequire = this.findByReqSid(req.getSid());
        dbRequire.setReadReason(type);
        dbRequire.setUpdatedDate(new Date());
        dbRequire.setUpdatedUser(user);
        dbRequire = requireRepository.save(dbRequire);

        // ====================================
        // 將『已經讀取』過單據的人，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.REQUIRE,
                dbRequire.getSid(),
                user.getSid());

        // ====================================
        //
        // ====================================
        req.setReadReason(dbRequire.getReadReason());
        req.setUpdatedDate(dbRequire.getUpdatedDate());
        req.setUpdatedUser(dbRequire.getUpdatedUser());
    }

    /**
     * 判斷是否為需求完成狀態
     *
     * @param requireNo
     * @return
     */
    public Boolean checkReqFinish(String requireNo) {
        return requireRepository.checkReqFinish(requireNo);
    }

    /**
     * 建立暫存訊息
     *
     * @param userSid
     * @param requireNo
     */
    @Transactional(rollbackFor = Exception.class)
    public void createReqTempNotify(Integer userSid, String requireNo) {
        RequireTempNotify obj = new RequireTempNotify();
        obj.setCreatedUser(userSid);
        obj.setRequireNo(requireNo);
        requireTempNotifyRepository.save(obj);
    }

    /**
     * 判斷是否有暫存訊息
     *
     * @param userSid
     * @param requireNo
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean checkReqTempNotify(Integer userSid, String requireNo) {
        try {
            List<String> sids = requireTempNotifyRepository.findSidByCreatedUserAndRequireNo(userSid, requireNo);
            if (sids != null && !sids.isEmpty()) {
                deleteReqTempNotify(sids);
                return true;
            }
        } catch (Exception e) {
            log.error("判斷是否有暫存訊息 ERROR!! userSid：" + userSid + " ，requireNo：" + requireNo, e);
        }
        return false;
    }

    /**
     * 需求完成時更新需求完成人員 2017-06-01
     * 
     * @param require
     * @param executor
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean updateReqFinishHelper(Require require, User executor) {
        try {
            reqModifyRepo.updateReqFinishUsr(require, executor, new Date());
            return true;
        } catch (Exception e) {
            log.error("更新需求完成時需求人員發生錯誤" + e.getMessage());
            return false;
        }
    }

    /**
     * 沙巴 報表
     * 
     * @return
     */
    public List<SabaQueryResult> findSabaByCondition(Date startDate, Date endDate, String searchText,
            String categorySid, String ncsDone, Boolean hasL1Sids, Boolean hasL2Sids, Boolean hasL3Sids,
            List<String> bCategorySids, List<String> mCategorySids, List<String> sCategorySids, Date startUpdatedDate,
            Date endUpdatedDate, Boolean hasUrgency, List<Integer> urgencyList, String requireNo, Boolean hasDepts,
            List<String> forwardDepts) {

        return requireRepository.findSabaByCondition(startDate, endDate, searchText, categorySid, ncsDone, hasL1Sids,
                hasL2Sids, hasL3Sids, bCategorySids, mCategorySids, sCategorySids, startUpdatedDate, endUpdatedDate,
                hasUrgency, urgencyList, requireNo, hasDepts, forwardDepts);
    }

    public Require save(Require entity) {
        return requireRepository.save(entity);
    }

    /**
     * 批次轉單程式-建立單位轉發(需求單)
     * 
     * @return
     */
    public List<TransRequireVO> findRequireByCreateDeptAndUser(List<Integer> deptSids, List<Integer> userSids) {
        List<TransRequireVO> resultList = trNamedQueryRepository.findRequireByCreateDeptAndUser(deptSids, userSids);
        return convert(resultList);
    }

    /**
     * 批次轉單程式-轉寄單位轉發(需求單)
     * 
     * @return
     */
    public List<TransRequireVO> findAlertBoxByDepts(List<Integer> deptSids) {
        List<TransRequireVO> resultList = trNamedQueryRepository.findAlertBoxByDepts(deptSids);
        return convert(resultList);
    }

    private List<TransRequireVO> convert(List<TransRequireVO> resultList) {
        for (TransRequireVO vo : resultList) {
            vo.setType(RequireTransProgramType.REQUIRE.name());
            vo.setCreateDept(WkOrgCache.getInstance().findNameBySid(vo.getDeptSid()));
            vo.setCreateUserName(WkUserCache.getInstance().findBySid(vo.getUserSid()).getId());
            vo.setTheme(searchService.combineFromJsonStr(vo.getTheme()));
            vo.setStatus(RequireStatusType.valueOf(vo.getStatus()).getValue());
        }
        return resultList;
    }

    /**
     * 刪除暫存訊息
     *
     * @param sids
     */
    public void deleteReqTempNotify(List<String> sids) {
        if (sids == null || sids.isEmpty()) {
            return;
        }
        sids.forEach(each -> {
            requireTempNotifyRepository.delete(each);
        });
    }

    /**
     * 以立案單位查詢
     * 
     * @param createDep
     * @return
     */
    public List<Require> findByCreateDep(Org createDep) {
        return this.requireRepository.findByCreateDep(createDep);
    }

    /**
     * update by 提交<br/>
     * 無需單位簽核時使用
     * 
     * @param requireSid           需求單sid
     * @param hasTrace             追蹤
     * @param requireStatus
     * @param readReason
     * @param readRecordGroup
     * @param requireEstablishDate
     * @param categoryConfirmCode
     * @return
     */
    public Require updateForFlowApporve(
            String requireSid,
            Boolean hasTrace,
            RequireStatusType requireStatus,
            ReqToBeReadType readReason,
            Date requireEstablishDate,
            CateConfirmType categoryConfirmCode,
            Integer execUserSid) {

        Require require = this.findByReqSid(requireSid);

        // ====================================
        // 將『已經讀取』過單據的人，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.REQUIRE,
                require.getSid(),
                execUserSid);

        // ====================================
        // 更新主檔
        // ====================================

        require.setHasTrace(hasTrace);
        require.setRequireStatus(requireStatus);
        require.setReadReason(readReason);
        require.setRequireEstablishDate(requireEstablishDate);
        require.setCategoryConfirmCode(categoryConfirmCode);

        return this.save(require);

    }

    /**
     * 1.需求單模版值css內容移至RequireCssContent <BR/>
     * 2.建立索引至 RequireIndexDictionary <BR/>
     * 3.不管新增或編輯都需要進行重建
     *
     * @param require
     */
    public Require buildCssContentAndReqIndex(
            String requireSid,
            String requireNo,
            Map<String, ComBase> mapValue,
            Date execDate) {

        Require require = this.findByReqSid(requireSid);

        // ====================================
        // 建立CSS內容
        // ====================================
        List<RequireCssContent> requireCssContents = Lists.newArrayList();

        for (ComBase com : mapValue.values()) {
            if (com instanceof ComBaseCss) {
                ComBaseCss comCss = (ComBaseCss) com;
                RequireCssContent css = new RequireCssContent();
                css.setComId(comCss.getComId());
                css.setFieldName(comCss.getName());
                css.setRequire(require);
                css.setRequireNo(requireNo);
                css.setContentCss(comCss.catchCssText());
                requireCssContents.add(css);
            }
        }

        // this.cssHelper.createByComs(requireSid, requireNo, mapValue.values(),
        // execDate);

        // ====================================
        // 建立索引內容
        // ====================================
        // this.indexHelper.createByComs(requireSid, requireNo, mapValue.values(),
        // execDate);

        List<RequireIndexDictionary> requireIndexDictionarys = Lists.newArrayList();

        for (ComBase com : mapValue.values()) {
            if (com.getNeedCreateIndex()) {
                RequireIndexDictionary index = new RequireIndexDictionary();
                index.setFieldName(com.getName());
                index.setFieldContent(com.getIndexContent());
                index.setMapping(require.getMapping());
                index.setRequire(require);
                index.setRequireNo(requireNo);
                index.setCreateDate(execDate);
                index.setLastUpdatedDate(execDate);
                requireIndexDictionarys.add(index);
            }
        }

        // ====================================
        // save
        // ====================================
        require.setCssContents(requireCssContents);
        require.setIndex(requireIndexDictionarys);
        this.save(require);

        // ====================================
        // 記錄後移除 (未知作用)
        // ====================================
        for (ComBase com : mapValue.values()) {
            if (com instanceof ComBaseCss) {
                ComBaseCss comCss = (ComBaseCss) com;
                comCss.clearCssText();
            }
        }

        return this.findByReqSid(requireSid);
    }

    /**
     * 儲存主檔資料, 且異動相關資料<br/>
     * 1.主檔 save tr_require <br/>
     * 2.儲存 檢查項目檔 save require_check_item <br/>
     * 3.處理附加檔案 save tr_attachment <br/>
     * 4.處理SABA設定 save tr_require_extra_setting <br/>
     * 5.重建 CSS欄位 內容和 index 資料
     * 
     * @param editRequire
     * @param selectedCheckItemTypes
     * @param valueMap
     * @param execUser
     * @param execDate
     * @param isSubmitAction
     */
    public void saveContentAndRelationData(
            Require editRequire,
            List<RequireCheckItemType> selectedCheckItemTypes,
            Map<String, ComBase> valueMap,
            User execUser,
            Date execDate,
            boolean isSubmitAction) {

        // ====================================
        // 儲存主檔 (save tr_require)
        // ====================================
        // 依據重設內容
        editRequire.setContent(this.createContentTo(valueMap));
        // 儲存
        editRequire = this.save(editRequire);

        // ====================================
        // 儲存 檢查項目檔 (save require_check_item)
        // ====================================
        this.requireCheckItemService.saveCheckItemTypes(
                editRequire.getSid(),
                selectedCheckItemTypes,
                execUser.getSid(),
                execDate);

        // ====================================
        // 處理SABA設定 save tr_require_extra_setting
        // ====================================
        if (isSubmitAction) {
            this.saveSabaSetting(
                    editRequire.getSid(),
                    editRequire.getRequireNo(),
                    valueMap,
                    execUser,
                    execDate);
        }

        // ====================================
        // 處理附加檔案
        // ====================================
        reqAttachService.saveAttach(editRequire.getAttachments());
        reqAttachService.linkRelation(editRequire.getAttachments(), editRequire, execUser);

        // ====================================
        // 重建 CSS欄位 內容和 index 資料
        // ====================================
        editRequire = this.removeAndBuildCssAndIndex(
                editRequire.getSid(),
                editRequire.getRequireNo(),
                valueMap,
                execDate);

    }

    /**
     * 初始化草稿
     *
     * @param execDept
     * @param execComp
     * @param editRequire
     * @param mapping
     * @param executor
     * @return
     * @throws UserMessageException
     */
    @Transactional(rollbackFor = Exception.class)
    public void initDraft(
            Require editRequire,
            CategoryKeyMapping mapping,
            Integer execUserSid,
            Date execDate) throws UserMessageException {

        // ====================================
        // 取得登入者資料
        // ====================================
        User execUser = this.wkUserCache.findBySid(execUserSid);
        if (execUser == null || execUser.getPrimaryOrg() == null || execUser.getPrimaryOrg().getCompanySid() == null) {
            throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }
        Org execDep = this.wkOrgCache.findBySid(execUser.getPrimaryOrg().getSid());
        Org execCompany = this.wkOrgCache.findBySid(execUser.getPrimaryOrg().getCompanySid());

        // ====================================
        // 設定主檔欄位
        // ====================================
        editRequire.setMapping(mapping);
        editRequire.setCreateCompany(execCompany);
        editRequire.setCreatedUser(execUser);
        editRequire.setCreateDep(execDep);
        editRequire.setRequireStatus(RequireStatusType.DRAFT);
        editRequire.setDraftDate(execDate);
        editRequire.setDraftUpdatedDate(execDate);
        editRequire.setRequireNo(this.formNumberService.generateDraftNum(execCompany.getId()));

        // 調整期望完成日
        editRequire.setHopeDate(
                this.prepareHopeFinishDate(
                        editRequire.getHopeDate(),
                        mapping.getSmall().getDefaultDueDays()));
    }

    /**
     * 初始化需求單 內容
     * 
     * @param editRequire
     * @param execUserSid 執行者 sid
     * @param execDate    執行時間
     * @param syncFormTo  MMS 介接時才傳入
     * @throws UserMessageException
     */
    @Transactional(rollbackFor = Exception.class)
    public void initRequire(
            Require editRequire,
            Integer execUserSid,
            Date execDate,
            SyncFormTo syncFormTo) throws UserMessageException {

        // ====================================
        // 取得執行者資料
        // ====================================
        User execUser = this.wkUserCache.findBySid(execUserSid);
        if (execUser == null || execUser.getPrimaryOrg() == null || execUser.getPrimaryOrg().getCompanySid() == null) {
            throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }
        Org execDep = this.wkOrgCache.findBySid(execUser.getPrimaryOrg().getSid());
        Org execCompany = this.wkOrgCache.findBySid(execUser.getPrimaryOrg().getCompanySid());

        // ====================================
        // 設定主檔欄位
        // ====================================
        Date currentDate = new Date();
        editRequire.setRequireNo(this.formNumberService.generateTRNum(execCompany.getId()));
        editRequire.setCreatedDate(currentDate);
        editRequire.setCreatedUser(execUser);
        editRequire.setCreateDep(execDep);
        editRequire.setCreateCompany(execCompany);
        editRequire.setMapping(editRequire.getMapping());
        editRequire.setUpdatedDate(execDate);
        editRequire.setUpdatedUser(execUser);
        // 結案後是否可執行on程式
        editRequire.setWhenCloseExeOnpg(editRequire.getMapping().getSmall().getWhenCloseExeOnpg());

        if (syncFormTo != null) {
            // 為MMS 介接時，期望完成日為 MMS 單據維護日
            editRequire.setHopeDate(syncFormTo.getMaintenanceDate());
        } else {
            // 調整期望完成日 (依據小類)
            editRequire.setHopeDate(
                    this.prepareHopeFinishDate(
                            editRequire.getHopeDate(),
                            editRequire.getMapping().getSmall().getDefaultDueDays()));
        }

    }

    /**
     * 處理期望完成日
     * 
     * @param nowValueDate
     * @param defaultDueDays
     * @return
     */
    private Date prepareHopeFinishDate(Date nowValueDate, int defaultDueDays) {

        // 判斷目前的期望完成日, 大於今日，則無需修改
        if (WkDateUtils.isAfter(nowValueDate, new Date(), false)) {
            return nowValueDate;
        }

        // 當日期小於今日時 產生新的期望完成日 （依據小類設定）
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, defaultDueDays);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }

    /**
     * 更新 <br/>
     * 1.需求單位是否簽核註記 <br/>
     * 2.需求製作進度
     * 
     * @param requireSid     需求單 sid
     * @param hasReqUnitSign 需求單位是否簽核
     * @param requireStatus  製作進度
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateHasReqUnitSignAndRequireStatus(
            String requireSid,
            boolean hasReqUnitSign,
            RequireUnitSignInfo reqUnitSign,
            RequireStatusType requireStatus) {

        Require require = this.findByReqSid(requireSid);
        require.setHasReqUnitSign(hasReqUnitSign);
        require.setReqUnitSign(reqUnitSign);
        require.setRequireStatus(requireStatus);
        this.reqModifyRepo.save(require);
        // this.reqModifyDao.updateHasReqUnitSignAndRequireStatus(
        // requireSid,
        // hasReqUnitSign,
        // reqUnitSign,
        // requireStatus);
    }

    /**
     * @param requireStatus
     * @param startCreateDate
     * @return
     */
    public List<Require> findByRequireStatusAndCreateDate(
            List<RequireStatusType> requireStatus,
            Date startCreateDate,
            Date endCreateDate) {

        if (WkStringUtils.isEmpty(requireStatus) || startCreateDate == null) {
            log.debug("傳入條件為空");
            return Lists.newArrayList();
        }

        // 查詢方法為 > 故減一天達成大於等於的效果
        return this.requireRepository.findByRequireStatusInAndCreatedDateGreaterThanAndCreatedDateLessThan(
                requireStatus,
                new LocalDate(startCreateDate).minusDays(1).toDate(),
                new LocalDate(endCreateDate).plusDays(1).toDate());
    }

    /**
     * 更新『主責單位』、『主責單位負責人』
     * 
     * @param requireSid     需求單 sid
     * @param inChargeDepSid 主責單位 orgSid
     * @param inChargeUsrSid 主責單位窗口 userSid
     * @param execUserSid    執行者 userSid
     * @param execDate       執行日期
     * @param isOrgTrns      是否為組織異動功能呼叫 (會在追蹤文字上增加訊息)
     * @throws UserMessageException 找不到需求單資料時拋出
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateInCharge(
            String requireSid,
            Integer inChargeDepSid,
            Integer inChargeUsrSid,
            Integer execUserSid,
            Date execDate,
            boolean isOrgTrns) throws UserMessageException {

        // ====================================
        // 取得主檔資料
        // ====================================
        // 查詢
        Require require = this.findByReqSid(requireSid);
        if (require == null) {
            log.warn("找不到需求單資料檔 requireSid:[{}]", requireSid);
            throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }
        // 記錄原有資訊
        Integer oldInchargeDepSid = require.getInChargeDep();
        Integer oldInchargeUserSid = require.getInChargeUsr();

        // ====================================
        // 建立追蹤訊息
        // ====================================
        String traceContent = requireTraceService.createChangeInChargeTraceMessage(
                oldInchargeDepSid,
                inChargeDepSid,
                oldInchargeUserSid,
                inChargeUsrSid);

        // 判斷無資料異動, 無需執行
        if (WkStringUtils.isEmpty(traceContent)) {
            log.debug("主責單位資料未異動, 無需執行");
            return;
        }

        // 組織異動備註文字
        if (isOrgTrns) {
            traceContent += "<br/><br/><span class='WS1-1-3b'>備註：因組織異動由管理員批次轉換</span>";
        }

        // ====================================
        // 計時 start
        // ====================================
        ProcessLog processLog = new ProcessLog(require.getRequireNo(), "", "異動主責單位");

        // ====================================
        // 寫追蹤記錄
        // ====================================
        // 修改時，需寫異動記錄(追蹤)。製作進度為『待檢查確認』前不寫, 因建單簽核階段都是需求單位異動，無需寫記錄
        // 組織異動時，強制寫追蹤
        boolean isFlowBeforeWaitCheck = require.getRequireStatus() != null
                && require.getRequireStatus().isFlowBeforeWaitCheck();

        if (!isFlowBeforeWaitCheck || isOrgTrns) {
            // 建立追蹤記錄
            RequireTrace requireTrace = this.requireTraceService.createNewTrace(
                    requireSid,
                    WkUserCache.getInstance().findBySid(execUserSid),
                    execDate);
            requireTrace.setRequireTraceType(RequireTraceType.CHANGE_IN_CHARGE);
            requireTrace.setRequireTraceContent(WkJsoupUtils.getInstance().clearCssTag(traceContent));
            requireTrace.setRequireTraceContentCss(traceContent);
            this.requireTraceService.save(requireTrace);

            // 註記主檔有追蹤
            require.setHasTrace(true);
        }

        // ====================================
        // update require
        // ====================================
        require.setInChargeDep(inChargeDepSid);
        require.setInChargeUsr(inChargeUsrSid);
        this.requireRepository.save(require);

        // ====================================
        // log
        // ====================================
        log.info(processLog.prepareCompleteLog(traceContent));
    }

    /**
     * 計算需求單(待檢查確認)的數量
     *
     * @return
     */
    @Transactional(readOnly = true)
    public Integer countWaitCheck(Integer userSid) {
        // ====================================
        // 查詢使用者『可檢查』的『檢查項目』
        // ====================================
        // 查詢
        List<RequireCheckItemType> checkItemTypes = this.settingCheckConfirmRightService.findUserCheckRights(userSid);
        if (WkStringUtils.isEmpty(checkItemTypes)) {
            return 0;
        }

        // ====================================
        // 查詢筆數
        // ====================================
        return this.requireRepository.queryWaitCheckCountByRequireCheckItemTypeIn(
                checkItemTypes.stream()
                        .map(RequireCheckItemType::name)
                        .collect(Collectors.toList()));
    }

    /**
     * 取得需求單主題
     *
     * @param req
     * @return
     */
    public String getReqTheme(Require req) {
        Map<String, ComBase> valueMap = templateService.loadTemplateFieldValue(req);
        return templateService.findTheme(req, valueMap);
    }

    /**
     * 準備閱讀記錄 (填單單位+分派單位+通知單位 人員)
     *
     * @param onpg
     * @return
     */
    public Set<Integer> prepareReadRecordUserSids(
            Require require,
            List<Integer> afterAssignDepSids,
            List<Integer> afterNoticeDepSids) {

        Set<Integer> allUserSids = Sets.newHashSet();

        // ====================================
        // 要被通知的單位
        // ====================================
        Set<Integer> allDepSids = Sets.newHashSet();

        // 建單單位
        if (require.getCreateDep() != null) {
            allDepSids.add(require.getCreateDep().getSid());
        }

        // 分派單位
        if (WkStringUtils.notEmpty(afterAssignDepSids)) {
            allDepSids.addAll(afterAssignDepSids);
        }

        // 通知單位
        if (WkStringUtils.notEmpty(afterNoticeDepSids)) {
            allDepSids.addAll(afterNoticeDepSids);
        }

        // ====================================
        // 取得部門下所有 user
        // ====================================
        Set<Integer> currUserSids = WkUserCache.getInstance().findUserSidByOrgsWithManager(
                allDepSids, Activation.ACTIVE);

        if (WkStringUtils.notEmpty(currUserSids)) {
            allUserSids.addAll(currUserSids);
        }

        // ====================================
        // 為組時，把部長拉進來 (組織扁平化調整 原最高為處, 降為部)
        // ====================================
        if (require.getCreateDep() != null
                && require.getCreateDep().getParent() != null
                && OrgLevel.THE_PANEL.equals(require.getCreateDep().getLevel())) {

            Set<Integer> managerUserSids = WkOrgUtils.findOrgManagerUserSidByOrgSid(
                    require.getCreateDep().getParent().getSid());
            if (WkStringUtils.notEmpty(managerUserSids)) {
                allUserSids.addAll(managerUserSids);
            }
        }

        // ====================================
        // 轉寄人員
        // ====================================
        Set<Integer> reciveUserSids = trAlertInboxService.findReciveUserSids(require.getSid());
        if (WkStringUtils.notEmpty(reciveUserSids)) {
            allUserSids.addAll(reciveUserSids);
        }

        // 移除停用人員
        return allUserSids.stream()
                .filter(userSid -> WkUserUtils.isActive(userSid))
                .collect(Collectors.toSet());
    }

    /**
     * 同步『是否有ON程式單flag』的狀態
     */
    public int fixHasOnpginfo() {
        StringBuffer sql = new StringBuffer();
        sql.append("UPDATE tr_require tr ");
        sql.append("SET    tr.has_onpginfo = 1 ");
        sql.append("WHERE  tr.has_onpginfo = 0 ");
        sql.append("       AND EXISTS (SELECT 1 ");
        sql.append("                   FROM   work_onpg wo ");
        sql.append("                   WHERE  wo.onpg_source_sid = tr.require_sid ");
        sql.append("                          AND status = 0);");

        int updateCount = this.jdbcTemplate.update(sql.toString());

        if (updateCount > 0) {
            log.warn("執行[修正ON程式單狀態]成功，修正[" + updateCount + "]筆");
        } else {
            log.debug("執行[修正ON程式單狀態]成功，修正[" + updateCount + "]筆");
        }

        return updateCount;

    }

}
