/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.othset;

import com.cy.tech.request.repository.trostrans.TrOsTransRepository;
import com.cy.tech.request.vo.require.tros.TrOs;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 其他設定資訊Service
 *
 * @author brain0925_liao
 */
@Component
public class TrOsTransService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8118060371755018352L;
    /** TrOsTransRepository */
    @Autowired
    private TrOsTransRepository trOsTransRepository;

    /**
     * 取得其他設定資訊設定 By os_no
     *
     * @param os_no
     * @return
     */
    public TrOs findByOsNo(String os_no) {
        return trOsTransRepository.findByOsNo(os_no);
    }

    /**
     * 更新其他設定資訊建立部門欄位 By os_no
     *
     * @param depSid 建立部門Sid
     * @param os_no
     */
    public void updateDepsidByPtNo(Integer depSid, String os_no) {
        trOsTransRepository.updateDepsid(os_no, depSid);
    }

    /**
     * 更新其他設定資訊通知部門欄位 By sid
     *
     * @param sid 其他設定資訊Sid
     * @param noticeDeps 通知部門物件
     * @return
     */
    public int updateNoticeDeps(String sid, JsonStringListTo noticeDeps) {
        return trOsTransRepository.updateNoticeDeps(sid, noticeDeps);
    }
}
