package com.cy.tech.request.logic.enumerate;

import com.cy.work.common.utils.WkStringUtils;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 退件原因
 *
 * @author jason_h
 */
@Slf4j
public enum RejectReason {

    /** 分類有誤 */
    CATEGORY_MISTAKE("分類有誤"),
    /** 其他 */
    OTHER("其他");

    @Getter
    private String label;

    /**
     * 此階段為流程終點狀態
     */
    @Getter
    private boolean inFinish;

    private RejectReason(String label) {
        this.label = label;
    }

    /**
     * @param itemName
     * @return
     */
    public static RejectReason safeValueOf(String itemName) {
        if (WkStringUtils.notEmpty(itemName)) {
            for (RejectReason item : RejectReason.values()) {
                if (item.name().equals(itemName)) {
                    return item;
                }
            }
        }
        log.warn("類別轉換失敗:[{}]", itemName);
        return null;
    }

    /**
     * @param itemName
     * @return
     */
    public static String safeGetDescr(String itemName) {
        RejectReason item = safeValueOf(itemName);
        if (item != null) {
            return item.getLabel();
        }
        return "";
    }
}
