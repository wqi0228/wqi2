/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.helper;

import com.cy.tech.request.vo.annotation.TemplateComMapParserDefaultValueAnnotation;
import com.cy.tech.request.vo.annotation.TemplateComMapParserInteractValueAnnotation;
import com.cy.tech.request.vo.annotation.TemplateComMapParserInteractValueAnnotations;
import com.cy.tech.request.vo.enums.ComType;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import com.cy.tech.request.vo.value.to.TemplateComMapParserTo;
import com.cy.tech.request.vo.value.to.TemplateDefaultValueTo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Component;

/**
 * 解析模版元件內標註
 *
 * @author shaun
 */
@Component
public class TemplateDefaultValueHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1242878934153440532L;

    /**
     * 建立模版元件預設值列表
     *
     * @param mappField
     * @return
     */
    public List<TemplateComMapParserTo> createTemplateComMapParserDefaultValueTos(FieldKeyMapping mappField) {
        //建立空資料模版
        List<TemplateComMapParserTo> empty = this.createEmptyComMapParserDefaultValueTos(mappField.getFieldComponentType());
        if (mappField.getDefaultValue() == null || mappField.getDefaultValue().getValue() == null) {
            return empty;
        }
        //載入原先資料
        Map<Integer, Object> map = mappField.getDefaultValue().getValue();
        for (TemplateComMapParserTo each : empty) {
            if (map.containsKey(each.getKey())) {
                each.setValue(map.get(each.getKey()));
            }
        }
        return empty;
    }

    /**
     * 建立模版元件互動預設值列表
     *
     * @param mappField
     * @return
     */
    public List<TemplateComMapParserTo> createTemplateComMapParserInteractValueTos(FieldKeyMapping mappField) {
        //建立空資料模版
        List<TemplateComMapParserTo> empty = this.createEmptyComMapParserInteractValueTos(mappField.getFieldComponentType());
        if (mappField.getInteractComValue() == null || mappField.getInteractComValue().getValue() == null) {
            return empty;
        }
        //載入原先資料
        Map<Integer, Object> map = mappField.getInteractComValue().getValue();
        for (TemplateComMapParserTo each : empty) {
            if (map.containsKey(each.getKey())) {
                each.setValue(map.get(each.getKey()));
            }
        }
        return empty;
    }

    /**
     * 建立模版元件預設值列表(空的)
     *
     * @param comType
     * @return
     */
    @SuppressWarnings("rawtypes")
    public List<TemplateComMapParserTo> createEmptyComMapParserDefaultValueTos(ComType comType) {
        Class comClz = comType.getComClz();
        return this.createEmptyComMapParserDefaultValueTos(comClz);
    }

    /**
     * 解析元件內容
     *
     * @param clz
     * @return
     */
    @SuppressWarnings("rawtypes")
    private List<TemplateComMapParserTo> createEmptyComMapParserDefaultValueTos(Class clz) {
        List<TemplateComMapParserTo> tos = Lists.newArrayList();
        List<Field> list = Lists.newArrayList();
        Field[] fields = clz.getDeclaredFields();
        Field[] superFields = clz.getSuperclass().getDeclaredFields();
        list.addAll(Lists.newArrayList(fields));
        list.addAll(Lists.newArrayList(superFields));
        for (Field each : list) {
            TemplateComMapParserDefaultValueAnnotation anno = each.getAnnotation(TemplateComMapParserDefaultValueAnnotation.class);
            if (anno == null) {
                continue;
            }
            TemplateComMapParserTo to = new TemplateComMapParserTo(anno.key(), anno.valueClz(), anno.valueName(), anno.desc(), null, anno.canEdit());
            tos.add(to);
        }
        return tos;
    }

    /**
     * 建立模版元件互動預設值列表(空的)
     *
     * @param comType
     * @return
     */
    @SuppressWarnings("rawtypes")
    public List<TemplateComMapParserTo> createEmptyComMapParserInteractValueTos(ComType comType) {
        Class comClz = comType.getComClz();
        return this.createEmptyComMapParserInteractValueTos(comClz);
    }

    /**
     * 解析元件互動內容
     *
     * @param clz
     * @return
     */
    @SuppressWarnings("rawtypes")
    private List<TemplateComMapParserTo> createEmptyComMapParserInteractValueTos(Class clz) {
        List<TemplateComMapParserTo> tos = Lists.newArrayList();
        Method[] methods = clz.getDeclaredMethods();
        //子項目
        for (Method each : methods) {
            TemplateComMapParserInteractValueAnnotation anno = each.getAnnotation(TemplateComMapParserInteractValueAnnotation.class);
            if (anno != null) {
                this.addParseInteractValue(tos, anno);
            }
            TemplateComMapParserInteractValueAnnotations annos = each.getAnnotation(TemplateComMapParserInteractValueAnnotations.class);
            if (annos != null) {
                for (TemplateComMapParserInteractValueAnnotation annoEach : annos.value()) {
                    this.addParseInteractValue(tos, annoEach);
                }
            }
        }
        return tos;
    }

    private void addParseInteractValue(List<TemplateComMapParserTo> tos, TemplateComMapParserInteractValueAnnotation anno) {
        TemplateComMapParserTo to = new TemplateComMapParserTo(anno.key(), anno.valueClz(), anno.valueName(), anno.desc(), null, true);
        tos.add(to);
    }

    /**
     * 建立模版欄位預設值物件
     *
     * @param tos
     * @return
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public TemplateDefaultValueTo createTemplateDefaultValue(List<TemplateComMapParserTo> tos) {
        TemplateDefaultValueTo defValueTo = new TemplateDefaultValueTo();
        Map<Integer, Object> map = Maps.newHashMap();
        for (TemplateComMapParserTo each : tos) {
            Class valueClz = each.getValueClz();
            //布林
            if (valueClz.isAssignableFrom(Boolean.class)) {
                if (each.getValue() != null) {
                    map.put(each.getKey(), Boolean.valueOf(each.getValue().toString()));
                } else {
                    map.put(each.getKey(), each.getValue());
                }
                continue;
            }
            //String and Null Or Other
            map.put(each.getKey(), each.getValue());
        }
        defValueTo.setValue(map);
        return defValueTo;
    }

}
