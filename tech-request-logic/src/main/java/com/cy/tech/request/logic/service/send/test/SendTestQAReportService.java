/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.send.test;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.RequireReadRecordHelper;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.repository.worktest.WorkTestQAReportRepo;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.vo.worktest.WorkTestQAReport;
import com.cy.tech.request.vo.worktest.enums.WorkTestInfoHistoryBehavior;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkJsoupUtils;

/**
 *
 * @author shaun
 */
@Component
public class SendTestQAReportService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4325014671920632454L;
    @Autowired
    private RequireService reqService;
    @Autowired
    private SendTestService stService;
    @Autowired
    private SendTestShowService stsService;
    @Autowired
    private SendTestHistoryService sthService;
    @Autowired
    private transient RequireReadRecordHelper requireReadRecordHelper;
    @Autowired
    private WkJsoupUtils jsoupUtils;
    @Autowired
    private WorkTestQAReportRepo qaDao;

    public WorkTestQAReport createEmptyQAReport(WorkTestInfo testInfo, User executor) {
        WorkTestQAReport qa = new WorkTestQAReport();
        qa.setTestInfo(testInfo);
        qa.setTestinfoNo(testInfo.getTestinfoNo());
        qa.setSourceType(testInfo.getSourceType());
        qa.setSourceSid(testInfo.getSourceSid());
        qa.setSourceNo(testInfo.getSourceNo());
        qa.setStatus(Activation.ACTIVE);
        qa.setCreatedUser(executor);
        qa.setCreatedDate(new Date());
        qa.setUpdatedUser(executor);
        qa.setUpdatedDate(new Date());
        qa.setHistory(sthService.createEmptyHistory(testInfo, executor));
        qa.getHistory().setBehavior(WorkTestInfoHistoryBehavior.QA_TEST_COMPLETE);
        return qa;
    }

    @Transactional(readOnly = true)
    public List<WorkTestQAReport> findByTestInfo(WorkTestInfo testInfo) {
        try {
            testInfo.getQaReport().size();
        } catch (LazyInitializationException e) {
            //log.debug("findByTestInfo lazy init error :" + e.getMessage(), e);
            testInfo.setQaReport(qaDao.findByTestInfoOrderByUpdatedDateDesc(testInfo));
        }
        return testInfo.getQaReport();
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveByNewQAReport(WorkTestQAReport qaReport, User loginUser) throws IllegalAccessException {
        if (!stsService.renderedTestReportBtn(
                reqService.findByReqSid(qaReport.getSourceSid()),
                stService.findByTestinfoNo(qaReport.getTestinfoNo()),
                WkUserCache.getInstance().findBySid(qaReport.getCreatedUser().getSid()))) {
            throw new IllegalAccessException("無操作權限，送測單已被更新！");
        }

        qaReport.setContent(jsoupUtils.clearCssTag(qaReport.getContentCss()));
        qaReport.setSid(qaDao.save(qaReport).getSid());

        WorkTestInfo testInfo = stService.findOne(qaReport.getTestInfo().getSid());
        
        this.findByTestInfo(testInfo).add(qaReport);

        testInfo.setTestinfoStatus(WorkTestStatus.QA_TEST_COMPLETE);
        testInfo.setReadReason(WaitReadReasonType.TEST_QA_TEST);
        testInfo.setReadUpdateDate(new Date());
        testInfo.setFinishDate(new Date());
        sthService.createQAReportHistory(testInfo, qaReport);
        stService.save(testInfo, loginUser);
        

        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.WORKTESTSIGNINFO,
                testInfo.getSid(),
                loginUser.getSid());
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveByEditQAReport(WorkTestQAReport qaReport, User executor) throws IllegalAccessException {
        if (!stsService.renderedTestReportBtn(
                reqService.findByReqSid(qaReport.getSourceSid()),
                stService.findByTestinfoNo(qaReport.getTestinfoNo()),
                WkUserCache.getInstance().findBySid(qaReport.getCreatedUser().getSid()))) {
            throw new IllegalAccessException("無操作權限，送測單已被更新！");
        }

        qaReport.setUpdatedDate(new Date());
        qaReport.setUpdatedUser(executor);
        qaDao.save(qaReport);
        sthService.update(qaReport.getHistory(), executor);
        sthService.sortHistory(qaReport.getTestInfo());
    }

}
