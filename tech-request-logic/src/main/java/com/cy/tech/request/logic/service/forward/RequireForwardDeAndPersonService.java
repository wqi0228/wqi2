/**
 * 
 */
package com.cy.tech.request.logic.service.forward;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.AlertInboxService;
import com.cy.tech.request.logic.service.RequireReadRecordHelper;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.TrAlertInboxSendGroupService;
import com.cy.tech.request.logic.service.TrAlertInboxService;
import com.cy.tech.request.repository.require.ReqModifyRepo;
import com.cy.tech.request.repository.require.TrAlertInboxRepository;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.ForwardType;
import com.cy.tech.request.vo.enums.InboxType;
import com.cy.tech.request.vo.enums.ReadRecordStatus;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.TrAlertInbox;
import com.cy.tech.request.vo.require.TrAlertInboxSendGroup;
import com.cy.tech.request.vo.require.TrAlertInboxVO;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.common.vo.value.to.ItemsCollectionDiffTo;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * @author allen
 */
@Service
public class RequireForwardDeAndPersonService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4106859714742064741L;
    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private TrAlertInboxService trAlertInboxService;
    @Autowired
    transient private TrAlertInboxSendGroupService trAlertInboxSendGroupService;
    @Autowired
    transient private TrAlertInboxRepository trAlertInboxRepository;
    @Autowired
    transient private AlertInboxService alertInboxService;
    @Autowired
    transient private ReqModifyRepo reqModifyRepo;
    @Autowired
    private transient RequireReadRecordHelper requireReadRecordHelper;

    // ========================================================================
    // 方法: 權限相關
    // ========================================================================
    /**
     * 判斷傳入部門中, 不能移除轉寄的, 回傳原因, 若無則回傳空字串
     * 
     * @param requireNo    需求單號
     * @param depSids      部門 sid
     * @param loginUserSid 登入者 sid
     * @return
     */
    public String prepareCannotRemoveForwordReasons(
            String requireNo,
            List<TrAlertInboxVO> trAlertInboxs,
            Integer loginUserSid) {

        return this.prepareCannotRemoveForwordDep(requireNo, trAlertInboxs, loginUserSid, null);
    }

    /**
     * 判斷傳入部門中, 不能移除轉寄的
     * 
     * @param requireNo    需求單號
     * @param depSids      部門 sid
     * @param loginUserSid 登入者 sid
     * @return
     */
    public List<Integer> prepareCannotRemoveForwordDepSids(
            String requireNo,
            List<TrAlertInboxVO> trAlertInboxs,
            Integer loginUserSid) {

        Set<Integer> cannotRemoveForwordDepSids = Sets.newHashSet();
        this.prepareCannotRemoveForwordDep(requireNo, trAlertInboxs, loginUserSid, cannotRemoveForwordDepSids);
        return Lists.newArrayList(cannotRemoveForwordDepSids);
    }

    /**
     * 判斷傳入部門中, 不能移除轉寄的
     * 
     * @param requireNo    需求單號
     * @param depSids      部門 sid
     * @param loginUserSid 登入者 sid
     * @return
     */
    private String prepareCannotRemoveForwordDep(
            String requireNo,
            List<TrAlertInboxVO> trAlertInboxs,
            Integer loginUserSid,
            Set<Integer> cannotRemoveForwordDepSids) {

        if (WkStringUtils.isEmpty(trAlertInboxs)) {
            return "";
        }

        // 轉寄單位中, 已被讀取的單位
        List<Integer> hasReadDepSids = this.requireService.filterHasReadOrgSid(
                requireNo,
                trAlertInboxs.stream().map(TrAlertInboxVO::getReceiveDep).collect(Collectors.toList()));

        // 取得登入者有權限移除的單位
        Set<Integer> relationDepSids = this.prepareRemoveForwordDepRight(
                loginUserSid);

        // 計算不能移除的單位
        // 逐筆檢察
        List<String> reasons = Lists.newArrayList();
        for (TrAlertInboxVO trAlertInbox : trAlertInboxs) {

            // 該部門已被讀取
            if (hasReadDepSids.contains(trAlertInbox.getReceiveDep())) {
                cannotRemoveForwordDepSids.add(trAlertInbox.getReceiveDep());
                reasons.add("【" + WkOrgUtils.findNameBySid(trAlertInbox.getReceiveDep()) + "】已有成員讀取, 無法移除!");
                continue;
            }

            // 登入者無權限異動 (不是自己或自己部門做的轉寄)
            if (!WkCommonUtils.compareByStr(loginUserSid, trAlertInbox.getSender())
                    && !relationDepSids.contains(trAlertInbox.getSenderDep())) {

                cannotRemoveForwordDepSids.add(trAlertInbox.getReceiveDep());

                String reason = "【" + WkOrgUtils.findNameBySid(trAlertInbox.getReceiveDep()) + "】";
                reason += "您無法移除此部門";
                reason += "(由&nbsp;<span class='WS1-1-3b'>";
                reason += WkOrgUtils.findNameBySid(trAlertInbox.getSenderDep()) + "-" + WkUserUtils.findNameBySid(trAlertInbox.getSender());
                reason += "</span>&nbsp;轉寄)";
                reasons.add(reason);
                continue;
            }
        }

        return String.join("<br/>", reasons);
    }

    /**
     * 準備有權限移除的轉寄部門
     * 
     * @param userSid
     * @return
     */
    private Set<Integer> prepareRemoveForwordDepRight(Integer userSid) {

        User user = WkUserCache.getInstance().findBySid(userSid);
        if (user == null) {
            return Sets.newHashSet();
        }

        // 1.管理部門
        Set<Integer> relationDepSids = WkOrgCache.getInstance().findManagerWithChildOrgSids(userSid).stream()
                .collect(Collectors.toSet());

        // 2.自己的主要部門
        if (user.getPrimaryOrg() != null) {
            relationDepSids.add(user.getPrimaryOrg().getSid());
        }
        return relationDepSids;
    }

    // ========================================================================
    // 方法:轉寄部門
    // ========================================================================
    /**
     * 儲存轉寄部門
     * 
     * @param requireNo           需求單號
     * @param afterForwardDepSids 異動後的轉寄部門
     * @param execUserSid         執行者
     * @throws UserMessageException 檢核失敗時拋出
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveForwardDep(
            String requireNo,
            List<Integer> afterForwardDepSids,
            Integer execUserSid) throws UserMessageException {

        // ================================
        // 前置
        // ================================
        Date sysDate = new Date();
        // 查詢需求單號
        Require require = this.requireService.findByReqNo(requireNo);
        if (require == null) {
            throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }
        //
        User execUser = WkUserCache.getInstance().findBySid(execUserSid);
        if (execUser == null) {
            throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }

        // ================================
        // 查詢所有部門轉寄資料
        // ================================
        // 查詢
        List<TrAlertInbox> trAlertInboxs = this.trAlertInboxRepository.findByRequireNoAndForwardTypeAndStatusOrderByCreatedDate(
                requireNo,
                ForwardType.FORWARD_DEPT,
                Activation.ACTIVE);

        // 防呆
        if (WkStringUtils.isEmpty(trAlertInboxs)) {
            trAlertInboxs = Lists.newArrayList();
        }

        // 整理未異動前轉寄部門資料
        List<Integer> beforeForwardDepSids = trAlertInboxs.stream()
                .map(TrAlertInbox::getReceiveDep)
                .collect(Collectors.toList());

        // 比對異動前後
        ItemsCollectionDiffTo<Integer> diffTo = WkCommonUtils.itemCollectionDiff(
                beforeForwardDepSids,
                afterForwardDepSids);

        // ================================
        // tr_alert_inbox_send_group
        // ================================
        String groupSid = this.processForwardDepGroup(
                require,
                trAlertInboxs,
                diffTo,
                execUserSid,
                sysDate);

        // ================================
        // tr_alert_inbox
        // ================================
        this.processUpdateForwardDep(require, trAlertInboxs, diffTo, groupSid, execUser, sysDate);

        // ================================
        // update 需求單
        // ================================
        boolean hasForwardDep = WkStringUtils.notEmpty(afterForwardDepSids);
        if (!WkCommonUtils.compareByStr(hasForwardDep, require.getHasForwardDep())) {
            require.setHasForwardDep(WkStringUtils.notEmpty(afterForwardDepSids));
            require.setUpdatedUser(execUser);
            require.setUpdatedDate(sysDate);

            this.reqModifyRepo.updateForwardMember(
                    require,
                    require.getHasForwardDep(),
                    require.getUpdatedUser(),
                    require.getUpdatedDate());

        }

        // ================================
        // 有新增轉寄單位時. 更新閱讀紀錄
        // ================================
        if (WkStringUtils.notEmpty(diffTo.getPlusItems())) {

            // 取得新增的轉寄部門下所有的 user (包含兼職主管)
            Set<Integer> forwardDepUserSids = WkUserCache.getInstance().findUserSidByOrgsWithManager(
                    diffTo.getPlusItems(),
                    Activation.ACTIVE);

            if (WkStringUtils.notEmpty(forwardDepUserSids)) {
                requireReadRecordHelper.saveUsersToWaitReadWithoutExecUser(
                        FormType.REQUIRE,
                        require.getSid(),
                        forwardDepUserSids,
                        execUser.getSid());
            }
        }

    }

    /**
     * 處理部門轉寄 tr_alert_inbox
     * 
     * @param require
     * @param trAlertInboxs
     * @param diffTo
     * @param groupSid
     * @param execUser
     * @param execDate
     */
    private void processUpdateForwardDep(
            Require require,
            List<TrAlertInbox> trAlertInboxs,
            ItemsCollectionDiffTo<Integer> diffTo,
            String groupSid,
            User execUser,
            Date execDate) {

        List<TrAlertInbox> modifyEntites = Lists.newArrayList();

        // ================================
        // 收集需要收回轉寄的部門
        // ================================
        for (TrAlertInbox trAlertInbox : trAlertInboxs) {
            if (diffTo.getReduceItems().contains(trAlertInbox.getReceiveDep())) {
                // 異動為停用
                trAlertInbox.setStatus(Activation.INACTIVE);
                trAlertInbox.setUpdatedUser(execUser.getSid());
                trAlertInbox.setUpdatedDate(execDate);

                // 加入異動清單
                modifyEntites.add(trAlertInbox);
            }
        }

        // ================================
        // 收集需要新增的轉寄部門
        // ================================
        // 不存在於已轉寄
        for (Integer addForwardDepSid : diffTo.getPlusItems()) {

            TrAlertInbox wk = new TrAlertInbox();
            wk.setRequireSid(require.getSid());
            wk.setRequireNo(require.getRequireNo());
            wk.setForwardType(ForwardType.FORWARD_DEPT);
            wk.setSender(execUser.getSid());
            wk.setSenderDep(execUser.getPrimaryOrg().getSid());
            wk.setMessage("");
            wk.setMessageCss("");
            wk.setReceive(null);
            wk.setReceiveDep(addForwardDepSid);
            wk.setInboxType(InboxType.INCOME_DEPT);
            wk.setAlert_group_sid(groupSid);
            if (WkCommonUtils.compareByStr(execUser.getPrimaryOrg().getSid(), addForwardDepSid)) {
                wk.setRead_record(ReadRecordStatus.READ);
                wk.setRead_dt(execDate);
            } else {
                wk.setRead_record(ReadRecordStatus.UNREAD);
            }
            wk.setCreatedDate(execDate);
            wk.setCreatedUser(execUser.getSid());
            modifyEntites.add(wk);
        }

        // ================================
        // insert or update
        // ================================
        if (WkStringUtils.notEmpty(modifyEntites)) {
            this.trAlertInboxService.save(modifyEntites);
        }

    }

    /**
     * 處理部門轉寄群組 tr_alert_inbox_send_group
     * 
     * @param require
     * @param trAlertInboxs
     * @param diffTo
     * @param loginUserSid
     * @param execDate
     * @return
     */
    private String processForwardDepGroup(
            Require require,
            List<TrAlertInbox> trAlertInboxs,
            ItemsCollectionDiffTo<Integer> diffTo,
            Integer loginUserSid,
            Date execDate) {

        // ================================
        //
        // ================================
        List<Integer> forwardDepSidsByLoginUser = Lists.newArrayList();
        // 收集已存在的轉寄單位中, 是由自己轉寄的
        for (TrAlertInbox trAlertInbox : trAlertInboxs) {
            // 排除不是自己轉寄的
            if (!WkCommonUtils.compareByStr(loginUserSid, trAlertInbox.getCreatedUser())) {
                continue;
            }
            // 排除已存在, 但是本次要被移除的
            if (diffTo.getReduceItems().contains(trAlertInbox.getReceiveDep())) {
                continue;
            }
            // 加入清單
            forwardDepSidsByLoginUser.add(trAlertInbox.getReceiveDep());
        }

        // 收集新增的
        forwardDepSidsByLoginUser.addAll(diffTo.getPlusItems());

        // ================================
        // 取得 tr_alert_inbox_send_group
        // ================================
        List<TrAlertInboxSendGroup> trAlertInboxSendGroups = trAlertInboxSendGroupService
                .findByRequireNoAndForwardTypeAndcreatedUser(
                        require.getRequireNo(),
                        ForwardType.FORWARD_DEPT,
                        loginUserSid);

        TrAlertInboxSendGroup trAlertInboxSendGroup = null;

        if (WkStringUtils.notEmpty(trAlertInboxSendGroups)) {
            // 同一個user 應該僅存在一筆
            trAlertInboxSendGroup = trAlertInboxSendGroups.get(0);
        } else {
            // 不存在時, 建立一筆新的
            trAlertInboxSendGroup = new TrAlertInboxSendGroup();
            trAlertInboxSendGroup.setMessage("");
            trAlertInboxSendGroup.setMessageCss("");
            trAlertInboxSendGroup.setRequireNo(require.getRequireNo());
            trAlertInboxSendGroup.setRequireSid(require.getSid());
            trAlertInboxSendGroup.setSendTo(WkOrgUtils.findNameBySid(forwardDepSidsByLoginUser, ","));
            trAlertInboxSendGroup.setForwardType(ForwardType.FORWARD_DEPT);
            trAlertInboxSendGroup.setStatus(Activation.ACTIVE);
            trAlertInboxSendGroup.setCreatedUser(loginUserSid);
            trAlertInboxSendGroup.setCreatedDate(new Date());
        }

        trAlertInboxSendGroup.setUpdatedUser(loginUserSid);
        trAlertInboxSendGroup.setUpdatedDate(execDate);

        // ================================
        // 異動
        // ================================
        if (WkStringUtils.isEmpty(forwardDepSidsByLoginUser)
                && trAlertInboxSendGroup.getSid() != null) {

            // 沒有任何一筆轉寄單位時, 需移除
            this.trAlertInboxSendGroupService.delete(trAlertInboxSendGroup);

        } else {
            // 更新資訊
            this.trAlertInboxSendGroupService.save(trAlertInboxSendGroup);
        }

        // ================================
        // return sid
        // ================================
        return trAlertInboxSendGroup.getSid();

    }

    // ========================================================================
    // 方法:轉寄部門
    // ========================================================================
    /**
     * 儲存轉寄人員
     * 
     * @param requireNo           需求單號
     * @param afterForwardDepSids 異動後的轉寄部門
     * @param execUserSid         執行者
     * @throws UserMessageException 檢核失敗時拋出
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveForwardUser(
            String requireNo,
            List<Integer> forwardUserSids,
            String messageCss,
            Integer execUserSid) throws UserMessageException {

        this.alertInboxService.createInboxMember(
                requireNo,
                execUserSid,
                forwardUserSids,
                messageCss);
    }
}
