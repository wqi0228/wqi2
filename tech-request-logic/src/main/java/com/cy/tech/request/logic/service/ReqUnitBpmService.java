/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.helper.ReqCreateHelper;
import com.cy.tech.request.logic.service.helper.RequireFlowHelper;
import com.cy.tech.request.logic.utils.ProcessLog;
import com.cy.tech.request.repository.require.ReqModifyRepo;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.ReqCateType;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireUnitSignInfo;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.base.Preconditions;

import lombok.extern.slf4j.Slf4j;

/**
 * 需求單位流程服務
 *
 * @author shaun
 */
@Slf4j
@Component
public class ReqUnitBpmService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 173274052792221273L;
    @Autowired
    private RequireService requireService;
    @Autowired
    private ReqCreateHelper reqCreateHelper;
    @Autowired
    private BpmService bpmService;
    @Autowired
    private RequireTraceService traceService;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private ReqModifyRepo reqModifyDao;
    @Autowired
    private RequireFlowHelper requireFlowHelper;
    @Autowired
    private transient RequireReadRecordHelper requireReadRecordHelper;

    /**
     * 設定簽核資訊經常性異動內容
     *
     * @param rmsi
     * @throws ProcessRestException
     */
    public void setupReqUnitSignInfo(RequireUnitSignInfo rmsi) throws ProcessRestException {
        List<ProcessTaskBase> tasks = bpmService.findSimulationById(rmsi.getBpmInstanceId());
        rmsi.setDefaultSignedName("");
        rmsi.getCanSignedIdsTo().setValue(new ArrayList<>());
        if (!tasks.isEmpty()) {
            rmsi.setDefaultSignedName(bpmService.findTaskDefaultUserName(tasks.get(tasks.size() - 1)));
            rmsi.getCanSignedIdsTo().setValue(bpmService.findTaskCanSignedUserIds(rmsi.getBpmInstanceId()));
        }
    }

    public boolean showSignBtn(Require req, User login) {
        if (!this.checkInfoStatus(req,
                RequireStatusType.NEW_INSTANCE,
                RequireStatusType.ROLL_BACK_NOTIFY)) {
            return false;
        }
        if (isNullFlowStatus(req, login)) {
            log.error("showSignBtn.isNullFlowStatus -> " + "login " + login.getId());
            return false;
        }
        RequireUnitSignInfo signInfo = req.getReqUnitSign();
        if (signInfo == null) {
            log.error("有需求單位流程但簽核流程資料為空值...!!單號:" + req.getRequireNo() + " 執行人:" + login.getId());
            return false;
        }
        return signInfo.getCanSignedIdsTo().getValue().contains(login.getId());
    }

    private boolean isNullFlowStatus(Require req, User login) {
        RequireUnitSignInfo signInfo = req.getReqUnitSign();
        if (signInfo == null) {
            log.error("有需求單位流程但簽核流程資料為空值！！ 單號:" + req.getRequireNo() + " 執行人:" + login.getId());
            return true;
        }
        return false;
    }

    private boolean checkInfoStatus(Require req, RequireStatusType... comparisons) {
        if (!req.getHasReqUnitSign()) {
            return false;
        }
        for (RequireStatusType each : comparisons) {
            if (req.getRequireStatus().equals(each)) {
                return true;
            }
        }
        return false;
    }

    public boolean showRollBackBtn(Require req, User login) {
        if (!this.checkInfoStatus(req,
                RequireStatusType.NEW_INSTANCE, RequireStatusType.ROLL_BACK_NOTIFY)) {
            return false;
        }
        //
        if (req.getReqUnitSign() == null || req.getReqUnitSign().getBpmInstanceId() == null) {
            return false;
        }
        boolean canShow = this.showSignBtn(req, login);
        String bpmId = req.getReqUnitSign().getBpmInstanceId();
        List<ProcessTaskBase> bpmInfos = findFlowChartByInstanceId(bpmId, login.getId());
        return canShow && bpmInfos.size() > 1;
    }

    public boolean showRecoveryBtn(Require req, User login) {
        if (!this.checkInfoStatus(
                req,
                RequireStatusType.NEW_INSTANCE, RequireStatusType.ROLL_BACK_NOTIFY)) {
            return false;
        }

        // 為待檢查確認時，若有任何一個項目已檢查，則不可退
        // 目前沒有做單據狀態復原流程，故此段先 mark
        // if (RequireStatusType.WAIT_CHECK.equals(req.getRequireStatus())) {
        // if (this.requireCheckItemService.hasChecked(req.getSid())) {
        // return false;
        // }
        // }

        if (this.isNullFlowStatus(req, login)) {
            return false;
        }

        RequireUnitSignInfo signInfo = req.getReqUnitSign();
        List<ProcessTaskBase> tasks = findFlowChartByInstanceId(signInfo.getBpmInstanceId(), login.getId());
        InstanceStatus iStatus = signInfo.getInstanceStatus();
        // 任務模擬圖需超過兩位才能進行復原，結案也無法復原
        if (tasks.size() <= 1 || iStatus.equals(InstanceStatus.CLOSED)) {
            return false;
        }
        // 如果為核准..
        int recoverySub = iStatus.equals(InstanceStatus.APPROVED) ? 1 : 2;
        // 前一個簽核者為當前執行者才可復原
        ProcessTaskBase rTask = tasks.get(tasks.size() - recoverySub);
        if (rTask instanceof ProcessTaskHistory) {
            if (login.getId().equals(((ProcessTaskHistory) rTask).getExecutorID())) {
                return true;
            }
        }
        return false;
    }

    public boolean showInvaildBtn(Require req, User login) {
        User reqCreateUser = WkUserCache.getInstance().findBySid(req.getCreatedUser().getSid());
        String createUserId = reqCreateUser.getId();
        String loginId = login.getId();
        // 是否為申請人
        Boolean sameCreatedUser = createUserId.equals(loginId);
        // 是否有流程
        Boolean hasRequireFlow = req.getHasReqUnitSign();
        // 需求製作進度狀態
        RequireStatusType rst = req.getRequireStatus();
        // 是否為需求單位主管
        boolean isReqManger = orgService.findOrgManagers(orgService.findBySid(req.getCreateDep().getSid())).contains(login);
        // 新建檔
        if (rst.equals(RequireStatusType.NEW_INSTANCE)) {
            // 無論有無流程皆可作廢
            return sameCreatedUser || isReqManger || includeProxyPermission(req, login);
        }
        // 待檢查確認 !@# 待邦妮確認是否
        if (rst.equals(RequireStatusType.WAIT_CHECK) && hasRequireFlow) {
            // 有BPM流程時才有待檢查確認狀態, 且一律不準作廢 (20180713 aken
            return false;
        }
        // 退件通知
        if (rst.equals(RequireStatusType.ROLL_BACK_NOTIFY) && hasRequireFlow) {
            // 僅被GM退件才有 退件通知狀態 (20180713 aken
            return isReqManger || includeProxyPermission(req, login);
        }

        return false;
    }

    /**
     * 需求單位簽名
     * 
     * @param req
     * @param execUser
     * @param comment
     * @throws ProcessRestException
     * @throws UserMessageException
     * @throws SystemOperationException
     */
    @Transactional(rollbackFor = Exception.class)
    public void doSign(Require req, User execUser, String comment)
            throws ProcessRestException, UserMessageException, SystemOperationException {

        ProcessLog processLog = new ProcessLog(req.getRequireNo(), "", "BPM 需求單簽名 userid:" + execUser.getId());

        // 檢核是否有簽名鈕權限
        if (!this.showSignBtn(requireService.findByReqObj(req), execUser)) {
            log.debug("無簽核權限!");
            throw new UserMessageException("簽核權限檢核失敗, 請重新整理畫面!", InfomationLevel.WARN);
        }

        // 檢核是否為當前流程可簽名人員
        bpmService.checkCanDoExceptRecovery(execUser, req.getReqUnitSign().getBpmInstanceId());
        // 執行需求單位簽名
        bpmService.doReqUnitSign(req, execUser, comment);

        log.info(processLog.prepareCompleteLog());

        // 更簽核資訊 (取回水管圖)
        this.updateSignInfo(req, execUser.getId());

        // 層簽完畢(核准)後續流程
        this.requireFlowHelper.processForDoSign(
                req.getSid(),
                req.getRequireNo(),
                execUser,
                new Date());
    }

    private void updateSignInfo(Require req, String loginUserId) throws ProcessRestException {
        this.setupReqUnitSignInfo(req.getReqUnitSign());
        String bpmId = req.getReqUnitSign().getBpmInstanceId();
        List<ProcessTaskBase> tasks = findFlowChartByInstanceId(bpmId, loginUserId);
        InstanceStatus is = bpmService.createInstanceStatus(req.getReqUnitSign().getBpmInstanceId(), tasks);
        req.getReqUnitSign().setInstanceStatus(is);
        bpmService.updateReqUnitSignInfo(req.getReqUnitSign());
    }

    public void updateSignInfo(Require req, InstanceStatus status) throws ProcessRestException {
        this.setupReqUnitSignInfo(req.getReqUnitSign());
        req.getReqUnitSign().setInstanceStatus(status);
        bpmService.updateReqUnitSignInfo(req.getReqUnitSign());
    }

    @Transactional(rollbackFor = Exception.class)
    public void doRecovery(Require req, User executor) throws UserMessageException {

        if (!this.showRecoveryBtn(requireService.findByReqObj(req), executor)) {
            throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.INFO);
        }

        String bpmId = req.getReqUnitSign().getBpmInstanceId();
        try {
            bpmService.checkCanRecovery(executor, bpmId);
            List<ProcessTaskBase> tasks = findFlowChartByInstanceId(bpmId, executor.getId());
            int recoverySub = req.getReqUnitSign().getInstanceStatus().equals(InstanceStatus.APPROVED) ? 1 : 2;
            bpmService.doRecovery(executor, (ProcessTaskHistory) tasks.get(tasks.size() - recoverySub));
            this.updateSignInfo(req, executor.getId());
            log.info("{} BPM需求單復原:{}", executor.getId(), req.getRequireNo());
        } catch (ProcessRestException e) {
            log.error("執行需求單復原失敗!", e);
            throw new UserMessageException(WkMessage.PROCESS_FAILED, InfomationLevel.ERROR);
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public void doRollBack(Require req, User executor, ProcessTaskHistory rollbackTask, String rollbackComment) throws ProcessRestException {
        // 待檢查確認時退件，略過檢查
        if (!RequireStatusType.WAIT_CHECK.equals(req.getRequireStatus())) {
            Preconditions.checkArgument(this.showRollBackBtn(requireService.findByReqObj(req), executor), "執行退回失敗！！");
            bpmService.checkCanDoExceptRecovery(executor, req.getReqUnitSign().getBpmInstanceId());
        }
        bpmService.doRollBack(executor, rollbackTask, rollbackComment);
        this.updateSignInfo(req, executor.getId());
        log.info("{} BPM需求單退回:{}", executor.getId(), req.getRequireNo());
    }

    @Transactional(rollbackFor = Exception.class)
    public void doInvaild(Require req, User executor) throws ProcessRestException {
        Preconditions.checkArgument(this.showInvaildBtn(requireService.findByReqObj(req), executor), "執行作廢失敗！！");
        if (!req.getHasReqUnitSign()) {
            this.executeReqInvaild(req, executor);
            return;
        }
        bpmService.checkCanDoInvaild(executor, req.getReqUnitSign().getBpmInstanceId());
        bpmService.invaildProcess(executor, req.getReqUnitSign().getBpmInstanceId());
        this.setupReqUnitSignInfo(req.getReqUnitSign());
        this.handlerInvaildSave(req.getReqUnitSign(), executor);
        log.info("{} BPM需求單作廢:{}", executor.getId(), req.getRequireNo());
    }

    private void handlerInvaildSave(RequireUnitSignInfo signInfo, User executor) {
        InstanceStatus is = InstanceStatus.INVALID;
        signInfo.setInstanceStatus(is);
        signInfo.setCanSignedIdsTo(new JsonStringListTo());// 清空待簽資訊
        signInfo.setDefaultSignedName("");// 清空待簽資訊
        bpmService.updateReqUnitSignInfo(signInfo);
        this.executeReqInvaild(signInfo.getRequire(), executor);
    }

    /**
     * 執行需求單作廢
     *
     * @param require
     * @param executor
     */
    @Transactional(rollbackFor = Exception.class)
    public void executeReqInvaild(Require require, User executor) {
        // ====================================
        // 寫追蹤
        // ====================================
        traceService.createRequireInvaildTrace(require.getSid(), executor);

        // ====================================
        // 更新主檔
        // ====================================
        require.setStatus(Activation.INACTIVE);
        require.setRequireStatus(RequireStatusType.INVALID);
        require.setReadReason(ReqToBeReadType.INVALID);
        require.setHasTrace(Boolean.TRUE);
        require.setInvalidCode(Boolean.TRUE);
        reqModifyDao.updateFlowInvaildByReqUnit(
                require,
                Boolean.TRUE,
                require.getStatus(),
                require.getRequireStatus(),
                require.getReadReason(),
                require.getHasTrace(),
                executor, new Date());
        
        // ====================================
        // 將『已經讀取』過單據的人，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.REQUIRE,
                require.getSid(),
                executor.getSid());
    }

    /**
     * 退件通知 - BPM 處理事項
     *
     * @param req
     * @throws ProcessRestException
     */
    public void doExecuteRejectNotity(Require req, String loginUserId, int taskHistoryIndex, String rollbackComment) throws ProcessRestException {
        if (req.getHasReqUnitSign()) {
            RequireUnitSignInfo rmsi = req.getReqUnitSign();
            List<ProcessTaskBase> tasks = findFlowChartByInstanceId(rmsi.getBpmInstanceId(), loginUserId);
            ProcessTaskHistory taskHistory = (ProcessTaskHistory) tasks.get(taskHistoryIndex);
            User executor = WkUserCache.getInstance().findById(loginUserId);
            this.doRollBack(req, executor, taskHistory, rollbackComment);
            // 單據狀態:再議
            rmsi.setInstanceStatus(InstanceStatus.RECONSIDERATION);
            bpmService.updateReqUnitSignInfo(rmsi);
        }
    }

    /**
     * 是否含有代理人權限
     * 
     * @return
     */
    public boolean includeProxyPermission(Require require, User loginUser) {
        if (require != null && !isNullFlowStatus(require, loginUser)) {
            return require.getReqUnitSign().getCanSignedIdsTo().getValue().contains(loginUser.getId());
        }
        return false;
    }

    /**
     * 取得水管圖 by BPM InstanceId
     * 
     * @param bpmId
     * @param loginUserId
     * @return
     */
    public List<ProcessTaskBase> findFlowChartByInstanceId(String bpmId, String loginUserId) {
        return bpmService.findFlowChartByInstanceId(bpmId, loginUserId);
    }

    /**
     * 二次處理
     *
     * @param require
     * @param executor
     * @throws UserMessageException
     * @throws SystemDevelopException
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateFlowApproveByReqUnitForInternal(Require require, User executor) throws UserMessageException, SystemDevelopException {

        // 防呆:僅處理內部需求類別
        if (!ReqCateType.INTERNAL.equals(require.getMapping().getBig().getReqCateType())) {
            return;
        }

        // 建立自動分派及判斷是否產生ONPG (內部需求用)
        reqCreateHelper.createInternalAction(
                require.getSid(),
                require.getRequireNo(),
                WkUserCache.getInstance().findBySid(require.getCreatedUser().getSid()),
                new Date());

        // 更新主檔資訊
        reqModifyDao.updateFlowApproveByReqUnitForInternal(
                require,
                require.getHasTrace(),
                require.getHasOnpg(),
                require.getHasAssign(),
                require.getRequireStatus(),
                require.getRequireEstablishDate(),
                require.getCategoryConfirmCode(),
                executor, new Date());

    }
}
