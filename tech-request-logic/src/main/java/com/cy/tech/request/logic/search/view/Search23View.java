/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 草稿查詢 vo (search23.xhtml)
 *
 * @author shaun
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Search23View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -9125881706345478508L;
    /** 草稿日期 */
    private Date draftDate;
    /** 需求類別 */
    private String bigName;
    /** 中類 */
    private String middleName;
    /** 小類 */
    private String smallName;
    /** 需求單位 */
    private Integer createDep;
    /** 需求人員 */
    private Integer createdUser;
    /** 本地端連結網址 */
    private String localUrlLink;
}
