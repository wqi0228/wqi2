package com.cy.tech.request.logic.service.send.test;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.NotificationService;
import com.cy.tech.request.logic.service.RequireReadRecordHelper;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.utils.DateUtils;
import com.cy.tech.request.repository.result.BatchAdjustDateVO;
import com.cy.tech.request.repository.worktest.WorkTestInfoRepo;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.NotificationEventType;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.vo.worktest.enums.WorkTestInfoHistoryBehavior;
import com.cy.tech.request.vo.worktest.enums.WorkTestInfoStatus;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author aken_kao
 */
@Slf4j
@Service
public class BatchAdjustDateService {
    @Autowired
    private WorkTestInfoRepo workTestInfoRepo;
    @Autowired
    private SearchService searchHelper;
    @Autowired
    private SendTestShowService sendTestShowService;
    @Autowired
    private SendTestService sendTestService;
    @Autowired
    private SendTestHistoryService sendTestHistoryService;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private transient RequireReadRecordHelper requireReadRecordHelper;

    /**
     * 只顯示可修改預計上線日 for 一般使用者
     * 
     * @param resultList
     * @param loginUser
     * @return
     */
    public List<BatchAdjustDateVO> convert(List<BatchAdjustDateVO> resultList, User loginUser) {
        Iterator<BatchAdjustDateVO> it = resultList.iterator();
        while (it.hasNext()) {
            BatchAdjustDateVO vo = it.next();
            if (skipVO(vo, loginUser)) {
                it.remove();
                continue;
            }
            vo.setRequireTheme(searchHelper.combineFromJsonStr(vo.getRequireTheme()));
            vo.setWtCreateUser(WkUserCache.getInstance().findBySid(vo.getWtCreateUserSid()).getName());
            vo.setWtCreateDept(WkOrgCache.getInstance().findNameBySid(vo.getWtCreateDeptSid()));
            vo.setWtStatus(WorkTestStatus.valueOf(vo.getWtStatus()).getValue());
            vo.setWtQaScheduleStatus(WorkTestInfoStatus.valueOf(vo.getWtQaScheduleStatus()).getValue());

        }
        return resultList;
    }

    /**
     * 只顯示可修改預計上線日 for 一般使用者 過濾不加入排程, 取消測試
     * 
     * @param resultList
     * @param loginUser
     * @param isQaUser
     * @return
     */
    public List<BatchAdjustDateVO> convertForQAManager(List<BatchAdjustDateVO> resultList) {
        Iterator<BatchAdjustDateVO> it = resultList.iterator();
        while (it.hasNext()) {
            BatchAdjustDateVO vo = it.next();
            vo.setRequireTheme(searchHelper.combineFromJsonStr(vo.getRequireTheme()));
            vo.setWtCreateUser(WkUserCache.getInstance().findBySid(vo.getWtCreateUserSid()).getName());
            vo.setWtCreateDept(WkOrgCache.getInstance().findNameBySid(vo.getWtCreateDeptSid()));
            vo.setWtStatus(WorkTestStatus.valueOf(vo.getWtStatus()).getValue());
            vo.setWtQaScheduleStatus(WorkTestInfoStatus.valueOf(vo.getWtQaScheduleStatus()).getValue());

        }
        return resultList;
    }

    /**
     * 是否符合權限
     * 
     * @param vo
     * @param loginUser
     * @return
     */
    private boolean skipVO(BatchAdjustDateVO vo, User loginUser) {
        WorkTestStatus workTestStatus = WorkTestStatus.valueOf(vo.getWtStatus());
        WorkTestInfoStatus qaAuditStatus = WorkTestInfoStatus.valueOf(vo.getQaAuditStatus());
        WorkTestInfoStatus qaScheduleStatus = WorkTestInfoStatus.valueOf(vo.getWtQaScheduleStatus());
        WorkTestInfoStatus commitStatus = WorkTestInfoStatus.valueOf(vo.getCommitStatus());
        User wtCreateUser = WkUserCache.getInstance().findBySid(vo.getWtCreateUserSid());
        Org wtCreateOrg = WkOrgCache.getInstance().findBySid(vo.getWtCreateDeptSid());

        return !sendTestShowService.modifyStatusByOnlineDate(workTestStatus, qaAuditStatus, qaScheduleStatus, commitStatus, wtCreateOrg, wtCreateUser,
                loginUser);
    }

    @Transactional(rollbackFor = Exception.class)
    public void batchAdjustDate(
            List<String> workTestSids,
            WorkTestInfoHistoryBehavior behavior,
            Date adjustDate,
            User loginUser) {
        List<WorkTestInfo> entities = workTestInfoRepo.findAll(workTestSids);
        WaitReadReasonType readReason = WaitReadReasonType.ADJUST_DATE;
        String newDate = adjustDate == null ? null : DateUtils.YYYY_MM_DD.print(adjustDate.getTime());

        outLoop: for (int i = 0; i < entities.size(); i++) {
            WorkTestInfo entity = entities.get(i);

            String oldDate = null;
            String reason = null;

            switch (behavior) {
            case ADJUST_ONLINE_DATE:
                if (entity.getExpectOnlineDate() != null && newDate != null) {
                    oldDate = DateUtils.YYYY_MM_DD.print(entity.getExpectOnlineDate().getTime());
                    reason = String.format("預計上線日由%s調整為%s", oldDate, newDate);
                } else if (entity.getExpectOnlineDate() != null && newDate == null) {
                    reason = "預計上線日調整為空值";
                } else if (entity.getExpectOnlineDate() == null && newDate == null) {
                    continue outLoop;
                } else {
                    reason = String.format("預計上線日調整為%s", newDate);
                }
                readReason = WaitReadReasonType.ADJUST_ONLINE_DATE;
                entity.setExpectOnlineDate(adjustDate);
                break;
            case ADJUST_TEST_DATE:
                if (entity.getTestDate() != null) {
                    oldDate = DateUtils.YYYY_MM_DD.print(entity.getTestDate().getTime());
                    reason = String.format("送測日由%s調整為%s", oldDate, newDate);
                } else {
                    reason = String.format("送測日調整為%s", newDate);
                }
                readReason = WaitReadReasonType.ADJUST_TEST_DATE;
                entity.setTestDate(adjustDate);
                break;
            case MODIFY_ESTABLISH_DATE:
                if (entity.getEstablishDate() != null) {
                    oldDate = DateUtils.YYYY_MM_DD.print(entity.getEstablishDate().getTime());
                    reason = String.format("預計完成日由%s調整為%s", oldDate, newDate);
                } else {
                    reason = String.format("預計完成日調整為%s", newDate);
                }
                readReason = WaitReadReasonType.TEST_UPDATE_ESTABLISHDATE;
                entity.setEstablishDate(adjustDate);
                break;
            case ADJUST_SCHEDULE_FINISH_DATE:
                if (entity.getScheduleFinishDate() != null && newDate != null) {
                    oldDate = DateUtils.YYYY_MM_DD.print(entity.getScheduleFinishDate().getTime());
                    reason = String.format("排定完成日由%s調整為%s", oldDate, newDate);
                } else if (entity.getScheduleFinishDate() != null && newDate == null) {
                    reason = "排定完成日調整為空值";
                } else if (entity.getScheduleFinishDate() == null && newDate == null) {
                    continue outLoop;
                } else {
                    reason = String.format("排定完成日調整為%s", newDate);
                }
                readReason = WaitReadReasonType.ADJUST_SCHEDULE_FINISH_DATE;
                entity.setScheduleFinishDate(adjustDate);
                break;
            default:
                break;
            }
            // 新增送測異動紀錄
            sendTestHistoryService.save(entity, behavior, reason, loginUser);

            entity.setReadReason(readReason);
            Date currentDate = new Date();
            entity.setReadUpdateDate(currentDate);
            entity.setUpdatedDate(currentDate);
            entity.setUpdatedUser(loginUser);

            // ====================================
            // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
            // ====================================
            this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                    FormType.WORKTESTSIGNINFO,
                    entity.getSid(),
                    loginUser.getSid());
        }
        workTestInfoRepo.save(entities);
        log.info("{} 批次{}, 共 {} 筆", loginUser.getId(), behavior.getValue(), workTestSids.size());

        if (WorkTestInfoHistoryBehavior.ADJUST_ONLINE_DATE.equals(behavior)) {
            notificationService.notifyQaUsersByStEvent(entities, NotificationEventType.BATCH_UP_OL_DATE, loginUser.getSid());
        } else if (WorkTestInfoHistoryBehavior.MODIFY_ESTABLISH_DATE.equals(behavior)) {
            notificationService.notifyQaUsersByStEvent(entities, NotificationEventType.BATCH_UP_ES_DATE, loginUser.getSid());
        }
    }

    /**
     * 過濾日期可修改時機
     * 
     * @param requireNo
     * @param behavior
     * @param loginUser
     * @return
     */
    public List<BatchAdjustDateVO> findWorkTestInfoByBehavior(String requireNo, WorkTestInfoHistoryBehavior behavior,
            User loginUser, boolean isQAManager) {
        List<WorkTestInfo> resultList = workTestInfoRepo.findByRequireNo(requireNo);

        return convertByWorkTestInfo(resultList, behavior, loginUser, isQAManager);
    }

    /**
     * 將WorkTestInfo 轉成 BatchAdjustDateVO 過濾日期可修改時機
     * 
     * @param entities
     * @param loginUser
     * @return
     */
    private List<BatchAdjustDateVO> convertByWorkTestInfo(List<WorkTestInfo> entities,
            WorkTestInfoHistoryBehavior behavior, User loginUser, boolean isQAManager) {
        List<BatchAdjustDateVO> resultList = Lists.newArrayList();
        Iterator<WorkTestInfo> it = entities.iterator();
        while (it.hasNext()) {
            WorkTestInfo entity = it.next();
            if (isQAManager) {
                if (!WorkTestInfoStatus.JOIN_SCHEDULE.equals(entity.getQaScheduleStatus()) ||
                        WorkTestStatus.CANCEL_TEST.equals(entity.getTestinfoStatus())) {
                    continue;
                }
            } else {
                if (!sendTestService.isContainsQADepts(entity) || !sendTestShowService.modifyStatusByOnlineDate(entity, loginUser)) {
                    continue;
                }
            }

            BatchAdjustDateVO vo = new BatchAdjustDateVO();
            vo.setTestInfoSid(entity.getSid());
            vo.setWtTheme(entity.getTheme());
            vo.setRequireNo(entity.getSourceNo());
            vo.setTestInfoNo(entity.getTestinfoNo());
            vo.setTestDate(entity.getTestDate());
            vo.setWtCreateUser(entity.getCreatedUser().getName());
            vo.setWtCreateDept(WkOrgUtils.getOrgName(entity.getCreateDep()));
            vo.setWtStatus(entity.getTestinfoStatus().getValue());
            vo.setWtQaScheduleStatus(entity.getQaScheduleStatus().getValue());
            vo.setWtExpectOnlineDate(entity.getExpectOnlineDate());
            vo.setWtEstablishDate(entity.getEstablishDate());
            vo.setWtScheduleFinishDate(entity.getScheduleFinishDate());
            resultList.add(vo);

        }
        return resultList;
    }
}
