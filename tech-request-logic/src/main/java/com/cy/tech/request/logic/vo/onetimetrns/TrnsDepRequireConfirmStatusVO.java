/**
 * 
 */
package com.cy.tech.request.logic.vo.onetimetrns;

import java.io.Serializable;

import com.cy.tech.request.vo.enums.ReqConfirmDepCompleteType;

import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 *
 */
@Getter
@Setter
public class TrnsDepRequireConfirmStatusVO implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 6954986228178876455L;

    /**
	 * 需求單號
	 */
	private String requireNo;

	/**
	 * 匯入單位名稱
	 */
	private String importDepName;

	/**
	 * 單位 sid
	 */
	private Integer depSid;

	/**
	 * 單位檢查結果
	 */
	private String checkDepDesc;

	/**
	 * 匯入單位名稱
	 */
	private String importStatus;

	/**
	 * 轉換標的狀態
	 */
	private ReqConfirmDepCompleteType trnsToCompleteType;

	/**
	 * 執行結果
	 */
	private String processResut;

}
