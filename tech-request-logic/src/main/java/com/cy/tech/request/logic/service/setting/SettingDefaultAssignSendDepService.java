package com.cy.tech.request.logic.service.setting;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.tech.request.repository.setting.SettingDefaultAssignSendDepRepository;
import com.cy.tech.request.vo.constants.CacheConstants;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.setting.asdep.SettingDefaultAssignSendDep;
import com.cy.tech.request.vo.setting.asdep.SettingDefaultAssignSendDepVO;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * 設定:檢查確認預設分派/通知單位
 * 
 * @author allen1214_wu
 */
@Slf4j
@Service
public class SettingDefaultAssignSendDepService {

    // ========================================================================
    // 服務區
    // ========================================================================

    @Autowired
    transient private SettingDefaultAssignSendDepRepository settingDefaultAssignSendDepRepository;
    @Autowired
    transient private SettingDefaultAssignSendDepHistoryService settingDefaultAssignSendDepHistoryService;

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * 查詢全部設定資料
     * 
     * @return 全部設定資料
     */
    public List<SettingDefaultAssignSendDepVO> findAll() {

        // 查詢全部
        List<SettingDefaultAssignSendDep> entitis = this.settingDefaultAssignSendDepRepository.findAll();
        // 過濾停用
        entitis = entitis.stream().filter(entity -> entity.isActive()).collect(Collectors.toList());

        // 轉 VO
        List<SettingDefaultAssignSendDepVO> results = entitis.stream()
                .map(entity -> new SettingDefaultAssignSendDepVO(entity))
                .collect(Collectors.toList());

        return results;
    }

    /**
     * 依據傳入小類準備設定資料
     * 
     * @param smallCategorySids 小類sid
     * @return Map[資料key, SettingDefaultAssignSendDepVO]
     */
    public Map<String, SettingDefaultAssignSendDepVO> prepareSettingInfoMap(
            List<String> smallCategorySids) {

        if (WkStringUtils.isEmpty(smallCategorySids)) {
            log.warn("未傳入小類項目！");
            return Maps.newHashMap();
        }

        // ====================================
        // 查詢
        // ====================================
        // 查詢
        List<SettingDefaultAssignSendDep> entitis = this.settingDefaultAssignSendDepRepository.findBySmallCategorySidIn(
                smallCategorySids);

        // 以『小類、檢查項目』做 index
        Map<String, SettingDefaultAssignSendDep> entitisMapBySmallCategorySidAndCheckItemType = Maps.newHashMap();

        for (SettingDefaultAssignSendDep entity : entitis) {

            // 組 key (『小類、檢查項目』)
            String key = this.prepareDataKey(
                    entity.getSmallCategorySid(),
                    entity.getCheckItemType(),
                    entity.getAssignSendType());

            // 放入
            entitisMapBySmallCategorySidAndCheckItemType.put(key, entity);
        }

        // ====================================
        // 拼裝 (不存在資料也需要生成一筆)
        // ====================================
        Map<String, SettingDefaultAssignSendDepVO> settingDepMapByDataKey = Maps.newHashMap();

        // 依據『小類』逐筆處理
        for (String smallCategorySid : smallCategorySids) {
            // 依據『檢查項目』逐筆處理
            for (RequireCheckItemType checkItemType : RequireCheckItemType.values()) {

                for (AssignSendType assignSendType : AssignSendType.values()) {

                    // 組 key (『小類、檢查項目』)
                    String key = this.prepareDataKey(
                            smallCategorySid,
                            checkItemType,
                            assignSendType);

                    // 取出對應資料
                    SettingDefaultAssignSendDep entity = entitisMapBySmallCategorySidAndCheckItemType.get(key);

                    // 準備VO
                    SettingDefaultAssignSendDepVO vo = null;
                    // 資料不存在產生一筆新的
                    if (entity == null) {
                        vo = new SettingDefaultAssignSendDepVO(smallCategorySid, checkItemType, assignSendType);
                    }
                    // 已存在時轉VO
                    else {
                        vo = new SettingDefaultAssignSendDepVO(entity);
                    }

                    // 放入容器map<key, vo>
                    settingDepMapByDataKey.put(key, vo);
                }
            }
        }
        return settingDepMapByDataKey;
    }

    /**
     * 由傳入的資料集中, 取得設定部門資訊
     * 
     * @param settingInfoMapByDataKey 資料集
     * @param smallCategorySid        小類 SID
     * @param checkItemType           檢查項目
     * @param assignSendType          分派/通知類別
     * @return 部門sid (list)
     */
    public List<Integer> findSettingDepSids(
            Map<String, SettingDefaultAssignSendDepVO> settingInfoMapByDataKey,
            String smallCategorySid,
            RequireCheckItemType checkItemType,
            AssignSendType assignSendType) {

        // 兜組 data key
        String dataKey = this.prepareDataKey(smallCategorySid, checkItemType, assignSendType);
        // 由 map 中取得對應資料
        SettingDefaultAssignSendDepVO vo = settingInfoMapByDataKey.get(dataKey);
        if (vo == null) {
            log.warn(""
                    + "傳入的 settingInfoMapByDataKey 查無資料, "
                    + "可能為開發時期錯誤 (誤用)! "
                    + "smallCategorySid:[{}], checkItemType:[{}], assignSendType:[{}]",
                    smallCategorySid,
                    checkItemType.name(),
                    assignSendType.name());
            return Lists.newArrayList();
        }

        return vo.getDepsInfo();
    }

    /**
     * 兜組資料 key
     * 
     * @param smallCategorySid 小類 SID
     * @param checkItemType    檢查項目
     * @param assignSendType   分派/通知類別
     * @return
     */
    public String prepareDataKey(
            String smallCategorySid,
            RequireCheckItemType checkItemType,
            AssignSendType assignSendType) {
        return smallCategorySid + "@" + checkItemType.name() + "@" + assignSendType.name();
    }

    /**
     * 
     * 查詢預設分派/通知單位
     * 
     * @param smallCategorySid 小類 SID
     * @param checkItemType    檢查項目
     * @param assignSendType   分派/通知類別
     * @return
     */
    public List<Integer> findDefaultAsDepsBySmallCategory(
            String smallCategorySid,
            RequireCheckItemType checkItemType,
            AssignSendType assignSendType) {

        // ====================================
        // 查詢
        // ====================================
        SettingDefaultAssignSendDep entity = this.settingDefaultAssignSendDepRepository.findBySmallCategorySidAndCheckItemTypeAndAssignSendType(
                smallCategorySid,
                checkItemType,
                assignSendType);

        if (entity == null || WkStringUtils.isEmpty(entity.getDepsInfo())) {
            return Lists.newArrayList();
        }

        // ====================================
        // 組結果
        // ====================================
        // 去重複
        List<Integer> depSids = entity.getDepsInfo().stream().distinct().collect(Collectors.toList());

        // 轉 integer + 排序
        return WkOrgUtils.sortByOrgTree(depSids);
    }

    /**
     * 查詢設定檔
     * 
     * @param smallCategorySid 小類 SID
     * @param checkItemType    檢查項目
     * @param assignSendType   分派/通知類別
     * @return
     */
    public SettingDefaultAssignSendDepVO findByUnqKey(
            String smallCategorySid,
            RequireCheckItemType checkItemType,
            AssignSendType assignSendType) {

        // ====================================
        // 查詢
        // ====================================
        SettingDefaultAssignSendDep entity = this.settingDefaultAssignSendDepRepository.findBySmallCategorySidAndCheckItemTypeAndAssignSendType(
                smallCategorySid, checkItemType, assignSendType);

        // ====================================
        // 回傳
        // ====================================
        if (entity == null) {
            return new SettingDefaultAssignSendDepVO(smallCategorySid, checkItemType, assignSendType);
        }
        return new SettingDefaultAssignSendDepVO(entity);
    }

    /**
     * 查詢設定的單位
     * 
     * @param smallCategorySid 小類 SID
     * @param checkItemType    檢查項目
     * @param assignSendType   分派/通知類別
     * @return
     */
    public List<Integer> findDepSidsByUnqKey(
            String smallCategorySid,
            RequireCheckItemType checkItemType,
            AssignSendType assignSendType) {

        // ====================================
        // 查詢
        // ====================================
        SettingDefaultAssignSendDepVO vo = this.findByUnqKey(smallCategorySid, checkItemType, assignSendType);

        // ====================================
        // 回傳設定單位
        // ====================================
        // 未設定
        if (vo == null || vo.getDepsInfo() == null) {
            return Lists.newArrayList();
        }

        // 去重複 (應該不會重複, 防止手動塞資料時有錯誤)
        return vo.getDepsInfo().stream().distinct().collect(Collectors.toList());
    }

    /**
     * 更新分派/通知預設單位
     * 
     * @param smallCategorySid 小類 sid
     * @param checkItemType    檢查項目
     * @param assignDepSids    預設分派單位
     * @param sendDepSids      預設通知單位
     * @param execUserSid      執行者 sid
     * @param execDate         執行日期
     * @return 是否有進行異動
     */
    @Transactional(rollbackFor = Exception.class)
    @Caching(evict = {
            @CacheEvict(value = CacheConstants.CACHE_TEMP_FIELD, allEntries = true),
            @CacheEvict(value = CacheConstants.CACHE_CATE_KEY_MAPPING, allEntries = true),
            @CacheEvict(value = CacheConstants.CACHE_ALL_CATEGORY_PICKER, allEntries = true),
            @CacheEvict(value = CacheConstants.CACHE_findActiveBigCategory, allEntries = true)
    })
    public boolean updateDefaultAsDeps(
            String smallCategorySid,
            RequireCheckItemType checkItemType,
            List<Integer> assignDepSids,
            List<Integer> sendDepSids,
            Integer execUserSid,
            Date execDate) {

        // ====================================
        // 異動預設分派單位
        // ====================================
        boolean isModifyAssignDep = this.updateDefaultDeps(
                smallCategorySid,
                checkItemType,
                AssignSendType.ASSIGN,
                assignDepSids,
                execUserSid,
                execDate);

        // ====================================
        // 異動預設通知單位
        // ====================================
        boolean isModifySendDep = this.updateDefaultDeps(
                smallCategorySid,
                checkItemType,
                AssignSendType.SEND,
                sendDepSids,
                execUserSid,
                execDate);

        // ====================================
        // 是否有任何異動
        // ====================================
        // 分派 / 通知單位有任一異動，即視為有異動
        return isModifyAssignDep || isModifySendDep;

    }

    /**
     * 更新預設單位
     * 
     * @param smallCategorySid 小類 sid
     * @param checkItemType    檢查項目
     * @param assignSendType   分派/通知
     * @param afterDepSids     異動後的預設單位
     * @param execUserSid      執行者 sid
     * @param execDate         執行日期
     * @return 是否有進行異動
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean updateDefaultDeps(
            String smallCategorySid,
            RequireCheckItemType checkItemType,
            AssignSendType assignSendType,
            List<Integer> afterDepSids,
            Integer execUserSid,
            Date execDate) {

        // ====================================
        // 查詢更新前的資料
        // ====================================
        // 查詢
        SettingDefaultAssignSendDep entity = this.settingDefaultAssignSendDepRepository.findBySmallCategorySidAndCheckItemTypeAndAssignSendType(
                smallCategorySid, checkItemType, assignSendType);

        // 不存在時，建立新容器
        if (entity == null) {
            entity = this.createEmptyEntity(smallCategorySid, checkItemType, assignSendType, execUserSid, execDate);
        }

        // 異動前單位清單
        List<Integer> beforeDepSids = entity.getDepsInfo();

        // 檢查資料是否有異動
        if (WkCommonUtils.compare(beforeDepSids, afterDepSids)) {
            // 資料相同無需異動
            return false;
        }

        // ====================================
        // 更新資料 (tr_setting_default_asdep)
        // ====================================
        // 異動者
        entity.setUpdatedUser(execUserSid);
        // 異動時間
        entity.setUpdatedDate(execDate);
        // 預設單位 sid
        entity.setDepsInfo(afterDepSids);
        // insert or update
        this.settingDefaultAssignSendDepRepository.save(entity);

        // ====================================
        // 異動記錄檔 (tr_setting_default_asdep_his)
        // ====================================
        this.settingDefaultAssignSendDepHistoryService.saveHistory(
                smallCategorySid,
                checkItemType,
                assignSendType,
                beforeDepSids,
                afterDepSids,
                execUserSid,
                execDate);

        return true;
    }

    /**
     * 建立空的 Entity
     * 
     * @param smallCategorySid 小類 sid
     * @param checkItemType    檢查項目
     * @param assignSendType   分派/通知
     * @param execUserSid      執行者 sid
     * @param execDate         執行日期
     * @return
     */
    public SettingDefaultAssignSendDep createEmptyEntity(
            String smallCategorySid,
            RequireCheckItemType checkItemType,
            AssignSendType assignSendType,
            Integer execUserSid,
            Date execDate) {

        SettingDefaultAssignSendDep entity = new SettingDefaultAssignSendDep();
        entity.setCreatedUser(execUserSid);
        entity.setCreatedDate(execDate);
        entity.setSmallCategorySid(smallCategorySid);
        entity.setCheckItemType(checkItemType);
        entity.setAssignSendType(assignSendType);

        return entity;
    }
}