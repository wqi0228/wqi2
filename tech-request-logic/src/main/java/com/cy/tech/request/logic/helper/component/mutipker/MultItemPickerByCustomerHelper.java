/**
 * 
 */
package com.cy.tech.request.logic.helper.component.mutipker;

import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.helper.component.ComponentHelper;
import com.cy.tech.request.logic.service.customer.ReqWorkCustomerHelper;
import com.cy.tech.request.logic.vo.SelectedItemPrepareInfoTo;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.customer.vo.WorkCustomer;
import com.cy.work.customer.vo.enums.EnableType;
import com.google.common.collect.Lists;

import lombok.NoArgsConstructor;

/**
 * @author allen1214_wu
 *
 */
@NoArgsConstructor
@Service
public class MultItemPickerByCustomerHelper implements InitializingBean, Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 3958558220920558959L;
    // ====================================
	// for InitializingBean
	// ====================================
	private static MultItemPickerByCustomerHelper instance;

	public static MultItemPickerByCustomerHelper getInstance() {
		return instance;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		MultItemPickerByCustomerHelper.instance = this;
	}

	// ========================================================================
	// 服務區
	// ========================================================================
	@Autowired
	private transient ReqWorkCustomerHelper workCustomerService;
	@Autowired
	private transient ComponentHelper componentHelper;

	// ========================================================================
	// 方法區
	// ========================================================================
	/**
	 * @return
	 */
	public List<WkItem> prepareAllItems() {

		// ====================================
		// 查詢所有廳主
		// ====================================
		List<WorkCustomer> customers = workCustomerService.findByEnable(EnableType.ENABLE);

		// ====================================
		// 轉換格式
		// ====================================
		List<WkItem> allItems = Lists.newArrayList();
		Long index = Long.parseLong("0");
		for (WorkCustomer workCustomer : customers) {

			WkItem item = new WkItem(
			        workCustomer.getSid() + "",
			        workCustomer.getName() + "@" + workCustomer.getLoginCode(),
			        true);
			
			//搜尋用名稱
			item.getItemNamesForSearch().add(workCustomer.getName());
			item.getItemNamesForSearch().add(workCustomer.getLoginCode());
			
			// 排序序號
			item.setItemSeq(index++);

			allItems.add(item);
		}

		return allItems;
	}

	/**
	 * 兜組顯示資訊
	 * 
	 * @param allCustomerItems
	 * @param blackList
	 * @return
	 */
	public String prepareShowInfo(
	        List<WkItem> allCustomerItems,
	        List<String> blackList) {

		// ====================================
		// 解析資料
		// ====================================
		SelectedItemPrepareInfoTo selectedItemPrepareInfoTo = componentHelper.prepareSelectedItemInfo(
		        allCustomerItems,
		        blackList);

		// ====================================
		// 強制訊息
		// ====================================
		if (WkStringUtils.notEmpty(selectedItemPrepareInfoTo.getForceMessage())) {
			return this.componentHelper.prepareForceMessageForSettingSysNotify(selectedItemPrepareInfoTo);
		}

		// ====================================
		// 組回傳字串
		// ====================================
		String infoStr = "";
		if (selectedItemPrepareInfoTo.isReverse()) {
			infoStr += "<span class='WS1-1-2b'>【不通知】</span>";
		}else {
			infoStr += "<span class='WS1-1-3b'>【通知】</span>";
		}

		infoStr += String.join("、", selectedItemPrepareInfoTo.getSelectedItemNames());

		return infoStr;

	}

	/**
	 * 兜組顯示資訊
	 * 
	 * @param allCustomerItems
	 * @param blackList
	 * @return
	 */
	public String prepareTooltip(
	        List<WkItem> allCustomerItems,
	        List<String> blackList) {

		// ====================================
		// 解析資料
		// ====================================
		SelectedItemPrepareInfoTo selectedItemPrepareInfoTo = componentHelper.prepareSelectedItemInfo(
		        allCustomerItems,
		        blackList);

		// ====================================
		// 強制訊息
		// ====================================
		if (WkStringUtils.notEmpty(selectedItemPrepareInfoTo.getForceMessage())) {
			return this.componentHelper.prepareForceMessageForSettingSysNotify(selectedItemPrepareInfoTo);
		}

		// ====================================
		// 組回傳字串
		// ====================================
		String infoStr = "";
		if (selectedItemPrepareInfoTo.isReverse()) {
			infoStr += "<span class='WS1-1-2b'>【不通知以下廳主】<span><hr/>";
		}else {
			infoStr += "<br/>";	
		}

		infoStr += "<table class='table table-striped'><tbody>";

		int colCount = 4;
		List<String> selectedItemNames = selectedItemPrepareInfoTo.getSelectedItemNames();
		//決定全部要顯示的列(row)數 : 總項目數 / 每列攔數
		int row = selectedItemNames.size() / colCount;
		//Integer 相除時，無條件捨去小數位，故計算餘數，若餘數>1時，增加一行
		if ((selectedItemNames.size() % colCount)>0) {
			row += 1;
		}

		boolean isFirst = true;
		int colStep = 0;
		for (int i = 0; i < row * colCount; i++) {
			if (isFirst) {
				infoStr += "<tr>";
				isFirst = false;
			}
			infoStr += "<td>";
			if (i <= selectedItemNames.size() - 1) {
				infoStr += selectedItemNames.get(i);
			} else {
				infoStr += "&nbsp;";
			}
			infoStr += "</td>";

			colStep++;
			if (colStep == colCount) {
				infoStr += "</tr>";
				isFirst = true;
				colStep = 0;
			}
		}
		infoStr += "</tbody></table>";
		return infoStr;

	}

}
