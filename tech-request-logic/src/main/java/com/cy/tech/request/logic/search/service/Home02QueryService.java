/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.tech.request.logic.search.view.Home02View;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.vo.require.AlertInboxSendGroup;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * @author shaun
 */
@Component("h02Query")
public class Home02QueryService implements QueryService<Home02View>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5305811826971501787L;
    @Autowired
    private URLService urlService;
    @Autowired
    private SearchService searchHelper;

    @PersistenceContext
    transient private EntityManager em;

    @Override
    public List<Home02View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {

        // ====================================
        // 查詢
        // ====================================
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        //資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        @SuppressWarnings("unchecked")
        List<Object[]> result = query.getResultList();
        //資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if(WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }
        
        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();
        LinkedHashMap<String, Home02View> viewResult = Maps.newLinkedHashMap();
        Home02View home02View = null;
        for (int i = 0; i < result.size(); i++) {

            Object[] record = result.get(i);
            int index = 0;

            String sid = (String) record[index++];
            String requireSid = (String) record[index++];
            String requireNo = (String) record[index++];
            String requireTheme = (String) record[index++];
            Integer urgency = (Integer) record[index++];
            Boolean hasForwardDep = (Boolean) record[index++];
            Boolean hasForwardMember = (Boolean) record[index++];
            Boolean hasLink = (Boolean) record[index++];
            String inboxSendGroup = (String) record[index++];
            Integer senderDep = (Integer) record[index++];
            Integer sender = (Integer) record[index++];
            Date createdDate = (Date) record[index++];
            Integer receiveDep = (Integer) record[index++];
            String bigName = (String) record[index++];
            Date requireUpdatedDate = (Date) record[index++];
            String hasTrace = (String) record[index++];

            if (viewResult.containsKey(requireNo)) {
                home02View = viewResult.get(requireNo);
                home02View.getReceiveDeps().add(receiveDep);
                home02View.setReceiveDepName(home02View.getReceiveDepName() + "、" + WkOrgUtils.getOrgNameByOrgSid(receiveDep));

            } else {
                home02View = new Home02View();
                home02View.setSid(sid);
                home02View.setRequireSid(requireSid);
                home02View.setRequireNo(requireNo);
                home02View.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
                home02View.setUrgency(UrgencyType.values()[urgency]);
                home02View.setHasForwardDep(hasForwardDep);
                home02View.setHasForwardMember(hasForwardMember);
                home02View.setHasLink(hasLink);
                AlertInboxSendGroup group = new AlertInboxSendGroup();
                group.setSid(inboxSendGroup);
                home02View.setInboxSendGroup(group);
                home02View.setSenderDep(senderDep);
                home02View.setSender(sender);
                home02View.setCreatedDate(createdDate);
                home02View.setReceiveDeps(Lists.newArrayList(receiveDep));
                home02View.setReceiveDepName(WkOrgUtils.getOrgNameByOrgSid(receiveDep));
                home02View.setBigName(bigName);
                home02View.setRequireUpdatedDate(requireUpdatedDate);
                home02View.setHasTrace(Boolean.valueOf(hasTrace));
            }

            // 處理共通欄位 (一定要擺在『取值』最後面, 否則 index 會不對)
            home02View.prepareCommonColumn(record, index);

            home02View.setLocalUrlLink(urlService.createLoacalURLLink(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(execUserSid, home02View.getRequireNo(), 1)));
            viewResult.put(requireNo, home02View);
        }
        // 解析資料-結束
        usageRecord.parserDataEnd();
        
        return viewResult.values().stream().collect(Collectors.toList());
    }
}
