/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.cy.tech.request.logic.search.view.Search25View;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author jason_h
 */
@Service
@Slf4j
public class Search25QueryService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5657566916202230077L;
    @Autowired
    private URLService urlService;
    @Autowired
    private SearchService searchHelper;
    @Autowired
    private WkJsonUtils jsonUtils;

    @PersistenceContext
    transient private EntityManager em;

    public List<Search25View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {

        // ====================================
        // 查詢
        // ====================================

        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        rawParameters.stream().forEach(entry -> query.setParameter(entry.getKey(), entry.getValue()));
        // 資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        @SuppressWarnings("unchecked")
        List<Object[]> result = query.getResultList();
        // 資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if (WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();
        List<Search25View> viewResult = Lists.newArrayList();
        for (int i = 0; i < result.size(); i++) {

            Search25View v = new Search25View();

            Object[] record = result.get(i);
            int index = 0;
            String sid = (String) record[index++];
            String requireNo = (String) record[index++];
            String requireTheme = (String) record[index++];
            Integer urgency = (Integer) record[index++];
            Date hopeDate = (Date) record[index++];
            Date createdDate = (Date) record[index++];
            String bigName = (String) record[index++];
            String middleName = (String) record[index++];
            String smallName = (String) record[index++];
            Integer createDep = (Integer) record[index++];
            Integer createdUser = (Integer) record[index++];
            String requireStatus = (String) record[index++];
            String wednesdayMaintain = (String) record[index++];
            // 處理共通欄位 (一定要擺在『取值』最後面, 否則 index 會不對)
            v.prepareCommonColumn(record, index);

            v.setSid(sid);
            v.setRequireNo(requireNo);
            v.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
            v.setUrgency(UrgencyType.values()[urgency]);
            v.setHopeDate(hopeDate);
            v.setCreatedDate(createdDate);
            v.setBigName(bigName);
            v.setMiddleName(middleName);
            v.setSmallName(smallName);
            v.setCreateDep(createDep);
            v.setCreatedUser(createdUser);
            v.setRequireStatus(RequireStatusType.valueOf(requireStatus));
            v.setLocalUrlLink(urlService.createLoacalURLLink(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(execUserSid, v.getRequireNo(), 1)));
            v.setWednesdayMaintain(this.findWednesdayMaintainValue(requireNo, wednesdayMaintain));
            viewResult.add(v);
        }
        return viewResult;
    }

    private String findWednesdayMaintainValue(String requireNo, String fromIdxContent) {
        try {
            List<String> idxContentList = jsonUtils.fromJsonToList(fromIdxContent, String.class);
            if (CollectionUtils.isEmpty(idxContentList)) {
                return "N by error parser！！";
            }
            String booleanStr = idxContentList.get(0);
            if (Strings.isNullOrEmpty(booleanStr)) {
                return "N by empty parser！！";
            }
            return booleanStr.toLowerCase().equals("true") ? "Y" : "N";
        } catch (IOException ex) {
            log.error("單號： " + requireNo + "轉換週三維護內容失敗！  " + ex.getMessage(), ex);
        }
        return "";
    }
}
