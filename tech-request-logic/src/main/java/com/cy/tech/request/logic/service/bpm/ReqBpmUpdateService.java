/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.bpm;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.tech.request.logic.service.BpmService;
import com.cy.tech.request.logic.service.ReqUnitBpmService;
import com.cy.tech.request.logic.service.pt.PtBpmService;
import com.cy.tech.request.logic.service.send.test.SendTestBpmService;
import com.cy.tech.request.repository.pt.PtSignInfoRepo;
import com.cy.tech.request.repository.require.RequireUnitSignInfoRepository;
import com.cy.tech.request.repository.worktest.WorkTestSignInfoRepo;
import com.cy.tech.request.vo.pt.PtSignInfo;
import com.cy.tech.request.vo.require.RequireUnitSignInfo;
import com.cy.tech.request.vo.worktest.WorkTestSignInfo;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 自動更新需求單流程
 *
 * @author shaun
 */
@Slf4j
@Component
public class ReqBpmUpdateService implements InitializingBean, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5046217248026037671L;
    private static ReqBpmUpdateService instance;

    public static ReqBpmUpdateService getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        ReqBpmUpdateService.instance = this;
    }

    @Autowired
    private BpmService bpmService;
    @Autowired
    private ReqUnitBpmService reqBpmService;
    @Autowired
    private PtBpmService ptBpmService;
    @Autowired
    private SendTestBpmService stBpmService;

    @Autowired
    private RequireUnitSignInfoRepository rmsiDao;
    @Autowired
    private PtSignInfoRepo ptSignRepo;
    @Autowired
    private WorkTestSignInfoRepo wkTestSignRepo;

    /** 可執行開關 */
    private boolean funSwitch = false;
    /** 重置中 */
    private boolean reseting = false;
    private int execCnt;
    @Getter
    private StringBuilder updateInfo;

    /**
     * 啟動
     */
    public void start() {
        funSwitch = true;
        this.autoUpdate();
    }

    boolean isFirst_autoUpdate = true;
    /**
     * 間隔時間四小時
     */
    private final long autoUpdateFixedDelay = (4 * 60 * 60 * 1000);

    /**
     * 1.重新啟動時觸發<BR/>
     * 2.啟動後每間隔四小時觸發<BR/>
     */
    @Scheduled(fixedDelay = autoUpdateFixedDelay)
    void autoUpdate() {
        if (!funSwitch) {
            if (updateInfo != null) {
                updateInfo.append("需求系統流程重建未開放...請執行 start();").append("<BR/>");
            }
            return;
        }

        this.resetAllFlow();

        // 為避免遺忘此排程，故第一次運作時，印出執行訊息
        if (isFirst_autoUpdate) {
            log.info(String.format(
                    "【排程】更新BPM流程, 執行間隔時間 = %s 小時, ",
                    (this.autoUpdateFixedDelay / 1000 / 60 / 60)));
            isFirst_autoUpdate = false;
        }
    }

    /**
     * 更新需求系統流程
     */
    public void resetAllFlow() {
        if (reseting) {
            updateInfo.append("需求系統流程重建執行中...").append("<BR/>");
            log.info("需求系統流程重建執行中...");
        } else {
            this.reseting = true;
            this.execCnt = 0;
            this.updateInfo = new StringBuilder();
            try {
                log.info("開始執行需求系統流程重建...");
                updateInfo.append("**********執行需求系統流程重建**********").append("<BR/>");
                this.resetReqUnit();
                this.resetPt();
                this.resetTest();
                updateInfo.append("**********總更新數量：").append(execCnt).append("<BR/>");
                updateInfo.append("**********完成需求系統流程重建**********").append("<BR/>");
            } finally {
                this.reseting = false;
                log.info("\n" + updateInfo.toString().replace("<BR/>", "\n"));
            }
        }
    }

    /**
     * 需求單位流程重建
     */
    private void resetReqUnit() {
        try {
            log.info("執行需求單位流程重建...");
            updateInfo.append("執行需求單位流程重建...").append("<BR/>");
            List<RequireUnitSignInfo> process = rmsiDao.findRunningProcess();
            process.stream().forEach(each -> {
                try {
                    reqBpmService.setupReqUnitSignInfo(each);
                    List<ProcessTaskBase> tasks = bpmService.findFlowChartByInstanceId(each.getBpmInstanceId(), "");
                    each.setInstanceStatus(bpmService.createInstanceStatus(each.getBpmInstanceId(), tasks));
                    bpmService.updateReqUnitSignInfo(each);
                    this.execCnt++;
                } catch (ProcessRestException e) {
                    log.error("需求單位流程重建失敗", e);
                    updateInfo.append("需求單位流程重建失敗:ReqNo :")
                            .append(each.getRequireNo())
                            .append(" BPM ID:")
                            .append(each.getBpmInstanceId())
                            .append(" ")
                            .append(e.getMessage())
                            .append("<BR/>");
                }
            });
            updateInfo.append("完成需求單位流程重建").append("<BR/>");
        } catch (Exception e) {
            log.error("需求單位流程重建失敗", e);
            updateInfo.append("查詢未完成的需求單位流程失敗！！").append(e.getMessage()).append("<BR/>");
        }
    }

    /**
     * 原型確認流程重建
     */
    private void resetPt() {
        try {
            updateInfo.append("執行原型確認流程重建...").append("<BR/>");
            List<PtSignInfo> process = ptSignRepo.findRunningProcess();
            process.stream().forEach(each -> {
                try {
                    ptBpmService.setupPtSignInfo(each);
                    each.setInstanceStatus(bpmService.createInstanceStatus(each.getBpmInstanceId(), each.getTaskTo().getTasks()));
                    bpmService.updatePtSignInfo(each);
                    this.execCnt++;
                } catch (ProcessRestException e) {
                    log.error("執行原型確認流程重建", e);
                    updateInfo.append("原型確認流程重建失敗: ReqNo :")
                            .append(each.getSourceNo())
                            .append(" PtNo  :")
                            .append(each.getPtNo())
                            .append(" BPM ID:")
                            .append(each.getBpmInstanceId())
                            .append(" ")
                            .append(e.getMessage())
                            .append("<BR/>");
                }
            });
            updateInfo.append("完成原型確認流程重建").append("<BR/>");
        } catch (Exception e) {
            log.error("執行原型確認流程重建", e);
            updateInfo.append("查詢未完成的原型確認流程失敗！！").append(e.getMessage()).append("<BR/>");
        }
    }

    /**
     * 送測流程重建
     */
    private void resetTest() {
        try {
            updateInfo.append("執行送測流程重建...").append("<BR/>");
            List<WorkTestSignInfo> process = wkTestSignRepo.findRunningProcess();
            process.stream().forEach(each -> {
                try {
                    stBpmService.setupSendTestSignInfo(each);
                    each.setInstanceStatus(bpmService.createInstanceStatus(each.getBpmInstanceId(), each.getTaskTo().getTasks()));
                    bpmService.updateSendTestSignInfo(each);
                    this.execCnt++;
                } catch (ProcessRestException e) {
                    log.error("執行送測流程重建", e);
                    updateInfo.append("送測流程重建失敗: ReqNo :")
                            .append(each.getSourceNo())
                            .append(" StNo  :")
                            .append(each.getTestinfoNo())
                            .append(" BPM ID:")
                            .append(each.getBpmInstanceId())
                            .append(" ")
                            .append(e.getMessage())
                            .append("<BR/>");
                }
            });
            updateInfo.append("完成送測流程重建").append("<BR/>");
        } catch (Exception e) {
            log.error("執行送測流程重建", e);
            updateInfo.append("查詢未完成的送測流程失敗！！").append(e.getMessage()).append("<BR/>");
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void checkReqSignInfo(StringBuilder infoScreenMsg, String bpmId) throws ProcessRestException {
        infoScreenMsg.append("檢查是否為需求單位流程...<BR/>");
        RequireUnitSignInfo singInfo = rmsiDao.findByBpmInstanceId(bpmId);
        if (singInfo != null) {
            infoScreenMsg.append("開始替換需求單位流程...<BR/>");
            infoScreenMsg.append("流程 sid:").append(singInfo.getSid()).append("<BR/>");
            infoScreenMsg.append("需求單號:").append(singInfo.getRequireNo()).append("<BR/>");
            reqBpmService.setupReqUnitSignInfo(singInfo);
            List<ProcessTaskBase> tasks = bpmService.findFlowChartByInstanceId(singInfo.getBpmInstanceId(), "");
            singInfo.setInstanceStatus(bpmService.createInstanceStatus(singInfo.getBpmInstanceId(), tasks));
            bpmService.updateReqUnitSignInfo(singInfo);
            infoScreenMsg.append("替換需求單位流程完成。 需求單號:").append(singInfo.getRequireNo()).append("<BR/>");
        }
    }

    public void checkPtSignInfo(StringBuilder infoScreenMsg, String bpmId) throws ProcessRestException {
        infoScreenMsg.append("檢查是否為原型確認流程...<BR/>");
        PtSignInfo singInfo = ptSignRepo.findByBpmInstanceId(bpmId);
        if (singInfo != null) {
            infoScreenMsg.append("開始替換原型確認單位流程...<BR/>");
            infoScreenMsg.append("流程 sid:").append(singInfo.getSid()).append("<BR/>");
            infoScreenMsg.append("需求單號:").append(singInfo.getSourceNo()).append("<BR/>");
            infoScreenMsg.append("原型確認單號:").append(singInfo.getPtNo()).append("<BR/>");
            ptBpmService.setupPtSignInfo(singInfo);
            singInfo.setInstanceStatus(bpmService.createInstanceStatus(singInfo.getBpmInstanceId(), singInfo.getTaskTo().getTasks()));
            bpmService.updatePtSignInfo(singInfo);
            infoScreenMsg.append("替換原型確認流程完成。 需求單號:").append(singInfo.getSourceNo())
                    .append(" 原型確認單號:").append(singInfo.getPtNo()).append("<BR/>");
        }
    }

    public void checkWkStSignInfo(StringBuilder infoScreenMsg, String bpmId) throws ProcessRestException {
        infoScreenMsg.append("檢查是否為送測流程...<BR/>");
        WorkTestSignInfo singInfo = wkTestSignRepo.findByBpmInstanceId(bpmId);
        if (singInfo != null) {
            infoScreenMsg.append("開始替換送測單位流程...<BR/>");
            infoScreenMsg.append("流程 sid:").append(singInfo.getSid()).append("<BR/>");
            infoScreenMsg.append("需求單號:").append(singInfo.getSourceNo()).append("<BR/>");
            infoScreenMsg.append("送測單號:").append(singInfo.getTestinfoNo()).append("<BR/>");
            stBpmService.setupSendTestSignInfo(singInfo);
            singInfo.setInstanceStatus(bpmService.createInstanceStatus(singInfo.getBpmInstanceId(), singInfo.getTaskTo().getTasks()));
            bpmService.updateSendTestSignInfo(singInfo);
            infoScreenMsg.append("替換送測流程完成。 需求單號:").append(singInfo.getSourceNo())
                    .append(" 送測單號:").append(singInfo.getTestinfoNo()).append("<BR/>");
        }
    }

}
