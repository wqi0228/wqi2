/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import com.cy.commons.vo.User;
import com.cy.tech.request.vo.enums.InboxType;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 收件夾訊息檢視(頁籤用)
 *
 * @author shaun
 */
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"alertGroupSid"})
@Data
public class InboxView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8679946188986365789L;
    /** require sid */
    private String requireSid;
    /** 需求單號 */
    private String requireNo;
    /** alert sid */
    private String alertSid;
    /** alert group sid */
    private String alertGroupSid;
    /** inbox type */
    private InboxType inboxType;
    /** 寄件人 */
    private User sender;
    /** 收件人 */
    private User receive;
    /** 建立日期 */
    private Date createdDate;
    /** 留言訊息 - 含css */
    private String messageCss;

}
