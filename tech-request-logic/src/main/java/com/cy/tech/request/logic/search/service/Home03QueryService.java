/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.tech.request.logic.search.view.Home03View;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.vo.enums.ForwardType;
import com.cy.tech.request.vo.require.AlertInboxSendGroup;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

/**
 * @author shaun
 */
@Component("h03Query")
public class Home03QueryService implements QueryService<Home03View>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -819262051227750837L;
    @Autowired
    private URLService urlService;
    @Autowired
    private SearchService searchHelper;

    @PersistenceContext
    transient private EntityManager em;

    @Override
    public List<Home03View> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) {

        // ====================================
        // 查詢
        // ====================================
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        //資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        @SuppressWarnings("unchecked")
        List<Object[]> result = query.getResultList();
        //資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if(WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }
        
        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();
        List<Home03View> viewResult = Lists.newArrayList();
        for (int i = 0; i < result.size(); i++) {

            Home03View v = new Home03View();
            int index = 0;
            Object[] record = (Object[]) result.get(i);
            String sid = (String) record[index++];
            String requireSid = (String) record[index++];
            String requireNo = (String) record[index++];
            String requireTheme = (String) record[index++];
            Integer urgency = (Integer) record[index++];
            Boolean hasForwardDep = (Boolean) record[index++];
            Boolean hasForwardMember = (Boolean) record[index++];
            Boolean hasLink = (Boolean) record[index++];
            String forwardType = (String) record[index++];
            String sendTo = (String) record[index++];
            Date createdDate = (Date) record[index++];
            String bigName = (String) record[index++];
            Date requireUpdatedDate = (Date) record[index++];
            String hasTrace = (String) record[index++];
            // 處理共通欄位 (一定要擺在『取值』最後面, 否則 index 會不對)
            v.prepareCommonColumn(record, index);

            v.setSid(sid);
            AlertInboxSendGroup group = new AlertInboxSendGroup();
            group.setSid(sid);
            v.setInboxSendGroup(group);
            v.setRequireSid(requireSid);
            v.setRequireNo(requireNo);
            v.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
            v.setUrgency(UrgencyType.values()[urgency]);
            v.setHasForwardDep(hasForwardDep);
            v.setHasForwardMember(hasForwardMember);
            v.setHasLink(hasLink);
            v.setForwardType(ForwardType.valueOf(forwardType));
            v.setSendTo(sendTo);
            v.setCreatedDate(createdDate);
            v.setBigName(bigName);
            v.setRequireUpdatedDate(requireUpdatedDate);
            v.setLocalUrlLink(urlService.createLocalUrlLinkParamForTab(
                    URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(execUserSid, v.getRequireNo(), 1),
                    URLService.URLServiceAttr.URL_ATTR_TAB_INBOX_SEND, sid));
            v.setHasTrace(Boolean.valueOf(hasTrace));
            viewResult.add(v);
        }
        // 解析資料-結束
        usageRecord.parserDataEnd();
        return viewResult;
    }
}
