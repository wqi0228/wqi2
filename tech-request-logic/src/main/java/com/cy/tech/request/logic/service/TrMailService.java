package com.cy.tech.request.logic.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.work.common.service.MailService;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.notify.vo.enums.NotifyType;

import lombok.extern.slf4j.Slf4j;

/**
 * 需求單寄發mail service
 * 
 * @author aken_kao
 */
@Slf4j
@Service
public class TrMailService {

    @Autowired
    private URLService urlService;
    @Autowired
    private MailService mailService;

    /**
     * 發送系統通知 mail
     * 
     * @param requireNo   需求單號
     * @param theme       主題
     * @param mailAddress
     * @param notifyType
     */
    public void sendSysNotify(
            String requireNo,
            String theme,
            Set<String> mailAddress,
            NotifyType notifyType) {

        if (WkStringUtils.isEmpty(mailAddress)) {
            log.debug("未傳入發送 mail address , 略過 mail 通知");
            return;
        }

        // send mail
        String pSubject = String.format("Werp需求單【%s】通知", WkJsoupUtils.getInstance().htmlToText(notifyType.getValue()));
        String url = urlService.buildRequireURL(requireNo);
        url = String.format("%s&notifyType=%s", url, notifyType.name());

        String settingSysNoity = String.format("<a href=\"%s\" target=\"_blank\">【系統通知訂閱】</a>", this.urlService.buildSettingSysNotifyURL());
        String currtheme = String.format("主題: <a href=\"%s\" target=\"_blank\">%s</a>", url, theme);

        String content = ""
                + "Hi"
                + "<br/>"
                + "<br/>"
                + "目前您有一則新的Werp需求單【" + WkJsoupUtils.getInstance().htmlToText(notifyType.getValue()) + "】通知，"
                + "<br/>"
                + notifyType.getMailContent() + "，請您確認。"
                + "<br/><br/>"
                + currtheme
                + "<br/><br/><br/>注意事項：<br/>"
                + "●若點選主題連結後，出現【無閱讀權限】，代表該則需求單撤回分派或通知，<br/>若有疑慮可提供主題與GM確認<br/>"
                + "●若不想再收到此類通知，請到" + settingSysNoity + "進行設定<br/>"
                + "謝謝!"
                + "<br/>";

        this.mailService.sendMailByMailAddress(mailAddress, pSubject, content);
    }
}
