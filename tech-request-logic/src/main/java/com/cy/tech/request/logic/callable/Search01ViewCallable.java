/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.callable;

import java.util.Date;
import java.util.Set;
import java.util.concurrent.Callable;

import com.cy.tech.request.logic.search.view.Search01View;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.work.common.enums.UrgencyType;
import com.google.common.base.Strings;

/**
 * @author brain0925_liao
 */
public class Search01ViewCallable implements Callable<Search01View> {

    private int resultIndex;

    private Object[] record;

    private SearchService searchHelper;

    private URLService urlService;

    private Integer execUserSid;

    private Set<Integer> relationOrgSids;

    public Search01ViewCallable(int resultIndex, Object[] record, SearchService searchHelper, 
            URLService urlService, 
            Integer execUserSid,
            Set<Integer> relationOrgSids) {
        this.resultIndex = resultIndex;
        this.record = record;
        this.searchHelper = searchHelper;
        this.urlService = urlService;
        this.execUserSid = execUserSid;
        this.relationOrgSids = relationOrgSids;
    }

    @Override
    public Search01View call() throws Exception {

        int index = 0;

        String sid = (String) record[index++];
        String requireNo = (String) record[index++];
        String requireTheme = (String) record[index++];
        Integer urgency = (Integer) record[index++];
        Boolean hasForwardDep = (Boolean) record[index++];
        Boolean hasForwardMember = (Boolean) record[index++];
        Boolean hasLink = (Boolean) record[index++];
        Date createdDate = (Date) record[index++];
        String bigName = (String) record[index++];
        String middleName = (String) record[index++];
        String smallName = (String) record[index++];
        Integer createDep = (Integer) record[index++];
        Integer createdUser = (Integer) record[index++];
        String requireStatus = (String) record[index++];
        Boolean checkAttachment = (Boolean) record[index++];
        Boolean hasAttachment = (Boolean) record[index++];
        Date hopeDate = (Date) record[index++];
        String readReason = (String) record[index++];
        Integer customer = (Integer) record[index++];
        Integer inteStaffUser = (Integer) record[index++];
        Date updatedDate = (Date) record[index++];

        // 處理共通欄位 (一定要擺在『取值』最後面, 否則 index 會不對)
        Search01View v = new Search01View();
        v.prepareCommonColumn(record, index);

        v.setResultIndex(resultIndex);
        v.setSid(sid);
        v.setRequireNo(requireNo);
        v.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
        v.setUrgency(UrgencyType.safeIndexOf(urgency));
        v.setHasForwardDep(hasForwardDep);
        v.setHasForwardMember(hasForwardMember);
        v.setHasLink(hasLink);
        v.setCreatedDate(createdDate);
        v.setUpdatedDate(updatedDate);
        v.setBigName(bigName);
        v.setMiddleName(middleName);
        v.setSmallName(smallName);
        v.setCreateDep(createDep);
        v.setCreatedUser(createdUser);
        v.setRequireStatus(RequireStatusType.valueOf(requireStatus));
        v.setCheckAttachment(checkAttachment);
        v.setHasAttachment(hasAttachment);
        v.setHopeDate(hopeDate);
        v.setReadReason(Strings.isNullOrEmpty(readReason) ? ReqToBeReadType.NO_TO_BE_READ : ReqToBeReadType.valueOf(readReason));
        v.setLocalUrlLink(urlService.createLoacalURLLink(
                URLService.URLServiceAttr.URL_ATTR_M,
                urlService.createSimpleUrlTo(execUserSid, v.getRequireNo(), 1)));
        v.setCreate(relationOrgSids.contains(createDep));
        if (customer != null) {
            v.setCustomer(customer.longValue());
        }
        v.setInteStaffUser(inteStaffUser);

        return v;
    }
}
