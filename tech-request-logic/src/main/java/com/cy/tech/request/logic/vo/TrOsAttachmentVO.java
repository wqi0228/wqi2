/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.vo;

import com.cy.tech.request.vo.enums.OthSetHistoryBehavior;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;

/** 
 * 其它設定資訊 - 附件物件
 * @author brain0925_liao
 */
public class TrOsAttachmentVO implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = -7543359911122091834L;
    public TrOsAttachmentVO(String os_attachment_sid, String os_history_sid,
            String os_sid, String os_no, String require_sid,
            String require_no, Integer departmentSid,
            OthSetHistoryBehavior behavior,
            Integer create_usr, Date create_dt,
            Integer update_usr, Date update_dt,
            String file_name, String description) {
        this.os_attachment_sid = os_attachment_sid;
        this.os_history_sid = os_history_sid;
        this.os_sid = os_sid;
        this.os_no = os_no;
        this.require_sid = require_sid;
        this.require_no = require_no;
        this.departmentSid = departmentSid;
        this.behavior = behavior;
        this.create_usr = create_usr;
        this.create_dt = create_dt;
        this.update_usr = update_usr;
        this.update_dt = update_dt;
        this.file_name = file_name;
        this.description = description;
    }

    @Getter
    private String os_attachment_sid;
    @Getter
    private String os_history_sid;
    @Getter
    private String os_sid;
    @Getter
    private String os_no;
    @Getter
    private String require_sid;
    @Getter
    private String require_no;
    @Getter
    private Integer departmentSid;
    @Getter
    private OthSetHistoryBehavior behavior;
    @Getter
    private Integer create_usr;
    @Getter
    private Date create_dt;
    @Getter
    private Integer update_usr;
    @Getter
    private Date update_dt;
    @Getter
    private String file_name;
    @Getter
    private String description;
}
