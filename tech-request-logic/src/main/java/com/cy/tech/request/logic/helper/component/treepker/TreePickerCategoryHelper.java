package com.cy.tech.request.logic.helper.component.treepker;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.helper.component.ComponentHelper;
import com.cy.tech.request.logic.vo.SelectedItemPrepareInfoTo;
import com.cy.tech.request.vo.enums.CategoryType;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkTreeUtils;
import com.cy.work.common.vo.WkItem;
import com.google.common.collect.Lists;
import lombok.NoArgsConstructor;

/**
 * @author allen1214_wu
 */
@NoArgsConstructor
@Service
public class TreePickerCategoryHelper implements InitializingBean, Serializable {

	// ========================================================================
	// InitializingBean
	// ========================================================================

	/**
     * 
     */
    private static final long serialVersionUID = -698039752640886025L;

    private static TreePickerCategoryHelper instance;

	public static TreePickerCategoryHelper getInstance() {
		return instance;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		TreePickerCategoryHelper.instance = this;
	}

	// ========================================================================
	//
	// ========================================================================
	/**
	 * 項目資訊 KEY : 項目類別
	 */
	public final String ITEM_INFO_CATEGORY_TYPE = "ITEM_INFO_CATEGORY_TYPE";

	/**
	 * 項目資訊 KEY : 父項SID
	 */
	public final String ITEM_INFO_PARENT_SID = "ITEM_INFO_PARENT_SID";

	// ========================================================================
	// 工具區
	// ========================================================================
	@Autowired
	private ComponentHelper componentHelper;

	// ========================================================================
	// 外部方法
	// ========================================================================

	/**
	 * 過濾器：僅留下小類
	 * 
	 * @param allItems
	 * @return
	 */
	public List<WkItem> filterOnlySmallCategory(List<WkItem> allItems) {
		if (WkStringUtils.isEmpty(allItems)) {
			return Lists.newArrayList();
		}

		return allItems.stream()
		        .filter(wkItem -> CategoryType.SMALL.equals(this.componentHelper.getItemCategoryType(wkItem)))
		        .collect(Collectors.toList());
	}

	/**
	 * @param allSmallCategoryItems
	 * @param blackList
	 * @return
	 */
	public String prepareCategoryShowInfo(
	        List<WkItem> allSmallCategoryItems,
	        List<String> blackList) {

		// ====================================
		// 解析資料
		// ====================================
		SelectedItemPrepareInfoTo selectedItemPrepareInfoTo = this.componentHelper.prepareSelectedItemInfo(
		        allSmallCategoryItems, blackList);

		// ====================================
		// 強制訊息
		// ====================================
		if (WkStringUtils.notEmpty(selectedItemPrepareInfoTo.getForceMessage())) {
			return this.componentHelper.prepareForceMessageForSettingSysNotify(selectedItemPrepareInfoTo);
		}

		// ====================================
		// 組回傳字串
		// ====================================
		String infoStr = "";
		if (selectedItemPrepareInfoTo.isReverse()) {
			infoStr += "<span class='WS1-1-2b'>【不通知】</span>";
		} else {
			infoStr += "<span class='WS1-1-3b'>【通知】</span>";
		}

		infoStr += String.join("、", selectedItemPrepareInfoTo.getSelectedItemNames());

		return infoStr;

	}

	/**
	 * @param allCategoryItems
	 * @param allSmallCategoryItems
	 * @param blackList
	 * @return
	 */
	public String prepareCategoryTooltip(
	        List<WkItem> allCategoryItems,
	        List<WkItem> allSmallCategoryItems,
	        List<String> blackList) {

		// ====================================
		// 解析資料
		// ====================================
		SelectedItemPrepareInfoTo selectedItemPrepareInfoTo = componentHelper.prepareSelectedItemInfo(allSmallCategoryItems, blackList);

		// ====================================
		// 強制訊息
		// ====================================
		if (WkStringUtils.notEmpty(selectedItemPrepareInfoTo.getForceMessage())) {
			return this.componentHelper.prepareForceMessageForSettingSysNotify(selectedItemPrepareInfoTo);
		}

		// ====================================
		// 組回傳字串
		// ====================================
		String infoStr = "";
		if (selectedItemPrepareInfoTo.isReverse()) {
			infoStr += "<span class='WS1-1-2b'>【不通知以下項目】</span><hr/>";
		} else {
			infoStr += "<span class='WS1-1-3b'>【通知】</span>";
		}

		infoStr += WkTreeUtils.prepareSelectedItemNameByTreeStyle(
		        allCategoryItems, selectedItemPrepareInfoTo.getSelectedItemSids(), 30);

		return infoStr;

	}

}
