/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.service;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.tech.request.logic.callable.Search07ViewCallable;
import com.cy.tech.request.logic.search.view.Search07View;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepService;
import com.cy.tech.request.vo.enums.RequireFinishCodeType;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * @author jason_h
 */
@Slf4j
@Component
public class Search07QueryService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7816695367766689919L;
    @Autowired
    private URLService urlService;
    @Autowired
    private WkJsonUtils jsonUtils;
    @Autowired
    private SearchService searchHelper;
    @Autowired
    private RequireConfirmDepService requireConfirmDepService;

    @PersistenceContext
    transient private EntityManager em;

    private Map<String, RequireFinishCodeType> finishCodeMap;

    /**
     * 初始化 暫存cache
     */
    private void init() {
        if (this.finishCodeMap == null) {
            this.finishCodeMap = Lists.newArrayList(RequireFinishCodeType.values()).stream()
                    .collect(Collectors.toMap(k -> k.getCode(), v -> v));
        }
    }

    @SuppressWarnings("unchecked")
    public List<Search07View> findWithQueryByMultiThread(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid,
            RequireReportUsageRecord usageRecord) { 
        // ====================================
        // 查詢
        // ====================================
        
        //資料庫查詢 - 開始
        usageRecord.dbQueryStart();
        @SuppressWarnings("rawtypes")
        List result = this.getResultList(sql, parameters);
        //資料庫查詢 - 結束
        usageRecord.dbQueryEnd((result == null) ? 0 : result.size());
        if(WkStringUtils.isEmpty(result)) {
            return Lists.newArrayList();
        }
        
        
        // ====================================
        // 封裝
        // ====================================
        // 解析資料-開始
        usageRecord.parserDataStart();
        
        this.init();
        ExecutorService pool = this.getExecutorService(result.size());
        List<Search07View> viewResult = Lists.newArrayList();
        List<Search07ViewCallable> search07ViewCallables = Lists.newArrayList();
        List<String> sids = Lists.newArrayList();

        List<Integer> assignRelateionDepSids = this.requireConfirmDepService.prepareUserMatchDepSids(execUserSid);
        try {
            @SuppressWarnings("rawtypes")
            CompletionService completionPool = new ExecutorCompletionService(pool);
            for (int index = 0; index < result.size(); index++) {
                Object[] record = (Object[]) result.get(index);
                String sid = (String) record[0];
                if (sids.contains(sid)) {
                    continue;
                }
                sids.add(sid);
                
                Search07ViewCallable sc = new Search07ViewCallable(
                        index,
                        jsonUtils,
                        finishCodeMap,
                        searchHelper,
                        urlService,
                        execUserSid,
                        record,
                        Sets.newHashSet(assignRelateionDepSids));
                
                search07ViewCallables.add(sc);
                completionPool.submit(sc);
            }
            IntStream.range(0, search07ViewCallables.size()).forEach(i -> {
                try {
                    Object obj = completionPool.take().get();
                    if (obj == null) {
                        return;
                    }
                    Search07View search07View = (Search07View) obj;
                    viewResult.add(search07View);
                } catch (Exception e) {
                    log.error("search16ViewCallables", e);
                }
            });
        } catch (Exception e) {
            log.error("search16ViewCallables", e);
        } finally {
            pool.shutdown();
        }
        this.compare(viewResult);
        
        // 解析資料-結束
        usageRecord.parserDataEnd();
        return viewResult;
    }

    /**
     * 查詢
     *
     * @param sql
     * @param parameters
     * @return
     */
    @SuppressWarnings("rawtypes")
    private List getResultList(String sql, Map<String, Object> parameters) {
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        try {
            return query.getResultList();
        } catch (Exception e) {
            log.error("查詢失敗!"
                    + "\r\n" + e.getMessage()
                    + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql.toString()) + "\r\n】"
                    + "\r\nPARAMs:【\r\n" + com.cy.work.common.utils.WkJsonUtils.getInstance().toPettyJson(parameters) + "\r\n】"
                    + "\r\n");
            throw e;
        }
    }

    private ExecutorService getExecutorService(Integer count) {
        int poolNum = 200;
        if (count < poolNum) {
            poolNum = count;
        }
        return Executors.newFixedThreadPool(poolNum);
    }

    private void compare(List<Search07View> viewResult) {
        Collections.sort(viewResult, new Comparator<Search07View>() {
            @Override
            public int compare(Search07View o1, Search07View o2) {
                return o1.getResultIndex() - o2.getResultIndex();
            }
        });
    }

}
