package com.cy.tech.request.logic.service.pps6;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.repository.pps6.Pps6ModelerFormRepository;
import com.cy.tech.request.vo.pps6.Pps6ModelerForm;
import com.cy.tech.request.vo.pps6.Pps6ModelerFormVO;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

/**
 * PPS6 Modeler 單據名稱定義資料檔 service
 * 
 * @author allen1214_wu
 */
@Service
public class Pps6ModelerFormService implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 1151869293992889282L;
    // ========================================================================
	// 服務區
	// ========================================================================
	@Autowired
	private transient Pps6ModelerFormRepository pps6ModelerFormRepository;

	// ========================================================================
	// 方法區
	// ========================================================================
	/**
	 * 查詢全部
	 * 
	 * @return
	 */
	public List<Pps6ModelerFormVO> findAll() {
		// ====================================
		// 查詢全部
		// ====================================
		List<Pps6ModelerForm> entities = this.pps6ModelerFormRepository.findAll();
		if (WkStringUtils.isEmpty(entities)) {
			return Lists.newArrayList();
		}

		// ====================================
		// 轉 VO
		// ====================================
		return entities.stream()
		        .map(entity -> new Pps6ModelerFormVO(entity))
		        .collect(Collectors.toList());
	}

}
