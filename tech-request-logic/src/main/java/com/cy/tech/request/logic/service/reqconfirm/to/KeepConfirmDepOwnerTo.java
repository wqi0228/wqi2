/**
 * 
 */
package com.cy.tech.request.logic.service.reqconfirm.to;

import java.io.Serializable;
import java.math.BigInteger;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author allen1214_wu
 */
@NoArgsConstructor
public class KeepConfirmDepOwnerTo implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -6992780995022054414L;

    /** sid */
    @Getter
    @Setter
    private BigInteger requireConfirmSid;

    /** 需求完成確認單位 */
    @Getter
    @Setter
    private Integer depSid;

    /** 負責人 sid */
    @Getter
    @Setter
    private Integer ownerSid;

    /** 需求單 sid */
    @Getter
    @Setter
    private String requireSid;

    /** 需求單 單號 */
    @Getter
    @Setter
    private String requireNo;

    /** 異動原因 */
    @Getter
    @Setter
    private String modifyReason;
}
