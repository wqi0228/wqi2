/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.send.test;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.BpmService;
import com.cy.tech.request.logic.service.RequireReadRecordHelper;
import com.cy.tech.request.logic.service.RequireTraceService;
import com.cy.tech.request.repository.require.ReqModifyRepo;
import com.cy.tech.request.repository.worktest.WorkTestInfoRepo;
import com.cy.tech.request.repository.worktest.WorkTestSignInfoRepo;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.worktest.WorkTestInfo;
import com.cy.tech.request.vo.worktest.WorkTestSignInfo;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.value.to.BpmTaskTo;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * 送測 - 流程
 *
 * @author shaun
 */
@Slf4j
@Component
public class SendTestBpmService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7148168399711093236L;
    @Autowired
    private BpmService bpmService;
    @Autowired
    private RequireTraceService traceService;
    @Autowired
    private SendTestService stService;
    @Autowired
    private SendTestStatusManager sendTestStatusManager;
    @Autowired
    private WorkTestInfoRepo testinfoDao;
    @Autowired
    private WorkTestSignInfoRepo workTestSignInfoRepo;
    @Autowired
    private ReqModifyRepo reqModifyDao;
    @Autowired
    private transient RequireReadRecordHelper requireReadRecordHelper;

    /**
     * 未完成的簽核狀態
     */
    private List<InstanceStatus> notCompleteStatus = Lists.newArrayList(
            InstanceStatus.NEW_INSTANCE,
            InstanceStatus.WAITING_FOR_APPROVE,
            InstanceStatus.APPROVING,
            InstanceStatus.RECONSIDERATION);

    /**
     * 建立送測簽核流程
     *
     * @param testInfo
     * @param executor
     * @return
     * @throws ProcessRestException
     */
    @Transactional(rollbackFor = Exception.class)
    public WorkTestSignInfo createSignInfo(WorkTestInfo testInfo, User executor) throws ProcessRestException {
        WorkTestSignInfo wtsi = new WorkTestSignInfo();
        wtsi.setInstanceStatus(InstanceStatus.NEW_INSTANCE);
        wtsi.setSourceType(WorkSourceType.TECH_REQUEST);
        wtsi.setSourceSid(testInfo.getSourceSid());
        wtsi.setSourceNo(testInfo.getSourceNo());
        wtsi.setTestInfo(testInfo);
        wtsi.setTestinfoNo(testInfo.getTestinfoNo());
        wtsi.setBpmInstanceId(bpmService.createSendTestSignFlow(testInfo, executor));
        this.setupSendTestSignInfo(wtsi);
        return workTestSignInfoRepo.save(wtsi);
    }

    /**
     * 設定簽核資訊經常性異動內容
     *
     * @param wtsi
     * @throws ProcessRestException
     */
    public void setupSendTestSignInfo(WorkTestSignInfo wtsi) throws ProcessRestException {
        List<ProcessTaskBase> tasks = bpmService.findSimulationById(wtsi.getBpmInstanceId());
        wtsi.getTaskTo().setTasks(tasks);
        wtsi.setDefaultSignedName("");
        if (!tasks.isEmpty()) {
            wtsi.setDefaultSignedName(bpmService.findTaskDefaultUserName(tasks.get(tasks.size() - 1)));
            wtsi.getCanSignedIdsTo().setValue(bpmService.findTaskCanSignedUserIds(wtsi.getBpmInstanceId()));
        }
    }

    public boolean showSignBtn(WorkTestInfo testInfo, User login) {
        if (this.checkInfoStatus(testInfo)) {
            return false;
        }
        WorkTestSignInfo signInfo = testInfo.getSignInfo();
        return signInfo.getCanSignedIdsTo().getValue().contains(login.getId());
    }

    private boolean checkInfoStatus(WorkTestInfo testInfo) {
        return testInfo == null || !testInfo.getHasSign() || !testInfo.getTestinfoStatus().equals(WorkTestStatus.SIGN_PROCESS);
    }

    public boolean showRollBackBtn(WorkTestInfo testInfo, User login) {
        boolean canShow = this.showSignBtn(testInfo, login);
        return canShow && testInfo.getSignInfo().getTaskTo().getTasks().size() > 1;
    }

    public boolean showRecoveryBtn(WorkTestInfo testInfo, User login) {
        if (this.checkInfoStatus(testInfo)) {
            return false;
        }
        WorkTestSignInfo signInfo = testInfo.getSignInfo();
        List<ProcessTaskBase> tasks = signInfo.getTaskTo().getTasks();
        InstanceStatus iStatus = signInfo.getInstanceStatus();
        // 任務模擬圖需超過兩位才能進行復原，結案也無法復原
        if (tasks.size() <= 1 || iStatus.equals(InstanceStatus.CLOSED)) {
            return false;
        }
        // 如果為核准..
        int recoverySub = iStatus.equals(InstanceStatus.APPROVED) ? 1 : 2;
        // 前一個簽核者為當前執行者才可復原
        ProcessTaskBase rTask = tasks.get(tasks.size() - recoverySub);
        if (rTask instanceof ProcessTaskHistory) {
            if (login.getId().equals(((ProcessTaskHistory) rTask).getExecutorID())) {
                return true;
            }
        }
        return false;
    }

    public boolean showInvaildBtn(WorkTestInfo testInfo, User login) {
        if (this.checkInfoStatus(testInfo)) {
            return false;
        }
        List<ProcessTaskBase> tasks = testInfo.getSignInfo().getTaskTo().getTasks();
        InstanceStatus iStatus = testInfo.getSignInfo().getInstanceStatus();
        if (iStatus.equals(InstanceStatus.APPROVED)) {
            if (tasks.size() > 0) {
                ProcessTaskHistory lastTask = (ProcessTaskHistory) tasks.get(tasks.size() - 1);
                return lastTask.getExecutorID().equals(login.getId());
            }
        }
        return iStatus.equals(InstanceStatus.NEW_INSTANCE);
    }

    @Transactional(rollbackFor = Exception.class)
    public void doSign(WorkTestInfo testInfo, User executor, String comment) throws ProcessRestException {

        // ====================================
        // 檢核
        // ====================================
        // 檢核是否可簽名
        Preconditions.checkArgument(this.showSignBtn(testinfoDao.findOne(testInfo.getSid()), executor), "執行簽核失敗！！");
        // 重撈 bpm 再判斷一次 (和上面重複=.=?)
        bpmService.checkCanDoExceptRecovery(executor, testInfo.getSignInfo().getBpmInstanceId());

        // ====================================
        // 簽名
        // ====================================
        // 執行 - 送測審核 - 簽核 (BPM)
        bpmService.doSendTestSign(testInfo, executor, comment);
        // 更新簽核資訊
        this.updateSignInfo(testInfo);

        // ====================================
        // 簽核流程全部完畢時(核准), 後續進行的動作
        // ====================================
        if (testInfo.getSignInfo().getInstanceStatus().equals(InstanceStatus.APPROVED)) {
            this.handlerApproveSave(testInfo, executor);
        }
        log.info("{} BPM送測單簽核:{}", executor.getId(), testInfo.getTestinfoNo());
    }

    /**
     * 更新簽核資訊
     * 
     * @param testInfo
     * @throws ProcessRestException
     */
    private void updateSignInfo(WorkTestInfo testInfo) throws ProcessRestException {
        this.setupSendTestSignInfo(testInfo.getSignInfo());

        // 判斷流程狀態
        InstanceStatus is = bpmService.createInstanceStatus(
                testInfo.getSignInfo().getBpmInstanceId(),
                testInfo.getSignInfo().getTaskTo().getTasks());

        testInfo.getSignInfo().setInstanceStatus(is);
        bpmService.updateSendTestSignInfo(testInfo.getSignInfo());
    }

    /**
     * 送測核准
     * @param testInfo
     * @param executor
     */
    @Transactional(rollbackFor = Exception.class)
    private void handlerApproveSave(WorkTestInfo testInfo, User executor) {
        // ====================================
        // 防呆，僅核准狀態可進行
        // ====================================
        if (!testInfo.getSignInfo().getInstanceStatus().equals(InstanceStatus.APPROVED)) {
            return;
        }

        // ====================================
        // update 送測審核資訊檔 work_test_sign_info
        // ====================================
        // 壓核准日
        testInfo.getSignInfo().setApprovalDate(new Date());
        // update
        bpmService.updateSendTestSignInfo(testInfo.getSignInfo());

        // ====================================
        // update 送測主檔
        // ====================================
        // 異動欄位狀態 - 核准
        this.sendTestStatusManager.changeStatusBySignApprove(testInfo);

        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.WORKTESTSIGNINFO,
                testInfo.getSid(),
                executor.getSid());

        stService.save(testInfo, executor);
    }

    @Transactional(rollbackFor = Exception.class)
    public void doRecovery(WorkTestInfo testInfo, User executor) throws ProcessRestException {
        Preconditions.checkArgument(this.showRecoveryBtn(testinfoDao.findOne(testInfo.getSid()), executor), "執行復原失敗！！");
        bpmService.checkCanRecovery(executor, testInfo.getSignInfo().getBpmInstanceId());
        BpmTaskTo taskTo = testInfo.getSignInfo().getTaskTo();
        int recoverySub = testInfo.getSignInfo().getInstanceStatus().equals(InstanceStatus.APPROVED) ? 1 : 2;
        bpmService.doRecovery(executor, (ProcessTaskHistory) taskTo.getTasks().get(taskTo.getTasks().size() - recoverySub));
        this.updateSignInfo(testInfo);
        log.info("{} BPM送測單復原:{}", executor.getId(), testInfo.getTestinfoNo());
    }

    @Transactional(rollbackFor = Exception.class)
    public void doRollBack(WorkTestInfo testInfo, User executor, ProcessTaskHistory rollbackTask, String rollbackComment) throws ProcessRestException {
        Preconditions.checkArgument(this.showRollBackBtn(testinfoDao.findOne(testInfo.getSid()), executor), "執行退回失敗！！");
        bpmService.checkCanDoExceptRecovery(executor, testInfo.getSignInfo().getBpmInstanceId());
        bpmService.doRollBack(executor, rollbackTask, rollbackComment);
        this.updateSignInfo(testInfo);
        log.info("{} BPM送測單退回:{}", executor.getId(), testInfo.getTestinfoNo());
    }

    @Transactional(rollbackFor = Exception.class)
    public void doInvaild(Require require, WorkTestInfo testInfo, User executor, String invaildTraceReason) throws ProcessRestException {
        Preconditions.checkArgument(this.showInvaildBtn(testinfoDao.findOne(testInfo.getSid()), executor), "執行作廢失敗！！");
        bpmService.checkCanDoInvaild(executor, testInfo.getSignInfo().getBpmInstanceId());
        bpmService.invaildProcess(executor, testInfo.getSignInfo().getBpmInstanceId());
        this.setupSendTestSignInfo(testInfo.getSignInfo());
        this.handlerInvaildSave(require, testInfo, executor, invaildTraceReason);
        log.info("{} BPM送測單作廢:{}", executor.getId(), testInfo.getTestinfoNo());
    }

    /**
     * 審核作廢
     * @param require
     * @param testInfo
     * @param executor
     * @param invaildTraceReason
     */
    private void handlerInvaildSave(Require require, WorkTestInfo testInfo, User executor, String invaildTraceReason) {
        WorkTestSignInfo signInfo = testInfo.getSignInfo();
        signInfo.setInstanceStatus(InstanceStatus.INVALID);
        signInfo.setCanSignedIdsTo(new JsonStringListTo());
        signInfo.setDefaultSignedName("");
        bpmService.updateSendTestSignInfo(signInfo);

        testInfo.setStatus(Activation.INACTIVE);
        testInfo.setTestinfoStatus(WorkTestStatus.VERIFY_INVAILD);
        testInfo.setReadReason(WaitReadReasonType.TEST_VERIFY_INVAILD);
        testInfo.setReadUpdateDate(new Date());
        stService.save(testInfo, executor);

        traceService.createSendTestProcessInvaildTrace(require.getSid(), testInfo, executor, invaildTraceReason);
        require.setHasTrace(Boolean.TRUE);
        reqModifyDao.updateHasTrace(require, require.getHasTrace());
        
        // ====================================
        // 將單據有閱讀記錄者，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.updateWaitReadWithoutExecUser(
                FormType.WORKTESTSIGNINFO,
                testInfo.getSid(),
                executor.getSid());
    }

    @Transactional(readOnly = true)
    public Boolean hasAnyProcessNotComplete(String requireNo) {
        return workTestSignInfoRepo.hasAnyProcessNotCompleteBySourceTypeAndSoruceNoAndStatusIn(
                WorkSourceType.TECH_REQUEST,
                requireNo, this.notCompleteStatus);
    }

    @Transactional(readOnly = true)
    public List<WorkTestSignInfo> findProcessNotComplete(String requireNo) {

        return workTestSignInfoRepo.findProcessNotCompleteBySourceTypeAndSoruceNoAndStatusIn(
                WorkSourceType.TECH_REQUEST,
                requireNo, this.notCompleteStatus);
    }

    @Transactional(readOnly = true)
    public Integer findProcessNotCompleteCount(String requireNo) {
        List<WorkTestSignInfo> result = this.findProcessNotComplete(requireNo);
        if (result == null) {
            return 0;
        }
        return result.size();
    }

    /**
     * @param requireNo
     * @param depSids
     * @return
     */
    public Integer queryCountByDepSid(String requireNo, List<Integer> depSids) {

        // ====================================
        // 檢核
        // ====================================
        if (WkStringUtils.isEmpty(requireNo)) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "傳入需求單號為空!");
            return null;
        }

        if (WkStringUtils.isEmpty(depSids)) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "傳入查詢單位為空!");
            return null;
        }

        // ====================================
        // 查詢
        // ====================================
        // 未完成代碼
        List<String> paperCodes = this.notCompleteStatus.stream()
                .map(InstanceStatus::getValue)
                .collect(Collectors.toList());

        // 查詢
        return this.workTestSignInfoRepo.queryCountByPaperCodeAndDepSid(
                WorkSourceType.TECH_REQUEST.name(),
                requireNo,
                paperCodes,
                depSids);
    }

}
