/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import com.cy.tech.request.repository.require.RequireUnitSignInfoRepository;
import com.cy.tech.request.vo.require.RequireUnitSignInfo;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 需求單位流程服務
 */
@Service
public class RequireUnitSignInfoService implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 6324862092662190012L;
    @Autowired
	private RequireUnitSignInfoRepository requireUnitSignInfoRepository;

	/**
	 * @param requireUnitSignInfo
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public RequireUnitSignInfo save(RequireUnitSignInfo requireUnitSignInfo) {
		return this.requireUnitSignInfoRepository.save(requireUnitSignInfo);
	}
}
