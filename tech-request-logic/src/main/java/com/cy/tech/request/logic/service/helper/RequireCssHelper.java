/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.helper;

import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.repository.require.RequireCssContentRepository;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireCssContent;
import com.cy.tech.request.vo.template.component.ComBase;
import com.cy.tech.request.vo.template.component.ComBaseCss;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shaun
 */
@Component
public class RequireCssHelper implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -475762933397365748L;
    @Autowired
	transient private RequireCssContentRepository dao;
	@Autowired
	transient private RequireService requireService;

	/**
	 * 建立索引前刪除原本索引
	 */
	@Transactional(rollbackFor = Exception.class)
	public void removeCssContent(String requireSid) {
		Require require = this.requireService.findByReqSid(requireSid);
		this.dao.deleteByRequire(require);
	}

	/**
	 * @param requireSid
	 * @return
	 */
	@Transactional(readOnly = true)
	public List<RequireCssContent> findCssContentByRequire(String requireSid) {
		return dao.findByRequireSid(requireSid);
	}

	/**
	 * @param requireSid
	 * @param requireNo
	 * @param coms
	 * @param execDate
	 */
	public void createByComs(
	        String requireSid,
	        String requireNo,
	        Collection<ComBase> coms,
	        Date execDate) {

		if (WkStringUtils.isEmpty(coms)) {
			return;
		}
		
		Require require = this.requireService.findByReqSid(requireSid);

		// ====================================
		// 建立物件
		// ====================================
		List<RequireCssContent> requireCssContents = Lists.newArrayList();

		for (ComBase com : coms) {
			if (com instanceof ComBaseCss) {
				ComBaseCss comCss = (ComBaseCss) com;
				RequireCssContent css = new RequireCssContent();
				css.setComId(comCss.getComId());
				css.setFieldName(comCss.getName());
				css.setRequire(require);
				css.setRequireNo(requireNo);
				css.setContentCss(comCss.catchCssText());
				requireCssContents.add(css);
			}
		}

		// ====================================
		// save
		// ====================================
		this.dao.save(requireCssContents);

		// ====================================
		// 記錄後移除 (未知作用)
		// ====================================
		for (ComBase com : coms) {
			if (com instanceof ComBaseCss) {
				ComBaseCss comCss = (ComBaseCss) com;
				comCss.clearCssText();
			}
		}
	}
}
