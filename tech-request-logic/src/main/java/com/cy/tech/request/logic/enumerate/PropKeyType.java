package com.cy.tech.request.logic.enumerate;

public enum PropKeyType {

    TECH_REQUEST_AP_URL("tech.request.ap.url"),
    TECH_REQUEST_REST_URL("tech.request.rest.url"),
    TECH_ISSUE_AP_URL("tech.issue.ap.url");
    
    private String value;
    
    private PropKeyType(String value){
        this.value = value;
    }
    
    public String getValue(){
        return value;
    }
}
