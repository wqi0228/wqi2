package com.cy.tech.request.logic.service.setting.onetimetrns.to;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TrnsForSetupInfoTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6412474956974877544L;

    @JsonProperty(value = "department")
    private List<String> department = Lists.newArrayList();

    @JsonProperty(value = "users")
    private List<String> users = Lists.newArrayList();
}
