/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.cy.work.common.enums.UrgencyType;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 檢查確認單據查詢頁面顯示vo (search03.xhtml)
 *
 * @author jason_h
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Search03View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -328834545895175637L;
    /** 緊急度 */
    private UrgencyType urgency;
    /** 部 */
    private Boolean hasForwardDep;
    /** 個 */
    private Boolean hasForwardMember;
    /** 關 */
    private Boolean hasLink;
    /** 立單日期 */
    private Date createdDate;
    /** 需求類別 */
    private String bigName;
    /** 中類 */
    private String middleName;
    /** 小類 */
    private String smallName;
    /** 需求單位 */
    private Integer createDep;
    /** 需求人員 */
    private Integer createdUser;
    /** 本地端連結網址 */
    private String localUrlLink;
    /**  */
    private List<Boolean> itemCheckeds;
    
    

}
