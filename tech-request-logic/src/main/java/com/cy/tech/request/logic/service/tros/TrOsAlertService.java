/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.tros;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.repository.require.tros.TrOsAlertRepository;
import com.cy.tech.request.vo.enums.OthSetAlertType;
import com.cy.tech.request.vo.require.tros.TrOsAlert;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.enums.ReadRecordType;

/**
 * 其他設定資訊Alert Service
 *
 * @author brain0925_liao
 */
@Component
public class TrOsAlertService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5247829120000182804L;
    /** TrOsAlertRepository */
    @Autowired
    private TrOsAlertRepository trOsAlertRepository;
    /** UserService */
    @Autowired
    private UserService userService;
    /** 日期格式 */
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

    /** *
     * 建立回覆的回覆Alert
     *
     * @param os_sid 其他設定資訊Sid
     * @param os_no 其他設定資訊單號
     * @param require_sid 需求單Sid
     * @param require_no 需求單單號
     * @param os_theme 主題
     * @param createUserSid 建立者Sid
     * @param replyCreateSid 回覆建立者Sid
     * @param os_reply_sid 回覆Sid
     * @param os_reply_and_reply_sid 回覆的回覆Sid
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void createReplyAndReplyAlert(String os_sid, String os_no, String require_sid,
            String require_no, String os_theme, Integer createUserSid, Integer replyCreateSid,
            String os_reply_sid, String os_reply_and_reply_sid) {
        Executors.newCachedThreadPool().execute(() -> {
            List<Integer> replyAndReplyUserSid = trOsAlertRepository.findSenderSidByReplySid(os_reply_sid);
            if (!replyAndReplyUserSid.contains(replyCreateSid)) {
                replyAndReplyUserSid.add(replyCreateSid);
            }
            replyAndReplyUserSid.forEach(item -> {
                TrOsAlert alert = this.createAlert(os_sid, os_no, require_sid, require_no,
                        createUserSid, item,
                        OthSetAlertType.REPLY_AND_REPLY
                );
                alert.setOs_theme(sdf.format(new Date()) + " " + os_theme + " - 回覆通知");
                alert.setOs_reply_sid(os_reply_sid);
                alert.setOs_reply_and_reply_sid(os_reply_and_reply_sid);
                trOsAlertRepository.save(alert);
            });

        });

    }

    /**
     * 建立取消Alert
     *
     * @param os_sid 其他設定資訊Sid
     * @param os_no 其他設定資訊單號
     * @param require_sid 需求單Sid
     * @param require_no 需求單單號
     * @param os_theme 主題
     * @param noticeDeps 通知單位Sids
     * @param createUserSid 建立者Sid
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void createCancelAlert(String os_sid, String os_no, String require_sid,
            String require_no, String os_theme, List<Integer> noticeDeps, Integer createUserSid) {
        //開始作業訊息更新成已閱讀
        trOsAlertRepository.updateReadStatus(os_sid, OthSetAlertType.NOTICE_WORKING, ReadRecordType.SYSTEM_UPDATE);
        Executors.newCachedThreadPool().execute(() -> {
            List<Org> noticeOrgs = WkOrgCache.getInstance().findBySids(noticeDeps);
            List<User> receiversUsers = userService.findByStatusAndPrimaryOrgIn(Activation.ACTIVE, noticeOrgs);
            receiversUsers.forEach(receiver -> {
                TrOsAlert alert = this.createAlert(os_sid, os_no, require_sid, require_no,
                        createUserSid, receiver.getSid(),
                        OthSetAlertType.CANCEL
                );
                alert.setOs_theme(sdf.format(new Date()) + " " + os_theme + " - 已作廢");
                trOsAlertRepository.save(alert);
            });
        });
    }

    /**
     * 建立完成Alert
     *
     * @param os_sid 其他設定資訊Sid
     * @param os_no 其他設定資訊單號
     * @param require_sid 需求單Sid
     * @param require_no 需求單單號
     * @param os_theme 主題
     * @param noticeDeps 通知單位Sids
     * @param createUserSid 建立者Sid
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void createFinishAlert(String os_sid, String os_no, String require_sid,
            String require_no, String os_theme, List<Integer> noticeDeps, Integer createUserSid) {
        //開始作業訊息更新成已閱讀
        trOsAlertRepository.updateReadStatus(os_sid, OthSetAlertType.NOTICE_WORKING, ReadRecordType.SYSTEM_UPDATE);
        Executors.newCachedThreadPool().execute(() -> {
            List<Org> noticeOrgs = WkOrgCache.getInstance().findBySids(noticeDeps);
            List<User> receiversUsers = userService.findByStatusAndPrimaryOrgIn(Activation.ACTIVE, noticeOrgs);
            receiversUsers.forEach(receiver -> {
                TrOsAlert alert = this.createAlert(os_sid, os_no, require_sid, require_no,
                        createUserSid, receiver.getSid(),
                        OthSetAlertType.FINISH
                );
                alert.setOs_theme(sdf.format(new Date()) + " " + os_theme + " - 已完成");
                trOsAlertRepository.save(alert);
            });
        });
    }

    /**
     * 取得Alert物件
     *
     * @param os_sid 其他設定資訊Sid
     * @param os_no 其他設定資訊單號
     * @param require_sid 需求單Sid
     * @param require_no 需求單單號
     * @param sendUserSid 發送者Sid
     * @param receviceUserSid 接收者Sid
     * @param type AlertType
     * @return
     */
    private TrOsAlert createAlert(String os_sid, String os_no, String require_sid,
            String require_no, Integer sendUserSid, Integer receviceUserSid,
            OthSetAlertType type) {
        TrOsAlert alert = new TrOsAlert();
        alert.setOs_sid(os_sid);
        alert.setOs_no(os_no);
        alert.setRequire_sid(require_sid);
        alert.setRequire_no(require_no);
        alert.setSend_usr(sendUserSid);
        alert.setSendDate(new Date());
        alert.setReceiver(receviceUserSid);
        alert.setReadStatus(ReadRecordType.UN_READ);
        alert.setType(type);
        return alert;
    }
}
