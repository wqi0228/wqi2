/**
 * 
 */
package com.cy.tech.request.logic.service;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.vo.enums.FormType;
import com.cy.work.common.logic.lib.manager.WorkCommonReadRecordManager;
import com.cy.work.common.logic.lib.manager.to.WorkCommonReadRecordTo;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * @author allen1214_wu
 */
@Service
public class RequireReadRecordHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 259348249139481933L;

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private transient WorkCommonReadRecordManager workCommonReadRecordManager;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 寫已讀記錄
     * 
     * @param formType
     * @param fromSid
     * @param userSid
     */
    public void saveAlreadyRead(
            FormType formType,
            String fromSid,
            Integer userSid) {

        this.workCommonReadRecordManager.saveAlreadyRead(
                formType.getReadRecordTableName(),
                formType.getReadRecordFormSidColumnName(),
                fromSid,
                Sets.newHashSet(userSid));
    }

    /**
     * 建立或更新
     * 1.執行者 -> 已讀
     * 2.其他使用者 -> 待閱讀
     * 
     * @param formType    單據類型
     * @param fromSid     單據 sid
     * @param userSids    要更新的人員
     * @param execUserSid 執行者 sid
     */
    public void saveExcutorReadAndOtherUserWaitRead(
            FormType formType,
            String fromSid,
            Collection<Integer> userSids,
            Integer execUserSid) {

        // ====================================
        // 其他使用者 -> 待閱讀
        // ====================================
        if (WkStringUtils.notEmpty(userSids)) {
            this.workCommonReadRecordManager.saveWaitRead(
                    formType.getReadRecordTableName(),
                    formType.getReadRecordFormSidColumnName(),
                    fromSid,
                    userSids);
        }

        // ====================================
        // 執行者 -> 已讀
        // ====================================
        // 順序不可調換，避免執行者包含在其他使用者的清單中
        this.workCommonReadRecordManager.saveAlreadyRead(
                formType.getReadRecordTableName(),
                formType.getReadRecordFormSidColumnName(),
                fromSid,
                Lists.newArrayList(execUserSid));

    }

    /**
     * 將傳入的user,新增或異動為『待閱讀』(執行者除外)
     * 
     * @param formType    單據類型
     * @param fromSid     單據 sid
     * @param userSids    要更新的人員
     * @param execUserSid 執行者 sid
     */
    public void saveUsersToWaitReadWithoutExecUser(
            FormType formType,
            String fromSid,
            Collection<Integer> userSids,
            Integer execUserSid) {

        // ====================================
        // 過濾掉執行者
        // ====================================
        Set<Integer> processcUserSids = WkCommonUtils.safeStream(userSids)
                .filter(userSid -> !WkCommonUtils.compareByStr(userSid, execUserSid))
                .collect(Collectors.toSet());

        if (WkStringUtils.isEmpty(processcUserSids)) {
            return;
        }

        // ====================================
        // 執行
        // ====================================
        this.workCommonReadRecordManager.saveWaitRead(
                formType.getReadRecordTableName(),
                formType.getReadRecordFormSidColumnName(),
                fromSid,
                processcUserSids);

    }

    /**
     * 1.新增或異動為『待閱讀』
     * 2.移除不在『傳入清單』，且『從未讀取』的 USER
     * 3.執行者為已讀
     * 
     * @param formType 單據類型
     * @param fromSid  單據 sid
     * @param userSids 要異動的 user sid list
     */
    public void resetUserList(
            FormType formType,
            String fromSid,
            Collection<Integer> userSids,
            Integer execUserSid) {

        this.workCommonReadRecordManager.saveWaitReadAndDeleteNotInListNeverReadUser(
                formType.getReadRecordTableName(),
                formType.getReadRecordFormSidColumnName(),
                fromSid,
                userSids);

        // ====================================
        // 執行者 -> 已讀
        // ====================================
        // 順序不可調換，避免執行者包含在其他使用者的清單中
        this.workCommonReadRecordManager.saveAlreadyRead(
                formType.getReadRecordTableName(),
                formType.getReadRecordFormSidColumnName(),
                fromSid,
                Lists.newArrayList(execUserSid));

    }

    /**
     * 將『已經讀取』過單據的人，更新為待閱讀 (執行者除外)
     * 
     * @param formType    單據類型
     * @param fromSid     單據 sid
     * @param execUserSid 執行者
     */
    public void updateWaitReadWithoutExecUser(
            FormType formType,
            String fromSid,
            Integer execUserSid) {

        this.workCommonReadRecordManager.updateWaitReadByFormSidWithoutExecUser(
                formType.getReadRecordTableName(),
                formType.getReadRecordFormSidColumnName(),
                fromSid,
                execUserSid);
    }

    /**
     * 取得單據閱讀記錄資料
     * 
     * @param formType 單據類型
     * @param fromSid  單據 sid
     * @return
     */
    public Set<WorkCommonReadRecordTo> findByFormSid(
            FormType formType,
            String fromSid) {
        return this.workCommonReadRecordManager.findByFormSid(
                formType.getReadRecordTableName(),
                formType.getReadRecordFormSidColumnName(),
                fromSid);
    }

    /**
     * 取得單據閱讀記錄資料
     * 
     * @param formType 單據類型
     * @param fromSid  單據 sid
     * @return
     */
    public Map<String, List<WorkCommonReadRecordTo>> findByFormSids(
            FormType formType,
            List<String> fromSids) {
        return this.workCommonReadRecordManager.findByFormSids(
                formType.getReadRecordTableName(),
                formType.getReadRecordFormSidColumnName(),
                fromSids);
    }

    /**
     * 將使用者改為已讀, 但是不 save 資料
     * 
     * @param readRecordTos
     * @param userSid
     */
    public void changeToAlreadyReadAndNotSave(
            List<WorkCommonReadRecordTo> readRecordTos,
            Integer userSid) {

        for (WorkCommonReadRecordTo readRecordTo : readRecordTos) {
            if (WkCommonUtils.compareByStr(readRecordTo.getUserSid(), userSid)) {
                readRecordTo.setWaitRead(false);
                readRecordTo.setReadDateTime(new Date());
                return;
            }
        }

        // 都找不到, 新增一筆新的
        WorkCommonReadRecordTo readRecordTo = new WorkCommonReadRecordTo();
        readRecordTo.setUserSid(userSid);
        readRecordTo.setWaitRead(false);
        readRecordTo.setReadDateTime(new Date());

        readRecordTos.add(readRecordTo);
    }

}
