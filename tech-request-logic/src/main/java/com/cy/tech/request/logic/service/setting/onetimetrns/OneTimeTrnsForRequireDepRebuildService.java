/**
 * 
 */
package com.cy.tech.request.logic.service.setting.onetimetrns;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.logic.service.AssignNoticeService;
import com.cy.tech.request.logic.service.setting.onetimetrns.to.RequireDepRebuildTo;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkConstants;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * 需求單位不再合併到部
 * 
 * @author allen1214_wu
 */
@Slf4j
@Service
public class OneTimeTrnsForRequireDepRebuildService implements Serializable {

    // 1.多個分派單位(組、部)合成『部級-需求確認單位』
    // a.減派所有組級, 僅留下部級
    // b.重算需求確認單位
    //
    // 2.一個分派單位，對應一個『部級-需求確認單位』
    // a.分派單位為組時
    // -分派單位：不異動
    // -需求確認單位 : update dep sid 為該組 (繼承原需求確認單位所有屬性)
    // b.分派單位為部級以上時
    // -分派單位：不異動
    // -需求確認單位 :不異動

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 5450261924347933829L;

    // ========================================================================
    // 服務區
    // ========================================================================
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private AssignNoticeService assignNoticeService;

    @Autowired
    private OneTimeTrnsForRequireDepCommonService requireDepCommonService;

    private Integer DIVISION_LEVEL = WkOrgUtils.getOrgLevelOrder(OrgLevel.DIVISION_LEVEL);

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * 查詢未處理
     * 
     * @param targetRequireNo
     * @param isOnlyClose
     * @return
     */
    public Map<String, String> findWaitTransRequireSids(
            String targetRequireNo,
            boolean isOnlyClose) {

        String statusStr = "";

        if (WkStringUtils.notEmpty(targetRequireNo)) {
            if (isOnlyClose) {
                // 僅結案
                Lists.newArrayList(
                        RequireStatusType.CLOSE,
                        RequireStatusType.AUTO_CLOSED).stream()
                        .map(v -> v.name())
                        .collect(Collectors.joining("','", "'", "'"));
            } else {
                Lists.newArrayList(
                        RequireStatusType.PROCESS,
                        RequireStatusType.SUSPENDED,
                        RequireStatusType.COMPLETED,
                        RequireStatusType.CLOSE,
                        RequireStatusType.AUTO_CLOSED).stream()
                        .map(v -> v.name())
                        .collect(Collectors.joining("','", "'", "'"));
            }
        }

        // ====================================
        // 查詢待轉檔
        // ====================================
        StringBuffer varname1 = new StringBuffer();
        varname1.append("SELECT tr.require_sid , tr.require_no ");
        varname1.append("FROM   tr_require  tr ");
        varname1.append("WHERE  1=1  ");

        if (WkStringUtils.notEmpty(statusStr)) {
            varname1.append("  AND  tr.require_status  IN (" + statusStr + ")  ");
        }

        if (WkStringUtils.notEmpty(targetRequireNo)) {
            varname1.append("  AND  tr.require_no = '" + targetRequireNo + "' ");
        } else {
            varname1.append("AND  tr.trns_flag_for_confirm_dep = 0  ");
            //
        }

        // 2020/6 前無確認單位規則 故排除無需求確認檔的單據
        varname1.append("AND  EXISTS ( "
                + "SELECT 1 "
                + "  FROM tr_require_confirm_dep cdep "
                + " WHERE cdep.status = 0 "
                + "   AND tr.require_sid = cdep.require_sid  ) ");

        varname1.append("ORDER  BY tr.create_dt DESC ");
        varname1.append("LIMIT  100;");

        List<Map<String, Object>> dataMaps = this.jdbcTemplate.queryForList(varname1.toString());
        if (WkStringUtils.isEmpty(dataMaps)) {
            return Maps.newHashMap();
        }

        log.debug("未轉主檔查詢:[{}]筆", dataMaps.size());

        return dataMaps.stream()
                .collect(Collectors.toMap(
                        dataMap -> dataMap.get("require_sid") + "",
                        dataMap -> dataMap.get("require_no") + ""));

    }

    @Transactional(rollbackFor = Exception.class)
    public void process(
            String requireSid,
            String requireNo,
            Set<Integer> beforeAssignDepSids,
            Set<Integer> beforeNoticeDepSids,
            List<RequireDepRebuildTo> requireDepRebuildTos) throws UserMessageException {

        if (beforeAssignDepSids == null) {
            beforeAssignDepSids = Sets.newHashSet();
        }
        if (beforeNoticeDepSids == null) {
            beforeNoticeDepSids = Sets.newHashSet();
        }
        if (requireDepRebuildTos == null) {
            requireDepRebuildTos = Lists.newArrayList();
        }

        // ====================================
        // 查詢現有的需求完成確認單位檔
        // ====================================

        // 以 dep sid 做索引
        Map<Integer, RequireDepRebuildTo> requireConfirmDepMapByDepSid = requireDepRebuildTos.stream()
                .collect(Collectors.toMap(
                        RequireDepRebuildTo::getDepSid,
                        vo -> vo));

        // 建立以需求確認單位檔的單位為 key 的容器 (分派單位 -> 需求確認單位對應)
        Map<Integer, Set<Integer>> assignDepSidsMapByConfirmDepSid = requireDepRebuildTos.stream()
                .collect(Collectors.toMap(
                        RequireDepRebuildTo::getDepSid,
                        vo -> Sets.newHashSet()));

        // ====================================
        // 整理分派單位 -> 需求確認單位對應
        // ====================================
        // 有問題的需求確認單位
        Set<Integer> warnDepSids = Sets.newHashSet();

        // 將分派單位以『收束到部』的邏輯歸類到需求確認單位檔
        for (Integer assignDepSid : beforeAssignDepSids) {

            // 收束到部
            Org baseDep = WkOrgUtils.prepareBasicDep(assignDepSid, OrgLevel.MINISTERIAL);

            // 試試看自己是否已經為需求單位 (for 已吃新規則的單據)
            if (assignDepSidsMapByConfirmDepSid.containsKey(assignDepSid)) {
                assignDepSidsMapByConfirmDepSid.get(assignDepSid).add(assignDepSid);
            }
            // 『分派單位』歸類到對應（收束）的『需求確認單位』
            else if (assignDepSidsMapByConfirmDepSid.containsKey(baseDep.getSid())) {
                assignDepSidsMapByConfirmDepSid.get(baseDep.getSid()).add(assignDepSid);

            }
            // 分派單位找不到對應的需求確認單位? 應該不可能發生
            else {
                warnDepSids.add(assignDepSid);
                log.warn("意外!!! reqSid:[{}], assignDepSid:[{}]", requireSid, assignDepSid);
            }
        }

        // ====================================
        // 資訊處理
        // ====================================
        // 新的分派部門清單
        Set<Integer> afterAssignDepSids = Sets.newHashSet();
        // 有調整的需求分派單位檔
        List<RequireDepRebuildTo> updateRequireConfirmDeps = Lists.newArrayList();

        for (Entry<Integer, Set<Integer>> entrySet : assignDepSidsMapByConfirmDepSid.entrySet()) {
            // 需求確認單位 sid
            Integer confirmDepSid = entrySet.getKey();
            // 對應的分派單位 sid
            Set<Integer> assignDepSids = entrySet.getValue();

            // ----------------
            // 為處級以上單位
            // ----------------
            Org confirmDep = WkOrgCache.getInstance().findBySid(confirmDepSid);
            if (confirmDep == null
                    || WkOrgUtils.getOrgLevelOrder(confirmDep.getLevel()) >= DIVISION_LEVEL) {
                // 處級以上單位不用處理
                continue;
            }

            // ----------------
            // 需求確認單位由多個分派單位合成
            // ----------------
            if (assignDepSids.size() > 1) {
                // 減派所有組級, 僅留下部級
                afterAssignDepSids.add(confirmDepSid);
                continue;
            }

            // ----------------
            // 單分派單位所產生的需求確認單位
            // ----------------
            if (assignDepSids.size() == 1) {
                // 取得分派單位
                Org assignDep = WkOrgCache.getInstance().findBySid(Lists.newArrayList(assignDepSids).get(0));

                // ...............
                // 分派單位和確認單位相同時
                // ...............
                // 分派單位和確認單位不同時才需要異動 (有些組織無部級, 故收束後依舊為組)
                if (WkCommonUtils.compareByStr(confirmDepSid, assignDep.getSid())) {
                    afterAssignDepSids.add(assignDep.getSid());
                    continue;
                }

                // ...............
                // 分派單位為組,確認單位為部
                // ...............
                if (assignDep != null && OrgLevel.THE_PANEL.equals(assignDep.getLevel())) {
                    // 分派單位：不異動
                    afterAssignDepSids.add(assignDep.getSid());

                    // 需求確認單位 : update dep sid 為該組 (繼承原需求確認單位所有屬性)
                    RequireDepRebuildTo requireConfirmDep = requireConfirmDepMapByDepSid.get(confirmDepSid);

                    if (requireConfirmDepMapByDepSid.containsKey(assignDep.getSid())) {
                        log.warn("\r\n需求單位已存在!!! reqSid:[{}], assignDepSid:[{}]", requireSid, assignDep.getSid());
                    } else {
                        requireConfirmDep.setDepSid(assignDep.getSid());
                        updateRequireConfirmDeps.add(requireConfirmDep); // 加入待更新清單
                    }

                    continue;
                }

                // ...............
                // 分派單位為部
                // ...............
                // 不會發生
                log.warn("\r\n意外!!! reqSid:[{}], assignDepSid:[{}]", requireSid, assignDep.getSid());
                continue;
            }

            // ----------------
            // 找不到對應單位
            // ----------------
            // 應該不可能發生-錯誤資料?
            if (WkStringUtils.isEmpty(assignDepSids)) {
                // 不做任何處理,在後面重算時，使其自然消失
                log.warn("\r\n分派單位找不到對應資料 reqSid:[{}], confirmDep:[{}]", requireSid, confirmDepSid);
                continue;
            }
        }

        // ====================================
        // 異動需求確認單位
        // ====================================
        if (WkStringUtils.notEmpty(updateRequireConfirmDeps)) {
            for (RequireDepRebuildTo requireDepRebuildTo : updateRequireConfirmDeps) {
                this.requireDepCommonService.updateDepSid(requireDepRebuildTo);
            }
        }

        // ====================================
        // 重建
        // ====================================
        this.assignNoticeService.processForRebuild(
                requireSid,
                requireNo,
                Lists.newArrayList(beforeAssignDepSids),
                Lists.newArrayList(beforeNoticeDepSids),
                Lists.newArrayList(afterAssignDepSids),
                Lists.newArrayList(beforeNoticeDepSids), // 通知單位不會變
                WkUserCache.getInstance().findBySid(WkConstants.ADMIN_USER_SID));

        // ====================================
        // 註記已轉檔
        // ====================================
        this.updateTrased(requireSid);
    }

    private void updateTrased(String requireSid) {
        String sql = "UPDATE tr_require SET trns_flag_for_confirm_dep=1 WHERE require_sid = '" + requireSid + "' ;";
        this.jdbcTemplate.execute(sql);
    }
}
