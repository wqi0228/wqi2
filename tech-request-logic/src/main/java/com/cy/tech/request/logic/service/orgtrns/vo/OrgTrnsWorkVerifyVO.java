package com.cy.tech.request.logic.service.orgtrns.vo;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.cy.system.rest.client.vo.OrgTransMappingTo;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.sp.vo.RoleInquireDepVO;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;

/**
 * 組織異動驗證檔, 查詢結果 VO
 * 
 * @author allen1214_wu
 */
public class OrgTrnsWorkVerifyVO extends OrgTransMappingTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6565391467653143460L;

    /**
     * @param entity
     */
    public OrgTrnsWorkVerifyVO(OrgTransMappingTo orgTransMappingTo) {
        // ====================================
        // 複製所有欄位屬性
        // ====================================
        BeanUtils.copyProperties(orgTransMappingTo, this);

        // ====================================
        // 準備頁面顯示資料
        // ====================================

        // 顯示用:轉換前單位
        this.showBeforeDep = "<span class='WS1-1-3'>[" + this.getBeforeOrgSid() + "]</span> - "
                + WkOrgUtils.findNameByDecorationStyle(this.getBeforeOrgSid(), true);

        // 顯示用:轉換後單位
        this.showAfterDep = "<span class='WS1-1-3'>[" + this.getAfterOrgSid() + "]</span> - "
                + WkOrgUtils.findNameByDecorationStyle(this.getAfterOrgSid(), true);

        // 顯示用:生效日
        this.showEffectiveDate = WkDateUtils.formatDate(this.getEffectiveDate(), WkDateUtils.YYYY_MM_DD);

    }

    /**
     * 顯示用:轉換前單位
     */
    @Getter
    private String showBeforeDep;

    /**
     * 顯示用:轉換前單位
     */
    @Getter
    private String showAfterDep;

    /**
     * 顯示用:生效日
     */
    @Getter
    private String showEffectiveDate;


    @Getter
    @Setter
    private Integer sortSeq;
    
    @Getter
    @Setter
    private List<RoleInquireDepVO> canViewSettingVOs = Lists.newArrayList();

}
