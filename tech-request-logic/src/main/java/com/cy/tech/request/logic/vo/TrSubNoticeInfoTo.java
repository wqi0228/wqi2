/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.vo;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 自訂物件 - 子程序異動記錄
 *
 * @author kasim
 */
@Data
@EqualsAndHashCode(of = {"sid"})
public class TrSubNoticeInfoTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8923128863620752622L;

    private String sid;

    /** 異動人員 */
    private String updatedUser;

    /** 異動前 */
    private String beforeInfo;

    /** 異動後 */
    private String afterInfo;

    /** 異動日期 */
    private String updatedDate;

    public TrSubNoticeInfoTo(String sid, String updatedUser,
            String beforeInfo, String afterInfo, String updatedDate
    ) {
        this.sid = sid;
        this.updatedUser = updatedUser;
        this.beforeInfo = beforeInfo;
        this.afterInfo = afterInfo;
        this.updatedDate = updatedDate;
    }
}
