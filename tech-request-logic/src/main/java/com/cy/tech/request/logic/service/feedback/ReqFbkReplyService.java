/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.feedback;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.helper.systemnotify.SysNotifyHelper;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.repository.require.feedback.ReqFbReplyRepo;
import com.cy.tech.request.vo.require.RequireTrace;
import com.cy.tech.request.vo.require.feedback.ReqFbkReply;
import com.cy.tech.request.vo.require.feedback.ReqFbkReplyAttach;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.vo.AttachmentService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.primitives.Longs;

import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 需求資訊補充回覆服務
 *
 * @author shaun
 */
@Service
@Slf4j
public class ReqFbkReplyService implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 962391354399993362L;
    @Autowired
	private WkJsoupUtils jsoupUtils;
	@Autowired
	private ReqFbReplyRepo fbkReplyDao;
	@Autowired
	@Qualifier("req_fbk_reply_attach")
	private AttachmentService<ReqFbkReplyAttach, ReqFbkReply, String> replyAttachService;
	@Autowired
	private OrganizationService orgService;
	@Autowired
	private SysNotifyHelper sysNotifyHelper;

	@Transactional(readOnly = true)
	public ReqFbkReply findReply(String replySid) {
		if (Strings.isNullOrEmpty(replySid)) {
			return null;
		}
		return fbkReplyDao.findOne(replySid);
	}

	/**
	 * 尋找對應的回覆內容
	 *
	 * @param trace
	 * @return
	 */
	@Transactional(readOnly = true)
	public List<ReqFbkReply> findReplys(RequireTrace trace) {
		if (trace == null || Strings.isNullOrEmpty(trace.getSid())) {
			return Lists.newArrayList();
		}
		try {
			trace.getReplys().size();
		} catch (LazyInitializationException e) {
			// log.debug("findReplys lazy init error :" + e.getMessage(), e);
			trace.setReplys(fbkReplyDao.findByTraceOrderByUpdateDateDesc(trace));
		}
		return trace.getReplys();
	}

	public ReqFbkReply createEmptyReply(RequireTrace trace, User executor) {
		ReqFbkReply reply = new ReqFbkReply();
		reply.setTrace(trace);
		reply.setRequire(trace.getRequire());
		reply.setRequireNo(trace.getRequireNo());
		Org dep = orgService.findBySid(executor.getPrimaryOrg().getSid());
		reply.setDep(dep);
		reply.setPerson(executor);
		reply.setDate(new Date());
		reply.setUpdateDate(new Date());
		return reply;
	}

	/**
	 * 儲存新回覆
	 *
	 * @param reply
	 * @param executor
	 */
	@Transactional(rollbackFor = Exception.class)
	public void saveByNewReply(ReqFbkReply reply, User executor) {
		reply.setContent(jsoupUtils.clearCssTag(reply.getContentCss()));
		reply.setSid(fbkReplyDao.save(reply).getSid());
		replyAttachService.linkRelation(reply.getAttachments(), reply, executor);
		RequireTrace trace = reply.getTrace();
		this.findReplys(trace).add(reply);
		this.sortReplys(trace);

		// ====================================
		// 處理系統通知 (需求回覆補充)
		// ====================================
		try {
			this.sysNotifyHelper.processForAddInfoReply(reply.getRequire().getSid(), reply, executor.getSid());
		} catch (Exception e) {
			log.error("執行系統通知失敗!" + e.getMessage(), e);
		}
	}

	/**
	 * 儲存編輯回覆
	 *
	 * @param editReply
	 * @param executor
	 * @param isAddNotify
	 */
	@Transactional(rollbackFor = Exception.class)
	public void saveByEditReply(ReqFbkReply editReply, User executor, Boolean isAddNotify) {
		editReply.setContent(jsoupUtils.clearCssTag(editReply.getContentCss()));
		editReply.setUpdateDate(new Date());
		fbkReplyDao.save(editReply);
		replyAttachService.findAttachsByLazy(editReply).forEach(each -> each.setKeyChecked(Boolean.TRUE));
		this.sortReplys(editReply.getTrace());
		// ====================================
		// 處理系統通知 (需求回覆補充)
		// ====================================
		if (isAddNotify) {
			try {
				this.sysNotifyHelper.processForAddInfoReply(editReply.getRequire().getSid(), editReply, executor.getSid());

			} catch (Exception e) {
				log.error("執行系統通知失敗!" + e.getMessage(), e);
			}
		}
	}

	/**
	 * 重新排序回覆
	 *
	 * @param trace
	 */
	private void sortReplys(RequireTrace trace) {
		List<ReqFbkReply> replys = this.findReplys(trace);
		Collections.sort(Lists.newArrayList(replys), (r1, r2) -> Longs.compare(r1.getUpdateDate().getTime(), r2.getUpdateDate().getTime()));
	}

}
