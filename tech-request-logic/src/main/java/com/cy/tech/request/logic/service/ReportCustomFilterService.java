/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import com.cy.tech.request.repository.require.ReportCustomFilterRepository;
import com.cy.tech.request.vo.require.ReportCustomFilter;
import java.io.Serializable;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 自訂元件報表元件Service
 *
 * @author brain0925_liao
 */
@Component
public class ReportCustomFilterService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1547839318420730773L;
    /** 自訂元件報表元件Repository */
    @Autowired
    private ReportCustomFilterRepository reportCustomFilterRepository;

    /**
     * 新增自訂元件報表物件
     *
     * @param reportCustomFilter 自訂元件報表物件
     * @param loinUserSid 登入者Sid
     * @return
     */
    public ReportCustomFilter createReportCustomFilter(ReportCustomFilter reportCustomFilter, Integer loinUserSid) {
        reportCustomFilter.setCreate_dt(new Date());
        reportCustomFilter.setCreate_usr(loinUserSid);
        reportCustomFilter = reportCustomFilterRepository.save(reportCustomFilter);
        return reportCustomFilter;
    }

    /**
     * 更新自訂元件報表物件
     *
     * @param reportCustomFilter 自訂元件報表物件
     * @param loinUserSid 登入者Sid
     * @return
     */
    public ReportCustomFilter updateReportCustomFilter(ReportCustomFilter reportCustomFilter, Integer loinUserSid) {
        reportCustomFilter.setUpdate_dt(new Date());
        reportCustomFilter.setUpdate_usr(loinUserSid);
        reportCustomFilter = reportCustomFilterRepository.save(reportCustomFilter);
        return reportCustomFilter;
    }

    /**
     * 取得自訂元件報表物件
     *
     * @param url 報表url
     * @param createUserSid 登入者Sid
     * @return
     */
    public ReportCustomFilter getReportCustomFilterByUserSidAndUrl(String url, Integer createUserSid) {
        return reportCustomFilterRepository.findByUrlAndUserSid(url, createUserSid);
    }
}
