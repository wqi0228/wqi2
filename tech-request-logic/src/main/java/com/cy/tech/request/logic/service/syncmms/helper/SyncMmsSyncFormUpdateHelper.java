/**
 * 
 */
package com.cy.tech.request.logic.service.syncmms.helper;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.logic.helper.systemnotify.SysNotifyHelper;
import com.cy.tech.request.logic.service.RequireCheckItemService;
import com.cy.tech.request.logic.service.RequireProcessCompleteRollbackService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.RequireTraceService;
import com.cy.tech.request.logic.service.TemplateService;
import com.cy.tech.request.logic.service.onpg.OnpgHistoryService;
import com.cy.tech.request.logic.service.onpg.OnpgService;
import com.cy.tech.request.logic.service.onpg.OnpgShowService;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepService;
import com.cy.tech.request.logic.service.syncmms.to.SyncFormTo;
import com.cy.tech.request.logic.vo.WorkOnpgTo;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.log.LogSyncMmsActionType;
import com.cy.tech.request.vo.onpg.WorkOnpg;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgHistoryBehavior;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.template.component.ComBase;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.exception.alert.TechRequestAlertException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.value.to.ItemsCollectionDiffTo;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.cy.work.customer.vo.WorkCustomer;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Slf4j
@Service
public class SyncMmsSyncFormUpdateHelper {

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    transient private SyncMmsSyncFormProcessDispatcher syncMmsCommonLogic;
    @Autowired
    transient private SyncMmsSyncFormCommonHelper syncMmsSyncFormCommonHelper;
    @Autowired
    private transient OnpgService onpgService;
    @Autowired
    private transient OnpgShowService onpgShowService;
    @Autowired
    private transient RequireCheckItemService requireCheckItemService;
    @Autowired
    transient private RequireService requireService;
    @Autowired
    transient private TemplateService templateService;
    @Autowired
    private transient RequireTraceService requireTraceService;
    @Autowired
    private transient OnpgHistoryService onpgHistoryService;
    @Autowired
    private transient RequireProcessCompleteRollbackService requireProcessCompleteRollbackService;
    @Autowired
    private transient SysNotifyHelper sysNotifyHelper;
    @Autowired
    private transient RequireConfirmDepService requireConfirmDepService;

    @PersistenceContext
    private transient EntityManager entityManager;
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private transient JdbcTemplate jdbcTemplate;

    // ========================================================================
    // 主方法
    // ========================================================================
    /**
     * @param syncFormTo
     * @throws SystemOperationException
     */
    public void process(SyncFormTo syncFormTo) throws SystemOperationException {

        // isFromMms 已經在準備欄位時比對
        // if(!syncFormTo.getWorkOnpgTo().isFromMms()) {}

        Date sysDate = new Date();

        // ====================================
        // 檢核單據狀態
        // ====================================
        // 檢核
        String canNotEditReason = onpgShowService.verifyCanEditByToStatus(
                syncFormTo.getRequire(),
                syncFormTo.getWorkOnpgTo(),
                true);
        // 無法編輯
        if (WkStringUtils.notEmpty(canNotEditReason)) {
            // 註記無法編輯原因
            syncFormTo.addErrorInfo(canNotEditReason);;
            return;
        }

        // ====================================
        // 整裡需要異動的欄位
        // ====================================
        ModifyData modifyData = this.prepareModifyData(syncFormTo);

        // 檢查沒有需要異動的欄位
        if (WkStringUtils.isEmpty(modifyData.modifyInfos)) {
            // 註記類型為無需異動
            syncFormTo.setLogSyncMmsActionType(LogSyncMmsActionType.UPDATE_NONE);
            return;
        }

        syncFormTo.setModifyInfos(modifyData.modifyInfos);

        syncFormTo.setLogSyncMmsActionType(LogSyncMmsActionType.UPDATE_ONPG);

        // ====================================
        // 異動『需求單』資料
        // ====================================
        // 更新需求單資料
        Require afterRequire = this.updateRequire(
                syncFormTo.getRequire().getSid(),
                modifyData,
                syncFormTo.getCreateUser(),
                syncFormTo.getLoginCompID());

        if (afterRequire != null) {
            syncFormTo.setRequire(afterRequire);
        }

        // 是否將期望完成日延後
        // 判斷維護日期是否 > 期望日期
        boolean isPostponeRequireHopeDate = WkDateUtils.isAfter(
                syncFormTo.getMaintenanceDate(), syncFormTo.getRequire().getHopeDate(), false);

        // 註記異動類型
        if (afterRequire != null || isPostponeRequireHopeDate) {
            syncFormTo.setLogSyncMmsActionType(LogSyncMmsActionType.UPDATE_REQ_AND_ONPG);
        }

        // 異動主單期望完成日
        if (isPostponeRequireHopeDate) {
            this.syncMmsSyncFormCommonHelper.postponeRequireHopeDate(
                    syncFormTo, 
                    syncFormTo.getWorkOnpgTo().getOnpgNo());
        }

        // ====================================
        // 異動『ON程式單』資料
        // ====================================
        // 更新ON程式單資料
        WorkOnpg afterWorkOnpg = this.updateOnpg(
                syncFormTo.getWorkOnpgTo().getOnpgNo(),
                modifyData,
                syncFormTo.getCreateUser(),
                syncFormTo.getLoginCompID());

        if (afterWorkOnpg != null) {
            syncFormTo.setWorkOnpgTo(onpgService.convert2To(afterWorkOnpg));
        }

        // ====================================
        // 寫需求單追蹤
        // ====================================
        this.requireTraceService.createSyncMmsTrace(syncFormTo);

        // ====================================
        // 再次開啟執行單位的【需求完成-確認流程】 (若需要開啟的話)
        // ====================================
        // 此程序要在倒回製作進度之前，否則單位異動記錄會不對
        this.requireConfirmDepService.reOpenProgStatusByUpdateSubCase(
                syncFormTo.getRequire().getRequireNo(),
                syncFormTo.getRequire().getSid(),
                syncFormTo.getCreateUser().getSid(),
                afterWorkOnpg.getCreateDep().getSid(),
                "因MMS同步，異動ON程式單:" + afterWorkOnpg.getOnpgNo(),
                sysDate);

        // ====================================
        // 若需求製作進度為已完成, 將進度倒回進行中
        // ====================================
        this.requireProcessCompleteRollbackService.executeProcess(
                syncFormTo.getRequire().getSid(),
                syncFormTo.getCreateUser(),
                sysDate,
                "由MMS同步異動資料");

    }

    /**
     * 準備有異動的欄位
     * 
     * @param syncFormTo
     * @return
     */
    private ModifyData prepareModifyData(SyncFormTo syncFormTo) {
        Require oldRequire = syncFormTo.getRequire();
        WorkOnpgTo oldWorkOnpg = syncFormTo.getWorkOnpgTo();

        // ====================================
        // 檢查要異動的欄位
        // ====================================
        ModifyData modifyData = new ModifyData();

        // -----------------
        // 廳主
        // -----------------
        if (syncFormTo.getWorkCustomer() != null) {
            WorkCustomer oldWorkCustomer = oldRequire.getCustomer();
            WorkCustomer newWorkCustomer = syncFormTo.getWorkCustomer();
            if (oldWorkCustomer == null // 廳主為空, 應該不可能 (防呆)
                    || !WkCommonUtils.compareByStr( // 比對前後廳主不一樣
                            oldWorkCustomer.getSid(),
                            newWorkCustomer.getSid())) {

                // 廳主需要被異動
                modifyData.requireWorkCustomer = syncFormTo.getWorkCustomer();

                // 記錄異動資訊
                modifyData.modifyInfos.add(
                        String.format(
                                "%s【%s】->【%s】",
                                WkHtmlUtils.addPurpleBlodClass("廳主："),
                                (oldWorkCustomer == null ? "無" : (oldWorkCustomer.getName() + "(" + oldWorkCustomer + ")")),
                                newWorkCustomer + "(" + newWorkCustomer.getSid() + ")"));
            }
        }

        // -----------------
        // 系統別
        // -----------------
        // 查詢目前單據的系統別
        List<RequireCheckItemType> oldCheckItemTypes = this.requireCheckItemService.findCheckItemTypesByRequireSid(
                oldRequire.getSid());
        modifyData.oldRequireCheckItemTypes = oldCheckItemTypes;

        if (syncFormTo.getCheckItemTypes() != null) {

            // 新的系統別
            List<RequireCheckItemType> newCheckItemTypes = syncFormTo.getCheckItemTypes();

            // 比對是否需要異動
            if (!WkCommonUtils.compare(oldCheckItemTypes, newCheckItemTypes)) {
                // 系統別需被異動
                modifyData.requireCheckItemTypes = newCheckItemTypes;

                // 記錄異動資訊
                modifyData.modifyInfos.add(
                        String.format(
                                "%s【%s】->【%s】",
                                WkHtmlUtils.addPurpleBlodClass("系統別："),
                                (WkStringUtils.isEmpty(oldCheckItemTypes) ? "無" : oldCheckItemTypes.stream().map(RequireCheckItemType::name).collect(Collectors.joining("、"))),
                                newCheckItemTypes.stream().map(RequireCheckItemType::name).collect(Collectors.joining("、"))));

            }
        }

        // -----------------
        // 是否通知市場
        // -----------------
        // 取得目前的系統別
        Set<Integer> oldNoticeDepSids = oldWorkOnpg.getNoticeDepSids();
        // 取得市場單位
        Set<Integer> marketDepSids = SyncMmsParamHelper.getInstance().findMmsSyncNoticeMarketDepSids();

        // 依據『是否通知市場』選項，組出新的通知單位清單
        // 複製一份
        Set<Integer> tempNewNoticeDepSids = oldNoticeDepSids.stream().collect(Collectors.toSet());
        // 新增或取消
        if (WkStringUtils.notEmpty(marketDepSids)) {
            if (syncFormTo.isNoticeMarket()) {
                // 移除停用單位
                marketDepSids = marketDepSids.stream()
                        .filter(depSid -> WkOrgUtils.isActive(depSid))
                        .collect(Collectors.toSet());
                // 需通知市場單位
                tempNewNoticeDepSids.addAll(marketDepSids);
            } else {
                // 取消通知市場單位
                tempNewNoticeDepSids.removeAll(marketDepSids);
            }
        }

        // 比對是否需要異動
        if (!WkCommonUtils.compare(oldNoticeDepSids, tempNewNoticeDepSids)) {
            // 通知單位需被異動
            modifyData.onpgNoticeDepSids = tempNewNoticeDepSids;

            // 記錄異動資訊
            String modifyInfo = "";
            modifyInfo += "通知市場單位: (" + ("1".equals(syncFormTo.getNoticeMarket_src()) ? "是" : "否") + ")<br/>";
            // 比對
            ItemsCollectionDiffTo<Integer> diffTo = WkCommonUtils.itemCollectionDiff(oldNoticeDepSids, modifyData.onpgNoticeDepSids);
            if (WkStringUtils.notEmpty(diffTo.getPlusItems())) {
                modifyInfo += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;新增：[" + WkOrgUtils.findNameBySid(diffTo.getPlusItems(), "、") + "]";
            }
            if (WkStringUtils.notEmpty(diffTo.getReduceItems())) {
                modifyInfo += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;移除[" + WkOrgUtils.findNameBySid(diffTo.getReduceItems(), "、") + "]";
            }
            modifyData.modifyInfos.add(modifyInfo);
        }

        // -----------------
        // 主題
        // -----------------
        String oldTheme = WkStringUtils.safeTrim(oldWorkOnpg.getTheme());
        String newTheme = WkStringUtils.safeTrim(syncFormTo.getTheme());
        if (!WkCommonUtils.compareByStr(oldTheme, newTheme)) {

            // 新的ON程式主題
            modifyData.onpgTheme = newTheme;

            // 記錄異動資訊
            modifyData.modifyInfos.add(
                    String.format(
                            "%s【%s】->【%s】",
                            WkHtmlUtils.addPurpleBlodClass("主題："),
                            oldTheme,
                            newTheme));

            // 僅存在一筆ON程式單時，一起異動主單主題
            // if (this.onpgService.countActiveByRequireNo(oldRequire.getRequireNo()) == 1) {
            // }
        }

        // -----------------
        // 內容
        // -----------------
        // 去 HTML 格式後再比對
        if (!this.syncMmsSyncFormCommonHelper.compareHtmlContent(oldWorkOnpg.getContentCss(), syncFormTo.getContent())) {

            // 新的ON程式單 內容 (HTML 格式)
            modifyData.onpgCentent = syncFormTo.getContent();

            // 記錄異動資訊

            // 若僅第一行有異動，不寫異動記錄 (第一行固定為異動日期+title)
            if (!this.syncMmsSyncFormCommonHelper.compareHtmlContentWithoutFirstLine(oldWorkOnpg.getContentCss(), syncFormTo.getContent())) {
                modifyData.modifyInfos.add(
                        String.format(
                                "%s 有被異動",
                                WkHtmlUtils.addPurpleBlodClass("內容：")));
            }

            // 僅存在一筆ON程式單時，一起異動主單內容
            if (this.onpgService.countActiveByRequireNo(oldRequire.getRequireNo()) == 1) {
                modifyData.requireCssCentent = modifyData.onpgCentent;
            }
        }

        // -----------------
        // 預計完成日
        // -----------------
        Date oldOnpgEstablishDate = oldWorkOnpg.getEstablishDate();
        Date newOnpgEstablishDate = syncFormTo.getMaintenanceDate();
        if (!WkCommonUtils.compareDateTime(
                oldOnpgEstablishDate,
                newOnpgEstablishDate)) {

            modifyData.onpgEstablishDate = newOnpgEstablishDate;

            // 記錄異動資訊
            modifyData.modifyInfos.add(
                    String.format(
                            "%s【%s】->【%s】",
                            WkHtmlUtils.addPurpleBlodClass("預計完成日："),
                            WkDateUtils.formatDate(oldOnpgEstablishDate, WkDateUtils.YYYY_MM_DD2),
                            WkDateUtils.formatDate(newOnpgEstablishDate, WkDateUtils.YYYY_MM_DD2)));
        }

        // ====================================
        // 僅存在一筆ON程式單時，一起異動主單
        // ====================================
        // 檢查與主單連動的欄位
        if (WkStringUtils.notEmpty(modifyData.onpgTheme)
                || WkStringUtils.notEmpty(modifyData.onpgCentent)
                || modifyData.onpgEstablishDate != null) {

            // 檢查僅存在一筆ON程式單
            if (this.onpgService.countActiveByRequireNo(oldRequire.getRequireNo()) == 1) {
                // 主題
                modifyData.requireTheme = modifyData.onpgTheme;
                // 內容
                modifyData.requireCssCentent = modifyData.onpgCentent;
                // 預計完成日 (改大於時才調整)
                // modifyData.requireEstablishDate = modifyData.onpgEstablishDate;
            }
        }

        return modifyData;
    }

    // ========================================================================
    // 內部方法
    // ========================================================================
    /**
     * 更新需求單資料
     * 
     * @param requireSid
     * @param modifyData
     * @param createUser
     * @param loginCompId
     * @return
     * @throws SystemOperationException
     */
    private Require updateRequire(
            String requireSid,
            ModifyData modifyData,
            User createUser,
            String loginCompId) throws SystemOperationException {

        // ====================================
        // 判斷沒有需要異動的欄位
        // ====================================
        // 判斷異動資料皆為空，無需異動
        if (modifyData.requireWorkCustomer == null
                && modifyData.requireCheckItemTypes == null
                && WkStringUtils.isEmpty(modifyData.requireTheme)
                && WkStringUtils.isEmpty(modifyData.requireCssCentent)) {
            return null;
        }

        // ====================================
        // 重新取得需求單主檔
        // ====================================
        Require editRequire = this.requireService.findByReqSid(requireSid);

        // ====================================
        // 準備異動資料
        // ====================================
        // 取得設定模版內容值
        Map<String, ComBase> comValueMap = this.templateService.loadTemplateFieldValue(editRequire);
        // 主題
        if (WkStringUtils.notEmpty(modifyData.requireTheme)) {
            this.syncMmsCommonLogic.setRequireTheme(comValueMap, modifyData.requireTheme, loginCompId);
        }

        // 內容
        if (WkStringUtils.notEmpty(modifyData.requireCssCentent)) {
            this.syncMmsCommonLogic.setRequireContent(comValueMap, modifyData.requireCssCentent, loginCompId);
        }

        // 廳主
        if (modifyData.requireWorkCustomer != null) {
            editRequire.setCustomer(modifyData.requireWorkCustomer);
            editRequire.setAuthor(modifyData.requireWorkCustomer);
        }

        // ====================================
        // 判斷沒有需要異動的欄位
        // ====================================
        Require bakRequire = this.requireService.findByReqSid(requireSid);

        try {
            return this.requireService.updateWhitoutTransactional(
                    createUser,
                    editRequire,
                    modifyData.requireCheckItemTypes == null ? modifyData.oldRequireCheckItemTypes : modifyData.requireCheckItemTypes,
                    bakRequire,
                    comValueMap);
        } catch (UserMessageException e) {
            log.error(e.getMessage(), e);
            throw new TechRequestAlertException(e.getMessage(), loginCompId);
        }
    }

    /**
     * 更新ON程式單資料
     * 
     * @param onpgNo
     * @param modifyData
     * @param createUser
     * @param loginCompId 操作者公司別 (可為空)
     * @return
     * @throws SystemOperationException
     */
    private WorkOnpg updateOnpg(
            String onpgNo,
            ModifyData modifyData,
            User createUser,
            String loginCompId) throws SystemOperationException {

        // ====================================
        // 判斷沒有需要異動的欄位
        // ====================================
        // 判斷異動資料皆為空，無需異動
        if (WkStringUtils.isEmpty(modifyData.onpgTheme)
                && WkStringUtils.isEmpty(modifyData.onpgCentent)
                && modifyData.onpgEstablishDate == null
                && modifyData.onpgNoticeDepSids == null) {
            return null;
        }

        // ====================================
        // 重新取得需求單主檔
        // ====================================
        WorkOnpg editOnpg = this.onpgService.findByOnpgNo(onpgNo);

        if (!editOnpg.isFromMms()) {
            String errorMessage = "ON程式單[" + onpgNo + "]不是由 MMS 建立，無法更新資料！";
            log.error(errorMessage);
            throw new TechRequestAlertException(errorMessage, loginCompId);
        }

        // ====================================
        // 準備異動資料
        // ====================================
        // 主題
        if (WkStringUtils.notEmpty(modifyData.onpgTheme)) {
            editOnpg.setTheme(modifyData.onpgTheme);
        }
        // 內容
        if (WkStringUtils.notEmpty(modifyData.onpgCentent)) {
            editOnpg.setContentCss(modifyData.onpgCentent);
        }
        // 通知單位
        if (WkStringUtils.notEmpty(modifyData.onpgNoticeDepSids)) {
            JsonStringListTo noticeDeps = new JsonStringListTo();
            noticeDeps.setValue(modifyData.onpgNoticeDepSids.stream()
                    .map(depSid -> depSid + "")
                    .collect(Collectors.toList()));
            editOnpg.setNoticeDeps(noticeDeps);
        }

        // 預計完成日
        if (modifyData.onpgEstablishDate != null) {
            editOnpg.setEstablishDate(modifyData.onpgEstablishDate);
        }

        // ====================================
        // 異動資料
        // ====================================
        editOnpg = this.onpgService.saveEdit(
                this.requireService.findByReqSid(editOnpg.getSourceSid()),
                editOnpg,
                createUser);

        // ====================================
        // 寫內容異動記錄
        // ====================================
        String content = "";
        content += WkHtmlUtils.addBlueBlodClass("【MMS同步】");
        content += "<br/><br/>";
        content += modifyData.modifyInfos.stream()
                .collect(Collectors.joining("<br/>"));

        this.onpgHistoryService.createCanViewWorkOnpgHistory(
                editOnpg.getOnpgNo(),
                WorkOnpgHistoryBehavior.MMS_UPDATE,
                content,
                createUser);

        // ====================================
        // 重新開啟ON程式單和需求單 (如果需要的話)
        // ====================================
        this.reopenOnpg(
                editOnpg.getOnpgNo(),
                createUser);

        return editOnpg;
    }

    /**
     * 重新開啟ON程式單 (如果需要的話)
     * 
     * @param onpgNo     ONPG單號
     * @param createUser 使用者
     * @throws SystemOperationException 錯誤時拋出
     */
    private void reopenOnpg(
            String onpgNo,
            User createUser) throws SystemOperationException {

        // ====================================
        // 查詢最新的ON程式單資料
        // ====================================
        WorkOnpg editOnpg = this.onpgService.findByOnpgNo(onpgNo);

        // ====================================
        // 判斷：若狀態不是『已ON上時，才需要處理』
        // ====================================
        if (WorkOnpgStatus.ALREADY_ON.equals(editOnpg.getOnpgStatus())) {
            return;
        }

        // 記錄原來的狀態
        WorkOnpgStatus oldWorkOnpgStatus = editOnpg.getOnpgStatus();

        // ====================================
        // 初始化主單狀態為已ON上, 讓GM重新檢查
        // ====================================
        this.jdbcTemplate.execute(""
                + "UPDATE work_onpg "
                + "   SET onpg_status = '" + WorkOnpgStatus.ALREADY_ON + "' "
                + " WHERE onpg_sid = '" + editOnpg.getSid() + "'  ");

        this.entityManager.refresh(editOnpg);

        // ====================================
        // 寫異動記錄
        // ====================================
        String content = "";
        content += WkHtmlUtils.addBlueBlodClass("【MMS同步】");
        content += "<br/><br/>";
        content += "復原ON程式單狀態";
        content += "<br/><br/>";
        content += "[" + oldWorkOnpgStatus.getLabel() + "] -> [" + WorkOnpgStatus.ALREADY_ON.getLabel() + "]";

        this.onpgHistoryService.createCanViewWorkOnpgHistory(
                editOnpg.getOnpgNo(),
                WorkOnpgHistoryBehavior.MMS_CHANGE_STATUS,
                content,
                createUser);

        // ====================================
        // ON程式通知
        // ====================================
        if (oldWorkOnpgStatus.isNeedNoticeWhenUpdateFromMmsSync()) {

            try {
                this.sysNotifyHelper.processForAddOnpg(
                        editOnpg.getSourceSid(),
                        editOnpg.getTheme(),
                        null, // 新增時，無異動前資料
                        (editOnpg.getNoticeDeps() != null) ? editOnpg.getNoticeDeps().getValue() : Lists.newArrayList(),
                        createUser.getSid());
            } catch (Exception e) {
                log.error("執行系統通知失敗!" + e.getMessage(), e);
            }
        }

    }

    // ========================================================================
    // 私有 bean
    // ========================================================================
    /**
     * @author allen1214_wu
     */
    private class ModifyData {
        /**
         * 需求單:廳主
         */
        public WorkCustomer requireWorkCustomer = null;
        /**
         * 需求單:系統別
         */
        public List<RequireCheckItemType> oldRequireCheckItemTypes = null;
        /**
         * 需求單:系統別
         */
        public List<RequireCheckItemType> requireCheckItemTypes = null;
        /**
         * 需求單:主題
         */
        public String requireTheme = "";
        /**
         * 需求單:內容
         */
        public String requireCssCentent = "";
        /**
         * ON程式單:主題
         */
        public String onpgTheme = "";
        /**
         * ON程式單:內容
         */
        public String onpgCentent = "";
        /**
         * ON程式單:通知單位 org sid
         */
        public Set<Integer> onpgNoticeDepSids = null;

        /**
         * ON程式單：預計完成日
         */
        public Date onpgEstablishDate;

        /**
         * 異動欄位說明
         */
        public List<String> modifyInfos = Lists.newArrayList();
    }

}
