/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.LazyInitializationException;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.helper.RequireIndexHelper;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.require.AlertInbox;
import com.cy.tech.request.vo.require.AlertInboxSendGroup;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireIndexDictionary;
import com.cy.work.common.utils.WkJsonUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class SearchService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5424748047878460178L;
    @Autowired
    private WkJsonUtils jsonUtils;
    @Autowired
    private RequireIndexHelper idxHelper;

    /**
     * 查詢啟始日的時間調整為00-00-00
     *
     * @param startDate
     * @return
     */
    public Date transStartDate(Date startDate) {
        return new LocalDate(startDate).toDate();
    }

    /**
     * 查詢結束日的時間調整為23-59-59
     *
     * @param endDate
     * @return
     */
    public Date transEndDate(Date endDate) {
        DateTime endTime = new DateTime(new LocalDate(endDate).plusDays(1).toDate()).minusMillis(1);
        return endTime.toDate();
    }

    /**
     * 每個月的第一天日期
     *
     * @param calendar
     * @return
     */
    public Date getFirstMonthDay(Calendar calendar) {
        calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DATE));
        return calendar.getTime();
    }

    /**
     * 每個月的最後一天日期
     *
     * @param calendar
     * @return
     */
    public Date getLastMonthDay(Calendar calendar) {
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        return calendar.getTime();
    }

    public Date addDay(Calendar calendar, int dayCnt) {
        calendar.add(Calendar.DATE, dayCnt);
        return calendar.getTime();
    }

    /**
     * 建立主題快取 Key = 需求單號
     *
     * @param requires
     * @return
     */
    public Map<String, String> createThemeCache(List<Require> requires) {
        Map<String, String> cache = Maps.newHashMap();
        if (requires == null || requires.isEmpty()) {
            return cache;
        }
        List<RequireIndexDictionary> result = idxHelper.findIndexByRequiresAndFieldName(requires, "主題");
        for (RequireIndexDictionary each : result) {
            if (!cache.containsKey(each.getRequireNo())) {
                cache.put(each.getRequireNo(), this.combineFromJsonStr(each.getFieldContent()));
            }
        }
        return cache;
    }

    public Map<String, String> createThemeCacheFromInbox(List<AlertInbox> inboxs) {
        List<Require> requires = Lists.newArrayList();
        for (AlertInbox each : inboxs) {
            if (!requires.contains(each.getRequire())) {
                requires.add(each.getRequire());
            }
        }
        return this.createThemeCache(requires);
    }

    public Map<String, String> createThemeCacheFromInboxSendGroups(List<AlertInboxSendGroup> groups) {
        List<Require> requires = Lists.newArrayList();
        for (AlertInboxSendGroup each : groups) {
            if (!requires.contains(each.getRequire())) {
                requires.add(each.getRequire());
            }
        }
        return this.createThemeCache(requires);
    }

    /**
     * 取得主題字串
     *
     * @param r
     * @return
     */
    public String getThemeStr(Require r) {
        String text = "主題";
        try {
            r.getIndex().size();
        } catch (LazyInitializationException e) {
            // log.debug("getThemeStr lazy init error :" + e.getMessage());
            r.setIndex(idxHelper.findByRequire(r));
        }
        for (RequireIndexDictionary index : r.getIndex()) {
            if (index.getFieldName().contains(text)) {
                return this.combineFromJsonStr(index.getFieldContent());
            }
        }
        return "";
    }

    /**
     * @param fieldContent JSON 格式字串
     * @return
     */
    public String combineFromJsonStr(String fieldContent) {
        if (Strings.isNullOrEmpty(fieldContent)) {
            return "";
        }
        try {
            return String.join(" ", jsonUtils.fromJsonToList(fieldContent, String.class));
        } catch (IOException ex) {
            log.trace("非json格式, 回傳原值 : {}", fieldContent);
            return fieldContent;
        }
    }

    public String getCashStr(Require r) {
        String text = "是否收費";
        try {
            r.getIndex().size();
        } catch (LazyInitializationException e) {
            // log.debug("getCashStr lazy init error :" + e.getMessage(), e);
            r.setIndex(idxHelper.findByRequire(r));
        }
        for (RequireIndexDictionary index : r.getIndex()) {
            if (index.getFieldName().equalsIgnoreCase(text)) {
                return this.getAmountCashContent(index);
            }
        }
        return "";
    }

    private String getAmountCashContent(RequireIndexDictionary index) {
        try {
            List<String> amountInfo = jsonUtils.fromJsonToList(index.getFieldContent(), String.class);
            // amountInfo.get(0) 是布林值 ； amountInfo.get(1) 才是金額
            return amountInfo.get(1);
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
            return index.getFieldContent();
        }
    }

    public Map<String, String> createAmountCashCache(List<Require> requires) {
        Map<String, String> cache = Maps.newHashMap();
        if (requires == null || requires.isEmpty()) {
            return cache;
        }
        List<RequireIndexDictionary> result = idxHelper.findIndexByRequiresAndFieldName(requires, "是否收費");
        for (RequireIndexDictionary each : result) {
            if (!cache.containsKey(each.getRequireNo())) {
                cache.put(each.getRequireNo(), this.getAmountCashContent(each));
            }
        }
        return cache;
    }

    /**
     * @param user
     * @return
     */
    public String createReadRecordJsonSearchStr(User user) {
        // $.records.4173.type
        if (user == null || user.getSid() == null) {
            return "$.naverfind";
        }
        return "$.records." + user.getSid() + ".type";
    }

    /**
     * 計算執行天數
     * 
     * @param requireStatusType    製作進度狀態
     * @param requireEstablishDate 需求成立日
     * @param requireFinishDate    需求完成日
     * @return
     */
    public int calcExecDays(
            RequireStatusType requireStatusType,
            Date requireEstablishDate,
            Date requireFinishDate) {

        if (requireStatusType == null) {
            log.warn("製作進度為空!");
            return 0;
        }

        // 以下狀態單據還未開始執行，直接回傳0天
        List<RequireStatusType> passTypes = Lists.newArrayList(
                RequireStatusType.DRAFT,
                RequireStatusType.NEW_INSTANCE,
                RequireStatusType.WAIT_CHECK,
                RequireStatusType.INVALID,
                RequireStatusType.SUSPENDED,
                RequireStatusType.ROLL_BACK_NOTIFY);

        if (passTypes.contains(requireStatusType)) {
            return 0;
        }

        // 需求起始日為空, 無法計算
        if (requireEstablishDate == null) {
            log.warn("製作進度:[{}], 但需求成立日(require_establish_dt)為空, 需重新確認邏輯!", requireStatusType);
            return 0;
        }

        // 以下狀態使用『需求完成日』
        List<RequireStatusType> targetTypes = Lists.newArrayList(
                RequireStatusType.COMPLETED,
                RequireStatusType.CLOSE,
                RequireStatusType.AUTO_CLOSED);

        // 截止計算日
        // 已完成則使用需求完成日計算, 否則為系統日
        Date endDate = targetTypes.contains(requireStatusType) ? requireFinishDate : new Date();

        // 需求完成日為空
        if (endDate == null) {
            // 開發警告
            if (requireFinishDate == null) {
                // log.warn("製作進度:[{}], 但需求完成日(require_finish_dt)為空, 需重新確認邏輯!", requireStatusType);
            }
            return 0;
        }

        // 回傳 需求完成日 - 截止計算日
        int days = Days.daysBetween(
                new DateTime(this.transStartDate(requireEstablishDate)),
                new DateTime(this.transEndDate(endDate)))
                .getDays();

        // 未滿一天算一天
        if (days == 0) {
            days = 1;
        }

        return days;
    }

}
