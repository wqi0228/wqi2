/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.service.tros;

import com.cy.tech.request.repository.require.tros.TrOsReplyAndReplyRepository;
import com.cy.tech.request.repository.require.tros.TrOsReplyRepository;
import com.cy.tech.request.vo.require.tros.TrOsReply;
import com.cy.tech.request.vo.require.tros.TrOsReplyAndReply;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author brain0925_liao
 */
@Component
public class TrOsReplyService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7139762278497264112L;
    /** TrOsReplyRepository */
    @Autowired
    private TrOsReplyRepository trOsReplyRepository;
    /** TrOsReplyAndReplyRepository */
    @Autowired
    private TrOsReplyAndReplyRepository trOsReplyAndReplyRepository;

    /**
     * 取得TrOsReply by os_reply_sid
     *
     * @param os_reply_sid 回覆Sid
     * @return
     */
    public TrOsReply getTrOsReplyBySid(String os_reply_sid) {
        return trOsReplyRepository.findOne(os_reply_sid);
    }

    /**
     * 取得TrOsReplyAndReply by os_reply_and_reply_sid
     *
     * @param os_reply_and_reply_sid 回覆的回覆Sid
     * @return
     */
    public TrOsReplyAndReply getTrOsReplyAndReplyBySid(String os_reply_and_reply_sid) {
        return trOsReplyAndReplyRepository.findOne(os_reply_and_reply_sid);
    }
}
