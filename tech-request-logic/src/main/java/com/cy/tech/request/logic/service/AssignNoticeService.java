package com.cy.tech.request.logic.service;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.helper.systemnotify.SysNotifyHelper;
import com.cy.tech.request.logic.service.helper.AssignSendSearchHelper;
import com.cy.tech.request.logic.service.onpg.OnpgService;
import com.cy.tech.request.logic.service.pt.PtService;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmAddReasonType;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepService;
import com.cy.tech.request.logic.service.send.test.SendTestService;
import com.cy.tech.request.logic.service.setting.SettingDefaultAssignSendDepService;
import com.cy.tech.request.logic.service.tros.TrOsService;
import com.cy.tech.request.logic.vo.AssignNoticeProcessTo;
import com.cy.tech.request.logic.vo.PtCheckTo;
import com.cy.tech.request.logic.vo.RequireConfirmDepVO;
import com.cy.tech.request.logic.vo.TrOsVO;
import com.cy.tech.request.logic.vo.WorkOnpgTo;
import com.cy.tech.request.logic.vo.WorkTestInfoTo;
import com.cy.tech.request.repository.require.AssignSendInfoHistoryRepository;
import com.cy.tech.request.repository.require.AssignSendInfoRepo;
import com.cy.tech.request.repository.require.RequireRepository;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.tech.request.vo.enums.ReqConfirmDepProgStatus;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.require.AssignSendInfo;
import com.cy.tech.request.vo.require.AssignSendInfoHistory;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.value.to.ItemsCollectionDiffTo;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Service
@NoArgsConstructor
@Slf4j
public class AssignNoticeService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7689382655368978973L;
    @Autowired
    private transient AssignSendInfoHistoryRepository assignSendInfoHistoryRepo;
    @Autowired
    private transient AssignSendInfoRepo assignSendInfoRepo;
    @Autowired
    private transient AssignSendInfoService assignSendInfoService;
    @Autowired
    private transient AssignSendSearchHelper assignSendSearchHelper;
    @Autowired
    private transient FogbugzService fbService;
    @Autowired
    private transient OnpgService onpgService;
    @Autowired
    private transient PtService ptService;
    @Autowired
    private transient RequireRepository requireRepository;
    @Autowired
    private transient RequireShowService reqShowService;
    @Autowired
    private transient RequireConfirmDepService requireConfirmDepService;
    @Autowired
    private transient RequireProcessCompleteRollbackService requireProcessCompleteRollbackService;
    @Autowired
    private transient RequireProcessCompleteService requireProcessCompleteService;
    @Autowired
    private transient RequireService requireService;
    @Autowired
    private transient SendTestService sendTestService;
    @Autowired
    private transient TrOsService trOsService;
    @Autowired
    private transient WkOrgCache wkOrgCache;
    @Autowired
    private transient WkUserCache wkUserCache;
    @Autowired
    private transient RequireTraceService requireTraceService;
    @Autowired
    private transient WorkBackendParamHelper workBackendParamHelper;
    @Autowired
    private transient SysNotifyHelper sysNotifyHelper;
    @Autowired
    private transient SettingDefaultAssignSendDepService settingDefaultAssignSendDepService;
    @Autowired
    private transient RequireCheckItemService requireCheckItemService;
    @Autowired
    private transient RequireReadRecordHelper requireReadRecordHelper;

    /**
     * 檢查有效時間
     * 
     * @param info
     * @param effectiveTime
     * @return
     */
    private boolean checkEffectiveTime(AssignSendInfo info, Long effectiveTime) {
        if (info != null && info.getCreatedDate() != null && info.getCreatedDate().getTime() > effectiveTime) {
            return false;
        }
        return true;
    }

    @Transactional(rollbackFor = Exception.class)
    private void doProcess(
            String requireSid,
            String requireNo,
            User execUser,
            List<Integer> afterAssignDepSids,
            List<Integer> afterNoticeDepSids,
            String modifyDepMessagePrefix,
            Date execDate,
            RequireConfirmAddReasonType addReasonType,
            String autoAssignDesc) throws UserMessageException {
        this.doProcess(
                requireSid,
                requireNo,
                execUser,
                afterAssignDepSids,
                afterNoticeDepSids,
                modifyDepMessagePrefix,
                execDate,
                addReasonType,
                autoAssignDesc,
                false, // 非批次執行
                null, // 非批次執行不傳
                null// 非批次執行不傳
        );
    }

    /**
     * 處理分派相關流程<br/>
     * 注意：調整時, 需同步調整組織異動轉檔功能!
     * 
     * @param requireSid             需求單主檔 sid
     * @param requireNo              需求單號
     * @param execUser               執行者
     * @param afterAssignDepSids     新的分派單位 SID
     * @param afterNoticeDepSids     新的通知單位 SID
     * @param modifyDepMessagePrefix 異動記錄文字前綴
     * @param execDate               執行的系統時間
     * @param addReasonType          新增需求確認單位的原因 (動作來源)
     * @param autoAssignDesc         (RequireConfirmAddReasonType = AUTO_ASSIGN 時,
     *                                   需輸入說明)
     * @throws UserMessageException 異常訊息
     */
    @Transactional(rollbackFor = Exception.class)
    private void doProcess(
            String requireSid,
            String requireNo,
            User execUser,
            List<Integer> afterAssignDepSids,
            List<Integer> afterNoticeDepSids,
            String modifyDepMessagePrefix,
            Date execDate,
            RequireConfirmAddReasonType addReasonType,
            String autoAssignDesc,
            boolean isBatch,
            List<Integer> beforeAssignDepSids,
            List<Integer> beforeNoticeDepSids) throws UserMessageException {

        boolean isShowDebug = !isBatch;
        boolean isDisableNotice = isBatch;

        // ====================================
        // 查詢舊的分派/通知單位資訊
        // ====================================
        if (!isBatch) {
            // 查詢
            Map<AssignSendType, AssignSendInfo> settingMap = this.assignSendInfoService.findByRequireSidAndStatus(requireSid);

            // 取得原分派單位設定
            beforeAssignDepSids = Lists.newArrayList();
            if (settingMap.containsKey(AssignSendType.ASSIGN)) {
                beforeAssignDepSids = this.assignSendInfoService.prepareDeps(
                        settingMap.get(AssignSendType.ASSIGN));
            }

            // 取得原通知單位設定
            beforeNoticeDepSids = Lists.newArrayList();
            if (settingMap.containsKey(AssignSendType.SEND)) {
                beforeNoticeDepSids = this.assignSendInfoService.prepareDeps(
                        settingMap.get(AssignSendType.SEND));
            }
        }

        // ====================================
        // 檢查無異動
        // ====================================
        // 分派單位異動前後是否相同
        boolean isAssignDepTheSame = WkCommonUtils.compare(beforeAssignDepSids, afterAssignDepSids);
        // 通知單位異動前後是否相同
        boolean isNoticeDepTheSame = WkCommonUtils.compare(beforeNoticeDepSids, afterNoticeDepSids);

        if (isAssignDepTheSame && isNoticeDepTheSame) {
            log.info("分派/通知單位都未異動");
            return;
        }

        // ====================================
        // 建立分派/通知單位{查詢}資訊
        // ====================================
        execDate = WkDateUtils.addOneSec(execDate);
        this.assignSendSearchHelper.buildAssignNoticeDepSearchInfo(
                requireSid,
                requireNo,
                execUser,
                execDate,
                afterAssignDepSids,
                afterNoticeDepSids,
                isShowDebug);

        // ====================================
        // 建立分派/通知資訊檔 tr_assign_send_info
        // ====================================
        this.process_saveAssignSendInfo(
                requireSid,
                requireNo,
                afterAssignDepSids,
                beforeAssignDepSids,
                afterNoticeDepSids,
                beforeNoticeDepSids,
                execUser,
                WkDateUtils.addOneSec(execDate),
                isShowDebug);

        // ====================================
        // 建立加減派追蹤訊息
        // ====================================
        execDate = WkDateUtils.addOneSec(execDate);
        this.createTraceForAssignNoticeModify(
                requireSid,
                requireNo,
                afterAssignDepSids,
                beforeAssignDepSids,
                afterNoticeDepSids,
                beforeNoticeDepSids,
                modifyDepMessagePrefix,
                execUser.getSid(),
                execDate);

        // ====================================
        // 維護分派單位需求確認檔
        // ====================================
        execDate = WkDateUtils.addOneSec(execDate);
        this.requireConfirmDepService.saveRequireConfirmDeps(
                requireSid,
                requireNo,
                beforeAssignDepSids,
                afterAssignDepSids,
                execUser.getSid(),
                execDate,
                addReasonType,
                autoAssignDesc,
                isShowDebug);

        // ====================================
        // 若需要 (現有分派單位皆已完成確認), 則執行需求完成
        // ====================================
        execDate = WkDateUtils.addOneSec(execDate);
        this.requireProcessCompleteService.executeByAllDepConfirm(
                requireSid,
                execUser,
                execDate,
                true,
                false);

        // ====================================
        // 若需要 (製作進度為已完成，但現有分派單位有任一未完成確認), 則執行反需求完成
        // ====================================
        execDate = WkDateUtils.addOneSec(execDate);
        this.requireProcessCompleteRollbackService.executeByModifyAssignDep(
                requireSid,
                execUser,
                execDate);

        // ====================================
        // 更新主檔資訊 (HasAssign + 待閱原因 + 閱讀記錄)
        // ====================================
        this.process_UpdateRequireByAssagnNotice(
                requireSid,
                afterAssignDepSids,
                afterNoticeDepSids,
                execUser,
                WkDateUtils.addOneSec(execDate),
                isShowDebug);

        // ====================================
        // 處理系統通知
        // ====================================
        if (!isDisableNotice) {
            try {
                this.sysNotifyHelper.processForAssignDepAndNoticeDep(
                        requireSid,
                        afterAssignDepSids,
                        beforeAssignDepSids,
                        afterNoticeDepSids,
                        beforeNoticeDepSids,
                        execUser.getSid(),
                        isShowDebug);
            } catch (Exception e) {
                log.error("執行系統通知失敗!" + e.getMessage(), e);
            }
        }

    }

    /**
     * 建立分派/通知追蹤訊息
     * 
     * @param requireSid             主單SID
     * @param requireNo              主單 NO
     * @param newAssignDepSids       新的分派單位
     * @param oldAssignDepSids       原分派單位設定
     * @param newNoticeDepSids       新的通知單位
     * @param oldNoticeDepSids       原通知單位設定
     * @param modifyDepMessagePrefix 異動記錄的前綴
     * @param executor               執行者
     * @param execDate               執日期
     */
    public void createTraceForAssignNoticeModify(
            String requireSid,
            String requireNo,
            List<Integer> newAssignDepSids,
            List<Integer> oldAssignDepSids,
            List<Integer> newNoticeDepSids,
            List<Integer> oldNoticeDepSids,
            String modifyDepMessagePrefix,
            Integer execUserSid,
            Date execDate) {

        // 兜組加減派訊息
        String modifyDepMessage = this.prepareModifyDepsInfo(
                oldAssignDepSids,
                newAssignDepSids,
                oldNoticeDepSids,
                newNoticeDepSids);

        if (WkStringUtils.notEmpty(modifyDepMessagePrefix)) {
            modifyDepMessage = "因" + modifyDepMessagePrefix + "<br/>" + modifyDepMessage;
        }

        this.requireTraceService.insertTrace(
                requireSid,
                requireNo,
                RequireTraceType.REQUIRE_ASSIGN,
                modifyDepMessage,
                execUserSid,
                execDate);
    }

    /**
     * 邏輯檢查
     * 
     * @param requireSid
     * @param effectiveTime
     * @param executor
     * @param oldAssignInfo
     * @param oldAssignDepSids
     * @param oldNoticInfo
     * @param newAssignDepSids
     * @param newNoticeDepSids
     * @param isInitByConfirmCheck
     * @throws UserMessageException
     */
    @Transactional(readOnly = true)
    public void logicCheck(
            String requireSid,
            Long effectiveTime,
            User executor,
            AssignSendInfo oldAssignInfo,
            AssignSendInfo oldNoticInfo,
            List<Integer> oldAssignDepSids,
            List<Integer> oldNoticeDepSids,
            List<Integer> newAssignDepSids,
            List<Integer> newNoticeDepSids,
            boolean isInitByConfirmCheck) throws UserMessageException {

        // ====================================
        // 防呆
        // ====================================
        if (oldAssignDepSids == null) {
            oldAssignDepSids = Lists.newArrayList();
        }
        if (newAssignDepSids == null) {
            newAssignDepSids = Lists.newArrayList();
        }
        if (newNoticeDepSids == null) {
            newNoticeDepSids = Lists.newArrayList();
        }

        // ====================================
        // 分派單位限制
        // ====================================
        // 至少需有一個分派單位
        if (WkStringUtils.isEmpty(newAssignDepSids)) {
            throw new UserMessageException("請至少選擇一個分派單位!", InfomationLevel.INFO);
        }

        // 排除已停用單位
        for (Integer depSid : newAssignDepSids) {
            Org dep = this.wkOrgCache.findBySid(depSid);
            if (dep == null) {
                throw new UserMessageException("選擇的分派單位 [" + depSid + "] 找不到資料, 請洽相關人員", InfomationLevel.ERROR);
            }
            if (Activation.INACTIVE.equals(dep.getStatus())) {
                throw new UserMessageException("選擇的分派單位 [" + WkOrgUtils.getOrgName(dep) + "] 已停用！", InfomationLevel.INFO);
            }
        }
        for (Integer depSid : newNoticeDepSids) {
            Org dep = this.wkOrgCache.findBySid(depSid);
            if (dep == null) {
                throw new UserMessageException("選擇的通知單位 [" + depSid + "] 找不到資料, 請洽相關人員", InfomationLevel.ERROR);
            }
            if (Activation.INACTIVE.equals(dep.getStatus())) {
                throw new UserMessageException("選擇的通知單位 [" + WkOrgUtils.getOrgName(dep) + "] 已停用！", InfomationLevel.INFO);
            }

        }

        // ====================================
        // 不可同時為分派/通知部門
        // ====================================
        // 複製分派單位後
        List<Integer> intersectionDeps = Lists.newArrayList(newAssignDepSids);
        // 取與通知單位的交集
        intersectionDeps.retainAll(newNoticeDepSids);
        if (WkStringUtils.notEmpty(intersectionDeps)) {
            for (Integer depSid : intersectionDeps) {
                Org dep = this.wkOrgCache.findBySid(depSid);
                if (dep == null) {
                    throw new UserMessageException("選擇單位 【" + depSid + "】 找不到資料, 請洽相關人員", InfomationLevel.ERROR);
                }

                String message = "單位 【" + WkOrgUtils.getOrgName(dep) + "】 同時存在於分派/通知單位中！";

                if (isInitByConfirmCheck) {
                    if (oldAssignDepSids.contains(depSid)) {
                        message += "<br/>(其他人已『分派』過, 請查閱單據下方『分派/通知資訊』!)";
                    }
                    if (oldNoticeDepSids.contains(depSid)) {
                        message += "<br/>(其他人已『通知』過, 請查閱單據下方『分派/通知資訊』!)";
                    }
                }

                throw new UserMessageException(message, InfomationLevel.INFO);

            }
        }

        // ====================================
        // 查詢主檔
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);

        // ====================================
        // 權限檢查
        // ====================================
        // 為【檢查確認】時權限檢查
        // 改在檢查確認 RequireService.executeConfirmCheck 自行檢查
        // if (isInitByConfirmCheck &&
        // !reqShowService.showCheckConfirmBtn(
        // require,
        // executor.getSid())) {
        // log.warn("無權限執行檢查確認: reqShowService.showCheckConfirmBtn");
        // throw new UserMessageException("資料已異動，請重新整理頁面!", InfomationLevel.INFO);
        // }

        // 為【分派】時權限檢查
        if (!isInitByConfirmCheck &&
                !reqShowService.showAssignSendBtn(
                        require,
                        executor.getSid())) {
            log.warn("無權限執行分派/通知設定: hasRole_AssignExec");
            throw new UserMessageException("資料已異動，請重新整理頁面!", InfomationLevel.INFO);
        }

        // ====================================
        // 檢查資料是否已被異動 (最新資料異動時間 > 設定時取得舊有資料時間)
        // ====================================
        if (!checkEffectiveTime(oldAssignInfo, effectiveTime)
                || !checkEffectiveTime(oldNoticInfo, effectiveTime)) {
            throw new UserMessageException("資料已被異動，請重新整理頁面", InfomationLevel.INFO);
        }

        // ====================================
        // 取得此次移除的分派單位(減派)
        // ====================================
        // 此次被移除的單位
        List<Integer> deleteAssignDepSids = WkCommonUtils.cloneObject(oldAssignDepSids, new TypeToken<List<Integer>>() {
        }.getType());
        // 舊指派單位 - 新指派單位 = 減派的單位
        if (WkStringUtils.notEmpty(newAssignDepSids)) {
            deleteAssignDepSids.removeAll(newAssignDepSids);
        }
        // 無減派單位, 無需檢核
        if (WkStringUtils.isEmpty(deleteAssignDepSids)) {
            return;
        }

        // ====================================
        // 檢查被減派的單位是否已經完成『需求確認』
        // ====================================
        this.logicCheck_isComplete(requireSid, oldAssignDepSids, newAssignDepSids);

        // ====================================
        // 檢查已開立子單的單位不可移除分派
        // ====================================
        this.logicCheck_hasSubCase(requireSid, deleteAssignDepSids);

        // ====================================
        // 檢查轉 FB 單位不可移除
        // ====================================
        // 查詢已轉 FB 單位 (並轉 orgSid)
        List<Integer> fogbugzOrgSids = this.fbService.findCreatedOrgByRequire(require)
                .stream()
                .map(Org::getSid)
                .collect(Collectors.toList());

        // 逐筆檢查是否有轉 FB
        for (Integer deleteAssignDepSid : deleteAssignDepSids) {
            if (fogbugzOrgSids.contains(deleteAssignDepSid)) {
                String msg = "單位：【" + this.wkOrgCache.findNameBySid(deleteAssignDepSid) + "】已轉FB，不可移除分派";
                log.info(msg);
                throw new UserMessageException(msg, InfomationLevel.INFO);
            }
        }
    }

    /**
     * 檢查已開立子單的單位不可減派
     * 
     * @param requireSid          需求單 sid
     * @param deleteAssignDepSids 檢派單位 sid
     * @throws UserMessageException
     */
    private void logicCheck_hasSubCase(
            String requireSid,
            List<Integer> deleteAssignDepSids) throws UserMessageException {

        // ====================================
        // 查詢主檔
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);

        // ====================================
        // 檢查單據開立狀態
        // ====================================
        List<String> msgs = Lists.newArrayList();

        // 原型確認
        this.logicCheck_hasSubCase_ptCheck(require, deleteAssignDepSids, msgs);
        // 送測
        this.logicCheck_hasSubCase_workTest(require, deleteAssignDepSids, msgs);
        // ONPG
        this.logicCheck_hasSubCase_onpg(require, deleteAssignDepSids, msgs);
        // 其他設定資訊
        this.logicCheck_hasSubCase_otherSet(require, deleteAssignDepSids, msgs);

        // 檢查通過
        if (WkStringUtils.isEmpty(msgs)) {
            return;
        }

        // ====================================
        // 組錯誤訊息
        // ====================================
        String alertMessage = "以下單位：<br/><br/>";
        alertMessage += String.join("<br/>", msgs);
        alertMessage += "<br/><br/>";
        alertMessage += "不可減派!";

        throw new UserMessageException(alertMessage, InfomationLevel.WARN);
    }

    /**
     * 檢查是否曾經開過送ONPG
     * 
     * @param require
     * @param deleteAssignDepSids
     * @return
     */
    public Set<Integer> logicCheck_hasSubCase_onpg(
            Require require,
            List<Integer> deleteAssignDepSids,
            List<String> msgs) {

        // 未曾開立時. 跳過
        if (!require.getHasOnpg()) {
            return Sets.newHashSet();
        }

        // 查詢所有的被減派單位, 開立的單據
        List<WorkOnpgTo> tos = this.onpgService.findByRequireNoAndCreateDepSids(
                require.getRequireNo(),
                deleteAssignDepSids);

        // 無相關資料時, 跳過
        if (WkStringUtils.isEmpty(tos)) {
            return Sets.newHashSet();
        }

        // 整理有開過單據的部門(distinct)
        Set<Integer> depSids = Sets.newHashSet(
                tos.stream()
                        .map(WorkOnpgTo::getCreateDepSid)
                        .collect(Collectors.toList()));

        if (msgs != null) {
            for (Integer ptDepSid : depSids) {
                msgs.add(WkOrgUtils.prepareBreadcrumbsByDepName(ptDepSid, OrgLevel.MINISTERIAL, false, "-") + "&nbsp;曾開立ONPG");
            }
        }

        return depSids;
    }

    /**
     * 檢查是否曾經開過其他送測資訊
     * 
     * @param require
     * @param deleteAssignDepSids
     * @return
     */
    public Set<Integer> logicCheck_hasSubCase_otherSet(
            Require require,
            List<Integer> deleteAssignDepSids,
            List<String> msgs) {

        // 未曾開立時. 跳過
        if (!require.getHasOthSet()) {
            return Sets.newHashSet();
        }

        // 查詢所有的被減派單位, 開立的單據
        List<TrOsVO> tos = this.trOsService.findByRequireNoAndCreateDepSids(
                require.getRequireNo(),
                deleteAssignDepSids);

        // 無相關資料時, 跳過
        if (WkStringUtils.isEmpty(tos)) {
            return Sets.newHashSet();
        }

        // 整理有開過單據的部門(distinct)
        Set<Integer> depSids = Sets.newHashSet(
                tos.stream()
                        .map(TrOsVO::getDep_Sid)
                        .collect(Collectors.toList()));

        if (msgs != null) {
            for (Integer ptDepSid : depSids) {
                msgs.add(WkOrgUtils.prepareBreadcrumbsByDepName(ptDepSid, OrgLevel.MINISTERIAL, false, "-") + "&nbsp;曾開立其他設定資訊");
            }
        }

        return depSids;
    }

    /**
     * @param require
     * @param deleteAssignDepSids
     * @return
     */
    public Set<Integer> logicCheck_hasSubCase_ptCheck(
            Require require,
            List<Integer> deleteAssignDepSids,
            List<String> msgs) {

        // 查詢所有的被減派單位, 開立的原型確認單據
        List<PtCheckTo> ptCheckTos = this.ptService.findByRequireNoAndCreateDepSids(
                require.getRequireNo(),
                deleteAssignDepSids);

        // 無相關資料時, 跳過
        if (WkStringUtils.isEmpty(ptCheckTos)) {
            return Sets.newHashSet();
        }

        // 整理有開過單據的部門(distinct)
        Set<Integer> ptDepSids = Sets.newHashSet(
                ptCheckTos.stream()
                        .map(PtCheckTo::getCreateDepSid)
                        .collect(Collectors.toList()));

        if (msgs != null) {
            for (Integer ptDepSid : ptDepSids) {
                msgs.add(WkOrgUtils.prepareBreadcrumbsByDepName(ptDepSid, OrgLevel.MINISTERIAL, false, "-") + "&nbsp;曾開立原型確認單");
            }
        }

        return ptDepSids;
    }

    /**
     * 檢查是否曾經開過送測單
     * 
     * @param require
     * @param deleteAssignDepSids
     * @return
     */
    public Set<Integer> logicCheck_hasSubCase_workTest(
            Require require,
            List<Integer> deleteAssignDepSids,
            List<String> msgs) {

        // 未曾開立時. 跳過
        if (!require.getHasTestInfo()) {
            return Sets.newHashSet();
        }

        // 查詢所有的被減派單位, 開立的單據
        List<WorkTestInfoTo> tos = this.sendTestService.findByRequireNoAndCreateDepSids(
                require.getRequireNo(),
                deleteAssignDepSids);

        // 無相關資料時, 跳過
        if (WkStringUtils.isEmpty(tos)) {
            return Sets.newHashSet();
        }

        // 整理有開過單據的部門(distinct)
        Set<Integer> depSids = Sets.newHashSet(
                tos.stream()
                        .map(WorkTestInfoTo::getCreateDepSid)
                        .collect(Collectors.toList()));

        if (msgs != null) {
            for (Integer ptDepSid : depSids) {
                msgs.add(WkOrgUtils.prepareBreadcrumbsByDepName(ptDepSid, OrgLevel.MINISTERIAL, false, "-") + "&nbsp;曾開立送測單");
            }
        }

        return depSids;
    }

    /**
     * 檢查被減派的單位是否已經完成『需求確認』
     * 
     * @param requireSid
     * @param oldAssignDepSids
     * @param newAssignDepSids
     * @throws UserMessageException
     */
    private void logicCheck_isComplete(
            String requireSid,
            List<Integer> oldAssignDepSids,
            List<Integer> newAssignDepSids) throws UserMessageException {

        // ====================================
        // 取得已存在的『需求確認單位』
        // ====================================
        List<RequireConfirmDepVO> requireConfirmDepVOs = this.requireConfirmDepService.findActiveByRequireSid(requireSid);

        // ====================================
        // 依據規則,
        // 計算出新的分派單位清單，所產生的『需求確認單位』
        // ====================================
        List<Org> newRequireConfirmDep = this.requireConfirmDepService.prepareRequireConfirmDeps(newAssignDepSids);
        if (newRequireConfirmDep == null) {
            newRequireConfirmDep = Lists.newArrayList();
        }
        List<Integer> newRequireConfirmDepSids = newRequireConfirmDep.stream().map(Org::getSid).collect(Collectors.toList());

        // ====================================
        // 逐筆檢查
        // ====================================
        for (RequireConfirmDepVO assignDepVO : requireConfirmDepVOs) {
            // 【需求完成確認狀態】不是【已完成】者不用檢查
            if (!ReqConfirmDepProgStatus.COMPLETE.equals(assignDepVO.getProgStatus())) {
                continue;
            }
            // 還存在的需求完成確認單位不檢查 （僅檢查被減派者）
            if (newRequireConfirmDepSids.contains(assignDepVO.getDepSid())) {
                continue;
            }

            // 取得與此『需求確認單位』相關的『被移除分派單位』
            String relationDepName = this.requireConfirmDepService.prepareAssignHistoryMessage(
                    assignDepVO.getDepSid(),
                    oldAssignDepSids,
                    true);

            String msg = "單位：【" + this.wkOrgCache.findNameBySid(assignDepVO.getDepSid()) + "】已完成需求確認，不可移除分派！";

            if (relationDepName.contains("、")) {
                relationDepName = relationDepName.replaceAll("、", "<br/>");
                msg += "<br/><br/><span style='font-weight:bold;text-decoration:underline;'>被分派單位：</span><br/>"
                        + relationDepName + "<br/><br/>"
                        + "請至少保留一個";
            }

            log.info(msg);
            throw new UserMessageException(msg, InfomationLevel.INFO);
        }
    }

    /**
     * @param require
     * @param processDesc
     * @return
     */
    public String prepareLogTitle(Require require, String processDesc) {
        return require.getRequireNo() + "-" + processDesc;
    }

    /**
     * @param require
     * @param processDesc
     * @return
     */
    public String prepareLogTitle(String requireNo, String processDesc) {
        return requireNo + "-" + processDesc;
    }

    /**
     * 兜組加減派資訊
     * 
     * @param oldAssignDepSids
     * @param newAssignDepSids
     * @param oldNoticeDepSids
     * @param newNoticeDepSids
     * @return
     */
    public String prepareModifyDepsInfo(
            List<Integer> oldAssignDepSids,
            List<Integer> newAssignDepSids,
            List<Integer> oldNoticeDepSids,
            List<Integer> newNoticeDepSids) {

        List<String> messages = Lists.newArrayList();

        // ====================================
        // 處理分派單位
        // ====================================
        ItemsCollectionDiffTo<Integer> diffTo = this.assignSendInfoService.prepareModifyDepsInfo(
                oldAssignDepSids,
                newAssignDepSids);

        String style = "font-weight:bold;text-decoration:underline;";
        Integer maxWidth = 300;

        if (WkStringUtils.notEmpty(diffTo.getPlusItems())) {
            messages.add("<font style='" + style + "'>加派:</font>");
            List<Integer> plusDeps = Lists.newArrayList(diffTo.getPlusItems());
            if (diffTo.getPlusItems().size() > 3) {
                messages.add(WkOrgUtils.prepareDepsNameByTreeStyle(plusDeps, 10) + "<br/>");
            } else {
                messages.add(WkOrgUtils.findNameBySid(plusDeps, "、", maxWidth) + "<br/>");
            }
        }

        if (WkStringUtils.notEmpty(diffTo.getReduceItems())) {

            List<Integer> reduceDeps = Lists.newArrayList(diffTo.getReduceItems());

            messages.add("<font style='" + style + "'>減派:</font>");
            if (reduceDeps.size() > 3) {
                messages.add(WkOrgUtils.prepareDepsNameByTreeStyle(reduceDeps, 10) + "<br/>");
            } else {
                messages.add(WkOrgUtils.findNameBySid(reduceDeps, "、", maxWidth) + "<br/>");
            }
        }

        // ====================================
        // 處理通知單位
        // ====================================
        diffTo = this.assignSendInfoService.prepareModifyDepsInfo(
                oldNoticeDepSids,
                newNoticeDepSids);

        if (WkStringUtils.notEmpty(diffTo.getPlusItems())) {
            List<Integer> plusDeps = Lists.newArrayList(diffTo.getPlusItems());

            messages.add("<font style='" + style + "'>通知:</font>");
            if (plusDeps.size() > 3) {
                messages.add(WkOrgUtils.prepareDepsNameByTreeStyle(plusDeps, 10) + "<br/>");
            } else {
                messages.add(WkOrgUtils.findNameBySid(plusDeps, "、", maxWidth) + "<br/>");
            }
        }

        if (WkStringUtils.notEmpty(diffTo.getReduceItems())) {

            List<Integer> reduceDeps = Lists.newArrayList(diffTo.getReduceItems());
            messages.add("<font style='" + style + "'>移除通知:</font>");
            if (reduceDeps.size() > 3) {
                messages.add(WkOrgUtils.prepareDepsNameByTreeStyle(reduceDeps, 5) + "<br/>");
            } else {
                messages.add(WkOrgUtils.findNameBySid(reduceDeps, "、", maxWidth) + "<br/>");
            }
        }

        if (WkStringUtils.isEmpty(messages)) {
            return "";
        }

        return "<span class='WS1-1-3'>"
                + "異動"
                + "【分派/通知】結果如下：</span>"
                + "<br/><br/>"
                + String.join("<br/>", messages);
    }

    /**
     * 執行分派 / 通知
     * 
     * @param requireSid           需求單 SID
     * @param requireNo            需求單號
     * @param effectiveTime
     * @param execUser             執行者
     * @param newAssignDepSids     新的分派單位
     * @param newNoticeDepSids     新的通知單位
     * @param isInitByConfirmCheck 是否為『檢查確認』功能
     * @param confirmCheckItems    檢查確認項目
     * @param sysDate              系統時間
     * @throws UserMessageException
     */
    @Transactional(rollbackFor = Exception.class)
    public void process(
            String requireSid,
            String requireNo,
            Long effectiveTime,
            User execUser,
            List<Integer> newAssignDepSids,
            List<Integer> newNoticeDepSids,
            boolean isInitByConfirmCheck,
            Date sysDate) throws UserMessageException {

        // ====================================
        // 查詢已存在的分派/通知資料
        // ====================================
        // 查詢
        Map<AssignSendType, AssignSendInfo> resultMap = this.assignSendInfoService.findByRequireSidAndStatus(
                requireSid);

        // 取得原分派資料
        AssignSendInfo oldAssignInfo = resultMap.get(AssignSendType.ASSIGN);
        List<Integer> oldAssignDepSids = this.assignSendInfoService.prepareDeps(oldAssignInfo);
        // 取得原通知資料
        AssignSendInfo oldNoticeInfo = resultMap.get(AssignSendType.SEND);
        List<Integer> oldNoticeDepSids = this.assignSendInfoService.prepareDeps(oldNoticeInfo);

        // ====================================
        // 邏輯檢查
        // ====================================
        this.logicCheck(
                requireSid,
                effectiveTime,
                execUser,
                oldAssignInfo,
                oldNoticeInfo,
                oldAssignDepSids,
                oldNoticeDepSids,
                newAssignDepSids,
                newNoticeDepSids,
                isInitByConfirmCheck);

        // ====================================
        // 執行相關流程
        // ====================================
        this.doProcess(
                requireSid,
                requireNo,
                execUser,
                newAssignDepSids,
                newNoticeDepSids,
                isInitByConfirmCheck ? "檢查確認" : "",
                sysDate,
                RequireConfirmAddReasonType.NORMAL,
                "");
    }

    @Transactional(rollbackFor = Exception.class)
    public void processForRebuild(
            String requireSid,
            String requireNo,
            List<Integer> beforeAssignDepSids,
            List<Integer> beforeNoticeDepSids,
            List<Integer> afterAssignDepSids,
            List<Integer> afterNoticeDepSids,
            User execUser) throws UserMessageException {

        this.doProcess(
                requireSid,
                requireNo,
                execUser,
                afterAssignDepSids,
                afterNoticeDepSids,
                "組織異動重建分派單位",
                new Date(),
                RequireConfirmAddReasonType.NORMAL,
                "",
                true,
                beforeAssignDepSids,
                beforeNoticeDepSids);
    }

    /**
     * 清除所有分派/通知單位
     * 
     * @param requireSid 需求單 sid
     * @param requireNo  需求單號
     * @param reason     執行原因
     * @param execUser   執行者
     * @param sysDate    系統日
     * @throws UserMessageException 異常訊息
     */
    @Transactional(rollbackFor = Exception.class)
    public void processClearAllAssignNoticeDeps(
            String requireSid,
            String requireNo,
            String reason,
            User execUser,
            Date sysDate) throws UserMessageException {

        this.doProcess(
                requireSid,
                requireNo,
                execUser,
                Lists.newArrayList(),
                Lists.newArrayList(),
                reason,
                sysDate,
                RequireConfirmAddReasonType.NORMAL,
                "");
    }

    /**
     * 儲存分派/通知單位 (並記錄異動異動記錄)<br/>
     * 1.[delete-insert] 2.modify tr_assign_send_info 、tr_assign_send_info_history
     * 
     * @param requireSid          主單SID
     * @param requireNo           主單 NO
     * @param afterAssignDepSids  新的分派單位
     * @param beforeAssignDepSids 原分派單位設定
     * @param afterNoticeDepSids  新的通知單位
     * @param beforeNoticeDepSids 原通知單位設定
     * @param executor            執行者
     * @param execDate            執日期
     */
    private void process_saveAssignSendInfo(
            String requireSid,
            String requireNo,
            List<Integer> afterAssignDepSids,
            List<Integer> beforeAssignDepSids,
            List<Integer> afterNoticeDepSids,
            List<Integer> beforeNoticeDepSids,
            User executor,
            Date execDate,
            boolean isShowDebug) {

        Long startTime = System.currentTimeMillis();
        String procTitle = this.prepareLogTitle(requireNo, "儲存分派/通知單位資訊[tr_assign_send_info]");
        // log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

        // 分派單位異動前後是否相同
        boolean isAssignDepTheSame = WkCommonUtils.compare(beforeAssignDepSids, afterAssignDepSids);
        // 通知單位異動前後是否相同
        boolean isNoticeDepTheSame = WkCommonUtils.compare(beforeNoticeDepSids, afterNoticeDepSids);

        // ====================================
        // 刪除舊檔 delete tr_assign_send_info by requireSid
        // ====================================
        if (!isAssignDepTheSame && !isNoticeDepTheSame) {
            this.assignSendInfoRepo.deleteByRequire(requireSid);
        } else {

            if (!isAssignDepTheSame) {
                this.assignSendInfoRepo.deleteByRequireAndType(requireSid, AssignSendType.ASSIGN);
            }
            if (!isNoticeDepTheSame) {
                this.assignSendInfoRepo.deleteByRequireAndType(requireSid, AssignSendType.SEND);
            }

        }

        // ====================================
        // 儲存『新』分派部門資訊 insert tr_assign_send_info
        // ====================================
        if (!isAssignDepTheSame) {
            AssignSendInfo assignInfo = assignSendInfoService.createInfo(
                    requireSid,
                    requireNo,
                    executor,
                    execDate,
                    AssignSendType.ASSIGN,
                    afterAssignDepSids);

            this.assignSendInfoRepo.save(assignInfo);
        }

        // ====================================
        // 儲存『新』通知部門資訊 insert tr_assign_send_info
        // ====================================
        if (!isNoticeDepTheSame) {
            AssignSendInfo noticeInfo = assignSendInfoService.createInfo(
                    requireSid,
                    requireNo,
                    executor,
                    execDate,
                    AssignSendType.SEND,
                    afterNoticeDepSids);

            this.assignSendInfoRepo.save(noticeInfo);
        }

        // ====================================
        // 儲存異動記錄
        // ====================================
        // 比對設定單位差異
        // 分派單位
        ItemsCollectionDiffTo<Integer> assignDepDiffTo = this.assignSendInfoService.prepareModifyDepsInfo(
                beforeAssignDepSids,
                afterAssignDepSids);

        // 通知單位
        ItemsCollectionDiffTo<Integer> noticeDepDiffTo = this.assignSendInfoService.prepareModifyDepsInfo(
                beforeNoticeDepSids,
                afterNoticeDepSids);

        // 建立 tr_assign_send_info_history
        AssignSendInfoHistory assignSendInfoHistory = new AssignSendInfoHistory();
        assignSendInfoHistory.setRequireSid(requireSid);
        try {
            assignSendInfoHistory.setAddAssignDeps(WkJsonUtils.getInstance().toJson(assignDepDiffTo.getPlusItems()));
            assignSendInfoHistory.setDeleteAssignDeps(WkJsonUtils.getInstance().toJson(assignDepDiffTo.getReduceItems()));
            assignSendInfoHistory.setAddSendDeps(WkJsonUtils.getInstance().toJson(noticeDepDiffTo.getPlusItems()));
            assignSendInfoHistory.setDeleteSendDeps(WkJsonUtils.getInstance().toJson(noticeDepDiffTo.getReduceItems()));
        } catch (IOException e) {
            log.error("轉 json 失敗!", e);
        }
        assignSendInfoHistory.setCreatedUser(executor.getSid());
        assignSendInfoHistory.setCreatedDate(execDate);
        this.assignSendInfoHistoryRepo.save(assignSendInfoHistory);

        if (isShowDebug) {
            log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
        }
    }

    /**
     * 更新主檔資訊 (HasAssign + 待閱原因 + 閱讀記錄)
     * 
     * @param requireSid         主單 SID
     * @param afterAssignDepSids 分派單位
     * @param afterNoticeDepSids 通知單位
     * @param executor           執行者
     * @param execDate           執行時間
     */
    @Transactional(rollbackFor = Exception.class)
    private void process_UpdateRequireByAssagnNotice(
            String requireSid,
            List<Integer> afterAssignDepSids,
            List<Integer> afterNoticeDepSids,
            User executor,
            Date execDate,
            boolean isShowDebug) {

        if (afterAssignDepSids == null) {
            afterAssignDepSids = Lists.newArrayList();
        }

        // ====================================
        // 查詢主檔
        // ====================================
        Require require = this.requireService.findByReqSid(requireSid);

        Long startTime = System.currentTimeMillis();
        String procTitle = this.prepareLogTitle(require.getRequireNo(), "因異動分派單位更新主檔");
        // log.debug(WkCommonUtils.prepareCostMessageStart(procTitle));

        // ====================================
        // 更新主檔欄位
        // ====================================
        require.setHasAssign(WkStringUtils.notEmpty(afterAssignDepSids));

        // 不為待檢查時，寫待閱原因
        if (!RequireStatusType.WAIT_CHECK.equals(require.getRequireStatus())) {
            require.setReadReason(ReqToBeReadType.MODIFY_ASSIGNED_UNIT);
        }

        this.requireService.save(require);

        // ====================================
        // 將『已經讀取』過單據的人，更新為待閱讀 (執行者除外)
        // ====================================
        this.requireReadRecordHelper.resetUserList(
                FormType.REQUIRE,
                require.getSid(),
                this.requireService.prepareReadRecordUserSids(require, afterAssignDepSids, afterNoticeDepSids),
                executor.getSid());

        if (isShowDebug) {
            log.debug(WkCommonUtils.prepareCostMessage(startTime, procTitle));
        }
    }

    /**
     * 自動新增分派單位 (若已存在則無動作)
     * 
     * @param requireSid       需求單 SID
     * @param requireNo        需求單號
     * @param addAssignDepSids 新增的分派單位
     * @param addReasonType    新增原因類型
     * @param autoAssignDesc   自動新增說明
     * @param execUser         執行者
     * @param execDate         執行時間
     * @throws UserMessageException 檢核異常時拋出
     */
    // @Transactional(rollbackFor = Exception.class)
    public void processForAutoAddAssignDep(
            String requireSid,
            String requireNo,
            Integer addAssignDepSids,
            String autoAssignDesc,
            User execUser,
            Date execDate) throws UserMessageException {

        this.processForAddAssignDeps(
                requireSid,
                requireNo,
                Lists.newArrayList(addAssignDepSids),
                autoAssignDesc + "，自動加派",
                RequireConfirmAddReasonType.AUTO_ASSIGN_BY_CREATE_CHILD_CASE,
                autoAssignDesc,
                execUser,
                execDate);

    }

    /**
     * 增加分派單位 (僅增加分派單位)
     * 
     * @param requireSid             需求單 SID
     * @param requireNo              需求單號
     * @param addAssignDepSids       新增的分派單位
     * @param modifyDepMessagePrefix 異動說明
     * @param addReasonType          新增原因類型
     * @param autoAssignDesc         自動新增說明
     * @param execUser               執行者
     * @param execDate               執行時間
     * @throws UserMessageException 檢核異常時拋出
     */
    @Transactional(rollbackFor = Exception.class)
    public void processForAddAssignDeps(
            String requireSid,
            String requireNo,
            List<Integer> addAssignDepSids,
            String modifyDepMessagePrefix,
            RequireConfirmAddReasonType addReasonType,
            String autoAssignDesc,
            User execUser,
            Date execDate) throws UserMessageException {

        if (execDate == null) {
            execDate = new Date();
        }

        // ====================================
        // 取得內部需求分派單位
        // ====================================
        // 取得登入者部門資料
        if (execUser == null) {
            String message = "取得登入者資料失敗!";
            log.warn(message);
            throw new UserMessageException(message);
        }

        if (execUser.getPrimaryOrg() == null) {
            String message = "取得登入者隸屬單位失敗! userSid:[" + execUser.getSid() + "]";
            log.warn(message);
            throw new UserMessageException(message);
        }

        // ====================================
        // 查詢已存在的分派/通知資料
        // ====================================
        // 查詢
        Map<AssignSendType, AssignSendInfo> resultMap = this.assignSendInfoService.findByRequireSidAndStatus(
                requireSid);

        // 取得原分派資料
        AssignSendInfo oldAssignInfo = resultMap.get(AssignSendType.ASSIGN);
        List<Integer> oldAssignDepSids = this.assignSendInfoService.prepareDeps(oldAssignInfo);
        // 取得原通知資料
        AssignSendInfo oldNoticeInfo = resultMap.get(AssignSendType.SEND);
        List<Integer> oldNoticeDepSids = this.assignSendInfoService.prepareDeps(oldNoticeInfo);

        // 新增單位已經分派時, 略過
        boolean isAllAssign = true;
        for (Integer assignDepSid : addAssignDepSids) {
            if (!oldAssignDepSids.contains(assignDepSid)) {
                isAllAssign = false;
                break;
            }
        }

        if (isAllAssign) {
            log.debug(
                    "單位:【{}】都已經是『分派』單位, 略過自動分派 by [{}]",
                    WkOrgUtils.findNameBySid(addAssignDepSids, "-", true),
                    autoAssignDesc);
            return;
        }

        // 新的分派單位, 為舊分派單位 + 新增傳入單位
        Set<Integer> newAssignDepSids = Sets.newHashSet();
        if (WkStringUtils.notEmpty(oldAssignDepSids)) {
            newAssignDepSids.addAll(oldAssignDepSids);
        }
        if (WkStringUtils.notEmpty(addAssignDepSids)) {
            newAssignDepSids.addAll(addAssignDepSids);
        }

        // ====================================
        // 執行相關流程
        // ====================================
        this.doProcess(
                requireSid,
                requireNo,
                execUser,
                Lists.newArrayList(newAssignDepSids),
                oldNoticeDepSids,
                modifyDepMessagePrefix,
                execDate,
                addReasonType,
                autoAssignDesc);
    }

    /**
     * 僅新增通知單位
     * 
     * @param requireSid             需求單 SID
     * @param requireNo              需求單號
     * @param addNoticeDepSids       新增的通知單位 SID
     * @param modifyDepMessagePrefix 異動記錄前綴
     * @param execUser               執行者
     * @param execDate               執行時間
     * @throws UserMessageException 檢核異常時拋出
     */
    @Transactional(rollbackFor = Exception.class)
    public void processForAddNoticeDeps(
            String requireSid,
            String requireNo,
            List<Integer> addNoticeDepSids,
            String modifyDepMessagePrefix,
            User execUser,
            Date execDate) throws UserMessageException {

        if (execDate == null) {
            execDate = new Date();
        }

        // ====================================
        // 取得內部需求分派單位
        // ====================================
        // 取得登入者部門資料
        if (execUser == null) {
            String message = "取得登入者資料失敗!";
            log.warn(message);
            throw new UserMessageException(message);
        }

        if (execUser.getPrimaryOrg() == null) {
            String message = "取得登入者隸屬單位失敗! userSid:[" + execUser.getSid() + "]";
            log.warn(message);
            throw new UserMessageException(message);
        }

        // ====================================
        // 查詢已存在的分派/通知資料
        // ====================================
        // 查詢
        Map<AssignSendType, AssignSendInfo> resultMap = this.assignSendInfoService.findByRequireSidAndStatus(
                requireSid);

        // 取得原分派資料
        AssignSendInfo oldAssignInfo = resultMap.get(AssignSendType.ASSIGN);
        List<Integer> oldAssignDepSids = this.assignSendInfoService.prepareDeps(oldAssignInfo);
        List<Integer> newAssignDepSids = oldAssignDepSids.stream().collect(Collectors.toList());
        // 取得原通知資料
        AssignSendInfo oldNoticeInfo = resultMap.get(AssignSendType.SEND);
        List<Integer> oldNoticeDepSids = this.assignSendInfoService.prepareDeps(oldNoticeInfo);

        // 新增單位已經分派時, 略過
        boolean isAllAssign = true;
        for (Integer noticeDepSid : addNoticeDepSids) {
            if (!oldNoticeDepSids.contains(noticeDepSid)) {
                isAllAssign = false;
                break;
            }
        }

        if (isAllAssign) {
            log.debug(
                    "單位:【{}】都已經是『通知』單位, 略過新增",
                    WkOrgUtils.findNameBySid(addNoticeDepSids, "-", true));
            return;
        }

        // 新的通知單位清單, 為舊通知單位 + 新增傳入單位
        Set<Integer> newNoticeDepSids = Sets.newHashSet();
        if (WkStringUtils.notEmpty(oldNoticeDepSids)) {
            newNoticeDepSids.addAll(oldNoticeDepSids);
        }
        if (WkStringUtils.notEmpty(addNoticeDepSids)) {
            newNoticeDepSids.addAll(addNoticeDepSids);
        }

        // ====================================
        // 建立分派/通知單位{查詢}資訊
        // ====================================
        this.assignSendSearchHelper.buildAssignNoticeDepSearchInfo(
                requireSid,
                requireNo,
                execUser,
                execDate,
                newAssignDepSids,
                Lists.newArrayList(newNoticeDepSids),
                true);

        // ====================================
        // 建立分派/通知資訊檔 tr_assign_send_info
        // ====================================
        this.process_saveAssignSendInfo(
                requireSid,
                requireNo,
                newAssignDepSids,
                oldAssignDepSids,
                Lists.newArrayList(newNoticeDepSids),
                oldNoticeDepSids,
                execUser,
                execDate,
                true);

        // ====================================
        // 建立加減派追蹤訊息
        // ====================================
        this.createTraceForAssignNoticeModify(
                requireSid,
                requireNo,
                newAssignDepSids,
                oldAssignDepSids,
                Lists.newArrayList(newNoticeDepSids),
                oldNoticeDepSids,
                modifyDepMessagePrefix,
                execUser.getSid(),
                execDate);
    }

    /**
     * 執行分派流程 (加派給自己單位), for 內部需求新增單據 <br/>
     * 1.無需邏輯檢查<br/>
     * 2.無需取得已存在通知/分派資料<br/>
     * 
     * @param requireSid 需求單 SID
     * @param requireNo  需求單號
     * @param execUser   執行者
     * @param execDate   執行時間
     * @throws UserMessageException 檢核異常時拋出
     */
    @Transactional(rollbackFor = Exception.class)
    public Require processForInternalAdd(
            String requireSid,
            String requireNo,
            User execUser,
            Date execDate) throws UserMessageException {

        // ====================================
        // 檢核
        // ====================================
        // 取得登入者部門資料
        if (execUser == null) {
            String message = "取得登入者資料失敗!";
            log.warn(message);
            throw new UserMessageException(message);
        }

        if (execUser.getPrimaryOrg() == null) {
            String message = "取得登入者隸屬單位失敗! userSid:[" + execUser.getSid() + "]";
            log.warn(message);
            throw new UserMessageException(message);
        }

        // ====================================
        // 查詢需求單資料
        // ====================================
        // 取得主檔
        Require require = this.requireService.findByReqSid(requireSid);
        if (require == null) {
            String message = WkMessage.NEED_RELOAD;
            log.warn(message + "require 找不到! sid:[{}]", requireSid);
            throw new UserMessageException(message);
        }

        if (require.getMapping() == null || require.getMapping().getSmall() == null) {
            String message = WkMessage.NEED_RELOAD;
            log.warn(message + "require 找不到類別資料! sid:[{}]", requireSid);
            throw new UserMessageException(message);
        }

        // 查詢系統別
        List<RequireCheckItemType> requireCheckItemTypes = this.requireCheckItemService.findCheckItemTypesByRequireSid(requireSid);
        if (WkStringUtils.isEmpty(requireCheckItemTypes)) {
            String message = WkMessage.NEED_RELOAD;
            log.warn(message + "require 沒有系統別! sid:[{}]", requireSid);
            throw new UserMessageException(message);
        }

        // ====================================
        // 自動分派單位
        // ====================================
        // 內部需求有簽核時，執行者可能為最高主管，故應取單據建立者
        // 取得申請人SID
        Integer createrUserOrgSid = require.getCreatedUser().getPrimaryOrg().getSid();

        // 1.內部需求分派單位 (自己部門)
        // 申請者主要部門
        Set<Integer> newAssignDepSids = Sets.newHashSet(createrUserOrgSid);
        // 申請者單位收束到【部】
        Org dep = this.requireConfirmDepService.prepareRequireConfirmDep(createrUserOrgSid);
        newAssignDepSids.add(dep.getSid());

        // 2.另外設定的預設分派單位
        Set<Integer> defaultAssignDepSids = this.prepareDefaultAssignSendDepSids(
                require.getMapping().getSmall().getSid(),
                requireCheckItemTypes,
                AssignSendType.ASSIGN);

        if (WkStringUtils.notEmpty(defaultAssignDepSids)) {
            newAssignDepSids.addAll(defaultAssignDepSids);
        }

        // 將停用單位移除
        newAssignDepSids = newAssignDepSids.stream()
                .filter(depSid -> WkOrgUtils.isActive(depSid))
                .collect(Collectors.toSet());

        // ====================================
        // 自動通知單位
        // ====================================
        // 設定的預設通知單位
        Set<Integer> defaultSendDepSids = this.prepareDefaultAssignSendDepSids(
                require.getMapping().getSmall().getSid(),
                requireCheckItemTypes,
                AssignSendType.SEND);

        final Set<Integer> tempAssignDepSids = newAssignDepSids.stream().collect(Collectors.toSet());

        // 將停用單位移除
        // 將同時為『分派單位』者移除 (無需既分派又通知, 以分派為主)
        defaultSendDepSids = defaultSendDepSids.stream()
                .filter(depSid -> WkOrgUtils.isActive(depSid))
                .filter(depSid -> !tempAssignDepSids.contains(depSid))
                .collect(Collectors.toSet());

        // ====================================
        // 執行相關流程
        // ====================================
        this.doProcess(
                requireSid,
                requireNo,
                execUser,
                Lists.newArrayList(newAssignDepSids),
                Lists.newArrayList(defaultSendDepSids),
                "內部需求，自動加派/通知",
                execDate,
                RequireConfirmAddReasonType.AUTO_ASSIGN_BY_CREATE_INTERNAL,
                "");

        return this.requireService.findByReqSid(requireSid);
    }

    /**
     * 準備小類的預設[分派/通知]單位
     * 
     * @param smallCategorySid      小類 sid
     * @param requireCheckItemTypes 單據勾選的系統別
     * @param assignSendType        分派/通知
     * @return 設定的預設部門 sid
     */
    private Set<Integer> prepareDefaultAssignSendDepSids(
            String smallCategorySid,
            List<RequireCheckItemType> requireCheckItemTypes,
            AssignSendType assignSendType) {

        // 收集該張單據所有系統別的設定 (去重複)
        Set<Integer> allDepSids = Sets.newHashSet();
        for (RequireCheckItemType requireCheckItemType : requireCheckItemTypes) {
            // 查詢[小類+系統別+分派/通知]設定
            List<Integer> currDepSids = this.settingDefaultAssignSendDepService.findDepSidsByUnqKey(
                    smallCategorySid,
                    requireCheckItemType,
                    assignSendType);

            if (WkStringUtils.notEmpty(currDepSids)) {
                allDepSids.addAll(currDepSids);
            }
        }

        return allDepSids;

    }

    /**
     * 案件單轉需求單
     * 
     * @param requireSid 需求單 SID
     * @param requireNo  需求單號
     * @param execUser   執行者
     * @param execDate   執行時間
     * @throws UserMessageException 檢核異常時拋出
     */
    @Transactional(rollbackFor = Exception.class)
    public void processForIssueTrns(
            String requireSid,
            String requireNo,
            User execUser,
            Date execDate) throws UserMessageException {

        // ====================================
        // 計算加派部門 (基本部門以下所有單位)
        // ====================================
        // 登入者單位
        Integer loginUserDepSid = execUser.getPrimaryOrg().getSid();
        Set<Integer> newAssignDepSids = Sets.newHashSet(loginUserDepSid);
        // 登入者單位收束到【部】
        Org dep = this.requireConfirmDepService.prepareRequireConfirmDep(loginUserDepSid);
        // 基本單位以下所有子單位
        Set<Integer> childDepSids = WkOrgCache.getInstance().findAllChildSids(dep.getSid());
        if (WkStringUtils.notEmpty(childDepSids)) {
            newAssignDepSids.addAll(childDepSids);
        }

        // 過濾停用
        newAssignDepSids = newAssignDepSids.stream()
                .filter(depSid -> WkOrgUtils.isActive(depSid))
                .collect(Collectors.toSet());

        // ====================================
        // 執行加派
        // ====================================
        this.processForAddAssignDeps(
                requireSid,
                requireNo,
                Lists.newArrayList(newAssignDepSids),
                "案件單轉ON程式，自動加派",
                RequireConfirmAddReasonType.AUTO_ASSIGN_BY_CREATE_CHILD_CASE,
                "案件單轉ON程式",
                execUser,
                execDate);
    }

    /**
     * 批次執行設定
     * 
     * @param assignNoticeProcessTos
     * @throws UserMessageException
     * @throws SystemDevelopException
     */
    public String batchDoPocess(
            List<AssignNoticeProcessTo> assignNoticeProcessTos,
            User execUser,
            Date sysDate,
            RequireConfirmAddReasonType addReasonType,
            String autoAssignDesc) throws UserMessageException {

        if (WkStringUtils.isEmpty(assignNoticeProcessTos)) {
            throw new UserMessageException("未傳入異動分派/通知部門資料!", InfomationLevel.WARN);
        }

        // ====================================
        // 查詢需求單主檔
        // ====================================
        // 收集需求單號
        Set<String> requireSids = assignNoticeProcessTos.stream()
                .map(AssignNoticeProcessTo::getRequireSid).collect(Collectors.toSet());

        // 查詢
        List<Require> requires = requireRepository.findAll(requireSids);
        if (WkStringUtils.isEmpty(requires)) {
            throw new UserMessageException("找不到需求單主檔!", InfomationLevel.ERROR);
        }

        // 以sid 做 index
        Map<String, Require> requireMapBySid = requires.stream()
                .collect(Collectors.toMap(
                        Require::getSid,
                        entity -> entity));

        // ====================================
        // 逐筆處理
        // ====================================
        Gson gson = new com.google.gson.GsonBuilder().setPrettyPrinting().create();
        List<String> errorMessage = Lists.newArrayList();
        for (AssignNoticeProcessTo assignNoticeProcessTo : assignNoticeProcessTos) {

            // 歸屬的需求單
            Require require = requireMapBySid.get(assignNoticeProcessTo.getRequireSid());
            if (require == null) {
                throw new UserMessageException(
                        "\r\n找不到對應的需求檔資料!\r\n" + gson.toJson(assignNoticeProcessTo), InfomationLevel.ERROR);
            }

            // 單筆執行 , 失敗繼續執行下一筆
            try {
                this.doProcess(
                        require.getSid(),
                        require.getRequireNo(),
                        execUser,
                        Lists.newArrayList(assignNoticeProcessTo.getNewAssignDepSids()),
                        Lists.newArrayList(assignNoticeProcessTo.getNewNoticeDepSids()),
                        "『分派單位』轉『通知單位』",
                        sysDate,
                        addReasonType,
                        autoAssignDesc);
            } catch (Exception e) {
                log.error("[" + require.getRequireNo() + "] 執行失敗 [" + e.getMessage() + "]", e);
                errorMessage.add("[" + require.getRequireNo() + "] 執行失敗 [" + e.getMessage() + "]");
            }
        }

        if (WkStringUtils.notEmpty(errorMessage)) {
            return String.join("\r\n", errorMessage);
        }
        return "";
    }

    /**
     * 以『已分派單位』 計算登入者開單時, 該單據的歸屬單位
     * 
     * @param requireSid   需求單 SID
     * @param loginUserSid 登入者 SID
     * @return 1.回傳可選的歸屬單位列表<br/>
     *             2.當與所有的已分派單位都沒有關係時, 回傳登入單位
     * @throws UserMessageException 取不到登入者資料時拋出
     */
    public List<Integer> prepareCaseOwnerDepsByAssignDep(String requireSid, Integer loginUserSid) throws SystemOperationException {

        // ====================================
        // 取得 user 資料
        // ====================================
        // 取得使用者資料
        User loginUser = this.wkUserCache.findBySid(loginUserSid);
        if (loginUser == null || loginUser.getPrimaryOrg() == null) {
            throw new SystemOperationException("取得使用者資料發生錯誤! user:[" + loginUserSid + "]", InfomationLevel.ERROR);
        }

        // 取得使用者主要部門
        Integer loginOrgSid = loginUser.getPrimaryOrg().getSid();

        // ====================================
        // 依據單據取得分派單位
        // ====================================
        Set<Integer> assignDepSids = this.assignSendInfoService.findAssignDepsByRequireSid(requireSid).stream()
                .filter(depSid -> WkOrgUtils.isActive(depSid))
                .collect(Collectors.toSet());

        // ====================================
        // 檢查與使用者相關的分派單位
        // 以下規則, 優先符合即 return
        // ====================================
        // -----------------------
        // 0. 無任何分派單位. 回傳使用者登入單位, 後續觸發自動加派
        // -----------------------
        if (WkStringUtils.isEmpty(assignDepSids)) {
            return Lists.newArrayList(loginOrgSid);
        }

        // -----------------------
        // 1. 分派單位為使用者主要部門，僅顯示登入單位
        // -----------------------
        if (assignDepSids.contains(loginOrgSid)) {
            return Lists.newArrayList(loginOrgSid);
        }

        // -----------------------
        // 2. 使用者為分派單位代理主管 (主要單位為其他單位, 但具分派單位主管身份)
        // -----------------------
        // 取得登入者所管理的單位 （過濾停用）
        List<Org> managerOrgs = this.wkOrgCache.findManagerOrgs(loginUserSid).stream()
                .filter(org -> Activation.ACTIVE.equals(org.getStatus()))
                .collect(Collectors.toList());

        // log.info("A:\r\n" + new
        // com.google.gson.GsonBuilder().setPrettyPrinting().create().toJson(
        // managerOrgs.stream().map(org-> org.getSid() +":"+
        // org.getName()).collect(Collectors.toList())));

        // 比對管理單位是否為分派單位
        if (WkStringUtils.notEmpty(managerOrgs)) {
            List<Integer> managerAssignDeps = Lists.newArrayList();
            for (Org org : managerOrgs) {
                if (assignDepSids.contains(org.getSid())) {
                    managerAssignDeps.add(org.getSid());
                }
            }

            if (WkStringUtils.notEmpty(managerAssignDeps)) {
                // log.info("B:\r\n" + new
                // com.google.gson.GsonBuilder().setPrettyPrinting().create().toJson(managerAssignDeps));
                return managerAssignDeps;
            }
        }

        // -----------------------
        // 3. 使用者為-分派單位-上層單位成員 （應該不會觸發 ,分派單位上層）
        // -----------------------
        // 不限定為上層部門主管的原因: 若因使用者身份為成員, 即自動加派其所屬單位, 可能會影響其他邏輯, 故不隨便自動加派
        List<Integer> assignDepParentSids = Lists.newArrayList();
        for (Integer assignDepSid : assignDepSids) {
            // 查詢分派單位所有上層部門 （過濾停用）
            List<Org> parentOrgs = this.wkOrgCache.findAllParent(assignDepSid).stream()
                    .filter(org -> Activation.ACTIVE.equals(org.getStatus()))
                    .collect(Collectors.toList());
            // 比對使用者『所屬部門』是否屬於『分派單位』的上層部門
            for (Org parentOrg : parentOrgs) {
                if (WkCommonUtils.compareByStr(loginOrgSid, parentOrg.getSid())) {
                    assignDepParentSids.add(assignDepSid);
                    break;
                }
            }
        }
        if (WkStringUtils.notEmpty(assignDepParentSids)) {
            return assignDepParentSids;
        }

        // -----------------------
        // 4. 使用者和所有已分派單位都沒有關係, 回傳登入部門, 後續觸發自動加派
        // -----------------------
        return Lists.newArrayList(loginOrgSid);
    }

    /**
     * 大於部級的單位，分派轉通知
     * 
     * @param assignDepSids
     * @param noticeDepSids
     * @return
     */
    public Set<Integer> trnsAssignToNoticeByMoreMinisterial(
            String requireSid,
            List<Integer> assignDepSids,
            List<Integer> noticeDepSids) {

        Set<Integer> procNewAssignDepSids = Sets.newHashSet(assignDepSids);
        Set<Integer> procNewNoticeDepSids = Sets.newHashSet(noticeDepSids);
        Set<Integer> trnsDepSids = Sets.newHashSet();

        // ====================================
        // 查詢所有單位確認狀態
        // ====================================
        Map<Integer, String> progStatusMapByDepSid = requireConfirmDepService.queryProgStatus(requireSid);

        // ====================================
        // 收集需要轉換的單位
        // ====================================
        for (Integer assignDepSid : assignDepSids) {
            Org assignDep = this.wkOrgCache.findBySid(assignDepSid);
            if (assignDep == null || assignDep.getLevel() == null) {
                continue;
            }

            // 比對是否大於部級
            if (WkOrgUtils.compareOrgLevel(assignDep.getLevel(), OrgLevel.MINISTERIAL) <= 0) {
                continue;
            }

            // 判斷該分派單位下已經沒有子單位, 則不適用此規則
            if (WkOrgCache.getInstance().findAllChildSids(assignDep.getSid()).size() == 0) {
                continue;
            }

            // 若單位已經確認完成, 則不轉換
            // 處級以上單位會對應相等部門的需求確認單位檔, 若未來異動規則, 需檢查此段
            if (progStatusMapByDepSid.containsKey(assignDepSid)) {
                String progStatus = progStatusMapByDepSid.get(assignDepSid);
                if (ReqConfirmDepProgStatus.COMPLETE.name().equals(progStatus)) {
                    continue;
                }
            }

            // 轉通知單位
            // 記錄於轉置清單
            trnsDepSids.add(assignDepSid);
            // 由分派單位中移除
            procNewAssignDepSids.remove(assignDepSid);
            // 加入通知單位
            procNewNoticeDepSids.add(assignDepSid);
        }

        // 將傳入清單, 置換為新的運算結果
        assignDepSids.clear();
        for (Integer depSid : procNewAssignDepSids) {
            assignDepSids.add(depSid);
        }
        noticeDepSids.clear();
        for (Integer depSid : procNewNoticeDepSids) {
            noticeDepSids.add(depSid);
        }

        return trnsDepSids;

    }

    /**
     * 分派轉通知 by 禁止分派名單 (後台參數)
     * 
     * @param assignDepSids
     * @param noticeDepSids
     * @return
     */
    public Set<Integer> trnsAssignToNoticeByAntiList(
            String requireSid,
            List<Integer> assignDepSids,
            List<Integer> noticeDepSids) {

        Set<Integer> procNewAssignDepSids = Sets.newHashSet(assignDepSids);
        Set<Integer> procNewNoticeDepSids = Sets.newHashSet(noticeDepSids);
        Set<Integer> trnsDepSids = Sets.newHashSet();

        // ====================================
        // 查詢所有單位確認狀態
        // ====================================
        Map<Integer, String> progStatusMapByDepSid = requireConfirmDepService.queryProgStatus(requireSid);

        // ====================================
        // 取得禁止分派的單位
        // ====================================
        Set<Integer> antiAssignDepSids = this.workBackendParamHelper.findReqAntiAssignDepSids();
        if (WkStringUtils.isEmpty(antiAssignDepSids)) {
            return Sets.newHashSet();
        }

        // ====================================
        // 收集需要轉換的單位
        // ====================================
        for (Integer assignDepSid : assignDepSids) {

            // 比對是否不在禁止分派單位名單
            if (!antiAssignDepSids.contains(assignDepSid)) {
                // 不在名單中 -> 略過
                continue;
            }

            // 若單位已經確認完成, 則不轉換
            if (progStatusMapByDepSid.containsKey(assignDepSid)) {
                String progStatus = progStatusMapByDepSid.get(assignDepSid);
                if (ReqConfirmDepProgStatus.COMPLETE.name().equals(progStatus)) {
                    continue;
                }
            }

            // 轉通知單位
            // 記錄於轉置清單
            trnsDepSids.add(assignDepSid);
            // 由分派單位中移除
            procNewAssignDepSids.remove(assignDepSid);
            // 加入通知單位
            procNewNoticeDepSids.add(assignDepSid);
        }

        // 將傳入清單, 置換為新的運算結果
        assignDepSids.clear();
        for (Integer depSid : procNewAssignDepSids) {
            assignDepSids.add(depSid);
        }
        noticeDepSids.clear();
        for (Integer depSid : procNewNoticeDepSids) {
            noticeDepSids.add(depSid);
        }

        return trnsDepSids;

    }

}
