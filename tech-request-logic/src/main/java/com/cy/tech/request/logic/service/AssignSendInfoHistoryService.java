package com.cy.tech.request.logic.service;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.OrgLevel;
import com.cy.tech.request.repository.require.AssignSendInfoHistoryRepository;
import com.cy.tech.request.vo.require.AssignSendInfoHistory;
import com.cy.tech.request.vo.require.AssignSendInfoHistoryVO;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * 分派通知紀錄服務
 */
@Component
@Slf4j
public class AssignSendInfoHistoryService implements InitializingBean, Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -4427432467260724690L;
    private static AssignSendInfoHistoryService instance;

	public static AssignSendInfoHistoryService getInstance() {
		return instance;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		AssignSendInfoHistoryService.instance = this;
	}

	/**
	 * 
	 */
	@Autowired
	private AssignSendInfoHistoryRepository assignSendInfoHistoryRepository;

	/**
	 * 查詢最早分派日
	 * 
	 * @param requireSid
	 * @return
	 */
	public Date findFirstAssignDate(String requireSid) {

		// ====================================
		// 查詢所有分派通知歷史資料
		// ====================================
		List<AssignSendInfoHistory> assignSendInfoHistorys = this.assignSendInfoHistoryRepository.findByRequireSidOrderByCreatedDateDescSidDesc(requireSid);

		if (WkStringUtils.isEmpty(assignSendInfoHistorys)) {
			return null;
		}

		// ====================================
		// 找出最早分派的一筆
		// ====================================
		for (AssignSendInfoHistory assignSendInfoHistory : assignSendInfoHistorys) {
			// 取出加派資料
			List<Integer> addAssignDepSids = parseDeps(assignSendInfoHistory.getAddAssignDeps());
			// 不為空時，代表最早分派日
			if (WkStringUtils.notEmpty(addAssignDepSids)) {
				return assignSendInfoHistory.getCreatedDate();
			}
		}
		return null;
	}

	/**
	 * 以需求單 SID 查詢
	 * 
	 * @param requireSid
	 * @return
	 */
	public List<AssignSendInfoHistoryVO> findAssignSendInfoHistory(String requireSid) {
		// 查詢
		List<AssignSendInfoHistory> results = this.assignSendInfoHistoryRepository.findByRequireSidOrderByCreatedDateDescSidDesc(requireSid);
		if (WkStringUtils.isEmpty(results)) {
			return Lists.newArrayList();
		}

		// 轉VO
		List<AssignSendInfoHistoryVO> resultsVOs = Lists.newArrayList();

		Integer depShowWidth = 100;

		for (AssignSendInfoHistory entity : results) {

			AssignSendInfoHistoryVO vo = new AssignSendInfoHistoryVO(entity);

			// 兜組顯示資訊
			List<Integer> addAssignDepSids = parseDeps(entity.getAddAssignDeps());
			vo.setShow_addAssignDeps(WkOrgUtils.findNameBySid(addAssignDepSids, "、", depShowWidth));
			vo.setShowTreeStyle_addAssignDeps(WkOrgUtils.prepareDepsNameByTreeStyle(addAssignDepSids, 30));

			List<Integer> deleteAssignDepSids = parseDeps(entity.getDeleteAssignDeps());
			vo.setShow_deleteAssignDeps(WkOrgUtils.findNameBySid(deleteAssignDepSids, "、", depShowWidth));
			vo.setShowTreeStyle_deleteAssignDeps(WkOrgUtils.prepareDepsNameByTreeStyle(deleteAssignDepSids, 30));

			List<Integer> addSendDepSids = parseDeps(entity.getAddSendDeps());
			vo.setShow_addSendDeps(WkOrgUtils.findNameBySid(addSendDepSids, "、", depShowWidth));
			vo.setShowTreeStyle_addSendDeps(WkOrgUtils.prepareDepsNameByTreeStyle(addSendDepSids, 30));

			List<Integer> deleteSendDepSids = parseDeps(entity.getDeleteSendDeps());
			vo.setShow_deleteSendDeps(WkOrgUtils.findNameBySid(deleteSendDepSids, "、", depShowWidth));
			vo.setShowTreeStyle_deleteSendDeps(WkOrgUtils.prepareDepsNameByTreeStyle(deleteSendDepSids, 30));

			// 執行者
			if (entity.getCreatedUser() != null) {
				vo.setShow_creater(
				        WkOrgUtils.makeupDepName(
				                WkUserUtils.prepareUserNameWithDep(
				                        entity.getCreatedUser(),
				                        OrgLevel.DIVISION_LEVEL,
				                        false,
				                        "-"),
				                "-"));
			}

			resultsVOs.add(vo);
		}

		return resultsVOs;

	}

	@SuppressWarnings("unchecked")
	private List<Integer> parseDeps(String jsonStr) {

		List<Integer> results = Lists.newArrayList();
		if (WkStringUtils.isEmpty(jsonStr)) {
			return results;
		}

		try {
			return WkJsonUtils.getInstance().fromJson(jsonStr, results.getClass());
		} catch (IOException e) {
			log.warn("json 資料轉換錯誤:【" + jsonStr + "】");
		}

		return results;
	}

}
