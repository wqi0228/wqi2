/**
 * 
 */
package com.cy.tech.request.logic.vo;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 *
 */
public class RequireConfirmDepTraceVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7414698397393055900L;

    /**
     * @param name             使用者名稱
     * @param date             時間
     * @param action           動作/行為
     * @param msg              執行說明
     * @param traceRowStyle    顯示樣式
     * @param reqConfirmDepSid 關連的需求完成單位確認檔 sid
     */
    public RequireConfirmDepTraceVO(
            String name,
            Date date,
            String action,
            String msg,
            String traceRowStyle,
            Integer reqConfirmDepSid) {

        this.name = name;
        this.date = date;
        this.action = action;
        this.msg = msg;
        this.traceRowStyle = traceRowStyle;
        this.reqConfirmDepSid = reqConfirmDepSid;
    }

    /**
     * 名稱
     */
    @Setter
    @Getter
    private String name;

    /**
     * 時間
     */
    @Setter
    @Getter
    private Date date;

    /**
     * 行為
     */
    @Setter
    @Getter
    private String action;

    /**
     * 訊息內容
     */
    @Setter
    @Getter
    private String msg;

    /**
     * 顯示樣式
     */
    @Setter
    @Getter
    private String traceRowStyle;

    /**
     * 關連的需求完成單位確認檔 sid
     */
    @Setter
    @Getter
    private Integer reqConfirmDepSid;
}
