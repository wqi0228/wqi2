/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.logic.search.view;

import com.cy.tech.request.vo.enums.RequireFinishCodeType;
import com.cy.work.common.enums.UrgencyType;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 待結案查詢 頁面顯示vo (search29.xhtml)
 *
 * @author jason_h
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class Search29View extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8119074179257954039L;
    /** 緊急度 */
    private UrgencyType urgency;
    /** 部 */
    private Boolean hasForwardDep;
    /** 個 */
    private Boolean hasForwardMember;
    /** 關 */
    private Boolean hasLink;
    /** 立單日期 */
    private Date createdDate;
    /** 需求類別 */
    private String bigName;
    /** 中類 */
    private String middleName;
    /** 小類 */
    private String smallName;
    /** 需求單位 */
    private Integer createDep;
    /** 需求人員 */
    private Integer createdUser;
    /** 確認附加檔案 */
    private Boolean checkAttachment;
    /** 是否有附加檔案 */
    private Boolean hasAttachment;
    /** 期望完成日 */
    private Date hopeDate;
    /** 執行天數 系統日-需求單開單日期 */
    private int execDays;
    /** 本地端連結網址 */
    private String localUrlLink;
    /** 統整人員 */
    private Integer inteStaffUser;
    /** 需求完成日 */
    private String requireFinishDate;
    /** 完成碼 */
    private RequireFinishCodeType finishCode;
}
