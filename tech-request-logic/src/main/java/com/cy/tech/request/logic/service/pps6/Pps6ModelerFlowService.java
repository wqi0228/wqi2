package com.cy.tech.request.logic.service.pps6;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.repository.pps6.Pps6ModelerFlowRepository;
import com.cy.tech.request.vo.pps6.Pps6ModelerFlow;
import com.cy.tech.request.vo.pps6.Pps6ModelerFlowVO;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

/**
 * PPS6 Modeler 流程定義資料檔 service
 * @author allen1214_wu
 */
@Service
public class Pps6ModelerFlowService implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 1711765304917462760L;
    // ========================================================================
	// 服務區
	// ========================================================================
	@Autowired
	private transient Pps6ModelerFlowRepository pps6ModelerFlowRepository;

	// ========================================================================
	// 方法區
	// ========================================================================
	/**
	 * 查詢全部
	 * @return
	 */
	public List<Pps6ModelerFlowVO> findAll() {
		// ====================================
		// 查詢全部
		// ====================================
		List<Pps6ModelerFlow> entities = this.pps6ModelerFlowRepository.findAll();
		if (WkStringUtils.isEmpty(entities)) {
			return Lists.newArrayList();
		}

		// ====================================
		// 轉 VO
		// ====================================
		return entities.stream()
		        .map(entity -> new Pps6ModelerFlowVO(entity))
		        .collect(Collectors.toList());
	}

	/**
	 * find and save
	 * 
	 * @param flowId
	 * @param flowName
	 * @param execUserSid
	 * @return
	 */
	public Pps6ModelerFlowVO findAndSave(String flowId, String flowName, Integer execUserSid) {

		Date execDate = new Date();

		// ====================================
		// 查詢
		// ====================================
		Pps6ModelerFlow pps6ModelerFlow = this.pps6ModelerFlowRepository.findByFlowId(flowId);

		// ====================================
		// 若不存在, 則新建一筆
		// ====================================
		if (pps6ModelerFlow == null) {
			pps6ModelerFlow = new Pps6ModelerFlow();
			pps6ModelerFlow.setFlowId(flowId);
			pps6ModelerFlow.setCreatedUser(execUserSid);
			pps6ModelerFlow.setCreatedDate(execDate);
		}

		// ====================================
		// save
		// ====================================
		pps6ModelerFlow.setFlowName(flowName);
		pps6ModelerFlow.setCreatedUser(execUserSid);
		pps6ModelerFlow.setUpdatedDate(execDate);

		pps6ModelerFlow = this.pps6ModelerFlowRepository.save(pps6ModelerFlow);

		// ====================================
		// to vo
		// ====================================
		return new Pps6ModelerFlowVO(pps6ModelerFlow);
	}

}
