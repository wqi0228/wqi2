# 技術需求系統 RELEASE NOTE
<HR width="35%" align="left">

## 7.15.3 
- [明細清單](http://werp-gateway.chungyo.net:8080/projects/REQ/versions/15644)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/tech-request+version+7.15.3)
> by Jimmy

## 7.5.0 
- [明細清單]( http://werp-gateway.chungyo.net:8080/projects/REQ/versions/15312 )
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/tech-request+version+7.5.0)
> by Allen


## 7.2.0 
- [明細清單](http://werp-gateway.chungyo.net:8080/projects/REQ/versions/15273)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/tech-request+version+7.2.0)
> by Allen

## 7.1.0 
- [明細清單](http://werp-gateway.chungyo.net:8080/projects/REQ/versions/15262)
- [環境確認清單](http://confluence.iwerp.net:8090/pages/viewpage.action?pageId=41741519)

> by Allen

## 7.0.0 
- [明細清單](http://werp-gateway.chungyo.net:8080/projects/REQ/versions/14879)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/tech-request+version+7.0.0)

> by Allen

## 6.8.0 
  1. 需求單位流程調整：市埸營銷部以下單位(含)建單流程，簽核流程組級不簽核直接由 部級->處級簽核 (20181011先行註冊至PROD)
  
  
## 6.7.0
[功能新增]
1.新增批次調整上線日報表，相關邏輯如下
    1-1.QA人員，可以看到此需求單下所有送測單(不包含 不納入排程狀態)
    1-2.非QA人員，可以看到此需求單下自己部門的送測單(不包含 不納入排程狀態)
    1-3.非QA人員可以看到同部門送測單據，故上線日的修改權限放寬至部門(原本是綁在建單者 和 建單者主管)
    1-4.新增 欄位按鈕，提供使用者增減欄位配置
2.填寫送測單時，挑選送測日當日狀態為滿時，彈出提示訊息(若送測單位不包含QA，則不需提醒)
3.預設上線日，將卡系統日之前的日期限制移除
4.修正日期元件，不再出現貼上第一次會失敗，不卡日期選擇限制
5.送測排程行事曆維護，新增事件時，名稱預設為"滿"，顏色為紅色
    5-1.將原本一個跨數天事件(1條bar)，將拆分成多條bar 顯示
6. 送測排程表
	1-1.新增「上週」、 「本週」、「下週」按鈕
	1-2.預設的區間改為今日，如 9/27 ~ 9/27
	1-3.送測日欄位，新增Filter輸入框
[功能修改]
1.收呈報報表，開啟會出現頁面錯誤 (已修正)
2.日期修改後，右邊歷程沒有即時更新 (已修正)
3.待辦事項，送測-退測顯示邏輯如下
	a.非退測狀態時，不再顯示
	b.非退測當天日期時，不再顯示(等於只顯示今日退測數量)

## 6.6.0
	[已分派單據查詢]增加日期條件(異動日期)
	組織異動轉單時，造成PMIS資料錯誤
## 6.5.2
	修正需求單獨立DB拆分, 所測到的BUG
## 6.5.1
	送測日期修改時出現異常
## 6.5.0
    [QA待審單據查詢] 增加功能按鈕
    [送測排程表]同一日的資訊使用合併儲存格
    調整拆分DB直接join user org 以及 使用fusion schema
        
## 6.4.1
	[逾期未完工需求報表] 尚未完工 且 超過期望完成日的單據才要進行顯示
## 6.4.0
	送測排程日期效能調整
	以及測試人員按鈕權限挑整
## 6.3.1
	WERP 轉 PMIS連結異常(開啟url網址錯誤)
## 6.3.0
	針對轉寄單位的主管以及上層的主管們均可透過url連結開啟該需求單
## 6.2.0
	送測排程需求變更調整
	報表sql調整(inner join)
#6.1.0
	需求單後台設定-批次轉單程式
#6.0.0
	送測排程第一階段上線
#5.16.0
	增加PMIS系統對接
#5.15.0
	新增組織異動後台設定
#5.14.0
	新增批次追蹤已完成按鈕實作
#5.13.0
 -REQ-1042 原型確認一覽表報表增加原型確認單號欄位
 -REQ-1041 其他資料設定報表增加其他資料設定單號欄位
#5.7.2
 -REQ-1014 已經結案的需求單再次on程式應該要檔掉
 -REQ-1013 增加轉fb log
#5.0.0
- 分點公司。
- REQ-956 [需求單]自動已完成功能異常

#4.19.0
- REQ-794 需求完成人員調整issue。
- REQ-815 on程式折疊bar顯示是否自動變更為已完成。
- REQ-856 需求暫緩原因框有兩個冒號。
- REQ-860 切換內容( 需求單查詢/ 未結案單據查詢 )在點擊搜尋後才出現切換內容.Failed Rate:100%。
- REQ-863 各子程序報表填單單位欄位增加datatable filter下拉選單。
- REQ-901 ON程式點檢查完成,需求製作進度連動有問題。
- REQ-909 關聯的需求單已結案的顯示 灰底。
- REQ-917 需求單增加ncs執行完成功能按鈕。
- REQ-948 模糊搜尋don't會顯示錯誤。

#4.18.0
- REQ-934 [技術需求單-需求單位簽核 ]-代理簽核套用。
- REQ-935 [技術需求單-技術單位簽核 ]-代理簽核套用。
- REQ-936 [技術需求單-原型確認]-代理簽核套用。
- REQ-937 [技術需求單-送測 ]-代理簽核套用。

- tech-request-rest-logic
<BR/><span style="color:red;">【dependencyManagement】bpm-rest-client【1.7.0】->【1.15.0】</span>
<BR/><span style="color:red;">【移除】【dependencyManagement】bpm-vo</span>

<BR/><span style="color:red;">【移除】bpm-vo</span>
<BR/><span style="color:red;">【新增】form-sign-api【3.10.0-SNAPSHOT】</span>
<BR/><span style="color:red;">【移除】formsigning-impl-base</span>

#4.17.0
REQ-941 已分派單據查詢很慢。

#4.16.1

#4.16.0
- REQ-915 請將需求單WEB與REST專案進行合併。
- REQ-923 將ONPG LIB合併至TECH-REQUEST專案中。
 -REQ-932 需求單-未分派單據查詢/待檢查確認單據查詢 - 模糊搜尋尋輸入「don't」提示網頁錯誤 -NG

- Logic
<BR/><span style="color:red;">【移除】work-onpg-logic</span>

4.15.1
- REQ-908 新增關聯需求單。

#4.15.0
- REQ-826 因應DBA工作需要的欄位互動，需新增元件。
- REQ-841 [類別設定-小類]可自定義此類別的需求方核決權限。
- REQ-844 開放沙巴的需求單需要有額外的表單進行查詢。
- REQ-858 模糊搜尋 若輸入縮寫符號會提示網頁錯誤。
- REQ-864 關聯檢視dialog顯示立案日期與提出單位欄位。
- REQ-871 報表-原型確認-待功能確認欄位名稱有錯。
- REQ-878 已分派報表增加分派日欄位與分派區間搜尋元件。
- REQ-879 小類基本設定增加[是否顯示於案件單轉需求單的dialog]選項。
- REQ-883 案件單轉需求單可以轉多次。
- REQ-898 需求單報表關聯檢視無權限的案件單會整個畫面跳頁面不見了。
- REQ-900 邦妮點編輯-取消-簽名，單據狀態不會變更。

- Logic
<BR/><span style="color:red;">work-mapp-create-trans-logic【2.3.0】->【2.4.0】</span>
<BR/><span style="color:red;">tech-issue-client【4.0.0-SNAPSHOT】->【4.2.0】</span>
<BR/><span style="color:red;">work-group-logic【2.2.2】->【2.3.0】</span>
- Vo
<BR/><span style="color:red;">work-group-vo【2.2.1】->【2.3.0】</span>

#4.14.0
- REQ-789 editer元件判斷tag異常。
- REQ-804 因應結案後ON程式的單據事後仍須進行結案管理故需調整相關資訊。
- REQ-839 [原型確認]最後一位簽核主管簽名後,原型確認狀態=原型確認已核准。

#4.13.0
-REQ-835 技術案件單-待檢查確認單據查詢 (已檢查) 或 (已分派)，點放大鏡後或切換下一筆時，新網頁是置中。

#4.12.0
-REQ-799 通知單位挑選檔的整個螢幕～也不能移動
-REQ-813 未結案報表增加轉入需求欄位
-REQ-822 需求暫緩時，請將需求完成日寫入。
-REQ-829 執行狀況一覽表搜尋區增加元件。
-REQ-831 待辦事項-原型確認資訊
-REQ-832 待辦事項-其他資料設定資訊
-REQ-833 子程序通知單位異動紀錄。
-REQ-848 原型確認(成員) 進行查詢導致資料洗掉。
-REQ-850 原型確認-待功能確認的待辦事項的文案與規格書不符。
-REQ-851 其他資料設定 - 待完成的待辦事項的文案與規格書不符。
-REQ-854 原型確認-待功能確認報表的核准日缺少時分秒。
-REQ-857 打開未結案單據查詢，頁面跳網頁錯誤。
-REQ-862 關聯檢視會跳權限不足，無法閱讀。

#4.11.0
-REQ-807 修正逾時錯誤
#4.10.0
-REQ-817 創間需求單元件radio button與主題欄位互動的元件調整
#4.9.0
- REQ-812 自動產生on程式的需求單-第一次新增會產生css語法
#4.8.0-2017/03/09
- REQ-787 待辦事項增加其他子程序的待辦資訊
- REQ-793 反需求issue
- REQ-806 ON程式待檢查完成的待辦事項提醒邏輯調整
- tech-request-vo
<BR/><span style="color:red;">【2.3.1】 -> 【2.4.0】work-notify-vo</span>
- tech-request-logic
<BR/><span style="color:red;">【2.3.1】 -> 【2.4.0】work-notify-logic</span>
- REQ-781 報表自定義
- REQ-782 報表自定義-需求單簽核進度查詢
- REQ-783 報表自定義-退件資訊查詢
- REQ-784 報表自定義-原型確認一覽表
- REQ-785 報表自定義-送測異動明細表
- REQ-786 報表自定義-ON程式異動明細表
- REQ-802 [期望完成日]按鈕權限開放給GM可以執行

#4.7.0-SNAPSHOT 2017/02/16
- 報表查詢自訂義 (V2)
- REQ-779 新包網資料提供模版的開放遊戲元件放置方式調整
- REQ-775 附加檔案異動要變更為待閱讀
- REQ-776 點選 需求單- 轉發 裡面 為什麼會有顯示 停用的部門。

#4.6.0 2017-01-19
- REQ-727 結案後各子程序仍可以使用的回覆按鈕與編輯按鈕 (V2)
- REQ-733 新增送測時，可以自行填寫FBID
- REQ-762 需求單查詢ForGM選All，單位挑選後，立分轉的顯示問題。
- REQ-766 需求單查詢(GM)元件互動調整
- REQ-773 登入者執行某些事件後,需求單報表對登入者還是維持待閱讀的狀態
- REQ-774 ON程式回覆區塊 - 因拉罷移動區域不大導致文字擋住了

#4.5.1 2017-01-11
- REQ-768 點退測後再度重測狀態不會改變
- REQ-769 QA轉測試報告的狀態沒有進入到送測歷程中
- REQ-770 自己異動的需求單,應該不要顯示待閱讀
- REQ-771 加大 viewScope 數量上限

#4.5.0 2016-12-29
- REQ-284 轉寄個人增加回收機制
- REQ-499 需求單查詢For GM用 (V2)。
- REQ-613 點選分派按鈕增加系統防呆提醒。
- REQ-749 送測頁籤中的功能存檔防呆
- REQ-750 其他資料設定頁籤中的功能存檔防呆
- REQ-751 原型確認頁籤中的功能存檔防呆
- REQ-753 ON程式通知單位-清單模式-若有勾選含子單位-停用的子單位不應該顯示。
- REQ-754 送測通知單位-清單模式-若有勾選含子單位-停用的子單位不應該顯示。
- REQ-755 其他資料設定通知單位-清單模式-若有勾選含子單位-停用的子單位不應該顯示。
- REQ-756 原型確認通知單位-清單模式-若有勾選含子單位-停用的子單位不應該顯示。
- REQ-759 PICK LIST 清單模式問題調整。
- REQ-764 收費金額一覽表 子類別樹優化。

#4.4.0 2016-12-22
- 修正需求單主題顯示。
- 報表查詢欄位自訂義。
- REQ-499 需求單查詢For GM用。
- REQ-706 附加檔案頁籤排序。
- REQ-715 案件單轉需求單的資訊請請額外獨立頁籤方便快速辨識。
- REQ-714 轉單程式-分派程序增加可以依照單號進行轉單。
- REQ-718 第二分類維護設定檔。
- REQ-684 已分派 加上 是否閱讀 的搜尋欄位
- REQ-705 需求單內部需求要跑流程的單據停用單位目前有顯示
- REQ-741 原型確認核准，通知單位與通知成員才能在報表中看到資訊
- REQ-735 需求單查詢增加轉入需求欄位
- REQ-734 修改的需求單 變成空白的
- REQ-737 組織樹 自動判斷是否預設要勾選全選
- REQ-760 查理從單據簽核作業開啟需求單失敗

- tech-request-repository
<BR/><span style="color:red;">【2.2.1】 -> 【2.3.0】work-mapp-create-trans-logic</span>
- tech-request-logic
<BR/><span style="color:red;">【2.2.1】 -> 【2.2.2】work-group-logic</span>

#4.3.1 2016-12-15
- REQ-748 ON程式頁籤中的功能存檔防呆

- tech-request-logic
<BR/><span style="color:red;">【2.2.1】 -> 【2.2.2】work-onpg-logic</span>

#4.3.0 2016-12-01
- REQ-698 ON程式檢查完成功能增加可執行的特殊角色。
- REQ-699 需求單自動完成後自動結案機制

- tech-request-repository
<BR/><span style="color:red;">【4.2.0】 -> 【4.3.0】tech-request-vo</span>

#4.2.0 2016-11-17
- REQ-671 需求單結案後可執行的按鈕功能調整
- REQ-645 使用模糊搜尋可以輸入單號後進行查資料
- REQ-647 有權限的同仁可以透過點擊超連結開啟需求單
- REQ-712 追蹤報表 - 請把空白追蹤提醒日拿掉

- tech-request-repository
<BR/><span style="color:red;">【4.1.0】 -> 【4.2.0】tech-request-vo</span>
- tech-request-logic
<BR/><span style="color:red;">【2.1.0-SNAPSHOT】 -> 【2.2.1】work-customer-repository</span>
<BR/><span style="color:red;">【2.1.0-SNAPSHOT】 -> 【2.2.1】work-pt-logic</span>
<BR/><span style="color:red;">【2.1.0-SNAPSHOT】 -> 【2.3.1】work-test-logic</span>
<BR/><span style="color:red;">【2.1.0-SNAPSHOT】 -> 【2.2.1】work-onpg-logic</span>
<BR/><span style="color:red;">【2.1.0-SNAPSHOT】 -> 【2.2.1】work-backend-logic</span>
<BR/><span style="color:red;">【2.1.0-SNAPSHOT】 -> 【2.2.1】work-trace-repository</span>
<BR/><span style="color:red;">【2.1.0-SNAPSHOT】 -> 【2.2.1】tech-customer-client</span>
<BR/><span style="color:red;">【2.2.0】 -> 【2.3.1】work-notify-logic</span>
<BR/><span style="color:red;">【2.1.0-SNAPSHOT】 -> 【2.2.1】work-mapp-create-trans-logic</span>
<BR/><span style="color:red;">【2.1.0-SNAPSHOT】 -> 【2.2.1】work-group-logic</span>

#4.1.1 2016-10-27
- 修正 轉寄個人選項 排序

#4.1.0 2016-10-21
- REQ-652 推撥機制類型自定義
- REQ-693 收藏夾與追蹤的筆數不見了
- REQ-694 追蹤報表排序調整

- tech-request-logic
<BR/><span style="color:red;">【2.1.0-SNAPSHOT】 -> 【2.2.0】work-notify-logic</span>

#4.0.0-SNAPSHOT 2016-10-22
- REQ-587 需求單優化-ON程式一覽表
- REQ-610 需求單-快選區設定是否可以增加"全選"、"全取消"                
- REQ-640 追蹤報表排序異常
- REQ-644 管理報表切半版時,不管點擊哪個地方都要能夠進行切換下一則資訊  
x REQ-648 需求單附加檔案可刪除的權限調整(待完成)
- REQ-649 需求製作進度報表效能優化
- REQ-657 ORG TREE HIGHLIGH 修正
- REQ-421 套用共用的Lib(COMMON-VO)

- tech-request-logic
<BR/><span style="color:red;">【4.0.0-SNAPSHOT】tech-issue-client</span>

#3.6.2-SNAPSHOT 2016-10-6
- REQ-650 需求資訊回覆補充附加檔案預設核取方塊要打勾

#3.6.1-SNAPSHOT 2016-09-22
- 3.4.3-SNAPSHOT(hotfix)          
- 調整logback                     
- 後台分派 還原機制               
- TECH-844 案件單轉需求單ONPG         
- REQ-551  ON程式檢查完成功能邏輯調整 
- REQ-552  案件單轉入報表             
- REQ-497  分派頁籤資訊顯示調整                              
- REQ-533  執行狀況查詢                                      
- REQ-550  子程序加入切換內容功能                            
- REQ-589  開啟需求單移動分頁追加                            
- REQ-581  其它設定資訊優化         
- REQ-498  需求單進度報表           
- REQ-624  需求製作進度報表調整
- REQ-588  原型確認增加通知部門     
- REQ-592  需求單轉單程式          

#3.5.0-SNAPSHOT 2016-08-18
- TECH-884 因應問題而產生的on程式。
- REQ-577 推撥次數調整優化
- tech-request-repository
<BR/><span style="color:red;">【3.5.0-SNAPSHOT】tech-request-vo</span>
- tech-request-logic
<BR/><span style="color:red;">【1.0.0-SNAPSHOT】work-mapp-create-trans-logic</span>
<BR/><span style="color:red;">【3.4.0-SNAPSHOT】tech-issue-client</span>
<BR/><span style="color:red;">【1.2.0-SNAPSHOT】work-notify-logic</span>

#3.4.0-SNAPSHOT 2016-0819
- REQ-530 需求單複製功能                
- REQ-531 需求單草稿功能                
- REQ-532 草稿查詢                      
- REQ-539 子程序回覆框跑版                     
- REQ-543 ON程式一覽表在閱讀時 很容易卡住            
- REQ-553 QA反應點QA轉測試報告異常               
- 修正Portal登錄問題 (變更查詢已分派SQL語法)    
- 後台分派功能實作                            
- REQ-555 模糊搜尋特殊符號查詢失敗            
- REQ-520 子程序顯示內容排版錯誤              
- REQ-561 下載檔案失效                        
- 套入HikariCP DataSource                     

#3.3.0-SNAPSHOT 2016-07-21
- REQ-493 子程序通知單位多餘公司名稱                       
- REQ-524 基礎結構調整(sonar-20160713)                     
- REQ-525 點選 寄發至 的另開視窗，資訊不會隨著視窗拉大變大 
- REQ-526 從新版單據簽核作業進入的原型確認,功能按鈕壞掉了   
- REQ-527 原型確認預設通知對象有問題                       
- REQ-528 ON程式頁籤資訊顯示異常                           
- REQ-529 附加檔案-單一檔案上傳限制顯示異常            
- REQ-534 因應內部需求類別基本設定增加邏輯                 

#3.2.0-SNAPSHOT 2016-07-14
- REQ-496 ON程式一覽表預設資料範圍調整   
- REQ-508 ON程式-GM回覆功能調整          
- REQ-511 ON程式回覆重新整理後，再一次的點鉛筆存檔失敗 
- REQ-512 原型確認編輯邏輯調整            
- REQ-513 浮動視窗邏輯調整   
- REQ-515 結案執行邏輯調整            
- REQ-516 效能優化(移除autoupdate)       
- REQ-517 基礎結構調整(UserMBean)        
- REQ-518 基礎結構調整(xhtml-unit)       

#3.1.0-SNAPSHOT 2016-0707
- REQ-388 部、個、關欄位同案件欄寬        
- REQ-444 更新閱讀資訊異常
- REQ-490 更新閱讀資訊異常                
- REQ-485 ON程式一覽表菜單筆數異常        
- REQ-491 需求單查詢方式調整(立分轉)      
- REQ-494 寄件備份搜尋區增加部門搜尋元件
- REQ-500 報表統整人員欄位無法定義關閉    
- REQ-502 Log系統錯誤(16/07/01-01 )       
- REQ-503 編輯存檔失敗      
- REQ-505 優化效能(附加檔案)              
- REQ-506 收件夾調整日期時間格式          
- REQ-507 ON程式回覆無法編輯                            

#3.0.0-SNAPSHOT 2016-06-30
- REQ-451 00_需求單內部流程實作     
- REQ-462 01_類別基本資料建立設定作業（程式調整）       
- REQ-463 01_類別設定作業增加角色權限設定                     
- REQ-465 02_新增需求單                               
- REQ-466 03_on程式功能調整與頁籤資訊調整          
- REQ-437 99_原型確認簽核層級調整 (bpm 調整)
- REQ-459 99_專案製作部的需求單位簽核流程只簽核一層 (bpm 調整)
- REQ-452 88_需求單查詢調整        
- REQ-453 88_未結案單據查詢調整    
- REQ-454 88_已分派單據查詢調整    
- REQ-455 88_其他資料設定報表調整  
- REQ-457 04_需求單結案權限調整    
- REQ-467 04_結案邏輯調整          
- REQ-346 案件單與需求單要可以互相關連 
- REQ-306 MSN浮動視窗的通知機制        
- REQ-456 頁碼顯示不正確，異動區間再次

#1.14.0 2016-06-08
- REQ-425 需求資訊回覆補充dialog可以拖拉大小
- REQ-449 點各頁籤中的編輯時,所彈跳的dialog要可以拉大(記得要可以移動！)
- REQ-450 需求資訊回覆補充-視窗無法移動
- REQ-469 需求資訊回覆補充編輯調整
- REQ-470 需求資訊回覆補充版面區塊調整
- REQ-474 變更分類確認碼改型態為 VARCHAR(10)
> 異動需求單主檔欄位 <BR/>
> ALTER TABLE `fusion`.`tr_require` CHANGE COLUMN `category_confirm_code` `category_confirm_code` VARCHAR(10) NOT NULL COMMENT '分類確認碼' ;<BR/>
> 異動已經檢查確認的 ＝Y <BR/>
> UPDATE  fusion.tr_require SET category_confirm_code= 'Y' WHERE category_confirm_code='1';<BR/>
> 異動尚未檢查確認的 ＝N <BR/>
> UPDATE  fusion.tr_require SET category_confirm_code='N' WHERE category_confirm_code='0';<BR/>
> 異動需求單菜單數量查詢語法欄位 <BR/>
> UPDATE `fusion`.`tr_fun_item` SET `count_sql`='SELECT COUNT(*) FROM tr_require r WHERE category_confirm_code = \'Y\' AND has_assign = 0 AND require_suspended_code = 0' WHERE `url`='/search/search06.xhtml';
- REQ-475 分派失敗
> hot-fix AND 1.14.0

#1.13.0 2016-06-02
- REQ-442 需求資訊回覆補充的回覆
- REQ-458 需求單流程更新排程
- REQ-445 getReadRecordStr方法進行log記錄

#1.12.0 2016-05-26 
- REQ-390 送測  -送測單位點編輯,僅能調整送測單位
- REQ-391 ON程式-通知單位點編輯,僅能調整通知單位
- REQ-413 需求單單據簽核效能調整
- REQ-418 需求單轉寄個人後～被轉寄的人看不到留言
- REQ-420 轉寄個人Dialog請於案件單的一致
- REQ-438 變更bpm-rest-client引用

#1.11.0 2016-05-12
- REQ-411 套用新版的formsign-web
- REQ-415 已分派單據查詢邏輯變更
- REQ-416 待檢查分類查詢邏輯變更
- REQ-419 需求單簽核進度查詢的邏輯要調整
- REQ-402 未結案單據查詢資料範圍顯示調整
- REQ-414 需求單查詢邏輯變更
- REQ-428 需求單轉FB要帶入標籤
- REQ-430 執行長的收件夾邏輯調整
<BR/>fusion.work_backend_param 加入新參數 data 
<BR/>&nbsp;&nbsp; INSERT INTO `fusion`.`work_backend_param` (`sid`, `keyword`, `content`, `description`) VALUES ('10', 'config.big.boss.sids', '72,82,146', 'JK、福哥、松哥 user sid'); 
- REQ-417 其他資料設定報表增加填單人員欄位
- REQ-424 收件夾排序方式調整

#1.10.0 2016-05-05
- REQ-412 收部門轉寄的排序方式
- REQ-422 其他資料設定網頁頁籤名稱請統一
- REQ-423 已分派單據查詢 - 欄位設定 裡面的是否閱讀選項故障

#1.9.0 2016-04-28
- REQ-405 分派/ 轉寄 當選擇部門時,要連同該部門底下的單位都要帶入
- REQ-310 追蹤功能		 
- REQ-314 請在功能清單-ON程式一覧表後方加上未讀數量
- REQ-408 on程式編輯邏輯調整

#1.8.0 2016-04-28
- REQ-246 【測試提醒清單調整                                         
- REQ-374 【需求單個人群組設定議題(比照案件單的設計)                 
- REQ-370 【ON程式資訊內容呈現調整 
- REQ-389 【統一其它設定資訊名稱                                     
- REQ-393 【原型確認 / 送測 / ON程式一覽表的填單日期,要顯示到時分秒  

#1.7.0
- REQ-373 【分派資訊呈現希望跟fb頁籤設計雷同   
- REQ-83  【分派通知 若移除某單位 要有記錄顯示 
- REQ-355 【當分派的部門，沒增加減少時，應該不用再寫一筆紀錄吧
- REQ-399 【期望完成日未更新成功               

#1.6.0
- REQ-193 【新增模版元件           
- REQ-345 【pony簽核的流程變更     

#1.5.0
- REQ-255 【點選需求單單頭的按鈕功能需防呆（若同時有兩個人以上共同編輯時） 
- REQ-375 【待辦事項 
- REQ-301 【報表
- REQ-302 【報表
- REQ-392 【報表增加欄位与下拉選單

#1.4.0
- REQ-291 【子程序功能編輯按鈕權限調整                                     
- REQ-344 【需求單待閱原因實作                                         
- REQ-361 【未結案單據查詢,若進度＝需求暫緩,則顯示在報表中以紅色字呈現 
- REQ-362 【已分派單據查詢,若進度＝需求暫緩,則顯示在報表中以紅色字呈現

#1.3.0
- REQ-251 【需求暫緩在特定條件下未刷新表單狀態        
- REQ-300 【已分派單據查詢-增對新進案的單據顯示粗體   
- REQ-299 【已分派單據查詢增加切換內容的功能
- REQ-328 【後端清cache工具                           
- REQ-329 【流程重置後台
- REQ-331 【原型確認通知功能行為調整                  
- REQ-342 【點需求完成前,若沒有子程序,請先彈跳提示訊息
- REQ-350 【需求單作廢邏輯調整                        
- REQ-354 【已分派查詢增加核取選項                    
- REQ-368 【調整客戶資料排程設定                      
- REQ-336 【會員服務處的需求單要簽核到wind

#1.2.0
- REQ-325 【收藏功能調整                            
- REQ-334 【管理報表下拉選單需求製作進度增加"已完成"
- REQ-268 【主題後面預設顯示 廳主名稱＠後置碼       
- REQ-335 【需求單 廳主 與 提出客戶 顯示調整        
- REQ-337 【需求資訊回覆補充顯示於前端頁面格式調整  
- REQ-326 【檢查確認執行分派前，不觸發相關程序      
- REQ-340 【追蹤頁籤，不顯示停止狀態資訊            
- REQ-338 【已閱讀/未閱讀/待閱讀目前異常       


#1.1.0
- REQ-197 【需求單維護作業]從報表點放大鏡另開新頁也要有上下筆
- REQ-283 【收件夾是否閱讀下拉選單可選項目異常               
- REQ-289 【需求單也需要有報表快選區（設計比照案件單）
<P/>  增加表格 
<BR/>&nbsp; fusion.tr_homepage_favorite
<BR/> 調整資料 
<BR/>&nbsp; fusion.tr_fun_item_group
<BR/>&nbsp; fusion.tr_fun_item
<BR/>&nbsp; fusion.ad_menuitem
<P/> 
- REQ-307 【期望完成日按鈕追加邏輯                           
- REQ-308 【未結案單據查詢 請不要顯示作廢的單據              
- REQ-309 【歷史單據查詢 - 增加作廢的單據
- REQ-311 【未結案單據查詢：預設顯示 立單區間＝系統日-30天 未結案的需求單
- REQ-312 【歷史單據查詢：預設顯示 立單據間＝系統日-30 已結案的需求單
- REQ-320 【結案後各子程序頁籤中按鈕邏輯                     
- REQ-321 【邦妮開單簽名前端拋出錯誤資訊   

#1.0.0
- REQ-288 【需求單報表功能清單筆數需與明細表實際筆數連動
- REQ-252 【收件夾功能清單筆數沒有連動     
- REQ-305 【需求單查詢邏輯增加                          
- REQ-280 【需求暫緩 / 需求完成  子流程按鈕的權限異常
