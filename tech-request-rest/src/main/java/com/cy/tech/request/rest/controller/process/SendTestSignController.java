/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.rest.controller.process;

import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.formsigning.enums.FormSigningType;
import com.cy.formsigning.enums.LanguageType;
import com.cy.formsigning.enums.QueryType;
import com.cy.formsigning.rest.impl.BaseFormSigningRestService;
import com.cy.formsigning.vo.BatchSignedResult;
import com.cy.formsigning.vo.ButtonInfo;
import com.cy.formsigning.vo.FormInfo;
import com.cy.tech.request.rest.logic.process.SendTestSignForRestService;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 送測簽核
 *
 * @author shaun
 */
@RestController
@RequestMapping("/process/send-test")
public class SendTestSignController extends BaseFormSigningRestService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7027670137158755698L;

    public SendTestSignController() {
        super(SendTestSignController.class);
    }

    public SendTestSignController(Class<? extends BaseFormSigningRestService> implClass) {
        super(implClass);
    }

    @Autowired
    private SendTestSignForRestService service;

    /**
     * 單據單號及使用者 uuid 查詢該單據
     *
     * @param formId
     * @param executorUuid
     * @return
     */
    @Override
    public FormInfo getFormImpl(final String formId, final String executorUuid, final LanguageType lang) {
        return service.findByFormId(formId, executorUuid, lang);
    }

    /**
     * 查詢該使用者所有待簽單據
     *
     * @param executorUuid
     * @return
     * @throws Exception
     */
    @Override
    public List<FormInfo> getFormsOfUnsingingImpl(final String executorUuid, final LanguageType lang) throws Exception {
        try {
            return service.findUnSignByUser(executorUuid, lang);
        } catch (ProcessRestException ex) {
            throw new Exception(ex);
        }
    }

    /**
     * 依 公司uuid、送簽起起迄日期、使用者uuid 查詢該使用者所有已簽單據
     *
     * @param orgUuid
     * @param from
     * @param to
     * @param executorUuid
     * @return
     * @throws Exception
     */
    @Override
    public List<FormInfo> getFormsOfSignedByCompanyImpl(
            final String orgUuid,
            final Date from,
            final Date to,
            final String executorUuid,
            final LanguageType lang,
            final QueryType queryType) throws Exception {
        try {
            return service.findSignedByUserAndOrg(orgUuid, from, to, executorUuid, lang);
        } catch (ProcessRestException ex) {
            throw new Exception(ex);
        }
    }

    /**
     * 依 送簽起迄日期、使用者uuid 查詢該使用者所有已簽單據
     *
     * @param from
     * @param to
     * @param executorUuid
     * @return
     * @throws Exception
     */
    @Override
    public List<FormInfo> getFormsOfSignedImpl(
            final Date from,
            final Date to,
            final String executorUuid,
            final LanguageType lang,
            final QueryType queryType) throws Exception {
        try {
            return service.findSignedByUser(from, to, executorUuid, lang);
        } catch (ProcessRestException ex) {
            throw new Exception(ex);
        }
    }

    /**
     * 依 單據單號 及 使用者uuid 查詢該使用者在該單據要顯示的簽核按鈕
     *
     * @param formId
     * @param executorUuid
     * @return
     */
    @Override
    public List<ButtonInfo> getButtonsImpl(final String formId, final String executorUuid, final LanguageType lang) {
        return service.combinFormBtn(formId, executorUuid);
    }

    /**
     * 以 使用者uuid 身份對 formIds 進行批次簽核
     *
     * @param formIds
     * @param executorUuid
     * @return
     */
    @Override
    public BatchSignedResult batchSigningImpl(final List<String> formIds, final String executorUuid, final LanguageType lang) throws Exception {
        return service.doBatchSign(formIds, executorUuid);
    }

    /**
     * 執行簽核相關作業
     *
     * @param action
     * @param inputData
     * @param executorUuid
     * @return
     */
    @Override
    public FormInfo doActionImpl(final String action, final Map<String, Object> inputData, final String executorUuid, final LanguageType lang) throws Exception {
        try {
            return service.doActionImpl(action, inputData, executorUuid, lang);
        } catch (ProcessRestException ex) {
            throw new Exception(ex);
        }
    }

    /**
     * 查詢該單據對該使用者而言是處於什麼狀態
     *
     * @param formId
     * @param executorUuid
     * @return
     * @throws Exception
     */
    @Override
    public FormSigningType getFormSigningTypeImpl(String formId, String executorUuid) throws Exception {
        return service.findFormTypeForUser(formId, executorUuid);
    }

    @Override
    public void validateExecutor(String executorUuid) {
        //throw new UnsupportedOperationException("todo...");
    }

    @Override
    public Map<String, Map<QueryType, Map<String, Integer>>> getTodoSummaryImpl(String executorUuid) throws Exception {
        return service.getUnSignCount(executorUuid);
    }

    @Override
    public List<FormInfo> getFormsOfUnsingingImpl(String executorUuid, Integer companySid, LanguageType lang,
            QueryType queryType) throws Exception {
        try {
            return service.findUnSignByUser(executorUuid, companySid, lang);
        } catch (ProcessRestException ex) {
            throw new Exception(ex);
        }
    }
}
