package com.cy.tech.request.rest.controller.schedule;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cy.commons.util.Base64Utils;
import com.cy.tech.request.logic.service.NotificationService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.reqconfirm.RequireConfirmDepService;
import com.cy.tech.request.logic.service.setting.onetimetrns.OneTimeTrnsForRequireDepCommonService;
import com.cy.tech.request.logic.service.setting.onetimetrns.OneTimeTrnsForRequireDepRebuildService;
import com.cy.tech.request.logic.service.setting.onetimetrns.to.RequireDepRebuildTo;
import com.cy.tech.request.logic.service.setting.sysnotify.SettingSysNotifyService;
import com.cy.tech.request.logic.service.syncmms.SyncMmsDominLogic;
import com.cy.tech.request.logic.service.syncmms.SyncMmsRequireDataLogic;
import com.cy.tech.request.logic.service.syncmms.SyncMmsSyncFormLogic;
import com.cy.tech.request.rest.logic.schedule.ScheduleAssignUnReadNoticeService;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.work.client.WorkCustomerSyncClient;
import com.cy.work.client.syncgm.SyncGMClient;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.logic.lib.RestUtil;
import com.cy.work.common.logic.lib.jsf.WorkSpringContextHolder;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.customer.logic.WorkCustomerSyncService;
import com.cy.work.customer.vo.to.RetTo;
import com.cy.work.notify.logic.manager.WorkNotifyManager;
import com.google.common.collect.Lists;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.HikariPoolMXBean;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * house keeping
 * 
 * @author aken_kao
 */
@RestController
@RequestMapping("/schedule")
@Slf4j
public class ScheduleController {

    @Autowired
    private transient NotificationService notificationService;
    @Autowired
    private transient WorkNotifyManager workNotifyManager;
    @Autowired
    private transient SettingSysNotifyService settingSysNotifyService;
    @Autowired
    private transient ScheduleAssignUnReadNoticeService scheduleAssignUnReadNoticeService;
    @Autowired
    private transient WorkCustomerSyncClient workCustomerSyncClient;
    @Autowired
    private transient WorkCustomerSyncService workCustomerSyncService;
    @Autowired
    private transient SyncGMClient syncGMClient;
    @Autowired
    private transient SyncMmsDominLogic syncMmsDominLogic;
    @Autowired
    private transient SyncMmsRequireDataLogic syncMmsRequireDataLogic;
    @Autowired
    private transient SyncMmsSyncFormLogic syncMmsSyncFormLogic;
    @Autowired
    private transient RequireConfirmDepService requireConfirmDepService;
    @Autowired
    private transient OneTimeTrnsForRequireDepRebuildService requireDepRebuildService;
    @Autowired
    private transient OneTimeTrnsForRequireDepCommonService requireDepCommonService;
    @Autowired
    private transient RequireService requireService;

    /**
     * 成員分派未讀提醒, for 主管
     * 
     * @return
     */
    @ApiOperation(value = "成員分派未讀提醒, for 主管")
    @RequestMapping(value = "/assignUnReadNoticeForManager", method = RequestMethod.GET)
    @Deprecated
    public ResponseEntity<String> assignUnReadNoticeForManager() {
        try {
            this.scheduleAssignUnReadNoticeService.sendToManagerByMemberDispatchNoticeUnRead();
        } catch (Exception e) {
            return RestUtil.createErrorResponse(e);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "text/html; charset=utf-8");
        return new ResponseEntity<String>("OK", headers, HttpStatus.OK);
    }

    /**
     * 移除停用帳號推撥相關資料 (通知檔、設定檔)
     * 
     * @return result
     */
    @ApiOperation(value = "housekeeping (每天一次)")
    @RequestMapping(value = "/housekeeping", method = RequestMethod.GET)
    public ResponseEntity<String> housekeeping() {
        try {
            // 移除停用帳號通知資料 （QA）
            notificationService.housekeeping();
            // 移除停用帳號、一個月前推撥
            workNotifyManager.housekeeping();
            // 移除停用帳號系統通知設定
            settingSysNotifyService.housekeeping();
            // 自動維護『需求確認單位』負責人
            requireConfirmDepService.keepConfirmDepOwner();

            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "text/html; charset=utf-8");
            return new ResponseEntity<String>("OK", headers, HttpStatus.OK);
        } catch (Exception e) {
            return RestUtil.createErrorResponse(e);
        }

    }

    /**
     * 更新廳主資料
     * 
     * @return result
     */
    @ApiOperation(value = " 更新廳主資料")
    @RequestMapping(value = "/syncRDWorkCustomer", method = RequestMethod.GET)
    public ResponseEntity<String> syncRDWorkCustomer() {
        String updateMsg = "";
        try {
            updateMsg = workCustomerSyncService.syncWorkCustomer();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "text/html; charset=utf-8");
            return new ResponseEntity<String>(updateMsg, headers, HttpStatus.OK);
        } catch (Exception e) {
            return RestUtil.createErrorResponse(e);
        }
    }

    /**
     * 取得廳主資料
     * 
     * @return
     */
    @ApiOperation(value = "取得廳主資料")
    @RequestMapping(value = "/findWorkCustomerData", method = RequestMethod.GET)
    public ResponseEntity<List<RetTo>> findWorkCustomerData() {
        try {
            return new ResponseEntity<List<RetTo>>(workCustomerSyncClient.process(), HttpStatus.OK);
        } catch (Exception e) {
            return RestUtil.createErrorResponse(e);
        }
    }

    @ApiOperation(value = "GM 管理系統同步")
    @RequestMapping(value = "/syncGM", method = RequestMethod.GET)
    public ResponseEntity<String> syncGM() {
        String response = "";
        try {
            response = syncGMClient.process(1); // create user 預設為 admin
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "text/html; charset=utf-8");
            return new ResponseEntity<String>(response, headers, HttpStatus.OK);
        } catch (Exception e) {
            return RestUtil.createErrorResponse(e);
        }
    }

    @ApiOperation(value = "維護管理系統(MMS)同步")
    @RequestMapping(value = "/syncMms", method = RequestMethod.GET)
    public ResponseEntity<String> syncMms() {
        List<String> responseMessages = Lists.newArrayList();
        boolean isFail = false;

        long startTime = System.currentTimeMillis();
        // ====================================
        // 同步廳主
        // ====================================
        try {
            Integer processCnt = this.syncMmsDominLogic.process("");
            responseMessages.add("執行[廳主同步]成功，共同步[" + processCnt + "]筆");
        } catch (SystemOperationException e) {
            isFail = true;
            responseMessages.add("執行[廳主同步]失敗! [" + e.getMessage() + "]");
        } catch (Exception e) {
            isFail = true;
            String errorMessage = WkMessage.PROCESS_FAILED + e.getMessage();
            log.error(errorMessage, e);
            responseMessages.add("執行[廳主同步]失敗! [" + e.getMessage() + "]");
        }

        // ====================================
        // 同步需求單列表
        // ====================================
        try {
            Integer processCnt = this.syncMmsRequireDataLogic.process(false, "");
            responseMessages.add("執行[需求單列表同步]成功，共同步[" + processCnt + "]筆");
        } catch (SystemOperationException e) {
            isFail = true;
            responseMessages.add("執行[需求單列表同步]失敗! [" + e.getMessage() + "]");
        } catch (Exception e) {
            isFail = true;
            String errorMessage = WkMessage.PROCESS_FAILED + e.getMessage();
            log.error(errorMessage, e);
            responseMessages.add("執行[需求單列表同步]失敗! [" + e.getMessage() + "]");
        }

        // ====================================
        // 同步維護單
        // ====================================
        try {
            Integer processCnt = this.syncMmsSyncFormLogic.process("");
            responseMessages.add("執行[同步維護單]成功，共同步[" + processCnt + "]次 (含重複)");
        } catch (SystemOperationException e) {
            isFail = true;
            responseMessages.add("執行[同步維護單]失敗! [" + e.getMessage() + "]");
        } catch (Exception e) {
            isFail = true;
            String errorMessage = WkMessage.PROCESS_FAILED + e.getMessage();
            log.error(errorMessage, e);
            responseMessages.add("執行[同步維護單]失敗! [" + e.getMessage() + "]");
        }

        // ====================================
        // 同步維護單
        // ====================================
        try {
            int updateCount = this.requireService.fixHasOnpginfo();
            responseMessages.add("執行[修正ON程式單狀態]成功，修正[" + updateCount + "]筆");
        } catch (Exception e) {
            isFail = true;
            String errorMessage = WkMessage.PROCESS_FAILED + e.getMessage();
            log.error(errorMessage, e);
            responseMessages.add("執行[修正ON程式單狀態]失敗! [" + e.getMessage() + "]");
        }

        // ====================================
        // 記錄處理時間
        // ====================================
        responseMessages.add(WkCommonUtils.prepareCostMessage(startTime, "MMS同步"));

        // ====================================
        // 回傳結果
        // ====================================
        String responseMessage = Base64Utils.encode(responseMessages.stream().collect(Collectors.joining("\r\n")));

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "text/html; charset=utf-8");

        if (isFail) {
            responseHeaders.add("error", responseMessage);
            return new ResponseEntity<String>(responseHeaders, HttpStatus.SEE_OTHER);
        } else {
            return new ResponseEntity<String>(responseMessage, responseHeaders, HttpStatus.OK);
        }

    }

    public static boolean requireDepRebuildServiceLock = false;

    @ApiOperation(value = "需求確認單位重建(已結案單據)")
    @RequestMapping(value = "/requireConfirmDepRebuildService", method = RequestMethod.GET)
    public ResponseEntity<String> requireDepRebuildService() {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "text/html; charset=utf-8");

        if (requireDepRebuildServiceLock) {
            return new ResponseEntity<String>("正在執行", headers, HttpStatus.OK);
        }

        requireDepRebuildServiceLock = true;

        try {
            long startTime = System.currentTimeMillis();

            HikariDataSource hikariDs = (HikariDataSource) WorkSpringContextHolder.getBean("dataSource");
            HikariPoolMXBean poolBean = hikariDs.getHikariPoolMXBean();
            poolBean.softEvictConnections();
            WkCommonUtils.memonyMonitor();

            // ====================================
            // 查詢未轉檔
            // ====================================
            Map<String, String> requireNoBySid = this.requireDepRebuildService.findWaitTransRequireSids(
                    "",
                    true);

            if (WkStringUtils.isEmpty(requireNoBySid)) {
                return new ResponseEntity<String>("無待執行資料", headers, HttpStatus.OK);
            }

            Set<String> requireSids = requireNoBySid.keySet();

            // ====================================
            // 查詢分派、通知單位
            // ====================================
            Map<String, Set<Integer>> assignDepSidMapByRequireSid = this.requireDepCommonService.findAssignNoticeDepSids(
                    requireSids, AssignSendType.ASSIGN);

            Map<String, Set<Integer>> noticeDepSidMapByRequireSid = this.requireDepCommonService.findAssignNoticeDepSids(
                    requireSids, AssignSendType.SEND);

            // ====================================
            // 查詢確認單位檔
            // ====================================
            Map<String, List<RequireDepRebuildTo>> requireDepRebuildTosMapByRequireSid = this.requireDepCommonService.findRequireConfirmDeps(requireSids);

            // ====================================
            // 逐筆處理
            // ====================================

            int seccess = 0;
            List<String> errorMessages = Lists.newArrayList();
            for (String requireSid : requireSids) {

                if (!requireDepRebuildTosMapByRequireSid.containsKey(requireSid)) {
                    String errorMessage = String.format(
                            "單據無確認單位檔 requireSid:[%s], requireNo:[%s]",
                            requireSid,
                            requireNoBySid.get(requireSid));
                    log.debug(errorMessage);
                    errorMessages.add(errorMessage);
                    continue;
                }

                if (!assignDepSidMapByRequireSid.containsKey(requireSid)) {
                    String errorMessage = String.format(
                            "單據無分派單位資料 requireSid:[%s], requireNo:[%s]",
                            requireSid,
                            requireNoBySid.get(requireSid));
                    log.debug(errorMessage);
                    errorMessages.add(errorMessage);
                    continue;
                }

                try {
                    this.requireDepRebuildService.process(
                            requireSid,
                            requireNoBySid.get(requireSid),
                            assignDepSidMapByRequireSid.get(requireSid),
                            noticeDepSidMapByRequireSid.get(requireSid),
                            requireDepRebuildTosMapByRequireSid.get(requireSid));

                    seccess++;
                } catch (UserMessageException e) {
                    String errorMessage = String.format(
                            e.getMessage() + "，requireSid:[%s], requireNo:[%s]",
                            requireSid,
                            requireNoBySid.get(requireSid));
                    log.info(errorMessage);
                    errorMessages.add(errorMessage);
                } catch (Exception e) {
                    String errorMessage = String.format(
                            e.getMessage() + "，requireSid:[%s], requireNo:[%s]",
                            requireSid,
                            requireNoBySid.get(requireSid));
                    log.error(errorMessage, e);
                    errorMessages.add(errorMessage);
                }
            }
            log.info(WkCommonUtils.prepareCostMessage(startTime, "轉檔結束![" + requireNoBySid.size() + "]筆"));

            String message = "\r\n" + String.format("本次處理:[%s]筆，成功[%s]筆，失敗[%s]",
                    requireSids.size(),
                    seccess,
                    requireSids.size() - seccess);

            if (WkStringUtils.notEmpty(errorMessages)) {
                message += "\r\n" + errorMessages.stream().collect(Collectors.joining("<br/>"));
            }

            return new ResponseEntity<String>(message, headers, HttpStatus.OK);
        } catch (Exception e) {
            String message = "執行失敗" + e.getMessage();
            log.error(message, e);
            return new ResponseEntity<String>(message, headers, HttpStatus.OK);
        } finally {
            requireDepRebuildServiceLock = false;
        }

    }
}
