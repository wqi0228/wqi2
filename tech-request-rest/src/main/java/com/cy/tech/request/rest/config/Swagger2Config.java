package com.cy.tech.request.rest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.cy.commons.util.WebVersion;

import io.swagger.annotations.ApiOperation;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * API Doc 設定
 */
@Configuration
@EnableSwagger2
@Component
public class Swagger2Config {
    /**
     * 排程類 API
     * 
     * @return
     */
    @Bean
    public Docket createScheduleDocket() {
        String groupName = "排程";
        return this.createApiDocket(
                "0." + groupName,
                this.apiInfo(groupName),
                "com.cy.tech.request.rest.controller.schedule");
    }

    /**
	 * 排程類 API
	 * 
	 * @return
	 */
	@Bean
	public Docket createPortalDocket() {
		String groupName = "Portal 待辦事項";
		return this.createApiDocket(
		        "1." + groupName,
		        this.apiInfo(groupName),
		        "com.cy.tech.request.rest.controller.portal");
	}
	
    /**
     * API
     * 
     * @return
     */
    @Bean
    public Docket createPortalAlertDocket() {
        String groupName = "Portal 最新消息";
        return this.createApiDocket(
                "2." + groupName,
                this.apiInfo(groupName),
                "com.cy.tech.request.rest.controller.alert");
    }

    @Bean
    public Docket createNotifyDocket() {
        String groupName = "推撥";
        return this.createApiDocket(
                "3." + groupName,
                this.apiInfo(groupName),
                "com.cy.tech.request.rest.controller.notify");
    }

    
    @Bean
    public Docket createClearCacheDocket() {
        String groupName = "清快取";
        return this.createApiDocket(
                "4." + groupName,
                this.apiInfo(groupName),
                "com.cy.tech.request.rest.controller.cache");
    }
    
    
    @Bean
    public Docket createOtherDocket() {
        String groupName = "其他";
        return this.createApiDocket(
                "5." + groupName,
                this.apiInfo(groupName),
                "com.cy.tech.request.rest.controller.process");
    }
    
    @Bean
    public Docket createBpmSignTestDocket() {
        String groupName = "BPM簽核測試";
        return this.createApiDocket(
                "6." + groupName,
                this.apiInfo(groupName),
                "com.cy.tech.request.rest.controller.signtest");
    }
    
    /**
     * 创建API应用. apiInfo() 增加API相关信息
     * 通过select()函数返回一个ApiSelectorBuilder实例,用来控制哪些接口暴露给Swagger来展现，
     * 本例采用指定扫描的包路径来定义指定要建立API的目录。
     *
     * @param groupName   分類名稱
     * @param apiInfo     API相關信息
     * @param basePackage 掃描的Package paths
     * @return Docket
     * @author Kasim v0.3.0
     */
    private Docket createApiDocket(
            final String groupName,
            final ApiInfo apiInfo,
            final String basePackage) {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(groupName)
                .apiInfo(apiInfo)
                .select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .apis(RequestHandlerSelectors.basePackage(basePackage))
                .paths(PathSelectors.any())
                .build();
    }

    /**
     * 建立API資訊.
     *
     * @param title - title for the API
     * @return API資訊
     * @author Kasim v0.3.0
     */
    private ApiInfo apiInfo(
            final String title) {
        return new ApiInfoBuilder()
                .title(title)
                .description("相關說明")
                .contact(new Contact("allen", null, null))
                .version(WebVersion.getInstance().getWebVersion())
                .build();
    }

}
