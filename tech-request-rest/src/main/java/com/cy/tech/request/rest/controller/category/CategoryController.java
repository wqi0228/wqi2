package com.cy.tech.request.rest.controller.category;

import com.cy.tech.request.rest.logic.category.CategoryService;
import com.cy.tech.request.rest.logic.to.CategoryTo;
import com.cy.work.common.logic.lib.RestUtil;

import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 類別 服務
 *
 * @author kasim
 */
@RestController
@RequestMapping("/category")
public class CategoryController implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6357347579453155361L;

    @Autowired
    private CategoryService categoryService;

    @Value("${tech.request.program.req.name}")
    private String programReqName;

    /**
     * 抓取小類為有效且須顯示在案件單的選項為true
     *
     * @param middleSid
     * @return
     */
    @RequestMapping(value = "/findSmallCategoryByShowItemInTc", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<CategoryTo>> findSmallCategoryByShowItemInTc() {
        try {
            return new ResponseEntity<List<CategoryTo>>(categoryService.findSmallCategoryByShowItemInTc(), HttpStatus.OK);
        } catch (Exception e) {
            return RestUtil.createErrorResponse(e);
        }
    }



    @RequestMapping(value = "/findCategoryToByCategoryName", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<CategoryTo> findCategoryToByCategoryName() {
        try {
            return new ResponseEntity<CategoryTo>(categoryService.findCategoryToByCategoryName(programReqName), HttpStatus.OK);
        } catch (Exception e) {
            return RestUtil.createErrorResponse(e);
        }
    }

}
