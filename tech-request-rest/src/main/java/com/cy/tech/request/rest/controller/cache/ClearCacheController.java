package com.cy.tech.request.rest.controller.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cy.system.rest.client.util.SystemCacheUtils;
import com.cy.tech.request.logic.config.ReqEhCacheHelper;
import com.cy.tech.request.logic.utils.ProjectCustomCacheClearUtil;
import com.cy.work.common.cache.WkCommonCache;
import com.cy.work.common.logic.lib.RestUtil;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 *
 */
@RestController
@RequestMapping("/cache")
@Slf4j
public class ClearCacheController {

    @Autowired
    private ReqEhCacheHelper reqEhCacheHelper;
    @Autowired
    private ProjectCustomCacheClearUtil projectCustomCacheClearUtil;

    /**
     * @return 清除 EhCache
     */
    @ApiOperation(value = "清除 EhCache ")
    @RequestMapping(value = "/clearEhCache", method = RequestMethod.GET)
    public ResponseEntity<HttpStatus> clearEhCache() {
        try {
            reqEhCacheHelper.clearCache();
            log.info("call rest : clear EhCache");
            return new ResponseEntity<HttpStatus>(HttpStatus.OK);
        } catch (Exception e) {
            return RestUtil.createErrorResponse(e);
        }
    }

    /**
     * 清除所有 cache [SYSTEM-REST、WK-COMMON、EhCache
     * 
     * @return
     */
    @ApiOperation(value = "清除所有 cache [SYSTEM-REST、WK-COMMON、EhCache] ")
    @RequestMapping(value = "/clearAllCache", method = RequestMethod.GET)
    public ResponseEntity<HttpStatus> clearAllCache() {
        try {
            log.info("開始清除 SYSTEM-REST 快取");
            SystemCacheUtils.getInstance().clearAllCache();

            log.info("開始清除 WK-COMMON 快取");
            WkCommonCache.getInstance().clearAllCache();

            projectCustomCacheClearUtil.doClear();

            log.info("所有快取清除完畢");
            return new ResponseEntity<HttpStatus>(HttpStatus.OK);
        } catch (Exception e) {
            return RestUtil.createErrorResponse(e);
        }
    }
}
