package com.cy.tech.request.rest.controller.notify;

import com.cy.tech.request.rest.logic.notify.ReqNotifyService;
import com.cy.tech.request.rest.logic.to.WorkNotifyTo;
import com.cy.work.common.logic.lib.RestUtil;
import com.cy.work.notify.vo.enums.NotifyType;

import io.swagger.annotations.ApiOperation;

import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 彈跳視窗 服務
 *
 * @author kasim
 */
@RestController
@RequestMapping("/notify")
public class NotifyController implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8117787458185439529L;
    @Autowired
    private ReqNotifyService notifyService;

    /**
     * 查詢
     *
     * @param userSid
     * @return
     */
    @ApiOperation(value = "查詢推撥資料")
    @RequestMapping(value = "/findFirst10ByUserSid", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<WorkNotifyTo>> findFirst10ByUserSid(@RequestParam(value = "userSid") Integer userSid) {
        try {
            return new ResponseEntity<List<WorkNotifyTo>>(notifyService.findFirst10ByUserSid(userSid), HttpStatus.OK);
        } catch (Exception e) {
            return RestUtil.createErrorResponse(e);
        }
    }

    /**
     * 讀取訊息
     *
     * @param userSid
     * @param notifyType
     * @param sourceNo
     * @return
     */
    @ApiOperation(value = "查詢將推撥訊息資料『已讀』")
    @RequestMapping(value = "/readNotify", method = RequestMethod.PUT)
    public ResponseEntity<String> readNotify(
            @RequestParam(value = "userSid") Integer userSid,
            @RequestParam(value = "notifyType") NotifyType notifyType,
            @RequestParam(value = "sourceNo") String sourceNo) {
        try {
            notifyService.readNotify(userSid, notifyType, sourceNo);
            return new ResponseEntity<String>(HttpStatus.OK);
        } catch (Exception e) {
            return RestUtil.createErrorResponse(e);
        }
    }
}
