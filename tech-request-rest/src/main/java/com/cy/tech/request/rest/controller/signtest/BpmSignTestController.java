/**
 * 
 */
package com.cy.tech.request.rest.controller.signtest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cy.tech.request.logic.service.BpmSignTestService;
import com.cy.work.common.logic.lib.RestUtil;

import io.swagger.annotations.ApiOperation;

/**
 * @author allen1214_wu
 *
 */
@RestController
@RequestMapping("/bpmsigntest")
public class BpmSignTestController {

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private BpmSignTestService bpmSignTestService;

    // ========================================================================
    // 方法
    // ========================================================================
    @ApiOperation(value = "建立流程測試")
    @RequestMapping(value = "/createFlow", method = RequestMethod.GET)
    public ResponseEntity<String> createFlow(@RequestParam(value = "userId") String userId) {
        String updateMsg = "";
        try {
            updateMsg = bpmSignTestService.createFlowByNormal(userId);
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "text/html; charset=utf-8");
            return new ResponseEntity<String>(updateMsg, headers, HttpStatus.OK);
        } catch (Exception e) {
            return RestUtil.createErrorResponse(e);
        }
    }
    
    @ApiOperation(value = "模擬測試")
    @RequestMapping(value = "/signSimulation", method = RequestMethod.GET)
    public ResponseEntity<String> signSimulation() {
        String updateMsg = "";
        try {
            updateMsg = bpmSignTestService.process();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "text/html; charset=utf-8");
            return new ResponseEntity<String>(updateMsg, headers, HttpStatus.OK);
        } catch (Exception e) {
            return RestUtil.createErrorResponse(e);
        }
    }

}
