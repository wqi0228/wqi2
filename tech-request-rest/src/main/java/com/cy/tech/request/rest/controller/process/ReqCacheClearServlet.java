/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.rest.controller.process;

import com.cy.tech.request.logic.config.ReqEhCacheHelper;
import com.cy.tech.request.logic.service.UserService;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkRoleCache;
import com.cy.work.common.cache.WkRoleGroupCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.cache.WkUserWithRolesCache;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
@WebServlet(name = "/cache/clear", urlPatterns = "/cache/clear")
public class ReqCacheClearServlet extends HttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = 5849720197975223566L;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            log.info("receive clear cache signal.");
            try {
                UserService.getInstance().clearCache();
            } catch (Exception ex) {
                log.error("UserService.getInstance().clearCache() ERROR", ex);
            }
            try {
                WkOrgCache.getInstance().updateCache();
                WkUserCache.getInstance().updateCache();
                WkRoleCache.getInstance().updateCache();
                WkRoleGroupCache.getInstance().updateCache();
                WkUserWithRolesCache.getInstance().updateCache();
                ReqEhCacheHelper.getInstance().clearCache();
                
            } catch (Exception ex) {
                log.error("OrganizationService.getInstance().clearCache() ERROR", ex);
            }
            log.info("receive clear cache signal done.");
            response.getWriter().write("ok");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
