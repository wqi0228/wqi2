/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.rest.config;

import com.cy.formsigning.rest.impl.config.FormSigningRestServiceSpringConfig;
import com.cy.tech.request.rest.logic.config.LogicConfig;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.naming.NamingException;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 * @author shaun
 */
@Configuration
@ComponentScan("com.cy.tech.request.rest")
@PropertySources({
    @PropertySource("classpath:config.properties"),
    @PropertySource("classpath:common.properties")
})
@Import({
	LogicConfig.class,
	com.cy.tech.request.logic.config.LogicConfig.class,
    FormSigningRestServiceSpringConfig.class})
@EnableWebMvc
@EnableScheduling
public class RestConfig extends RepositoryRestMvcConfiguration {

    @Bean
    public static PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() {
        PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = new PropertyPlaceholderConfigurer();
        propertyPlaceholderConfigurer.setLocations(
                  new Resource[]{
                      new ClassPathResource("config.properties"),
                      new ClassPathResource("common.properties")
                  }
        );
        propertyPlaceholderConfigurer.setIgnoreUnresolvablePlaceholders(true);
        return propertyPlaceholderConfigurer;
    }

    @Bean
    public static PropertyPlaceholderConfigurer initProp() throws NamingException, IOException {
        String configHome = System.getenv("FUSION_CONFIG_HOME");
        Path techPath = Paths.get(configHome, "tech-request.properties");
        Path fbservicePath = Paths.get(configHome, "fbservice.properties");
        Path urlPath = Paths.get(configHome, "url.properties");
        Path customer = Paths.get(configHome, "tech-customer.properties");
        Path workCommon = Paths.get(configHome, "work-common.properties");
        Path bpmRest = Paths.get(configHome, "bpm-rest.properties");
        PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = new PropertyPlaceholderConfigurer();
        propertyPlaceholderConfigurer.setLocations(
                  new PathResource[]{
                      new PathResource(techPath),
                      new PathResource(fbservicePath),
                      new PathResource(urlPath),
                      new PathResource(workCommon),
                      new PathResource(customer),
                      new PathResource(bpmRest),
                      }
        );
        //開啟後@Value如果有預設值則會永遠使用預設值需注意...
        propertyPlaceholderConfigurer.setIgnoreUnresolvablePlaceholders(true);
        return propertyPlaceholderConfigurer;
    }

}
