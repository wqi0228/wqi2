/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.rest.controller.alert;

import com.cy.commons.util.Base64Utils;
import com.cy.tech.request.rest.logic.ReqUrlRestService;
import com.cy.tech.request.rest.logic.alert.OnpgAlertRestService;
import com.cy.tech.request.rest.logic.to.WorkOnpgAlertTo;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.ReadRecordType;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * ON程式通知rest控制
 *
 * @author shaun
 */
@RestController
@RequestMapping("/alert/onpg")
@Slf4j
@SuppressWarnings({ "rawtypes", "unchecked" })
public class OnpgAlertController implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5234371362819011919L;
    @Autowired
    private OnpgAlertRestService oarService;
    @Autowired
    private ReqUrlRestService rurService;
    @RequestMapping(value = "/find/by/read/status/and/receiver/and/limit", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<WorkOnpgAlertTo>> findByReadStatusAndReceiverLimit(
            @RequestParam(value = "readStatus") ReadRecordType readStatus,
            @RequestParam(value = "receiverSid") Integer receiverSid,
            @RequestParam(value = "limit") Integer limit) {
        try {
            return new ResponseEntity(oarService.findByReadStatusAndReceiverLimit(
            		readStatus, 
            		WkUserCache.getInstance().findBySid(receiverSid), 
            		limit), HttpStatus.OK);
        } catch (Exception e) {
            return this.createErrorResponse(e.getMessage());
        }
    }

    @RequestMapping(value = "/find/by/read/status/and/receiver", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<WorkOnpgAlertTo>> findByReadStatusAndReceiver(
            @RequestParam(value = "readStatus") ReadRecordType readStatus,
            @RequestParam(value = "receiverSid") Integer receiverSid,
            @RequestParam(value = "startDate") Long pStartDate,
            @RequestParam(value = "endDate") Long pEndDate) {
        try {
            Date startDate = pStartDate == -1 ? null : new Date(pStartDate);
            Date endDate = pEndDate == -1 ? null : new Date(pEndDate);
            return new ResponseEntity<List<WorkOnpgAlertTo>>(oarService.findByReadStatusAndReceiverAndSendDate(readStatus, WkUserCache.getInstance().findBySid(receiverSid), startDate, endDate), HttpStatus.OK);
        } catch (Exception e) {
            return this.createErrorResponse(e.getMessage());
        }
    }

    @RequestMapping(value = "/update/click/time", method = RequestMethod.PUT)
    public ResponseEntity updateClickTime(@RequestParam(value = "alertSid") String alertSid) {
        try {
            oarService.updateClickTime(alertSid);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return this.createErrorResponse(e.getMessage());
        }
    }

    @RequestMapping(value = "/find/url", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> findUrl(
            @RequestParam(value = "userSid") Integer userSid,
            @RequestParam(value = "requireNo") String requireNo,
            @RequestParam(value = "onpgSid") String onpgSid) {
        try {
            return new ResponseEntity(rurService.findOnpgUrl(userSid, requireNo, onpgSid), HttpStatus.OK);
        } catch (Exception e) {
            return this.createErrorResponse(e.getMessage());
        }
    }

    /**
     * 建立錯誤訊息
     *
     * @param errMsg
     * @return
     */
    public ResponseEntity createErrorResponse(String errMsg) {
        log.error(errMsg);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("error", Base64Utils.encode(errMsg));
        return new ResponseEntity(responseHeaders, HttpStatus.SEE_OTHER);
    }

}
