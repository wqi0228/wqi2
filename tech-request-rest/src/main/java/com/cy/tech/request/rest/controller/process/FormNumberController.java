package com.cy.tech.request.rest.controller.process;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cy.tech.request.logic.service.FormNumberService;
import com.cy.tech.request.vo.enums.FormType;
import com.cy.work.common.exception.alert.TechRequestAlertException;
import com.cy.work.common.logic.lib.RestUtil;
import com.cy.work.common.utils.WkStringUtils;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/process")
public class FormNumberController {

    @Autowired
    private transient FormNumberService formNumberService;

    /**
     * @param formType 單據類別
     * @return
     */
    @ApiOperation(value = "表單號碼產生器")
    @RequestMapping(value = "/formNumberGenerator", method = RequestMethod.GET)
    public ResponseEntity<String> formNumberGenerator(final String formTypeStr, final String compId) {

        try {
            // ====================================
            //
            // ====================================
            FormType formType = FormType.safeValueOf(WkStringUtils.safeTrim(formTypeStr));
            if (formType == null) {
                throw new TechRequestAlertException("無法辨識單據類別! formType:[" + formTypeStr + "]", "");
            }

            String response = "";
            switch (formType) {
            case REQUIRE:
                response = this.formNumberService.generateTRNum(compId);
                break;

            case REQUIRE_DREFT:
                response = this.formNumberService.generateDraftNum(compId);
                break;

            case PTCHECK:
                response = this.formNumberService.generatePtNum();
                break;

            case WORKTESTSIGNINFO:
                response = this.formNumberService.generateWtNum();
                break;

            case WORKONPG:
                response = this.formNumberService.generateOpNum();
                break;

            case OTHSET:
                response = this.formNumberService.generateOsNum();
                break;
                
            default:
                throw new TechRequestAlertException("未實做的單據類別! formType:[" + formType + "]", "");
            }
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "text/html; charset=utf-8");
            return new ResponseEntity<String>(response, headers, HttpStatus.OK);
        } catch (Exception e) {
            return RestUtil.createErrorResponse(e);
        }
    }
}
