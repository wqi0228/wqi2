/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.rest.config;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.cy.tech.request.logic.service.FormNumberService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author shaun
 */
@Component
@Slf4j
public class StartupAware implements ApplicationContextAware {

    @Override
    public void setApplicationContext(ApplicationContext actx) throws BeansException {

        FormNumberService.isFormNumberCenter = true;
        log.info("此AP為單號產生中心");
    }
}
