/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.rest.controller.process;

import com.cy.tech.request.logic.service.bpm.ReqBpmUpdateService;
import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
@WebServlet(name = "/cache/clear/for/bpm", urlPatterns = "/cache/clear/for/bpm")
public class ReqBpmCacheClearServlet extends HttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = 3544864831357504481L;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            log.info("receive clear cache signal.");
            try {
                ReqBpmUpdateService.getInstance().resetAllFlow();
            } catch (Exception ex) {
                log.error("ReqBpmUpdateService.getInstance().resetAllFlow() ERROR", ex);
            }
            log.info("receive clear cache signal done.");
            response.getWriter().write("ok");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
