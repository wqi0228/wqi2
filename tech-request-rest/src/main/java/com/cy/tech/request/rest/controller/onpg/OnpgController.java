/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.rest.controller.onpg;

import com.cy.tech.request.rest.logic.onpg.ReqRestWorkCountService;
import com.cy.tech.request.rest.logic.to.WorkOnpgTo;
import com.cy.work.common.logic.lib.RestUtil;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * onpg 服務
 *
 * @author kasim
 */
@RestController
@RequestMapping("/onpg")
public class OnpgController implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5262087689645312487L;
    @Autowired
    private ReqRestWorkCountService onpgService;

    @RequestMapping(value = "/findWorkOnpgToListBySidsAndUserSid", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<WorkOnpgTo>> findWorkOnpgToListBySidsAndUserSid(
            @RequestParam(value = "sids") String sids,
            @RequestParam(value = "userSid") Integer userSid) {
        try {
            return new ResponseEntity<List<WorkOnpgTo>>(onpgService.findToBySidInAndUserSid(
                    userSid, Lists.newArrayList(sids.split(","))), HttpStatus.OK);
        } catch (Exception e) {
            return RestUtil.createErrorResponse(e);
        }
    }

    @RequestMapping(value = "/findByTransReq", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<WorkOnpgTo>> findByTransReq(
            @RequestParam(value = "userSid") Integer userSid,
            @RequestParam(value = "issueSid") String issueSid) {
        try {
            return new ResponseEntity<List<WorkOnpgTo>>(onpgService.findByTransReq(
                    userSid, issueSid), HttpStatus.OK);
        } catch (Exception e) {
            return RestUtil.createErrorResponse(e);
        }
    }

}
