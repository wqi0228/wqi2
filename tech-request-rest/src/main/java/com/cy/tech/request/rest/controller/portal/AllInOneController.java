package com.cy.tech.request.rest.controller.portal;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cy.tech.request.logic.vo.WaitingTaskCountTo;
import com.cy.tech.request.rest.logic.CountToDoService;
import com.cy.work.common.logic.lib.RestUtil;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/allinone")
public class AllInOneController {

    @Autowired
    private CountToDoService countToDoService;
    
    /**
     * 回傳所有的待辦事項項目 -> 數量統計
     * @param userSid
     * @return List<WaitingTaskCountTo>
     */
    @ApiOperation(value = "回傳所有的待辦事項項目 -> 數量統計")
    @RequestMapping(value = "/countAllWaitingTask", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<WaitingTaskCountTo>> countAllWaitingTask(
            @RequestParam(value = "userSid") Integer userSid) {
        try {
            return new ResponseEntity<List<WaitingTaskCountTo>>(
                    countToDoService.syncCountAllWaitingTask(userSid),
                    HttpStatus.OK);
        } catch (Exception e) {
            return RestUtil.createErrorResponse(e);
        }
    }
}
