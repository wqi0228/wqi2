package com.cy.tech.request.rest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.cy.tech.request.logic.config.ReqEhCacheHelper;

/**
 * 啟動需求單快取
 *
 * @author allen
 */
@EnableCaching
@Configuration
public class ReqCacheConfig {


	@Autowired
	transient private ReqEhCacheHelper reqCacheHelper;

	@Bean
	public CacheManager cacheManager() {
		return reqCacheHelper.createCacheManager(ehCacheCacheManager().getObject());
	}
	
	@Bean
	public EhCacheManagerFactoryBean ehCacheCacheManager() {
		EhCacheManagerFactoryBean cmfb = new EhCacheManagerFactoryBean();
		return cmfb;
	}

}
