package com.cy.tech.request.rest.controller.url;

import com.cy.commons.util.Base64Utils;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.URLService;
import java.io.Serializable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author kasim
 */
@RestController
@RequestMapping("/req")
@Slf4j
@SuppressWarnings({ "rawtypes", "unchecked" })
public class ReqUrlController implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2857477357761856761L;
    @Autowired
    private URLService urlService;
    @Autowired
    private RequireService reqService;

    @RequestMapping(value = "/find/url", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> findUrl(
            @RequestParam(value = "userSid") Integer userSid,
            @RequestParam(value = "requireNo") String requireNo) {
        try {
            String url = urlService.createUrlLinkParam(URLService.URLServiceAttr.URL_ATTR_M,
                    urlService.createSimpleUrlTo(userSid, requireNo, 1));
            return new ResponseEntity(url, HttpStatus.OK);
        } catch (Exception e) {
            return this.createErrorResponse(e.getMessage());
        }
    }

    @RequestMapping(value = "/checkReqFinish", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Boolean> checkReqFinish(
            @RequestParam(value = "requireNo") String requireNo) {
        try {
            return new ResponseEntity(reqService.checkReqFinish(requireNo), HttpStatus.OK);
        } catch (Exception e) {
            return this.createErrorResponse(e.getMessage());
        }
    }

    /**
     * 建立暫存訊息
     *
     * @param userSid
     * @param sourceNo
     * @return
     */
    @RequestMapping(value = "/createReqTempNotify", method = RequestMethod.POST)
    public ResponseEntity createReqTempNotify(
            @RequestParam(value = "userSid") Integer userSid,
            @RequestParam(value = "sourceNo") String sourceNo) {
        try {
            reqService.createReqTempNotify(userSid, sourceNo);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return this.createErrorResponse(e.getMessage());
        }
    }

    /**
     * 建立錯誤訊息
     *
     * @param errMsg
     * @return
     */
    public ResponseEntity createErrorResponse(String errMsg) {
        log.error(errMsg);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("error", Base64Utils.encode(errMsg));
        return new ResponseEntity(responseHeaders, HttpStatus.SEE_OTHER);
    }
}
