package com.cy.tech.request.rest.controller.alert;

import com.cy.commons.util.Base64Utils;
import com.cy.tech.request.rest.logic.ReqUrlRestService;
import com.cy.tech.request.rest.logic.alert.OthSetAlertRestService;
import com.cy.tech.request.rest.logic.to.OthSetAlertTo;
import com.cy.work.common.enums.ReadRecordType;
import com.google.common.collect.Lists;

import io.swagger.annotations.ApiOperation;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 其它設定通知rest控制
 *
 * @author shaun
 */
@RestController
@RequestMapping("/alert/othset")
@Slf4j
@SuppressWarnings({ "rawtypes", "unchecked" })
public class OthSetAlertController implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1895257047464556975L;
    @Autowired
    private OthSetAlertRestService osarService;
    @Autowired
    private ReqUrlRestService rurService;

    @ApiOperation(value = "查詢最新消息 for 其他資料單 (限制筆數)")
    @RequestMapping(value = "/find/by/read/status/and/receiver/and/limit", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<OthSetAlertTo>> findByReadStatusAndReceiverLimit(
            @RequestParam(value = "readStatus") ReadRecordType readStatus,
            @RequestParam(value = "receiverSid") Integer receiverSid,
            @RequestParam(value = "limit") Integer limit) {
        try {

            // 查詢
            List<OthSetAlertTo> results = this.osarService.nativeQueryByReadStatusAndReceiverForLimit(
                    readStatus,
                    receiverSid,
                    limit);

            // Response
            return new ResponseEntity<List<OthSetAlertTo>>(results, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return this.createErrorResponse(e.getMessage());
        }
    }

    @ApiOperation(value = "查詢最新消息 for 其他資料單 (限制日期區間)")
    @RequestMapping(value = "/find/by/read/status/and/receiver", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<OthSetAlertTo>> findByReadStatusAndReceiver(
            @RequestParam(value = "readStatus") ReadRecordType readStatus,
            @RequestParam(value = "receiverSid") Integer receiverSid,
            @RequestParam(value = "startDate") Long pStartDate,
            @RequestParam(value = "endDate") Long pEndDate) {
        try {
            Date startDate = pStartDate == -1 ? null : new Date(pStartDate);
            Date endDate = pEndDate == -1 ? null : new Date(pEndDate);

            // REQ-1549 【REST】【待辦事項】【其他資訊單】查詢優化
            // 將查詢方式改為 native sql

            List<OthSetAlertTo> results = Lists.newArrayList();

            // 查詢
            results = this.osarService.nativeQueryByReadStatusAndReceiver(
                    readStatus,
                    receiverSid,
                    startDate,
                    endDate,
                    0);

            // Response
            return new ResponseEntity<List<OthSetAlertTo>>(results, HttpStatus.OK);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return this.createErrorResponse(e.getMessage());
        }
    }

    @RequestMapping(value = "/update/click/time", method = RequestMethod.PUT)
    public ResponseEntity updateClickTime(@RequestParam(value = "alertSid") String alertSid) {
        try {
            osarService.updateClickTime(alertSid);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return this.createErrorResponse(e.getMessage());
        }
    }

    @RequestMapping(value = "/find/url", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> findUrl(
            @RequestParam(value = "userSid") Integer userSid,
            @RequestParam(value = "requireNo") String requireNo,
            @RequestParam(value = "othsetSid") String othsetSid) {
        try {
            return new ResponseEntity<String>(rurService.findOthsetUrl(userSid, requireNo, othsetSid), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return this.createErrorResponse(e.getMessage());
        }
    }

    /**
     * 建立錯誤訊息
     *
     * @param errMsg
     * @return
     */
    public ResponseEntity createErrorResponse(String errMsg) {
        log.error(errMsg);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("error", Base64Utils.encode(errMsg));
        return new ResponseEntity(responseHeaders, HttpStatus.SEE_OTHER);
    }

}
