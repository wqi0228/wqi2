package com.cy.tech.request.rest.config;

import com.cy.tech.request.logic.config.ReqConstants;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.util.Properties;
import javax.sql.DataSource;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(value = {
        "com.cy.tech.request.repository",
        "com.cy.work.backend.repository",
        "com.cy.work.customer.repository",
        "com.cy.work.trace.repository",
        "com.cy.work.notify.repository",
        "com.cy.work.mapp.create.trans.repository",
        "com.cy.work.group.repository",
        "com.cy.work.sp.repository",
        "com.cy.work.repository"
})
public class JpaConfig {

    @Value("${tech-request.db.driverClassName}")
    private String dbDriverClassName;
    @Value("${tech-request.db.url}")
    private String dbUrl;
    @Value("${tech-request.db.username}")
    private String dbUserName;
    @Value("${tech-request.db.password}")
    private String dbPassword;

    @Value("${tech-request.db.maxPoolSize}")
    private int maxPoolSize;
    @Value("${tech-request.db.minPoolSize}")
    private int minPoolSize;
    @Value("${tech-request.db.maxStatements}")
    private int maxStatements;
    @Value("${tech-request.db.cachePrepStmts}")
    private int cachePrepStmts;
    @Value("${tech-request.db.prepStmtCacheSqlLimit}")
    private int prepStmtCacheSqlLimit;

    @Value("${tech-request.db.dialect.hibernate}")
    private String hibernateDialect;
    @Value("${tech-request.db.hibernate.show_sql}")
    private String hibernateShowSql;

    @Bean(name = ReqConstants.REQ_JDBC_TEMPLATE)
    public JdbcTemplate reqJdbcTemplate() {
        JdbcTemplate template = new JdbcTemplate();
        template.setDataSource(this.dataSource());
        return template;
    }

    @Bean
    public DataSource dataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(dbDriverClassName);
        hikariConfig.setJdbcUrl(dbUrl);
        hikariConfig.setUsername(dbUserName);
        hikariConfig.setPassword(dbPassword);
        hikariConfig.setMaximumPoolSize(maxPoolSize);
        hikariConfig.setMinimumIdle(minPoolSize);
        //Connection Leak 檢測 (大於 2000 即印出 exception)
        //hikariConfig.setLeakDetectionThreshold(2000);
        hikariConfig.setConnectionTestQuery("SELECT 1");
        hikariConfig.setPoolName("springHikariCP");
        hikariConfig.addDataSourceProperty("dataSource.cachePrepStmts", "true");
        hikariConfig.addDataSourceProperty("dataSource.prepStmtCacheSize", cachePrepStmts);
        hikariConfig.addDataSourceProperty("dataSource.prepStmtCacheSqlLimit", prepStmtCacheSqlLimit);
        hikariConfig.addDataSourceProperty("dataSource.useServerPrepStmts", "true");
        HikariDataSource dataSource = new HikariDataSource(hikariConfig);
        return dataSource;
//        ComboPooledDataSource dataSource = new ComboPooledDataSource();
//        try {
//            dataSource.setDriverClass(dbDriverClassName);
//            dataSource.setJdbcUrl(dbUrl);
//            dataSource.setUser(dbUserName);
//            dataSource.setPassword(dbPassword);
//            dataSource.setMaxPoolSize(maxPoolSize);
//            dataSource.setMinPoolSize(minPoolSize);
//            dataSource.setMaxStatements(maxStatements);
//            dataSource.setTestConnectionOnCheckin(true);
//            dataSource.setTestConnectionOnCheckout(true);
//            log.info("dataSource connection is valid");
//        } catch (PropertyVetoException e) {
//            log.error("dataSource get Driver is fail", e);
//        }
//        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        return new JpaTransactionManager(entityManagerFactory().getObject());
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(this.dataSource());
        entityManagerFactoryBean.setPackagesToScan(new String[] {
                "com.cy.tech.request.vo",
                "com.cy.work.backend.vo",
                "com.cy.work.customer.vo",
                "com.cy.work.trace.vo",
                "com.cy.work.notify.vo",
                "com.cy.work.group.vo",
                "com.cy.work.mapp.create.trans.vo",
                "com.cy.work.sp.vo",
                "com.cy.work.vo",
                "com.cy.commons.vo",
                "com.cy.system.vo"
        });
        entityManagerFactoryBean.setJpaDialect(new HibernateJpaDialect());
        entityManagerFactoryBean.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        Properties properties = new Properties();
        properties.put("hibernate.dialect", hibernateDialect);
        properties.put("hibernate.show_sql", hibernateShowSql);
        properties.put("hibernate.jdbc", "true");
        // properties.put("hibernate.connection.release_mode", "after_transaction");
        // properties.put("hibernate.format_sql", "true");
        // properties.put("hibernate.hbm2ddl.auto", "update");
        entityManagerFactoryBean.setJpaProperties(properties);
        entityManagerFactoryBean.setMappingResources(new String[] { "META-INF/tr-orm.xml", "META-INF/rest-count-orm.xml" });
        return entityManagerFactoryBean;
    }

}
