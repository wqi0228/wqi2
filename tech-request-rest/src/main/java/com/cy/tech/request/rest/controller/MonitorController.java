/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.rest.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.formsigning.enums.LanguageType;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.service.pmis.PmisService;
import com.cy.tech.request.logic.vo.PmisResponseVO;
import com.cy.tech.request.rest.logic.CountToDoService;
import com.cy.tech.request.rest.logic.ReqUrlRestService;
import com.cy.tech.request.rest.logic.alert.OnpgAlertRestService;
import com.cy.tech.request.rest.logic.alert.OthSetAlertRestService;
import com.cy.tech.request.rest.logic.alert.PtAlertRestService;
import com.cy.tech.request.rest.logic.alert.SendTestAlertRestService;
import com.cy.tech.request.rest.logic.notify.ReqNotifyService;
import com.cy.tech.request.rest.logic.process.PtSignService;
import com.cy.tech.request.rest.logic.process.ReqUnitSignService;
import com.cy.tech.request.rest.logic.process.SendTestSignForRestService;
import com.cy.work.client.WorkCustomerSyncClient;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkLibChecker;
import com.cy.work.notify.vo.enums.NotifyType;

import lombok.extern.slf4j.Slf4j;

/**
 * 服務狀態檢測 <BR/>
 * for op team check
 *
 * @author shaun
 */
@RestController
@RequestMapping("/monitor")
@Slf4j
public class MonitorController implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 2620147424179571880L;
    @Autowired
	private CountToDoService countToDoService;
	@Autowired
	private OnpgAlertRestService opAlert;
	@Autowired
	private OthSetAlertRestService osAlert;
	@Autowired
	private PtAlertRestService ptAlert;
	@Autowired
	private SendTestAlertRestService stAlert;
	@Autowired
	private ReqUrlRestService reqUrlService;
	@Autowired
	private transient WorkCustomerSyncClient workCustomerSyncClient;
	@Autowired
	private ReqNotifyService notifyService;
	@Autowired
	private URLService urlService;
	@Autowired
	private PtSignService ptProcess;
	@Autowired
	private ReqUnitSignService reqUnitProcess;
	@Autowired
	private SendTestSignForRestService stProcess;
	@Autowired
	private PmisService pmisService;

	private final int testUsrSid = 72;
	private final int testOrgSid = 84;

	@RequestMapping(value = "/libVersionCheck", method = RequestMethod.GET)
	public ResponseEntity<String> libVersionCheck() {

		WkLibChecker wkLibChecker = new WkLibChecker("com.cy.work", "work-common");
		wkLibChecker.compareLibVersion("com.cy.commons", "employee-rest-client");

		return new ResponseEntity<String>(
		        "finish", HttpStatus.OK);
	}

	/**
	 * @return
	 */
	// @RequestMapping(value = "/status/full/check", method = RequestMethod.GET)
	// 配合公共探針(common-util) , 將本身專案的探針API改名
	@RequestMapping(value = "/status/full/selfcheck", method = RequestMethod.GET)
	public ResponseEntity<String> responseFullCheck() {
		String statusMsg = this.collectErrStatus();
		if (statusMsg.contains("faild")) {
			return new ResponseEntity<String>(statusMsg, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}

	@RequestMapping(value = "/status/response/check", method = RequestMethod.GET)
	public ResponseEntity<String> responseCheck() {
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}

	@RequestMapping(value = "/status", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
	public ResponseEntity<String> monitor() {
		String statusMsg = this.collectErrStatus();
		if (statusMsg.contains("faild")) {
			return new ResponseEntity<String>(statusMsg, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}

	@RequestMapping(value = "/status/report", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
	public ResponseEntity<String> report() {
		String statusMsg = this.collectErrStatus();
		if (statusMsg.contains("faild")) {
			return new ResponseEntity<String>(statusMsg, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<String>(statusMsg, HttpStatus.OK);
	}

	private String collectErrStatus() {
		log.info("monitor status check start");
		User testUsr = WkUserCache.getInstance().findBySid(testUsrSid);
		Org testOrg = WkOrgCache.getInstance().findBySid(testOrgSid);
		StringBuilder sb = new StringBuilder();
		this.combinTitle(sb);
		StopWatch sw = null;
		StopWatch total = new StopWatch();
		total.start();
		sb.append("<TABLE BORDER>");
		checkPmisService(sb);
		this.statusByWkCnt(sw, sb);
		this.statusByAlert(sw, sb, testUsr);
		this.statusByReqUrl(sw, sb);
		this.statusByCustomer(sw, sb);
		this.statusByNotify(sw, sb);
		this.statusByUrl(sw, sb);
		this.statusByProcess(sw, sb, testOrg, testUsr);
		sb.append("</TABLE>");
		total.stop();
		sb.append("TOTAL ").append(total.shortSummary());
		log.info("monitor status check end");
		return sb.toString();
	}

	private void combinTitle(StringBuilder sb) {
		sb.append("<TABLE BORDER>");
		sb.append("<TR>").append("<TD>").append("test usr sid").append("</TD>").append("<TD>").append(testUsrSid).append("</TD>").append("</TR>");
		sb.append("<TR>").append("<TD>").append("test org sid  ").append("</TD>").append("<TD>").append(testOrgSid).append("</TD>").append("</TR>");
		sb.append("</TABLE>");
		sb.append("<HR>");
	}

	private void combinMessage(StringBuilder sb, String clzName, String method, String note) {
		sb.append("<TR>")
		        .append("<TD>").append(clzName).append("</TD>")
		        .append("<TD>").append(method).append("</TD>")
		        .append("<TD>").append(note).append("</TD>")
		        .append("</TR>");
	}

	private void checkPmisService(StringBuilder sb) {
		StopWatch sw = new StopWatch();
		sw.start();
		PmisResponseVO vo;
		try {
			vo = pmisService.sendDataToPmis("Rest full check", "{\"token\":\"\"}");
		} catch (UserMessageException e) {
			sw.stop();
			this.combinMessage(sb, "pmisService", "checkPmisService faild", e.getMessage());
			return;
		}

		if (!vo.isOk()) {
			sw.stop();
			this.combinMessage(sb, "pmisService", "checkPmisService faild", vo.getMessage());
			return;
		}

		sw.stop();
		this.combinMessage(sb, "pmisService", "checkPmisService", sw.shortSummary());
	}

	private void statusByWkCnt(StopWatch sw, StringBuilder sb) {
		try {
			sw = new StopWatch();
			sw.start();
			countToDoService.countAllWaitingTask(testUsrSid);
			sw.stop();
			this.combinMessage(sb, "cntService", "countAllWaitingTask", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "cntService", "countAllWaitingTask faild", e.getMessage());
		}
	}

	private void statusByAlert(StopWatch sw, StringBuilder sb, User testUsr) {
		// onpg
		try {
			sw = new StopWatch();
			sw.start();
			opAlert.findByReadStatusAndReceiverLimit(ReadRecordType.NEED_READ, testUsr, 1);
			sw.stop();
			this.combinMessage(sb, "opAlert", "findByReadStatusAndReceiverLimit", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "opAlert", "findByReadStatusAndReceiverLimit faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			opAlert.findByReadStatusAndReceiver(ReadRecordType.NEED_READ, testUsr);
			sw.stop();
			this.combinMessage(sb, "opAlert", "findByReadStatusAndReceiver", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "opAlert", "findByReadStatusAndReceiver faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			opAlert.updateClickTime("1");
			sw.stop();
			this.combinMessage(sb, "opAlert", "updateClickTime", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "opAlert", "updateClickTime faild", e.getMessage());
		}
		// othset
		try {
			sw = new StopWatch();
			sw.start();
			osAlert.findByReadStatusAndReceiverLimit(ReadRecordType.NEED_READ, testUsr, 1);
			sw.stop();
			this.combinMessage(sb, "osAlert", "findByReadStatusAndReceiverLimit", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "osAlert", "findByReadStatusAndReceiverLimit faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			osAlert.findByReadStatusAndReceiver(ReadRecordType.NEED_READ, testUsr);
			sw.stop();
			this.combinMessage(sb, "osAlert", "findByReadStatusAndReceiver", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "osAlert", "findByReadStatusAndReceiver faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			osAlert.updateClickTime("1");
			sw.stop();
			this.combinMessage(sb, "osAlert", "updateClickTime", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "osAlert", "updateClickTime faild", e.getMessage());
		}
		// prototype
		try {
			sw = new StopWatch();
			sw.start();
			ptAlert.findByReadStatusAndReceiverLimit(ReadRecordType.NEED_READ, testUsr, 1);
			sw.stop();
			this.combinMessage(sb, "ptAlert", "findByReadStatusAndReceiverLimit", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "ptAlert", "findByReadStatusAndReceiverLimit faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			ptAlert.findByReadStatusAndReceiver(ReadRecordType.NEED_READ, testUsr);
			sw.stop();
			this.combinMessage(sb, "ptAlert", "findByReadStatusAndReceiver", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "ptAlert", "findByReadStatusAndReceiver faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			ptAlert.updateClickTime("1");
			sw.stop();
			this.combinMessage(sb, "ptAlert", "updateClickTime", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "ptAlert", "updateClickTime faild", e.getMessage());
		}
		// testinfo
		try {
			sw = new StopWatch();
			sw.start();
			stAlert.findByReadStatusAndReceiverLimit(ReadRecordType.NEED_READ, testUsr, 1);
			sw.stop();
			this.combinMessage(sb, "stAlert", "findByReadStatusAndReceiverLimit", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "stAlert", "findByReadStatusAndReceiverLimit faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			stAlert.findByReadStatusAndReceiver(ReadRecordType.NEED_READ, testUsr);
			sw.stop();
			this.combinMessage(sb, "stAlert", "findByReadStatusAndReceiver", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "stAlert", "findByReadStatusAndReceiver faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			stAlert.updateClickTime("1");
			sw.stop();
			this.combinMessage(sb, "stAlert", "updateClickTime", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "stAlert", "updateClickTime faild", e.getMessage());
		}
	}

	private void statusByReqUrl(StopWatch sw, StringBuilder sb) {
		try {
			sw = new StopWatch();
			sw.start();
			reqUrlService.findOnpgUrl(testUsrSid, "", "");
			sw.stop();
			this.combinMessage(sb, "reqUrlService", "findOnpgUrl", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "reqUrlService", "findOnpgUrl faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			reqUrlService.findOthsetUrl(testUsrSid, "", "");
			sw.stop();
			this.combinMessage(sb, "reqUrlService", "findOthsetUrl", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "reqUrlService", "findOthsetUrl faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			reqUrlService.findPtUrl(testUsrSid, "", "");
			sw.stop();
			this.combinMessage(sb, "reqUrlService", "findPtUrl", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "reqUrlService", "findPtUrl faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			reqUrlService.findSendTestUrl(testUsrSid, "", "");
			sw.stop();
			this.combinMessage(sb, "reqUrlService", "findSendTestUrl", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "reqUrlService", "findSendTestUrl faild", e.getMessage());
		}
	}

	private void statusByCustomer(StopWatch sw, StringBuilder sb) {
		try {
			sw = new StopWatch();
			sw.start();
			workCustomerSyncClient.process();
			sw.stop();
			this.combinMessage(sb, "customerController", "findCustomerSource", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "customerController", "findCustomerSource faild", e.getMessage());
		}
	}

	private void statusByNotify(StopWatch sw, StringBuilder sb) {
		try {
			sw = new StopWatch();
			sw.start();
			notifyService.findFirst10ByUserSid(testUsrSid);
			sw.stop();
			this.combinMessage(sb, "notifyService", "findFirst10ByUserSid", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "notifyService", "findFirst10ByUserSid faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			notifyService.readNotify(1, NotifyType.REQUEST_AGAIN_REPLY, "1");
			sw.stop();
			this.combinMessage(sb, "notifyService", "readNotify", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "notifyService", "readNotify faild", e.getMessage());
		}
	}

	private void statusByUrl(StopWatch sw, StringBuilder sb) {
		try {
			sw = new StopWatch();
			sw.start();
			urlService.createUrlLinkParam(URLService.URLServiceAttr.URL_ATTR_M, urlService.createSimpleUrlTo(testUsrSid, "", 1));
			sw.stop();
			this.combinMessage(sb, "urlService", "createUrlLinkParam", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "urlService", "createUrlLinkParam faild", e.getMessage());
		}
	}

	private void statusByProcess(StopWatch sw, StringBuilder sb, Org testOrg, User testUsr) {
		// require unit
		try {
			sw = new StopWatch();
			sw.start();
			reqUnitProcess.findByFormId("", testUsr.getUuid(), LanguageType.ZH);
			sw.stop();
			this.combinMessage(sb, "reqUnitProcess", "findByFormId", sw.shortSummary());
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			this.combinMessage(sb, "reqUnitProcess", "findByFormId faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			reqUnitProcess.findUnSignByUser(testUsr.getUuid(), LanguageType.ZH);
			sw.stop();
			this.combinMessage(sb, "reqUnitProcess", "findUnSignByUser", sw.shortSummary());
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			this.combinMessage(sb, "reqUnitProcess", "findUnSignByUser faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			reqUnitProcess.findSignedByUserAndOrg(testOrg.getUuid(), new Date(), new Date(), testUsr.getUuid(), LanguageType.ZH);
			sw.stop();
			this.combinMessage(sb, "reqUnitProcess", "findSignedByUserAndOrg", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "reqUnitProcess", "findSignedByUserAndOrg faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			reqUnitProcess.findSignedByUser(new Date(), new Date(), testUsr.getUuid(), LanguageType.ZH);
			sw.stop();
			this.combinMessage(sb, "reqUnitProcess", "findSignedByUser", sw.shortSummary());
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			this.combinMessage(sb, "reqUnitProcess", "findSignedByUser faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			reqUnitProcess.combinFormBtn("", testUsr.getUuid());
			sw.stop();
			this.combinMessage(sb, "reqUnitProcess", "combinFormBtn", sw.shortSummary());
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			this.combinMessage(sb, "reqUnitProcess", "combinFormBtn faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			reqUnitProcess.doBatchSign(new ArrayList<String>(), testUsr.getUuid());
			sw.stop();
			this.combinMessage(sb, "reqUnitProcess", "doBatchSign", sw.shortSummary());
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			this.combinMessage(sb, "reqUnitProcess", "doBatchSign faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			reqUnitProcess.doActionImpl("", new HashMap<String, Object>(), testUsr.getUuid(), LanguageType.ZH);
			sw.stop();
			this.combinMessage(sb, "reqUnitProcess", "doActionImpl", sw.shortSummary());
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			this.combinMessage(sb, "reqUnitProcess", "doActionImpl faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			reqUnitProcess.findFormTypeForUser("", testUsr.getUuid());
			sw.stop();
			this.combinMessage(sb, "reqUnitProcess", "findFormTypeForUser", sw.shortSummary());
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			this.combinMessage(sb, "reqUnitProcess", "findFormTypeForUser faild", e.getMessage());
		}
		
		// prototype
		try {
			sw = new StopWatch();
			sw.start();
			ptProcess.findByFormId("", testUsr.getUuid(), LanguageType.ZH);
			sw.stop();
			this.combinMessage(sb, "ptProcess", "findByFormId", sw.shortSummary());
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			this.combinMessage(sb, "ptProcess", "findByFormId faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			ptProcess.findUnSignByUser(testUsr.getUuid(), LanguageType.ZH);
			sw.stop();
			this.combinMessage(sb, "ptProcess", "findUnSignByUser", sw.shortSummary());
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			this.combinMessage(sb, "ptProcess", "findUnSignByUser faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			ptProcess.findSignedByUserAndOrg(testOrg.getUuid(), new Date(), new Date(), testUsr.getUuid(), LanguageType.ZH);
			sw.stop();
			this.combinMessage(sb, "ptProcess", "findSignedByUserAndOrg", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "ptProcess", "findSignedByUserAndOrg faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			ptProcess.findSignedByUser(new Date(), new Date(), testUsr.getUuid(), LanguageType.ZH);
			sw.stop();
			this.combinMessage(sb, "ptProcess", "findSignedByUser", sw.shortSummary());
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			this.combinMessage(sb, "ptProcess", "findSignedByUser faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			ptProcess.combinFormBtn("", testUsr.getUuid());
			sw.stop();
			this.combinMessage(sb, "ptProcess", "combinFormBtn", sw.shortSummary());
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			this.combinMessage(sb, "ptProcess", "combinFormBtn faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			ptProcess.doBatchSign(new ArrayList<String>(), testUsr.getUuid());
			sw.stop();
			this.combinMessage(sb, "ptProcess", "doBatchSign", sw.shortSummary());
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			this.combinMessage(sb, "ptProcess", "doBatchSign faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			ptProcess.doActionImpl("", new HashMap<String, Object>(), testUsr.getUuid(), LanguageType.ZH);
			sw.stop();
			this.combinMessage(sb, "ptProcess", "doActionImpl", sw.shortSummary());
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			this.combinMessage(sb, "ptProcess", "doActionImpl faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			ptProcess.findFormTypeForUser("", testUsr.getUuid());
			sw.stop();
			this.combinMessage(sb, "ptProcess", "findFormTypeForUser", sw.shortSummary());
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			this.combinMessage(sb, "ptProcess", "findFormTypeForUser faild", e.getMessage());
		}
		// test info
		try {
			sw = new StopWatch();
			sw.start();
			stProcess.findByFormId("", testUsr.getUuid(), LanguageType.ZH);
			sw.stop();
			this.combinMessage(sb, "stProcess", "findByFormId", sw.shortSummary());
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			this.combinMessage(sb, "stProcess", "findByFormId faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			stProcess.findUnSignByUser(testUsr.getUuid(), LanguageType.ZH);
			sw.stop();
			this.combinMessage(sb, "stProcess", "findUnSignByUser", sw.shortSummary());
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			this.combinMessage(sb, "stProcess", "findUnSignByUser faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			stProcess.findSignedByUserAndOrg(testOrg.getUuid(), new Date(), new Date(), testUsr.getUuid(), LanguageType.ZH);
			sw.stop();
			this.combinMessage(sb, "stProcess", "findSignedByUserAndOrg", sw.shortSummary());
		} catch (Exception e) {
			this.combinMessage(sb, "stProcess", "findSignedByUserAndOrg faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			stProcess.findSignedByUser(new Date(), new Date(), testUsr.getUuid(), LanguageType.ZH);
			sw.stop();
			this.combinMessage(sb, "stProcess", "findSignedByUser", sw.shortSummary());
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			this.combinMessage(sb, "stProcess", "findSignedByUser faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			stProcess.combinFormBtn("", testUsr.getUuid());
			sw.stop();
			this.combinMessage(sb, "stProcess", "combinFormBtn", sw.shortSummary());
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			this.combinMessage(sb, "stProcess", "combinFormBtn faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			stProcess.doBatchSign(new ArrayList<String>(), testUsr.getUuid());
			sw.stop();
			this.combinMessage(sb, "stProcess", "doBatchSign", sw.shortSummary());
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			this.combinMessage(sb, "stProcess", "doBatchSign faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			stProcess.doActionImpl("", new HashMap<String, Object>(), testUsr.getUuid(), LanguageType.ZH);
			sw.stop();
			this.combinMessage(sb, "stProcess", "doActionImpl", sw.shortSummary());
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			this.combinMessage(sb, "stProcess", "doActionImpl faild", e.getMessage());
		}
		try {
			sw = new StopWatch();
			sw.start();
			stProcess.findFormTypeForUser("", testUsr.getUuid());
			sw.stop();
			this.combinMessage(sb, "stProcess", "findFormTypeForUser", sw.shortSummary());
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			this.combinMessage(sb, "stProcess", "findFormTypeForUser faild", e.getMessage());
		}
	}
}
