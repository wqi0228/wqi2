/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.rest.controller.alert;

import com.cy.commons.util.Base64Utils;
import com.cy.tech.request.rest.logic.ReqUrlRestService;
import com.cy.tech.request.rest.logic.alert.SendTestAlertRestService;
import com.cy.tech.request.rest.logic.to.WorkTestAlertTo;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.ReadRecordType;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 送測通知rest控制
 *
 * @author shaun
 */
@RestController
@RequestMapping("/alert/send-test")
@Slf4j
@SuppressWarnings({ "rawtypes", "unchecked" })
public class SendTestAlertController implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3959640133734363360L;
    @Autowired
    private SendTestAlertRestService starService;
    @Autowired
    private ReqUrlRestService rurService;
    @RequestMapping(value = "/find/by/read/status/and/receiver/and/limit", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<WorkTestAlertTo>> findByReadStatusAndReceiverLimit(
            @RequestParam(value = "readStatus") ReadRecordType readStatus,
            @RequestParam(value = "receiverSid") Integer receiverSid,
            @RequestParam(value = "limit") Integer limit) {
        try {
            return new ResponseEntity(starService.findByReadStatusAndReceiverLimit(readStatus, WkUserCache.getInstance().findBySid(receiverSid), limit), HttpStatus.OK);
        } catch (Exception e) {
            return this.createErrorResponse(e.getMessage());
        }
    }

    @RequestMapping(value = "/find/by/read/status/and/receiver", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<WorkTestAlertTo>> findByReadStatusAndReceiver(
            @RequestParam(value = "readStatus") ReadRecordType readStatus,
            @RequestParam(value = "receiverSid") Integer receiverSid,
            @RequestParam(value = "startDate") Long pStartDate,
            @RequestParam(value = "endDate") Long pEndDate) {
        try {
            Date startDate = pStartDate == -1 ? null : new Date(pStartDate);
            Date endDate = pEndDate == -1 ? null : new Date(pEndDate);
            return new ResponseEntity(starService.findByReadStatusAndReceiverAndSendDate(readStatus, WkUserCache.getInstance().findBySid(receiverSid), startDate, endDate),
                    HttpStatus.OK);
        } catch (Exception e) {
            return this.createErrorResponse(e.getMessage());
        }
    }

    @RequestMapping(value = "/update/click/time", method = RequestMethod.PUT)
    public ResponseEntity updateClickTime(@RequestParam(value = "alertSid") String alertSid) {
        try {
            starService.updateClickTime(alertSid);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return this.createErrorResponse(e.getMessage());
        }
    }

    @RequestMapping(value = "/find/url", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> findUrl(
            @RequestParam(value = "userSid") Integer userSid,
            @RequestParam(value = "requireNo") String requireNo,
            @RequestParam(value = "testinfoSid") String testinfoSid) {
        try {
            return new ResponseEntity(rurService.findSendTestUrl(userSid, requireNo, testinfoSid), HttpStatus.OK);
        } catch (Exception e) {
            return this.createErrorResponse(e.getMessage());
        }
    }

    /**
     * 建立錯誤訊息
     *
     * @param errMsg
     * @return
     */
    public ResponseEntity createErrorResponse(String errMsg) {
        log.error(errMsg);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("error", Base64Utils.encode(errMsg));
        return new ResponseEntity(responseHeaders, HttpStatus.SEE_OTHER);
    }

}
