/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.rest.controller.alert;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cy.commons.util.Base64Utils;
import com.cy.tech.request.rest.logic.ReqUrlRestService;
import com.cy.tech.request.rest.logic.alert.PtAlertRestService;
import com.cy.tech.request.rest.logic.to.PtAlertTo;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.ReadRecordType;

import lombok.extern.slf4j.Slf4j;

/**
 * 原型確認通知rest控制
 *
 * @author shaun
 */
@RestController
@RequestMapping("/alert/prototype")
@Slf4j
@SuppressWarnings({ "rawtypes", "unchecked" })
public class PtAlertController implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7806956980751511762L;
    @Autowired
    private PtAlertRestService starService;
    @Autowired
    private ReqUrlRestService rurService;

    @RequestMapping(value = "/find/by/read/status/and/receiver/and/limit", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<PtAlertTo>> findByReadStatusAndReceiverLimit(
              @RequestParam(value = "readStatus") ReadRecordType readStatus,
              @RequestParam(value = "receiverSid") Integer receiverSid,
              @RequestParam(value = "limit") Integer limit) {
        try {
            return new ResponseEntity(starService.findByReadStatusAndReceiverLimit(readStatus, WkUserCache.getInstance().findBySid(receiverSid), limit), HttpStatus.OK);
        } catch (Exception e) {
            return this.createErrorResponse(e.getMessage());
        }
    }

    @RequestMapping(value = "/find/by/read/status/and/receiver", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<PtAlertTo>> findByReadStatusAndReceiver(
              @RequestParam(value = "readStatus") ReadRecordType readStatus,
              @RequestParam(value = "receiverSid") Integer receiverSid,
              @RequestParam(value = "startDate") Long pStartDate,
              @RequestParam(value = "endDate") Long pEndDate) {
        try {
            Date startDate = pStartDate == -1 ? null : new Date(pStartDate);
            Date endDate = pEndDate == -1 ? null : new Date(pEndDate);
            List<PtAlertTo> list = starService.findByReadStatusAndReceiverAndSendDate(readStatus,
                    WkUserCache.getInstance().findBySid(receiverSid), startDate, endDate);
            return new ResponseEntity<List<PtAlertTo>>(list, HttpStatus.OK);
        } catch (Exception e) {
            return this.createErrorResponse(e.getMessage());
        }
    }

    @RequestMapping(value = "/update/click/time", method = RequestMethod.PUT)
    public ResponseEntity updateClickTime(@RequestParam(value = "alertSid") String alertSid) {
        try {
            starService.updateClickTime(alertSid);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return this.createErrorResponse(e.getMessage());
        }
    }

    @RequestMapping(value = "/find/url", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> findUrl(
              @RequestParam(value = "userSid") Integer userSid,
              @RequestParam(value = "requireNo") String requireNo,
              @RequestParam(value = "ptSid") String ptSid) {
        try {
            return new ResponseEntity(rurService.findPtUrl(userSid, requireNo, ptSid), HttpStatus.OK);
        } catch (Exception e) {
            return this.createErrorResponse(e.getMessage());
        }
    }

    /**
     * 建立錯誤訊息
     *
     * @param errMsg
     * @return
     */
    public ResponseEntity createErrorResponse(String errMsg) {
        log.error(errMsg);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("error", Base64Utils.encode(errMsg));
        return new ResponseEntity(responseHeaders, HttpStatus.SEE_OTHER);
    }

}
