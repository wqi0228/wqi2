/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.pt.converter;

import com.cy.tech.request.vo.pt.enums.PtHistoryBehavior;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 送測 歷史 狀態
 *
 * @author shaun
 */
@Slf4j
@Converter
public class PtHistoryBehaviorConverter implements AttributeConverter<PtHistoryBehavior, String> {

    @Override
    public String convertToDatabaseColumn(PtHistoryBehavior attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public PtHistoryBehavior convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return PtHistoryBehavior.valueOf(dbData);
        } catch (Exception e) {
            log.error("PtHistoryBehavior Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
