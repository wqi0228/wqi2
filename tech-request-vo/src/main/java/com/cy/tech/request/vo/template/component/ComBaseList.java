/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.template.component;

/**
 * List 型態元件覆寫
 *
 * @author shaun
 */
public interface ComBaseList {

    /**
     * 新增列
     *
     * @param webNotify - web層通知介面
     */
    public void add(ComWebLayerNotify webNotify);

    /** 刪除列
     *
     * @param index */
    public void sub(int index);
}
