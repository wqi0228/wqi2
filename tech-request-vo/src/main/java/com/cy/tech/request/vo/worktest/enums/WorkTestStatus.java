/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.worktest.enums;

import com.cy.work.common.utils.WkStringUtils;

import lombok.Getter;

/**
 * 送測狀態
 *
 * @author shaun
 */
public enum WorkTestStatus {

    /** 0 送測簽核中 */
    SIGN_PROCESS("送測簽核中", false),
    /** 1 審核作廢 */
    VERIFY_INVAILD("審核作廢", true),
    /** 2 送測 */
    SONGCE("送測", false),
    /** 3 測試中 */
    TESTING("測試中", false),
    /** 4 重測 - 重測無法取消測試, 故不為流程終點 */
    RETEST("重測", false),
    /** 5 QA測試完成 */
    QA_TEST_COMPLETE("QA測試完成", true),
    /** 6 測試完成 */
    TEST_COMPLETE("測試完成", true),
    /** 7 退測 */
    ROLL_BACK_TEST("退測", true),
    /** 8 取消測試 */
    CANCEL_TEST("取消測試", true),
    /** 9 因強制需求完成或強制結案而關閉 */
    FORCE_CLOSE("強制關閉", true);
    
    @Getter
    private String value;
    
    /**
     * 此階段為流程終點狀態
     */
    @Getter
    private boolean inFinish;
    
    private WorkTestStatus(String value, boolean inFinish){
        this.value = value;
        this.inFinish = inFinish;
    }
    
    public static WorkTestStatus safeValueOf(String str) {
        if (WkStringUtils.notEmpty(str)) {
            for (WorkTestStatus item : WorkTestStatus.values()) {
                if (item.name().equals(str)) {
                    return item;
                }
            }
        }
        return null;
    }
}
