/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.value.to;

import java.io.Serializable;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * JPA Converter 無法直接 轉換 HashMap<String,Object>
 *
 * @author shaun
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TemplateDefaultValueTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8240974403697882939L;

    private String json;

    private Map<Integer, Object> value;

}
