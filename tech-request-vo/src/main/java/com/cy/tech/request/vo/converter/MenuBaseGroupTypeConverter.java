package com.cy.tech.request.vo.converter;

import com.cy.tech.request.vo.enums.MenuBaseType;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 功能列表型態轉換器
 *
 * @author shaun
 */
@Slf4j
@Converter
public class MenuBaseGroupTypeConverter implements AttributeConverter<MenuBaseType, Integer> {

    @Override
    public MenuBaseType convertToEntityAttribute(Integer x) {
        for (MenuBaseType each : MenuBaseType.values()) {
            if (x.equals(each.ordinal())) {
                return each;
            }
        }
        log.error("轉換MenuGroupTypeEnum失敗!! 資料庫常數 = " + x);
        return MenuBaseType.UNSUPPORT;
    }

    @Override
    public Integer convertToDatabaseColumn(MenuBaseType y) {
        if (y.equals(MenuBaseType.UNSUPPORT)) {
            log.error("請檢查是否有MenuGroupTypeEnum未定義型態。" + y);
        }
        return y.ordinal();
    }

}
