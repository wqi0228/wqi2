/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import lombok.extern.slf4j.Slf4j;

/**
 * 送測異動明細表-自訂搜尋元件底層列舉
 *
 * @author brain0925_liao
 */
@Slf4j
public enum Search15QueryColumn implements SearchReportCustomEnum {
    /** 送測異動明細表-key */
    Search15Query("送測異動明細表"),
    /** 需求類別 */
    DemandType("需求類別"),
    /** 填單單位 */
    DemandDep("填單單位"),
    /** 送測單位 */
    SendTestDep("送測單位"),
    /** 模糊搜尋 */
    SearchText("模糊搜尋"),
    /** 送測單號 */
    SendTestNo("送測單號"),
    /** 類別組合 */
    CategoryCombo("類別組合"),
    /** 送測狀態 */
    SendTestStatus("送測狀態"),
    /** 立單區間-索引 */
    DateIndex("立單區間-索引");

    /** 欄位名稱 */
    private final String val;

    Search15QueryColumn(String val) {
        this.val = val;
    }

    @Override
    public String getVal() {
        return val;
    }

    @Override
    public String getUrl() {
        return "Search15";
    }

    @Override
    public String getKey() {
        return this.name();
    }

    @Override
    public SearchReportCustomEnum getSearchReportCustomEnumByName(String name) {
        try {
            return Search15QueryColumn.valueOf(name);
        } catch (Exception e) {
            log.error("getSearchReportCustomEnumByName ERROR", e);
        }
        return null;
    }
}
