/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.onpg.enums;

import lombok.Getter;

/**
 * on程式狀態
 *
 * @author shaun
 */
public enum WorkOnpgStatus {

    /**
     * 已ON上<br/>
     * 初始時的開單狀態
     */
    ALREADY_ON("已ON上", false, false, true, true, false, 1),
    /**
     * 檢查回覆<br/>
     * 只要有一筆回覆資訊 ON 程式 狀態 立刻變更為 檢查回覆
     */
    CHECK_REPLY("檢查回覆", false, false, true, true, false, 1),
    /** 取消ON程式 */
    CANCEL_ONPG("取消ON程式", true, false, false, true, true, 2),
    /** 檢查完成 */
    CHECK_COMPLETE("檢查完成", true, false, false, true, true, 0),
    /** 因強制需求完成或強制結案而關閉 */
    FORCE_CLOSE("強制關閉", true, true, false, true, true, 2),
    /** 7 單位於製作進度面板強制關閉 */
    UNIT_FORCE_CLOSE("立案單位強制關閉", true, true, false, true, true, 2);
    ;

    @Getter
    private String label;

    /**
     * 此階段為流程終點狀態
     */
    @Getter
    private boolean inFinish;

    /**
     * 強制結案的類型
     */
    @Getter
    private boolean forceCompleteStatus;

    /**
     * 為可編輯狀態
     */
    @Getter
    private boolean isCanEdit;

    /**
     * 可由MMS更新資料
     */
    @Getter
    private boolean isCanUpdateFromMmsSync;
    
    /**
     * 當MMS資料update時，因調整回 ALREADY_ON, 需重發ON程式通知者
     */
    @Getter
    private boolean isNeedNoticeWhenUpdateFromMmsSync;
    
    /**
     * 當MMS資料 delete 時是否可以取消  0:不可取消 / 1:可取消 / 2:無需動作
     */
    @Getter
    private int inCancelFromMMSSync;

    private WorkOnpgStatus(
            String label,
            boolean inFinish,
            boolean forceCompleteStatus,
            boolean isCanEdit,
            boolean isCanUpdateFromMmsSync,
            boolean isNeedNoticeWhenUpdateFromMmsSync,
            int inCancelFromMMSSync
            ) {
        this.label = label;
        this.inFinish = inFinish;
        this.forceCompleteStatus = forceCompleteStatus;
        this.isCanEdit = isCanEdit;
        this.isCanUpdateFromMmsSync = isCanUpdateFromMmsSync;
        this.isNeedNoticeWhenUpdateFromMmsSync = isNeedNoticeWhenUpdateFromMmsSync;
        this.inCancelFromMMSSync = inCancelFromMMSSync;

    }
}
