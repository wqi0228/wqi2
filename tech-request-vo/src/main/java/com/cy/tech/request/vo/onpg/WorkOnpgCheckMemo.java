/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.onpg;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.vo.converter.SidOrgConverter;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.common.vo.converter.WorkSourceTypeConverter;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * ON程式檢查註記
 *
 * @author shaun
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "work_onpg_check_memo")
public class WorkOnpgCheckMemo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5989450121190922703L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "onpg_check_memo_sid", length = 36)
    private String sid;

    /** on程式單sid */
    @ManyToOne
    @JoinColumn(name = "onpg_sid")
    private WorkOnpg onpg;

    /** on程式單號 */
    @Column(name = "onpg_no", length = 21)
    private String onpgNo;

    /** on程式來源 */
    @Convert(converter = WorkSourceTypeConverter.class)
    @Column(name = "onpg_source_type")
    private WorkSourceType sourceType;

    /** on程式來源 sid */
    @Column(name = "onpg_source_sid", length = 36)
    private String sourceSid;

    /** on程式來源 單號 */
    @Column(name = "onpg_source_no", length = 21)
    private String sourceNo;

    /** 填寫此則人員的部門 */
    @Convert(converter = SidOrgConverter.class)
    @Column(name = "memo_person_dep", nullable = false)
    private Org dept;

    /** 填寫日期 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "memo_person_sid", nullable = false)
    private User person;

    /** 建立日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "memo_dt", nullable = false)
    private Date createdDate;

    /** 異動日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "memo_udt")
    private Date updatedDate;

    /** 檢查註記內容 */
    @Column(name = "memo_content")
    private String content;

    /** 檢查註記內容 有存css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "memo_content_css")
    private String contentCss;

    /** 歷程 */
    @OneToOne(mappedBy = "checkMemo", cascade = CascadeType.PERSIST)
    private WorkOnpgHistory history;

}
