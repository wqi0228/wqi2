/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.pt.enums;

import lombok.Getter;

/**
 * 原型確認 歷程 狀態
 *
 * @author shaun
 */
public enum PtHistoryBehavior {

    /** 0 回覆 */
    REPLY("回覆"),
    /** 1 回覆的回覆 */
    REPLY_AND_REPLY("回覆的回覆"),
    /** 2 重新實作 */
    REDO("重新實作"),
    /** 3 功能符合需求 */
    FUNCTION_CONFORM("功能符合需求"),
    /** 4 修改預計完成日 */
    MODIFY_ESTABLISH_DATE("修改預計完成日"),
    /** 5 審核作廢 */
    VERIFY_INVAILD("審核作廢"),
    /** 6 因強制需求完成或強制結案而關閉 */
    FORCE_CLOSE("強制關閉"),
    ;
    
    
    @Getter
    private String label;

    private PtHistoryBehavior(String value){
        this.label = value;
    }
}
