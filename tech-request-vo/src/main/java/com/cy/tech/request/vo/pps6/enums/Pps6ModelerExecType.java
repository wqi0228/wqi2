package com.cy.tech.request.vo.pps6.enums;

import com.cy.work.common.utils.WkStringUtils;

import lombok.Getter;

/**
 * PPS6 Modeler 執行設定類別
 *
 * @author Allen
 */
public enum Pps6ModelerExecType {

    ROLE("角色"),
    USER("使用者"),
    DEP("部門"),
    ;

    /**
     * 說明
     */
    @Getter
    private String descr;

    /**
     * @param descr 類別說明
     */
    private Pps6ModelerExecType(
            String descr) {
        this.descr = descr;
    }

    /**
     * @param nameStr str
     * @return Pps6ModelerExecType
     */
    public static Pps6ModelerExecType safeValueOf(String nameStr) {
        if (WkStringUtils.notEmpty(nameStr)) {
            for (Pps6ModelerExecType pps6ModelerExecType : Pps6ModelerExecType.values()) {
                if (pps6ModelerExecType.name().equals(nameStr)) {
                    return pps6ModelerExecType;
                }
            }
        }
        return null;
    }

    /**
     * @param descrStr 說明文字
     * @return Pps6ModelerExecType
     */
    public static Pps6ModelerExecType safeDescrOf(String descrStr) {
        if (WkStringUtils.notEmpty(descrStr)) {
            for (Pps6ModelerExecType pps6ModelerExecType : Pps6ModelerExecType.values()) {
                if (pps6ModelerExecType.descr.equals(descrStr)) {
                    return pps6ModelerExecType;
                }
            }
        }
        return null;
    }

    /**
     * @param str type name
     * @return type descr
     */
    public static String safeGetDescr(String str) {
        Pps6ModelerExecType pps6ModelerExecType = safeValueOf(str);
        if (pps6ModelerExecType != null) {
            return pps6ModelerExecType.getDescr();
        }
        return "";
    }
}
