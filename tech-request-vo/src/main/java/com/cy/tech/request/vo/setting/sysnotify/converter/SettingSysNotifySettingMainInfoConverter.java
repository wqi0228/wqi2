package com.cy.tech.request.vo.setting.sysnotify.converter;

import com.cy.tech.request.vo.setting.sysnotify.to.SettingSysNotifyMainInfo;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 系統通知設定資料轉換器
 *
 * @author allen
 */
@Slf4j
@Converter
public class SettingSysNotifySettingMainInfoConverter implements AttributeConverter<SettingSysNotifyMainInfo, String> {

	/**
	 * to DB 欄位資料
	 */
	@Override
	public String convertToDatabaseColumn(SettingSysNotifyMainInfo mainInfo) {
		if (mainInfo == null) {
			return null;
		}

		try {
//			return new GsonBuilder()
//			        .setPrettyPrinting()
//			        .create()
//			        .toJson(mainInfo);
			return WkJsonUtils.getInstance().toJson(mainInfo);
		} catch (Exception e) {
			log.error("轉 json 失敗!", e);
		}

		return null;
	}

	/**
	 * DB value to SettingManagetSysNotifyVO
	 */
	@Override
	public SettingSysNotifyMainInfo convertToEntityAttribute(String dbData) {
		if (WkStringUtils.isEmpty(dbData)) {
			return null;
		}

		try {
			return WkJsonUtils.getInstance().fromJson(dbData,
			        SettingSysNotifyMainInfo.class);
//			return new Gson().fromJson(
//			        dbData,
//			        new TypeToken<SettingSysNotifyMainInfo>() {
//			        }.getType());
		} catch (Exception e) {
			log.error("轉換資料失敗! json str:【\r\n" + dbData + "\r\n】", e);
			return null;
		}
	}

}
