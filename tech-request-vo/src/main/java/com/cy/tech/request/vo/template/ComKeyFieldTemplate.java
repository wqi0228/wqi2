/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.template;

import com.cy.tech.request.vo.enums.ComType;
import java.io.Serializable;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 元件欄位模版<BR/>
 * 每次開啟單據時，都需呼叫此模版套入<BR/>
 * <BR/>
 *
 * 1. 載入模版需先進行排序 <BR/>
 * 2. 如為新建單據則建立空 Map<String, ComBase> comValueMap;<BR/>
 * 3. 如為載入檔單據則進行DocComTemplateValue Map轉換
 *
 * @author shaun
 */
@Data
@EqualsAndHashCode(of = {"fieldName", "comId"})
@AllArgsConstructor
@NoArgsConstructor
public class ComKeyFieldTemplate implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7249913211724956724L;
    private int sid;
    /** 對應key值 */
    private int keyMappingSid;
    /** 欄位名稱 */
    private String fieldName;
    /** 顯示欄位名稱 */
    private boolean showFieldName;
    /** 元件型態 */
    private ComType type;
    /** 必要輸入欄位 */
    private boolean needInput;
    /** 排序 */
    private int seq;

    /** 元件ID */
    private String comId;
    /** 元件預設值內容 */
    private Map<Integer, Object> defValue;
    /** 進行互動的元件Id */
    private String interactComId;
    /** 進行互動的元件Id 設定值 */
    private Map<Integer, Object> interactComValue;

}
