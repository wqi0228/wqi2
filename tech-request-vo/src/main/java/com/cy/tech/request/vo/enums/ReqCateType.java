/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import com.cy.work.common.utils.WkStringUtils;

/**
 * 需求分類型態
 *
 * @author shaun
 */
public enum ReqCateType {
    /** 外部 */
    EXTERNAL,
    /** 內部 */
    INTERNAL;
    
    
    /**
     * @param str 列舉 name 字串
     * @return ReqCateType
     */
    public static ReqCateType safeValueOf(String str) {
        if (WkStringUtils.notEmpty(str)) {
            for (ReqCateType enumType : ReqCateType.values()) {
                if (enumType.name().equals(str)) {
                    return enumType;
                }
            }
        }
        return null;
    }
}
