package com.cy.tech.request.vo.converter;

import com.cy.tech.request.vo.require.setting.SettingSpecSignMode;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * SettingSpecSignMode converter
 * @author allen1214_wu
 */
@Slf4j
@Converter
public class SettingSpecSignModeConverter implements AttributeConverter<SettingSpecSignMode, String> {

    @Override
    public String convertToDatabaseColumn(SettingSpecSignMode attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public SettingSpecSignMode convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return SettingSpecSignMode.valueOf(dbData);
        } catch (Exception e) {
            log.error("SettingSpecSignMode Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
