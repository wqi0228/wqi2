/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import com.cy.tech.request.vo.template.component.ComListStringTypeOne;
import com.cy.tech.request.vo.template.component.ComListStringTypeTwo;
import com.cy.tech.request.vo.template.component.ComRadioTypeOne;
import com.cy.tech.request.vo.template.component.ComSeparatorTypeOne;
import com.cy.tech.request.vo.template.component.ComEditorTypeOne;
import com.cy.tech.request.vo.template.component.ComLabelTypeOne;
import com.cy.tech.request.vo.template.component.ComLabelTypeTwo;
import com.cy.tech.request.vo.template.component.ComListStringTypeThree;
import com.cy.tech.request.vo.template.component.ComManyCheckBoxTypeOne;
import com.cy.tech.request.vo.template.component.ComRadioTypeFour;
import com.cy.tech.request.vo.template.component.ComRadioTypeThree;
import com.cy.tech.request.vo.template.component.ComRadioTypeTwo;
import com.cy.tech.request.vo.template.component.ComSelectItemCheckBoxListTypeOne;
import com.cy.tech.request.vo.template.component.ComSelectItemTypeOne;
import com.cy.tech.request.vo.template.component.ComTextTypeOne;
import lombok.Getter;

/**
 * Template 元件型態
 *
 * @author shaun
 */
@SuppressWarnings("rawtypes")
public enum ComType {

    TEXT_TYPE_ONE(ComTextTypeOne.class, "純文字輸入 一個輸入欄位"),
    EDITOR_TYPE_ONE(ComEditorTypeOne.class, "文件輸入 僅一個輸入欄位"),
    RADIO_TYPE_ONE(ComRadioTypeOne.class, "單選按鈕選項 - 互動TEXT_TYPE_ONE value03"),
    RADIO_TYPE_TWO(ComRadioTypeTwo.class, "單選按鈕選項含數值驗證"),
    RADIO_TYPE_THREE(ComRadioTypeThree.class, "多筆 - 單選按鈕選項 - 互動TEXT_TYPE_ONE value03"),
    RADIO_TYPE_FOUR(ComRadioTypeFour.class, "動態列表元件 - 單選按鈕選項 - 互動TEXT_TYPE_ONE value03"),
    SEPARATOR_TYPE_ONE(ComSeparatorTypeOne.class, "分隔線"),
    LIST_STRING_TYPE_ONE(ComListStringTypeOne.class, "多筆條列型資料 - 顯示筆數"),
    LIST_STRING_TYPE_TWO(ComListStringTypeTwo.class, "多筆條列型資料 - 未顯示筆數 - 方型輸入框"),
    LIST_STRING_TYPE_THREE(ComListStringTypeThree.class, "多筆條列型資料 - 兩個輸入框"),
    LABEL_TYPE_ONE(ComLabelTypeOne.class, "文字輸出，含欄位名稱、冒號"),
    LABEL_TYPE_TWO(ComLabelTypeTwo.class, "單純文字輸出"),
    CHECK_BOX_MANY_TYPE_ONE(ComManyCheckBoxTypeOne.class, "多筆勾選選項"),
    SELECT_ITEM_TYPE_ONE(ComSelectItemTypeOne.class, "下拉式選單"),
    SELECT_ITEM_CHECK_BOX_LIST_TYPE_ONE(ComSelectItemCheckBoxListTypeOne.class, "使用下拉式選項，切換多筆勾選選項"),;

    /**
     * 元件對應物件
     */
    
    @Getter
    private final Class comClz;

    @Getter
    private final String tip;

    /**
     * 取得元件對應物件
     *
     * @param comClz
     */
    private ComType(Class comClz, String tip) {
        this.comClz = comClz;
        this.tip = tip;
    }

}
