/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require;

import com.cy.commons.enums.Activation;
import com.cy.commons.interfaces.BaseEntity;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.work.common.vo.converter.SidOrgConverter;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * FB 及 Fusion 部門對應<BR/>
 * 舊系統 - 所以未照規範命名 column name<BR/>
 * 需求單專案不存檔，僅讀取
 *
 * @author shaun
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tc_dep_project_mapping")
public class FogbugzProjectMapping implements Serializable, BaseEntity<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 3256957457157917216L;

    @Id
    @Column(name = "MappingSid", length = 36)
    private Long sid;

    /** 對應的FB專案 */
    @Column(name = "ProjectName", length = 255)
    private String projectName;

    /** 對應的Fusion部門 */
    @Convert(converter = SidOrgConverter.class)
    @Column(name = "DepSid", nullable = false, unique = true)
    private Org dept;

    /** 狀態 */
    @Column(name = "Status", nullable = false)
    private Activation status = Activation.ACTIVE;

    /** 建立日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "Created", nullable = false)
    private Date createdDate;

    /** 建立者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "CreatedBy", nullable = false)
    private User createdUser;

    /** 異動日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "Updated")
    private Date updatedDate;

    /** 異動者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "UpdatedBy")
    private User updatedUser;
}
