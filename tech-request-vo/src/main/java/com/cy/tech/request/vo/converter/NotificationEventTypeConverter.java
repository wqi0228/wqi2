/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.cy.tech.request.vo.enums.NotificationEventType;
import com.google.common.base.Strings;

import lombok.extern.slf4j.Slf4j;

/**
 * 通知事件
 *
 * @author aken_kao
 */
@Slf4j
@Converter
public class NotificationEventTypeConverter implements AttributeConverter<NotificationEventType, String> {

    @Override
    public String convertToDatabaseColumn(NotificationEventType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public NotificationEventType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return NotificationEventType.valueOf(dbData);
        } catch (Exception e) {
            log.error("轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }

}
