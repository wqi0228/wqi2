/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.tech.request.vo.value.to.CustomColumnTo;
import com.google.common.base.Strings;
import java.io.IOException;
import java.nio.charset.Charset;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 自訂報表欄位轉換器
 *
 * @author shaun
 */
@Slf4j
@Converter
public class CustomColumnToConverter implements AttributeConverter<CustomColumnTo, byte[]> {

    @Override
    public byte[] convertToDatabaseColumn(CustomColumnTo attribute) {
        try {
            if (attribute == null) {
                return "".getBytes();
            }
            String result = WkJsonUtils.getInstance().toJson(attribute);
            if (Strings.isNullOrEmpty(result)) {
                return "".getBytes();
            }
            return result.getBytes(Charset.forName("UTF-8"));
        } catch (IOException ex) {
            log.error("parser CustomColumnTo to json fail!!" + ex.getMessage());
        }
        return "".getBytes();
    }

    @Override
    public CustomColumnTo convertToEntityAttribute(byte[] dbData) {
        if (dbData == null) {
            return new CustomColumnTo();
        }
        try {
            String jsonStr = new String(dbData, "UTF-8");
            if (Strings.isNullOrEmpty(jsonStr)) {
                return new CustomColumnTo();
            }
            return WkJsonUtils.getInstance().fromJson(jsonStr, CustomColumnTo.class);
        } catch (IOException ex) {
            log.error("parser json to CustomColumnTo fail!!" + ex.getMessage());
            return new CustomColumnTo();
        }
    }

}
