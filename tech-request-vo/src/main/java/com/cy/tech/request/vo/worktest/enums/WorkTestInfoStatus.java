package com.cy.tech.request.vo.worktest.enums;

import com.cy.work.common.utils.WkStringUtils;

import lombok.Getter;

/**
 * 送測主檔狀態
 * 
 * @author aken_kao
 *             work_test_info.qa_audit_status,
 *             work_test_info.commit_status,
 *             work_test_info.qa_schedule_status
 */
public enum WorkTestInfoStatus {
    /** qa_audit_status */
    WAIT_APPROVE("待審核 "),
    APPROVED("已審核"),
    UNNEEDED_APPROVE("不需審核"),
    /** commit_status */
    UNDEFINED("尚未定義"),
    UNCOMMIT("未提交"),
    COMMITED("已提交"),
    UNNEEDED_COMMIT("不需提交"),
    /** qa_schedule_status */
    UNJOIN_SCHEDULE("不納入排程"),
    JOIN_SCHEDULE("納入排程"),
    UNNEEDED_SCHEDULE("不需排程");

    @Getter
    private String value;

    private WorkTestInfoStatus(String value) {
        this.value = value;
    }

    public static WorkTestInfoStatus safeValueOf(String str) {
        if (WkStringUtils.notEmpty(str)) {
            for (WorkTestInfoStatus item : WorkTestInfoStatus.values()) {
                if (item.name().equals(str)) {
                    return item;
                }
            }
        }
        return null;
    }
}
