package com.cy.tech.request.vo.menu;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 功能菜單子類群組
 *
 * @author shaun
 */
@Data
@ToString(exclude = {"items"})
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_fun_item_group")
public class TrMenuItemGroup implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7638344759528118253L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "sid")
    private Integer sid;

    @ManyToOne
    @JoinColumn(name = "group_base_sid", nullable = false)
    private TrMenuGroupBase menuGroupBase;

    @Column(name = "name")
    private String name;

    @Column(name = "seq")
    private Integer seq;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "menuItemGroup", fetch = FetchType.EAGER)
    @OrderBy("seq")
    private List<TrMenuItem> items;

}
