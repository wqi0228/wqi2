package com.cy.tech.request.vo.worktest;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

public class InplaceControl implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -2310745959411942647L;
    @Getter
    @Setter
    private boolean unitEditable = false;
    @Getter
    @Setter
    private boolean themeEditable = false;
    @Getter
    @Setter
    private boolean contentEditable = false;
    @Getter
    @Setter
    private boolean commentEditable = false;
    @Getter
    @Setter
    private boolean fbNoEditable = false;
    
    public void cancel(){
        unitEditable = false;
        themeEditable = false;
        contentEditable = false;
        commentEditable = false;
        fbNoEditable = false;
    }
}
