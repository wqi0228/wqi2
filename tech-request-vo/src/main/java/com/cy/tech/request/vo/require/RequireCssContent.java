/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require;

import com.cy.work.common.vo.converter.StringBlobConverter;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 記錄需求單主檔欄位的css內容物
 *
 * @author shaun
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_require_css_content")
public class RequireCssContent implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8701260654959549935L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "css_content_sid", length = 36)
    private String sid;

    /** 需求單主檔 */
    @ManyToOne
    @JoinColumn(name = "require_sid", nullable = false)
    private Require require;

    /** 需求單單號 */
    @Column(name = "require_no", nullable = false, length = 21)
    private String requireNo;

    /** 欄位名稱 */
    @Column(name = "field_name", nullable = false, length = 255)
    private String fieldName;

    /** 欄位對應元件Id */
    @Column(name = "com_id", nullable = false, length = 36)
    private String comId;

    /** 欄位css內容 */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "content_css", nullable = true)
    private String contentCss;

}
