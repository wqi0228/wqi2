/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.onpg;

import com.cy.commons.vo.User;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.vo.converter.ReadRecordTypeConverter;
import com.cy.work.common.vo.converter.WorkSourceTypeConverter;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * ON程式通知訊息
 *
 * @author shaun
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "work_onpg_alert")
public class WorkOnpgAlert implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 541075358965291692L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "onpg_alert_sid", length = 36)
    private String sid;

    /** on程式單sid */
    @ManyToOne
    @JoinColumn(name = "onpg_sid")
    private WorkOnpg onpg;

    /** on程式單號 */
    @Column(name = "onpg_no", length = 21)
    private String onpgNo;

    /** on程式來源 */
    @Convert(converter = WorkSourceTypeConverter.class)
    @Column(name = "onpg_source_type")
    private WorkSourceType sourceType;

    /** on程式來源 sid */
    @Column(name = "onpg_source_sid", length = 36)
    private String sourceSid;

    /** on程式來源 單號 */
    @Column(name = "onpg_source_no", length = 21)
    private String sourceNo;

    /** on程式主題 */
    @Column(name = "onpg_theme", nullable = false, length = 255)
    private String onpgTheme;

    /** 寄件者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "send_usr", nullable = false)
    private User sender;

    /** 寄件日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "send_dt", nullable = false)
    private Date sendDate;

    /** 收件者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "receiver", nullable = false)
    private User receiver;

    /** 來自於哪一則ON程式檢記錄的sid */
    @OneToOne
    @JoinColumn(name = "onpg_check_record_sid", nullable = false)
    private WorkOnpgCheckRecord checkRecord;

    /** 來自於哪一則ON程式檢記錄回覆的sid */
    @OneToOne
    @JoinColumn(name = "onpg_check_record_reply_sid", nullable = false)
    private WorkOnpgCheckRecordReply checkRecordReply;

    /** 是否讀取 */
    @Convert(converter = ReadRecordTypeConverter.class)
    @Column(name = "read_status")
    private ReadRecordType readStatus;

    /** 點擊時間 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "click_time")
    private Date clickTime;

}
