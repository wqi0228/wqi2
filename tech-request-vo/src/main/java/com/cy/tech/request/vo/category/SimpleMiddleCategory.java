package com.cy.tech.request.vo.category;

import com.cy.work.common.vo.AbstractEntity;
import com.cy.work.common.vo.converter.UrgencyTypeConverter;
import com.cy.work.common.enums.UrgencyType;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

/**
 * 中類
 */
@NoArgsConstructor
@EqualsAndHashCode(of = { "sid" }, callSuper = false)
@Entity
@Table(name = "tr_basic_data_middle_category")
public class SimpleMiddleCategory extends AbstractEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7484128016599638312L;

    /**
     * SID
     */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "basic_data_middle_category_sid", length = 36)
    @Getter
    @Setter
    private String sid;

    /**
     * ID
     */
    @Column(name = "middle_category_id", unique = true, nullable = false, length = 8)
    @Getter
    @Setter
    private String id;

    /**
     * 
     */
    @Column(name = "middle_category_name", unique = true, nullable = false)
    @Getter
    @Setter
    private String name;

    /**
     * 
     */
    @Column(name = "parent_big_category", nullable = false)
    @Getter
    @Setter
    private String parentBigCategorySid;

    /**
     * 
     */
    @Convert(converter = UrgencyTypeConverter.class)
    @Column(name = "urgency", nullable = false)
    @Getter
    @Setter
    private UrgencyType urgency = UrgencyType.GENERAL;

    /**
     * 
     */
    @Column(name = "single_upload_limited", nullable = false, length = 11)
    @Getter
    @Setter
    private Integer singleUploadLimited = 10;

    /**
     * 
     */
    @Column(name = "prototype_upload_limited", nullable = false, length = 11)
    @Getter
    @Setter
    private Integer prototypeUploadLimited = 10;

    /**
     * 
     */
    @Column(name = "note", length = 255)
    @Getter
    @Setter
    private String note;

    /**
     * 
     */
    @Column(name = "seq")
    @Getter
    @Setter
    private Integer seq;

    /**
     * 權限對應 如果有多個角色字串以 '，' 分隔
     */
    @Column(name = "permission_role")
    @Getter
    @Setter
    private String permissionRole;
}
