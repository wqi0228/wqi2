package com.cy.tech.request.vo.enums;

import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;

import lombok.Getter;

/**
 * 需求製作進度型別
 *
 * @author Allen
 */
public enum RequireStatusType {
    // =============================2======3======4======5======6======7======8======9=====10=====11=====12=====13=====14=====15===16===17==
    /** 草稿 */
    DRAFT("草稿", false, false, false, true, false, false, false, false, false, true, false, false, false, false, 0, false),
    /** 新建檔 */
    NEW_INSTANCE("新建檔", false, false, false, true, false, false, false, false, false, true, true, false, false, false, 2, false),
    /** 待檢查確認 */
    WAIT_CHECK("待檢查確認", false, false, false, false, false, true, true, false, true, false, true, true, true, false, 2, false),
    /** 退件通知 */
    ROLL_BACK_NOTIFY("退件通知", false, false, false, true, false, false, true, false, false, true, true, false, false, false, 2, false),
    /** 進行中 */
    PROCESS("進行中", true, true, false, true, true, true, true, true, true, false, true, true, true, true, 1, true),
    /** 需求暫緩 */
    SUSPENDED("需求暫緩", false, false, false, true, false, true, true, false, false, false, true, false, true, false, 0, false),
    /** 已完成 */
    COMPLETED("已完成", true, true, true, true, true, true, true, true, true, false, true, true, true, true, 1, false),
    /** 結案 */
    CLOSE("結案", true, false, true, true, true, true, true, false, false, false, true, true, true, true, 1, false),
    /** 作廢 */
    INVALID("作廢", false, false, true, true, false, false, true, false, false, false, true, false, false, false, 0, false),
    /** 自動結案 */
    AUTO_CLOSED("自動結案", true, false, true, true, true, true, true, false, false, false, true, true, true, true, 1, false);

    /**
     * 1.進度名稱
     */
    @Getter
    private final String value;

    /**
     * 2.顯示需求確認完成單位面板
     */
    @Getter
    private final boolean showRequireConfirmPanel;

    /**
     * 3.可執行單位確認功能
     */
    @Getter
    private final boolean canUseRequireConfirm;

    /**
     * 4.可執行反需求完成
     */
    @Getter
    private boolean canUseRollBackFinish;

    /**
     * 5.可編輯『檢查項目(系統別)』欄位 by 需求單位<br/>
     * 對象：申請者+立案部門直系主管
     * 說明：全製作進度可編輯，但檢查時期(待檢查確認)不可編輯，避免被抽掉檢查單位
     */
    @Getter
    private boolean canEditCheckItemsByReqDep;

    /**
     * 6.可編輯『檢查項目(系統別)』欄位 by GM + 分派單位 + 通知單位
     * 對象：分派單位 (不含向上主管、向下部門成員， 不含通知單位)
     * 說明：製作進度通過『進行中』後可編輯 (作廢、暫緩單據不可編輯)
     */
    @Getter
    private boolean canEditCheckItemsByReqAssignDep;

    /**
     * 7.可編輯『檢查項目(系統別)』欄位 by GM
     * 對象：GM後台維護 -> GM設定(一般同仁)
     * 說明：進入『待檢查確認』之後的流程都可以編輯 (作廢、暫緩單據不可編輯)
     */
    @Getter
    private boolean canEditCheckItemsByGM;

    /**
     * 8.編輯『檢查項目(系統別)』時，需寫追蹤記錄
     */
    @Getter
    private boolean addTraceWhenEditCheckItems;

    /**
     * 9.可分派 (一般使用者)
     */
    @Getter
    private boolean canAssgin;

    /**
     * 10.可分派 (GM)
     */
    @Getter
    private boolean canAssginByGM;

    /**
     * 11.流程是否位於檢查確認之前<br/>
     * 1.REQ-1440:異動主責單位時 需寫異動記錄(追蹤)。製作進度為『待檢查確認』前不寫, 因建單簽核階段都是需求單位異動，無需寫記錄<br/>
     * 2.判斷系統別『檢查狀態』
     */
    @Getter
    private boolean flowBeforeWaitCheck;

    /**
     * 12.可使用的查詢選項 for 報表
     */
    @Getter
    private boolean reportQueryCondition;

    /**
     * 13.顯示檢查項目的檢查歷程
     */
    @Getter
    private boolean showRequireCheckHistory;

    /**
     * 14.可轉PMIS
     */
    @Getter
    private boolean canTrnsPMIS;

    /**
     * 15.是否可為分派相關查詢功能的可查詢狀態 (search7)
     */
    @Getter
    private boolean isAssignSearch;

    /**
     * 16.傳送給MMS 的狀態
     * 0:已刪除/或停用
     * 1:可附掛ON程式單
     * 2:簽核/檢查中 (請稍後再撥)，對應WERP的enable
     */
    @Getter
    private int syncMMSStatus;

    /**
     * 17.子單是否可編輯 (主單卡控子單)
     */
    @Getter
    private boolean isCanEditSubCase;

    private RequireStatusType(
            String value,
            boolean showRequireConfirmPanel,
            boolean canUseRequireConfirm,
            boolean canUseRollBackFinish,
            boolean canEditCheckItemsByReqDep,
            boolean canEditCheckItemsByReqAssignDep,
            boolean canEditCheckItemsByGM,
            boolean addTraceWhenEditCheckItems,
            boolean canAssgin,
            boolean canAssginByGM,
            boolean flowBeforeWaitCheck,
            boolean reportQueryCondition,
            boolean showRequireCheckHistory,
            boolean canTrnsPMIS,
            boolean isAssignSearch,
            int syncMMSStatus,
            boolean isCanEditSubCase) {

        this.value = value;

        this.showRequireConfirmPanel = showRequireConfirmPanel;
        this.canUseRequireConfirm = canUseRequireConfirm;
        this.canUseRollBackFinish = canUseRollBackFinish;
        this.canEditCheckItemsByReqDep = canEditCheckItemsByReqDep;
        this.canEditCheckItemsByReqAssignDep = canEditCheckItemsByReqAssignDep;
        this.canEditCheckItemsByGM = canEditCheckItemsByGM;
        this.addTraceWhenEditCheckItems = addTraceWhenEditCheckItems;
        this.canAssgin = canAssgin;
        this.canAssginByGM = canAssginByGM;
        this.flowBeforeWaitCheck = flowBeforeWaitCheck;
        this.reportQueryCondition = reportQueryCondition;
        this.showRequireCheckHistory = showRequireCheckHistory;
        this.canTrnsPMIS = canTrnsPMIS;
        this.isAssignSearch = isAssignSearch;
        this.syncMMSStatus = syncMMSStatus;
        this.isCanEditSubCase = isCanEditSubCase;
    }

    /**
     * @param str 列舉 name 字串
     * @return RequireStatusType
     */
    public static RequireStatusType safeValueOf(String str) {
        if (WkStringUtils.notEmpty(str)) {
            for (RequireStatusType enumType : RequireStatusType.values()) {
                if (enumType.name().equals(str)) {
                    return enumType;
                }
            }
        }
        WkCommonUtils.logWithStackTrace(InfomationLevel.ERROR, "RequireStatusType 無法解析:[" + str + "]");
        return null;
    }

    // 備份格式用
    // //=============================2======3======4======5======6======7======8======9=====10=====11=====12=====13=====14=====15===16===17==
    // /** 草稿 */
    // DRAFT("草稿", false, false, false, true, false, false, false, false, false, true, false, false, false, false, 0,
    // false),
    // /** 新建檔 */
    // NEW_INSTANCE("新建檔", false, false, false, true, false, false, false, false, false, true, true, false, false, false, 2,
    // false),
    // /** 待檢查確認 */
    // WAIT_CHECK("待檢查確認", false, false, false, false, false, true, true, false, true, false, true, true, true, false, 2,
    // false),
    // /** 退件通知 */
    // ROLL_BACK_NOTIFY("退件通知", false, false, false, true, false, false, true, false, false, true, true, false, false,
    // false, 2, false),
    // /** 進行中 */
    // PROCESS("進行中", true, true, false, true, true, true, true, true, true, false, true, true, true, true, 1, true),
    // /** 需求暫緩 */
    // SUSPENDED("需求暫緩", false, false, false, true, false, true, true, false, false, false, true, false, true, false, 0,
    // false),
    // /** 已完成 */
    // COMPLETED("已完成", true, true, true, true, true, true, true, true, true, false, true, true, true, true, 1, false),
    // /** 結案 */
    // CLOSE("結案", true, false, true, true, true, true, true, false, false, false, true, true, true, true, 1, false),
    // /** 作廢 */
    // INVALID("作廢", false, false, true, true, false, false, true, false, false, false, true, false, false, false, 0,
    // false),
    // /** 自動結案 */
    // AUTO_CLOSED("自動結案 ", true, false, true, true, true, true, true, false, false, false, true, true, true, true, 1,
    // false);

}
