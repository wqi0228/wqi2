/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 需求單-檢查項目 converter
 * @author allen1214_wu
 */
@Slf4j
@Converter
public class RequireCheckItemTypeConverter implements AttributeConverter<RequireCheckItemType, String> {

    @Override
    public String convertToDatabaseColumn(RequireCheckItemType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public RequireCheckItemType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return RequireCheckItemType.valueOf(dbData);
        } catch (Exception e) {
            log.error("RequireCheckItemType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
