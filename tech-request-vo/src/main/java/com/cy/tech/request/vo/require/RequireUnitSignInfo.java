/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require;

import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.vo.converter.InstanceStatusConverter;
import com.cy.work.common.vo.converter.JsonStringListToConverter;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 需求單位主管審核資訊
 *
 * @author shaun
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_require_manager_sign_info")
public class RequireUnitSignInfo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -9005021098509111828L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "require_manager_sign_sid", length = 36)
    private String sid;

    /** 需求單主檔 */
    @OneToOne
    @JoinColumn(name = "require_sid", nullable = false, unique = true)
    private Require require;

    /** 需求單單號 */
    @Column(name = "require_no", nullable = false, length = 21, unique = true)
    private String requireNo;

    /** 流程狀態 */
    @Convert(converter = InstanceStatusConverter.class)
    @Column(name = "paper_code", nullable = false)
    private InstanceStatus instanceStatus;

    /** 流程碼 */
    @Column(name = "bpm_instance_id", nullable = false)
    private String bpmInstanceId;

    /** 預設簽核人員暱稱 */
    @Column(name = "bpm_default_signed_name", nullable = true)
    private String defaultSignedName;

    /** 可簽核人員id列表 */
    @Convert(converter = JsonStringListToConverter.class)
    @Column(name = "bpm_can_signed_id_list", nullable = true)
    private JsonStringListTo canSignedIdsTo = new JsonStringListTo();

}
