/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.tech.request.vo.value.to.SpecificDepTo;
import com.cy.tech.request.vo.value.to.DepTo;
import com.google.common.base.Strings;
import java.io.IOException;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 關聯部門轉換器
 *
 * @author shaun
 */
@Converter
@Slf4j
public class ForwardDepToConverter implements AttributeConverter<SpecificDepTo, String> {

    @Override
    public String convertToDatabaseColumn(SpecificDepTo attribute) {
        throw new UnsupportedOperationException();
    }

    @Override
    public SpecificDepTo convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return new SpecificDepTo();
        }
        try {
            SpecificDepTo to = new SpecificDepTo();
            List<DepTo> resultList = WkJsonUtils.getInstance().fromJsonToList(dbData, DepTo.class);
            to.setDepInfo(resultList);
            return to;
        } catch (IOException ex) {
            log.error("parser json to GroupsTo fail!!" + ex.getMessage());
            return new SpecificDepTo();
        }
    }

}
