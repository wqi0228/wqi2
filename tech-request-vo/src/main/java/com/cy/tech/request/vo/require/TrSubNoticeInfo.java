/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.converter.SubNoticeTypeConverter;
import com.cy.tech.request.vo.enums.SubNoticeType;
import com.cy.work.common.vo.converter.JsonStringListToConverter;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 子程序異動記錄
 *
 * @author shaun
 */
@Entity
@Table(name = "tr_sub_notice_info")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
public class TrSubNoticeInfo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8956786651480696993L;

    @Id
    @Column(name = "notice_info_sid", nullable = false, length = 21)
    private String sid;

    /** 通知類型 */
    @Convert(converter = SubNoticeTypeConverter.class)
    @Column(name = "notice_type", nullable = false, length = 45)
    private SubNoticeType type;

    /** 需求單主檔sid */
    @Column(name = "require_sid", nullable = false)
    private String requireSid;

    /** 需求單單號 */
    @Column(name = "require_no", nullable = false)
    private String requireNo;

    /** 子程序sid */
    @Column(name = "sub_process_sid", nullable = false)
    private String subProcessSid;

    /** 子程序單號 */
    @Column(name = "sub_process_no", nullable = false)
    private String subProcessNo;

    /** 舊的(通知部門 或 通知人員) */
    @Convert(converter = JsonStringListToConverter.class)
    @Column(name = "old_notice_info", nullable = false)
    private JsonStringListTo oldNoticeInfo = new JsonStringListTo();

    /** 新的(通知部門 或 通知人員) */
    @Convert(converter = JsonStringListToConverter.class)
    @Column(name = "new_notice_info", nullable = false)
    private JsonStringListTo newNoticeInfo = new JsonStringListTo();

    /** 狀態 */
    @Column(name = "status", nullable = false)
    private Activation status;

    /** 建立者 */
    @Column(name = "create_usr", nullable = false)
    private Integer createdUser;

    /** 建立時間 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    /** 更新者 */
    @Column(name = "update_usr", nullable = false)
    private Integer updatedUser;

    /** 更新時間 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt", nullable = false)
    private Date updatedDate;

}
