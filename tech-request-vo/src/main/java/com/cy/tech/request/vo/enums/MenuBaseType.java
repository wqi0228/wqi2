package com.cy.tech.request.vo.enums;

/**
 * 功能列表
 *
 * @author shaun
 */
public enum MenuBaseType {

    /** 設定 */
    SETTING,
    /** 建立 */
    CREATE,
    /** 報表 */
    REPORT,
    /** 首頁 */
    HOME,
    /** 未支援型態 */
    UNSUPPORT
}
