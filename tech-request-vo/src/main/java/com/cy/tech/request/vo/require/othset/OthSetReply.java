/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require.othset;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.common.vo.converter.SidOrgConverter;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 其他設定資訊 回覆
 *
 * @author shaun
 */
@Data
@NoArgsConstructor
@ToString(exclude = {"alreadyReplys"})
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_os_reply")
public class OthSetReply implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3350736650969267463L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "os_reply_sid", length = 36)
    private String sid;

    /** 其他設定資訊主檔 */
    @ManyToOne
    @JoinColumn(name = "os_sid", nullable = false)
    private OthSet othset;

    /** 其他設定資訊單號 */
    @Column(name = "os_no", nullable = false, length = 21)
    private String osNo;

    /** 需求單主檔 */
    @ManyToOne
    @JoinColumn(name = "require_sid", nullable = false)
    private Require require;

    /** 需求單單號 */
    @Column(name = "require_no", nullable = false, length = 21)
    private String requireNo;

    /** 回覆人員的部門 */
    @Convert(converter = SidOrgConverter.class)
    @Column(name = "reply_person_dep", nullable = false)
    private Org dep;

    /** 回覆人員 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "reply_person_sid", nullable = false)
    private User person;

    /** 回覆日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "reply_dt", nullable = false)
    private Date date;

    /** 異動回覆資訊的日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "reply_udt", nullable = false)
    private Date updateDate;

    /** 回覆內容 原文 */
    @Column(name = "reply_content", nullable = false)
    private String content;

    /** 回覆內容 含css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "reply_content_css")
    private String contentCss;

    /** 本回覆的回覆 */
    @OneToMany(mappedBy = "reply", cascade = CascadeType.ALL)
    @OrderBy("updateDate DESC")
    private List<OthSetAlreadyReply> alreadyReplys = Lists.newArrayList();

    /** 回覆歷程 */
    @OneToOne(mappedBy = "reply", cascade = CascadeType.PERSIST)
    private OthSetHistory history;
}
