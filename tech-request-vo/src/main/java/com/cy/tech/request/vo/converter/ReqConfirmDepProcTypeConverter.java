package com.cy.tech.request.vo.converter;

import com.cy.tech.request.vo.enums.ReqConfirmDepProcType;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 需求完成確認部門 - 操作類型 型態轉換器
 */
@Slf4j
@Converter
public class ReqConfirmDepProcTypeConverter implements AttributeConverter<ReqConfirmDepProcType, String> {

    @Override
    public String convertToDatabaseColumn(ReqConfirmDepProcType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public ReqConfirmDepProcType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return ReqConfirmDepProcType.valueOf(dbData);
        } catch (Exception e) {
            log.error("GroupSetupType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
