/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import com.cy.tech.request.vo.enums.RequireFinishCodeType;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 製作進度完成碼
 *
 * @author shaun
 */
@Slf4j
@Converter
public class RequireFinishCodeTypeConverter implements AttributeConverter<RequireFinishCodeType, String> {

    @Override
    public String convertToDatabaseColumn(RequireFinishCodeType attribute) {
        return attribute.getCode();
    }

    @Override
    public RequireFinishCodeType convertToEntityAttribute(String dbData) {
        if (dbData == null) {
            return null;
        }
        for (RequireFinishCodeType each : RequireFinishCodeType.values()) {
            if (dbData.equals(each.getCode())) {
                return each;
            }
        }
        log.error("製作進度完成碼無法對應enum型態，dbData = " + dbData);
        return null;
    }
}
