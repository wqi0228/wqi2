/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import com.cy.tech.request.vo.value.to.TemplateDefaultValueTo;
import com.cy.work.common.utils.WkJsonUtils;
import com.google.common.base.Strings;
import java.io.IOException;
import java.util.Map;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 需求單模版預設資料轉換器
 *
 * @author shaun
 */
@Slf4j
@Converter
public class DefaultValueJsonConverter implements AttributeConverter<TemplateDefaultValueTo, String> {

    @Override
    public String convertToDatabaseColumn(TemplateDefaultValueTo attribute) {
        try {
            if (attribute == null) {
                return "";
            }
            return WkJsonUtils.getInstance().toJson(attribute.getValue());
        } catch (IOException ex) {
            log.error("parser TemplateDefaultValueTo to json fail!!" + ex.getMessage());
        }
        return "";
    }

    @Override
    public TemplateDefaultValueTo convertToEntityAttribute(String dbData) {
        if (dbData == null) {
            return null;
        }
        try {
            TemplateDefaultValueTo dv = new TemplateDefaultValueTo();
            dv.setJson(dbData);
            if (Strings.isNullOrEmpty(dbData)) {
                return dv;
            }
            dv.setValue((Map<Integer, Object>) WkJsonUtils.getInstance().fromJsonToHashMapKeyInteger(dbData));
            return dv;
        } catch (IOException ex) {
            log.error("parser json to TemplateDefaultValueTo fail!!" + ex.getMessage());
            return null;
        }
    }

}
