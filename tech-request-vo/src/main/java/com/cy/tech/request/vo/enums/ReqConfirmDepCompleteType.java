package com.cy.tech.request.vo.enums;

import lombok.Getter;

/**
 * 需求完成確認單位 - 處理結果類型
 * @author allen1214_wu
 */
public enum ReqConfirmDepCompleteType {

    FINISH("已執行 / 確認"),
    WONT_DO("無需處理");

    /**
     * 顯示名稱
     */
    @Getter
    private final String label;


    /**
     * @param label
     * @param progressPercent
     * @param progressColer
     */
    private ReqConfirmDepCompleteType(String label) {
        this.label = label;
    }
}
