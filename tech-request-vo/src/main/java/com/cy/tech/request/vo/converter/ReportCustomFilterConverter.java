/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.tech.request.vo.value.to.ReportCustomFilterDetailTo;
import com.cy.tech.request.vo.value.to.ReportCustomFilterTo;
import com.google.common.base.Strings;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 關聯部門轉換器
 *
 * @author shaun
 */
@Converter
@Slf4j
public class ReportCustomFilterConverter implements AttributeConverter<ReportCustomFilterTo, String> {

    @Override
    public String convertToDatabaseColumn(ReportCustomFilterTo attribute) {
        if (attribute == null) {
            return "";
        }
        try {
            return WkJsonUtils.getInstance().toJson(attribute.getReportCustomFilterDetailTos());
        } catch (Exception ex) {
            log.error("parser ReportCustomFilterConverter to json fail!!" + ex.getMessage());
            return "";
        }
    }

    @Override
    public ReportCustomFilterTo convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return new ReportCustomFilterTo();
        }
        try {
            ReportCustomFilterTo to = new ReportCustomFilterTo();
            List<ReportCustomFilterDetailTo> resultList = WkJsonUtils.getInstance().fromJsonToList(dbData, ReportCustomFilterDetailTo.class);
            to.setReportCustomFilterDetailTos(resultList);
            return to;
        } catch (Exception ex) {
            log.error("parser json to ReportCustomFilterTo fail!!" + ex.getMessage());
            return new ReportCustomFilterTo();
        }
    }

}
