/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.onpg;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.tech.request.vo.onpg.converter.WorkOnpgAttachmentBehaviorConverter;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgAttachmentBehavior;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.vo.Attachment;
import com.cy.work.common.vo.converter.SidOrgConverter;
import com.cy.work.common.vo.converter.WorkSourceTypeConverter;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * ON程式附加檔案
 *
 * @author shaun
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "work_onpg_attachment")
public class WorkOnpgAttachment implements Serializable, Attachment<String> {

    /**
     * 
     */
    private static final long serialVersionUID = -1575125504386243500L;

    @Id
    @Column(name = "onpg_attachment_sid", length = 36)
    private String sid;

    /** on程式單sid */
    @ManyToOne
    @JoinColumn(name = "onpg_sid")
    private WorkOnpg onpg;

    /** on程式單號 */
    @Column(name = "onpg_no", length = 21)
    private String onpgNo;

    /** on程式來源 */
    @Convert(converter = WorkSourceTypeConverter.class)
    @Column(name = "onpg_source_type")
    private WorkSourceType sourceType;

    /** on程式來源 sid */
    @Column(name = "onpg_source_sid", length = 36)
    private String sourceSid;

    /** on程式來源 單號 */
    @Column(name = "onpg_source_no", length = 21)
    private String sourceNo;

    /** on程式歷程記錄sid */
    @ManyToOne
    @JoinColumn(name = "onpg_history_sid")
    private WorkOnpgHistory history;

    /** 附加檔案 上傳狀態 */
    @Convert(converter = WorkOnpgAttachmentBehaviorConverter.class)
    @Column(name = "behavior")
    private WorkOnpgAttachmentBehavior behavior;

    /** 檔案名稱 */
    @Column(name = "file_name", nullable = false, length = 255)
    private String fileName;

    /** 檔案說明 */
    @Column(name = "description", length = 255)
    private String desc = "";

    /** 上傳檔案者所屬部門 */
    @Convert(converter = SidOrgConverter.class)
    @Column(name = "department", nullable = false)
    private Org uploadDept;

    /** 狀態 */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    /** 建立者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "create_usr", nullable = false)
    private User createdUser;

    /** 建立日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    /** 異動者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "update_usr")
    private User updatedUser;

    /** 異動日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

    /**
     * 畫面用，進行編輯時，附加檔案的flag
     */
    @Transient
    private Boolean keyCheckEdit = Boolean.FALSE;

    /**
     * 畫面用，上傳附加檔案如果取消勾選，則不儲存
     */
    @Transient
    private Boolean keyChecked = Boolean.FALSE;
}
