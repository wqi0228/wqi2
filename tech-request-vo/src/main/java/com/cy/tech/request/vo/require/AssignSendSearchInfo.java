/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.work.common.vo.converter.SidOrgConverter;
import com.cy.tech.request.vo.enums.AssignSendType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 需求單分派資訊搜尋
 *
 * @author shaun
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_assign_send_search_info")
public class AssignSendSearchInfo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4353382593175707299L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "search_sid", length = 36)
    private String sid;

    /** 需求單主檔 */
    @ManyToOne
    @JoinColumn(name = "require_sid", nullable = false)
    private Require require;

    /** 需求單單號 */
    @Column(name = "require_no", nullable = false, length = 21)
    private String requireNo;

    /** 類型 分派 | 通知 */
    @Column(name = "type", nullable = false, length = 21)
    private AssignSendType type = AssignSendType.ASSIGN;

    /** 部門 */
    @Convert(converter = SidOrgConverter.class)
    @Column(name = "dep_sid", nullable = true)
    private Org department;

    /** 成員 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "member_sid", nullable = true)
    private User member;

    /** 分派此批單據的人員 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "create_usr", nullable = false)
    private User createdUser;

    /** 分派此批單據的時間 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

}
