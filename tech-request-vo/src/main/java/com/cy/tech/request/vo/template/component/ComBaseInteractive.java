/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.template.component;

import com.cy.tech.request.vo.value.to.TemplateDefaultValueTo;

/**
 * 模版內部資料基礎型別 - 互動元件型
 *
 * @author shaun
 */
public interface ComBaseInteractive extends ComBase {

    /**
     * 互動元件觸發動作
     *
     * @param interactCom
     * @param interactComValue
     */
    public void change(ComBase interactCom, TemplateDefaultValueTo interactComValue);

}
