/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require.othset;

import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

/**
 * 其他設定資訊主題
 *
 * @author shaun
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_os_theme_df_dep")
public class OthSetTheme implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4241976991653296392L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "totdd_sid", length = 36)
    private String sid;

    /** 主題 */
    @Column(name = "theme", nullable = false, length = 50, unique = true)
    private String theme;

    /** 資訊內容 */
    @Column(name = "context", nullable = false)
    private String context;

    /** 資訊內容 */
    @Column(name = "seq", nullable = false)
    private Integer seq;
    
    /** 對應通知部門 */
    @OneToMany(mappedBy = "othSetTheme", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<OthSetThemeDetail> noticeDetails = Lists.newArrayList();

    @Override
    public String toString() {
        return "OthSetTheme [theme=" + theme + "]";
    }
    
}
