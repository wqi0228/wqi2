package com.cy.tech.request.vo.setting;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.cy.tech.request.vo.anew.converter.SignLevelTypeConverter;
import com.cy.tech.request.vo.anew.enums.SignLevelType;
import com.cy.tech.request.vo.converter.SettingSpecSignModeConverter;
import com.cy.tech.request.vo.require.setting.SettingSpecSignMode;
import com.cy.work.common.vo.AbstractEntity;
import com.cy.work.common.vo.converter.SplitListIntConverter;
import com.cy.work.common.vo.converter.SplitListStrConverter;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 設定：『需求單位申請流程』特殊簽核群組
 * 
 * @author allen1214_wu
 */
@EqualsAndHashCode(callSuper = false)
@Table(name = "tr_setting_spec_sign_group")
@Entity
public class SettingSpecSignGroup extends AbstractEntity {

    /**
     * 
     */
    private static final long serialVersionUID = 2203927527890751796L;

    /**
     * SID
     */
    @Id
    @Getter
    @Setter
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "sid")
    private String sid;

    /**
     * 公司別ID
     */
    @Getter
    @Setter
    @Column(name = "comp_id")
    private String compID;

    /**
     * 群組名稱
     */
    @Getter
    @Setter
    @Column(name = "group_name")
    private String groupName;

    /**
     * 群組部門，以『,』分隔
     */
    @Getter
    @Setter
    @Convert(converter = SplitListIntConverter.class)
    @Column(name = "dep_sids")
    private List<Integer> depSids;

    /**
     * 群組部門是否含以下
     */
    @Getter
    @Setter
    @Column(name = "dep_is_below")
    private boolean depIsBelow;

    /**
     * 除外部門，以『,』分隔 (當群組含以下時可設定除外部門)
     */
    @Getter
    @Setter
    @Convert(converter = SplitListIntConverter.class)
    @Column(name = "except_dep_sids")
    private List<Integer> exceptDepSids;

    /**
     * 除外部門是否含以下
     */
    @Getter
    @Setter
    @Column(name = "except_dep_is_below")
    private boolean exceptDepIsBelow;

    /** 簽核模式 */
    @Getter
    @Setter
    @Convert(converter = SettingSpecSignModeConverter.class)
    @Column(name = "spec_sign_mode", nullable = false)
    private SettingSpecSignMode specSignMode;

    /** 最高簽核層級 （NO_SIGN:不需簽核/ THE_PANEL:組 / MINISTERIAL:部 / DIVISION_LEVEL:處 / GROUPS:群 / CEO 執行長） */
    @Getter
    @Setter
    @Convert(converter = SignLevelTypeConverter.class)
    @Column(name = "sign_level")
    private SignLevelType signLevel;

    /**
     * 指定簽核角色 (當flow_type為SPEC_SINGER時，流程依序固定跑設定角色，以『,』分隔，依順序，最多設定三人)
     */
    @Getter
    @Setter
    @Convert(converter = SplitListStrConverter.class)
    @Column(name = "signer_role_ids")
    private List<String> signerRoleIds;

    /**
     * 備註
     */
    @Getter
    @Setter
    @Column(name = "memo")
    private String memo;

    /**
     * 顯示排序
     */
    @Getter
    @Setter
    @Column(name = "show_seq")
    private Integer showSeq;
}
