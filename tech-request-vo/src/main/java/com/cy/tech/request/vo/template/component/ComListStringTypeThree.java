/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.template.component;

import com.cy.work.common.utils.WkStringUtils;
import com.cy.tech.request.vo.annotation.TemplateComMapParserDefaultValueAnnotation;
import com.cy.tech.request.vo.enums.ComType;
import com.cy.tech.request.vo.value.to.TemplateDefaultValueTo;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import com.cy.work.common.utils.WkJsonUtils;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * input text
 *
 * @author shaun
 */
@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ComListStringTypeThree implements ComBase, ComBaseList, Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 8653559476053650468L;

    /** 元件名稱陣列，於基礎欄位設定中顯示 */
	public List<String> displayComponents = Lists.newArrayList();

	/** 元件欄位名稱 */
	@JsonProperty("name")
	public String name;

	/** 元件Id */
	@JsonProperty("comId")
	public String comId;

	/** 元件type */
	@JsonProperty("type")
	public ComType type;

	/** 實作介面名稱 */
	@JsonProperty("interfaceName")
	public String interfaceName;

	/** 是否需要建立索引 */
	@JsonProperty("needCreateIndex")
	public Boolean needCreateIndex;

	/** 值 1 第一個輸入框所有值 */
	@JsonProperty("value01")
	private List<String> value01;

	/** 值 2 第二個輸入框所有值 */
	@JsonProperty("value02")
	private List<String> value02;

	/** 值3 第一個輸入框 */
	// @TemplateComMapParserDefaultValueAnnotation(key = THREE, valueName =
	// "value03", valueClz = String.class, desc = "第一個輸入框欄位暫存值。")
	@JsonProperty("value03")
	private String value03;

	@TemplateComMapParserDefaultValueAnnotation(key = FOUR, valueName = "value04", valueClz = String.class, desc = "第一個輸入框後方文字")
	@JsonProperty("value04")
	private String value04;

	/** 值5 第二個輸入框 */
	// @TemplateComMapParserDefaultValueAnnotation(key = FIVE, valueName =
	// "value05", valueClz = String.class, desc = "第二個輸入框欄位暫存值。")
	@JsonProperty("value05")
	private String value05;

	@TemplateComMapParserDefaultValueAnnotation(key = SIX, valueName = "value06", valueClz = String.class, desc = "第二個輸入框後方文字")
	@JsonProperty("value06")
	private String value06;

	@Override
	public void setDefValue(FieldKeyMapping template) {
		name = template.getFieldName();
		comId = template.getComId();
		type = template.getFieldComponentType();
		interfaceName = ComBase.class.getSimpleName();
		needCreateIndex = template.getRequiredIndex();

		TemplateDefaultValueTo dv = template.getDefaultValue();
		Map<Integer, Object> valueMap = dv.getValue();
		if (valueMap == null) {
			return;
		}
		if (valueMap.containsKey(FOUR)) {
			value04 = (String) valueMap.get(FOUR);
		}
		if (valueMap.containsKey(SIX)) {
			value06 = (String) valueMap.get(SIX);
		}
	}

	@Override
	public Boolean checkRequireValueIsEmpty() {
		// 存檔前檢查空值時，將未按儲存的值進行保存
		this.add();
		// 檢查使用者輸入欄位
		if (WkStringUtils.isEmpty(value01) || WkStringUtils.isEmpty(value02)) {
			return Boolean.TRUE;
		}
		int index = 0;
		for (String each : value01) {
			if (WkStringUtils.isEmpty(each) || WkStringUtils.isEmpty(value02.get(index++))) {
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	@Override
	public void add(ComWebLayerNotify webNotify) {
		String errMsg = this.add();
		if (!Strings.isNullOrEmpty(errMsg)) {
			webNotify.notify(errMsg);
		}
	}

	private String add() {
		if (Strings.isNullOrEmpty(this.value03) || Strings.isNullOrEmpty(this.value05)) {
			return "請輸入資訊。";
		}
		if (this.value01 == null) {
			this.value01 = Lists.newArrayList();
		}
		if (this.value02 == null) {
			this.value02 = Lists.newArrayList();
		}
		if (this.value03 == null) {
			this.value03 = "";
		}
		if (this.value05 == null) {
			this.value05 = "";
		}
		if (this.value01.contains(this.value03)) {
			int index01 = this.value01.indexOf(this.value03);
			if (this.value02.get(index01).equals(this.value05)) {
				return "已包含重覆資訊！";
			}
		}

		if (this.value02.contains(this.value05)) {
			int index02 = this.value02.indexOf(this.value05);
			if (this.value01.get(index02).equals(this.value03)) {
				return "已包含重覆資訊！";
			}
		}
		this.value01.add(this.value03);
		this.value02.add(this.value05);
		this.value03 = "";
		this.value05 = "";
		return "";
	}

	@Override
	public void sub(int index) {
		if (index < 0 || index > value01.size()) {
			return;
		}

		String v1 = value01.get(index);
		String v2 = value02.get(index);

		if (value01.contains(v1)) {
			value01.remove(v1);
		}
		if (value02.contains(v2)) {
			value02.remove(v2);
		}
	}

	@Override
	public String getCompositionText(boolean showField, int maxFieldNameLen) {
		StringBuilder sb = new StringBuilder();
		if (showField) {
			sb.append(WkStringUtils.getInstance().padEndEmpty(name, maxFieldNameLen, "：")).append(" \r\n");
		}
		sb.append("\r\n");
		for (String each : value01) {
			sb.append("  ").append(Strings.padStart(String.valueOf(value01.indexOf(each) + 1), 3, ' ')).append(". ").append(each).append(" ");
			sb.append(value04).append(" \r\n");
			sb.append("            ");
			sb.append(value02.get(value01.indexOf(each))).append(" ").append(value06);
			sb.append("\r\n\r\n");
		}
		return sb.toString();
	}

	@Override
	public String getIndexContent() {
		try {
			if (value01 == null) {
				value01 = Lists.newArrayList();
			}
			if (value02 == null) {
				value02 = Lists.newArrayList();
			}
			List<String> idxList = Lists.newArrayList();
			int idxcnt = 0;
			for (String str : value01) {
				idxList.add(str + " " + value02.get(idxcnt++));
			}
			return WkJsonUtils.getInstance().toJson(Lists.newArrayList(idxList));
		} catch (IOException | ArrayIndexOutOfBoundsException ex) {
			log.error("parse value to json string fail...");
			log.error("fail value01 = " + value01);
			log.error("fail value02 = " + value02);
			log.error("fail error " + ex.getMessage(), ex);
			return value01.toString() + " " + value02.toString();
		}
	}

}
