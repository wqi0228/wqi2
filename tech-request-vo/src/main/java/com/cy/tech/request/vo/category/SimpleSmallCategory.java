package com.cy.tech.request.vo.category;

import com.cy.tech.request.vo.anew.converter.SignLevelTypeConverter;
import com.cy.tech.request.vo.anew.enums.SignLevelType;
import com.cy.work.common.vo.AbstractEntity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 小類
 *
 * @author shaun
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = { "sid" }, callSuper = false)
@Entity
@Table(name = "tr_basic_data_small_category")
public class SimpleSmallCategory extends AbstractEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3439594262126373372L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "basic_data_small_category_sid", length = 36)
    @Getter
    @Setter
    private String sid;

    /**
     * 代碼 - 由系統管理員編碼
     */
    @Column(name = "small_category_id", unique = true, nullable = false, length = 8)
    @Getter
    @Setter
    private String id;

    /**
     * 小類名稱
     */
    @Column(name = "small_category_name", unique = true, nullable = false)
    @Getter
    @Setter
    private String name;

    /**
     * 對應的中類 [tr_basic_data_category].[BasicDataMiddleCategorySid]
     */
    @Column(name = "parent_middle_category", nullable = false)
    @Getter
    @Setter
    private String parentMiddleCategorySid;

    /**
     * 主管簽核設定
     */
    @Column(name = "tech_manager_sign", nullable = false)
    @Getter
    @Setter
    private Boolean techManagerSign = Boolean.FALSE;

    /**
     * 送測階段是否簽核
     */
    @Column(name = "test_sign", nullable = false)
    @Getter
    @Setter
    private Boolean testSign = Boolean.FALSE;

    /**
     * 
     */
    @Column(name = "pt_sign", nullable = false)
    @Getter
    @Setter
    private Boolean ptSign = Boolean.TRUE;

    /**
     * 需求單存檔是否需檢核附件
     */
    @Column(name = "check_attachment", nullable = false)
    @Getter
    @Setter
    private Boolean checkAttachment = Boolean.FALSE;

    /**
     * 是否自動結案
     */
    @Column(name = "auto_closed", nullable = false)
    @Getter
    @Setter
    private Boolean autoClosed = Boolean.FALSE;

    /** 是否可顯示於案件單轉需求單選項 */
    @Column(name = "show_info", nullable = false)
    @Getter
    @Setter
    private Boolean showItemInTc = Boolean.FALSE;

    /**
     * 
     */
    @Column(name = "note")
    @Getter
    @Setter
    private String note;

    /**
     * 
     */
    @Column(name = "seq")
    @Getter
    @Setter
    private Integer seq;

    /** 結案後是可執行on程式 */
    @Column(name = "a_c_onpg", nullable = false)
    @Getter
    @Setter
    private Boolean whenCloseExeOnpg = Boolean.FALSE;

    /** 是否自動產生ON程式頁籤資訊 */
    @Column(name = "aoto_onpg", nullable = false)
    @Getter
    @Setter
    private Boolean autoCreateOnpg = Boolean.FALSE;

    /** 權限對應 如果有多個角色字串以 '，' 分隔 */
    @Column(name = "permission_role")
    @Getter
    @Setter
    private String permissionRole;

    /** ON程式檢查完成可執行的角色 */
    @Column(name = "exec_onpg_ok")
    @Getter
    @Setter
    private String execOnpgOkRole;

    /** 需求完成後是否自動結案 */
    @Column(name = "check_finish_auto_closed", nullable = false)
    @Getter
    @Setter
    private Boolean checkFinishAutoClosed = Boolean.FALSE;

    /** 系統預設帶入期望完成日天數 */
    @Column(name = "default_due_day", nullable = false)
    @Getter
    @Setter
    private Integer defaultDueDays;
    
    /** 簽核層級 */
    @Convert(converter = SignLevelTypeConverter.class)
    @Column(name = "sign_level", nullable = false)
    @Getter
    @Setter
    private SignLevelType signLevel;
}
