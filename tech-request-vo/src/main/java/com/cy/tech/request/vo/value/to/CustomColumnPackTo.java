/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.value.to;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author kasim
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomColumnPackTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8582515077610682772L;

    /** 報表url */
    @JsonProperty(value = "url")
    private String url;

    /** 分頁筆數 */
    @JsonProperty(value = "pageCount")
    private String pageCount = "50";

    /** 欄位明細 */
    @JsonProperty(value = "detailMap")
    List<CustomColumnDetailTo> details = Lists.newArrayList();

}
