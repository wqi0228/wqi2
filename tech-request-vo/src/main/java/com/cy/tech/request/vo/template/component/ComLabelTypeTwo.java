/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.template.component;

import com.cy.tech.request.vo.annotation.TemplateComMapParserDefaultValueAnnotation;
import com.cy.tech.request.vo.enums.ComType;
import com.cy.tech.request.vo.value.to.TemplateDefaultValueTo;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import com.cy.work.common.utils.WkJsonUtils;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * input text
 *
 * @author shaun
 */
@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ComLabelTypeTwo implements ComBase, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -9055075602527191219L;

    /** 元件名稱陣列，於基礎欄位設定中顯示 */
    public List<String> displayComponents = Lists.newArrayList();

    /** 元件欄位名稱 */
    @JsonProperty("name")
    public String name;

    /** 元件Id */
    @JsonProperty("comId")
    public String comId;

    /** 元件type */
    @JsonProperty("type")
    public ComType type;

    /** 實作介面名稱 */
    @JsonProperty("interfaceName")
    public String interfaceName;

    /** 是否需要建立索引 */
    @JsonProperty("needCreateIndex")
    public Boolean needCreateIndex;

    /** 值 1 */
    @TemplateComMapParserDefaultValueAnnotation(key = ONE, valueName = "value01", valueClz = String.class, desc = "標籤顯示內容")
    @JsonProperty("value01")
    private String value01;

    @TemplateComMapParserDefaultValueAnnotation(key = TWO, valueName = "value02", valueClz = String.class, desc = "標籤文字粗體 \n"
            + "normal | bold | bolder | lighter | 100 | 200 | 300 | 400 | 500 | 600 | 700 | 800 | 900 | inherit")
    @JsonProperty("value02")
    private String value02;

    @TemplateComMapParserDefaultValueAnnotation(key = THREE, valueName = "value03", valueClz = String.class, desc = "標籤顏色 "
            + "red | rgb(255,0,0)")
    @JsonProperty("value03")
    private String value03;

    @TemplateComMapParserDefaultValueAnnotation(key = FOUR, valueName = "value04", valueClz = String.class, desc = "標籤文字大小 "
            + "16px | 16pt | 150%")
    @JsonProperty("value04")
    private String value04;

    @Override
    public void setDefValue(FieldKeyMapping template) {
        name = template.getFieldName();
        comId = template.getComId();
        type = template.getFieldComponentType();
        interfaceName = ComBase.class.getSimpleName();
        needCreateIndex = template.getRequiredIndex();

        TemplateDefaultValueTo dv = template.getDefaultValue();
        Map<Integer, Object> valueMap = dv.getValue();
        if (valueMap == null) {
            return;
        }
        if (valueMap.containsKey(ONE)) {
            value01 = (String) valueMap.get(ONE);
        }
        if (valueMap.containsKey(TWO)) {
            value02 = (String) valueMap.get(TWO);
        }
        if (valueMap.containsKey(THREE)) {
            value03 = (String) valueMap.get(THREE);
        }
        if (valueMap.containsKey(FOUR)) {
            value04 = (String) valueMap.get(FOUR);
        }
    }

    @Override
    public Boolean checkRequireValueIsEmpty() {
        //檢查使用者輸入欄位
        return Boolean.FALSE;
    }

    @Override
    public String getCompositionText(boolean showField, int maxFieldNameLen) {
        StringBuilder sb = new StringBuilder();
        sb.append(Strings.nullToEmpty(value01)).append(" \r\n");
        return sb.toString();
    }

    @Override
    public String getIndexContent() {
        try {
            return WkJsonUtils.getInstance().toJson(Lists.newArrayList(Strings.nullToEmpty(value01)));
        } catch (IOException ex) {
            log.error("parse value to json string fail...");
            log.error("fail value = " + value01, ex);
            return value01;
        }
    }

}
