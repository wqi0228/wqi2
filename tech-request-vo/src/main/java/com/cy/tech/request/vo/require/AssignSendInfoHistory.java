package com.cy.tech.request.vo.require;

import com.cy.work.common.vo.AbstractEntity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 分派單位歷史檔
 * @author allen1214_wu
 */
@NoArgsConstructor
@EqualsAndHashCode(of = { "sid" }, callSuper = false)
@Entity
@Table(name = "tr_assign_send_info_history")
public class AssignSendInfoHistory extends AbstractEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2937165663048809212L;

    /**
     * SID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "info_his_sid")
    @Getter
    @Setter
    private Long sid;

    /**
     * 需求單 SID
     */
    @Column(name = "require_sid", nullable = false, length = 36)
    @Getter
    @Setter
    private String requireSid;


    /**
     * 此次加派部門
     */
    @Column(name = "add_assign_dep")
    @Getter
    @Setter
    private String addAssignDeps;
    
    /**
     * 此次減派部門
     */
    @Column(name = "delete_assign_dep")
    @Getter
    @Setter
    private String deleteAssignDeps;
    
    /**
     * 此次刪除部門
     */
    @Column(name = "add_send_dep")
    @Getter
    @Setter
    private String addSendDeps;
    
    /**
     * 此次減派部門
     */
    @Column(name = "delete_send_dep")
    @Getter
    @Setter
    private String deleteSendDeps;
}
