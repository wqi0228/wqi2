/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import lombok.Getter;

/**
 * 其它設定資訊狀態
 *
 * @author shaun
 */
public enum OthSetStatus {
	/** 進行中 */
	PROCESSING("進行中", false, false),
	/** 完成 */
	FINISH("完成", true, false),
	/** 作廢 */
	INVALID("作廢", true, false),
	/**
	 * 因強制需求完成或強制結案而關閉
	 */
	FORCE_CLOSE("強制關閉", true, true),
	/** 單位於製作進度面板強制關閉 */
	UNIT_FORCE_CLOSE("立案單位強制關閉", true, true);

	@Getter
	private String val;
	/**
	 * 此階段為流程終點狀態
	 */
	@Getter
	private boolean inFinish;

	/**
	 * 強制結案的類型
	 */
	@Getter
	private boolean forceCompleteStatus;

	private OthSetStatus(String label, boolean inFinish, boolean forceCompleteStatus) {
		this.val = label;
		this.inFinish = inFinish;
		this.forceCompleteStatus = forceCompleteStatus;
	}
}
