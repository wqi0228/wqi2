/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.pt;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.vo.converter.SidOrgConverter;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.common.vo.converter.WorkSourceTypeConverter;
import com.cy.work.common.enums.WorkSourceType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 原型確認回覆的回覆
 *
 * @author shaun
 */
@Data
@NoArgsConstructor
@ToString(of = {"sid"})
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "work_pt_already_reply")
public class PtAlreadyReply implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4741389104966548246L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "already_reply_sid", length = 36)
    private String sid;

    /** 原型確認主檔 */
    @OneToOne
    @JoinColumn(name = "pt_check_sid", nullable = false)
    private PtCheck ptCheck;

    /** 原型確認單號 */
    @Column(name = "pt_check_no", nullable = false, length = 21, unique = true)
    private String ptNo;

    /** 原型確認來源 */
    @Convert(converter = WorkSourceTypeConverter.class)
    @Column(name = "pt_check_source_type", nullable = false)
    private WorkSourceType sourceType;

    /** 原型確認來源 sid */
    @Column(name = "pt_check_source_sid", nullable = false, length = 36)
    private String sourceSid;

    /** 原型確認來源 單號 */
    @Column(name = "pt_check_source_no", nullable = false, length = 21)
    private String sourceNo;

    /** 原型確認回覆主檔 */
    @ManyToOne
    @JoinColumn(name = "reply_sid", nullable = false)
    private PtReply reply;

    /** 回覆人員的部門 */
    @Convert(converter = SidOrgConverter.class)
    @Column(name = "reply_person_dep", nullable = false)
    private Org dep;

    /** 回覆人員 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "reply_person_sid", nullable = false)
    private User person;

    /** 回覆日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "reply_dt", nullable = false)
    private Date date;

    /** 異動回覆資訊的日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "reply_udt", nullable = false)
    private Date updateDate;

    /** 回覆內容 原文 */
    @Column(name = "reply_content", nullable = false)
    private String content;

    /** 回覆內容 含css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "reply_content_css")
    private String contentCss;

    /** 已回覆歷程 */
    @OneToOne(mappedBy = "alreadyReply", cascade = CascadeType.PERSIST)
    private PtHistory history;

}
