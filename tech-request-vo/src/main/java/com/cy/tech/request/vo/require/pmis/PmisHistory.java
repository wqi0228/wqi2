package com.cy.tech.request.vo.require.pmis;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.cy.commons.vo.User;
import com.cy.work.common.vo.converter.SidUserConverter;

import lombok.Data;

@Data
@Entity
@Table(name = "tr_to_pmis_history")
public class PmisHistory {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "history_sid", length = 36)
    private String sid;

    @Column(name = "d_item", nullable = false)
    private String dItem;

    @Column(name = "d_mid_item")
    private String dMidItem;

    @Column(name = "d_theme", nullable = false)
    private String dTheme;

    @Column(name = "d_reason")
    private String dReason;

    @Column(name = "d_desc")
    private String dDesc;

    @Column(name = "d_dept", nullable = false)
    private Integer dDept;

    @Column(name = "d_date", nullable = false)
    private Date dDate;

    @Column(name = "d_co_dept", nullable = false)
    private String dCoDept;

    @Column(name = "note_werp_id", nullable = false)
    private String noteWerpId;

    @Column(name = "note_werp_link", nullable = false)
    private String noteWerpLink;

    @Column(name = "case_number")
    private Integer fbCaseNo;

    /** 建立者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "create_usr", nullable = false)
    private User createdUser;

    /** 建立日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    public PmisHistory(){
        
    }
}
