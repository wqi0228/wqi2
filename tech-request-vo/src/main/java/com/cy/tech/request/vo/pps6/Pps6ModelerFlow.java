/**
 * 
 */
package com.cy.tech.request.vo.pps6;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.cy.work.common.vo.AbstractEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * PPS6 Modeler 流程定義資料檔
 * 
 * @author allen1214_wu
 */
@EqualsAndHashCode(callSuper = false, of = "sid")
@Table(name = "pps6_modeler_flow")
@Entity
@Getter
@Setter
public class Pps6ModelerFlow extends AbstractEntity {

	/**
     * 
     */
    private static final long serialVersionUID = 2075055740087832628L;

    /**
	 * SID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "flow_sid")
	private Integer sid;

	/**
	 * 流程代號
	 */
	@Column(name = "flow_id")
	private String flowId;

	/**
	 * 流程名稱
	 */
	@Column(name = "flow_name")
	private String flowName;

	/**
	 * 單據分類 pps6_modeler_form.form_sid
	 */
	@Column(name = "form_sid")
	private Integer formSid;
}
