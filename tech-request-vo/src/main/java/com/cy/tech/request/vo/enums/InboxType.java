/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import lombok.Getter;

/**
 * 收件夾類型
 *
 * @author shaun
 */
public enum InboxType {

    /** 收呈報 下屬寄給上層主管 */
    INCOME_REPORT(-2, 0, true),
    /** 收指示 上層主管寄給下屬 */
    INCOME_INSTRUCTION(-2, 0, true),
    /** 收個人 不同組織互寄 */
    INCOME_MEMBER(-2, 0, true),
    /** 收部門轉發 部門互寄 */
    INCOME_DEPT(-2, 0, false),;

    @Getter
    private final int startDateRange;
    @Getter
    private final int endDateRange;
    @Getter
    private final boolean isForwardMember;

    private InboxType(int startDateRange, int endDateRange, boolean isForwardMember) {
        this.startDateRange = startDateRange;
        this.endDateRange = endDateRange;
        this.isForwardMember = isForwardMember;
    }

}
