/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import com.cy.tech.request.vo.enums.SubNoticeType;
import javax.persistence.AttributeConverter;

/**
 * 子程序異動記錄 通知類型 Converter
 *
 * @author kasim
 */
public class SubNoticeTypeConverter implements AttributeConverter<SubNoticeType, String> {

    @Override
    public SubNoticeType convertToEntityAttribute(String name) {
        return (name == null) ? null : SubNoticeType.valueOf(name);
    }

    @Override
    public String convertToDatabaseColumn(SubNoticeType status) {
        return (status == null) ? null : status.name();
    }

}
