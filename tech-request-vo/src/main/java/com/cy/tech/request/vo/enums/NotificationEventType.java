package com.cy.tech.request.vo.enums;

import lombok.Getter;

/**
 * 
 * @author aken_kao
 *
 */
public enum NotificationEventType {

    UPDATE_ONLINE_DATE("修改預計上線日", "修改日期為：%s"),
    BATCH_UP_OL_DATE("批次修改上線日", "批次修改日期為：%s"),
    UPDATE_ESTABLISH_DATE("修改預計完成日", "修改日期為：%s"),
    BATCH_UP_ES_DATE("批次修改預計完成日", "批次修改日期為：%s"),
    UPDATE_THTEM("修改主題", "修改主題"),
    UPDATE_CONTENT("修改其他項目", "修改內容"),
    UPDATE_NOTE("修改其他項目", "修改備註"),
    UPDATE_FB_NO("修改連結", "修改FB NO為：%s");
    
    @Getter
    private String label;
    @Getter
    private String format;
    
    private NotificationEventType (String label, String format) {
        this.label = label;
        this.format = format;
    }
}
