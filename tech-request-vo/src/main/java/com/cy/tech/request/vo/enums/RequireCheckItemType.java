package com.cy.tech.request.vo.enums;

import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import lombok.Getter;

/**
 * 需求單-檢查項目 (對應系統別欄位)
 * 
 * @author allen1214_wu
 */
public enum RequireCheckItemType {

    /**
     * 無
     */
    NONE("無", true),

    /**
     * BBIN
     */
    BBIN("BBIN平台", false),

    /**
     * XBB
     */
    XBB("XBB平台", false),
    
    /**
     * XBB
     */
    BBGP("BBGP平台", false),
    
    ;

    /**
     * 顯示/說明
     */
    @Getter
    private final String descr;

    /**
     * 
     */
    @Getter
    private final boolean noneItem;

    private RequireCheckItemType(String descr, boolean noneItem) {
        this.descr = descr;
        this.noneItem = noneItem;
    }

    /**
     * @param test
     * @return isContains
     */
    public static boolean isContains(String test) {
        for (RequireCheckItemType c : RequireCheckItemType.values()) {
            if (c.name().equals(test)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param str
     * @return RequireCheckItemType
     */
    public static RequireCheckItemType safeValueOf(String str) {
        if (WkStringUtils.notEmpty(str)) {
            for (RequireCheckItemType item : RequireCheckItemType.values()) {
                if (item.name().equals(str)) {
                    return item;
                }
            }
        }
        return null;
    }

    /**
     * 取得項目說明 (避免傳入字串對應不到項目)
     * @param codeName type name
     * @return 項目說明
     */
    public static String safeGetDescr(String codeName) {
        RequireCheckItemType item = safeValueOf(codeName);
        if (item != null) {
            return item.getDescr();
        }
        return "";
    }
    
    /**
     * 取回真正的系統別個數 (排除 none)
     * @return 所有項目個數
     */
    public static long getItemTypeCount() {
        return Lists.newArrayList(RequireCheckItemType.values()).stream()
                .filter(item->!item.noneItem)
                .count();
    }
}
