package com.cy.tech.request.vo.setting.sysnotify.to;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.cy.work.notify.vo.enums.NotifyFilterType;
import com.cy.work.notify.vo.enums.NotifyMode;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;

/**
 * 設定：系統通知『類別』設定資料
 * 
 * @author allen1214_wu
 */

public class SettingSysNotifyTypeInfo implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -6152528640170055987L;

    /**
	 * 
	 */
	public SettingSysNotifyTypeInfo() {
		// 初始化
		for (NotifyMode notifyMode : NotifyMode.values()) {
			this.openSwitch.put(notifyMode, false);

		}
		for (NotifyFilterType notifyFilterType : NotifyFilterType.values()) {
			this.blackList.put(notifyFilterType, Lists.newArrayList());
		}
	}

	/**
	 * 通知方式開關 by 通知方式
	 */
	@Getter
	@Setter
	private Map<NotifyMode, Boolean> openSwitch = Maps.newHashMap();


	/**
	 * 黑名單(不發送通知的項目) by 過濾方式 (需求小類或通知)
	 */
	@Getter
	@Setter
	private Map<NotifyFilterType, List<String>> blackList = Maps.newHashMap();

	// ========================================================================
	// 以下欄位 view 使用, 不轉換為 json 資料
	// ========================================================================
	/**
	 * 畫面顯示 (僅作為傳遞資料欄位-不儲存)
	 */
	@Getter
	@JsonIgnore
	private Map<NotifyFilterType, String> showInfoForFilterDescr = Maps.newHashMap();
	
	/**
	 * 畫面顯示 (僅作為傳遞資料欄位-不儲存)
	 */
	@Getter
	@JsonIgnore
	private Map<NotifyFilterType, String> showInfoForFilterTooltip = Maps.newHashMap();
}
