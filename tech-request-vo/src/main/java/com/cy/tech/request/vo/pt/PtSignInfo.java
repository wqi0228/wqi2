/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.pt;

import com.cy.work.common.enums.InstanceStatus;
import com.cy.work.common.vo.value.to.BpmTaskTo;
import com.cy.work.common.vo.converter.BpmTaskToConverter;
import com.cy.work.common.vo.converter.JsonStringListToConverter;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.vo.converter.InstanceStatusConverter;
import com.cy.work.common.vo.converter.WorkSourceTypeConverter;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 原型確認審核資訊
 *
 * @author shaun
 */
@Data
@ToString(of = {"sid", "ptNo", "sourceNo", "bpmInstanceId"})
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "work_pt_sign_info")
public class PtSignInfo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -865784533677274592L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "pt_check_sign_sid", length = 36)
    private String sid;

    /** 原型確認主檔 */
    @OneToOne
    @JoinColumn(name = "pt_check_sid", nullable = false)
    private PtCheck ptCheck;

    /** 原型確認單號 */
    @Column(name = "pt_check_no", nullable = false, length = 21, unique = true)
    private String ptNo;

    /** 原型確認來源 */
    @Convert(converter = WorkSourceTypeConverter.class)
    @Column(name = "pt_check_source_type", nullable = false)
    private WorkSourceType sourceType;

    /** 原型確認來源 sid */
    @Column(name = "pt_check_source_sid", nullable = false, length = 36)
    private String sourceSid;

    /** 原型確認來源 單號 */
    @Column(name = "pt_check_source_no", nullable = false, length = 21)
    private String sourceNo;

    /** 核准日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "approval_dt", nullable = true)
    private Date approvalDate;

    /** 原型確認審核狀態 */
    @Convert(converter = InstanceStatusConverter.class)
    @Column(name = "paper_code", nullable = false)
    private InstanceStatus instanceStatus;

    /** 流程碼 */
    @Column(name = "bpm_instance_id", nullable = false)
    private String bpmInstanceId;

    /** 預設簽核人員暱稱 */
    @Column(name = "bpm_default_signed_name", nullable = true)
    private String defaultSignedName;

    /** 可簽核人員id列表 */
    @Convert(converter = JsonStringListToConverter.class)
    @Column(name = "bpm_can_signed_id_list", nullable = true)
    private JsonStringListTo canSignedIdsTo = new JsonStringListTo();

    /** 流程水管圖 */
    @Convert(converter = BpmTaskToConverter.class)
    @Column(name = "bpm_sim_info", nullable = true)
    private BpmTaskTo taskTo = new BpmTaskTo();

}
