/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.constants;

/**
 * 快取常數
 *
 * @author shaun
 */
public class CacheConstants {

    // 注意！ 和 REST 有共用方法時， 更新時需呼叫 WkLogicUtil.clearRequireRestEhCache();

    /** 需求單模版快取 - List型態需開啟 setCopyOnRead & setCopyOnWrite */
    public final static String CACHE_TEMP_FIELD = "req_temp_field_cache";
    /** 需求單入口菜單 */
    public final static String CACHE_PORTAL_MENU = "req_portal_menu_cache";
    /** 需求單類別模版Key快取 */
    public final static String CACHE_CATE_KEY_MAPPING = "req_cate_key_mapping";

    /**
     * 需求單-查詢所有大類
     */
    public final static String CACHE_findActiveBigCategory = "req_findActiveBigCategory";

    /**
     * 所有類別組合
     */
    public final static String CACHE_ALL_CATEGORY_PICKER = "CACHE_ALL_CATEGORY_PICKER";

    /**
     * 需求確認單位計算結果
     */
    public final static String CACHE_prepareRequireConfirmDep = "CACHE_prepareRequireConfirmDep";

    /**
     * 可檢查確認權限
     */
    public final static String CACHE_SETTING_CHECK_CONFIRM_RIGHT = "CACHE_SETTING_CHECK_CONFIRM_RIGHT";

    /**
     * 是否為可檢查確認人員
     */
    public final static String CACHE_IS_CANCHECK_USER = "CACHE_IS_CANCHECK_USER";
}
