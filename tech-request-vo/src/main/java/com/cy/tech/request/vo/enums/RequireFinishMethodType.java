/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import lombok.Getter;

/**
 * 需求完成方式
 *
 * @author shaun
 */
public enum RequireFinishMethodType {

    /** 0 手動 */
    MANUAL(0),
    /** 1 自動 */
    AUTO(1);

    @Getter
    private final int code;

    RequireFinishMethodType(int code) {
        this.code = code;
    }

}
