/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import lombok.Getter;

/**
 * 轉單程式 - 程序挑選類型列舉
 */
public enum RequireTransProgramType {
    /** 需求單 */
    REQUIRE("需求單",
            "tr_require", "require_sid", "dep_sid",
            "tid.field_content", "require_status", "require_no", "",
            "", "",
            TrnsType.ORG_TRNS_CREATE_DEP_REQUIRE,
            null),

    /** 原型確認 */
    PTCHECK("原型確認",
            "work_pt_check", "pt_check_sid", "dep_sid",
            "pt_check_theme", "pt_check_status", "pt_check_no", "pt_check_source_no",
            "pt_check_source_sid", "pt_notice_deps",
            TrnsType.ORG_TRNS_CREATE_DEP_PTCHECK,
            SubNoticeType.PT_NOTICE_DEP),

    /** 送測 */
    WORKTESTSIGNINFO("送測",
            "work_test_info", "testinfo_sid", "dep_sid",
            "testinfo_theme", "testinfo_status", "testinfo_no", "testinfo_source_no",
            "testinfo_source_sid", "testinfo_deps",
            TrnsType.ORG_TRNS_CREATE_DEP_WORKTESTSIGNINFO,
            SubNoticeType.TEST_INFO_DEP),

    /** 其他資料設定 */
    OTHSET("其他資料設定",
            "tr_os", "os_sid", "dep_sid",
            "os_theme", "os_status", "os_no", "require_no",
            "require_sid", "os_notice_deps",
            TrnsType.ORG_TRNS_CREATE_DEP_OTHSET,
            SubNoticeType.OS_INFO_DEP),

    /** ON程式 */
    WORKONPG("ON程式",
            "work_onpg", "onpg_sid", "dep_sid",
            "onpg_theme", "onpg_status", "onpg_no", "onpg_source_no",
            "onpg_source_sid", "onpg_deps",
            TrnsType.ORG_TRNS_CREATE_DEP_WORKONPG,
            SubNoticeType.ONPG_INFO_DEP);

    @Getter
    private String descr;

    @Getter
    private String tableName;
    @Getter
    private String sidColName;
    @Getter
    private String createDepColName;
    @Getter
    private String themeColName;
    @Getter
    private String statusColName;
    @Getter
    private String caseNoColName;
    @Getter
    private String masterCaseNoColName;
    @Getter
    private String masterCaseSidColName;
    @Getter
    private String noticeDepsColName;

    @Getter
    private TrnsType trnsType;
    @Getter
    private SubNoticeType subNoticeType;

    private RequireTransProgramType(
            String descr,
            String tableName,
            String sidColumnName,
            String createDepColumnName,
            String themeColName,
            String statusColName,
            String caseNoColName,
            String masterCaseNoColName,
            String masterCaseSidColName,
            String noticeDepsColName,
            TrnsType trnsType,
            SubNoticeType subNoticeType
            ) {
        
        this.descr = descr;
        this.tableName = tableName;
        this.sidColName = sidColumnName;
        this.createDepColName = createDepColumnName;
        this.themeColName = themeColName;
        this.statusColName = statusColName;
        this.caseNoColName = caseNoColName;
        this.masterCaseNoColName = masterCaseNoColName;
        this.masterCaseSidColName = masterCaseSidColName;
        this.noticeDepsColName = noticeDepsColName;

        this.trnsType = trnsType;
        this.subNoticeType = subNoticeType;

    }
}
