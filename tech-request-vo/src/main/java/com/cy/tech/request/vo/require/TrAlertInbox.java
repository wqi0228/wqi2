/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require;

import com.cy.commons.enums.Activation;
import com.cy.commons.interfaces.BaseEntity;
import com.cy.tech.request.vo.converter.ForwardTypeConverter;
import com.cy.tech.request.vo.converter.InboxTypeConverter;
import com.cy.tech.request.vo.converter.ReadRecordStatusConverter;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.tech.request.vo.enums.ForwardType;
import com.cy.tech.request.vo.enums.InboxType;
import com.cy.tech.request.vo.enums.ReadRecordStatus;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 收件夾
 *
 * @author shaun
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_alert_inbox")
public class TrAlertInbox implements Serializable, BaseEntity<String> {

    /**
     * 
     */
    private static final long serialVersionUID = -5614884464768515498L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "alert_sid", length = 36)
    private String sid;

    /** 回覆來源訊息 */
    @Column(name = "reply_alert_sid", nullable = true, length = 36)
    private String reply_alert_sid;

    /** 需求單主檔 */
    @Column(name = "require_sid", nullable = false, length = 36)
    private String requireSid;

    /** 需求單單號 */
    @Column(name = "require_no", nullable = false, length = 21)
    private String requireNo;

    /** 轉寄類型 */
    @Convert(converter = ForwardTypeConverter.class)
    @Column(name = "forward_type", nullable = false)
    private ForwardType forwardType;

    /** 寄件人 */
    @Column(name = "sender", nullable = false)
    private Integer sender;

    /** 寄件部門 */
    @Column(name = "sender_dep", nullable = false)
    private Integer senderDep;

    /** 收件人 */
    @Column(name = "receive")
    private Integer receive;

    /** 收件部門 */
    @Column(name = "receive_dep")
    private Integer receiveDep;

    /** 留言訊息 */
    @Column(name = "message")
    private String message;

    /** 留言訊息 - 含css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "message_css")
    private String messageCss;

    /** 收件夾類型 */
    @Convert(converter = InboxTypeConverter.class)
    @Column(name = "inbox_type", nullable = false)
    private InboxType inboxType;

    /** 寄發群組sid */
    @Column(name = "alert_group_sid", nullable = false, length = 36)
    private String alert_group_sid;

    /** 狀態 */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    /** 建立者 */
    @Column(name = "create_usr", nullable = false)
    private Integer createdUser;

    /** 建立日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    /** 異動者 */
    @Column(name = "update_usr")
    private Integer updatedUser;

    /** 異動日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

    @Convert(converter = ReadRecordStatusConverter.class)
    @Column(name = "read_record", nullable = true)
    private ReadRecordStatus read_record;

    @Column(name = "read_dt", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date read_dt;

}
