/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import com.cy.tech.request.vo.enums.RequireStatusType;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 需求製作進度型態轉換器
 *
 * @author shaun
 */
@Slf4j
@Converter
public class RequireStatusTypeConverter implements AttributeConverter<RequireStatusType, String> {

    @Override
    public String convertToDatabaseColumn(RequireStatusType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public RequireStatusType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return RequireStatusType.valueOf(dbData);
        } catch (Exception e) {
            log.error("RequireStatusType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }

}
