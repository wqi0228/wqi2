/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 解析元件物件時，依照標註得到元件預設值列表
 *
 * @author shaun
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface TemplateComMapParserInteractValueAnnotation {

    //Map 中的 key值 
    int key();

    //儲存型態
    @SuppressWarnings("rawtypes")
    Class valueClz();

    //Com元件對應的互動值名
    String valueName();
    
    //說明
    String desc();
}
