/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.onpg.converter;

import com.cy.tech.request.vo.onpg.enums.WorkOnpgHistoryBehavior;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author shaun
 */
@Slf4j
@Converter
public class WorkOnpgHistoryBehaviorConverter implements AttributeConverter<WorkOnpgHistoryBehavior, String> {

    @Override
    public String convertToDatabaseColumn(WorkOnpgHistoryBehavior attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public WorkOnpgHistoryBehavior convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return WorkOnpgHistoryBehavior.valueOf(dbData);
        } catch (Exception e) {
            log.error("WorkOnpgHistoryBehavior Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
