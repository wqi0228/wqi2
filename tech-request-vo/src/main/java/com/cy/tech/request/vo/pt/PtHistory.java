/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.pt;

import com.cy.commons.enums.Activation;
import com.cy.commons.interfaces.BaseEntity;
import com.cy.commons.vo.User;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.common.vo.converter.WorkSourceTypeConverter;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.tech.request.vo.pt.converter.PtHistoryBehaviorConverter;
import com.cy.tech.request.vo.pt.converter.PtStatusConverter;
import com.cy.tech.request.vo.pt.enums.PtHistoryBehavior;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

/**
 * 原型確認歷程
 *
 * @author shaun
 */
@Data
@NoArgsConstructor
@ToString(exclude = {"attachments"})
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "work_pt_check_history")
public class PtHistory implements Serializable, BaseEntity<String> {

    /**
     * 
     */
    private static final long serialVersionUID = 5456441561058380393L;

    /** 原型確認sid */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "pt_check_history_sid", length = 36)
    private String sid;

    /** 原型確認主檔 */
    @OneToOne
    @JoinColumn(name = "pt_check_sid", nullable = false)
    private PtCheck ptCheck;

    /** 原型確認單號 */
    @Column(name = "pt_check_no", nullable = false, length = 21, unique = true)
    private String ptNo;

    /** 原型確認來源 */
    @Convert(converter = WorkSourceTypeConverter.class)
    @Column(name = "pt_check_source_type", nullable = false)
    private WorkSourceType sourceType;

    /** 原型確認來源 sid */
    @Column(name = "pt_check_source_sid", nullable = false, length = 36)
    private String sourceSid;

    /** 原型確認來源 單號 */
    @Column(name = "pt_check_source_no", nullable = false, length = 21)
    private String sourceNo;

    /** 歷史資訊行為狀態 */
    @Convert(converter = PtHistoryBehaviorConverter.class)
    @Column(name = "behavior", nullable = false)
    private PtHistoryBehavior behavior;

    /** 此時的原型確認單狀態 */
    @Convert(converter = PtStatusConverter.class)
    @Column(name = "behavior_status", nullable = false)
    private PtStatus behaviorStatus;

    /** 回覆資訊sid */
    @OneToOne
    @JoinColumn(name = "reply_sid")
    private PtReply reply;

    /** 回覆的回覆資訊sid */
    @OneToOne
    @JoinColumn(name = "already_reply_sid")
    private PtAlreadyReply alreadyReply;

    /** 原因 */
    @Column(name = "reason")
    private String reason;

    /** 原因 含css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "reason_css")
    private String reasonCss;

    /** 功能符合需求的回覆資訊 */
    @Column(name = "fun_conform_reply")
    private String funConformReply;

    /** 功能符合需求的回覆資訊_css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "fun_conform_reply_css")
    private String funConformReplyCss;

    /** 顯示於異動明細表上 */
    @Column(name = "visiable", nullable = false)
    private Boolean visiable = Boolean.FALSE;

    /** 狀態 */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    /** 建立者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "create_usr", nullable = false)
    private User createdUser;

    /** 建立日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    /** 異動者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "update_usr")
    private User updatedUser;

    /** 異動日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

    /** 附加檔案 */
    @OneToMany(mappedBy = "history", cascade = CascadeType.PERSIST)
    @Where(clause = "status = 0")
    @OrderBy(value = "createdDate DESC")
    private List<PtAttachment> attachments = Lists.newArrayList();

}
