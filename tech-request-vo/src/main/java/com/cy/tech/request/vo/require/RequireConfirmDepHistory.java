package com.cy.tech.request.vo.require;

import com.cy.tech.request.vo.converter.ReqConfirmDepProcTypeConverter;
import com.cy.tech.request.vo.enums.ReqConfirmDepProcType;
import com.cy.work.common.vo.AbstractEntity;
import com.cy.work.common.vo.listener.RemoveContorlCharListener;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 需求完成確認檔
 * 
 * @author allen1214_wu
 */
@NoArgsConstructor
@EqualsAndHashCode(of = { "sid" }, callSuper = false)
@Entity
@Table(name = "tr_require_confirm_dep_history")
// 移除 vo 中字串型態欄位值中的控制字元
@EntityListeners(RemoveContorlCharListener.class)
public class RequireConfirmDepHistory extends AbstractEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1121705158169847573L;

    /**
     * SID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "history_sid")
    @Getter
    @Setter
    private Long sid;

    /**
     * 歷程歸屬部門
     */
    @Column(name = "create_dep", nullable = false)
    @Getter
    @Setter
    private Integer createDep;

    /**
     * 需求完成確認單位檔 SID
     */
    @Column(name = "require_confirm_sid", nullable = false)
    @Getter
    @Setter
    private Long requireConfirmSid;

    /**
     * 需求單 SID
     */
    @Column(name = "require_sid", nullable = false, length = 36)
    @Getter
    @Setter
    private String requireSid;

    /**
     * 需求完成確認單位 SID
     */
    @Column(name = "dep_sid", nullable = false)
    @Getter
    @Setter
    private Integer depSid;

    /**
     * 需求完成進度
     */
    @Convert(converter = ReqConfirmDepProcTypeConverter.class)
    @Column(name = "proc_type", nullable = false)
    @Getter
    @Setter
    private ReqConfirmDepProcType progType;

    /**
     * 需求完成說明
     */
    @Column(name = "memo")
    @Getter
    @Setter
    private String memo;
}
