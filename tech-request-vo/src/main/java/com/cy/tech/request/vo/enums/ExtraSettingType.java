package com.cy.tech.request.vo.enums;

import lombok.Getter;

public enum ExtraSettingType {

    SABA_ENABLED("SABA_ENABLED", "是否開放沙巴"),
    NCS_CONFIRM("NCS_CONFIRM", "NCS是否確認");

    @Getter
    private final String code;

    @Getter
    private final String name;

    private ExtraSettingType(String code, String name) {
        this.code = code;
        this.name = name;
    }

}
