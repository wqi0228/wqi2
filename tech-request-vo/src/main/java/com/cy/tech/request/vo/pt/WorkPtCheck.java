/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.pt;

import com.cy.commons.enums.Activation;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.vo.converter.JsonStringListToConverter;
import com.cy.work.common.vo.converter.ReadRecordGroupToConverter;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.common.vo.converter.WorkSourceTypeConverter;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.cy.work.common.vo.value.to.ReadRecordGroupTo;
import com.cy.tech.request.vo.enums.WaitReadReasonConverter;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.pt.converter.PtStatusConverter;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author brain0925_liao
 */
@Data
@ToString(of = {"sid"})
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "work_pt_check")
public class WorkPtCheck implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5217916561207702407L;

    /** 原型確認sid */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "pt_check_sid", length = 36)
    private String sid;

    /** 原型確認單號 */
    @Column(name = "pt_check_no", nullable = false, length = 21, unique = true)
    private String ptNo;

    /** 原型確認來源 */
    @Convert(converter = WorkSourceTypeConverter.class)
    @Column(name = "pt_check_source_type", nullable = false)
    private WorkSourceType sourceType;

    /** 原型確認來源 sid */
    @Column(name = "pt_check_source_sid", nullable = false, length = 36)
    private String sourceSid;

    /** 原型確認來源 單號 */
    @Column(name = "pt_check_source_no", nullable = false, length = 21)
    private String sourceNo;

    /** 原型確認通知對象 */
    @Convert(converter = JsonStringListToConverter.class)
    @Column(name = "pt_notice_member", nullable = false)
    private JsonStringListTo noticeMember = new JsonStringListTo();

    /** 原型確認通知部門 */
    @Convert(converter = JsonStringListToConverter.class)
    @Column(name = "pt_notice_deps", nullable = false)
    private JsonStringListTo noticeDeps = new JsonStringListTo();

    /** 是否有流程 */
    @Column(name = "has_sign", nullable = false)
    private Boolean hasSign = Boolean.FALSE;

    /** 原型確認主題 */
    @Column(name = "pt_check_theme", nullable = false, length = 255)
    private String theme;

    /** 原型確認內容 */
    @Column(name = "pt_check_content", nullable = false)
    private String content;

    /** 原型確認內容 含css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "pt_check_content_css", nullable = false)
    private String contentCss;

    /** 備註說明 */
    @Column(name = "pt_check_note")
    private String note;

    /** 備註說明 含css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "pt_check_note_css")
    private String noteCss;

    /** 預計完成日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "pt_check_estimate_dt", nullable = false)
    private Date establishDate;

    /** 實際完成日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "pt_check_finish_dt")
    private Date finishDate;

    /** 原型確認重做日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "pt_redo_dt", nullable = true)
    private Date redoDate;

    /** 此次原型確認版本 */
    @Column(name = "currently_ver", nullable = false)
    private Integer version;

    /** 待閱異動日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "read_update_dt")
    private Date readUpdateDate;

    /** 待閱原因 */
    @Convert(converter = WaitReadReasonConverter.class)
    @Column(name = "read_reason")
    private WaitReadReasonType readReason;

    /** 待閱人員清單 - 閱讀紀錄資訊 */
    @Convert(converter = ReadRecordGroupToConverter.class)
    @Column(name = "read_record")
    private ReadRecordGroupTo readRecord = new ReadRecordGroupTo();

    /** 原型確認狀態 */
    @Convert(converter = PtStatusConverter.class)
    @Column(name = "pt_check_status", nullable = false)
    private PtStatus ptStatus;

    @Column(name = "comp_sid", nullable = false)
    private Integer comp_sid;

    @Column(name = "dep_sid", nullable = false)
    private Integer dep_sid;
    /** 狀態 */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;
   /** 建立者Sid */
    @Column(name = "create_usr", nullable = false)
    private Integer create_usr;
    /** 建立日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date create_dt;
    /** 更新者 */
    @Column(name = "update_usr", nullable = true)
    private Integer update_usr;
    /** 更新時間 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;
}
