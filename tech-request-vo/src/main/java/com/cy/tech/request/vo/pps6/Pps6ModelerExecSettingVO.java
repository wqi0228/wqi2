/**
 * 
 */
package com.cy.tech.request.vo.pps6;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Id;

import org.springframework.beans.BeanUtils;

import com.cy.tech.request.vo.pps6.enums.Pps6ModelerExecType;
import com.cy.work.common.vo.AbstractEntityVO;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * PPS6 Modeler 執行設定資料檔 VO
 * 
 * @author allen1214_wu
 */
@EqualsAndHashCode(callSuper = false, of = "sid")
@NoArgsConstructor
@Getter
@Setter
public class Pps6ModelerExecSettingVO extends AbstractEntityVO implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 4075535614187308822L;

    /**
	 * 建構子
	 * @param pps6ModelerExecSetting Pps6ModelerExecSetting
	 */
	public Pps6ModelerExecSettingVO(Pps6ModelerExecSetting pps6ModelerExecSetting) {
		// ====================================
		// copy Properties
		// ====================================
		BeanUtils.copyProperties(pps6ModelerExecSetting, this);
	}

	/**
	 * SID
	 */
	@Id
	private String sid;

	/**
	 * 流程資料 sid
	 */
	private Integer flowSid;

	/**
	 * 角色、帳號或變數ID
	 */
	private String execId;

	/**
	 * 是否為變數
	 */
	private boolean variable;

	/**
	 * 執行類型
	 */
	private Pps6ModelerExecType execType;

	/**
	 * @return ExecTypeName
	 */
	public String getExecTypeName() {
		if (execType != null) {
			return execType.getDescr();
		}
		return "";
	}

	/**
	 * 名稱
	 */
	private String name;

	/**
	 * 自定義說明
	 */
	private String userDescr;

	/**
	 * 使用節點名稱
	 */
	private String modelerNodeName;

	/**
	 * 流程 VO
	 */
	private Pps6ModelerFlowVO flowVO;

	/**
	 * WERP 對應使用者資訊 (顯示用)
	 */
	private String werpMappingInfo;

	/**
	 * WERP 對應使用者 ID
	 */
	private List<String> werpMappingUserIDs;
}
