/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.value.to;

import com.cy.tech.request.vo.enums.ComType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * 單據內自動生成頁面的模版資料<BR/>
 * 使用此物件建立MAP然後讓模版取資料<P/>
 *
 * 頁面條件MBean<P/>
 * 1.要有 Map<String, ComBase> comValueMap;<BR/>
 * <BR/>
 * 存檔<P/>
 * 1.將Map資料轉型為 List<DocComTemplateValue> <BR/>
 * 2.將Map儲存的對應物件Json化成字串<BR/>
 * 3.最後Json化整個List<DocComTemplateValue> 物件 <BR/>
 * 4.存入資料庫<BR/>
 * 5.依需求看是否建立索引<BR/>
 *
 * <BR/>
 * 載入<P/>
 * 1. 取得單據儲存 Json欄位 <BR/>
 * 2. Json -> List<DocComTemplateValue><BR/>
 * 3. foreach List<DocComTemplateValue><BR/>
 * 4. 建立資料源 Map <BR/>
 * &nbsp; K = comId , V = comValue<BR/>
 *
 *
 * @author shaun
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DocComTemplateTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6446984386182714087L;

    /** 元件Id */
    @JsonProperty("comId")
    private String comId;

    /** 元件型態 */
    @JsonProperty("type")
    private ComType type;

    /** 每個元件的 */
    @JsonProperty("comValue")
    private String comValue;

}
