package com.cy.tech.request.vo.category;

import com.cy.tech.request.vo.anew.enums.SignLevelType;
import com.cy.work.common.utils.WkEntityUtils;
import com.cy.work.common.vo.AbstractEntity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 小類
 *
 * @author shaun
 */
@NoArgsConstructor
@EqualsAndHashCode(of = { "sid" }, callSuper = false)
@Entity
public class SimpleSmallCategoryVO extends AbstractEntity implements Serializable {

    
    /**
     * 
     */
    private static final long serialVersionUID = -4135389456376838420L;

    /**
     * @param simpleSmallCategory
     */
    public SimpleSmallCategoryVO(SimpleSmallCategory simpleSmallCategory) {
        if (simpleSmallCategory == null) {
            return;
        }
        WkEntityUtils.getInstance().copyProperties(simpleSmallCategory, this);
    }
    
    @Id
    @Getter
    @Setter
    private String sid;

    /**
     * 代碼 - 由系統管理員編碼
     */
    @Getter
    @Setter
    private String id;

    /**
     * 小類名稱
     */
    @Getter
    @Setter
    private String name;

    /**
     * 對應的中類 [tr_basic_data_category].[BasicDataMiddleCategorySid]
     */
    @Getter
    @Setter
    private String parentMiddleCategorySid;

    /**
     * 主管簽核設定
     */
    @Getter
    @Setter
    private Boolean techManagerSign = Boolean.FALSE;

    /**
     * 送測階段是否簽核
     */
    @Getter
    @Setter
    private Boolean testSign = Boolean.FALSE;

    /**
     * 
     */
    @Getter
    @Setter
    private Boolean ptSign = Boolean.TRUE;


    /**
     * 需求單存檔是否需檢核附件
     */
    @Getter
    @Setter
    private Boolean checkAttachment = Boolean.FALSE;

    /**
     * 是否自動結案
     */
    @Getter
    @Setter
    private Boolean autoClosed = Boolean.FALSE;

    /** 是否可顯示於案件單轉需求單選項 */
    @Getter
    @Setter
    private Boolean showItemInTc = Boolean.FALSE;

    /**
     * 
     */
    @Getter
    @Setter
    private String note;

    /**
     * 
     */
    @Getter
    @Setter
    private Integer seq;

    /** 結案後是可執行on程式 */
    @Getter
    @Setter
    private Boolean whenCloseExeOnpg = Boolean.FALSE;

    /** 是否自動產生ON程式頁籤資訊 */
    @Getter
    @Setter
    private Boolean autoCreateOnpg = Boolean.FALSE;

    /** 權限對應 如果有多個角色字串以 '，' 分隔 */
    @Getter
    @Setter
    private String permissionRole;

    /** ON程式檢查完成可執行的角色 */
    @Getter
    @Setter
    private String execOnpgOkRole;

    /** 需求完成後是否自動結案 */
    @Getter
    @Setter
    private Boolean checkFinishAutoClosed = Boolean.FALSE;

    /** 系統預設帶入期望完成日天數 */
    @Getter
    @Setter
    private Integer defaultDueDays;
    
    /** 簽核層級 */
    @Getter
    @Setter
    private SignLevelType signLevel;
}
