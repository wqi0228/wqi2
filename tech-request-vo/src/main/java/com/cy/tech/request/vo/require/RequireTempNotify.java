/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 需求單 暫存訊息
 *
 * @author shaun
 */
@Data
@ToString(of = {"sid"})
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_require_temp_notify")
public class RequireTempNotify implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3210265197678057710L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "temp_sid", length = 36)
    private String sid;

    /** 需求單單號 */
    @Column(name = "require_no", nullable = false, length = 21, unique = true)
    private String requireNo;

    /** 通知者 */
    @Column(name = "create_usr", nullable = false)
    private Integer createdUser;

}
