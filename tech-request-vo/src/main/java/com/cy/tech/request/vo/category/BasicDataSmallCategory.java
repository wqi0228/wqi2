package com.cy.tech.request.vo.category;

import com.cy.commons.enums.Activation;
import com.cy.commons.interfaces.BaseEntity;
import com.cy.commons.vo.User;
import com.cy.tech.request.vo.anew.converter.SignLevelTypeConverter;
import com.cy.tech.request.vo.anew.enums.SignLevelType;
import com.cy.work.common.vo.converter.SidUserConverter;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 小類
 *
 * @author shaun
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = { "sid" })
@Entity
@Table(name = "tr_basic_data_small_category")
public class BasicDataSmallCategory implements Serializable, BaseEntity<String> {

    /**
     * 
     */
    private static final long serialVersionUID = -4180171292683486599L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "basic_data_small_category_sid", length = 36)
    private String sid;

    /**
     * 代碼 - 由系統管理員編碼
     */
    @Column(name = "small_category_id", unique = true, nullable = false, length = 8)
    private String id;

    /**
     * 小類名稱
     */
    @Column(name = "small_category_name", unique = true, nullable = false)
    private String name;

    /**
     * 對應的中類 [tr_basic_data_category].[BasicDataMiddleCategorySid]
     */
    @ManyToOne
    @JoinColumn(name = "parent_middle_category", nullable = false)
    private BasicDataMiddleCategory parentMiddleCategory;

    /**
     * 主管簽核設定
     */
    @Column(name = "tech_manager_sign", nullable = false)
    private Boolean techManagerSign = Boolean.FALSE;

    /**
     * 送測階段是否簽核
     */
    @Column(name = "test_sign", nullable = false)
    private Boolean testSign = Boolean.FALSE;

    /**
     * 
     */
    @Column(name = "pt_sign", nullable = false)
    private Boolean ptSign = Boolean.TRUE;


    /**
     * 需求單存檔是否需檢核附件
     */
    @Column(name = "check_attachment", nullable = false)
    private Boolean checkAttachment = Boolean.FALSE;

    /**
     * 是否自動結案
     */
    @Column(name = "auto_closed", nullable = false)
    private Boolean autoClosed = Boolean.FALSE;

    /** 是否可顯示於案件單轉需求單選項 */
    @Column(name = "show_info", nullable = false)
    private Boolean showItemInTc = Boolean.FALSE;

    @Column(name = "note")
    private String note;

    @Column(name = "seq")
    private Integer seq;

    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    @Convert(converter = SidUserConverter.class)
    @Column(name = "create_usr", nullable = false)
    private User createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    @Convert(converter = SidUserConverter.class)
    @Column(name = "update_usr", nullable = false)
    private User updatedUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

    /** 結案後是可執行on程式 */
    @Column(name = "a_c_onpg", nullable = false)
    private Boolean whenCloseExeOnpg = Boolean.FALSE;

    /** 是否自動產生ON程式頁籤資訊 */
    @Column(name = "aoto_onpg", nullable = false)
    private Boolean autoCreateOnpg = Boolean.FALSE;

    /** 權限對應 如果有多個角色字串以 '，' 分隔 */
    @Column(name = "permission_role")
    private String permissionRole;

    /** ON程式檢查完成可執行的角色 */
    @Column(name = "exec_onpg_ok")
    private String execOnpgOkRole;

    /** 需求完成後是否自動結案 */
    @Column(name = "check_finish_auto_closed", nullable = false)
    private Boolean checkFinishAutoClosed = Boolean.FALSE;

    /** 系統預設帶入期望完成日天數 */
    @Column(name = "default_due_day", nullable = false)
    private Integer defaultDueDays;
    
    /** 簽核層級 */
    @Convert(converter = SignLevelTypeConverter.class)
    @Column(name = "sign_level", nullable = false)
    private SignLevelType signLevel;
       
}
