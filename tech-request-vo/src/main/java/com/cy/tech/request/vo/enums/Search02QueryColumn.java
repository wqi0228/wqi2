/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import lombok.extern.slf4j.Slf4j;

/**
 * 需求單查詢-自訂搜尋元件底層列舉
 *
 * @author brain0925_liao
 */
@Slf4j
public enum Search02QueryColumn implements SearchReportCustomEnum {
    /** 需求單位簽核進度查詢-key */
    Search02Query("需求單位簽核進度查詢"),
    /** 需求類別 */
    DemandType("需求類別"),
    /** 待審核人員 */
    WaitApprovingPerson("待審核人員"),
    /** 需求單位 */
    Department("需求單位"),
    /** 需求人員 */
    DemandPerson("需求人員"),
    /** 模糊搜尋 */
    SearchText("模糊搜尋"),
    /** 類別組合 */
    CategoryCombo("類別組合"),
    /** 立單區間-索引 */
    DateIndex("立單區間-索引"),
    /** 需求單位審核狀態 */
    ApprovingProcess("需求單位審核狀態");

    /** 欄位名稱 */
    private final String val;

    Search02QueryColumn(String val) {
        this.val = val;
    }

    @Override
    public String getVal() {
        return val;
    }

    @Override
    public String getUrl() {
        return "Search02";
    }

    @Override
    public String getKey() {
        return this.name();
    }

    @Override
    public SearchReportCustomEnum getSearchReportCustomEnumByName(String name) {
        try {
            return Search02QueryColumn.valueOf(name);
        } catch (Exception e) {
            log.error("getSearchReportCustomEnumByName ERROR", e);
        }
        return null;
    }
}
