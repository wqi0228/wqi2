package com.cy.tech.request.vo.report.favorite;

import com.cy.commons.vo.User;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.work.common.vo.converter.JsonStringListToConverter;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 使用者自訂義報表快選區：tr_homepage_favorite
 *
 * @author kasim
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_homepage_favorite",
        uniqueConstraints = @UniqueConstraint(columnNames = {"usersid", "define_sid"}))
public class HomePageFavorite implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6254688696243517432L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "define_sid", length = 36)
    private String sid;

    /** 使用此設定的使用者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "usersid", nullable = false)
    private User createdUser;

    /** 顯示於首頁的報表(Json) */
    @Convert(converter = JsonStringListToConverter.class)
    @Column(name = "report_name", nullable = true)
    private JsonStringListTo homePages = new JsonStringListTo();

    /** 異動日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

}
