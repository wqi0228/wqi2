/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.onpg.enums;

/**
 * on程式上傳行為
 *
 * @author shaun
 */
public enum WorkOnpgAttachmentBehavior {

    /** 第一次填寫on程式時所上傳的附加檔案 */
    FIRST_FILE,
    /** 檢查記錄所上傳的附加檔案 */
    CHECK_RECORD_FILE,
    /** 檢查記錄回覆的回覆所上傳的附加檔案 */
    CHECK_RECORD_REPLY_FILE,
    /** 檢查註記所上傳的附加檔案 */
    CHECK_MEMO_FILE,
    /** 取消所上傳的附加檔案 */
    CANCEL_ONPG_FILE,
    /** 一般回覆所上傳的附加檔案 */
    GENERAL_REPLY_FILE,
    /** GM回覆所上傳的附加檔案 */
    GM_REPLY_FILE,
    /** QA回覆所上傳的附加檔案 */
    QA_REPLY_FILE,;
}
