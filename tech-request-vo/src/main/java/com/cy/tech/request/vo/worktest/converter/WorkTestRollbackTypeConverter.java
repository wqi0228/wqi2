/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.worktest.converter;

import com.cy.tech.request.vo.worktest.enums.WorkTestRollbackType;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 退測分類
 *
 * @author shaun
 */
@Slf4j
@Converter
public class WorkTestRollbackTypeConverter implements AttributeConverter<WorkTestRollbackType, String> {

    @Override
    public String convertToDatabaseColumn(WorkTestRollbackType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public WorkTestRollbackType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return WorkTestRollbackType.valueOf(dbData);
        } catch (Exception e) {
            log.error("WorkTestRollbackType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }

}
