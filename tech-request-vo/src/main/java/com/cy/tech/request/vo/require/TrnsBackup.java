package com.cy.tech.request.vo.require;

import com.cy.work.common.vo.AbstractEntity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 轉檔備份資料檔
 * 
 * @author allen1214_wu
 */
@NoArgsConstructor
@EqualsAndHashCode(of = { "sid" }, callSuper = false)
@Entity
@Table(name = "tr_trns_backup")
public class TrnsBackup extends AbstractEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1938106607419981502L;

    /**
     * SID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "backup_sid")
    @Getter
    @Setter
    private Long sid;

    /**
     * 需求單 SID
     */
    @Column(name = "trns_type", nullable = false, length = 36)
    @Getter
    @Setter
    private String trnsType;
    
    /**
     * 類別自定義 key
     */
    @Column(name = "cust_key",  length = 36)
    @Getter
    @Setter
    private String custKey;

    /**
     * 備份資料
     */
    @Column(name = "back_data")
    @Getter
    @Setter
    private String backData;
}
