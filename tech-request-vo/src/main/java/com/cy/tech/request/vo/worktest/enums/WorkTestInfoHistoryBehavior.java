/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.worktest.enums;

import lombok.Getter;

/**
 * 送測 歷史 狀態
 *
 * @author shaun
 */
public enum WorkTestInfoHistoryBehavior {

    REPLY("回覆"),
    REPLY_AND_REPLY("回覆的回覆"),
    ROLL_BACK("退測"),
    RETEST("重測 "),
    QA_TEST_COMPLETE("QA測試完成"),
    CANCEL_TEST("取消測試"),
    TEST_COMPLETE("測試完成"),
    MODIFY_ESTABLISH_DATE("修改預計完成日"),
    QA_JOIN_SCHEDULE("QA已納入排程"),
    QA_UNJOIN_SCHEDULE("QA不納入排程"),
    PAPER_READY("文件補齊"),
    ADJUST_TEST_DATE("調整送測日"),
    ADJUST_ONLINE_DATE("調整預計上線日"),
    ASSIGN_MASTER_TESTER("賦予主測人員"),
    ASSIGN_SLAVE_TESTER("賦予協測人員"),
    ADJUST_MASTER_TESTER("調整主測人員"),
    ADJUST_SLAVE_TESTER("調整協測人員"),
    TESTING("測試中"),
    ST_ESTABLISH("送測單已成立"),
    ADJUST_SCHEDULE_FINISH_DATE("調整排定完成日"),
    ADJUST_DATE("調整日期");

    @Getter
    private String value;
    
    private WorkTestInfoHistoryBehavior(String value) {
        this.value = value;
    }
}
