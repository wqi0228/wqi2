package com.cy.tech.request.vo.setting.asdep;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.cy.tech.request.vo.converter.AssignSendTypeConverter;
import com.cy.tech.request.vo.converter.RequireCheckItemTypeConverter;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.work.common.vo.AbstractEntity;
import com.cy.work.common.vo.converter.SplitListIntConverter;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 設定:檢查確認預設分派/通知單位
 * 
 * @author allen1214_wu
 */
@EqualsAndHashCode(callSuper = false, of = "sid")
@Table(name = "tr_setting_default_asdep")
@Entity
public class SettingDefaultAssignSendDep extends AbstractEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3612595758013728536L;

    /**
     * SID
     */
    @Id
    @Getter
    @Setter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sid")
    private String sid;

    /**
     * 小類SID
     */
    @Getter
    @Setter
    @Column(name = "basic_data_small_category_sid")
    private String smallCategorySid;

    /**
     * 檢查項目(對應系統別欄位)
     */
    @Getter
    @Setter
    @Convert(converter = RequireCheckItemTypeConverter.class)
    @Column(name = "check_item_type")
    private RequireCheckItemType checkItemType;

    /** 類型 分派 | 通知 */
    @Getter
    @Setter
    @Convert(converter = AssignSendTypeConverter.class)
    @Column(name = "assign_send_type")
    private AssignSendType assignSendType = AssignSendType.ASSIGN;

    /**
     * 可檢查部門 sid (以 , 分隔)
     */
    @Getter
    @Setter
    @Convert(converter = SplitListIntConverter.class)
    @Column(name = "deps_info")
    private List<Integer> depsInfo;

}