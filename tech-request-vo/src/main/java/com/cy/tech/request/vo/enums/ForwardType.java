/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import lombok.Getter;

/**
 * 轉寄類型
 *
 * @author shaun
 */
public enum ForwardType {

    FORWARD_DEPT("轉寄部門"),
    FORWARD_MEMBER("轉寄個人");
    
    @Getter
    private String value;
    
    private ForwardType(String value) {
        this.value = value;
    }
}
