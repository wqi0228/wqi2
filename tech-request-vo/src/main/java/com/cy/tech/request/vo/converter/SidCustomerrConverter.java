/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import com.cy.work.customer.vo.WorkCustomer;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 *
 * @author kasim
 */
@Converter
public class SidCustomerrConverter implements AttributeConverter<WorkCustomer, Long> {

    @Override
    public WorkCustomer convertToEntityAttribute(Long dbData) {
        return (dbData == null) ? null : new WorkCustomer(dbData);
    }

    @Override
    public Long convertToDatabaseColumn(WorkCustomer javaType) {
        return (javaType == null) ? null : javaType.getSid();
    }
}
