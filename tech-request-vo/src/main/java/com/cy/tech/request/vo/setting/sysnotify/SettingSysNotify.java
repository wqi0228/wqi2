package com.cy.tech.request.vo.setting.sysnotify;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.cy.tech.request.vo.setting.sysnotify.converter.SettingSysNotifySettingMainInfoConverter;
import com.cy.tech.request.vo.setting.sysnotify.to.SettingSysNotifyMainInfo;
import com.cy.work.common.vo.AbstractEntity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 設定：系統通知設定
 * 
 * @author allen1214_wu
 */
@EqualsAndHashCode(callSuper = false, of = "sid")
@Table(name = "tr_setting_sys_notify")
@Entity
@Getter
@Setter
public class SettingSysNotify extends AbstractEntity implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 4905831700444896983L;

    /**
	 * SID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sid")
	private String sid;

	/**
	 * 使用者 sid
	 */
	@Column(name = "user_sid")
	private Integer userSid;

	/**
	 * 設定資料(json string)
	 */
	@Convert(converter = SettingSysNotifySettingMainInfoConverter.class)
	@Column(name = "setting_info")
	private SettingSysNotifyMainInfo settingInfo;
}
