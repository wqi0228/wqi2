/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require.notify;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.converter.NotificationEventTypeConverter;
import com.cy.tech.request.vo.enums.NotificationEventType;

import lombok.Getter;
import lombok.Setter;

/**
 * 需求單 通知事件
 *
 * @author aken_kao
 */
@Entity
@Table(name = "tr_notification_event")
@Getter
@Setter
public class NotificationEvent implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6665229861020921803L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "ne_sid", length = 36)
    private String sid;

    @Column(name = "receiver", nullable = false)
    private Integer receiver;
    
    @Convert(converter = NotificationEventTypeConverter.class)
    @Column(name = "event_type", nullable = false)
    private NotificationEventType eventType;
    
    @Column(name = "event_desc", nullable = false)
    private String eventDesc;
    
    @Column(name = "source_no", nullable = false)
    private String sourceNo;
    
    @Column(name = "source_theme", nullable = false)
    private String sourceTheme;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    @Column(name = "create_usr", nullable = false)
    private Integer createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

    @Column(name = "update_usr")
    private Integer updatedUser;
    
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

}
