/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.value.to;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 特殊權限用的部門資料
 *
 * @author shaun
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DepTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4146394373289627660L;

    @JsonProperty(value = "sid")
    private Integer sid;

    @JsonProperty(value = "orgName")
    private String orgName;

    @JsonProperty(value = "uuid")
    private String uuid;

}
