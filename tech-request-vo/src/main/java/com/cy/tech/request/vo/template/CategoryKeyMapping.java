/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.template;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.category.BasicDataBigCategory;
import com.cy.tech.request.vo.category.BasicDataMiddleCategory;
import com.cy.tech.request.vo.category.BasicDataSmallCategory;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 類別鍵值對照表<BR/>
 * 模版組合鍵值表
 *
 * @author shaun
 */
@Data
@ToString(exclude = {"big","middle","small"})
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(
        name = "tr_category_key_mapping",
        uniqueConstraints = @UniqueConstraint(columnNames = {"mapping_id", "mapping_ver"})
)
public class CategoryKeyMapping implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -9061258239024294499L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "key_sid", length = 36)
    private String sid;

    /**
     * 大類ID+中類ID+小類ID 得到此 mapping_id <br/>
     * 基本上不會重覆
     */
    @Column(name = "mapping_id", length = 255, nullable = false)
    private String id;

    /** 鍵值版號 */
    @Column(name = "mapping_ver", nullable = false)
    private Integer version;

    /**
     * 大類
     */
    @ManyToOne
    @JoinColumn(name = "big_category_sid", nullable = false)
    private BasicDataBigCategory big;

    /**
     * 大類名稱
     */
    @Column(name = "big_category_name", length = 255, nullable = false)
    private String bigName;

    /**
     * 中類
     */
    @ManyToOne
    @JoinColumn(name = "middle_category_sid", nullable = false)
    private BasicDataMiddleCategory middle;

    /**
     * 中類名稱
     */
    @Column(name = "middle_category_name", length = 255, nullable = false)
    private String middleName;

    /**
     * 小類
     */
    @ManyToOne
    @JoinColumn(name = "small_category_sid", nullable = false)
    private BasicDataSmallCategory small;

    /**
     * 小類名稱
     */
    @Column(name = "small_category_name", length = 255, nullable = false)
    private String smallName;

    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

}
