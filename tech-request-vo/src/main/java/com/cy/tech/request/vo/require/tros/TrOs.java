/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require.tros;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.converter.OthSetStatusConverter;
import com.cy.tech.request.vo.enums.OthSetStatus;
import com.cy.work.common.vo.converter.JsonStringListToConverter;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 其他設定資訊物件
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "tr_os")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class TrOs implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5982598599336229200L;
    /** sid */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "os_sid", length = 36)
    private String sid;
    /** 其他設定資訊單號 */
    @Column(name = "os_no", nullable = false, length = 21, unique = true)
    private String os_no;
    /** 需求單Sid */
    @Column(name = "require_sid", nullable = false, length = 36)
    private String require_sid;
    /** 需求單單號 */
    @Column(name = "require_no", nullable = false, length = 21)
    private String require_no;
    /** 建立者公司Sid */
    @Column(name = "comp_sid", nullable = false)
    private Integer comp_sid;
    /** 建立者部門Sid */
    @Column(name = "dep_sid", nullable = false)
    private Integer dep_sid;
    /** 建立者Sid */
    @Column(name = "create_usr", nullable = false)
    private Integer create_usr;
    /** 建立日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date create_dt;
    /** 通知單位Json */
    @Convert(converter = JsonStringListToConverter.class)
    @Column(name = "os_notice_deps", nullable = false)
    private JsonStringListTo noticeDeps = new JsonStringListTo();
    /** 其他設定資訊狀態 */
    @Convert(converter = OthSetStatusConverter.class)
    @Column(name = "os_status", nullable = false)
    private OthSetStatus osStatus;
    /** 主題 */
    @Column(name = "os_theme", nullable = false, length = 255)
    private String theme;
    /** 完成日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "os_finish_dt")
    private Date finishDate;
    /** 取消日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "os_cancel_dt")
    private Date cancelDate;
    /** 內容 */
    @Column(name = "os_content", nullable = false)
    private String content;
    /** 內容css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "os_content_css", nullable = false)
    private String contentCss;
    /** 備註內容 */
    @Column(name = "os_note")
    private String note;
    /** 備註內容css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "os_note_css")
    private String noteCss;
    /** 狀態 */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.INACTIVE;
    /** 更新者 */
    @Column(name = "update_usr", nullable = true)
    private Integer update_usr;
    /** 更新時間 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;
}
