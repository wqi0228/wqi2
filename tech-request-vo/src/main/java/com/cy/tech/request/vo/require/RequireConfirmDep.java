package com.cy.tech.request.vo.require;

import com.cy.tech.request.vo.converter.ReqConfirmDepCompleteTypeConverter;
import com.cy.tech.request.vo.converter.ReqConfirmDepProgStatusConverter;
import com.cy.tech.request.vo.enums.ReqConfirmDepCompleteType;
import com.cy.tech.request.vo.enums.ReqConfirmDepProgStatus;
import com.cy.work.common.vo.AbstractEntity;
import com.cy.work.common.vo.listener.RemoveContorlCharListener;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 需求完成確認檔
 * 
 * @author allen1214_wu
 */
@NoArgsConstructor
@EqualsAndHashCode(of = { "sid" }, callSuper = false)
@Entity
@Table(name = "tr_require_confirm_dep")
// 移除 vo 中字串型態欄位值中的控制字元
@EntityListeners(RemoveContorlCharListener.class)
public class RequireConfirmDep extends AbstractEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1775547804231595417L;

    /**
     * SID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "require_confirm_sid")
    @Getter
    @Setter
    private Long sid;

    /**
     * 需求單 SID
     */
    @Column(name = "require_sid", nullable = false, length = 36)
    @Getter
    @Setter
    private String requireSid;

    /**
     * 分派單位 SID
     */
    @Column(name = "dep_sid", nullable = false)
    @Getter
    @Setter
    private Integer depSid;

    /**
     * 分派單位負責人 SID
     */
    @Column(name = "owner_sid", nullable = false)
    @Getter
    @Setter
    private Integer ownerSid;

    /**
     * 是否已領單
     */
    @Getter
    @Setter
    @Column(name = "received")
    private boolean received;

    /**
     * 需求確認進度
     */
    @Convert(converter = ReqConfirmDepProgStatusConverter.class)
    @Column(name = "prog_status", nullable = false)
    @Getter
    @Setter
    private ReqConfirmDepProgStatus progStatus;

    /**
     * 需求確認類型
     */
    @Convert(converter = ReqConfirmDepCompleteTypeConverter.class)
    @Column(name = "complete_type")
    @Getter
    @Setter
    private ReqConfirmDepCompleteType completeType;

    /**
     * 確認時間
     */
    @Getter
    @Setter
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "confirm_dt")
    private Date confirmDate;

    /**
     * 確認者
     */
    @Getter
    @Setter
    @Column(name = "confirm_user")
    private Integer confirmUser;

    /**
     * 需求完成說明
     */
    @Column(name = "complete_memo")
    @Getter
    @Setter
    private String completeMemo;
}
