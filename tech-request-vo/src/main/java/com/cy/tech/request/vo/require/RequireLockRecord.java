/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require;

import com.cy.commons.vo.User;
import com.cy.work.common.vo.converter.SidUserConverter;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 鎖定記錄：tr_lock_record
 *
 * @author jason_h
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_lock_record")
public class RequireLockRecord implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5925810833743921460L;

    @Id
    @Column(name = "require_sid", length = 36)
    private String sid;

    @Column(name = "require_no", nullable = false, length = 21, unique = true)
    private String requireNo;

    /** 鎖定的人員 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "lock_usr", nullable = false)
    private User lockedUser;

    /** 是否鎖定 */
    @Column(name = "locked")
    private Boolean hasLocked = Boolean.FALSE;

    /** 鎖定的時間 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

}
