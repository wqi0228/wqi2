/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.tech.request.vo.value.to.SetupInfoTo;
import com.google.common.base.Strings;
import java.io.IOException;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 群組明細資訊轉換器
 *
 * @author shaun
 */
@Slf4j
@Converter
public class SetupInfoToConverter implements AttributeConverter<SetupInfoTo, String> {

    @Override
    public String convertToDatabaseColumn(SetupInfoTo attribute) {
        if (attribute == null) {
            return "";
        }
        try {
            return WkJsonUtils.getInstance().toJson(attribute);
        } catch (IOException ex) {
            log.error("parser SetupInfoTo to json fail!!" + ex.getMessage());
            return "";
        }
    }

    @Override
    public SetupInfoTo convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return WkJsonUtils.getInstance().fromJson(dbData, SetupInfoTo.class);
        } catch (IOException ex) {
            log.error("parser json to SetupInfoTo fail!!" + ex.getMessage());
            return null;
        }
    }

}
