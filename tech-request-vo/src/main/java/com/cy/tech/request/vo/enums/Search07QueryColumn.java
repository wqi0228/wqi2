/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import lombok.extern.slf4j.Slf4j;

/**
 * 需求單查詢-自訂搜尋元件底層列舉
 *
 * @author brain0925_liao
 */
@Slf4j
public enum Search07QueryColumn implements SearchReportCustomEnum {
    /** 已分派單據查詢-key */
    Search07Query("已分派/通知單據查詢"),
    /** 需求類別 */
    DemandType("需求類別"),
    /** 被分派單位 */
    Department("被分派單位"),
    /** 完成碼 */
    FinishCode("完成碼"),
    /** 結案狀態 */
    CloseStatus("結案狀態"),
    /** 是否閱讀 */
    ReadStatus("是否閱讀"),
    /** 待閱原因 */
    WaitReadReson("待閱原因"),
    /** 類別組合 */
    CategoryCombo("類別組合"),
    /** 立單區間-索引 */
    DateIndex("立單區間-索引"),
    /** 立單區間-起始 */
    StartDate("立單區間-起始"),
    /** 立單區間-結束 */
    EndDate("立單區間-結束"),
    /** 模糊搜尋 */
    SearchText("模糊搜尋"),
    /** 區間選擇 */
    DateTimeType("區間選擇"),
    ASSIGN_DATE("分派區間"),
    UPDATE_DATE("異動區間");
    
    /** 欄位名稱 */
    private final String val;

    Search07QueryColumn(String val) {
        this.val = val;
    }

    @Override
    public String getVal() {
        return val;
    }

    @Override
    public String getUrl() {
        return "Search07";
    }

    @Override
    public String getKey() {
        return this.name();
    }

    @Override
    public SearchReportCustomEnum getSearchReportCustomEnumByName(String name) {
        try {
            return Search07QueryColumn.valueOf(name);
        } catch (Exception e) {
            log.error("getSearchReportCustomEnumByName ERROR", e);
        }
        return null;
    }
}
