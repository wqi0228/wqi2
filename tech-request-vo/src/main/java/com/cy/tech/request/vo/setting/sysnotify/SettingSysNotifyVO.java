package com.cy.tech.request.vo.setting.sysnotify;

import java.io.Serializable;

import org.springframework.beans.BeanUtils;

import com.cy.tech.request.vo.setting.sysnotify.to.SettingSysNotifyMainInfo;
import com.cy.work.common.vo.AbstractEntityVO;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 設定：系統通知設定 VO
 * 
 * @author allen1214_wu
 */
@EqualsAndHashCode(callSuper = false, of = "sid")
@Getter
@Setter
public class SettingSysNotifyVO extends AbstractEntityVO implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 2510228506931312692L;

    /**
	 * 建構子
	 * 
	 * @param entity SettingSysNotify
	 */
	public SettingSysNotifyVO(SettingSysNotify entity) {
		// ====================================
		// copy Properties
		// ====================================
		BeanUtils.copyProperties(entity, this);
	}
	
	/**
	 * 建構子 
	 * @param userSid userSid
	 */
	public SettingSysNotifyVO(Integer userSid) {
		this.userSid = userSid;
	}
	

	/**
	 * SID
	 */
	private String sid;

	/**
	 * 使用者 sid
	 */
	private Integer userSid;

	/**
	 * 設定資料
	 */
	private SettingSysNotifyMainInfo settingInfo = new SettingSysNotifyMainInfo();
}
