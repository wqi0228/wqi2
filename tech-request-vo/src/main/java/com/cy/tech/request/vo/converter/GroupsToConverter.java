/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.tech.request.vo.value.to.GroupsTo;
import com.google.common.base.Strings;
import java.io.IOException;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 群組資訊轉換器
 *
 * @author shaun
 */
@Slf4j
@Converter
public class GroupsToConverter implements AttributeConverter<GroupsTo, String> {

    @Override
    public String convertToDatabaseColumn(GroupsTo attribute) {
        if (attribute == null) {
            return "";
        }
        try {
            return WkJsonUtils.getInstance().toJson(attribute);
        } catch (IOException ex) {
            log.error("parser GroupsTo to json fail!!" + ex.getMessage());
            return "";
        }
    }

    @Override
    public GroupsTo convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return new GroupsTo();
        }
        try {
            return WkJsonUtils.getInstance().fromJson(dbData, GroupsTo.class);
        } catch (IOException ex) {
            log.error("parser json to GroupsTo fail!!" + ex.getMessage());
            return new GroupsTo();
        }
    }

}
