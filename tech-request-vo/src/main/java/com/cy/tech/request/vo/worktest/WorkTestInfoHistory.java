/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.worktest;

import com.cy.commons.enums.Activation;
import com.cy.commons.interfaces.BaseEntity;
import com.cy.commons.vo.User;
import com.cy.tech.request.vo.worktest.converter.WorkTestInfoHistoryBehaviorConverter;
import com.cy.tech.request.vo.worktest.converter.WorkTestRollbackTypeConverter;
import com.cy.tech.request.vo.worktest.converter.WorkTestStatusConverter;
import com.cy.tech.request.vo.worktest.enums.WorkTestInfoHistoryBehavior;
import com.cy.tech.request.vo.worktest.enums.WorkTestRollbackType;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.common.vo.converter.WorkSourceTypeConverter;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

/**
 * 送測異動紀錄
 *
 * @author shaun
 */
@Data
@NoArgsConstructor
@ToString(exclude = {"attachments"})
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "work_test_info_history")
public class WorkTestInfoHistory implements Serializable, BaseEntity<String> {

    /**
     * 
     */
    private static final long serialVersionUID = 9032274057913494478L;

    /** 送測單sid */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "testinfo_history_sid", length = 36)
    private String sid;

    /** 送測單主檔 */
    @ManyToOne
    @JoinColumn(name = "testinfo_sid", nullable = false)
    private WorkTestInfo testInfo;

    /** 送測單號 */
    @Column(name = "testinfo_no", nullable = false, length = 21)
    private String testinfoNo;

    /** 送測來源 */
    @Convert(converter = WorkSourceTypeConverter.class)
    @Column(name = "testinfo_source_type", nullable = false)
    private WorkSourceType sourceType;

    /** 送測來源 sid */
    @Column(name = "testinfo_source_sid", nullable = false, length = 36)
    private String sourceSid;

    /** 送測來源 單號 */
    @Column(name = "testinfo_source_no", nullable = false, length = 21)
    private String sourceNo;

    /** 歷史資訊行為狀態 */
    @Convert(converter = WorkTestInfoHistoryBehaviorConverter.class)
    @Column(name = "behavior", nullable = false)
    private WorkTestInfoHistoryBehavior behavior;

    /** 此時的送測單狀態 */
    @Convert(converter = WorkTestStatusConverter.class)
    @Column(name = "behavior_status", nullable = false)
    private WorkTestStatus behaviorStatus;

    /** 測試回覆資訊sid */
    @OneToOne
    @JoinColumn(name = "reply_sid")
    private WorkTestReply reply;

    /** 測試回覆的回覆資訊sid */
    @OneToOne
    @JoinColumn(name = "already_reply_sid")
    private WorkTestAlreadyReply alreadyReply;

    /** 退測分類 */
    @Convert(converter = WorkTestRollbackTypeConverter.class)
    @Column(name = "rollback_type")
    private WorkTestRollbackType rollBackType;

    /** 測試回覆的回覆資訊sid */
    @OneToOne
    @JoinColumn(name = "testinfo_qa_report_sid")
    private WorkTestQAReport qaReport;

    /** 原因 */
    @Column(name = "reason")
    private String reason;

    /** 原因 含css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "reason_css")
    private String reasonCss;

    /** 測試完成的回覆資訊 */
    @Column(name = "finish_test_reply")
    private String finishTestReply;

    /** 測試完成的回覆資訊 含css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "finish_test_reply_css")
    private String finishTestReplyCss;

    /** 顯示於異動明細表上 */
    @Column(name = "visiable", nullable = false)
    private Boolean visiable = Boolean.FALSE;

    /** 狀態 */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    /** 建立者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "create_usr", nullable = false)
    private User createdUser;

    /** 建立日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    /** 異動者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "update_usr")
    private User updatedUser;

    /** 異動日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

    /** 附加檔案 */
    @OneToMany(mappedBy = "history", cascade = CascadeType.PERSIST)
    @Where(clause = "status = 0")
    @OrderBy(value = "createdDate DESC")
    private List<WorkTestAttachment> attachments = Lists.newArrayList();


}
