package com.cy.tech.request.vo.enums;

import lombok.Getter;

/**
 * 需求完成確認部門 - 操作類型
 * 
 * @author allen1214_wu
 */
public enum ReqConfirmDepProcType {

    ASSIGN_CREATE("加派並建立確認流程"),
    ASSIGN("加派"),
    REMOVE_ASSIGN("減派"),
    REMOVE_ASSIGN_DELETE("減派並移除確認流程"),
    CHANGE_OWNER("異動負責人"),
    RECEIVE("領單"),
    CONFIRM("確認完成"),
    WONT_DO("無需處理"),
    RECOVERY("復原"),
    PASS_BY_REQ_COMPLETE("無須確認"),
    REOPEN_BY_REQ_COMPLETE_ROLLBACK("重新開啟"),
    ORG_TRNS("組織異動"),
    ADD_SUB_CASE("新增子單"),
    ;

    /**
     * 顯示名稱
     */
    @Getter
    private final String label;

    /**
     * @param label
     */
    private ReqConfirmDepProcType(String label) {
        this.label = label;
    }
}
