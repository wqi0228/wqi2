/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import lombok.extern.slf4j.Slf4j;

/**
 * @author aken_kao
 */
@Slf4j
public enum OverdueUnclosedColumn implements SearchReportCustomEnum {
    
    OVERDUE_UNCLOSED("逾期未結案需求報表"),
    /** 需求類別 */
    DEMAND_TYPE("需求類別"),
    /** 單位挑選 */
    DEPARTMENT("單位挑選"),
    /** 需求製作進度 */
    DEMAND_PROCESS("需求製作進度"),
    /** 是否閱讀 */
    READ_STATUS("是否閱讀"),
    /** 待閱原因 */
    WAIT_READ_REASON("待閱原因"),
    /** 需求人員 */
    DEMAND_PERSON("需求人員"),
    /** 類別組合 */
    CATEGORY_COMBO("類別組合"),
    /** 立單區間-索引 */
    DATE_INDEX("立單區間-索引"),
    /** 模糊搜尋 */
    SEARCH_TEXT("模糊搜尋");

    /** 欄位名稱 */
    private final String value;

    OverdueUnclosedColumn(String val) {
        this.value = val;
    }

    @Override
    public String getVal() {
        return value;
    }

    @Override
    public String getUrl() {
        return "overdueUnclosed";
    }

    @Override
    public String getKey() {
        return this.name();
    }

    @Override
    public SearchReportCustomEnum getSearchReportCustomEnumByName(String name) {
        try {
            return OverdueUnclosedColumn.valueOf(name);
        } catch (Exception e) {
            log.error("getSearchReportCustomEnumByName ERROR", e);
        }
        return null;
    }
}
