/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.value.to;

import com.cy.work.common.vo.value.to.ReadRecordHistoryTo;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.tech.request.vo.require.AlertInbox;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 閱讀記錄 僅供收件夾使用，勿轉成Json格式
 *
 * @author shaun
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(of = "reader")
public class ReadRecordAdvanceTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 568002769823635901L;
    /** 閱讀人員Sid */
    @JsonProperty(value = "reader")
    private String reader;
    /** 閱讀狀態碼 */
    @JsonProperty(value = "type")
    private ReadRecordType type;
    /** 閱讀日期 */
    @JsonProperty(value = "readDay")
    private Date readDay;
    /** 回收日期 */
    @JsonProperty(value = "recoverDay")
    private Date recoverDay;
    /** 對應的 inbox */
    @JsonProperty(value = "inbox")
    private AlertInbox inbox;

    /** 閱讀狀態歷程 - 最新的在最上面 history.addFirst() - 極限僅存放5個 history.removeLast() */
    @JsonProperty(value = "history")
    private LinkedList<ReadRecordHistoryTo> history = Lists.newLinkedList();

}
