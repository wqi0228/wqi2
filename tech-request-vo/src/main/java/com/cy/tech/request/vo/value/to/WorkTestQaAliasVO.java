package com.cy.tech.request.vo.value.to;

import lombok.Data;

@Data
public class WorkTestQaAliasVO {

    private String sid;
    private Integer userSid;
    private String userId;
    private String userAlias;
    private String orgName;
    
    public WorkTestQaAliasVO(){
        
    }
}
