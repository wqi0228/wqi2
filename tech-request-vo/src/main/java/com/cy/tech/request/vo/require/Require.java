/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require;

import com.cy.commons.enums.Activation;
import com.cy.commons.interfaces.BaseEntity;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.vo.converter.CateConfirmConverter;
import com.cy.tech.request.vo.converter.ReqToBeReadTypeConverter;
import com.cy.work.common.vo.converter.ReadRecordGroupToConverter;
import com.cy.tech.request.vo.converter.RequireContentToConverter;
import com.cy.tech.request.vo.converter.RequireFinishCodeTypeConverter;
import com.cy.tech.request.vo.converter.RequireFinishMethodTypeConverter;
import com.cy.tech.request.vo.converter.RequireStatusTypeConverter;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.tech.request.vo.enums.CateConfirmType;
import com.cy.tech.request.vo.enums.ReqToBeReadType;
import com.cy.tech.request.vo.enums.RequireFinishCodeType;
import com.cy.tech.request.vo.enums.RequireFinishMethodType;
import com.cy.work.common.vo.converter.SidOrgConverter;
import com.cy.work.common.vo.converter.UrgencyTypeConverter;
import com.cy.tech.request.vo.enums.RequireStatusType;
import com.cy.tech.request.vo.require.setting.RequireExtraSetting;
import com.cy.work.common.enums.UrgencyType;
import com.cy.tech.request.vo.template.CategoryKeyMapping;
import com.cy.work.common.vo.value.to.ReadRecordGroupTo;
import com.cy.tech.request.vo.value.to.RequireContentTo;
import com.cy.work.customer.vo.WorkCustomer;
import com.cy.work.group.vo.WorkLinkGroup;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

/**
 * 需求單主檔
 *
 * @author shaun
 */
@Data
@ToString(of = { "sid", "requireNo" })
@NoArgsConstructor
@EqualsAndHashCode(of = { "sid" })
@Entity
@Table(name = "tr_require")
public class Require implements Serializable, BaseEntity<String> {

    /**
     * 
     */
    private static final long serialVersionUID = 647177289196105907L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "require_sid", length = 36)
    private String sid;

    /** 需求單單號 */
    @Column(name = "require_no", nullable = false, length = 21, unique = true)
    private String requireNo;

    /**
     * 模版鍵值 tr_category_key_mapping.key_sid mapping_sid 命名有點問題,應該叫 mapping_key_sid
     * 才不易混淆原意
     */
    @ManyToOne
    @JoinColumn(name = "mapping_sid", nullable = false)
    private CategoryKeyMapping mapping;

    /** 申請人所歸屬的公司 */
    @Convert(converter = SidOrgConverter.class)
    @Column(name = "comp_sid", nullable = false)
    private Org createCompany;

    /** 申請人所歸屬單位 */
    @Convert(converter = SidOrgConverter.class)
    @Column(name = "dep_sid", nullable = false)
    private Org createDep;

    /** 建單成員 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "create_usr", nullable = false)
    private User createdUser;

    /** 需求單內容 */
    @Convert(converter = RequireContentToConverter.class)
    @Column(name = "content")
    private RequireContentTo content;

    /** 需求單欄位css內容 */
    @OneToMany(mappedBy = "require", cascade = CascadeType.ALL)
    private List<RequireCssContent> cssContents = Lists.newArrayList();

    /** 是否有附加檔案 */
    @Column(name = "has_attachment", nullable = false)
    private Boolean hasAttachment = Boolean.FALSE;

    /** 附加檔案 */
    @OneToMany(mappedBy = "require", cascade = CascadeType.PERSIST)
    @Where(clause = "status = 0")
    @OrderBy(value = "createdDate DESC")
    private List<RequireAttachment> attachments = Lists.newArrayList();

    /** 需求單位主管是否審核 */
    @Column(name = "has_require_manager_sign", nullable = false)
    private Boolean hasReqUnitSign = Boolean.FALSE;

    /** 需求單位流程審核資訊 */
    @OneToOne(mappedBy = "require", cascade = CascadeType.ALL)
    private RequireUnitSignInfo reqUnitSign;

    /** 需求成立日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "require_establish_dt")
    private Date requireEstablishDate;

    /** 技術事業群主管是否審核 */
    @Column(name = "has_tech_manager_sign", nullable = false)
    private Boolean hasTechManagerSign = Boolean.FALSE;

    /** 技術單位流程審核資訊 */
    @OneToOne(mappedBy = "require")
    private TechManagerSignInfo techManagerSignInfo;

    /** 是否有原型確認 */
    @Column(name = "has_prototype", nullable = false)
    private Boolean hasPrototype = Boolean.FALSE;

    /** 原型確認重做碼 */
    @Column(name = "prototype_redo_code", nullable = false)
    private Boolean redoCode = Boolean.FALSE;

    /** 需求製作進度 */
    @Convert(converter = RequireStatusTypeConverter.class)
    @Column(name = "require_status", nullable = false)
    private RequireStatusType requireStatus = RequireStatusType.NEW_INSTANCE;

    /** 緊急度 */
    @Convert(converter = UrgencyTypeConverter.class)
    @Column(name = "urgency", nullable = false)
    private UrgencyType urgency = UrgencyType.GENERAL;

    /** 立單日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt")
    private Date createdDate;

    /** 是否有關聯頁面 */
    @Column(name = "has_link", nullable = false)
    private Boolean hasLink = Boolean.FALSE;

    /** 關聯群組 */
    @OneToOne
    @JoinColumn(name = "link_group_sid")
    private WorkLinkGroup linkGroup;

    /** 分類確認碼 */
    @Convert(converter = CateConfirmConverter.class)
    @Column(name = "category_confirm_code", length = 10, nullable = false)
    private CateConfirmType categoryConfirmCode = CateConfirmType.N;

    /** 統整人員 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "inte_staff")
    private User inteStaffUser;

    /** 結案後是否可執行on程式 */
    @Column(name = "a_c_onpg", nullable = false)
    private Boolean whenCloseExeOnpg = Boolean.FALSE;

    /** 退件通知碼 */
    @Column(name = "back_code", nullable = false)
    private Boolean backCode = Boolean.FALSE;

    /** 結案碼 */
    @Column(name = "close_code", nullable = false)
    private Boolean closeCode = Boolean.FALSE;

    /** 結案日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "close_dt")
    private Date closeDate;

    /** 結案成員 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "close_usr")
    private User closeUser;

    /** 製作進度完成碼 */
    @Convert(converter = RequireFinishCodeTypeConverter.class)
    @Column(name = "require_finish_code", nullable = false)
    private RequireFinishCodeType finishCode = RequireFinishCodeType.INCOMPLETE;

    /** 需求完成方式 */
    @Convert(converter = RequireFinishMethodTypeConverter.class)
    @Column(name = "require_finish_method")
    private RequireFinishMethodType finishMethod;

    /** 需求完成日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "require_finish_dt")
    private Date finishDate;

    /** 期望完成日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "hope_dt", nullable = false)
    private Date hopeDate;

    /** 需求暫緩碼 */
    @Column(name = "require_suspended_code", nullable = false)
    private Boolean requireSuspendedCode = Boolean.FALSE;

    /** 作廢碼 */
    @Column(name = "invalid_code", nullable = false)
    private Boolean invalidCode = Boolean.FALSE;

    /** 是否有追蹤頁籤 */
    @Column(name = "has_trace", nullable = false)
    private Boolean hasTrace = Boolean.FALSE;

    /** 是否有回覆頁籤 */
    @Column(name = "has_reply", nullable = false)
    private Boolean hasReply = Boolean.FALSE;

    /** 異動日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

    /** 異動成員 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "update_usr")
    private User updatedUser;

    /** 是否有轉寄部門 */
    @Column(name = "has_forward_dep", nullable = false)
    private Boolean hasForwardDep = Boolean.FALSE;

    /** 是否有轉寄個人 */
    @Column(name = "has_forward_member", nullable = false)
    private Boolean hasForwardMember = Boolean.FALSE;

    /** 是否已分派 */
    @Column(name = "has_assign", nullable = false)
    private Boolean hasAssign = Boolean.FALSE;

    /** 閱讀紀錄資訊 */
    @Convert(converter = ReadRecordGroupToConverter.class)
    @Column(name = "read_record")
    private ReadRecordGroupTo readRecordGroup = new ReadRecordGroupTo();

    public void setReadRecordGroup(ReadRecordGroupTo readRecordGroup) { this.readRecordGroup = readRecordGroup; }

    /** 單據狀態 */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    /** 索引資料集 */
    @OneToMany(mappedBy = "require", cascade = CascadeType.ALL)
    private List<RequireIndexDictionary> index = Lists.newArrayList();

    /** 功能符合需求碼 */
    @Column(name = "function_ok_code", nullable = false)
    private Boolean functionOkCode = Boolean.FALSE;

    /** 功能符合需求日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "function_ok_dt", nullable = true)
    private Date functionOkDate;

    /** 是否有轉FB紀錄 */
    @Column(name = "has_fb_record", nullable = false)
    private Boolean hasFbRecord = Boolean.FALSE;

    /** 是否有送測 */
    @Column(name = "has_testinfo", nullable = false)
    private Boolean hasTestInfo = Boolean.FALSE;

    /** 是否有on程式 */
    @Column(name = "has_onpginfo", nullable = false)
    private Boolean hasOnpg = Boolean.FALSE;

    /** 是否有其它設定資訊 */
    @Column(name = "has_os", nullable = false)
    private Boolean hasOthSet = Boolean.FALSE;

    /** 廳主 */
    @OneToOne
    @JoinColumn(name = "customer", nullable = false)
    private WorkCustomer customer;

    /** 提出客戶 */
    @OneToOne
    @JoinColumn(name = "author")
    private WorkCustomer author;

    /** 待閱原因 */
    @Convert(converter = ReqToBeReadTypeConverter.class)
    @Column(name = "read_reason")
    private ReqToBeReadType readReason;

    /** 草稿建立日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "draft_dt")
    private Date draftDate;

    /** 草稿異動日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "draft_update_dt")
    private Date draftUpdatedDate;

    /** 需求完成人員 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "require_finish_usr")
    private User requireFinishUsr;

    /** 移除草稿後給予狀態判讀 */
    @Transient
    @Getter
    @Setter
    private Boolean isDraftDelete = false;

    @OneToMany(mappedBy = "require", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<RequireExtraSetting> extraSettings;

    /** NCS是否已執行完成 */
    @Transient
    @Getter
    @Setter
    private Boolean isNcsDone;

    /**
     * 主責單位 (REQ-1440)
     */
    @Getter
    @Setter
    @Column(name = "in_charge_dep")
    private Integer inChargeDep;

    /**
     * 主責單位窗口 (REQ-1440)
     */
    @Getter
    @Setter
    @Column(name = "in_charge_usr")
    private Integer inChargeUsr;
}