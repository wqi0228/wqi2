package com.cy.tech.request.vo.enums;

import lombok.Getter;

/**
 * 分派部門需求完成進度
 * 
 * @author allen1214_wu
 */
public enum ReqConfirmDepProgStatus {

	WAIT_CONFIRM("待確認", 10, "#f63a0f", false),
	IN_PROCESS("進行中", 50, "#f2b01e", false),
	COMPLETE("已完成", 100, "#86e01e", true),
	PASS("關閉", 100, "gray", true);

	/**
	 * 顯示名稱
	 */
	@Getter
	private final String label;

	/**
	 * 進度百分比
	 */
	@Getter
	private final Integer progressPercent;

	/**
	 * 進度條顏色
	 */
	@Getter
	private final String progressColer;

	/**
	 * 此階段為流程終點狀態
	 */
	@Getter
	private boolean inFinish;

	/**
	 * @param label
	 * @param progressPercent
	 * @param progressColer
	 */
	private ReqConfirmDepProgStatus(String label, Integer progressPercent, String progressColer, boolean inFinish) {
		this.label = label;
		this.progressPercent = progressPercent;
		this.progressColer = progressColer;
		this.inFinish = inFinish;
	}

	/**
	 * 字串是否符合任一列舉項目名稱
	 * 
	 * @param test
	 * @return true/false
	 */
	public static boolean isContains(String test) {
		for (ReqConfirmDepProgStatus c : ReqConfirmDepProgStatus.values()) {
			if (c.name().equals(test)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 字串轉列舉項目
	 * 
	 * @param dbStr
	 * @return ReqConfirmDepProgStatus
	 */
	public static ReqConfirmDepProgStatus trnsFromStr(String dbStr) {
		for (ReqConfirmDepProgStatus c : ReqConfirmDepProgStatus.values()) {
			if (c.name().equals(dbStr)) {
				return c;
			}
		}
		return null;
	}
}
