/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.worktest.enums;

/**
 * 退測單位分類
 *
 * @author shaun
 */
public enum WorkTestRollbackType {

    /** 0 技術單位 */
    TECH,
    /** 1 市場單位 */
    MARKET,
    /** 2 未知 */
    UNKNOW,;
}
