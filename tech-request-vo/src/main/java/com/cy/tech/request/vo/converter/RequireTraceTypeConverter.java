/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import com.cy.tech.request.vo.enums.RequireTraceType;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 需求追蹤型態轉換器
 *
 * @author shaun
 */
@Slf4j
@Converter
public class RequireTraceTypeConverter implements AttributeConverter<RequireTraceType, String> {

    @Override
    public String convertToDatabaseColumn(RequireTraceType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public RequireTraceType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return RequireTraceType.valueOf(dbData);
        } catch (Exception e) {
            log.error("RequireTraceType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }

}
