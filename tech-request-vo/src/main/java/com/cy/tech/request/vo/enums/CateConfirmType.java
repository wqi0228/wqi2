/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

/**
 * 檢查確認碼型態(分類確認碼)
 * <BR/>
 * 需求類別指的是大類(BasicDataBigCategory)
 *
 * @author shaun
 */
public enum CateConfirmType {
    /** 一般需求類別 - 已確認 */
    Y,
    /** 一般需求類別 - 待確認 */
    N,
    /** 內部需求類別 */
    n,;
}
