/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.template.component;

/**
 * 模版設定用
 *
 * @author shaun
 */
public interface ComBaseMapListStr {

    public String getSelectItemKey();

    public void setSelectItemKey(String key);

    /**
     * 新增下拉式選單key值
     */
    public void addSelectItem();

    /**
     * 移除下拉式選單key值
     *
     * @param index */
    public void subSelectItem(String index);

    /**
     * 新增下拉式選單對應選項
     *
     */
    public void addCheckBoxItem();

    /**
     * 移除下拉式選單對應選項
     *
     * @param index
     */
    public void subCheckBoxItem(int index);

}
