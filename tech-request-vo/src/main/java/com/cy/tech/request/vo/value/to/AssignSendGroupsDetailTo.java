/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. 
 */
package com.cy.tech.request.vo.value.to;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 分派通知群組物件清單資訊
 *
 * @author shaun
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AssignSendGroupsDetailTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2808322055506241012L;

    /** 群組設定sid */
    @JsonProperty(value = "sid")
    private String sid;

    /** 群組設定名稱 */
    @JsonProperty(value = "name")
    private String name;

    /** 群組設定對應個人及部門資訊 */
    @JsonProperty(value = "info")
    private SetupInfoTo info = new SetupInfoTo();
}
