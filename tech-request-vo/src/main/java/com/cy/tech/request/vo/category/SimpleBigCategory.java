package com.cy.tech.request.vo.category;

import com.cy.tech.request.vo.converter.ReqCateTypeConverter;
import com.cy.tech.request.vo.enums.ReqCateType;
import com.cy.work.common.vo.AbstractEntity;
import com.cy.work.common.vo.converter.JsonStringListToConverter;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

/**
 * 01_類別基本資料(類別)
 *
 * @author shaun
 */
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"}, callSuper = false)
@Entity
@Table(name = "tr_basic_data_big_category")
public class SimpleBigCategory extends AbstractEntity implements  Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3895586154721167375L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "basic_data_big_category_sid", length = 36)
    @Getter
    @Setter
    private String sid;

    /**
     * 代碼 - 由系統管理員編碼
     */
    @Column(name = "big_category_id", unique = true, nullable = false, length = 8)
    @Getter
    @Setter
    private String id;

    /**
     * 名稱
     */
    @Column(name = "big_category_name", unique = true, nullable = false)
    @Getter
    @Setter
    private String name;

    /**
     * 備註事項
     */
    @Column(name = "note", length = 255)
    @Getter
    @Setter
    private String note;

    /**
     * 排序
     */
    @Column(name = "seq")
    @Getter
    @Setter
    private Integer seq;

    /**
     * 
     */
    @Convert(converter = JsonStringListToConverter.class)
    @Column(name = "use_dep", nullable = true)
    @Getter
    @Setter
    private JsonStringListTo canUseDepts = new JsonStringListTo();

    /**
     * 
     */
    @Convert(converter = ReqCateTypeConverter.class)
    @Column(name = "req_category", nullable = true)
    @Getter
    @Setter
    private ReqCateType reqCateType = ReqCateType.EXTERNAL;

    /**
     * 權限對應 如果有多個角色字串以 '，' 分隔
     */
    @Column(name = "permission_role")
    @Getter
    @Setter
    private String permissionRole;

}
