/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require.othset;

import com.cy.commons.enums.Activation;
import com.cy.commons.interfaces.BaseEntity;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.vo.converter.OthSetStatusConverter;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.tech.request.vo.enums.OthSetStatus;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.converter.JsonStringListToConverter;
import com.cy.work.common.vo.converter.SidOrgConverter;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

/**
 * 其他設定資訊
 *
 * @author shaun
 */
@Data
@NoArgsConstructor
@ToString(exclude = {"historys", "replys", "attachments"})
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_os")
public class OthSet implements Serializable, BaseEntity<String> {

    /**
     * 
     */
    private static final long serialVersionUID = -6994586888377958457L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "os_sid", length = 36)
    private String sid;

    /** 其他設定資訊單號 */
    @Column(name = "os_no", nullable = false, length = 21, unique = true)
    private String osNo;

    /** 需求單主檔 */
    @ManyToOne
    @JoinColumn(name = "require_sid", nullable = false)
    private Require require;

    /** 需求單單號 */
    @Column(name = "require_no", nullable = false, length = 21)
    private String requireNo;

    /** 填單人所歸屬的公司 */
    @Convert(converter = SidOrgConverter.class)
    @Column(name = "comp_sid", nullable = false)
    private Org createCompany;

    /** 填單人所歸屬的部門 */
    @Convert(converter = SidOrgConverter.class)
    @Column(name = "dep_sid", nullable = false)
    private Org createDep;

    /** 建立者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "create_usr", nullable = false)
    private User createdUser;

    /** 建立日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    /** 通知單位 */
    @Convert(converter = JsonStringListToConverter.class)
    @Column(name = "os_notice_deps", nullable = false)
    private JsonStringListTo noticeDeps = new JsonStringListTo();
    
    /**
     * 取得通知單位SID
     * 
     * @return
     */
    public Set<Integer> getNoticeDepSids() {
        if (noticeDeps == null || WkStringUtils.isEmpty(noticeDeps.getValue())) {
            return Sets.newHashSet();
        }
        return noticeDeps.getValue().stream()
                .filter(sidStr -> WkStringUtils.isNumber(sidStr))
                .map(sidStr -> Integer.parseInt(sidStr))
                .collect(Collectors.toSet());
    }

    /** 設定資訊狀態 */
    @Convert(converter = OthSetStatusConverter.class)
    @Column(name = "os_status", nullable = false)
    private OthSetStatus osStatus;

    /** 其他設定資訊主題 <BR/>
     * 設定資訊-新包網  <BR/>
     * 設定資訊-購物網 <BR/>
     * DBA協助 <BR/>
     * 新增廳主 <BR/>
     * 版面設計 <BR/>
     * 送翻 <BR/>
     * 其他 <BR/>
     * PS下拉式清單... by anna virus...20160111
     */
    @Column(name = "os_theme", nullable = false, length = 255)
    private String theme;
    
    @Transient
    private String theme_sid;

    /** 完成日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "os_finish_dt")
    private Date finishDate;

    /** 取消日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "os_cancel_dt")
    private Date cancelDate;

    /** 資訊內容 */
    @Column(name = "os_content", nullable = false)
    private String content;

    /** 資訊內容_CSS格式 */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "os_content_css", nullable = false)
    private String contentCss;

    /** 備註說明 */
    @Column(name = "os_note")
    private String note;

    /** 備註說明_有CSS格式 */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "os_note_css")
    private String noteCss;

    /** 此版本的使用狀態 */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.INACTIVE;

    /** 異動者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "update_usr")
    private User updatedUser;

    /** 異動日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

    /** ON程式歷程紀錄 */
    @OneToMany(mappedBy = "othset", cascade = CascadeType.PERSIST)
    @OrderBy("updatedDate DESC")
    @Where(clause = "behavior != 'REPLY_AND_REPLY' ")
    private List<OthSetHistory> historys = Lists.newArrayList();

    /** 回覆 */
    @OneToMany(mappedBy = "othset", cascade = CascadeType.ALL)
    private List<OthSetReply> replys = Lists.newArrayList();

    /** 附加檔案 */
    @OneToMany(mappedBy = "othset", cascade = CascadeType.PERSIST)
    @Where(clause = "status = 0 AND os_history_sid IS NULL")
    @OrderBy(value = "createdDate DESC")
    private List<OthSetAttachment> attachments = Lists.newArrayList();

}
