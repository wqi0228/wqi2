/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

/**
 * 其它設定資訊通知型態
 *
 * @author shaun
 */
public enum OthSetAlertType {
    /** 回覆 */
    REPLY,
    /** 回覆的回覆 */
    REPLY_AND_REPLY,
    /** 完成 */
    FINISH,
    /** 取消 */
    CANCEL,
    /** 通知要開始作業 */
    NOTICE_WORKING
}
