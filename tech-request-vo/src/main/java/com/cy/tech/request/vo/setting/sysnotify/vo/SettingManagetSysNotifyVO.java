package com.cy.tech.request.vo.setting.sysnotify.vo;

import java.io.Serializable;
import java.util.Map;

import com.cy.work.notify.vo.enums.NotifyType;
import com.google.common.collect.Maps;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 設定：系統通知設定管理 view object
 * 
 * @author allen1214_wu
 */
@EqualsAndHashCode(of = { "userSid", "itemSid" })
public class SettingManagetSysNotifyVO implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -6167742026792604326L;

    /**
	 * 建構子 (參數為 key)
	 * 
	 * @param userSid 使用者 sid
	 * @param itemSid 小類 sid ,或廳主sid
	 */
	public SettingManagetSysNotifyVO(Integer userSid, String itemSid) {
		this.userSid = userSid;
		this.itemSid = itemSid;

		// 先建立起通知類別設定資料容器
		this.openSwitchMapByNotifyType = Maps.newHashMap();
		for (NotifyType notifyType : NotifyType.values()) {
			this.openSwitchMapByNotifyType.put(notifyType, false);
		}
	}

	/**
	 * 設定是否開啟
	 * @param notifyType 系統通知類別
	 * @return 設定是否開啟
	 */
	public boolean isOpen(NotifyType notifyType) {
		return Boolean.valueOf(this.openSwitchMapByNotifyType.get(notifyType) + "");
	}

	/**
	 * user Sid
	 */
	@Getter
	@Setter
	private Integer userSid;

	/**
	 * 小類 sid ,或廳主sid
	 */
	@Getter
	@Setter
	private String itemSid;

	/**
	 * 通知類別設定資料
	 */
	@Getter
	@Setter
	private Map<NotifyType, Boolean> openSwitchMapByNotifyType = Maps.newHashMap();

	/**
	 * user 暱稱
	 */
	@Getter
	@Setter
	private String userName;

	/**
	 * user 畫面顯示資訊
	 */
	@Getter
	@Setter
	private String userInfo;

	/**
	 * user 主要部門
	 */
	@Getter
	@Setter
	private Integer userDepSid;

}
