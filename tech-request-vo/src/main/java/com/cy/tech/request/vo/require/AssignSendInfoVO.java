/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require;

import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.value.to.AssignSendGroupsTo;
import com.cy.tech.request.vo.value.to.SetupInfoTo;
import com.cy.work.common.vo.AbstractEntityVO;

import java.io.Serializable;
import javax.persistence.Id;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 需求單分派資訊主檔
 *
 * @author shaun
 */
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = { "sid" }, callSuper = false)
public class AssignSendInfoVO extends AbstractEntityVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1188893679778158608L;

    @Id
    @Getter
    @Setter
    private String sid;

    /** 需求單主檔 */
    @Getter
    @Setter
    private String requireSid;

    /** 需求單單號 */
    @Getter
    @Setter
    private String requireNo;

    /** 類型 分派 | 通知 */
    @Getter
    @Setter
    private AssignSendType type = AssignSendType.ASSIGN;

    /** 部門及成員資訊 */
    @Getter
    @Setter
    private SetupInfoTo info = new SetupInfoTo();

    /** 群組資訊 */
    @Getter
    @Setter
    private AssignSendGroupsTo groupInfo = new AssignSendGroupsTo();
}
