/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require.othset;

import com.cy.commons.vo.User;
import com.cy.tech.request.vo.converter.OthSetAlertTypeConverter;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.tech.request.vo.enums.OthSetAlertType;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.vo.converter.ReadRecordTypeConverter;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 其他設定資訊 通知訊息
 *
 * @author shaun
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_os_alert")
public class OthSetAlert implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1517445772291818954L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "os_alert_sid", length = 36)
    private String sid;

    /** 其他設定資訊主檔 */
    @ManyToOne
    @JoinColumn(name = "os_sid", nullable = false)
    private OthSet othset;

    /** 其他設定資訊單號 */
    @Column(name = "os_no", nullable = false, length = 21)
    private String osNo;

    /** 需求單主檔 */
    @ManyToOne
    @JoinColumn(name = "require_sid", nullable = false)
    private Require require;

    /** 需求單單號 */
    @Column(name = "require_no", nullable = false, length = 21)
    private String requireNo;

    /** 其他設定資訊主題 */
    @Column(name = "os_theme", nullable = false, length = 50)
    private String osTheme;

    /** 訊息類型 */
    @Convert(converter = OthSetAlertTypeConverter.class)
    @Column(name = "type", nullable = false)
    private OthSetAlertType type;

    /** 寄件者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "send_usr", nullable = false)
    private User sender;

    /** 寄件日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "send_dt", nullable = false)
    private Date sendDate;

    /** 收件者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "receiver", nullable = false)
    private User receiver;

    /** 來自於哪一則回覆sid */
    @OneToOne
    @JoinColumn(name = "os_reply_sid")
    private OthSetReply reply;

    /** 來自於哪一則回覆的回覆sid */
    @OneToOne
    @JoinColumn(name = "os_reply_and_reply_sid")
    private OthSetAlreadyReply alreadyReply;

    /** 此時狀態 */
    @Convert(converter = ReadRecordTypeConverter.class)
    @Column(name = "read_status", nullable = false)
    private ReadRecordType readStatus;

    /** 點擊時間 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "click_time")
    private Date clickTime;

}
