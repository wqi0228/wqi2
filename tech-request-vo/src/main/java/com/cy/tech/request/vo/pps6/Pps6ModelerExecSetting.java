/**
 * 
 */
package com.cy.tech.request.vo.pps6;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.cy.tech.request.vo.pps6.converter.Pps6ModelerExecTypeConverter;
import com.cy.tech.request.vo.pps6.enums.Pps6ModelerExecType;
import com.cy.work.common.vo.AbstractEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * PPS6 Modeler 執行設定資料檔
 * 
 * @author allen1214_wu
 */
@EqualsAndHashCode(callSuper = false, of = "sid")
@Table(name = "pps6_modeler_exec_setting")
@Entity
@Getter
@Setter
public class Pps6ModelerExecSetting extends AbstractEntity {

	/**
     * 
     */
    private static final long serialVersionUID = 357213008166215254L;

    /**
	 * SID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sid")
	private String sid;

	/**
	 * 流程代號
	 */
	@Column(name = "flow_sid")
	private Integer flowSid;

	/**
	 * 角色、帳號或變數ID
	 */
	@Column(name = "exec_id")
	private String execId;

	/**
	 * 是否為變數
	 */
	@Column(name = "is_variable")
	private boolean variable;

	/**
	 * 執行類型
	 */
	@Convert(converter = Pps6ModelerExecTypeConverter.class)
	@Column(name = "exec_type")
	private Pps6ModelerExecType execType;

	/**
	 * 名稱
	 */
	@Column(name = "name")
	private String name;

	/**
	 * 自定義說明
	 */
	@Column(name = "user_descr")
	private String userDescr;

	/**
	 * 使用節點名稱
	 */
	@Column(name = "modeler_node_name")
	private String modelerNodeName;
}
