/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require.tros;

import com.cy.commons.enums.Activation;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 其他資訊設定附件 (若os_history_sid == null 代表為其他設定資訊的附件 ,若os_history_sid != null
 * 待表為其他設定資訊回覆或回覆的回覆的附件)
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "tr_os_attachment")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class TrOsAttachment implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7071187809846145196L;
    /** Sid */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "os_attachment_sid", length = 36)
    private String sid;
    /** 其他設定資訊Sid */
    @Column(name = "os_sid", nullable = false, length = 36)
    private String os_sid;
    /** 其他設定資訊單號 */
    @Column(name = "os_no", nullable = false, length = 21)
    private String os_no;
    /** 需求單Sid */
    @Column(name = "require_sid", nullable = false, length = 36)
    private String require_sid;
    /** 需求單單號 */
    @Column(name = "require_no", nullable = false, length = 21)
    private String require_no;
    /** historySid */
    @Column(name = "os_history_sid", nullable = true, length = 36)
    private String os_history_sid;
    /** 流程代碼 */
    @Column(name = "behavior", nullable = true, length = 45)
    private String behavior;
    /** 附件名稱 */
    @Column(name = "file_name", nullable = true, length = 255)
    private String file_name;
    /** 附件描述 */
    @Column(name = "description", nullable = true, length = 255)
    private String description;
    /** 附件上傳者部門 */
    @Column(name = "department", nullable = false)
    private Integer department;
    /** 狀態 */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.INACTIVE;
    /** 建立者 */
    @Column(name = "create_usr", nullable = false)
    private Integer create_usr;
    /** 建立日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date create_dt;
    /** 更新者 */
    @Column(name = "update_usr", nullable = false)
    private Integer update_usr;
    /** 更新日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt", nullable = false)
    private Date update_dt;

}
