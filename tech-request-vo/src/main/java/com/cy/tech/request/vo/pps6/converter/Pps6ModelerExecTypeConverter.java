package com.cy.tech.request.vo.pps6.converter;

import com.cy.tech.request.vo.pps6.enums.Pps6ModelerExecType;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 轉換器
 *
 * @author allen
 */
@Slf4j
@Converter
public class Pps6ModelerExecTypeConverter implements AttributeConverter<Pps6ModelerExecType, String> {

	/**
	 * to DB 欄位資料
	 */
	@Override
	public String convertToDatabaseColumn(Pps6ModelerExecType attribute) {
		if (attribute == null) {
			return "";
		}
		return attribute.name();
	}

	/**
	 * DB value to enum
	 */
	@Override
	public Pps6ModelerExecType convertToEntityAttribute(String dbData) {
		if (Strings.isNullOrEmpty(dbData)) {
			return null;
		}

		for (Pps6ModelerExecType eachEnum : Pps6ModelerExecType.values()) {
			if (eachEnum.name().equals(dbData)) {
				return eachEnum;
			}
		}

		log.error("無法解析列舉型態 （Pps6ModelerExecType）dbData:[{}]", dbData);
		return null;
	}

}
