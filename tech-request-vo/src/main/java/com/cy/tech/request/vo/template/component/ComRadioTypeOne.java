/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.template.component;

import com.cy.work.common.utils.WkStringUtils;
import com.cy.tech.request.vo.annotation.TemplateComMapParserDefaultValueAnnotation;
import com.cy.tech.request.vo.annotation.TemplateComMapParserInteractValueAnnotation;
import com.cy.tech.request.vo.annotation.TemplateComMapParserInteractValueAnnotations;
import com.cy.tech.request.vo.enums.ComType;
import com.cy.tech.request.vo.value.to.TemplateDefaultValueTo;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import com.cy.work.common.utils.WkJsonUtils;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Boolean Radio
 *
 * @author shaun
 */
@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(callSuper = false)
public class ComRadioTypeOne extends AbstractComBase implements ComSaba, ComBaseInteractive, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3462782270277787543L;

    /** 元件名稱陣列，於基礎欄位設定中顯示 */
    public List<String> displayComponents = Lists.newArrayList();

    /** 元件欄位名稱 */
    @JsonProperty("name")
    public String name;

    /** 元件Id */
    @JsonProperty("comId")
    public String comId;

    /** 元件type */
    @JsonProperty("type")
    public ComType type;

    /** 實作介面名稱 */
    @JsonProperty("interfaceName")
    public String interfaceName;

    /** 是否需要建立索引 */
    @JsonProperty("needCreateIndex")
    public Boolean needCreateIndex;

    /** 值 1 */
    @TemplateComMapParserDefaultValueAnnotation(key = ONE, valueName = "value01", valueClz = Boolean.class, desc = "單選按鈕預設值 - TRUE = 左 | FALSE=右 | 無預設值")
    @JsonProperty("value01")
    private Boolean value01;

    /** 值 2 radio button 左側描述 */
    @TemplateComMapParserDefaultValueAnnotation(key = TWO, valueName = "value02", valueClz = String.class, desc = "單選按鈕描述(左)")
    @JsonProperty("value02")
    private String value02;

    /** 值 3 radio button 右側描述 */
    @TemplateComMapParserDefaultValueAnnotation(key = THREE, valueName = "value03", valueClz = String.class, desc = "單選按鈕描述(右)")
    @JsonProperty("value03")
    private String value03;

    /** 值 4 radio group 後方描述 */
    @TemplateComMapParserDefaultValueAnnotation(key = FOUR, valueName = "value04", valueClz = String.class, desc = "按鈕群組描述標籤(最後面)")
    @JsonProperty("value04")
    private String value04;

    /** 值 5 radio group 按鈕群組下方輸入框值 */
    @TemplateComMapParserDefaultValueAnnotation(key = FIVE, valueName = "value05", valueClz = String.class, desc = "按鈕群組下方輸入框值")
    @JsonProperty("value05")
    private String value05;

    /** 值 6 radio group 按鈕群組下方輸入框內預設提示輸入 */
    @TemplateComMapParserDefaultValueAnnotation(key = SIX, valueName = "value06", valueClz = String.class, desc = "按鈕群組下方輸入框內預設提示輸入\n注意:有值才會顯示下方輸入框")
    @JsonProperty("value06")
    private String value06;

    @JsonProperty("isSabaComponent")
    private Boolean isSabaComponent;

    @Override
    public void setDefValue(FieldKeyMapping template) {
        name = template.getFieldName();
        comId = template.getComId();
        type = template.getFieldComponentType();
        interfaceName = ComBaseInteractive.class.getSimpleName();
        needCreateIndex = template.getRequiredIndex();
        isSabaComponent = template.getIsSabaComponent();
        this.displayComponents.add(ComBaseInteractive.class.getSimpleName());
        this.displayComponents.add(ComSaba.class.getSimpleName());

        TemplateDefaultValueTo dv = template.getDefaultValue();
        Map<Integer, Object> valueMap = dv.getValue();
        if (valueMap == null) {
            return;
        }
        if (valueMap.containsKey(ONE)) {
            value01 = (Boolean) valueMap.get(ONE);
        }
        if (valueMap.containsKey(TWO)) {
            value02 = (String) valueMap.get(TWO);
        }
        if (valueMap.containsKey(THREE)) {
            value03 = (String) valueMap.get(THREE);
        }
        if (valueMap.containsKey(FOUR)) {
            value04 = (String) valueMap.get(FOUR);
        }
        if (valueMap.containsKey(FIVE)) {
            value05 = (String) valueMap.get(FIVE);
        }
        if (valueMap.containsKey(SIX)) {
            value06 = (String) valueMap.get(SIX);
        }
    }

    /**
     * 當頁面選擇了是否時，觸發
     *
     * @param interactCom
     * @param interactComValue
     */
    @TemplateComMapParserInteractValueAnnotations({
        @TemplateComMapParserInteractValueAnnotation(key = 1, valueClz = String.class, valueName = "value03", desc = "當為元件左值時內容填塞至 對應元件(TEXT_TYPE_ONE) ，使用者輸入框右測的標籤值。"),
        @TemplateComMapParserInteractValueAnnotation(key = 2, valueClz = String.class, valueName = "value03", desc = "當為元件右值時內容填塞至 對應元件(TEXT_TYPE_ONE) ，使用者輸入框右測的標籤值。")
    })
    @Override
    public void change(ComBase interactCom, TemplateDefaultValueTo interactComValue) {
        if (interactCom == null || interactComValue == null || !(interactCom instanceof ComTextTypeOne)) {
            return;
        }
        ComTextTypeOne interactTarget = (ComTextTypeOne) interactCom;
        Map<Integer, Object> mapValue = interactComValue.getValue();
        if (mapValue == null) {
            interactTarget.setValue03("");
            return;
        }
        if (value01 && mapValue.containsKey(1)) {
            interactTarget.setValue03((String) mapValue.get(1));
            return;
        }
        if (!value01 && mapValue.containsKey(2)) {
            interactTarget.setValue03((String) mapValue.get(2));
            return;
        }
        interactTarget.setValue03("");
    }

    @Override
    public Boolean checkRequireValueIsEmpty() {
        //檢查使用者輸入欄位
        return this.value01 == null;
    }

    @Override
    public String getCompositionText(boolean showField, int maxFieldNameLen) {
        StringBuilder sb = new StringBuilder();
        if (showField) {
            sb.append(WkStringUtils.getInstance().padEndEmpty(name, maxFieldNameLen, "："));
        }
        if (value01) {
            sb.append(Strings.isNullOrEmpty(value02) ? "是" : value02).append(" ");
        } else {
            sb.append(Strings.isNullOrEmpty(value03) ? "否" : value03).append(" ");
        }
        sb.append(Strings.nullToEmpty(value04)).append(" \r\n");
        if (!Strings.isNullOrEmpty(value06)) {
            sb.append("\r\n");
            sb.append(WkStringUtils.getInstance().readLine(value05, "  "));
        }
        sb.append(" \r\n");
        return sb.toString();
    }

    @Override
    public String getIndexContent() {
        try {
            List<String> idxList = Lists.newArrayList();
            idxList.add(value01 == null ? "" : value01.toString());
            idxList.add(Strings.nullToEmpty(value05));
            return WkJsonUtils.getInstance().toJson(idxList);
        } catch (IOException ex) {
            log.error("parse value to json string fail...");
            log.error("fail value01 = " + value01);
            log.error("fail value05 = " + value05);
            log.error("fail error " + ex.getMessage(), ex);
            return value01 == null ? "" + value02 : value01.toString() + " " + value05;
        }
    }

    @Override
    public String transferToPmisValue() {
        String result = "";
        if(value01 != null) {
            if(value01) {
                result = StringUtils.isNotBlank(value02) ? value02 : "是";
            } else {
                result = StringUtils.isNotBlank(value03) ? value03 : "否";
            }
        }
        
        return result;
    }

}
