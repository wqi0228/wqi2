package com.cy.tech.request.vo.require;

import com.cy.work.common.vo.AbstractEntityVO;
import java.io.Serializable;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 分派單位歷史檔 VO
 * 
 * @author allen1214_wu
 */
@NoArgsConstructor
@EqualsAndHashCode(of = { "sid" }, callSuper = false)
public class AssignSendInfoHistoryVO extends AbstractEntityVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5975115222515233853L;
    /**
     * @param assignSendInfoHistory
     */
    public AssignSendInfoHistoryVO(AssignSendInfoHistory assignSendInfoHistory) {

        // 複製
        BeanUtils.copyProperties(assignSendInfoHistory, this);

    }

    /**
     * SID
     */
    @Id
    @Getter
    @Setter
    private Long sid;

    /**
     * 需求單 SID
     */
    @Getter
    @Setter
    private String requireSid;

    /**
     * 執行者
     */
    @Getter
    @Setter
    private String show_creater;

    /**
     * 此次加派部門
     */
    @Getter
    @Setter
    private String addAssignDeps;
    @Getter
    @Setter
    private String show_addAssignDeps;
    @Getter
    @Setter
    private String showTreeStyle_addAssignDeps;

    /**
     * 此次減派部門
     */
    @Getter
    @Setter
    private String deleteAssignDeps;
    @Getter
    @Setter
    private String show_deleteAssignDeps;
    @Getter
    @Setter
    private String showTreeStyle_deleteAssignDeps;

    /**
     * 此次刪除部門
     */
    @Getter
    @Setter
    private String addSendDeps;
    @Getter
    @Setter
    private String show_addSendDeps;
    @Getter
    @Setter
    private String showTreeStyle_addSendDeps;

    /**
     * 此次減派部門
     */
    @Getter
    @Setter
    private String deleteSendDeps;
    @Getter
    @Setter
    private String show_deleteSendDeps;
    @Getter
    @Setter
    private String showTreeStyle_deleteSendDeps;
}
