package com.cy.tech.request.vo.category;

import com.cy.commons.enums.Activation;
import com.cy.commons.interfaces.BaseEntity;
import com.cy.commons.vo.User;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.work.common.vo.converter.UrgencyTypeConverter;
import com.cy.work.common.enums.UrgencyType;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import org.hibernate.annotations.GenericGenerator;

/**
 * 中類
 *
 * @author shaun
 */
@Data
@ToString(exclude = {"children"})
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_basic_data_middle_category")
public class BasicDataMiddleCategory implements Serializable, BaseEntity<String> {

    /**
     * 
     */
    private static final long serialVersionUID = -6184309258925112630L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "basic_data_middle_category_sid", length = 36)
    private String sid;

    @Column(name = "middle_category_id", unique = true, nullable = false, length = 8)
    private String id;

    @Column(name = "middle_category_name", unique = true, nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "parent_big_category", nullable = false)
    private BasicDataBigCategory parentBigCategory;

    @Convert(converter = UrgencyTypeConverter.class)
    @Column(name = "urgency", nullable = false)
    private UrgencyType urgency = UrgencyType.GENERAL;

    @Column(name = "single_upload_limited", nullable = false, length = 11)
    private Integer singleUploadLimited = 10;

    @Column(name = "prototype_upload_limited", nullable = false, length = 11)
    private Integer prototypeUploadLimited = 10;

    @Column(name = "note", length = 255)
    private String note;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parentMiddleCategory")
    @OrderBy("id ASC")
    private List<BasicDataSmallCategory> children = Lists.newArrayList();

    @Column(name = "seq")
    private Integer seq;

    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    @Convert(converter = SidUserConverter.class)
    @Column(name = "create_usr", nullable = false)
    private User createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    @Convert(converter = SidUserConverter.class)
    @Column(name = "update_usr", nullable = false)
    private User updatedUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

    /** 權限對應 如果有多個角色字串以 '，' 分隔 */
    @Column(name = "permission_role")
    private String permissionRole;
}
