/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require;

import com.cy.tech.request.vo.enums.ForwardType;
import com.cy.work.common.vo.AbstractEntityVO;
import java.io.Serializable;
import java.util.Date;

import org.springframework.beans.BeanUtils;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 收件夾 VO for 轉寄記錄
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = { "sid" }, callSuper = false)
public class TrAlertInboxHisVO extends AbstractEntityVO implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 2063573967496256340L;

    /**
     * 建構子
     * @param trAlertInbox
     */
    public TrAlertInboxHisVO(TrAlertInbox trAlertInbox) {
        //複製
        BeanUtils.copyProperties(trAlertInbox, this);
    }


    /** sid */
    private String sid;

    /** 轉寄類型 */
    private ForwardType forwardType;

    /** 寄件人 */
    private Integer sender;
    /** 寄件者顯示名稱 */
    private String senderShowName;

    /** 收件人 */
    private Integer receive;
    /** 收件者顯示名稱 */
    private String receiveShowName;
    /** 收件部門 */
    private Integer receiveDep;
    /** 收件部門顯示名稱 */
    private String receiveDepShowName;

    /** 建立日期 */
    private Date createdDate;
    /** 寄件時間 */
    private String createdDateShowName;

    /** 狀態說明 */
    private String forwardStatus;

}
