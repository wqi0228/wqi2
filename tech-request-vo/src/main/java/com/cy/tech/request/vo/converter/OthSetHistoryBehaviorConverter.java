/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import com.cy.tech.request.vo.enums.OthSetHistoryBehavior;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 其它設定資訊狀態
 *
 * @author shaun
 */
@Slf4j
@Converter
public class OthSetHistoryBehaviorConverter implements AttributeConverter<OthSetHistoryBehavior, String> {

    @Override
    public String convertToDatabaseColumn(OthSetHistoryBehavior attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public OthSetHistoryBehavior convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return OthSetHistoryBehavior.valueOf(dbData);
        } catch (Exception e) {
            log.error("OthSetHistoryBehavior Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }

}
