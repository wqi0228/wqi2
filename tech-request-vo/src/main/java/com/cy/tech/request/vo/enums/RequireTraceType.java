/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import lombok.Getter;

/**
 * 追蹤類型
 *
 * @author shaun
 */
public enum RequireTraceType {

	/** 0 使用追蹤功能輸入的資訊 */
	USER_TRACE_FUN_INPUT_INFO("使用追蹤功能輸入的資訊"),
	/** 1 檢查確認 */
	CONFIRMED_CHECK("檢查確認"),
	/** 2 退件通知 */
	ROLL_BACK_NOTIFY("退件通知"),
	/** 3 異動緊急度 */
	MODIFY_URGENCY("異動緊急度"),
	/** 4 異動分派/通知單位 */
	REQUIRE_ASSIGN("異動分派/通知單位"),
	/** 5 需求單作廢 */
	REQUIRE_DOC_INVALID("需求單作廢"),
	/** 6 需求暫緩 */
	REQUIRE_SUSPENDED("需求暫緩"),
	/** 7 需求成立 */
	REQUIRE_ESTABLISH("需求成立"),
	/** 8 功能符合需求 */
	REQUIRE_FUNCTION_MATCH("功能符合需求"),
	/** 9 原型確認重做 */
	PROTOTYPE_CONFIRM_REDO("重新實作"),
	/** 10 原型確認審核作廢 */
	PROTOTYPE_CONFIRM_VERIFY_INVAILD("原型確認審核作廢"),
	/** 11 分派資訊移除 */
	ASSIGN_INFO_REMOVE("分派資訊移除"),
	/** 12 通知資訊移除 */
	NOTICE_INFO_REMOVE("通知資訊移除"),
	/** 13 需求資訊補充 */
	REQUIRE_INFO_MEMO("需求資訊回覆補充"),
	/** 14 待閱原因 */
	WAIT_READ_REASON("待閱原因"),
	/** 15 送測審核作廢 */
	SEND_TEST_SIGN_INVAILD("送測審核作廢"),
	/** 16 結案說明 */
	CLOSE_MEMO("結案說明"),
	/** 17 需求完成 */
	REQUIRE_FINISH("需求完成"),
	/** 18 需求完成資訊補充 */
	REQUIRE_FINISH_MEMO("需求完成資訊補充"),
	/** 19 取消需求完成 */
	CANCEL_REQUIRE_FINISH("取消需求完成"),
	/** 20 需求暫緩資訊說明 */
	REQUIRE_SUSPENDED_INFO("需求暫緩資訊說明"),
	/** 21 修改期望完成日 */
	MODIFY_HOPE_DT("修改期望完成日"),
	/** 22 需求暫緩失效 */
	REQUIRE_SUSPENDED_INVALID("需求暫緩失效"),
	/** 23 修改提出客戶 */
	MODIFY_AUTHOR("修改提出客戶"),
	/** 24 修改廳主 */
	MODIFY_CUSTMOER("修改廳主"),
	/** 25 修改廳主與提出客戶 */
	MODIFY_CUSTOMER_AUTHOR("修改廳主與提出客戶"),
	/** 26 刪除附加檔案 */
	DELETE_ATTACHMENT("刪除附加檔案"),
	/** 27 異動統整人員 */
	MODEFY_INTEGRATION_INFO("異動統整人員"),
	/** 28 轉入資訊 */
	TRANSFER_FROM_TECH("轉入資訊"),
	/** 29 反需求完成 */
	UNREQUIRE_FINISH("反需求完成"),
	/** 30 NCS確認完成 */
	NCS_CHECK_OK("NCS確認完成"),
	/** 31 無法自動結案說明 */
	CANNOT_AUTO_CLOSED_REASON("無法自動結案說明"),
	/** 32 自動結案 */
	AUTO_CLOSED("自動結案"),
	/** 33 轉PMIS */
	TRANS_TO_PMIS("轉PMIS"),
	/** 34 因需求確認強制結子單 */
	FORCE_CLOSE_SUBCASE_BY_UNIT_CONFIRM("因需求確認強制結子單"),
	/** 35 異動系統別 (檢查項目) */
	CHANGE_CHECK_ITEMS("異動系統別"),
	/** 36 異動主責單位資訊 */
	CHANGE_IN_CHARGE("異動主責單位資訊"),
	/** 37 反結案 */
	UNREQUIRE_CLOSE("反結案"),
	/** 38 維護管理系統(MMS)同步 */
	SYNC_MMS("維護管理系統(MMS)同步"),
	;

	@Getter
	private String label;

	private RequireTraceType(String label) {
		this.label = label;
	}
}
