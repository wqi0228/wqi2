/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import lombok.extern.slf4j.Slf4j;

/**
 * ON程式一覽表-自訂搜尋元件底層列舉
 *
 * @author brain0925_liao
 */
@Slf4j
public enum Search16QueryColumn implements SearchReportCustomEnum {
    /** ON程式一覽表-key */
    Search16Query("ON程式一覽表"),
    /** 需求類別 */
    DemandType("需求類別"),
    /** 需求製作進度 */
    DemandProcess("需求製作進度"),
    /** 填單單位 */
    Department("填單單位"),
    /** 通知單位 */
    NotifyDepartment("通知單位"),
    /** 是否閱讀 */
    ReadStatus("是否閱讀"),
    /** 填單人員 */
    DemandPerson("填單人員"),
    /** 待閱原因 */
    WaitReadReson("待閱原因"),
    /** On程式狀態 */
    OnpgStatus("On程式狀態"),
    /** 類別組合 */
    CategoryCombo("類別組合"),
    /** 立單區間-索引 */
    DateIndex("立單區間-索引"),
    /** 立單區間-起始 */
    StartDate("立單區間-起始"),
    /** 立單區間-結束 */
    EndDate("立單區間-結束"),
    /** 模糊搜尋 */
    SearchText("模糊搜尋"),
    /** 區間選擇 */
    DateTimeType("區間選擇"),
    CREATE_DATE("create_dt"),
    UPDATE_DATE("update_dt"),
    ESTABLISH_DATE("onpg_estimate_dt");

    /** 欄位名稱 */
    private final String val;

    Search16QueryColumn(String val) {
        this.val = val;
    }

    @Override
    public String getVal() {
        return val;
    }

    @Override
    public String getUrl() {
        return "Search16";
    }

    @Override
    public String getKey() {
        return this.name();
    }

    @Override
    public SearchReportCustomEnum getSearchReportCustomEnumByName(String name) {
        try {
            return Search16QueryColumn.valueOf(name);
        } catch (Exception e) {
            log.error("getSearchReportCustomEnumByName ERROR", e);
        }
        return null;
    }
}
