/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.constants;

/**
 * 需求單用到的角色權限
 *
 * @author Shaun
 */
public class ReqPermission {

	/** 需求單系統管理員 */
	public static final String ROLE_SYS_ADMIN = "需求單系統管理員:*";

	/** 技術需求單需求單位設定管理員 */
	public static final String REQ_UNIT_SET_MGR = "技術需求單需求單位設定管理員:*";
	/** 技術需求單系統客服設定管理員 */
	// REQ-1487 【GM後台維護】新增『GM管理者設定』 (移除此角色功能)
	// public static final String REQ_SYS_CUS_SERVICE_SET_MGR = "技術需求單系統客服設定管理員:*";
	/** 分類確認檢查員 */
	// REQ-1486 【GM後台維護】新增『GM設定』 (移除此角色功能)
	// public static final String CATE_CONFIRM_CHECK = "分類確認檢查員:*";
	/** 分派執行操作者 REQ-1601 - 移除管控 */
	// public static final String ASSIGN_EXEC = "分派執行操作者:*";
	/** FB分派執行操作者 */
	public static final String FB_ASSIGN_EXEC = "FB分派執行操作者:*";
	/** 原型確認執行者  REQ-1601 - 移除管控 */
	//public static final String PT_EXEC = "原型確認執行者:*";
	/** 送測資訊填單者 */
	public static final String ST_INFO_CREATE = "送測資訊填單者:*";
	/** 送測管理員 */
	public static final String ST_MGR = "送測管理員:*";
	/** 送測異動報表閱覽者 */
	public static final String ST_HISTORY_VIEWER = "送測異動報表閱覽者:*";
	/** ON程式資訊填單者 REQ-1601 - 移除管控 */
	/** ON程式管理員 */
	public static final String OP_MGR = "ON程式管理員:*";
	/** 業務單位主管 */
	public static final String BUSINESS_UNIT_MGR = "業務單位主管:*";
	/** 系統服務處GM角色 */
	// REQ-1486 GM後台維護】新增『GM設定』 (移除此角色功能)
	// public static final String SYS_SERVICE_DIVISION_GM = "系統服務處GM:*";
	/** 客戶資料管理者 */
	public static final String CUSTOMER_DATA_MGR = "客戶資料管理者:*";
	/** PMIS管理者 */
	public static final String ROLE_PMIS_MGR = "PMIS管理者:*";
	/** 需求單專案管理員 */
	public static final String ROLE_PROJECT_MGR = "需求單專案管理員:*";
	/** 通知人員設定使用者 */
	public static final String ROLE_NOTIFY_SETTING_USER = "通知人員設定使用者:*";
	/** QA主管 */
	public static final String ROLE_QA_MANAGER = "QA軟體測試部-主管:*";
	/** 審單人員(送測單) */
	public static final String ROLE_QA_REVIEW_MGR = "QA審單人員:*";

}
