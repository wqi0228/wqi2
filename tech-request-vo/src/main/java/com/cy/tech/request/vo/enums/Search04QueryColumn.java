/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import lombok.extern.slf4j.Slf4j;

/**
 * 需求單查詢-自訂搜尋元件底層列舉
 *
 * @author brain0925_liao
 */
@Slf4j
public enum Search04QueryColumn implements SearchReportCustomEnum {
    /** 退件資訊查詢-key */
    Search04Query("退件資訊查詢"),
    /** 需求類別 */
    DemandType("需求類別"),
    /** 需求單位 */
    Department("需求單位"),
    /** 需求製作進度 */
    DemandProcess("需求製作進度"),
    /** 退件原因 */
    RejectReason("退件原因"),
    /** 退件人員 */
    RejectPerson("退件人員"),
    /** 需求人員 */
    DemandPerson("需求人員"),
    /** 模糊搜尋 */
    SearchText("模糊搜尋"),
    /** 類別組合 */
    CategoryCombo("類別組合"),
    /** 立單區間-索引 */
    DateIndex("立單區間-索引");

    /** 欄位名稱 */
    private final String val;

    Search04QueryColumn(String val) {
        this.val = val;
    }

    @Override
    public String getVal() {
        return val;
    }

    @Override
    public String getUrl() {
        return "Search04";
    }

    @Override
    public String getKey() {
        return this.name();
    }

    @Override
    public SearchReportCustomEnum getSearchReportCustomEnumByName(String name) {
        try {
            return Search04QueryColumn.valueOf(name);
        } catch (Exception e) {
            log.error("getSearchReportCustomEnumByName ERROR", e);
        }
        return null;
    }
}
