/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.onpg.converter;

import com.cy.tech.request.vo.onpg.enums.WorkOnpgCheckRecordReplyType;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * ON程式檢查記錄回覆類型
 *
 * @author shaun
 */
@Slf4j
@Converter
public class WorkOnpgCheckRecordReplyTypeConverter implements AttributeConverter<WorkOnpgCheckRecordReplyType, String> {

    @Override
    public String convertToDatabaseColumn(WorkOnpgCheckRecordReplyType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public WorkOnpgCheckRecordReplyType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return WorkOnpgCheckRecordReplyType.valueOf(dbData);
        } catch (Exception e) {
            log.error("WorkOnpgCheckRecordReplyType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
