/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums.tros;

/**
 * 其它設定資訊 - Query語法 UNION ALL Type
 *
 * @author brain0925_liao
 */
public enum TrOsType {
    /** 其它設定資訊-回覆類型 */
    tr_os_reply,
    /** 其它設定資訊-回覆的回覆類型 */
    tr_os_reply_and_reply,
    /** 其它設定資訊-歷程 */
    tr_os_history,
    /** 其它設定資訊-歷程附件 */
    tr_os_attachment
}
