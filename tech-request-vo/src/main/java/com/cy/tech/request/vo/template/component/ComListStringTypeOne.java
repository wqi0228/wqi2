/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.template.component;

import com.cy.work.common.utils.WkStringUtils;
import com.cy.tech.request.vo.annotation.TemplateComMapParserDefaultValueAnnotation;
import com.cy.tech.request.vo.enums.ComType;
import com.cy.tech.request.vo.value.to.TemplateDefaultValueTo;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import com.cy.work.common.utils.WkJsonUtils;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * input text
 *
 * @author shaun
 */
@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ComListStringTypeOne implements ComBase, ComBaseList, Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -3591305498863342609L;

    /** 元件名稱陣列，於基礎欄位設定中顯示 */
	public List<String> displayComponents = Lists.newArrayList();

	/** 元件欄位名稱 */
	@JsonProperty("name")
	public String name;

	/** 元件Id */
	@JsonProperty("comId")
	public String comId;

	/** 元件type */
	@JsonProperty("type")
	public ComType type;

	/** 實作介面名稱 */
	@JsonProperty("interfaceName")
	public String interfaceName;

	/** 是否需要建立索引 */
	@JsonProperty("needCreateIndex")
	public Boolean needCreateIndex;

	/** 值 1 所有值 */
	@JsonProperty("value01")
	private List<String> value01;

	/** 值 2 字串輸入暫存值 */
	@TemplateComMapParserDefaultValueAnnotation(key = THREE, valueName = "value02", valueClz = String.class, desc = "輸入欄位暫存值。")
	@JsonProperty("value02")
	private String value02;

	/** 值 3 筆數 */
	@JsonProperty("value03")
	private Integer value03;

	/** 值 4 筆數後註記 */
	@TemplateComMapParserDefaultValueAnnotation(key = ONE, valueName = "value04", valueClz = String.class, desc = "數量後方的灰色標籤註記。")
	@JsonProperty("value04")
	private String value04;

	/** 值 5 輸入暫存值後註記 */
	@TemplateComMapParserDefaultValueAnnotation(key = TWO, valueName = "value05", valueClz = String.class, desc = "輸入框內的預設提示輸入。")
	@JsonProperty("value05")
	private String value05;

	@Override
	public void setDefValue(FieldKeyMapping template) {
		name = template.getFieldName();
		comId = template.getComId();
		type = template.getFieldComponentType();
		interfaceName = ComBase.class.getSimpleName();
		needCreateIndex = template.getRequiredIndex();
		value03 = 0;

		TemplateDefaultValueTo dv = template.getDefaultValue();
		Map<Integer, Object> valueMap = dv.getValue();
		if (valueMap == null) {
			return;
		}
		if (valueMap.containsKey(ONE)) {
			value04 = (String) valueMap.get(ONE);
		}
		if (valueMap.containsKey(TWO)) {
			value05 = (String) valueMap.get(TWO);
		}
		if (valueMap.containsKey(THREE)) {
			value02 = (String) valueMap.get(THREE);
		}
	}

	@Override
	public Boolean checkRequireValueIsEmpty() {
		// 存檔前檢查空值時，將未按儲存的值進行保存
		this.add();
		// 檢查使用者輸入欄位
		if (WkStringUtils.isEmpty(value01)) {
			return Boolean.TRUE;
		}

		for (String each : value01) {
			if (WkStringUtils.isEmpty(each)) {
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	@Override
	public void add(ComWebLayerNotify webNotify) {
		String errMsg = this.add();
		if (!Strings.isNullOrEmpty(errMsg)) {
			webNotify.notify(errMsg);
		}
	}

	private String add() {
		if (WkStringUtils.isEmpty(this.value02)) {
			return "請輸入資訊。";
		}
		if (this.value01 == null) {
			this.value01 = Lists.newArrayList();
		}
		if (this.value01.contains(this.value02)) {
			return "已包含重覆資訊！";
		}
		this.value01.add(this.value02);
		this.value03 = this.value01.size();
		this.value02 = null;
		return "";
	}

	@Override
	public void sub(int index) {
		if (index < 0 || index > value01.size()) {
			return;
		}
		String removeStr = this.value01.get(index);
		if (this.value01.contains(removeStr)) {
			this.value01.remove(removeStr);
			value03 = value01.size();
		}
	}

	@Override
	public String getCompositionText(boolean showField, int maxFieldNameLen) {
		StringBuilder sb = new StringBuilder();
		if (showField) {
			sb.append(WkStringUtils.getInstance().padEndEmpty(name, maxFieldNameLen, "：")).append(" ");
		}
		sb.append(value03).append(" 筆 ").append("\r\n");
		sb.append("\r\n");
		for (String each : value01) {
			sb.append("  ").append(Strings.padStart(String.valueOf(value01.indexOf(each) + 1), 3, ' ')).append(". ").append(each).append(" \r\n\r\n");
		}
		return sb.toString();
	}

	@Override
	public String getIndexContent() {
		try {
			if (value01 == null) {
				value01 = Lists.newArrayList();
			}
			return WkJsonUtils.getInstance().toJson(Lists.newArrayList(value01));
		} catch (IOException ex) {
			log.error("parse value to json string fail...");
			log.error("fail value = " + value01, ex);
			return value01.toString();
		}
	}
}
