package com.cy.tech.request.vo.template.component;



import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonProperty;

import com.cy.tech.request.vo.annotation.TemplateComMapParserDefaultValueAnnotation;

import lombok.Getter;
import lombok.Setter;

/**
 * @author aken_kao
 * 共用父代元件, 目前僅使用PMIS 
 */
public abstract class AbstractComBase implements ComBase, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8961318077235404563L;

    public enum PMIS{
        THEME,
        REASON,
        DESC;
    }
    
    @Getter
    @Setter
    @TemplateComMapParserDefaultValueAnnotation(key = PMIS_IDX, valueName = "PMIS", valueClz = PMIS.class, desc = "選擇傳入PMIS的欄位類型")
    @JsonProperty("pmis")
    protected PMIS pmis;
    
    /**
     * 轉給PMIS的值
     * @return string
     */
    public abstract String transferToPmisValue();
    
}
