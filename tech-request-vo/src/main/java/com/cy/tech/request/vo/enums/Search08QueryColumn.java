/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import lombok.extern.slf4j.Slf4j;

/**
 * 需求單查詢-自訂搜尋元件底層列舉
 *
 * @author brain0925_liao
 */
@Slf4j
public enum Search08QueryColumn implements SearchReportCustomEnum {
    /** 原型確認一覽表-key */
    Search08Query("原型確認一覽表"),
    /** 需求類別 */
    DemandType("需求類別"),
    /** 需求製作進度 */
    DemandProcess("需求製作進度"),
    /** 是否閱讀 */
    ReadStatus("是否閱讀"),
    /** 填單人員 */
    DemandPerson("填單人員"),
    /** 模糊搜尋 */
    SearchText("模糊搜尋"),
    /** 類別組合 */
    CategoryCombo("類別組合"),
    /** 待閱原因 */
    WaitReadReason("待閱原因"),
    /** 原型確認狀態 */
    CheckStatus("原型確認狀態"),
    /** 立單區間-索引 */
    DateIndex("立單區間-索引"),
    /** 版次顯示 */
    VersonView("版次顯示"),
    /** 排序方式 */
    SortType("排序方式");

    /** 欄位名稱 */
    private final String val;

    Search08QueryColumn(String val) {
        this.val = val;
    }

    @Override
    public String getVal() {
        return val;
    }

    @Override
    public String getUrl() {
        return "Search08";
    }

    @Override
    public String getKey() {
        return this.name();
    }

    @Override
    public SearchReportCustomEnum getSearchReportCustomEnumByName(String name) {
        try {
            return Search08QueryColumn.valueOf(name);
        } catch (Exception e) {
            log.error("getSearchReportCustomEnumByName ERROR", e);
        }
        return null;
    }
}
