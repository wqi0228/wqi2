/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.onpg.converter;

import com.cy.tech.request.vo.onpg.enums.WorkOnpgAttachmentBehavior;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * on程式上傳行為
 *
 * @author shaun
 */
@Slf4j
@Converter
public class WorkOnpgAttachmentBehaviorConverter implements AttributeConverter<WorkOnpgAttachmentBehavior, String> {

    @Override
    public String convertToDatabaseColumn(WorkOnpgAttachmentBehavior attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public WorkOnpgAttachmentBehavior convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return WorkOnpgAttachmentBehavior.valueOf(dbData);
        } catch (Exception e) {
            log.error("WorkOnpgAttachmentBehavior Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
