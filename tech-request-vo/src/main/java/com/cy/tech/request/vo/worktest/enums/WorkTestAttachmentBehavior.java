/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.worktest.enums;

import lombok.Getter;

/**
 * 附加檔案 行為
 *
 * @author shaun
 */
public enum WorkTestAttachmentBehavior {

    SONGCE("送測"),
    REPLY("測試回覆"),
    REPLY_AND_REPLY("測試回覆的回覆"),
    ROLL_BACK("退測"),
    RETEST("重測"),
    QA_TEST_COMPLETE("QA轉測試報告"),
    TEST_COMPLETE("測試完成"),
    CANCEL_TEST("取消測試");

    @Getter
    private String value;
    
    private WorkTestAttachmentBehavior(String value) {
        this.value = value;
    }
}
