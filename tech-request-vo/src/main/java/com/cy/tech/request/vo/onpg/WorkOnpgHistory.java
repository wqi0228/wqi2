/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.onpg;

import com.cy.commons.enums.Activation;
import com.cy.commons.interfaces.BaseEntity;
import com.cy.commons.vo.User;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.tech.request.vo.onpg.converter.WorkOnpgHistoryBehaviorConverter;
import com.cy.tech.request.vo.onpg.converter.WorkOnpgStatusConverter;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgHistoryBehavior;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.common.vo.converter.WorkSourceTypeConverter;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

/**
 * ON程式的歷程記錄
 *
 * @author shaun
 */
@Data
@NoArgsConstructor
@ToString(exclude = {"attachments"})
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "work_onpg_history")
public class WorkOnpgHistory implements Serializable, BaseEntity<String> {

    /**
     * 
     */
    private static final long serialVersionUID = -532344537355898675L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "onpg_history_sid", length = 36)
    private String sid;

    /** on程式單sid */
    @ManyToOne
    @JoinColumn(name = "onpg_sid")
    private WorkOnpg onpg;

    /** on程式單號 */
    @Column(name = "onpg_no", length = 21)
    private String onpgNo;

    /** on程式來源 */
    @Convert(converter = WorkSourceTypeConverter.class)
    @Column(name = "onpg_source_type")
    private WorkSourceType sourceType;

    /** on程式來源 sid */
    @Column(name = "onpg_source_sid", length = 36)
    private String sourceSid;

    /** on程式來源 單號 */
    @Column(name = "onpg_source_no", length = 21)
    private String sourceNo;

    /** 歷史資訊行為狀態 */
    @Convert(converter = WorkOnpgHistoryBehaviorConverter.class)
    @Column(name = "behavior", nullable = false)
    private WorkOnpgHistoryBehavior behavior;

    /** 此時的on程式狀態 */
    @Convert(converter = WorkOnpgStatusConverter.class)
    @Column(name = "behavior_status", nullable = false)
    private WorkOnpgStatus behaviorStatus;

    /** 檢查記錄sid */
    @OneToOne
    @JoinColumn(name = "onpg_check_record_sid")
    private WorkOnpgCheckRecord checkRecord;

    /** 檢查記錄的回覆資訊sid */
    @OneToOne
    @JoinColumn(name = "onpg_check_record_reply_sid")
    private WorkOnpgCheckRecordReply checkRecordReply;

    /** 檢查註記 */
    @OneToOne
    @JoinColumn(name = "onpg_check_memo_sid")
    private WorkOnpgCheckMemo checkMemo;

    /** 顯示於異動明細表上 */
    @Column(name = "visiable", nullable = false)
    private Boolean visiable = Boolean.FALSE;

    /** 原因 */
    @Column(name = "reason")
    private String reason;

    /** 原因 含css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "reason_css")
    private String reasonCss;

    /** 狀態 */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    /** 建立者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "create_usr", nullable = false)
    private User createdUser;

    /** 建立日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    /** 異動者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "update_usr")
    private User updatedUser;

    /** 異動日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

    /** 附加檔案 */
    @OneToMany(mappedBy = "onpg", cascade = CascadeType.PERSIST)
    @Where(clause = "status = 0 AND onpg_history_sid IS NULL")
    @OrderBy(value = "createdDate DESC")
    private List<WorkOnpgAttachment> attachments = Lists.newArrayList();

}
