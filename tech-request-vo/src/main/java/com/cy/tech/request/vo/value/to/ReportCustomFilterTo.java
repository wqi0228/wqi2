/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.value.to;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author brain0925_liao
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReportCustomFilterTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -9119333176971690694L;
    @JsonProperty(value = "reportCustomFilterDetailTos")
    private List<ReportCustomFilterDetailTo> reportCustomFilterDetailTos = Lists.newArrayList();
}
