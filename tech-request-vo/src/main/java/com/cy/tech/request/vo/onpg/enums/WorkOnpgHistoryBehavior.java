/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.onpg.enums;

import lombok.Getter;

/**
 * 歷程行為
 *
 * @author shaun
 */
public enum WorkOnpgHistoryBehavior {

    CHECK_RECORD("檢查記錄"),
    CHECK_RECORD_REPLY("檢查記錄的回覆"),
    CHECK_MEMO("檢查註記"),
    CANCEL_ONPG("取消on程式"),
    FINISH_CHECK("檢查完成"),
    MODIFY_ESTIMATE_DT("更改預計完成日"),
    MODIFY_MEMO_INFO("更改備註資訊"),
    GENERAL_REPLY("一般回覆"),
    GM_REPLY("GM回覆"),
    QA_REPLY("QA回覆"),
    /**
     * 因強制需求完成或強制結案而關閉
     */
    FORCE_CLOSE("強制關閉"),

    // ========================
    // 以下為MMS同步狀態
    // ========================
    MMS_UPDATE("因MMS同步，異動資料"),
    MMS_CHANGE_STATUS("因MMS同步，改變單據狀態"),
    MMS_CANCEL("因MMS同步，取消ON程式"),
    ;

    @Getter
    private String label;

    private WorkOnpgHistoryBehavior(String label) {
        this.label = label;
    }
}
