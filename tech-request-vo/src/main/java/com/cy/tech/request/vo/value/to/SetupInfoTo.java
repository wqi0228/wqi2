/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.value.to;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 群組明細資訊
 *
 * @author shaun
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SetupInfoTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -784143279801467130L;

    @JsonProperty(value = "department")
    private List<String> department = Lists.newArrayList();

    @JsonProperty(value = "users")
    private List<String> users = Lists.newArrayList();
}
