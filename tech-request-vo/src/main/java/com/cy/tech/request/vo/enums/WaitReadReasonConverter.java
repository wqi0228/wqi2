/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * 待閱原因轉換器
 *
 * @author kasim
 */
@Slf4j
@Converter
public class WaitReadReasonConverter implements AttributeConverter<WaitReadReasonType, String> {

    @Override
    public String convertToDatabaseColumn(WaitReadReasonType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public WaitReadReasonType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return WaitReadReasonType.valueOf(dbData);
        } catch (Exception e) {
            log.error("WaitReadReasonType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
