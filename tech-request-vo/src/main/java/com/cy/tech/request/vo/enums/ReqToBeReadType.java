/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import java.util.List;
import java.util.stream.Collectors;

import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import lombok.Getter;

/**
 * 需求單待閱原因
 *
 * @author shaun
 */
public enum ReqToBeReadType {
    /** 無待閱原因 */
    NO_TO_BE_READ("無待閱原因"),
    /** GM審核中 */
    GM_REVIEW("GM審核中"),
    /** 已分派 */
    ASSIGNED("已分派"),
    /** 待分派 */
    UNASSIGNED("待分派"),
    /** GM審核-退件 */
    GM_REVIEW_BOUNCED("GM審核-退件"),
    /** 需求實作完畢 */
    COMPLETED("需求實作完畢"),
    /** 作廢 */
    INVALID("作廢"),
    /** 結案 */
    CLOSED("結案"),
    /** 需求暫緩 */
    SUSPENDED("需求暫緩"),
    /** 已完成 > 進行中 */
    COMPLETED_TO_PROCESS("已完成 > 進行中"),
    /** 變更期望完成日 */
    CHANGE_HOPE_DATE("變更期望完成日"),
    /** 新增需求資訊回覆補充 */
    NEW_REQ_INFO_MEMO("新增需求資訊回覆補充"),
    /** 新增需求完成補充 */
    NEW_COMPLETED_INFO_MEMO("新增需求完成補充"),
    /** 異動分派單位 */
    MODIFY_ASSIGNED_UNIT("異動分派單位"),
    /** 增加附加檔案 */
    ADD_REQ_ATTACHMENT("增加附加檔案"),
    /** 異動需求資訊回覆補充 */
    UPDATE_REQ_INFO_MEMO("異動需求資訊回覆補充"),
    /** 刪除附加檔案 */
    DEL_REQ_ATTACHMENT("刪除附加檔案"),
    /** 修改附加檔案描述資訊內容 */
    UPDATE_REQ_ATTACHMENT("修改附加檔案描述資訊內容"),
    /** 結案 > 已完成 */
    CLOSE_TO_PROCESS("反結案"),
    ;

    @Getter
    private String label;

    private ReqToBeReadType(String label) {
        this.label = label;
    }

    /**
     * @param str 列舉 name 字串
     * @return RequireStatusType
     */
    public static ReqToBeReadType safeValueOf(String str) {
        if (WkStringUtils.notEmpty(str)) {
            for (ReqToBeReadType enumType : ReqToBeReadType.values()) {
                if (enumType.name().equals(str)) {
                    return enumType;
                }
            }
        }
        return null;
    }

    /**
     * 取得所有類型名稱
     * 
     * @return all ReqToBeReadType name
     */
    public static List<String> valueNames() {
        return Lists.newArrayList(ReqToBeReadType.values()).stream()
                .map(ReqToBeReadType::name)
                .collect(Collectors.toList());
    }

}
