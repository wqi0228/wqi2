package com.cy.tech.request.vo.setting.asdep;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.work.common.vo.AbstractEntityVO;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 設定:檢查確認預設分派/通知單位
 * 
 * @author allen1214_wu
 */
@EqualsAndHashCode(callSuper = false, of = "sid")
@NoArgsConstructor
public class SettingDefaultAssignSendDepHistoryVO extends AbstractEntityVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8149952408601456172L;
    /**
     * 建構子
     * 
     * @param smallCategorySid 小類SID
     * @param checkItemType    檢查項目
     * @param assignSendType   分派通知類別
     */
    public SettingDefaultAssignSendDepHistoryVO(
            String smallCategorySid,
            RequireCheckItemType checkItemType,
            AssignSendType assignSendType) {
        super();
        this.smallCategorySid = smallCategorySid;
        this.checkItemType = checkItemType;
        this.assignSendType = assignSendType;
    }

    /**
     * 建構子
     * 
     * @param entity SettingDefaultAssignSendDepHistory
     */
    public SettingDefaultAssignSendDepHistoryVO(SettingDefaultAssignSendDepHistory entity) {
        // ====================================
        // copy Properties
        // ====================================
        BeanUtils.copyProperties(entity, this);
    }

    /**
     * SID
     */
    @Getter
    @Setter
    private String sid;

    /**
     * 小類SID
     */
    @Getter
    @Setter
    private String smallCategorySid;

    /**
     * 檢查項目(對應系統別欄位)
     */
    @Getter
    @Setter
    private RequireCheckItemType checkItemType;

    /** 類型 分派 | 通知 */
    @Getter
    @Setter
    private AssignSendType assignSendType = AssignSendType.ASSIGN;

    /**
     * 此次異動新增的預設部門SID , 以『,』分隔
     */
    @Getter
    @Setter
    private List<Integer> addDepsInfo;

    /**
     * 此次異動移除的預設部門SID , 以『,』分隔
     */
    @Getter
    @Setter
    private List<Integer> removeDepsInfo;

    // ======================================
    // view 用
    // ======================================
    /**
     * 
     */
    @Getter
    @Setter
    private String userName;
    @Getter
    @Setter
    private String dateForDisplay;
    /**
     * 新增單位名稱
     */
    @Getter
    @Setter
    private String addDepsName;
    /**
     * 新增單位名稱 for tooltip
     */
    @Getter
    @Setter
    private String addDepsForTooltip;
    /**
     * 移除單位名稱
     */
    @Getter
    @Setter
    private String removeDepsName;
    /**
     * 移除單位名稱 for tooltip
     */
    @Getter
    @Setter
    private String removeDepsForTooltip;
}