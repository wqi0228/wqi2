/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.pt.enums;

/**
 * 附加檔案 行為
 *
 * @author shaun
 */
public enum PtAttachmentBehavior {

    /** 0 填寫原型確認單時鎖上傳的附加檔案 */
    SONGCE,
    /** 1 原型確認回覆 所上傳的附加檔案 */
    REPLY,
    /** 2 原型確認回覆的回覆 - 所上傳的附加檔案 */
    REPLY_AND_REPLY,
    /** 3 重新實作所上傳的附加檔案 */
    REDO,
    /** 4 功能符合需求所上傳的附加檔案 */
    FUNCTION_CONFORM,;
}
