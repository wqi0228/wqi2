/**
 * 
 */
package com.cy.tech.request.vo.log;

import lombok.Getter;

/**
 * MMS維護單同步記錄 動作類型
 * 
 * @author allen1214_wu
 */
public enum LogSyncMmsActionType {
    NONE("未動作", false, false),
    CREATE_REQ_AND_ONPG("開立需求單，並建立ON程式單", true, false),
    CREATE_ONPG("新增ON程式單", true, false),
    UPDATE_REQ_AND_ONPG("異動需求單和ON程式單", true, true),
    UPDATE_ONPG("異動ON程式單", true, true),
    UPDATE_NONE("要求資料異動，但資料未改變", true, false),
    CANCEL_ONPG_AND_CLOSE_REQ("取消ON程式並將需求單結案", true, false), //暫不實做
    CANCEL_ONPG("取消ON程式", true, false),
    CANCEL_NONE("要求取消ON程式，但資料未改變", true, false),
    CANCEL_DENY("要求取消ON程式，但狀態鎖定無法取消", false, false),
    VIRIFY_FAIL("資料檢核失敗", false, false),
    EXCEPTION("處理失敗", false, false),
    CALL_API_FAIL("CALL API 失敗", false, false);

    /**
     * 說明
     */
    @Getter
    private String descr;

    /**
     * 是否處理成功
     */
    @Getter
    private boolean success;

    /**
     * 為異動，且成功
     */
    @Getter
    private boolean updateSuccess;

    /**
     * @param descr
     */
    LogSyncMmsActionType(String descr, boolean success, boolean updateSuccess) {
        this.descr = descr;
        this.success = success;
        this.updateSuccess = updateSuccess;
    }
}
