/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require;

import com.cy.commons.enums.Activation;
import com.cy.commons.interfaces.BaseEntity;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.vo.converter.ForwardTypeConverter;
import com.cy.tech.request.vo.converter.InboxTypeConverter;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.work.common.vo.converter.SidOrgConverter;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.tech.request.vo.enums.ForwardType;
import com.cy.tech.request.vo.enums.InboxType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 收件夾
 *
 * @author shaun
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity(name = "tr_alert_inbox")
@Table(name = "tr_alert_inbox")
public class AlertInbox implements Serializable, BaseEntity<String> {

    /**
     * 
     */
    private static final long serialVersionUID = 5926379920432634244L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "alert_sid", length = 36)
    private String sid;

    /** 回覆來源訊息 */
    @OneToOne
    @JoinColumn(name = "reply_alert_sid")
    private AlertInbox replyAlertInbox;

    /** 需求單主檔 */
    @ManyToOne
    @JoinColumn(name = "require_sid", nullable = false)
    private Require require;

    /** 需求單單號 */
    @Column(name = "require_no", nullable = false, length = 21)
    private String requireNo;

    /** 轉寄類型 */
    @Convert(converter = ForwardTypeConverter.class)
    @Column(name = "forward_type", nullable = false)
    private ForwardType forwardType;

    /** 寄件人 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "sender", nullable = false)
    private User sender;

    /** 寄件部門 */
    @Convert(converter = SidOrgConverter.class)
    @Column(name = "sender_dep", nullable = false)
    private Org senderDep;

    /** 收件人 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "receive")
    private User receive;

    /** 收件部門 */
    @Convert(converter = SidOrgConverter.class)
    @Column(name = "receive_dep")
    private Org receiveDep;

    /** 留言訊息 */
    @Column(name = "message")
    private String message;

    /** 留言訊息 - 含css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "message_css")
    private String messageCss;

    /** 收件夾類型 */
    @Convert(converter = InboxTypeConverter.class)
    @Column(name = "inbox_type", nullable = false)
    private InboxType inboxType;

    /** 寄發群組sid */
    @ManyToOne
    @JoinColumn(name = "alert_group_sid", nullable = false)
    private AlertInboxSendGroup sendInboxGroup;

    /** 狀態 */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    /** 建立者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "create_usr", nullable = false)
    private User createdUser;

    /** 建立日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    /** 異動者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "update_usr")
    private User updatedUser;

    /** 異動日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;
}
