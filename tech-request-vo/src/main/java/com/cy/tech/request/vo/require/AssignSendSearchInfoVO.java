package com.cy.tech.request.vo.require;

import com.cy.tech.request.vo.enums.AssignSendType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 需求單分派資訊搜尋
 *
 * @author allen
 */
@NoArgsConstructor
@EqualsAndHashCode(of = { "sid" })
@Entity
public class AssignSendSearchInfoVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8379989234893476436L;

    @Id
    @Getter
    @Setter
    private String sid;

    /** 需求單主檔 */
    @Getter
    @Setter
    private String requireSid;

    /** 需求單單號 */
    @Getter
    @Setter
    private String requireNo;

    /** 類型 分派 | 通知 */
    @Getter
    @Setter
    private AssignSendType type = AssignSendType.ASSIGN;
    
    /** 類型 分派 | 通知 */
    @Getter
    @Setter
    private int typeInt;

    /** 部門 */
    @Getter
    @Setter
    private Integer depSid;
    
    
    /** 部門 */
    @Getter
    @Setter
    private Integer memberSid;

    /** 分派此批單據的人員 */
    @Getter
    @Setter
    private Integer createdUserSid;

    /** 分派此批單據的時間 */
    @Getter
    @Setter
    private Date createdDate;

}
