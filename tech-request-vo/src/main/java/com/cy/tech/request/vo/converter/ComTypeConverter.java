/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import com.cy.tech.request.vo.enums.ComType;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 需求單模版元件
 *
 * @author shaun
 */
@Slf4j
@Converter
public class ComTypeConverter implements AttributeConverter<ComType, String> {

    @Override
    public String convertToDatabaseColumn(ComType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public ComType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return ComType.valueOf(dbData);
        } catch (Exception e) {
            log.error("ComType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }

}
