/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require.tros;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.converter.OthSetHistoryBehaviorConverter;
import com.cy.tech.request.vo.converter.OthSetStatusConverter;
import com.cy.tech.request.vo.enums.OthSetHistoryBehavior;
import com.cy.tech.request.vo.enums.OthSetStatus;
import com.cy.work.common.vo.converter.StringBlobConverter;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 基本設定資訊History 物件 (For 回覆,回覆的回覆)
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "tr_os_history")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class TrOsHistory implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 603708066219775984L;
    /** Sid */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "os_history_sid", length = 36)
    private String sid;
    /** 基本資訊設定Sid */
    @Column(name = "os_sid", nullable = false, length = 36)
    private String os_sid;
    /** 基本資訊設定單號 */
    @Column(name = "os_no", nullable = false, length = 21)
    private String os_no;
    /** 需求單Sid */
    @Column(name = "require_sid", nullable = false, length = 36)
    private String require_sid;
    /** 需求單單號 */
    @Column(name = "require_no", nullable = false, length = 21)
    private String require_no;
    /** 基本資訊設定行為 */
    @Convert(converter = OthSetHistoryBehaviorConverter.class)
    @Column(name = "behavior", nullable = false)
    private OthSetHistoryBehavior behavior;
    /** 基本資訊設定狀態 */
    @Convert(converter = OthSetStatusConverter.class)
    @Column(name = "behavior_status", nullable = false)
    private OthSetStatus behaviorStatus;
    /** 回覆Sid */
    @Column(name = "os_reply_sid", nullable = true, length = 36)
    private String os_reply_sid;
    /** 回覆的回覆Sid */
    @Column(name = "os_reply_and_reply_sid", nullable = true, length = 36)
    private String os_reply_and_reply_sid;
    /** 該欄位For 取消與完成 內容 */
    @Column(name = "input_info")
    private String inputInfo;
    /** 該欄位For 取消與完成 內容css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "input_info_css")
    private String inputInfoCss;
    /** 狀態 */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;
    /** 建立者Sid */
    @Column(name = "create_usr", nullable = false)
    private Integer create_usr;
    /** 建立者日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date create_dt;
    /** 更新者Sid */
    @Column(name = "update_usr", nullable = true)
    private Integer update_usr;
    /** 更新者日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt", nullable = true)
    private Date update_dt;

}
