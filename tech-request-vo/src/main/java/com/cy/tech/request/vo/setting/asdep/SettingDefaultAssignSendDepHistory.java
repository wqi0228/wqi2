package com.cy.tech.request.vo.setting.asdep;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.cy.tech.request.vo.converter.AssignSendTypeConverter;
import com.cy.tech.request.vo.converter.RequireCheckItemTypeConverter;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.work.common.vo.AbstractEntity;
import com.cy.work.common.vo.converter.SplitListIntConverter;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 異動記錄:檢查確認預設分派/通知單位設定
 * 
 * @author allen1214_wu
 */
@EqualsAndHashCode(callSuper = false, of = "sid")
@Table(name = "tr_setting_default_asdep_his")
@Entity
public class SettingDefaultAssignSendDepHistory extends AbstractEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2945113553270404251L;

    /**
     * SID
     */
    @Id
    @Getter
    @Setter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "his_sid")
    private String sid;

    /**
     * 小類SID
     */
    @Getter
    @Setter
    @Column(name = "basic_data_small_category_sid")
    private String smallCategorySid;

    /**
     * 檢查項目(對應系統別欄位)
     */
    @Getter
    @Setter
    @Convert(converter = RequireCheckItemTypeConverter.class)
    @Column(name = "check_item_type")
    private RequireCheckItemType checkItemType;

    /** 類型 分派 | 通知 */
    @Getter
    @Setter
    @Convert(converter = AssignSendTypeConverter.class)
    @Column(name = "assign_send_type")
    private AssignSendType assignSendType = AssignSendType.ASSIGN;

    /**
     * 此次異動新增的預設部門SID , 以『,』分隔
     */
    @Getter
    @Setter
    @Convert(converter = SplitListIntConverter.class)
    @Column(name = "add_deps_info")
    private List<Integer> addDepsInfo;
    
    /**
     * 此次異動移除的預設部門SID , 以『,』分隔
     */
    @Getter
    @Setter
    @Convert(converter = SplitListIntConverter.class)
    @Column(name = "remove_deps_info")
    private List<Integer> removeDepsInfo;

}