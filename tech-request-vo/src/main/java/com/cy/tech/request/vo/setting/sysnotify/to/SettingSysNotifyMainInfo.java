package com.cy.tech.request.vo.setting.sysnotify.to;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.notify.vo.enums.NotifyType;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 設定：系統通知設定資料 (master)
 * 
 * @author allen1214_wu
 */

@NoArgsConstructor
public class SettingSysNotifyMainInfo implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 8400662045392567590L;

    /**
	 * 開啟總開關
	 */
	@Getter
	@Setter
	private boolean openMainSwich = false;

	/**
	 * 自定義郵件通知地址
	 */
	@Getter
	@Setter
	private String notifyMailAddrs;


	/**
	 * @return Notify Mail Addrs
	 */
	public List<String> getNotifyMailAddrByList() {
		if (WkStringUtils.isEmpty(notifyMailAddrs)) {
			return Lists.newArrayList();
		}
		List<String> results = Lists.newArrayList();
		for (String mailAddrs : notifyMailAddrs.split(";")) {
			results.add(mailAddrs);
		}
		return results;
	}

	/**
	 * 通知類別設定資料
	 */
	@Getter
	@Setter
	private Map<NotifyType, SettingSysNotifyTypeInfo> typeInfo;
}
