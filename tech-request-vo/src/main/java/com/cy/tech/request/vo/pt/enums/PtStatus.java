/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.pt.enums;

import lombok.Getter;

/**
 * 原型確認狀態
 *
 * @author shaun
 */
public enum PtStatus {

	/** 0 原型確認簽核中 */
	SIGN_PROCESS("原型確認簽核中", false, false),
	/** 1 原型確認已核准 */
	APPROVE("原型確認已核准", false, false),
	/** 2 原型確認中 */
	PROCESS("原型確認中", false, false),
	/** 3 重新實作 */
	REDO("重新實作", true, false),
	/** 4 審核作廢 */
	VERIFY_INVAILD("審核作廢", true, false),
	/** 5 符合需求 */
	FUNCTION_CONFORM("符合需求", true, false),
	/** 6 因強制需求完成或強制結案而關閉 */
	FORCE_CLOSE("強制關閉", true, true),
	/** 7 單位於製作進度面板強制關閉 */
	UNIT_FORCE_CLOSE("需求確認單位強制關閉", true, true);

	@Getter
	private String label;

	/**
	 * 此階段為流程終點狀態
	 */
	@Getter
	private boolean inFinish;

	/**
	 * 強制結案的類型
	 */
	@Getter
	private boolean forceCompleteStatus;

	private PtStatus(String label, boolean inFinish, boolean forceCompleteStatus) {
		this.label = label;
		this.inFinish = inFinish;
		this.forceCompleteStatus = forceCompleteStatus;
	}
}
