package com.cy.tech.request.vo.menu;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 功能菜單
 *
 * @author shaun
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_fun_item")
public class TrMenuItem implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5770609718917553944L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "sid")
    private Integer sid;

    @ManyToOne
    @JoinColumn(name = "fun_item_group_sid", nullable = false)
    private TrMenuItemGroup menuItemGroup;

    @Column(name = "function_title")
    private String title;

    @Column(name = "component_id")
    private String componentID;

    @Column(name = "url")
    private String url;

    @Column(name = "seq")
    private Integer seq;

    /** menu 圖示 */
    @Column(name = "icon")
    private String icon;

    /** menu 數量 sql 語法 */
    @Column(name = "count_sql")
    private String countSql;

    /** 權限對應 如果有多個角色字串以 '，' 分隔 */
    @Column(name = "permission_role")
    private String permissionRole;

}
