/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.value.to;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 *
 * @author shaun
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomFilterColumnTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8646796599580163246L;

    @JsonProperty(value = "key")
    private String key;

    @JsonProperty(value = "name")
    private String name;

    @JsonProperty(value = "value")
    private String value;

    @JsonProperty(value = "searchReportCustomType")
    private String searchReportCustomType;

}
