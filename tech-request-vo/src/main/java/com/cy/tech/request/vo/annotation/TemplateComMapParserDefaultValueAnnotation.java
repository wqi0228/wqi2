/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 解析元件物件時，依照標註得到元件預設值列表
 *
 * @author shaun
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface TemplateComMapParserDefaultValueAnnotation {

    //Map 中的 key值 
    int key();

    /**
     * 儲存型態(呈現編輯元件內容畫面，依不同型態有不同的編輯內容 - setting03.xhtml)<BR/>
     * 1. String.class <BR/>
     * 2. ComBaseTextArea.class <BR/>
     * 3. Boolean.class <BR/>
     * 4. Float.class <BR/>
     * 5. ComBaseList.class <BR/>
     * 6. ComBaseMapListStr.class <BR/>
     *
     * 借由TemplateComMapParserTo物件進行塞值處理
     *
     * @return class
     */
    @SuppressWarnings("rawtypes")
    Class valueClz();

    //Com元件對應的值名
    String valueName();

    //說明
    String desc();

    //是否可編輯
    boolean canEdit() default true;
}
