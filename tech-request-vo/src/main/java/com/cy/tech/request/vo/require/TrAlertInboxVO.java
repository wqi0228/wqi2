/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require;

import com.cy.work.common.vo.AbstractEntityVO;
import com.cy.tech.request.vo.enums.ForwardType;
import com.cy.tech.request.vo.enums.InboxType;
import com.cy.tech.request.vo.enums.ReadRecordStatus;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;

import org.springframework.beans.BeanUtils;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(of = { "sid" }, callSuper = false)
public class TrAlertInboxVO extends AbstractEntityVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7027690788031013682L;

    /**
     * 建構子
     * @param trAlertInbox
     */
    public TrAlertInboxVO(TrAlertInbox trAlertInbox) {
        //複製
        BeanUtils.copyProperties(trAlertInbox, this);
    }

    private String sid;

    /** 回覆來源訊息 */
    private String reply_alert_sid;

    /** 需求單主檔 */
    private String requireSid;

    /** 需求單單號 */
    private String requireNo;

    /** 轉寄類型 */
    private ForwardType forwardType;

    /** 寄件人 */
    @Column(name = "sender", nullable = false)
    private Integer sender;

    /** 寄件部門 */
    private Integer senderDep;

    /** 收件人 */
    private Integer receive;

    /** 收件部門 */
    private Integer receiveDep;

    /** 留言訊息 */
    private String message;

    /** 留言訊息 - 含css */
    private String messageCss;

    /** 收件夾類型 */
    private InboxType inboxType;

    /** 寄發群組sid */
    private String alert_group_sid;

    private ReadRecordStatus read_record;

    private Date read_dt;

}
