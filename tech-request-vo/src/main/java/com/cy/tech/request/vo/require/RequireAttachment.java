/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.work.common.vo.Attachment;
import com.cy.work.common.vo.converter.SidOrgConverter;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 技術需求單附加檔案
 *
 * @author shaun
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_attachment")
public class RequireAttachment implements Serializable, Attachment<String> {

    /**
     * 
     */
    private static final long serialVersionUID = -5217352527108757344L;

    /** 附加檔案 sid 由系統邏輯產生產 */
    @Id
    @Column(name = "attachment_sid", length = 36, nullable = false, unique = true)
    private String sid;

    /**
     * 需求單主檔
     */
    @ManyToOne
    @JoinColumn(name = "require_sid")
    private Require require;

    /**
     * 需求單單號
     */
    @Column(name = "require_no", length = 21)
    private String requireNo;

    /**
     * 檔案名稱
     */
    @Column(name = "file_name", nullable = false, length = 255)
    private String fileName;

    /**
     * 檔案說明
     */
    @Column(name = "description", length = 255)
    private String desc = "";

    /**
     * 上傳檔案者所屬部門
     */
    @Convert(converter = SidOrgConverter.class)
    @Column(name = "department", nullable = false)
    private Org uploadDept;

    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    @Convert(converter = SidUserConverter.class)
    @Column(name = "create_usr", nullable = false)
    private User createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    @Convert(converter = SidUserConverter.class)
    @Column(name = "update_usr", nullable = false)
    private User updatedUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

    /**
     * 畫面用，進行編輯時，附加檔案的flag
     */
    @Transient
    private Boolean keyCheckEdit = Boolean.FALSE;

    /**
     * 畫面用，上傳附加檔案如果取消勾選，則不儲存
     */
    @Transient
    private Boolean keyChecked = Boolean.FALSE;

}
