/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import com.cy.tech.request.vo.enums.ForwardType;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author shaun
 */
@Slf4j
@Converter
public class ForwardTypeConverter implements AttributeConverter<ForwardType, String> {

    @Override
    public String convertToDatabaseColumn(ForwardType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public ForwardType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return ForwardType.valueOf(dbData);
        } catch (Exception e) {
            log.error("ForwardType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }

}
