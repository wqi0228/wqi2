/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import lombok.extern.slf4j.Slf4j;

/**
 * ON程式異動明細表-自訂搜尋元件底層列舉
 *
 * @author brain0925_liao
 */
@Slf4j
public enum Search17QueryColumn implements SearchReportCustomEnum {
    /** ON程式異動明細表-key */
    Search17Query("ON程式異動明細表"),
    /** 需求類別 */
    DemandType("需求類別"),
    /** 填單單位 */
    DemandDep("填單單位"),
    /** 通知單位 */
    NotifyDep("通知單位"),
    /** 模糊搜尋 */
    SearchText("模糊搜尋"),
    /** ON程式單號 */
    OnpgNo("ON程式單號"),
    /** 類別組合 */
    CategoryCombo("類別組合"),
    /** On程式狀態 */
    OnpgStatus("On程式狀態"),
    /** 立單區間-索引 */
    DateIndex("立單區間-索引");

    /** 欄位名稱 */
    private final String val;

    Search17QueryColumn(String val) {
        this.val = val;
    }

    @Override
    public String getVal() {
        return val;
    }

    @Override
    public String getUrl() {
        return "Search17";
    }

    @Override
    public String getKey() {
        return this.name();
    }

    @Override
    public SearchReportCustomEnum getSearchReportCustomEnumByName(String name) {
        try {
            return Search17QueryColumn.valueOf(name);
        } catch (Exception e) {
            log.error("getSearchReportCustomEnumByName ERROR", e);
        }
        return null;
    }
}
