/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.template.component;

import com.cy.work.common.utils.WkStringUtils;
import com.cy.tech.request.vo.annotation.TemplateComMapParserDefaultValueAnnotation;
import com.cy.tech.request.vo.enums.ComType;
import com.cy.tech.request.vo.value.to.TemplateDefaultValueTo;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import com.cy.tech.request.vo.template.component.to.ComRadioTo;
import com.cy.work.common.utils.WkJsonUtils;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.IOException;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 單選項目列表 <BR/>
 * 可互動TEXT_TYPE_ONE元件
 *
 * @author shaun
 */
@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ComRadioTypeThree implements ComBaseInteractive, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4266489617195954367L;

    /** 元件名稱陣列，於基礎欄位設定中顯示 */
    public List<String> displayComponents = Lists.newArrayList();

    /** 元件欄位名稱 */
    @JsonProperty("name")
    public String name;

    /** 元件Id */
    @JsonProperty("comId")
    public String comId;

    /** 元件type */
    @JsonProperty("type")
    public ComType type;

    /** 實作介面名稱 */
    @JsonProperty("interfaceName")
    public String interfaceName;

    /** 是否需要建立索引 */
    @JsonProperty("needCreateIndex")
    public Boolean needCreateIndex;

    @TemplateComMapParserDefaultValueAnnotation(key = ONE, valueName = "value01", valueClz = ComBaseMultipleRadio.class, desc = "單選項目列表")
    @JsonProperty("value01")
    private Map<String, ComRadioTo> value01;

    /** 值 2 radio group 按鈕群組下方輸入框值 */
    @TemplateComMapParserDefaultValueAnnotation(key = TWO, valueName = "value02", valueClz = String.class, desc = "按鈕群組下方輸入框值")
    @JsonProperty("value02")
    private String value02;

    /** 值 3 radio group 按鈕群組下方輸入框內預設提示輸入 */
    @TemplateComMapParserDefaultValueAnnotation(key = THREE, valueName = "value03", valueClz = String.class, desc = "按鈕群組下方輸入框內預設提示輸入\n注意:有值才會顯示下方輸入框")
    @JsonProperty("value03")
    private String value03;

    /** 值 4 互動提示的前綴詞(TEXT_TYPE_ONE value03) */
    @TemplateComMapParserDefaultValueAnnotation(key = FOUR, valueName = "value04", valueClz = String.class, desc = "互動提示的前綴詞 (TEXT_TYPE_ONE value03)")
    @JsonProperty("value04")
    private String value04;

    @SuppressWarnings("unchecked")
    @Override
    public void setDefValue(FieldKeyMapping template) {
        name = template.getFieldName();
        comId = template.getComId();
        type = template.getFieldComponentType();
        interfaceName = ComBaseInteractive.class.getSimpleName();
        needCreateIndex = template.getRequiredIndex();
        this.displayComponents.add(ComBaseInteractive.class.getSimpleName());
        TemplateDefaultValueTo dv = template.getDefaultValue();
        Map<Integer, Object> valueMap = dv.getValue();
        if (valueMap == null) {
            return;
        }
        if (valueMap.containsKey(ONE)) {
            value01 = (LinkedHashMap<String, ComRadioTo>) valueMap.get(ONE);
        }
        if (valueMap.containsKey(TWO)) {
            value02 = (String) valueMap.get(TWO);
        }
        if (valueMap.containsKey(THREE)) {
            value03 = (String) valueMap.get(THREE);
        }
        if (valueMap.containsKey(FOUR)) {
            value04 = (String) valueMap.get(FOUR);
        }
    }

    /**
     * 當頁面選擇了是否時，觸發
     *
     * @param interactCom
     * @param interactComValue
     */
    @Override
    public void change(ComBase interactCom, TemplateDefaultValueTo interactComValue) {
        if (interactCom == null || interactComValue == null || !(interactCom instanceof ComTextTypeOne)) {
            return;
        }
        this.recoveryComRadioToType();
        ComTextTypeOne interactTarget = (ComTextTypeOne) interactCom;
        Map<Integer, Object> mapValue = interactComValue.getValue();
        if (mapValue == null) {
            interactTarget.setValue03("");
        }
        StringBuilder sb = new StringBuilder();

        this.value01.values().forEach(each -> {
            if (each.getValue01() != null) {
                if (each.getValue01()) {
                    if (!Strings.isNullOrEmpty(each.getValue05())) {
                        sb.append(each.getValue05()).append("、");
                    }
                } else if (!Strings.isNullOrEmpty(each.getValue06())) {
                    sb.append(each.getValue06()).append("、");
                }
            }
        });
        if (Strings.isNullOrEmpty(sb.toString())) {
            interactTarget.setValue03("");
        } else {
            sb.delete(sb.length() - 1, sb.length());
            interactTarget.setValue03("(" + this.value04 + sb.toString() + ")");
        }
    }

    /**
     * 異常狀況時要強制轉型(當可能取得value01的值時，就必需檢測替換)
     */
    private void recoveryComRadioToType() {
        @SuppressWarnings({ "unchecked", "rawtypes" })
        Map<String, Object> valueMap = (Map) value01;
        valueMap.keySet().forEach(eachKey -> {
            try {
                if (!(valueMap.get(eachKey) instanceof ComRadioTo)) {
                    ComRadioTo radioTo = WkJsonUtils.getInstance().fromJson(WkJsonUtils.getInstance().toJson(valueMap.get(eachKey)), ComRadioTo.class);
                    value01.put(radioTo.getValue00(), radioTo);
                }
            } catch (IOException ex) {
                log.error("parser ComRadioTo faild", ex);
            }
        });
    }

    @Override
    public Boolean checkRequireValueIsEmpty() {
        //檢查使用者輸入欄位
        if (this.value01 == null) {
            this.value01 = Maps.newLinkedHashMap();
            return true;
        }
        recoveryComRadioToType();
        return this.value01.values().stream().anyMatch(each -> each.getValue01() == null);
    }

    @Override
    public String getCompositionText(boolean showField, int maxFieldNameLen) {
        StringBuilder sb = new StringBuilder();
        if (showField) {
            sb.append(WkStringUtils.getInstance().padEndEmpty(name, maxFieldNameLen, "："));
        }
        sb.append(this.createMapInnerText());
        if (!Strings.isNullOrEmpty(value03)) {
            sb.append("\r\n");
            sb.append(WkStringUtils.getInstance().readLine(value02, "  "));
        }
        return sb.toString();
    }

    /**
     * 建立map內文
     *
     * @return
     */
    private String createMapInnerText() {
        this.recoveryComRadioToType();
        StringBuilder sb = new StringBuilder();
        this.value01.values().forEach(each -> {
            sb.append(each.getValue00());
            if (each.getValue01()) {
                sb.append(Strings.isNullOrEmpty(each.getValue02()) ? "是" : each.getValue02()).append(" ");
            } else {
                sb.append(Strings.isNullOrEmpty(each.getValue03()) ? "否" : each.getValue03()).append(" ");
            }
            sb.append(Strings.nullToEmpty(each.getValue04())).append(" \r\n");
        });
        return sb.toString();
    }

    @Override
    public String getIndexContent() {
        try {
            List<String> idxList = Lists.newArrayList();
            idxList.add(value01 == null ? "" : this.createMapInnerText());
            idxList.add(Strings.nullToEmpty(value02));
            return WkJsonUtils.getInstance().toJson(idxList);
        } catch (IOException ex) {
            log.error("parse value to json string fail...");
            log.error("fail value01 = " + this.createMapInnerText());
            log.error("fail value02 = " + value02);
            log.error("fail error " + ex.getMessage(), ex);
            return value01 == null ? "" + value02 : this.createMapInnerText() + " " + value02;
        }
    }

}
