/**
 * 
 */
package com.cy.tech.request.vo.pps6;

import java.io.Serializable;

import javax.persistence.Id;

import org.springframework.beans.BeanUtils;

import com.cy.work.common.vo.AbstractEntityVO;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * PPS6 Modeler 單據名稱定義資料檔 VO
 * 
 * @author allen1214_wu
 */
@EqualsAndHashCode(callSuper = false, of = "sid")
@NoArgsConstructor
@Getter
@Setter
public class Pps6ModelerFormVO extends AbstractEntityVO implements Serializable{

	/**
     * 
     */
    private static final long serialVersionUID = 6377538417026565405L;

    /**
	 * 建構子
	 * @param pps6ModelerForm Pps6ModelerForm
	 */
	public Pps6ModelerFormVO(Pps6ModelerForm pps6ModelerForm) {
		// ====================================
		// copy Properties
		// ====================================
		BeanUtils.copyProperties(pps6ModelerForm, this);
	}

	/**
	 * SID
	 */
	@Id
	private Integer sid;

	/**
	 * 單據名稱
	 */
	private String name;
}
