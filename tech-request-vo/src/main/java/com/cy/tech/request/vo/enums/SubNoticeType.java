/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

/**
 * 子程序異動記錄 通知類型
 *
 * @author kasim
 */
public enum SubNoticeType {
    /** 原型確認通知單位 */
    PT_NOTICE_DEP("原型確認通知單位"),
    /** 原型確認通知人員 */
    PT_NOTICE_MEMBER("原型確認通知人員"),
    /** 送測 */
    TEST_INFO_DEP("送測"),
    /** ON程式 */
    ONPG_INFO_DEP("ON程式"),
    /** 其他資料設定 */
    OS_INFO_DEP("其他資料設定");
    
     private final String name;

    SubNoticeType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
