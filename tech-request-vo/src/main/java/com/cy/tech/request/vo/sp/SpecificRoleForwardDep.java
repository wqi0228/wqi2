/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.sp;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.Role;
import com.cy.commons.vo.User;
import com.cy.tech.request.vo.converter.ForwardDepToConverter;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.work.common.vo.converter.SidOrgConverter;
import com.cy.work.common.vo.converter.SidRoleConverter;
import com.cy.tech.request.vo.value.to.DepTo;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 角色可轉發部門 !@#
 *
 * @author shaun
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "sp_role_forward_dep")
public class SpecificRoleForwardDep implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3676394244264261479L;

    @Id
    @Column(name = "send_issue_sid", length = 36)
    private Integer sid;

    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    @Convert(converter = SidUserConverter.class)
    @Column(name = "create_usr", nullable = false)
    private User createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    @Convert(converter = SidUserConverter.class)
    @Column(name = "update_usr")
    private User updatedUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

    /** 角色sid */
    @Convert(converter = SidRoleConverter.class)
    @Column(name = "ad_role_sid", nullable = false)
    private Role role;

    /** 公司sid */
    @Convert(converter = SidOrgConverter.class)
    @Column(name = "comp_sid", nullable = false)
    private Org createCompany;

    /** 部門Json格式 */
    @Convert(converter = ForwardDepToConverter.class)
    @Column(name = "dep_sid", nullable = false)
    private DepTo dep;

    /** 角色所歸屬的公司全部的部門都要能轉發(包含停用） */
    @Column(name = "send_all_dep", nullable = false)
    private Boolean sendAllDep = Boolean.FALSE;

}
