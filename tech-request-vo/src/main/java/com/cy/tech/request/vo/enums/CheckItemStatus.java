package com.cy.tech.request.vo.enums;

import lombok.Getter;

public enum CheckItemStatus {
    
    WAIT_CHECK("【待檢查】", "WS1-1-2b"),
    CHECKED("【已檢查】", "WS1-1-3"),
    CHECKED_BUT_REMOVE("【已檢查，但項目已移除】", "WS1-1-9"),
    NO_RIGHT("【非項目檢查人員】", "WS1-1-9"),
    ADD_AFTER_CHECK("【檢查後新增項目，無需檢查】", "WS1-1-9"),
    INTERNAL("【內部需求，無需檢查】", "WS1-1-9"),
    ;

    /**
     * 顯示文字
     */
    @Getter
    private String descr;

    /**
     * CSS
     */
    @Getter
    private String cssClass;

    private CheckItemStatus(
            String descr,
            String cssClass) {
        this.descr = descr;
        this.cssClass = cssClass;
    }
}
