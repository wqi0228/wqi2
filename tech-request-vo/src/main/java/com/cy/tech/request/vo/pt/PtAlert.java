/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.pt;

import com.cy.commons.vo.User;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.vo.converter.ReadRecordTypeConverter;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.work.common.vo.converter.WorkSourceTypeConverter;
import com.cy.work.common.enums.WorkSourceType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 原型確認通知訊息
 *
 * @author shaun
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "work_pt_alert")
public class PtAlert implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8644303544122853442L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "pt_check_alert_sid", length = 36)
    private String sid;

    /** 原型確認主檔 */
    @OneToOne
    @JoinColumn(name = "pt_check_sid", nullable = false)
    private PtCheck ptCheck;

    /** 原型確認單號 */
    @Column(name = "pt_check_no", nullable = false, length = 21, unique = true)
    private String ptNo;

    /** 原型確認來源 */
    @Convert(converter = WorkSourceTypeConverter.class)
    @Column(name = "pt_check_source_type", nullable = false)
    private WorkSourceType sourceType;

    /** 原型確認來源 sid */
    @Column(name = "pt_check_source_sid", nullable = false, length = 36)
    private String sourceSid;

    /** 原型確認來源 單號 */
    @Column(name = "pt_check_source_no", nullable = false, length = 21)
    private String sourceNo;

    /** 原型確認主題 */
    @Column(name = "pt_check_theme", nullable = false, length = 255)
    private String ptTheme;

    /** 寄件者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "send_usr", nullable = false)
    private User sender;

    /** 寄件日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "send_dt", nullable = false)
    private Date sendDate;

    /** 收件者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "receiver", nullable = false)
    private User receiver;

    /** 來自於哪一則原型確認回覆sid */
    @OneToOne
    @JoinColumn(name = "reply_sid", nullable = false)
    private PtReply reply;

    /** 來自於哪一則原型確認回覆的回覆sid */
    @OneToOne
    @JoinColumn(name = "already_reply_sid", nullable = false)
    private PtAlreadyReply alreadyReply;

    /** 此時的原型確認單狀態 */
    @Convert(converter = ReadRecordTypeConverter.class)
    @Column(name = "read_status", nullable = false)
    private ReadRecordType readStatus;

    /** 點擊時間 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "click_time")
    private Date clickTime;

}
