/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import com.cy.tech.request.vo.enums.GroupSetupType;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 群組設定型態轉換器
 *
 * @author jason_h
 */
@Slf4j
@Converter
public class GroupSetupTypeConverter implements AttributeConverter<GroupSetupType, String> {

    @Override
    public String convertToDatabaseColumn(GroupSetupType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public GroupSetupType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return GroupSetupType.valueOf(dbData);
        } catch (Exception e) {
            log.error("GroupSetupType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
