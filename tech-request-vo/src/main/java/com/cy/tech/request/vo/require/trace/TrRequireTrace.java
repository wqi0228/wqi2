/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require.trace;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.converter.RequireTraceTypeConverter;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.work.common.vo.converter.StringBlobConverter;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "tr_require_trace")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class TrRequireTrace implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5030434608553380478L;
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "require_trace_sid", length = 36)
    private String sid;
    @Column(name = "require_sid", nullable = false, length = 36)
    private String require_sid;

    @Column(name = "require_no", nullable = false, length = 21)
    private String require_no;

    /** 追蹤類型 */
    @Convert(converter = RequireTraceTypeConverter.class)
    @Column(name = "require_trace_type", nullable = false, length = 45)
    private RequireTraceType requireTraceType;

    @Column(name = "require_trace_content")
    private String require_trace_content;

    @Convert(converter = StringBlobConverter.class)
    @Column(name = "require_trace_content_css")
    private String require_trace_content_css;

    @Column(name = "reason", length = 21)
    private String reason;

    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    @Column(name = "create_usr", nullable = false)
    private Integer create_usr;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date create_dt;

    @Column(name = "update_usr", nullable = true)
    private Integer update_usr;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt", nullable = true)
    private Date update_dt;
}
