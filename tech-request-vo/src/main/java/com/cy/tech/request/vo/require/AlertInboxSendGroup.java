/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require;

import com.cy.commons.enums.Activation;
import com.cy.commons.interfaces.BaseEntity;
import com.cy.commons.vo.User;
import com.cy.tech.request.vo.converter.ForwardTypeConverter;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.tech.request.vo.enums.ForwardType;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 收件夾訊息寄發群組
 *
 * @author shaun
 */
@Data
@ToString(exclude = {"inboxs"})
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_alert_inbox_send_group")
public class AlertInboxSendGroup implements Serializable, BaseEntity<String> {

    /**
     * 
     */
    private static final long serialVersionUID = 9168871652404765434L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "alert_group_sid", length = 36)
    private String sid;

    /** 需求單主檔 */
    @ManyToOne
    @JoinColumn(name = "require_sid", nullable = false)
    private Require require;

    /** 需求單單號 */
    @Column(name = "require_no", nullable = false, length = 21)
    private String requireNo;

    /** 留言訊息 */
    @Column(name = "message")
    private String message;

    /** 留言訊息 - 含css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "message_css")
    private String messageCss;

    /** 轉寄類型 */
    @Convert(converter = ForwardTypeConverter.class)
    @Column(name = "forward_type", nullable = false)
    private ForwardType forwardType;

    /** 寄發至 - 可能是 寄發到哪個單位 或是 寄發給哪個人 ex.:E化發展部、總管理總處、總務資訊部 */
    @Column(name = "send_to", nullable = false)
    private String sendTo;

    /** 狀態 */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    /** 建立者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "create_usr", nullable = false)
    private User createdUser;

    /** 建立日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    /** 異動者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "update_usr")
    private User updatedUser;

    /** 異動日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

    @OneToMany(mappedBy = "sendInboxGroup", cascade = CascadeType.ALL)
    private List<AlertInbox> inboxs = Lists.newArrayList();

}
