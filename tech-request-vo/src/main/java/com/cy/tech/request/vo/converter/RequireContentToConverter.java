/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.tech.request.vo.template.component.ComBase;
import com.cy.tech.request.vo.value.to.DocComTemplateTo;
import com.cy.tech.request.vo.value.to.RequireContentTo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 需求單內容轉換器
 *
 * @author shaun
 */
@Slf4j
@Converter
public class RequireContentToConverter implements AttributeConverter<RequireContentTo, String> {

    @Override
    public String convertToDatabaseColumn(RequireContentTo attribute) {
        try {
            Map<String, ComBase> loadDocValueMap = attribute.getComValueMap();
            List<DocComTemplateTo> dctvList = Lists.newArrayList();
            for (String key : loadDocValueMap.keySet()) {
                ComBase com = loadDocValueMap.get(key);
                String comJson = WkJsonUtils.getInstance().toJson(com);
                DocComTemplateTo dctv = new DocComTemplateTo(com.getComId(), com.getType(), comJson);
                dctvList.add(dctv);
            }
            return WkJsonUtils.getInstance().toJson(dctvList);
        } catch (IOException ex) {
            log.error("parser RequireContentTo to json fail!!" + ex.getMessage());
            return "";
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public RequireContentTo convertToEntityAttribute(String dbData) {
        RequireContentTo to = new RequireContentTo();
        try {
            Map<String, ComBase> dataControllMap = Maps.newHashMap();
            List<DocComTemplateTo> dctvList = WkJsonUtils.getInstance().fromJsonToList(dbData, DocComTemplateTo.class);
            for (DocComTemplateTo each : dctvList) {
                String comId = each.getComId();
                ComBase obj = (ComBase) WkJsonUtils.getInstance().fromJson(each.getComValue(), each.getType().getComClz());
                dataControllMap.put(comId, obj);
            }
            to.setComValueMap(dataControllMap);
            return to;
        } catch (IOException ex) {
            log.error("parser RequireContentTo to json fail!!" + ex.getMessage());
        }
        return to;
    }

}
