/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.worktest.converter;

import com.cy.tech.request.vo.worktest.enums.WorkTestAttachmentBehavior;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 附加檔案 行為
 *
 * @author shaun
 */
@Slf4j
@Converter
public class WorkTestAttachmentBehaviorConverter implements AttributeConverter<WorkTestAttachmentBehavior, String> {

    @Override
    public String convertToDatabaseColumn(WorkTestAttachmentBehavior attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public WorkTestAttachmentBehavior convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return WorkTestAttachmentBehavior.valueOf(dbData);
        } catch (Exception e) {
            log.error("WorkTestAttachmentBehavior Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
