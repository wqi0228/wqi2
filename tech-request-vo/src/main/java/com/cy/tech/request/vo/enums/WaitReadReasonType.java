/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import java.util.List;
import java.util.stream.Collectors;

import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import lombok.Getter;

/**
 * 待閱原因型態
 *
 * @author kasim
 */
public enum WaitReadReasonType {

    PROTOTYPE_NOT_WAITREASON("無待閱原因", FormType.PTCHECK),
    PROTOTYPE_UPDATE_ESTABLISHDATE("修改預計完成日", FormType.PTCHECK),
    PROTOTYPE_SIGN_PROCESS("原型確認簽核中", FormType.PTCHECK),
    PROTOTYPE_VERIFY_INVAILD("審核作廢", FormType.PTCHECK),
    PROTOTYPE_APPROVE("原型確認已核准", FormType.PTCHECK),
    PROTOTYPE_HAS_REPLY("原型確認有回覆資訊", FormType.PTCHECK),
    PROTOTYPE_REDO("重新實作", FormType.PTCHECK),
    PROTOTYPE_FUNCTION_CONFORM("符合需求", FormType.PTCHECK),

    TEST_NOT_WAITREASON("無待閱原因", FormType.WORKTESTSIGNINFO),
    TEST_SIGN_PROCESS("送測簽核中", FormType.WORKTESTSIGNINFO),
    TEST_APPROVE("送測已核准", FormType.WORKTESTSIGNINFO),
    TEST_HAS_REPLY("有回覆資訊", FormType.WORKTESTSIGNINFO),
    TEST_RETEST("重測", FormType.WORKTESTSIGNINFO),
    TEST_QA_TEST("QA轉測試報告", FormType.WORKTESTSIGNINFO),
    TEST_COMPLETE("測試完成", FormType.WORKTESTSIGNINFO),
    TEST_ROLL_BACK("退測", FormType.WORKTESTSIGNINFO),
    TEST_CANCEL("取消測試", FormType.WORKTESTSIGNINFO),
    TEST_UPDATE_ESTABLISHDATE("調整預計完成日", FormType.WORKTESTSIGNINFO),
    TEST_HAS_PERSONAL_REPLY("有個人回覆資訊", FormType.WORKTESTSIGNINFO),
    TEST_VERIFY_INVAILD("審核作廢", FormType.WORKTESTSIGNINFO),
    TEST_PAPER_READY("文件補齊", FormType.WORKTESTSIGNINFO),

    ONPG_NOT_WAITREASON("無待閱原因", FormType.WORKONPG),
    ONPG_CHECK_RECORD("有檢查記錄資訊", FormType.WORKONPG),
    ONPG_CHECK_REPLY("有檢查記錄回覆", FormType.WORKONPG),
    ONPG_CANCEL("取消ON程式", FormType.WORKONPG),
    ONPG_CHECK_COMPLETE("檢查完成", FormType.WORKONPG),
    ONPG_ANNOTATION("有註記資訊", FormType.WORKONPG),
    ONPG_UPDATE_ESTABLISHDATE("更改預計完成日", FormType.WORKONPG),
    ONPG_UPDATE_NOTE("更改備註說明", FormType.WORKONPG),

    QA_JOIN_SCHEDULE("QA已納入排程", FormType.WORKTESTSIGNINFO),
    QA_UNJOIN_SCHEDULE("QA不納入排程", FormType.WORKTESTSIGNINFO),
    ADJUST_TEST_DATE("調整送測日", FormType.WORKTESTSIGNINFO),
    ADJUST_ONLINE_DATE("調整預計上線日", FormType.WORKTESTSIGNINFO),
    ADJUST_MASTER_TESTER("調整主測人員", FormType.WORKTESTSIGNINFO),
    ADJUST_SLAVE_TESTER("調整協測人員", FormType.WORKTESTSIGNINFO),
    ADJUST_SCHEDULE_FINISH_DATE("調整排定完成日", FormType.WORKTESTSIGNINFO),
    ADJUST_DATE("調整日期", FormType.WORKTESTSIGNINFO),

    FORCE_CLOSE("強制關閉", null);

    @Getter
    private String value;
    @Getter
    private FormType formType;

    WaitReadReasonType(String value, FormType formType) {
        this.value = value;
        this.formType = formType;
    }

    /**
     * 取得所有類型名稱
     * 
     * @param formType
     * @return
     */
    public static List<String> valueNames(FormType formType) {
        List<String> valueNames = Lists.newArrayList(WaitReadReasonType.values()).stream()
                .filter(type -> type.getFormType() != null)
                .filter(type -> type.getFormType().equals(formType))
                .map(WaitReadReasonType::name)
                .collect(Collectors.toList());

        valueNames.add(WaitReadReasonType.FORCE_CLOSE.name());
        return valueNames;
    }

    /**
     * @param str
     * @return RequireCheckItemType
     */
    public static WaitReadReasonType safeValueOf(String str) {
        if (WkStringUtils.notEmpty(str)) {
            for (WaitReadReasonType item : WaitReadReasonType.values()) {
                if (item.name().equals(str)) {
                    return item;
                }
            }
        }
        //WkCommonUtils.logWithStackTrace(InfomationLevel.ERROR, "WaitReadReasonType 無法解析:[" + str + "]");
        return null;
    }

    /**
     * 取得項目說明 (避免傳入字串對應不到項目)
     * 
     * @param codeName type name
     * @return 項目說明
     */
    public static String safeGetDescr(String codeName) {
        WaitReadReasonType item = safeValueOf(codeName);
        if (item != null) {
            return item.getValue();
        }
        return "";
    }

}
