/**
 * 
 */
package com.cy.tech.request.vo.setting;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.cy.tech.request.vo.converter.RequireCheckItemTypeConverter;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.work.common.vo.AbstractEntity;
import com.cy.work.common.vo.converter.SplitListIntConverter;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 設定：可檢查確認權限
 * 
 * @author allen1214_wu
 */
@EqualsAndHashCode(callSuper=false)
@Table(name = "tr_setting_check_confirm_right")
@Entity
public class SettingCheckConfirmRight extends AbstractEntity {

	/**
     * 
     */
    private static final long serialVersionUID = 2203927525890751796L;

    /**
	 * SID
	 */
	@Id
	@Getter
	@Setter
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sid")
	private String sid;

    /**
     * 檢查項目(對應系統別欄位)
     */
    @Getter
    @Setter
    @Convert(converter = RequireCheckItemTypeConverter.class)
    @Column(name = "check_item_type")
    private RequireCheckItemType checkItemType;
	
	
	/**
	 * 可檢查部門 sid (以 , 分隔)
	 */
	@Getter
	@Setter
	@Convert(converter = SplitListIntConverter.class)
	@Column(name = "can_check_depts")
	private List<Integer> canCheckDepts;
	
	/**
	 * 可檢查人員 sid (以 , 分隔)
	 */
	@Getter
	@Setter
	@Convert(converter = SplitListIntConverter.class)
	@Column(name = "can_check_users")
	private List<Integer> canCheckUsers;
}
