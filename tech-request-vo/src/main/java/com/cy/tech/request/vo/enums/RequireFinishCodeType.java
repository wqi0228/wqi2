/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import lombok.Getter;

/**
 * 製作進度完成碼
 *
 * @author shaun
 */
public enum RequireFinishCodeType {

    /** Y 已完成 */
    COMPLETE("Y", "已完成"),
    /** y 已完成 */
    HALF_COMPLETE("y", "已完成"),
    /** N 未完成 */
    INCOMPLETE("N", "未完成"),
    /** n 終止 */
    TERMINATED("n", "終止"),;

    @Getter
    private final String code;
    @Getter
    private final String codeName;

    RequireFinishCodeType(String code, String codeName) {
        this.code = code;
        this.codeName = codeName;
    }

}
