/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.onpg;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.tech.request.vo.onpg.converter.WorkOnpgCheckRecordReplyTypeConverter;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgCheckRecordReplyType;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.vo.converter.SidOrgConverter;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.common.vo.converter.WorkSourceTypeConverter;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * ON程式檢查記錄
 *
 * @author shaun
 */
@Data
@NoArgsConstructor
@ToString(exclude = {"checkRecordReplys"})
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "work_onpg_check_record")
public class WorkOnpgCheckRecord implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5984382914039691096L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "onpg_check_record_sid", length = 36)
    private String sid;

    /** on程式單sid */
    @ManyToOne
    @JoinColumn(name = "onpg_sid")
    private WorkOnpg onpg;

    /** on程式單號 */
    @Column(name = "onpg_no", length = 21)
    private String onpgNo;

    /** on程式來源 */
    @Convert(converter = WorkSourceTypeConverter.class)
    @Column(name = "onpg_source_type")
    private WorkSourceType sourceType;

    /** on程式來源 sid */
    @Column(name = "onpg_source_sid", length = 36)
    private String sourceSid;

    /** on程式來源 單號 */
    @Column(name = "onpg_source_no", length = 21)
    private String sourceNo;

    /** 記錄人員部門 */
    @Convert(converter = SidOrgConverter.class)
    @Column(name = "record_person_dep", nullable = false)
    private Org dep;

    /** 記錄人員sid */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "record_person_sid", nullable = false)
    private User person;

    /** 記錄日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "record_dt", nullable = false)
    private Date date;

    /** 異動日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "record_udt", nullable = false)
    private Date updateDate;

    /** 記錄內容 */
    @Column(name = "record_content", nullable = false)
    private String content;

    /** 記錄內容 有存css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "record_content_css")
    private String contentCss;

    /** 記錄內容回覆 */
    @OneToMany(mappedBy = "checkRecord", cascade = CascadeType.ALL)
    @OrderBy("updateDate DESC")
    private List<WorkOnpgCheckRecordReply> checkRecordReplys = Lists.newArrayList();

    /** 檢查記錄歷程 */
    @OneToOne(mappedBy = "checkRecord", cascade = CascadeType.PERSIST)
    private WorkOnpgHistory history;

    /** 檢查記錄類型(回覆類型) */
    @Convert(converter = WorkOnpgCheckRecordReplyTypeConverter.class)
    @Column(name = "reply_type")
    private WorkOnpgCheckRecordReplyType replyType = WorkOnpgCheckRecordReplyType.GENERAL_REPLY;
}
