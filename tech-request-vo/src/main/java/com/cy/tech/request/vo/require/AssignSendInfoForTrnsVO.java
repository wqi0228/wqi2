/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require;

import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Id;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 需求單分派資訊檔 for 轉置
 */
@NoArgsConstructor
@EqualsAndHashCode(of = { "sid" }, callSuper = false)
public class AssignSendInfoForTrnsVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5212595656220979947L;

    @Id
    @Getter
    @Setter
    private String sid;

    /** 需求單主檔 */
    @Getter
    @Setter
    private String requireSid;

    /** 需求單單號 */
    @Getter
    @Setter
    private String requireNo;

    /** 類型 分派 | 通知 */
    @Getter
    @Setter
    private int assignSendtype;

    /** 部門及成員資訊 */
    @Getter
    @Setter
    private List<Integer> depSids = Lists.newArrayList();
    @Getter
    @Setter
    private String depSids_src;

    /**
     * 建立者
     */
    @Getter
    @Setter
    private Integer createdUser;

    /**
     * 建立日期
     */
    @Getter
    @Setter
    private Date createdDate;
    
    /**
     * 需求單製作進度
     */
    @Getter
    @Setter
    private String requireStatus;
}
