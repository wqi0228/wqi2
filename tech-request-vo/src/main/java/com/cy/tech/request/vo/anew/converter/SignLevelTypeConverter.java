/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.anew.converter;

import com.cy.tech.request.vo.anew.enums.SignLevelType;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 簽核層級 Converter
 *
 * @author kasim
 */
@Slf4j
@Converter
public class SignLevelTypeConverter implements AttributeConverter<SignLevelType, String> {

    @Override
    public String convertToDatabaseColumn(SignLevelType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public SignLevelType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return SignLevelType.valueOf(dbData);
        } catch (Exception e) {
            log.error("SignLevelType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
