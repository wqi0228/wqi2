package com.cy.tech.request.vo.category;

import com.cy.work.common.vo.AbstractEntity;
import com.cy.work.common.enums.UrgencyType;
import com.cy.work.common.utils.WkEntityUtils;

import java.io.Serializable;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;

/**
 * 中類
 */
public class SimpleMiddleCategoryVO extends AbstractEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -250373050657501761L;

    /**
     * @param simpleMiddleCategory
     */
    public SimpleMiddleCategoryVO(SimpleMiddleCategory simpleMiddleCategory) {
        if (simpleMiddleCategory == null) {
            return;
        }
        WkEntityUtils.getInstance().copyProperties(simpleMiddleCategory, this);
    }
    
    /**
     * SID
     */
    @Id
    @Getter
    @Setter
    private String sid;

    /**
     * ID
     */
    @Getter
    @Setter
    private String id;

    /**
     * 
     */
    @Getter
    @Setter
    private String name;

    /**
     * 
     */
    @Getter
    @Setter
    private String parentBigCategorySid;

    /**
     * 
     */
    @Getter
    @Setter
    private UrgencyType urgency;

    /**
     * 
     */
    @Getter
    @Setter
    private Integer singleUploadLimited;

    /**
     * 
     */
    @Getter
    @Setter
    private Integer prototypeUploadLimited;

    /**
     * 
     */
    @Getter
    @Setter
    private String note;

    /**
     * 
     */
    @Getter
    @Setter
    private Integer seq;

    /**
     * 權限對應 如果有多個角色字串以 '，' 分隔
     */
    @Getter
    @Setter
    private String permissionRole;
}
