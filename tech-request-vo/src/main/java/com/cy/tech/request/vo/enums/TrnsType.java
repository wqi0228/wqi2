/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import lombok.Getter;

/**
 * @author allen1214_wu
 */
public enum TrnsType {

    ASSIGN_SEND_INFO_TO_HISTORY("ASSIGN_SEND_INFO_TO_HISTORY", "V7.0 分派單位轉檔"),
    ORG_TRNS_IN_CHARGE_DEP("ORG_TRNS_IN_CHARGE_DEP", "組織異動-主責單位"),
    ORG_TRNS_CREATE_DEP_REQUIRE("ot_01_01", "組織異動-立案單位-需求單"),
    ORG_TRNS_CREATE_DEP_PTCHECK("ot_01_02", "組織異動-立案單位-原型確認"),
    ORG_TRNS_CREATE_DEP_WORKTESTSIGNINFO("ot_01_03", "組織異動-立案單位-送測"),
    ORG_TRNS_CREATE_DEP_OTHSET("ot_01_04", "組織異動-立案單位-其他資料設定"),
    ORG_TRNS_CREATE_DEP_WORKONPG("ot_01_05", "組織異動-立案單位-ON程式"),
    ORG_TRNS_ALERT_INBOX_RECEVIE_DEP("ORG_TRNS_ALERT_INBOX_RECEVIE_DEP", "組織異動-被轉寄單位轉檔");

    /**
     * 存入db值
     */
    @Getter
    private final String value;

    /**
     * 說明
     */
    @Getter
    private final String desc;

    TrnsType(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }
}
