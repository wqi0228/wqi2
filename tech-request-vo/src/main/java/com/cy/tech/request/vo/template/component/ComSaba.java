package com.cy.tech.request.vo.template.component;

/**
 * 沙巴元件
 * @author aken_kao
 */
public interface ComSaba {

    public Boolean getIsSabaComponent();

    public Boolean getValue01();
}
