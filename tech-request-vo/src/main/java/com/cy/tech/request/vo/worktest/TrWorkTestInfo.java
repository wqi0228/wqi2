/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.worktest;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.enums.WaitReadReasonConverter;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.worktest.converter.WorkTestStatusConverter;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.vo.converter.JsonStringListToConverter;
import com.cy.work.common.vo.converter.ReadRecordGroupToConverter;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.common.vo.converter.WorkSourceTypeConverter;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.cy.work.common.vo.value.to.ReadRecordGroupTo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author brain0925_liao
 */
@Data
@NoArgsConstructor
@ToString(of = {"sid"})
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "work_test_info")
public class TrWorkTestInfo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5137649404401792124L;

    /** 送測單sid */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "testinfo_sid", length = 36)
    private String sid;

    /** 送測單號 */
    @Column(name = "testinfo_no", nullable = false, length = 21, unique = true)
    private String testinfoNo;

    /** 送測來源 */
    @Convert(converter = WorkSourceTypeConverter.class)
    @Column(name = "testinfo_source_type", nullable = false)
    private WorkSourceType sourceType;

    /** 送測來源 sid */
    @Column(name = "testinfo_source_sid", nullable = false, length = 36)
    private String sourceSid;

    /** 送測來源 單號 */
    @Column(name = "testinfo_source_no", nullable = false, length = 21)
    private String sourceNo;

    /** 挑選的送測單位 */
    @Convert(converter = JsonStringListToConverter.class)
    @Column(name = "testinfo_deps", nullable = false)
    private JsonStringListTo sendTestDep = new JsonStringListTo();

    /** 是否有流程 */
    @Column(name = "has_sign", nullable = false)
    private Boolean hasSign = Boolean.FALSE;

    /** 送測主題 */
    @Column(name = "testinfo_theme", nullable = false, length = 255)
    private String theme;

    /** 送測內容 */
    @Column(name = "testinfo_content", nullable = false)
    private String content;

    /** 送測內容 含css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "testinfo_content_css", nullable = false)
    private String contentCss;

    /** 備註說明 */
    @Column(name = "testinfo_note")
    private String note;

    /** 備註說明 含css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "testinfo_note_css")
    private String noteCss;

    /** 預計完成日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "testinfo_estimate_dt", nullable = false)
    private Date establishDate;

    /** 實際完成日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "testinfo_finish_dt")
    private Date finishDate;

    /** 送測取消日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "testinfo_cancel_dt")
    private Date cancelDate;

    /** 待閱異動日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "read_update_dt")
    private Date readUpdateDate;

    /** 待閱原因 */
    @Convert(converter = WaitReadReasonConverter.class)
    @Column(name = "read_reason")
    private WaitReadReasonType readReason;

    /** 待閱人員清單 - 閱讀紀錄資訊 */
    @Convert(converter = ReadRecordGroupToConverter.class)
    @Column(name = "read_record")
    private ReadRecordGroupTo readRecord = new ReadRecordGroupTo();

    /** 送測狀態 */
    @Convert(converter = WorkTestStatusConverter.class)
    @Column(name = "testinfo_status", nullable = false)
    private WorkTestStatus testinfoStatus;

    /** FB Case No */
    @Column(name = "fb_id", length = 11)
    private Integer fbId;
    
     @Column(name = "comp_sid", nullable = false)
    private Integer comp_sid;

    @Column(name = "dep_sid", nullable = false)
    private Integer dep_sid;
    /** 狀態 */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;
   /** 建立者Sid */
    @Column(name = "create_usr", nullable = false)
    private Integer create_usr;
    /** 建立日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date create_dt;
    /** 更新者 */
    @Column(name = "update_usr", nullable = true)
    private Integer update_usr;
    /** 更新時間 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;
}
