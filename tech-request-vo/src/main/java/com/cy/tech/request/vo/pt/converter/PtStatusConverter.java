/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.pt.converter;

import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 原型確認狀態
 *
 * @author shaun
 */
@Slf4j
@Converter
public class PtStatusConverter implements AttributeConverter<PtStatus, String> {

    @Override
    public String convertToDatabaseColumn(PtStatus attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public PtStatus convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return PtStatus.valueOf(dbData);
        } catch (Exception e) {
            log.error("PtStatus Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
