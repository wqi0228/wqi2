/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.value.to;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 分派通知群組物件
 *
 * @author shaun
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AssignSendGroupsTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4456174887115687639L;
    /**
     * 群組清單
     */
    @JsonProperty(value = "groups")
    private List<AssignSendGroupsDetailTo> groups = Lists.newArrayList();

}
