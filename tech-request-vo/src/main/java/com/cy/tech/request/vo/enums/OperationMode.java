package com.cy.tech.request.vo.enums;

import lombok.Getter;

public enum OperationMode {
    LIST("清單模式"),
    TREE("組織樹模式"),
    ;
    
    
    @Getter
    private final String label;

    OperationMode(String label) {
        this.label = label;
    }
}
