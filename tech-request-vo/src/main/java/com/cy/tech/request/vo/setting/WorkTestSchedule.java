package com.cy.tech.request.vo.setting;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.cy.commons.enums.Activation;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author aken_kao
 */
@Data
@EqualsAndHashCode(callSuper=false)
@Entity
@Table(name = "work_test_schedule")
public class WorkTestSchedule {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "schedule_sid", length = 36)
    private String sid;

    @Column(name = "event_id", nullable = false)
    private String eventId;

    @Column(name = "schedule_name", nullable = false)
    private String scheduleName;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "start_date", nullable = false)
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_date", nullable = false)
    private Date endDate;

    @Column(name = "color_code", nullable = false)
    private String colorCode;

    @Column(name = "is_full", nullable = false)
    @Type(type = "yes_no")
    private boolean full;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    @Column(name = "create_usr", nullable = false)
    private Integer createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

    @Column(name = "update_usr")
    private Integer updatedUser;
    
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;
    
}
