package com.cy.tech.request.vo.setting.asdep;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.work.common.vo.AbstractEntityVO;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 設定:檢查確認預設分派/通知單位
 * 
 * @author allen1214_wu
 */
@EqualsAndHashCode(callSuper = false, of = "sid")
@NoArgsConstructor
@Getter
@Setter
public class SettingDefaultAssignSendDepVO extends AbstractEntityVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3210559965021844272L;
    /**
     * 建構子
     * 
     * @param smallCategorySid 小類SID
     * @param checkItemType    檢查項目
     * @param assignSendType   分派通知類別
     */
    public SettingDefaultAssignSendDepVO(
            String smallCategorySid,
            RequireCheckItemType checkItemType,
            AssignSendType assignSendType) {
        super();
        this.smallCategorySid = smallCategorySid;
        this.checkItemType = checkItemType;
        this.assignSendType = assignSendType;
    }

    /**
     * 建構子
     * 
     * @param entity SettingDefaultAssignSendDep
     */
    public SettingDefaultAssignSendDepVO(SettingDefaultAssignSendDep entity) {
        // ====================================
        // copy Properties
        // ====================================
        BeanUtils.copyProperties(entity, this);
    }

    /**
     * SID
     */
    private String sid;

    /**
     * 小類SID
     */
    private String smallCategorySid;

    /**
     * 檢查項目(對應系統別欄位)
     */
    private RequireCheckItemType checkItemType;

    /** 類型 分派 | 通知 */
    private AssignSendType assignSendType = AssignSendType.ASSIGN;

    /**
     * 可檢查部門 sid (以 , 分隔)
     */
    private List<Integer> depsInfo;

    // ======================================
    // native query 用
    // ======================================
    /**
     * 可檢查部門 sid (以 , 分隔)
     */
    private String depsInfo_src;

    // ======================================
    // view 用
    // ======================================
    /**
     * 部門設定顯示標籤
     */
    private String depsInfoTag;
    /**
     * 部門設定明細 (for tooltip)
     */
    private String depsInfoForTooltip;
    
    /**
     * 部門設定明細 (for excel)
     */
    private String depsInfoForExcel;

}