/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.onpg;

import com.cy.commons.enums.Activation;
import com.cy.commons.interfaces.BaseEntity;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.tech.request.vo.enums.WaitReadReasonConverter;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.onpg.converter.WorkOnpgStatusConverter;
import com.cy.tech.request.vo.onpg.enums.WorkOnpgStatus;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.converter.JsonStringListToConverter;
import com.cy.work.common.vo.converter.ReadRecordGroupToConverter;
import com.cy.work.common.vo.converter.SidOrgConverter;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.common.vo.converter.WorkSourceTypeConverter;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.cy.work.common.vo.value.to.ReadRecordGroupTo;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

/**
 * ON程式主檔
 *
 * @author shaun
 */
@Data
@NoArgsConstructor
@ToString(exclude = { "checkMemos", "historys", "checkRecords", "attachments" })
@EqualsAndHashCode(of = { "sid" })
@Entity
@Table(name = "work_onpg")
public class WorkOnpg implements Serializable, BaseEntity<String> {

    /**
     * 
     */
    private static final long serialVersionUID = 37691601324921934L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "onpg_sid", length = 36)
    private String sid;

    /** ON程式單號 */
    @Column(name = "onpg_no", nullable = false, length = 21, unique = true)
    private String onpgNo;

    /** ON程式來源 */
    @Convert(converter = WorkSourceTypeConverter.class)
    @Column(name = "onpg_source_type", nullable = false)
    private WorkSourceType sourceType;

    /** ON程式來源 sid */
    @Column(name = "onpg_source_sid", nullable = false, length = 36)
    private String sourceSid;

    /** ON程式來源 單號 */
    @Column(name = "onpg_source_no", nullable = false, length = 21)
    private String sourceNo;

    /** ON程式 通知單位 */
    @Convert(converter = JsonStringListToConverter.class)
    @Column(name = "onpg_deps", nullable = false)
    private JsonStringListTo noticeDeps = new JsonStringListTo();

    /**
     * 取得通知單位SID
     * 
     * @return
     */
    public Set<Integer> getNoticeDepSids() {
        if (noticeDeps == null || WkStringUtils.isEmpty(noticeDeps.getValue())) {
            return Sets.newHashSet();
        }
        return noticeDeps.getValue().stream()
                .filter(sidStr -> WkStringUtils.isNumber(sidStr))
                .map(sidStr -> Integer.parseInt(sidStr))
                .collect(Collectors.toSet());
    }

    /** ON程式主題 */
    @Column(name = "onpg_theme", nullable = false, length = 255)
    private String theme;

    /** ON程式內容 */
    @Column(name = "onpg_content", nullable = false)
    private String content;

    /** ON程式內容_有CSS格式 */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "onpg_content_css", nullable = false)
    private String contentCss;

    /** 備註說明 */
    @Column(name = "onpg_note")
    private String note;

    /** 備註說明_有CSS格式 */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "onpg_note_css")
    private String noteCss;

    /** 預計完成日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "onpg_estimate_dt", nullable = false)
    private Date establishDate;

    /** 檢查完成日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "onpg_finish_dt")
    private Date finishDate;

    /** ON程式取消日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "onpg_cancel_dt")
    private Date cancelDate;

    /** 待閱異動日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "read_update_dt")
    private Date readUpdateDate;

    /** 待閱原因 */
    @Convert(converter = WaitReadReasonConverter.class)
    @Column(name = "read_reason")
    private WaitReadReasonType readReason;

    /** 待閱人員清單 - 閱讀紀錄資訊 */
    @Convert(converter = ReadRecordGroupToConverter.class)
    @Column(name = "read_record")
    private ReadRecordGroupTo readRecord = new ReadRecordGroupTo();

    /** ON程式狀態 */
    @Convert(converter = WorkOnpgStatusConverter.class)
    @Column(name = "onpg_status", nullable = false)
    private WorkOnpgStatus onpgStatus;

    /** 填單人所歸屬的公司 */
    @Convert(converter = SidOrgConverter.class)
    @Column(name = "comp_sid", nullable = false)
    private Org createCompany;

    /** 填單人所歸屬的部門 */
    @Convert(converter = SidOrgConverter.class)
    @Column(name = "dep_sid", nullable = false)
    private Org createDep;

    /** 狀態 */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    /** 建立者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "create_usr", nullable = false)
    private User createdUser;

    /** 建立日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    /** 異動者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "update_usr")
    private User updatedUser;

    /** 異動日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

    /** ON程式歷程紀錄 */
    @OneToMany(mappedBy = "onpg", cascade = CascadeType.PERSIST)
    private List<WorkOnpgCheckMemo> checkMemos = Lists.newArrayList();

    /** ON程式歷程紀錄 */
    @OneToMany(mappedBy = "onpg", cascade = CascadeType.PERSIST)
    @OrderBy("updatedDate DESC")
    @Where(clause = "behavior != 'CHECK_RECORD_REPLY' ")
    private List<WorkOnpgHistory> historys = Lists.newArrayList();

    /** ON程式回覆 */
    @OneToMany(mappedBy = "onpg", cascade = CascadeType.ALL)
    private List<WorkOnpgCheckRecord> checkRecords = Lists.newArrayList();

    /** 附加檔案 */
    @OneToMany(mappedBy = "onpg", cascade = CascadeType.PERSIST)
    @Where(clause = "status = 0 AND onpg_history_sid IS NULL")
    @OrderBy(value = "createdDate DESC")
    private List<WorkOnpgAttachment> attachments = Lists.newArrayList();

    /**
     * 由MMS介接同步過來的的資料
     */
    @Column(name = "is_from_mms", nullable = false)
    private boolean fromMms;

}
