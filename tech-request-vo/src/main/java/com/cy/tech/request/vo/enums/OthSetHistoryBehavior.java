/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

/**
 * 資訊行為
 *
 * @author shaun
 */
public enum OthSetHistoryBehavior {

    /** 取消 */
    CANCEL_OS("取消"),
    /** 完成 */
    FINISH_OS("完成"),
    /** 回覆 */
    REPLY("回覆"),
    /** 回覆的回覆 */
    REPLY_AND_REPLY("回覆的回覆"),
    /**
     * 因強制需求完成或強制結案而關閉
     */
    FORCE_CLOSE("強制關閉");
    ;

    private final String val;

    OthSetHistoryBehavior(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }

}
