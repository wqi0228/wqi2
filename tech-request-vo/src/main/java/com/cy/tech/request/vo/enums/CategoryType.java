/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import com.cy.work.common.utils.WkStringUtils;

import lombok.Getter;

/**
 * 需求單分類項目類型
 * 
 * @author allen
 */
public enum CategoryType {

	BIG("大類", "WS1-1-3"),
	MIDDLE("中類", "WS1-1-2"),
	SMALL("小類", "WS1-1-4b"),
	;

	/**
	 * 
	 */
	@Getter
	private final String label;

	@Getter
	private final String styleClass;

	/**
	 * @param label
	 * @param styleClass
	 */
	private CategoryType(String label, String styleClass) {
		this.label = label;
		this.styleClass = styleClass;
	}

	/**
	 * @param str 列舉 name 字串
	 * @return CategoryType
	 */
	public static CategoryType safeValueOf(String str) {
		if (WkStringUtils.notEmpty(str)) {
			for (CategoryType item : CategoryType.values()) {
				if (item.name().equals(str)) {
					return item;
				}
			}
		}
		return null;
	}
}
