/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.column;

import com.cy.commons.vo.User;
import com.cy.tech.request.vo.converter.CustomColumnToConverter;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.tech.request.vo.value.to.CustomColumnTo;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 自訂報表欄位物件
 *
 * @author shaun
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_report_custom_column")
public class CustomColumn implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -684644143727575138L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "custom_column_sid", length = 36)
    private String sid;

    @Convert(converter = SidUserConverter.class)
    @Column(name = "user_sid", nullable = false, unique = true)
    private User user;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

    @Convert(converter = CustomColumnToConverter.class)
    @Column(name = "info")
    private CustomColumnTo info;

}
