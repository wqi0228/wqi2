/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.value.to;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 自訂報表欄位包裝
 *
 * @author shaun
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomColumnTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5617914202168067844L;
    /** key = url，報表對應網址 */
    @JsonProperty(value = "detailMap")
    Map<String, CustomColumnPackTo> detailMap = Maps.newHashMap();

}
