/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.template.component;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import com.cy.tech.request.vo.annotation.TemplateComMapParserDefaultValueAnnotation;
import com.cy.tech.request.vo.enums.ComType;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import com.cy.tech.request.vo.value.to.TemplateDefaultValueTo;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Boolean Radio<BR/>
 * 記錄多彈出的金額部份
 *
 * @author shaun
 */
@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ComRadioTypeTwo implements ComSaba, ComBase, Serializable, ComBaseClear {

    /**
     * 
     */
    private static final long serialVersionUID = 6223031922744354469L;

    /** 元件名稱陣列，於基礎欄位設定中顯示 */
    public List<String> displayComponents = Lists.newArrayList();

    /** 元件欄位名稱 */
    @JsonProperty("name")
    public String name;

    /** 元件Id */
    @JsonProperty("comId")
    public String comId;

    /** 元件type */
    @JsonProperty("type")
    public ComType type;

    /** 實作介面名稱 */
    @JsonProperty("interfaceName")
    public String interfaceName;

    /** 是否需要建立索引 */
    @JsonProperty("needCreateIndex")
    public Boolean needCreateIndex;

    /** 值 1 */
    @TemplateComMapParserDefaultValueAnnotation(key = ONE, valueName = "value01", valueClz = Boolean.class, desc = "單選按鈕預設值 - TRUE = 左 | FALSE=右 | 無預設值")
    @JsonProperty("value01")
    private Boolean value01;

    /** 值 2 radio button 左側描述 */
    @TemplateComMapParserDefaultValueAnnotation(key = TWO, valueName = "value02", valueClz = String.class, desc = "單選按鈕描述(左)")
    @JsonProperty("value02")
    private String value02;

    /** 值 3 radio button 右側描述 */
    @TemplateComMapParserDefaultValueAnnotation(key = THREE, valueName = "value03", valueClz = String.class, desc = "單選按鈕描述(右)")
    @JsonProperty("value03")
    private String value03;

    /** 值 4 radio group 單選按鈕後方輸入框標題 */
    @TemplateComMapParserDefaultValueAnnotation(key = FOUR, valueName = "value04", valueClz = String.class, desc = "單選按鈕後方輸入框標題")
    @JsonProperty("value04")
    private String value04;

    /** 值 5 radio group 單選按鈕後方輸入框 */
    @TemplateComMapParserDefaultValueAnnotation(key = FIVE, valueName = "value05", valueClz = Float.class, desc = "單選按鈕後方輸入框，僅能輸入數字 ex 0 、 0.1 、 1.0")
    @JsonProperty("value05")
    private String value05;

    /** 值 6 單選按鈕後方輸入框後方說明 */
    @TemplateComMapParserDefaultValueAnnotation(key = SIX, valueName = "value06", valueClz = String.class, desc = "單選按鈕後方輸入框後方說明")
    @JsonProperty("value06")
    private String value06;

    /** 錯誤訊息提示暫存 */
    @JsonProperty("errorMsg")
    private String errorMsg;

    @JsonProperty("isSabaComponent")
    private Boolean isSabaComponent;

    @Override
    public void setDefValue(FieldKeyMapping template) {
        name = template.getFieldName();
        comId = template.getComId();
        type = template.getFieldComponentType();
        interfaceName = ComBaseInteractive.class.getSimpleName();
        needCreateIndex = template.getRequiredIndex();
        isSabaComponent = template.getIsSabaComponent();
        this.displayComponents.add(ComBaseInteractive.class.getSimpleName());
        this.displayComponents.add(ComSaba.class.getSimpleName());
        TemplateDefaultValueTo dv = template.getDefaultValue();
        Map<Integer, Object> valueMap = dv.getValue();
        if (valueMap == null) {
            return;
        }
        if (valueMap.containsKey(ONE)) {
            value01 = (Boolean) valueMap.get(ONE);
        }
        if (valueMap.containsKey(TWO)) {
            value02 = (String) valueMap.get(TWO);
        }
        if (valueMap.containsKey(THREE)) {
            value03 = (String) valueMap.get(THREE);
        }
        if (valueMap.containsKey(FOUR)) {
            value04 = (String) valueMap.get(FOUR);
        }
        if (valueMap.containsKey(FIVE)) {
            value05 = (String) valueMap.get(FIVE);
        }
        if (valueMap.containsKey(SIX)) {
            value06 = (String) valueMap.get(SIX);
        }
    }

    @Override
    public void checkValidatorRule() {
        if (Strings.isNullOrEmpty(this.value05)) {
            return;
        }
        this.errorMsg = "";
        Pattern pattern = Pattern.compile("^\\d+(\\.\\d+)?");
        Matcher matcher = pattern.matcher(this.value05);
        if (!matcher.matches()) {
            this.value05 = "";
            this.errorMsg = "請輸入數值格式！！";
        }
    }

    @Override
    public Boolean checkRequireValueIsEmpty() {
        //檢查使用者輸入欄位
        if (this.value01 == null) {
            return Boolean.TRUE;
        }
        //如果為是的話..需連同簽查數值5
        if (this.value01) {
            return WkStringUtils.isEmpty(this.value05);
        }
        return Boolean.FALSE;
    }

    @Override
    public String getCompositionText(boolean showField, int maxFieldNameLen) {
        StringBuilder sb = new StringBuilder();
        if (showField) {
            sb.append(WkStringUtils.getInstance().padEndEmpty(name, maxFieldNameLen, "："));
        }
        if (value01) {
            sb.append(Strings.isNullOrEmpty(value02) ? "是" : value02).append(" ");
            sb.append(Strings.nullToEmpty(value04)).append("：").append(Strings.nullToEmpty(value05)).append(" ");
            sb.append(value06);
        } else {
            sb.append(Strings.isNullOrEmpty(value03) ? "否" : value03).append(" ");
        }
        sb.append("\r\n");
        return sb.toString();
    }

    @Override
    public String getIndexContent() {
        try {
            List<String> idxList = Lists.newArrayList();
            idxList.add(value01 == null ? "" : value01.toString());
            idxList.add(Strings.nullToEmpty(value05));
            return WkJsonUtils.getInstance().toJson(idxList);
        } catch (IOException ex) {
            log.error("parse value to json string fail...");
            log.error("fail value01 = " + value01);
            log.error("fail value05 = " + value05);
            log.error("fail error " + ex.getMessage(), ex);
            return value01 == null ? "" : value01.toString() + " " + value05;
        }
    }
}
