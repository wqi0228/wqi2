/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.value.to;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 自訂報表欄位明細
 *
 * @author shaun
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomColumnDetailTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6107854057242530406L;
    /** 欄位名稱 */
    @JsonProperty(value = "name")
    private String name;
    /** 欄位寬度 */
    @JsonProperty(value = "width")
    private String width;
}
