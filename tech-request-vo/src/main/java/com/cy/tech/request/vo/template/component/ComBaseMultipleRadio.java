package com.cy.tech.request.vo.template.component;

/**
 * ComRadioTypeThree 初始值編輯物件
 *
 * @author shaun
 */
public interface ComBaseMultipleRadio {

    /**
     * 新增Radio
     * @param webNotify 單選按鍵
     */
    public void addRadioGroup(ComWebLayerNotify webNotify);

    /**
     * 刪除Radio
     * @param key key
     */
    public void subRadioGroup(String key);

    /**
     * 渲染單選項目子項目參數列表
     *
     */
    public void renderRadioToParam();

    /**
     * 儲存設定好的項目參數
     */
    public void saveRadioToParam(ComWebLayerNotify webNotify);
}
