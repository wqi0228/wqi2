package com.cy.tech.request.vo.menu;

import com.cy.tech.request.vo.converter.MenuBaseGroupTypeConverter;
import com.cy.tech.request.vo.enums.MenuBaseType;
import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 功能基本群組
 *
 * @author shaun
 */
@Data
@ToString(exclude = {"menuItemGroup"})
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_fun_group_base")
public class TrMenuGroupBase implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4904776281744834961L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "sid")
    private Integer sid;

    @Convert(converter = MenuBaseGroupTypeConverter.class)
    @Column(name = "type", unique = true, nullable = false)
    private MenuBaseType type = MenuBaseType.SETTING;

    @Column(name = "name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "menuGroupBase", fetch = FetchType.EAGER)
    @OrderBy("seq")
    private List<TrMenuItemGroup> menuItemGroup;

}
