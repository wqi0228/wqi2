package com.cy.tech.request.vo.template.component;

import com.cy.tech.request.vo.enums.ComType;
import com.cy.tech.request.vo.template.FieldKeyMapping;

/**
 * 模版內部資料基礎型別
 *
 * @author shaun
 */
public interface ComBase {

    static final int ONE = 1;
    static final int TWO = 2;
    static final int THREE = 3;
    static final int FOUR = 4;
    static final int FIVE = 5;
    static final int SIX = 6;
    static final int SEVEN = 7;
    static final int EIGHT = 8;
    static final int NINE = 9;
    static final int TEN = 10;
    static final int ELE = 11;
    static final int TWE = 12;
    static final int THIRTEEN = 13;
    static final int FOURTEEN = 14;
    static final int FIFTEEN = 15;
    static final int PMIS_IDX = 999;

    /**
     * 取得欄位名稱
     * 
     * @return 欄位名稱
     */
    public String getName();

    /**
     * 取得元件Id
     *
     * @return 元件Id
     */
    public String getComId();

    /**
     * 取得元件型態
     *
     * @return 元件型態
     */
    public ComType getType();

    /**
     * 取得元件實作類型類別名稱
     *
     * @return 元件實作類型類別名稱
     */
    public String getInterfaceName();

    /**
     * 設定模版資料 - 各介面皆需實作
     *
     * @param template
     */
    public void setDefValue(FieldKeyMapping template);

    /**
     * 預設值檢查<BR/>
     * 當模版含有必要輸入時，需進行對應欄位檢測<BR/>
     * 如果應輸入未輸入時，回傳Boolean.TRUE<BR/>
     * 當無可檢查的輸入欄位，但模版設定需要檢查時，將會回傳Null值！！
     *
     * @return 預設值檢查
     */
    public Boolean checkRequireValueIsEmpty();

    /**
     * 是否需建立索引
     *
     * @return 是否需建立索引
     */
    public Boolean getNeedCreateIndex();

    /**
     * 取得建立索引內容
     *
     * @return 索引內容
     */
    public String getIndexContent();

    /**
     * 暫時未考量將indexContent組合變數取出來<BR/>
     * 註:如果未覆寫此方法，會造成jackson在倒回來物件時，<BR/>
     * 因找不到indexContent變數而造成錯誤。
     *
     * @param indexContent
     */
    default void setIndexContent(String indexContent) {
    }

    /**
     * 取得元件排版後文字內容
     *
     * @param showField       是否顯示欄位
     * @param maxFieldNameLen 最長欄位名稱長度
     * @return 元件排版後文字內容
     */
    public String getCompositionText(boolean showField, int maxFieldNameLen);

}
