package com.cy.tech.request.vo.enums;

import com.cy.work.common.utils.WkStringUtils;

import lombok.Getter;

/**
 * 單據類型
 * 
 * @author allen1214_wu
 */
public enum FormType {
    /** 需求單 */
    REQUIRE("需求單", "tr_require_read_record", "require_sid", "tr_require"),
    /**
     * 需求單-草稿 (無閱讀記錄)
     */
    REQUIRE_DREFT("需求單-草稿", "tr_require_read_record", "require_sid", "tr_require"),

    /** 原型確認 */
    PTCHECK("原型確認", "work_pt_check_read_record", "pt_check_sid", "work_pt_check"),

    /** 送測 */
    WORKTESTSIGNINFO("送測", "work_test_info_read_record", "testinfo_sid", "work_test_info"),

    /**
     * 其他資料設定 (無閱讀記錄)
     */
    OTHSET("其他資料設定", "", "", ""),

    /** ON程式 */
    WORKONPG("ON程式", "work_onpg_read_record", "onpg_sid", "work_onpg");

    @Getter
    private String descr;

    /**
     * 閱讀記錄 table 名稱
     */
    @Getter
    private String readRecordTableName;
    /**
     * 閱讀記錄 單據sid 欄位名稱
     */
    @Getter
    private String readRecordFormSidColumnName;
    /**
     * 主檔名稱
     */
    @Getter
    private String mainTableName;

    private FormType(
            String descr,
            String readRecordTableName,
            String readRecordFormColumnName,
            String mainTableName) {
        this.descr = descr;
        this.readRecordTableName = readRecordTableName;
        this.readRecordFormSidColumnName = readRecordFormColumnName;
        this.mainTableName = mainTableName;
    }

    /**
     * @param str
     * @return FormrType
     */
    public static FormType safeValueOf(String str) {
        if (WkStringUtils.notEmpty(str)) {
            for (FormType item : FormType.values()) {
                if (item.name().equals(str)) {
                    return item;
                }
            }
        }
        return null;
    }
}
