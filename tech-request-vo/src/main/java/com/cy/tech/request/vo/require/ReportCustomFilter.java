/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require;

import com.cy.tech.request.vo.converter.ReportCustomFilterConverter;
import com.cy.tech.request.vo.value.to.ReportCustomFilterTo;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author brain0925_liao
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_report_custom_filter")
public class ReportCustomFilter implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5264773002762639617L;
    /** sid */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "tr_report_custom_filter_sid", length = 36)
    private String sid;
    @Column(name = "url", nullable = false)
    private String url;

    @Convert(converter = ReportCustomFilterConverter.class)
    @Column(name = "custom_filter", nullable = true)
    private ReportCustomFilterTo customFilter;

    /** 建立者Sid */
    @Column(name = "create_usr", nullable = false)
    private Integer create_usr;
    /** 建立者日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date create_dt;
    /** 更新者Sid */
    @Column(name = "update_usr", nullable = true)
    private Integer update_usr;
    /** 更新者日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt", nullable = true)
    private Date update_dt;
}
