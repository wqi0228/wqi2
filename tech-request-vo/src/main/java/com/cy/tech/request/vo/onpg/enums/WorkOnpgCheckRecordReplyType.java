/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.onpg.enums;

import com.cy.work.notify.vo.enums.NotifyType;

import lombok.Getter;

/**
 * ON程式檢查記錄回覆類型
 *
 * @author shaun
 */
public enum WorkOnpgCheckRecordReplyType {

	/** 檢查記錄 */
	CHECK_RECORD("檢查記錄", null, null),
	/** 一般回覆 */
	GENERAL_REPLY("一般回覆", NotifyType.REQUEST_GENERAL_REPLY, null),
	/** GM回覆 */
	GM_REPLY("GM回覆", NotifyType.REQUEST_GM_REPLY, NotifyType.REQUEST_GM_AGAIN_REPLY),
	/** QA回覆 */
	QA_REPLY("QA回覆", NotifyType.REQUEST_QA_REPLY, NotifyType.REQUEST_QA_AGAIN_REPLY),
	;

	@Getter
	private String label;

	@Getter
	private NotifyType notifyTypeForCheckRecord;

	@Getter
	private NotifyType notifyTypeForCheckRecordReply;

	private WorkOnpgCheckRecordReplyType(
	        String label,
	        NotifyType notifyTypeForCheckRecord,
	        NotifyType notifyTypeForCheckRecordReply) {
		this.label = label;
		this.notifyTypeForCheckRecord = notifyTypeForCheckRecord;
		this.notifyTypeForCheckRecordReply = notifyTypeForCheckRecordReply;
	}

}
