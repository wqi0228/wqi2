/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require.tros;

import com.cy.tech.request.vo.converter.OthSetAlertTypeConverter;
import com.cy.tech.request.vo.enums.OthSetAlertType;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.vo.converter.ReadRecordTypeConverter;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 其他資訊設定Alert
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "tr_os_alert")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class TrOsAlert implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -926220583052288095L;
    /** Sid */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "os_alert_sid", length = 36)
    private String sid;
    /** 其他設定資訊Sid */
    @Column(name = "os_sid", nullable = false, length = 36)
    private String os_sid;
    /** 其他設定資訊單號 */
    @Column(name = "os_no", nullable = false, length = 21)
    private String os_no;
    /** 需求單Sid */
    @Column(name = "require_sid", nullable = false, length = 36)
    private String require_sid;
    /** 需求單單號 */
    @Column(name = "require_no", nullable = false, length = 21)
    private String require_no;
    /** Alert主題 */
    @Column(name = "os_theme", nullable = false, length = 50)
    private String os_theme;
    /** Alert類型 */
    @Convert(converter = OthSetAlertTypeConverter.class)
    @Column(name = "type", nullable = false)
    private OthSetAlertType type;
    /** 寄件者Sid */
    @Column(name = "send_usr", nullable = false)
    private Integer send_usr;

    /** 寄件日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "send_dt", nullable = false)
    private Date sendDate;
    /** 接收者Sid */
    @Column(name = "receiver", nullable = false)
    private Integer receiver;
    /** 其他設定資訊回覆Sid */
    @Column(name = "os_reply_sid", nullable = true, length = 36)
    private String os_reply_sid;
    /** 其他設定資訊回覆的回覆Sid */
    @Column(name = "os_reply_and_reply_sid", nullable = true, length = 36)
    private String os_reply_and_reply_sid;
    /** 閱讀狀態 */
    @Convert(converter = ReadRecordTypeConverter.class)
    @Column(name = "read_status", nullable = false)
    private ReadRecordType readStatus;

    /** 點擊時間 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "click_time")
    private Date clickTime;
}
