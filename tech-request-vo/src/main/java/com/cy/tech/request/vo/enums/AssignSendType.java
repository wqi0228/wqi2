/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import com.cy.work.common.utils.WkStringUtils;

import lombok.Getter;

/**
 * 分派類型
 *
 * @author shaun
 */
public enum AssignSendType {

    /** 分派 */
    ASSIGN("分派"),
    /** 通知 */
    SEND("通知"),;

    /**
     * 進度條顏色
     */
    @Getter
    private final String label;

    /**
     * @param label
     * @param progressPercent
     * @param progressColer
     */
    private AssignSendType(String label) {
        this.label = label;
    }
    
    public static boolean isContains(String test) {
        for (AssignSendType c : AssignSendType.values()) {
            if (c.name().equals(test)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * @param str
     * @return AssignSendType
     */
    public static AssignSendType safeValueOf(String str) {
        if (WkStringUtils.notEmpty(str)) {
            for (AssignSendType item : AssignSendType.values()) {
                if (item.name().equals(str)) {
                    return item;
                }
            }
        }
        return null;
    }
}
