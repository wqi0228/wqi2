/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.onpg;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.vo.converter.SidOrgConverter;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.common.vo.converter.WorkSourceTypeConverter;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * ON程式檢查記錄回覆
 *
 * @author shaun
 */
@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "work_onpg_check_record_reply")
public class WorkOnpgCheckRecordReply implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3100113816589029626L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "onpg_check_record_reply_sid", length = 36)
    private String sid;

    /** on程式單sid */
    @ManyToOne
    @JoinColumn(name = "onpg_sid")
    private WorkOnpg onpg;

    /** on程式單號 */
    @Column(name = "onpg_no", length = 21)
    private String onpgNo;

    /** on程式來源 */
    @Convert(converter = WorkSourceTypeConverter.class)
    @Column(name = "onpg_source_type")
    private WorkSourceType sourceType;

    /** on程式來源 sid */
    @Column(name = "onpg_source_sid", length = 36)
    private String sourceSid;

    /** on程式來源 單號 */
    @Column(name = "onpg_source_no", length = 21)
    private String sourceNo;

    /** ON程式檢查記錄sid */
    @ManyToOne
    @JoinColumn(name = "onpg_check_record_sid", nullable = false)
    private WorkOnpgCheckRecord checkRecord;

    /** 回覆人員的部門 */
    @Convert(converter = SidOrgConverter.class)
    @Column(name = "reply_person_dep", nullable = false)
    private Org dep;

    /** 回覆人員 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "reply_person_sid", nullable = false)
    private User person;

    /** 回覆日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "reply_dt", nullable = false)
    private Date date;

    /** 更新日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "reply_udt", nullable = false)
    private Date updateDate;

    /** 回覆內容 原文 */
    @Column(name = "reply_content", nullable = false)
    private String content;

    /** 回覆內容 含css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "reply_content_css")
    private String contentCss;

    /** 已回覆歷程 */
    @OneToOne(mappedBy = "checkRecordReply", cascade = CascadeType.PERSIST)
    private WorkOnpgHistory history;

}
