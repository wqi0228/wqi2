/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.worktest.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.cy.tech.request.vo.worktest.enums.WorkTestInfoStatus;
import com.google.common.base.Strings;

import lombok.extern.slf4j.Slf4j;

/**
 * 送測狀態
 *
 * @author shaun
 */
@Slf4j
@Converter
public class WorkTestInfoStatusConverter implements AttributeConverter<WorkTestInfoStatus, String> {

    @Override
    public String convertToDatabaseColumn(WorkTestInfoStatus attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public WorkTestInfoStatus convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return WorkTestInfoStatus.valueOf(dbData);
        } catch (Exception e) {
            log.error("WorkTestStatus Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }

}
