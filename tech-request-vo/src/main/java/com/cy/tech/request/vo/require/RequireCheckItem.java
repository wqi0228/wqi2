package com.cy.tech.request.vo.require;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cy.tech.request.vo.converter.RequireCheckItemTypeConverter;
import com.cy.tech.request.vo.enums.RequireCheckItemType;
import com.cy.work.common.vo.AbstractEntity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 需求單-檢查項檔
 * 
 * @author allen1214_wu
 */
@EqualsAndHashCode(callSuper = false, of = "sid")
@Table(name = "tr_require_check_item")
@Entity
public class RequireCheckItem extends AbstractEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3915185696348075678L;

    /**
     * SID
     */
    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "check_item_sid")
    private Integer sid;

    /**
     * 需求單SID
     */
    @Getter
    @Setter
    @Column(name = "require_sid")
    private String requireSid;

    /**
     * 檢查項目(對應系統別欄位)
     */
    @Getter
    @Setter
    @Convert(converter = RequireCheckItemTypeConverter.class)
    @Column(name = "check_item_type")
    private RequireCheckItemType checkItemType;

    /**
     * 檢查人員 SID
     */
    @Getter
    @Setter
    @Column(name = "check_user_sid")
    private Integer checkUserSid;

    /**
     * 檢查時間
     */
    @Getter
    @Setter
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "check_date")
    private Date checkDate;

    /**
     * 為重構時轉檔轉入 (轉置失敗時復原用)
     */
    @Getter
    @Setter
    @Column(name = "from_trns", nullable = false)
    private boolean fromTrns = false;

}
