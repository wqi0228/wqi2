/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.template.component;

import com.cy.tech.request.vo.annotation.TemplateComMapParserDefaultValueAnnotation;
import com.cy.tech.request.vo.enums.ComType;
import com.cy.tech.request.vo.value.to.TemplateDefaultValueTo;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import com.cy.work.common.utils.WkJsonUtils;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * separtor
 *
 * @author shaun
 */
@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ComSeparatorTypeOne implements ComBase, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5778642031937818082L;

    /** 元件名稱陣列，於基礎欄位設定中顯示 */
    public List<String> displayComponents = Lists.newArrayList();

    /** 元件欄位名稱 */
    @JsonProperty("name")
    public String name;

    /** 元件Id */
    @JsonProperty("comId")
    public String comId;

    /** 元件type */
    @JsonProperty("type")
    public ComType type;

    /** 實作介面名稱 */
    @JsonProperty("interfaceName")
    public String interfaceName;

    /** 是否需要建立索引 */
    @JsonProperty("needCreateIndex")
    public Boolean needCreateIndex;

    @TemplateComMapParserDefaultValueAnnotation(key = ONE, valueName = "value01", valueClz = String.class, desc = "間隔線中間顯示文字。")
    @JsonProperty("value01")
    private String value01;

    @Override
    public void setDefValue(FieldKeyMapping template) {
        name = template.getFieldName();
        comId = template.getComId();
        type = template.getFieldComponentType();
        interfaceName = ComBase.class.getSimpleName();
        needCreateIndex = template.getRequiredIndex();

        TemplateDefaultValueTo dv = template.getDefaultValue();
        Map<Integer, Object> valueMap = dv.getValue();
        if (valueMap == null) {
            return;
        }
        if (valueMap.containsKey(ONE)) {
            value01 = (String) valueMap.get(ONE);
        }
    }

    @Override
    public Boolean checkRequireValueIsEmpty() {
        //設定錯誤
        return Boolean.FALSE;
    }

    @Override
    public String getCompositionText(boolean showField, int maxFieldNameLen) {
        StringBuilder sb = new StringBuilder();
        if (Strings.isNullOrEmpty(value01)) {
            sb.append("===============================").append(" \r\n");
        } else {
            sb.append("=============").append(value01).append("=============").append(" \r\n");
        }
        return sb.toString();
    }

    @Override
    public String getIndexContent() {
        try {
            return WkJsonUtils.getInstance().toJson(Lists.newArrayList(Strings.nullToEmpty(value01)));
        } catch (IOException ex) {
            log.error("parse value to json string fail...");
            log.error("fail value = " + value01, ex);
            return value01;
        }
    }

}
