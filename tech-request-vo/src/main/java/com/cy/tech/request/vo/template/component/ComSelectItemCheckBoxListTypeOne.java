/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.template.component;

import com.cy.work.common.utils.WkStringUtils;
import com.cy.tech.request.vo.annotation.TemplateComMapParserDefaultValueAnnotation;
import com.cy.tech.request.vo.enums.ComType;
import com.cy.tech.request.vo.value.to.TemplateDefaultValueTo;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import com.cy.work.common.utils.WkJsonUtils;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * input text
 *
 * @author shaun
 */
@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ComSelectItemCheckBoxListTypeOne implements ComBase, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -871342101033244081L;

    /** 元件名稱陣列，於基礎欄位設定中顯示 */
    public List<String> displayComponents = Lists.newArrayList();

    /** 元件欄位名稱 */
    @JsonProperty("name")
    public String name;

    /** 元件Id */
    @JsonProperty("comId")
    public String comId;

    /** 元件type */
    @JsonProperty("type")
    public ComType type;

    /** 實作介面名稱 */
    @JsonProperty("interfaceName")
    public String interfaceName;

    /** 是否需要建立索引 */
    @JsonProperty("needCreateIndex")
    public Boolean needCreateIndex;

    /** 值 1 */
    @TemplateComMapParserDefaultValueAnnotation(key = ONE, valueName = "value01", valueClz = ComBaseMapListStr.class, desc = "新增Key值後，選擇下拉列表進行切換，再輸入下方的checkBox選項")
    @JsonProperty("value01")
    private Map<String, List<String>> value01;

    /** 值 2 第一筆預設值 使用者選擇項目 select item key */
    @JsonProperty("value02")
    private String value02;

    /** 值 3 使用者勾選項目 */
    @JsonProperty("value03")
    private List<String> value03;

    @SuppressWarnings("unchecked")
    @Override
    public void setDefValue(FieldKeyMapping template) {
        name = template.getFieldName();
        comId = template.getComId();
        type = template.getFieldComponentType();
        interfaceName = ComBase.class.getSimpleName();
        needCreateIndex = template.getRequiredIndex();
        value03 = Lists.newArrayList();

        TemplateDefaultValueTo dv = template.getDefaultValue();
        Map<Integer, Object> valueMap = dv.getValue();
        if (valueMap == null) {
            return;
        }
        if (valueMap.containsKey(ONE)) {
            value01 = (Map<String, List<String>>) valueMap.get(ONE);
        }
    }

    @Override
    public Boolean checkRequireValueIsEmpty() {
        //檢查使用者使用者勾選項目
        return value03 == null || value03.isEmpty();
    }

    public void change() {
        value03.clear();
    }

    @Override
    public String getCompositionText(boolean showField, int maxFieldNameLen) {
        StringBuilder sb = new StringBuilder();
        if (showField) {
            sb.append(WkStringUtils.getInstance().padEndEmpty(name, maxFieldNameLen, "：")).append("  ").append(value02).append(" \r\n");
        }
        sb.append("\r\n");
        for (String each : value01.get(value02)) {
            if (value03.contains(each)) {
                sb.append("  ").append(" ✔ ").append(each).append("\r\n");
            } else {
                sb.append("  ").append(" 　 ").append(each).append("\r\n");
            }
        }
        sb.append(" \r\n");
        return sb.toString();
    }

    @Override
    public String getIndexContent() {
        try {
            if (value03 == null) {
                value03 = Lists.newArrayList();
            }
            List<String> idxList = Lists.newArrayList();
            idxList.add(Strings.nullToEmpty(value02));
            idxList.addAll(value03);
            return WkJsonUtils.getInstance().toJson(Lists.newArrayList(idxList));
        } catch (IOException ex) {
            log.error("parse value to json string fail...");
            log.error("fail value02 = " + value02);
            log.error("fail value03 = " + value03);
            log.error("fail error " + ex.getMessage(), ex);
            return value02 + " " + value03.toString();
        }
    }
}
