/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import com.cy.tech.request.vo.enums.CateConfirmType;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 檢查確認碼型態
 *
 * @author shaun
 */
@Slf4j
@Converter
public class CateConfirmConverter implements AttributeConverter<CateConfirmType, String> {

    @Override
    public String convertToDatabaseColumn(CateConfirmType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public CateConfirmType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return CateConfirmType.valueOf(dbData);
        } catch (Exception e) {
            log.error("CateConfirmType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }

}
