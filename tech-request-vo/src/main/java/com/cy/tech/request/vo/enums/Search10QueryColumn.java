/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import lombok.extern.slf4j.Slf4j;

/**
 * 未結案單據查詢-自訂搜尋元件底層列舉
 *
 * @author brain0925_liao
 */
@Slf4j
public enum Search10QueryColumn implements SearchReportCustomEnum {
    /** 未結案單據查詢-key */
    Search10Query("未結案單據查詢"),
    /** 需求類別 */
    DemandType("需求類別"),
    /** 需求單位 */
    Department("需求單位"),
    /** 需求製作進度 */
    DemandProcess("需求製作進度"),
    /** 是否閱讀 */
    ReadStatus("是否閱讀"),
    /** 需求人員 */
    DemandPerson("需求人員"),
    /** 類別組合 */
    CategoryCombo("類別組合"),
    /** 立單區間-索引 */
    DateIndex("立單區間-索引"),
    /** 立單區間-起始 */
    StartDate("立單區間-起始"),
    /** 立單區間-結束 */
    EndDate("立單區間-結束"),
    /** 模糊搜尋 */
    SearchText("模糊搜尋");

    /** 欄位名稱 */
    private final String val;

    Search10QueryColumn(String val) {
        this.val = val;
    }

    @Override
    public String getVal() {
        return val;
    }

    @Override
    public String getUrl() {
        return "Search10";
    }

    @Override
    public String getKey() {
        return this.name();
    }

    @Override
    public SearchReportCustomEnum getSearchReportCustomEnumByName(String name) {
        try {
            return Search10QueryColumn.valueOf(name);
        } catch (Exception e) {
            log.error("getSearchReportCustomEnumByName ERROR", e);
        }
        return null;
    }
}
