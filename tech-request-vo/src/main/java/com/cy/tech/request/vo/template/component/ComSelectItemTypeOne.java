/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.template.component;

import com.cy.work.common.utils.WkStringUtils;
import com.cy.tech.request.vo.annotation.TemplateComMapParserDefaultValueAnnotation;
import com.cy.tech.request.vo.enums.ComType;
import com.cy.tech.request.vo.value.to.TemplateDefaultValueTo;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import com.cy.work.common.utils.WkJsonUtils;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * input text
 *
 * @author shaun
 */
@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ComSelectItemTypeOne implements ComBaseInteractive, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3986384262853813738L;

    /** 元件名稱陣列，於基礎欄位設定中顯示 */
    public List<String> displayComponents = Lists.newArrayList();

    /** 元件欄位名稱 */
    @JsonProperty("name")
    public String name;

    /** 元件Id */
    @JsonProperty("comId")
    public String comId;

    /** 元件type */
    @JsonProperty("type")
    public ComType type;

    /** 實作介面名稱 */
    @JsonProperty("interfaceName")
    public String interfaceName;

    /** 是否需要建立索引 */
    @JsonProperty("needCreateIndex")
    public Boolean needCreateIndex;

    /** 值 1 */
    @TemplateComMapParserDefaultValueAnnotation(key = ONE, valueName = "value01", valueClz = ComBaseList.class, desc = "選擇項目設定(多筆)")
    @JsonProperty("value01")
    private List<String> value01;

    /** 值 2 使用者選擇項目 */
    @JsonProperty("value02")
    private String value02;

    @SuppressWarnings("unchecked")
    @Override
    public void setDefValue(FieldKeyMapping template) {
        name = template.getFieldName();
        comId = template.getComId();
        type = template.getFieldComponentType();
        interfaceName = ComBase.class.getSimpleName();
        needCreateIndex = template.getRequiredIndex();

        TemplateDefaultValueTo dv = template.getDefaultValue();
        Map<Integer, Object> valueMap = dv.getValue();
        if (valueMap == null) {
            return;
        }
        if (valueMap.containsKey(ONE)) {
            value01 = (List<String>) valueMap.get(ONE);
        }
    }

    @Override
    public Boolean checkRequireValueIsEmpty() {
        //檢查使用者輸入欄位
        return Strings.isNullOrEmpty(this.value02);
    }

    @Override
    public String getCompositionText(boolean showField, int maxFieldNameLen) {
        StringBuilder sb = new StringBuilder();
        if (showField) {
            sb.append(WkStringUtils.getInstance().padEndEmpty(name, maxFieldNameLen, "：")).append(" ").append(value02);
        }
        sb.append(" \r\n");
        return sb.toString();
    }

    @Override
    public String getIndexContent() {
        try {
            return WkJsonUtils.getInstance().toJson(Lists.newArrayList(Strings.nullToEmpty(value02)));
        } catch (IOException ex) {
            log.error("parse value to json string fail...");
            log.error("fail value = " + value02, ex);
            return value02;
        }
    }

    /**
     * 小類:DBA工作, 選擇站別須帶入主題value01
     */
    @Override
    public void change(ComBase interactCom, TemplateDefaultValueTo interactComValue) {
        if (interactCom == null || !(interactCom instanceof ComTextTypeOne)) {
            return;
        }
        ComTextTypeOne interactTarget = (ComTextTypeOne) interactCom;
        interactTarget.setAppendStr(value02);

    }

}
