package com.cy.tech.request.vo.sp;

import com.cy.commons.enums.Activation;
import com.cy.commons.interfaces.BaseEntity;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.Role;
import com.cy.commons.vo.User;
import com.cy.tech.request.vo.converter.ForwardDepToConverter;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.work.common.vo.converter.SidOrgConverter;
import com.cy.work.common.vo.converter.SidRoleConverter;
import com.cy.tech.request.vo.value.to.SpecificDepTo;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 特殊權限-角色可查詢的部門:sp_role_inquire_dep
 *
 * @author kasim
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "sp_role_inquire_dep")
public class SpecificRoleInquireDep implements Serializable, BaseEntity<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -766504820598408941L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "read_issue_sid", nullable = false)
    private Long sid;

    /** 公司 */
    @Convert(converter = SidOrgConverter.class)
    @Column(name = "comp_sid", nullable = false)
    private Org comp;

    /** 角色 */
    @Convert(converter = SidRoleConverter.class)
    @Column(name = "ad_role_sid", nullable = false)
    private Role role;

    /** 單據狀態 */
    @Column(name = "status", nullable = false)
    private Activation status;

    /** 建立日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    /** 建立人成員 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "create_usr", nullable = false)
    private User createdUser;

    /** 異動日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

    /** 異動成員 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "update_usr")
    private User updatedUser;

    /** 部門Json格式 */
    @Column(name = "dep_sid")
    @Convert(converter = ForwardDepToConverter.class)
    private SpecificDepTo dep;

    /** 角色所歸屬的公司全部的部門都要能閱讀(包含停用） */
    @Column(name = "read_all_dep", nullable = false)
    private Boolean readAllDep;

}
