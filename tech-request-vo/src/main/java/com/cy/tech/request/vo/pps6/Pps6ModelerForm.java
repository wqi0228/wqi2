/**
 * 
 */
package com.cy.tech.request.vo.pps6;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.cy.work.common.vo.AbstractEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * PPS6 Modeler 單據名稱定義資料檔
 * 
 * @author allen1214_wu
 */
@EqualsAndHashCode(callSuper = false, of = "sid")
@Table(name = "pps6_modeler_form")
@Entity
@Getter
@Setter
public class Pps6ModelerForm extends AbstractEntity {

	/**
     * 
     */
    private static final long serialVersionUID = -876218028916705801L;

    /**
	 * SID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "form_sid")
	private Integer sid;
	
	/**
	 * 單據名稱
	 */
	@Column(name = "name")
	private String name;
}
