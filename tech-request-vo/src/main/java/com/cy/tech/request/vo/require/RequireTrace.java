/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require;

import com.cy.commons.enums.Activation;
import com.cy.commons.interfaces.BaseEntity;
import com.cy.commons.vo.User;
import com.cy.tech.request.vo.converter.RequireTraceTypeConverter;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.tech.request.vo.enums.RequireTraceType;
import com.cy.tech.request.vo.require.feedback.ReqFbkReply;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

/**
 * 追蹤紀錄資訊
 *
 * @author shaun
 */
@Data
@ToString(exclude = {"attachments", "replys"})
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_require_trace")
public class RequireTrace implements Serializable, BaseEntity<String> {

    /**
     * 
     */
    private static final long serialVersionUID = 5647048394243671879L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "require_trace_sid", length = 36)
    private String sid;

    /** 需求單主檔 */
    @ManyToOne
    @JoinColumn(name = "require_sid", nullable = false)
    private Require require;

    /** 需求單單號 */
    @Column(name = "require_no", nullable = false, length = 21)
    private String requireNo;

    /** 追蹤類型 */
    @Convert(converter = RequireTraceTypeConverter.class)
    @Column(name = "require_trace_type", nullable = false, length = 45)
    private RequireTraceType requireTraceType;

    /** 追蹤資訊內容 */
    @Column(name = "require_trace_content")
    private String requireTraceContent;

    /** 追蹤資訊內容 css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "require_trace_content_css")
    private String requireTraceContentCss;

    /** 原因 */
    @Column(name = "reason", length = 21)
    private String reason;

    /** 狀態 */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    /** 建立者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "create_usr", nullable = false)
    private User createdUser;

    /** 建立日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    /** 異動者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "update_usr")
    private User updatedUser;

    /** 異動日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

    /** 附加檔案 */
    @OneToMany(mappedBy = "trace", cascade = CascadeType.PERSIST)
    @Where(clause = "status = 0")
    @OrderBy(value = "createdDate DESC")
    private List<RequireFbkAttachment> attachments = Lists.newArrayList();

    /** 需求資訊補充回覆 */
    @OneToMany(mappedBy = "trace", cascade = CascadeType.PERSIST)
    @OrderBy(value = "updateDate DESC")
    private List<ReqFbkReply> replys = Lists.newArrayList();

}
