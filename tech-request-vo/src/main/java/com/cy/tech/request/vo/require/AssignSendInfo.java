/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.tech.request.vo.converter.AssignSendGroupsToConverter;
import com.cy.tech.request.vo.converter.SetupInfoToConverter;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.value.to.AssignSendGroupsTo;
import com.cy.tech.request.vo.value.to.SetupInfoTo;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 需求單分派資訊主檔
 *
 * @author shaun
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = { "sid" })
@Entity
@Table(name = "tr_assign_send_info")
public class AssignSendInfo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1758689226787021040L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "info_sid", length = 36)
    private String sid;

    /** 需求單主檔 */
    @Column(name = "require_sid", nullable = false)
    private String requireSid;

    /** 需求單單號 */
    @Column(name = "require_no", nullable = false, length = 21)
    private String requireNo;

    /** 類型 分派 | 通知 */
    @Column(name = "type", nullable = false, length = 21)
    private AssignSendType type = AssignSendType.ASSIGN;

    /** 分派此批單據的人員 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "create_usr", nullable = false)
    private User createdUser;

    /** 分派此批單據的時間 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    /** 狀態 */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    /** 部門及成員資訊 */
    @Convert(converter = SetupInfoToConverter.class)
    @Column(name = "info", nullable = true)
    private SetupInfoTo info = new SetupInfoTo();

    /** 群組資訊 */
    @Convert(converter = AssignSendGroupsToConverter.class)
    @Column(name = "group_info", nullable = true)
    private AssignSendGroupsTo groupInfo = new AssignSendGroupsTo();

}
