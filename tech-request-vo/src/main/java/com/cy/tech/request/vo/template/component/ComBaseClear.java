/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.template.component;

/**
 * 單純就是為了兩個不同型態的物件做媒合用
 * <BR/>
 * 當初我在寫啥啊！！已忘記當初的原意 20160331 by shaun
 *
 * @author shaun
 */
public interface ComBaseClear {

    /**
     * 檢查輸入格式，進行清除
     */
    public void checkValidatorRule();

    /**
     * 取得錯誤訊息
     *
     * @return 錯誤訊息
     */
    public String getErrorMsg();

    /**
     * 設定錯誤訊息
     *
     * @param msg
     */
    public void setErrorMsg(String msg);
}
