/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.anew.enums;

import lombok.Getter;

/**
 * 簽核層級
 */
public enum SignLevelType {

    /** 不需簽核 */
    NO_SIGN("不需簽核", "", Integer.MIN_VALUE),

    /** 組 */
    THE_PANEL("組", "預設路線-最高至層級41", 41),

    /** 部 */
    MINISTERIAL("部", "預設路線-最高至層級31", 31),

    /** 處 */
    DIVISION_LEVEL("處", "預設路線-最高至層級21", 21),

    /** 群 */
    GROUPS("群", "預設路線-最高至層級11", 11),

    /** 執行長 */
    CEO("執行長", "預設路線-最高至層級1", 1);

    /**
     * 說明
     */
    @Getter
    private final String descr;
    /**
     * BPM簽核層級
     */
    @Getter
    private final String bpmSignLv;

    @Getter
    private final Integer bpmSignLvNum;

    SignLevelType(String descr, String bpmSignLv, Integer bpmSignLvNum) {
        this.descr = descr;
        this.bpmSignLv = bpmSignLv;
        this.bpmSignLvNum = bpmSignLvNum;
    }
    
    /**
     * 取得對應 type
     * @param bpmSignLvNum 簽核層級
     * @return SignLevelType
     */
    public static SignLevelType valueOfByBpmSignLvNum(Integer bpmSignLvNum) {
        for (SignLevelType type : SignLevelType.values()) {
            if(type.getBpmSignLvNum() == bpmSignLvNum) {
                return type;
            }
        }
        return null;
    }
}
