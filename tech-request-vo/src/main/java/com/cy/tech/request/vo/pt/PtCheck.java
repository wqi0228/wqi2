/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.pt;

import com.cy.commons.enums.Activation;
import com.cy.commons.interfaces.BaseEntity;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.converter.JsonStringListToConverter;
import com.cy.work.common.vo.converter.ReadRecordGroupToConverter;
import com.cy.work.common.vo.converter.SidOrgConverter;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.common.vo.converter.WorkSourceTypeConverter;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.cy.work.common.vo.value.to.ReadRecordGroupTo;
import com.cy.tech.request.vo.enums.WaitReadReasonConverter;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.pt.converter.PtStatusConverter;
import com.cy.tech.request.vo.pt.enums.PtStatus;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

/**
 * 原型確認主檔
 *
 * @author shaun
 */
@Data
@NoArgsConstructor
@ToString(exclude = { "noticeMember", "readRecord", "historys", "replys", "attachments" })
@EqualsAndHashCode(of = { "sid" })
@Entity
@Table(name = "work_pt_check")
public class PtCheck implements Serializable, BaseEntity<String> {

    /**
     * 
     */
    private static final long serialVersionUID = -3006080654044249247L;

    /** 原型確認sid */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "pt_check_sid", length = 36)
    private String sid;

    /** 原型確認單號 */
    @Column(name = "pt_check_no", nullable = false, length = 21, unique = true)
    private String ptNo;

    /** 原型確認來源 */
    @Convert(converter = WorkSourceTypeConverter.class)
    @Column(name = "pt_check_source_type", nullable = false)
    private WorkSourceType sourceType;

    /** 原型確認來源 sid */
    @Column(name = "pt_check_source_sid", nullable = false, length = 36)
    private String sourceSid;

    /** 原型確認來源 單號 */
    @Column(name = "pt_check_source_no", nullable = false, length = 21)
    private String sourceNo;

    /** 原型確認通知對象 */
    @Convert(converter = JsonStringListToConverter.class)
    @Column(name = "pt_notice_member", nullable = false)
    private JsonStringListTo noticeMember = new JsonStringListTo();
    
    /**
     * 取得通知人員SID
     * 
     * @return
     */
    public Set<Integer> getNoticeMemberSids() {
        if (this.noticeMember == null || WkStringUtils.isEmpty(this.noticeMember.getValue())) {
            return Sets.newHashSet();
        }
        return noticeMember.getValue().stream()
                .filter(sidStr -> WkStringUtils.isNumber(sidStr))
                .map(sidStr -> Integer.parseInt(sidStr))
                .collect(Collectors.toSet());
    }
    

    /** 原型確認通知部門 */
    @Convert(converter = JsonStringListToConverter.class)
    @Column(name = "pt_notice_deps", nullable = false)
    private JsonStringListTo noticeDeps = new JsonStringListTo();

    /**
     * 取得通知單位SID
     * 
     * @return
     */
    public Set<Integer> getNoticeDepSids() {
        if (noticeDeps == null || WkStringUtils.isEmpty(noticeDeps.getValue())) {
            return Sets.newHashSet();
        }
        return noticeDeps.getValue().stream()
                .filter(sidStr -> WkStringUtils.isNumber(sidStr))
                .map(sidStr -> Integer.parseInt(sidStr))
                .collect(Collectors.toSet());
    }

    /** 是否有流程 */
    @Column(name = "has_sign", nullable = false)
    private Boolean hasSign = Boolean.FALSE;

    /** 原型確認主題 */
    @Column(name = "pt_check_theme", nullable = false, length = 255)
    private String theme;

    /** 原型確認內容 */
    @Column(name = "pt_check_content", nullable = false)
    private String content;

    /** 原型確認內容 含css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "pt_check_content_css", nullable = false)
    private String contentCss;

    /** 備註說明 */
    @Column(name = "pt_check_note")
    private String note;

    /** 備註說明 含css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "pt_check_note_css")
    private String noteCss;

    /** 預計完成日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "pt_check_estimate_dt", nullable = false)
    private Date establishDate;

    /** 實際完成日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "pt_check_finish_dt")
    private Date finishDate;

    /** 原型確認重做日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "pt_redo_dt", nullable = true)
    private Date redoDate;

    /** 此次原型確認版本 */
    @Column(name = "currently_ver", nullable = false)
    private Integer version;

    /** 待閱異動日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "read_update_dt")
    private Date readUpdateDate;

    /** 待閱原因 */
    @Convert(converter = WaitReadReasonConverter.class)
    @Column(name = "read_reason")
    private WaitReadReasonType readReason;

    /** 待閱人員清單 - 閱讀紀錄資訊 */
    @Convert(converter = ReadRecordGroupToConverter.class)
    @Column(name = "read_record")
    private ReadRecordGroupTo readRecord = new ReadRecordGroupTo();

    /** 原型確認狀態 */
    @Convert(converter = PtStatusConverter.class)
    @Column(name = "pt_check_status", nullable = false)
    private PtStatus ptStatus;

    /** 填單人所歸屬的公司 */
    @Convert(converter = SidOrgConverter.class)
    @Column(name = "comp_sid", nullable = false)
    private Org createCompany;

    /** 填單人所歸屬的部門 */
    @Convert(converter = SidOrgConverter.class)
    @Column(name = "dep_sid", nullable = false)
    private Org createDep;

    /** 狀態 */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    /** 建立者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "create_usr", nullable = false)
    private User createdUser;

    /** 建立日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    /** 異動者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "update_usr")
    private User updatedUser;

    /** 異動日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

    /** 原型確認審核資訊 */
    @OneToOne(mappedBy = "ptCheck", cascade = CascadeType.ALL)
    private PtSignInfo signInfo;

    /** 原型確認歷程紀錄 */
    @OneToMany(mappedBy = "ptCheck", cascade = CascadeType.PERSIST)
    @OrderBy("updatedDate DESC")
    @Where(clause = "behavior != 'REPLY_AND_REPLY' ")
    private List<PtHistory> historys = Lists.newArrayList();

    /** 原型確認回覆 */
    @OneToMany(mappedBy = "ptCheck", cascade = CascadeType.ALL)
    private List<PtReply> replys = Lists.newArrayList();

    /** 附加檔案 */
    @OneToMany(mappedBy = "ptCheck", cascade = CascadeType.PERSIST)
    @Where(clause = "status = 0 AND pt_check_history_sid IS NULL")
    @OrderBy(value = "createdDate DESC")
    private List<PtAttachment> attachments = Lists.newArrayList();

}
