/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.template.component;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import com.cy.tech.request.vo.annotation.TemplateComMapParserDefaultValueAnnotation;
import com.cy.tech.request.vo.enums.ComType;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import com.cy.tech.request.vo.value.to.TemplateDefaultValueTo;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Editor
 *
 * @author shaun
 */
@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(callSuper = false)
public class ComEditorTypeOne extends AbstractComBase implements ComBaseCss, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1711348002967751886L;

    /** 元件名稱陣列，於基礎欄位設定中顯示 */
    public List<String> displayComponents = Lists.newArrayList();

    /** 元件欄位名稱 */
    @JsonProperty("name")
    public String name;

    /** 元件Id */
    @JsonProperty("comId")
    public String comId;

    /** 元件type */
    @JsonProperty("type")
    public ComType type;

    /** 實作介面名稱 */
    @JsonProperty("interfaceName")
    public String interfaceName;

    /** 是否需要建立索引 */
    @JsonProperty("needCreateIndex")
    public Boolean needCreateIndex;

    private final static String WARP_TEMP = "%newLine%";
    private final static String JUMP_LINE = "\r\n";

    /** 值 1 css */
    @TemplateComMapParserDefaultValueAnnotation(key = ONE, valueName = "value01", valueClz = String.class, desc = "輸入框內容 可接受HTML語法")
    @JsonProperty("value01")
    private String value01;

    /** 值 2 不含css的純文字資料 */
    @JsonProperty("value02")
    private String value02;

    /** 值 3 元件高度調整 */
    @TemplateComMapParserDefaultValueAnnotation(key = THREE, valueName = "value03", valueClz = String.class, desc = "元件height值，預設:235")
    @JsonProperty("value03")
    private String value03;

    @Override
    public void setDefValue(FieldKeyMapping template) {
        name = template.getFieldName();
        comId = template.getComId();
        type = template.getFieldComponentType();
        interfaceName = ComBaseCss.class.getSimpleName();
        needCreateIndex = template.getRequiredIndex();

        TemplateDefaultValueTo dv = template.getDefaultValue();
        Map<Integer, Object> valueMap = dv.getValue();
        if (valueMap == null) {
            return;
        }
        if (valueMap.containsKey(ONE)) {
            value01 = (String) valueMap.get(ONE);
        }
        if (valueMap.containsKey(THREE)) {
            value03 = (String) valueMap.get(THREE);
        }
    }

    @Override
    public Boolean checkRequireValueIsEmpty() {
        // 檢查使用者輸入欄位
        this.change();
        return WkStringUtils.isEmpty(value02);
    }

    /**
     * 儲存無HTML文字
     */
    public void change() {
        if (Strings.isNullOrEmpty(value01)) {
            value02 = "";
            return;
        }
        this.value02 = WkJsoupUtils.getInstance().htmlToText(this.value01);
    }

    @Override
    public String catchCssText() {
        return value01;
    }

    @Override
    public void clearCssText() {
        this.change();
        value01 = "";
    }

    @Override
    public void resetCssText(String css) {
        value01 = css;
    }

    @Override
    public String getCompositionText(boolean showField, int maxFieldNameLen) {
        StringBuilder sb = new StringBuilder();
        if (showField) {
            sb.append(WkStringUtils.getInstance().padEndEmpty(name, maxFieldNameLen, "：")).append(JUMP_LINE);
        }
        // String fbText = this.changeToFBText();
        sb.append(WkJsoupUtils.getInstance().htmlToText(this.value01));
        sb.append(JUMP_LINE);
        return sb.toString();
    }

    @SuppressWarnings("unused")
    @Deprecated
    private String changeToFBText() {
        if (Strings.isNullOrEmpty(value01)) {
            return "";
        }
        String temp = value01;

        temp = temp.replaceAll(JUMP_LINE, WARP_TEMP);
        temp = temp.replaceAll("\r", WARP_TEMP);
        temp = temp.replaceAll("\n", WARP_TEMP);
        temp = temp.replaceFirst("<div>", WARP_TEMP);
        temp = temp.replaceAll("</div>", WARP_TEMP);
        temp = temp.replaceAll("<BR>", WARP_TEMP);
        temp = temp.replaceAll("<br>", WARP_TEMP);
        temp = temp.replaceAll("</BR>", WARP_TEMP);
        temp = temp.replaceAll("</br>", WARP_TEMP);
        temp = WkJsoupUtils.getInstance().clearCssTag(temp);
        temp = temp.replaceAll(WARP_TEMP, JUMP_LINE);
        return temp;
    }

    @Override
    public String getIndexContent() {
        if (WkStringUtils.isEmpty(this.value01)) {
            return "";
        }

        try {
            return WkJsonUtils.getInstance().toJson(
                    Lists.newArrayList(
                            Strings.nullToEmpty(
                                    WkJsoupUtils.getInstance().htmlToText(this.value01))));
        } catch (IOException ex) {
            log.error("parse value to json string fail...");
            log.error("fail value = " + value02, ex);
            return value02;
        }
    }

    @Override
    public String transferToPmisValue() {
        return value01;
    }

}
