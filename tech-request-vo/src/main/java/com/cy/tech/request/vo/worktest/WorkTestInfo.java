/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.worktest;

import com.cy.commons.enums.Activation;
import com.cy.commons.interfaces.BaseEntity;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.vo.enums.WaitReadReasonConverter;
import com.cy.tech.request.vo.enums.WaitReadReasonType;
import com.cy.tech.request.vo.worktest.converter.WorkTestInfoStatusConverter;
import com.cy.tech.request.vo.worktest.converter.WorkTestStatusConverter;
import com.cy.tech.request.vo.worktest.enums.WorkTestInfoStatus;
import com.cy.tech.request.vo.worktest.enums.WorkTestStatus;
import com.cy.work.common.vo.converter.JsonStringListToConverter;
import com.cy.work.common.vo.converter.ReadRecordGroupToConverter;
import com.cy.work.common.vo.converter.SidOrgConverter;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.cy.work.common.vo.value.to.ReadRecordGroupTo;
import com.cy.work.common.vo.converter.WorkSourceTypeConverter;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

/**
 * 送測主檔
 *
 * @author shaun
 */
@Data
@NoArgsConstructor
@ToString(exclude = {"qaReport", "infoHistorys", "replys", "attachments"})
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "work_test_info")
public class WorkTestInfo implements Serializable, BaseEntity<String> {

    /**
     * 
     */
    private static final long serialVersionUID = -8315148491816921032L;

    /** 送測單sid */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "testinfo_sid", length = 36)
    private String sid;

    /** 送測單號 */
    @Column(name = "testinfo_no", nullable = false, length = 21, unique = true)
    private String testinfoNo;

    /** 送測來源 */
    @Convert(converter = WorkSourceTypeConverter.class)
    @Column(name = "testinfo_source_type", nullable = false)
    private WorkSourceType sourceType;

    /** 送測來源 sid */
    @Column(name = "testinfo_source_sid", nullable = false, length = 36)
    private String sourceSid;

    /** 送測來源 單號 */
    @Column(name = "testinfo_source_no", nullable = false, length = 21)
    private String sourceNo;

    /** 挑選的送測單位 */
    @Convert(converter = JsonStringListToConverter.class)
    @Column(name = "testinfo_deps", nullable = false)
    private JsonStringListTo sendTestDep = new JsonStringListTo();
    
    /**
     * 取得送測單位SID
     * 
     * @return
     */
    public Set<Integer> getSendTestDepSids() {
        if (this.sendTestDep == null || WkStringUtils.isEmpty(this.sendTestDep.getValue())) {
            return Sets.newHashSet();
        }
        return this.sendTestDep.getValue().stream()
                .filter(sidStr -> WkStringUtils.isNumber(sidStr))
                .map(sidStr -> Integer.parseInt(sidStr))
                .collect(Collectors.toSet());
    }

    /** 是否有流程 */
    @Column(name = "has_sign", nullable = false)
    private Boolean hasSign = Boolean.FALSE;

    /** 送測主題 */
    @Column(name = "testinfo_theme", nullable = false, length = 255)
    private String theme;

    /** 送測內容 */
    @Column(name = "testinfo_content", nullable = false)
    private String content;

    /** 送測內容 含css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "testinfo_content_css", nullable = false)
    private String contentCss;

    /** 備註說明 */
    @Column(name = "testinfo_note")
    private String note;

    /** 備註說明 含css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "testinfo_note_css")
    private String noteCss;

    /** 預計完成日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "testinfo_estimate_dt", nullable = false)
    private Date establishDate;

    /** 送測日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "test_date", nullable = false)
    private Date testDate;

    /** 預設上線日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "expect_online_date")
    private Date expectOnlineDate;

    /** QA審核狀態 */
    @Convert(converter = WorkTestInfoStatusConverter.class)
    @Column(name = "qa_audit_status", nullable = false)
    private WorkTestInfoStatus qaAuditStatus;
    
    /** QA審核日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "qa_audit_dt")
    private Date qaAuditDate;
    
    /** 提交狀態 */
    @Convert(converter = WorkTestInfoStatusConverter.class)
    @Column(name = "commit_status", nullable = false)
    private WorkTestInfoStatus commitStatus;

    /** 是否納入排程 */
    @Convert(converter = WorkTestInfoStatusConverter.class)
    @Column(name = "qa_schedule_status", nullable = false)
    private WorkTestInfoStatus qaScheduleStatus;

    /** QA連結顯示名稱 */
    @Column(name = "qa_link_name")
    private String qaLinkName;

    /** QA搜尋連結 */
    @Column(name = "qa_search_link")
    private String qaSearchLink;
    
    /** 主測人員 */
    @Column(name = "master_testers")
    private String masterTesters;
    
    /** 協測人員 */
    @Column(name = "slave_testers")
    private String slaveTesters;
    
    /** 排定完成日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "schedule_finish_date")
    private Date scheduleFinishDate;

    /** 實際完成日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "testinfo_finish_dt")
    private Date finishDate;

    /** 送測取消日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "testinfo_cancel_dt")
    private Date cancelDate;

    /** 待閱異動日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "read_update_dt")
    private Date readUpdateDate;

    /** 待閱原因 */
    @Convert(converter = WaitReadReasonConverter.class)
    @Column(name = "read_reason")
    private WaitReadReasonType readReason;

    /** 待閱人員清單 - 閱讀紀錄資訊 */
    @Convert(converter = ReadRecordGroupToConverter.class)
    @Column(name = "read_record")
    private ReadRecordGroupTo readRecord = new ReadRecordGroupTo();

    /** 送測狀態 */
    @Convert(converter = WorkTestStatusConverter.class)
    @Column(name = "testinfo_status", nullable = false)
    private WorkTestStatus testinfoStatus;

    /** FB Case No */
    @Column(name = "fb_id", length = 11)
    private Integer fbId;

    /** 填單人所歸屬的公司 */
    @Convert(converter = SidOrgConverter.class)
    @Column(name = "comp_sid", nullable = false)
    private Org createCompany;

    /** 填單人所歸屬的部門 */
    @Convert(converter = SidOrgConverter.class)
    @Column(name = "dep_sid", nullable = false)
    private Org createDep;

    /** 狀態 */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    /** 建立者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "create_usr", nullable = false)
    private User createdUser;

    /** 建立日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    /** 異動者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "update_usr")
    private User updatedUser;

    /** 異動日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

    /** 送測審核資訊 */
    @OneToOne(mappedBy = "testInfo", cascade = CascadeType.ALL)
    private WorkTestSignInfo signInfo;

    /** QA測試報告 */
    @OneToMany(mappedBy = "testInfo", cascade = CascadeType.ALL)
    private List<WorkTestQAReport> qaReport = Lists.newArrayList();

    /** 送測歷程紀錄 */
    @OneToMany(mappedBy = "testInfo", cascade = CascadeType.PERSIST)
    @OrderBy("updatedDate DESC")
    @Where(clause = "behavior != 'REPLY_AND_REPLY' ")
    private List<WorkTestInfoHistory> infoHistorys = Lists.newArrayList();

    /** 送測回覆 */
    @OneToMany(mappedBy = "testInfo", cascade = CascadeType.ALL)
    private List<WorkTestReply> replys = Lists.newArrayList();

    /** 轉入FB的網址 - 取出entity時，自行塞入 */
    @Transient
    private String forwardToFbUrl;

    /** 附加檔案 */
    @OneToMany(mappedBy = "testInfo", cascade = CascadeType.PERSIST)
    @Where(clause = "status = 0 AND testinfo_history_sid IS NULL")
    @OrderBy(value = "createdDate DESC")
    private List<WorkTestAttachment> attachments = Lists.newArrayList();

    @Transient
    private InplaceControl inplaceControl = new InplaceControl();
    
}
