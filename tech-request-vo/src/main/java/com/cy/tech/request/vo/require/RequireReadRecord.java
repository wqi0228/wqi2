package com.cy.tech.request.vo.require;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 需求單閱讀記錄
 * 
 * @author allen1214_wu
 */
@NoArgsConstructor
@EqualsAndHashCode(of = { "sid" }, callSuper = false)
@Entity
@Table(name = "tr_require_read_record")
public class RequireReadRecord implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 7721579986142130093L;

    /**
     * SID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "record_sid")
    @Getter
    @Setter
    private Long sid;

    /**
     * 需求單 SID
     */
    @Column(name = "require_sid", nullable = false, length = 36)
    @Getter
    @Setter
    private String requireSid;

    /**
     * 使用者 SID
     */
    @Column(name = "user_sid", nullable = false)
    @Getter
    @Setter
    private Integer userSid;

    /**
     * 分派單位負責人 SID
     */
    @Column(name = "is_wait_read", nullable = false)
    @Getter
    @Setter
    private boolean waitRead = false;

    /**
     * 讀取時間
     */
    @Getter
    @Setter
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "read_dt")
    private Date readDate;
}
