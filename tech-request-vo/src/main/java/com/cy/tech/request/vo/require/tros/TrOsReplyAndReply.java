/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require.tros;

import com.cy.work.common.vo.converter.StringBlobConverter;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "tr_os_reply_and_reply")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class TrOsReplyAndReply implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -248507669280568566L;
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "os_reply_and_reply_sid", length = 36)
    private String sid;
    @Column(name = "os_reply_sid", nullable = false, length = 36)
    private String os_reply_sid;

    @Column(name = "os_sid", nullable = false, length = 36)
    private String os_sid;

    @Column(name = "os_no", nullable = false, length = 21)
    private String os_no;

    @Column(name = "require_sid", nullable = false, length = 36)
    private String require_sid;

    @Column(name = "require_no", nullable = false, length = 21)
    private String require_no;

    @Column(name = "reply_person_dep", nullable = false)
    private Integer reply_person_dep;

    @Column(name = "reply_person_sid", nullable = false)
    private Integer reply_person_sid;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "reply_dt", nullable = false)
    private Date reply_dt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "reply_udt", nullable = false)
    private Date reply_udt;

    @Column(name = "reply_content")
    private String reply_content;

    @Convert(converter = StringBlobConverter.class)
    @Column(name = "reply_content_css")
    private String reply_content_css;
}
