/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.template.component.to;

import com.cy.tech.request.vo.annotation.TemplateComMapParserDefaultValueAnnotation;
import com.cy.tech.request.vo.template.component.ComSaba;
import com.cy.tech.request.vo.value.to.TemplateComMapParserTo;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 基礎Radio集合體
 *
 * @author shaun
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ComRadioTo implements ComSaba, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -468354959489980555L;

    /** Radio名稱(編輯預設值時識別用) */
    @TemplateComMapParserDefaultValueAnnotation(key = 0, valueName = "value00", valueClz = String.class, desc = "單選項目名稱", canEdit = true)
    @JsonProperty("value00")
    private String value00;

    /** TRUE OR FALSE */
    @TemplateComMapParserDefaultValueAnnotation(key = 1, valueName = "value01", valueClz = Boolean.class, desc = "單選預設值 - TRUE = 左 | FALSE=右 | 無預設值")
    @JsonProperty("value01")
    private Boolean value01;

    /** TRUE值 文字描述 */
    @TemplateComMapParserDefaultValueAnnotation(key = 2, valueName = "value02", valueClz = String.class, desc = "單選描述(左) 預設值 = 是")
    @JsonProperty("value02")
    private String value02;

    /** FALSE值 文字描述 */
    @TemplateComMapParserDefaultValueAnnotation(key = 3, valueName = "value03", valueClz = String.class, desc = "單選描述(右) 預設值 = 否")
    @JsonProperty("value03")
    private String value03;

    /** 群組 - 文字描述 */
    @TemplateComMapParserDefaultValueAnnotation(key = 4, valueName = "value04", valueClz = String.class, desc = "單選項目描述標籤(最後面)")
    @JsonProperty("value04")
    private String value04;

    /** 左值互動描述(TRUE) */
    @TemplateComMapParserDefaultValueAnnotation(key = 5, valueName = "value05", valueClz = String.class, desc = "左值互動描述(TRUE)需指定互動元件才生效")
    @JsonProperty("value05")
    private String value05;

    /** 右值互動描述(FALSE) */
    @TemplateComMapParserDefaultValueAnnotation(key = 6, valueName = "value06", valueClz = String.class, desc = "右值互動描述(FALSE)需指定互動元件才生效")
    @JsonProperty("value06")
    private String value06;

    @TemplateComMapParserDefaultValueAnnotation(key = 7, valueName = "isSabaComponent", valueClz = Boolean.class, desc = "是否為沙巴元件")
    @JsonProperty("isSabaComponent")
    private Boolean isSabaComponent = false;

    public ComRadioTo(String value00) {
        this.value00 = value00;
    }

    /**
     * 填入編輯模版元件值(新建或已有)
     *
     * @param parsertTos
     */
    public void resetParam(List<TemplateComMapParserTo> parsertTos) {
        parsertTos.forEach(each -> {
            if (each.getKey() == 0) {
                each.setValue(this.value00);
            }
            if (each.getKey() == 1) {
                if (this.value01 != null) {
                    each.setValue(this.value01);
                } else {
                    each.setValue(null);
                }
            }
            if (each.getKey() == 2) {
                each.setValue(this.value02);
            }
            if (each.getKey() == 3) {
                each.setValue(this.value03);
            }
            if (each.getKey() == 4) {
                each.setValue(this.value04);
            }
            if (each.getKey() == 5) {
                each.setValue(this.value05);
            }
            if (each.getKey() == 6) {
                each.setValue(this.value06);
            }
            if (each.getKey() == 7) {
                each.setValue(this.isSabaComponent);
            }
        });
    }

    /**
     * 儲存設定好的單選項目參數
     *
     * @param parsertTos
     */
    public void saveRadioToParam(List<TemplateComMapParserTo> parsertTos) {
        parsertTos.forEach(each -> {
            if (each.getKey() == 0) {
                if (each.getValue() != null) {
                    this.value00 = (String) each.getValue();
                }
            }
            if (each.getKey() == 1) {
                if (each.getValue() != null) {
                    this.value01 = Boolean.valueOf((String) each.getValue());
                } else {
                    this.value01 = null;
                }
            }
            if (each.getKey() == 2) {
                if (each.getValue() != null) {
                    this.value02 = (String) each.getValue();
                }
            }
            if (each.getKey() == 3) {
                if (each.getValue() != null) {
                    this.value03 = (String) each.getValue();
                }
            }
            if (each.getKey() == 4) {
                if (each.getValue() != null) {
                    this.value04 = (String) each.getValue();
                }
            }
            if (each.getKey() == 5) {
                if (each.getValue() != null) {
                    this.value05 = (String) each.getValue();
                }
            }
            if (each.getKey() == 6) {
                if (each.getValue() != null) {
                    this.value06 = (String) each.getValue();
                }
            }
            if (each.getKey() == 7) {
                if (each.getValue() != null) {
                    this.isSabaComponent = Boolean.valueOf((String) each.getValue());
                } else {
                    this.isSabaComponent = null;
                }
            }
        });
    }

}
