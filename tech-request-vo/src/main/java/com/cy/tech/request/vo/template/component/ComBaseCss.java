/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.template.component;

/**
 * 模版內部資料基礎型別 - 含CSS
 *
 * @author shaun
 */
public interface ComBaseCss extends ComBase {

    /**
     * 資料存檔時期取得css文字，並另存資料庫
     *
     * @return css文字
     */
    public String catchCssText();

    /**
     * 資料存檔完畢時移除css文字，實作時要記得將純文字存至value02
     */
    public void clearCssText();

    /**
     * 資料導入時期將css設定回去
     *
     * @param css
     */
    public void resetCssText(String css);

}
