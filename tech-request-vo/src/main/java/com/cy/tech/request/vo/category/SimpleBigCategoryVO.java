package com.cy.tech.request.vo.category;

import com.cy.tech.request.vo.enums.ReqCateType;
import com.cy.work.common.utils.WkEntityUtils;
import com.cy.work.common.vo.AbstractEntity;

import java.io.Serializable;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;

/**
 * 01_類別基本資料(類別)
 */
public class SimpleBigCategoryVO extends AbstractEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7260397284576121221L;

    /**
     * @param simpleBigCategory
     */
    public SimpleBigCategoryVO(SimpleBigCategory simpleBigCategory) {
        if (simpleBigCategory == null) {
            return;
        }
        WkEntityUtils.getInstance().copyProperties(simpleBigCategory, this);
    }

    @Id
    @Getter
    @Setter
    private String sid;

    /**
     * 代碼 - 由系統管理員編碼
     */
    @Getter
    @Setter
    private String id;

    /**
     * 名稱
     */
    @Getter
    @Setter
    private String name;

    /**
     * 備註事項
     */
    @Getter
    @Setter
    private String note;

    /**
     * 排序
     */
    @Getter
    @Setter
    private Integer seq;

    /**
     * 
     */
    @Getter
    @Setter
    private ReqCateType reqCateType;

    /**
     * 
     */
    @Getter
    @Setter
    private String permissionRole;
}
