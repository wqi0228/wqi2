/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.tech.request.vo.value.to.AssignSendGroupsTo;
import com.google.common.base.Strings;
import java.io.IOException;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 分派通知設定群組轉換器
 *
 * @author shaun
 */
@Slf4j
@Converter
public class AssignSendGroupsToConverter implements AttributeConverter<AssignSendGroupsTo, String> {

    @Override
    public String convertToDatabaseColumn(AssignSendGroupsTo attribute) {
        try {
            if (attribute == null) {
                return "";
            }
            return WkJsonUtils.getInstance().toJson(attribute);
        } catch (IOException ex) {
            log.error("parser AssignSendGroupsTo to json fail!!" + ex.getMessage());
        }
        return "";
    }

    @Override
    public AssignSendGroupsTo convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return new AssignSendGroupsTo();
        }
        try {
            return WkJsonUtils.getInstance().fromJson(dbData, AssignSendGroupsTo.class);
        } catch (IOException ex) {
            log.error("parser json to AssignSendGroupsTo fail!!" + ex.getMessage());
            return null;
        }
    }
}
