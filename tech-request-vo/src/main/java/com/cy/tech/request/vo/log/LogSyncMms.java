/**
 * 
 */
package com.cy.tech.request.vo.log;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.cy.tech.request.vo.converter.LogSyncMmsActionTypeConverter;
import com.cy.work.client.syncmms.to.formdata.SyncMmsFormDataModel;
import com.cy.work.client.syncmms.to.formdata.SyncMmsFormDataModelConverter;
import com.cy.work.client.syncmms.to.processresult.SyncMmsProcessResultModel;
import com.cy.work.client.syncmms.to.processresult.SyncMmsProcessResultModelConverter;
import com.cy.work.common.vo.AbstractEntity;
import com.cy.work.common.vo.converter.BooleanYNConverter;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * MMS維護單同步記錄 Entity
 * 
 * @author allen1214_wu
 */
@EqualsAndHashCode(callSuper = false, of = "sid")
@Table(name = "tr_log_sync_mms")
@Entity
public class LogSyncMms extends AbstractEntity {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 2516161094401698095L;

    /**
     * SID
     */
    @Getter
    @Setter
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "log_sid", length = 36)
    private String sid;

    /**
     * 是否執行成功 Y/N
     */
    @Getter
    @Setter
    @Convert(converter = BooleanYNConverter.class)
    @Column(name = "is_success", nullable = false)
    private boolean success = true;
    
    /**
     * 當本次執行失敗時，是否重新被執行了 (重新執行後，不管成功失敗，都將同一工作單號者， update 成 1)
     */
    @Getter
    @Setter
    @Column(name = "is_sync_again", nullable = false)
    private boolean syncAgain = false;

    /**
     * 動作類別
     */
    @Getter
    @Setter
    @Convert(converter = LogSyncMmsActionTypeConverter.class)
    @Column(name = "action_type", nullable = false)
    private LogSyncMmsActionType actionType;

    /**
     * 需求單 SID
     */
    @Getter
    @Setter
    @Column(name = "require_sid", length = 36)
    private String requireSid;

    /**
     * on程式單 SID
     */
    @Getter
    @Setter
    @Column(name = "onpg_sid", length = 36)
    private String onpgSid;
    
    /**
     * MMS單號
     */
    @Getter
    @Setter
    @Column(name = "mms_id" )
    private String mmsID;

    /**
     * 資料修改時間 (MMS 主機回傳)
     */
    @Getter
    @Setter
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mms_data_update_dt")
    private Date mmsDateTime;

    /**
     * 由MMS收到的完整資料
     */
    @Getter
    @Setter
    @Convert(converter = SyncMmsFormDataModelConverter.class)
    @Column(name = "mms_receive_data")
    private SyncMmsFormDataModel mmsReceiveData;

    /**
     * 回應給MMS的處理結果資料
     */
    @Getter
    @Setter
    @Convert(converter = SyncMmsProcessResultModelConverter.class)
    @Column(name = "mms_response_data")
    private SyncMmsProcessResultModel processResult;

    /**
     * 備註
     */
    @Getter
    @Setter
    @Column(name = "memo")
    private String memo;

}
