/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.exception;

/**
 *
 * @author jason_h
 */
public class LockRecordException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -253488368842743723L;

    public LockRecordException(String message) {
        super(message);
    }

    public LockRecordException(String message, Throwable cause) {
        super(message, cause);
    }
}
