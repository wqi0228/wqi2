/**
 * 
 */
package com.cy.tech.request.vo.enums;

import lombok.Getter;

/**
 * 快取資料類型
 * 
 * @author allen1214_wu
 */
public enum EhCacheType {

	//====================================
	//長快取
	//====================================
	TEMP_FIELD("需求單模版", false),
	PORTAL_MENU("需求單入口選單", false),
	CATE_KEY_MAPPING("需求單類別模版Key", false),
	FIND_ACTIVE_BIG_CATEGORY("需求單查詢所有大類", false),
	PREPARE_REQUIRE_CONFIRM_DEP("需求確認單位計算結果", false),
	WORK_PARAM("工作紀錄參數", false),
	
	//====================================
	//短快取
	//====================================
	ALL_CATEGORY_PICKER("所有類別組合", true),
	
	;

	/**
     * 說明
     */
    @Getter
    private String descr;

	/**
	 * 是否為短快取
	 */
	@Getter
    private boolean shortCeahe;

    private EhCacheType(String descr, boolean shortCeahe) {
        this.descr = descr;
        this.shortCeahe = shortCeahe;
    }
    
    
//    types.put(CacheConstants.CACHE_TEMP_FIELD, "需求單模版");
//    types.put(CacheConstants.CACHE_PORTAL_MENU, "需求單入口選單");
//    types.put(CacheConstants.CACHE_CATE_KEY_MAPPING, "需求單類別模版Key");
//    types.put(CacheConstants.CACHE_findActiveBigCategory, "需求單查詢所有大類");
//    types.put(CacheConstants.CACHE_prepareRequireConfirmDep, "需求確認單位計算結果");
//    types.put(WkBackendConstants.CACHE_WORK_PARAM, "工作紀錄參數");
}
