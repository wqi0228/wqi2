package com.cy.tech.request.vo.value.to;

import java.util.Date;

import com.cy.tech.request.vo.setting.WorkTestSchedule;

import lombok.Data;

@Data
public class WorkTestScheduleVO {
    private String sid;
    private String scheduleName = "滿";
    private Date startDate;
    private Date endDate;
    private String colorCode = "D50000";
    private String isFull = "Y";
    
    private String eventId;
    
    public WorkTestScheduleVO(){
        
    }
    
    public WorkTestScheduleVO(WorkTestSchedule model){
        this.sid = model.getSid();
        this.eventId = model.getEventId();
        this.scheduleName = model.getScheduleName();
        this.startDate = model.getStartDate();
        this.endDate = model.getEndDate();
        this.colorCode = model.getColorCode();
        this.isFull = model.isFull() ? "Y" : "N";
    }
}
