/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.template.component;

import com.cy.work.common.utils.WkStringUtils;
import com.cy.tech.request.vo.annotation.TemplateComMapParserDefaultValueAnnotation;
import com.cy.tech.request.vo.enums.ComType;
import com.cy.tech.request.vo.value.to.TemplateDefaultValueTo;
import com.cy.tech.request.vo.template.FieldKeyMapping;
import com.cy.work.common.utils.WkJsonUtils;
import org.codehaus.jackson.annotate.JsonProperty;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * input text
 *
 * @author shaun
 */
@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(callSuper = false)
public class ComTextTypeOne extends AbstractComBase implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2031235588548361980L;

    /** 元件名稱陣列，於基礎欄位設定中顯示 */
    public List<String> displayComponents = Lists.newArrayList();

    /** 元件欄位名稱 */
    @JsonProperty("name")
    public String name;

    /** 元件Id */
    @JsonProperty("comId")
    public String comId;

    /** 元件type */
    @JsonProperty("type")
    public ComType type;

    /** 實作介面名稱 */
    @JsonProperty("interfaceName")
    public String interfaceName;

    /** 是否需要建立索引 */
    @JsonProperty("needCreateIndex")
    public Boolean needCreateIndex;

    /** 值 1 */
    @TemplateComMapParserDefaultValueAnnotation(key = ONE, valueName = "value01", valueClz = String.class, desc = "標籤顯示，使用者輸入左方。")
    @JsonProperty("value01")
    private String value01;

    /** 值 2 */
    @TemplateComMapParserDefaultValueAnnotation(key = TWO, valueName = "value02", valueClz = String.class, desc = "使用者輸入欄位。")
    @JsonProperty("value02")
    private String value02;

    /** 值 3 */
    @TemplateComMapParserDefaultValueAnnotation(key = THREE, valueName = "value03", valueClz = String.class, desc = "標籤顯示，使用者輸入右方。")
    @JsonProperty("value03")
    private String value03;

    /** 值4 */
    @TemplateComMapParserDefaultValueAnnotation(key = FOUR, valueName = "value04", valueClz = String.class, desc = "使用者輸入欄位，輸入提示")
    @JsonProperty("value04")
    private String value04;

    /** 值5 */
    @TemplateComMapParserDefaultValueAnnotation(key = FIVE, valueName = "value05", valueClz = String.class, desc = "欄位寬度(px or %) 預設 80%")
    @JsonProperty("value05")
    private String value05;

    /**
     * 透過互動元件選擇的值，append to value01
     */
    private String appendStr = "";
    /**
     * 廳主名稱@Code
     */
    private String customerNameCode;
    
    /**
     * 設定互動元件，value01串接互動元件的值(appendStr) for頁面的主題欄位, 顯示return值 (小類:DBA工作)
     */
    public String getFinalValue() {
        if (!Strings.isNullOrEmpty(value01)) {
            StringBuilder sb = new StringBuilder(value01 + " " + appendStr);
            return sb.toString();
        }
        return appendStr;
    }

    @Override
    public void setDefValue(FieldKeyMapping template) {
        name = template.getFieldName();
        comId = template.getComId();
        type = template.getFieldComponentType();
        interfaceName = ComBase.class.getSimpleName();
        needCreateIndex = template.getRequiredIndex();

        TemplateDefaultValueTo dv = template.getDefaultValue();
        Map<Integer, Object> valueMap = dv.getValue();
        if (valueMap == null) {
            return;
        }
        if (valueMap.containsKey(ONE)) {
            value01 = (String) valueMap.get(ONE);
        }
        if (valueMap.containsKey(TWO)) {
            value02 = (String) valueMap.get(TWO);
        }
        if (valueMap.containsKey(THREE)) {
            value03 = (String) valueMap.get(THREE);
        }
        if (valueMap.containsKey(FOUR)) {
            value04 = (String) valueMap.get(FOUR);
        }
        if (valueMap.containsKey(FIVE)) {
            value05 = (String) valueMap.get(FIVE);
        }
    }

    @Override
    public Boolean checkRequireValueIsEmpty() {
        //檢查使用者輸入欄位
        return WkStringUtils.isEmpty(this.value02);
    }

    @Override
    public String getCompositionText(boolean showField, int maxFieldNameLen) {
        StringBuilder sb = new StringBuilder();
        if (showField) {
            sb.append(WkStringUtils.getInstance().padEndEmpty(name, maxFieldNameLen, "："));
        }
        sb.append(Strings.nullToEmpty(value01)).append(" ");
        if (!Strings.isNullOrEmpty(value01)) {
            sb.append(value01).append(" ");
        }
        sb.append(Strings.nullToEmpty(value02)).append(" ");
        sb.append(Strings.nullToEmpty(value03)).append(" ");
        sb.append("\r\n");
        return sb.toString();
    }

    @Override
    public String getIndexContent() {
        try {
            List<String> idxList = Lists.newArrayList();
            idxList.add(this.getFinalValue());
            idxList.add(value02);
            idxList.add(value03);
            return WkJsonUtils.getInstance().toJson(Lists.newArrayList(idxList));
        } catch (IOException | ArrayIndexOutOfBoundsException ex) {
            log.error("parse value to json string fail...");
            log.error("fail value01 = " + value01);
            log.error("fail value02 = " + value02);
            log.error("fail value03 = " + value03);
            log.error("fail error " + ex.getMessage(), ex);
            return value01 + " " + value02 + " " + value03;
        }
    }

    @Override
    public String transferToPmisValue() {
        return value02;
    }

}
