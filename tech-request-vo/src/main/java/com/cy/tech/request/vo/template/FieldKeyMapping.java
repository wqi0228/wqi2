/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.template;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.vo.converter.ComTypeConverter;
import com.cy.tech.request.vo.value.to.TemplateDefaultValueTo;
import com.cy.tech.request.vo.converter.DefaultValueJsonConverter;
import com.cy.tech.request.vo.enums.ComType;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 鍵值欄位對照表<BR/>
 * 元件欄位模版<BR/>
 * 每次開啟單據時，都需呼叫此模版套入<BR/>
 * <BR/>
 *
 * 1. 載入模版需先進行排序 <BR/>
 * 2. 如為新建單據則建立空 Map<String, ComBase> comValueMap;<BR/>
 * 3. 如為載入檔單據則進行DocComTemplateValue Map轉換
 *
 * @author shaun
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_field_key_mapping")
public class FieldKeyMapping implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -60515661524659737L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "sid", length = 36)
    private String sid;

    /** 對應鍵值Id */
    @Column(name = "mapping_id", length = 255, nullable = false)
    private String mappingId;

    /** 鍵值版號 */
    @Column(name = "mapping_ver", nullable = false)
    private Integer mappingVersion;

    /** BasicDataField.fieldName 欄位名稱 */
    @Column(name = "field_name", length = 255, nullable = false)
    private String fieldName;

    /** 元件型態 */
    @Column(name = "field_component_type")
    @Convert(converter = ComTypeConverter.class)
    private ComType fieldComponentType;

    /** 狀態 */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    /** 排序 */
    @Column(name = "seq", nullable = false)
    private Integer seq;

    /** 是否為必要輸入欄位 */
    @Column(name = "required_input", nullable = false)
    private Boolean requiredInput;

    /** 是否建立索引 */
    @Column(name = "required_index", nullable = false)
    private Boolean requiredIndex;

    /** 是否顯示欄位名稱 */
    @Column(name = "show_field_name", nullable = false)
    private Boolean showFieldName;

    /** 元件ID */
    @Column(name = "com_id", length = 255, nullable = false)
    private String comId;

    /** 元件預設值 */
    @Column(name = "component_default_value", nullable = false)
    @Convert(converter = DefaultValueJsonConverter.class)
    private TemplateDefaultValueTo defaultValue;

    /** 進行互動的元件Id */
    @Column(name = "interact_com_id", nullable = true)
    private String interactComId;

    /** 進行互動的元件Id 設定值 */
    @Column(name = "component_interact_com_value", nullable = true)
    @Convert(converter = DefaultValueJsonConverter.class)
    private TemplateDefaultValueTo interactComValue;

    @Column(name = "is_saba_compnt", nullable = true)
    private Boolean isSabaComponent;
}
