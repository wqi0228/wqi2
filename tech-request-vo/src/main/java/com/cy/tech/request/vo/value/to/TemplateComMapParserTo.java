/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.value.to;

import com.cy.tech.request.vo.annotation.TemplateComMapParserDefaultValueAnnotation;
import com.cy.tech.request.vo.template.component.ComBaseClear;
import com.cy.tech.request.vo.template.component.ComBaseList;
import com.cy.tech.request.vo.template.component.ComBaseMapListStr;
import com.cy.tech.request.vo.template.component.ComWebLayerNotify;
import com.cy.tech.request.vo.template.component.to.ComRadioTo;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.cy.tech.request.vo.template.component.ComBaseMultipleRadio;
import com.cy.work.common.utils.WkJsonUtils;
import java.io.IOException;
import java.lang.reflect.Field;

/**
 * 解析模版元件物件用
 *
 * @author shaun
 */
@Slf4j
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"key"})
public class TemplateComMapParserTo implements Serializable, ComBaseClear, ComBaseList, ComBaseMapListStr, ComBaseMultipleRadio {

    /**
     * 
     */
    private static final long serialVersionUID = 6052339814501347377L;
    /** Map 對應 Key */
    private int key;
    /** 型態 */
    @SuppressWarnings("rawtypes")
    private Class valueClz;
    /** 名稱 */
    private String valueName;
    /** 說明 */
    private String desc;
    /** 存進Map內的值 ，需透過valueClz轉型，再存進Map物件內 */
    private Object value;
    /** 可編輯 */
    private boolean canEdit;
    /** 錯誤訊息提示暫存 */
    private String errorMsg;
    /** 新增用暫存值 */
    private String tempStrValue;
    /** 新增用暫存值2 */
    private String tempStrValue2;
    /** 選擇的select item key (ComBaseMapListStr) */
    private String selectItemKey;
    /** value如果為list或map內部的物件，設定值處理 */
    private List<TemplateComMapParserTo> parserTos;

    @SuppressWarnings("rawtypes")
    public TemplateComMapParserTo(int key, Class valueClz, String valueName, String desc, Object value, boolean canEdit) {
        this.key = key;
        this.valueClz = valueClz;
        this.valueName = valueName;
        this.desc = desc;
        this.value = value;
        this.canEdit = canEdit;
    }

    @Override
    public void checkValidatorRule() {
        if (this.value == null || !(this.value instanceof String) || Strings.isNullOrEmpty(this.value.toString())) {
            return;
        }
        this.errorMsg = "";
        Pattern pattern = Pattern.compile("^\\d+(\\.\\d+)?");
        Matcher matcher = pattern.matcher(this.value.toString());
        if (!matcher.matches()) {
            this.value = null;
            this.errorMsg = "請輸入數值格式！！";
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void add(ComWebLayerNotify webNotify) {
        if (Strings.isNullOrEmpty(this.tempStrValue)) {
            webNotify.notify("請輸入資訊。");
            return;
        }
        if (this.value == null) {
            this.value = Lists.newArrayList();
        }
        @SuppressWarnings("rawtypes")
        List valueList = (ArrayList) this.value;
        if (valueList.contains(this.tempStrValue)) {
            webNotify.notify("已包含重覆資訊！");
            return;
        }
        valueList.add(this.tempStrValue);
        this.tempStrValue = null;
    }

    @Override
    public void sub(int index) {
        @SuppressWarnings("rawtypes")
        List valueList = (ArrayList) this.value;
        if (index < 0 || index > valueList.size()) {
            return;
        }
        String removeStr = (String) valueList.get(index);
        if (valueList.contains(removeStr)) {
            valueList.remove(removeStr);
        }
    }

    @Override
    public void addSelectItem() {
        if (Strings.isNullOrEmpty(this.tempStrValue)) {
            return;
        }
        if (this.value == null) {
            this.value = Maps.newLinkedHashMap();
        }
        @SuppressWarnings("unchecked")
        Map<String, List<String>> map = (LinkedHashMap<String, List<String>>) this.value;
        if (!map.containsKey(this.tempStrValue)) {
            map.put(this.tempStrValue, new ArrayList<String>());
        }
        this.tempStrValue = "";
    }

    @Override
    public void subSelectItem(String key) {
        @SuppressWarnings("unchecked")
        Map<String, List<String>> map = (LinkedHashMap<String, List<String>>) this.value;
        if (map.containsKey(key)) {
            map.remove(key);
        }
    }

    @Override
    public void addCheckBoxItem() {
        if (Strings.isNullOrEmpty(this.tempStrValue2)) {
            return;
        }
        @SuppressWarnings("unchecked")
        Map<String, List<String>> map = (LinkedHashMap<String, List<String>>) this.value;
        List<String> valueList = map.get(this.selectItemKey);
        if (!valueList.contains(this.tempStrValue2)) {
            valueList.add(this.tempStrValue2);
        }
        this.tempStrValue2 = "";
    }

    @Override
    public void subCheckBoxItem(int index) {
        @SuppressWarnings("unchecked")
        Map<String, List<String>> map = (LinkedHashMap<String, List<String>>) this.value;
        List<String> valueList = map.get(this.selectItemKey);
        if (index < 0 || index > valueList.size()) {
            return;
        }
        String removeStr = (String) valueList.get(index);
        if (valueList.contains(removeStr)) {
            valueList.remove(removeStr);
        }
    }

    @Override
    public void addRadioGroup(ComWebLayerNotify webNotify) {
        if (Strings.isNullOrEmpty(this.tempStrValue)) {
            webNotify.notify("請輸入名稱資訊。");
            return;
        }
        if (this.value == null) {
            this.value = Maps.newLinkedHashMap();
        }
        @SuppressWarnings("unchecked")
        Map<String, ComRadioTo> valueList = (Map<String, ComRadioTo>) this.value;
        if (valueList.containsKey(this.tempStrValue)) {
            webNotify.notify("已包含重覆資訊！");
            return;
        }
        valueList.put(this.tempStrValue, new ComRadioTo(this.tempStrValue));
        this.tempStrValue = null;
    }

    @Override
    public void subRadioGroup(String key) {
        @SuppressWarnings("unchecked")
        Map<String, ComRadioTo> map = (LinkedHashMap<String, ComRadioTo>) this.value;
        if (map.containsKey(key)) {
            map.remove(key);
        }
    }

    @Override
    public void renderRadioToParam() {
        if (value == null || Strings.isNullOrEmpty(selectItemKey)) {
            this.parserTos = null;
            return;
        }
        //異常狀況時要強制轉型
        @SuppressWarnings("unchecked")
        Map<String, ComRadioTo> valueMap = (LinkedHashMap<String, ComRadioTo>) value;
        valueMap.keySet().forEach(eachKey -> {
            try {
                if (!(valueMap.get(eachKey) instanceof ComRadioTo)) {
                    ComRadioTo radioTo = WkJsonUtils.getInstance().fromJson(WkJsonUtils.getInstance().toJson(valueMap.get(eachKey)), ComRadioTo.class);
                    valueMap.put(radioTo.getValue00(), radioTo);
                }
            } catch (IOException ex) {
                log.error("parser ComRadioTo faild", ex);
            }
        });
        //執行渲染
        @SuppressWarnings("unchecked")
        Map<String, ComRadioTo> map = (LinkedHashMap<String, ComRadioTo>) this.value;
        if (!map.containsKey(selectItemKey)) {
            return;
        }
        ComRadioTo radioTo = map.get(selectItemKey);
        this.parserTos = Lists.newArrayList();
        Field[] fields = radioTo.getClass().getDeclaredFields();
        for (Field each : fields) {
            TemplateComMapParserDefaultValueAnnotation anno = each.getAnnotation(TemplateComMapParserDefaultValueAnnotation.class);
            if (anno == null) {
                continue;
            }
            TemplateComMapParserTo to = new TemplateComMapParserTo(anno.key(), anno.valueClz(), anno.valueName(), anno.desc(), null, anno.canEdit());
            this.parserTos.add(to);
        }
        radioTo.resetParam(parserTos);
    }

    @Override
    public void saveRadioToParam(ComWebLayerNotify webNotify) {
        if (Strings.isNullOrEmpty(this.selectItemKey)) {
            webNotify.notify("未選擇任何輸入名稱資訊。");
            return;
        }
        @SuppressWarnings("unchecked")
        Map<String, ComRadioTo> map = (LinkedHashMap<String, ComRadioTo>) this.value;
        if (!map.containsKey(selectItemKey)) {
            webNotify.notify("模版異常無法找到單選選項。");
            return;
        }
        ComRadioTo radioTo = map.get(selectItemKey);
        radioTo.saveRadioToParam(parserTos);
        this.selectItemKey = "";
        this.parserTos = null;
    }
}
