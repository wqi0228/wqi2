/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require.setting;

import lombok.Getter;

/**
 * 特殊簽核模式
 */
public enum SettingSpecSignMode {

    /** 限制最高簽核層級 */
    SIGN_LEVEL_LIMIT("限制最高簽核層級"),

    /** 僅簽一層 */
    UP_ONE_LEVEL("僅簽一層"),

    /** 指定簽核角色 */
    SPEC_SINGER("指定簽核角色"),

    ;

    /**
     * 說明
     */
    @Getter
    private final String descr;

    SettingSpecSignMode(String descr) {
        this.descr = descr;
    }
}
