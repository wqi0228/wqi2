/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 需求索引資料集
 *
 * @author allen
 */
@Data
@ToString
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_index_dictionary")
public class SimpleRequireIndexDictionary implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5827673493237928356L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "index_sid", length = 36)
    private String sid;

    /** 需求單主檔 */
    @Column(name = "require_sid", nullable = false)
    private String requireSid;

    /** 需求單單號 */
    @Column(name = "require_no", nullable = false, length = 21)
    private String requireNo;

    /** 模版鍵值 */
    @Column(name = "mapping_key_sid", nullable = false)
    private String mappingKeySid;

    /** 欄位名稱 */
    @Column(name = "field_name", nullable = false, length = 255)
    private String fieldName;

    /** 欄位內容 */
    @Column(name = "field_content", nullable = false)
    private String fieldContent;

    /** 單據建立日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createDate;

    /** 最後異動日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_udt")
    private Date lastUpdatedDate;

}
