/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import com.cy.tech.request.vo.enums.*;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 需求單待閱原因
 *
 * @author shaun
 */
@Slf4j
@Converter
public class ReqToBeReadTypeConverter implements AttributeConverter<ReqToBeReadType, String> {

    @Override
    public String convertToDatabaseColumn(ReqToBeReadType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public ReqToBeReadType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return ReqToBeReadType.valueOf(dbData);
        } catch (Exception e) {
            log.error("ReqToBeReadType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
