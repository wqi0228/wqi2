/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.cy.tech.request.vo.log.LogSyncMmsActionType;
import com.google.common.base.Strings;

import lombok.extern.slf4j.Slf4j;

/**
 * MMS維護單同步記錄 動作類型轉換器
 * @author allen1214_wu
 */
@Slf4j
@Converter
public class LogSyncMmsActionTypeConverter implements AttributeConverter<LogSyncMmsActionType, String> {

    @Override
    public String convertToDatabaseColumn(LogSyncMmsActionType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public LogSyncMmsActionType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return LogSyncMmsActionType.valueOf(dbData);
        } catch (Exception e) {
            log.error("LogSyncMmsActionType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
