/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import lombok.extern.slf4j.Slf4j;

/**
 * 送測狀況一覽表-自訂搜尋元件底層列舉
 *
 * @author brain0925_liao
 */
@Slf4j
public enum Search13QueryColumn implements SearchReportCustomEnum {
    /** 送測狀況一覽表-key */
    Search13Query("送測狀況一覽表"),
    /** 需求類別 */
    DemandType("需求類別"),
    /** 需求製作進度 */
    DemandProcess("需求製作進度"),
    /** 填單單位 */
    Department("填單單位"),
    /** 送測單位 */
    TestDepartment("送測單位"),
    /** 是否閱讀 */
    ReadStatus("是否閱讀"),
    /** 填單人員 */
    DemandPerson("填單人員"),
    /** 待閱原因 */
    WaitReadReson("待閱原因"),
    /** 送測狀態 */
    TestStatus("送測狀態"),
    /** 排序方式 */
    OrderType("排序方式"),
    /** 類別組合 */
    CategoryCombo("類別組合"),
    /** 立單區間-索引 */
    DateIndex("立單區間-索引"),
    /** 立單區間-起始 */
    StartDate("立單區間-起始"),
    /** 立單區間-結束 */
    EndDate("立單區間-結束"),
    /** 模糊搜尋 */
    SearchText("模糊搜尋");

    /** 欄位名稱 */
    private final String val;

    Search13QueryColumn(String val) {
        this.val = val;
    }

    @Override
    public String getVal() {
        return val;
    }

    @Override
    public String getUrl() {
        return "Search13";
    }

    @Override
    public String getKey() {
        return this.name();
    }

    @Override
    public SearchReportCustomEnum getSearchReportCustomEnumByName(String name) {
        try {
            return Search13QueryColumn.valueOf(name);
        } catch (Exception e) {
            log.error("getSearchReportCustomEnumByName ERROR", e);
        }
        return null;
    }
}
