/**
 * 
 */
package com.cy.tech.request.vo.pps6;

import java.io.Serializable;

import javax.persistence.Id;

import org.springframework.beans.BeanUtils;

import com.cy.work.common.vo.AbstractEntityVO;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * PPS6 Modeler 流程定義資料檔 VO
 * 
 * @author allen1214_wu
 */
@EqualsAndHashCode(callSuper = false, of = "sid")
@NoArgsConstructor
@Getter
@Setter
public class Pps6ModelerFlowVO extends AbstractEntityVO implements Serializable{

	/**
     * 
     */
    private static final long serialVersionUID = 4773678307225841210L;

    /**
	 * 建構子
	 * @param pps6ModelerFlow Pps6ModelerFlow
	 */
	public Pps6ModelerFlowVO(Pps6ModelerFlow pps6ModelerFlow) {
		// ====================================
		// copy Properties
		// ====================================
		BeanUtils.copyProperties(pps6ModelerFlow, this);
	}

	/**
	 * SID
	 */
	@Id
	private Integer sid;

	/**
	 * 流程代號
	 */
	private String flowId;

	/**
	 * 流程名稱
	 */
	private String flowName;
	
	/**
	 * 單據分類 pps6_modeler_form.form_sid
	 */
	private Integer formSid;

	/**
	 * 單據名稱資料
	 */
	private Pps6ModelerFormVO formVO;

	/**
	 * 取得單據名稱
	 * 
	 * @return 單據名稱
	 */
	public String getFormName() {
		if (formVO != null) {
			return formVO.getName();
		}
		return "";
	}
}
