/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

/**
 * 附加檔案 行為
 *
 * @author shaun
 */
public enum OthSetAttachmentBehavior {

    /** 0 填寫其他設定資訊時所上傳的附加檔案 */
    FIRST_FILE,
    /** 1 回覆所上傳的附加檔案 */
    REPLY_FILE,
    /** 2 回覆的回覆所上傳的附加檔案 */
    REPLY_AND_REPLY_FILE,
    /** 3 取消所上傳的附加檔案 */
    CANCEL_OS_FILE,
    /** 4 完成所上傳的附加檔案 */
    FINISH_OS_FILE,;
}
