/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

/**
 * 共用群組類型
 *
 * @author jason_h
 */
public enum GroupSetupType {

    /** 0 分派通知 */
    ASSIGN,
    /** 1 原型確認 */
    PROTOTYPE;
}
