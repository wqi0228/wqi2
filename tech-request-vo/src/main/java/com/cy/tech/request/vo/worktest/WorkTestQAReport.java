/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.worktest;

import com.cy.commons.enums.Activation;
import com.cy.commons.interfaces.BaseEntity;
import com.cy.commons.vo.User;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.common.vo.converter.WorkSourceTypeConverter;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.vo.converter.SidUserConverter;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * work_qa_report
 *
 * @author shaun
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "work_test_qa_report")
public class WorkTestQAReport implements Serializable, BaseEntity<String> {

    /**
     * 
     */
    private static final long serialVersionUID = -3620492417216122563L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "testinfo_qa_report_sid", length = 36)
    private String sid;

    /** 送測單主檔 */
    @ManyToOne
    @JoinColumn(name = "testinfo_sid", nullable = false)
    private WorkTestInfo testInfo;

    /** 送測單號 */
    @Column(name = "testinfo_no", nullable = false, length = 21)
    private String testinfoNo;

    /** 送測來源 */
    @Convert(converter = WorkSourceTypeConverter.class)
    @Column(name = "testinfo_source_type", nullable = false)
    private WorkSourceType sourceType;

    /** 送測來源 sid */
    @Column(name = "testinfo_source_sid", nullable = false, length = 36)
    private String sourceSid;

    /** 送測來源 單號 */
    @Column(name = "testinfo_source_no", nullable = false, length = 21)
    private String sourceNo;

    /** 測試報告內容 */
    @Column(name = "report_content")
    private String content;

    /** 測試報告內容 含css */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "report_content_css")
    private String contentCss;

    /** 狀態 */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    /** 建立者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "create_usr", nullable = false)
    private User createdUser;

    /** 建立日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    /** 異動者 */
    @Convert(converter = SidUserConverter.class)
    @Column(name = "update_usr")
    private User updatedUser;

    /** 異動日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

    /** 回覆歷程 */
    @OneToOne(mappedBy = "qaReport", cascade = CascadeType.PERSIST)
    private WorkTestInfoHistory history;
}
