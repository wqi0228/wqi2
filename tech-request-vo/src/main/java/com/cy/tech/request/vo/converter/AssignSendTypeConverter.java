/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import com.cy.tech.request.vo.enums.AssignSendType;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author allen
 */
@Slf4j
@Converter
public class AssignSendTypeConverter implements AttributeConverter<AssignSendType, String> {

    @Override
    public String convertToDatabaseColumn(AssignSendType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public AssignSendType convertToEntityAttribute(String dbData) {
        AssignSendType type = AssignSendType.safeValueOf(dbData);
        if (type == null) {
            log.error("AssignSendType Converter 轉型失敗。 dbData:[{}]", dbData);
        }
        return type;

    }

}
