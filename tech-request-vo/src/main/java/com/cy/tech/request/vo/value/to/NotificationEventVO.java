package com.cy.tech.request.vo.value.to;

import java.util.Date;

import com.cy.commons.vo.User;
import com.cy.tech.request.vo.enums.NotificationEventType;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author aken_kao
 *
 */
@Getter
@Setter
public class NotificationEventVO {
    
    private String sid;
    private NotificationEventType eventType;    //通知事件類型
    private String eventDesc;       //通知事件描述
    private String sourceNo;        //來源單號
    private String sourceTheme;     //來源主題
    private Date createdDate;       //通知時間
    private User createdUser;       //通知人員
    private String url;             //link url
    private boolean checked;
    
}
