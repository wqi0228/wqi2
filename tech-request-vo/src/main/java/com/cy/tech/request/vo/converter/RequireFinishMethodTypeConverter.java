/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.converter;

import com.cy.tech.request.vo.enums.RequireFinishMethodType;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 需求完成方式
 *
 * @author shaun
 */
@Slf4j
@Converter
public class RequireFinishMethodTypeConverter implements AttributeConverter<RequireFinishMethodType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(RequireFinishMethodType attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute.getCode();
    }

    @Override
    public RequireFinishMethodType convertToEntityAttribute(Integer dbData) {
        if (dbData == null) {
            return null;
        }
        for (RequireFinishMethodType each : RequireFinishMethodType.values()) {
            if (dbData.equals(each.getCode())) {
                return each;
            }
        }
        log.error("需求完成方式無法對應enum型態，dbData = " + dbData);
        return null;
    }
}
