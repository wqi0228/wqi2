/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.require.othset;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author aken
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_os_theme_df_dep_detail")
public class OthSetThemeDetail implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5002922105663097580L;


    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "totdd_detail_sid", length = 36)
    private String sid;


    /** 資訊內容 */
    @ManyToOne
    @JoinColumn(name = "totdd_sid", nullable = false)
    private OthSetTheme othSetTheme;

    /** 對應通知部門sid */
    @Column(name = "dep_sid", nullable = false)
    private Integer depSid;

}
