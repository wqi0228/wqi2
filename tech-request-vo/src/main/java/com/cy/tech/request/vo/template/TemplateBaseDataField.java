/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.template;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 欄位基本資料模版
 *
 * @author shaun
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_basic_data_field")
public class TemplateBaseDataField implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8854820999093553954L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "field_sid", length = 36)
    private String sid;

    @JsonProperty("fieldName")
    @Column(name = "field_name", length = 255, nullable = false)
    private String fieldName;

    /**
     * 是否為必要輸入欄位
     */
    @JsonProperty("requiredInput")
    @Column(name = "required_input", nullable = false)
    private Boolean requiredInput;

    /** 是否建立索引 */
    @JsonProperty("requiredIndex")
    @Column(name = "required_index", nullable = false)
    private Boolean requiredIndex;

    @JsonProperty("showFieldName")
    @Column(name = "show_field_name", nullable = false)
    private Boolean showFieldName;

    @Column(name = "note", length = 255)
    private String note;
}
