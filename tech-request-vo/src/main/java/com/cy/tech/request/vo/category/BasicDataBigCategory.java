package com.cy.tech.request.vo.category;

import com.cy.commons.enums.Activation;
import com.cy.commons.interfaces.BaseEntity;
import com.cy.commons.vo.User;
import com.cy.tech.request.vo.converter.ReqCateTypeConverter;
import com.cy.work.common.vo.converter.SidUserConverter;
import com.cy.tech.request.vo.enums.ReqCateType;
import com.cy.work.common.vo.converter.JsonStringListToConverter;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import org.hibernate.annotations.GenericGenerator;

/**
 * 01_類別基本資料(類別)
 *
 * @author shaun
 */
@Data
@ToString(exclude = {"children"})
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "tr_basic_data_big_category")
public class BasicDataBigCategory implements Serializable, BaseEntity<String> {

    /**
     * 
     */
    private static final long serialVersionUID = 717067384228142794L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "basic_data_big_category_sid", length = 36)
    private String sid;

    /**
     * 代碼 - 由系統管理員編碼
     */
    @Column(name = "big_category_id", unique = true, nullable = false, length = 8)
    private String id;

    /**
     * 名稱
     */
    @Column(name = "big_category_name", unique = true, nullable = false)
    private String name;

    /**
     * 備註事項
     */
    @Column(name = "note", length = 255)
    private String note;

    /**
     * 所有關連的中類
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parentBigCategory")
    @OrderBy("id ASC")
    private List<BasicDataMiddleCategory> children = Lists.newArrayList();

    /**
     * 排序
     */
    @Column(name = "seq")
    private Integer seq;

    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    @Convert(converter = SidUserConverter.class)
    @Column(name = "create_usr", nullable = false)
    private User createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;

    @Convert(converter = SidUserConverter.class)
    @Column(name = "update_usr", nullable = false)
    private User updatedUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

    @Convert(converter = JsonStringListToConverter.class)
    @Column(name = "use_dep", nullable = true)
    private JsonStringListTo canUseDepts = new JsonStringListTo();

    @Convert(converter = ReqCateTypeConverter.class)
    @Column(name = "req_category", nullable = true)
    private ReqCateType reqCateType = ReqCateType.EXTERNAL;

    /** 權限對應 如果有多個角色字串以 '，' 分隔 */
    @Column(name = "permission_role")
    private String permissionRole;

}
