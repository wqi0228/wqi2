/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.enums;

import java.io.Serializable;

/**
 * 自訂查詢物件 - 報表查詢類型底層列舉
 *
 * @author brain0925_liao
 */
public interface SearchReportCustomEnum extends Serializable {

    /** 取得key */
    public String getKey();

    /** 取得Url */
    public String getUrl();

    /** 取得名稱 */
    public String getVal();

    public SearchReportCustomEnum getSearchReportCustomEnumByName(String name);

}
