/**
 * 
 */
package com.cy.tech.request.vo.enums;

import lombok.Getter;

/**
 * 快取資料類型
 * 
 * @author allen1214_wu
 */
public enum WkCommonCacheType {

    REQUEST_REST_COUNT_WAIT_TASK("REST-查詢所有待辦事項筆數"),
    REQUEST_ALL_CATEGORY_PICKER_ITEM("所有類別"),
    REQUEST_REST_PORTAL_ATERT_OTHERSET_LIMIT("REST-查詢最新消息-其他資訊單"),
    ;

    @Getter
    private String value;

    private WkCommonCacheType(String value) {
        this.value = value;
    }
}
