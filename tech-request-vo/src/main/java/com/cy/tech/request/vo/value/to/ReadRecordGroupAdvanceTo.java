/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.vo.value.to;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.logic.lib.manager.to.WorkCommonReadRecordTo;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.collect.Lists;

import lombok.Data;

/**
 * 閱讀記錄群組 僅供收件夾使用，勿轉成Json格式
 *
 * @author shaun
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReadRecordGroupAdvanceTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7724088207370525655L;

    /** 閱讀記錄清單 */
    private List<WorkCommonReadRecordTo> values = Lists.newArrayList();

    /** 應點閱人數 */
    public int getTotalSize() {
        if (WkStringUtils.isEmpty(this.values)) {
            return 0;
        }
        return this.values.size();
    }

    /** 已點閱人數 */
    public int getReadSize() {
        if (WkStringUtils.isEmpty(this.values)) {
            return 0;
        }
        long size = this.values.stream()
                .filter(to -> to.getReadDateTime() != null)
                .count();

        return Integer.parseInt(size + "");
    }

    public String findTotalSizeStr() {
        return String.format("%0$10s", String.valueOf(this.getTotalSize()).replace(" ", ""));
    }

    public String findReadSizeStr() {
        return String.format("%0$10s", String.valueOf(this.getReadSize()).replace(" ", ""));
    }

    public void sortValues() {
        if (WkStringUtils.notEmpty(values)) {
            // 排序:
            // 部門
            Comparator<WorkCommonReadRecordTo> comparator = Comparator.comparing(to -> findOrgSeq(to.getUserSid()));
            // user name
            comparator = comparator.thenComparing(Comparator.comparing(to -> WkUserUtils.findNameBySid(to.getUserSid())));

            values = values.stream()
                    .sorted(comparator)
                    .collect(Collectors.toList());
        }
    }

    private Integer findOrgSeq(Integer userSid) {
        User user = WkUserCache.getInstance().findBySid(userSid);
        if (user == null) {
            return Integer.MAX_VALUE;
        }

        return WkOrgCache.getInstance().findOrgOrderSeq(user.getPrimaryOrg());
    }

}
