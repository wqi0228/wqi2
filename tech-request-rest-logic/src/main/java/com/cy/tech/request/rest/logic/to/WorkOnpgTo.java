package com.cy.tech.request.rest.logic.to;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkOnpgTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 502245801318766012L;
    //reqSid-onpgNo
    private String sid;
    private String onpgNo;
    private Date createDate;
    private Integer depSid;
    private Integer userSid;
    private String onpgTheme;
    private String onpgUrl;

    public WorkOnpgTo() {
    }

    public WorkOnpgTo(String sid, String onpgNo, Date createDate,
            Integer depSid, Integer userSid, String onpgTheme, String onpgUrl) {
        this.sid = sid;
        this.onpgNo = onpgNo;
        this.createDate = createDate;
        this.depSid = depSid;
        this.userSid = userSid;
        this.onpgTheme = onpgTheme;
        this.onpgUrl = onpgUrl;
    }
}
