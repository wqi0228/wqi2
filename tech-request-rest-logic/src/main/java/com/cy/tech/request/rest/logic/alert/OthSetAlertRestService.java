/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.rest.logic.alert;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.antkorwin.xsync.XSync;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.repository.require.othset.OthSetAlertRepo;
import com.cy.tech.request.rest.logic.to.OthSetAlertTo;
import com.cy.tech.request.vo.converter.OthSetAlertTypeConverter;
import com.cy.tech.request.vo.enums.WkCommonCacheType;
import com.cy.tech.request.vo.require.othset.OthSetAlert;
import com.cy.work.common.cache.WkCommonCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkEntityUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.common.vo.converter.ReadRecordTypeConverter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * 其它設定通知rest 邏輯
 *
 * @author shaun
 */
@Service
@Slf4j
public class OthSetAlertRestService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 596172730206267081L;
    @Autowired
    private WkEntityUtils entityUtils;
    @Autowired
    private OthSetAlertRepo alertDao;
    @Autowired
    private NativeSqlRepository nativeSqlRepository;

    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private transient WkCommonCache wkCommonCache;
    @Autowired
    private XSync<String> xSync;
    /**
     * 快取逾期時間 : 10秒
     */
    private long overdueMillisecond = 10 * 1000;

    public List<OthSetAlertTo> nativeQueryByReadStatusAndReceiverForLimit(
            ReadRecordType readStatus,
            Integer receiverSid,
            Integer limit) {

        String key = readStatus.name() + ";" + receiverSid + ";" + limit;

        xSync.execute(key, () -> {
            this.cacheNativeQueryByReadStatusAndReceiverForLimit(key);
        });

        List<OthSetAlertTo> results = this.wkCommonCache.getCache(
                WkCommonCacheType.REQUEST_REST_PORTAL_ATERT_OTHERSET_LIMIT.name(),
                Lists.newArrayList(key),
                overdueMillisecond);

        return results;
    }

    public List<OthSetAlertTo> cacheNativeQueryByReadStatusAndReceiverForLimit(String key) {

        // ====================================
        // 由快取中取回
        // ====================================
        List<OthSetAlertTo> cacheData = this.wkCommonCache.getCache(
                WkCommonCacheType.REQUEST_REST_PORTAL_ATERT_OTHERSET_LIMIT.name(),
                Lists.newArrayList(key),
                overdueMillisecond);

        if (cacheData != null) {
            return cacheData;
        }

        // ====================================
        // 解析 key
        // ====================================
        String[] keys = key.split(";");
        if (keys.length != 3) {
            log.warn("傳入 key 值有誤:[{}]", key);
            return Lists.newArrayList();
        }

        ReadRecordType readStatus = new ReadRecordTypeConverter().convertToEntityAttribute(keys[0]);
        Integer receiverSid = Integer.parseInt(keys[1]);
        Integer limit = Integer.parseInt(keys[2]);

        // ====================================
        // 查詢
        // ====================================
        // 作業起始時間
        //Long startTime = System.currentTimeMillis();
        // 查詢

        // use jdbc templet
        List<OthSetAlertTo> results = this.nativeQueryByReadStatusAndReceiverByJdbcTemplet(
                readStatus,
                receiverSid,
                limit);

        // use execNativeSql
        // List<OthSetAlertTo> results = this.nativeQueryByReadStatusAndReceiver(readStatus, receiverSid, null, null, limit);

        // log.info(WkCommonUtils.prepareCostMessage(startTime, "alert_for_otherset_limit[" + receiverSid + "]"));

        // ====================================
        // 記錄快取
        // ====================================
        this.wkCommonCache.putCache(
                WkCommonCacheType.REQUEST_REST_PORTAL_ATERT_OTHERSET_LIMIT.name(),
                Lists.newArrayList(key),
                results);

        return results;
    }

    /**
     * @param readStatus  閱讀狀態
     * @param receiverSid 收件者 sid
     * @param startDate   option: 查詢起始時間 (可為 null)
     * @param endDate     option: 查詢結束時間 (可為 null)
     * @param limit       最大查詢筆數 (<1 時為全部 )
     * @return
     */
    public List<OthSetAlertTo> nativeQueryByReadStatusAndReceiver(
            ReadRecordType readStatus,
            Integer receiverSid,
            Date startDate,
            Date endDate,
            Integer limit) {

        // ====================================
        // 兜組 SQL
        // ====================================
        Map<String, Object> parameters = Maps.newHashMap();

        StringBuffer sql = new StringBuffer();
        sql.append("SELECT os_alert_sid           AS sid, ");
        sql.append("       os_sid                 AS othsetSid, ");
        sql.append("       os_no                  AS osNo, ");
        sql.append("       require_sid            AS requireSid, ");
        sql.append("       require_no             AS requireNo, ");
        sql.append("       os_theme               AS osTheme, ");
        sql.append("       type                   AS type_src, ");
        sql.append("       send_usr               AS senderSid, ");
        sql.append("       send_dt                AS sendDate, ");
        sql.append("       receiver               AS receiverSid, ");
        sql.append("       os_reply_sid           AS replySid, ");
        sql.append("       os_reply_and_reply_sid AS alreadyReplySid, ");
        sql.append("       read_status            AS readStatus_src, ");
        sql.append("       click_time             AS clickTime ");
        sql.append("FROM   tr_os_alert ");

        sql.append("WHERE  read_status = '" + readStatus.name() + "' ");
        sql.append("  AND  receiver = " + receiverSid + " ");

        if (startDate != null) {
            sql.append("  AND  send_dt >= :startDate ");
            parameters.put("startDate", new DateTime(startDate).withTime(0, 0, 0, 0).toDate());
        }

        if (endDate != null) {
            sql.append("  AND  send_dt <= :endDate ");
            parameters.put("endDate", new DateTime(endDate).withTime(23, 59, 59, 999).toDate());
        }

        sql.append("ORDER BY send_dt DESC ");

        if (limit > 0) {
            sql.append(" LIMIT " + limit);
        }

        // ====================================
        // 查詢
        // ====================================
        // 查詢
        List<OthSetAlertTo> results = this.nativeSqlRepository.getResultList(
                sql.toString(),
                parameters,
                OthSetAlertTo.class);

        if (WkStringUtils.isEmpty(results)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 資料處理
        // ====================================
        OthSetAlertTypeConverter othSetAlertTypeConverter = new OthSetAlertTypeConverter();
        ReadRecordTypeConverter readRecordTypeConverter = new ReadRecordTypeConverter();
        for (OthSetAlertTo othSetAlertTo : results) {

            othSetAlertTo.setRequire(othSetAlertTo.getRequireSid());

            // 訊息類型
            othSetAlertTo.setType(
                    othSetAlertTypeConverter.convertToEntityAttribute(
                            othSetAlertTo.getType_src()));

            // 閱讀狀態
            othSetAlertTo.setReadStatus(
                    readRecordTypeConverter.convertToEntityAttribute(
                            othSetAlertTo.getReadStatus_src()));

            // 寄件者名稱
            othSetAlertTo.setSenderName(WkUserUtils.findNameBySid(othSetAlertTo.getSenderSid()));
            // 收件者名稱
            othSetAlertTo.setReceiverName(WkUserUtils.findNameBySid(othSetAlertTo.getReceiverSid()));
        }

        // ====================================
        // 回傳
        // ====================================
        return results;
    }

    public List<OthSetAlertTo> nativeQueryByReadStatusAndReceiverByJdbcTemplet(
            ReadRecordType readStatus,
            Integer receiverSid,
            Integer limit) {

        // ====================================
        // 兜組 SQL
        // ====================================
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT os_alert_sid           AS sid, ");
        sql.append("       os_sid                 AS othsetSid, ");
        sql.append("       os_no                  AS osNo, ");
        sql.append("       require_sid            AS requireSid, ");
        sql.append("       require_no             AS requireNo, ");
        sql.append("       os_theme               AS osTheme, ");
        sql.append("       type                   AS type_src, ");
        sql.append("       send_usr               AS senderSid, ");
        sql.append("       send_dt                AS sendDate, ");
        sql.append("       receiver               AS receiverSid, ");
        sql.append("       os_reply_sid           AS replySid, ");
        sql.append("       os_reply_and_reply_sid AS alreadyReplySid, ");
        sql.append("       read_status            AS readStatus_src, ");
        sql.append("       click_time             AS clickTime ");
        sql.append("FROM   tr_os_alert ");

        sql.append("WHERE  read_status = '" + readStatus.name() + "' ");
        sql.append("  AND  receiver = " + receiverSid + " ");
        sql.append("ORDER BY send_dt DESC ");

        if (limit > 0) {
            sql.append(" LIMIT " + limit);
        }

        // ====================================
        // 查詢
        // ====================================
        OthSetAlertTypeConverter othSetAlertTypeConverter = new OthSetAlertTypeConverter();
        ReadRecordTypeConverter readRecordTypeConverter = new ReadRecordTypeConverter();

        List<OthSetAlertTo> results = this.jdbcTemplate.query(
                sql.toString(),
                (ResultSet rs, int rowNum) -> {
                    OthSetAlertTo to = new OthSetAlertTo();
                    to.setSid(rs.getString("sid"));
                    to.setOthsetSid(rs.getString("sid"));
                    to.setOsNo(rs.getString("osNo"));
                    to.setRequire(rs.getString("requireSid"));
                    to.setRequireSid(rs.getString("requireSid"));
                    to.setRequireNo(rs.getString("requireNo"));
                    to.setOsTheme(rs.getString("osTheme"));
                    to.setType(
                            othSetAlertTypeConverter.convertToEntityAttribute(
                                    rs.getString("type_src")));
                    to.setSenderSid(rs.getInt("senderSid"));
                    to.setSenderName(WkUserUtils.findNameBySid(to.getSenderSid()));
                    to.setSendDate(rs.getTimestamp("sendDate"));
                    // 收件者
                    to.setReceiverSid(rs.getInt("receiverSid"));
                    to.setReceiverName(WkUserUtils.findNameBySid(to.getReceiverSid()));
                    to.setReplySid(rs.getString("replySid"));
                    to.setAlreadyReplySid(rs.getString("alreadyReplySid"));
                    // 閱讀狀態
                    to.setReadStatus(
                            readRecordTypeConverter.convertToEntityAttribute(
                                    rs.getString("readStatus_src")));
                    to.setClickTime(rs.getTimestamp("clickTime"));
                    return to;
                });

        return results;
    }

    @Transactional(readOnly = true)
    public List<OthSetAlertTo> findByReadStatusAndReceiverLimit(ReadRecordType readStatus, User receiver, Integer limit) {

        List<OthSetAlert> result = alertDao.findByReadStatusAndReceiverOrderBySendDateDesc(readStatus, receiver, new PageRequest(0, limit));
        return result.stream().map(wAlert -> {
            this.fillUser(wAlert);
            return transTo(wAlert);
        }).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<OthSetAlertTo> findByReadStatusAndReceiver(ReadRecordType readStatus, User receiver) {
        List<OthSetAlert> result = alertDao.findByReadStatusAndReceiverOrderBySendDateDesc(readStatus, receiver);
        return result.stream().map(wAlert -> {
            this.fillUser(wAlert);
            return transTo(wAlert);
        }).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<OthSetAlertTo> findByReadStatusAndReceiverAndSendDate(ReadRecordType readStatus, User receiver, Date startDate, Date endDate) {
        List<OthSetAlert> result = alertDao.findByReadStatusAndReceiverAndSendDate(readStatus, receiver, startDate, endDate);
        return result.stream().map(wAlert -> {
            this.fillUser(wAlert);
            return transTo(wAlert);
        }).collect(Collectors.toList());
    }

    private void fillUser(OthSetAlert alert) {
        alert.setSender(WkUserCache.getInstance().findBySid(alert.getSender().getSid()));
        alert.setReceiver(WkUserCache.getInstance().findBySid(alert.getReceiver().getSid()));
    }

    @Transactional(rollbackFor = Exception.class)
    public void updateClickTime(String alertSid) {
        alertDao.updateClickTime(alertSid, new Date(), ReadRecordType.HAS_READ);
    }

    /**
     * 轉型為rest傳輸物件
     *
     * @param alert
     * @return
     */
    private OthSetAlertTo transTo(OthSetAlert alert) {
        OthSetAlertTo to = new OthSetAlertTo();
        entityUtils.copyProperties(alert, to);
        // to.setSid(alert.getSid());
        if (alert.getOthset() != null) {
            to.setOthsetSid(alert.getOthset().getSid());
        }
        // to.setTestinfoNo(alert.getTestinfoNo());
        // to.setSourceSid(alert.getSourceSid());
        // to.setSourceNo(alert.getSourceNo());
        // to.setTestinfoTheme(alert.getTestinfoTheme());
        // to.setSender(alert.getSender());
        if (alert.getSender() != null) {
            to.setSenderSid(alert.getSender().getSid());
            to.setSenderName(alert.getSender().getName());
        }
        // to.setSendDate(alert.getSendDate());
        // to.setReceiver(alert.getReceiver());
        if (alert.getReceiver() != null) {
            to.setReceiverSid(alert.getReceiver().getSid());
            to.setReceiverName(alert.getReceiver().getName());
        }
        if (alert.getReply() != null) {
            to.setReplySid(alert.getReply().getSid());
        }
        if (alert.getAlreadyReply() != null) {
            to.setAlreadyReplySid(alert.getAlreadyReply().getSid());
        }
        // to.setReadStatus(alert.getReadStatus());
        // to.setClickTime(alert.getClickTime());
        return to;
    }
}
