/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.rest.logic.process;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.bpm.rest.client.TaskClient;
import com.cy.bpm.rest.to.HistoryTaskTo;
import com.cy.bpm.rest.vo.ProcessTask;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.formsigning.enums.FormSigningType;
import com.cy.formsigning.enums.LanguageType;
import com.cy.formsigning.enums.QueryType;
import com.cy.formsigning.vo.BatchSignedResult;
import com.cy.formsigning.vo.ButtonInfo;
import com.cy.formsigning.vo.FormInfo;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.vo.BpmCreateTo;
import com.cy.tech.request.repository.require.RequireUnitSignInfoRepository;
import com.cy.tech.request.rest.logic.enums.ActionType;
import com.cy.tech.request.rest.logic.helper.ReqUnitFormInfoHelper;
import com.cy.tech.request.rest.logic.helper.ReqUnitProcessHelper;
import com.cy.tech.request.rest.logic.search.service.ReqUnitQueryForRestService;
import com.cy.tech.request.rest.logic.search.vo.RequireUnitView;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireUnitSignInfo;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * 需求單位簽核服務
 *
 * @author shaun
 */
@Slf4j
@Component
public class ReqUnitSignService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2749424594511725342L;
    @Autowired
    private RequireService reqService;
    @Autowired
    private ReqUnitQueryForRestService reqUnitQueryForRestService;
    @Autowired
    private ReqUnitFormInfoHelper formHelper;
    @Autowired
    private ReqUnitProcessHelper processHelper;
    @Autowired
    private TaskClient taskClient;
    @Autowired
    private RequireUnitSignInfoRepository rmsiDao;
    @Autowired
    private OrganizationService organizationService;

    public FormInfo findByFormId(String formId, String executorUuid, LanguageType lType) {
        User executor = WkUserCache.getInstance().findByUUID(executorUuid);
        Preconditions.checkArgument(executor != null, "傳送的執行者uuid，無法訪查出有效成員資訊 uuid:" + executorUuid);
        RequireUnitSignInfo rmsi = rmsiDao.findByRequireNo(formId);
        Preconditions.checkArgument(rmsi != null, "查詢不到需求單位相關流程 單號:" + formId);
        return formHelper.transform(rmsi, executor, lType);
    }

    /**
     * 取得待簽筆數
     *
     * @param userID 登入者ID
     * @return
     */
    public Map<String, Map<QueryType, Map<String, Integer>>> getUnSignCount(String executorUuid) throws ProcessRestException {
        Map<String, Map<QueryType, Map<String, Integer>>> result = Maps.newHashMap();
        User user = WkUserCache.getInstance().findByUUID(executorUuid);
        Preconditions.checkArgument(user != null, "傳送的成員uuid，無法訪查出有效成員資訊 uuid:" + executorUuid);
        List<ProcessTask> process = taskClient.findTaskByUserAndDefinition(user.getId(), BpmCreateTo.RequireFlowType.REQUIRE_UNIT.getDefinition());
        Map<String, ProcessTask> processMap = Maps.newHashMap();
        process.stream().filter(p -> !Strings.isNullOrEmpty(p.getDocumentID()))
                  .forEach(p -> processMap.put(p.getDocumentID().replace(BpmCreateTo.RequireFlowType.REQUIRE_UNIT.getDefinition() + "-", ""), p));
        List<String> requireNoList
                  = process.stream().filter(t -> !Strings.isNullOrEmpty(t.getDocumentID()))
                  .map(t -> t.getDocumentID().replace(BpmCreateTo.RequireFlowType.REQUIRE_UNIT.getDefinition() + "-", ""))
                  .collect(Collectors.toList());
        List<RequireUnitView> viewResult = this.searchByReqNo(requireNoList, user);
        viewResult.forEach(item -> {
            Map<QueryType, Map<String, Integer>> qDetail = null;
            Org comp = organizationService.findBySid(item.getCreateCompany().getSid());
            if (result.containsKey(comp.getId())) {
                qDetail = result.get(comp.getId());
            } else {
                qDetail = Maps.newHashMap();
            }
            QueryType qtype = null;
            qtype = QueryType.ALL;
            Map<String, Integer> userIdCount = qDetail.get(qtype);
            if (userIdCount == null || userIdCount.isEmpty()) {
                userIdCount = Maps.newHashMap();
            }
            try {
                ProcessTask task = processMap.get(item.getRequireNo());
                String userId = formHelper.getApprover(task).get(0);
                if (userIdCount.containsKey(userId)) {
                    int value = userIdCount.get(userId);
                    userIdCount.put(userId, ++value);
                } else {
                    userIdCount.put(userId, 1);
                }
            } catch (ProcessRestException e1) {
                log.error("取得預設簽核人員列表失敗, 錯誤原因: " + e1.getMessage(), e1);
            }
            qDetail.put(qtype, userIdCount);
            result.put(comp.getId(), qDetail);
        });
        log.info("user:{} 取得待簽筆數 :{}", user.getId(), result.size());
        return result;
    }

    public List<FormInfo> findUnSignByUser(String executorUuid, Integer companySid, LanguageType lType) throws ProcessRestException {
        User user = WkUserCache.getInstance().findByUUID(executorUuid);
        Preconditions.checkArgument(user != null, "傳送的成員uuid，無法訪查出有效成員資訊 uuid:" + executorUuid);
        List<ProcessTask> process = taskClient.findTaskByUserAndDefinition(user.getId(), BpmCreateTo.RequireFlowType.REQUIRE_UNIT.getDefinition());
        Map<String, ProcessTask> processMap = Maps.newHashMap();
        process.stream().filter(p -> !Strings.isNullOrEmpty(p.getDocumentID()))
                  .forEach(p -> processMap.put(p.getDocumentID().replace(BpmCreateTo.RequireFlowType.REQUIRE_UNIT.getDefinition() + "-", ""), p));
        List<String> requireNoList
                  = process.stream().filter(t -> !Strings.isNullOrEmpty(t.getDocumentID()))
                  .map(t -> t.getDocumentID().replace(BpmCreateTo.RequireFlowType.REQUIRE_UNIT.getDefinition() + "-", ""))
                  .collect(Collectors.toList());
        List<RequireUnitView> viewResult = this.searchByReqNo(requireNoList, user);
        List<RequireUnitView> unSignReuslt = Lists.newArrayList();
        viewResult.forEach(item -> {
            if (!item.getCreateCompany().getSid().equals(companySid)) {
                return;
            }
            unSignReuslt.add(item);
        });
        return unSignReuslt.stream().map(view -> formHelper.transform(view, processMap.get(view.getRequireNo()), user, lType)).collect(Collectors.toList());
    }

    public List<FormInfo> findUnSignByUser(String executorUuid, LanguageType lType) throws ProcessRestException {
        User user = WkUserCache.getInstance().findByUUID(executorUuid);
        Preconditions.checkArgument(user != null, "傳送的成員uuid，無法訪查出有效成員資訊 uuid:" + executorUuid);
        List<ProcessTask> process = taskClient.findTaskByUserAndDefinition(user.getId(), BpmCreateTo.RequireFlowType.REQUIRE_UNIT.getDefinition());
        Map<String, ProcessTask> processMap = Maps.newHashMap();
        process.stream().filter(p -> !Strings.isNullOrEmpty(p.getDocumentID()))
                  .forEach(p -> processMap.put(p.getDocumentID().replace(BpmCreateTo.RequireFlowType.REQUIRE_UNIT.getDefinition() + "-", ""), p));
        List<String> requireNoList
                  = process.stream().filter(t -> !Strings.isNullOrEmpty(t.getDocumentID()))
                  .map(t -> t.getDocumentID().replace(BpmCreateTo.RequireFlowType.REQUIRE_UNIT.getDefinition() + "-", ""))
                  .collect(Collectors.toList());
        List<RequireUnitView> viewResult = this.searchByReqNo(requireNoList, user);
        return viewResult.stream().map(view -> formHelper.transform(view, processMap.get(view.getRequireNo()), user, lType)).collect(Collectors.toList());
    }

    private List<RequireUnitView> searchByReqNo(List<String> requireNoList, User executor) {
        if (requireNoList.isEmpty()) {
            return Lists.newArrayList();
        }
        Map<String, Object> param = Maps.newHashMap();
        StringBuilder sb = new StringBuilder();
        sb.append(" SELECT tr.require_sid,");
        sb.append("        tr.require_no,");
        sb.append("        tid.field_content,");
        sb.append("        tr.comp_sid,");
        sb.append("        tr.create_usr,");
        sb.append("        tr.create_dt,");
        sb.append("        rmsi.bpm_instance_id,");
        sb.append("        rmsi.paper_code ");
        sb.append(" FROM ");
        sb.append("  (SELECT * FROM tr_require tr WHERE tr.require_no IN (:requireNoList)) AS tr ");
        sb.append("    INNER JOIN (SELECT tid.require_sid,tid.field_content FROM tr_index_dictionary tid WHERE ");
        sb.append("      tid.field_name='主題') AS tid ON tr.require_sid=tid.require_sid ");
        sb.append("    INNER JOIN (SELECT * FROM tr_require_manager_sign_info rmsi ) AS rmsi ON tr.require_sid=rmsi.require_sid ");
        sb.append(" WHERE tr.require_sid IS NOT NULL GROUP BY tr.require_sid ORDER BY tr.create_dt ASC");

        param.put("requireNoList", requireNoList);
        return reqUnitQueryForRestService.findWithQuery(sb.toString(), param, executor);
    }

    public List<FormInfo> findSignedByUser(Date from, Date to, String executorUuid, LanguageType lType) throws ProcessRestException {
        User user = WkUserCache.getInstance().findByUUID(executorUuid);
        Preconditions.checkArgument(user != null, "傳送的成員uuid，無法訪查出有效成員資訊 uuid:" + executorUuid);
        List<HistoryTaskTo> historys
                  = taskClient.findHistoryByUserAndTimeAndDefinition(user.getId(), from, to, BpmCreateTo.RequireFlowType.REQUIRE_UNIT.getDefinition());
        Map<String, HistoryTaskTo> processMap = Maps.newHashMap();
        historys.forEach(p -> processMap.put(p.getDocumentID().replace(BpmCreateTo.RequireFlowType.REQUIRE_UNIT.getDefinition() + "-", ""), p));
        List<String> requireNoList = historys.stream()
                  .map(t -> t.getDocumentID().replace(BpmCreateTo.RequireFlowType.REQUIRE_UNIT.getDefinition() + "-", ""))
                  .collect(Collectors.toList());
        List<RequireUnitView> viewResult = this.searchByReqNo(requireNoList, user);
        return viewResult.stream()
                  .map(view -> formHelper.transHistoryForm(view, processMap.get(view.getRequireNo()), user, lType))
                  .collect(Collectors.toList());
    }

    public List<FormInfo> findSignedByUserAndOrg(String orguuid, Date from, Date to, String executorUuid, LanguageType lType) throws ProcessRestException {
        return this.findSignedByUser(from, to, executorUuid, lType).stream()
                  .filter(form -> form.getCompanyUuid().equals(orguuid))
                  .collect(Collectors.toList());
    }

    public List<ButtonInfo> combinFormBtn(String formId, String executorUuid) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(formId), "需求單單號為空值！");
        Require req = reqService.findByReqNo(formId);
        Preconditions.checkArgument(req != null, "查詢不到需求單資訊 單號:" + formId);
        User executor = WkUserCache.getInstance().findByUUID(executorUuid);
        Preconditions.checkArgument(executor != null, "傳送的成員uuid，無法訪查出有效成員資訊 uuid:" + executorUuid);
        List<ButtonInfo> btns = Lists.newArrayList();
        processHelper.createInvaildBtn(btns, req, executor);
        processHelper.createRollBackBtn(btns, req, executor);
        processHelper.createRecoveryBtn(btns, req, executor);
        processHelper.createSignBtn(btns, req, executor);
        return btns;
    }

    public BatchSignedResult doBatchSign(List<String> formIds, String executorUuid) {
        Preconditions.checkArgument(formIds != null && !formIds.isEmpty(), "傳送的formIds 不可為空值");
        User executor = WkUserCache.getInstance().findByUUID(executorUuid);
        Preconditions.checkArgument(executor != null, "傳送的成員uuid，無法訪查出有效成員資訊 uuid:" + executorUuid);
        List<Require> reqs = reqService.findByRequireNoIn(formIds);
        Preconditions.checkArgument(reqs != null && !reqs.isEmpty(), "無法產生有效批次簽核資訊 formIds = " + formIds);
        return processHelper.doBatchSign(reqs, executor);
    }

    public FormInfo doActionImpl(String action, Map<String, Object> inputData, String executorUuid, LanguageType lType) throws ProcessRestException, UserMessageException, SystemOperationException {
        String[] param = action.split(",");
        Preconditions.checkArgument(param.length == 2, "簽核action參數無法切割為效數值 action = " + action);
        Preconditions.checkArgument(!Strings.isNullOrEmpty(param[1]), "簽核action參數-執行方法為空值 action = " + action);
        User executor = WkUserCache.getInstance().findByUUID(executorUuid);
        Preconditions.checkArgument(executor != null, "傳送的執行者uuid，無法訪查出有效成員資訊 uuid:" + executorUuid);
        Require req = reqService.findByReqNo(param[0]);
        Preconditions.checkArgument(req != null, "查詢不到需求單資訊 單號:" + param[0]);

        ActionType actionType = null;
        try {
            actionType = ActionType.valueOf(param[1]);
        } catch (Exception e) {
            log.error("傳入簽核類型錯誤!" + e.getMessage(), e);
            Preconditions.checkArgument(false, "傳入簽核類型錯誤!" + e.getMessage());
        }
                
        req = processHelper.doAction(actionType, req, executor, inputData);
        return formHelper.transform(req, executor, lType);
    }

    public FormSigningType findFormTypeForUser(String formId, String executorUuid) {
        User executor = WkUserCache.getInstance().findByUUID(executorUuid);
        Preconditions.checkArgument(executor != null, "傳送的成員uuid，無法訪查出有效成員資訊 uuid:" + executorUuid);
        RequireUnitSignInfo rmsi = rmsiDao.findByRequireNo(formId);
        Preconditions.checkArgument(rmsi != null, "查詢不到需求單位相關流程 單號:" + formId);
        return formHelper.findFormTypeForUser(rmsi, executor);
    }

}
