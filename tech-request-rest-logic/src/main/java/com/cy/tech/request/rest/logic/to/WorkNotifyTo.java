package com.cy.tech.request.rest.logic.to;

import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.notify.vo.enums.NotifyType;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * 浮動資訊(Rest傳輸用)
 *
 * @author kasim
 */
@Data
@ToString
@EqualsAndHashCode(of = {"sid"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkNotifyTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2032286079558713834L;

    @JsonProperty("sid")
    private String sid;

    /** 來源sid */
    @JsonProperty("sourceSid")
    private String sourceSid;

    /** 來源單號 */
    @JsonProperty("sourceNo")
    private String sourceNo;

    /** 來源類型 */
    @JsonProperty("sourceType")
    private WorkSourceType sourceType;

    /** 浮動視窗類型 */
    @JsonProperty("notifyType")
    private NotifyType notifyType;

    /** 浮動視窗類型說明 */
    @JsonProperty("notifyTypeDesc")
    private String notifyTypeDesc;

    /** 顯示的內容 */
    @JsonProperty("content")
    private String content;

    /** 需顯示此資訊的使用者 */
    @JsonProperty("userSid")
    private Integer userSid;
}
