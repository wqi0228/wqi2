package com.cy.tech.request.rest.logic.schedule;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.commons.vo.simple.SimpleOrg;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.service.MailService;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.notify.logic.manager.WorkNotifyManager;
import com.cy.work.notify.vo.WorkNotify;
import com.cy.work.notify.vo.enums.NotifyType;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import lombok.extern.slf4j.Slf4j;

/**
 * @author aken_kao
 */
@Service
@Slf4j
public class ScheduleAssignUnReadNoticeService {

	@Autowired
	private WorkNotifyManager notifyManager;
	@Autowired
	private MailService mailService;
	@Autowired
	private URLService urlService;
	@Autowired
	private SearchService searchService;
	@Autowired
	private RequireService requireService;

	/**
	 * 發送主管通知信：所有成員未讀【分派通知】
	 * @throws SystemOperationException 
	 */
	public void sendToManagerByMemberDispatchNoticeUnRead() throws SystemOperationException {

		log.info("觸發：發送主管通知信：所有成員未讀【分派通知】");

		// 來源類型
		WorkSourceType sourceType = WorkSourceType.TECH_REQUEST;
		// 通知類型
		List<NotifyType> notifyTypes = Lists.newArrayList(NotifyType.REQUEST_DISPATCH_NOTIFY);

		// ====================================
		// 所有未讀取的通知
		// ====================================
		List<WorkNotify> workNotifys = notifyManager.queryNoticeByNaverRead(sourceType, notifyTypes);

		// ====================================
		// 整理資料
		// ====================================
		Map<Integer, Set<String>> noticeReqMapByManagerSid = Maps.newHashMap();

		for (WorkNotify workNotify : workNotifys) {
			// 3天未讀通知組級
			this.prepareNoticeData(workNotify, 3, OrgLevel.THE_PANEL, noticeReqMapByManagerSid);
			// 5天未讀通知部級
			this.prepareNoticeData(workNotify, 5, OrgLevel.MINISTERIAL, noticeReqMapByManagerSid);
			// 7天通未讀知處級
			this.prepareNoticeData(workNotify, 7, OrgLevel.DIVISION_LEVEL, noticeReqMapByManagerSid);
		}

		// ====================================
		// 發送郵件通知
		// ====================================
		// 名單為空時不發送
		if (noticeReqMapByManagerSid == null || noticeReqMapByManagerSid.isEmpty()) {
			log.info("無須通知");
			return;
		}

		log.info("發送清單如下：\r\n" + new GsonBuilder().setPrettyPrinting().create().toJson(noticeReqMapByManagerSid));
		this.sendMailToManagerByMemberDispatchNoticeUnRead(noticeReqMapByManagerSid);
	}

	/**
	 * 整理為要通知的資料 Map<被通知主管SID, List<需求單號>>
	 * 
	 * @param workNotify
	 * @param unReadDay                未讀天數
	 * @param notifyManagerOrgLevel    通知層級
	 * @param noticeReqMapByManagerSid 容器
	 */
	private void prepareNoticeData(
	        WorkNotify workNotify,
	        int unReadDay,
	        OrgLevel notifyManagerOrgLevel,
	        Map<Integer, Set<String>> noticeReqMapByManagerSid) {

		// ====================================
		// 準備日期資料
		// ====================================
		Date currentDate = new Date();
		// 通知建立時間
		DateTime createDate = new DateTime(workNotify.getCreateDate());
		// 檢核日 (超過此日即觸發通知)
		Date varifyDate = createDate.plusDays(unReadDay).toDate();

		// ====================================
		// 未超過檢核日 => pass
		// ====================================
		if (!currentDate.after(varifyDate)) {
			return;
		}

		// ====================================
		// 只發一次規則 - 不連續通知
		// ====================================
		Interval interval = new Interval(varifyDate.getTime(), currentDate.getTime());
		int diff = (int) interval.toDurationMillis() / 1000 / 60 / 60 / 24;
		if (diff != 0) {
			// 已超過通知時間 => pass
			return;
		}

		// ====================================
		// 取得被通知者資料
		// ====================================
		// 取得被通知者資料
		User noticeUser = WkUserCache.getInstance().findBySid(workNotify.getUserSid());
		if (noticeUser == null || noticeUser.getPrimaryOrg() == null) {
			return;
		}

		// 被通知者單位
		SimpleOrg noticeUserDep = noticeUser.getPrimaryOrg();
		if (noticeUserDep == null) {
			return;
		}

		// ====================================
		// 取得單位主管
		// ====================================
		// 依據傳入層級，查詢被通知者的直系上層單位
		Integer parentOrgSid = WkOrgCache.getInstance().findParentByOrgLevel(
		        noticeUserDep.getSid(),
		        notifyManagerOrgLevel);

		if (parentOrgSid == null) {
			return;
		}

		Org parentOrg = WkOrgCache.getInstance().findBySid(parentOrgSid);
		if (parentOrg == null) {
			return;
		}

		// 加入收集容器
		this.addToCollection(
		        workNotify.getSourceNo(),
		        noticeUser.getSid(), parentOrg,
		        noticeReqMapByManagerSid);
	}

	/**
	 * @param sourceNo
	 * @param noticeUserSid
	 * @param managerDep
	 * @param noticeReqMapMyManergerSid
	 */
	private void addToCollection(
	        String sourceNo,
	        Integer noticeUserSid,
	        Org managerDep,
	        Map<Integer, Set<String>> noticeReqMapMyManergerSid) {

		if (managerDep == null || managerDep.getManager() == null || managerDep.getManager().getSid() == null) {
			return;
		}

		// 被通知單位主管 sid
		Integer managerSid = managerDep.getManager().getSid();

		// 若自己就是主管不發通知
		if (WkCommonUtils.compareByStr(noticeUserSid, managerSid)) {
			return;
		}

		// 加入收集容器
		Set<String> reqNos = noticeReqMapMyManergerSid.get(managerSid);
		if (reqNos == null) {
			reqNos = Sets.newHashSet();
			noticeReqMapMyManergerSid.put(managerSid, reqNos);
		}
		reqNos.add(sourceNo);
	}

	/**
	 * 發送通知信
	 * 
	 * @param noticeReqMapByManagerSid
	 * @throws SystemOperationException 
	 */
	private void sendMailToManagerByMemberDispatchNoticeUnRead(Map<Integer, Set<String>> noticeReqMapByManagerSid) throws SystemOperationException {

		// 名單為空時不發送
		if (noticeReqMapByManagerSid == null || noticeReqMapByManagerSid.isEmpty()) {
			return;
		}

		// ====================================
		// 收集要被發送的需求單資料
		// ====================================
		Map<String, String> reqThemeMapByReqNo = Maps.newHashMap();
		for (Set<String> requireNoSet : noticeReqMapByManagerSid.values()) {
			for (String requireNo : requireNoSet) {
				if (reqThemeMapByReqNo.containsKey(requireNo)) {
					continue;
				}
				// 需求單主檔
				Require require = requireService.findByReqNo(requireNo);
				if (require == null) {
					log.warn("分派未讀通知：找不到案件單! reqno=" + requireNo + "");
					continue;
				}
				// 案件連結
				String link_url = urlService.buildRequireURLForMail(requireNo, "TG");
				// 兜組內容
				String theme = String.format("主題: <a href=\"%s\" target=\"_blank\">%s</a><br/>", link_url, searchService.getThemeStr(require));

				reqThemeMapByReqNo.put(requireNo, theme);
			}
		}

		// ====================================
		// 發送 mail
		// ====================================
		for (Integer managerSid : noticeReqMapByManagerSid.keySet()) {

			// 取得主管要被通知的需求單號
			Set<String> reqNos = noticeReqMapByManagerSid.get(managerSid);

			// 依據單號逐筆加入內容
			StringBuffer caseContent = new StringBuffer();
			for (String reqNo : reqNos) {
				String content = reqThemeMapByReqNo.get(reqNo);
				if (WkStringUtils.isEmpty(content)) {
					continue;
				}
				caseContent.append(content);
			}
			if (WkStringUtils.isEmpty(caseContent)) {
				log.warn("分派未讀通知：找不到可通知的內容! reqno=" + new Gson().toJson(reqNos) + "");
				continue;
			}

			// 兜組信件內容
			StringBuffer sb = new StringBuffer("主管，您好<br/><br/>以下 Werp需求單 已分派未處理 ，請您儘速與同仁確認。<br/>");
			sb.append(caseContent);
			sb.append("<br/><br/>注意事項：<br/>");
			sb.append("●若點選主題連結後，出現【無閱讀權限】，代表 該則需求單撤回分派，<br/>若有疑慮可提供主題與GM確認，謝謝<br/><br/>");

			// 發送 mail
			mailService.sendHtmlMail(
			        Lists.newArrayList(managerSid),
			        "Werp需求單分派未處理通知",
			        sb.toString());
		}
	}
}
