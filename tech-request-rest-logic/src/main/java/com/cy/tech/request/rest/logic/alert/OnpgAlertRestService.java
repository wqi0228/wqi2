/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.rest.logic.alert;

import com.cy.commons.vo.User;
import com.cy.tech.request.repository.onpg.WorkOnpgAlertRepo;
import com.cy.tech.request.rest.logic.to.WorkOnpgAlertTo;
import com.cy.tech.request.vo.onpg.WorkOnpgAlert;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.utils.WkEntityUtils;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 送測通知rest 邏輯
 *
 * @author shaun
 */
@Component
public class OnpgAlertRestService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -501258642208505785L;
    @Autowired
    private WkEntityUtils entityUtils;
    @Autowired
    private WorkOnpgAlertRepo alertDao;
    @Transactional(readOnly = true)
    public List<WorkOnpgAlertTo> findByReadStatusAndReceiverLimit(ReadRecordType readStatus, User receiver, Integer limit) {
        List<WorkOnpgAlert> result = alertDao.findBySourceTypeAndReadStatusAndReceiverOrderBySendDateDesc(
        		WorkSourceType.TECH_REQUEST,
                readStatus, 
                receiver, 
                new PageRequest(0, limit));
        return result.stream().map(wAlert -> {
            this.fillUser(wAlert);
            return transTo(wAlert);
        }).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<WorkOnpgAlertTo> findByReadStatusAndReceiver(ReadRecordType readStatus, User receiver) {
        List<WorkOnpgAlert> result = alertDao.findBySourceTypeAndReadStatusAndReceiverOrderBySendDateDesc(WorkSourceType.TECH_REQUEST,
                readStatus, receiver);
        return result.stream().map(wAlert -> {
            this.fillUser(wAlert);
            return transTo(wAlert);
        }).collect(Collectors.toList());
    }
    
    @Transactional(readOnly = true)
    public List<WorkOnpgAlertTo> findByReadStatusAndReceiverAndSendDate(ReadRecordType readStatus, User receiver, Date startDate, Date endDate) {
        List<WorkOnpgAlert> result = alertDao.findByReadStatusAndReceiverAndSendDate(WorkSourceType.TECH_REQUEST,
                readStatus, receiver, startDate, endDate);
        return result.stream().map(wAlert -> {
            this.fillUser(wAlert);
            return transTo(wAlert);
        }).collect(Collectors.toList());
    }

    private void fillUser(WorkOnpgAlert alert) {
        alert.setSender(WkUserCache.getInstance().findBySid(alert.getSender().getSid()));
        alert.setReceiver(WkUserCache.getInstance().findBySid(alert.getReceiver().getSid()));
    }

    @Transactional(rollbackFor = Exception.class)
    public void updateClickTime(String alertSid) {
        alertDao.updateClickTime(alertSid, new Date(), ReadRecordType.HAS_READ);
    }

    /**
     * 轉型為rest傳輸物件
     *
     * @param alert
     * @return
     */
    private WorkOnpgAlertTo transTo(WorkOnpgAlert alert) {
        WorkOnpgAlertTo to = new WorkOnpgAlertTo();
        entityUtils.copyProperties(alert, to);
        //to.setSid(alert.getSid());
        if (alert.getOnpg() != null) {
            to.setOnpgSid(alert.getOnpg().getSid());
        }
        //to.setTestinfoNo(alert.getTestinfoNo());
        //to.setSourceSid(alert.getSourceSid());
        //to.setSourceNo(alert.getSourceNo());
        //to.setTestinfoTheme(alert.getTestinfoTheme());
        //to.setSender(alert.getSender());
        if (alert.getSender() != null) {
            to.setSenderSid(alert.getSender().getSid());
            to.setSenderName(alert.getSender().getName());
        }
        //to.setSendDate(alert.getSendDate());
        //to.setReceiver(alert.getReceiver());
        if (alert.getReceiver() != null) {
            to.setReceiverSid(alert.getReceiver().getSid());
            to.setReceiverName(alert.getReceiver().getName());
        }
        if (alert.getCheckRecord() != null) {
            to.setCheckRecordSid(alert.getCheckRecord().getSid());
        }
        if (alert.getCheckRecordReply() != null) {
            to.setCheckRecordReplySid(alert.getCheckRecordReply().getSid());
        }
        //to.setReadStatus(alert.getReadStatus());
        //to.setClickTime(alert.getClickTime());
        return to;
    }
}
