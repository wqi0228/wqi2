/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.rest.logic.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author shaun
 */
@Configuration("com.cy.tech.request.rest.logic.config")
@ComponentScan("com.cy.tech.request.rest.logic")
public class LogicConfig {

}
