package com.cy.tech.request.rest.logic.to;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * rest 傳遞用(案件單使用)
 *
 * @author kasim
 */
@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CategoryTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2910536102298531298L;
    /** 小類 SID */
    @JsonProperty(value = "sid")
    private String sid;
    /** 小類名稱 */
    @JsonProperty(value = "name")
    private String name;

    public CategoryTo(String sid, String name) {
        this.sid = sid;
        this.name = name;
    }
}
