package com.cy.tech.request.rest.logic.helper;

import com.cy.bpm.rest.client.BpmOrganizationClient;
import com.cy.bpm.rest.to.HistoryTaskTo;
import com.cy.bpm.rest.vo.ProcessTask;
import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.commons.util.FusionUrlServiceUtils;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.formsigning.enums.FormSigningType;
import com.cy.formsigning.enums.FormType;
import com.cy.formsigning.enums.LanguageType;
import com.cy.formsigning.vo.FormInfo;
import com.cy.tech.request.logic.enumerate.PropKeyType;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.rest.logic.search.vo.PtView;
import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.pt.PtSignInfo;
import com.cy.work.common.utils.WkOrgUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PtFormInfoHelper extends AbstractFormInfoHelper {

    /**
     * 
     */
    private static final long serialVersionUID = 2628051001836888061L;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private URLService urlService;
    @Autowired
    private FormHelper formHelper;
    @Autowired
    private BpmOrganizationClient organizationClient;

    public FormInfo transform(PtView view, ProcessTaskBase base, User executor, LanguageType lType) {
        FormInfo info = new FormInfo();
        info.setFormId(view.getPtNo());
        info.setBpmFlowId(view.getBpmInstanceId());
        info.setCompany(WkOrgUtils.getOrgName(view.getCreateCompany()));
        info.setCompanyUuid(view.getCreateCompany().getUuid());
        info.setFormType(FormType.REQUIRE_PROTOTYPE);
        info.setFormDate(view.getCreatedDate());
        if (base instanceof ProcessTaskHistory) {
            ProcessTaskHistory history = (ProcessTaskHistory) base;
            super.assignFormInfoByTaskHistory(info, history, executor.getId());
            info.setSubmittedDate(history.getStartTime() != null ? history.getStartTime() : history.getDeliverTime());
            info.setSignedDate(history.getTaskEndTime());
        }
        if (base instanceof ProcessTask) {
            ProcessTask task = (ProcessTask) base;
            info.setSubmittedDate(task.getStartTime() != null ? task.getStartTime() : task.getDeliverTime());
            try {
                info.setBeingAgentId(this.getApprover(base, executor)); // set beingAgentId field here
            } catch (ProcessRestException e) {
                log.error("Call getApprover method is fail(taskId: " + task.getInstanceID() + "), error cause: " + e.getMessage(), e);
            }
        }
        super.restApplierInfo(info, view.getCreatedUser().getId());
        info.setDescription(view.getPtTheme());
        info.setUrl(getContextPath() + "/require/require02.xhtml" + this.getPtUrl(executor.getSid(), view.getRequireNo(), view.getPtCheckSid()));
        info.setFormStatus(formHelper.createFormStatus(view.getPaperCode(), lType));
        info.setHasEditMode(Boolean.FALSE);
        return info;
    }

    private String getPtUrl(Integer userSid, String requireNo, String ptCheckSid) {
        return urlService.createUrlLinkParamForTab(
                  URLService.URLServiceAttr.URL_ATTR_M,
                  urlService.createSimpleUrlTo(userSid, requireNo, 1),
                  URLService.URLServiceAttr.URL_ATTR_TAB_PT,
                  ptCheckSid);
    }

    public FormInfo transform(PtCheck ptCheck, User executor, LanguageType lType) {
        return this.transform(ptCheck.getSignInfo(), executor, lType);
    }

    public FormInfo transform(PtSignInfo wtsi, User executor, LanguageType lType) {

        Org company = orgService.findBySid(wtsi.getPtCheck().getCreateCompany().getSid());
        FormInfo info = new FormInfo();
        info.setFormId(wtsi.getPtNo());
        info.setBpmFlowId(wtsi.getBpmInstanceId());
        info.setCompany(WkOrgUtils.getOrgName(company));
        info.setCompanyUuid(company.getUuid());
        info.setFormType(FormType.REQUIRE_PROTOTYPE);
        info.setFormDate(wtsi.getPtCheck().getCreatedDate());
        for (int i = wtsi.getTaskTo().getTasks().size() - 1; i >= 0; i--) {
            ProcessTaskBase each = wtsi.getTaskTo().getTasks().get(i);
            if (each instanceof ProcessTaskHistory) {
                ProcessTaskHistory history = (ProcessTaskHistory) each;
                try {
                    info.setDefApproves(bpmHelper.getDefaultApprover(history));
                } catch (ProcessRestException e) {
                    log.error("info.setDefApproves fail..", e);
                }
                if (history.getExecutorID().equals(executor.getId()) || info.getDefApproves().contains(executor.getId())) {
                    super.assignFormInfoByTaskHistory(info, history, executor.getId());
                    info.setSubmittedDate(history.getStartTime() != null ? history.getStartTime() : history.getDeliverTime());
                    info.setSignedDate(history.getTaskEndTime());
                }
                break;
            }
        }
        if (info.getSubmittedDate() == null) {
            if (wtsi.getCanSignedIdsTo().getValue().contains(executor.getId())) {
                List<ProcessTaskBase> tasks = wtsi.getTaskTo().getTasks();
                ProcessTaskBase base = tasks.get(tasks.size() - 1);
                if (base instanceof ProcessTask) {
                    ProcessTask task = (ProcessTask) base;
                    info.setSubmittedDate(task.getStartTime() != null ? task.getStartTime() : task.getDeliverTime());
                    try {
                        info.setBeingAgentId(this.getApprover(base, executor)); // set beingAgentId field here
                    } catch (ProcessRestException e) {
                        log.error("Call getApprover method is fail(taskId: " + task.getInstanceID() + "), error cause: " + e.getMessage(), e);
                    }
                }
            }
        }
        super.restApplierInfo(info, wtsi.getPtCheck().getCreatedUser().getSid());
        info.setDescription(wtsi.getPtCheck().getTheme());
        info.setUrl(getContextPath() + "/require/require02.xhtml" + this.getPtUrl(executor.getSid(), wtsi.getSourceNo(), wtsi.getPtCheck().getSid()));
        info.setFormStatus(formHelper.createFormStatus(wtsi.getInstanceStatus(), lType));
        info.setHasEditMode(Boolean.FALSE);
        return info;
    }

    public FormSigningType findFormTypeForUser(PtSignInfo wtsi, User executor) {
        if (wtsi.getCanSignedIdsTo().getValue().contains(executor.getId())) {
            return FormSigningType.CURRENT_SIGNER;
        }
        if (wtsi.getTaskTo().getTasks().stream()
                  .filter(base -> base instanceof ProcessTaskHistory)
                  .map(base -> (ProcessTaskHistory) base)
                  .anyMatch(history -> history.getExecutorID().equals(executor.getId()))) {
            return FormSigningType.NOT_CURRENT_SIGNER_PROCESSED;
        } else {
            return FormSigningType.NOT_SIGNER;
        }
    }

    public FormInfo transHistoryForm(PtView view, HistoryTaskTo historyTo, User executor, LanguageType lType) {
        FormInfo info = new FormInfo();
        info.setFormId(view.getPtNo());
        info.setBpmFlowId(view.getBpmInstanceId());
        info.setCompany(WkOrgUtils.getOrgName(view.getCreateCompany()));
        info.setCompanyUuid(view.getCreateCompany().getUuid());
        info.setFormType(FormType.REQUIRE_PROTOTYPE);
        info.setFormDate(view.getCreatedDate());
        info.setSubmittedDate(historyTo.getStartTime() != null ? historyTo.getStartTime() : historyTo.getDeliverTime());
        info.setSignedDate(historyTo.getTaskEndTime());
        super.restApplierInfo(info, view.getCreatedUser().getId());
        info.setDescription(view.getPtTheme());
        info.setUrl(getContextPath() + "/require/require02.xhtml" + this.getPtUrl(executor.getSid(), view.getRequireNo(), view.getPtCheckSid()));
        info.setFormStatus(formHelper.createFormStatus(view.getPaperCode(), lType));
        info.setHasEditMode(Boolean.FALSE);
        super.assignFormInfoByHistoryTo(info, historyTo, executor.getId());
        return info;
    }

    private String getContextPath() {
        return FusionUrlServiceUtils.getApPathByPropKey(PropKeyType.TECH_REQUEST_AP_URL.getValue());
    }

    public List<String> getApprover(ProcessTaskBase taskBase) throws ProcessRestException {
        String userId = taskBase.getUserID();
        String roleId = taskBase.getRoleID();
        if (taskBase instanceof ProcessTaskHistory) {
            userId = ((ProcessTaskHistory) taskBase).getExecutorID();
            roleId = ((ProcessTaskHistory) taskBase).getExecutorRoleID();
        }
        return !Strings.isNullOrEmpty(userId) ? Lists.newArrayList(userId) : organizationClient.findUserFromRole(roleId);
    }

    public String getApprover(ProcessTaskBase taskBase, User executor) throws ProcessRestException {
        String userId = taskBase.getUserID();
        String roleId = taskBase.getRoleID();
        if (taskBase instanceof ProcessTaskHistory) {
            userId = ((ProcessTaskHistory) taskBase).getExecutorID();
            roleId = ((ProcessTaskHistory) taskBase).getExecutorRoleID();
        }
        log.debug("executor:{}", executor.getId());
        log.debug("userId:{}, roleId:{}", userId, roleId);
        List<String> list = !Strings.isNullOrEmpty(userId) ? Lists.newArrayList(userId) : organizationClient.findUserFromRole(roleId);
        log.debug("list:{}", list);
        return list.contains(executor.getId()) ? executor.getId() : list.get(0);
    }
}
