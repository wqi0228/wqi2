package com.cy.tech.request.rest.logic.search.vo;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.search.view.BaseSearchView;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 需求單位流程容器
 *
 * @author shaun
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class RequireUnitView extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4497429329551394935L;
    /** 申請人所歸屬的公司 */
    private Org createCompany;
    /** 建單成員 */
    private User createdUser;
    /** 建立日期 */
    private Date createdDate;
    /** 流程編號 */
    private String bpmInstanceId;
    /** 流程狀態碼 */
    private String paperCode;
}
