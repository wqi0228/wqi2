/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.rest.logic;

import com.cy.commons.util.FusionUrlServiceUtils;
import com.cy.tech.request.logic.enumerate.PropKeyType;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.logic.vo.UrlParamTo;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 網址服務
 *
 * @author shaun
 */
@Component
public class ReqUrlRestService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4794473038161414925L;
    @Autowired
    private URLService urlService;
    private final String req02Path = "/require/require02.xhtml";

    public String findRequireUrl(Integer userSid, String requireNo) {
        UrlParamTo para = urlService.createSimpleUrlTo(userSid, requireNo, 1);
        return getContextPath() + req02Path + urlService.createUrlLinkParam(URLService.URLServiceAttr.URL_ATTR_M, para);
    }

    public String findPtUrl(Integer userSid, String requireNo, String ptSid) {
        UrlParamTo para = urlService.createSimpleUrlTo(userSid, requireNo, 1);
        return getContextPath() + req02Path + urlService.createUrlLinkParamForTab(URLService.URLServiceAttr.URL_ATTR_M, para, URLService.URLServiceAttr.URL_ATTR_TAB_PT, ptSid);
    }

    public String findSendTestUrl(Integer userSid, String requireNo, String testinfoSid) {
        UrlParamTo para = urlService.createSimpleUrlTo(userSid, requireNo, 1);
        return getContextPath() + req02Path + urlService.createUrlLinkParamForTab(URLService.URLServiceAttr.URL_ATTR_M, para, URLService.URLServiceAttr.URL_ATTR_TAB_ST, testinfoSid);
    }

    public String findOnpgUrl(Integer userSid, String requireNo, String onpgSid) {
        UrlParamTo para = urlService.createSimpleUrlTo(userSid, requireNo, 1);
        return getContextPath() + req02Path + urlService.createUrlLinkParamForTab(URLService.URLServiceAttr.URL_ATTR_M, para, URLService.URLServiceAttr.URL_ATTR_TAB_OP, onpgSid);
    }

    public String findOthsetUrl(Integer userSid, String requireNo, String othsetSid) {
        UrlParamTo para = urlService.createSimpleUrlTo(userSid, requireNo, 1);
        return getContextPath() + req02Path + urlService.createUrlLinkParamForTab(URLService.URLServiceAttr.URL_ATTR_M, para, URLService.URLServiceAttr.URL_ATTR_TAB_OS, othsetSid);
    }
    
    private String getContextPath(){
        return FusionUrlServiceUtils.getApPathByPropKey(PropKeyType.TECH_REQUEST_AP_URL.getValue());
    }
}
