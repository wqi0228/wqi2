package com.cy.tech.request.rest.logic.helper;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.cy.bpm.rest.to.HistoryTaskTo;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.vo.User;
import com.cy.formsigning.vo.FormInfo;
import com.cy.tech.request.logic.service.UserService;
import com.cy.tech.request.rest.logic.process.BPMHelper;
import com.cy.work.common.cache.WkUserCache;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author aken_kao
 */
@Slf4j
public abstract class AbstractFormInfoHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1724420247920921104L;
    @Autowired
    protected BPMHelper bpmHelper;
    @Autowired
    protected UserService userService;

    /**
     * form-sign-api-3.13.2 add
     *
     * @param info
     * @param history
     * @param executorId
     */
    public void assignFormInfoByTaskHistory(FormInfo info, ProcessTaskHistory history, String executorId) {
        try {
            // form-sign-api-3.13.2 add
            info.setIsHistoryTask(true);
            info.setBpmTaskId(history.getSid());
            info.setBpmDefUserId(history.getUserID());
            info.setBpmDefRoleId(history.getRoleID());
            info.setExecutorId(history.getExecutorID());
            if (info.getDefApproves() == null) {
                info.setDefApproves(bpmHelper.getDefaultApprover(history));
            }
            this.resetAgentInfo(info, executorId);
        } catch (ProcessRestException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * form-sign-api-3.13.2 add
     *
     * @param info
     * @param historyTo
     * @param executorId
     */
    public void assignFormInfoByHistoryTo(FormInfo info, HistoryTaskTo historyTo, String executorId) {
        try {
            // form-sign-api-3.13.2 add
            info.setIsHistoryTask(true);
            info.setBpmTaskId(historyTo.getSid());
            info.setBpmDefUserId(historyTo.getUserID());
            info.setBpmDefRoleId(historyTo.getRoleID());
            info.setExecutorId(historyTo.getExecutorID());
            info.setDefApproves(bpmHelper.getDefaultApprover(historyTo));
            if (!Strings.isNullOrEmpty(executorId)) {
                this.resetAgentInfo(info, executorId);
            }
        } catch (ProcessRestException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 設定代理人資訊 (FormInfo 的 defApproves & executorId需先設值)
     *
     * @param info
     * @param viewerId
     * @return
     */
    public FormInfo resetAgentInfo(FormInfo info, String viewerId) {
        List<String> defApproves = Lists.newArrayList();
        if (info.getDefApproves() != null) {
            defApproves = info.getDefApproves(); // 預設簽核名單
        }
        String executorId = info.getExecutorId();// 執行者ID
        //代理他人 = 預設簽核名單不包含檢閱者 且檢閱者ID等於執行者ID
        //被代理   = 預設簽核名單包含檢閱者   且檢閱者ID不等於執行者ID
        //本身單據 = 預設簽核名單包含檢閱者   且檢閱者ID等於執行者ID
        if (!defApproves.contains(viewerId) && viewerId.equals(executorId)) {
            if (!defApproves.isEmpty()) {
                info.setBeAgentName(WkUserCache.getInstance().findById(defApproves.get(0)).getName());//顯示被代理人ID
            }
        } else if (defApproves.contains(viewerId) && !viewerId.equals(executorId)) {
            if (!defApproves.isEmpty()) {
                info.setAgentName(WkUserCache.getInstance().findById(executorId).getName());//顯示執行者ID
            }
        }
        return info;
    }

    /**
     * 設定申請人資訊
     *
     * @param info
     * @param applierId
     * @return
     */
    public FormInfo restApplierInfo(FormInfo info, String applierId) {
        try {
            User applier = WkUserCache.getInstance().findById(applierId);
            info.setApplierId(applier.getId());
            info.setApplierName(applier.getName());
        } catch (Exception e) {
            log.error("成員資訊錯誤!! ID : " + applierId, e);
            info.setApplierId(applierId);
            info.setApplierName(applierId);
        }
        return info;
    }

    /**
     * 設定申請人資訊
     *
     * @param info
     * @param applierId
     * @return
     */
    public FormInfo restApplierInfo(FormInfo info, Integer applierSid) {
        try {
            User applier = WkUserCache.getInstance().findBySid(applierSid);
            info.setApplierId(applier.getId());
            info.setApplierName(applier.getName());
        } catch (Exception e) {
            log.error("成員資訊錯誤!! SID : " + applierSid, e);
            info.setApplierId(applierSid.toString());
            info.setApplierName(applierSid.toString());
        }
        return info;
    }
}
