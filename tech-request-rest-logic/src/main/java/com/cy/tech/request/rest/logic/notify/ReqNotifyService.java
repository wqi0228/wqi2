package com.cy.tech.request.rest.logic.notify;

import com.cy.tech.request.logic.config.ReqConstants;
import com.cy.tech.request.rest.logic.to.WorkNotifyTo;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.notify.logic.manager.WorkNotifyManager;
import com.cy.work.notify.vo.enums.NotifyType;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kasim
 */
@Slf4j
@Component
public class ReqNotifyService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1161850011547712540L;
    @Autowired
    @Qualifier(ReqConstants.REQ_JDBC_TEMPLATE)
    private JdbcTemplate jdbc;
    @Autowired
    private WorkNotifyManager notifyManager;

    /**
     * 查詢
     *
     * @param userSid
     * @return
     */
    public List<WorkNotifyTo> findFirst10ByUserSid(Integer userSid) {
        try {
            List<WorkNotifyTo> dbResult = this.jdbc.query(
                    this.buildNotifySql(userSid),
                    (ResultSet rs, int rowNum) -> this.transTo(rs));
            return dbResult;
        } catch (EmptyResultDataAccessException e) {
            log.error(e.getMessage(), e);
            return Lists.newArrayList();
        }
    }

    /**
     * 組合SQL
     *
     * @param userSid
     * @param customizes
     * @return
     */
    private String buildNotifySql(Integer userSid) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT notify.wn_sid, ");
        sql.append("notify.source_sid, notify.source_no, ");
        sql.append("notify.source_type, notify.wn_type, ");
        sql.append("notify.wn_content, notify.get_wn_usr ");
        sql.append("FROM work_notify notify ");
        sql.append("WHERE notify.source_type = 'TECH_REQUEST' ");
        sql.append("AND notify.get_wn_usr = ").append(userSid).append(" ");
        sql.append(" ORDER BY notify.create_dt DESC");
        sql.append(" LIMIT 10 ");
        return sql.toString();
    }

    /**
     * 轉換自訂物件
     *
     * @param obj
     * @return
     */
    private WorkNotifyTo transTo(ResultSet rs) throws SQLException {
        WorkNotifyTo result = new WorkNotifyTo();
        result.setSid(rs.getString("wn_sid"));
        result.setSourceSid(rs.getString("source_sid"));
        result.setSourceNo(rs.getString("source_no"));
        result.setSourceType(WorkSourceType.valueOf(rs.getString("source_type")));
        result.setNotifyType(NotifyType.valueOf(rs.getString("wn_type")));
        result.setNotifyTypeDesc(NotifyType.valueOf(rs.getString("wn_type")).getValue());
        result.setContent(rs.getString("wn_content"));
        result.setUserSid(rs.getInt("get_wn_usr"));
        return result;
    }

    /**
     * 讀取訊息
     *
     * @param userSid
     * @param notifyType
     * @param sourceNo
     */
    @Transactional(rollbackFor = Exception.class)
    public void readNotify(Integer userSid, NotifyType notifyType, String sourceNo) {
        notifyManager.readNotify(userSid, notifyType, sourceNo);
    }
}
