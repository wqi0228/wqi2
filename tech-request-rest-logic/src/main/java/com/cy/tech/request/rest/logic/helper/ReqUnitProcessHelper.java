package com.cy.tech.request.rest.logic.helper;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.vo.User;
import com.cy.formsigning.enums.FormType;
import com.cy.formsigning.vo.BatchSignedResult;
import com.cy.formsigning.vo.ButtonInfo;
import com.cy.formsigning.vo.ButtonInfoWithOneSelectMenuAndOneTextarea;
import com.cy.formsigning.vo.SelectItems;
import com.cy.tech.request.logic.service.ReqUnitBpmService;
import com.cy.tech.request.rest.logic.enums.ActionType;
import com.cy.tech.request.vo.require.Require;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ReqUnitProcessHelper implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -5235000410959738659L;
    @Autowired
	private ReqUnitBpmService bpmService;

	public void createSignBtn(List<ButtonInfo> btns, Require req, User executor) {
		if (bpmService.showSignBtn(req, executor)) {
			btns.add(new ButtonInfo("簽名", req.getRequireNo() + "," + ActionType.SIGN));
		}
	}

	public void createRollBackBtn(List<ButtonInfo> btns, Require req, User executor) {
		if (bpmService.showRollBackBtn(req, executor)) {
			ButtonInfoWithOneSelectMenuAndOneTextarea btn = new ButtonInfoWithOneSelectMenuAndOneTextarea();
			btn.setButtonValue("退回");
			btn.setAction(req.getRequireNo() + "," + ActionType.ROLLBACK);
			btn.setInputTitle("退回原因");// 退回視窗下方輸入框旁左側文字
			btn.setSelectItems(this.createRollBackSelectItem(req));// 退回視窗下拉選項
			btns.add(btn);
		}
	}

	private SelectItems createRollBackSelectItem(Require req) {
		List<ProcessTaskBase> tasks = bpmService.findFlowChartByInstanceId(req.getReqUnitSign().getBpmInstanceId(), "");
		SelectItems items = SelectItems.create("退回至");
		tasks.remove(tasks.size() - 1);
		tasks.forEach(each -> {
			if (each instanceof ProcessTaskHistory) {
				ProcessTaskHistory history = (ProcessTaskHistory) each;
				// key = 顯示名稱 , value = 回傳資訊
				items.addItem(history.getTaskName().contains("申請人")
				        ? "申請人-" + history.getExecutorName()
				        : history.getRoleName() + "-" + history.getExecutorName(),
				        history.getSid());
			}
		});
		return items;
	}

	public void createRecoveryBtn(List<ButtonInfo> btns, Require req, User executor) {
		if (bpmService.showRecoveryBtn(req, executor)) {
			btns.add(new ButtonInfo("復原", req.getRequireNo() + "," + ActionType.RECOVERY));
		}
	}

	public void createInvaildBtn(List<ButtonInfo> btns, Require req, User executor) {
		if (bpmService.showInvaildBtn(req, executor)) {
			btns.add(new ButtonInfo("作廢", req.getRequireNo() + "," + ActionType.INVAILD));
		}
	}

	/**
	 * 執行批次簽核
	 *
	 * @param reqs
	 * @param executor
	 * @return
	 */
	public BatchSignedResult doBatchSign(List<Require> reqs, User executor) {

		if (reqs == null) {
			reqs = Lists.newArrayList();
		}

		BatchSignedResult toReturn = new BatchSignedResult(reqs.size(), FormType.REQUIRE_UNIT);

		for (Require require : reqs) {
			if (!this.bpmService.showSignBtn(require, executor)) {
				continue;
			}

			try {
				// 單筆簽名
				this.bpmService.doSign(require, executor, "");
				// 記錄成功資訊
				toReturn.addSuccess(require.getRequireNo());

			} catch (ProcessRestException | UserMessageException | SystemOperationException ex) {
				String msg = "Unexpected exception: when " + executor.getId() + " sign the form(" + require.getRequireNo() + ") -- "
				        + ex.getClass().getName() + ": " + ex.getMessage();
				log.error(msg, ex);
				toReturn.addFail(require.getRequireNo(), msg);
			}

		}
		return toReturn;
	}

	/**
	 * 執行流程動作
	 *
	 * @param actType
	 * @param req
	 * @param executor
	 * @param inputData
	 * @return
	 * @throws ProcessRestException
	 * @throws UserMessageException
	 * @throws SystemDevelopException
	 */
	public Require doAction(ActionType actType, Require req, User executor, Map<String, Object> inputData)
	        throws ProcessRestException, UserMessageException, SystemOperationException {
		log.info(executor.getName() + " 執行 [" + req.getRequireNo() + "]   " + actType.getDesc());
		switch (actType) {
		case SIGN:
			return this.sign(req, executor);
		case RECOVERY:
			return this.recovery(req, executor);
		case ROLLBACK:
			return this.rollback(req, executor, inputData);
		case INVAILD:
			return this.invaild(req, executor);
		default:
			throw new IllegalArgumentException("非有效簽核型態 action type = " + actType + " 單號:" + req.getRequireNo());
		}
	}

	private Require sign(Require req, User executor) throws ProcessRestException, UserMessageException, SystemOperationException {
		Preconditions.checkArgument(bpmService.showSignBtn(req, executor), "工作節點可能已變更，無法進行簽核！！");
		bpmService.doSign(req, executor, "");
		return req;
	}

	private Require recovery(Require req, User executor) throws UserMessageException {
		bpmService.doRecovery(req, executor);
		return req;
	}

	private Require rollback(Require req, User executor, Map<String, Object> inputData) throws ProcessRestException {
		Preconditions.checkArgument(bpmService.showRollBackBtn(req, executor), "工作節點可能已變更，無法進行退回！！");
		// 退回理由
		String comment = inputData.containsKey(ButtonInfoWithOneSelectMenuAndOneTextarea.INPUT_TEXTAREA)
		        ? Strings.nullToEmpty((String) inputData.get(ButtonInfoWithOneSelectMenuAndOneTextarea.INPUT_TEXTAREA))
		        : "";
		// 任務task sid
		String key = inputData.containsKey(ButtonInfoWithOneSelectMenuAndOneTextarea.SELECT_ITEMS)
		        ? Strings.nullToEmpty((String) inputData.get(ButtonInfoWithOneSelectMenuAndOneTextarea.SELECT_ITEMS))
		        : "";
		Preconditions.checkArgument(!Strings.isNullOrEmpty(key), "指定退回節點為空值！！");
		List<ProcessTaskBase> tasks = bpmService.findFlowChartByInstanceId(req.getReqUnitSign().getBpmInstanceId(), "");
		ProcessTaskHistory rollbackTask = (ProcessTaskHistory) tasks.stream().filter(each -> each.getSid().equals(key)).findFirst().get();
		bpmService.doRollBack(req, executor, rollbackTask, comment);
		return req;
	}

	private Require invaild(Require req, User executor) throws ProcessRestException {
		Preconditions.checkArgument(bpmService.showInvaildBtn(req, executor), "工作節點可能已變更，無法進行作廢！！");
		bpmService.doInvaild(req, executor);
		return req;
	}

}
