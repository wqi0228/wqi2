package com.cy.tech.request.rest.logic.to;

import com.cy.tech.request.vo.enums.OthSetAlertType;
import com.cy.work.common.enums.ReadRecordType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * 其他設定資訊 通知訊息
 *
 * @author kasim
 */
@Data
@ToString
@EqualsAndHashCode(of = { "sid" })
@JsonIgnoreProperties(ignoreUnknown = true)
public class OthSetAlertTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1134762289118820029L;

    @JsonProperty("sid")
    private String sid;

    /** 其他設定資訊主檔Sid */
    @JsonProperty("othsetSid")
    private String othsetSid;

    /** 其他設定資訊單號 */
    @JsonProperty("osNo")
    private String osNo;

    /** 需求單主檔Sid */
    @JsonProperty("requireSid")
    private String require;
    @JsonIgnore
    private String requireSid;

    /** 需求單單號 */
    @JsonProperty("requireNo")
    private String requireNo;

    /** 其他設定資訊主題 */
    @JsonProperty("osTheme")
    private String osTheme;

    /** 訊息類型 */
    @JsonIgnore
    private String type_src;

    /** 訊息類型 */
    @JsonProperty("type")
    private OthSetAlertType type;

    /** 寄件者Sid */
    @JsonProperty("senderSid")
    private Integer senderSid;

    /** 寄件者Name */
    @JsonProperty("senderName")
    private String senderName;

    /** 寄件日期 */
    @JsonProperty("sendDate")
    private Date sendDate;

    /** 收件者Sid */
    @JsonProperty("receiverSid")
    private Integer receiverSid;

    /** 收件者Name */
    @JsonProperty("receiverName")
    private String receiverName;

    /** 來自於哪一則回覆sid */
    @JsonProperty("replySid")
    private String replySid;

    /** 來自於哪一則回覆的回覆sid */
    @JsonProperty("alreadyReplySid")
    private String alreadyReplySid;

    /** 訊息類型 */
    @JsonIgnore
    private String readStatus_src;

    /** 讀取狀態 */
    @JsonProperty("readStatus")
    private ReadRecordType readStatus;

    /** 點擊時間 */
    @JsonProperty("clickTime")
    private Date clickTime;

}
