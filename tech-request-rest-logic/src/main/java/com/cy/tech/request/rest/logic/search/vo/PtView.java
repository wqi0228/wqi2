package com.cy.tech.request.rest.logic.search.vo;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.search.view.BaseSearchView;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 原型確認流程容器
 *
 * @author shaun
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {"ptCheckSid"})
public class PtView extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1153446795387701841L;
    /** 原型確認sid */
    private String ptCheckSid;
    /** 原型確認單號 */
    private String ptNo;
    /** 原型確認主題 */
    private String ptTheme;
    /** 原型確認單申請人所歸屬的公司 */
    private Org createCompany;
    /** 原型確認單建單成員 */
    private User createdUser;
    /** 原型確認單建立日期 */
    private Date createdDate;
    /** 原型確認單流程編號 */
    private String bpmInstanceId;
    /** 原型確認單流程狀態碼 */
    private String paperCode;
}
