/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.rest.logic.helper;

import com.cy.formsigning.enums.LanguageType;
import com.cy.tech.request.logic.service.CommonService;
import com.cy.work.common.enums.InstanceStatus;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author shaun
 */
@Component
public class FormHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1077376353026097159L;
    @Autowired
    private CommonService commonService;

    public String createFormStatus(String paperCode, LanguageType type) {
        return commonService.get(InstanceStatus.fromPaperCode(paperCode));
    }

    public String createFormStatus(InstanceStatus status, LanguageType type) {
        return commonService.get(status);
    }
}
