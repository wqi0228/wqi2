package com.cy.tech.request.rest.logic.search.vo;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.search.view.BaseSearchView;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 送測流程容器
 *
 * @author shaun
 */
@Data
@EqualsAndHashCode(callSuper = true, of = {"testinfoSid"})
public class SendTestView extends BaseSearchView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2413201217949387384L;
    /** 送測sid */
    private String testinfoSid;
    /** 送測單號 */
    private String testinfoNo;
    /** 送測主題 */
    private String testinfoTheme;
    /** 送測單申請人所歸屬的公司 */
    private Org createCompany;
    /** 送測單建單成員 */
    private User createdUser;
    /** 送測單建立日期 */
    private Date createdDate;
    /** 送測單流程編號 */
    private String bpmInstanceId;
    /** 送測單流程狀態碼 */
    private String paperCode;
}
