package com.cy.tech.request.rest.logic.to;

import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.enums.WorkSourceType;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * 原型確認通知訊息(Rest傳輸用)
 *
 * @author kasim
 */
@Data
@ToString
@EqualsAndHashCode(of = {"sid"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class PtAlertTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5104530359855173739L;

    /** 原型確認通知Sid */
    @JsonProperty("sid")
    private String sid;

    /** 原型確認單主檔 */
    @JsonProperty("ptSid")
    private String ptSid;

    /** 原型確認單號 */
    @JsonProperty("ptNo")
    private String ptNo;

    /** 原型來源 */
    @JsonProperty("sourceType")
    private WorkSourceType sourceType;

    /** 需求單Sid */
    @JsonProperty("sourceSid")
    private String sourceSid;

    /** 需求單單號 */
    @JsonProperty("sourceNo")
    private String sourceNo;

    /** 原型確認主題 */
    @JsonProperty("ptTheme")
    private String ptTheme;

    /** 寄件者Sid */
    @JsonProperty("senderSid")
    private Integer senderSid;

    /** 寄件者Name */
    @JsonProperty("senderName")
    private String senderName;

    /** 寄件日期 */
    @JsonProperty("sendDate")
    private Date sendDate;

    /** 收件者Sid */
    @JsonProperty("receiverSid")
    private Integer receiverSid;

    /** 收件者Name */
    @JsonProperty("receiverName")
    private String receiverName;

    /** 來自於哪一則原型確認回覆sid */
    @JsonProperty("replySid")
    private String replySid;

    /** 來自於哪一則原型確認回覆的回覆sid */
    @JsonProperty("alreadyReplySid")
    private String alreadyReplySid;

    /** 讀取狀態 */
    @JsonProperty("readStatus")
    private ReadRecordType readStatus;

    /** 點擊時間 */
    @JsonProperty("clickTime")
    private Date clickTime;

}
