/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.rest.logic.alert;

import com.cy.commons.vo.User;
import com.cy.tech.request.repository.pt.PtAlertRepo;
import com.cy.tech.request.rest.logic.to.PtAlertTo;
import com.cy.tech.request.vo.pt.PtAlert;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.enums.WorkSourceType;
import com.cy.work.common.utils.WkEntityUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 原型確認通知rest 邏輯
 *
 * @author shaun
 */
@Component
public class PtAlertRestService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5212210430336055130L;
    @Autowired
    private WkEntityUtils entityUtils;
    @Autowired
    private PtAlertRepo alertDao;
    @Transactional(readOnly = true)
    public List<PtAlertTo> findByReadStatusAndReceiverLimit(ReadRecordType readStatus, User receiver, Integer limit) {
        List<PtAlert> result = alertDao.findBySourceTypeAndReadStatusAndReceiverOrderBySendDateDesc(WorkSourceType.TECH_REQUEST,
                readStatus, receiver, new PageRequest(0, limit));
        return result.stream().map(wAlert -> {
            this.fillUser(wAlert);
            return transTo(wAlert);
        }).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<PtAlertTo> findByReadStatusAndReceiver(ReadRecordType readStatus, User receiver) {
        List<PtAlert> result = alertDao.findBySourceTypeAndReadStatusAndReceiverOrderBySendDateDesc(WorkSourceType.TECH_REQUEST,
                readStatus, receiver);
        return result.stream().map(wAlert -> {
            this.fillUser(wAlert);
            return transTo(wAlert);
        }).collect(Collectors.toList());
    }
    
    @Transactional(readOnly = true)
    public List<PtAlertTo> findByReadStatusAndReceiverAndSendDate(ReadRecordType readStatus, User receiver, Date startDate, Date endDate) {
        List<PtAlert> result = alertDao.findBySourceTypeAndReadStatusAndReceiverAndSendDate(WorkSourceType.TECH_REQUEST,
                readStatus, receiver, startDate, endDate);
        return result.stream().map(wAlert -> {
            this.fillUser(wAlert);
            return transTo(wAlert);
        }).collect(Collectors.toList());
    }

    private void fillUser(PtAlert alert) {
        alert.setSender(WkUserCache.getInstance().findBySid(alert.getSender().getSid()));
        alert.setReceiver(WkUserCache.getInstance().findBySid(alert.getReceiver().getSid()));
    }

    @Transactional(rollbackFor = Exception.class)
    public void updateClickTime(String alertSid) {
        alertDao.updateClickTime(alertSid, new Date(), ReadRecordType.HAS_READ);
    }

    /**
     * 轉型為rest傳輸物件
     *
     * @param alert
     * @return
     */
    private PtAlertTo transTo(PtAlert alert) {
        PtAlertTo to = new PtAlertTo();
        entityUtils.copyProperties(alert, to);
        //to.setSid(alert.getSid());
        if (alert.getPtCheck() != null) {
            to.setPtSid(alert.getPtCheck().getSid());
        }
        //to.setPtNo(alert.getPtNo());
        //to.setSourceSid(alert.getSourceSid());
        //to.setSourceNo(alert.getSourceNo());
        //to.setPtTheme(alert.getPtTheme());
        //to.setSender(alert.getSender());
        if (alert.getSender() != null) {
            to.setSenderSid(alert.getSender().getSid());
            to.setSenderName(alert.getSender().getName());
        }
        //to.setSendDate(alert.getSendDate());
        //to.setReceiver(alert.getReceiver());
        if (alert.getReceiver() != null) {
            to.setReceiverSid(alert.getReceiver().getSid());
            to.setReceiverName(alert.getReceiver().getName());
        }
        if (alert.getReply() != null) {
            to.setReplySid(alert.getReply().getSid());
        }
        if (alert.getAlreadyReply() != null) {
            to.setAlreadyReplySid(alert.getAlreadyReply().getSid());
        }
        //to.setReadStatus(alert.getReadStatus());
        //to.setClickTime(alert.getClickTime());
        return to;
    }
}
