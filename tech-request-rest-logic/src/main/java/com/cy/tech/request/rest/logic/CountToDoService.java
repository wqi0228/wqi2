/**
 * 
 */
package com.cy.tech.request.rest.logic;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.antkorwin.xsync.XSync;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.tech.request.logic.search.enums.Search07ConditionType;
import com.cy.tech.request.logic.search.service.ReqConfirmDepSearchHelper;
import com.cy.tech.request.logic.service.ReqWorkCountService;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.URLService.URLServiceAttr;
import com.cy.tech.request.logic.service.setting.SettingCheckConfirmRightService;
import com.cy.tech.request.logic.vo.WaitingTaskCountTo;
import com.cy.tech.request.vo.constants.ReqPermission;
import com.cy.tech.request.vo.enums.AssignSendType;
import com.cy.tech.request.vo.enums.InboxType;
import com.cy.tech.request.vo.enums.ReqConfirmDepProgStatus;
import com.cy.tech.request.vo.enums.WkCommonCacheType;
import com.cy.work.backend.logic.WorkBackendParamHelper;
import com.cy.work.common.cache.WkCommonCache;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.cache.WkUserWithRolesCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Service
@Slf4j
public class CountToDoService {

    // ========================================================================
    // 服務
    // ========================================================================

    @Autowired
    private XSync<Integer> xSync;
    @Autowired
    private transient WkCommonCache wkCommonCache;
    @Autowired
    private transient ReqWorkCountService reqWorkCountService;
    @Autowired
    private transient SettingCheckConfirmRightService settingCheckConfirmRightService;
    @Autowired
    private transient WorkBackendParamHelper workBackendParamHelper;
    @Autowired
    private transient RequireService requireService;
    @Autowired
    private transient ReqConfirmDepSearchHelper reqConfirmDepSearchHelper;

    // ========================================================================
    //
    // ========================================================================
    /** 代辦事項class style */
    private final String todoClz = "todoClz";

    /**
     * 快取逾期時間 : 10秒
     */
    private long overdueMillisecond = 10 * 1000;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 計算所有的待辦事項項目數量 (synchronized by userSid)
     * 
     * @param userSid
     * @return
     */
    public List<WaitingTaskCountTo> syncCountAllWaitingTask(Integer userSid) {

        // final int uid = this.getUid();

        // log.debug("★★ [" + userSid + "][" + uid + "] Call ..");
        xSync.execute(userSid, () -> {
            // log.debug("★★ [" + userSid + "][" + uid + "] in sync - Start..");
            this.countAllWaitingTask(userSid);
            // log.debug("★★ [" + userSid + "][" + uid + "] in sync - Finish..");
        });

        List<WaitingTaskCountTo> results = this.wkCommonCache.getCache(
                WkCommonCacheType.REQUEST_REST_COUNT_WAIT_TASK.name(),
                Lists.newArrayList(userSid + ""),
                overdueMillisecond);

        // log.debug("★★ [" + userSid + "][" + uid + "] return");
        return results;
    }

    /**
     * 計算所有的待辦事項項目數量
     * 
     * @param userSid
     * @return
     */
    public List<WaitingTaskCountTo> countAllWaitingTask(Integer userSid) {

        // 所有待辦事項
        List<WaitingTaskCountTo> waitingTaskCountResults = Lists.newArrayList();
        // 作業起始時間
        Long startTime = System.currentTimeMillis();

        // ====================================
        // 由快取中取回
        // ====================================
        List<String> keys = Lists.newArrayList(userSid + "");
        List<WaitingTaskCountTo> cacheData = this.wkCommonCache.getCache(
                WkCommonCacheType.REQUEST_REST_COUNT_WAIT_TASK.name(),
                keys,
                overdueMillisecond);

        if (cacheData != null) {
            // log.debug("countAllWaitingTask[" + userSid + "] return by cache!");
            return (List<WaitingTaskCountTo>) cacheData;
        }

        // ====================================
        // 查詢使用者資料
        // ====================================
        // 使者用資料檔
        User user = WkUserCache.getInstance().findBySid(userSid);
        if (user == null) {
            log.warn("查詢作業失敗, 找不到使用者資料 userSid:[" + userSid + "]");
            return waitingTaskCountResults;
        }

        // 使用者單位檔
        Org dep = WkOrgCache.getInstance().findBySid(user.getPrimaryOrg().getSid());
        if (dep == null) {
            log.warn("查詢作業失敗, 找不到使用者單位資料 userSid:[" + userSid + "]");
            return waitingTaskCountResults;
        }

        Org comp = WkOrgCache.getInstance().findBySid(dep.getCompany().getSid());

        // 使用者角色
        List<String> roleNames = WkUserWithRolesCache.getInstance().findRoleNamesByUserAndLoginCompID(
                userSid,
                dep.getCompany().getId());

        List<Long> roleSids = WkUserWithRolesCache.getInstance().findActiveUserRoleSidsByUser(userSid);
        // 登入者為角色:分類確認 檢查員
        // boolean isCheckManager = roleNames.contains("分類確認 檢查員");
        // REQ-1471、REQ-1472 調整檢查員角色判斷方式
        boolean isCheckManager = settingCheckConfirmRightService.hasCanCheckUserRight(userSid);
        // 登入者為角色:ON程式管理員
        boolean isOnpgManager = roleNames.contains("ON程式管理員");

        // ====================================
        // 各種待辦事項筆數查詢
        // ====================================
        this.totalQuery(
                waitingTaskCountResults,
                userSid,
                dep,
                comp,
                roleSids,
                roleNames,
                isCheckManager,
                isOnpgManager);

        // ====================================
        // 移除筆數為空
        // ====================================
        waitingTaskCountResults = waitingTaskCountResults.stream()
                .filter(vo -> !"".equals(vo.getToDoCount()) && !"0".equals(vo.getToDoCount()))
                .collect(Collectors.toList());

        // ====================================
        // 記錄快取
        // ====================================
        this.wkCommonCache.putCache(WkCommonCacheType.REQUEST_REST_COUNT_WAIT_TASK.name(), keys, waitingTaskCountResults);

        log.info(WkCommonUtils.prepareCostMessage(startTime, "查詢所有待辦[" + userSid + "-" + user.getName() + "]"));

        return waitingTaskCountResults;

    }

    /**
     * 全部的項目筆數查詢
     * 
     * @param waitingTaskCountResults
     * @param userSid
     * @param dep
     * @param roleSids
     * @param roleNames
     * @param isCheckManager
     * @param isOnpgManager
     */
    private void totalQuery(
            List<WaitingTaskCountTo> waitingTaskCountResults,
            Integer userSid,
            Org dep,
            Org comp,
            List<Long> roleSids,
            List<String> roleNames,
            boolean isCheckManager,
            boolean isOnpgManager) {

        Long subStartTime = System.currentTimeMillis();

        boolean showSubProcTime = WorkBackendParamHelper.getInstance().isShowRequirePortalToDoDetail();

        if (showSubProcTime) {
            log.info("開始待辦事項查詢：【{}-{}】", userSid, WkUserUtils.findNameBySid(userSid));
        }

        // ====================================
        // 收呈報 (因特殊規則, 一定要在第一個處理)
        // ====================================
        subStartTime = System.currentTimeMillis();
        String procName = "";
        try {
            procName = "呈報資訊";
            waitingTaskCountResults.add(
                    new WaitingTaskCountTo(procName + "：",
                            this.reqWorkCountService.countUnreadInboxForForwardMember(userSid, InboxType.INCOME_REPORT),
                            "/tech-request/home/home04.xhtml?mode=portal",
                            todoClz));
        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }
        if (showSubProcTime) {
            log.info(WkCommonUtils.prepareCostMessage(subStartTime, procName));
        }

        // v7.0 特殊規則
        if (this.workBackendParamHelper.isPortalWaitlistReduce(userSid)) {
            log.debug("使用者[" + userSid + "]僅顯示呈報");
            return;
        }

        // ====================================
        // 部門未領單
        // ====================================
        subStartTime = System.currentTimeMillis();
        try {
            procName = "部門未領單";
            waitingTaskCountResults.add(
                    new WaitingTaskCountTo(procName + "：",
                            this.reqWorkCountService.countWaitReceiveByDep(userSid, ReqConfirmDepProgStatus.WAIT_CONFIRM),
                            "/tech-request/newsearch/newsearch02.xhtml",
                            ""));
        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }

        if (showSubProcTime) {
            log.info(WkCommonUtils.prepareCostMessage(subStartTime, procName));
        }

        // ====================================
        // 個人未領單
        // ====================================
        subStartTime = System.currentTimeMillis();
        try {
            String url = "/tech-request/newsearch/newsearch02.xhtml"
                    + "?"
                    + URLServiceAttr.URL_ATTR_SEARCH_OWNERSID.getAttr()
                    + "="
                    + userSid;
            procName = "個人未領單";
            waitingTaskCountResults.add(
                    new WaitingTaskCountTo(procName + "：",
                            this.reqConfirmDepSearchHelper.countRequireByRequireConfirmDeAndLoginUserRelation(
                                    ReqConfirmDepProgStatus.WAIT_CONFIRM,
                                    userSid,
                                    comp == null ? "" : comp.getId()),
                            url,
                            todoClz));
        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }

        if (showSubProcTime) {
            log.info(WkCommonUtils.prepareCostMessage(subStartTime, procName));
        }

        // ====================================
        // 部門未完成需求
        // ====================================
        subStartTime = System.currentTimeMillis();
        try {

            // 組預設查詢條件
            List<String> params = Lists.newArrayList();
            params.add(URLServiceAttr.URL_ATTR_SEARCH07_CONDITION_TYPE.getAttr() + "=" + Search07ConditionType.NOT_FINISH_BY_DEP.getVal());

            procName = "部門未完成需求";
            waitingTaskCountResults.add(
                    new WaitingTaskCountTo(procName + "：",
                            this.reqWorkCountService.countWaitReceiveByDep(userSid, ReqConfirmDepProgStatus.IN_PROCESS),
                            "/tech-request/search/search07.xhtml?" + String.join("&", params),
                            ""));
        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }

        if (showSubProcTime) {
            log.info(WkCommonUtils.prepareCostMessage(subStartTime, procName));
        }

        // ====================================
        // 個人未完成需求
        // ====================================
        subStartTime = System.currentTimeMillis();
        try {

            // 組預設查詢條件
            List<String> params = Lists.newArrayList();
            params.add(URLServiceAttr.URL_ATTR_SEARCH07_CONDITION_TYPE.getAttr() + "=" + Search07ConditionType.NOT_FINISH_BY_SELF.getVal());

            procName = "個人未完成需求";
            waitingTaskCountResults.add(
                    new WaitingTaskCountTo(procName + "：",
                            // this.reqWorkCountService.countWaitReceiveByPerson(userSid, ReqConfirmDepProgStatus.IN_PROCESS),
                            this.reqConfirmDepSearchHelper.countRequireByRequireConfirmDeAndLoginUserRelation(
                                    ReqConfirmDepProgStatus.IN_PROCESS,
                                    userSid,
                                    comp == null ? "" : comp.getId()),
                            "/tech-request/search/search07.xhtml?" + String.join("&", params),
                            ""));
        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }

        if (showSubProcTime) {
            log.info(WkCommonUtils.prepareCostMessage(subStartTime, procName));
        }

        // ====================================
        // 檢查確認
        // ====================================
        subStartTime = System.currentTimeMillis();
        try {
            procName = "檢查確認";
            if (isCheckManager) {
                waitingTaskCountResults.add(
                        new WaitingTaskCountTo(procName + "：",
                                this.requireService.countWaitCheck(userSid),
                                "/tech-request/search/search03.xhtml",
                                todoClz));
            }
        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }

        if (showSubProcTime) {
            log.info(WkCommonUtils.prepareCostMessage(subStartTime, procName));
        }

        // ====================================
        // 需求通知
        // ====================================
        subStartTime = System.currentTimeMillis();
        try {
            // 組預設查詢條件
            List<String> params = Lists.newArrayList();
            params.add(URLServiceAttr.URL_ATTR_NEWSEARCH03_CONDITION_TYPE.getAttr() + "=A");

            procName = "需求通知";
            waitingTaskCountResults.add(
                    new WaitingTaskCountTo(procName + "：",
                            this.reqWorkCountService.countAssignNotice(userSid, AssignSendType.SEND),
                            "/tech-request/newsearch/newsearch03.xhtml?" + String.join("&", params),
                            ""));

        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }

        if (showSubProcTime) {
            log.info(WkCommonUtils.prepareCostMessage(subStartTime, procName));
        }

        // ====================================
        // 退件通知
        // ====================================
        subStartTime = System.currentTimeMillis();
        try {
            procName = "退件通知";
            waitingTaskCountResults.add(
                    new WaitingTaskCountTo(procName + "：",
                            this.reqWorkCountService.countReject(dep.getSid(), userSid),
                            "/tech-request/search/search20.xhtml",
                            todoClz));
        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }

        if (showSubProcTime) {
            log.info(WkCommonUtils.prepareCostMessage(subStartTime, procName));
        }

        // ====================================
        // ON程式
        // ====================================
        subStartTime = System.currentTimeMillis();
        try {
            procName = "ON程式";
            waitingTaskCountResults.add(
                    new WaitingTaskCountTo(procName + "：",
                            this.reqWorkCountService.countHasOn(userSid, isOnpgManager),
                            "/tech-request/search/search16.xhtml?" + URLServiceAttr.URL_ATTR_SEARCH16_FROM_PORTAL.getAttr() + "=A",
                            ""));
        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }

        if (showSubProcTime) {
            log.info(WkCommonUtils.prepareCostMessage(subStartTime, procName));
        }

        // ====================================
        // ON程式-待檢查完成
        // ====================================
        subStartTime = System.currentTimeMillis();
        try {
            procName = "ON程式-待檢查完成";
            waitingTaskCountResults.add(
                    new WaitingTaskCountTo(procName + "：",
                            this.reqWorkCountService.conuntOnpgCheck(userSid),
                            "/tech-request/search/search28.xhtml",
                            ""));
        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }

        if (showSubProcTime) {
            log.info(WkCommonUtils.prepareCostMessage(subStartTime, procName));
        }

        // ====================================
        // 原型確認-待功能確認
        // ====================================
        subStartTime = System.currentTimeMillis();
        try {
            procName = "原型確認-待功能確認";
            waitingTaskCountResults.add(
                    new WaitingTaskCountTo(procName + "：",
                            this.reqWorkCountService.workPtCheck(userSid, dep.getSid()),
                            "/tech-request/search/search30.xhtml",
                            ""));
        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }

        if (showSubProcTime) {
            log.info(WkCommonUtils.prepareCostMessage(subStartTime, procName));
        }

        // ====================================
        // 其他資料設定-待完成
        // ====================================
        subStartTime = System.currentTimeMillis();
        try {
            procName = "其他資料設定-待完成";
            waitingTaskCountResults.add(
                    new WaitingTaskCountTo(procName + "：",
                            this.reqWorkCountService.trOsCheck(dep.getSid()),
                            "/tech-request/search/search31.xhtml",
                            ""));
        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }

        if (showSubProcTime) {
            log.info(WkCommonUtils.prepareCostMessage(subStartTime, procName));
        }

        // ====================================
        // 待結案
        // ====================================
        subStartTime = System.currentTimeMillis();
        try {
            procName = "待結案";
            waitingTaskCountResults.add(
                    new WaitingTaskCountTo(procName + "：",
                            this.reqWorkCountService.conuntWaitClose(userSid),
                            "/tech-request/search/search29.xhtml",
                            ""));
        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }

        if (showSubProcTime) {
            log.info(WkCommonUtils.prepareCostMessage(subStartTime, procName));
        }

        // ====================================
        // 逾期未結案
        // ====================================
        subStartTime = System.currentTimeMillis();
        try {
            procName = "逾期未結案";
            waitingTaskCountResults.add(
                    new WaitingTaskCountTo(procName + "：",
                            this.reqWorkCountService.countOverdueUnclosed(userSid),
                            "/tech-request/search/overdueUnclosed.xhtml?mode=portal",
                            ""));
        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }

        if (showSubProcTime) {
            log.info(WkCommonUtils.prepareCostMessage(subStartTime, procName));
        }

        // ====================================
        // 逾期未完工
        // ====================================
        subStartTime = System.currentTimeMillis();

        try {
            procName = "逾期未完工";
            waitingTaskCountResults.add(
                    new WaitingTaskCountTo(procName + "：",
                            this.reqWorkCountService.countOverdueUnfinished(userSid),
                            "/tech-request/search/overdueUnfinished.xhtml?mode=portal",
                            ""));
        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }

        if (showSubProcTime) {
            log.info(WkCommonUtils.prepareCostMessage(subStartTime, procName));
        }

        // ====================================
        // 送測-QA待審核單據
        // ====================================
        subStartTime = System.currentTimeMillis();
        // "QA審單人員"
        if (WkUserUtils.isUserHasRole(userSid, dep.getCompany().getId(), ReqPermission.ROLE_QA_REVIEW_MGR)) {
            try {
                procName = "送測－QA待審核單據";
                waitingTaskCountResults.add(
                        new WaitingTaskCountTo(procName + "：",
                                this.reqWorkCountService.countStQaWaitApprove(),
                                "/tech-request/search/search33.xhtml?mode=portal",
                                ""));
            } catch (Exception e) {
                log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
            }
        }

        if (showSubProcTime) {
            log.info(WkCommonUtils.prepareCostMessage(subStartTime, procName));
        }

        // ====================================
        // 送測－送測文件待提交
        // ====================================
        subStartTime = System.currentTimeMillis();
        try {
            procName = "送測－送測文件待提交";
            waitingTaskCountResults.add(
                    new WaitingTaskCountTo(procName + "：",
                            this.reqWorkCountService.countStUncommitByUser(userSid),
                            "/tech-request/search/search32.xhtml?mode=portal&bySelf=true",
                            ""));
        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }

        if (showSubProcTime) {
            log.info(WkCommonUtils.prepareCostMessage(subStartTime, procName));
        }

        // ====================================
        // 送測-退測
        // ====================================
        subStartTime = System.currentTimeMillis();
        try {
            procName = "送測－退測";
            waitingTaskCountResults.add(
                    new WaitingTaskCountTo(procName + "：",
                            this.reqWorkCountService.countStRollbackByUser(userSid),
                            "/tech-request/search/search13.xhtml?mode=portal",
                            ""));
        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }

        if (showSubProcTime) {
            log.info(WkCommonUtils.prepareCostMessage(subStartTime, procName));
        }

        // ====================================
        // 送測－今日送測文件待補齊數
        // ====================================
        subStartTime = System.currentTimeMillis();
        try {
            procName = "送測－今日送測文件待補齊數";
            waitingTaskCountResults.add(
                    new WaitingTaskCountTo(procName + "：",
                            this.reqWorkCountService.countStTodayUncommit(userSid),
                            "/tech-request/search/search32.xhtml?mode=portal",
                            ""));
        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }

        if (showSubProcTime) {
            log.info(WkCommonUtils.prepareCostMessage(subStartTime, procName));
        }

        // ====================================
        // 送測單異動
        // ====================================
        subStartTime = System.currentTimeMillis();
        try {
            procName = "送測單異動";
            waitingTaskCountResults.add(
                    new WaitingTaskCountTo(procName + "：",
                            this.reqWorkCountService.countNotification(userSid),
                            "/tech-request/home/portal.xhtml",
                            ""));
        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }

        if (showSubProcTime) {
            log.info(WkCommonUtils.prepareCostMessage(subStartTime, procName));
        }
    }

}
