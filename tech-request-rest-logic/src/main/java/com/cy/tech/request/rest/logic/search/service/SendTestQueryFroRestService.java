/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.rest.logic.search.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.commons.vo.User;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.rest.logic.search.vo.SendTestView;
import com.cy.work.common.cache.WkUserCache;

/**
 * @author shaun
 */
@Service
public class SendTestQueryFroRestService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1971808367785175645L;

    @Autowired
    private OrganizationService orgService;

    @Autowired
    private SearchService searchHelper;

    @PersistenceContext
    private EntityManager em;

    @SuppressWarnings("unchecked")
    public List<SendTestView> findWithQuery(String sql, Map<String, Object> parameters, User executor) {
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        rawParameters.stream().forEach(entry -> query.setParameter(entry.getKey(), entry.getValue()));
        return (List<SendTestView>) query.getResultList().stream().map(obj -> this.createView((Object[]) obj)).collect(Collectors.toList());
    }

    private SendTestView createView(Object[] record) {
        int idx = 0;
        String sid = (String) record[idx++];
        String requireNo = (String) record[idx++];
        String requireTheme = (String) record[idx++];
        String testinfoSid = (String) record[idx++];
        String testinfoNo = (String) record[idx++];
        String testinfoTheme = (String) record[idx++];
        Integer createCompany = (Integer) record[idx++];
        Integer createdUser = (Integer) record[idx++];
        Date createdDate = (Date) record[idx++];
        String bpmInstanceId = (String) record[idx++];
        String paperCode = (String) record[idx++];

        SendTestView v = new SendTestView();
        v.setSid(sid);
        v.setRequireNo(requireNo);
        v.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
        v.setTestinfoSid(testinfoSid);
        v.setTestinfoNo(testinfoNo);
        v.setTestinfoTheme(testinfoTheme);
        v.setCreateCompany(orgService.findBySid(createCompany));
        v.setCreatedUser(WkUserCache.getInstance().findBySid(createdUser));
        v.setCreatedDate(createdDate);
        v.setBpmInstanceId(bpmInstanceId);
        v.setPaperCode(paperCode);
        return v;
    }
}
