/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.rest.logic.enums;

import lombok.Getter;

/**
 * 簽核執行動作
 *
 * @author shaun
 */
public enum ActionType {
    /** 簽核 */
    SIGN("簽核"),
    /** 退回 */
    ROLLBACK("退回"),
    /** 復原 */
    RECOVERY("復原"),
    /** 作廢 */
    INVAILD("作廢"),
    /** 需求暫緩 */
    SUSPENDED("需求暫緩"),;

    /**
     * 說明
     */
    @Getter
    private final String desc;

    ActionType(String desc) {
        this.desc = desc;
    }
}
