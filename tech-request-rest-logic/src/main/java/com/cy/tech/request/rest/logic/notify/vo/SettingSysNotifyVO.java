/**
 * 
 */
package com.cy.tech.request.rest.logic.notify.vo;

import java.io.Serializable;

import com.cy.work.notify.vo.enums.NotifyType;

import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 *
 */
@Getter
@Setter
public class SettingSysNotifyVO implements Serializable{
	
	/**
     * 
     */
    private static final long serialVersionUID = 4249318802482969351L;
    /**
	 * 通知類別
	 */
	private NotifyType notifyType;

}
