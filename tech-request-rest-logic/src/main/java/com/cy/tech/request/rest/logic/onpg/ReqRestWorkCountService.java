/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.rest.logic.onpg;

import com.cy.tech.request.logic.anew.service.onpg.ReqOnpgService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.rest.logic.to.WorkOnpgTo;
import com.cy.tech.request.vo.anew.onpg.ReqWorkOnpg;
import com.cy.work.mapp.create.trans.logic.MappCreateTransManager;
import com.cy.work.mapp.create.trans.logic.vo.MappTransView;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * onpg 服務
 *
 * @author kasim
 */
@Component
public class ReqRestWorkCountService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -341610879773824461L;
    @Autowired
    transient private ReqOnpgService onpgService;
    @Autowired
    transient private URLService urlService;
    @Autowired
    private MappCreateTransManager mappCreateTransManager;

    /**
     *
     * @param userSid
     * @param sids
     * @return
     */
    public List<WorkOnpgTo> findToBySidInAndUserSid(Integer userSid, List<String> sids) {
        return onpgService.findBySourceSidIn(sids).stream()
                .map(each -> {
                    return new WorkOnpgTo(
                            each.getSourceSid() + each.getOnpgNo(),
                            each.getOnpgNo(),
                            each.getCreatedDate(),
                            each.getDepSid(),
                            each.getCreateUser(),
                            each.getTheme(),
                            urlService.createUrlLinkParamForTab(
                                    URLService.URLServiceAttr.URL_ATTR_M,
                                    urlService.createSimpleUrlTo(userSid, each.getSourceNo(), 1),
                                    URLService.URLServiceAttr.URL_ATTR_TAB_OP, each.getSid())
                    );
                })
                .collect(Collectors.toList());
    }

    /**
     *
     * @param userSid
     * @param sids
     * @return
     */
    public List<WorkOnpgTo> findByTransReq(Integer userSid, String issueSid) {
        List<MappTransView> result = mappCreateTransManager.findMappOnpgBySourceSidAndTypeIn(issueSid, mappCreateTransManager.getTypesByTransReq());
        if (result == null || result.isEmpty()) {
            return Lists.newArrayList();
        }
        Map<String, ReqWorkOnpg> onpgMap = onpgService.findByOnpgNoIn(
                result.stream().map(each -> each.getOnpgNo())
                .collect(Collectors.toList())).stream()
                .collect(Collectors.toMap(k -> k.getOnpgNo(), v -> v));
        return result.stream()
                .map(each -> {
                    ReqWorkOnpg obj = onpgMap.get(each.getOnpgNo());
                    return new WorkOnpgTo(
                            obj.getSourceSid() + obj.getOnpgNo(),
                            each.getOnpgNo(),
                            each.getCreateDate(),
                            each.getCreateDep(),
                            each.getCreateUser(),
                            each.getOnpgTheme(),
                            urlService.createUrlLinkParamForTab(
                                    URLService.URLServiceAttr.URL_ATTR_M,
                                    urlService.createSimpleUrlTo(userSid, obj.getSourceNo(), 1),
                                    URLService.URLServiceAttr.URL_ATTR_TAB_OP, obj.getSid())
                    );
                })
                .collect(Collectors.toList());
    }

}
