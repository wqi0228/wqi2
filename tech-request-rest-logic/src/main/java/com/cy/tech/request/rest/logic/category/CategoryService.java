package com.cy.tech.request.rest.logic.category;

import com.cy.commons.enums.Activation;
import com.cy.tech.request.repository.category.BasicDataSmallCategoryRepository;
import com.cy.tech.request.rest.logic.to.CategoryTo;
import com.cy.tech.request.vo.category.BasicDataSmallCategory;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 類別 服務
 *
 * @author kasim
 */
@Component
public class CategoryService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6043321526363795346L;

    @Autowired
    private BasicDataSmallCategoryRepository smallCategoryRepository;

    @PersistenceContext
    private EntityManager em;

    /**
     * 抓取小類為有效且須顯示在案件單的選項為true
     *
     * @return
     */
    public List<CategoryTo> findSmallCategoryByShowItemInTc() {
        return this.findActiveByShowItemInTc(true).stream()
                .map(each -> new CategoryTo(each.getSid(), each.getName()))
                .collect(Collectors.toList());
    }

    /**
     * 查詢有效資料
     *
     * @param showItemInTc
     * @return
     */
    private List<BasicDataSmallCategory> findActiveByShowItemInTc(boolean showItemInTc) {
        return smallCategoryRepository.findByStatusAndShowItemInTc(
                Activation.ACTIVE, showItemInTc);
    }

    public CategoryTo findCategoryToByCategoryName(String categoryName) {
        String sql = "SELECT basic_data_small_category_sid, small_category_name FROM tr_basic_data_small_category WHERE small_category_name = :categoryName and status = 0";
        Query query = em.createNativeQuery(sql);
        query.setParameter("categoryName", categoryName);
        
        @SuppressWarnings("unchecked")
        List<Object[]> resultList = query.getResultList();

        if (resultList != null && !resultList.isEmpty()) {
            Object[] arr = resultList.get(0);
            return new CategoryTo((String) arr[0], (String) arr[1]);
        }
        return null;
    }

}
