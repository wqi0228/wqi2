package com.cy.tech.request.rest.logic.to;

import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.enums.WorkSourceType;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * ON程式通知訊息(Rest傳輸用)
 *
 * @author kasim
 */
@Data
@ToString
@EqualsAndHashCode(of = {"sid"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkOnpgAlertTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1744526696870360674L;

    @JsonProperty("sid")
    private String sid;

    /** on程式單sid */
    @JsonProperty("onpgSid")
    private String onpgSid;

    /** on程式單號 */
    @JsonProperty("onpgNo")
    private String onpgNo;

    /** on程式來源 */
    @JsonProperty("sourceType")
    private WorkSourceType sourceType;

    /** 需求單Sid */
    @JsonProperty("sourceSid")
    private String sourceSid;

    /** 需求單單號 */
    @JsonProperty("sourceNo")
    private String sourceNo;

    /** on程式主題 */
    @JsonProperty("onpgTheme")
    private String onpgTheme;

    /** 寄件者Sid */
    @JsonProperty("senderSid")
    private Integer senderSid;

    /** 寄件者Name */
    @JsonProperty("senderName")
    private String senderName;

    /** 寄件日期 */
    @JsonProperty("sendDate")
    private Date sendDate;

    /** 收件者Sid */
    @JsonProperty("receiverSid")
    private Integer receiverSid;

    /** 收件者Name */
    @JsonProperty("receiverName")
    private String receiverName;

    /** 來自於哪一則ON程式檢記錄的sid */
    @JsonProperty("checkRecordSid")
    private String checkRecordSid;

    /** 來自於哪一則ON程式檢記錄回覆的sid */
    @JsonProperty("checkRecordReplySid")
    private String checkRecordReplySid;

    /** 讀取狀態 */
    @JsonProperty("readStatus")
    private ReadRecordType readStatus;

    /** 點擊時間 */
    @JsonProperty("clickTime")
    private Date clickTime;

}
