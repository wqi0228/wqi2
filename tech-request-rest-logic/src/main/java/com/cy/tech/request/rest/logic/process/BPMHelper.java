/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.rest.logic.process;

import com.cy.bpm.rest.client.BpmOrganizationClient;
import com.cy.bpm.rest.to.HistoryTaskTo;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author brain0925_liao
 */
@Component
public class BPMHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8426506554804491128L;
    @Autowired
    private BpmOrganizationClient organizationClient;

     public List<String> getDefaultApprover(ProcessTaskHistory processTaskHistory) throws ProcessRestException {
        return !Strings.isNullOrEmpty(processTaskHistory.getUserID())
                ? Lists.newArrayList(processTaskHistory.getUserID())
                : organizationClient.findUserFromRole(processTaskHistory.getRoleID());
    }

    public List<String> getDefaultApprover(HistoryTaskTo historyTaskTo) throws ProcessRestException {
        return !Strings.isNullOrEmpty(historyTaskTo.getUserID()) && !historyTaskTo.getUserID().equals("nvl")
                ? Lists.newArrayList(historyTaskTo.getUserID())
                : organizationClient.findUserFromRole(historyTaskTo.getRoleID());
    }
    
}
