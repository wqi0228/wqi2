package com.cy.tech.request.rest.logic.helper;

import com.cy.bpm.rest.client.BpmOrganizationClient;
import com.cy.bpm.rest.to.HistoryTaskTo;
import com.cy.bpm.rest.vo.ProcessTask;
import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.commons.util.FusionUrlServiceUtils;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.formsigning.enums.FormSigningType;
import com.cy.formsigning.enums.FormType;
import com.cy.formsigning.enums.LanguageType;
import com.cy.formsigning.vo.FormInfo;
import com.cy.tech.request.logic.enumerate.PropKeyType;
import com.cy.tech.request.logic.service.BpmService;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.logic.service.URLService;
import com.cy.tech.request.rest.logic.search.vo.RequireUnitView;
import com.cy.tech.request.vo.require.Require;
import com.cy.tech.request.vo.require.RequireUnitSignInfo;
import com.cy.work.common.utils.WkOrgUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ReqUnitFormInfoHelper extends AbstractFormInfoHelper {

    /**
     * 
     */
    private static final long serialVersionUID = 5906712083271269414L;
    @Autowired
    private SearchService searchService;
    @Autowired
    private OrganizationService orgService;
    @Autowired
    private URLService urlService;
    @Autowired
    private FormHelper formHelper;
    @Autowired
    private BpmOrganizationClient organizationClient;
    @Autowired
    private BpmService bpmService;
    
    public FormInfo transform(RequireUnitView view, ProcessTaskBase base, User executor, LanguageType lType) {
        FormInfo info = new FormInfo();
        info.setFormId(view.getRequireNo());
        info.setBpmFlowId(view.getBpmInstanceId());
        info.setCompany(WkOrgUtils.getOrgName(view.getCreateCompany()));
        info.setCompanyUuid(view.getCreateCompany().getUuid());
        info.setFormType(FormType.REQUIRE_UNIT);
        info.setFormDate(view.getCreatedDate());
        if (base instanceof ProcessTaskHistory) {
            ProcessTaskHistory history = (ProcessTaskHistory) base;
            super.assignFormInfoByTaskHistory(info, history, executor.getId());
            info.setSubmittedDate(history.getStartTime() != null ? history.getStartTime() : history.getDeliverTime());
            info.setSignedDate(history.getTaskEndTime());
        }
        if (base instanceof ProcessTask) {
            ProcessTask task = (ProcessTask) base;
            info.setSubmittedDate(task.getStartTime() != null ? task.getStartTime() : task.getDeliverTime());
            try {
                info.setBeingAgentId(this.getApprover(base, executor)); // set beingAgentId field here
            } catch (ProcessRestException e) {
                log.error("Call getApprover method is fail(taskId: " + task.getInstanceID() + "), error cause: " + e.getMessage(), e);
            }
        }
        super.restApplierInfo(info, view.getCreatedUser().getId());
        info.setDescription(view.getRequireTheme());
        info.setUrl(getContextPath() + "/require/require02.xhtml" + urlService.createSimpleURLLink(URLService.URLServiceAttr.URL_ATTR_M, view.getRequireNo(), 1));
        info.setFormStatus(formHelper.createFormStatus(view.getPaperCode(), lType));
        info.setHasEditMode(Boolean.TRUE);
        return info;
    }

    public List<String> getApprover(ProcessTaskBase taskBase) throws ProcessRestException {
        String userId = taskBase.getUserID();
        String roleId = taskBase.getRoleID();
        if (taskBase instanceof ProcessTaskHistory) {
            userId = ((ProcessTaskHistory) taskBase).getExecutorID();
            roleId = ((ProcessTaskHistory) taskBase).getExecutorRoleID();
        }
        return !Strings.isNullOrEmpty(userId) ? Lists.newArrayList(userId) : organizationClient.findUserFromRole(roleId);
    }

    public FormInfo transform(Require req, User executor, LanguageType lType) {
        return this.transform(req.getReqUnitSign(), executor, lType);
    }

    public FormInfo transform(RequireUnitSignInfo rmsi, User executor, LanguageType lType) {
        Org company = orgService.findBySid(rmsi.getRequire().getCreateCompany().getSid());
        FormInfo info = new FormInfo();
        info.setFormId(rmsi.getRequireNo());
        info.setBpmFlowId(rmsi.getBpmInstanceId());
        info.setCompany(WkOrgUtils.getOrgName(company));
        info.setCompanyUuid(company.getUuid());
        info.setFormType(FormType.REQUIRE_UNIT);
        info.setFormDate(rmsi.getRequire().getCreatedDate());
        List<ProcessTaskBase> tasks = bpmService.findFlowChartByInstanceId(rmsi.getBpmInstanceId(), executor.getId());
        for (int i = tasks.size() - 1; i >= 0; i--) {
            ProcessTaskBase each = tasks.get(i);
            if (each instanceof ProcessTaskHistory) {
                ProcessTaskHistory history = (ProcessTaskHistory) each;
                try {
                    info.setDefApproves(bpmHelper.getDefaultApprover(history));
                } catch (ProcessRestException e) {
                    log.error("info.setDefApproves fail..", e);
                }
                if (history.getExecutorID().equals(executor.getId()) || info.getDefApproves().contains(executor.getId())) {
                    super.assignFormInfoByTaskHistory(info, history, executor.getId());
                    info.setSubmittedDate(history.getStartTime() != null ? history.getStartTime() : history.getDeliverTime());
                    info.setSignedDate(history.getTaskEndTime());
                }
                break;
            }
        }
        if (info.getSubmittedDate() == null) {
            if (rmsi.getCanSignedIdsTo().getValue().contains(executor.getId())) {
                ProcessTaskBase base = tasks.get(tasks.size() - 1);
                if (base instanceof ProcessTask) {
                    ProcessTask task = (ProcessTask) base;
                    info.setSubmittedDate(task.getStartTime() != null ? task.getStartTime() : task.getDeliverTime());
                    try {
                        info.setBeingAgentId(this.getApprover(base, executor)); // set beingAgentId field here
                    } catch (ProcessRestException e) {
                        log.error("Call getApprover method is fail(taskId: " + task.getInstanceID() + "), error cause: " + e.getMessage(), e);
                    }
                }
            }
        }
        super.restApplierInfo(info, rmsi.getRequire().getCreatedUser().getSid());
        info.setDescription(searchService.getThemeStr(rmsi.getRequire()));
        info.setUrl(getContextPath() + "/require/require02.xhtml" + urlService.createSimpleURLLink(URLService.URLServiceAttr.URL_ATTR_M, rmsi.getRequireNo(), 1));
        info.setFormStatus(formHelper.createFormStatus(rmsi.getInstanceStatus(), lType));
        info.setHasEditMode(Boolean.TRUE);
        return info;
    }

    public FormSigningType findFormTypeForUser(RequireUnitSignInfo rmsi, User executor) {
        if (rmsi.getCanSignedIdsTo().getValue().contains(executor.getId())) {
            return FormSigningType.CURRENT_SIGNER;
        }
        List<ProcessTaskBase> tasks = bpmService.findFlowChartByInstanceId(rmsi.getBpmInstanceId(), executor.getId());
        if (tasks.stream()
                  .filter(base -> base instanceof ProcessTaskHistory)
                  .map(base -> (ProcessTaskHistory) base)
                  .anyMatch(history -> history.getExecutorID().equals(executor.getId()))) {
            return FormSigningType.NOT_CURRENT_SIGNER_PROCESSED;
        } else {
            return FormSigningType.NOT_SIGNER;
        }
    }

    public FormInfo transHistoryForm(RequireUnitView view, HistoryTaskTo historyTo, User executor, LanguageType lType) {
        FormInfo info = new FormInfo();
        info.setFormId(view.getRequireNo());
        info.setBpmFlowId(view.getBpmInstanceId());
        info.setCompany(WkOrgUtils.getOrgName(view.getCreateCompany()));
        info.setCompanyUuid(view.getCreateCompany().getUuid());
        info.setFormType(FormType.REQUIRE_UNIT);
        info.setFormDate(view.getCreatedDate());
        info.setSubmittedDate(historyTo.getStartTime() != null ? historyTo.getStartTime() : historyTo.getDeliverTime());
        info.setSignedDate(historyTo.getTaskEndTime());
        super.restApplierInfo(info, view.getCreatedUser().getId());
        info.setDescription(view.getRequireTheme());
        info.setUrl(getContextPath() + "/require/require02.xhtml" + urlService.createSimpleURLLink(URLService.URLServiceAttr.URL_ATTR_M, view.getRequireNo(), 1));
        info.setFormStatus(formHelper.createFormStatus(view.getPaperCode(), lType));
        info.setHasEditMode(Boolean.TRUE);
        super.assignFormInfoByHistoryTo(info, historyTo, executor.getId());
        return info;
    }

    private String getContextPath() {
        return FusionUrlServiceUtils.getApPathByPropKey(PropKeyType.TECH_REQUEST_AP_URL.getValue());
    }

    public String getApprover(ProcessTaskBase taskBase, User executor) throws ProcessRestException {
        String userId = taskBase.getUserID();
        String roleId = taskBase.getRoleID();
        if (taskBase instanceof ProcessTaskHistory) {
            userId = ((ProcessTaskHistory) taskBase).getExecutorID();
            roleId = ((ProcessTaskHistory) taskBase).getExecutorRoleID();
        }
        log.debug("executor:{}", executor.getId());
        log.debug("userId:{}, roleId:{}", userId, roleId);
        List<String> list = !Strings.isNullOrEmpty(userId) ? Lists.newArrayList(userId) : organizationClient.findUserFromRole(roleId);
        log.debug("list:{}", list);
        return list.contains(executor.getId()) ? executor.getId() : list.get(0);
    }
}
