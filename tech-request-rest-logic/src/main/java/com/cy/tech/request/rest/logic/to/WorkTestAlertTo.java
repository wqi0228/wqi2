package com.cy.tech.request.rest.logic.to;

import com.cy.work.common.enums.ReadRecordType;
import com.cy.work.common.enums.WorkSourceType;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * 送測通知訊息(Rest傳輸用)
 *
 * @author kasim
 */
@Data
@ToString
@EqualsAndHashCode(of = {"sid"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkTestAlertTo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8247112615271100168L;

    /** 送測通知Sid */
    @JsonProperty("sid")
    private String sid;

    /** 送測單主檔 */
    @JsonProperty("testInfoSid")
    private String testInfoSid;

    /** 送測單號 */
    @JsonProperty("testinfoNo")
    private String testinfoNo;

    /** 送測來源 */
    @JsonProperty("sourceType")
    private WorkSourceType sourceType;

    /** 需求單Sid */
    @JsonProperty("sourceSid")
    private String sourceSid;

    /** 需求單單號 */
    @JsonProperty("sourceNo")
    private String sourceNo;

    /** 送測主題 */
    @JsonProperty("testinfoTheme")
    private String testinfoTheme;

    /** 寄件者Sid */
    @JsonProperty("senderSid")
    private Integer senderSid;

    /** 寄件者Name */
    @JsonProperty("senderName")
    private String senderName;

    /** 寄件日期 */
    @JsonProperty("sendDate")
    private Date sendDate;

    /** 收件者Sid */
    @JsonProperty("receiverSid")
    private Integer receiverSid;

    /** 收件者Name */
    @JsonProperty("receiverName")
    private String receiverName;

    /** 來自於哪一則送測回覆sid */
    @JsonProperty("replySid")
    private String replySid;

    /** 來自於哪一則送測回覆的回覆sid */
    @JsonProperty("alreadyReplySid")
    private String alreadyReplySid;

    /** 讀取狀態 */
    @JsonProperty("readStatus")
    private ReadRecordType readStatus;

    /** 點擊時間 */
    @JsonProperty("clickTime")
    private Date clickTime;

}
