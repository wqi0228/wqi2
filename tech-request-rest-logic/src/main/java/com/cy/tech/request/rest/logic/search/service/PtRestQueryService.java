/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.rest.logic.search.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.service.SearchService;
import com.cy.tech.request.rest.logic.search.vo.PtView;
import com.cy.work.common.cache.WkUserCache;
import com.google.common.collect.Lists;

/**
 * @author shaun
 */
@Service
public class PtRestQueryService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -136374070525868285L;

    @Autowired
    private OrganizationService orgService;

    @Autowired
    private SearchService searchHelper;

    @PersistenceContext
    private EntityManager em;

    @SuppressWarnings("unchecked")
    public List<PtView> findWithQuery(
            String sql,
            Map<String, Object> parameters,
            Integer execUserSid) {

        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        rawParameters.stream().forEach(entry -> query.setParameter(entry.getKey(), entry.getValue()));

        // ====================================
        // 查詢
        // ====================================
        List<Object[]> dbResults = query.getResultList();
        // ====================================
        // 封裝
        // ====================================
        List<PtView> ptViews = Lists.newArrayList();
        for (Object[] rowDatas : dbResults) {
            ptViews.add(this.createView((Object[]) rowDatas));
        }
        return ptViews;
    }

    private PtView createView(Object[] record) {
        int idx = 0;
        String sid = (String) record[idx++];
        String requireNo = (String) record[idx++];
        String requireTheme = (String) record[idx++];
        String ptCheckSid = (String) record[idx++];
        String ptNo = (String) record[idx++];
        String ptTheme = (String) record[idx++];
        Integer createCompany = (Integer) record[idx++];
        Integer createdUser = (Integer) record[idx++];
        Date createdDate = (Date) record[idx++];
        String bpmInstanceId = (String) record[idx++];
        String paperCode = (String) record[idx++];

        PtView v = new PtView();
        v.setSid(sid);
        v.setRequireNo(requireNo);
        v.setRequireTheme(searchHelper.combineFromJsonStr(requireTheme));
        v.setPtCheckSid(ptCheckSid);
        v.setPtNo(ptNo);
        v.setPtTheme(ptTheme);
        v.setCreateCompany(orgService.findBySid(createCompany));
        v.setCreatedUser(WkUserCache.getInstance().findBySid(createdUser));
        v.setCreatedDate(createdDate);
        v.setBpmInstanceId(bpmInstanceId);
        v.setPaperCode(paperCode);
        return v;
    }

}
