package com.cy.tech.request.rest.logic.helper;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.vo.User;
import com.cy.formsigning.enums.FormType;
import com.cy.formsigning.vo.BatchSignedResult;
import com.cy.formsigning.vo.ButtonInfo;
import com.cy.formsigning.vo.ButtonInfoWithOneSelectMenuAndOneTextarea;
import com.cy.formsigning.vo.ButtonInfoWithOneTextarea;
import com.cy.formsigning.vo.SelectItems;
import com.cy.tech.request.logic.service.RequireService;
import com.cy.tech.request.logic.service.pt.PtBpmService;
import com.cy.tech.request.rest.logic.enums.ActionType;
import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.require.Require;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class PtProcessHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2067380847568700791L;
    @Autowired
    private RequireService reqService;
    @Autowired
    private PtBpmService bpmService;

    public void createSignBtn(List<ButtonInfo> btns, PtCheck wti, User executor) {
        if (bpmService.showSignBtn(wti, executor)) {
            btns.add(new ButtonInfo("簽名", wti.getPtNo() + "," + ActionType.SIGN));
        }
    }

    public void createRollBackBtn(List<ButtonInfo> btns, PtCheck wti, User executor) {
        if (bpmService.showRollBackBtn(wti, executor)) {
            ButtonInfoWithOneSelectMenuAndOneTextarea btn = new ButtonInfoWithOneSelectMenuAndOneTextarea();
            btn.setButtonValue("退回");
            btn.setAction(wti.getPtNo() + "," + ActionType.ROLLBACK);
            btn.setInputTitle("退回原因");//退回視窗下方輸入框旁左側文字
            btn.setSelectItems(this.createRollBackSelectItem(wti));//退回視窗下拉選項
            btns.add(btn);
        }
    }

    private SelectItems createRollBackSelectItem(PtCheck wti) {
        List<ProcessTaskBase> tasks = Lists.newArrayList(wti.getSignInfo().getTaskTo().getTasks());
        SelectItems items = SelectItems.create("退回至");
        tasks.remove(tasks.size() - 1);
        tasks.forEach(each -> {
            if (each instanceof ProcessTaskHistory) {
                ProcessTaskHistory history = (ProcessTaskHistory) each;
                //key = 顯示名稱 , value = 回傳資訊
                items.addItem(history.getTaskName().contains("申請人")
                          ? "申請人-" + history.getExecutorName()
                          : history.getRoleName() + "-" + history.getExecutorName(),
                          history.getSid()
                );
            }
        });
        return items;
    }

    public void createRecoveryBtn(List<ButtonInfo> btns, PtCheck wti, User executor) {
        if (bpmService.showRecoveryBtn(wti, executor)) {
            btns.add(new ButtonInfo("復原", wti.getPtNo() + "," + ActionType.RECOVERY));
        }
    }

    public void createInvaildBtn(List<ButtonInfo> btns, PtCheck wti, User executor) {
        if (bpmService.showInvaildBtn(wti, executor)) {
            btns.add(new ButtonInfoWithOneTextarea("作廢", wti.getPtNo() + "," + ActionType.INVAILD, "作廢原因"));
        }
    }

    /**
     * 執行批次簽核
     *
     * @param wtis
     * @param executor
     * @return
     */
    public BatchSignedResult doBatchSign(List<PtCheck> wtis, User executor) {
        BatchSignedResult toReturn = new BatchSignedResult(wtis.size(), FormType.REQUIRE_SEND_TEST);
        wtis.stream()
                  .filter(each -> bpmService.showSignBtn(each, executor))
                  .forEach(each -> {
                      try {
                          bpmService.doSign(each, executor, "");
                          toReturn.addSuccess(each.getPtNo());
                      } catch (ProcessRestException ex) {
                          String msg = "Unexpected exception: when " + executor.getId() + " sign the form(" + each.getPtNo() + ") -- " + ex.getClass().getName() + ": " + ex.getMessage();
                          log.error(msg, ex);
                          toReturn.addFail(each.getPtNo(), msg);
                      }
                  });
        return toReturn;
    }

    /**
     * 執行流程動作
     *
     * @param actType
     * @param req
     * @param executor
     * @param inputData
     * @return
     * @throws ProcessRestException
     */
    public PtCheck doAction(ActionType actType, PtCheck wti, User executor, Map<String, Object> inputData) throws ProcessRestException {
        switch (actType) {
            case SIGN:
                return this.sign(wti, executor);
            case RECOVERY:
                return this.recovery(wti, executor);
            case ROLLBACK:
                return this.rollback(wti, executor, inputData);
            case INVAILD:
                return this.invaild(wti, executor, inputData);
            default:
                throw new IllegalArgumentException("非有效簽核型態 action type = " + actType + " 單號:" + wti.getPtNo());
        }
    }

    private PtCheck sign(PtCheck wti, User executor) throws ProcessRestException {
        Preconditions.checkArgument(bpmService.showSignBtn(wti, executor), "工作節點可能已變更，無法進行簽核！！");
        bpmService.doSign(wti, executor, "");
        return wti;
    }

    private PtCheck recovery(PtCheck wti, User executor) throws ProcessRestException {
        Preconditions.checkArgument(bpmService.showRecoveryBtn(wti, executor), "工作節點可能已變更，無法進行復原！！");
        bpmService.doRecovery(wti, executor);
        return wti;
    }

    private PtCheck rollback(PtCheck wti, User executor, Map<String, Object> inputData) throws ProcessRestException {
        Preconditions.checkArgument(bpmService.showRollBackBtn(wti, executor), "工作節點可能已變更，無法進行退回！！");
        //退回理由
        String comment = inputData.containsKey(ButtonInfoWithOneSelectMenuAndOneTextarea.INPUT_TEXTAREA)
                  ? Strings.nullToEmpty((String) inputData.get(ButtonInfoWithOneSelectMenuAndOneTextarea.INPUT_TEXTAREA))
                  : "";
        //任務task sid
        String key = inputData.containsKey(ButtonInfoWithOneSelectMenuAndOneTextarea.SELECT_ITEMS)
                  ? Strings.nullToEmpty((String) inputData.get(ButtonInfoWithOneSelectMenuAndOneTextarea.SELECT_ITEMS))
                  : "";
        Preconditions.checkArgument(!Strings.isNullOrEmpty(key), "指定退回節點為空值！！");
        ProcessTaskHistory rollbackTask = (ProcessTaskHistory) wti.getSignInfo().getTaskTo().getTasks().stream().filter(each -> each.getSid().equals(key)).findFirst().get();
        bpmService.doRollBack(wti, executor, rollbackTask, comment);
        return wti;
    }

    private PtCheck invaild(PtCheck wti, User executor, Map<String, Object> inputData) throws ProcessRestException {
        Preconditions.checkArgument(bpmService.showInvaildBtn(wti, executor), "工作節點可能已變更，無法進行作廢！！");
        //作廢原因
        String comment = inputData.containsKey(ButtonInfoWithOneTextarea.INPUT_TEXTAREA)
                  ? Strings.nullToEmpty((String) inputData.get(ButtonInfoWithOneTextarea.INPUT_TEXTAREA))
                  : "";
        Require req = reqService.findByReqNo(wti.getSourceNo());
        bpmService.doInvaild(req, wti, executor, comment);
        return wti;
    }

}
