/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.tech.request.rest.logic.process;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.bpm.rest.client.TaskClient;
import com.cy.bpm.rest.to.HistoryTaskTo;
import com.cy.bpm.rest.vo.ProcessTask;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.formsigning.enums.FormSigningType;
import com.cy.formsigning.enums.LanguageType;
import com.cy.formsigning.enums.QueryType;
import com.cy.formsigning.vo.BatchSignedResult;
import com.cy.formsigning.vo.ButtonInfo;
import com.cy.formsigning.vo.FormInfo;
import com.cy.tech.request.logic.service.OrganizationService;
import com.cy.tech.request.logic.vo.BpmCreateTo;
import com.cy.tech.request.repository.pt.PtCheckRepo;
import com.cy.tech.request.repository.pt.PtSignInfoRepo;
import com.cy.tech.request.rest.logic.enums.ActionType;
import com.cy.tech.request.rest.logic.helper.PtFormInfoHelper;
import com.cy.tech.request.rest.logic.helper.PtProcessHelper;
import com.cy.tech.request.rest.logic.search.service.PtRestQueryService;
import com.cy.tech.request.rest.logic.search.vo.PtView;
import com.cy.tech.request.vo.pt.PtCheck;
import com.cy.tech.request.vo.pt.PtSignInfo;
import com.cy.work.common.cache.WkUserCache;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * 原型確認簽核服務
 *
 * @author shaun
 */
@Slf4j
@Component
public class PtSignService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1996599096950605241L;
    @Autowired
    private PtRestQueryService ptRestQueryService;
    @Autowired
    private PtFormInfoHelper formHelper;
    @Autowired
    private PtProcessHelper processHelper;
    @Autowired
    private TaskClient taskClient;
    @Autowired
    private PtCheckRepo wtiDao;
    @Autowired
    private PtSignInfoRepo wtsiDao;
    @Autowired
    private OrganizationService organizationService;

    public FormInfo findByFormId(String formId, String executorUuid, LanguageType lType) {
        User executor = WkUserCache.getInstance().findByUUID(executorUuid);
        Preconditions.checkArgument(executor != null, "傳送的執行者uuid，無法訪查出有效成員資訊 uuid:" + executorUuid);
        PtSignInfo wtsi = wtsiDao.findByPtNo(formId);
        Preconditions.checkArgument(wtsi != null, "查詢不到需求單位相關流程 單號:" + formId);
        return formHelper.transform(wtsi, executor, lType);
    }

    /**
     * 取得待簽筆數
     *
     * @param userID 登入者ID
     * @return
     */
    public Map<String, Map<QueryType, Map<String, Integer>>> getUnSignCount(String executorUuid) throws ProcessRestException {
        Map<String, Map<QueryType, Map<String, Integer>>> result = Maps.newHashMap();
        User user = WkUserCache.getInstance().findByUUID(executorUuid);
        Preconditions.checkArgument(user != null, "傳送的成員uuid，無法訪查出有效成員資訊 uuid:" + executorUuid);
        List<ProcessTask> process = taskClient.findTaskByUserAndDefinition(user.getId(), BpmCreateTo.RequireFlowType.PROTOTYPE_CHECK.getDefinition());
        Map<String, ProcessTask> processMap = Maps.newHashMap();
        process.stream().filter(p -> !Strings.isNullOrEmpty(p.getDocumentID()))
                .forEach(p -> processMap.put(p.getDocumentID().replace(BpmCreateTo.RequireFlowType.PROTOTYPE_CHECK.getDefinition() + "-", ""), p));
        List<String> ptCheckNoList = process.stream().filter(t -> !Strings.isNullOrEmpty(t.getDocumentID()))
                .map(t -> t.getDocumentID().replace(BpmCreateTo.RequireFlowType.PROTOTYPE_CHECK.getDefinition() + "-", ""))
                .collect(Collectors.toList());
        List<PtView> viewResult = this.searchPtCheckNo(ptCheckNoList, user);
        viewResult.forEach(item -> {
            Map<QueryType, Map<String, Integer>> qDetail = null;
            Org comp = organizationService.findBySid(item.getCreateCompany().getSid());
            if (result.containsKey(comp.getId())) {
                qDetail = result.get(comp.getId());
            } else {
                qDetail = Maps.newHashMap();
            }
            QueryType qtype = null;
            qtype = QueryType.ALL;
            Map<String, Integer> userIdCount = qDetail.get(qtype);

            if (userIdCount == null || userIdCount.isEmpty()) {
                userIdCount = Maps.newHashMap();
            }
            try {
                ProcessTask task = processMap.get(item.getPtNo());
                String userId = formHelper.getApprover(task).get(0);
                if (userIdCount.containsKey(userId)) {
                    int value = userIdCount.get(userId);
                    userIdCount.put(userId, ++value);
                } else {
                    userIdCount.put(userId, 1);
                }
            } catch (ProcessRestException e1) {
                log.error("取得預設簽核人員列表失敗, 錯誤原因: " + e1.getMessage(), e1);
            }
            qDetail.put(qtype, userIdCount);
            result.put(comp.getId(), qDetail);
        });
        log.info("result count : " + result.size());
        return result;
    }

    public List<FormInfo> findUnSignByUser(String executorUuid, LanguageType lType) throws ProcessRestException {
        User user = WkUserCache.getInstance().findByUUID(executorUuid);
        Preconditions.checkArgument(user != null, "傳送的成員uuid，無法訪查出有效成員資訊 uuid:" + executorUuid);
        List<ProcessTask> process = taskClient.findTaskByUserAndDefinition(user.getId(), BpmCreateTo.RequireFlowType.PROTOTYPE_CHECK.getDefinition());
        Map<String, ProcessTask> processMap = Maps.newHashMap();
        process.stream().filter(p -> !Strings.isNullOrEmpty(p.getDocumentID()))
                .forEach(p -> processMap.put(p.getDocumentID().replace(BpmCreateTo.RequireFlowType.PROTOTYPE_CHECK.getDefinition() + "-", ""), p));
        List<String> ptCheckNoList = process.stream().filter(t -> !Strings.isNullOrEmpty(t.getDocumentID()))
                .map(t -> t.getDocumentID().replace(BpmCreateTo.RequireFlowType.PROTOTYPE_CHECK.getDefinition() + "-", ""))
                .collect(Collectors.toList());
        List<PtView> viewResult = this.searchPtCheckNo(ptCheckNoList, user);
        return viewResult.stream()
                .map(view -> formHelper.transform(view, processMap.get(view.getPtNo()), user, lType))
                .collect(Collectors.toList());
    }

    public List<FormInfo> findUnSignByUser(String executorUuid, Integer companySid, LanguageType lType) throws ProcessRestException {
        User user = WkUserCache.getInstance().findByUUID(executorUuid);
        Preconditions.checkArgument(user != null, "傳送的成員uuid，無法訪查出有效成員資訊 uuid:" + executorUuid);
        List<ProcessTask> process = taskClient.findTaskByUserAndDefinition(user.getId(), BpmCreateTo.RequireFlowType.PROTOTYPE_CHECK.getDefinition());
        Map<String, ProcessTask> processMap = Maps.newHashMap();
        process.stream().filter(p -> !Strings.isNullOrEmpty(p.getDocumentID()))
                .forEach(p -> processMap.put(p.getDocumentID().replace(BpmCreateTo.RequireFlowType.PROTOTYPE_CHECK.getDefinition() + "-", ""), p));
        List<String> ptCheckNoList = process.stream().filter(t -> !Strings.isNullOrEmpty(t.getDocumentID()))
                .map(t -> t.getDocumentID().replace(BpmCreateTo.RequireFlowType.PROTOTYPE_CHECK.getDefinition() + "-", ""))
                .collect(Collectors.toList());
        List<PtView> viewResult = this.searchPtCheckNo(ptCheckNoList, user);
        List<PtView> unSignReuslt = Lists.newArrayList();
        viewResult.forEach(item -> {
            if (!item.getCreateCompany().getSid().equals(companySid)) {
                return;
            }
            unSignReuslt.add(item);
        });
        return unSignReuslt.stream()
                .map(view -> formHelper.transform(view, processMap.get(view.getPtNo()), user, lType))
                .collect(Collectors.toList());
    }

    private List<PtView> searchPtCheckNo(List<String> ptCheckNoList, User executor) {
        if (ptCheckNoList.isEmpty()) {
            return Lists.newArrayList();
        }
        // return
        Map<String, Object> param = Maps.newHashMap();
        StringBuilder sb = new StringBuilder();
        sb.append(" SELECT wti.pt_check_source_sid,");
        sb.append("        wti.pt_check_source_no,");
        sb.append("        tid.field_content,");
        sb.append("        wti.pt_check_sid,");
        sb.append("        wti.pt_check_no,");
        sb.append("        wti.pt_check_theme,");
        sb.append("        wti.comp_sid,");
        sb.append("        wti.create_usr,");
        sb.append("        wti.create_dt,");
        sb.append("        wtsi.bpm_instance_id,");
        sb.append("        wtsi.paper_code ");
        sb.append(" FROM ");
        sb.append("  (SELECT * FROM work_pt_check wti WHERE wti.pt_check_no IN (:ptCheckNoList)) AS wti ");
        sb.append("    INNER JOIN (SELECT * FROM work_pt_sign_info wtsi ) AS wtsi ON wti.pt_check_sid=wtsi.pt_check_sid ");
        sb.append("    INNER JOIN (SELECT tid.require_sid,tid.field_content FROM tr_index_dictionary tid WHERE ");
        sb.append("      tid.field_name='主題') AS tid ON wti.pt_check_source_sid=tid.require_sid ");
        sb.append(" WHERE wti.pt_check_sid IS NOT NULL GROUP BY wti.pt_check_sid ORDER BY wti.create_dt ASC");
        param.put("ptCheckNoList", ptCheckNoList);
        // 不用記查詢記錄
        return ptRestQueryService.findWithQuery(sb.toString(), param, executor.getSid());
    }

    public List<FormInfo> findSignedByUser(Date from, Date to, String executorUuid, LanguageType lType) throws ProcessRestException {
        User user = WkUserCache.getInstance().findByUUID(executorUuid);
        Preconditions.checkArgument(user != null, "傳送的成員uuid，無法訪查出有效成員資訊 uuid:" + executorUuid);
        List<HistoryTaskTo> historys = taskClient.findHistoryByUserAndTimeAndDefinition(user.getId(), from, to, BpmCreateTo.RequireFlowType.PROTOTYPE_CHECK.getDefinition());
        Map<String, HistoryTaskTo> processMap = Maps.newHashMap();
        historys.forEach(p -> processMap.put(p.getDocumentID().replace(BpmCreateTo.RequireFlowType.PROTOTYPE_CHECK.getDefinition() + "-", ""), p));
        List<String> ptCheckNoList = historys.stream()
                .map(t -> t.getDocumentID().replace(BpmCreateTo.RequireFlowType.PROTOTYPE_CHECK.getDefinition() + "-", ""))
                .collect(Collectors.toList());
        List<PtView> viewResult = this.searchPtCheckNo(ptCheckNoList, user);
        return viewResult.stream()
                .map(view -> formHelper.transHistoryForm(view, processMap.get(view.getPtNo()), user, lType))
                .collect(Collectors.toList());
    }

    public List<FormInfo> findSignedByUserAndOrg(String orguuid, Date from, Date to, String executorUuid, LanguageType lType) throws ProcessRestException {
        return this.findSignedByUser(from, to, executorUuid, lType).stream()
                .filter(form -> form.getCompanyUuid().equals(orguuid))
                .collect(Collectors.toList());
    }

    public List<ButtonInfo> combinFormBtn(String formId, String executorUuid) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(formId), "原型確認單號為空值！");
        PtCheck wti = wtiDao.findByPtNo(formId);
        Preconditions.checkArgument(wti != null, "查詢不到原型確認資訊 單號:" + formId);
        User executor = WkUserCache.getInstance().findByUUID(executorUuid);
        Preconditions.checkArgument(executor != null, "傳送的成員uuid，無法訪查出有效成員資訊 uuid:" + executorUuid);
        List<ButtonInfo> btns = Lists.newArrayList();
        processHelper.createInvaildBtn(btns, wti, executor);
        processHelper.createRollBackBtn(btns, wti, executor);
        processHelper.createRecoveryBtn(btns, wti, executor);
        processHelper.createSignBtn(btns, wti, executor);
        return btns;
    }

    public BatchSignedResult doBatchSign(List<String> formIds, String executorUuid) {
        Preconditions.checkArgument(formIds != null && !formIds.isEmpty(), "傳送的formIds 不可為空值");
        User executor = WkUserCache.getInstance().findByUUID(executorUuid);
        Preconditions.checkArgument(executor != null, "傳送的成員uuid，無法訪查出有效成員資訊 uuid:" + executorUuid);
        List<PtCheck> wtis = wtiDao.findByPtNoIn(formIds);
        Preconditions.checkArgument(wtis != null && !wtis.isEmpty(), "無法產生有效批次簽核資訊 formIds = " + formIds);
        return processHelper.doBatchSign(wtis, executor);
    }

    public FormInfo doActionImpl(String action, Map<String, Object> inputData, String executorUuid, LanguageType lType) throws ProcessRestException {
        String[] param = action.split(",");
        Preconditions.checkArgument(param.length == 2, "簽核action參數無法切割為效數值 action = " + action);
        Preconditions.checkArgument(!Strings.isNullOrEmpty(param[1]), "簽核action參數-執行方法為空值 action = " + action);
        User executor = WkUserCache.getInstance().findByUUID(executorUuid);
        Preconditions.checkArgument(executor != null, "傳送的執行者uuid，無法訪查出有效成員資訊 uuid:" + executorUuid);
        PtCheck wti = wtiDao.findByPtNo(param[0]);
        Preconditions.checkArgument(wti != null, "查詢不到原型確認資訊 單號:" + param[0]);

        wti = processHelper.doAction(ActionType.valueOf(param[1]), wti, executor, inputData);
        return formHelper.transform(wti, executor, lType);
    }

    public FormSigningType findFormTypeForUser(String formId, String executorUuid) {
        User executor = WkUserCache.getInstance().findByUUID(executorUuid);
        Preconditions.checkArgument(executor != null, "傳送的成員uuid，無法訪查出有效成員資訊 uuid:" + executorUuid);
        PtSignInfo wtsi = wtsiDao.findByPtNo(formId);
        Preconditions.checkArgument(wtsi != null, "查詢不到需求單位相關流程 單號:" + formId);
        return formHelper.findFormTypeForUser(wtsi, executor);
    }

}
